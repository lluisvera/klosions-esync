﻿namespace klsync
{
    partial class frGraellaTyC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frGraellaTyC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTyC = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCargarArticulosTyC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBuscaArt = new System.Windows.Forms.ToolStripButton();
            this.tsSincronizarTyC = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonReseteoTyC = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgvArticulosTyC = new System.Windows.Forms.DataGridView();
            this.dgvArticulosSimilaresTyC = new System.Windows.Forms.DataGridView();
            this.dgvGraella = new System.Windows.Forms.DataGridView();
            this.contextMenuStripOpciones = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.oPCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.seleccionarTodosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbGenerarTodasCombinaciones = new System.Windows.Forms.ToolStripButton();
            this.tabControlGraella = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvCruzados = new System.Windows.Forms.DataGridView();
            this.contextMenuStripVerificaciones = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sincronizarSeleccionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCruzados = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTyC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosTyC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosSimilaresTyC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGraella)).BeginInit();
            this.contextMenuStripOpciones.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabControlGraella.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCruzados)).BeginInit();
            this.contextMenuStripVerificaciones.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTyC
            // 
            this.toolStripTyC.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripTyC.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripTyC.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCargarArticulosTyC,
            this.toolStripButtonBuscaArt,
            this.tsSincronizarTyC,
            this.toolStripButtonReseteoTyC});
            this.toolStripTyC.Location = new System.Drawing.Point(0, 0);
            this.toolStripTyC.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.toolStripTyC.Name = "toolStripTyC";
            this.toolStripTyC.Padding = new System.Windows.Forms.Padding(10, 5, 1, 5);
            this.toolStripTyC.Size = new System.Drawing.Size(1166, 49);
            this.toolStripTyC.TabIndex = 1;
            this.toolStripTyC.Text = "toolStrip1";
            // 
            // toolStripButtonCargarArticulosTyC
            // 
            this.toolStripButtonCargarArticulosTyC.Image = global::klsync.Properties.Resources.refresh_button;
            this.toolStripButtonCargarArticulosTyC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCargarArticulosTyC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCargarArticulosTyC.Name = "toolStripButtonCargarArticulosTyC";
            this.toolStripButtonCargarArticulosTyC.Size = new System.Drawing.Size(158, 36);
            this.toolStripButtonCargarArticulosTyC.Text = "Cargar Artículos";
            this.toolStripButtonCargarArticulosTyC.Click += new System.EventHandler(this.toolStripButtonBusquedaTyC_Click);
            // 
            // toolStripButtonBuscaArt
            // 
            this.toolStripButtonBuscaArt.Image = global::klsync.Properties.Resources.magnifying_glass;
            this.toolStripButtonBuscaArt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonBuscaArt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBuscaArt.Name = "toolStripButtonBuscaArt";
            this.toolStripButtonBuscaArt.Size = new System.Drawing.Size(92, 36);
            this.toolStripButtonBuscaArt.Text = "Buscar";
            this.toolStripButtonBuscaArt.Click += new System.EventHandler(this.toolStripButtonBuscaArt_Click);
            // 
            // tsSincronizarTyC
            // 
            this.tsSincronizarTyC.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsSincronizarTyC.Image = ((System.Drawing.Image)(resources.GetObject("tsSincronizarTyC.Image")));
            this.tsSincronizarTyC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsSincronizarTyC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsSincronizarTyC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSincronizarTyC.Name = "tsSincronizarTyC";
            this.tsSincronizarTyC.Size = new System.Drawing.Size(179, 36);
            this.tsSincronizarTyC.Text = "Guarda y actualizar";
            this.tsSincronizarTyC.Click += new System.EventHandler(this.tsSincronizarTyC_Click);
            // 
            // toolStripButtonReseteoTyC
            // 
            this.toolStripButtonReseteoTyC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReseteoTyC.Image")));
            this.toolStripButtonReseteoTyC.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonReseteoTyC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReseteoTyC.Name = "toolStripButtonReseteoTyC";
            this.toolStripButtonReseteoTyC.Size = new System.Drawing.Size(164, 36);
            this.toolStripButtonReseteoTyC.Text = "Resetear producto";
            this.toolStripButtonReseteoTyC.Click += new System.EventHandler(this.toolStripButtonReseteoTyC_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvGraella);
            this.splitContainer1.Size = new System.Drawing.Size(1152, 532);
            this.splitContainer1.SplitterDistance = 557;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvArticulosTyC);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvArticulosSimilaresTyC);
            this.splitContainer2.Size = new System.Drawing.Size(557, 532);
            this.splitContainer2.SplitterDistance = 315;
            this.splitContainer2.TabIndex = 0;
            // 
            // dgvArticulosTyC
            // 
            this.dgvArticulosTyC.AllowUserToAddRows = false;
            this.dgvArticulosTyC.AllowUserToDeleteRows = false;
            this.dgvArticulosTyC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvArticulosTyC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosTyC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvArticulosTyC.Location = new System.Drawing.Point(0, 0);
            this.dgvArticulosTyC.Name = "dgvArticulosTyC";
            this.dgvArticulosTyC.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvArticulosTyC.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvArticulosTyC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosTyC.ShowCellToolTips = false;
            this.dgvArticulosTyC.Size = new System.Drawing.Size(557, 315);
            this.dgvArticulosTyC.TabIndex = 1;
            this.dgvArticulosTyC.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArticulosTyC_CellClick);
            this.dgvArticulosTyC.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArticulosTyC_CellValueChanged);
            this.dgvArticulosTyC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvArticulosTyC_KeyUp);
            // 
            // dgvArticulosSimilaresTyC
            // 
            this.dgvArticulosSimilaresTyC.AllowUserToAddRows = false;
            this.dgvArticulosSimilaresTyC.AllowUserToDeleteRows = false;
            this.dgvArticulosSimilaresTyC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvArticulosSimilaresTyC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosSimilaresTyC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvArticulosSimilaresTyC.Location = new System.Drawing.Point(0, 0);
            this.dgvArticulosSimilaresTyC.Name = "dgvArticulosSimilaresTyC";
            this.dgvArticulosSimilaresTyC.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvArticulosSimilaresTyC.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvArticulosSimilaresTyC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvArticulosSimilaresTyC.ShowCellToolTips = false;
            this.dgvArticulosSimilaresTyC.Size = new System.Drawing.Size(557, 213);
            this.dgvArticulosSimilaresTyC.TabIndex = 2;
            // 
            // dgvGraella
            // 
            this.dgvGraella.AllowUserToAddRows = false;
            this.dgvGraella.AllowUserToDeleteRows = false;
            this.dgvGraella.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGraella.ContextMenuStrip = this.contextMenuStripOpciones;
            this.dgvGraella.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGraella.Location = new System.Drawing.Point(0, 0);
            this.dgvGraella.Name = "dgvGraella";
            this.dgvGraella.Size = new System.Drawing.Size(591, 532);
            this.dgvGraella.TabIndex = 1;
            this.dgvGraella.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvGraella_ColumnHeaderMouseDoubleClick);
            this.dgvGraella.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvGraella_RowHeaderMouseDoubleClick);
            // 
            // contextMenuStripOpciones
            // 
            this.contextMenuStripOpciones.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuStripOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oPCIONESToolStripMenuItem,
            this.toolStripMenuItem1,
            this.seleccionarTodosToolStripMenuItem});
            this.contextMenuStripOpciones.Name = "contextMenuStripOpciones";
            this.contextMenuStripOpciones.Size = new System.Drawing.Size(203, 62);
            // 
            // oPCIONESToolStripMenuItem
            // 
            this.oPCIONESToolStripMenuItem.Enabled = false;
            this.oPCIONESToolStripMenuItem.Name = "oPCIONESToolStripMenuItem";
            this.oPCIONESToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.oPCIONESToolStripMenuItem.Text = "OPCIONES";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(199, 6);
            // 
            // seleccionarTodosToolStripMenuItem
            // 
            this.seleccionarTodosToolStripMenuItem.Name = "seleccionarTodosToolStripMenuItem";
            this.seleccionarTodosToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.seleccionarTodosToolStripMenuItem.Text = "Seleccionar todos";
            this.seleccionarTodosToolStripMenuItem.Click += new System.EventHandler(this.seleccionarTodosToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbGenerarTodasCombinaciones});
            this.toolStrip1.Location = new System.Drawing.Point(0, 620);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1166, 39);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbGenerarTodasCombinaciones
            // 
            this.tsbGenerarTodasCombinaciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tsbGenerarTodasCombinaciones.Image = ((System.Drawing.Image)(resources.GetObject("tsbGenerarTodasCombinaciones.Image")));
            this.tsbGenerarTodasCombinaciones.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbGenerarTodasCombinaciones.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGenerarTodasCombinaciones.Name = "tsbGenerarTodasCombinaciones";
            this.tsbGenerarTodasCombinaciones.Size = new System.Drawing.Size(221, 36);
            this.tsbGenerarTodasCombinaciones.Text = "Generar Todas las Combinaciones";
            this.tsbGenerarTodasCombinaciones.ToolTipText = "Generar Todas\r\nlas Combinaciones";
            this.tsbGenerarTodasCombinaciones.Click += new System.EventHandler(this.tsbGenerarTodasCombinaciones_Click);
            // 
            // tabControlGraella
            // 
            this.tabControlGraella.Controls.Add(this.tabPage1);
            this.tabControlGraella.Controls.Add(this.tabPage2);
            this.tabControlGraella.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGraella.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tabControlGraella.Location = new System.Drawing.Point(0, 49);
            this.tabControlGraella.Name = "tabControlGraella";
            this.tabControlGraella.SelectedIndex = 0;
            this.tabControlGraella.Size = new System.Drawing.Size(1166, 571);
            this.tabControlGraella.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1158, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Principal";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvCruzados);
            this.tabPage2.Controls.Add(this.toolStrip2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1158, 538);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Check";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvCruzados
            // 
            this.dgvCruzados.AllowUserToAddRows = false;
            this.dgvCruzados.AllowUserToDeleteRows = false;
            this.dgvCruzados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCruzados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCruzados.ContextMenuStrip = this.contextMenuStripVerificaciones;
            this.dgvCruzados.Location = new System.Drawing.Point(8, 45);
            this.dgvCruzados.Name = "dgvCruzados";
            this.dgvCruzados.ReadOnly = true;
            this.dgvCruzados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCruzados.Size = new System.Drawing.Size(1142, 487);
            this.dgvCruzados.TabIndex = 1;
            this.dgvCruzados.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCruzados_CellContentDoubleClick);
            this.dgvCruzados.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvCruzados_CellMouseDoubleClick);
            // 
            // contextMenuStripVerificaciones
            // 
            this.contextMenuStripVerificaciones.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.contextMenuStripVerificaciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarSeleccionToolStripMenuItem});
            this.contextMenuStripVerificaciones.Name = "contextMenuStripVerificaciones";
            this.contextMenuStripVerificaciones.Size = new System.Drawing.Size(203, 28);
            // 
            // sincronizarSeleccionToolStripMenuItem
            // 
            this.sincronizarSeleccionToolStripMenuItem.Name = "sincronizarSeleccionToolStripMenuItem";
            this.sincronizarSeleccionToolStripMenuItem.Size = new System.Drawing.Size(202, 24);
            this.sincronizarSeleccionToolStripMenuItem.Text = "Sincronizar selección";
            this.sincronizarSeleccionToolStripMenuItem.Click += new System.EventHandler(this.sincronizarSeleccionToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCruzados,
            this.toolStripButton1});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1152, 39);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonCruzados
            // 
            this.toolStripButtonCruzados.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCruzados.Image")));
            this.toolStripButtonCruzados.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCruzados.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCruzados.Name = "toolStripButtonCruzados";
            this.toolStripButtonCruzados.Size = new System.Drawing.Size(211, 36);
            this.toolStripButtonCruzados.Text = "Verificar combinaciones";
            this.toolStripButtonCruzados.Click += new System.EventHandler(this.toolStripButtonCruzados_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::klsync.adMan.Resources.cogwheel28;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(271, 36);
            this.toolStripButton1.Text = "Verificar Combinaciones Visibles";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // frGraellaTyC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 659);
            this.Controls.Add(this.tabControlGraella);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripTyC);
            this.Name = "frGraellaTyC";
            this.Text = "Tallas y Colores";
            this.toolStripTyC.ResumeLayout(false);
            this.toolStripTyC.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosTyC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosSimilaresTyC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGraella)).EndInit();
            this.contextMenuStripOpciones.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControlGraella.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCruzados)).EndInit();
            this.contextMenuStripVerificaciones.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripTyC;
        private System.Windows.Forms.ToolStripButton toolStripButtonCargarArticulosTyC;
        private System.Windows.Forms.ToolStripButton toolStripButtonBuscaArt;
        private System.Windows.Forms.ToolStripButton tsSincronizarTyC;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dgvArticulosTyC;
        private System.Windows.Forms.DataGridView dgvArticulosSimilaresTyC;
        private System.Windows.Forms.DataGridView dgvGraella;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripOpciones;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oPCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbGenerarTodasCombinaciones;
        private System.Windows.Forms.ToolStripButton toolStripButtonReseteoTyC;
        private System.Windows.Forms.TabControl tabControlGraella;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonCruzados;
        private System.Windows.Forms.DataGridView dgvCruzados;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripVerificaciones;
        private System.Windows.Forms.ToolStripMenuItem sincronizarSeleccionToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}