﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Data;
using System.IO;
using MySql.Data.MySqlClient;

namespace klsync
{
    class csXMLConfig
    {

        Utilidades.csLogErrores cslog = new Utilidades.csLogErrores();
        static Utilidades.csConexiones dbConnectEsync = new Utilidades.csConexiones();



        public void añadirNodo(string modeAp, string nombreConexion, string ServerPS, string DatabasePS, string UserPS,
            string PasswordPS, string APIUrl, string UserFTP, string PasswordFTP, string ServerA3, string DatabaseA3, string UserA3, string PasswordA3,
            string usarClienteGenerico, string ClienteGenerico, string Serie, string VersionPS, string docDestino, string Analitica,
            string rutaA3, string rutaFicConfig, string rutaImagenes, string rutaFTPImagenes, string nombreServidor, string rutaRemotaImg, string almacenA3, string eCommerce, string categoria,
            string ivaIncluido, string defaultLang, bool integratedSecurity, string defCustomerGroup, string usaPPago, string estadosPedidosBloqueados, string modoTallasYColores)
        {

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");
            XmlNode nodo = xDoc.CreateNode(XmlNodeType.Element, "conexion", null);
            ((XmlElement)nodo).SetAttribute("id", nombreConexion);
            //Creo el contenido del nodo
            //NODOS PRESTASHOP
            XmlNode nodeModeAp = xDoc.CreateElement("ServidorPS");
            nodeModeAp.InnerText = modeAp;
            XmlNode nodeServidorPS = xDoc.CreateElement("ServidorPS");
            nodeServidorPS.InnerText = ServerPS;
            nodo.AppendChild(nodeServidorPS);
            XmlNode nodeDatabasePS = xDoc.CreateElement("DatabasePS");
            nodeDatabasePS.InnerText = DatabasePS;
            nodo.AppendChild(nodeDatabasePS);
            XmlNode nodeUsuarioPS = xDoc.CreateElement("UsuarioPS");
            nodeUsuarioPS.InnerText = UserPS;
            nodo.AppendChild(nodeUsuarioPS);
            XmlNode nodePasswordPS = xDoc.CreateElement("PasswordPS");
            nodePasswordPS.InnerText = PasswordPS;
            nodo.AppendChild(nodePasswordPS);
            XmlNode nodeAPIUrl = xDoc.CreateElement("APIUrl");
            nodeAPIUrl.InnerText = PasswordPS;
            nodo.AppendChild(nodeAPIUrl);
            XmlNode nodeUsuarioFTP = xDoc.CreateElement("UsuarioFTP");
            nodeUsuarioPS.InnerText = UserFTP;
            nodo.AppendChild(nodeUsuarioPS);
            XmlNode nodePassworFTP = xDoc.CreateElement("PasswordFTP");
            nodePasswordPS.InnerText = PasswordFTP;
            nodo.AppendChild(nodePasswordPS);
            //NODOS A3ERP
            XmlNode nodeServidorA3 = xDoc.CreateElement("ServidorA3");
            nodeServidorA3.InnerText = ServerA3;
            nodo.AppendChild(nodeServidorA3);
            XmlNode nodeDatabaseA3 = xDoc.CreateElement("DatabaseA3");
            nodeDatabaseA3.InnerText = DatabaseA3;
            nodo.AppendChild(nodeDatabaseA3);
            XmlNode nodeUsuarioA3 = xDoc.CreateElement("UsuarioA3");
            nodeUsuarioA3.InnerText = UserA3;
            nodo.AppendChild(nodeUsuarioA3);
            XmlNode nodePasswordA3 = xDoc.CreateElement("PasswordA3");
            nodePasswordA3.InnerText = PasswordA3;
            nodo.AppendChild(nodePasswordA3);
            //Datos Generales
            XmlNode nodeUsarClienteGenerico = xDoc.CreateElement("UsarClienteGenerico");
            nodeUsarClienteGenerico.InnerText = usarClienteGenerico;
            nodo.AppendChild(nodeUsarClienteGenerico);
            XmlNode nodeClienteGenerico = xDoc.CreateElement("ClienteGenerico");
            nodeClienteGenerico.InnerText = ClienteGenerico;
            nodo.AppendChild(nodeClienteGenerico);
            XmlNode nodeSerieDestino = xDoc.CreateElement("SerieDestino");
            nodeSerieDestino.InnerText = Serie;
            nodo.AppendChild(nodeSerieDestino);
            XmlNode nodeVersionPS = xDoc.CreateElement("VersionPS");
            nodeVersionPS.InnerText = VersionPS;
            nodo.AppendChild(nodeVersionPS);
            XmlNode nodeDocDestino = xDoc.CreateElement("DocDestino");
            nodeDocDestino.InnerText = docDestino;
            nodo.AppendChild(nodeDocDestino);
            XmlNode nodeAnalitica = xDoc.CreateElement("Analitica");
            nodeAnalitica.InnerText = Analitica;
            nodo.AppendChild(nodeAnalitica);
            XmlNode nodeRutaA3 = xDoc.CreateElement("RutaA3");
            nodeRutaA3.InnerText = rutaA3;
            nodo.AppendChild(nodeRutaA3);
            XmlNode nodeRutaficConfig = xDoc.CreateElement("RutaFicConfig");
            nodeRutaficConfig.InnerText = rutaFicConfig;
            nodo.AppendChild(nodeRutaficConfig);
            XmlNode nodeRutaImagenes = xDoc.CreateElement("RutaImagenes");
            nodeRutaImagenes.InnerText = rutaImagenes;
            nodo.AppendChild(nodeRutaImagenes);
            XmlNode nodeRutaImagenesFTP = xDoc.CreateElement("RutaFTPImagenes");
            nodeRutaImagenesFTP.InnerText = rutaImagenes;
            nodo.AppendChild(nodeRutaImagenesFTP);
            XmlNode nodeNombreServidor = xDoc.CreateElement("NombreServidor");
            nodeNombreServidor.InnerText = rutaImagenes;
            nodo.AppendChild(nodeNombreServidor);
            XmlNode nodeRutaRemotaImg = xDoc.CreateElement("RutaRemotaImagenes");
            nodeRutaRemotaImg.InnerText = rutaImagenes;
            nodo.AppendChild(nodeRutaRemotaImg);
            XmlNode nodeAlmacenA3 = xDoc.CreateElement("AlmacenA3");
            nodeAlmacenA3.InnerText = almacenA3;
            nodo.AppendChild(nodeAlmacenA3);
            XmlNode nodeEcommerce = xDoc.CreateElement("Ecommerce");
            nodeEcommerce.InnerText = eCommerce;
            nodo.AppendChild(nodeEcommerce);
            XmlNode nodeCategoria = xDoc.CreateElement("Categoria");
            nodeCategoria.InnerText = categoria;
            nodo.AppendChild(nodeCategoria);
            XmlNode nodeIvaIncluido = xDoc.CreateElement("IvaIncluido");
            nodeIvaIncluido.InnerText = ivaIncluido;
            nodo.AppendChild(nodeIvaIncluido);
            XmlNode nodeDefaultLang = xDoc.CreateElement("DefaultLang");
            nodeDefaultLang.InnerText = ivaIncluido;
            nodo.AppendChild(nodeDefaultLang);
            XmlNode ModTxt = xDoc.CreateElement("ModTxt");
            ModTxt.InnerText = ModTxt.ToString();
            nodo.AppendChild(ModTxt);
            XmlNode nodeIntegratedSecurity = xDoc.CreateElement("IntegratedSecurity");
            nodeIntegratedSecurity.InnerText = integratedSecurity.ToString();
            nodo.AppendChild(nodeIntegratedSecurity);
            XmlNode nodeTienda = xDoc.CreateElement("Tienda");
            nodeTienda.InnerText = nodeTienda.ToString();
            nodo.AppendChild(nodeTienda);
            XmlNode nodeBackOffice = xDoc.CreateElement("BackOffice");
            nodeBackOffice.InnerText = nodeBackOffice.ToString();
            nodo.AppendChild(nodeBackOffice);
            XmlNode fpa3 = xDoc.CreateElement("FPA3");
            fpa3.InnerText = fpa3.ToString();
            nodo.AppendChild(fpa3);
            XmlNode ServirPedido = xDoc.CreateElement("ServirPedido");
            ServirPedido.InnerText = ServirPedido.ToString();
            nodo.AppendChild(ServirPedido);
            XmlNode EmailEnvio = xDoc.CreateElement("EmailEnvio");
            EmailEnvio.InnerText = EmailEnvio.ToString();
            nodo.AppendChild(EmailEnvio);
            XmlNode EmailDestino = xDoc.CreateElement("EmailDestino");
            EmailDestino.InnerText = EmailDestino.ToString();
            nodo.AppendChild(EmailDestino);
            XmlNode EmailSMTP = xDoc.CreateElement("EmailSMTP");
            EmailSMTP.InnerText = EmailSMTP.ToString();
            nodo.AppendChild(EmailSMTP);
            XmlNode UsuarioEmail = xDoc.CreateElement("UsuarioEmail");
            UsuarioEmail.InnerText = UsuarioEmail.ToString();
            nodo.AppendChild(UsuarioEmail);
            XmlNode UsuarioPass = xDoc.CreateElement("UsuarioPass");
            UsuarioPass.InnerText = UsuarioPass.ToString();
            nodo.AppendChild(UsuarioPass);
            XmlNode UserWS = xDoc.CreateElement("UserWS");
            UserWS.InnerText = UserWS.ToString();
            nodo.AppendChild(UserWS);
            XmlNode PassWS = xDoc.CreateElement("PassWS");
            PassWS.InnerText = PassWS.ToString();
            nodo.AppendChild(PassWS);
            XmlNode defaultCustomerGroup = xDoc.CreateElement("DefaultCustomerGroup");
            defaultCustomerGroup.InnerText = defaultCustomerGroup.ToString();
            nodo.AppendChild(defaultCustomerGroup);
            XmlNode usaProntoPago = xDoc.CreateElement("UsaProntoPago");
            usaProntoPago.InnerText = usaProntoPago.ToString();
            nodo.AppendChild(usaProntoPago);
            XmlNode estadoPedidosBloqueados = xDoc.CreateElement("EstadosPedidoBloqueados");
            estadoPedidosBloqueados.InnerText = estadoPedidosBloqueados.ToString();
            nodo.AppendChild(estadoPedidosBloqueados);
            XmlNode modoTyC = xDoc.CreateElement("ModoTallasYColores");
            modoTyC.InnerText = modoTallasYColores.ToString();
            nodo.AppendChild(modoTyC);


            xDoc.DocumentElement.AppendChild(nodo);
            xDoc.Save(csGlobal.rutaFicConfig + "miXML.xml");
        }

        static void xx()
        {
            var values = new Dictionary<string, string>();
            values.Add("A", "uppercase letter A");
            values.Add("c", "lowercase letter C");

            // Use inline "out string" with TryGetValue.
        }


        public string[] cargarConexionesXML()
        {
            try
            {
                string rutaFicConfig = csGlobal.rutaFicConfig + "miXML.xml";
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(rutaFicConfig);

                XmlNodeList conexiones = xDoc.GetElementsByTagName("conexion");
                XmlNodeList lista = xDoc.GetElementsByTagName("conexion");
                string[] enlaces = new string[lista.Count];
                int i = 0;
                
                foreach (XmlElement nodo in lista)
                {
                    enlaces[i] = nodo.GetAttribute("id").ToString();
                    i++;
                }

                return enlaces;
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
                return null;
            }
           
        }

        public void cargarProvinciasA3()
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                csGlobal.dtProvinciasA3 = sql.obtenerDatosSQLScript("SELECT ID,LTRIM(CODPROVI) AS CODPROVI,NOMPROVI, RPST_ID_PROV FROM PROVINCI WHERE RPST_ID_PROV IS NOT NULL ");
                if (csGlobal.dtProvinciasA3.Rows.Count == 0) 
                { 
                    sql.obtenerDatosSQLScript("UPDATE PROVINCI SET RPST_ID_PROV=CODPROVI*1 WHERE CODPROVI*1<53");
                    csGlobal.dtProvinciasA3 = sql.obtenerDatosSQLScript("SELECT ID,LTRIM(CODPROVI) AS CODPROVI,NOMPROVI, RPST_ID_PROV FROM PROVINCI");
                }
            }
            catch (Exception ex)
            {
            }
        }


        public void cargarTiposIVA()
        {
            try
            {
                csGlobal.a3tiposIVA.Clear();
                DataTable dtTiposIVA = new DataTable();
                csSqlConnects sql = new csSqlConnects();
                dtTiposIVA = sql.obtenerDatosSQLScript("SELECT RPST_ID_IVA,TIPIVA  FROM TIPOIVA WHERE RPST_ID_IVA IS NOT NULL");
                if (dtTiposIVA!=null && dtTiposIVA.Rows.Count > 0)
                {

                    foreach (DataRow dr in dtTiposIVA.Rows)
                    {
                        csGlobal.a3tiposIVA.Add(dr["RPST_ID_IVA"].ToString(), dr["TIPIVA"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modeDebug) csUtilidades.log(ex.Message);
            }

        }


        public void cargarRegimenesImpuestos() {

            try
            {
                csGlobal.a3RegimenesImpto.Clear();
                DataTable dtTiposIVA = new DataTable();
                csSqlConnects sql = new csSqlConnects();
                dtTiposIVA = sql.obtenerDatosSQLScript("SELECT RPST_ID_REGIVA,REGIVA FROM REGIVA WHERE RPST_ID_REGIVA IS NOT NULL");

                if (dtTiposIVA != null && dtTiposIVA.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtTiposIVA.Rows)
                    {
                        csGlobal.a3RegimenesImpto.Add(dr["RPST_ID_REGIVA"].ToString(), dr["REGIVA"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modeDebug) csUtilidades.log(ex.Message);
            }

        }

        public bool validarConexionMySQLConfig()
        {
            try
            {
                //82.98.175.235

                //cslog.guardarLogProceso("Paso 0");

                string valorValidacion = null;
                csMySqlConnect mysql = new klsync.csMySqlConnect();
                //cslog.guardarLogProceso("Paso 1 - Conexion");
                valorValidacion = csUtilidades.obtenerValorMySQL(dbConnectEsync.cadenaConexionEsyncDB(), "select count(*) from config");
                //cslog.guardarLogProceso("Paso 2 - Conexion Realizada " + valorValidacion + " filas");

                if (string.IsNullOrEmpty(valorValidacion))
                {
                    csUtilidades.log("Error de conexión a DB de Configuración");
                    Program.guardarErrorFichero("Error de conexión a DB de Configuración");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                string messageerror = ex.Message;
                return false;
            }

        }

        public void cargarConfiguracionConexionesXML()
         {
            try
            {
                csGlobal.localConfig = true;
                string nombreConexion = "";
                string connection_string = "";
                csMySqlConnect mySql = new csMySqlConnect();
                DataTable dtConfig = new DataTable();
                DataTable dtReminder = new DataTable();

                //Si tenemos problemas de conexión, añadimos la opción de que se puedan gestionar los datos de configuración en el XML
                if (csGlobal.conexionDB.Contains("_LOCALCONFIG"))
                {
                    //csGlobal.conexionDB = csGlobal.conexionDB.Replace("_LOCALCONFIG", "");
                    csGlobal.localConfig = true;
                }
                //CARGO LOS FICHEROS DE CONFIGURACIÓN DE ESYNC
                else
                {
                    if (csGlobal.modeDebug) csUtilidades.log("Paso 1 conexion");
                    if (!validarConexionMySQLConfig()) csUtilidades.log("problemas conexión DB Configuración");
                                  
                    //Obtenemos los datos de configuración
                    dtConfig = csUtilidades.conectarMySQL(dbConnectEsync.cadenaConexionEsyncDB(), "select * from config where connection_name = '" + csGlobal.conexionDB + "'");
                    
                    //cslog.guardarLogProceso("Paso 3 - Datos Conexion realizados");
                    dtReminder = csUtilidades.conectarMySQL(dbConnectEsync.cadenaConexionEsyncDB(),"select * from reminder where connection_name = '" + csGlobal.conexionDB + "'");
                    
                    //cslog.guardarLogProceso("Paso 4");
                    if (csGlobal.modeDebug) csUtilidades.log("DATOS OBTENIDOS: INICIO ITERACIÓN");
                }
                
               

                 
                if (dtConfig.hasRows())
                {
                    foreach (DataRow dr in dtConfig.Rows)
                    {
                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 1");

                        string fecha_actual = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        //csUtilidades.ConsultaMySQL(connection_string, string.Format("update config set updated_at = '{0}' where id = {1}", fecha_actual, dr["id"].ToString()));
                        csUtilidades.ConsultaMySQL(dbConnectEsync.cadenaConexionEsyncDB(), string.Format("update config set updated_at = '{0}' where id = {1}", fecha_actual, dr["id"].ToString()));
//

                        csGlobal.modeAp = dr["mode_ap"].ToString();
                        csGlobal.modeExternalConnection = dr["mode_ext_connection"].ToString();

                        csGlobal.nombreServidor = dr["connection_name"].ToString();
                        csGlobal.ServerPS = dr["ps_host"].ToString();

                        // Manejo seguro de la conversión de fecha

                        try
                        {
                            string dateStr = dr["expiration_date"].ToString();
                            DateTime expirationDate;

                            if (DateTime.TryParseExact(dateStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out expirationDate) ||
                            DateTime.TryParseExact(dateStr, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out expirationDate))
                            {
                                csGlobal.expiration_date = expirationDate;
                            }
                            else
                            {
                                string formatoEsperado = "MM/dd/yyyy";
                                expirationDate = csUtilidades.TransformarFormatoInvalido(dateStr, formatoEsperado);
                                csGlobal.expiration_date = expirationDate;
                            }
                        }
                        catch (FormatException ex)
                        {
                            MessageBox.Show($"Formato inválido para la fecha: {ex.Message}\nStack Trace: {ex.StackTrace}");
                        }

                        //                    try
                        //                    {
                        //                        string dateStr = dr["expiration_date"].ToString();
                        //                        DateTime expirationDate;

                        //                        if (DateTime.TryParseExact(dateStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out expirationDate) ||
                        //DateTime.TryParseExact(dateStr, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out expirationDate))
                        //                        {
                        //                            csGlobal.expiration_date = expirationDate;
                        //                        }
                        //                        else
                        //                        {
                        //                            MessageBox.Show($"Error: La fecha '{dateStr}' no se pudo convertir.");
                        //                        }
                        //                    }
                        //                    catch (FormatException ex)
                        //                    {
                        //                        MessageBox.Show($"Formato inválido para la fecha: {ex.Message}\nStack Trace: {ex.StackTrace}");
                        //                    }

                        csGlobal.PortPS = dr["ps_port"].ToString();
                        csGlobal.databasePS = dr["ps_database"].ToString();
                        csGlobal.userPS = dr["ps_username"].ToString();
                        csGlobal.passwordPS = dr["ps_password"].ToString();
                        csGlobal.APIUrl = dr["ps_apiurl"].ToString();
                        csGlobal.webServiceKey = dr["ps_apikey"].ToString();
//
                        //csGlobal.modeAp = dr["mode_ap"].ToString();
                        //csGlobal.modeExternalConnection = dr["mode_ext_connection"].ToString();

                        //csGlobal.nombreServidor = dr["connection_name"].ToString();
                        //csGlobal.ServerPS = dr["ps_host"].ToString();
                        //csGlobal.expiration_date = Convert.ToDateTime(dr["expiration_date"].ToString());
                        //csGlobal.PortPS = dr["ps_port"].ToString();
                        //csGlobal.databasePS = dr["ps_database"].ToString();
                        //csGlobal.userPS = dr["ps_username"].ToString();
                        //csGlobal.passwordPS = dr["ps_password"].ToString();
                        //csGlobal.APIUrl = dr["ps_apiurl"].ToString();
                        //csGlobal.webServiceKey = dr["ps_apikey"].ToString();

                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 2");


                        if (csUtilidades.isKlosionsNetwork())
                        {
                            if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 2.1");

                            csGlobal.ServerA3 = @"KLSBRAIN\SQLEXPRESS";
                            csGlobal.databaseA3 = dr["a3_database"].ToString();

                            csGlobal.userA3 = "sa";
                            csGlobal.passwordA3 = "klosions";
                            csGlobal.serie = "SHOP";
                            csGlobal.rutaFicConfig = @"C:\esync project\klsync\bin\Debug\";
                        }
                        //TODO: AÑADIDO POR FRANCO 190704
                        else 
                        {
                            if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 2.2");

                            csGlobal.ServerA3 = dr["a3_host"].ToString();
                            csGlobal.databaseA3 = dr["a3_database"].ToString();
                            csGlobal.userA3 = dr["a3_username"].ToString();
                            csGlobal.serie = dr["a3_series"].ToString();
                            csGlobal.rutaFicConfig = dr["esync_path"].ToString();
                            csGlobal.passwordA3 = dr["a3_password"].ToString();
                        }
                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 3");



                        //AÑADIR UNA VALIDACIÓN PARA ENTRAR AQUÍ SI EL ENLACE ES CON PRESTASHOP
                        csGlobal.rutaImagenes = procesoCarpetaImagenes(dr["images_path"].ToString());
                        csGlobal.log = dr["log"].ToString();
                        csGlobal.ps_visible_combinations = Convert.ToBoolean(Convert.ToInt32(dr["ps_visible_combinations"].ToString()));

                        csGlobal.ocultarArticuloSiStock0 = dr["hide_product_stock0"].ToString() == "Y" ? true : false;

                        csGlobal.rutaA3 = dr["a3_path"].ToString();
                        csGlobal.usarClienteGenerico = dr["a3_use_generic_user"].ToString();
                        csGlobal.clienteGenerico = dr["a3_generic_user"].ToString();
                        csGlobal.versionPS = dr["ps_version"].ToString();
                        csGlobal.docDestino = dr["a3_doc"].ToString();
                        csGlobal.nivelesAnalitica = dr["a3_analytics"].ToString();
                        csGlobal.nivel1Analitica = dr["a3_analytics_1"].ToString();
                        csGlobal.nivel2Analitica = dr["a3_analytics_2"].ToString();
                        csGlobal.nivel3Analitica = dr["a3_analytics_3"].ToString();

                        csGlobal.almacenA3 = dr["a3_warehouse"].ToString();
                        csGlobal.ecommerce = dr["ecommerce"].ToString();
                        csGlobal.categoria = dr["ps_category"].ToString();
                        csGlobal.id_tax_group= dr["ps_id_tax_rules_group"].ToString();
                        csGlobal.ivaIncluido = dr["tax_included"].ToString();
                        csGlobal.defaultLang = dr["a3_default_lang"].ToString();
                        csGlobal.ModTxt = dr["mod_txt"].ToString();
                        //csGlobal.integratedSecurity = ToBoolean(dr["a3_integrated_security"].ToString());
                        csGlobal.nodeTienda = dr["ps_frontoffice"].ToString();
                        csGlobal.nodeBackOffice = dr["ps_backoffice"].ToString();
                        csGlobal.fpa3 = dr["a3_fpa3"].ToString();
                        csGlobal.ServirPedido = dr["a3_serve_order"].ToString();
                        csGlobal.EmailEnvio = dr["logystics_from"].ToString();
                        csGlobal.EmailDestino = dr["logystics_to"].ToString();
                        csGlobal.EmailSMTP = dr["logystics_host"].ToString();
                        csGlobal.UsuarioEmail = dr["logystics_username"].ToString();
                        csGlobal.UsuarioPass = dr["logystics_password"].ToString();
                        csGlobal.defCustomerGroup = dr["ps_default_customer_group"].ToString();
                        csGlobal.usaProntoPago = dr["a3_pronto_pago"].ToString();
                        csGlobal.estadosPedidosBloqueados = dr["ps_blocked_order_status"].ToString();
                        csGlobal.separadorImagen = dr["image_separator"].ToString();
                        csGlobal.modoTallasYColores = dr["a3_tyc_mode"].ToString();
                        csGlobal.userFTP = "";
                        csGlobal.passwordFTP = "";
                        csGlobal.rutaFTPImagenes = "";
                        csGlobal.rutaRemotaImagenes = "";
                        csGlobal.UserWS = "";
                        csGlobal.PassWS = "";
                        csGlobal.emailNotificacionCliente = dr["email_notify_customer"].ToString();
                        csGlobal.fileExportPath = dr["export_files_path"].ToString();
                        csGlobal.A3ERPConnection = dr["a3erp_connection"].ToString();
                        csGlobal.activarMonotallas = dr["monotallas"].ToString();
                        csGlobal.modeDebug = dr["mode_debug"].ToString()=="Y"?true:false;
                        csGlobal.customerPassword = dr["custom_password"].ToString();
                        csGlobal.preciosAtributosVariables = dr["preciosAtributosVariable"].ToString() == "Y" ? true : false;
                        csGlobal.cupon_as_line = dr["cupon_as_line"].ToString() == "Y" ? true : false;
                        csGlobal.productCupon = dr["product_Cupon"].ToString();
                        csGlobal.gestionPreciosHijos = dr["gestion_precios_hijos"].ToString()== "Y" ? true : false;
                        csGlobal.modoGestionCarteraRPST = dr["gestion_cartera"].ToString() == "REPASAT" ? true : false;
                        csGlobal.tipoContable = Convert.ToInt16(dr["tipoContable"].ToString());
                        csGlobal.nivelProyecto = dr["a3_nivelProyecto"].ToString();
                        csGlobal.reminderPayment = dr["reminder"].ToString() == "Y" ? true : false;
                        csGlobal.articuloRegalo = dr["codart_regalo"].ToString();
                        csGlobal.a3_gestion_precio_dto = dr["a3_gestion_precio_dto"].ToString() == "Y" ? true : false;
                        csGlobal.gestionArticulosPedidos = dr["gestionArticulosPedidos"].ToString() == "Y" ? true : false;
                        csGlobal.usarDocProRefA3 = dr["rpst_usar_docprov_refA3"].ToString() == "Y" ? true : false;
                        csGlobal.caracterCuota = dr["caracterCuota"].ToString() == "Y" ? true : false;
                        csGlobal.combinacionesVisibles = dr["combinacionesVisibles"].ToString() == "Y" ? true : false;
                        csGlobal.medidasA3 = dr["medidasA3"].ToString() == "Y" ? true : false;
                        csGlobal.activarUnidadesNegocio = dr["activar_unidades_negocio"].ToString() == "Y" ? true : false;
                        csGlobal.idUnidadNegocio = dr["id_unidad_negocio"].ToString();
                        csGlobal.sync_adv_mode = dr["mode_sync_adv"].ToString() == "Y" ? true : false;
                        csGlobal.id_conexion_externa = dr["id_conexion_ext"].ToString();
                        csGlobal.isODBCConnection = dr["isODBCConnection"].ToString() == "Y" ? true : false;
                        csGlobal.preciosA3 = dr["preciosA3"].ToString() == "Y" ? true : false;
                        csGlobal.FormaPagoA3 = dr["FormaPagoA3"].ToString() == "Y" ? true : false;
                        csGlobal.DocPagoA3 = dr["DocPagoA3"].ToString() == "Y" ? true : false;
                        csGlobal.familia_art = dr["familia_art"].ToString();
                        csGlobal.subfamilia_art = dr["subfamilia_art"].ToString();
                        csGlobal.marcas_art = dr["marcas_art"].ToString();
                        csGlobal.filtro_art = dr["filtro_art"].ToString();
                        csGlobal.tipo_art = dr["tipo_art"].ToString();
                        csGlobal.filtro_fecha = dr["filtro_fecha"].ToString();

                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 4");


                        //con esta función cargo los tipos de IVA en el sistema con sus asociaciones entre A3ERP y Repasat
                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                        {
                            cargarTiposIVA();
                            cargarRegimenesImpuestos();
                            cargarProvinciasA3();
                        }

                        //cargo información sobre ticket bai
                        TicketBai.csTicketBai ticketbai = new TicketBai.csTicketBai();
                        csGlobal.ticketBaiActivo = ticketbai.ticketBaiActivo();

                        csSqlConnects sql = new csSqlConnects();
                        csGlobal.versionA3ERP = Convert.ToInt32(sql.obtenerCampoTabla("select version from [A3ERP$SISTEMA].[dbo].[VERSION] where LIBRERIA='A3ERP'"));

                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 5");


                        if (csGlobal.reminderPayment)
                        {
                            foreach (DataRow drRemind in dtReminder.Rows)
                            {
                                csGlobal.remindSendToCustomer = drRemind["send_to_customer"].ToString() == "Y" ? true : false;
                                csGlobal.emailNotifyEmpresa = drRemind["email_notify_empresa"].ToString();
                                csGlobal.mailerHost = drRemind["mailer_host"].ToString();
                                csGlobal.mailerFrom = drRemind["mailer_from"].ToString();
                                csGlobal.mailerTo = drRemind["mailer_to"].ToString();
                                csGlobal.mailerUserName = drRemind["mailer_username"].ToString();
                                csGlobal.mailerPassword = drRemind["mailer_password"].ToString();
                                csGlobal.reminderDaysBefore = Convert.ToInt16(drRemind["days_before_filter"].ToString());
                                csGlobal.reminderDaysAfter =Convert.ToInt16(drRemind["days_after_filter"].ToString());
                                csGlobal.reminderExportPath = drRemind["export_files_path"].ToString();
                                
                            }
                        }
                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 6");


                        //CAMBIO preciosAtributos
                        csGlobal.precioAtributos= dr["precioAtributos"].ToString() == "Y" ? true : false;

                        csGlobal.productosConPeso = dr["productosConPeso"].ToString() == "Y" ? true : false;
                        csGlobal.codart_regalo = dr["codart_regalo"].ToString();
                        
                        if (csGlobal.modeAp.ToUpper()=="ESYNC")
                        {
                            if (csGlobal.ServerPS != "")
                            {
                                try 
                                {
                                    //var testQuery = "SELECT ps_configuration.value FROM ps_configuration WHERE ps_configuration.name = 'PS_LANG_DEFAULT'";
                                    //var testResult = mySql.obtenerDatoFromQuery(testQuery);
                                    //MessageBox.Show($"Resultado de prueba: {testResult}");



                                    ////string query = "SELECT  LEFT(`value`, 256) FROM `ps_esportissim`.`ps_configuration` WHERE name IN('PS_LANG_DEFAULT')";
                                    //string query = "SELECT company from ps_customer where id_customer=5205";

                                    csGlobal.defaultIdLangPS = mySql.obtenerDatoFromQuery("Select  ps_configuration.value From   ps_configuration Where   ps_configuration.name ='PS_LANG_DEFAULT'");
                                    //csGlobal.defaultIdLangPS = mySql.obtenerDatoFromQuery(query);
                                }
                                catch (FormatException ex)
                                {
                                    MessageBox.Show($"Formato inválido para la fecha: {ex.Message}\nStack Trace: {ex.StackTrace}");
                                }
                            }
                        }
                    }
                }
                else
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("miXML.xml");

                    XmlNodeList conexiones = xDoc.GetElementsByTagName("conexion");
                    XmlNodeList lista = xDoc.GetElementsByTagName("conexion");

                    //Validación si están todos los nodos
                    foreach (XmlElement nodo in lista)
                    {
                        if (nodo.GetAttribute("id").ToString().ToUpper() == csGlobal.conexionDB.ToUpper())
                        {
                            var matches = nodo.GetElementsByTagName("SeparadorImagen");
                            if (matches.Count == 0)
                            {
                                //MessageBox.Show("no existe el nodo");

                                if (csUtilidades.isKlosionsNetwork())
                                {
                                    csGlobal.ServerA3 = @"KLSBRAIN\SQLEXPRESS";
                                    //csGlobal.databaseA3 = dr["a3_database"].ToString();
                                    //// Para andreu toys
                                    ////csGlobal.userA3 = "ifeu";
                                    ////csGlobal.passwordA3 = "Minotaure0";

                                    //csGlobal.userA3 = "sa";
                                    //csGlobal.passwordA3 = "klosions";
                                    //csGlobal.serie = "SHOP";
                                    csGlobal.rutaFicConfig = @"C:\esync project\klsync\bin\Debug\";
                                }

                                    xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");
           
                                
                                XmlNode conexion = xDoc.SelectSingleNode("//*[@id='" + csGlobal.conexionDB.ToUpper() + "']");
                                if (conexion == null)
                                {
                                    if (csGlobal.modoManual)
                                    {
                                        MessageBox.Show("Revisar el fichero XML de configuración, nombre de nodo y textos");
                                    }
                                }
                                int contador = 0;
                                foreach (XmlNode ancla in conexion.ChildNodes)
                                {
                                    if (ancla.Name == "RutaRemotaImagenes")
                                    {
                                        XmlNode rutaRemotaImg = conexion.ChildNodes[contador];
                                        XmlNode xmlRecordNo = xDoc.CreateNode(XmlNodeType.Element, "SeparadorImagen", null);
                                        xmlRecordNo.InnerText = "-";
                                        conexion.InsertAfter(xmlRecordNo, rutaRemotaImg);
                                        break;
                                    }
                                    contador++;

                                }

                                xDoc.Save(csGlobal.rutaFicConfig + "miXML.xml");
                            }
                        }
                    }

                    //Cargo el contenido de los nodos

                    foreach (XmlElement nodo in lista)
                    {
                        nombreConexion = nodo.GetAttribute("id").ToString().ToUpper();
                        if (nombreConexion == csGlobal.conexionDB.ToUpper())
                        {
                            csGlobal.modeAp = nodo.GetElementsByTagName("ModeAp")[0].InnerText;
                            csGlobal.ServerPS = nodo.GetElementsByTagName("ServidorPS")[0].InnerText;
                            csGlobal.PortPS = nodo.GetElementsByTagName("PuertoPS")[0].InnerText;
                            csGlobal.databasePS = nodo.GetElementsByTagName("DatabasePS")[0].InnerText;
                            csGlobal.userPS = nodo.GetElementsByTagName("UsuarioPS")[0].InnerText;
                            csGlobal.passwordPS = nodo.GetElementsByTagName("PasswordPS")[0].InnerText;
                            csGlobal.APIUrl = nodo.GetElementsByTagName("APIUrl")[0].InnerText;
                            csGlobal.userFTP = nodo.GetElementsByTagName("UsuarioFTP")[0].InnerText;
                            csGlobal.passwordFTP = nodo.GetElementsByTagName("PasswordFTP")[0].InnerText;
                            csGlobal.ServerA3 = nodo.GetElementsByTagName("ServidorA3")[0].InnerText;
                            csGlobal.databaseA3 = nodo.GetElementsByTagName("DatabaseA3")[0].InnerText;
                            csGlobal.userA3 = nodo.GetElementsByTagName("UsuarioA3")[0].InnerText;
                            csGlobal.rutaA3 = nodo.GetElementsByTagName("RutaA3")[0].InnerText;
                            csGlobal.passwordA3 = nodo.GetElementsByTagName("PasswordA3")[0].InnerText;
                            csGlobal.usarClienteGenerico = nodo.GetElementsByTagName("UsarClienteGenerico")[0].InnerText;
                            csGlobal.A3ERPConnection = nodo.GetElementsByTagName("A3ERPConnection")[0].InnerText;
                            // csGlobal.clienteGenerico = nodo.GetElementsByTagName("ClienteGenerico")[0].InnerText;
                            //csGlobal.ServerPS = nodo.GetElementsByTagName("SerieDestino")[0].InnerText;
                            //csGlobal.versionPS = nodo.GetElementsByTagName("VersionPS")[0].InnerText;
                            csGlobal.docDestino = nodo.GetElementsByTagName("DocDestino")[0].InnerText;
                            //string[] analitica = nodo.GetElementsByTagName("Analitica")[0].InnerText.Split(',');
                            //csGlobal.nivelesAnalitica = analitica[1];
                            //csGlobal.nivel1Analitica = analitica[2];
                            //csGlobal.nivel2Analitica = analitica[3];
                            //csGlobal.nivel3Analitica = analitica[4];
                            csGlobal.serie = nodo.GetElementsByTagName("SerieDestino")[0].InnerText;
                            //csGlobal.rutaFicConfig = nodo.GetElementsByTagName("RutaFicConfig")[0].InnerText;
                            //csGlobal.nombreServidor = nodo.GetElementsByTagName("NombreServidor")[0].InnerText;
                            //csGlobal.rutaRemotaImagenes = nodo.GetElementsByTagName("RutaRemotaImagenes")[0].InnerText;
                            //csGlobal.almacenA3 = nodo.GetElementsByTagName("AlmacenA3")[0].InnerText;
                            //csGlobal.ecommerce = nodo.GetElementsByTagName("Ecommerce")[0].InnerText;
                            //csGlobal.categoria = nodo.GetElementsByTagName("Categoria")[0].InnerText;
                            //csGlobal.ivaIncluido = nodo.GetElementsByTagName("IvaIncluido")[0].InnerText;
                            //csGlobal.webServiceKey = nodo.GetElementsByTagName("WebserviceKey")[0].InnerText;
                            //csGlobal.defaultLang = nodo.GetElementsByTagName("DefaultLang")[0].InnerText;
                            //csGlobal.ModTxt = nodo.GetElementsByTagName("ModTxt")[0].InnerText;
                            //csGlobal.integratedSecurity = ToBoolean(nodo.GetElementsByTagName("IntegratedSecurity")[0].InnerText);
                            //csGlobal.nodeTienda = nodo.GetElementsByTagName("Tienda")[0].InnerText;
                            //csGlobal.nodeBackOffice = nodo.GetElementsByTagName("BackOffice")[0].InnerText;
                            //csGlobal.fpa3 = nodo.GetElementsByTagName("FPA3")[0].InnerText;
                            //csGlobal.ServirPedido = nodo.GetElementsByTagName("ServirPedido")[0].InnerText;
                            //csGlobal.EmailEnvio = nodo.GetElementsByTagName("EmailEnvio")[0].InnerText;
                            //csGlobal.EmailDestino = nodo.GetElementsByTagName("EmailDestino")[0].InnerText;
                            //csGlobal.EmailSMTP = nodo.GetElementsByTagName("EmailSMTP")[0].InnerText;
                            //csGlobal.UsuarioEmail = nodo.GetElementsByTagName("UsuarioEmail")[0].InnerText;
                            //csGlobal.UsuarioPass = nodo.GetElementsByTagName("UsuarioPass")[0].InnerText;
                            //csGlobal.UserWS = nodo.GetElementsByTagName("UserWS")[0].InnerText;
                            //csGlobal.PassWS = nodo.GetElementsByTagName("PassWS")[0].InnerText;
                            //csGlobal.defCustomerGroup = nodo.GetElementsByTagName("DefaultCustomerGroup")[0].InnerText;
                            //csGlobal.usaProntoPago = nodo.GetElementsByTagName("UsaProntoPago")[0].InnerText;
                            //csGlobal.estadosPedidosBloqueados = nodo.GetElementsByTagName("EstadosPedidoBloqueados")[0].InnerText;
                            //csGlobal.modoTallasYColores = nodo.GetElementsByTagName("ModoTallasYColores")[0].InnerText;
                            //csGlobal.separadorImagen = nodo.GetElementsByTagName("SeparadorImagen")[0].InnerText;

                            //csGlobal.rutaImagenes = nodo.GetElementsByTagName("RutaImagenes")[0].InnerText;
                            //csGlobal.rutaImagenes = procesoCarpetaImagenes(csGlobal.rutaImagenes);
                            //string path = System.Environment.GetEnvironmentVariable("USERPROFILE") + @"\Imagenes\" + csGlobal.conexionDB; ;
                            //csGlobal.rutaImagenes = path;

                            //csGlobal.rutaFTPImagenes = nodo.GetElementsByTagName("RutaFTPImagenes")[0].InnerText;
                            //if (csGlobal.modeAp != "Repasat" || csGlobal.conexionDB == "Repasat")
                            //csGlobal.defaultIdLangPS = mySql.obtenerDatoFromQuery("Select  ps_configuration.value From   ps_configuration Where   ps_configuration.name ='PS_LANG_DEFAULT'");
                        }
                    }
                }
                if (csGlobal.modeDebug)
                {
                    csUtilidades.log("FIN ITERACIÓN Y ASIGNACIÓN VARIABLES GLOBALES");
                }

                csSqlConnects conector = new csSqlConnects();

                //CONTROLO LOS NODOS QUE NO ACCEDEN A PRESTASHOP
                List<string> noMySQL = new List<string>();
                noMySQL.Add("REPASAT");
                noMySQL.Add("ADMAN ESPAÑA");
                noMySQL.Add("ADMAN FRANCIA");
                noMySQL.Add("EUROLINE");
                noMySQL.Add("PUNTES_LOCALCONFIG");

                if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 7");

                if (csGlobal.cadenaConexion == "HEBO")
                {
                    csGlobal.ps_visible_combinations = false;
                }


                if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 8");
                if (csGlobal.modeExternalConnection == "PS")
                {
                    if (csUtilidades.check_MySQL_DB())
                    {
                        if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 8.1");
                        if (csGlobal.databasePS != "")
                        {
                            if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 8.2");
                            csMySqlConnect mysql = new csMySqlConnect();
                            if (csGlobal.modeAp == "esync")
                            {
                                if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 8.3");
                                csGlobal.versionPS = mysql.obtenerVersionPS();
                            }
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Los datos de acceso a MySQL no son correctos");
                        csUtilidades.salirDelPrograma();
                    }
                }
                
                if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 9");

                if (csGlobal.modeAp.ToUpper() == "ESYNC")
                {
                    obtenerTransportistasYFormasDePago();
                }

                if (csGlobal.modeAp.ToUpper() == "REPASAT")
                {
                    obtenerRegimenImpuestos();
                }


                //TODO: HE AÑADIDO QUE TE PUEDAS CONECTAR SIN A3ERP PARA GESTION DE PRODUCTOS 190524 FRANCO
                if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 10");
                if (csGlobal.A3ERPConnection == "Y")
                {
                    if (csUtilidades.check_SQL_DB())
                    {

                        if (csGlobal.A3ERPConnection == "Y")
                            csGlobal.nombreEmpresaA3 = conector.obtenerNombreEmpresaA3();

                        if (!csGlobal.conexionDB.Contains("VILARDELL") && csGlobal.modeAp != "Repasat")
                        {
                            //Cargo los transportistas únicamente para Esync con Prestashop
                            if (csGlobal.modeAp.ToUpper() == "ESYNC")
                                obtenerTransportistasYFormasDePago();


                            //csGlobal.IvaIncluidoPreferencias = (conector.obtenerCampoTabla("SELECT IVAINCLUIDO FROM DATOSCONFIG") == "T") ? true : false;
                            //csGlobal.tieneTallas = (conector.obtenerCampoTabla("SELECT TALLAS FROM DATOSCONFIG") == "T") ? true : false;
                            if (csGlobal.A3ERPConnection == "Y")
                            {
                                if (csGlobal.conexionDB.ToUpper().Contains("DISMAY") || conector.obtenerCampoTabla("SELECT TALLAS FROM DATOSCONFIG") == "T")
                                    csGlobal.tieneTallas = true;
                                else
                                    csGlobal.tieneTallas = false;

                                // Fundació Miró, Slide, etc - Cuando en A3 no tiene tallas pero en PS sí
                                if (csGlobal.modoTallasYColores == "A3NOPSSI")
                                {
                                    csGlobal.tieneTallas = true;
                                }
                            }
                            if (csGlobal.A3ERPConnection == "Y")
                            {
                                csUtilidades.GuardarConexion();
                                csUtilidades.GuardarVersionA3();
                                csUtilidades.GuardarVersionEsync();
                            }
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Los datos de acceso a SQL no son correctos");
                        csUtilidades.salirDelPrograma();
                    }
                }
                else {
                    if (csGlobal.modeDebug) csUtilidades.log("CARGA NODOS 11");
                    MessageBox.Show("Conexion sin A3ERP"); }
               
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show(ex.Message + "\n" + "\n" +
                        "ERROR EN CONEXIÓN A DATOS DE CONFIGURACIÓN" + "\n" +
                        "Revise lo siguiente: " + "\n" +
                        "Que el nombre de los nodos esté bien escrito" + "\n" +
                        "Conexión con el dominio www.esync.es");
                }
            }
        }

        private string procesoCarpetaImagenes(string ruta_imagenes)
        {
            string ruta = "";
            string ruta_imagenes_alt = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + @"\" + csGlobal.conexionDB;

            if (Directory.Exists(ruta_imagenes))
            {
                ruta = ruta_imagenes;
            }
            else if (Directory.Exists(ruta_imagenes_alt))
            {
                ruta = ruta_imagenes_alt;
            }
            else
            {
                if (!crearCarpetaImagenes(ruta_imagenes_alt))
                {
                    ruta = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                }
                else
                {
                    ruta = ruta_imagenes_alt;
                }
            }

            return ruta;
        }

        private bool crearCarpetaImagenes(string ruta)
        {
            try
            {
                Directory.CreateDirectory(ruta);
                Directory.CreateDirectory(ruta + @"\UPLOAD");

                return true;
            }
            catch (Exception)
            {


                return false;
            }
            
        }

        private void obtenerTransportistasYFormasDePago()
        {
            try
            {
                csGlobal.formas_pago = new Dictionary<string, string>();
                csGlobal.transportistas = new Dictionary<string, string>();
                csSqlConnects sql = new csSqlConnects();
                DataTable t = sql.cargarDatosTablaA3("SELECT KLS_ID_PS, CODTRA FROM TRANSPOR where kls_id_ps is not null");

                if (csUtilidades.verificarDt(t))
                {
                    foreach (DataRow item in t.Rows)
                    {
                        csGlobal.transportistas.Add(item["KLS_ID_PS"].ToString().Trim(), item["CODTRA"].ToString().Trim());
                    }

                    DataTable fp = sql.cargarDatosTablaA3("select KLS_ID_PS, DOCPAG from DOCUPAGO where kls_id_ps is not null");
                    foreach (DataRow item in fp.Rows)
                    {
                        csGlobal.formas_pago.Add(item["KLS_ID_PS"].ToString().Trim(), item["DOCPAG"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }


        /// <summary>
        /// Función para cargar los regimenes de impuestos de A3ERP y hacer su paridad en Repasat
        /// </summary>
        private void obtenerRegimenImpuestos()
        {
            try
            {
                csGlobal.regimenImpuesto = new Dictionary<string, string>();
                csSqlConnects sql = new csSqlConnects();
                DataTable t = sql.cargarDatosTablaA3("SELECT REGIVA,NOMRIVA,RPST_ID_REGIVA  FROM REGIVA WHERE RPST_ID_REGIVA IS NOT NULL");

                if (csUtilidades.verificarDt(t))
                {
                    foreach (DataRow item in t.Rows)
                    {
                        csGlobal.regimenImpuesto.Add(item["RPST_ID_REGIVA"].ToString().Trim(), item["REGIVA"].ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {

            }



        }

        public string cargarConfiguracionConexionesBD()
        {
            string xml = string.Empty;
            try
            {
                csSqlConnects conector = new csSqlConnects();
                DataTable dt = new DataTable();
                dt = conector.cargarDatosTabla("KLS_ESYNC_CONFIGURATION");
                dt.TableName = "conexion";

                MemoryStream ms = new MemoryStream();
                dt.WriteXml(ms, XmlWriteMode.IgnoreSchema);
                ms.Seek(0, SeekOrigin.Begin);
                StreamReader sr = new StreamReader(ms);

                xml = sr.ReadToEnd();
                ms.Close();
            }
            catch (Exception ex)
            {
            }

            return xml;

        }

        //FUNCIÓN PARA CONVERTIR UN VALOR EN BOOLEAN
        public bool ToBoolean(string value)
        {
            switch (value.ToLower())
            {
                case "true":
                    return true;
                case "t":
                    return true;
                case "1":
                    return true;
                case "0":
                    return false;
                case "false":
                    return false;
                case "f":
                    return false;
                default:
                    throw new InvalidCastException("You can't cast a weird value to a bool!");
            }
        }

        public bool comprobarIdEnlace(string nuevoId)
        {
            bool idCorrecto = true;
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");

            XmlNodeList conexiones = xDoc.GetElementsByTagName("conexion");
            XmlNodeList lista = xDoc.GetElementsByTagName("conexion");
            foreach (XmlElement nodo in lista)
            {
                if (nodo.GetAttribute("id").ToString() == nuevoId)
                {
                    idCorrecto = false;
                    //MessageBox.Show("No se puede utilizar este Id, ya existe \npor favor pruebe con otro Nombre de Conexión");
                    return idCorrecto;
                }
            }
            return idCorrecto;
        }


        public void modificarNodos(string enlace, string nodo, string nuevoValor)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");

            XmlNode node;
            node = xDoc.DocumentElement;

            try
            {
                foreach (XmlNode nodoConex in node.ChildNodes)
                {
                    if (nodoConex.Attributes["id"].Value.ToString() == enlace)
                    {
                        //foreach (XmlNode node2 in nodoConex.ChildNodes)
                        //{
                        //    if (node2.Name == nodo)
                        //    {
                        //        node2.InnerText = nuevoValor;
                        //    }
                        //}
                        if (nodo == xDoc.SelectSingleNode(nodo).InnerText.ToString())
                        {
                            xDoc.SelectSingleNode(nodo).InnerText = nuevoValor;
                        }
                    }
                }

                xDoc.Save(csGlobal.rutaFicConfig + "miXML.xml");
            }
            catch (Exception ex)
            { }
        }
    }
}

