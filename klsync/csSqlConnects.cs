﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data.Sql;
using System.Data;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using klsync.SQL;

namespace klsync
{
    class csSqlConnects
    {

        SqlConnection dataconnection = new SqlConnection();

        private OdbcConnection connection;

        // Constructor original, sin parámetros
        public csSqlConnects()
        {
            // Esto mantendrá el comportamiento original de la clase
            connection = new OdbcConnection("DSN=ps_shop");
        }

        // Nuevo constructor que acepta una conexión existente (proveniente del Singleton)
        public csSqlConnects(OdbcConnection conexion)
        {
            this.connection = conexion;
        }

        public object obtenerValorScalarSQLServer(string consulta, bool conexionOpen = false)
        {
            object result = null;
            SqlConnection connection = null; 

            try
            {
                if (!conexionOpen)
                {
                    connection = new SqlConnection(csGlobal.cadenaConexion);
                    connection.Open();
                }

                SqlCommand cmd = new SqlCommand(consulta, connection);
                result = cmd.ExecuteScalar(); 

                if (!conexionOpen && connection != null)
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error en obtenerValorScalarSQLServer: {ex.Message}");
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return result;
        }



        public void ComprobarBDServidor()
        {
            SqlConnection SqlCon = new SqlConnection("Data Source=VBA3ERP\\SQLEXPRESS;Initial Catalog=TutoKlosions;Integrated Security=true;User Id=sa;Password=klosions;");

            //             ("server=VBA3ERP\\SQLEXPRESS;uid=sa;password=klosions");
            try
            {
                SqlCon.Open();
            }
            catch (Exception)
            {
                ////MessageBox.Show(ex.Message);
            }
            System.Data.SqlClient.SqlCommand SqlCom = new System.Data.SqlClient.SqlCommand();
            SqlCom.Connection = SqlCon;
            SqlCom.CommandType = CommandType.StoredProcedure;
            SqlCom.CommandText = "sp_databases";

            System.Data.SqlClient.SqlDataReader SqlDR;
            SqlDR = SqlCom.ExecuteReader();

            while (SqlDR.Read())
            {
                ////MessageBox.Show(SqlDR.GetString(0));
            }
        }

        public void ComprobarServidoresSQL()
        {
            SqlDataSourceEnumerator servers = SqlDataSourceEnumerator.Instance;
            DataTable serversTable = servers.GetDataSources();
            //Mediante la función GetDataSources, obtengo una tabla con la información de los servidores

            foreach (DataRow row in serversTable.Rows)
            {
                string serverName = row["ServerName"].ToString();
                string InstanceName = row["InstanceName"].ToString();
                // Add this to your list
                ////MessageBox.Show(serverName + InstanceName);
            }
        }








        public void devolverInstancias()
        {
            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
            System.Data.DataTable table = instance.GetDataSources();
            // Display the contents of the table.
            DisplayData(table);
        }

        private static void DisplayData(System.Data.DataTable table)
        {
            foreach (System.Data.DataRow row in table.Rows)
            {
                foreach (System.Data.DataColumn col in table.Columns)
                {
                    ////MessageBox.Show(col.ColumnName);
                }

            }
        }

        public void abrirConexion()
        {
            try
            {
                dataconnection.ConnectionString = csGlobal.cadenaConexion;
                dataconnection.Open();
            }
            catch (Exception ex){ ex.Message.mb(); }
        }

        public void cerrarConexion()
        {
            dataconnection.Close();
        }

        /// <summary>
        /// Devuelve un string con el count de la tabla que se le pasa por parámetro
        /// </summary>
        /// <param name="tabla">Tabla de SQL</param>
        /// <param name="where">Por si se requiere un WHERE</param>
        /// <returns></returns>
        public string contarTabla(string tabla, string where = "")
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("select count(*) from " + tabla + where, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);

            return t.Rows[0].ItemArray[0].ToString();
        }

        public void actualizarNombresA3ERP(DataGridViewSelectedRowCollection dgv)
        {
            string codart = "";
            string descart = "";

            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();

            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataGridViewRow fila in dgv)
            {
                codart = fila.Cells["REFERENCE"].Value.ToString();
                descart = fila.Cells["NAME"].Value.ToString();
                datacommand.CommandText = "UPDATE ARTICULO SET DESCART='" + descart + "' WHERE LTRIM(CODART)='" + descart + "'";
                datacommand.ExecuteNonQuery();

            }

            //datacommand.CommandText = script;
            //datacommand.ExecuteNonQuery();

            cerrarConexion();


        }

        public void insertarProductosEnA3(DataTable tabla)
        {
            string id_product = "";
            string reference = "";
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;
            string script = "";
            bool check = true;
            int i = 0;
            foreach (DataRow fila in tabla.Rows)
            {
                id_product = fila.ItemArray[0].ToString();
                reference = fila.ItemArray[1].ToString();

                if (check)
                {
                    script = "insert into KLS_PRESTA_PRODUCTS (id_producto, reference) values (" + id_product + ",'" + reference + "')";

                    check = false;
                }
                else
                {
                    script += ",(" + id_product + ",'" + reference + "')";
                }

                if (i == 995)
                {
                    datacommand.CommandText = script;
                    datacommand.ExecuteNonQuery();
                    script = "";
                    check = true;
                    i = 0;
                }

                i++;
            }

            datacommand.CommandText = script;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public void insertarIdiomasEnlazados(DataTable tabla)
        {
            string idiomaPS = "";
            string idiomaA3 = "";
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                try
                {
                    idiomaPS = fila.ItemArray[0].ToString();
                    idiomaA3 = fila.ItemArray[1].ToString();
                    ////MessageBox.Show(fila.ItemArray[0].ToString());
                    datacommand.CommandText = "insert into KLS_ESYNC_IDIOMAS (IDIOMAPS,IDIOMAA3) values (" + idiomaPS + ",'" + idiomaA3 + "')";
                    datacommand.ExecuteNonQuery();
                }
                catch
                { }
            }
            cerrarConexion();
        }

        public void insertarTallasyColoresEnlazados(DataTable tabla)
        {
            string PS_Grupo = "";
            string A3_Grupo = "";
            string Ps_Atributo = "";
            string A3_Atributo = "";
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                PS_Grupo = fila.ItemArray[0].ToString();
                A3_Grupo = fila.ItemArray[2].ToString();
                Ps_Atributo = fila.ItemArray[1].ToString();
                A3_Atributo = fila.ItemArray[3].ToString();
                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "insert into KLS_ESYNC_TALLASYCOLORES (PS_GRUPO,PS_ATRIBUTO,A3_GRUPO,A3_ATRIBUTO) values ('" +
                    PS_Grupo + "','" + Ps_Atributo + "','" + A3_Grupo + "','" + A3_Atributo + "')";
                datacommand.ExecuteNonQuery();
            }
            cerrarConexion();
        }

        public void insertarCategoriasA3(DataTable tabla)
        {
            try
            {
                string codCategoria = "";
                string nomCategoria = "";
                string idPrestashop = "";
                abrirConexion();
                SqlCommand datacommand = new SqlCommand();
                datacommand.Connection = dataconnection;

                foreach (DataRow fila in tabla.Rows)
                {
                    idPrestashop = fila.ItemArray[0].ToString();
                    codCategoria = fila.ItemArray[0].ToString();
                    nomCategoria = fila.ItemArray[1].ToString().Trim();
                    if (nomCategoria.Length > 50)
                    {
                        nomCategoria = nomCategoria.Substring(0, 50);
                    }

                    datacommand.CommandText = "insert into KLS_CATEGORIAS (KLS_COD_PS,KLS_CODCATEGORIA, KLS_DESCCATEGORIA) values (" + idPrestashop + "," +
                        codCategoria + ",'" + nomCategoria.Replace("'", "''") + "')";
                    datacommand.ExecuteNonQuery();
                }

                MessageBox.Show("Categorias seleccionadas importadas a A3");
                cerrarConexion();
            }
            catch (Exception)
            {
            }

        }

        public void insertarCaracteristicasArticuloA3(DataGridViewRow fila)
        {
            string codCaracteristica = fila.Cells["id_feature_value"].Value.ToString();
            string codArticulo = fila.Cells["reference"].Value.ToString();
            codArticulo = obtenerCampoTabla("select codart from articulo where ltrim(codart) = '" + codArticulo + "'");
            if (codArticulo != "")
            {
                abrirConexion();
                SqlCommand datacommand = new SqlCommand();
                datacommand.Connection = dataconnection;

                //string funcion = csGlobal.databaseA3 + ".dbo.obtenerCodart('" + codArticulo + "')";

                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "INSERT INTO KLS_CARACTERISTICAS_ART values ('" + codArticulo + "','" + codCaracteristica + "')";
                datacommand.ExecuteNonQuery();
                cerrarConexion();
            }
        }

        public void insertarCaracteristicasA3(DataTable tabla)
        {
            string id_feature = "";
            string name = "";
            string caracteristica = "";
            string value = "";
            abrirConexion();
            SqlCommand datacommand = new SqlCommand();
            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                caracteristica = fila["Caracteristica"].ToString();
                value = fila["value"].ToString();
                id_feature = fila["id_feature"].ToString();
                name = fila["name"].ToString();

                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "insert into KLS_CARACTERISTICAS values (" + caracteristica + "," + id_feature + ",'" + name + "','" + value + "', " + caracteristica + ")";
                datacommand.ExecuteNonQuery();
            }
            cerrarConexion();

        }

        public void insertarDescripa(DataTable tabla)
        {
            string codCaracteristica = "";
            string nomCaracteristica = "";
            abrirConexion();
            SqlCommand datacommand = new SqlCommand();
            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                codCaracteristica = fila.ItemArray[2].ToString();
                nomCaracteristica = fila.ItemArray[3].ToString();

                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "insert into DESCRIPA values (" + codCaracteristica + ",'" + nomCaracteristica + "')";
                datacommand.ExecuteNonQuery();
            }
            cerrarConexion();
        }

        // Funciona 20/01/2017
        public void updateDescripa(DataGridViewRow fila)
        {
            string articulo = fila.Cells["CODIGO"].Value.ToString();
            string id_product = fila.Cells["ID EN TIENDA"].Value.ToString();
            if (id_product != "" || id_product != "0")
            {
                csMySqlConnect mysql = new csMySqlConnect();
                foreach (KeyValuePair<int, string> entry in csGlobal.IdiomasEsync)
                {
                    string idioma = entry.Key.ToString();
                    string descCorta = mysql.obtenerDatoFromQuery("select description_short from ps_product_lang where id_product = " + id_product + " and id_lang = " + idioma);
                    string descLarga = mysql.obtenerDatoFromQuery("select description from ps_product_lang where id_product = " + id_product + " and id_lang = " + idioma);
                    string funcion = csGlobal.databaseA3 + ".dbo.obtenerCodart('" + articulo + "')";

                    string consulta = "UPDATE DESCRIPA SET TEXTO = '" + descCorta + "', KLS_DESC_LARGA = '" + descLarga + "' WHERE ltrim(codart) = '" + articulo + "' AND CODIDIOMA = '" + entry.Value + "' ";
                    csUtilidades.ejecutarConsulta(consulta, false);
                }
            }
        }

        public void insertarTodasCombinacionesAtributosA3(DataTable dt)
        {
            try
            {
                string query = "";
                string codart = "";
                string atributoH = "";
                string atributoV = "";
                string campos = "";
                List<string> values = new List<string>();
                SqlCommand datacommand = new SqlCommand();
                datacommand.Connection = dataconnection;

                abrirConexion();
                foreach (DataRow fila in dt.Rows)
                {
                    codart = fila["CODART"].ToString();
                   
                    atributoH = fila["CODATRIBUTO1"].ToString().Trim();
                    if (atributoH == "")
                        atributoH = "NULL";
                    else
                        atributoH = "'" + atributoH + "'";

                    atributoV = fila["CODATRIBUTO2"].ToString().Trim();
                    if (atributoV == "")
                        atributoV = "NULL";
                    else
                        atributoV = "'" + atributoV + "'";
                    campos = "(ATRIBUTOH, ATRIBUTOV, CODART)";
                    // pasado al nuevo sistema - funciona 7/2/17
                    values.Add("(" + atributoH + ", " + atributoV + ", '" + codart + "')");
                    //query = " INSERT INTO KLS_COMBINACIONES_VISIBLES (ATRIBUTOH, ATRIBUTOV,CODART )VALUES (" + atributoH + "," + atributoV + ",'" + codart + "')";
                    //datacommand.CommandText = query;
                    //datacommand.ExecuteNonQuery();
                }

                // pasado al nuevo sistema - funciona 7/2/17
                csUtilidades.WriteListToDatabase("KLS_COMBINACIONES_VISIBLES", campos, values, csUtilidades.SQL, false);
                cerrarConexion();
            }
            catch (Exception ex)
            {
                //Program.guardarErrorFichero("QUERY: " + query + "\n" + ex.ToString());
            }
        }

        public void borrarDatosSqlTabla(string tabla)
        {
            borrarDatosTabla(tabla);
        }

        private void borrarDatosTabla(string Tabla)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            datacommand.CommandText = "Delete from " + Tabla;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public void borrarDatosTabla(string Tabla, string filtro)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            datacommand.CommandText = "Delete from " + Tabla + " where " + filtro;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public void consultaBorrar(string consulta)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            datacommand.CommandText = consulta;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public string[] obtenerListaDatos(string tabla, string Id, string Campo1)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + Campo1 + " FROM " + tabla.ToString(), conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            string[] Listado;
            Listado = new string[ds.Tables[0].Rows.Count];
            int i = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Listado[i] = row.ItemArray[0].ToString();
                i++;
            }
            return Listado;
        }

        public string[] obtenerCategoriasArticulo(string Articulo)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT KLS_CODCATEGORIA FROM KLS_CATEGORIAS WHERE CODART='" + Articulo + "'", conn);
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT KLS_COD_PS FROM dbo.KLS_CATEGORIAS RIGHT OUTER JOIN dbo.KLS_CATEGORIAS_ART ON dbo.KLS_CATEGORIAS.KLS_CODCATEGORIA = dbo.KLS_CATEGORIAS_ART.KLS_CODCATEGORIA  where LTRIM(KLS_CATEGORIAS_ART.CODART)='" + Articulo + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            string[] Listado;
            Listado = new string[ds.Tables[0].Rows.Count];
            int i = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Listado[i] = row.ItemArray[0].ToString();
                i++;
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                Array.Resize(ref Listado, 1);
                Listado[0] = "2";
            }
            return Listado;
        }


        public string[] obtenerDatosEnArray(string query)
        {

            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(query, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            string[] Listado;
            Listado = new string[ds.Tables[0].Rows.Count];
            int i = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Listado[i] = row.ItemArray[0].ToString();
                i++;
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            return Listado;



        }

        public DataTable obtenerTextoPorDefecto(string elementoXML)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT ID_LANG,TEXTO FROM dbo.KLS_ESYNC_DEFAULT_VALUES WHERE ELEMENTO='" + elementoXML + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            DataTable Listado = ds.Tables[0];
            return Listado;

        }

        public void cargarIdiomas()
        {
            csGlobal.IdiomasEsync = null;

                if (csGlobal.modeAp.ToUpper() == "ESYNC" && csGlobal.modeExternalConnection=="PS")
                {
                    try
                    {
                        SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
                        SqlCommand datacomand = new SqlCommand();
                        SqlDataAdapter adapt = new SqlDataAdapter("SELECT idiomaPS ,idiomaA3  FROM " + csGlobal.databaseA3 + ".[dbo].[KLS_ESYNC_IDIOMAS]", conn);
                        DataSet ds = new DataSet();
                        adapt.Fill(ds);
                        conn.Close();
                        //String[] Listado;
                        csGlobal.IdiomasEsync = ds.Tables[0].Rows.OfType<DataRow>().ToDictionary(d => d.Field<int>(0), v => v.Field<string>(1));
                        //foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                        //{
                        //    //MessageBox.Show(valuePair.Key.ToString() + " - " + valuePair.Value.ToString());
                        //}
                    }
                    catch
                    { }
                }
        }

        public string obtenerValorArticuloA3(string Id, string Campo)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + Campo + " FROM ARTICULO WHERE LTRIM(CODART)='" + Id + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            string Listado = ds.Tables[0].Rows.Count==0 ? "": ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return Listado;
        }

        public string obtenerValorArticuloA3(string idProducto)
        {
            string resultado = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("select  KLS_ARTICULO_BLOQUEAR from ARTICULO where KLS_ID_SHOP=" + idProducto, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            if (ds.Tables.Count == 0)
            {
                resultado = "0";
            }
            else
            {
                //String[] Listado;
                resultado = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                if (resultado == "T")
                {
                    resultado = "0";
                }
                else
                {
                    resultado = "1";
                }
            }

            return resultado;

        }

        public string obtenerEstadoArticulo(string idProduct, string codart)
        {
            string resultado = "1";
            bool check = false;
            //Para artículos subidos en la tienda: Obtengo el valor del campo para saber si está activo o bloqueado
            string query = "SELECT KLS_ARTICULO_BLOQUEAR from ARTICULO where KLS_ID_SHOP=" + idProduct;

            //Para artículos nuevos: Obtengo el valor del campo para saber si está activo o bloqueado
            if (idProduct == string.Empty)
            {
                query = "SELECT KLS_ARTICULO_BLOQUEAR from ARTICULO where LTRIM(CODART)='" + codart + "'";
            }

            //13/01/2017 comento porque no veo utilidad 
            //else
            //    check = true;

            //if (!check)
            //{
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(query, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            if (ds.Tables.Count == 0)
            {
                resultado = "0";
            }
            else
            {
                //String[] Listado;
                resultado = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                if (resultado == "T")
                {
                    resultado = "0";
                }
                else
                {
                    resultado = "1";
                }
            }
            //}

            return resultado;
        }

        public string obtenerNumDireccion(string idDirent, string campo)
        {
            string Listado = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + campo + " FROM DIRENT WHERE ID_PS=" + idDirent, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            if (ds.Tables[0].Rows.Count == 0)
            {
                Listado = "";
            }
            else
            {
                Listado = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }

            return Listado;

        }

        public string obtenerFormaPagoA3ERP(string codCli)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT dbo.DOCUPAGO.DESCDOC + ' ' + dbo.FORMAPAG.DESCFOR AS 'FORMA PAGO' " +
                            "FROM dbo.CLIENTES INNER JOIN dbo.FORMAPAG ON dbo.CLIENTES.FORPAG = dbo.FORMAPAG.FORPAG INNER JOIN " +
                            " dbo.DOCUPAGO ON dbo.CLIENTES.DOCPAG = dbo.DOCUPAGO.DOCPAG WHERE CODCLI='" + codCli + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            string formaPago = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return formaPago;
        }

        public string obtenerCodigoTallaParaPS(string A3Grupo, string A3Valor)
        {

            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT PS_ATRIBUTO FROM KLS_ESYNC_TALLASYCOLORES WHERE A3_GRUPO='" + A3Grupo + "' AND A3_ATRIBUTO='" + A3Valor + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            string Valor = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return Valor;

        }

        public string obtenerTraduccionArticuloA3(string codart, string idioma)
        {
            string consulta = "SELECT DESCART FROM DESCRIPA WHERE LTRIM(CODART) = '" + codart + "' AND CODIDIOMA ='" + idioma + "'";
            string traduccion = obtenerCampoTabla(consulta);
            if (string.IsNullOrEmpty(traduccion))
            {
                consulta = "SELECT DESCART FROM DESCRIPA WHERE LTRIM(CODART) = '" + codart + "'";
                traduccion = obtenerCampoTabla(consulta);
            }

            if (string.IsNullOrEmpty(traduccion))
            {
                consulta = "SELECT DESCART FROM ARTICULO WHERE LTRIM(CODART) = '" + codart + "'";
                traduccion = obtenerCampoTabla(consulta);
            }

            return traduccion;
        }

        public string obtenerTraduccionArticuloA3(string Id, string Campo, string Idioma)
        {
            string traduccion = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + Campo + " FROM " + csGlobal.databaseA3 + ".[dbo].[DESCRIPA]  where LTRIM(CODART)='" + Id + "' and CODIDIOMA='" + Idioma + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            //String[] Listado;
            if (ds.Tables[0].Rows.Count == 0 & csGlobal.defaultLang == Idioma)
            {
                traduccion = "";
            }
            else if (ds.Tables[0].Rows.Count == 0 & csGlobal.defaultLang != Idioma) // Si ese producto no está para ese idioma asignamos el mismo nombre
            {
                traduccion = "";
            }
            else
            {
                traduccion = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                if (csGlobal.defaultLang == Idioma & traduccion == "")
                {
                    traduccion = "";
                }
            }

            if (csGlobal.conexionDB == "Thagson")
            {
                adapt = new SqlDataAdapter("SELECT DESCART FROM " + csGlobal.databaseA3 + ".[dbo].[ARTICULO]  where LTRIM(CODART)='" + Id + "'", conn);
                DataSet ds2 = new DataSet();
                adapt.Fill(ds2);
                traduccion = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
            }

            conn.Close();

            return traduccion;
        }

        public string obtenerUrlArticuloA3(string Id, string Campo, string Idioma)
        {
            string traduccion = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + Campo + " FROM " + csGlobal.databaseA3 + ".[dbo].[DESCRIPA]  where LTRIM(CODART)='" + Id + "' and CODIDIOMA='" + Idioma + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            if (ds.Tables[0].Rows.Count == 0 & csGlobal.defaultLang == Idioma)
            {
                traduccion = obtenerValorArticuloA3(Id, "DESCART");
                traduccion = checkUrlArticulo(traduccion);
            }
            else if (ds.Tables[0].Rows.Count == 0 & csGlobal.defaultLang != Idioma)
            {
                traduccion = "";
                traduccion = obtenerValorArticuloA3(Id, "DESCART");
                traduccion = checkUrlArticulo(traduccion);
            }
            else
            {
                traduccion = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                if (csGlobal.defaultLang == Idioma & traduccion.Trim() == "")
                {
                    traduccion = obtenerValorArticuloA3(Id, "DESCART");
                    traduccion = checkUrlArticulo(traduccion);
                }
            }

            if (traduccion.Trim() == "")
            {
                traduccion = csUtilidades.quitarAcentos(obtenerCampoTabla("select descart from articulo where ltrim(codart) = '" + Id + "'").ToLower().Replace(" ", "-"));
            }

            return traduccion;
        }

        private string checkUrlArticulo(string traduccionSinPulir)
        {
            string textoCorregido = traduccionSinPulir;
            Regex reg = new Regex("[^a-zA-Z0-9 ]");
            textoCorregido = textoCorregido.Normalize(NormalizationForm.FormD);
            textoCorregido = reg.Replace(textoCorregido, "");
            textoCorregido = textoCorregido.Replace(" ", "-").Replace(">", "").Replace("<", "");
            textoCorregido = textoCorregido.ToLower();

            return textoCorregido;

        }

        public string obtenerPrecioArticuloA3(string Id, string Campo)
        {
            string precio = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " + Campo + " FROM " + csGlobal.databaseA3 + ".[dbo].[ARTICULO]  where LTRIM(CODART)='" + Id + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            precio = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return precio;
        }

        public string obtenerNumeroAlbaran(string idAlb)
        {
            string numeroAlbaran = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT NUMDOC FROM CABEALBV WHERE IDALBV=" + idAlb, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            numeroAlbaran = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return numeroAlbaran;
        }

        public string obtenerIdDocumentoA3ERP(string serie, string numdoc, string tipoDoc = null, string numEfecto=null)
        {
            string result = "";
            string script = "";

            if (tipoDoc == "saleinvoices")
            {
                script = "SELECT CAST(IDFACV AS INT)   FROM CABEFACV   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "purchaseinvoices")
            {
                script = "SELECT CAST(IDFACC AS INT)   FROM CABEFACC   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "saleorders")
            {
                script = "SELECT CAST(IDPEDV AS INT)   FROM CABEPEDV   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "purchaseorders")
            {
                script = "SELECT CAST(IDPEDC AS INT)   FROM CABEPEDC   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "saledelivery")
            {
                script = "SELECT CAST(IDALBV AS INT)   FROM CABEALBV   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "purchasedelivery")
            {
                script = "SELECT CAST(IDALBC AS INT)   FROM CABEALBC   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            if (tipoDoc == "incomingpayments")
            {
                script = "select CAST(IDCARTERA AS INT) from cartera where LTRIM(SERIE)='" + serie + "'  and numdoc=" + numdoc + " and NUMVEN=" + numEfecto + " and COBPAG='C'";
            }
            if (tipoDoc == "outgoingpayments")
            {
                script = "select CAST(IDCARTERA AS INT) from cartera where LTRIM(SERIE)='" + serie + "'  and numdoc=" + numdoc + " and NUMVEN=" + numEfecto + " and COBPAG='P'";
            }
            if (tipoDoc == "remittances")
            {
                script = "SELECT CAST(IDREMESA AS INT) FROM __REMESAS WHERE RPST_ID_REMESA = " + numdoc;
            }



            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(script, conn);
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT DBO.FAMILIAS.COD_PS FROM DBO.FAMILIAS JOIN DBO.ARTICULO ON DBO.ARTICULO.FAMARTDESCVEN = DBO.FAMILIAS.CODFAM WHERE LTRIM(DBO.ARTICULO.CODART)='" + codArt + "'", conn); 

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                result = null;
            }
            else
            {
                //adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                result = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }

            return result;
        }


        public string obtenerIdDocumentoVenta(string serie, string numdoc, string tipoDoc=null)
        {
            string result = "";
            string script = "SELECT IDPEDV   FROM CABEPEDV   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            if (tipoDoc == "FRAV")
            {
                script= "SELECT IDFACV   FROM CABEFACV   WHERE LTRIM(SERIE) = '" + serie + "' AND NUMDOC = " + numdoc;
            }
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(script, conn);
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT DBO.FAMILIAS.COD_PS FROM DBO.FAMILIAS JOIN DBO.ARTICULO ON DBO.ARTICULO.FAMARTDESCVEN = DBO.FAMILIAS.CODFAM WHERE LTRIM(DBO.ARTICULO.CODART)='" + codArt + "'", conn); 

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                result = "2";
            }
            else
            {
                //adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                result = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }

            return result;
        }

        public string obtenerMarcaArticulo(string codart)
        {

            string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT KLS_CODMARCA FROM ARTICULO WHERE LTRIM(CODART)='" + codart + "'", conn);

            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            if (ds.Tables[0].Rows.Count == 0)
            {
                result = "<id_manufacturer/>";
            }
            else
            {
                //adapt.Fill(ds);

                //String[] Listado;
                result = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                result = "<id_manufacturer>" + result + "</id_manufacturer>";
            }

            return result;
        }

        public string[] obtenerInformacionDocumentoVenta(string serie, string numdoc)
        {
            string[] Listado = new string[2];
            try
            {
                SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
                SqlCommand datacomand = new SqlCommand();
                csSqlScriptRepasat ScriptRepasat = new csSqlScriptRepasat();
                SqlDataAdapter adapt = new SqlDataAdapter("SELECT IDPEDV,LTRIM(CODCLI) FROM CABEPEDV WHERE LTRIM(SERIE)='" + serie + "' AND NUMDOC=" + numdoc, conn);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                //Array.Resize(ref Listado, ds.Tables[0].Rows[0].ItemArray.Count);
                //Listado = new string[];
                int i = 0;


                //IDPEDV
                Listado[0] = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                //CODCLI
                Listado[1] = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message);

            }

            return Listado;
        }

        public string[] obtenerInformacionDocumentoVentaAlbaran(string serie, string numdoc)
        {
            string[] Listado = new string[2];
            try
            {
                SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
                SqlCommand datacomand = new SqlCommand();
                csSqlScriptRepasat ScriptRepasat = new csSqlScriptRepasat();
                SqlDataAdapter adapt = new SqlDataAdapter("SELECT idalbv,LTRIM(CODCLI) FROM cabealbv WHERE LTRIM(SERIE)='" + serie + "' AND NUMDOC=" + numdoc, conn);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                //Array.Resize(ref Listado, ds.Tables[0].Rows[0].ItemArray.Count);
                //Listado = new string[];
                int i = 0;


                //IDPEDV
                Listado[0] = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                //CODCLI
                Listado[1] = ds.Tables[0].Rows[0].ItemArray[1].ToString();

            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message);

            }

            return Listado;
        }

        public string codFamiliaArticulo(string codArt)
        {
            string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT FAMARTDESCVEN FROM .[dbo].[ARTICULO]  where LTRIM(CODART)='" + codArt + "'", conn);
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT DBO.FAMILIAS.COD_PS FROM DBO.FAMILIAS JOIN DBO.ARTICULO ON DBO.ARTICULO.FAMARTDESCVEN = DBO.FAMILIAS.CODFAM WHERE LTRIM(DBO.ARTICULO.CODART)='" + codArt + "'", conn); 
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();
            //String[] Listado;
            result = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return result;
        }

        public int obtenerStockArticulo(string codArt, string codtallav = "")
        {

            int result = 0;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            if (codtallav != "")
            {
                SqlDataAdapter adapt = new SqlDataAdapter(string.Format("select unidadesstock from stockalm where ltrim(codart)='{0}' and codtallav = '{1}'", codArt, codtallav), conn);
                adapt.Fill(ds);
            }
            else
            {
                SqlDataAdapter adapt = new SqlDataAdapter("SELECT UNIDADESSTOCK FROM STOCKALM where ltrim(CODART)='" + codArt + "'", conn);
                adapt.Fill(ds);
            }
            
            conn.Close();
            //String[] Listado;
            if (ds.Tables[0].Rows.Count > 0)
            {
                result = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
            }
            else
            {
                result = 0;
            }
            return result;
        }

        public DataTable obtenerLineasPedido(string numPedido)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            string sb = string.Format("SELECT dbo.LINEPEDI.CODART, " +
            "       dbo.LINEPEDI.DESCLIN, " +
            "       SUM(dbo.LINEPEDI.UNIDADES)-SUM(dbo.LINEPEDI.UNIANULADA)-SUM(dbo.LINEPEDI.UNISERVIDA) AS UNIDADES, " +
            "       dbo.LINEPEDI.CODTALLAV, " +
            "       dbo.LINEPEDI.ORDLIN, " +
            "       dbo.LINEPEDI.NUMLINPED, " +
            "       dbo.TALLAS.DESCRIPCION, " +
            "         articulo.artalias," + 
            "       dbo.LINEPEDI.IDPEDV " +
            "FROM dbo.LINEPEDI " +
            " LEFT OUTER JOIN dbo.TALLAS ON dbo.LINEPEDI.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA " +
            " and (dbo.LINEPEDI.CODTALLAH = dbo.TALLAS.CODTALLA OR dbo.LINEPEDI.CODTALLAV = dbo.TALLAS.CODTALLA) " +
            " left join articulo on articulo.codart = linepedi.codart " +
            "AND dbo.TALLAS.CODTALLA = dbo.LINEPEDI.CODTALLAV " +
            "WHERE (dbo.LINEPEDI.IDPEDV = {0}) and situacion = 'A' " +
            "GROUP BY dbo.LINEPEDI.CODART, " +
            "         dbo.LINEPEDI.DESCLIN, " +
            "         dbo.LINEPEDI.ORDLIN, " +
            "       dbo.LINEPEDI.NUMLINPED, " +
            "         dbo.LINEPEDI.CODTALLAV, " +
            "         dbo.TALLAS.DESCRIPCION, " +
            "         articulo.artalias," + 
            "         dbo.LINEPEDI.IDPEDV ", numPedido);
		
		
            SqlDataAdapter a = new SqlDataAdapter(sb, dataConnection);
            
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public DataTable obtenerLineasAlbaran(string numAlbaran)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            string sb = string.Format("SELECT dbo.LINEALBA.CODART, " +
            "       dbo.LINEALBA.DESCLIN, " +
            "       SUM(dbo.LINEALBA.UNIDADES) AS UNIDADES, " +
            "       dbo.LINEALBA.CODTALLAV, " +
            "       dbo.LINEALBA.ORDLIN, " +
            "       dbo.LINEALBA.NUMLINALB, " +
            "       dbo.TALLAS.DESCRIPCION, " +
            "       dbo.LINEALBA.IDALBV " +
            "FROM dbo.LINEALBA " +
            "LEFT OUTER JOIN dbo.TALLAS ON dbo.LINEALBA.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA " +
            "AND dbo.TALLAS.CODTALLA = dbo.LINEALBA.CODTALLAV " +
            "WHERE (dbo.LINEALBA.IDALBV = {0}) " +
            "GROUP BY dbo.LINEALBA.CODART, " +
            "         dbo.LINEALBA.DESCLIN, " +
            "         dbo.LINEALBA.ORDLIN, " +
            "         dbo.LINEALBA.NUMLINALB, " +
            "         dbo.LINEALBA.CODTALLAV, " +
            "         dbo.TALLAS.DESCRIPCION, " +
            "         dbo.LINEALBA.IDALBV ", numAlbaran);


            SqlDataAdapter a = new SqlDataAdapter(sb, dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        //public DataTable obtenerDatosSQLScript(string scriptSql)
        //{
        //    csSqlConnects sqlConnect = new csSqlConnects();
        //    SqlConnection dataConnection = new SqlConnection();

        //    try
        //    {
        //        dataConnection.ConnectionString = csGlobal.cadenaConexion;
        //        dataConnection.Open();

        //        SqlDataAdapter a = new SqlDataAdapter(scriptSql, dataConnection);
        //        a.SelectCommand.CommandTimeout = 180;
        //        DataTable t = new DataTable();
        //        a.Fill(t);
        //        dataconnection.Close();
        //        return t;
        //    }
        //    catch(Exception ex) {
        //        dataconnection.Close();
        //        return null;
        //    }
        //}

        public DataTable obtenerDatosSQLScript(string scriptSql)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            Utilidades.csLogErrores logProceso = new Utilidades.csLogErrores(); // Asumiendo que tienes un sistema de logs similar

            try
            {
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                logProceso.guardarLogProceso("Cadena conexion: " + csGlobal.cadenaConexion.ToString() + "\n Script dentro de obtener datos: " + scriptSql );

                SqlDataAdapter a = new SqlDataAdapter(scriptSql, dataConnection);
                a.SelectCommand.CommandTimeout = 500; 
                DataTable t = new DataTable();

                a.Fill(t);

                logProceso.guardarLogProceso($"Consulta ejecutada correctamente. Total de filas obtenidas: {t.Rows.Count}");

                dataConnection.Close(); 

                return t; 
            }
            catch (Exception ex)
            {
                logProceso.guardarLogProceso($"Error al ejecutar la consulta: {ex.Message}");

                if (dataConnection.State == ConnectionState.Open)
                {
                    dataConnection.Close();
                }

                return null;
            }
        }


        public string[] obtenerObjetoDireccionA3ERP(string idDireccion)
        {
            try
            {
                string[] objetoDireccion = new string[2];
                string scriptSql = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;

                dataConnection.Open();

                scriptSql = "SELECT CODCLI, NUMDIR FROM DIRENT WHERE RPST_ID_DIR=" + idDireccion;

                SqlDataAdapter a = new SqlDataAdapter(scriptSql, dataConnection);
                a.SelectCommand.CommandTimeout = 180;
                DataTable t = new DataTable();
                a.Fill(t);
                dataconnection.Close();
                objetoDireccion[0] = t.Rows[0][0].ToString();
                objetoDireccion[1] = t.Rows[0][1].ToString();
                return objetoDireccion;

            }
            catch { return null; }
        }

        public string[] obtenerObjetoContactoA3ERP(string idContacto)
        {
            try
            {
                string[] objetoContacto = new string[2];
                string scriptSql = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;

                dataConnection.Open();
                //Actualizar a cómo obtener el contacto
                scriptSql = "SELECT CODCLI, NUMDIR FROM DIRENT WHERE RPST_ID_DIR=" + idContacto;

                SqlDataAdapter a = new SqlDataAdapter(scriptSql, dataConnection);
                a.SelectCommand.CommandTimeout = 180;
                DataTable t = new DataTable();
                a.Fill(t);
                dataconnection.Close();
                objetoContacto[0] = t.Rows[0][0].ToString();
                objetoContacto[1] = t.Rows[0][1].ToString();
                return objetoContacto;

            }
            catch { return null; }


        }

        public DataTable obtenerArticulosA3()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("select ltrim(codart), kls_id_shop from ARTICULO where kls_id_shop<>''", dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public string codCategoriaArticulo(string codArt)
        {
            string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT KLS_COD_PS FROM dbo.KLS_CATEGORIAS RIGHT OUTER JOIN dbo.KLS_CATEGORIAS_ART ON dbo.KLS_CATEGORIAS.KLS_CODCATEGORIA = dbo.KLS_CATEGORIAS_ART.KLS_CODCATEGORIA  where LTRIM(KLS_CATEGORIAS_ART.CODART)='" + codArt.Replace(" ", "") + "' AND LTRIM(KLS_CATEGORIAS_ART.KLS_PRINCIPAL)='T'", conn);
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT DBO.FAMILIAS.COD_PS FROM DBO.FAMILIAS JOIN DBO.ARTICULO ON DBO.ARTICULO.FAMARTDESCVEN = DBO.FAMILIAS.CODFAM WHERE LTRIM(DBO.ARTICULO.CODART)='" + codArt + "'", conn); 

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count == 0)
            {
                result = "2";
            }
            else
            {
                //adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                result = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            }

            return result;
        }

        public bool consultaExiste(string campo, string tabla, string where)
        {
            if (where == "")
                return false;
            //string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            conn.Open();
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter("select " + campo + " from " + tabla + " where ltrim(" + campo + ") = '" + where + "'", conn);

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            conn.Close();
            return false;
        }

        public bool consultaExisteFilaClave(string campo, string tabla, string valorKey)
        {
            if (valorKey == "")
                return false;
            //string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            conn.Open();
            SqlCommand datacomand = new SqlCommand();
            string sqlScript = "select " + campo + " from " + tabla + " where " + campo + " = " + valorKey + "";
            SqlDataAdapter adapt = new SqlDataAdapter(sqlScript, conn);

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            conn.Close();
            return false;
        }


        public bool obtenerExisteDocumentoEnA3(string query)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(query, conn);

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            conn.Close();
            return false;
        }
        
        
        //
        public bool consultaExiste(string consulta)
        {
            //string result = "";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(consulta, conn);

            DataSet ds = new DataSet();
            adapt.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            conn.Close();
            return false;
        }

        public void crearTablasAuxiliares(string sqlScript)
        {
            try
            {

                abrirConexion();
                //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
                SqlCommand datacommand = new SqlCommand();

                datacommand.Connection = dataconnection;
                datacommand.CommandText = sqlScript;
                datacommand.ExecuteNonQuery();
                cerrarConexion();

            }
            catch (SqlException ex)
            {
                if (ex.Number == 2714)
                {
                    //MessageBox.Show("Las Tablas Auxiliares ya están creadas");
                    cerrarConexion();
                }
                else
                {
                    //MessageBox.Show(ex.Message);
                }

            }


        }

        //función para inicializar el indice de una tabla
        public void inicializarIndiceTabla(string tabla)
        {

            try
            {

                abrirConexion();
                //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
                SqlCommand datacommand = new SqlCommand();

                datacommand.Connection = dataconnection;
                datacommand.CommandText = "DBCC CHECKIDENT (" + tabla + ", RESEED,0)";
                datacommand.ExecuteNonQuery();
                cerrarConexion();

            }
            catch (SqlException ex)
            {
                cerrarConexion();
            }



        }

        public DataTable cargarDatosScriptEnTabla(string script, string filtro=null)
        {

            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(script + filtro, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public DataTable cargarDatosTabla(string tabla)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("select * from " + tabla, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public DataTable cargarAtributosDismay()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("SELECT LTRIM(CODART) AS CODART, DESCART, EAN13, KLS_ARTICULO_PADRE, KLS_ARTICULO_TIENDA, KLS_ID_SHOP FROM dbo.ARTICULO WHERE (KLS_ARTICULO_PADRE <> '') ORDER BY DESCART", dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);

            return t;
        }

        public DataTable cargarCombinacionTallasColores()
        {
            //Se anula por cambio de tabla con todas las combinaciones de tallas
            //string query = "SELECT dbo.STOCKALM.CODALM, dbo.STOCKALM.CODART, dbo.STOCKALM.CODFAMTALLAH, dbo.STOCKALM.CODFAMTALLAV, dbo.STOCKALM.CODTALLAH, " +
            //              " dbo.STOCKALM.CODTALLAV, dbo.STOCKALM.ID, dbo.TALLAS.ID AS IDTALLAH, TALLAS_1.ID AS IDTALLAV " +
            //              " FROM dbo.STOCKALM INNER JOIN " +
            //              " dbo.TALLAS ON dbo.STOCKALM.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA AND dbo.STOCKALM.CODTALLAH = dbo.TALLAS.CODTALLA INNER JOIN " +
            //              " dbo.TALLAS AS TALLAS_1 ON dbo.STOCKALM.CODFAMTALLAV = TALLAS_1.CODFAMTALLA AND dbo.STOCKALM.CODTALLAV = TALLAS_1.CODTALLA ";

            //NUEVA TABLA DE TALLAS Y COLORES
            string query = "SELECT " +
                           " '' AS CODALM, CODART, CODFAMTALLAH, CODFAMTALLAV, CODIGO_COLOR, CODIGO_TALLA, ID, ID_COLOR, ID_TALLA " +
                           " FROM " +
                           " dbo.KLS_ESYNC_STOCK_TALLASYCOLORES";

            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(query, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        //nueva función para obtener las combinaciones de tallas y colores 
        public DataTable cargarCombinacionTallasYColores(string filtroAtributos)
        {

            string query =  " SELECT " +
                            " dbo.TALLAS.CODALT, dbo.TALLAS.CODFAMTALLA, dbo.TALLAS.CODTALLA, dbo.TALLAS.DESCRIPCION, dbo.TALLAS.ID, dbo.TALLAS.ORDENTALLA, " +
                            " dbo.TALLAS.KLS_CODCOLOR, dbo.FAMILIATALLA.VERTICAL " +
                            " FROM " +
                            " dbo.TALLAS INNER JOIN " +
                            " dbo.FAMILIATALLA ON dbo.TALLAS.CODFAMTALLA = dbo.FAMILIATALLA.CODFAMTALLA " +
                            " WHERE  dbo.TALLAS.ID IN " + filtroAtributos;

            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(query, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            dataConnection.Close();
            return t;
        }

        public DataTable cargarFamiliasTallasyColores()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            // Cambiamos codfamtalla por kls_idgroup 17/01/17
            // porque el codfamtalla puede ser alfanumerico
            SqlDataAdapter a = new SqlDataAdapter("SELECT KLS_IDGROUP, CASE WHEN VERTICAL='T' THEN 'TALLA' ELSE 'COLOR' END AS DESCRIPCION " +
                                         " FROM FAMILIATALLA", dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public DataTable cargarDatosTabla(string tabla, string campos, string filtro)
        {
            string query = "";
            query = "select " + campos + " from " + tabla + " where " + filtro;
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(query, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }

        public DataTable cargarDatosTabla(string tabla, string camposSelect)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("select " + camposSelect + " from " + tabla, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            return t;
        }



        //public DataTable cargarDatosTablaA3(string consulta)
        //{
        //    try
            
        //    {
        //        // Si no se ha configurado un tipo de conexión, asignar SQL Server por defecto
        //        if (!csGlobal.isODBCConnection && !csGlobal.isMySQLConnection && !csGlobal.isSqlServerConnection)
        //        {

        //            csGlobal.isSqlServerConnection = true; // Predeterminado a SQL Server
        //        }

        //        // Obtener la conexión SQL Server del Singleton
        //        var conexion = ConexionSingleton.ObtenerInstancia();
        //        SqlConnection dataConnection = conexion.GetSqlServerConnection();

        //        // Abrir la conexión si está cerrada
        //        conexion.AbrirConexion();

        //        // Ejecutar la consulta
        //        SqlDataAdapter a = new SqlDataAdapter(consulta, dataConnection);
        //        DataTable t = new DataTable();
        //        a.Fill(t);

        //        // Cerrar la conexión
        //        conexion.CerrarConexion();

        //        return t;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (csGlobal.modoManual)
        //            ex.Message.mb();
        //        return null;
        //    }
        //}

        //public DataTable cargarDatosTablaA3(string consulta)
        //{
        //    try
        //    {
        //        SqlConnection dataConnection = new SqlConnection();
        //        dataConnection.ConnectionString = csGlobal.cadenaConexion;
        //        dataConnection.Open();
        //        SqlDataAdapter a = new SqlDataAdapter(consulta, dataConnection);
        //        DataTable t = new DataTable();
        //        a.Fill(t);
        //        dataConnection.Close();
        //        return t;

        //    }
        //    catch (Exception ex)
        //    {
        //        if (csGlobal.modoManual)
        //            ex.Message.mb();
        //        return null;
        //    }
        //}


        public DataTable cargarDatosTablaA3(string consulta)
        {
            try
            {
                using (SqlConnection dataConnection = new SqlConnection(csGlobal.cadenaConexion))
                {
                    dataConnection.Open();
                    SqlDataAdapter a = new SqlDataAdapter(consulta, dataConnection);
                    DataTable t = new DataTable();
                    a.Fill(t);
                    return t;
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                    ex.Message.mb();
                return null;
            }
        }




        /// <summary>
        /// Esta funcion actualiza el item insertado y devuelve el id de prestashop
        /// </summary>
        /// <param name="tabla"></param>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public string actualizarIdA3(DataTable tabla, string objeto)
        {
            string COD_A3 = "";
            string COD_PS = "";
            string tablaUpdate = "";
            string campoUpdate = "";
            string nombreCampoClave = "";
            string campoFechaSync = "";
            string fechaSync = "";
            string anexoFechasync = "";
            string query = "";

            if (objeto == "customers")
            {
                tablaUpdate = "__CLIENTES";
                campoUpdate = "KLS_CODCLIENTE";
                nombreCampoClave = "CODCLI";
            }
            else if (objeto == "products")
            {
                tablaUpdate = "ARTICULO";
                campoUpdate = "KLS_ID_SHOP";
                nombreCampoClave = "CODART";
            }
            else if (objeto == "categories")
            {
                tablaUpdate = "KLS_CATEGORIAS";
                campoUpdate = "KLS_COD_PS";
                nombreCampoClave = "KLS_CODCATEGORIA";
            }
            else if (objeto == "addresses")
            {
                tablaUpdate = "DIRENT";
                campoUpdate = "ID_PS";
                nombreCampoClave = "IDDIRENT";
                campoFechaSync = "KLS_ESYNCDATE";
            }



            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                if (campoFechaSync != "")
                {
                    anexoFechasync = " , KLS_ESYNCDATE= '" + fila.ItemArray[2].ToString() + "' ";
                }

                COD_A3 = fila.ItemArray[1].ToString();
                COD_PS = fila.ItemArray[0].ToString();

                if (objeto == "addresses")
                {
                    datacommand.CommandText = "UPDATE " + tablaUpdate + " SET " + campoUpdate + "=" + COD_PS + anexoFechasync + " WHERE " + nombreCampoClave + "=" + COD_A3;
                }
                else
                {
                    anexoFechasync = "";
                    datacommand.CommandText = "UPDATE " + tablaUpdate + " SET " + campoUpdate + "=" + COD_PS + anexoFechasync + " WHERE LTRIM(" + nombreCampoClave + ")='" + COD_A3 + "'";
                }
                datacommand.ExecuteNonQuery();
            }

            cerrarConexion();

            return COD_PS;
        }

        //indicamos si un artículo será visible o activo en la tienda o no
        public void actualizarArticuloA3(DataTable tabla, bool activar)
        {
            string id_product = "";
            string codart = "";
            string activarArticulo = "F";
            string bloquearArticulo = "F";
            csRepLog repLog = new csRepLog();

            if (activar)
            {
                activarArticulo = "T";
                bloquearArticulo = "F";
            }
            else
            {
                activarArticulo = "F";
                bloquearArticulo = "T";
            }

            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                codart = fila.ItemArray[0].ToString();
                //id_product = fila.ItemArray[1].ToString();

                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "UPDATE ARTICULO SET  KLS_ARTICULO_TIENDA='" + activarArticulo + "', KLS_ARTICULO_BLOQUEAR='" + bloquearArticulo + "' WHERE LTRIM(CODART)='" + codart + "'";
                datacommand.ExecuteNonQuery();

                //Añado la actualización en la tabla replog del articulo modificado
                repLog.insertarRegistroRegLog(codart, "ARTICULO", repLog.MOD);

            }
            cerrarConexion();
        }

        public void actualizarClienteA3(DataTable tabla, bool activar)
        {
            string codcli = "";

            string activarArticulo = "F";

            if (activar)
            {
                activarArticulo = "T";
            }
            else
            {
                activarArticulo = "F";
            }

            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                codcli = fila.ItemArray[0].ToString();

                datacommand.CommandText = "UPDATE __CLIENTES SET KLS_CLIENTE_TIENDA='" + activarArticulo + "' WHERE LTRIM(CODCLI)='" + codcli.Trim() + "'";
                datacommand.ExecuteNonQuery();
            }
            cerrarConexion();
        }

        //public void actualizarCampo(string consulta, bool gestionarConexiones=true)
        //{
        //    if (gestionarConexiones) abrirConexion();
        //    SqlCommand datacommand = new SqlCommand();

        //    datacommand.Connection = dataconnection;

        //    datacommand.CommandText = consulta;
        //    datacommand.ExecuteNonQuery();

        //    if (gestionarConexiones) cerrarConexion();
        //}

        public void actualizarCampo(string consulta, bool gestionarConexiones = true)
        {
            if (gestionarConexiones)
                abrirConexion();

            try
            {
                using (SqlCommand datacommand = new SqlCommand(consulta, dataconnection))
                {
                    datacommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                    MessageBox.Show(ex.Message);
            }
            finally
            {
                if (gestionarConexiones)
                    cerrarConexion();
            }
        }



        public void actualizarCampoA3(DataTable dt, string tabla, string campos)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in dt.Rows)
            {
                datacommand.CommandText = "UPDATE " + tabla + " SET " + campos + "='" + fila.ItemArray[0].ToString() + "' WHERE LTRIM(CODART)='" + fila.ItemArray[1].ToString().Replace(" ", "") + "'";
                datacommand.ExecuteNonQuery();
            }

            cerrarConexion();
        }

        public void actualizarIdPSenA3ERP(string codart, string idPS)
        {
            try
            {
                //Abrimos la conexión llamando al método definido arriba
                abrirConexion();
                //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
                SqlCommand datacommand = new SqlCommand();

                datacommand.Connection = dataconnection;

                datacommand.CommandText = "UPDATE ARTICULO SET KLS_ID_SHOP=" + idPS + " WHERE LTRIM(CODART)='" + codart + "'";
                datacommand.ExecuteNonQuery();
                cerrarConexion();
            }
            catch
            {
                cerrarConexion();
            }
        }

        /**
         * Actualiza el campo de la tabla
         * especificada a NULL
         * 
         * @params tabla type=string
         * @params campo type=string
         * 
         */
        public void actualizarANull(string tabla, string campo)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            datacommand.CommandText = "UPDATE " + tabla + " SET " + campo + "= NULL";
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public void actualizarANull(string tabla, string campo, string where)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            datacommand.CommandText = "UPDATE " + tabla + " SET " + campo + "= NULL " + where;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }

        public void insertarValoresEnTabla(string tabla, string campos, string values)
        {
            try
            {
                string script = "";
                //Abrimos la conexión llamando al método definido arriba
                abrirConexion();
                //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
                SqlCommand datacommand = new SqlCommand();

                datacommand.Connection = dataconnection;
                script = "INSERT INTO " + tabla + " " + campos + " VALUES " + values;
                datacommand.CommandText = script;
                datacommand.ExecuteNonQuery();

                cerrarConexion();
            }
            catch (Exception)
            {
            }

        }

        public void consultaInsertar(string consulta)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.CommandTimeout = 9000;
            datacommand.Connection = dataconnection;
            datacommand.CommandText = consulta;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }


        public void ejecutarQuery(string consulta)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.CommandTimeout = 9000;
            datacommand.Connection = dataconnection;
            datacommand.CommandText = consulta;
            datacommand.ExecuteNonQuery();

            cerrarConexion();
        }





        public int insert(string consulta)
        {
            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL



            cerrarConexion();

            return 0;
        }

        public string[] obtenerTrabajadoresRepasat()
        {
            string[] Listado = new string[1];
            try
            {
                SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
                SqlCommand datacomand = new SqlCommand();
                csSqlScriptRepasat ScriptRepasat = new csSqlScriptRepasat();
                SqlDataAdapter adapt = new SqlDataAdapter(ScriptRepasat.selectTrabajadoresActividadesRPST(), conn);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                conn.Close();
                //String[] Listado;
                Array.Resize(ref Listado, ds.Tables[0].Rows.Count);
                //Listado = new string[];
                int i = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Listado[i] = row.ItemArray[0].ToString();
                    i++;
                }
                if (ds.Tables[0].Rows.Count == 0)
                {
                    Array.Resize(ref Listado, 1);
                    Listado[0] = "2";
                }

            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.Message);

            }
            return Listado;
        }

        public void actualizarclientesEnlazados(DataTable tabla)
        {
            string codCli = "";
            string idCliRPST = "";

            //Abrimos la conexión llamando al método definido arriba
            abrirConexion();
            //Creamos un objeto SqlCommand que nos permitirá interactuar con SQL
            SqlCommand datacommand = new SqlCommand();

            datacommand.Connection = dataconnection;

            foreach (DataRow fila in tabla.Rows)
            {
                codCli = fila.ItemArray[3].ToString();
                idCliRPST = fila.ItemArray[1].ToString();
                //id_product = fila.ItemArray[1].ToString();

                ////MessageBox.Show(fila.ItemArray[0].ToString());
                datacommand.CommandText = "UPDATE __CLIENTES SET  KLS_CODCLIENTE='" + idCliRPST + "' WHERE LTRIM(CODCLI)='" + codCli + "'";
                datacommand.ExecuteNonQuery();
            }
            cerrarConexion();
        }

        public string caracteristicasXMLProductos(string articulo, DataTable articulosToUpdate = null)
        {
            string result = "";
            string codCaracteristica = "";
            string valorCaracteristica = "";
            string query = "SELECT dbo.KLS_CARACTERISTICAS.KLS_CODCARACTERISTICA, dbo.KLS_CARACTERISTICAS.KLS_VALORCARACTERISTICA " +
                           " FROM dbo.KLS_CARACTERISTICAS INNER JOIN " +
                           " dbo.KLS_CARACTERISTICAS_ART ON " +
                           " dbo.KLS_CARACTERISTICAS.KLS_VALORCARACTERISTICA = dbo.KLS_CARACTERISTICAS_ART.KLS_VALORCARACTERISTICA " +
                           " WHERE     (LTRIM(dbo.KLS_CARACTERISTICAS_ART.CODART) = '" + articulo + "')";
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();
            SqlDataAdapter adapt = new SqlDataAdapter(query, conn);
            //SqlDataAdapter adapt = new SqlDataAdapter("SELECT DBO.FAMILIAS.COD_PS FROM DBO.FAMILIAS JOIN DBO.ARTICULO ON DBO.ARTICULO.FAMARTDESCVEN = DBO.FAMILIAS.CODFAM WHERE LTRIM(DBO.ARTICULO.CODART)='" + codArt + "'", conn); 

            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();

            if (ds.Tables[0].Rows.Count == 0 & articulosToUpdate.Rows.Count == 0)
            {
                result = "<product_features>" +
                            "<product_feature>" +
                                "<id/>" +
                                "<custom/>" +
                                "<id_feature_value/>" +
                            "</product_feature>" +
                        "</product_features>";
            }
            else
            {
                result = "<product_features>";

                if (articulosToUpdate.Rows.Count > 0)
                {
                    string[] caracteristicas;
                    string[] valorCaracteristicas;
                    string[] conjuntoCaracteristica;
                    string caracteristicasPS = articulosToUpdate.Rows[0]["caracteristicas"].ToString();
                    int registros = 0;

                    conjuntoCaracteristica = caracteristicasPS.Split('|');
                    caracteristicas = conjuntoCaracteristica[0].Split(',');
                    valorCaracteristicas = conjuntoCaracteristica[1].Split(',');
                    registros = caracteristicas.Count();
                    for (int i = 0; i < registros; i++)
                    {
                        result = result + "<product_feature>" +
                                "<id>" + caracteristicas[i] + "</id>" +
                                "<custom/>" +
                                "<id_feature_value>" + valorCaracteristicas[i] + "</id_feature_value>" +
                            "</product_feature>";

                    }
                    result = result + "</product_features>";

                }
                else
                {

                    //adapt.Fill(ds);

                    //String[] Listado;

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        codCaracteristica = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                        valorCaracteristica = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                        result = result + "<product_feature>" +
                                            "<id>" + codCaracteristica + "</id>" +
                                            "<custom/>" +
                                            "<id_feature_value>" + valorCaracteristica + "</id_feature_value>" +
                                        "</product_feature>";
                    }
                    result = result + "</product_features>";
                }
            }
            return result;
        }

        public DataTable obtenerMarcasNuevas(string marcasWeb)
        {

            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            SqlDataAdapter adapt = new SqlDataAdapter("SELECT LTRIM([KLS_CODMARCA]) AS CODIGO,[KLS_DESCMARCA] AS FABRICANTE FROM KLS_MARCAS WHERE KLS_CODMARCA NOT IN (" + marcasWeb + ")", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();

            return ds.Tables[0];

        }
        

        //obtengo los datos que utilizaré para enviar el mail
        public DataTable obtenerCabeceraParaMail(string numDoc, string serie)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            SqlDataAdapter adapt = new SqlDataAdapter("SELECT " +
                      " dbo.CABEPEDV.CODCLI,dbo.CABEPEDV.NUMDOC,dbo.CABEPEDV.IDPEDV, dbo.CABEPEDV.IDDIRENT, dbo.CABEPEDV.NOMCLI, dbo.CABEPEDV.FECHA, dbo.CABEPEDV.REFERENCIA, dbo.DIRENT.DIRENT1, " +
                      " dbo.CLIENTES.TELCLI, dbo.DIRENT.POBENT, dbo.DIRENT.NOMENT, dbo.DIRENT.DTOENT, dbo.PROVINCI.NOMPROVI, dbo.CLIENTES.NIFCLI, dbo.CLIENTES.KLS_EMAIL_USUARIO, " +
                      " CLIENTES.NIFCLI, paises.NOMPAIS, clientes.telcli2 " +
                      " FROM " +
                      " dbo.CABEPEDV LEFT JOIN  " +
                      "  dbo.DIRENT ON dbo.CABEPEDV.IDDIRENT = dbo.DIRENT.IDDIRENT LEFT JOIN " +
                      " dbo.PROVINCI ON dbo.DIRENT.CODPROVI = dbo.PROVINCI.CODPROVI LEFT JOIN " +
                      " dbo.CLIENTES ON dbo.CABEPEDV.CODCLI = dbo.CLIENTES.CODCLI " +
                      " left join paises on dirent.codpais = paises.CODPAIS " +
                       " WHERE CABEPEDV.IDPEDV = '" + numDoc + "'", conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();

            return ds.Tables[0];
        }

        public DataTable obtenerCabeceraParaMailAlbaran(string numDoc, string serie)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            string sb = string.Format("SELECT dbo.CABEALBV.CODCLI, " +
            "       dbo.CABEALBV.NUMDOC, " +
            "       dbo.CABEALBV.idalbv, " +
            "       dbo.CABEALBV.IDDIRENT, " +
            "       dbo.CABEALBV.NOMCLI, " +
            "       dbo.CABEALBV.FECHA, " +
            "       dbo.CABEALBV.REFERENCIA, " +
            "       dbo.DIRENT.DIRENT1, " +
            "       dbo.CLIENTES.TELCLI, " +
            "       dbo.DIRENT.POBENT, " +
            "       dbo.DIRENT.NOMENT, " +
            "       dbo.DIRENT.DTOENT, " +
            "       dbo.PROVINCI.NOMPROVI, " +
            "       dbo.CLIENTES.NIFCLI, " +
            "       dbo.CLIENTES.KLS_EMAIL_USUARIO, " +
            "       CLIENTES.NIFCLI, " +
            "       paises.NOMPAIS, " +
            "       clientes.telcli2 " +
            "FROM dbo.CABEALBV " +
            "LEFT JOIN dbo.DIRENT ON dbo.CABEALBV.IDDIRENT = dbo.DIRENT.IDDIRENT " +
            "LEFT JOIN dbo.PROVINCI ON dbo.DIRENT.CODPROVI = dbo.PROVINCI.CODPROVI " +
            "LEFT JOIN dbo.CLIENTES ON dbo.CABEALBV.CODCLI = dbo.CLIENTES.CODCLI " +
            "LEFT JOIN paises ON dirent.codpais = paises.CODPAIS " +
            "WHERE CABEALBV.idalbv = '{0}'", numDoc);

            SqlDataAdapter adapt = new SqlDataAdapter(sb, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();

            return ds.Tables[0];
        }



        public DataTable obtenerCabeceraPedidosV(string fechaDesde, string fechaHasta)
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            string sb = string.Format("SELECT dbo.CABEPEDV.CODCLI, " +
                        "       dbo.CABEPEDV.NUMDOC, " +
                        "       dbo.CABEPEDV.IDPEDV, " +
                        "       dbo.CABEPEDV.IDDIRENT, " +
                        "       dbo.CABEPEDV.NOMCLI, " +
                        "       dbo.CABEPEDV.FECHA, " +
                        "       dbo.CABEPEDV.REFERENCIA " +
                        "FROM dbo.CABEPEDV " +
                        "WHERE situaciondetalle IN ('PENDIENTE', " +
                        "                           'PENDIENTE PARCIAL') " +
                        "  AND dbo.CABEPEDV.FECHA >= '{0}' " +
                        "  AND dbo.CABEPEDV.FECHA <= '{1}' ", fechaDesde, fechaHasta);

            SqlDataAdapter adapt = new SqlDataAdapter(sb, conn);
            DataSet ds = new DataSet();
            adapt.Fill(ds);
            conn.Close();

            return ds.Tables[0];
        }

        public DataTable cargarTabla(string tabla, string campos, string where = "", string order_by = "")
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            SqlDataAdapter adapt = new SqlDataAdapter("select " + campos + " from " + tabla + where + order_by, conn);
            DataTable dt = new DataTable();
            adapt.Fill(dt);
            conn.Close();

            return dt;
        }

        public string obtenerCampoTabla(string consulta, bool conexionAbierta=true)
         {
            
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            if (!conexionAbierta)
            {
                conn.Open();
            }

            SqlDataAdapter adapt = new SqlDataAdapter(consulta, conn);
            DataTable dt = new DataTable();
            adapt.Fill(dt);


            if (!conexionAbierta)
            {
                conn.Close();
            }

            if (dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();
            else
                return "";
        }

        public string obtenerNombreEmpresaA3()
        {
            SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);
            SqlCommand datacomand = new SqlCommand();

            SqlDataAdapter adapt = new SqlDataAdapter("SELECT DESCRIPCION FROM A3ERP$SISTEMA.DBO.EMPRESAS  A3ERP$SISTEMA where databasename='" + csGlobal.databaseA3 + "'", conn);
            DataTable dt = new DataTable();
            adapt.Fill(dt);
            conn.Close();

            return dt.Rows[0][0].ToString();
        }


        public bool probarConexionA3()
        {
            try
            {
                using (var conexion = new SqlConnection(csGlobal.cadenaConexion))
                {
                    conexion.Open();

                    using (var comando = new SqlCommand("SELECT TOP 1 CODCLI FROM CLIENTES ORDER BY CODCLI DESC", conexion))
                    {
                        using (var reader = comando.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                //MessageBox.Show("Conexión con A3ERP establecida. Primer registro: " + reader[0].ToString());
                                string idCli = reader["CODCLI"].ToString();
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
    }
}