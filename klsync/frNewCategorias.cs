﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frNewCategorias : Form
    {
        private static frNewCategorias m_FormDefInstance;
        public static frNewCategorias DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frNewCategorias();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frNewCategorias()
        {
            InitializeComponent();
        }

        private void cargarCategoriasPS()
        {
            csSqlScripts mySqlScript = new csSqlScripts();
            csMySqlConnect mysql = new csMySqlConnect();

            dgvCategorias.DataSource = mysql.cargarTabla(mySqlScript.selectCategoriasPS());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = csGlobal.ServerA3;
            csb.InitialCatalog = csGlobal.databaseA3;
            csb.IntegratedSecurity = true;
            csb.UserID = csGlobal.userA3;
            csb.Password = csGlobal.passwordA3;
            return csb.ConnectionString;
        }

        private void conectarDB(string sqlScript, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();

            try { dgv.DataSource = conector.cargarTabla(sqlScript); }
            catch (MySql.Data.MySqlClient.MySqlException) {}
        }

        private void copiarCategoriasPStoA3()
        {
            csSqlConnects sql = new csSqlConnects();
            csNetUtilities utilidades = new csNetUtilities();
            DataTable categorias = csUtilidades.fillDataTableFromDataGridView(dgvCategorias, false);
            
            sql.insertarCategoriasA3(categorias);
            cargarCategoriasA3();
        }

        private void cargarCategoriasA3()
        {
            csSqlConnects sql = new csSqlConnects();

            dgvCategorias.DataSource = sql.cargarDatosTablaA3("SELECT KLS_CODCATEGORIA, KLS_DESCCATEGORIA FROM dbo.KLS_CATEGORIAS");
        }

        private void btCargarCategoriasA3_Click(object sender, EventArgs e)
        {
            cargarCategoriasA3();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            copiarCategoriasPStoA3();
        }

        private void frCategorias_Load(object sender, EventArgs e)
        {
            cargarCategoriasPS();
        }

        private void cargarCategoríasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarCategoriasPS();
        }

        private void copiarCategoríasSeleccionadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copiarCategoriasPStoA3();
        }

        private void cargarFamiliasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarFam();
        }

        private void cargarFam()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a;

            a = new SqlDataAdapter(sqlScript.selectFamilias(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvFamCat.DataSource = t;
        }

        private void exportarSeleccionadasAPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();

            try
            {
                foreach (DataGridViewRow fila in dgvFamCat.SelectedRows)
                {
                    try
                    {
                        psWebService.cdPSWebService("POST", "categories", "", fila.Cells["COD_PS"].Value.ToString(), true, "");
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(ex.ToString());
                    }
                }
            }
            catch (Exception ex) { }

            cargarFam(); 
        }

        private void tabCatCar_Click(object sender, EventArgs e)
        {
            if (tabCatCar.SelectedTab == tabCatCar.TabPages[0])
            {
                cargarCategoriasA3();
            }
            else if (tabCatCar.SelectedTab == tabCatCar.TabPages[1])
            {
                cargarFam();
            }
            else if (tabCatCar.SelectedTab == tabCatCar.TabPages[2])
            { }
        }

        private void cargarCaracterísticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(mySqlScript.selectCaracteristicasPS(), dgvCaracteristicasPS);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cargarCaracteristicasA3v2();
        }

        private void cargarCaracteristicasA3v2()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectCaracteristicasA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvCaracteristicasA3.DataSource = t;
        }

        private void copiarCaracterísticasToolStripMenuItem_Click(object sender, EventArgs e)
        {
             CopiarCaracteristicasToA3();
        }

        private void CopiarCaracteristicasToA3()
        {

            csSqlConnects SqlConnects = new csSqlConnects();
            csNetUtilities utilidades = new csNetUtilities();

            SqlConnects.insertarCaracteristicasA3(utilidades.PopulateDataTable(dgvCaracteristicasPS));
            cargarCaracteristicasA3v2();

        }

        private void cargarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string consulta = "Select ps_product.id_product, ps_feature_product.id_product As id_product1, ps_feature_product.id_feature_value, ps_feature_product.id_feature, ps_product.reference From ps_product Inner Join ps_feature_product On ps_product.id_product = ps_feature_product.id_product";
            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(consulta, dgvFeaturesProducts);
        }
    }
}
