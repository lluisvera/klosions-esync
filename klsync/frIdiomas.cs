﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace klsync
{
    public partial class frIdiomas : Form
    {


        private static frIdiomas m_FormDefInstance;
        public static frIdiomas DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frIdiomas();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }



        public frIdiomas()
        {
            InitializeComponent();
        }

        private void frIdiomas_Load(object sender, EventArgs e)
        {

        }

        private void btLoadIdiomas_Click(object sender, EventArgs e)
        {
            cargarIdiomasA3();
            cargarIdiomasPS();
            cargarIdiomasEnlazados();
        }

        private void cargarIdiomasA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectIdiomasA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvIdiomasA3.DataSource = t;
        }



        private void cargarIdiomasPS()
        {
            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(mySqlScript.selectIdiomasPS(), dgvIdiomasPS);

        }


        private void cargarIdiomasEnlazados()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectIdiomasEnlazados(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvIdiomasEnlazados.DataSource = t;
        }


        //Para Borrar se pasa mediante variable Global
        //public string CadenaConexion()
        //{
        //    SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
        //    csb.DataSource = csGlobal.ServerA3;
        //    csb.InitialCatalog = csGlobal.databaseA3;
        //    csb.IntegratedSecurity = true;
        //    csb.UserID = csGlobal.userA3;
        //    csb.Password = csGlobal.passwordA3;
        //    return csb.ConnectionString;
        //}

        private void conectarDB(string sqlScript, DataGridView dgv)
        {

            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;


            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }

        private void btEnlazarIdiomas_Click(object sender, EventArgs e)
        {

            try
            {
                if (dgvIdiomasEnlazados.Columns.Count < 2)
                {
                    dgvIdiomasEnlazados.Columns.Add("Col1", "Col1");
                    dgvIdiomasEnlazados.Columns.Add("Col2", "Col2");
                }
                //DataGridViewRow row = (DataGridViewRow)dgvIdiomasEnlazados.Rows[0].Clone();
            }
            catch (Exception ex)
            {

            }
            //Convierto El datasource en un datatable para poder añadir nuevos datos
            DataTable dt = dgvIdiomasEnlazados.DataSource as DataTable;
            DataRow nuevoIdiomaEnlazado = dt.NewRow();
            nuevoIdiomaEnlazado[0] = dgvIdiomasPS.SelectedRows[0].Cells[0].Value.ToString();
            nuevoIdiomaEnlazado[1] = dgvIdiomasA3.SelectedRows[0].Cells[0].Value.ToString();
            dt.Rows.Add(nuevoIdiomaEnlazado);
            dgvIdiomasEnlazados.DataSource = dt;

                //dgvIdiomasEnlazados.Rows.Add(dgvIdiomasPS.SelectedRows[0].Cells[0].Value.ToString(), dgvIdiomasA3.SelectedRows[0].Cells[0].Value.ToString());
                dgvIdiomasPS.Rows.Remove(dgvIdiomasPS.SelectedRows[0]);
                dgvIdiomasA3.Rows.Remove(dgvIdiomasA3.SelectedRows[0]);


        }

        private void btGrabarEnlaces_Click(object sender, EventArgs e)
        {
            csSqlConnects mySqlConnects = new csSqlConnects();
            csNetUtilities utilidades = new csNetUtilities();

            mySqlConnects.insertarIdiomasEnlazados(utilidades.PopulateDataTable(dgvIdiomasEnlazados));
            mySqlConnects.cargarIdiomas();

            //MessageBox.Show("Idiomas Grabados");
        }

        private void dgvIdiomasPS_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
            dgvIdiomasPS.Rows[e.RowIndex].Selected = true;
        }

        private void dgvIdiomasA3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvIdiomasA3.Rows[e.RowIndex].Selected = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            csIdiomas Idiomas = new csIdiomas();
            int i = 0;
            string[] idioma=new string[Idiomas.idiomasActivosPS().Count()];
            idioma=Idiomas.idiomasActivosPS();
         
            foreach (string codIdioma in idioma)
            {
                //MessageBox.Show (codIdioma.ToString());
            }
        }

        private void btnFixLang_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Clear();
            dict.Add("de", "ALE");
            dict.Add("es", "CAS");
            dict.Add("ca", "CAT");
            dict.Add("us", "ING");
            dict.Add("gb", "ING");
            dict.Add("en", "ING");
            dict.Add("it", "ITA");
            dict.Add("pt", "POR"); 

            DataTable ps = new DataTable();

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string consulta = "", tabla = "KLS_ESYNC_IDIOMAS", idiomaps = "";
            

            sql.borrarDatosSqlTabla(tabla);
            ps = mysql.cargarTabla("select id_lang, iso_code from ps_lang where active = 1");
            foreach (DataRow dr in ps.Rows)
            {
                foreach (KeyValuePair<string, string> entry in dict)
                {
                    idiomaps = dr["iso_code"].ToString();
                    if (entry.Key == idiomaps)
                    {
                        consulta = "insert into " + tabla + " (IDIOMAA3, IDIOMAPS) values ('"+entry.Value+"',"+dr["id_lang"].ToString()+")";
                        csUtilidades.ejecutarConsulta(consulta, false);
                        break;
                    }
                }
            }

            cargarIdiomasA3();
            cargarIdiomasPS();
            cargarIdiomasEnlazados();
        }        

    }
}
