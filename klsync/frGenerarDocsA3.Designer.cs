﻿namespace klsync
{
    partial class frGenerarDocsA3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frGenerarDocsA3));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.cBoxSerie = new System.Windows.Forms.ComboBox();
            this.lblRefPedido = new System.Windows.Forms.Label();
            this.txtRefPedido = new System.Windows.Forms.TextBox();
            this.rbtnMultipleDocument = new System.Windows.Forms.RadioButton();
            this.rbtnSingleDocument = new System.Windows.Forms.RadioButton();
            this.lblFechaDoc = new System.Windows.Forms.Label();
            this.dtpFechaDoc = new System.Windows.Forms.DateTimePicker();
            this.tbColumnas = new System.Windows.Forms.TextBox();
            this.lblTipoDocumento = new System.Windows.Forms.Label();
            this.cboxTipoDocumento = new System.Windows.Forms.ComboBox();
            this.lblTercero = new System.Windows.Forms.Label();
            this.cboxTercero = new System.Windows.Forms.ComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cboxModulo = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGenerarDocumento = new System.Windows.Forms.ToolStripButton();
            this.dgvLineasDocumentos = new System.Windows.Forms.DataGridView();
            this.contextMenuRightBtn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarLineaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbStStatus = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasDocumentos)).BeginInit();
            this.contextMenuRightBtn.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cBoxSerie);
            this.splitContainer1.Panel1.Controls.Add(this.lblRefPedido);
            this.splitContainer1.Panel1.Controls.Add(this.txtRefPedido);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnMultipleDocument);
            this.splitContainer1.Panel1.Controls.Add(this.rbtnSingleDocument);
            this.splitContainer1.Panel1.Controls.Add(this.lblFechaDoc);
            this.splitContainer1.Panel1.Controls.Add(this.dtpFechaDoc);
            this.splitContainer1.Panel1.Controls.Add(this.tbColumnas);
            this.splitContainer1.Panel1.Controls.Add(this.lblTipoDocumento);
            this.splitContainer1.Panel1.Controls.Add(this.cboxTipoDocumento);
            this.splitContainer1.Panel1.Controls.Add(this.lblTercero);
            this.splitContainer1.Panel1.Controls.Add(this.cboxTercero);
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvLineasDocumentos);
            this.splitContainer1.Size = new System.Drawing.Size(1194, 714);
            this.splitContainer1.SplitterDistance = 356;
            this.splitContainer1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Serie";
            // 
            // cBoxSerie
            // 
            this.cBoxSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxSerie.FormattingEnabled = true;
            this.cBoxSerie.Location = new System.Drawing.Point(174, 189);
            this.cBoxSerie.Name = "cBoxSerie";
            this.cBoxSerie.Size = new System.Drawing.Size(88, 21);
            this.cBoxSerie.TabIndex = 12;
            // 
            // lblRefPedido
            // 
            this.lblRefPedido.AutoSize = true;
            this.lblRefPedido.Location = new System.Drawing.Point(21, 288);
            this.lblRefPedido.Name = "lblRefPedido";
            this.lblRefPedido.Size = new System.Drawing.Size(94, 13);
            this.lblRefPedido.TabIndex = 11;
            this.lblRefPedido.Text = "Referencia pedido";
            // 
            // txtRefPedido
            // 
            this.txtRefPedido.Location = new System.Drawing.Point(24, 304);
            this.txtRefPedido.Name = "txtRefPedido";
            this.txtRefPedido.Size = new System.Drawing.Size(152, 20);
            this.txtRefPedido.TabIndex = 10;
            // 
            // rbtnMultipleDocument
            // 
            this.rbtnMultipleDocument.AutoSize = true;
            this.rbtnMultipleDocument.Location = new System.Drawing.Point(135, 84);
            this.rbtnMultipleDocument.Name = "rbtnMultipleDocument";
            this.rbtnMultipleDocument.Size = new System.Drawing.Size(129, 17);
            this.rbtnMultipleDocument.TabIndex = 9;
            this.rbtnMultipleDocument.Text = "Múltiples Documentos";
            this.rbtnMultipleDocument.UseVisualStyleBackColor = true;
            this.rbtnMultipleDocument.CheckedChanged += new System.EventHandler(this.rbtnMultipleDocument_CheckedChanged);
            // 
            // rbtnSingleDocument
            // 
            this.rbtnSingleDocument.AutoSize = true;
            this.rbtnSingleDocument.Checked = true;
            this.rbtnSingleDocument.Location = new System.Drawing.Point(24, 84);
            this.rbtnSingleDocument.Name = "rbtnSingleDocument";
            this.rbtnSingleDocument.Size = new System.Drawing.Size(89, 17);
            this.rbtnSingleDocument.TabIndex = 8;
            this.rbtnSingleDocument.TabStop = true;
            this.rbtnSingleDocument.Text = "1 Documento";
            this.rbtnSingleDocument.UseVisualStyleBackColor = true;
            this.rbtnSingleDocument.CheckedChanged += new System.EventHandler(this.rbtnSingleDocument_CheckedChanged);
            // 
            // lblFechaDoc
            // 
            this.lblFechaDoc.AutoSize = true;
            this.lblFechaDoc.Location = new System.Drawing.Point(21, 230);
            this.lblFechaDoc.Name = "lblFechaDoc";
            this.lblFechaDoc.Size = new System.Drawing.Size(95, 13);
            this.lblFechaDoc.TabIndex = 7;
            this.lblFechaDoc.Text = "Fecha Documento";
            // 
            // dtpFechaDoc
            // 
            this.dtpFechaDoc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaDoc.Location = new System.Drawing.Point(24, 246);
            this.dtpFechaDoc.Name = "dtpFechaDoc";
            this.dtpFechaDoc.Size = new System.Drawing.Size(121, 20);
            this.dtpFechaDoc.TabIndex = 6;
            // 
            // tbColumnas
            // 
            this.tbColumnas.Location = new System.Drawing.Point(24, 353);
            this.tbColumnas.Multiline = true;
            this.tbColumnas.Name = "tbColumnas";
            this.tbColumnas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbColumnas.Size = new System.Drawing.Size(267, 308);
            this.tbColumnas.TabIndex = 5;
            this.tbColumnas.Text = "COLUMNAS A INCORPORAR (TEST)\r\n(INCLUIR TITULOS COLUMNA AL PEGAR)\r\n\r\nCOLUMNAS\r\n1 -" +
    " CODART\r\n2 - COLOR\r\n3 - TALLAS\r\n4 - CANTIDAD\r\n5 - PRECIO\r\n";
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.AutoSize = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(21, 173);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(101, 13);
            this.lblTipoDocumento.TabIndex = 4;
            this.lblTipoDocumento.Text = "Tipo de Documento";
            // 
            // cboxTipoDocumento
            // 
            this.cboxTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTipoDocumento.FormattingEnabled = true;
            this.cboxTipoDocumento.Items.AddRange(new object[] {
            "PEDIDO",
            "ALBARÁN",
            "FACTURA"});
            this.cboxTipoDocumento.Location = new System.Drawing.Point(24, 189);
            this.cboxTipoDocumento.Name = "cboxTipoDocumento";
            this.cboxTipoDocumento.Size = new System.Drawing.Size(121, 21);
            this.cboxTipoDocumento.TabIndex = 3;
            // 
            // lblTercero
            // 
            this.lblTercero.AutoSize = true;
            this.lblTercero.Location = new System.Drawing.Point(21, 122);
            this.lblTercero.Name = "lblTercero";
            this.lblTercero.Size = new System.Drawing.Size(75, 13);
            this.lblTercero.TabIndex = 2;
            this.lblTercero.Text = "Codigo Cliente";
            // 
            // cboxTercero
            // 
            this.cboxTercero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTercero.FormattingEnabled = true;
            this.cboxTercero.Location = new System.Drawing.Point(24, 138);
            this.cboxTercero.Name = "cboxTercero";
            this.cboxTercero.Size = new System.Drawing.Size(238, 21);
            this.cboxTercero.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.cboxModulo,
            this.toolStripSeparator1,
            this.btnGenerarDocumento});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(356, 54);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(58, 51);
            this.toolStripLabel1.Text = "MÓDULO";
            // 
            // cboxModulo
            // 
            this.cboxModulo.Items.AddRange(new object[] {
            "VENTAS",
            "COMPRAS"});
            this.cboxModulo.Name = "cboxModulo";
            this.cboxModulo.Size = new System.Drawing.Size(121, 54);
            this.cboxModulo.TextChanged += new System.EventHandler(this.cboxModulo_TextChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 54);
            // 
            // btnGenerarDocumento
            // 
            this.btnGenerarDocumento.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerarDocumento.Image")));
            this.btnGenerarDocumento.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnGenerarDocumento.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGenerarDocumento.Name = "btnGenerarDocumento";
            this.btnGenerarDocumento.Size = new System.Drawing.Size(76, 51);
            this.btnGenerarDocumento.Text = "Generar Doc";
            this.btnGenerarDocumento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGenerarDocumento.Click += new System.EventHandler(this.btnGenerarDocumento_Click);
            // 
            // dgvLineasDocumentos
            // 
            this.dgvLineasDocumentos.AllowUserToAddRows = false;
            this.dgvLineasDocumentos.AllowUserToDeleteRows = false;
            this.dgvLineasDocumentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLineasDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLineasDocumentos.ContextMenuStrip = this.contextMenuRightBtn;
            this.dgvLineasDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLineasDocumentos.Location = new System.Drawing.Point(0, 0);
            this.dgvLineasDocumentos.Name = "dgvLineasDocumentos";
            this.dgvLineasDocumentos.ReadOnly = true;
            this.dgvLineasDocumentos.Size = new System.Drawing.Size(834, 714);
            this.dgvLineasDocumentos.TabIndex = 1;
            // 
            // contextMenuRightBtn
            // 
            this.contextMenuRightBtn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pegarToolStripMenuItem,
            this.limpiarToolStripMenuItem,
            this.borrarLineaToolStripMenuItem});
            this.contextMenuRightBtn.Name = "contextMenuRightBtn";
            this.contextMenuRightBtn.Size = new System.Drawing.Size(138, 70);
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pegarToolStripMenuItem.Text = "&Pegar";
            this.pegarToolStripMenuItem.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // limpiarToolStripMenuItem
            // 
            this.limpiarToolStripMenuItem.Name = "limpiarToolStripMenuItem";
            this.limpiarToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.limpiarToolStripMenuItem.Text = "&Limpiar";
            this.limpiarToolStripMenuItem.Click += new System.EventHandler(this.limpiarToolStripMenuItem_Click_1);
            // 
            // borrarLineaToolStripMenuItem
            // 
            this.borrarLineaToolStripMenuItem.Name = "borrarLineaToolStripMenuItem";
            this.borrarLineaToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.borrarLineaToolStripMenuItem.Text = "&Borrar Linea";
            this.borrarLineaToolStripMenuItem.Click += new System.EventHandler(this.borrarLineaToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbStStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 692);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1194, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbStStatus
            // 
            this.lbStStatus.Name = "lbStStatus";
            this.lbStStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // frGenerarDocsA3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1194, 714);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frGenerarDocsA3";
            this.Text = "Generador Documentos A3ERP";
            this.Load += new System.EventHandler(this.frGenerarDocsA3_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasDocumentos)).EndInit();
            this.contextMenuRightBtn.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblTipoDocumento;
        private System.Windows.Forms.ComboBox cboxTipoDocumento;
        private System.Windows.Forms.Label lblTercero;
        private System.Windows.Forms.ComboBox cboxTercero;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnGenerarDocumento;
        private System.Windows.Forms.ToolStripComboBox cboxModulo;
        private System.Windows.Forms.Label lblFechaDoc;
        private System.Windows.Forms.DateTimePicker dtpFechaDoc;
        private System.Windows.Forms.TextBox tbColumnas;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightBtn;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbtnMultipleDocument;
        private System.Windows.Forms.RadioButton rbtnSingleDocument;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dgvLineasDocumentos;
        private System.Windows.Forms.ToolStripStatusLabel lbStStatus;
        private System.Windows.Forms.Label lblRefPedido;
        private System.Windows.Forms.TextBox txtRefPedido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBoxSerie;
        private System.Windows.Forms.ToolStripMenuItem borrarLineaToolStripMenuItem;
    }
}