﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a3ERPActiveX;
using System.IO;

namespace klsync
{
    class csa3erpDocs
    {
        public string imprimirFactura(string nombrefichero, string identificador = "72171", string directorioCliente = @"C:\Temp", string modelo = "LSTIMPRFACV.002")
        {
            csa3erp a3erp = new csa3erp();
            Listado factura = new Listado();
            
            a3erp.abrirEnlace();
            string directorio = "";
            
            if (modelo == "DEFECTO")
            {
                modelo = "LSTIMPRFACV.002";
            }

            try
            {
                factura.Iniciar();
                factura.IdListado = modelo.Split('.')[0];

                factura.AnadirParametro("Identificador", identificador);

                if (!Directory.Exists(directorioCliente))
                    Directory.CreateDirectory(directorioCliente);

                factura.Destino = DestinoListado.destPDF;
                factura.Fichero = directorioCliente + "\\" + nombrefichero + ".PDF";
                directorio = directorioCliente + "\\" + nombrefichero + ".PDF";

                factura.Modelo = modelo;
                factura.Imprimir();
            }

            finally
            {
                factura.Acabar();
                a3erp.cerrarEnlace();
            }

            return directorio;
        }


        public string imprimirFacturaSinAbrirEnlace(string nombrefichero, string identificador = "72171", string directorioCliente = @"C:\Temp", string modelo = "LSTIMPRFACV.002")
        {
            Listado factura = new Listado();
            string directorio = "";

            if (modelo == "DEFECTO")
            {
                modelo = "LSTIMPRFACV.002";
            }
            

            try
            {
                factura.Iniciar();
                factura.IdListado = modelo.Split('.')[0];

                factura.AnadirParametro("Identificador", identificador);

                if (!Directory.Exists(directorioCliente))
                    Directory.CreateDirectory(directorioCliente);

                factura.Destino = DestinoListado.destPDF;
                factura.Fichero = directorioCliente + "\\" + nombrefichero + ".PDF";
                directorio = directorioCliente + "\\" + nombrefichero + ".PDF";

                factura.Modelo = modelo;
                factura.Imprimir();
            }

            finally
            {
                factura.Acabar();
            }

            return directorio;
        }

        public void sendReminderPayment()
        {

            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            Listado carta = new Listado();
            string modelo = "DEFECTO";
            string directorio = "";
            string directorioCliente = @"C:\Temp";
            string nombrefichero = "xxx";
            string idListado = "";

            if (modelo == "DEFECTO")
            {
                modelo = "LSTIMPROFEV.000";
            }
            modelo = "LSTIMPROFEV.000";
            modelo = "LSTREOFERVECL.000"; 
            modelo = "LstCartasCobro.000";
            string identificador = "378";

            try
            {
               
                idListado= modelo.Split('.')[0];
                carta.Iniciar();

                carta.IdListado = idListado;
                //carta.IdListado = modelo;

                if (!Directory.Exists(directorioCliente))
                    Directory.CreateDirectory(directorioCliente);

                carta.AnadirParametro("Identificador", "46016");
                carta.AnadirParametro("idcartera", "46016");
                carta.AnadirParametro("numcartera", "38859");
                // carta.AnadirParametro("codini", "1");
                // carta.AnadirParametro("codFin", "zzzz");
                // carta.AnadirParametro("codFin", "1");

                // carta.AnadirParametro("idcartera", "45107");
                // carta.AnadirParametro("NumVen ", "1");
                carta.AnadirParametro("TipCon", "1");
                // carta.AnadirParametro("FecIni", "01/01/2016");
                // carta.AnadirParametro("FecFin","31/12/2019");

                carta.Destino = DestinoListado.destPDF;
                //carta.Destino = DestinoListado.destIMPRESORA;
                carta.Fichero = directorioCliente + "\\" + nombrefichero + ".PDF";
                directorio = directorioCliente + "\\" + nombrefichero + ".PDF";

                carta.Modelo = modelo;
                carta.Imprimir();
            }

            finally
            {
                carta.Acabar();
                a3erp.cerrarEnlace();
            }

        }
    }
}
