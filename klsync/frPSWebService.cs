﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Net.Mail;
using System.Threading;
using a3ERPActiveX;


namespace klsync
{
    public partial class frPSWebService : Form
    {
        private static frPSWebService m_FormDefInstance;
        public static frPSWebService DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPSWebService();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frPSWebService()
        {
            InitializeComponent();
        }

        private void btLoadApi_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();
            psWebService.cdPSWebService("GET", cbApiObjetos.Text, "");
        }


        //Obtener Lista de Nodos

        private void ActualizarEstadoPedido()
        {

            //string documentoXML;
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            xdoc = psWebService.readPSWebService("GET", cbApiObjetos.Text + "/" + textBox1.Text);
            //textBox2.Text = xdoc.ToString();
            XmlNodeList listaNodos = xdoc.GetElementsByTagName("order");
            ////MessageBox.Show(xdoc.SelectSingleNode("//current_state").InnerText.ToString());
            xdoc.SelectSingleNode("//current_state").InnerText = "6";
            StringWriter sw = new StringWriter();
            XmlTextWriter tx = new XmlTextWriter(sw);
            xdoc.WriteTo(tx);

            string str = sw.ToString();
            ////MessageBox.Show(str);
            str = str.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            str = str.Replace("<![CDATA[", "");
            str = str.Replace("]]>", "");
            textBox2.Text = str;

            //psWebService.cdPSWebService("PUT", "orders", str, "", false, ""); //en minúsculas el objeto


            ////MessageBox.Show("Documento Actualizado");



        }



        private void button4_Click(object sender, EventArgs e)
        {
            ActualizarEstadoPedido2();
        }

        private void ActualizarEstadoPedido2()
        {
            csPSWebService psWebService = new csPSWebService();
            psWebService.cdPSWebService("PUT", "orders", textBox2.Text, "", false, ""); //en minúsculas el objeto


        }


        private void ObtenerListaDeNodos()
        {

            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            xdoc = psWebService.readPSWebService("GET", cbApiObjetos.Text + "/" + textBox1.Text);
            //textBox2.Text = xdoc.ToString();
            XmlNodeList listaNodos = xdoc.GetElementsByTagName("order");

            foreach (XmlNode elemento in listaNodos)
            {

                foreach (XmlNode subNodo in elemento.ChildNodes)
                {
                    if (subNodo.Name == "id")
                    {
                        //MessageBox.Show("Id insertado " + subNodo.Name.ToString() + " - " + subNodo.InnerText.ToString());
                    }

                }

            }

        }


        private void ObtenerIdInsertado(XmlDocument xdocInsertado)
        {

            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            xdoc = xdocInsertado;
            //textBox2.Text = xdoc.ToString();
            XmlNodeList listaNodos = xdoc.GetElementsByTagName("products");

            foreach (XmlNode elemento in listaNodos)
            {

                foreach (XmlNode subNodo in elemento.ChildNodes)
                {
                    if (subNodo.Name == "id")
                    {
                        //MessageBox.Show("Id insertado " + subNodo.Name.ToString() + " - " + subNodo.InnerText.ToString());
                    }

                }

            }

        }







        private void button1_Click(object sender, EventArgs e)
        {

            //ObtenerListaDeNodos();
            ActualizarEstadoPedido();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();
            psWebService.cdPSWebService("GET", cbApiObjetos.Text + "/" + textBox1.Text, "");
        }

        private void btExtract_Click(object sender, EventArgs e)
        {
            string valor = "";
            valor = tbOrigen.Text.Replace("<![CDATA[", "");
            valor = valor.Replace("]]>", "");
            tbFinal.Text = valor;

        }

        private DataTable A3CabeceraDocs()
        {
            DataTable CabeceraDocs = new DataTable();
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScripts sqlScript = new csSqlScripts();
            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript.selectDocsPSTest(), conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvDocumentosPSXML.DataSource = bSource;
                CabeceraDocs = table;

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

            return CabeceraDocs;
        }


        private void btXMLOrdere_Click(object sender, EventArgs e)
        {

            A3CabeceraDocs();


        }



        private DataTable A3XMLClientes()
        {
            dgvDetalleLineas.Rows.Clear();
            DataTable lineasDePedidoParaA3 = new DataTable();


            Dictionary<string, string> diccioOrderLines = new Dictionary<string, string>();
            DataRow filaX;

            string[] TyC = new string[4];
            string urlRequest = "";
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();


            urlRequest = "customers/1";
            xdoc = psWebService.readPSWebService("GET", urlRequest);
            //textBox2.Text = xdoc.ToString();
            XmlNodeList listaNodos = xdoc.GetElementsByTagName("customer");
            //for (int ii=0;ii<50;ii++)
            //{
            //    dgvBorrame.Columns.Add("col" + ii, "col" + ii);
            //}
            //
            int i = 0;


            //Añado las Columnas
            foreach (XmlNode elementoName in listaNodos)
            {
                //Leo los valores del Nodo de Clientes
                foreach (XmlNode nodoCliente in elementoName.ChildNodes)
                {
                    dgvBorrame.Columns.Add(nodoCliente.Name.ToString(), nodoCliente.Name.ToString());
                }
            }
            dgvBorrame.Rows.Add();
            foreach (XmlNode elementoValor in listaNodos)
            {
                //Leo los valores del Nodo de Clientes
                foreach (XmlNode nodoCliente in elementoValor.ChildNodes)
                {
                    if (nodoCliente.Name == "firstname")
                    {
                        nodoCliente.InnerText = "LLUIS";
                    }

                    if (nodoCliente.Name == "lastname")
                    {
                        nodoCliente.InnerText = "VERA GORMAZ";
                    }
                    dgvBorrame.Rows[0].Cells[i].Value = nodoCliente.InnerText.ToString();
                    i++;
                }
            }
            textBox2.Text = xdoc.OuterXml;
            xdoc.Save("clientes.xml");

            csPSWebService PSWebService = new csPSWebService();
            PSWebService.cdPSWebService("PUT", "customers", xdoc.OuterXml, "", false, "");




            return lineasDePedidoParaA3;

        }




        private DataTable A3TablaLineas()
        {

            dgvDetalleLineas.Rows.Clear();
            DataTable lineasDePedidoParaA3 = new DataTable();
            int numLinea = 0;
            // dgvDetalleLineas.Rows.Add();
            int numLineaPedido = 0;
            Dictionary<string, string> diccioOrderLines = new Dictionary<string, string>();
            DataRow filaX;

            string[] TyC = new string[4];
            string urlRequest = "";
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            foreach (DataGridViewRow fila in dgvDocumentosPSXML.Rows)
            {
                urlRequest = "orders/" + fila.Cells[0].Value.ToString();
                xdoc = psWebService.readPSWebService("GET", urlRequest);
                //textBox2.Text = xdoc.ToString();
                XmlNodeList listaNodos = xdoc.GetElementsByTagName("order");

                foreach (XmlNode elemento in listaNodos)
                {
                    foreach (XmlNode subNodo in elemento.ChildNodes)
                    {
                        if (subNodo.Name == "associations")
                        {
                            foreach (XmlNode asociaciones in subNodo.ChildNodes)
                            {
                                if (asociaciones.Name == "order_rows")
                                {
                                    foreach (XmlNode lineasPedido in asociaciones.ChildNodes)
                                    {
                                        dgvDetalleLineas.Rows.Add();
                                        DataGridViewRow filaAsignada = dgvDetalleLineas.Rows[numLinea];
                                        numLinea += 1;

                                        if (lineasPedido.Name == "order_row")
                                        {
                                            //COMIENZO A LEER LAS LINEAS DE PEDIDO 

                                            //UTILIZAR EL WEB SERVICE PARA OBTENER EL DETALLE DE LAS LÍNEAS


                                            foreach (XmlNode lineaDePedido in lineasPedido.ChildNodes)
                                            {
                                                if (lineaDePedido.Name == "id")
                                                {
                                                    numLineaPedido = Convert.ToInt32(lineaDePedido.InnerText);
                                                    urlRequest = "order_details/" + numLineaPedido.ToString();
                                                    xdoc = psWebService.readPSWebService("GET", urlRequest);

                                                    //CREO LA TABLA QUE CONTENDRÁ LOS VALORES DE LAS LÍNEAS
                                                    XmlNodeList listaNodosLineas = xdoc.GetElementsByTagName("order_detail");
                                                    if (lineasDePedidoParaA3.Columns.Count == 0)
                                                    {
                                                        foreach (XmlNode elementoLinea in listaNodosLineas)
                                                        {
                                                            foreach (XmlNode campoLinea in elementoLinea.ChildNodes)
                                                            {
                                                                lineasDePedidoParaA3.Columns.Add(campoLinea.Name);
                                                            }
                                                            lineasDePedidoParaA3.Columns.Add("Nom_Atributo_1");
                                                            lineasDePedidoParaA3.Columns.Add("Valor_Atributo_1");
                                                            lineasDePedidoParaA3.Columns.Add("Nom_Atributo_2");
                                                            lineasDePedidoParaA3.Columns.Add("Valor_Atributo_2");
                                                        }
                                                    }

                                                    filaX = lineasDePedidoParaA3.NewRow();
                                                    foreach (XmlNode elementoLinea in listaNodosLineas)
                                                    {
                                                        foreach (XmlNode campoLinea in elementoLinea.ChildNodes)
                                                        {
                                                            filaX[campoLinea.Name] = campoLinea.InnerText;
                                                            if (campoLinea.Name == "product_attribute_id")
                                                            {
                                                                TyC = tallayColor(campoLinea.InnerText);
                                                                filaX["Nom_Atributo_1"] = TyC[0];
                                                                filaX["Valor_Atributo_1"] = TyC[1];
                                                                filaX["Nom_Atributo_2"] = TyC[2];
                                                                filaX["Valor_Atributo_2"] = TyC[3];
                                                            }
                                                        }
                                                    }
                                                    lineasDePedidoParaA3.Rows.Add(filaX);

                                                }
                                                if (lineaDePedido.Name == "product_attribute_id")
                                                {
                                                    TyC = tallayColor(lineaDePedido.InnerText);
                                                    filaAsignada.Cells["Nom_Atributo_1"].Value = TyC[0];
                                                    filaAsignada.Cells["Valor_Atributo_1"].Value = TyC[1];
                                                    filaAsignada.Cells["Nom_Atributo_2"].Value = TyC[2];
                                                    filaAsignada.Cells["Valor_Atributo_2"].Value = TyC[3];
                                                }

                                                filaAsignada.Cells[lineaDePedido.Name.ToString()].Value = lineaDePedido.InnerText.ToString();
                                                //c += 1;
                                                ////MessageBox.Show("Pedido: " + fila.Cells[0].Value.ToString() + " / Linea  " + numLinea + " / " +  lineaDePedido.Name.ToString() + " - " + lineaDePedido.InnerText.ToString());
                                            }


                                        }
                                    }
                                }
                            }
                        }

                    }

                }
                //dgvBorrame.DataSource = lineasDePedidoParaA3;

            }
            return lineasDePedidoParaA3;
        }


        private void btOrderDetail_Click(object sender, EventArgs e)
        {
            A3TablaLineas();
        }


        private string[] tallayColor(string atributoArticulo)
        {
            int cont = 0;
            string[] arrayAtributos = new string[4];
            csSqlConnects sqlconector = new csSqlConnects();
            DataTable tablaTyC = sqlconector.cargarDatosTabla("KLS_ESYNC_TALLASYCOLORES");
            csMySqlConnect mysqlconector = new csMySqlConnect();
            DataTable tablaAtributos = mysqlconector.cargarTabla("ps_product_attribute_combination");

            string expresionPS = "id_product_attribute='" + atributoArticulo + "'";
            DataRow[] filasAtributos;
            filasAtributos = tablaAtributos.Select(expresionPS);
            for (int i = 0; i < filasAtributos.Length; i++)
            {
                ////MessageBox.Show("Atributo:" + filasAtributos[i][0] + " - Id Product Atribute: " + filasAtributos[i][1]);

                string expression;
                expression = "PS_Atributo='" + filasAtributos[i][0] + "'";
                DataRow[] foundRows;
                foundRows = tablaTyC.Select(expression);
                for (int ii = 0; ii < foundRows.Length; ii++)
                {
                    ////MessageBox.Show(foundRows[ii][0] + " - " + foundRows[ii][1] + " - " + foundRows[ii][2] + " - " + foundRows[ii][3]);
                    arrayAtributos[cont] = foundRows[ii][2].ToString();
                    cont = cont + 1;
                    arrayAtributos[cont] = foundRows[ii][3].ToString();

                }
                cont = cont + 1;
            }

            return arrayAtributos;
        }

        private void btEnviarA3_Click(object sender, EventArgs e)
        {
            DataSet A3Dataset = new DataSet("DocumentosA3");
            DataTable LineasDocs = new DataTable("LineasDocs");
            DataTable CabeceraDocs = new DataTable("CabeceraDocs");

            //MessageBox.Show("Dataset Cargado");
            CabeceraDocs = A3CabeceraDocs();
            LineasDocs = A3TablaLineas();
            CabeceraDocs.TableName = "CabeceraDocs";
            LineasDocs.TableName = "LineasDocs";
            A3Dataset.Tables.Add(CabeceraDocs);
            A3Dataset.Tables.Add(LineasDocs);

            csa3erp a3 = new csa3erp();
            a3.abrirEnlace();
            a3.generaDocA3Dataset(A3Dataset);
            a3.cerrarEnlace();

        }

        private void btExportarArticuloPS_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();
            psWebService.cdPSWebService("POST", "products", ""); //en minúsculas el objeto

        }

        private void button3_Click(object sender, EventArgs e)
        {
            csSqlConnects conectorSQl = new csSqlConnects();
            conectorSQl.cargarIdiomas();
        }

        private void btModificarEstadoDoc_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();
            //psWebService.cdPSWebService("PUT", "orders", "", "", false, ""); //en minúsculas el objeto
            psWebService.cdPSWebService("PUT", "orders", "");

            //MessageBox.Show("Documento Actualizado");

            //DialogResult Resultado;
            //Resultado = //MessageBox.Show("Se han importado " + dgvProductosA3.SelectedRows.Count + " artículos correctamente, " +
            //    "es recomendable reindexar los artículos para que su búsqueda sea eficiente. \n" +
            //    "¿Desea reindexar ahora?\n(si elige Sí, se abrirá momentaneamente un navegador)", "Reindexar Artículos", //MessageBoxButtons.YesNo, //MessageBoxIcon.Question);
            //if (Resultado == DialogResult.Yes)
            //{
            //    reindexar();
            //}
        }

        private void btAddCustomers_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();

            psWebService.cdPSWebService("POST", "customers", "clientesXML");

            //MessageBox.Show("Documento Actualizado");


        }

        private void btLoadXMLClientes_Click(object sender, EventArgs e)
        {
            A3XMLClientes();
        }

        private void btInsertCustomerAddress_Click(object sender, EventArgs e)
        {
            csPSWebService psWebService = new csPSWebService();

            psWebService.cdPSWebService("POST", "addresses", "direccionesClientesXML");

            //MessageBox.Show("Documento Actualizado");
        }


        private void btnContador_Click(object sender, EventArgs e)
        {
            if (dropTablas.SelectedIndex > -1)
            {
                try
                {
                    csMySqlConnect mysql = new csMySqlConnect();
                    MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
                    conn.Open();
                    MySqlDataAdapter MyDA = new MySqlDataAdapter();
                    MyDA.SelectCommand = new MySqlCommand("ALTER TABLE " + dropTablas.SelectedItem + " auto_increment = " + contadorForm.Value, conn);
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void btnLoadDrop_Click(object sender, EventArgs e)
        {
            // rellenar droptablas
            csMySqlConnect conector = new csMySqlConnect();

            try
            {
                MySqlConnection conn = new MySqlConnection(csGlobal.cadenaConexionPS);
                conn.Open();
                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand("SHOW TABLES", conn);

                DataSet t = new DataSet();
                MyDA.Fill(t);

                foreach (DataTable table in t.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        dropTablas.Items.Add(row.ItemArray[0].ToString());
                    }
                }

                conn.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Close();
            }

        }

        private void btnCreateClientA3_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new csa3erp();
            
            try
            {
                a3.abrirEnlace();
                csa3erpTercero t = new csa3erpTercero();
                t.crearClienteA3("PEPITO", "BORRAME");
                //MessageBox.Show(a3.crearClienteA3());
                //MessageBox.Show(a3.crearArticuloA3());
                a3.cerrarEnlace();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnServir_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new csa3erp();
            try
            {
                //a3.servirPedidoPorLineas("2015", "151138");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
            }
        }

        private void btnCheckFTP_Click(object sender, EventArgs e)
        {
            //csFtpTools ftpTool = new csFtpTools();
            //ftpTool.ExisteDirectorio(csGlobal.rutaFTPImagenes, csGlobal.userFTP, csGlobal.passwordFTP);

            FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(csGlobal.rutaFTPImagenes);
            ftpRequest.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
            StreamReader streamReader = new StreamReader(response.GetResponseStream());

            List<string> directories = new List<string>();

            string line = streamReader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {

                directories.Add(line);
                line = streamReader.ReadLine();
            }

            streamReader.Close();
        }

        public void DownloadFileNames()
        {
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(csGlobal.rutaFTPImagenes);
            reqFTP.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);

        }

        private void btSendMail_Click(object sender, EventArgs e)
        {
            MailMessage mail = new MailMessage("lluisvera@repasat.com", "lluisvera@klosions.com");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "mail.repasat.com";
            client.Credentials = new System.Net.NetworkCredential("lluisvera@repasat.com", "Kl0s10ns");
            mail.Subject = "this is a test email.";
            mail.Body = "this is my test email body";
            client.Send(mail);
        }

        private void btnUpdateFamilyProduct_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            Control.CheckForIllegalCrossThreadCalls = false;
            DataTable dt = sql.cargarDatosTablaA3("SELECT dbo.ARTICULO.KLS_ID_SHOP, dbo.FAMILIAS.ID " +
                                                  " FROM dbo.FAMILIAS JOIN " +
                                                  " dbo.ARTICULO ON dbo.FAMILIAS.CODFAM = dbo.ARTICULO.FAMARTDESCVEN " +
                                                  " WHERE (dbo.ARTICULO.KLS_ID_SHOP IS NOT NULL)");
            foreach (DataRow dr in dt.Rows)
            {
                Thread t = new Thread(delegate() { actualizarFamiliasProductos(dr, mysql); });
                t.Start();
            }

            MessageBox.Show("OPERATION COMPLETED");
        }

        private void actualizarFamiliasProductos(DataRow dr, csMySqlConnect mysql)
        {
            mysql.actualizaStock("update ps_product set id_manufacturer = " + dr["ID"].ToString() + " where reference = '" + dr["KLS_ID_SHOP"].ToString() + "'");
        }

        private void btnArticulosBloqueados_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();

            try
            {
                DataTable dt = sql.cargarDatosTablaA3("select codart, bloqueado, kls_articulo_bloquear from articulo");

                foreach (DataRow dr in dt.Rows)
                {
                    sql.actualizarCampo("update articulo set kls_articulo_bloquear = '" + dr["bloqueado"].ToString() + "' where ltrim(codart) = '" + dr["codart"].ToString().Trim() + "'");
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            csXMLConfig xml = new csXMLConfig();

            MessageBox.Show(xml.cargarConfiguracionConexionesBD());
        }

        private void btnCrearDomBancaA3_Click(object sender, EventArgs e)
        {
            csa3erp a3erp = new csa3erp();
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            Objetos.csDomBanca domiciliacion = new Objetos.csDomBanca();

            domiciliacion.iban = "ES21";
            domiciliacion.banco = "0081";
            domiciliacion.agencia = "0023";
            domiciliacion.digitoControl = "11";
            domiciliacion.numcuenta = "1111222234";
            domiciliacion.titular = "KLOSIONS";
            domiciliacion.bic = "BSABESBBXXX";
            domiciliacion.codTercero = "4";


            a3erp.abrirEnlace();

            a3erpTercero.crearDomiciliacion(domiciliacion, true);

            a3erp.cerrarEnlace();

        }

        private void btnFra2Pdf_Click(object sender, EventArgs e)
        {
            csa3erpDocs a3erpdocs = new csa3erpDocs();
            //a3erpdocs.imprimirFactura();
        }

        private void btnSyncTYCNuevo_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string reference = "", codfamtallah = "", codfamtallav = "", codigo_color = "", codigo_talla = "", id = "";

            DataTable dt = sql.cargarDatosTablaA3("SELECT ID, LTRIM(CODART) as reference, CODFAMTALLAH, CODFAMTALLAV, CODIGO_COLOR, CODIGO_TALLA");

            foreach (DataRow dr in dt.Rows)
            {
                reference = dr["reference"].ToString();
                codfamtallah = dr["CODFAMTALLAH"].ToString();
                codfamtallav = dr["CODFAMTALLAV"].ToString();
                codigo_color = dr["CODIGO_COLOR"].ToString();
                codigo_talla = dr["CODIGO_TALLA"].ToString();
                id = dr["ID"].ToString();

                mysql.actualizaStock("UPDATE ps_attribute SET reference = '" + reference +
                    "', codfamtallah = '" + codfamtallah +
                    "', codfamtallav = '" + codfamtallav +
                    "', codigo_color = '" + codigo_color +
                    "', codigo_talla = '" + codigo_talla +
                    "' where id = " + id);
            }
        }

        private void buttonAddAddress_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new csa3erp();
            a3.abrirEnlace();

            csa3erpDirecciones dir = new csa3erpDirecciones();

            a3.cerrarEnlace();
        }

    }
}
