﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Linq;

namespace klsync
{
    public partial class frEnvioDocumentos : Form
    {
        DataTable dt2 = new DataTable();

        private ToolStripControlHost tsdtIni;
        private ToolStripControlHost tsdtFin;
        private ToolStripControlHost tslblIni;
        private ToolStripControlHost tslblFin;
        private ToolStripControlHost tsComboOrigen;
        private ToolStripControlHost tsComboFiltro;

        private string tabla = "";
        private string campos = "";
        private bool email = false;
        ToolStripTextBox txtFiltro = new ToolStripTextBox();
        ComboBox comboOrigen = new ComboBox();
        ComboBox comboBoxFiltro = new ComboBox();
        DateTimePicker dtpDesde = new DateTimePicker();
        DateTimePicker dtpHasta = new DateTimePicker();
        ToolStripButton btnCargarDocumentos = new ToolStripButton();
        ToolStripButton btnGuardarFormatos = new ToolStripButton();
        ToolStripButton btnEnviarDocumento = new ToolStripButton();
        ToolStripButton btnImprimirEnviarDocumento = new ToolStripButton();
        ToolStripButton btnAbrirCarpeta = new ToolStripButton();
        ToolStripTextBox tStripTextBoxFiltro = new ToolStripTextBox();
        private static frEnvioDocumentos m_FormDefInstance;
        public static frEnvioDocumentos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frEnvioDocumentos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        string rutaXML = (Directory.Exists(Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + @"\" + csGlobal.nombreEmpresaA3 + @"\") ? Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + @"\" + csGlobal.nombreEmpresaA3 + @"\" : "");
        
        private int rellenarComboConLosFicheros()
        {
            string[] ficheros = Directory.GetFiles(rutaXML)
                                     .Select(path => Path.GetFileName(path))
                                     .ToArray();

            BindingSource theBindingSource = new BindingSource();
            theBindingSource.DataSource = ficheros;
            comboBoxSelector.DataSource = theBindingSource.DataSource;

            return ficheros.Length;
        }

        public frEnvioDocumentos()
        {
            InitializeComponent();

            // Si el directorio aun no ha sido creado, lo creamos
            if (rutaXML == "")
            {
                // The folder for the roaming current user 
                string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                // Combine the base folder with your specific folder....
                string specificFolder = Path.Combine(folder, csGlobal.nombreEmpresaA3);

                if (!Directory.Exists(specificFolder))
                {
                    Directory.CreateDirectory(specificFolder);
                    rutaXML = specificFolder;
                }
            }

            comboBoxSelector.SelectedIndexChanged += new EventHandler(comboBoxSelector_SelectedIndexChanged);
            // Leer fichero formato predefinido
            try
            {
                // leer nombre ficheros de la carpeta rutaXML
                if (rellenarComboConLosFicheros() == 0)
                {
                    txtFormat.Text = "[year][month][day]-[tipodocumento]-[numdocumento]-[nomcliente]";
                    comprobarBotones();
                }
            }
            catch (Exception)
            {
                
                //tbMensaje.Text = csGlobal.conexionDB + " te envía las facturas del mes.";
            }

            dtpDesde.Width = 120;
            dtpHasta.Width = 120;

            comboBoxFiltro.Items.AddRange(new object[] { "Filtrar por cliente", "Filtrar por email", "Filtrar por serie" });
            comboBoxFiltro.SelectedIndex = 0;
            comboBoxFiltro.Width = 150;

            comboOrigen.Items.Add("Facturas de venta");
            comboOrigen.Items.Add("Facturas de compra");
            comboOrigen.Items.Add("Pedidos de venta");
            comboOrigen.Items.Add("Pedidos de compra");
            comboOrigen.Items.Add("Albaranes de venta");
            comboOrigen.Items.Add("Albaranes de compra");
            comboOrigen.Width = 175;
            comboOrigen.SelectedIndex = 0;

            Label lblDesde = new Label();
            lblDesde.Text = "Desde:";
            lblDesde.TextAlign = ContentAlignment.MiddleCenter;
            lblDesde.BackColor = Color.Transparent;
            lblDesde.Padding = new Padding(10);

            dtpDesde.Format = DateTimePickerFormat.Short;
            dtpHasta.Format = DateTimePickerFormat.Short;

            Label lblHasta = new Label();
            lblHasta.Text = "Hasta:";
            lblHasta.TextAlign = ContentAlignment.MiddleCenter;
            lblHasta.BackColor = Color.Transparent;
            lblHasta.Padding = new Padding(20);

            btnCargarDocumentos.Image = klsync.Properties.Resources.magnifying_glass;
            btnCargarDocumentos.ImageScaling = ToolStripItemImageScaling.None;
            btnCargarDocumentos.ToolTipText = "Cargar documentos";
            btnCargarDocumentos.TextImageRelation = TextImageRelation.ImageAboveText;
            btnCargarDocumentos.Padding = new Padding(20);
            btnCargarDocumentos.Click += new EventHandler(cargarDocumentos);
            btnCargarDocumentos.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnCargarDocumentos.Text = "Buscar";

            btnEnviarDocumento.Image = klsync.Properties.Resources.symbol_pdf;
            btnEnviarDocumento.ImageScaling = ToolStripItemImageScaling.None;
            btnEnviarDocumento.ToolTipText = "Generar los PDF de los documentos seleccionados";
            btnEnviarDocumento.TextImageRelation = TextImageRelation.ImageAboveText;
            btnEnviarDocumento.Padding = new Padding(20);
            btnEnviarDocumento.Click += new EventHandler(enviarDocumentos);
            btnEnviarDocumento.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnEnviarDocumento.Text = "Generar PDF";

            btnImprimirEnviarDocumento.Image = klsync.Properties.Resources.email_outbox;
            btnImprimirEnviarDocumento.ImageScaling = ToolStripItemImageScaling.None;
            btnImprimirEnviarDocumento.ToolTipText = "Generar los PDF y enviar los documentos por correo electrónico";
            btnImprimirEnviarDocumento.TextImageRelation = TextImageRelation.ImageAboveText;
            btnImprimirEnviarDocumento.Padding = new Padding(20);
            btnImprimirEnviarDocumento.Click += new EventHandler(imprimirEnviarDocumentos);
            btnImprimirEnviarDocumento.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnImprimirEnviarDocumento.Text = "Generar y enviar";

            btnAbrirCarpeta.Image = klsync.Properties.Resources.folder;
            btnAbrirCarpeta.ImageScaling = ToolStripItemImageScaling.None;
            btnAbrirCarpeta.ToolTipText = "Abrir la carpeta donde se guardan los documentos generados";
            btnAbrirCarpeta.TextImageRelation = TextImageRelation.ImageAboveText;
            btnAbrirCarpeta.Padding = new Padding(20);
            btnAbrirCarpeta.Click += new EventHandler(abrirCarpeta);
            btnAbrirCarpeta.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnAbrirCarpeta.Text = "Abrir carpeta";

            btnGuardarFormatos.Image = klsync.Properties.Resources.save31;
            btnGuardarFormatos.ImageScaling = ToolStripItemImageScaling.None;
            btnGuardarFormatos.ToolTipText = "Guardar plantilla, formato, ruta de la carpeta y ajustes del mensaje";
            btnGuardarFormatos.TextImageRelation = TextImageRelation.ImageAboveText;
            btnGuardarFormatos.Padding = new Padding(20);
            btnGuardarFormatos.Click += new EventHandler(guardarFormatos);
            btnGuardarFormatos.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnGuardarFormatos.Text = "Guardar configuración";

            comboOrigen.SelectedIndexChanged += new EventHandler(comboOrigen_SelectedIndexChanged);
            tsComboFiltro = new ToolStripControlHost(comboBoxFiltro);

            txtFiltro.Margin = new Padding(10, 0, 10, 0);
            txtFiltro.BorderStyle = BorderStyle.FixedSingle;
            txtFiltro.Font = new Font(txtFiltro.Font.FontFamily, 12);
            tStripTextBoxFiltro = txtFiltro;
            tsComboOrigen = new ToolStripControlHost(comboOrigen);
            tslblIni = new ToolStripControlHost(lblDesde);
            tsdtIni = new ToolStripControlHost(dtpDesde);
            tslblFin = new ToolStripControlHost(lblHasta);
            tsdtFin = new ToolStripControlHost(dtpHasta);

            tsEnvioDocumentos.Items.Add(tsComboFiltro);
            tsEnvioDocumentos.Items.Add(tStripTextBoxFiltro);
            tsEnvioDocumentos.Items.Add(tsComboOrigen);
            tsEnvioDocumentos.Items.Add(tslblIni);
            tsEnvioDocumentos.Items.Add(tsdtIni);
            tsEnvioDocumentos.Items.Add(tslblFin);
            tsEnvioDocumentos.Items.Add(tsdtFin);
            tsEnvioDocumentos.Items.Add(btnCargarDocumentos);
            tsEnvioDocumentos.Items.Add(btnEnviarDocumento);
            tsEnvioDocumentos.Items.Add(btnImprimirEnviarDocumento);
            tsEnvioDocumentos.Items.Add(new ToolStripSeparator());
            tsEnvioDocumentos.Items.Add(btnAbrirCarpeta);
            tsEnvioDocumentos.Items.Add(btnGuardarFormatos);

            comboOrigen.Margin = new Padding(20, 0, 0, 0);
            //tsComboFiltro.Margin = new Padding(0, 0, 20, 0);

            DataTable formatos = cargarFormatos();
            if (formatos.Rows.Count > 0)
                cargarCombo(formatos);

            comprobarComboBoxSelector();
        }

        private void comboOrigen_SelectedIndexChanged(object sender, EventArgs e)
        {
            string nombre = ((ComboBox)sender).SelectedItem.ToString();
            string destino = "";

            //switch (nombre)
            //{
            //    case "Albaranes de venta":
            //        destino = "ALBV";
            //        break;
            //    case "Albaranes de compra":
            //        break;
            //    case "Facturas de venta":
            //        destino = "FACV";
            //        break;
            //    case "Facturas de compra":
            //        break;
            //    case "Pedidos de venta":
            //        break;
            //    case "Pedidos de compra":
            //        destino = "PEDC";
            //        break;
            //    default:
            //        break;
            //}

            DataTable formatos = cargarFormatos();
            if (formatos.Rows.Count > 0)
                cargarCombo(formatos);
        }

        private DataTable cargarFormatos()
        {
            DataTable formatos = new DataTable();

            try
            {
                string tipoDoc = "";
                string modelo = "";

                //File.ReadAllLines(@"C:\Program Files (x86)\A3\A3Erp\Sistema.ini", Encoding.UTF7)[2].Substring(10);
                string path = File.ReadAllLines(@"C:\Program Files (x86)\A3\A3Erp\Sistema.ini")[2].Substring(10);
                path += @"Sistema\Listados.ini";
                path = @"\" + path;
                formatos.Columns.Add("tipoDoc");
                formatos.Columns.Add("modeloDoc");

                string[] listado = File.ReadAllLines(path, Encoding.GetEncoding("Windows-1252"));

                foreach (string list in listado)
                {
                    if (list.Contains("[") && !list.Contains("}"))
                    {
                        tipoDoc = list;
                    }
                    else if (list.Contains("{") || list.Contains("}")) { }
                    else if (list.Split('=')[1] != "")
                    {
                        modelo = list;
                        DataRow fila = formatos.NewRow();
                        fila["tipoDoc"] = modelo.Split('=')[0];
                        fila["modeloDoc"] = modelo.Split('=')[1];

                        formatos.Rows.Add(fila);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {

            }
            catch (Exception ex)
            {

            }

            return formatos;
        }

        private void cargarCombo(DataTable formatos)
        {
            DataTable filtered = formatos.AsEnumerable()
                    .Where(r => r.Field<String>("tipoDoc").Contains("")).CopyToDataTable();

            comboModelo.ValueMember = filtered.Columns[0].ToString();
            comboModelo.DisplayMember = filtered.Columns[1].ToString();
            comboModelo.DataSource = filtered;

            if (dt2.Rows.Count > 0)
            {
                if (File.Exists(dt2.Rows[0]["path"].ToString()))
                {
                    comboModelo.SelectedValue = dt2.Rows[0]["model"].ToString();
                    if (comboModelo.SelectedValue == null)
                    {
                        comboModelo.SelectedIndex = 0;
                    }
                }
            }
        }

        private void changeColor(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["IMPRESO"].Value.ToString() == "T")
                {
                    row.DefaultCellStyle.ForeColor = Color.Green;
                }

                if (row.Cells["ENVIADO"].Value.ToString() == "T")
                {
                    row.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
        }

        private void cargarDocs()
        {
            try
            {
                string texto = tStripTextBoxFiltro.Text.Trim();
                string where = "";
                if (comboBoxFiltro.SelectedItem.ToString() == "Filtrar por cliente" && texto != "")
                    where = " AND LTRIM(CLIENTES.CODCLI) = '" + texto + "' ";
                else if (comboBoxFiltro.SelectedItem.ToString() == "Filtrar por email" && texto != "")
                    where = " AND CLIENTES.E_MAIL_DOCS = '" + texto + "' ";
                else if (comboBoxFiltro.SelectedItem.ToString() == "Filtrar por serie" && texto != "")
                    where = " AND LTRIM(CABEFACV.SERIE) = '" + texto + "' ";
                else
                    where = "";
                Stopwatch watch = Stopwatch.StartNew();

                string combo = comboOrigen.SelectedItem.ToString();

                DateTime FechaDesde = dtpDesde.Value;
                DateTime FechaHasta = dtpHasta.Value;
                string strFechaDesde = FechaDesde.ToString("yyyy-MM-dd 00:00:00");
                string strFechaHasta = FechaHasta.ToString("yyyy-MM-dd 23:59:59");

                switch (combo)
                {
                    case "Facturas de venta":
                        tabla = "CABEFACV";
                        campos = tabla + ".FECHA, LTRIM(" + tabla + ".SERIE) AS SERIE, " + tabla + ".NUMDOC,LTRIM("
                            + tabla + ".CODCLI) AS CODIGO, " + tabla + ".NOMCLI, " + tabla + ".NIFCLI,  " + tabla + ".BASE, "
                            + tabla + ".TOTIVA, " + tabla + ".TOTDOC, " + tabla + ".DOCPAG,LTRIM(" + tabla + ".CODREP) AS AGENTE,"
                            + tabla + ".IMPRESO, " + tabla + ".ENVIADO, " + tabla + ".IDFACV,E_MAIL_DOCS AS EMAIL, " + tabla + ".CODCLI";

                        break;
                    case "Facturas de compra":
                        tabla = "CABEFACC";
                        break;
                    case "Pedidos de venta":
                        tabla = "CABEPEDV";
                        break;
                    case "Pedidos de compra":
                        tabla = "CABEPEDC";
                        break;
                    case "Albaranes de venta":
                        tabla = "CABEALBV";
                        break;
                    case "Albaranes de compra":
                        tabla = "CABEALBC";
                        break;
                }

                string emailDocumentos = (email) ? " AND CLIENTES.E_MAIL_DOCS <> '' " : "";

                csSqlConnects sql = new csSqlConnects();
                DataTable dt = sql.cargarDatosTablaA3("SELECT " + campos + " FROM " + tabla + " JOIN CLIENTES ON CLIENTES.CODCLI = "
                    + tabla + ".CODCLI WHERE " + tabla + ".FECHA  >= '" + Convert.ToDateTime(strFechaDesde) + "' AND " + tabla + ".FECHA  <= '"
                    + Convert.ToDateTime(strFechaHasta) + "'" + emailDocumentos + "" + where + " ORDER BY " + tabla + ".CODCLI");

                csUtilidades.addDataSource(dgvEnvioDocumentos, dt);
                //dgvEnvioDocumentos.DataSource = dt;

                changeColor(dgvEnvioDocumentos);

                //FORMATEO COLUMNAS
                dgvEnvioDocumentos.Columns["NUMDOC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvEnvioDocumentos.Columns["NUMDOC"].DefaultCellStyle.Format = "N0";
                dgvEnvioDocumentos.Columns["BASE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvEnvioDocumentos.Columns["BASE"].DefaultCellStyle.Format = "N2";
                dgvEnvioDocumentos.Columns["TOTIVA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvEnvioDocumentos.Columns["TOTIVA"].DefaultCellStyle.Format = "N2";
                dgvEnvioDocumentos.Columns["TOTDOC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvEnvioDocumentos.Columns["TOTDOC"].DefaultCellStyle.Format = "N2";
                dgvEnvioDocumentos.Columns["IDFACV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvEnvioDocumentos.Columns["IDFACV"].DefaultCellStyle.Format = "N0";

                csUtilidades.contarFilasGrid(dt, watch, numFilas);
            }
            catch (Exception ex)
            {
                //Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void cargarDocumentos(object sender, EventArgs e)
        {
            cargarDocs();
        }

        private void enviarDocumentos(object sender, EventArgs e)
        {
            if (dgvEnvioDocumentos.SelectedRows.Count > 0)
            {
                enviarDocumentos();
                cargarDocs();
                MessageBox.Show("Facturas generadas a PDF con éxito");
                csUtilidades.openFolder(this.dt2.Rows[0]["path"].ToString());
            }
            else
            {
                MessageBox.Show("Selecciona al menos una factura");
            }
        }

        private DataTable enviarDocumentos()
        {
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("codcli", typeof(string));
            dt2.Columns.Add("ruta", typeof(string));
            dt2.Columns.Add("email", typeof(string));

            try
            {
                DataTable dt = ((DataTable)dgvEnvioDocumentos.DataSource).Clone();
                foreach (DataGridViewRow row in dgvEnvioDocumentos.SelectedRows)
                {
                    dt.ImportRow(((DataTable)dgvEnvioDocumentos.DataSource).Rows[row.Index]);
                }
                dt.AcceptChanges();

                csa3erpDocs doc = new csa3erpDocs();

                
                string directorio = "";
                try
                {
                    directorio = this.dt2.Rows[0]["path"].ToString();
                }
                catch (Exception)
                {
                }

                string year = DateTime.Now.Year.ToString();
                string month = DateTime.Now.Month.ToString();
                string day = DateTime.Now.Day.ToString();
                string nombreFichero = "";
                string numeroDoc = "";
                string idDoc = "";
                string ruta = "";

                foreach (DataRow dr in dt.Rows)
                {

                    //if (dr["EMAIL"].ToString() != "")
                    //{
                    numeroDoc = (Convert.ToInt32(dr["NUMDOC"])).ToString();
                    idDoc = (Convert.ToInt32(dr["IDFACV"])).ToString();
                    nombreFichero = setFormatFile(numeroDoc, dr["NOMCLI"].ToString(), dr["CODCLI"].ToString(), dr["AGENTE"].ToString(), dr["DOCPAG"].ToString(), dr["FECHA"].ToString());

                    ruta = doc.imprimirFactura(nombreFichero, idDoc, directorio, comboModelo.SelectedValue.ToString());
                    
                    nombreFichero = "";

                    DataRow dtRow = dt2.NewRow();
                    dtRow[0] = dr["CODCLI"].ToString().Trim(); ;
                    dtRow[1] = ruta;
                    dtRow[2] = dr["EMAIL"].ToString();
                    dt2.Rows.Add(dtRow);

                    csUtilidades.ejecutarConsulta("UPDATE CABEFACV SET IMPRESO = 'T' WHERE IDFACV = '" + idDoc + "'", false);
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dt2;
        }

        private string setFormatFile(string numDoc, string nomCli, string codCli, string agente, string docPago, string fecha)
        {
            string tipoDocumento = "";
            switch (comboOrigen.SelectedItem.ToString())
            {
                case "Facturas de venta":
                    tipoDocumento = "FRAV";
                    break;
                case "Facturas de compra":
                    tipoDocumento = "FRAC";
                    break;
                case "Pedidos de venta":
                    tipoDocumento = "PEDV";
                    break;
                case "Pedidos de compra":
                    tipoDocumento = "PEDC";
                    break;
                case "Albaranes de venta":
                    tipoDocumento = "ALBV";
                    break;
                case "Albaranes de compra":
                    tipoDocumento = "ALBC";
                    break;
            }

            string year = Convert.ToDateTime(fecha).Year.ToString();
            string month = Convert.ToDateTime(fecha).Month.ToString();
            string day = Convert.ToDateTime(fecha).Day.ToString();
            if (Convert.ToInt32(month) < 10)
                month = '0' + month;
            if (Convert.ToInt32(day) < 10)
                day = '0' + day;
            string format = "";
            string txt = txtFormat.Text.Trim();

            if (txt != "")
            {
                txt = txt.Replace("[year]", year);
                txt = txt.Replace("[month]", month);
                txt = txt.Replace("[day]", day);
                txt = txt.Replace("[codcliente]", codCli.Trim());
                txt = txt.Replace("[nomcliente]", nomCli);
                txt = txt.Replace("[numdocumento]", numDoc);
                txt = txt.Replace("[agente]", nomCli);
                txt = txt.Replace("[docpago]", numDoc);
                txt = txt.Replace("[tipodocumento]", tipoDocumento);

                format = txt;
            }
            else
            {
                format = year + month + day + "-" + numDoc + "-" + nomCli;
            }

            return format;
        }

        private void imprimirEnviarDocumentos(object sender, EventArgs e)
        {
            if (tbAsunto.Text.Trim() != "" && tbMensaje.Text.Trim() != "" && txtFormat.Text.Trim() != "" && comboModelo.SelectedValue != null)
            {
                if (dgvEnvioDocumentos.SelectedRows.Count > 0)
                {
                    try
                    {
                        csMail mail = new csMail();
                        string codcli = "";
                        IEnumerable<DataRow> results = null;
                        DataTable copia = new DataTable();
                        DataTable dt = enviarDocumentos();
                        copia = dt.Clone();

                        foreach (DataGridViewRow item in dgvEnvioDocumentos.SelectedRows)
                        {
                            csUtilidades.ejecutarConsulta("UPDATE CABEFACV SET ENVIADO = 'T' WHERE NUMDOC = '" + Convert.ToInt32(item.Cells["NUMDOC"].Value).ToString() + "'", false);
                        }

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["CODCLI"].ToString() != codcli)
                            {
                                results = from myRow in dt.AsEnumerable()
                                          where myRow.Field<string>("CODCLI") == dr["CODCLI"].ToString()
                                          select myRow;

                                copia.Rows.Clear();
                                results.CopyToDataTable(copia, LoadOption.OverwriteChanges);

                                mail.sendMail(tbAsunto.Text, tbMensaje.Text, "", "", csGlobal.EmailEnvio, copia, true);
                                codcli = dr["CODCLI"].ToString();
                            }
                        }

                        cargarDocs();

                        MessageBox.Show("Facturas generadas y enviadas con éxito");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("(ERROR 500) Ha ocurrido un error inesperado.");
                    }
                }
                else
                {
                    MessageBox.Show("Debes seleccionar una o más lineas");
                }
            }
            else
            {
                MessageBox.Show("Debes rellenar el asunto, el mensaje, el formato y la plantilla");
            }
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    cargarDocumentos(null, EventArgs.Empty);
                }
            }
            catch (Exception)
            {
            }
        }

        private void guardarFormatos(object sender, EventArgs e)
        {
            if (comboBoxSelector.Text.Trim() != "" && txtFormat.Text.Trim() != "" && comboModelo.SelectedValue != null && tbAsunto.Text != "" && tbMensaje.Text != "")
            {
                string carpeta = "";
                string ruta = "";
                if (MessageBox.Show("¿Quieres guardar los ajustes? \n\nSon: carpeta de las facturas, tipo de plantilla, formato, asunto y mensaje", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.SelectedPath = ((dt2.Rows.Count > 0) ? dt2.Rows[0]["path"].ToString() : "");
                    if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        carpeta = fbd.SelectedPath;
                    }
                    else
                    {
                        frDialogInput pathruta = new frDialogInput("Ruta donde se guardarán los PDF: ", ((dt2.Rows.Count > 0) ? dt2.Rows[0]["path"].ToString() : ""));
                        if (pathruta.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            carpeta = csGlobal.comboFormValue;
                        }
                    }

                    if (carpeta != "")
                        guardarFormatoEnTXT(txtFormat.Text.Trim(), rutaXML, carpeta);
                    else
                        MessageBox.Show("Operación cancelada");
                }
                else
                {
                    MessageBox.Show("Operación cancelada");
                }
            }
            else
            {
                MessageBox.Show("Tanto el asunto, el mensaje, el formato y la plantilla deben estar informados.");
            }
        }

        private void guardarFormatoEnTXT(string formato, string ruta, string carpeta)
        {
            try
            {
                string ficheroFinal = rutaXML +@"\"+ comboBoxSelector.Text;
                if (!ficheroFinal.Contains(".xml")) ficheroFinal += ".xml";
                //if (File.Exists(ficheroFinal)) File.Delete(ficheroFinal);

                new XDocument(
                    new XElement(comboBoxSelector.Text.Replace(" ", "_").Replace(".xml", "").Replace("(", "").Replace(")","").Trim(), 
                        new XElement("path", carpeta), 
                        new XElement("model", comboModelo.Text), 
                        new XElement("subject", tbAsunto.Text), 
                        new XElement("msgtext", tbMensaje.Text),
                        new XElement("format", formato))
                )
                .Save(ficheroFinal);

                rellenarComboConLosFicheros();
                comprobarComboBoxSelector();
                
                MessageBox.Show("Se ha guardado todo correctamente:\n\n > El directorio se ha establecido en: " + carpeta
                    + "\n > Formato del fichero: " + formato
                    + "\n > Plantilla: " + comboModelo.Text
                    + "\n\n -- CORREO --"
                    + "\n > Asunto: " + tbAsunto.Text.Trim()
                    + "\n > Mensaje: " + tbMensaje.Text.Trim());
            }
            catch (Exception ex)
            {
                MessageBox.Show("El fichero no se ha podido guardar.\n\nError: " + ex.ToString());
            }
        }

        private void comboModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboModelo.Items.Count > 0)
            {

            }
        }

        private void abrirCarpeta(object sender, EventArgs e)
        {
            try
            {
                csUtilidades.openFolder(dt2.Rows[0]["path"].ToString());
            }
            catch (Exception)
            {
            }
        }

        private void cancelarIMPRESOSeleccionadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelarEnviadoImpresoDocs(true);
        }

        private void cancelarENVIADOSeleccionadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cancelarEnviadoImpresoDocs(false);
        }

        private void cancelarEnviadoImpresoDocs(bool impreso = true)
        {
            if (dgvEnvioDocumentos.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgvEnvioDocumentos.SelectedRows)
                {
                    csUtilidades.ejecutarConsulta("UPDATE CABEFACV SET " + ((impreso) ? " IMPRESO " : " ENVIADO ") + " = 'F' WHERE NUMDOC = '" + Convert.ToInt32(item.Cells["NUMDOC"].Value).ToString() + "'", false);
                }

                cargarDocs();
            }
        }

        private void documentosConEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            email = !email;
            documentosConEmailToolStripMenuItem.Checked = email;
        }

        private void comboBoxSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            comprobarComboBoxSelector();
        }

        private void comprobarComboBoxSelector()
        {
            try
            {
                XDocument xDocument = null;
                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings { CheckCharacters = false };
                using (XmlReader xmlReader = XmlReader.Create(rutaXML +@"\"+ comboBoxSelector.Text, xmlReaderSettings))
                {
                    string combo = comboBoxSelector.Text.Trim().Replace(" ", "");
                    xDocument = XDocument.Load(rutaXML + @"\" + comboBoxSelector.Text);
                    DataTable dt = csUtilidades.XmlDocument2DataTable(csUtilidades.ToXmlDocument(xDocument));

                    foreach (DataRow dr in dt.Rows)
                    {
                        tbAsunto.Text = dr["subject"].ToString();
                        tbMensaje.Text = dr["msgtext"].ToString();
                        comboModelo.Text = dr["model"].ToString();
                        txtFormat.Text = dr["format"].ToString();
                    }

                    this.dt2.Rows.Clear();
                    this.dt2.Columns.Clear();

                    this.dt2 = dt.Clone();

                    foreach (DataRow dr in dt.Rows)
                    {
                        this.dt2.ImportRow(dr);
                    }
                    
                    comprobarBotones();
                }
            }
            catch (Exception) { }
        }

        private void comprobarBotones()
        {
            if (this.dt2.Rows.Count > 0)
            {
                btnAbrirCarpeta.Enabled = true;
                btnCargarDocumentos.Enabled = true;
                btnEnviarDocumento.Enabled = true;
                btnImprimirEnviarDocumento.Enabled = true;
            }
            else
            {
                btnAbrirCarpeta.Enabled = false;
                btnCargarDocumentos.Enabled = false;
                btnEnviarDocumento.Enabled = false;
                btnImprimirEnviarDocumento.Enabled = false;
            }
        }

        private void aCCIONESToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void frEnvioDocumentos_Load(object sender, EventArgs e)
        {

        }
    }
}
