﻿namespace klsync
{
    partial class frCaracteristicas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cboxCodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btLoadAdvance = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cBoxCaracteristicas = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvProductosA3 = new System.Windows.Forms.DataGridView();
            this.buttonExcel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosA3)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.buttonExcel);
            this.splitContainer1.Panel1.Controls.Add(this.cboxCodigo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.btLoadAdvance);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.cBoxCaracteristicas);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvProductosA3);
            this.splitContainer1.Size = new System.Drawing.Size(1540, 626);
            this.splitContainer1.SplitterDistance = 168;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // cboxCodigo
            // 
            this.cboxCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxCodigo.Location = new System.Drawing.Point(27, 116);
            this.cboxCodigo.Name = "cboxCodigo";
            this.cboxCodigo.Size = new System.Drawing.Size(120, 20);
            this.cboxCodigo.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "CARACTERÍSTICA";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btLoadAdvance
            // 
            this.btLoadAdvance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btLoadAdvance.Location = new System.Drawing.Point(71, 208);
            this.btLoadAdvance.Name = "btLoadAdvance";
            this.btLoadAdvance.Size = new System.Drawing.Size(75, 33);
            this.btLoadAdvance.TabIndex = 16;
            this.btLoadAdvance.Text = "&Buscar";
            this.btLoadAdvance.UseVisualStyleBackColor = true;
            this.btLoadAdvance.Click += new System.EventHandler(this.btLoadAdvance_Click_1);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(88, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "FILTROS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cBoxCaracteristicas
            // 
            this.cBoxCaracteristicas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cBoxCaracteristicas.Location = new System.Drawing.Point(26, 174);
            this.cBoxCaracteristicas.Name = "cBoxCaracteristicas";
            this.cBoxCaracteristicas.Size = new System.Drawing.Size(120, 20);
            this.cBoxCaracteristicas.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "REF-NOM";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dgvProductosA3
            // 
            this.dgvProductosA3.AllowUserToAddRows = false;
            this.dgvProductosA3.AllowUserToDeleteRows = false;
            this.dgvProductosA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductosA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProductosA3.Location = new System.Drawing.Point(0, 0);
            this.dgvProductosA3.Name = "dgvProductosA3";
            this.dgvProductosA3.ReadOnly = true;
            this.dgvProductosA3.RowHeadersWidth = 20;
            this.dgvProductosA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductosA3.Size = new System.Drawing.Size(1369, 626);
            this.dgvProductosA3.TabIndex = 2;
            // 
            // buttonExcel
            // 
            this.buttonExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExcel.Image = global::klsync.Properties.Resources.excel;
            this.buttonExcel.Location = new System.Drawing.Point(27, 262);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(111, 44);
            this.buttonExcel.TabIndex = 24;
            this.buttonExcel.Text = "Excel";
            this.buttonExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonExcel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttonExcel_Click);
            // 
            // frCaracteristicas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1540, 626);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frCaracteristicas";
            this.Text = "frCaracteristicas";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosA3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvProductosA3;
        private System.Windows.Forms.TextBox cboxCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btLoadAdvance;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cBoxCaracteristicas;
        private System.Windows.Forms.Label label4;
    }
}