﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace klsync
{
    public partial class frSincronizarMarcas : Form
    {
        private static frSincronizarMarcas m_FormDefInstance;
        public static frSincronizarMarcas DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSincronizarMarcas();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frSincronizarMarcas()
        {
            InitializeComponent();
        }

        private void toolStripButtonA3_Click(object sender, EventArgs e)
        {
            cargarA3();
        }
        
        private void toolStripButtonPS_Click(object sender, EventArgs e)
        {
            cargarPS();
        }

        private void toolStripButtonActA3_Click(object sender, EventArgs e)
        {
            if (dgvA3.Rows.Count > 0)
            {
                Thread t = new Thread(actualizarMarcasA3);
                t.Start();
            }
        }

        private void toolStripButtonActPS_Click(object sender, EventArgs e)
        {
            if (dgvPS.Rows.Count > 0)
            {
                Thread t = new Thread(actualizarMarcasPS);
                t.Start();
            }
        }

        private void cargarA3()
        {
            //csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_CODMARCA = (SELECT ID FROM FAMILIAS WHERE ARTICULO.FAMARTDESCVEN = FAMILIAS.CODFAM)", false);
            csUtilidades.cargarDGV(dgvA3, "SELECT LTRIM(CODART) AS CODART, KLS_ID_SHOP, FAMILIAS.DESCFAM, KLS_CODMARCA FROM ARTICULO LEFT JOIN FAMILIAS ON FAMILIAS.CODFAM = ARTICULO.FAMARTDESCVEN WHERE CODART <> '0' AND KLS_ID_SHOP <> '' AND DESCFAM <> '' ORDER BY KLS_CODMARCA asc", false);        
        }

        private void cargarPS()
        {
            csUtilidades.cargarDGV(dgvPS, "select id_product, id_manufacturer from ps_product where reference <> ''", true);
        }

        private void actualizarMarcasA3()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            DataTable dtA3 = (DataTable)dgvA3.DataSource;
            DataTable dtPS = (DataTable)dgvPS.DataSource;
            int i = 0;
            int contador = 0;

            toolStripProgressBarActA3.Maximum = dtA3.Rows.Count;
            dtA3.Columns.Add("PS_CODMARCA");
            foreach (DataRow dr in dtA3.Rows)
            {
                dtA3.Rows[i]["PS_CODMARCA"]= "";
                
                toolStripProgressBarActA3.Value = i;
                i++;
            }

            toolStripProgressBarActA3.Maximum = dtA3.Rows.Count + dtPS.Rows.Count;
            foreach (DataRow drps in dtPS.Rows)
            {
                foreach (DataRow dra3 in dtA3.Rows)
                {
                    if (dra3["KLS_ID_SHOP"].ToString() == drps["id_product"].ToString())
                    {
                        dra3["PS_CODMARCA"] = drps["id_manufacturer"].ToString();
                        
                        toolStripProgressBarActA3.Value = contador;
                        contador++;

                        break;
                    }
                }
            }
            
            contador = 0;
            toolStripProgressBarActA3.Maximum = dtA3.Rows.Count;
            foreach (DataRow dra3 in dtA3.Rows)
            {
                string pscodmarca = dra3["PS_CODMARCA"].ToString(), klscodmarca = dra3["KLS_CODMARCA"].ToString(), klsidshop = dra3["KLS_ID_SHOP"].ToString();
                
                if ((pscodmarca != klscodmarca) && pscodmarca != "" && klscodmarca != "")
                {
                    csUtilidades.ejecutarConsulta("update ps_product set id_manufacturer = " + klscodmarca + " where id_product = " + klsidshop, true);

                    toolStripProgressBarActA3.Value = contador;
                    contador++;
                }
            }
            
            MessageBox.Show("Marcas de PS actualizadas");
            toolStripProgressBarActA3.Value = 0;
        }

        private void actualizarMarcasPS()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            int contador = 0;

            DataTable dt = sql.cargarDatosTablaA3("SELECT DISTINCT DESCFAM FROM ARTICULO LEFT JOIN FAMILIAS ON FAMILIAS.CODFAM = ARTICULO.FAMARTDESCVEN WHERE KLS_CODMARCA is NULL AND DESCFAM <> ''");

            if (dt.Rows.Count > 0)
            {
                toolStripProgressActPS.Maximum = dt.Rows.Count;

                foreach (DataRow dr in dt.Rows)
                {
                    mysql.insertItems("insert into ps_manufacturer (name) values('" + dr["DESCFAM"].ToString() + "') ");

                    toolStripProgressActPS.Value = contador;
                    contador++;
                }

                MessageBox.Show("Marcas de PS actualizadas");
            }
            else
            {
                MessageBox.Show("No hay nada que actualizar");
            }
        }

        private void toolStripButtonInsertarMarcasPS_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            // Seleccionamos todas las marcas de A3 que no tienen código de PS
            DataTable dtA3 = sql.cargarDatosTablaA3("SELECT DISTINCT DESCFAM FROM ARTICULO LEFT JOIN FAMILIAS ON FAMILIAS.CODFAM = ARTICULO.FAMARTDESCVEN WHERE KLS_CODMARCA is NULL");
            DataTable dtPS = csUtilidades.check2DataTablesMYSQL(dtA3, "select name from ps_manufacturer");
            int contador = 0;
           
            if (dtPS.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPS.Rows)
                {
                    mysql.insertItems("insert into ps_manufacturer (name) values('" + dr["DESCFAM"].ToString() + "') ");

                    toolStripProgressActPS.Value = contador;
                    contador++;
                }
                
                MessageBox.Show("Marcas de PS actualizadas");
            }
            else
            {
                MessageBox.Show("No hay nada que actualizar");
            }
        }
    }
}
