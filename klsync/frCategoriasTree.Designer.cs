﻿namespace klsync
{
    partial class frCategoriasTree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeVCategorias = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeVCategorias
            // 
            this.treeVCategorias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeVCategorias.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeVCategorias.Location = new System.Drawing.Point(0, 0);
            this.treeVCategorias.Name = "treeVCategorias";
            this.treeVCategorias.Size = new System.Drawing.Size(620, 425);
            this.treeVCategorias.TabIndex = 0;
            // 
            // frCategoriasTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 425);
            this.Controls.Add(this.treeVCategorias);
            this.Name = "frCategoriasTree";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estructura Arbol Categorías";
            this.Load += new System.EventHandler(this.frCategoriasTree_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeVCategorias;
    }
}