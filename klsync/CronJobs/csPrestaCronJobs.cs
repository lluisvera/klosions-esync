﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.CronJobs
{
    class csPrestaCronJobs
    {
        Utilidades.csLogErrores errorLog = new Utilidades.csLogErrores();
        public void documentos()
        {
            try
            {
                frSincronizarDocs docs = new frSincronizarDocs();

                DataTable cab = docs.A3CabeceraDocs(false);
                DataTable lines = docs.A3TablaLineas(false);

                docs.sincronicarDocsA3(false, cab, lines);
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        public void stock()
        {
            try
            {
                csStocks stock = new csStocks();
                stock.stock();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        public void preciostarifasclientes()
        {
            frPrecios precios = new frPrecios();
            precios.tarifasclientes();
        }

        public void stockTyC() // esportissim
        {
            try
            {
                frArticulos art = new frArticulos();
                art.actualizarTallasYColores();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        public void TyC_dismay()
        {
            Dismay.csDismay dismay = new Dismay.csDismay();
            dismay.procedimientoDismay();
        }

        public void familias_cliente_articulo()
        {
            csPrecios prices = new csPrecios();
            prices.actualiarFamiliasArticulosEnPS();
            prices.actualizarFamiliasClientesPS();
        }

        /// <summary>
        /// HEBO
        /// </summary>
        public void precios_descuentos()
        {
            csPrecios precio = new csPrecios();
            precio.descuentos();
        }

        public void act_categorias()
        {
            frTesting test = new frTesting();
            test.actualizarCategorias();
        }

        public void replog_tallas()
        {
            csRepLog rep = new csRepLog();
            rep.cambiosTabla("TALLAS");
        }

        public void replog_articulos()
        {
            csRepLog rep = new csRepLog();
            rep.cambiosTabla("ARTICULO");
        }



    }
}
