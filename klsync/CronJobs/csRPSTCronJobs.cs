﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Xml.Linq;
using System.Windows.Forms;
using klsync.Utilidades;
using klsync.Repasat;

namespace klsync.CronJobs
{
    public class csRPSTCronJobs
    {
        csLogErrores errorLog = new csLogErrores();
        csUpdateData updateDataRPST = new csUpdateData();

        public void documentosRPST()
        {
            try
            {
                Repasat.SyncRepasat.csSyncRepasat syncRepasat = new Repasat.SyncRepasat.csSyncRepasat();
                syncRepasat.syncroDocs();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        public void subirCliente()
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest("customers?schema=synopsis&ws_key=" + csGlobal.webServiceKey, Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                orderEl.Element("passwd").Value = "holitas".Hash(); // !
                orderEl.Element("lastname").Value = "Prueba"; // !
                orderEl.Element("firstname").Value = "Mister"; // !
                orderEl.Element("email").Value = "mister@prueba.com"; // !
                orderEl.Element("active").Value = "1";
                orderEl.Element("id_default_group").Value = "3";
                orderEl.Element("associations").Element("groups").Descendants().Last().Value = "3";

                #region campos no obligatorios
                //orderEl.Element("id_lang").Value = mysql.defaultLangPS().ToString();
                //orderEl.Element("newsletter_date_add").Value = "";
                //orderEl.Element("ip_registration_newsletter").Value = "";
                //orderEl.Element("last_passwd_gen").Value = "";
                //orderEl.Element("secure_key").Value = "";
                //orderEl.Element("deleted").Value = "";
                //orderEl.Element("birthday").Value = ""; 
                //orderEl.Element("newsletter").Value = "";
                //orderEl.Element("optin").Value = "";
                //orderEl.Element("website").Value = "";
                //orderEl.Element("company").Value = "";
                //orderEl.Element("siret").Value = "";
                //orderEl.Element("ape").Value = "";
                //orderEl.Element("outstanding_allow_amount").Value = "";
                //orderEl.Element("show_public_prices").Value = "";
                //orderEl.Element("id_risk").Value = "";
                //orderEl.Element("max_payment_days").Value = "";            
                //orderEl.Element("note").Value = "";
                //orderEl.Element("is_guest").Value = "";
                //orderEl.Element("id_shop").Value = "";
                //orderEl.Element("id_shop_group").Value = "";
                //orderEl.Element("date_add").Value = "";
                //orderEl.Element("date_upd").Value = "";
                #endregion

                //orderEl.Element("kls_a3erp_id").Value = "";
                //orderEl.Element("kls_a3erp_fam_id").Value = "";


                request = new RestRequest("customers?ws_key=" + csGlobal.webServiceKey, Method.POST);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception ex)
            { errorLog.guardarErrorFichero(ex.ToString()); }
        }

        public  void facturasA3ERPToRepasat(string serie = null)
        {
            try
            {
                Repasat.SyncRepasat.csSyncRepasat syncRepasat = new Repasat.SyncRepasat.csSyncRepasat();
                syncRepasat.clientesRepasatToA3ERP();
                syncRepasat.facturasA3ERPToRepasat(serie);
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString()); 
            }

        }

        public void actualizarStockA3ERPToRepasat()
        {
            try
            {
                bool selectProductsMovsChecked = false; 
                bool allProductsChecked = false; 
                int numUpdownMovDays = 5;

                Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
                updateDataRPST.actualizarStockA3ERPToRepasat(selectProductsMovsChecked, allProductsChecked, numUpdownMovDays);
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        public void actualizarClientesA3ERPToRepasat()
        {
            try
            {
                //var updateDataRPST = new csUpdateData();
                updateDataRPST.actualizarCuentasA3ToRepasat(clientes: true, update: true, accountsToUpdate: null, cronjob: true);
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.registrarError(ex, "Error en función 'ActualizarClientesA3ERPToRepasat'");
            }
        }

        public void facturasVRepasatToA3ERP(string serie = null)
        {
            try
            {
                //para añadir en parametros de ejecución automática
                //TEST_ESYNC_KLS facturasRepasatToA3ERP 23A

                bool docsVenta = true;
                bool consulta = false;

                DateTime currentDateTime = DateTime.Now;

                DateTime fechaInicio = currentDateTime.AddDays(-7);
                DateTime fechaFin = currentDateTime;

                //Si es lacor hacer que pase de principio de año hasta el dia 1 del messiguiente
                switch (csGlobal.filtro_fecha)
                {
                    case "year":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, 1, 1).AddYears(1).AddDays(-1);
                        break;
                    case "moth":
                        fechaInicio = new DateTime(currentDateTime.Year, currentDateTime.Month, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                    case "yearNextMoth":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                }

                string filtroFechaIni = fechaInicio.Date.ToString("yyyy-MM-dd");
                string filtroFechaFin = fechaFin.Date.ToString("yyyy-MM-dd");

                csDismay repasat = new klsync.csDismay();
                repasat.cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, docsVenta, consulta, "NO", serie);

            }
            catch (Exception ex) { errorLog.guardarErrorFichero(ex.ToString()); }

        }
        public void facturasCRepasatToA3ERP(string serie = null)
        {
            try
            {
                //para añadir en parametros de ejecución automática
                //TEST_ESYNC_KLS facturasCompraRepasatToA3ERP 23A

                bool docsVenta = false;
                bool consulta = false;

                DateTime currentDateTime = DateTime.Now;

                DateTime fechaInicio = currentDateTime.AddDays(-7);
                DateTime fechaFin = currentDateTime;

                //Si es lacor hacer que pase de principio de año hasta el dia 1 del messiguiente
                switch (csGlobal.filtro_fecha)
                {
                    case "year":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, 1, 1).AddYears(1).AddDays(-1);
                        break;
                    case "moth":
                        fechaInicio = new DateTime(currentDateTime.Year, currentDateTime.Month, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                    case "yearNextMoth":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                }

                string filtroFechaIni = fechaInicio.Date.ToString("yyyy-MM-dd");
                string filtroFechaFin = fechaFin.Date.ToString("yyyy-MM-dd");

                csDismay repasat = new klsync.csDismay();
                repasat.cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, docsVenta, consulta, "NO", serie);

            }
            catch (Exception ex) { errorLog.guardarErrorFichero(ex.ToString()); }

        }

        public void pedidosVRepasatToA3ERP(string serie = null)
        {
            try
            {
                //para añadir en parametros de ejecución automática
                //TEST_ESYNC_KLS facturasRepasatToA3ERP 23A

                bool docsVenta = true;
                bool consulta = false;

                DateTime currentDateTime = DateTime.Now;

                DateTime fechaInicio = currentDateTime.AddDays(-7);
                DateTime fechaFin = currentDateTime;

                //Si es lacor hacer que pase de principio de año hasta el dia 1 del messiguiente
                switch (csGlobal.filtro_fecha)
                {
                    case "year":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, 1, 1).AddYears(1).AddDays(-1);
                        break;
                    case "moth":
                        fechaInicio = new DateTime(currentDateTime.Year, currentDateTime.Month, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                    case "yearNextMoth":
                        fechaInicio = new DateTime(currentDateTime.Year, 1, 1);
                        fechaFin = new DateTime(currentDateTime.Year, currentDateTime.Month, 1).AddMonths(1).AddDays(-1);
                        break;
                    case "30days":
                        fechaInicio = currentDateTime.AddDays(-30);
                        break;
                }

                string filtroFechaIni = fechaInicio.Date.ToString("yyyy-MM-dd");
                string filtroFechaFin = fechaFin.Date.ToString("yyyy-MM-dd");

                csGlobal.docDestino = "Pedido";

                csDismay repasat = new klsync.csDismay();
                repasat.cargarPedidos(filtroFechaIni, filtroFechaFin, docsVenta, consulta, "NO");

            }
            catch (Exception ex) {
                errorLog.guardarErrorFichero(ex.ToString());
            }

        }

        public void cuentasRepasatToA3ERP(bool clientes)
        {
            try
            {
                //var updateDataRPST = new csUpdateData();
                updateDataRPST.traspasarCuentasRepasatToA3(clientes);
            }
            catch (Exception ex)
            {
                errorLog.registrarError(ex, "Error en función 'CuentasRepasatToA3ERP'");
            }
        }

        public void actualizarAuxiliaresA3ERPToRepasat()
        {
            try
            {
                Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
                updateDataRPST.auxiliaresA3ToRepasat();
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }

        }
        public void actualizarArticulosA3toRPST()
        {
            try
            {
                csDismay repasat = new klsync.csDismay();
                repasat.crearArticulosDeA3ERPToRepasat(true);
                System.Windows.Forms.Application.Exit();
            }
            catch(Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }
        public void crearArticulosA3toRPST()
        {
            try
            {
                csDismay repasat = new klsync.csDismay();
                repasat.crearArticulosDeA3ERPToRepasat();
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }
        public void actualizarArticulosRPSTtoA3()
        {
            try
            {
                csDismay repasat = new klsync.csDismay();
                repasat.actualizarArticulosRPST();
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }

        //PRUEBA CRONJOB CENTROS DE COSTE
        public void sincroCentrosCosteA3ToRPST()
        {
            try
            {
                csDismay repasat = new klsync.csDismay();
                repasat.cargarCentrosDeCoste();
                System.Windows.Forms.Application.Exit();
            }
            catch (Exception ex)
            {
                errorLog.guardarErrorFichero(ex.ToString());
            }
        }
        

        //public void crearArticulosRPSTtoA3()
        //{
        //    try
        //    {
        //        frRepasat repasat = new klsync.frRepasat();
        //        repasat.cargarDatosArticulos(dgvAccountsRepasat, false, null, false);
        //        System.Windows.Forms.Application.Exit();
        //    }
        //    catch (Exception ex)
        //    {
        //        errorLog.guardarErrorFichero(ex.ToString());
        //    }
        //}
    }
}
