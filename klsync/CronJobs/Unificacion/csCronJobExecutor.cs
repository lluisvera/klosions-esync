﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.CronJobs.Unificacion
{
    /// <summary>
    /// Clase para gestionar la logica de la unificación de los cronjobs
    /// </summary>
    public class csCronJobExecutor
    {
        CronJobs.csRPSTCronJobs RPSTcronJobs = new CronJobs.csRPSTCronJobs();
        public void ejecutarTareasProgramadas(List<csCronJobConfig> tareasAEjecutar)
        {
            Program.guardarProcesoTareasProgramadas("CronJobs ejecutados a las " + System.DateTime.UtcNow);
            foreach (var tarea in tareasAEjecutar)
            {
                switch (tarea.Tarea)
                {
                    case "sincroCentrosCosteA3ToRPST":
                        Program.guardarProcesoTareasProgramadas("ENTRA A: sincroCentrosCosteA3ToRPST");
                        RPSTcronJobs.sincroCentrosCosteA3ToRPST();
                        break;

                    case "actualizarArticulosRPSTtoA3":
                        Program.guardarProcesoTareasProgramadas("ENTRA A: actualizarArticulosRPSTtoA3");
                        RPSTcronJobs.actualizarArticulosRPSTtoA3();
                        break;
                    default:
                        Console.WriteLine($"Tarea desconocida: {tarea.Tarea}");
                        break;
                }

                tarea.UltimaEjecucion = DateTime.Now;
                new csCronJobService().guardarRegistros(new List<csCronJobConfig> { tarea });
            }
        }
    }
}
