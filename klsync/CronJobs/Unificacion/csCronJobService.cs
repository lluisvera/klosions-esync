﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32.TaskScheduler;

namespace klsync.CronJobs.Unificacion
{
    /// <summary>
    /// CLASE PARA GESTIONAR CONSULTAS CON LA BASE DE DATOS
    /// </summary>
    public class csCronJobService
    {    
        private readonly Utilidades.csConexiones dbConnectEsync; 

        public csCronJobService()
        {        
            dbConnectEsync = new Utilidades.csConexiones();   
        }

        /// <summary>
        /// GUARDA O ACTUALIZA EL ESTADO DE LOS CRONJOBS EN LA BASE DE DATOS DE RPST/PRESTA
        /// </summary>
        public void guardarRegistros(List<csCronJobConfig> registros)
        {
            try
            {
                string connectionString = dbConnectEsync.cadenaConexionEsyncDB();

                foreach (var reg in registros)
                {
                    string checkQuery = $"SELECT COUNT(*) FROM cronjobsParameters WHERE idTask = {reg.Id} AND connection_name = '{reg.Conexion}'";
                    //string checkQuery = $"SELECT COUNT(*) FROM cronjobsParameters WHERE taskName = '{reg.Tarea}' AND connection_name = '{reg.Conexion}'";
                    bool registroExiste = false;

                    try
                    {
                        var resultado = csUtilidades.obtenerValorMySQL(connectionString, checkQuery);
                        registroExiste = int.TryParse(resultado, out int count) && count > 0;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error al verificar la existencia del registro: {ex.Message}");
                    }

                    string query;
                    if (registroExiste)
                    {
                        // Actualizar el registro existente
                        query = $"UPDATE cronjobsParameters " +
                                $"SET isChecked = {(reg.Ejecutar ? 1 : 0)}, " +
                                $"frequency = '{reg.Frecuencia}', " +
                                $"repetitionMinutes = {(reg.RepeticionMinutos.HasValue ? reg.RepeticionMinutos.Value.ToString() : "NULL")}, " +
                                $"lastExecution = {(reg.UltimaEjecucion.HasValue ? $"'{reg.UltimaEjecucion.Value:yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                                $"executionTime = {(reg.HoraEjecucion.HasValue ? $"'{DateTime.Today.Add(reg.HoraEjecucion.Value):yyyy-MM-dd HH:mm:ss}'" : "NULL")} " +
                                $"WHERE idTask = {reg.Id} AND connection_name = '{reg.Conexion}'";
                    }
                    else
                    {
                        // Insertar un nuevo registro
                        query = $"INSERT INTO cronjobsParameters (idTask, isChecked, frequency, connection_name, lastExecution, executionTime, repetitionMinutes) " +
                                $"VALUES ({reg.Id}, {(reg.Ejecutar ? 1 : 0)}, '{reg.Frecuencia}', '{reg.Conexion}', " +
                                $"{(reg.UltimaEjecucion.HasValue ? $"'{reg.UltimaEjecucion.Value:yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                                $"{(reg.HoraEjecucion.HasValue ? $"'{DateTime.Today.Add(reg.HoraEjecucion.Value):yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                                $"{(reg.RepeticionMinutos.HasValue ? reg.RepeticionMinutos.Value.ToString() : "NULL")})";
                    }
                    //if (registroExiste)
                    //{
                    //    query = $"UPDATE cronjobsParameters " +
                    //            $"SET isChecked = {(reg.Ejecutar ? 1 : 0)}, " +
                    //            $"frequency = '{reg.Frecuencia}', " +
                    //            $"repetitionMinutes = {(reg.RepeticionMinutos.HasValue ? reg.RepeticionMinutos.Value.ToString() : "NULL")}, " +
                    //            $"lastExecution = {(reg.UltimaEjecucion.HasValue ? $"'{reg.UltimaEjecucion.Value:yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                    //            $"executionTime = {(reg.HoraEjecucion.HasValue ? $"'{DateTime.Today.Add(reg.HoraEjecucion.Value):yyyy-MM-dd HH:mm:ss}'" : "NULL")} " +
                    //            $"WHERE taskName = '{reg.Tarea}' AND connection_name = '{reg.Conexion}'";
                    //}
                    //else
                    //{
                    //    query = $"INSERT INTO cronjobsParameters (taskName, isChecked, frequency, connection_name, lastExecution, executionTime, repetitionMinutes) " +
                    //            $"VALUES ('{reg.Tarea}', {(reg.Ejecutar ? 1 : 0)}, '{reg.Frecuencia}', '{reg.Conexion}', " +
                    //            $"{(reg.UltimaEjecucion.HasValue ? $"'{reg.UltimaEjecucion.Value:yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                    //            $"{(reg.HoraEjecucion.HasValue ? $"'{DateTime.Today.Add(reg.HoraEjecucion.Value):yyyy-MM-dd HH:mm:ss}'" : "NULL")}, " +
                    //            $"{(reg.RepeticionMinutos.HasValue ? reg.RepeticionMinutos.Value.ToString() : "NULL")})";
                    //}

                    try
                    {
                        csUtilidades.ejecutarComandoMySQL(connectionString, query);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error al ejecutar la consulta para tarea '{reg.Tarea}': {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al guardar los estados de cronjobs: {ex.Message}");
            }
        }

        /// <summary>
        /// OBTIENE LOS DATOS DE TODOS LOS REGISTROS DE ESA CONEXION
        /// </summary>
        public List<csCronJobConfig> obtenerTodosLosRegistros()
        {
            var cronJobs = new List<csCronJobConfig>();
            string connectionString = dbConnectEsync.cadenaConexionEsyncDB(); 
            string query = "SELECT idTask, isChecked, frequency, connection_name, lastExecution, executionTime, repetitionMinutes  FROM cronjobsParameters WHERE connection_name = '" + csGlobal.conexionDB + "'";

            try
            {
                DataTable resultados = csUtilidades.cargarTablaMySQL(connectionString, query);

                if (resultados != null)
                {
                    foreach (DataRow row in resultados.Rows)
                    {
                        var cronJob = new csCronJobConfig
                        {
                            Id = Convert.ToInt32(row["idTask"]),
                            //Tarea = row["taskName"].ToString(),
                            Ejecutar = Convert.ToBoolean(row["isChecked"]),
                            Frecuencia = row["frequency"].ToString(),
                            Conexion = row["connection_name"].ToString(),
                            UltimaEjecucion = row["lastExecution"] != DBNull.Value ? DateTime.Parse(row["lastExecution"].ToString()) : (DateTime?)null,
                            HoraEjecucion = row["executionTime"] != DBNull.Value ? TimeSpan.Parse(row["executionTime"].ToString()) : (TimeSpan?)null,
                            RepeticionMinutos = row["repetitionMinutes"] != DBNull.Value ? int.Parse(row["repetitionMinutes"].ToString()) : (int?)null
                        };

                        cronJobs.Add(cronJob);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener los estados de cronjobs: {ex.Message}");
            }

            return cronJobs;
        }

        /// <summary>
        /// FUNCION QUE SE USA PARA DETECTAR QUE TAREAS AUN NO SE HAN CREADO EN ESA CONEXION Y ASI MOSTRARLAS EN EL DESPLEGABLE
        /// </summary>
        public List<string> obtenerTareasDisponibles(string conexionActual)
        {
            int tipoTarea = (csGlobal.modeAp.ToUpper() == "REPASAT") ? 0 : 1;
            string connectionString = dbConnectEsync.cadenaConexionEsyncDB();
            string queryTodasLasTareas = "SELECT DISTINCT taskName FROM cronjobsAsignation WHERE connection_name = '" + csGlobal.conexionDB + "' AND taskType = " + tipoTarea;
            List<string> todasLasTareas = new List<string>();

            //ASIGNA TODAS LAS TAREAS A UNA LISTA
            try
            {
                DataTable todasLasTareasResult = csUtilidades.cargarTablaMySQL(connectionString, queryTodasLasTareas);

                if (todasLasTareasResult != null)
                {
                    todasLasTareas = todasLasTareasResult.AsEnumerable().Select(row => row["taskName"].ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }

            //string queryTareasExistentes = $"SELECT taskName FROM cronjobsParameters WHERE connection_name = '{conexionActual}'";
            string queryTareasExistentes = $"SELECT DISTINCT ca.taskName FROM cronjobsParameters cp INNER JOIN cronjobsAsignation ca ON cp.idTask = ca.id WHERE cp.connection_name = '{conexionActual}'";
            List<string> tareasExistentes = new List<string>();

            //ASIGNA LAS TAREAS YA CREADAS A UNA LISTA
            try
            {
                DataTable resultados = csUtilidades.cargarTablaMySQL(connectionString, queryTareasExistentes);

                if (resultados != null)
                {
                    tareasExistentes = resultados.AsEnumerable().Select(row => row["taskName"].ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }

            //ASIGNA LAS TAREAS PENDIENTES POR CREAR
            var tareasDisponibles = todasLasTareas.Except(tareasExistentes).ToList();

            return tareasDisponibles;
        }

        //FUNCION PARA ELIMINAR UN REGISTRO DE LA BASE DE DATOS
        public bool borrarRegistro(string nombreTarea, string connectionName)
        {
            try
            {
                string connectionString = dbConnectEsync.cadenaConexionEsyncDB();
                string query = $"DELETE FROM cronjobsParameters WHERE taskName = '{nombreTarea}' AND connection_name = '{connectionName}'";

                csUtilidades.ejecutarComandoMySQL(connectionString, query);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //FUNCION PARA CREAR O EDITAR TAREAS PROGRAMADAS DE WINDOWS
        public void crearTareaProgramada(csCronJobConfig cronJob)
        {
            using (TaskService ts = new TaskService())
            {
                string carpetaTareas = @"\Klosions_eSync";

                TaskFolder folder = ts.RootFolder.SubFolders.FirstOrDefault(f => f.Path == carpetaTareas);
                if (folder == null)
                {
                    folder = ts.RootFolder.CreateFolder(carpetaTareas);
                }

                string nombreTarea = $"ESYNC_{cronJob.Tarea}";

                Microsoft.Win32.TaskScheduler.Task existingTask = folder.Tasks.FirstOrDefault(t => t.Name == nombreTarea);
                if (existingTask != null)
                {
                    folder.DeleteTask(nombreTarea);
                }

                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = $"TAREA PROGRAMADA: {cronJob.Tarea}";
                td.Principal.RunLevel = TaskRunLevel.Highest;
                td.Principal.LogonType = TaskLogonType.ServiceAccount;
                td.Principal.UserId = "SYSTEM";

                switch (cronJob.Frecuencia.ToUpper())
                {
                    case "DIA":
                        td.Triggers.Add(new DailyTrigger
                        {
                            StartBoundary = DateTime.Today.Add(cronJob.HoraEjecucion.GetValueOrDefault())
                        });
                        break;

                    case "HORA":
                        td.Triggers.Add(new TimeTrigger
                        {
                            StartBoundary = DateTime.Today.Add(cronJob.HoraEjecucion.GetValueOrDefault()),
                            Repetition = new RepetitionPattern(TimeSpan.FromHours(1), TimeSpan.Zero)
                        });
                        break;

                    case "MINUTO":
                        td.Triggers.Add(new TimeTrigger
                        {
                            StartBoundary = DateTime.Today.Add(cronJob.HoraEjecucion.GetValueOrDefault()),
                            Repetition = new RepetitionPattern(TimeSpan.FromMinutes(cronJob.RepeticionMinutos ?? 5), TimeSpan.Zero)
                        });
                        break;
                }

                string programaPath = @"C:\Program Files (x86)\Klosions eSync\Klosions.exe";
                string argumentos = $"{csGlobal.conexionDB} {cronJob.Tarea}";
                string iniciarEn = @"C:\Program Files (x86)\Klosions eSync";
                td.Actions.Add(new ExecAction(programaPath, argumentos, iniciarEn));

                td.Settings.RunOnlyIfIdle = false;
                td.Settings.DisallowStartIfOnBatteries = false;
                td.Settings.StopIfGoingOnBatteries = false;

                folder.RegisterTaskDefinition(nombreTarea, td);
            }
        }

        public void borrarTareaProgramada(string nombreTarea)
        {
            using (TaskService ts = new TaskService())
            {
                string carpetaTareas = @"\Klosions_eSync";

                try
                {
                    TaskFolder folder = ts.GetFolder(carpetaTareas);

                    string taskName = $"ESYNC_{nombreTarea}";

                    if (folder.Tasks.Any(t => t.Name == taskName))
                    {
                        folder.DeleteTask(taskName, false);
                        Console.WriteLine($"Tarea '{taskName}' eliminada correctamente.");
                    }
                    else
                    {
                        Console.WriteLine($"La tarea '{taskName}' no existe en la carpeta '{carpetaTareas}'.");
                    }
                }
                catch (FileNotFoundException)
                {
                    Console.WriteLine($"La carpeta '{carpetaTareas}' no existe. No se pudo eliminar la tarea '{nombreTarea}'.");
                }
            }
        }



        public bool mostrarTareasProgramadas(string nombreConexion)
        {
            try
            {
                string query = $"SELECT COUNT(*) FROM cronjobsAsignation WHERE connection_name = '{nombreConexion}'";

                string connectionString = dbConnectEsync.cadenaConexionEsyncDB();

                var resultado = csUtilidades.obtenerValorMySQL(connectionString, query);

                if (int.TryParse(resultado, out int cantidadTareas) && cantidadTareas > 0)
                {
                    return true; 
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine($"Error al verificar las tareas programadas: {ex.Message}");
            }

            return false;
        }





        /// <summary>
        /// Se encarga de indicar que tareas deben ejecutarse según estado y frequencia
        /// </summary>
        /// <returns></returns>
        //public List<csCronJobConfig> obtenerTareasAEjecutar()
        //{
        //    List<csCronJobConfig> tareasAEjecutar = new List<csCronJobConfig>();
        //    List<csCronJobConfig> todasLasTareas = obtenerTodosLosCronjobs();

        //    Program.guardarErrorFichero($"Total de tareas obtenidas: {todasLasTareas.Count}");

        //    DateTime horaActual = DateTime.Now;

        //    foreach (var cronJob in todasLasTareas)
        //    {
        //        bool ejecutarTarea = false;
        //        Program.guardarErrorFichero($"Total de tareas obtenidas: {"FRECUENCIA: " + cronJob.Frecuencia}");
        //        switch (cronJob.Frecuencia.ToUpper())
        //        {
        //            case "DIARIA":
        //                Program.guardarErrorFichero($"Comparando fechas - Tarea: {cronJob.Tarea}, UltimaEjecucion: {cronJob.UltimaEjecucion?.Date}, horaActual.Date: {horaActual.Date}");

        //                ejecutarTarea = !cronJob.UltimaEjecucion.HasValue || cronJob.UltimaEjecucion.Value.AddHours(24) <= horaActual;

        //                Program.guardarErrorFichero($"Resultado de evaluación - Tarea: {cronJob.Tarea}, Ejecutar: {cronJob.Ejecutar}, ejecutarTarea (Diaria): {ejecutarTarea}");
        //                break;

        //            case "HORA":
        //                Program.guardarErrorFichero($"Comparando horas - Tarea: {cronJob.Tarea}, UltimaEjecucion: {cronJob.UltimaEjecucion?.ToString("yyyy-MM-dd HH:mm:ss")}, horaActual: {horaActual}");

        //                ejecutarTarea = !cronJob.UltimaEjecucion.HasValue || cronJob.UltimaEjecucion.Value.AddHours(1) <= horaActual;

        //                Program.guardarErrorFichero($"Resultado de evaluación - Tarea: {cronJob.Tarea}, Ejecutar: {cronJob.Ejecutar}, ejecutarTarea (Hora): {ejecutarTarea}");
        //                break;

        //            case "MINUTO":
        //                Program.guardarErrorFichero($"Comparando minutos - Tarea: {cronJob.Tarea}, UltimaEjecucion: {cronJob.UltimaEjecucion?.ToString("yyyy-MM-dd HH:mm:ss")}, horaActual: {horaActual}");

        //                ejecutarTarea = !cronJob.UltimaEjecucion.HasValue || cronJob.UltimaEjecucion.Value.AddMinutes(5) <= horaActual;

        //                Program.guardarErrorFichero($"Resultado de evaluación - Tarea: {cronJob.Tarea}, Ejecutar: {cronJob.Ejecutar}, ejecutarTarea (Minuto): {ejecutarTarea}");
        //                break;

        //            default:
        //                Program.guardarErrorFichero($"Frecuencia desconocida para la tarea: {cronJob.Tarea}");
        //                break;
        //        }

        //        if (cronJob.Ejecutar && ejecutarTarea)
        //        {
        //            tareasAEjecutar.Add(cronJob);
        //        }
        //    }
        //    Program.guardarErrorFichero($"Tareas a ejecutar: {tareasAEjecutar.Count}");
        //    return tareasAEjecutar;
        //}
    }
}
