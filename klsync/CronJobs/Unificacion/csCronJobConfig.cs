﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.CronJobs.Unificacion
{
    /// <summary>
    /// Clase para crear modelo de los registros de la unificación de cronjobs
    /// </summary>
    public class csCronJobConfig
    {
        public int Id { get; set; }
        public string Tarea { get; set; }
        public bool Ejecutar { get; set; }
        public string Frecuencia { get; set; }
        public string Conexion { get; set; }
        public DateTime? UltimaEjecucion { get; set; }
        public TimeSpan? HoraEjecucion { get; set; }
        public int? RepeticionMinutos { get; set; }
    }
}
