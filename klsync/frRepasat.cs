﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using a3ERPActiveX;
using MySql.Data.MySqlClient;
//using System.Xaml;
using klsync.Repasat.LAUData;
using klsync.Utilidades;
using RestSharp.Extensions.MonoHttp;
using HttpUtility = System.Web.HttpUtility;
using klsync.A3ERP;
using klsync.Dismay;
using klsync.A3ERP.Auxiliares;
using klsync.CronJobs.Unificacion;

namespace klsync
{
    public partial class csDismay : Form
    {
        private static csDismay m_FormDefInstance;
        csRepasatUtilities rpstUtil = new csRepasatUtilities();
        csCronJobService cronJobService = new csCronJobService();

        SqlConnection dataconnection = new SqlConnection();
        SqlCommand datacommand = new SqlCommand();
        Repasat.LoadObjects.csDtObjects loadRPSTObject = new Repasat.LoadObjects.csDtObjects();
        Utilidades.csLogErrores csLogProceso = new Utilidades.csLogErrores();

        public static csDismay DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new csDismay();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        string tipoDocumento = "";
        string almacenA3ERP = "";

        public csDismay()
        {
            InitializeComponent();
        }

        Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
        csSqlConnects sql = new csSqlConnects();
        string tipoObjeto = "";
        string campoExternoRepasat = "";
        bool check = false;
        string tablaToUpdate = "";
        string campoKeyToUpdate = "";
        string repasatKeyField = "";
        private const int maxPanelSize = 250;
        bool inicializarDgvRPST = true;



        // Sincronizacion Clientes - Repasat > A3

        public void cargarDatosArticulos(DataGridView dgv, bool consulta = false, string productId = null, bool pendientes = false, bool update = false)
        {
            string errorProducto = "";

            try
            {

                Repasat.LoadObjects.csObjectArticulo objArticulo = new Repasat.LoadObjects.csObjectArticulo();
                objArticulo.dgv = dgv;
                objArticulo.consulta = consulta;
                objArticulo.productId = productId;
                objArticulo.pendientes = pendientes;
                objArticulo.update = update;
                objArticulo.syncAllData = rbSyncAll.Checked;
                objArticulo.syncYesData = rbSyncYes.Checked;
                objArticulo.syncNoData = rbSyncNo.Checked;
                objArticulo.accountId = tbAccountId.Text;

                Repasat.LoadData.csLoadArticulos loadArticulos = new Repasat.LoadData.csLoadArticulos();

                dgv.DataSource = loadArticulos.cargarDatosArticulos(objArticulo);
        }
            catch (System.Runtime.InteropServices.COMException ex) { MessageBox.Show(ex.Message); }
            catch (JsonException ex) { MessageBox.Show(ex.Message); }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + errorProducto);

            }

        }
        private void cargarDireccionesCuentasA3ERP(DataTable direcciones)
        {
            string tabla = cboxMaestros.SelectedText == "Clientes" ? "DIRENT" : "__DIRENTPRO";
            string keyField = cboxMaestros.SelectedText == "Clientes" ? "CODCLI" : "CODPRO";

            string filtroQuery = csUtilidades.delimitarPorComasDataTableNotDouble(direcciones, "IDCUENTAA3");

            string query = "SELECT " +
                " RPST_ID_DIRPROV AS IDREPASAT, NOMENT AS NOMBRE, DIRENT1 AS DIRECCION, DTOENT AS CP, " +
                " POBENT AS POBLACION, CAST(IDDIRENT AS INT) AS EXTERNALID FROM " + tabla + " WHERE LTRIM(" + keyField + ") IN ('" + filtroQuery + "')";

            DataTable dt = sql.cargarDatosTablaA3(query);
            dgvAccountsA3ERP.DataSource = dt;
        }

        // Mediante la API de Repasat > Objeto facturas de venta, introducimos las facturas que no tengan código externo


        private bool existProduct(string reference)
        {
            csSqlConnects sql = new csSqlConnects();
            if (sql.consultaExiste("SELECT ltrim(codart) FROM ARTICULO WHERE LTRIM(CODART) = '" + reference + "'"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void SetBasicAuthHeader(WebRequest request, String userName, String userPassword)
        {
            string authInfo = userName + ":" + userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
        }


        private void cargarClientesA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void cargarClientesA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = null;

            a = new SqlDataAdapter(sqlScript.selectClientesConCamposA3(" CODCLI, NOMCLI, E_MAIL, TELCLI, NIFCLI, PAGINAWEB, BLOQUEADO "), dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            dgvAccountsRepasat.DataSource = t;
        }

        public string subirClientesdeA3aRepasat(string nomCli, string cifCli, string codExternoCli, string webCli, string tel1, string emailCli, string bloqueado, string url = "http://localhost:8080/repasat/es/api/customer")
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    System.Collections.Specialized.NameValueCollection reqparm = new System.Collections.Specialized.NameValueCollection();
                    reqparm.Add("nomCli", nomCli);
                    reqparm.Add("cifCli", cifCli);
                    reqparm.Add("codExternoCli", codExternoCli);
                    reqparm.Add("webCli", webCli);
                    reqparm.Add("tel1", tel1);
                    reqparm.Add("emailCli", emailCli);
                    reqparm.Add("bloqueado", bloqueado);

                    client.Credentials = new NetworkCredential(csGlobal.UserWS, csGlobal.PassWS);
                    byte[] responsebytes = client.UploadValues(url, "PUT", reqparm);
                    string responsebody = Encoding.UTF8.GetString(responsebytes);
                    return responsebody;
                }
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd(); // Si la id es foranea se puede debuggar el mensaje por aqui
                return resp;
            }
        }

        public XmlDocument ToXmlDocument(XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();

            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        public XDocument ToXDocument(XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }


        private void linkLabelJSonViewer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://jsonviewer.stack.hu/");
        }

        private void frRepasat_Load(object sender, EventArgs e)
        {
            var now = DateTime.Now;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            dtpFromInvoicesV.Value = startOfMonth;
            cboxSeries.Visible = false;
            grBoxEstadoCobro.Visible = false;
            grBoxRemesado.Visible = false;
            btnSyncToA3ERP.Visible = false;
            btnSyncToRPST.Visible = false;
            btnLoadDocsA3ERP.Visible = false;
            btnLoadDocsRPST.Visible = false;
            cargarCuentas(true);
            ctextMenuSync.Items["carteraToolStripMenuItem"].Visible = false;
            mostrarSelectorFechasCartera(false);
            this.dgvRPST.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
            this.dgvRPST.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10);
            if (csGlobal.activarUnidadesNegocio) {
                lblUdadNegocio.Visible = true;
                lblUdadNegocio.Text = "Udad Negocio:" + csGlobal.idUnidadNegocio;
            }
            ticketBaiToolStripMenuItem.Visible = false;
            cboxTicketBai.Checked = csGlobal.ticketBaiActivo ? true : false;


            ////// 
            /////SE COMENTA HASTA IMPLEMENTACION
            //actualizarDataGridView();
            //cargarTareasDisponibles();
            //cmbFrequency.SelectedIndexChanged += cmbFrequency_SelectedIndexChanged;

            //bool mostrarCronJobs = cronJobService.mostrarTareasProgramadas(csGlobal.conexionDB);

            //if (!mostrarCronJobs)
            //{
            tabControl1.TabPages.Remove(tabCronJobs);
            //}
            //////
            ///
            setMaxDateForDateTimePicker();

        }

        private void setMaxDateForDateTimePicker()
         {  
            //FECHA MAXIMA SELECTOR
            var fechaMaxima = csUtilidades.obtenerFechaMaximaDesdeDB();

            if (fechaMaxima.HasValue)
            {
                dtpToInvoicesV.MaxDate = fechaMaxima.Value;
            }
            else
            {
                dtpToInvoicesV.MaxDate = DateTime.Now;
            }

            if (dtpToInvoicesV.Value > dtpToInvoicesV.MaxDate)
            {
                dtpToInvoicesV.Value = dtpToInvoicesV.MaxDate;
            }

            // FECHA MINIMA SELECTOR
            var fechaMinima = csUtilidades.obtenerFechaMinimaDesdeDB();

            if (fechaMinima.HasValue)
            {
                dtpFromInvoicesV.MinDate = fechaMinima.Value;
            }
            else
            {
                dtpFromInvoicesV.MinDate = DateTimePicker.MinimumDateTime;
            }

            dtpFromInvoicesV.Value = new DateTime(DateTime.Now.Year, 1, 1);

            if (dtpFromInvoicesV.Value < dtpFromInvoicesV.MinDate)
            {
                dtpFromInvoicesV.Value = dtpFromInvoicesV.MinDate;
            }
        }




        //private void cmbFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cmbFrequency.SelectedItem != null && cmbFrequency.SelectedItem.ToString().ToUpper() == "DIA")
        //    {
        //        dtpStartHour.Visible = true;
        //        lblStartHour.Visible = true;
        //    }
        //    else
        //    {
        //        dtpStartHour.Visible = false;
        //        lblStartHour.Visible = false;
        //    }
        //}


        private void formatearCeldaImporte(DataGridView dgv, string nombreColumna, int columna, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv.Columns[columna].Name == nombreColumna)
            {
                dgv.Columns[nombreColumna].DefaultCellStyle.Format = "N2";
                dgv.Columns[nombreColumna].ValueType = typeof(Decimal);
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

        }

        private void formatearCeldaFecha(string nombreColumna, int columna, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgvRPST.Columns[columna].Name == nombreColumna)
            {
                dgvRPST.Columns[nombreColumna].DefaultCellStyle.Format = "d";
                //dgvRPST.Columns[nombreColumna].ValueType = typeof(Decimal);
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

        }

        private void dgvRPST_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            formatearCeldaImporte(dgvRPST, "IMPORTE_DOC", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "BASE_DOC", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "IMPORTE_EFECTO", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "IMPORTE", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "BASE", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "IMPORTECOB", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "TOTDOC", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "TOTIVA", e.ColumnIndex, e);
            formatearCeldaImporte(dgvRPST, "TotalDoc", e.ColumnIndex, e);
            formatearCeldaFecha("FECHA", e.ColumnIndex, e);
            string[] columnsShortCuts = {"NUMFRA_QR", "NUMSERIE_QR" };
            if (columnsShortCuts.Contains(this.dgvRPST.Columns[e.ColumnIndex].Name))
            {
                dgvRPST.Columns["NUM_FRA"].DefaultCellStyle.Format = "N0";
                dgvRPST.Columns["NUM_FRA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvRPST.Columns["NUMFRA_QR"].DefaultCellStyle.Format = "N0";
                dgvRPST.Columns["NUMFRA_QR"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvRPST.Columns[e.ColumnIndex].Name == "LINEAS")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            //Asignación Imagenes link
            if (dgvRPST.Rows.Count > 0)
            {
                if (this.dgvRPST.Columns[e.ColumnIndex].Name == "RPST")
                {
                    if (dgvRPST.Columns.Contains("REPASAT_ID_DOC"))
                    {
                        if (!string.IsNullOrEmpty(this.dgvRPST["REPASAT_ID_DOC", e.RowIndex].Value.ToString()))
                        {
                            e.Value = Properties.Resources.Repasat16;
                        }
                        else
                        {
                            e.Value = Properties.Resources.transp16; ;
                        }
                    }
                }

                //if (this.dgvRPST.Columns[e.ColumnIndex].Name == "ATR")
                //{

                //    if (this.dgvRPST["TyC", e.RowIndex].Value.ToString() == "T")
                //    {
                //        e.Value = Properties.Resources.tycAttributes;
                //    }
                //    else
                //    {
                //        e.Value = Properties.Resources.tycAttributesVoid;
                //    }

                //}
            }


        }

        private void btLoadInvoicesV_Click(object sender, EventArgs e)
        {
            cargarFacturasRepasat();
        }

        private void cargarFacturasRepasat()
        {
            double totalInvoices = 0;
            string fechaDesde = dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
            string fechaHasta = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");
            csMySqlConnect mysql = new csMySqlConnect();
            string query = "select idDocumento,codExternoDocumento as IdSync,tipoDocumento,facturas.idCli,nomCli as Cliente,facturas.idSerie,series.nomSerie,numSerieDocumento as NumDoc, " +
                " fecDocumento as Fecha,refDocumento as Referencia,totalDocumento as TotalDoc " +
                " from facturas " +
                " inner join clientes on facturas.idCli=clientes.idCli " +
                " inner join series on   facturas.idSerie=series.idSerie " +
                " where idEntidadDocumento=102  and fecDocumento between '" + fechaDesde + "' and '" + fechaHasta + "'";
            csUtilidades.addDataSource(dgvRPST, mysql.cargarTabla(query));
            foreach (DataGridViewRow factura in dgvRPST.Rows)
            {
                totalInvoices = totalInvoices + Convert.ToDouble(factura.Cells["TotalDoc"].Value.ToString().Replace('.', ','));
            }
            totalInvoices = Math.Round(totalInvoices, 2);

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            actualizarCuentasA3ToRepasat(true, true);
        }

        private void actualizarCuentasA3ToRepasat(bool clientes, bool update)
        {
            Repasat.csUpdateData rpstUpdateData = new Repasat.csUpdateData();
            rpstUpdateData.cargarCuentasA3ToRepasat(true, true);
        }




        public void cargarRutasA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat rutasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = rutasRPST.cargarObjetoERPToRepasat("RUTAS");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("routes", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }


        private void crearRutasA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosRutasRepasat();

            String consulta = "";

            if (dtDatos.Rows.Count >= 1)  
            {
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if (fila["CODIGOERP"] == "" || fila["CODIGOERP"] == null)
                    {
                        consulta = "insert into rutas (ruta, nomruta, RPST_ID_RUTA) values ('" + fila["CODRUTA"] + "', '" + fila["DESCRIPCION"] + "', '" + fila["IDREPASAT"] + "');";

                        csUtilidades.ejecutarConsulta(consulta, false);
                    }

                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), fila["CODRUTA"].ToString());
                }

            }
        }

        public void cargarZonasA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat zonasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = zonasRPST.cargarObjetoERPToRepasat("ZONAS");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("geozones", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }

        }

        private void crearZonaA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosZonasRepasat();

            String consulta = "";

            if (dtDatos.Rows.Count >= 1)
            {
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if (fila["CODIGOERP"] == "" || fila["CODIGOERP"] == null)
                    {
                        consulta = "insert into zonas (zona, nomzona, RPST_ID_ZONA) values ('" + fila["CODZONA"] + "', '" + fila["DESCRIPCION"] + "', '" + fila["IDREPASAT"] + "');";

                        csUtilidades.ejecutarConsulta(consulta, false);
                    }

                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), fila["CODZONA"].ToString());
                }

            }
        }

        private void cargarSeriesA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat seriesRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = seriesRPST.cargarObjetoERPToRepasat("SERIES");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("seriesdocs", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }

        //private void crearSerieA3()
        //{
        //    csRepasatWebService rpstWS = new csRepasatWebService();
        //    try
        //    {
        //        if (dgvRepasatData.Rows.Count >= 1)
        //        {
        //            foreach (DataGridViewRow fila in dgvRepasatData.Rows)
        //            {
        //                string query = "";
        //                query = "insert into SERIES (nomserie, serie, numeroinicial, RPST_ID_SERIE, RPST_SINCRONIZAR) values ('" + fila.Cells["DESCRIPCION"].Value + "', '" + fila.Cells["DESCRIPCION"].Value + "', '1', '" + fila.Cells["IDREPASAT"].Value + "', '1')";
        //                csUtilidades.ejecutarConsulta(query, false);

        //                rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila.Cells["IDREPASAT"].Value.ToString(), fila.Cells["DESCRIPCION"].Value.ToString());
        //                //rpstWS.sincronizarObjetosRepasat("seriesdocs","PUT",dtKey, dtParams, "fila.Cells[\"IDREPASAT\"].Value");
        //            }
        //        }
        //    }
        //    catch (Exception ex) { }
        //}

        private void crearSerieA3()
        {
            csRepasatWebService rpstWS = new csRepasatWebService();
            try
            {
                if (dgvRepasatData.Rows.Count >= 1)
                {
                    foreach (DataGridViewRow fila in dgvRepasatData.Rows)
                    {
                        string nomSerie = fila.Cells["DESCRIPCION"].Value.ToString();
                        string idRepasat = fila.Cells["IDREPASAT"].Value.ToString();

                        // Verificar si la serie ya existe en A3ERP usando consulta SQL
                        if (SerieExiste(nomSerie))
                        {
                            MessageBox.Show($"La serie '{nomSerie}' ya existe en A3ERP. Se omite la creación.");
                            continue;
                        }

                        // Inicializamos el objeto Maestro para gestionar las series en A3ERP
                        Maestro a3maestro = new Maestro();
                        a3maestro.Iniciar("series");

                        // Crear una nueva serie en A3ERP si no existe
                        a3maestro.Nuevo();
                        a3maestro.set_AsString("SERIE", nomSerie);
                        a3maestro.set_AsString("NOMSERIE", nomSerie);
                        a3maestro.set_AsString("RPST_ID_SERIE", idRepasat);
                        a3maestro.set_AsString("NUMEROINICIAL", "1");
                        a3maestro.Guarda(true);

                        // Actualizar en Repasat que la serie ha sido sincronizada
                        rpstWS.actualizarDocumentoRepasat("seriesdocs", "codExternoSerie", idRepasat, nomSerie);

                        // Liberar los recursos del objeto Maestro
                        a3maestro.Acabar();
                        a3maestro = null;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al crear las series en A3ERP: {ex.Message}");
            }
        }

        private bool SerieExiste(string nomSerie)
        {
            try
            {
                // Eliminar posibles espacios en blanco del nombre de la serie
                
                nomSerie = nomSerie.Trim();

                // Consulta SQL para verificar si la serie ya existe en A3ERP
                string query = $"SELECT COUNT(*) FROM series WHERE LTRIM(NOMSERIE) = '{nomSerie}'";

                // Usar el método obtenerDatoFromQuery para ejecutar la consulta y obtener el resultado

                
                string result = sql.obtenerCampoTabla(query);

                if (!string.IsNullOrEmpty(result) && Convert.ToInt32(result) > 0)
                {
                    return true; // La serie existe
                }
                else
                {
                    return false; // La serie no existe
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al comprobar si la serie '{nomSerie}' existe: {ex.Message}");
                return false;
            }
        }





        //060924
        //private bool SerieExiste(string nomSerie, Maestro a3maestro)
        //{
        //    try
        //    {
        //        // Aplicar filtro para verificar si existe la serie
        //        a3maestro.Filtro = $"SERIE = '{nomSerie}'";
        //        a3maestro.Filtrado = true;

        //        // Verificar si se encontró algún registro
        //        bool existe = !a3maestro.EOF;

        //        // Limpiar el filtro después de la búsqueda
        //        a3maestro.Filtrado = false;
        //        a3maestro.Filtro = "";  // Limpiar filtro

        //        return existe;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Error al comprobar si la serie '{nomSerie}' existe: {ex.Message}");
        //        return false;
        //    }
        //}

        //private void crearSerieA3(Objetos.csSeries[] series)
        //{
        //    try
        //    {
        //        for (int i = 0; i < series.Length; i++)
        //        {
        //            // Inicializamos el objeto Maestro para gestionar las series en A3ERP
        //            Maestro a3maestro = new Maestro();
        //            a3maestro.Iniciar("series");

        //            // Verificamos si la serie ya existe en A3ERP
        //            if (SerieExiste(series[i].nomSerie, a3maestro))
        //            {
        //                a3maestro.Acabar();
        //                a3maestro = null;
        //                continue;
        //            }

        //            // Si la serie no existe, la creamos en A3ERP
        //            a3maestro.Nuevo();
        //            a3maestro.set_AsString("SERIE", series[i].nomSerie);
        //            a3maestro.set_AsString("NOMSERIE", series[i].nomSerie);
        //            a3maestro.set_AsString("RPST_ID_SERIE", series[i].codExternoSerie);
        //            a3maestro.Guarda(true);

        //            // Liberamos los recursos del objeto Maestro
        //            a3maestro.Acabar();
        //            a3maestro = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Error al crear las series en A3ERP: {ex.Message}");
        //    }
        //}
        //

        private void crearAlmacenA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosAlmacenes();

            String consulta = "";
            if (dtDatos.Rows.Count >= 1)
            {
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if(fila["CODALMACEN"].ToString().Length > 8)
                    {
                        fila["CODALMACEN"] = fila["CODALMACEN"].ToString().Substring(0, 8);
                    }

                    if (fila["CODIGOERP"] == "" || fila["CODIGOERP"] == null)
                    {
                        consulta = "insert into ALMACEN (codalm, descalm, RPST_ID_ALM) values ('" + fila["CODALMACEN"] + "', '" + fila["DESCRIPCION"] + "', '" + fila["IDREPASAT"] + "');";

                        csUtilidades.ejecutarConsulta(consulta, false);
                    }

                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), fila["CODALMACEN"].ToString());
                }

            }
        }

        private void cargarEmpleadosA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat rutasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = rutasRPST.cargarObjetoERPToRepasat("REPRESENTANTES");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("employees", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }


        public void cargarMetodosPago()
        {
            try
            {
                Repasat.csObjetoRepasat rutasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = rutasRPST.cargarObjetoERPToRepasat("FORMASPAG");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("paymentmethods", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }

        }

        public void cargarCentrosDeCoste()
        {
            try
            {
                Repasat.csObjetoRepasat costCentersRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = costCentersRPST.cargarObjetoERPToRepasat("CENTROSC");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("costcenters", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }

        }

        private void cargarFamiliaArtA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat familiaArtRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = familiaArtRPST.cargarObjetoERPToRepasat("FAMILIAART");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("families", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }

        private void cargarSubfamiliaArtA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat familiaArtRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = familiaArtRPST.cargarObjetoERPToRepasat("SUBFAMILIAART");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("families", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }

        public void cargarMarcasA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat marcaArtRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = marcaArtRPST.cargarObjetoERPToRepasat("MARCAART");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("brands", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }
        public void cargarTipoArtA3ToRepasat()
        {
            try
            {
                Repasat.csObjetoRepasat marcaArtRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = marcaArtRPST.cargarObjetoERPToRepasat("TIPOART");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("producttypes", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }
        private void crearTipoArtA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosTipoArt();

            String consulta = "";
            if (dtDatos.Rows.Count >= 1)
            {
                string tipoArt = csGlobal.tipo_art;
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if (fila["CODIGOERP"] == "" || fila["CODIGOERP"] == null)
                    {
                        consulta = "insert into CARACTERISTICAS (codcar, desccar, RPST_ID_CARACTERISTICA, numCar, TIPCAR) values ('R" + fila["CODREPASAT"] + "', '" + fila["DESCRIPCION"] + "', '" + fila["IDREPASAT"] + "', '" + tipoArt + "', 'A');";

                        csUtilidades.ejecutarConsulta(consulta, false);
                        rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), "R"+fila["CODREPASAT"].ToString());
                    }

                }

            }
        }

        //  23/02/2024  
        //  Creamos las opciones de cargar y traspasar Tipo de articulos 
        private void btnTipoArt_Click(object sender, EventArgs e)
        {
            informObjectType("producttypes");
            tablaToUpdate = "CARACTERISTICAS";
            campoKeyToUpdate = "CODCAR";
            repasatKeyField = "RPST_ID_CARACTERISTICA";
            campoExternoRepasat = "codExternoTipoArticulo";
            dgvRepasatData.DataSource = loadRPSTObject.TipoArt();
            tipoArtA3ERP();
            btnCrearTipoArtA3.Enabled = true;
        }

        private void btnUploadTipoArt_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat los Tipos de articulos no asignadas, ¿está seguro/a?", "Sinronizar Tipos de articulos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarTipoArtA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.TipoArt();
                tipoArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnCrearTipoArtA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp los Tipos de articulos no asignadas, ¿está seguro/a?", "Sinronizar Tipos Articulo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                crearTipoArtA3();
                dgvRepasatData.DataSource = loadRPSTObject.TipoArt();
                tipoArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        public void cargarDocumentosPago()
        {
            try
            {
                Repasat.csObjetoRepasat rutasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = rutasRPST.cargarObjetoERPToRepasat("DOCSPAG");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("paymentdocuments", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }
        }

        private void crearDocumentosPagoA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosDocumentosPagoRepasat();

            String consulta = "";

            if (dtDatos.Rows.Count >= 1)
            {
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if (fila["RECIBIR"].ToString() == "1")
                    {
                        fila["RECIBIR"] = 'T';
                    }
                    else
                    {
                        fila["RECIBIR"] = 'F';
                    }
                    if (fila["REMESABLE"].ToString() == "1")
                    {
                        fila["REMESABLE"] = 'T';
                    }
                    else
                    {
                        fila["REMESABLE"] = 'F';
                    }
                    if (fila["EFECTIVO"].ToString() == "1")
                    {
                        fila["EFECTIVO"] = 'T';
                    }
                    else
                    {
                        fila["EFECTIVO"] = 'F';
                    }
                    if (fila["CODEXTERNO"] == "" || fila["CODEXTERNO"] == null)
                    {
                        consulta = "insert into DOCUPAGO (ACEPTABLE, DESCDOC, DOCPAG, RECIBIR, REMESABLE, SINCAMBIO, METALICO, RPST_ID_DOCPAGO) values ('F', '" + fila["DESCRIPCION"] + "', '" + fila["CODDOCUPAGO"] + "', '" + fila["RECIBIR"] + "', '" + fila["REMESABLE"] + "', 'F', '" + fila["EFECTIVO"] + "', '" + fila["IDREPASAT"] + "');";
                        
                        csUtilidades.ejecutarConsulta(consulta, false);
                        rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), fila["CODDOCUPAGO"].ToString());

                    }
                }
            } 
        }


        private void crearTransportistasA3()
        {
            Repasat.LoadObjects.csDtObjects rpst = new Repasat.LoadObjects.csDtObjects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            DataTable dtDatos = new DataTable();

            dtDatos = rpst.datosTransportistasRepasat();

            String consulta = "";

            if (dtDatos.Rows.Count >= 1)
            {
                foreach (DataRow fila in dtDatos.Rows)
                {
                    if (fila["CODIGOERP"] == "" || fila["CODIGOERP"] == null)
                    {
                        consulta = "insert into transpor (codtra, nomtra, RPST_ID_TRANSPORT) values ('" + fila["CODTRANSPORT"] + "', '" + fila["DESCRIPCION"] + "', '" + fila["IDREPASAT"] + "');";

                        csUtilidades.ejecutarConsulta(consulta, false);
                    }

                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, fila["IDREPASAT"].ToString(), fila["CODTRANSPORT"].ToString());
                }

            }
        }

        private void cargarCuentasContablesToRPST()
        {
            try
            {
                Repasat.csObjetoRepasat rutasRPST = new Repasat.csObjetoRepasat();
                DataSet dtsERPObjetc = rutasRPST.cargarObjetoERPToRepasat("CUENTASCONTABLES");
                if (dtsERPObjetc != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("accountingaccounts", "POST", dtsERPObjetc.Tables["dtKeys"], dtsERPObjetc.Tables["dtParams"], "");
                }
            }
            catch (InvalidOperationException ex) { }

        }

        private void cargarDireccionesA3ToRepasat(string codCli)
        {

            try
            {
                Repasat.csAddresses addressesRPST = new Repasat.csAddresses();
                DataSet dtsAddresses = addressesRPST.cargarDireccionesA3ToRepasat(codCli);

                csRepasatWebService rpstWS = new csRepasatWebService();
                rpstWS.sincronizarObjetoRepasat("addresses", "POST", dtsAddresses.Tables["dtKeys"], dtsAddresses.Tables["dtParams"], "");

            }
            catch (InvalidOperationException ex) { }
        }



        private void btnDownloadOrders_Click(object sender, EventArgs e)
        {

            DialogResult Resultado = MessageBox.Show("Vas a crear documentos en A3ERP. \n ¿Está seguro?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                csGlobal.docDestino = "Pedido";
                string filtroFechaIni = dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                string filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                cargarPedidos(filtroFechaIni, filtroFechaFin, rbVentas.Checked);
            }
        }


        public DataTable cargarPedidos(string filtroFechaIni, string filtroFechaFin, bool ventas, bool consulta = false, string sincronizado = "TODOS", string idSerieRPST = null, string numDocumento = null)
        {

            tipoDocumento = "Pedido";

            if (csGlobal.modoManual)
            {
                tipoDocumento = cboxTipoDoc.SelectedItem.ToString();
            }
            int errorPagina = 0;
            long errorNumDocumento = 0;
            string errorSerieDoc = "";
            int errorLineaDoc = 0;
            string errorArticulo = "";
            string tipoTercero = ventas ? "CLIENTE" : "PROVEEDOR";
            string estadoSincro = "";

            try
            {
                statusLabel.Text = "";
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                //string filtroFechaURL = "&filter[fecDocumento][]=" + filtroFechaIni + "&filter[fecDocumento][]=" + filtroFechaFin;
                string filtroFechaURL = "&filter_gte[fecDocumento]=" + filtroFechaIni + "&filter_lte[fecDocumento]=" + filtroFechaFin;
                string filtroDocsDownload = consulta ? "" : "&filter[sincronizarDocumento]=1";
                string filtroExtDocNull = "&filter[codExternoDocumento]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroSyncDocs = "&sincronizarDocumento=1";
                string filtroURL = "";
                int numCabeceras = 0;


                //filtro para cargar todos los documentos o los pendientes
                if (sincronizado == "TODOS")
                {
                    filtroExtDocNull = "";
                }
                else if (sincronizado == "NO")
                {
                    filtroExtDocNull = "&filter[codExternoDocumento]=";
                }
                else if (sincronizado == "SI")
                {
                    filtroExtDocNull = "&filter_ne[codExternoDocumento]=";
                }

                filtroURL = filtroDocsDownload + filtroFechaURL + filtroExtDocNull + filtroSyncDocs;


                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtPedidos = new DataTable();
                dtPedidos.Columns.Add("ACCIÓN");
                dtPedidos.Columns.Add("SYNC");
                //dtPedidos.Columns.Add("TIPO_DOC");
                dtPedidos.Columns.Add("SERIE");
                dtPedidos.Columns.Add("NUM_DOC");
                dtPedidos.Columns.Add("FECHA");
                dtPedidos.Columns.Add(tipoTercero);
                dtPedidos.Columns.Add("LINEAS");
                dtPedidos.Columns.Add("BASE_DOC");
                dtPedidos.Columns.Add("IMPORTE_DOC");
                dtPedidos.Columns.Add("REPASAT_ID_DOC");
                dtPedidos.Columns.Add("EXTERNALID");

                double totalDocument = 0;
                string tipoObjeto = "";

                //Primero valido si hay clientes pendientes de traspasar
                //Paso como parámetro pendientes a true, para que mire aquellos clientes pendientes de traspasar

                DataSet dtAccounts = new DataSet();
                DataTable accounts = new DataTable();
                dtAccounts = updateData.traspasarCuentasRepasatToA3(rbVentas.Checked, false, null, false, true, false);


                if (dtAccounts != null)
                {
                    if (updateData.traspasarCuentasRepasatToA3(rbVentas.Checked, false, null, false, false, false).Tables[0].Rows.Count == 0)
                    {
                        accounts = updateData.traspasarCuentasRepasatToA3(rbVentas.Checked, false, null, false, false, false).Tables[0];
                        if (accounts.hasRows())
                        {
                            MessageBox.Show("Se han creado clientes " + accounts.Rows.Count + " , ¿Desea Continuar?");
                        }
                    }
                }


                //Continuo con la carga de facturas
                csSqlConnects sql = new csSqlConnects();
                Objetos.csCabeceraDoc[] cabeceras = null;
                Objetos.csLineaDocumento[] lineas = null;
                Objetos.csTercero[] clientes = null;
                csa3erp a3 = new csa3erp();
                csa3erpTercero tercero = new csa3erpTercero();
                int total_lineas = 0, contador = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    if (tipoDocumento == "Albaranes")
                    {
                        tipoObjeto = ventas ? "saledeliverynotes" : "purchasedeliverynotes";
                    }
                    else
                    {
                        tipoObjeto = ventas ? "saleorders" : "purchaseorders";
                    }

                    // tipoObjeto = ventas ? "saleorders" : "purchaseorders";

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                    if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csSaleOrders.RootObject>(json);


                        cabeceras = new Objetos.csCabeceraDoc[repasat.data.Count];
                        clientes = new Objetos.csTercero[repasat.data.Count];

                        #region Iteracion documentos
                        for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                        {
                            //Reviso el número de líneas de documentos en el total de los nodos
                            //Si un documento tiene 1 linea y otro tiene 4 líneas "total_lineas=5"
                            for (int ii = 0; ii < repasat.data.Count; ii++)
                            {
                                total_lineas += repasat.data[ii].lines.Count;
                            }

                            //inicializo el objeto lineas de documento con el número de lineas totales
                            lineas = new Objetos.csLineaDocumento[total_lineas];

                            /////////////////////////////////////////////////////
                            //////////////////   CABECERAS  ////////////////////
                            ////////////////////////////////////////////////////

                            //Comienzo iteración de las cabeceras
                            for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                            {

                                //Añadimos un try catch para que no se pare si falla solo 1 documento
                                try
                                {
                                    if (!consulta)
                                    {
                                        if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) && repasat.data[ii].codExternoDocumento.ToString() != "0") || repasat.data[ii].codExternoDocumento.ToString() == "X" || repasat.data[ii].account.codExternoCli == "X")
                                            continue;
                                    }

                                    DataRow rowInvoices = dtPedidos.NewRow();

                                    //Valido que el documento no esté bajado o que no esté marcado para on bajar
                                    //También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                                    //Cuando se borra un codExternoDocumento, el campo se pone a 0
                                    // if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) && repasat.data[ii].codExternoDocumento.ToString() != "0") || repasat.data[ii].codExternoDocumento.ToString() == "X" || repasat.data[ii].account.codExternoCli == "X")
                                    //     continue;
                                    cabeceras[numCabeceras] = new Objetos.csCabeceraDoc();

                                    cabeceras[numCabeceras].extNumdDoc = repasat.data[ii].idDocumento.ToString();
                                    cabeceras[numCabeceras].serieDoc = repasat.data[ii].series.nomSerie.ToString();
                                    cabeceras[numCabeceras].numDocA3 = repasat.data[ii].numSerieDocumento.ToString();
                                    cabeceras[numCabeceras].codIC = repasat.data[ii].account.codExternoCli;
                                    cabeceras[numCabeceras].referencia = repasat.data[ii].refDocumento;
                                    cabeceras[numCabeceras].fechaDoc = Convert.ToDateTime(repasat.data[ii].fecDocumento);
                                    cabeceras[numCabeceras].nombreIC = repasat.data[ii].account.nomCli;
                                    cabeceras[numCabeceras].nif = repasat.data[ii].account.cifCli;

                                    cabeceras[numCabeceras].regIva = string.IsNullOrEmpty(repasat.data[ii].taxoperation.codExternoRegimenesImpuestos.ToString()) ? "" : repasat.data[ii].taxoperation.codExternoRegimenesImpuestos.ToString();

                                    //gestiono errores
                                    errorSerieDoc = repasat.data[ii].series.nomSerie.ToString();
                                    errorNumDocumento = Convert.ToInt64(repasat.data[ii].numSerieDocumento);

                                    if (!string.IsNullOrEmpty(repasat.data[ii].idTrabajador.ToString()))
                                    {
                                        cabeceras[numCabeceras].agenteComercial = repasat.data[ii].employee.codExternoTrabajador.ToString();
                                    }

                                    if (!string.IsNullOrEmpty(repasat.data[ii].idTransportista.ToString()))
                                    {
                                        cabeceras[numCabeceras].transportista = repasat.data[ii].carrier.codExternoTransportista.ToString();
                                    }

                                    //Comentarios Documento

                                    if (!string.IsNullOrEmpty(repasat.data[ii].observacionesCabeceraDocumento))
                                    {
                                        string texto = repasat.data[ii].observacionesCabeceraDocumento;
                                        texto = ScrubHtml(texto);
                                        texto = HttpUtility.HtmlDecode(texto);

                                        cabeceras[numCabeceras].comentariosCabecera = texto;
                                    }

                                    if (!string.IsNullOrEmpty(repasat.data[ii].observacionesPieDocumento))
                                    {
                                        string texto = repasat.data[ii].observacionesPieDocumento;
                                        texto = ScrubHtml(texto);
                                        texto = HttpUtility.HtmlDecode(texto);
                                        cabeceras[numCabeceras].comentariosPie = texto;
                                    }

                                    //Información de Pago
                                    cabeceras[numCabeceras].codFormaPago = repasat.data[ii].payment_method.codExternoFormaPago.ToString();
                                    cabeceras[numCabeceras].codDocumentoPago = repasat.data[ii].payment_document.codExternoDocuPago.ToString();

                                    if (!consulta)
                                    {
                                        //Dirección de Facturación
                                        if (repasat.data[ii].billing_address != null)
                                        {
                                            cabeceras[numCabeceras].idDireccionFacturacionA3ERP = repasat.data[ii].billing_address.codExternoDireccion.ToString();
                                            cabeceras[numCabeceras].direccionDirFacturacion = repasat.data[ii].billing_address.direccion1Direccion.ToString();
                                            cabeceras[numCabeceras].codigoPostalDirFacturacion = repasat.data[ii].billing_address.cpDireccion.ToString();
                                            if (repasat.data[ii].billing_address.poblacionDireccion != null && !string.IsNullOrEmpty(repasat.data[ii].billing_address.poblacionDireccion))
                                            {
                                                cabeceras[numCabeceras].poblacionDirFacturacion = repasat.data[ii].billing_address.poblacionDireccion.ToString();
                                            }
                                            cabeceras[numCabeceras].provinciaDirFacturacion = rpstUtil.obtenerProvincia(repasat.data[ii].billing_address.idProvincia.ToString(), repasat.data[ii].billing_address.idPais.ToString().Substring(0, 2));
                                        }

                                        //Dirección de Envío
                                        if (!string.IsNullOrEmpty(repasat.data[ii].idDireccionEnvio.ToString()))
                                        {
                                            cabeceras[numCabeceras].idDireccionEnvioA3ERP = repasat.data[ii].delivery_address.codExternoDireccion.ToString();
                                            cabeceras[numCabeceras].direccionDirEnvio = repasat.data[ii].delivery_address.direccion1Direccion.ToString();
                                            cabeceras[numCabeceras].codigoPostalDirEnvio = repasat.data[ii].delivery_address.cpDireccion.ToString();
                                            if (repasat.data[ii].delivery_address.poblacionDireccion != null && !string.IsNullOrEmpty(repasat.data[ii].delivery_address.poblacionDireccion))
                                            {
                                                cabeceras[numCabeceras].poblacionDirEnvio = repasat.data[ii].delivery_address.poblacionDireccion.ToString();
                                            }
                                            
                                            cabeceras[numCabeceras].provinciaDirFacturacion = rpstUtil.obtenerProvincia(repasat.data[ii].delivery_address.idProvincia.ToString(), repasat.data[ii].delivery_address.idPais.ToString().Substring(0, 2));
                                        }
                                    }

                                    cabeceras[numCabeceras].numSerieDocumento = repasat.data[ii].numSerieDocumento.ToString();

                                    /* estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) ? " pendiente de traspaso " : " traspasado ";
                                     rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].numSerieDocumento + estadoSincro;
                                     rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie;
                                     rowInvoices["NUM_FRA"] = repasat.data[ii].numSerieDocumento;
                                     rowInvoices["FECHA"] = (DateTime.Parse(repasat.data[ii].fecDocumento)).ToString("dd/MM/yyyy");
                                     rowInvoices["CLIENTE"] = repasat.data[ii].account.nomCli.ToUpper() + " (" + cabeceras[numCabeceras].codIC + ")";
                                     rowInvoices["LINEAS"] = repasat.data[ii].lines.Count;
                                     rowInvoices["BASE_DOC"] = repasat.data[ii].totalBaseImponibleDocumento.ToString().Replace('.', ',');
                                     rowInvoices["IMPORTE_DOC"] = repasat.data[ii].totalDocumento.ToString().Replace('.', ',');
                                     rowInvoices["EXTERNALID"] = repasat.data[ii].codExternoDocumento.ToString();
                                     rowInvoices["REPASAT_ID_DOC"] = repasat.data[ii].idDocumento;

                                     totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].totalDocumento.ToString().Replace('.', ','));
                                     */

                                    //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                                    estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) ? " pendiente de traspaso " : " traspasado ";
                                    rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].numSerieDocumento + estadoSincro;
                                    rowInvoices["SYNC"] = (repasat.data[ii].sincronizarDocumento == null || repasat.data[ii].sincronizarDocumento == true) ? "SI" : "NO";
                                    rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie;
                                    rowInvoices["NUM_DOC"] = repasat.data[ii].numSerieDocumento;
                                    rowInvoices["FECHA"] = (DateTime.Parse(repasat.data[ii].fecDocumento)).ToString("dd/MM/yyyy");
                                    rowInvoices[tipoTercero] = repasat.data[ii].account.nomCli.ToUpper() + " (" + cabeceras[numCabeceras].codIC + ")"; ;
                                    rowInvoices["LINEAS"] = repasat.data[ii].lines.Count;
                                    rowInvoices["BASE_DOC"] = repasat.data[ii].totalBaseImponibleDocumento.ToString().Replace('.', ',');
                                    rowInvoices["IMPORTE_DOC"] = repasat.data[ii].totalDocumento.ToString().Replace('.', ',');
                                    rowInvoices["REPASAT_ID_DOC"] = repasat.data[ii].idDocumento;
                                    rowInvoices["EXTERNALID"] = repasat.data[ii].codExternoDocumento.ToString();

                                    totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].totalDocumento.ToString().Replace('.', ','));

                                    dtPedidos.Rows.Add(rowInvoices);

                                    //Iteramos las lineas únicamente si no estamos haciendo la consulta
                                    if (!consulta)
                                    {

                                        // Si no tiene cliente ponemos a null esa factura
                                        if (cabeceras[numCabeceras].codIC == null)
                                        {
                                            //Crear el Cliente
                                            cabeceras[numCabeceras] = null;
                                            lineas[ii] = null;
                                            clientes[ii] = null;
                                        }
                                        else
                                        {
                                            /////////////////////////////////////////////////////
                                            //////////////////    LINEAS    ////////////////////
                                            ////////////////////////////////////////////////////
                                            #region doclines
                                            for (int iii = 0; iii < repasat.data[ii].lines.Count; iii++) // lineas
                                            {
                                                lineas[contador] = new Objetos.csLineaDocumento();
                                                lineas[contador].nombreArticulo = repasat.data[ii].lines[iii].nomArticuloLineaDocumento;
                                                lineas[contador].cantidad = double.Parse(repasat.data[ii].lines[iii].unidadesArticuloLineaDocumento, CultureInfo.InvariantCulture).ToString().Replace(",", ".");
                                                lineas[contador].price = double.Parse(repasat.data[ii].lines[iii].precioVentaArticuloLineaDocumento, CultureInfo.InvariantCulture);
                                                lineas[contador].idArticulo = repasat.data[ii].lines[iii].idArticulo.ToString();
                                                lineas[contador].numCabecera = repasat.data[ii].idDocumento.ToString();
                                                lineas[contador].tipoIVA = rpstUtil.obtenerTipoImpuesto(repasat.data[ii].lines[iii].idTipoImpuesto.ToString());
                                                lineas[contador].descuento = double.Parse(repasat.data[ii].lines[iii].descuentoLineaDocumento, CultureInfo.InvariantCulture).ToString();

                                                /////////////////////////////////////////////////////
                                                //////////////////  ARTICULOS   ////////////////////
                                                ////////////////////////////////////////////////////



                                                if (repasat.data[ii].lines[iii].refArticuloLineaDocumento != "") //Verificamos que la referencia esté informada
                                                {
                                                    if (existProduct(repasat.data[ii].lines[iii].refArticuloLineaDocumento))
                                                    {
                                                        lineas[contador].codigoArticulo = repasat.data[ii].lines[iii].refArticuloLineaDocumento;
                                                    }
                                                    else // Si no, lo creamos
                                                    {
                                                        csa3erpItm item = new csa3erpItm();

                                                        a3.abrirEnlace();
                                                        lineas[contador].codigoArticulo = item.crearArticuloA3Repasat(lineas[contador].nombreArticulo,
                                                            "",
                                                            lineas[contador].price.ToString(),
                                                            //lineas[contador].codigoArticulo,  corregido ya que era erroneo 17/11/2019
                                                            repasat.data[ii].lines[iii].refArticuloLineaDocumento,
                                                            repasat.data[ii].lines[iii].idArticulo.ToString()).Trim();
                                                        a3.cerrarEnlace();
                                                    }
                                                }
                                                else //Si la referencia no está informada le asignamos el artículo 0 de A3ERP
                                                {
                                                    lineas[contador].codigoArticulo = "0";  //Si por casualidad no han puesto una referencia en el artículo le asignamos el código 0
                                                }

                                                //Si está desmarcada la opción, traspasamos el nombre de la linea al documento
                                                if (!cboxDescripcionArticulosA3.Checked)
                                                {
                                                    lineas[contador].descripcionArticulo = repasat.data[ii].lines[iii].nomArticuloLineaDocumento.ToString();
                                                }
                                                contador++;
                                            }
                                            #endregion doclines
                                            numCabeceras++;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                                    "Se ha encontrado un error en el siguiente número de documento de Repasat \n" +
                                    "Serie: " + errorSerieDoc.ToString() + "\n" +
                                    "Documento: " + errorNumDocumento.ToString() + "\n" +
                                    "por favor solucionar para poder traspasar documento";

                                    csUtilidades.log(ex.Message);
                                }
                            }
                            #endregion

                            check = false;

                            for (int iiii = 0; iiii < cabeceras.Length; iiii++)
                            {
                                if (cabeceras[iiii] != null)
                                    check = true;
                            }





                            //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                            //json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                            json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                            if (i + 1 <= repasat.last_page)
                            {
                                repasat = JsonConvert.DeserializeObject<Repasat.csSaleOrders.RootObject>(json);

                                //Inicializo total_lineas para que cuente las nuevas lineas
                                total_lineas = 0;
                                //Redimensiono el Array de Lineas
                                for (int ii = 0; ii < repasat.data.Count; ii++)
                                {
                                    total_lineas += repasat.data[ii].lines.Count;
                                }
                                //Redimensiono el Array de Cabeceras
                                Array.Resize(ref cabeceras, cabeceras.Length + repasat.data.Count);
                                //Redimensiono el Array de Lineas, hay que coger el tamaño actual y añadirle las nuevas lineas
                                Array.Resize(ref lineas, lineas.Length + total_lineas);
                                Array.Resize(ref clientes, clientes.Length + repasat.data.Count);
                            }

                            //contador = 0;






                        }

                        if (consulta)
                        {
                            DataRow rowInvoices = dtPedidos.NewRow();
                            rowInvoices["ACCIÓN"] = "Total Documentos: ";
                            rowInvoices["IMPORTE_DOC"] = totalDocument.ToString();
                            dtPedidos.Rows.Add(rowInvoices);
                            return dtPedidos;
                        }
                        else
                        {
                            DialogResult Resultado;
                            if (csGlobal.modoManual)
                            {
                                Resultado = MessageBox.Show("¿Quiere traspasar " + cabeceras.Length + " documentos a A3ERP?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            else
                            {
                                Resultado = DialogResult.Yes;
                            }
                            if (Resultado == DialogResult.Yes)
                            {
                                a3.abrirEnlace();
                                a3.generarDocA3ObjetoRepasat(cabeceras, lineas, rbCompras.Checked, "NO", clientes, null);
                                a3.cerrarEnlace();
                                if (csGlobal.modoManual)
                                    MessageBox.Show("Los documentos se han importado con éxito");

                            }

                        }




                        return null;

                    }
                    else
                    {
                        if (csGlobal.modoManual)
                        {
                            "No hay documentos para descargar entre los periodos seleccionados".mb();
                        }
                        return null;
                    }
                }
                statusLabel.Text = "proceso finalizado";
            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                   "Se ha encontrado un error en el siguiente número de documento de Repasat \n" +
                   "Serie: " + errorSerieDoc.ToString() + "\n" +
                   "Documento: " + errorNumDocumento.ToString() + "\n" +
                   "por favor solucionar para poder traspasar documento";


                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;

            }
        }

        public DataTable cargarFacturasFromRPSToA3(string filtroFechaIni, string filtroFechaFin, bool ventas, bool consulta = false, string sincronizado = "TODOS", string idSerieRPST = null, string numDocumento = null)
        {
            tipoDocumento = "Factura";
            int errorPagina = 0;
            long errorNumDocumento = 0;
            string errorSerieDoc = "";
            string errorIdDocRPST = "";
            string stepErrorLog = "";
            string numdocQR = "";
            string queryCheckAccount = "";
            bool errorcargaWebService = true;
            bool errorCrearDocsA3 = false;

            int errorLineaDoc = 0;
            string errorArticulo = "";
            int documentosGenerados = 0;
            bool docCompras = ventas ? false : true;

            bool generarMovsContablesYCartera = cboxGenerarMovsContables.Checked ? true : false;

            // 29-3-2020 parche para incorporar clientes
            DataTable dtCuentas = new DataTable();
            dtCuentas = sql.obtenerDatosSQLScript("select NOMFISCAL, NOMCLI,NIFCLI, CODCLI,RPST_ID_CLI from clientes where (RPST_ID_CLI) IS NOT NULL");

            try
            {
                stepErrorLog = "step 1";

                int numCabeceras = 0;
                int numLineas = 0;
                double totalDocument = 0;
                string tipoObjeto = "";

                string filtroFechaURL = "&filter_gte[fecDocumento]=" + filtroFechaIni + "&filter_lte[fecDocumento]=" + filtroFechaFin;
                string filtroDocsDownload = consulta ? "" : "&filter[sincronizarDocumento]=1";

                string filtroExtDocNull = "&filter[codExternoDocumento]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroSyncDocs = "&sincronizarDocumento=1";
                string filtroSerie = "";
                string codArt = "";
                string estadoSincro = "";
                DataTable accounts = new DataTable();

                //Filtro Serie
                string idSerie = "";
                idSerie = cboxSeries.SelectedIndex == -1 ? idSerie = "0" : cboxSeries.SelectedValue.ToString();
                filtroSerie = cboxSeries.SelectedValue != null && !string.IsNullOrEmpty(cboxSeries.SelectedValue.ToString()) ? "&filter[idSerie]=" + idSerie : filtroSerie;
                
                //filtro para cargar todos los documentos o los pendientes
                filtroExtDocNull = sincronizado == "TODOS" ?  "" : filtroExtDocNull;
                filtroExtDocNull = sincronizado == "NO" ? "&filter[codExternoDocumento]=" : filtroExtDocNull;
                filtroExtDocNull = sincronizado == "SI" ? "&filter_ne[codExternoDocumento]=" : filtroExtDocNull;
                
                //filtro por número de documento
                string filtroNumSerieDoc = "";
                filtroNumSerieDoc = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? "" : "&filter[idSerie] =" + idSerieRPST + "&filter[numSerieDocumento]=" + numDocumento;
                filtroExtDocNull = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? filtroExtDocNull : "";

                string filtroURL = filtroDocsDownload + filtroFechaURL + filtroExtDocNull + filtroSerie + filtroNumSerieDoc + filtroSyncDocs;

                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtInvoices = new DataTable();
                dtInvoices.Columns.Add("ACCIÓN");
                dtInvoices.Columns.Add("SYNC");
                dtInvoices.Columns.Add("TIPO_DOC");
                dtInvoices.Columns.Add("SERIE");
                dtInvoices.Columns.Add("SERIE_RECT");
                dtInvoices.Columns.Add("NUM_FRA"); 
                if (csGlobal.ticketBaiActivo && ventas) dtInvoices.Columns.Add("TICKETBAI");
                if (csGlobal.ticketBaiActivo && ventas) dtInvoices.Columns.Add("SERIE_QR"); 
                if (csGlobal.ticketBaiActivo && ventas) dtInvoices.Columns.Add("NUMFRA_QR");
                dtInvoices.Columns.Add("FECHA");
                dtInvoices.Columns.Add("CLIENTE");
                dtInvoices.Columns.Add("LINEAS");
                dtInvoices.Columns.Add("BASE_DOC");
                dtInvoices.Columns.Add("IMPORTE_DOC");
                dtInvoices.Columns.Add("EXTERNALID");
                dtInvoices.Columns.Add("REPASAT_ID_DOC");

                DataTable dtCuentasContables = new DataTable();
                dtCuentasContables.Columns.Add("CUENTA");
                dtCuentasContables.Columns.Add("IMPORTE");

                //Primero valido si hay clientes pendientes de traspasar

                if (!consulta)
                {
                    if (cboxCheckCuentas.Checked)
                    {
                        accounts = cargarDatosCuentasRepasat(ventas, false, null, true, true, false);
                        //Valido si se han creado clientes nuevos
                        if (csGlobal.modoManual && accounts.hasRows())
                        {
                            MessageBox.Show("Se han creado clientes, ¿Desea Continuar?");
                        }

                        //Actualizo artículos pendientes de traspasar a A3ERP

                        //updateDataRPST.traspasarArticulosRepasatToA3(consulta, null, true, false, false, true, null, null);
                        //revisar esta linea, ya que los para,metros que se le pasan son de test
                        cargarDatosArticulos(dgvAccountsRepasat, true, "", true, true);
                    }
                }


                stepErrorLog = "step 2";

                //Continuo con la carga de facturas
                csSqlConnects sql = new csSqlConnects();
                Objetos.csCabeceraDoc[] cabeceras = null;
                Objetos.csLineaDocumento[] lineas = null;
                Objetos.csTercero[] clientes = null;
                stepErrorLog = "step 2.0";
                csa3erp a3 = new csa3erp();
                stepErrorLog = "step 2.1";
                csa3erpTercero tercero = new csa3erpTercero();
                stepErrorLog = "step 2.2";
                int total_lineas = 0, contador = 0;
                stepErrorLog = "step 2.3";
                //filtroURL =  filtroFechaURL + filtroExtDocNull + filtroSerie + filtroNumSerieDoc + filtroSyncDocs;
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    tipoObjeto = ventas ? "saleinvoices" : "purchaseinvoices";

                    stepErrorLog = "step 3";
                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    //var json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + 1, "GET", tipoObjeto, "", "");

                    if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                    {
                        stepErrorLog = "step 4";

                        var repasat = JsonConvert.DeserializeObject<Repasat.csSaleInvoice.RootObject>(json);

                        if (consulta == false)
                        {
                            stepErrorLog = "step 4.1:PRUEBA ACTU CLI";
                            foreach (var factura in repasat.data)
                            {
                                if (factura.account != null)
                                {
                                    string clienteJson = JsonConvert.SerializeObject(factura.account);
                                    var csLauClientes = new csLAUClientes(updateDataRPST);
                                    csLauClientes.SincronizarClienteDesdeRepasatA3ERP(clienteJson);
                                }
                            }
                        }

                        cabeceras = new Objetos.csCabeceraDoc[repasat.data.Count];
                        clientes = new Objetos.csTercero[repasat.data.Count];

                        stepErrorLog = "step 5";

                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            total_lineas += repasat.data[ii].lines.Count;
                        }

                        //inicializo el objeto lineas de documento con el número de lineas totales
                        //Posteriormente redimensionaré el objeto
                        lineas = new Objetos.csLineaDocumento[total_lineas];

                        stepErrorLog = "step 6";
                        for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                        {
                            errorPagina = i;
                            #region kwebfkjwebf

                            //Reviso el número de líneas de documentos en el total de los nodos
                            //Si un documento tiene 1 linea y otro tiene 4 líneas "total_lineas=5"


                            /////////////////////////////////////////////////////
                            //////////////////   CABECERAS  ////////////////////
                            ////////////////////////////////////////////////////

                            //Comienzo iteración de las cabeceras
                            for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                            {
                                DataRow rowInvoices = dtInvoices.NewRow();
                                stepErrorLog = "step 7";

                                //Valido que el documento no esté bajado o que no esté marcado para on bajar
                                //También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)

                                if (!consulta)
                                {
                                    if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) && repasat.data[ii].codExternoDocumento.ToString() != "0") || repasat.data[ii].codExternoDocumento.ToString() == "X" || repasat.data[ii].account.codExternoCli == "X")
                                        continue;
                                }

                                cabeceras[numCabeceras] = new Objetos.csCabeceraDoc();

                                try
                                {
                                    cabeceras[numCabeceras].extNumdDoc = repasat.data[ii].idDocumento.ToString();
                                    cabeceras[numCabeceras].numDocA3 = repasat.data[ii].numSerieDocumento == null ? "NUMERO NO VALIDO" : repasat.data[ii].numSerieDocumento.ToString();
                                    cabeceras[numCabeceras].serieDoc = repasat.data[ii].series == null ? "SERIE NO INFORMADA" : (string.IsNullOrEmpty(repasat.data[ii].series.codExternoSerie) ? "SERIE NO VINCULADA" : repasat.data[ii].series.codExternoSerie.ToString());
                                }
                                catch (Exception errorException) { }


                                //gestiono errores
                                errorIdDocRPST = repasat.data[ii].idDocumento.ToString();
                                errorNumDocumento = repasat.data[ii].numSerieDocumento == null ? 0 : Convert.ToInt64(repasat.data[ii].numSerieDocumento.ToString());
                                errorSerieDoc = repasat.data[ii].series == null ? "SERIE NO INFORMADA" : repasat.data[ii].series.nomSerie.ToString();
                                //fin gestion errores

                                //GENERAR MOVIMIENTOS CONTABLES Y CARTERA
                                cabeceras[numCabeceras].repercusionesContables = generarMovsContablesYCartera ? true : false;

                                //Reviso si el cliente está creado o no

                                if (!string.IsNullOrEmpty(repasat.data[ii].account.codExternoCli))
                                {
                                    cabeceras[numCabeceras].codIC = repasat.data[ii].account.codExternoCli;
                                }
                                else
                                {
                                    //Solo doy de alta la cuenta si estamos sincronizando
                                    if (!consulta)
                                    {
                                        //Verifico primero si no lo he dado previamente en el mismo procedimiento, 
                                        //si no lo doy de alta

                                        queryCheckAccount = ventas ? "SELECT LTRIM(CODCLI) AS CODCLI, RPST_ID_CLI FROM __CLIENTES WHERE RPST_ID_CLI=" : "SELECT LTRIM(CODPRO) AS CODPRO, RPST_ID_PROV FROM __PROVEED WHERE RPST_ID_PROV=";
                                        queryCheckAccount = queryCheckAccount + repasat.data[ii].account.idCli.ToString();
                                        if (string.IsNullOrEmpty(sql.obtenerCampoTabla(queryCheckAccount)))
                                        {
                                            cargarDatosCuentasRepasat(ventas, false, repasat.data[ii].account.idCli.ToString(), true, true, false);
                                            cabeceras[numCabeceras].codIC = sql.obtenerCampoTabla(queryCheckAccount);
                                        }
                                        else
                                        {
                                            cabeceras[numCabeceras].codIC = sql.obtenerCampoTabla(queryCheckAccount);
                                        }
                                    }

                                }

                                //17-03-2021 SERGI Opcion para poder usar el campo documento proveedor como referencia en a3ERP
                                cabeceras[numCabeceras].referencia = (csGlobal.usarDocProRefA3 == true) ? repasat.data[ii].documentoProveedorDocumento : repasat.data[ii].refDocumento;
                                if (!string.IsNullOrEmpty(repasat.data[ii].documentoProveedorDocumento))
                                {
                                    cabeceras[numCabeceras].numeroDocProveedor = repasat.data[ii].documentoProveedorDocumento.ToString();
                                }
                                cabeceras[numCabeceras].fechaDoc = Convert.ToDateTime(repasat.data[ii].fecDocumento);
                                cabeceras[numCabeceras].fechaContableDoc = (repasat.data[ii].fecContableDocumento == null) ? Convert.ToDateTime(repasat.data[ii].fecDocumento) : Convert.ToDateTime(repasat.data[ii].fecContableDocumento);
                                cabeceras[numCabeceras].fechaContableDoc = csGlobal.conexionDB.Contains("DRISSA") ? cabeceras[numCabeceras].fechaDoc : cabeceras[numCabeceras].fechaContableDoc;
                                cabeceras[numCabeceras].nombreIC = repasat.data[ii].account.nomCli;
                                cabeceras[numCabeceras].nif = repasat.data[ii].account.cifCli;
                                cabeceras[numCabeceras].tipoDoc = repasat.data[ii].tipoDocumento.ToString();
                                cabeceras[numCabeceras].clienteGenerico = repasat.data[ii].clienteGenerico;
                                cabeceras[numCabeceras].direccionGenerica = repasat.data[ii].direccionGenerica;

                                //INFORMACIÓN SOBRE TICKET BAI EN EL TRASPASO DE FACTURAS
                                cabeceras[numCabeceras].ticketBaiActivo = csGlobal.ticketBaiActivo;
                                cabeceras[numCabeceras].ticketBaiFraBorrador = rbTraspasarComoBorradorSi.Checked;
                                if (csGlobal.ticketBaiActivo && repasat.data[ii].idTicketBai != null && ventas)
                                {
                                    cabeceras[numCabeceras].serieDoc = repasat.data[ii].sale_ticket_bai.series != null ? repasat.data[ii].sale_ticket_bai.series.codExternoSerie : string.Empty;
                                    cabeceras[numCabeceras].serieDocTBAI = repasat.data[ii].sale_ticket_bai.series != null ? repasat.data[ii].series.codExternoSerie : string.Empty;
                                }else if (csGlobal.ticketBaiActivo && !ventas && repasat.data[ii].tipoDocumento == "CREDIT_NOTE")
                                {
                                    cabeceras[numCabeceras].serieDoc = string.IsNullOrEmpty(repasat.data[ii].series.refSerie) ? repasat.data[ii].series.nomSerie : repasat.data[ii].series.refSerie;
                                }

                                if (repasat.data[ii].idRegimenImpuesto != null) {
                                    cabeceras[numCabeceras].regIva = rpstUtil.obtenerRegIVA(repasat.data[ii].idRegimenImpuesto.ToString());
                                }

                                if (!string.IsNullOrEmpty(repasat.data[ii].idTrabajador.ToString()))
                                {
                                    cabeceras[numCabeceras].agenteComercial = repasat.data[ii].employee.codExternoTrabajador.ToString();
                                }

                                if (!string.IsNullOrEmpty(repasat.data[ii].idTransportista.ToString()))
                                {
                                    //Pendiente de solucionar
                                    // cabeceras[numCabeceras].transportista = repasat.data[ii].carrier.codExternoTransportista.ToString();
                                }

                                // Portes Documento
                                cabeceras[numCabeceras].portes = (repasat.data[ii].portesDocumento == 0) ? "0" : repasat.data[ii].portesDocumento.ToString().Replace(".", ",");

                                //Comentarios Documento

                                if (!string.IsNullOrEmpty(repasat.data[ii].observacionesCabeceraDocumento))
                                {
                                    string texto = repasat.data[ii].observacionesCabeceraDocumento;
                                    texto = ScrubHtml(texto);
                                    texto = HttpUtility.HtmlDecode(texto);

                                    cabeceras[numCabeceras].comentariosCabecera = texto;
                                }

                                if (!string.IsNullOrEmpty(repasat.data[ii].observacionesPieDocumento))
                                {
                                    string texto = repasat.data[ii].observacionesPieDocumento;
                                    texto = ScrubHtml(texto);
                                    texto = HttpUtility.HtmlDecode(texto);
                                    cabeceras[numCabeceras].comentariosPie = texto;
                                }

                                //Si ha retencion lo informo
                                if (repasat.data[ii].idTipoRetencion != null)
                                {
                                    cabeceras[numCabeceras].fraConRetencion = true;
                                    cabeceras[numCabeceras].porcentajeRetencion = repasat.data[ii].retentiontype.impuestosTipoRetencion.ToString();
                                    cabeceras[numCabeceras].tipoRentencion = repasat.data[ii].retentiontype.codTipoRetencion.ToString();
                                    cabeceras[numCabeceras].baseRetencion = repasat.data[ii].totalBaseRetencionesDocumento.ToString();
                                }

                                //Información de Pago
                                cabeceras[numCabeceras].codFormaPago = repasat.data[ii].payment_method.codExternoFormaPago.ToString();
                                cabeceras[numCabeceras].codDocumentoPago = repasat.data[ii].payment_document.codExternoDocuPago.ToString();


                                //Dirección de Facturación

                                if (repasat.data[ii].billing_address == null && repasat.data[ii].clienteGenerico == 0)
                                {
                                    rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].numSerieDocumento + " CON INDICENCIAS ****";
                                    rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie;
                                    rowInvoices["NUM_FRA"] = repasat.data[ii].numSerieDocumento;
                                    rowInvoices["FECHA"] = repasat.data[ii].fecDocumento;
                                    rowInvoices["CLIENTE"] = repasat.data[ii].account.nomCli.ToUpper() + " (" + cabeceras[numCabeceras].codIC + ")";
                                    rowInvoices["REPASAT_ID_DOC"] = repasat.data[ii].idDocumento;

                                    dtInvoices.Rows.Add(rowInvoices);
                                    
                                    if (!csGlobal.modoManual)
                                    {
                                        Program.guardarErrorFichero("Fra: " + rowInvoices["SERIE"] + "/" + rowInvoices["NUM_FRA"] + repasat.data[ii].numSerieDocumento + "\n" +
                                            " Cuenta " + rowInvoices["CLIENTE"] + " sin dirección fiscal");
                                    }
                                    continue;
                                }

                                //Gestión de dirección Fiscal 
                                if (repasat.data[ii].clienteGenerico == 0)
                                {
                                    cabeceras[numCabeceras].idDireccionFacturacionA3ERP = string.IsNullOrEmpty(repasat.data[ii].billing_address.codExternoDireccion) ? "" : repasat.data[ii].billing_address.codExternoDireccion.ToString();
                                    cabeceras[numCabeceras].direccionDirFacturacion = repasat.data[ii].billing_address.direccion1Direccion.ToString();
                                    cabeceras[numCabeceras].codigoPostalDirFacturacion = string.IsNullOrEmpty(repasat.data[ii].billing_address.cpDireccion) ? "" : repasat.data[ii].billing_address.cpDireccion.ToString();
                                    cabeceras[numCabeceras].poblacionDirFacturacion = string.IsNullOrEmpty(repasat.data[ii].billing_address.poblacionDireccion) ? "" : repasat.data[ii].billing_address.poblacionDireccion.ToString();
                                    cabeceras[numCabeceras].provinciaDirFacturacion = rpstUtil.obtenerProvincia(repasat.data[ii].billing_address.idProvincia.ToString(), string.IsNullOrEmpty(repasat.data[ii].billing_address.idPais) ? "ES" : repasat.data[ii].billing_address.idPais.ToString().Substring(0, 2), repasat.data[ii].billing_address.nomDireccion.ToString());
                                }
                                else 
                                {
                                    cabeceras[numCabeceras].idDireccionFacturacionA3ERP = "";
                                    cabeceras[numCabeceras].nif = string.IsNullOrEmpty(repasat.data[ii].cifCli) ? "" : repasat.data[ii].cifCli.ToString();
                                    cabeceras[numCabeceras].direccionDirFacturacion = repasat.data[ii].direccion1Cli.ToString();
                                    cabeceras[numCabeceras].codigoPostalDirFacturacion = string.IsNullOrEmpty(repasat.data[ii].cpCli) ? "" : repasat.data[ii].cpCli.ToString();
                                    cabeceras[numCabeceras].poblacionDirFacturacion = string.IsNullOrEmpty(repasat.data[ii].poblacionCli) ? "" : repasat.data[ii].poblacionCli.ToString();
                                    cabeceras[numCabeceras].provinciaDirFacturacion = rpstUtil.obtenerProvincia(repasat.data[ii].idProvinciaCli.ToString(), string.IsNullOrEmpty(repasat.data[ii].idPaisCli) ? "ES" : repasat.data[ii].idPaisCli.ToString().Substring(0, 2));
                                    cabeceras[numCabeceras].pais = rpstUtil.obtenerPais(repasat.data[ii].idPaisCli.ToString());

                                }

                                //Dirección de Envío
                                if (!string.IsNullOrEmpty(repasat.data[ii].idDireccionEnvio.ToString()))
                                {
                                    cabeceras[numCabeceras].idDireccionEnvioA3ERP = repasat.data[ii].delivery_address.codExternoDireccion.ToString();
                                    cabeceras[numCabeceras].direccionDirEnvio = repasat.data[ii].delivery_address.direccion1Direccion.ToString();
                                    cabeceras[numCabeceras].codigoPostalDirEnvio = string.IsNullOrEmpty(repasat.data[ii].delivery_address.cpDireccion) ? "" : repasat.data[ii].delivery_address.cpDireccion.ToString();
                                    cabeceras[numCabeceras].poblacionDirEnvio = string.IsNullOrEmpty(repasat.data[ii].delivery_address.poblacionDireccion) ? "" : repasat.data[ii].delivery_address.poblacionDireccion.ToString();
                                    cabeceras[numCabeceras].provinciaDirFacturacion = rpstUtil.obtenerProvincia(repasat.data[ii].delivery_address.idProvincia.ToString(), string.IsNullOrEmpty(repasat.data[ii].delivery_address.idPais) ? "ES" : repasat.data[ii].delivery_address.idPais.ToString().Substring(0, 2));
                                }

                                cabeceras[numCabeceras].tipoDoc = repasat.data[ii].tipoDocumento == "CREDIT_NOTE" ? "CREDIT_NOTE" : "INVOICE";
                                rowInvoices["TIPO_DOC"] = repasat.data[ii].tipoDocumento == "CREDIT_NOTE" ? "CREDIT_NOTE" : "INVOICE";

                                //GESTIÓN DE SERIES DE DOCUMENTOS
                                
                                
                                
                                if (repasat.data[ii].tipoDocumento == "CREDIT_NOTE")
                                {
                                    if (!csGlobal.ticketBaiActivo)
                                    {
                                        cabeceras[numCabeceras].serieDoc = string.IsNullOrEmpty(repasat.data[ii].series.refSerie) ? repasat.data[ii].series.nomSerie : repasat.data[ii].series.refSerie;
                                    }
                                    cabeceras[numCabeceras].idDocumentoAbonado = string.IsNullOrEmpty( repasat.data[ii].idDocumentoAbonado)? string.Empty : repasat.data[ii].idDocumentoAbonado;
                                }
                                else
                                {
                                    //Editado lvg 17-3-21
                                    if (!csGlobal.ticketBaiActivo)
                                    {
                                        cabeceras[numCabeceras].serieDoc = repasat.data[ii].series == null ? "SERIE NO INFORMADA" : (string.IsNullOrEmpty(repasat.data[ii].series.codExternoSerie) ? "SERIE NO VINCULADA" : repasat.data[ii].series.codExternoSerie.ToString());
                                    }
                                }
                                
                                //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                                estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) ? " pendiente de traspaso " : " traspasado ";
                                rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].numSerieDocumento + estadoSincro;
                                rowInvoices["SYNC"] = (repasat.data[ii].sincronizarDocumento == null || repasat.data[ii].sincronizarDocumento == true) ? "SI" : "NO";

                                // rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie + " (" + (string.IsNullOrEmpty(repasat.data[ii].series.codExternoSerie) ? "SERIE NO VINCULADA" : repasat.data[ii].series.codExternoSerie.ToString() + ")");
                                if (csGlobal.ticketBaiActivo && ventas)
                                {
                                    if (repasat.current_page == 8 && ii == 16) {
                                        string hola = "hola";
                                    }
                                    rowInvoices["TICKETBAI"] = repasat.data[ii].idTicketBai != null && repasat.data[ii].sale_ticket_bai.url != null ? "PRESENTADA" : "BORRADOR";
                                    rowInvoices["SERIE"] = repasat.data[ii].idTicketBai != null && repasat.data[ii].sale_ticket_bai.url != null ? repasat.data[ii].series.nomSerie : "TBAI";

                                    numdocQR = repasat.data[ii].idTicketBai != null && !string.IsNullOrEmpty(repasat.data[ii].sale_ticket_bai.url) && repasat.data[ii].sale_ticket_bai.url.Contains("&") ? repasat.data[ii].sale_ticket_bai.url.Split('&')[2].Replace("nf=", "") : "0";
                                    rowInvoices["NUMFRA_QR"] = Convert.ToInt32 ( numdocQR);
                                    rowInvoices["SERIE_QR"] = repasat.data[ii].idTicketBai != null && !string.IsNullOrEmpty(repasat.data[ii].sale_ticket_bai.url) && repasat.data[ii].sale_ticket_bai.url.Contains("&") ? repasat.data[ii].sale_ticket_bai.url.Split('&')[1].Replace("s=", "") : "0";
                                    
                                }
                                else {
                                    rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie + " (" + (string.IsNullOrEmpty(repasat.data[ii].series.codExternoSerie) ? "SERIE NO VINCULADA" : repasat.data[ii].series.codExternoSerie.ToString() + ")");
                                }
                                rowInvoices["SERIE_RECT"] = repasat.data[ii].tipoDocumento == "CREDIT_NOTE" ? string.IsNullOrEmpty(repasat.data[ii].series.refSerie) ? repasat.data[ii].series.nomSerie : repasat.data[ii].series.refSerie : null ;
                                rowInvoices["NUM_FRA"] = repasat.data[ii].numSerieDocumento;
                                rowInvoices["FECHA"] = (DateTime.Parse(repasat.data[ii].fecDocumento)).ToString("dd/MM/yyyy");
                                rowInvoices["CLIENTE"] = repasat.data[ii].account.nomCli.ToUpper() + " (" + cabeceras[numCabeceras].codIC + ")";
                                rowInvoices["LINEAS"] = repasat.data[ii].lines.Count;
                                rowInvoices["BASE_DOC"] = repasat.data[ii].totalBaseImponibleDocumento.ToString().Replace('.', ',');
                                rowInvoices["IMPORTE_DOC"] = repasat.data[ii].totalDocumento.ToString().Replace('.', ',');
                                rowInvoices["EXTERNALID"] = repasat.data[ii].codExternoDocumento.ToString();
                                rowInvoices["REPASAT_ID_DOC"] = repasat.data[ii].idDocumento;

                                totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].totalDocumento.ToString().Replace('.', ','));


                                dtInvoices.Rows.Add(rowInvoices);


                                //Iteramos las lineas únicamente si no estamos haciendo la consulta
                                if (!consulta)
                                {

                                    // Si no tiene cliente ponemos a null esa factura
                                    if (cabeceras[numCabeceras].codIC == null)
                                    {
                                        //Crear el Cliente
                                        cabeceras[numCabeceras] = null;
                                        lineas[ii] = null;
                                        clientes[ii] = null;
                                    }
                                    else
                                    {
                                        /////////////////////////////////////////////////////
                                        //////////////////    LINEAS    ////////////////////
                                        ////////////////////////////////////////////////////

                                        for (int iii = 0; iii < repasat.data[ii].lines.Count; iii++) // lineas
                                        {
                                            if (contador == 45)
                                            {
                                                string txt = "hola";
                                            }
                                            lineas[contador] = new Objetos.csLineaDocumento();

                                            lineas[contador].nombreArticulo = repasat.data[ii].lines[iii].nomArticuloLineaDocumento;
                                            lineas[contador].cantidad = double.Parse(repasat.data[ii].lines[iii].unidadesArticuloLineaDocumento, CultureInfo.InvariantCulture).ToString().Replace(",", ".");
                                            lineas[contador].price = double.Parse(repasat.data[ii].lines[iii].precioVentaArticuloLineaDocumento, CultureInfo.InvariantCulture);
                                            lineas[contador].idArticulo = repasat.data[ii].lines[iii].idArticulo.ToString();
                                            lineas[contador].numCabecera = repasat.data[ii].idDocumento.ToString();
                                            lineas[contador].tipoIVA = rpstUtil.obtenerTipoImpuesto(repasat.data[ii].lines[iii].idTipoImpuesto.ToString());
                                            //lineas[contador].descuento = double.Parse(repasat.data[ii].lines[iii].descuentoLineaDocumento, CultureInfo.InvariantCulture).ToString();
                                            lineas[contador].descuento = repasat.data[ii].lines[iii].descuentoLineaDocumento.ToString().Replace(".", ",");
                                            lineas[contador].descuento2 = repasat.data[ii].lines[iii].descuento2LineaDocumento.ToString().Replace(".", ",");
                                            lineas[contador].cuentaContable = string.IsNullOrEmpty(repasat.data[ii].lines[iii].idCuentaContable.ToString()) ? "" : repasat.data[ii].lines[iii].accounting_account.numCuentaContable.ToString();
                                            lineas[contador].caracterCuota = repasat.data[ii].lines[iii].caracterCuotaLineaDocumeto;
                                            //ANALÍTICA Y CENTROS DE COSTE
                                            lineas[contador].centroCoste1 = !string.IsNullOrEmpty(repasat.data[ii].lines[iii].idCostCenter1) ? repasat.data[ii].lines[iii].cost_center1.codExternoCentroC : "";
                                            lineas[contador].centroCoste2 = !string.IsNullOrEmpty(repasat.data[ii].lines[iii].idCostCenter2) ? repasat.data[ii].lines[iii].cost_center2.codExternoCentroC : "";
                                            lineas[contador].centroCoste3 = !string.IsNullOrEmpty(repasat.data[ii].lines[iii].idCostCenter3) ? repasat.data[ii].lines[iii].cost_center3.codExternoCentroC : "";




                                            DataRow rowCuentasContables = dtCuentasContables.NewRow();
                                            rowCuentasContables["CUENTA"] = lineas[contador].cuentaContable;
                                            dtCuentasContables.Rows.Add(rowCuentasContables);

                                            /////////////////////////////////////////////////////
                                            //////////////////  ARTICULOS   ////////////////////
                                            ////////////////////////////////////////////////////

                                            string articuloCorregido = repasat.data[ii].lines[iii].refArticuloLineaDocumento.Length > 15 ? repasat.data[ii].lines[iii].refArticuloLineaDocumento.Substring(0, 15) : repasat.data[ii].lines[iii].refArticuloLineaDocumento;

                                            if (!string.IsNullOrEmpty(articuloCorregido)) //Verificamos que la referencia esté informada
                                            {
                                                errorArticulo = articuloCorregido;

                                                if (existProduct(articuloCorregido))
                                                {
                                                    lineas[contador].codigoArticulo = articuloCorregido;
                                                }
                                                else // Si no, lo creamos
                                                {
                                                    csa3erpItm item = new csa3erpItm();

                                                    a3.abrirEnlace();
                                                    lineas[contador].codigoArticulo = item.crearArticuloA3Repasat(lineas[contador].nombreArticulo,
                                                        "",
                                                        lineas[contador].price.ToString(),
                                                        repasat.data[ii].lines[iii].refArticuloLineaDocumento,
                                                        repasat.data[ii].lines[iii].idArticulo.ToString()).TrimStart();
                                                    a3.cerrarEnlace();
                                                }
                                            }
                                            else //Si la referencia no está informada le asignamos el artículo 0 de A3ERP
                                            {
                                                lineas[contador].codigoArticulo = "0";  //Si por casualidad no han puesto una referencia en el artículo le asignamos el código 0
                                                lineas[contador].descripcionArticulo = repasat.data[ii].lines[iii].nomArticuloLineaDocumento.ToString();

                                            }
                                            //Si está desmarcada la opción, traspasamos el nombre de la linea al documento
                                            if (!cboxDescripcionArticulosA3.Checked)
                                            {
                                                lineas[contador].descripcionArticulo = repasat.data[ii].lines[iii].nomArticuloLineaDocumento.ToString();
                                            }
                                            stepErrorLog = "step 7.1-"+contador;
                                            contador++;

                                        }
                                    }

                                }
                                //Fin de iteración con las lineas
                                numCabeceras++;
                            }

                            #endregion

                            //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                            // json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                            json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                            if (i + 1 <= repasat.last_page)
                            {
                                repasat = JsonConvert.DeserializeObject<Repasat.csSaleInvoice.RootObject>(json);

                                //Inicializo total_lineas para que cuente las nuevas lineas
                                total_lineas = 0;
                                //Redimensiono el Array de Lineas
                                for (int ii = 0; ii < repasat.data.Count; ii++)
                                {
                                    total_lineas += repasat.data[ii].lines.Count;
                                }
                                //Redimensiono el Array de Cabeceras
                                Array.Resize(ref cabeceras, cabeceras.Length + repasat.data.Count);
                                //Redimensiono el Array de Lineas, hay que coger el tamaño actual y añadirle las nuevas lineas
                                Array.Resize(ref lineas, lineas.Length + total_lineas);
                                Array.Resize(ref clientes, clientes.Length + repasat.data.Count);
                            }

                            //contador = 0;
                        }
                        if (consulta)
                        {
                            //DataRow rowInvoices = dtInvoices.NewRow();
                            //rowInvoices["ACCIÓN"] = "Total Documentos: ";
                            //rowInvoices["IMPORTE_DOC"] = totalDocument.ToString();
                            //dtInvoices.Rows.Add(rowInvoices);
                            tssTotalAmountRows.Text = "TOTAL DOCUMENTOS: " + totalDocument.ToString("N2");

                        }
                        if (!consulta)
                        {
                            string cuentasContablesAfectadas = cuentasContablesTratadas(dtCuentasContables);
                            //cuentas contables

                            //Gestionamos errores para indicar en donde está el error
                            errorCrearDocsA3 = true;
                            errorcargaWebService = false;

                            //Gestionamos mensajes para Ticket Bai
                            string modoTraspasoTicketBaiBorrador = "Se van a generar las facturas en modo borrador. \n Posteriormente puede presentarlas desde A3ERP \n" +
                                    "¿Está seguro?";
                            string modoTraspasoTicketBaiDirecto = "Se van a traspasar las facturas a A3ERP y se van a presentar en Ticket Bai. \n" +
                                "Este procedimiento es irreversible. ¿Está seguro?";

                            DialogResult Resultado = DialogResult.Yes;

                            if (csGlobal.ticketBaiActivo && ventas)
                            {
                                Resultado = MessageBox.Show(rbTraspasarComoBorradorSi.Checked ? modoTraspasoTicketBaiBorrador : modoTraspasoTicketBaiDirecto, "Traspasar Documentos Ticket Bai", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            else
                            {
                                if (csGlobal.modoManual)
                                {
                                    Resultado = MessageBox.Show("¿Quiere traspasar " + (cabeceras.Length) + " documentos a A3ERP?\n\n" + cuentasContablesAfectadas, "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else { Resultado = DialogResult.Yes; }
                            }

                            if (Resultado == DialogResult.Yes)
                            {
                                a3.abrirEnlace();
                                csGlobal.docDestino = "Factura";
                                documentosGenerados = a3.generarDocA3ObjetoRepasat(cabeceras, lineas, docCompras, "NO", clientes, null);

                                a3.cerrarEnlace();
                                if (csGlobal.modoManual)
                                    MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos traspasados a A3ERP de " + cabeceras.Length.ToString());
                                return null;
                            }
                        }

                    }
                    else
                    {
                        if (csGlobal.modoManual)
                        {
                            "No hay documentos para descargar entre los periodos seleccionados".mb();
                        }
                        return null;
                    }

                }
                if (consulta)
                {
                    return dtInvoices;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                    "Se ha encontrado un error en el siguiente número de documento de Repasat \n" +
                    "Serie: " + errorSerieDoc.ToString() + "\n" +
                    "Documento: " + errorNumDocumento.ToString() + "\n" +
                    "Id Documento Repasat: " + errorIdDocRPST.ToString() + "\n" +
                    "(Step Error Log: " + stepErrorLog + ")" + "\n" +
                    "por favor solucionar para poder traspasar documento";

                errorLog = errorCrearDocsA3 ? "Revisar creación de documentos en A3ERP \n" : errorLog;

                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;
            }

        }

        private string cuentasContablesTratadas(DataTable dtCuentas)
        {
            try
            {
                if (dtCuentas.Rows.Count > 0)
                {
                    string cuentas = "Cuentas contables imputadas en la sincronización:\n\n";
                    string cuentaContable = "";

                    dtCuentas = dtCuentas.AsEnumerable()
                        .GroupBy(r => new { Col1 = r["CUENTA"] })
                        .Select(g => g.OrderBy(r => r["CUENTA"]).First())
                        .CopyToDataTable();

                    for (int i = 0; i < dtCuentas.Rows.Count; i++)
                    {
                        cuentaContable = string.IsNullOrEmpty(dtCuentas.Rows[i]["CUENTA"].ToString()) ? "Se han encontrado líneas sin asignación (se asignará valor definido en A3ERP)" : dtCuentas.Rows[i]["CUENTA"].ToString();
                        cuentas = cuentas + "- " + cuentaContable + "\n";
                    }
                    return cuentas;
                }
                else return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        
            

        /// <summary>
        /// Funcion para traspasar efectos de cartera, impagados y agrupaciones
        /// </summary>
        /// <param name="filtroFechaIni"></param>
        /// <param name="filtroFechaFin"></param>
        /// <param name="ventas"></param>
        /// <param name="sincronizar"></param>
        /// <param name="sincronizado"></param>
        /// <param name="numRemesa"></param>
        /// <param name="impagados"></param>
        /// <param name="consultaEfectosRemesa"></param>
        /// <param name="idFacturaRPST"></param>
        /// <param name="idNumVto"></param>
        /// <param name="idSerieRPST"></param>
        /// <param name="cargarTodosEfectos"></param>
        /// <param name="agrupaciones"></param>
        /// <returns></returns>
        public DataTable cargarEfectos(string filtroFechaIni, string filtroFechaFin, bool ventas, bool sincronizar = false, string sincronizado = "TODOS", int numRemesa = 0, bool impagados = false, bool consultaEfectosRemesa = false, string idFacturaRPST = null, string idNumVto = null, string idSerieRPST = null, bool cargarTodosEfectos = false, bool agrupaciones = false)
        {

            int errorPagina = 0;
            int errorNumDocumento = 0;
            string errorSerieDoc = "";
            int errorLineaDoc = 0;
            string errorArticulo = "";
            string anexoError = "";
            int documentosGenerados = 0;
            int numEfectosSync = 0;
            bool cargaEfectosRemesa = numRemesa > 0 ? true : false;
            bool remesaValidaSync = true;
            string idCuentaRPST = "";
            string filtroFechaURL = "";
            string filtroDocPago = "";
            string filtroAgrupaciones = "";
            string errorEnRemesa = "";
            string filtroSync = sincronizar ? "&filter[sincronizarCartera]=1" : "";
            tipoDocumento = cboxTipoDoc.SelectedItem.ToString();


            try
            {
                csSqlConnects sql = new klsync.csSqlConnects();

                bool generarMovsContablesYCartera = cboxGenerarMovsContables.Checked ? true : false;

                if (checkBoxCuentas.Checked && !string.IsNullOrEmpty(cboxCuentas.SelectedValue.ToString()))
                {
                    string querySelect = rbVentas.Checked ? "select RPST_ID_CLI from __CLIENTES where  ltrim(CODCLI)='" + cboxCuentas.SelectedValue.ToString() + "'" : "select RPST_ID_PROV from PROVEED where  ltrim(CODPRO)='" + cboxCuentas.SelectedValue.ToString() + "'";
                    idCuentaRPST = sql.obtenerCampoTabla(querySelect);
                }


                if (rbutFechaFactura.Checked)
                {
                    filtroFechaURL = "&filter_gte[invoice][fecDocumento]=" + filtroFechaIni + "&filter_lte[invoice][fecDocumento]=" + filtroFechaFin;
                }
                else
                {
                    filtroFechaURL = "&filter_gte[fecVencimientoCarteraCobros]=" + filtroFechaIni + "&filter_lte[fecVencimientoCarteraCobros]=" + filtroFechaFin;
                }

                filtroDocPago = checkBoxDocPagos.Checked ? "&filter[paymentDocument][nomDocuPago]=" + cboxDocPagos.Text : "";


                filtroFechaURL = consultaEfectosRemesa ? "" : filtroFechaURL;
                string filtroExtDocNull = "";   //filtro para cargar sólo los documentos no descargados en A3ERP
                //Podemos borrar apikey
                //string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroSerie = "";
                string estadoSincro = "";
                string filtroEstadoCobro = "";
                string filtroCuenta = string.IsNullOrEmpty(idCuentaRPST) ? "" : "&filter[idCli]=" + idCuentaRPST;
                DataTable accounts = new DataTable();

                //Si sólo descargamo, cargamos sólo los pagados
                filtroEstadoCobro = !cargaEfectosRemesa && sincronizar ? "&filter[estadoCarteraCobros]=1" : filtroEstadoCobro;


                string idSerie = "";
                if (cboxSeries.SelectedIndex == -1)
                {
                    idSerie = "0";
                }
                else
                {
                    idSerie = cboxSeries.SelectedValue.ToString();
                    filtroSerie = "&filter[idSerie]=" + idSerie;
                }

                int numEfectos = 0;
                int numLineas = 0;

                //filtro para cargar todos los documentos o los pendientes
                filtroExtDocNull = (sincronizado == "NO") ? "&filter[codExternoCarteraCobros]=" : filtroExtDocNull;
                filtroExtDocNull = (sincronizado == "SI") ? "&filter_ne[codExternoCarteraCobros]=" : filtroExtDocNull;
                filtroExtDocNull = sincronizar ? "&filter[codExternoCarteraCobros]=" : filtroExtDocNull;
                filtroExtDocNull = cargarTodosEfectos ? "" : filtroExtDocNull;


                //filtro para cargar todos los documentos no remesados
                string filtroRemesado = "";
                filtroRemesado = rbRemesadoSi.Checked ? "&filter_ne[refSerie]=" : filtroRemesado;
                filtroRemesado = rbRemesadoNo.Checked ? "&filter[refSerie]=" : filtroRemesado;
                filtroRemesado = sincronizar ? "&filter[refSerie]=" : filtroRemesado;
                filtroRemesado = rbCompras.Checked ? "" : filtroRemesado;

                //filtro para cargar documentos por estado de cobro
                filtroEstadoCobro = rbPaidYes.Checked ? "&filter[estadoCarteraCobros]=1" : filtroEstadoCobro;
                filtroEstadoCobro = rbPaidNo.Checked ? "&filter[estadoCarteraCobros]=0" : filtroEstadoCobro;
                filtroEstadoCobro = rbPaidAll.Checked ? "" : filtroEstadoCobro;
                filtroEstadoCobro = sincronizar ? "&filter[estadoCarteraCobros]=1" : filtroEstadoCobro;
                filtroEstadoCobro = impagados && !sincronizar ? "&filter[estadoCarteraCobros]=2" : filtroEstadoCobro;
                filtroEstadoCobro = cargarTodosEfectos ? "" : filtroEstadoCobro;

                //Gestión de impagados
                filtroEstadoCobro = impagados ? "&filter[estadoCarteraCobros]=2" : filtroEstadoCobro;
                filtroRemesado = impagados ? "&filter_ne[remittance][codExternoRemesa]=" : filtroRemesado;

                //Gestion de Agrupaciones
                filtroAgrupaciones = agrupaciones ? "&filter_ne[numAgrupacionCarteraCobros]=" : "";
                if (agrupaciones) {
                    filtroEstadoCobro = "";
                    filtroRemesado = "";
                    filtroExtDocNull = (sincronizado == "NO") ? "&filter[codExternoAgrupacion]=" : filtroExtDocNull;
                    filtroExtDocNull = (sincronizado == "SI") ? "&filter_ne[codExternoAgrupacion]=" : filtroExtDocNull;
                }

                //Si paso como parámetro un número de remesa, cargará sólo los efectos de aquella remesa
                string filtroNumRemesa = "";
                filtroNumRemesa = numRemesa > 0 ? "&filter[refSerie]=" + numRemesa : "";
                filtroFechaURL = numRemesa > 0 ? "" : filtroFechaURL;
                filtroEstadoCobro = numRemesa > 0 ? "" : filtroEstadoCobro;

                string filtroURL = filtroFechaURL + filtroExtDocNull + filtroSerie + filtroEstadoCobro + filtroRemesado + filtroNumRemesa + filtroCuenta + filtroDocPago + filtroAgrupaciones+ filtroSync;

                //Para resetear enlace de codExterno o buscar un efecto
                filtroURL = string.IsNullOrEmpty(idFacturaRPST) ? filtroURL : "&filter[idDocumento]=" + idFacturaRPST + "&filter[numVencimientoCarteraCobros]=" + idNumVto;

                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtCartera = new DataTable();
                dtCartera.Columns.Add("ACCIÓN");
                dtCartera.Columns.Add("SYNC");
                dtCartera.Columns.Add("FECHA VTO");
                dtCartera.Columns.Add("CODIGO");
                dtCartera.Columns.Add("CLIENTE");
                dtCartera.Columns.Add("COBRADO");
                dtCartera.Columns.Add("IMPORTE_EFECTO");
                dtCartera.Columns.Add("BANCO");
                if (ventas) dtCartera.Columns.Add("REMESADO");
                if (ventas) dtCartera.Columns.Add("NUMREMESA");
                dtCartera.Columns.Add("SERIE");
                dtCartera.Columns.Add("NUM_FRA");
                dtCartera.Columns.Add("NUM VTO");
                dtCartera.Columns.Add("IDA3 FRA");
                dtCartera.Columns.Add("FECHA FRA");
                dtCartera.Columns.Add("FECHA PAGO");
                dtCartera.Columns.Add("DOC PAGO");
                dtCartera.Columns.Add("EXTERNALID");
                dtCartera.Columns.Add("REPASAT_ID_DOC");
                dtCartera.Columns.Add("CODBANCO");
                dtCartera.Columns.Add("CUENTA CONTABLE BANCO");
                dtCartera.Columns.Add("TIPO_DOC");
                dtCartera.Columns.Add("RPST IDFRA");
                dtCartera.Columns.Add("ANTICIPO");
                dtCartera.Columns.Add("REFANTICIPO");
                dtCartera.Columns.Add("IMPAGADO");
                dtCartera.Columns.Add("FECHA IMPAGADO");
                dtCartera.Columns.Add("AGRUPACION");


                double totalDocument = 0;
                string tipoObjeto = "";

                //Continuo con la carga de la cartera
                Objetos.csCartera[] cartera = null;

                //Objetos.csLineaDocumento[] lineas = null;
                //Objetos.csTercero[] clientes = null;
                csa3erp a3 = new csa3erp();
                csa3erpTercero tercero = new csa3erpTercero();
                int total_lineas = 0, contador = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    tipoObjeto = ventas ? "incomingpayments" : "outgoingpayments";


                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    //var json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + 1, "GET", tipoObjeto, "", "");

                    if (ventas)
                    {
                        #region cobros
                        if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                        {
                            var repasat = JsonConvert.DeserializeObject<Repasat.csIncomingPayments.RootObject>(json);
                            cartera = new Objetos.csCartera[repasat.data.Count];

                            for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                            {
                                errorPagina = i;
                                #region carteraCobros

                                /////////////////////////////////////////////////////
                                //////////////////   CARTERA  ////////////////////
                                ////////////////////////////////////////////////////

                                //Comienzo iteración de los efectos
                                for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                                {
                                    DataRow rowCarteras = dtCartera.NewRow();


                                    if (cargaEfectosRemesa && !consultaEfectosRemesa && repasat.data[ii].anticipoCarteraCobros != 1) //Validamos si podemos cargar todos los efectos de la Remesa, si sólo falla uno, la remesa no se debe traspasar.
                                    {
                                        errorEnRemesa = string.IsNullOrEmpty(repasat.data[ii].idDocumento.ToString()) && repasat.data[ii].anticipoCarteraCobros != 1 ? "Efecto sin factura" : errorEnRemesa;
                                        remesaValidaSync = string.IsNullOrEmpty(repasat.data[ii].idDocumento.ToString()) && repasat.data[ii].anticipoCarteraCobros != 1 ? false : remesaValidaSync;        //Efecto sin factura (anticipos por ejemplo)

                                        errorEnRemesa = string.IsNullOrEmpty(repasat.data[ii].idDocumento.ToString()) ? "Factura no descargada" : errorEnRemesa;
                                        remesaValidaSync = string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento) ? false : remesaValidaSync;    // Factura no descargada

                                        errorEnRemesa = !string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? "Efecto Sincronizado" : errorEnRemesa;
                                        remesaValidaSync = !string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? false : remesaValidaSync;    // Efecto Sincronizado

                                        if (!remesaValidaSync)
                                        {
                                            if (csGlobal.modoManual)
                                            {
                                                string documentoRef = repasat.data[ii].invoice.series.nomSerie + "/" + repasat.data[ii].invoice.numSerieDocumento;
                                                MessageBox.Show(errorEnRemesa + "\n Error en el efecto de la factura: " + documentoRef + "\n" + repasat.data[ii].idDocumento + "\n" + errorEnRemesa);
                                            }
                                            return null;
                                        }//Si algún efecto de la Remesa ha dado problemas, no se carga la Remesa
                                    }

                                    //Valido que el documento no esté bajado o que no esté marcado para on bajar
                                    if (sincronizar)
                                    {
                                        //Si la factura no está bajada no la tengo en cuenta
                                        if (string.IsNullOrEmpty(repasat.data[ii].idDocumento.ToString()))
                                        {
                                            break;
                                        }
                                        if (string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento ) && !agrupaciones)
                                        {
                                            continue;
                                        }
                                        //Si el efecto no está cobrado no lo tengo en cuenta para traspasarlo a A3ERP
                                        if (repasat.data[ii].estadoCarteraCobros == 0 && !agrupaciones)
                                        {
                                            continue;
                                        }
                                    }

                                    numEfectosSync++;

                                    cartera[numEfectos] = new Objetos.csCartera();

                                    //gestiono errores
                                    errorSerieDoc = repasat.data[ii].idDocumento == null ? repasat.data[ii].idSerieAnticipo.ToString() : repasat.data[ii].invoice.series.nomSerie.ToString();   // pendiente obtener el nombre de la serie
                                    errorNumDocumento = repasat.data[ii].idDocumento == null ? Convert.ToInt32(repasat.data[ii].numDocumentoAnticipo.ToString()) : Convert.ToInt32(repasat.data[ii].invoice.numSerieDocumento.ToString());


                                    cartera[numEfectos].extNumDoc = repasat.data[ii].idCarteraCobros.ToString();
                                    cartera[numEfectos].cobrado = repasat.data[ii].estadoCarteraCobros.ToString();
                                    cartera[numEfectos].serieDoc = repasat.data[ii].idDocumento == null ? csUtilidades.obtenerSerieA3ERP(repasat.data[ii].idSerieAnticipo.ToString()) : repasat.data[ii].invoice.series.nomSerie.ToString();   // pendiente obtener el nombre de la serie
                                    cartera[numEfectos].numDocA3 = repasat.data[ii].idDocumento == null ? repasat.data[ii].numDocumentoAnticipo.ToString() : repasat.data[ii].invoice.numSerieDocumento.ToString();
                                    cartera[numEfectos].esAnticipo = repasat.data[ii].idDocumento == null ? true : false;
                                    cartera[numEfectos].codigoClienteA3 = repasat.data[ii].customer.codExternoCli == null ? "" : repasat.data[ii].customer.codExternoCli.ToString();
                                    cartera[numEfectos].nombreIC = string.IsNullOrEmpty(repasat.data[ii].customer.razonSocialCli) ? repasat.data[ii].customer.nomCli.ToString() : repasat.data[ii].customer.razonSocialCli.ToString();
                                    cartera[numEfectos].externalFacturaId = repasat.data[ii].idDocumento == null || string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento) ? "" : repasat.data[ii].invoice.codExternoDocumento.ToString();
                                    cartera[numEfectos].numVencimiento = repasat.data[ii].numVencimientoCarteraCobros.ToString();
                                    cartera[numEfectos].idRemesa = string.IsNullOrEmpty(repasat.data[ii].refSerie) ? "" : repasat.data[ii].refSerie.ToString();
                                    cartera[numEfectos].remesado = string.IsNullOrEmpty(repasat.data[ii].refSerie) ? "NO" : "SI";
                                    cartera[numEfectos].importeEfecto = Convert.ToDouble(repasat.data[ii].importeCarteraCobros.ToString());
                                    cartera[numEfectos].importeEfectoCobrado = Convert.ToDouble(repasat.data[ii].importeCobradoCarteraCobros.ToString());
                                    cartera[numEfectos].importeGastosImpagado = string.IsNullOrEmpty(repasat.data[ii].gastosImpagadoCarteraCobros.ToString()) ? 0 : Convert.ToDouble(repasat.data[ii].gastosImpagadoCarteraCobros.ToString());
                                    cartera[numEfectos].fechaDoc = repasat.data[ii].idDocumento == null ? Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros) : Convert.ToDateTime(repasat.data[ii].invoice.fecDocumento);
                                    cartera[numEfectos].fechaImpagado = repasat.data[ii].idDocumento == null ? "" : Convert.ToDateTime(repasat.data[ii].fecImpagadoCarteraCobros).ToShortDateString();
                                    cartera[numEfectos].esEfectoAgrupado = false;
                                    cartera[numEfectos].codExternoAgrupacion = string.IsNullOrEmpty(repasat.data[ii].codExternoAgrupacion.ToString()) ? string.Empty : repasat.data[ii].codExternoAgrupacion.ToString();

                                    //Gestiono cobros VISA Klosions
                                    if (repasat.data[ii].estadoCarteraCobros == 1 && repasat.data[ii].idDatosBancarios.ToString()== "22460") {
                                        cartera[numEfectos].esEfectoCobradoVISA = true;
                                    }

                                    //Gestiono Agrupación 
                                    if (!string.IsNullOrEmpty(repasat.data[ii].numAgrupacionCarteraCobros))
                                    {
                                        cartera[numEfectos].esEfectoAgrupado = repasat.data[ii].padreAgrupacionCarteraCobros ? true : false;
                                        cartera[numEfectos].perteneceEfectoAgrupado = !repasat.data[ii].padreAgrupacionCarteraCobros && !string.IsNullOrEmpty(repasat.data[ii].numAgrupacionCarteraCobros) ? true : false;
                                        cartera[numEfectos].idAgrupacion = Convert.ToInt32(repasat.data[ii].numAgrupacionCarteraCobros);
                                        cartera[numEfectos].refAgrupacionCarteraCobros = repasat.data[ii].refAgrupacionCarteraCobros;
                                    }

                                    //Fin Gestión Agrupación

                                    cartera[numEfectos].generarMovimientosContables = generarMovsContablesYCartera;

                                    if (repasat.data[ii].fecCobroCarteraCobros == null)
                                    {
                                        if (repasat.data[ii].impagadoCarteraCobros == 1) // Si es un impagado (pendiente de asignar fecha de impagado), pongo la fecha de la remesa
                                        {
                                            cartera[numEfectos].fechaCobroPago = Convert.ToDateTime(repasat.data[ii].remittance.fechaCobro);
                                        }
                                        else
                                        {
                                            cartera[numEfectos].fechaCobroPago = Convert.ToDateTime("01/01/0001");
                                        }
                                    }
                                    else
                                    {
                                        cartera[numEfectos].fechaCobroPago = Convert.ToDateTime(repasat.data[ii].fecCobroCarteraCobros);
                                    }

                                    //si el efecto esta pagado/cobrado

                                    if (!string.IsNullOrEmpty(repasat.data[ii].idDatosBancarios.ToString()))
                                    {
                                        cartera[numEfectos].codBanco = string.IsNullOrEmpty(repasat.data[ii].bank.codExternoDatosBancarios) ? "" : repasat.data[ii].bank.codExternoDatosBancarios.ToString();
                                        cartera[numEfectos].cuentaContableBanco = string.IsNullOrEmpty(repasat.data[ii].bank.cuentaContableDatosBancarios) ? "" : repasat.data[ii].bank.cuentaContableDatosBancarios.ToString();
                                    }

                                    cartera[numEfectos].codIC = repasat.data[ii].customer.codExternoCli;
                                    cartera[numEfectos].nombreIC = repasat.data[ii].customer.nomCli;
                                    cartera[numEfectos].fechaVencimiento = Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros);
                                    //cartera[numEfectos].tipoDoc = repasat.data[ii].tipoDocumento.ToString();

                                    //Información de Pago
                                    //cartera[numEfectos].codFormaPago = repasat.data[ii].payment_method.codExternoFormaPago.ToString();
                                    cartera[numEfectos].codDocumentoPago = repasat.data[ii].payment_document.codExternoDocuPago.ToString();

                                    //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                                    estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? " pendiente de traspaso " : " traspasado ";

                                    if (tipoDocumento == "Cartera")
                                    {
                                        estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? " pendiente de traspaso " : " traspasado ";
                                    }
                                    else if (tipoDocumento == "Agrupaciones")
                                    {
                                        estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) && string.IsNullOrEmpty(repasat.data[ii].refAgrupacionCarteraCobros) ? " pendiente de traspaso " : " traspasado ";
                                    }

                                    rowCarteras["ACCIÓN"] = "EFECTO Nº " + (repasat.data[ii].idDocumento == null ? repasat.data[ii].numDocumentoAnticipo.ToString() : repasat.data[ii].invoice.numSerieDocumento.ToString()) + estadoSincro;
                                    rowCarteras["SYNC"] = (repasat.data[ii].sincronizarCartera == null || repasat.data[ii].sincronizarCartera == true) ? "SI" : "NO";
                                    rowCarteras["TIPO_DOC"] = "EFECTOS";
                                    rowCarteras["DOC PAGO"] = repasat.data[ii].payment_document.nomDocuPago.ToString();
                                    string esAgrupacion = "";
                                    if (cartera[numEfectos].perteneceEfectoAgrupado) {
                                        esAgrupacion = " (AGR)";
                                    }
                                    rowCarteras["COBRADO"] = repasat.data[ii].estadoCarteraCobros == 1 ? "SI" + esAgrupacion : "NO";
                                    rowCarteras["BANCO"] = (repasat.data[ii].idDatosBancarios == null) ? "" : repasat.data[ii].idDatosBancarios.ToString();
                                    if (ventas) rowCarteras["REMESADO"] = (repasat.data[ii].refSerie == null) ? "NO" : "SI";
                                    if (ventas) rowCarteras["NUMREMESA"] = (repasat.data[ii].refSerie == null) ? "" : repasat.data[ii].refSerie.ToString();
                                    rowCarteras["SERIE"] = repasat.data[ii].idDocumento == null ? csUtilidades.obtenerSerieA3ERP(repasat.data[ii].idSerieAnticipo.ToString()) : repasat.data[ii].invoice.series.nomSerie.ToString();
                                    rowCarteras["NUM_FRA"] = repasat.data[ii].idDocumento == null ? repasat.data[ii].numDocumentoAnticipo.ToString() : repasat.data[ii].invoice.numSerieDocumento.ToString();
                                    rowCarteras["IDA3 FRA"] = repasat.data[ii].idDocumento == null || string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento) ? "" : repasat.data[ii].invoice.codExternoDocumento.ToString();
                                    rowCarteras["FECHA FRA"] = repasat.data[ii].idDocumento == null ? Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros).ToString("dd/MM/yyyy") : Convert.ToDateTime(repasat.data[ii].invoice.fecDocumento).ToString("dd/MM/yyyy");
                                    rowCarteras["FECHA VTO"] = Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros).ToString("dd/MM/yyyy");
                                    rowCarteras["FECHA PAGO"] = (repasat.data[ii].fecCobroCarteraCobros == null) ? "" : Convert.ToDateTime(repasat.data[ii].fecCobroCarteraCobros).ToString("dd/MM/yyyy");
                                    rowCarteras["NUM VTO"] = repasat.data[ii].numVencimientoCarteraCobros;
                                    rowCarteras["CODIGO"] = repasat.data[ii].customer.codExternoCli;
                                    rowCarteras["CLIENTE"] = repasat.data[ii].customer.nomCli + " (" + repasat.data[ii].customer.codExternoCli + ")";
                                    rowCarteras["IMPORTE_EFECTO"] = repasat.data[ii].importeCarteraCobros;
                                    rowCarteras["EXTERNALID"] = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? "" : repasat.data[ii].codExternoCarteraCobros.ToString();
                                    rowCarteras["REPASAT_ID_DOC"] = repasat.data[ii].idCarteraCobros;
                                    rowCarteras["CODBANCO"] = cartera[numEfectos].codBanco;
                                    rowCarteras["CUENTA CONTABLE BANCO"] = string.IsNullOrEmpty(cartera[numEfectos].cuentaContableBanco) ? "" : cartera[numEfectos].cuentaContableBanco;
                                    rowCarteras["RPST IDFRA"] = repasat.data[ii].idDocumento == null ? 0 : repasat.data[ii].invoice.idDocumento;
                                    rowCarteras["ANTICIPO"] = repasat.data[ii].anticipoCarteraCobros == 1 ? "SI" : "NO";
                                    rowCarteras["REFANTICIPO"] = string.IsNullOrEmpty(repasat.data[ii].referenciaAnticipo) ? "" : repasat.data[ii].referenciaAnticipo.ToString();
                                    rowCarteras["IMPAGADO"] = repasat.data[ii].impagadoCarteraCobros == 1 ? "SI" : "NO";
                                    rowCarteras["AGRUPACION"] = string.IsNullOrEmpty(repasat.data[ii].numAgrupacionCarteraCobros) ? "NO" : "SI";


                                    totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].importeCarteraCobros.ToString().Replace('.', ','));

                                    dtCartera.Rows.Add(rowCarteras);

                                    numEfectos++;
                                }

                                #endregion

                                //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                                // json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                                json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                                if (i + 1 <= repasat.last_page)
                                {
                                    repasat = JsonConvert.DeserializeObject<Repasat.csIncomingPayments.RootObject>(json);

                                    //Redimensiono el Array de Cabeceras
                                    Array.Resize(ref cartera, cartera.Length + repasat.data.Count);
                                }

                                //contador = 0;
                            }
                            if (!sincronizar && !cargaEfectosRemesa)
                            {
                                tssTotalAmountRows.Text = "TOTAL DOCUMENTOS: " + totalDocument.ToString("N2");
                            }
                            if (sincronizar)
                            {

                                DialogResult Resultado = DialogResult.Yes;
                                if (csGlobal.modoManual)
                                {
                                    string tituloBox = impagados ? "Revisión Impagados" : "Traspasar Documentos";
                                    Resultado = MessageBox.Show("¿Quiere traspasar " + (numEfectosSync.ToString()) + " documentos a A3ERP?", tituloBox, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }
                                else
                                {
                                    Resultado = DialogResult.Yes;
                                }


                                if (Resultado == DialogResult.Yes)

                                {
                                    //traspaso A3ERP
                                    csa3erpCartera carteraA3 = new csa3erpCartera();
                                    if (agrupaciones)
                                    {
                                        documentosGenerados = carteraA3.crearAgrupacion(cartera, ventas);
                                    }
                                    else
                                    {
                                        documentosGenerados = carteraA3.cobrarPagarEfecto(cartera, ventas, impagados, generarMovsContablesYCartera);
                                    }


                                    if (csGlobal.modoManual)
                                        MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos procesados a A3ERP de " + cartera.Length.ToString());
                                    return null;
                                }
                            }



                        }
                        else
                        {
                            if (csGlobal.modoManual)
                            {
                                string mensaje = "No hay efectos para descargar entre los periodos seleccionados";
                                mensaje = impagados ? "No hay impagados para descargar entre los periodos seleccionados" : mensaje;
                                mensaje.mb();
                            }

                            return null;
                        }
                        #endregion cobros
                    }
                    else if (!ventas)  //Pagos
                    {
                        #region pagos

                        if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                        {
                            var repasat = JsonConvert.DeserializeObject<Repasat.csOutgoingPayments.RootObject>(json);
                            cartera = new Objetos.csCartera[repasat.data.Count];

                            for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                            {
                                errorPagina = i;
                                #region kwebfkjwebf

                                ////////////////////////////////////////////////////
                                //////////////////   CARTERA PAGOS /////////////////
                                ////////////////////////////////////////////////////

                                //Comienzo iteración de los efectos
                                for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                                {
                                    DataRow rowCarteras = dtCartera.NewRow();

                                    //Valido que el documento no esté bajado o que no esté marcado para on bajar
                                    //En las Remesas 

                                    if (!sincronizar && numRemesa > 0)
                                    {
                                        //Si la factura no está bajada no la tengo en cuenta
                                        if (string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento))
                                        {
                                            continue;
                                        }
                                        //Si el efecto no está cobrado no lo tengo en cuenta para traspasarlo a A3ERP
                                        if (repasat.data[ii].estadoCarteraCobros == 0)
                                        {
                                            continue;
                                        }
                                    }

                                    numEfectosSync++;

                                    cartera[numEfectos] = new Objetos.csCartera();

                                    cartera[numEfectos].extNumDoc = repasat.data[ii].idCarteraCobros.ToString();
                                    cartera[numEfectos].serieDoc = repasat.data[ii].invoice.idSerie.ToString();  // pendiente obtener el nombre de la serie
                                    cartera[numEfectos].numDocA3 = repasat.data[ii].invoice.numSerieDocumento.ToString();
                                    cartera[numEfectos].externalFacturaId = string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento) ? "" : repasat.data[ii].invoice.codExternoDocumento.ToString();
                                    cartera[numEfectos].numVencimiento = repasat.data[ii].numVencimientoCarteraCobros.ToString();
                                    //cartera[numEfectos].idRemesa = string.IsNullOrEmpty(repasat.data[ii].refSerie) ? "" : repasat.data[ii].refSerie.ToString();
                                    //cartera[numEfectos].remesado = string.IsNullOrEmpty(repasat.data[ii].refSerie) ? "NO" : "SI";
                                    cartera[numEfectos].importeEfecto = Convert.ToDouble(repasat.data[ii].importeCarteraCobros.ToString());
                                    cartera[numEfectos].importeEfectoCobrado = Convert.ToDouble(repasat.data[ii].importeCobradoCarteraCobros.ToString());
                                    cartera[numEfectos].fechaDoc = Convert.ToDateTime(repasat.data[ii].invoice.fecDocumento);
                                    cartera[numEfectos].fechaCobroPago = Convert.ToDateTime(repasat.data[ii].fecCobroCarteraCobros);

                                    //si el efecto esta pagado/cobrado
                                    cartera[numEfectos].codBanco = repasat.data[ii].idDatosBancarios == null || repasat.data[ii].bank.codExternoDatosBancarios == null || string.IsNullOrEmpty(repasat.data[ii].idDatosBancarios.ToString()) ? "" : repasat.data[ii].bank.codExternoDatosBancarios.ToString();
                                    if (!string.IsNullOrEmpty(repasat.data[ii].idDatosBancarios.ToString()) && repasat.data[ii].bank.cuentaContableDatosBancarios != null)
                                    {
                                        cartera[numEfectos].cuentaContableBanco = string.IsNullOrEmpty(repasat.data[ii].bank.cuentaContableDatosBancarios.ToString()) ? "" : repasat.data[ii].bank.cuentaContableDatosBancarios.ToString();
                                    }

                                    //gestiono errores
                                    errorSerieDoc = repasat.data[ii].invoice.idSerie.ToString();   // pendiente obtener el nombre de la serie
                                    errorNumDocumento = Convert.ToInt32(repasat.data[ii].invoice.numSerieDocumento.ToString());
                                    //fin gestion errores

                                    cartera[numEfectos].codIC = repasat.data[ii].customer.codExternoCli;
                                    cartera[numEfectos].nombreIC = repasat.data[ii].customer.nomCli;
                                    cartera[numEfectos].fechaVencimiento = Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros);
                                    //cartera[numEfectos].tipoDoc = repasat.data[ii].tipoDocumento.ToString();

                                    //Información de Pago
                                    //cartera[numEfectos].codFormaPago = repasat.data[ii].payment_method.codExternoFormaPago.ToString();
                                    cartera[numEfectos].codDocumentoPago = repasat.data[ii].payment_document.codExternoDocuPago.ToString();

                                    cartera[numEfectos].generarMovimientosContables = generarMovsContablesYCartera;

                                    //Añado toda la información en el datatable para mostrar al final los documentos que traspaso

                                    estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? " pendiente de traspaso " : " traspasado ";
                                    rowCarteras["ACCIÓN"] = "EFECTO Nº " + repasat.data[ii].invoice.numSerieDocumento + estadoSincro;
                                    rowCarteras["TIPO_DOC"] = "EFECTOS";
                                    rowCarteras["COBRADO"] = repasat.data[ii].estadoCarteraCobros == 1 ? "SI" : "NO";
                                    rowCarteras["BANCO"] = (repasat.data[ii].idDatosBancarios == null) ? "" : repasat.data[ii].idDatosBancarios.ToString();
                                    if (ventas) rowCarteras["REMESADO"] = (repasat.data[ii].refSerie == null) ? "NO" : "SI";
                                    if (ventas) rowCarteras["NUMREMESA"] = (repasat.data[ii].refSerie == null) ? "" : repasat.data[ii].refSerie.ToString();
                                    rowCarteras["SERIE"] = repasat.data[ii].invoice.series.codExternoSerie;
                                    rowCarteras["NUM_FRA"] = repasat.data[ii].invoice.numSerieDocumento;
                                    rowCarteras["IDA3 FRA"] = string.IsNullOrEmpty(repasat.data[ii].invoice.codExternoDocumento) ? "" : repasat.data[ii].invoice.codExternoDocumento;
                                    rowCarteras["FECHA FRA"] = Convert.ToDateTime(repasat.data[ii].invoice.fecDocumento).ToString("dd/MM/yyyy");
                                    rowCarteras["FECHA VTO"] = Convert.ToDateTime(repasat.data[ii].fecVencimientoCarteraCobros).ToString("dd/MM/yyyy");
                                    rowCarteras["FECHA PAGO"] = Convert.ToDateTime(repasat.data[ii].fecCobroCarteraCobros).ToString("dd/MM/yyyy");
                                    rowCarteras["NUM VTO"] = repasat.data[ii].numVencimientoCarteraCobros;
                                    rowCarteras["CODIGO"] = repasat.data[ii].customer.codExternoCli;
                                    rowCarteras["CLIENTE"] = repasat.data[ii].customer.nomCli;
                                    rowCarteras["IMPORTE_EFECTO"] = repasat.data[ii].importeCarteraCobros;
                                    rowCarteras["EXTERNALID"] = string.IsNullOrEmpty(repasat.data[ii].codExternoCarteraCobros) ? "" : repasat.data[ii].codExternoCarteraCobros.ToString();
                                    rowCarteras["REPASAT_ID_DOC"] = repasat.data[ii].idCarteraCobros;
                                    rowCarteras["CODBANCO"] = cartera[numEfectos].codBanco;
                                    rowCarteras["CUENTA CONTABLE BANCO"] = cartera[numEfectos].cuentaContableBanco;
                                    rowCarteras["RPST IDFRA"] = repasat.data[ii].invoice.idDocumento;

                                    totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].importeCarteraCobros.ToString().Replace('.', ','));

                                    dtCartera.Rows.Add(rowCarteras);

                                    numEfectos++;
                                }

                                #endregion

                                //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                                // json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                                json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                                if (i + 1 <= repasat.last_page)
                                {
                                    repasat = JsonConvert.DeserializeObject<Repasat.csOutgoingPayments.RootObject>(json);

                                    //Redimensiono el Array de Cabeceras
                                    Array.Resize(ref cartera, cartera.Length + repasat.data.Count);
                                }

                                //contador = 0;
                            }
                            if (!sincronizar)
                            {
                                tssTotalAmountRows.Text = "TOTAL DOCUMENTOS: " + totalDocument.ToString("N2");

                            }
                            if (sincronizar)
                            {
                                DialogResult Resultado = DialogResult.Yes;
                                if (csGlobal.modoManual)
                                {
                                    Resultado = MessageBox.Show("¿Quiere traspasar " + (numEfectosSync.ToString()) + " documentos a A3ERP?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                }

                                if (Resultado == DialogResult.Yes)

                                {
                                    csa3erpCartera carteraA3 = new csa3erpCartera();

                                    documentosGenerados = carteraA3.cobrarPagarEfecto(cartera, ventas, false, generarMovsContablesYCartera);

                                    MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos procesados a A3ERP de " + cartera.Length.ToString());
                                    return null;
                                }
                            }



                        }
                        else
                        {
                            if (csGlobal.modoManual)
                                "No hay documentos para descargar entre los periodos seleccionados".mb();
                            return null;
                        }
                        #endregion pagos

                    }
                    return dtCartera;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                    "Documento: " + errorNumDocumento.ToString() + "\n";

                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;
            }
        }




        public void anularPago(string idEfecto, bool ventas)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    tipoObjeto = ventas ? "incomingpayments" : "outgoingpayments";

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, null, idEfecto, "cancelPayment");

                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Query para obtener las remesas bancarias de Repasat
        /// </summary>
        /// <param name="filtroFechaIni"></param>
        /// <param name="filtroFechaFin"></param>
        /// <param name="ventas"></param>
        /// <param name="consulta"></param>
        /// <param name="sincronizado"></param>
        /// <returns></returns>
        public DataTable gestionarRemesas(string filtroFechaIni, string filtroFechaFin, bool ventas, bool consulta = false, string sincronizado = "TODOS")
        {
            //Creo un dataset para guardar las cabeceras y las lineas
            //informo el nombre para luego gestionarlo en A3ERP
            DataSet datasetRemesa = new DataSet();
            DataTable dtEfectosRemesaToA3ERP = new DataTable();    //Datatable para enviar todos los efectos de todas las remesas
            DataTable dtEfectosEnRemesa = new DataTable();  //Datatable para cargar los efectos de cada remesa (luego se incluyen en dtEfectosRemesaToA3ERP)
            dtEfectosRemesaToA3ERP.TableName = "EFECTOS";

            int errorPagina = 0;
            int errorNumDocumento = 0;
            string errorSerieDoc = "";
            int errorLineaDoc = 0;
            string errorArticulo = "";
            int documentosGenerados = 0;
            string errorNumRemesa = "";

            try
            {
                string filtroFechaURL = "&filter_gte[fechaGeneracion]=" + filtroFechaIni + "&filter_lte[fechaGeneracion]=" + filtroFechaFin + " 23:59:59";
                string filtroExtDocNull = "&filter[codExternoRemesa]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroSerie = "";
                string estadoSincro = "";
                string estadoCobro = "&filter[estadoCobro]=1";
                string filtroSync = consulta ? "" : "&filter[sincronizarRemesa]=1";
                DataTable accounts = new DataTable();

                //Si sólo descargamo, cargamos sólo los pagados
                estadoCobro = consulta ? "" : estadoCobro;
                bool sincronizar = consulta ? false : true;

                //filtro para cargar documentos por estado de cobro
                estadoCobro = rbPaidYes.Checked ? "&filter[estadoCobro]=1" : estadoCobro;
                estadoCobro = rbPaidNo.Checked ? "&filter[estadoCobro]=0" : estadoCobro;
                estadoCobro = rbPaidAll.Checked ? "" : estadoCobro;
                estadoCobro = sincronizar ? "&filter[estadoCobro]=1" : estadoCobro;

                string idSerie = "";
                if (cboxSeries.SelectedIndex == -1)
                {
                    idSerie = "0";
                }
                else
                {
                    idSerie = cboxSeries.SelectedValue.ToString();
                    filtroSerie = "&filter[idSerie]=" + idSerie;
                }

                int numRemesas = 0;
                int numLineas = 0;

                //filtro para cargar todos los documentos o los pendientes
                filtroExtDocNull = sincronizado == "TODOS" ? "" : filtroExtDocNull;
                filtroExtDocNull = sincronizado == "NO" ? "&filter[codExternoRemesa]=" : filtroExtDocNull;
                filtroExtDocNull = sincronizado == "SI" ? "&filter_ne[codExternoRemesa]=" : filtroExtDocNull;

                filtroExtDocNull = !consulta ? "&filter[codExternoRemesa]=&filter[estadoCobro]=1" : filtroExtDocNull;

                string filtroURL = filtroFechaURL + filtroExtDocNull + filtroSerie + estadoCobro+ filtroSync;

                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtRemesas = new DataTable();
                dtRemesas.TableName = "CABECERA";
                dtRemesas.Columns.Add("ACCIÓN");
                dtRemesas.Columns.Add("SINCRONIZAR");
                dtRemesas.Columns.Add("NUMREMESA");
                dtRemesas.Columns.Add("FECHA");
                dtRemesas.Columns.Add("FECHA_COBRO");
                dtRemesas.Columns.Add("IMPORTE");
                dtRemesas.Columns.Add("COBRADO");
                dtRemesas.Columns.Add("BANCO");
                dtRemesas.Columns.Add("CUENTA");
                dtRemesas.Columns.Add("EXTERNALID");
                dtRemesas.Columns.Add("REPASAT_ID_DOC");

                double totalDocument = 0;
                string tipoObjeto = "";

                //Continuo con la carga de la cartera
                csSqlConnects sql = new csSqlConnects();
                Objetos.csRemesas[] remesa = null;

                int total_lineas = 0, contador = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    tipoObjeto = "remittances";

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    //var json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + 1, "GET", tipoObjeto, "", "");

                    if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                    {

                        var repasat = JsonConvert.DeserializeObject<Repasat.csRemittances.RootObject>(json);
                        remesa = new Objetos.csRemesas[repasat.data.Count];


                        for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                        {
                            errorPagina = i;
                            #region kwebfkjwebf

                            /////////////////////////////////////////////////////
                            //////////////////   REMESAS  ////////////////////
                            ////////////////////////////////////////////////////

                            //Comienzo iteración de las Remesas
                            for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                            {
                                DataRow rowRemesas = dtRemesas.NewRow();
                                remesa[numRemesas] = new Objetos.csRemesas();
                                remesa[numRemesas].extNumDoc = repasat.data[ii].idRemesa.ToString();
                                errorNumRemesa = remesa[numRemesas].extNumDoc;
                                remesa[numRemesas].numeroRemesa = repasat.data[ii].idRefSerie.ToString();
                                remesa[numRemesas].extNumDoc = string.IsNullOrEmpty(repasat.data[ii].codExternoRemesa) ? "" : repasat.data[ii].codExternoRemesa.ToString();
                                remesa[numRemesas].codBanco = string.IsNullOrEmpty(repasat.data[ii].bank.codExternoDatosBancarios) ? "" : repasat.data[ii].bank.codExternoDatosBancarios.ToString();
                                //remesa[numRemesas].codBanco = obtenerCodigoBanco(repasat.data[ii].idBancoRemesa.ToString());

                                remesa[numRemesas].cuentaBanco = string.IsNullOrEmpty(repasat.data[ii].bank.cuentaContableDatosBancarios) ? "" : repasat.data[ii].bank.cuentaContableDatosBancarios.ToString();
                                //remesa[numRemesas].cuentaBanco = obtenerCuentaContableBanco(repasat.data[ii].idBancoRemesa.ToString());

                                remesa[numRemesas].fechaRemesa = Convert.ToDateTime(repasat.data[ii].fechaGeneracion);
                                remesa[numRemesas].fechaCobro = Convert.ToDateTime(repasat.data[ii].fechaCobro);
                                remesa[numRemesas].cobrado = repasat.data[ii].estadoCobro.ToString();

                                //Añado toda la información en el datatable para mostrar al final los documentos que traspaso

                                estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoRemesa) ? " pendiente de traspaso " : " traspasado ";
                                rowRemesas["ACCIÓN"] = "Remesa nº " + repasat.data[ii].idRefSerie + estadoSincro;
                                rowRemesas["SINCRONIZAR"] = (repasat.data[ii].sincronizarRemesa == null || repasat.data[ii].sincronizarRemesa == true) ? "SI" : "NO";
                                rowRemesas["NUMREMESA"] = repasat.data[ii].idRefSerie;
                                rowRemesas["FECHA"] = Convert.ToDateTime(repasat.data[ii].fechaGeneracion).ToString("dd/MM/yyyy");

                                //TODO:FRANCO
                                rowRemesas["FECHA_COBRO"] = Convert.ToDateTime(repasat.data[ii].fechaCobro).ToString("dd/MM/yyyy");


                                rowRemesas["IMPORTE"] = repasat.data[ii].importeRemesaTotal.ToString().Replace(".", ","); ;
                                rowRemesas["COBRADO"] = repasat.data[ii].estadoCobro == 1 ? "SI" : "NO";
                                rowRemesas["BANCO"] = string.IsNullOrEmpty(repasat.data[ii].bank.codExternoDatosBancarios) ? "" : repasat.data[ii].bank.codExternoDatosBancarios.ToString();
                                rowRemesas["CUENTA"] = remesa[numRemesas].cuentaBanco;
                                rowRemesas["EXTERNALID"] = string.IsNullOrEmpty(repasat.data[ii].codExternoRemesa) ? "" : repasat.data[ii].codExternoRemesa.ToString();
                                rowRemesas["REPASAT_ID_DOC"] = repasat.data[ii].idRemesa;

                                totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].importeRemesaTotal.ToString().Replace('.', ','));
                                dtRemesas.Rows.Add(rowRemesas);
                                numRemesas++;
                            }

                            #endregion

                            //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)

                            json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                            if (i + 1 <= repasat.last_page)
                            {
                                repasat = JsonConvert.DeserializeObject<Repasat.csRemittances.RootObject>(json);

                                //Redimensiono el Array de Cabeceras
                                Array.Resize(ref remesa, remesa.Length + repasat.data.Count);
                            }
                        }
                        if (consulta) //lluisvera
                        {
                            tssTotalAmountRows.Text = "TOTAL DOCUMENTOS: " + totalDocument.ToString("N2");
                            return dtRemesas;


                        }
                        if (!consulta)
                        {
                            DialogResult Resultado = DialogResult.Yes;
                            if (csGlobal.modoManual)
                                Resultado = MessageBox.Show("¿Quiere sincronizar " + (numRemesas.ToString()) + " documentos a A3ERP?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (Resultado == DialogResult.Yes)
                            {
                                //(string filtroFechaIni, string filtroFechaFin, bool ventas, bool consulta = false, string sincronizado = "TODOS")
                                int numRemesaLoad = 0;
                                int contadorRemesasLoad = 0;

                                //Junto en un único datatable todos los efectos
                                //foreach (DataRow fila in dtRemesas.Rows)
                                for (int numRem = 0; numRem < dtRemesas.Rows.Count; numRem++)
                                {
                                    if (string.IsNullOrEmpty(dtRemesas.Rows[numRem]["NUMREMESA"].ToString())) break;

                                    numRemesaLoad = Convert.ToInt16(dtRemesas.Rows[numRem]["NUMREMESA"].ToString());
                                    //Cargo los Efectos de la Remesa

                                    dtEfectosEnRemesa = cargarEfectos(filtroFechaIni, filtroFechaFin, true, false, "TODOS", numRemesaLoad);
                                    //Si me devuelve null, es que hay efectos en la Remesa que no se pueden traspasar a A3
                                    if (dtEfectosEnRemesa == null)
                                    {
                                        dtRemesas.Rows[numRem]["ACCIÓN"] = "NOSYNC";
                                        continue;
                                    }

                                    if (contadorRemesasLoad == 0)
                                    {
                                        dtEfectosRemesaToA3ERP = dtEfectosEnRemesa.Copy();  //metodo copy si es el primer datatable
                                    }
                                    else
                                    {
                                        dtEfectosEnRemesa = cargarEfectos(filtroFechaIni, filtroFechaFin, true, false, "TODOS", numRemesaLoad);
                                        if (dtEfectosEnRemesa != null)
                                        {
                                            dtEfectosRemesaToA3ERP.Merge(dtEfectosEnRemesa);  //metodo merge si hay más de un datatable y se hace a partir del segundo
                                        }
                                    }
                                    contadorRemesasLoad++;
                                }

                                //dtEfectosRemesa= cargarEfectos(filtroFechaIni, filtroFechaFin, true, true, "TODOS", 1);
                                dtEfectosRemesaToA3ERP.TableName = "EFECTOS";
                                datasetRemesa.Tables.Add(dtRemesas);
                                datasetRemesa.Tables.Add(dtEfectosRemesaToA3ERP);

                                csa3erpCartera a3Cartera = new csa3erpCartera();
                                documentosGenerados = a3Cartera.crearRemesa(datasetRemesa);
                                if (csGlobal.modoManual)
                                    MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos traspasados a A3ERP de " + remesa.Length.ToString());
                                return dtRemesas;
                            }
                            else
                            {
                                return dtRemesas;
                            }
                        }


                    }
                    else
                    {
                        if (csGlobal.modoManual)
                            "No hay documentos para descargar entre los periodos seleccionados".mb();
                        return null;
                    }
                    return null;

                }


            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                    "Remesa: " + errorNumRemesa.ToString() + "\n" +
                    "Documento: " + errorNumDocumento.ToString() + "\n";

                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;
            }
        }


        private string obtenerCodigoBanco(string idBanco)
        {
            return sql.obtenerCampoTabla("SELECT CODBAN FROM BANCOS WHERE RPST_ID_BANCO=" + idBanco);
        }

        private string obtenerCuentaContableBanco(string idBanco)
        {
            return sql.obtenerCampoTabla("SELECT CUENTA FROM BANCOS WHERE RPST_ID_BANCO=" + idBanco);
        }

        private void metodosPagoA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT ltrim(FORPAG) AS CODERP, ltrim(FORPAG) as CODIGO ,DESCFOR ,RPST_ID_FORMAPAG AS ID_REPASAT,NUMVENCIM ,PRIMLAPSO ,SIGLAPSO  FROM FORMAPAG"));
        }


        private void centrosCosteA3ERP()
        {
            string anexoUdadNegocio = "";
            if (csGlobal.activarUnidadesNegocio)
            {
                anexoUdadNegocio = " ," + csGlobal.idUnidadNegocio + " as idUnidadNegocio";
            }
            //string query = " SELECT ltrim(CENTROCOSTE) AS CODERP, ltrim(CENTROCOSTE) as CODIGO, DESCCENTRO, RPST_ID_CENTROC AS ID_REPASAT, " +
            //                " CASE WHEN APLINIVEL1 = 'T' THEN 1 ELSE 0 END AS level1, " +
            //                " CASE WHEN APLINIVEL2 = 'T' THEN 1 ELSE 0 END AS level2, " +
            //                " CASE WHEN APLINIVEL3 = 'T' THEN 1 ELSE 0 END AS level3 " + anexoUdadNegocio +
            //                " FROM CENTROSC " +
            //                " WHERE BLOQUEADO = 'F'";
            string query = " SELECT ltrim(CENTROCOSTE) as CODIGOERP, DESCCENTRO AS DESCRIPCION, RPST_ID_CENTROC AS IDREPASAT, " +
                            " CASE WHEN APLINIVEL1 = 'T' THEN 1 ELSE 0 END AS 'LEVEL 1', " +
                            " CASE WHEN APLINIVEL2 = 'T' THEN 1 ELSE 0 END AS 'LEVEL 2', " +
                            " CASE WHEN APLINIVEL3 = 'T' THEN 1 ELSE 0 END AS 'LEVEL 3' " + anexoUdadNegocio +
                            " FROM CENTROSC " +
                            " WHERE BLOQUEADO = 'F'";
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3(query));
        }


        private void documentoPagoA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(DOCPAG) AS CODERP, LTRIM(DOCPAG) AS CODIGO, DESCDOC, RPST_ID_DOCPAGO AS ID_REPASAT,REMESABLE, RECIBIR from docupago"));
        }

        private void regimenesImpuestosA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT REGIVA AS CODERP,NOMRIVA,RPST_ID_REGIVA AS ID_REPASAT FROM REGIVA"));
        }

        private void tiposIVAA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("  SELECT TIPIVA AS CODERP, NOMTIVA, FORMAT(PORIVA, 'N2') AS '%', RPST_ID_IVA AS ID_REPASAT FROM TIPOIVA"));
        }

        private void transportistasA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT LTRIM(CODTRA) AS CODERP, LTRIM(CODTRA) AS CODIGO,NOMTRA AS TRANSPORTISTA,RPST_ID_TRANSPORT AS ID_REPASAT FROM TRANSPOR"));
        }

        private void cuentasContablesA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT CUENTA AS CODERP, DESCCUE,PLACON,RPST_ID_CTA, CUENTA AS CODIGO FROM CUENTAS where NIVEL=5 AND PLACON='NPGC' ORDER BY CUENTA"));
        }

        private void representantesA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODREP) AS CODERP, LTRIM(CODREP) AS CODIGO, NOMREP AS REPRESENTANTE, RPST_ID_REPRE AS ID_REPASAT,E_MAIL,TELREP AS TELEFONO from REPRESEN ORDER BY CODREP"));
        }

        private void rutasA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(RUTA) AS CODERP, LTRIM(RUTA) AS CODIGO, NOMRUTA AS RUTA, RPST_ID_RUTA AS ID_REPASAT from RUTAS ORDER BY RUTA"));
        }

        private void bancosA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT CODBAN AS CODERP, NOMBAN AS BANCO, CUENTA, RPST_ID_BANCO FROM BANCOS"));
        }
        private void proyectosA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("SELECT LTRIM(CENTROCOSTE) AS CODERP, DESCCENTRO AS PROYECTO, RPST_ID_CENTROC as RPST_ID_PROYECTO FROM CENTROSC WHERE APLINIVEL" + csGlobal.nivelProyecto + "='T'"));
        }


        private void zonasA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(ZONA) AS CODERP, LTRIM(ZONA) AS CODIGO, NOMZONA AS ZONA,  RPST_ID_ZONA AS ID_REPASAT from ZONAS ORDER BY ZONA"));
        }

        private void seriesA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(SERIE) AS CODERP, LTRIM(SERIE) AS CODIGO, UPPER(NOMSERIE) AS SERIE,  RPST_ID_SERIE AS ID_REPASAT from SERIES ORDER BY SERIE"));
        }

        private void almacenesA3ERP()
        {
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODALM) AS CODERP, LTRIM(CODALM) AS CODIGO, UPPER(DESCALM) AS ALMACEN,  RPST_ID_ALM AS ID_REPASAT from ALMACEN ORDER BY CODALM"));
        }

        private void familiaArtA3ERP()
        {
            string fam = csGlobal.familia_art;
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODCAR) AS CODERP, LTRIM(CODCAR) AS CODIGO, UPPER(DESCCAR) AS FAMILIA,  RPST_ID_CARACTERISTICA AS ID_REPASAT from CARACTERISTICAS where LTRIM(NUMCAR) = '"+ fam +"' and TIPCAR = 'A' ORDER BY CODCAR;"));
        }

        private void subfamiliaArtA3ERP()
        {
            string subfam = csGlobal.subfamilia_art;
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODCAR) AS CODERP, LTRIM(CODCAR) AS CODIGO, UPPER(DESCCAR) AS SUBFAMILIA,  RPST_ID_CARACTERISTICA AS ID_REPASAT from CARACTERISTICAS where LTRIM(NUMCAR) = '" + subfam + "' and TIPCAR = 'A' ORDER BY CODCAR;"));
        }

        private void marcaArtA3ERP()
        {
            string marca = csGlobal.marcas_art;
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODCAR) AS CODERP, LTRIM(CODCAR) AS CODIGO, UPPER(DESCCAR) AS MARCA,  RPST_ID_CARACTERISTICA AS ID_REPASAT from CARACTERISTICAS where LTRIM(NUMCAR) = '" + marca + "' and TIPCAR = 'A' ORDER BY CODCAR;"));
        }
        private void tipoArtA3ERP()
        {
            string tipo = csGlobal.tipo_art;
            csUtilidades.addDataSource(dgvA3ERPData, sql.cargarDatosTablaA3("select LTRIM(CODCAR) AS CODERP, LTRIM(CODCAR) AS CODIGO, UPPER(DESCCAR) AS TipoArt,  RPST_ID_CARACTERISTICA AS ID_REPASAT from CARACTERISTICAS where LTRIM(NUMCAR) = '" + tipo + "' and TIPCAR = 'A' ORDER BY CODCAR;"));
        }

        private void cuentasA3ERP(bool clientes, bool direcciones = false)
        {
            string anexoFiltroSync = "";
            if (rbSyncYes.Checked)
            {
                anexoFiltroSync = clientes ? " WHERE RPST_ID_CLI IS NOT NULL " : " WHERE RPST_ID_PROV IS NOT NULL ";
            }
            if (rbSyncNo.Checked)
            {
                anexoFiltroSync = clientes ? " WHERE RPST_ID_CLI IS NULL OR RPST_ID_CLI=0 " : " WHERE RPST_ID_PROV IS NULL OR RPST_ID_PROV=0 ";
            }


            if (clientes && !direcciones)
            {
                string selectQuery = "SELECT CODCLI, NOMCLI, NIFCLI, RPST_ID_CLI, CONVERT(varchar, CLIENTES.FECALTA, 103) AS FECHAALTA,  " +
                                        " CASE WHEN CLIENTES.RPST_SINCRONIZAR = 0 THEN 'NO' ELSE 'SI' END AS SYNC ,  " +
                                        " CONCAT(REPRESEN.RPST_ID_REPRE, ' | ', UPPER(REPRESEN.NOMREP), ' (', LTRIM(CLIENTES.CODREP), ')') as CIAL, " +
                                        " CONCAT(CLIENTES.DOCPAG,' (',DOCUPAGO.RPST_ID_DOCPAGO,')') AS DOCUPAGO, " +
                                        " CONCAT(CLIENTES.FORPAG, ' (', FORMAPAG.RPST_ID_FORMAPAG, ')') AS FORMAPAGO, " +
                                        " CONCAT(CLIENTES.RUTA,' (',RUTAS.RPST_ID_RUTA,')') AS RUTA, " +
                                        " CONCAT(CLIENTES.ZONA,' (',ZONAS.RPST_ID_ZONA,')') AS ZONA " +
                                        " FROM dbo.CLIENTES " +
                                        " LEFT JOIN REPRESEN ON CLIENTES.CODREP = REPRESEN.CODREP " +
                                        " LEFT JOIN FORMAPAG ON CLIENTES.FORPAG = FORMAPAG.FORPAG " +
                                        " LEFT JOIN DOCUPAGO ON CLIENTES.DOCPAG = DOCUPAGO.DOCPAG" +
                                        " LEFT JOIN RUTAS ON CLIENTES.RUTA = RUTAS.RUTA " +
                                        " LEFT JOIN ZONAS ON CLIENTES.ZONA = ZONAS.ZONA " +
                                         anexoFiltroSync +
                                        " ORDER BY CODCLI";

                csUtilidades.addDataSource(dgvAccountsA3ERP, sql.cargarDatosTablaA3(selectQuery));
            }
            else if (!clientes && !direcciones)
            {
                csUtilidades.addDataSource(dgvAccountsA3ERP, sql.cargarDatosTablaA3("SELECT CODPRO, NOMpro, NIFPRO, RPST_ID_PROV, CONVERT(varchar, FECALTA, 103) AS FECHAALTA, " +
                    " CASE WHEN RPST_SINCRONIZAR=0 THEN 'NO' ELSE 'SI' END AS SYNC FROM dbo.PROVEED" + anexoFiltroSync));
            }
            else if (clientes && direcciones)
            {
                csUtilidades.addDataSource(dgvAccountsA3ERP, sql.cargarDatosTablaA3("SELECT CONVERT(INT, IDDIRENT) AS IDDIRENT, RPST_ID_DIR, LTRIM(CODCLI) AS CODCLI, NOMENT, DIRENT1, DTOENT AS CP, POBENT,CODPAIS, CODPROVI  FROM DIRENT" + anexoFiltroSync));
            }
            else if (!clientes && direcciones)
            {
                csUtilidades.addDataSource(dgvAccountsA3ERP, sql.cargarDatosTablaA3("SELECT CONVERT(INT, IDDIRENT) AS IDDIRENT, RPST_ID_DIRPROV, LTRIM(CODPRO) AS CODPRO, NOMENT, DIRENT1, DTOENT AS CP, POBENT,CODPAIS, CODPROVI  FROM __DIRENTPRO" + anexoFiltroSync));
            }
        }




        private void Bancos()
        {
            string tipoObjeto = "";
            string filtroBancosEmpresa = "&filter_has[entities]=1";
            //string filtroExtDocNull = "&filter[codExternoRuta]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            //string filtroURL = filtroExtDocNull;
            string filtroURL = filtroBancosEmpresa;

            DataTable dtRoute = new DataTable();
            dtRoute.Columns.Add("IDREPASAT");
            dtRoute.Columns.Add("DESCRIPCION");
            dtRoute.Columns.Add("CODIGOERP");
            dtRoute.Columns.Add("CUENTA");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csRuta[] ruta = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "banks";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csBancs.RootObject>(json);
                    ruta = new Objetos.csRuta[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtRoute.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            ruta[ii] = new Objetos.csRuta();

                            ruta[ii].idRuta = repasat.data[ii].idDatosBancarios.ToString();
                            ruta[ii].codExternoRuta = repasat.data[ii].codExternoDatosBancarios;
                            ruta[ii].nomRuta = repasat.data[ii].nomDatosBancarios;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idDatosBancarios.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomDatosBancarios;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoDatosBancarios;
                            rowCarrier["CUENTA"] = repasat.data[ii].cuentaContableDatosBancarios;

                            dtRoute.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < ruta.Length; iiii++)
                        {
                            if (ruta[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csBancs.RootObject>(json);
                    }
                    dgvRepasatData.DataSource = dtRoute;

                }
                else
                {
                    toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                }
            }


        }

        private void Proyectos()
        {
            string tipoObjeto = "";
            //string filtroExtDocNull = "&filter[codExternoRuta]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = "";

            DataTable dtProject = new DataTable();
            dtProject.Columns.Add("IDREPASAT");
            dtProject.Columns.Add("DESCRIPCION");
            dtProject.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csProyecto[] proyecto = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "projects";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csProjects.RootObject>(json);
                    proyecto = new Objetos.csProyecto[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtProject.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            proyecto[ii] = new Objetos.csProyecto();

                            proyecto[ii].idProyecto = repasat.data[ii].idProyecto.ToString();
                            proyecto[ii].codExternoProyecto = repasat.data[ii].codExternoProyecto;
                            proyecto[ii].nomProyecto = repasat.data[ii].nomProyecto;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idProyecto.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomProyecto;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoProyecto;

                            dtProject.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < proyecto.Length; iiii++)
                        {
                            if (proyecto[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csProjects.RootObject>(json);
                    }
                    dgvRepasatData.DataSource = dtProject;

                }
                else
                {
                    toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                }
            }


        }


        private void Retenciones()
        {
            string tipoObjeto = "";
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = "";

            DataTable dtRetencion = new DataTable();
            dtRetencion.Columns.Add("IDREPASAT");
            dtRetencion.Columns.Add("DESCRIPCION");
            dtRetencion.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTiposRentencion[] retenciones = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "retentiontypes";

                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csRetentionTypes.RootObject>(json);
                    retenciones = new Objetos.csTiposRentencion[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowRetention = dtRetencion.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            retenciones[ii] = new Objetos.csTiposRentencion();

                            retenciones[ii].idTipoRentencion = repasat.data[ii].idTipoRetencion.ToString();
                            retenciones[ii].codExterno = repasat.data[ii].codTipoRetencion;
                            retenciones[ii].nomTipoRentencion = repasat.data[ii].nomTipoRetencion;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowRetention["IDREPASAT"] = repasat.data[ii].idTipoRetencion.ToString();
                            rowRetention["DESCRIPCION"] = repasat.data[ii].nomTipoRetencion;
                            rowRetention["CODIGOERP"] = repasat.data[ii].codTipoRetencion;

                            dtRetencion.Rows.Add(rowRetention);
                        }



                        check = false;
                        for (int iiii = 0; iiii < retenciones.Length; iiii++)
                        {
                            if (retenciones[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csRetentionTypes.RootObject>(json);
                    }
                    dgvRepasatData.DataSource = dtRetencion;

                }
                else
                {
                    toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                }
            }


        }



        private DataTable AlmacenesRPST()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoAlmacen]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtSerie = new DataTable();
            dtSerie.Columns.Add("IDREPASAT");
            dtSerie.Columns.Add("DESCRIPCION");
            dtSerie.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csSeries[] serieDoc = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "warehouses";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    serieDoc = new Objetos.csSeries[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowAlmacen = dtSerie.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            serieDoc[ii] = new Objetos.csSeries();

                            serieDoc[ii].idSerie = repasat.data[ii].idAlmacen.ToString();
                            serieDoc[ii].codExternoSerie = repasat.data[ii].codExternoAlmacen;
                            serieDoc[ii].nomSerie = repasat.data[ii].nomAlmacen;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowAlmacen["IDREPASAT"] = repasat.data[ii].idAlmacen.ToString();
                            rowAlmacen["DESCRIPCION"] = repasat.data[ii].nomAlmacen;
                            rowAlmacen["CODIGOERP"] = repasat.data[ii].codExternoAlmacen;

                            dtSerie.Rows.Add(rowAlmacen);
                        }



                        check = false;
                        for (int iiii = 0; iiii < serieDoc.Length; iiii++)
                        {
                            if (serieDoc[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    }
                    dgvRepasatData.DataSource = dtSerie;
                    return dtSerie;

                }
                else
                {
                    toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }
        }



        private void series()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoSerie]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtSerie = new DataTable();
            dtSerie.Columns.Add("IDREPASAT");
            dtSerie.Columns.Add("DESCRIPCION");
            dtSerie.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csSeries[] serie = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "seriesdocs";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
                    serie = new Objetos.csSeries[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtSerie.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            serie[ii] = new Objetos.csSeries();

                            serie[ii].idSerie = repasat.data[ii].idSerie.ToString();
                            serie[ii].codExternoSerie = repasat.data[ii].nomSerie.ToString();
                            serie[ii].nomSerie = repasat.data[ii].nomSerie;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idSerie.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomSerie;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].nomSerie;

                            dtSerie.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < serie.Length; iiii++)
                        {
                            if (serie[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
                    }
                    dgvRepasatData.DataSource = dtSerie;

                }
                else
                {
                    "No hay información para descargar".mb();
                }
            }
        }

        private void transportistasRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoTransportista]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtCarrier = new DataTable();
            dtCarrier.Columns.Add("IDREPASAT");
            dtCarrier.Columns.Add("DESCRIPCION");
            dtCarrier.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTransportistas[] transportista = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "carriers";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                    transportista = new Objetos.csTransportistas[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtCarrier.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            transportista[ii] = new Objetos.csTransportistas();

                            transportista[ii].idTransportista = repasat.data[ii].idTransportista.ToString();
                            transportista[ii].codExternoTransportista = repasat.data[ii].codExternoTransportista;
                            transportista[ii].nomTransportista = repasat.data[ii].nomTransportista;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idTransportista.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomTransportista;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoTransportista;

                            dtCarrier.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < transportista.Length; iiii++)
                        {
                            if (transportista[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                        contador = 0;
                    }
                    dgvRepasatData.DataSource = dtCarrier;

                }
                else
                {
                    "No hay información para descargar".mb();
                }
            }
        }


        private void centrosCosteRepasat()
        {
            string tipoObjeto = "";
            //string filtroExtDocNull = "&filter[codExternoCentroC]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroExtDocNull = "";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtCostCenters = new DataTable();
            dtCostCenters.Columns.Add("IDREPASAT");
            dtCostCenters.Columns.Add("DESCRIPCION");
            dtCostCenters.Columns.Add("CODIGOERP");
            dtCostCenters.Columns.Add("LEVEL 1");
            dtCostCenters.Columns.Add("LEVEL 2");
            dtCostCenters.Columns.Add("LEVEL 3");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csCostCenters[] centrosCoste = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "costcenters";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csCostCenters.RootObject>(json);
                    centrosCoste = new Objetos.csCostCenters[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtCostCenters.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            centrosCoste[ii] = new Objetos.csCostCenters();

                            centrosCoste[ii].idCentroCoste = repasat.data[ii].uuidCentroCoste.ToString();
                            centrosCoste[ii].codExternoCentroCoste = repasat.data[ii].codExternoCentroC;
                            centrosCoste[ii].nomCentroCoste = repasat.data[ii].descCentroCoste;
                            centrosCoste[ii].level1 = repasat.data[ii].level1.ToString();
                            centrosCoste[ii].level2 = repasat.data[ii].level2.ToString();
                            centrosCoste[ii].level3 = repasat.data[ii].level3.ToString();

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].uuidCentroCoste.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].descCentroCoste;
                            rowMethod["CODIGOERP"] = repasat.data[ii].codExternoCentroC;
                            rowMethod["LEVEL 1"] = repasat.data[ii].level1;
                            rowMethod["LEVEL 2"] = repasat.data[ii].level2;
                            rowMethod["LEVEL 3"] = repasat.data[ii].level3;

                            dtCostCenters.Rows.Add(rowMethod);
                        }

                        check = false;
                        for (int iiii = 0; iiii < centrosCoste.Length; iiii++)
                        {
                            if (centrosCoste[iiii] != null)
                                check = true;
                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csCostCenters.RootObject>(json);
                        contador = 0;
                    }
                    dgvRepasatData.DataSource = dtCostCenters;
                }
                else
                {
                    "No hay información para descargar".mb();
                }
            }
        }

        public static string ScrubHtml(string value)
        {
            var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
            var step2 = Regex.Replace(step1, @"\s{2,}", " ");
            return step2;
        }

        private void btnPaymentMethods_Click(object sender, EventArgs e)
        {
            informObjectType("paymentmethods");
            campoExternoRepasat = "codExternoFormaPago";

            tablaToUpdate = "FORMAPAG";
            campoKeyToUpdate = "FORPAG";
            repasatKeyField = "RPST_ID_FORMAPAG";

            dgvRepasatData.DataSource = loadRPSTObject.metodosPagoRepasat();
            metodosPagoA3ERP();
        }

        private void btnCarriers_Click(object sender, EventArgs e)
        {
            informObjectType("carriers");
            campoExternoRepasat = "codExternoTransportista";

            tablaToUpdate = "TRANSPOR";
            campoKeyToUpdate = "CODTRA";
            repasatKeyField = "RPST_ID_TRANSPORT";

            transportistasRepasat();
            dgvRepasatData.DataSource = loadRPSTObject.transportistas();
            transportistasA3ERP();
            btnCrearTranspotA3.Enabled = true;
        }

        private void btnCrearTranspotA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp los transportistas no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                crearTransportistasA3();
                dgvRepasatData.DataSource = loadRPSTObject.transportistas();
                transportistasA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnRepresentantes_Click(object sender, EventArgs e)
        {
            informObjectType("employees");
            campoExternoRepasat = "codExternoTrabajador";

            tablaToUpdate = "REPRESEN";
            campoKeyToUpdate = "CODREP";
            repasatKeyField = "RPST_ID_REPRE";

            dgvRepasatData.DataSource = loadRPSTObject.empleadosRepasat();
            representantesA3ERP();
        }

       
        private void btPaymentDocs_Click(object sender, EventArgs e)
        {
            informObjectType("paymentdocuments");
            campoExternoRepasat = "codExternoDocuPago";

            tablaToUpdate = "DOCUPAGO";
            campoKeyToUpdate = "DOCPAG";
            repasatKeyField = "RPST_ID_DOCPAGO";

            dgvRepasatData.DataSource = loadRPSTObject.documentosPagoRepasat();
            documentoPagoA3ERP();
            btnCrearDocsPagoA3.Enabled = true;
        }
        
        private void informObjectType(string objectType) {
            
            tipoObjeto = objectType;
            dgvA3ERPData.DataSource = null;
            dgvRepasatData.DataSource = null;
        }


        private void dgvA3ERPData_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }






        //////////////////////////////////////
        //////////////////////////////////////
        // INICIO DRAG & DROP ////////////////
        //////////////////////////////////////
        //////////////////////////////////////

        private Rectangle dragBoxFromMouseDown;

        private object valueFromMouseDown;

        private void dgvA3ERPData_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgvA3ERPData.DoDragDrop(valueFromMouseDown, DragDropEffects.Copy);
                }
            }
        }

        private void dgvA3ERPData_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            var hittestInfo = dgvA3ERPData.HitTest(e.X, e.Y);

            if (hittestInfo.RowIndex != -1 && hittestInfo.ColumnIndex != -1)
            {

                if (dgvA3ERPData.Columns[hittestInfo.ColumnIndex].Name == "CODERP")
                {
                    valueFromMouseDown = dgvA3ERPData.Rows[hittestInfo.RowIndex].Cells[hittestInfo.ColumnIndex].Value;
                    if (valueFromMouseDown != null)
                    {
                        // Remember the point where the mouse down occurred. 
                        // The DragSize indicates the size that the mouse can move 
                        // before a drag event should be started.                
                        Size dragSize = SystemInformation.DragSize;

                        // Create a rectangle using the DragSize, with the mouse position being
                        // at the center of the rectangle.
                        dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                    }
                }
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvRepasatData_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvRepasatData_DragDrop(object sender, DragEventArgs e)
        {
            string idRegistroRepasat = "";
            string codigoExterno = "";
            csRepasatWebService rpstWS = new csRepasatWebService();
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = dgvRepasatData.PointToClient(new Point(e.X, e.Y));

            // If the drag operation was a copy then add the row to the other control.
            if (e.Effect == DragDropEffects.Copy)
            {

                string cellvalue = e.Data.GetData(typeof(string)) as string;
                var hittest = dgvRepasatData.HitTest(clientPoint.X, clientPoint.Y);
                if (hittest.ColumnIndex != -1
                    && hittest.RowIndex != -1)
                {
                    if (dgvRepasatData.Columns[hittest.ColumnIndex].Name == "CODIGOERP" || (dgvRepasatData.Columns[hittest.ColumnIndex].Name == "SERIERECT"))
                    {
                        dgvRepasatData[hittest.ColumnIndex, hittest.RowIndex].Value = cellvalue;

                        //Actualizo Repasat
                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                        {
                            //Para enlazar las series para los abonos
                            if (dgvRepasatData.Columns[hittest.ColumnIndex].Name == "SERIERECT") campoExternoRepasat = "refSerie";
                            codigoExterno = cellvalue;
                            idRegistroRepasat = dgvRepasatData["IDREPASAT", hittest.RowIndex].Value.ToString();



                            if (csGlobal.sync_adv_mode)
                            {
                                Repasat.csTablasExternasAdv tablaExt = new Repasat.csTablasExternasAdv();
                                tablaExt.codigoExterno = codigoExterno;
                                tablaExt.idRegistroRepasat = idRegistroRepasat;
                                DataSet dsTablasExternas = new DataSet();
                                dsTablasExternas = syncronizarTablasAuxiliaresAdv(tablaExt);

                                rpstWS.sincronizarObjetoRepasat("externalauxiliarytables", "POST", dsTablasExternas.Tables["dtKeys"], dsTablasExternas.Tables["dtParams"], null);

                            }
                            else
                            {

                                rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, codigoExterno);

                                if (tipoObjeto == "banks")
                                {
                                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, "bank[cuentaContableDatosBancarios]", idRegistroRepasat, dgvA3ERPData.SelectedRows[0].Cells["CUENTA"].Value.ToString());
                                }


                                //actualizo A3ERP (excepto si estoy actualizando Series de Facturas Rectificativas
                                if (dgvRepasatData.Columns[hittest.ColumnIndex].Name == "SERIERECT")
                                {
                                    campoExternoRepasat = "codExternoSerie";
                                    //return; // No actualizo en A3ERP porque las series de facturas rectificativas se pueden utilizar en diferentes series de Repasat
                                }
                                string anexoCaracteristicas = "";
                                if(tipoObjeto == "producttypes")
                                {
                                    string tipoArt = csGlobal.tipo_art;
                                    anexoCaracteristicas = " and LTRIM(NUMCAR) = '" + tipoArt + "' and TIPCAR = 'A' ";
                                }
                                else if(tipoObjeto == "brands")
                                {
                                    string marca = csGlobal.marcas_art;
                                    anexoCaracteristicas = " and LTRIM(NUMCAR) = '" + marca + "' and TIPCAR = 'A' ";
                                }
                                else if(tipoObjeto == "families")
                                {
                                    string familia = csGlobal.familia_art;
                                    anexoCaracteristicas = " and LTRIM(NUMCAR) = '"+ familia +"' and TIPCAR = 'A' ";
                                }
                                 csUtilidades.ejecutarConsulta("UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "='" + idRegistroRepasat + "' WHERE LTRIM(" + campoKeyToUpdate + ")='" + codigoExterno.Trim() + "'" + anexoCaracteristicas, false);
                                
                            }

                        }
                    }
                }

            }
        }



        private DataSet syncronizarTablasAuxiliaresAdv(Repasat.csTablasExternasAdv tablaExt) {
            try
            {
                string tipoValorTabla = "";
                switch (tipoObjeto)
                {
                    case "paymentdocuments":
                        tipoValorTabla = "paymentdocuments";
                        break;
                    case "paymentmethods":
                        tipoValorTabla = "paymentmethods";
                        break;
                    case "carriers":
                        tipoValorTabla = "carriers";
                        break;
                    case "routes":
                        tipoValorTabla = "routes";
                        break;
                    case "geozones":
                        tipoValorTabla = "geozones";
                        break;
                    case "seriesdocs":
                        tipoValorTabla = "seriesdocs";
                        break;
                    case "warehouses":
                        tipoValorTabla = "warehouses";
                        break;
                    case "costcenters":
                        tipoValorTabla = "costcenters";
                        break;
                    case "banks":
                        tipoValorTabla = "banks";
                        break;

                }



                DataSet dataSet = new DataSet();

                DataTable dtKeys = new DataTable();
                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");
                DataRow drKeys = dtKeys.NewRow();
                drKeys["KEY"] = tablaExt.codigoExterno;
                dtKeys.Rows.Add(drKeys);

                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataRow drParams = dtParams.NewRow();
                drParams["FIELD"] = "tipoValor";
                drParams["KEY"] = tablaExt.codigoExterno;
                drParams["VALUE"] = tipoValorTabla;
                dtParams.Rows.Add(drParams);

                drParams = dtParams.NewRow();
                drParams["FIELD"] = "idExterno";
                drParams["KEY"] = tablaExt.codigoExterno;
                drParams["VALUE"] = tablaExt.codigoExterno;
                dtParams.Rows.Add(drParams);

                drParams = dtParams.NewRow();
                drParams["FIELD"] = "idRepasat";
                drParams["KEY"] = tablaExt.codigoExterno;
                drParams["VALUE"] = tablaExt.idRegistroRepasat;
                dtParams.Rows.Add(drParams);

                drParams = dtParams.NewRow();
                drParams["FIELD"] = "idConExt";
                drParams["KEY"] = tablaExt.codigoExterno;
                drParams["VALUE"] = csGlobal.id_conexion_externa;
                dtParams.Rows.Add(drParams);

                dataSet.Tables.Add(dtKeys);
                dataSet.Tables.Add(dtParams);
                return dataSet;

            }
            catch (Exception ex) {
                return null;
            }

        }


        //////////////////////////////////////
        //////////////////////////////////////
        // FIN DRAG & DROP ////////////////
        //////////////////////////////////////
        //////////////////////////////////////




        private void btSendCustomersA3ERPToRepasat_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "SINCRONIZANDO";
            syncroData(true, false, true);
            toolStripStatusLabel4.Text = "PROCESO FINALIZADO";
        }

        private void btSendCustomersRepasatToA3ERP_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "SINCRONIZANDO";
            syncroData(true, false, false);
            toolStripStatusLabel4.Text = "PROCESO FINALIZADO";
        }


        /// <summary>
        /// Función para crear o actualizar artículos con origen A3ERP
        /// Se pasa parámetro si queremos crear o actualizar
        /// </summary>
        /// <param name="update"></param>
        public void crearArticulosDeA3ERPToRepasat(bool update = false, string fechaUltActualizacion = null)
        {
            try
            {
                Repasat.csProducts productsRPST = new Repasat.csProducts();
                DataSet dtsAccounts = new DataSet();
                DataTable dtProducts = new DataTable();
                dtsAccounts.Tables.Add(dtProducts);

                //Si es actualización vamos a buscar la última fecha sincronizada
                fechaUltActualizacion = update ? sql.obtenerCampoTabla("SELECT MAX(DATE_SYNC) FROM KLS_REPASAT_UPDATE WHERE TABLA='PRODUCTS'") : "";

                foreach (DataGridViewColumn column in dgvAccountsA3ERP.Columns)
                    dtProducts.Columns.Add(column.Name); //better to have cell type

                //Añadir modo automatico
                if (!csGlobal.modoManual)
                {
                    csSqlScripts sqlScript = new csSqlScripts();
                    csSqlConnects sqlConnect = new csSqlConnects();

                    string selectProducts = sqlScript.selectProductosFromA3ToRepasat(update, fechaUltActualizacion);
                    dtProducts = sqlConnect.obtenerDatosSQLScript(selectProducts);
                }
                else { 
                    for (int i = 0; i < dgvAccountsA3ERP.SelectedRows.Count; i++)
                    {
                        DataRow row = dtProducts.NewRow();
                        //dtProducts.Rows.Add();
                        foreach (DataGridViewColumn column in dgvAccountsA3ERP.Columns)
                        {
                            row[column.Name] = dgvAccountsA3ERP.SelectedRows[i].Cells[column.Name].Value;
                        }
                        dtProducts.Rows.Add(row);
                    }
                }


                dtsAccounts = productsRPST.uploadProductosA3ToRepasat(update, fechaUltActualizacion, dtProducts);

                if (dtsAccounts != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    if (!update)    //Para creación de artículos en Repasat desde ERP
                    {
                        rpstWS.sincronizarObjetoRepasat("products", "POST", dtsAccounts.Tables["dtKeys"], dtsAccounts.Tables["dtParams"], "", false);
                    }
                    else               // Para actualizar datos en Repasat desde ERP
                    {
                        rpstWS.sincronizarObjetoRepasat("products", "PUT", dtsAccounts.Tables["dtKeys"], dtsAccounts.Tables["dtParams"], null, false, true);
                    }
                }

            }
            catch (InvalidOperationException ex) { }
        }

        private void btnSendProductsToRepasat_Click(object sender, EventArgs e)
        {

        }

        private void btnSyncStock_Click(object sender, EventArgs e)
        {

            DialogResult Resultado = MessageBox.Show("¿Quieres actualizar el stock?", "Actualización de Stock", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                inicioProceso();
                Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
                updateDataRPST.actualizarStockA3ERPToRepasat(rbtnSelectProductsMovs.Checked, rbtnAllProducts.Checked, (int)numUpdownMovDays.Value);
                inicioProceso(false);
            }

        }



        //public void actualizarStockA3ERPToRepasat()
        //{
        //    Utilidades.csLogErrores logProceso = new Utilidades.csLogErrores();
        //    try
        //    {
        //        csSqlConnects sql = new csSqlConnects();
        //        DataTable dtStock = new DataTable();
        //        DataSet datasetRPST = new DataSet();
        //        string rowKey = "";
        //        string anexoQuery = string.Empty;
        //        string[] almacenes = null;
        //        string anexoAlmacenes = "";

        //        // Contador para el número de registros que se van a actualizar
        //        int totalRows = 0;
        //        int successfulUpdates = 0;

        //        // Definición del anexo de almacenes según el formato en csGlobal.almacenA3
        //        if (csGlobal.almacenA3.Contains(","))
        //        {
        //            almacenes = csGlobal.almacenA3.Split(',');
        //            anexoAlmacenes += " and (LTRIM(STOCKALM.CODALM) in (";
        //            for (int i = 0; i < almacenes.Length; i++)
        //            {
        //                anexoAlmacenes += " '" + almacenes[i] + "'";
        //                if (i < almacenes.Length - 1)
        //                {
        //                    anexoAlmacenes += ", ";
        //                }
        //            }
        //            anexoAlmacenes += ")) ";
        //        }
        //        else
        //        {
        //            anexoAlmacenes = " and (LTRIM(STOCKALM.CODALM) in ('" + csGlobal.almacenA3 + "')) ";
        //        }


        //        if (!csGlobal.modoManual) 
        //        {
        //            anexoQuery = @"INNER JOIN (
        //                    SELECT CODART
        //                    FROM MOVSTOCKS
        //                    WHERE FECDOC > GETDATE()-5
        //                    GROUP BY CODART
        //                ) AS MovimientosRecientes ON dbo.STOCKALM.CODART = MovimientosRecientes.CODART";
        //        }
        //        else if (csGlobal.modoManual && rbtnSelectProductsMovs.Checked)  
        //        {
        //            anexoQuery = @"INNER JOIN (
        //                    SELECT CODART
        //                    FROM MOVSTOCKS
        //                    WHERE FECDOC > GETDATE()-" + numUpdownMovDays.Value.ToString() + @"
        //                    GROUP BY CODART
        //                ) AS MovimientosRecientes ON dbo.STOCKALM.CODART = MovimientosRecientes.CODART";
        //        }
        //        else  
        //        {
        //            anexoQuery = ""; 
        //        }

        //        string query = @"SELECT
        //                    dbo.STOCKALM.CODART,
        //                    dbo.ARTICULO.RPST_ID_PROD,
        //                    SUM(dbo.STOCKALM.UNIDADES) AS cantidadArticulo
        //                FROM
        //                    dbo.STOCKALM
        //                INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART
        //                " + anexoQuery + @"
        //                WHERE
        //                    dbo.ARTICULO.RPST_ID_PROD > 0
        //                    AND dbo.ARTICULO.BLOQUEADO = 'F' " + anexoAlmacenes + @"
        //                GROUP BY
        //                    dbo.STOCKALM.CODART,
        //                    dbo.ARTICULO.RPST_ID_PROD";

        //        dtStock = sql.obtenerDatosSQLScript(query);

        //        if (csGlobal.modoManual && rbtnAllProducts.Checked)
        //        {
        //            dtStock = sql.obtenerDatosSQLScript("select ARTICULO.CODART, dbo.ARTICULO.RPST_ID_PROD, SUM(ISNULL(STOCKALM.UNIDADES, 0)) AS cantidadArticulo " +
        //                " from ARTICULO left outer join STOCKALM  on ARTICULO.codart = STOCKALM.CODART " + anexoAlmacenes +
        //                " where (dbo.ARTICULO.RPST_ID_PROD > 0) AND dbo.ARTICULO.BLOQUEADO = 'F' GROUP BY ARTICULO.CODART, ARTICULO.RPST_ID_PROD; ");
        //        }

        //        if (dtStock != null)
        //        {
        //            totalRows = dtStock.Rows.Count; // Contamos el total de filas obtenidas
        //            logProceso.guardarLogProceso($"Total de líneas obtenidas: {totalRows}");

        //            // Preparar tablas dtParams y dtKeys
        //            DataTable dtParams = new DataTable();
        //            dtParams.TableName = "dtParams";
        //            dtParams.Columns.Add("FIELD");
        //            dtParams.Columns.Add("KEY");
        //            dtParams.Columns.Add("VALUE");

        //            DataTable dtKeys = new DataTable();
        //            dtKeys.TableName = "dtKeys";
        //            dtKeys.Columns.Add("KEY");

        //            foreach (DataRow drFilaStock in dtStock.Rows)
        //            {
        //                rowKey = drFilaStock["RPST_ID_PROD"].ToString();
        //                DataRow drKeys = dtKeys.NewRow();
        //                drKeys["KEY"] = rowKey;
        //                dtKeys.Rows.Add(drKeys);

        //                DataRow drParams = dtParams.NewRow();
        //                drParams["FIELD"] = "cantidadArticulo";
        //                drParams["KEY"] = rowKey;
        //                drParams["VALUE"] = drFilaStock["cantidadArticulo"];
        //                dtParams.Rows.Add(drParams);
        //            }

        //            datasetRPST.Tables.Add(dtKeys);
        //            datasetRPST.Tables.Add(dtParams);

        //            // Llamada al servicio web de Repasat
        //            csRepasatWebService rpstWS = new csRepasatWebService();
        //            rpstWS.sincronizarObjetoRepasat("products", "PUT", datasetRPST.Tables["dtKeys"], datasetRPST.Tables["dtParams"], "");
        //        }
        //        else
        //        {
        //            logProceso.guardarLogProceso("No se obtuvieron datos de stock." + "\n Variable dtStock: " + dtStock.Rows.Count);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logProceso.guardarLogProceso($"Error en la actualización de stock: {ex.ToString()}");
        //    }
        //}


        //public void actualizarStockA3ERPToRepasat()
        //{
        //    try
        //    {
        //        DataTable dtStock = new DataTable();
        //        csSqlConnects sql = new csSqlConnects();
        //        DataSet datasetRPST = new DataSet();
        //        string rowKey = "";
        //        string anexoQuery = string.Empty;
        //        string[] almacenes = null;
        //        string anexoAlmacenes = "";

        //        if (csGlobal.almacenA3.Contains(","))
        //        {
        //            almacenes = csGlobal.almacenA3.Split(',');
        //            anexoAlmacenes += " and (LTRIM(STOCKALM.CODALM) in (";
        //            for (int i = 0; i < almacenes.Length; i++)
        //            {
        //                anexoAlmacenes += " '" + almacenes[i] + "'";
        //                if (i < almacenes.Length - 1)
        //                {
        //                    anexoAlmacenes += ", ";
        //                }
        //            }
        //            anexoAlmacenes += ")) ";
        //        }
        //        else
        //        {
        //            anexoAlmacenes = " and (LTRIM(STOCKALM.CODALM) in ('" + csGlobal.almacenA3 + "')) ";
        //        }

        //        if (!csGlobal.modoManual)
        //        {
        //            anexoQuery = "AND dbo.STOCKALM.CODART IN (SELECT CODART FROM MOVSTOCKS WHERE FECDOC > GETDATE()-5 GROUP BY CODART)";
        //        }
        //        else if (csGlobal.modoManual && rbtnSelectProductsMovs.Checked)
        //        {
        //            anexoQuery = "AND dbo.STOCKALM.CODART IN (SELECT CODART FROM MOVSTOCKS WHERE FECDOC > GETDATE()-" + numUpdownMovDays.Value.ToString() + " GROUP BY CODART)";
        //        }

        //        dtStock = sql.obtenerDatosSQLScript("SELECT " +
        //            " dbo.STOCKALM.CODART, dbo.ARTICULO.RPST_ID_PROD, SUM(dbo.STOCKALM.UNIDADES) AS cantidadArticulo " +
        //            " FROM   " +
        //            " dbo.STOCKALM INNER JOIN " +
        //            " dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
        //            " WHERE  " +
        //            " (dbo.ARTICULO.RPST_ID_PROD > 0) " +
        //            " AND dbo.ARTICULO.BLOQUEADO='F' " + anexoAlmacenes + anexoQuery +
        //            " GROUP BY dbo.STOCKALM.CODART, dbo.ARTICULO.RPST_ID_PROD");

        //        if (csGlobal.modoManual && rbtnAllProducts.Checked)
        //        {
        //            dtStock = sql.obtenerDatosSQLScript("select ARTICULO.CODART, dbo.ARTICULO.RPST_ID_PROD, SUM(ISNULL(STOCKALM.UNIDADES, 0)) AS cantidadArticulo " +
        //                " from ARTICULO left outer join STOCKALM  on ARTICULO.codart = STOCKALM.CODART " + anexoAlmacenes +
        //                " where (dbo.ARTICULO.RPST_ID_PROD > 0) AND dbo.ARTICULO.BLOQUEADO = 'F' GROUP BY ARTICULO.CODART, ARTICULO.RPST_ID_PROD; ");
        //        }

        //        DataTable dtParams = new DataTable();
        //        dtParams.TableName = "dtParams";
        //        dtParams.Columns.Add("FIELD");
        //        dtParams.Columns.Add("KEY");
        //        dtParams.Columns.Add("VALUE");

        //        DataTable dtKeys = new DataTable();
        //        dtKeys.TableName = "dtKeys";
        //        dtKeys.Columns.Add("KEY");


        //        foreach (DataRow drFilaStock in dtStock.Rows)
        //        {
        //            rowKey = drFilaStock["RPST_ID_PROD"].ToString();
        //            DataRow drKeys = dtKeys.NewRow();
        //            drKeys["KEY"] = rowKey;
        //            dtKeys.Rows.Add(drKeys);

        //            DataRow drParams = dtParams.NewRow();
        //            drParams["FIELD"] = "cantidadArticulo";
        //            drParams["KEY"] = rowKey;
        //            drParams["VALUE"] = drFilaStock["cantidadArticulo"];
        //            dtParams.Rows.Add(drParams);
        //        }

        //        datasetRPST.Tables.Add(dtKeys);
        //        datasetRPST.Tables.Add(dtParams);

        //        cantidadArticulo

        //        csRepasatWebService rpstWS = new csRepasatWebService();
        //        rpstWS.sincronizarObjetoRepasat("products", "PUT", datasetRPST.Tables["dtKeys"], datasetRPST.Tables["dtParams"], "");
        //    }
        //    catch (Exception ex) { }
        //}

        private void btnRutas_Click(object sender, EventArgs e)
        {
            informObjectType("routes");
            tablaToUpdate = "RUTAS";
            campoKeyToUpdate = "RUTA";
            repasatKeyField = "RPST_ID_RUTA";
            campoExternoRepasat = "codExternoRuta";
            dgvRepasatData.DataSource = loadRPSTObject.Rutas();
            rutasA3ERP();
            btnCrearRutasA3.Enabled = true;
        }

        private void btnLoadAccountsERP_Click(object sender, EventArgs e)
        {
            inicioProceso();
            syncroData(false, true, false);
            inicioProceso(false);
        }

        private void borrarSincronizacion()
        {
            try {
                int contador = 1;
                int contador2 = 1;
                string updateAddres = "";
                string updateQuery = "";
                string objetoCuenta = "";
                if (cboxMaestros.SelectedItem.ToString() == "Clientes")
                {
                    objetoCuenta = "CODCLI";
                    updateQuery = "UPDATE __CLIENTES SET RPST_ID_CLI = NULL WHERE LTRIM(CODCLI) = '";
                    updateAddres = "UPDATE DIRENT SET RPST_ID_DIR = NULL WHERE LTRIM(CODCLI) = '";
                }
                else if (cboxMaestros.SelectedItem.ToString() == "Proveedores")
                {
                    objetoCuenta = "CODPRO";
                    updateQuery = "UPDATE __PROVEED SET RPST_ID_PROV= NULL WHERE LTRIM(CODPRO) = '";
                    updateAddres = "UPDATE __DIRENTPRO SET RPST_ID_DIRPROV = NULL WHERE LTRIM(CODPRO) = '";
                }
                else if (cboxMaestros.SelectedItem.ToString() == "Productos")
                {
                    objetoCuenta = "CODIGO";
                    updateQuery = "UPDATE ARTICULO SET RPST_ID_PROD= NULL WHERE LTRIM(CODART) = '";
                }



                DialogResult Resultado;
                Resultado = MessageBox.Show("¿Quiere borrar la sincronización?", "Borrar Sincronización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.Yes)
                {
                    foreach (DataGridViewRow fila in dgvAccountsA3ERP.SelectedRows)
                    {
                        csUtilidades.ejecutarConsulta(updateQuery + fila.Cells[objetoCuenta].Value.ToString().Trim() + "'", false);
                        if (objetoCuenta == "CODCLI" || objetoCuenta == "CODPRO") //SOLO RESETEO DIRECCIONES CUANDO SON CLIENTES O PROVEEDORES
                        {
                            csUtilidades.ejecutarConsulta(updateAddres + fila.Cells[objetoCuenta].Value.ToString().Trim() + "'", false);
                        }
                    }
                    MessageBox.Show("Proceso finalizado");
                    if (cboxMaestros.SelectedText == "Clientes")
                    {
                        cuentasA3ERP(true);
                    }
                    else if (cboxMaestros.SelectedText == "Proveedores")
                    {
                        cuentasA3ERP(false);
                    }
                    else if (cboxMaestros.SelectedText == "Productos")
                    {
                        syncroData(false, true, false);
                        establecerColumnaImagen();
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

        }

        private void borrarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            borrarSincronizacion();
        }

        private void btnZonas_Click(object sender, EventArgs e)
        {

            informObjectType("geozones");
            tablaToUpdate = "ZONAS";
            campoKeyToUpdate = "ZONA";
            repasatKeyField = "RPST_ID_ZONA";
            campoExternoRepasat = "codExternoZonaGeo";

            dgvRepasatData.DataSource = loadRPSTObject.zonas();
            zonasA3ERP();

            btnCrearZonasA3.Enabled = true;
        }

        private void btnUploadRutas_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las Rutas no asignadas, ¿está seguro/a?", "Sinronizar Rutas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarRutasA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.Rutas();
                rutasA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnCrearRutasA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp las Rutas no asignadas, ¿está seguro/a?", "Sinronizar Rutas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                crearRutasA3();
                dgvRepasatData.DataSource = loadRPSTObject.Rutas();
                rutasA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnRepresen_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat los empleados no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarEmpleadosA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.empleadosRepasat();
                representantesA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }


        //Borrar sincronización datos auxiliares
        private void borrarSincronizaciónToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string cuenta = "";
            string updateReg = "";
            string updateAddres = "";
            string objetoCuenta = "";

            updateReg = "UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "=NULL WHERE LTRIM(" + campoKeyToUpdate + ")='";

            DialogResult Resultado;
            Resultado = MessageBox.Show("¿Quiere borrar la sincronización?", "Borrar Sincronización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                foreach (DataGridViewRow fila in dgvA3ERPData.SelectedRows)
                {
                    csUtilidades.ejecutarConsulta(updateReg + fila.Cells["CODIGO"].Value.ToString().Trim() + "'", false);
                }
                MessageBox.Show("Proceso finalizado");
                cuentasA3ERP(objetoCliente());
            }
        }

        private void btnMetPago_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat los métodos de pago no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarMetodosPago();
                dgvRepasatData.DataSource = loadRPSTObject.metodosPagoRepasat();
                metodosPagoA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnDocsPago_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat los documentos de pago no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarDocumentosPago();
                dgvRepasatData.DataSource = loadRPSTObject.documentosPagoRepasat();
                documentoPagoA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnCrearDocsPagoA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp los documentos de pago no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                crearDocumentosPagoA3();
                dgvRepasatData.DataSource = loadRPSTObject.documentosPagoRepasat();
                documentoPagoA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnLoadProducts_Click(object sender, EventArgs e)
        {

        }

        private void loadProductsERP()
        {
            string anexoObsoletos = "";
            anexoObsoletos = rbObsoletYes.Checked ? " AND OBSOLETO='T'" : anexoObsoletos;
            anexoObsoletos = rbObsoletNo.Checked ? " AND OBSOLETO='F'" : anexoObsoletos;

            string anexoEstado = "";
            anexoEstado = rbActiveYes.Checked ? " AND BLOQUEADO = 'F' " : anexoEstado;
            anexoEstado = rbActiveNo.Checked ? " AND BLOQUEADO = 'T' " : anexoEstado;

            string anexoSyncro = "";
            anexoSyncro = rbSyncNo.Checked ? " AND (RPST_ID_PROD IS NULL OR RPST_ID_PROD = 0) " : anexoSyncro;
            anexoSyncro = rbSyncYes.Checked ? " AND RPST_ID_PROD>0 " : anexoSyncro;

            string anexo_select_familia = csGlobal.familia_art == null || csGlobal.familia_art == "" ? "" : " C" + csGlobal.familia_art + ".RPST_ID_CARACTERISTICA AS idFam, ";
            string anexo_where_familia = csGlobal.familia_art == null || csGlobal.familia_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.familia_art + " ON CAR" + csGlobal.familia_art + " = C" + csGlobal.familia_art + ".CODCAR and C" + csGlobal.familia_art + ".TIPCAR='A' AND C" + csGlobal.familia_art + ".NUMCAR='" + csGlobal.familia_art + "' ";
            string anexo_select_subfamilia = csGlobal.subfamilia_art == null || csGlobal.subfamilia_art == "" ? "" : " C" + csGlobal.subfamilia_art + ".RPST_ID_CARACTERISTICA AS idSubFam, ";
            string anexo_where_subfamilia = csGlobal.subfamilia_art == null || csGlobal.subfamilia_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.subfamilia_art + " ON CAR" + csGlobal.subfamilia_art + " = C" + csGlobal.subfamilia_art + ".CODCAR and C" + csGlobal.subfamilia_art + ".TIPCAR='A' AND C" + csGlobal.subfamilia_art + ".NUMCAR='" + csGlobal.subfamilia_art + "' ";
            string anexo_select_marca = csGlobal.marcas_art == null || csGlobal.marcas_art == "" ? "" : " C" + csGlobal.marcas_art + ".RPST_ID_CARACTERISTICA AS uuidMarca, ";
            string anexo_where_marca = csGlobal.marcas_art == null || csGlobal.marcas_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.marcas_art + " ON CAR" + csGlobal.marcas_art + " = C" + csGlobal.marcas_art + ".CODCAR and C" + csGlobal.marcas_art + ".TIPCAR='A' AND C" + csGlobal.marcas_art + ".NUMCAR='" + csGlobal.marcas_art + "' ";
            string anexo_filtroConstante = csGlobal.filtro_art == null || csGlobal.filtro_art == "" ? "" : csGlobal.filtro_art;
            //09/02/2024 AÑADO ANEXO PARA EL TIPO PRODUCTO
            string anexo_select_tipo = csGlobal.tipo_art == null || csGlobal.tipo_art == "" ? "" : " C"+ csGlobal.tipo_art + ".RPST_ID_CARACTERISTICA AS idTipoArticulo, ";
            string anexo_where_tipo = csGlobal.tipo_art == null || csGlobal.tipo_art == "" ? "" : " left outer JOIN CARACTERISTICAS C"+ csGlobal.tipo_art + " ON CAR" + csGlobal.tipo_art + " = C" + csGlobal.tipo_art + ".CODCAR and C" + csGlobal.tipo_art + ".TIPCAR='A' AND C" + csGlobal.tipo_art + ".NUMCAR='" + csGlobal.tipo_art + "' ";

            string mayusculas = csGlobal.nombreServidor.Contains("PURE") || csGlobal.nombreServidor.Contains("PRUEBAS") ? " (ARTALIAS) as aliasArticulo, " : " UPPER(ARTALIAS) as aliasArticulo, ";

            string queryProducts = "SELECT LTRIM(A.CODART) as CODIGO, RPST_ID_PROD AS REPASAT_ID,DESCART as nomArticulo, " +
                 " CASE WHEN A.RPST_SINCRONIZAR=1 then 'SI' ELSE 'NO' END AS SYNC, " +
                 //COMROBAMOS SI HAY UN PRECIO EN LA TARIFA SINO PONEMOS EL ORIGINAL
                    " CASE WHEN T.PRECIO=0 OR T.PRECIO IS NULL then PRCVENTA ELSE T.PRECIO END AS precioV, " +
                    " PRCCOMPRA as precioC, " +
                    anexo_select_familia + anexo_select_subfamilia + anexo_select_tipo + anexo_select_marca +
                    " CASE WHEN ESCOMPRA ='T' THEN '1' ELSE '0' END as compraArticulo,  " +
                    " CASE WHEN ESVENTA ='T' THEN '1' ELSE '0' END as ventaArticulo,  CASE WHEN BLOQUEADO ='T' THEN '0' ELSE '1' END as activoArticulo,  " +
                    " CASE WHEN AFESTOCK ='T' THEN '1' ELSE '0' END as stockArticulo,  CASE WHEN OBSOLETO ='T' THEN 'SI' ELSE 'NO' END as obsoletoArticulo,  FECALTA AS FECHAALTA, " +

                    mayusculas +

                    " LTRIM(A.CODART) as codExternoArticulo, LTRIM(RPST_ID_PROD) AS idArticulo, LTRIM(A.CODART) AS refArticulo " +
                    " FROM ARTICULO A " +
                    anexo_where_familia +
                    anexo_where_subfamilia +
                    anexo_where_tipo +
                    anexo_where_marca +
                    " left outer JOIN TARIFAVE T ON A.CODART = T.CODART and T.TARIFA = 8 " +
                    " WHERE A.CODART IS NOT NULL " + anexoObsoletos + anexoEstado + anexoSyncro + anexo_filtroConstante +
                    //" WHERE RPST_ID_PROD IS NULL OR RPST_ID_PROD = 0  " +
                    " ORDER BY A.CODART";
            csUtilidades.addDataSource(dgvAccountsA3ERP, sql.cargarDatosTablaA3(queryProducts));
        }

        private void btnUploadZonas_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las Zonas no asignadas, ¿está seguro/a?", "Sinronizar Zonas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarZonasA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.zonas();
                zonasA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnCrearZonasA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp las Zonas no asignadas, ¿está seguro/a?", "Sinronizar Zonas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {

                crearZonaA3();
                dgvRepasatData.DataSource = loadRPSTObject.zonas();
                zonasA3ERP();
                MessageBox.Show("Proceso finalizado");

            }
        }

        private void btnLoadAccountsRepasat_Click(object sender, EventArgs e)
        {
            statusLabel.Text = "P R O C E S A N D O";
            statusLabel.BackColor = Color.Orange;
            this.Refresh();
            syncroData(false, true, true);
            establecerColumnaImagen();
            tssLabelTotalRowsRPST.Text = dgvAccountsRepasat.Rows.Count.ToString() + " Filas";
            statusLabel.Text = "PROCESO FINALIZADO";
            statusLabel.BackColor = SystemColors.Window;
        }


        private string filtroDocumentosCarga()
        {
            string filtroDocs = "TODOS";

            filtroDocs = rbSyncDocNo.Checked ? "NO" : filtroDocs;
            filtroDocs = rbSyncDocYes.Checked ? "SI" : filtroDocs;

            return filtroDocs;
        }


        private void deleteSyncRPSTAccounts()
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("¿Quiere borrar la sincronización de  " + (dgvAccountsRepasat.SelectedRows.Count - 1) + " registros en REPASAT?", "Borrar Sincronización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                int contador = 1;
                string idAccount = "";
                progress.Maximum = dgvAccountsRepasat.SelectedRows.Count;
                csRepasatWebService rpstWS = new klsync.csRepasatWebService();
                foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                {
                    progress.Value = contador;
                    idAccount = fila.Cells["IDREPASAT"].Value.ToString();
                    rpstWS.actualizarDocumentoRepasat("accounts", "codExternoCli", idAccount, "");
                    contador++;
                }
                "Proceso finalizado".mb();
                dgvAccountsRepasat.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, null, false, false, false);
            }

        }

        private void borrarSincronizaciónToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            deleteSyncRPSTAccounts();
        }

        private void btnRPSTAddress_Click(object sender, EventArgs e)
        {


            statusLabel.Text = "P R O C E S A N D O";
            statusLabel.BackColor = Color.Orange;
            this.Refresh();
            dgvAccountsRepasat.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, tbAccountId.Text, true, false, false, true);
            tipoObjeto = "accountaddresses";
            tablaToUpdate = objetoCliente() ? "DIRENT" : "__DIRENTPROV";
            campoKeyToUpdate = "IDDIRENT";
            repasatKeyField = objetoCliente() ? "RPST_ID_DIRP" : "RPST_ID_DIRPROV";
            campoExternoRepasat = "address[codExternoDireccion]";

            statusLabel.Text = "PROCESO FINALIZADO";
            statusLabel.BackColor = SystemColors.Window;

        }

        private DataTable cargarDatosCuentasRepasat(bool clientes, bool consulta = false, string idAccount = null, bool checkDirecciones = false, bool pendientes = false, bool update = false, bool loadDirecciones = false, bool validationProcess = false)
        {

            int errorNumPagina = 0;
            try
            {
                DataSet dtset = new DataSet();
                DataTable dt = new DataTable();
                dtset = updateDataRPST.traspasarCuentasRepasatToA3(clientes, consulta, idAccount, checkDirecciones, pendientes, update, rbSyncYes.Checked, false, null, true);

                if (dtset != null)
                {

                    if (dtset.Tables.Count != 0 && !loadDirecciones)
                    {
                        //return dt = updateDataRPST.traspasarCuentasRepasatToA3(clientes, consulta, idAccount, checkDirecciones, pendientes, update, rbSyncYes.Checked).Tables[0];
                        return dt = dtset.Tables[0];
                    }
                    else if (loadDirecciones)
                    {
                        return dt = updateDataRPST.traspasarCuentasRepasatToA3(clientes, consulta, idAccount, checkDirecciones, pendientes, update, rbSyncYes.Checked).Tables[1];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual && csGlobal.modeDebug)
                {
                    ex.Message.mb();
                }
                return null;
            }
        }

        private void cargarDireccionesCuentasrepasat(bool clientes, bool consulta = false, string idAccount = null, bool pendientes = false, bool update = false)
        {
            updateDataRPST.gestionarDireccionesA3ToRepasat(null, clientes, update);
        }

        //////////////////////////////////////
        //////////////////////////////////////
        // INICIO DRAG & DROP ////////////////
        //////////////////////////////////////
        //////////////////////////////////////


        private string valorFromMouseDown;


        private void dgvAccountsA3ERP_MouseMove(object sender, MouseEventArgs e)
        {

            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgvAccountsA3ERP.DoDragDrop(valorFromMouseDown, DragDropEffects.Copy);
                }
            }
        }


        private void dgvAccountsA3ERP_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            var hittestInfo = dgvAccountsA3ERP.HitTest(e.X, e.Y);

            if (hittestInfo.RowIndex != -1 && hittestInfo.ColumnIndex != -1)
            {

                if (dgvAccountsA3ERP.Columns[hittestInfo.ColumnIndex].Name == "EXTERNALID")
                {
                    valorFromMouseDown = dgvAccountsA3ERP.Rows[hittestInfo.RowIndex].Cells[hittestInfo.ColumnIndex].Value.ToString();
                    if (valorFromMouseDown != null)
                    {
                        // Remember the point where the mouse down occurred. 
                        // The DragSize indicates the size that the mouse can move 
                        // before a drag event should be started.                
                        Size dragSize = SystemInformation.DragSize;

                        // Create a rectangle using the DragSize, with the mouse position being
                        // at the center of the rectangle.
                        dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                    }
                }
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvAccountsRepasat_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvAccountsRepasat_DragDrop(object sender, DragEventArgs e)
        {
            string idRegistroRepasat = "";
            string codigoExterno = "";
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = dgvAccountsRepasat.PointToClient(new Point(e.X, e.Y));

            // If the drag operation was a copy then add the row to the other control.
            if (e.Effect == DragDropEffects.Copy)
            {

                string cellvalue = e.Data.GetData(typeof(string)) as string;
                var hittest = dgvAccountsRepasat.HitTest(clientPoint.X, clientPoint.Y);
                if (hittest.ColumnIndex != -1
                    && hittest.RowIndex != -1)
                {
                    if (dgvAccountsRepasat.Columns[hittest.ColumnIndex].Name == "EXTERNALID")
                    {
                        dgvAccountsRepasat[hittest.ColumnIndex, hittest.RowIndex].Value = cellvalue;

                        //Actualizo Repasat
                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                        {
                            codigoExterno = cellvalue;
                            idRegistroRepasat = dgvAccountsRepasat["IDREPASAT", hittest.RowIndex].Value.ToString();
                            csRepasatWebService rpstWS = new csRepasatWebService();
                            rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, codigoExterno, objetoCliente());
                            //actualizo A3ERP
                            csUtilidades.ejecutarConsulta("UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "=" + idRegistroRepasat + " WHERE LTRIM(" + campoKeyToUpdate + ")='" + codigoExterno.Trim() + "'", false);





                        }
                    }
                }

            }
        }



        private void validarPorCódigoExternoToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }


        private void validarRegistroRepasat(string tipoObjeto, string campoValidacion, string valorValidacion)
        {
            Objetos.csTercero tercero = new Objetos.csTercero();

        }

        private void btnRegImpuestos_Click(object sender, EventArgs e)
        {
            tipoObjeto = "taxoperation";
            campoExternoRepasat = "codExternoDocuPago";
            //campoExternoRepasat = "codExternoRegimenesImpuestos";

            tablaToUpdate = "REGIVA";
            campoKeyToUpdate = "REGIVA";
            repasatKeyField = "RPST_ID_REGIVA";

            dgvRepasatData.DataSource = loadRPSTObject.regimenImpuestosRepasat();
            regimenesImpuestosA3ERP();
        }

        private void btnUpdateClientesRPST_Click(object sender, EventArgs e)
        {
            inicioProceso();
            updateSyncDate();
            updateDataRPST.actualizarCuentasA3ToRepasat(true, true,null,false,dtpDateUpdateClientes.Value.ToShortDateString());
            loadSyncDate();
            inicioProceso(false);
        }

        private void btnArticulos_Click(object sender, EventArgs e)
        {
            cargarDatosArticulos(dgvAccountsRepasat, false, null, false, true);
        }
        private void btnSendProductsToA3ERP_Click(object sender, EventArgs e)
        {

        }

        private void btnProductUpdateToRepasat_Click(object sender, EventArgs e)
        {

            crearArticulosDeA3ERPToRepasat(true);
        }

        private void btnUpdateProducts_Click(object sender, EventArgs e)
        {
            crearArticulosDeA3ERPToRepasat(true);
            cargarDatosArticulos(dgvAccountsRepasat, false, null, false, true);
        }

        private void btnResetUpdateDateSync_Click(object sender, EventArgs e)
        {
            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
            updateDataRPST.resetLastUpdateSyncDate(dtpickerResetFrom.Value);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new csa3erp();
            csa3erpTercero a3ERP = new klsync.csa3erpTercero();

            a3.abrirEnlace();
            a3ERP.crearConctactoA3("", "", false);
            a3.cerrarEnlace();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new csa3erp();
            csa3erpTercero a3ERP = new klsync.csa3erpTercero();

            a3.abrirEnlace();
            a3ERP.crearConctactoA3("11", "", true);
            a3.cerrarEnlace();
        }

        private void btnContactsTorepasat_Click(object sender, EventArgs e)
        {
            updateDataRPST.gestionarContactosA3ToRepasat(true, false);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            updateDataRPST.gestionarDireccionesA3ToRepasat(null, true, true);
        }


        /// <summary>
        /// INICIO FUNCIONES PARA TRASPASAR DE AREA REPASAT A A3ERP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void dgvRepasatData_DragEnter(object sender, DragEventArgs e)
        //{
        //    e.Effect = DragDropEffects.Copy;
        //}


        private Rectangle dragBoxFromMouseDown2;
        private object valueFromMouseDown2;

        private void dgvRepasatData_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown2 != Rectangle.Empty && !dragBoxFromMouseDown2.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgvRepasatData.DoDragDrop(valueFromMouseDown2, DragDropEffects.Copy);
                }
            }
        }

        private void dgvRepasatData_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            var hittestInfo = dgvRepasatData.HitTest(e.X, e.Y);

            if (hittestInfo.RowIndex != -1 && hittestInfo.ColumnIndex != -1)
            {

                if (dgvRepasatData.Columns[hittestInfo.ColumnIndex].Name == "IDREPASAT")
                {
                    valueFromMouseDown2 = dgvRepasatData.Rows[hittestInfo.RowIndex].Cells[hittestInfo.ColumnIndex].Value;
                    if (valueFromMouseDown2 != null)
                    {
                        // Remember the point where the mouse down occurred. 
                        // The DragSize indicates the size that the mouse can move 
                        // before a drag event should be started.                
                        Size dragSize = SystemInformation.DragSize;

                        // Create a rectangle using the DragSize, with the mouse position being
                        // at the center of the rectangle.
                        dragBoxFromMouseDown2 = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                    }
                }
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgvA3ERPData_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvA3ERPData_DragDrop(object sender, DragEventArgs e)
        {
            {
                string idRegistroRepasat = "";
                string codigoExterno = "";
                // The mouse locations are relative to the screen, so they must be 
                // converted to client coordinates.
                Point clientPoint = dgvA3ERPData.PointToClient(new Point(e.X, e.Y));

                // If the drag operation was a copy then add the row to the other control.
                if (e.Effect == DragDropEffects.Copy)
                {

                    string cellvalue = e.Data.GetData(typeof(string)) as string;
                    var hittest = dgvA3ERPData.HitTest(clientPoint.X, clientPoint.Y);
                    if (hittest.ColumnIndex != -1
                        && hittest.RowIndex != -1)
                    {
                        if (dgvA3ERPData.Columns[hittest.ColumnIndex].Name == "ID_REPASAT")
                        {
                            //dgvA3ERPData[hittest.ColumnIndex, hittest.RowIndex].Value = cellvalue;
                            dgvA3ERPData[hittest.ColumnIndex, hittest.RowIndex].Value = cellvalue;
                            Refresh();

                            //Actualizo Repasat
                            if (csGlobal.modeAp.ToUpper() == "REPASAT")
                            {
                                codigoExterno = cellvalue;
                                idRegistroRepasat = dgvA3ERPData["ID_REPASAT", hittest.RowIndex].Value.ToString();
                                csRepasatWebService rpstWS = new csRepasatWebService();
                                if (tablaToUpdate != "REGIVA" && tablaToUpdate != "TIPOIVA")
                                {
                                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, codigoExterno);
                                }
                                if (tablaToUpdate == "REGIVA")
                                {
                                    codigoExterno = dgvA3ERPData["CODERP", hittest.RowIndex].Value.ToString();
                                    csUtilidades.ejecutarConsulta("UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "=" + idRegistroRepasat + " WHERE LTRIM(" + campoKeyToUpdate + ")='" + codigoExterno.Trim() + "'", false);
                                }
                                if (tablaToUpdate == "TIPOIVA")
                                {
                                    codigoExterno = dgvA3ERPData["CODERP", hittest.RowIndex].Value.ToString();
                                    csUtilidades.ejecutarConsulta("UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "=" + idRegistroRepasat + " WHERE LTRIM(" + campoKeyToUpdate + ")='" + codigoExterno.Trim() + "'", false);
                                }
                                //actualizo A3ERP
                                //csUtilidades.ejecutarConsulta("UPDATE " + tablaToUpdate + " SET " + repasatKeyField + "=" + idRegistroRepasat + " WHERE LTRIM(" + campoKeyToUpdate + ")='" + codigoExterno.Trim() + "'", false);

                            }
                        }
                        else if (dgvA3ERPData.Columns[hittest.ColumnIndex].Name == "ID_REPASAT")
                        {

                        }
                    }

                }
            }
        }

        private void dgvRepasatData_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            updateDataRPST.gestionarDireccionesRepasatToA3(true, false, null, false, true);
        }

        private void btnContactsToA3_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "PROCESANDO PETICIÓN";
            toolStripStatusLabel1.Font = new Font(label1.Font, FontStyle.Bold);
            toolStripStatusLabel1.ForeColor = Color.OrangeRed;
            m_FormDefInstance.Refresh();
            //Alta de nuevos contactos
            updateDataRPST.gestionarContactosRepasatToA3(true, false, null, true, false);
            //Actualización de existentes
            updateDataRPST.gestionarContactosRepasatToA3(true, false, null, false, true);
            toolStripStatusLabel1.Text = "PROCESO FINALIZADO";
            toolStripStatusLabel1.Font = new Font(label1.Font, FontStyle.Bold);
            toolStripStatusLabel1.ForeColor = Color.Green;
            m_FormDefInstance.Refresh();


        }

        private void marcarSyncroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarSyncronizacionRegistro(dgvAccountsA3ERP.SelectedRows, cboxMaestros.SelectedItem.ToString(), true);
        }



        private void activarSyncronizacionRegistro(DataGridViewSelectedRowCollection dgv, string tabla, bool syncronizar = false)
        {

            try
            {
                string tablaToUpdate = "";
                string fieldToUpdate = "";

                switch (tabla) {

                    case "Clientes":
                        tablaToUpdate = "__CLIENTES";
                        fieldToUpdate = "RPST_SINCRONIZAR";
                        break;
                    case "Proveedores":
                        tablaToUpdate = "__PROVEED";
                        fieldToUpdate = "RPST_SINCRONIZAR";
                        break;
                    case "Productos":
                        tablaToUpdate = "ARTICULO";
                        fieldToUpdate = "RPST_SINCRONIZAR";
                        break;
                    default:
                        break;

                }
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                updateData.syncronizarRegistro(dgv, tabla, syncronizar);

            }
            catch (Exception ex)
            {

            }


        }



        private void marcarNoSyncroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarSyncronizacionRegistro(dgvAccountsA3ERP.SelectedRows, cboxMaestros.SelectedItem.ToString(), false);
        }

        private void btnUpdateClientesA3ERP_Click(object sender, EventArgs e)
        {
            cargarDatosCuentasRepasat(objetoCliente(), false, null, false, false, true);
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            cargarDatosCuentasRepasat(false, false, null, false, false, true);
        }


        /// <summary>
        /// Sincronizar clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateClientes_Click(object sender, EventArgs e)
        {
            //Generación de clientes nuevos
            updateDataRPST.cargarCuentasA3ToRepasat(true, false);
            updateDataRPST.traspasarCuentasRepasatToA3(true, false, null, true, true, false);

            //Actualización de Clientes
            cargarDatosCuentasRepasat(objetoCliente(), false, null, false, false, true);
            updateDataRPST.cargarCuentasA3ToRepasat(true, true);

            //Actualización de Direcciones
            updateDataRPST.gestionarDireccionesRepasatToA3(true, false, null, false, true);
            updateDataRPST.gestionarDireccionesA3ToRepasat(null, true, true);

            //Alta de nuevos contactos
            updateDataRPST.gestionarContactosRepasatToA3(true, false, null, true, false);
            //Actualización de existentes
            updateDataRPST.gestionarContactosRepasatToA3(true, false, null, false, true);
        }

        private void btnBanksToRepasat_Click(object sender, EventArgs e)
        {
            updateDataRPST.gestionarCuentasBancariasA3ToRepasat(true, false);
        }

        private void btnGestionarBancosEnA3ERP_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "SINCRONIZANDO";
            this.Refresh();
            updateDataRPST.gestionarCuentasBancariasRepasatToA3ERP(true, false);
            toolStripStatusLabel4.Text = "FIN SINCRONIZACIÓN";
        }

        private void btUpdateRepasatToA3ERP_Click(object sender, EventArgs e)
        {
            inicioProceso();
            bool todos = false;
            string cuenta = null;
            string codCuenta = null;
            bool esCliente = objetoCliente();
            int i = 0;

            string tipoFichero = cboxMaestros.SelectedItem.ToString();

            if (dgvAccountsA3ERP.SelectedRows.Count != 0)
                {
                DialogResult Resultado = MessageBox.Show("Se van a actualizar " + dgvAccountsA3ERP.SelectedRows.Count + " "+ tipoFichero + " en Repasat con los datos de A3. \n ¿Está seguro?", "Actualizar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.Yes)
                {
                    if (tipoFichero == "Productos")
                    {
                        crearArticulosDeA3ERPToRepasat(true);
                    }
                    else if (tipoFichero == "Clientes" || tipoFichero == "Proveedores")
                    {
                        if (dgvAccountsA3ERP.SelectedRows.Count == dgvAccountsA3ERP.Rows.Count)
                        {
                            todos = true;
                            cuenta = "ALL";
                        }
                        else
                        {
                            foreach (DataGridViewRow dr in dgvAccountsA3ERP.SelectedRows)
                            {
                                codCuenta = objetoCliente() ? dr.Cells["CODCLI"].Value.ToString().Trim() : dr.Cells["CODPRO"].Value.ToString().Trim();

                                cuenta = i == 0 ? "'" + codCuenta + "'" : "'" + codCuenta + "'," + cuenta;
                                i++;
                            }
                        }
                        updateDataRPST.cargarCuentasA3ToRepasat(esCliente, true, cuenta, false);
                    }
                    else
                    {
                        MessageBox.Show("Las " + tipoFichero + " no se pueden actualizar.", "Actualizar Registros");
                    }
                }
            }
            else
            {
                MessageBox.Show("Debes seleccionar los registros que quieres actualizar.", "Actualizar Registros");
            }   
            inicioProceso(false);
        }

        private void códigoExternoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            validarCuentas(false, true);
        }

        private void nIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            validarCuentas(true, false);
        }


        private void validarCuentas(bool checkNif, bool checkCodExterno)
        {
            try
            {
                csRepasatWebService rpstWS = new csRepasatWebService();
                bool clientes = objetoCliente();
                string rpst_id = "";
                string rpst_razon = "";
                string rpst_nombre = "";
                string rpst_extCod = "";
                string rpst_cif = "";
                string a3_rpst_id = "";
                string a3_razon = "";
                string a3_codCli = "";
                string a3_cif = "";

                string scriptUpdateA3 = "";

                bool checkCuenta = false;

                string fichaCuenta = "";


                DataTable dtAccounts = updateDataRPST.traspasarCuentasRepasatToA3(objetoCliente(), true, null, false, false, false).Tables[0];

                foreach (DataGridViewRow fila in dgvAccountsA3ERP.SelectedRows)
                {
                    checkCuenta = false;
                    a3_cif = clientes ? fila.Cells["NIFCLI"].Value.ToString() : fila.Cells["NIFPRO"].Value.ToString();
                    a3_codCli = clientes ? fila.Cells["CODCLI"].Value.ToString().Trim() : fila.Cells["CODPRO"].Value.ToString().Trim();
                    a3_razon = clientes ? fila.Cells["NOMCLI"].Value.ToString().Trim() : fila.Cells["NOMPRO"].Value.ToString().Trim();
                    foreach (DataRow filaRPST in dtAccounts.Rows)
                    {
                        rpst_extCod = filaRPST["CODIGOERP"].ToString();
                        rpst_id = filaRPST["IDREPASAT"].ToString();
                        rpst_cif = filaRPST["NIF"].ToString();
                        rpst_razon = filaRPST["RAZON"].ToString();
                        rpst_nombre = filaRPST["NOMBRE"].ToString();

                        if ((checkNif && rpst_cif == a3_cif) || (checkCodExterno && a3_codCli == rpst_extCod))
                        {
                            checkCuenta = true;
                        }

                        //Valido si el CIF está en blanco. En ese caso, paso al siguiente registro de Repasat
                        if (checkNif && string.IsNullOrEmpty(rpst_cif))
                        {
                            continue;
                        }
                        //Valido si el CIF está en blanco. En ese caso, paso al siguiente registro de A3
                        if (checkNif && string.IsNullOrEmpty(a3_cif))
                        {
                            break;
                        }

                        if (checkCuenta)
                        {
                            if (rpst_cif == a3_cif && !string.IsNullOrEmpty(a3_cif))
                            {
                                scriptUpdateA3 = clientes ? "UPDATE __CLIENTES SET RPST_ID_CLI=" + rpst_id + " WHERE LTRIM(CODCLI)='" + a3_codCli + "'" : "UPDATE __PROVEED SET RPST_ID_PROV=" + rpst_id + " WHERE LTRIM(CODPRO)='" + a3_codCli + "'";
                                csUtilidades.ejecutarConsulta(scriptUpdateA3, false);
                                //Actualizar el id en Repasat

                                rpstWS.actualizarDocumentoRepasat("accounts", "codExternoCli", rpst_id, a3_codCli);
                            }
                            else
                            {
                                fichaCuenta = "NOMBRE EN REPASAT: " + "\t" + rpst_nombre + "\n" +
                                     "RAZON EN REPASAT: " + "\t" + rpst_razon + "\n" + "\n" +
                                     "NOMBRE EN A3ERP: " + "\t" + a3_razon + "\n" + "\n" + "\n" +
                                     "¿ACTUALIZO ID EN A3ERP?";

                                scriptUpdateA3 = clientes ? "UPDATE __CLIENTES SET RPST_ID_CLI=" + rpst_id + " WHERE LTRIM(CODCLI)='" + a3_codCli + "'" : "UPDATE __PROVEED SET RPST_ID_PROV=" + rpst_id + " WHERE LTRIM(CODPRO)='" + a3_codCli + "'";
                                DialogResult result = MessageBox.Show(fichaCuenta, "Actualización", MessageBoxButtons.YesNoCancel);

                                if (result == DialogResult.Yes)
                                {
                                    csUtilidades.ejecutarConsulta(scriptUpdateA3, false);
                                }
                            }
                            break;
                        }
                        //validarRegistroRepasat("Accounts", "cifCli", nif);        
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }


        }


        private void todosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            traspasarCuentasA3ERPToRepasat();
        }

        private void selecciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            traspasarCuentasA3ERPToRepasat();
        }

        private void traspasarCuentasA3ERPToRepasat(bool forzeSync = false, DataTable dtCuentas = null)
        {
            inicioProceso();
            string filtro = null;
            int i = 0;
            string codCli = "";
            string numRegistros = "";
            bool todos = false;
            todos = (dgvAccountsA3ERP.SelectedRows.Count == dgvAccountsA3ERP.Rows.Count) ? true : false;
            string tipoCuenta = (cboxMaestros.SelectedItem.ToString().ToUpper() == "CLIENTES") ? "CODCLI" : "CODPRO";
            tipoCuenta = (cboxMaestros.SelectedItem.ToString().ToUpper() == "PRODUCTOS") ? "CODIGO" : tipoCuenta;

            numRegistros = todos ? " todos los " : dgvAccountsA3ERP.SelectedRows.Count.ToString();

            DialogResult Resultado = MessageBox.Show("Se van a traspasar " + numRegistros + " registros a REPASAT. \n ¿Está seguro?", "Traspasar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                if (!todos)
                {
                    foreach (DataGridViewRow fila in dgvAccountsA3ERP.SelectedRows)
                    {
                        if (fila.Cells["SYNC"].Value.ToString() == "NO") continue;
                        codCli = "'" + fila.Cells[tipoCuenta].Value.ToString().Trim() + "'";
                        filtro = (i == 0) ? codCli : codCli + "," + filtro;
                        i++;
                    }
                }
                updateDataRPST.cargarCuentasA3ToRepasat(objetoCliente(), false, filtro);

            }
            //VUELVE A MOSTRAR LOS DATOS
            inicioProceso(false);
            syncroData(false, true, false);
            establecerColumnaImagen();

        }

        private bool objetoCliente()
        {
            if (cboxMaestros.SelectedItem.ToString() == "Clientes" || cboxMaestros.SelectedItem.ToString() == "Direcciones Clientes") return true;
            else return false;
        }

        /// <summary>
        /// Función para sincronizar/traspasar datos entre Repasat y A3ERP
        /// Se puede sincronizar clientes nuevos, y también forzar la sincronización si se ha realizado la migración desde Repasat
        /// </summary>
        /// <param name="syncButton"></param>
        /// <param name="loadButton"></param>
        /// <param name="repasat"></param>
        /// <param name="forceSync"></param>
        private void syncroData(bool syncButton, bool loadButton, bool repasat, bool forceSync = false, DataGridView dgv = null, bool validationProcess = false)
        {
            if (dgv == null) {
                dgv = dgvAccountsRepasat;
            }



            bool clientes = false;

            string idCliente = "";
            try
            {
                //Syncronización de Datos
                if (syncButton)
                {
                    string destino = repasat ? "REPASAT" : "A3ERP";
                    
                    DialogResult Resultado = MessageBox.Show("Se van a traspasar registros a " + destino + ". \n ¿Está seguro?", "Traspasar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        csLAUClientes traspasoCliente = new csLAUClientes(updateDataRPST);
                        if (cboxMaestros.SelectedItem.ToString() == "Clientes")
                        {
                            clientes = true;
                            if (repasat)
                            {
                                traspasarCuentasA3ERPToRepasat(); // revisar datos bancarios
                            }
                            else
                            {                            
                                traspasoCliente.TraspasarCuentasToA3(dgv, clientes, syncButton, forceSync);
                            }
                        }
                        if (cboxMaestros.SelectedItem.ToString() == "Proveedores")
                        {
                            clientes = false;
                            if (repasat) {
                                traspasarCuentasA3ERPToRepasat();
                            }
                            else 
                            {
                                traspasoCliente.TraspasarCuentasToA3(dgv, clientes, syncButton, forceSync);
                            }
                        }
                        if (cboxMaestros.SelectedItem.ToString() == "Productos")
                        {

                            if (repasat)    //A3ERP ==> REPASAT
                            {
                                crearArticulosDeA3ERPToRepasat();
                            }
                            else            // REPASAT ==> A3ERP
                            {
                                string codart = "";
                                bool articuloSync = false;
                                DataTable dtArticulos = new DataTable();
                                dtArticulos.Columns.Add("CODART");

                                if (dgvAccountsRepasat.SelectedRows.Count > 0)
                                {
                                    foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                                    {
                                        articuloSync = false;
                                        codart = fila.Cells["REFERENCIA"].Value.ToString();

                                        if (string.IsNullOrEmpty(sql.obtenerValorArticuloA3(codart, "CODART")))
                                        {
                                            DataRow drItems = dtArticulos.NewRow();
                                            drItems["CODART"] = fila.Cells["REFERENCIA"].Value.ToString();
                                            dtArticulos.Rows.Add(drItems);
                                        }
                                        else
                                        { //actualizo el artículo en Repasat

                                            DataTable dtParams = new DataTable();
                                            dtParams.Columns.Add("FIELD");
                                            dtParams.Columns.Add("KEY");
                                            dtParams.Columns.Add("VALUE");

                                            DataTable dtKeys = new DataTable();
                                            dtKeys.Columns.Add("KEY");

                                            csRepasatWebService rpstWS = new csRepasatWebService();

                                            DataRow drParams = dtParams.NewRow();
                                            drParams["FIELD"] = "codExternoArticulo";
                                            drParams["KEY"] = fila.Cells["IDREPASAT"].Value.ToString();
                                            drParams["VALUE"] = codart;
                                            dtParams.Rows.Add(drParams);

                                            DataRow drKeys = dtKeys.NewRow();
                                            drKeys["KEY"] = fila.Cells["IDREPASAT"].Value.ToString();
                                            dtKeys.Rows.Add(drKeys);
                                            //Sincronización de cuentas

                                            rpstWS.sincronizarObjetoRepasat("products", "PUT", dtKeys, dtParams, "");

                                            csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET RPST_ID_PROD=" + fila.Cells["IDREPASAT"].Value.ToString() + " WHERE LTRIM(CODART)='" + codart + "'", false);

                                        }
                                    }
                                }
                                updateDataRPST.traspasarArticulosRepasatToA3(syncButton, null, false, false, false, true, null, dtArticulos);
                            }
                        }
                    }
                }
                //Consulta de Datos
                if (loadButton)
                {
                    if (cboxMaestros.SelectedItem.ToString() == "Clientes")
                    {
                        if (repasat)
                        {
                            dgv.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, null, false, rbSyncNo.Checked, false, false, true);
                        }
                        else
                        {
                            cuentasA3ERP(objetoCliente());
                        }
                    }
                    if (cboxMaestros.SelectedItem.ToString() == "Proveedores")
                    {
                        if (repasat)
                        {
                            dgvAccountsRepasat.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, null, false, rbSyncNo.Checked, false);
                        }
                        else
                        {
                            cuentasA3ERP(objetoCliente());
                        }
                    }
                    if (cboxMaestros.SelectedItem.ToString() == "Productos")
                    {
                        if (repasat)
                        {
                            cargarDatosArticulos(dgvAccountsRepasat, true, null, rbSyncNo.Checked, false);
                        }
                        else
                        {
                            loadProductsERP();
                        }
                    }

                    if (cboxMaestros.SelectedItem.ToString() == "Direcciones Clientes")
                    {
                        if (repasat)
                        {
                            dgvAccountsRepasat.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, null, true, rbSyncNo.Checked, false, true);
                        }
                        else
                        {
                            cuentasA3ERP(objetoCliente(), true);
                        }
                    }

                    if (cboxMaestros.SelectedItem.ToString() == "Direcciones Proveedores")
                    {
                        if (repasat)
                        {
                            dgvAccountsRepasat.DataSource = cargarDatosCuentasRepasat(objetoCliente(), true, null, true, rbSyncNo.Checked, false, true);
                        }
                        else
                        {
                            cuentasA3ERP(objetoCliente(), true);
                        }
                    }


                }
                tssLabelTotalRows.Text = dgvAccountsA3ERP.Rows.Count.ToString() + " filas";
                tssLabelTotalRowsRPST.Text = dgvAccountsRepasat.Rows.Count.ToString() + " filas";
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    (ex.Message).mb();
                }
            }
        }

        private void cboxMaestros_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvAccountsA3ERP.DataSource = null;
            dgvAccountsRepasat.DataSource = null;
            articulosToolStripMenuItem.Visible = cboxMaestros.SelectedItem == "Productos" ? true : false;
            groupBox5.Visible = cboxMaestros.SelectedItem == "Productos" ? true : false;
            grBoxActivos.Visible = cboxMaestros.SelectedItem == "Productos" ? true : false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tipoDocumento = "Albaran";
        }

        private void splitContainer5_Panel1_SizeChanged(object sender, EventArgs e)
        {
            splitContainer5.SplitterDistance = maxPanelSize;
        }

        private void btnUpdateProveedorsRPST_Click(object sender, EventArgs e)
        {
            updateDataRPST.cargarCuentasA3ToRepasat(false, true);
        }


        public DataTable docsFromA3ToRPST(string objeto, bool consulta = false, string syncDocumento = "TODOS", bool forceUpdate = false, string serieA3 = null, string numDoc = null, string fechaIni = null, string fechaFin = null, bool updateEfectosPagados=false, bool checkDoc = false)
        {
            try
            {
                //filtro para cargar documentos por estado de cobro
                string filtroEstadoCobro = "TODOS";
                filtroEstadoCobro = rbPaidYes.Checked ? "SI" : filtroEstadoCobro;
                filtroEstadoCobro = rbPaidNo.Checked ? "NO" : filtroEstadoCobro;
                //filtroEstadoCobro = !consulta ? "&filter[estadoCarteraCobros]=1" : filtroEstadoCobro;
                bool filtrarPorFechaFactura = rbutFechaFactura.Checked ? true : false;


                string filtroCodCuenta = checkBoxCuentas.Checked && !string.IsNullOrEmpty(cboxCuentas.SelectedValue.ToString()) ? cboxCuentas.SelectedValue.ToString() : null;

                var parameterDate = DateTime.ParseExact("01/01/2020", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                if (dtpFromInvoicesV.Value.Date < parameterDate)
                {
                    parameterDate = DateTime.ParseExact("01/01/2020", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    fechaIni = parameterDate.Date.ToString("dd-MM-yyyy");
                }
                else
                {
                    fechaIni = dtpFromInvoicesV.Value.Date.ToString("dd-MM-yyyy");
                }


                //string filtroFechaIni = dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                // string filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");

                if (checkBoxSeries.Checked)
                {
                    serieA3 = cboxSeries.Text;
                }


                // fechaIni = dtpFromInvoicesV.Value.ToShortDateString();
                fechaFin = dtpToInvoicesV.Value.ToShortDateString();
                Repasat.csRPSTSqlDataMigration dataMig = new Repasat.csRPSTSqlDataMigration();
                DataTable dt = new DataTable();

                dt = dataMig.dtLoadDocsFromA3ToRPST(objeto, rbVentas.Checked, fechaIni, fechaFin, consulta, syncDocumento, filtroEstadoCobro, filtroCodCuenta, forceUpdate, serieA3, numDoc, filtrarPorFechaFactura, updateEfectosPagados, false, checkDoc);

                return dt;
            }
            catch
            {
                return null;
            }

        }

        private void btnMigrationUtility_Click(object sender, EventArgs e)
        {
            // MigracionExt.frMigracionExterna.DefInstance.MdiParent = this;
            MigracionExt.frMigracionExterna.DefInstance.Show();
            MigracionExt.frMigracionExterna.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void btnSeries_Click(object sender, EventArgs e)
        {
            informObjectType("seriesdocs");
            tablaToUpdate = "SERIES";
            campoKeyToUpdate = "SERIE";
            repasatKeyField = "RPST_ID_SERIE";
            campoExternoRepasat = "codExternoSerie";
            dgvRepasatData.DataSource = loadRPSTObject.Series();
            seriesA3ERP();
            btnCrearSeriesA3.Enabled = true;
        }

        private void btnAlmacenes_Click(object sender, EventArgs e)
        {
            informObjectType("warehouses");
            tablaToUpdate = "ALMACEN";
            campoKeyToUpdate = "CODALM";
            repasatKeyField = "RPST_ID_ALM";
            campoExternoRepasat = "codExternoAlmacen";
            dgvRepasatData.DataSource = loadRPSTObject.Almacenes();
            almacenesA3ERP();
            btnCrearAlmacenA3.Enabled = true;
        }

        private void btnCrearAlmacenA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp los Almacenes no asignados, ¿está seguro/a?", "Sinronizar Almacen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                crearAlmacenA3();
                dgvRepasatData.DataSource = loadRPSTObject.Almacenes();
                almacenesA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnFamiliaArt_Click(object sender, EventArgs e)
        {
            informObjectType("families");
            tablaToUpdate = "CARACTERISTICAS";
            campoKeyToUpdate = "CODCAR";
            repasatKeyField = "RPST_ID_CARACTERISTICA";
            campoExternoRepasat = "codExternoFamilia";
            dgvRepasatData.DataSource = loadRPSTObject.FamiliaArt();
            familiaArtA3ERP();
            btnCrearFamiliaArtA3.Enabled = true;
        }
        //public void prueba(string key, string value)
        //{
        //    csRepasatWebService rpstWS = new csRepasatWebService();
        //    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, key, value);
        //}

        private void btnCrearFamiliaArtA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3erp las familias de articulos no asignadas, ¿está seguro/a?", "Sinronizar Familias Articulo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                //crearAlmacenA3();
                dgvRepasatData.DataSource = loadRPSTObject.FamiliaArt();
                familiaArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnSubfamiliaArt_Click(object sender, EventArgs e)
        {
            informObjectType("families");
            tablaToUpdate = "CARACTERISTICAS";
            campoKeyToUpdate = "CODCAR";
            repasatKeyField = "RPST_ID_CARACTERISTICA";
            campoExternoRepasat = "codExternoFamilia";
            dgvRepasatData.DataSource = loadRPSTObject.FamiliaArt(true);
            subfamiliaArtA3ERP();
            btnCrearSubfamiliaArtA3.Enabled = true;
        }

        private void btnMarcaArt_Click(object sender, EventArgs e)
        {
            informObjectType("brands");
            tablaToUpdate = "CARACTERISTICAS";
            campoKeyToUpdate = "CODCAR";
            repasatKeyField = "RPST_ID_CARACTERISTICA";
            campoExternoRepasat = "codExternoMarca";
            dgvRepasatData.DataSource = loadRPSTObject.MarcaArt();
            marcaArtA3ERP();
            btnCrearMarcasArtA3.Enabled = true;
        }

        private void btnUploadFamiliaArt_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las familias de articulos no asignadas, ¿está seguro/a?", "Sinronizar Familias", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarFamiliaArtA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.FamiliaArt();
                familiaArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnUploadSubfamiliaArt_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las subfamilias de articulos no asignadas, ¿está seguro/a?", "Sinronizar Subfamilias", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarSubfamiliaArtA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.FamiliaArt(true);
                subfamiliaArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void btnUploadMarcasArt_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las Marcas de articulos no asignadas, ¿está seguro/a?", "Sinronizar Marcas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarMarcasA3ToRepasat();
                dgvRepasatData.DataSource = loadRPSTObject.MarcaArt();
                marcaArtA3ERP();
                MessageBox.Show("Proceso finalizado");
            }

        }

        private void enlazarFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enlazarDocumentoRepasata();
        }

        public string obtenerTipoObjetoRPST()
        {
            tipoDocumento = cboxTipoDoc.SelectedItem.ToString();

            bool compras = rbCompras.Checked;

            //DocsVenta
            if (tipoDocumento == "Pedidos" && !compras) tipoObjeto = "saleorders";
            if (tipoDocumento == "Albaran" && !compras) tipoObjeto = "saledelivery";
            if (tipoDocumento == "Facturas" && !compras) tipoObjeto = "saleinvoices";
            if (tipoDocumento == "Cartera" && !compras) tipoObjeto = "incomingpayments";
            if (tipoDocumento == "Impagados" && !compras) tipoObjeto = "incomingpayments";
            if (tipoDocumento == "Remesas" && !compras) tipoObjeto = "remittances";
            if (tipoDocumento == "Tarifas" && !compras) tipoObjeto = "rates";
            //DocsCompra
            if (tipoDocumento == "Pedidos" && compras) tipoObjeto = "purchaseorders";
            if (tipoDocumento == "Albaran" && compras) tipoObjeto = "purchasedelivery";
            if (tipoDocumento == "Facturas" && compras) tipoObjeto = "purchaseinvoices";
            if (tipoDocumento == "Cartera" && compras) tipoObjeto = "outgoingpayments";

            return tipoObjeto;
        }


        private void enlazarDocumentoRepasata()
        {
            string[] serieDoc = new string[2];
            string numSerie = "";
            string numDoc = "";
            string idNumDocRPST = "";
            string idDocERP = "";
            string messageUpdate = "";
            string messageConfirm = "¿Quieres omitir las confirmaciones?";
            string numVencimiento = "";
            csSqlConnects sqlConnect = new csSqlConnects();
            csRepasatWebService rpstWS = new csRepasatWebService();
            string nomCampoExternoRPST = "";
            int contPreguntas = 0;
            string serieDocumento = "";
            DialogResult resultConfirmacion = new DialogResult();
            DialogResult result = new DialogResult();

            string tipoObjetoRPST = obtenerTipoObjetoRPST();

            foreach (DataGridViewRow dgvRow in dgvRPST.SelectedRows)
            {
                if (dgvRPST.Columns.Contains("serie"))
                {
                    if (dgvRPST.Columns.Contains("TIPO_DOC")) 
                    {
                        serieDocumento = dgvRow.Cells["TIPO_DOC"].Value.ToString() == "CREDIT_NOTE" ? "SERIE_RECT" : "SERIE";
                        serieDoc = dgvRow.Cells[serieDocumento].Value.ToString().Split('(');
                        numSerie = serieDoc[0].Trim();
                    }
                    else if (tipoObjetoRPST == "saleorders")
                    {
                        numSerie = dgvRow.Cells["SERIE"].Value.ToString();
                    }
                }

                if (dgvRPST.Columns.Contains("NUM_FRA"))
                {
                    numDoc = dgvRow.Cells["NUM_FRA"].Value.ToString();
                }
                idNumDocRPST = dgvRow.Cells["REPASAT_ID_DOC"].Value.ToString();
                
                if (dgvRPST.Columns.Contains("NUM VTO"))
                {
                    numVencimiento = tipoDocumento.ToUpper() == "CARTERA" ? dgvRow.Cells["NUM VTO"].Value.ToString() : numVencimiento;
                }

                //Para obtener el Id de la Remesa
                if (tipoObjetoRPST == "remittances") {
                    numDoc = idNumDocRPST;
                }

                if (tipoObjetoRPST == "saleorders")
                {
                    numDoc = dgvRow.Cells["NUM_DOC"].Value.ToString();
                }

                idDocERP = sqlConnect.obtenerIdDocumentoA3ERP(numSerie, numDoc, tipoObjetoRPST, numVencimiento);

                if (!string.IsNullOrEmpty(idDocERP))
                {
                    messageUpdate = "¿Quieres enlazar el documento: " + numSerie + "/" + numDoc + " ?";

                    contPreguntas++;
                    if (contPreguntas == 2) {
                        resultConfirmacion = MessageBox.Show(messageConfirm, "Confirmación", MessageBoxButtons.YesNoCancel);
                        if (resultConfirmacion == DialogResult.Yes)
                        {
                            resultConfirmacion = DialogResult.Yes;
                        }
                    }

                    if (resultConfirmacion != DialogResult.Yes)
                    {
                        result = MessageBox.Show(messageUpdate, "Actualización", MessageBoxButtons.YesNoCancel);
                    }

                    if (result == DialogResult.Yes || resultConfirmacion == DialogResult.Yes)
                    {
                        nomCampoExternoRPST = tipoDocumento.ToUpper() == "CARTERA" ? "codExternoCarteraCobros" : "codExternoDocumento";
                        nomCampoExternoRPST = tipoDocumento.ToUpper() == "REMESAS" ? "codExternoRemesa" : nomCampoExternoRPST;



                        rpstWS.actualizarDocumentoRepasat(tipoObjeto, nomCampoExternoRPST, idNumDocRPST, idDocERP);
                    }
                }


                inicioProceso(false);

            }


        }


        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }


        private void cargarSeriesDocs()
        {
            try
            {
                DataTable dt = new DataTable();
                cboxSeries.Items.Clear();
                cboxSeries.ValueMember = "IDREPASAT";
                cboxSeries.DisplayMember = "DESCRIPCION";
                cboxSeries.DataSource = loadRPSTObject.Series();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarAlmacenesRPST()
        {
            try
            {
                DataTable dt = new DataTable();
                cboxAlmacen.Items.Clear();
                cboxAlmacen.ValueMember = "IDREPASAT";
                cboxAlmacen.DisplayMember = "DESCRIPCION";
                cboxAlmacen.DataSource = AlmacenesRPST();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }


        private void checkBoxSeries_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSeries.Checked)
            {
                cboxSeries.Visible = true;
                cargarSeriesDocs();
            }
            else
            {
                cboxSeries.Visible = false;
                cboxSeries.DataSource = null;
                cboxSeries.Refresh();
            }
        }

        private void verificarImportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarImportesRemesas("facturas");

        }

        private void verificarSituacionEfectos(DataGridView dgv)
        {
            csSqlConnects sql = new klsync.csSqlConnects();
            string importeFraA3 = "";
            string importeFraRPST = "";
            int numFila = 0;
            string serieDoc = "";
            string numDoc = "";
            string numVto = "";
            string idDoc = "";
            string situacionRPST = "";
            string situacionA3ERP = "";
            string filterCuenta = rbVentas.Checked ? " AND CODCLI IS NOT NULL" : " AND CODPRO IS NOT NULL";

            if (dgv.Rows.Count > 0)
            {
                //Solo gestiono si estoy consultando información desde Repasat
                if (dgvRPST.Columns.Contains("origenDocumento")) return;

                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgv.Rows)
                {

                    serieDoc = fila.Cells["SERIE"].Value.ToString();
                    numDoc = fila.Cells["NUM_FRA"].Value.ToString();
                    numVto = fila.Cells["NUM VTO"].Value.ToString();
                    situacionRPST = fila.Cells["COBRADO"].Value.ToString();


                    //idDoc = fila.Cells["EXTERNALID"].Value.ToString();
                    //if (string.IsNullOrEmpty(idDoc)) continue;

                    importeFraRPST = fila.Cells["IMPORTE_EFECTO"].Value.ToString();
                    if (rbVentas.Checked)
                    {
                        importeFraA3 = sql.obtenerCampoTabla("SELECT PAGADO FROM CARTERA WHERE NUMDOC='" + numDoc + "' AND LTRIM(SERIE)='" + serieDoc + "' AND NUMVEN=" + numVto + filterCuenta);
                    }
                    else
                    {
                        importeFraA3 = sql.obtenerCampoTabla("SELECT PAGADO FROM CARTERA  INNER JOIN dbo.SERIES ON dbo.CARTERA.SERIE = dbo.SERIES.SERIE " +
                            " WHERE NUMDOC=" + numDoc + " AND LTRIM(dbo.SERIES.SERIE)='" + serieDoc + "' AND NUMVEN=" + numVto + filterCuenta);
                    }
                    if (string.IsNullOrEmpty(importeFraA3))
                    {
                        fila.Cells["ACCIÓN"].Value = "Efecto No Localizado en A3ERP Fra." + numDoc + "  (" + fila.Cells["FECHA FRA"].Value.ToString() + ")";
                        continue;
                    }

                    //Mensaje deAcción
                    if ((importeFraA3 == "T" && situacionRPST == "NO") || (importeFraA3 == "F" && situacionRPST == "SI"))
                    {
                        fila.Cells["ACCIÓN"].Value = importeFraA3 == "T" ? "Revisar Efecto Pagado en A3ERP" : "Revisar Efecto Pendiente en A3ERP";
                    }
                    else
                    {
                        fila.Cells["ACCIÓN"].Value = importeFraA3 == "T" ? "Efecto Pagado en A3ERP" : "Efecto Pendiente en A3ERP";
                    }
                }
                sql.cerrarConexion();
            }

        }

        private void borrarSincronizacionDocumentos(string origen, bool borrarSoloEnlace = false)
        {
            try
            {
                tipoDocumento = cboxTipoDoc.SelectedItem.ToString();
                if (origen == "A3ERP")
                {
                    string idDocumento = "";
                    if (dgvRPST.Rows[0].Cells["origenDocumento"].Value.ToString() == "A3ERP")
                    {
                        if (tipoDocumento == "Facturas")
                            foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                            {
                                idDocumento = fila.Cells["IDFACV"].Value.ToString();
                                csUtilidades.ejecutarConsulta("UPDATE CABEFACV SET RPST_ID_FACV=NULL WHERE IDFACV=" + idDocumento, false);

                            }

                        if (tipoDocumento == "Albaranes")
                            foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                            {
                                idDocumento = fila.Cells["IDALBV"].Value.ToString();
                                csUtilidades.ejecutarConsulta("UPDATE CABEALBV SET RPST_ID_ALBV=NULL WHERE IDALBV=" + idDocumento, false);
                            }
                                if (tipoDocumento == "Pedidos")
                            foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                            {
                                idDocumento = fila.Cells["IDDOC"].Value.ToString();
                                if (rbCompras.Checked)
                                {
                                    csUtilidades.ejecutarConsulta("UPDATE CABEPEDC SET RPST_ID_PEDC=NULL WHERE IDPEDC=" + idDocumento, false);
                                }
                                else {
                                    csUtilidades.ejecutarConsulta("UPDATE CABEPEDCV SET RPST_ID_PEDV=NULL WHERE IDPEDV=" + idDocumento, false);
                                }

                            }
                        else if (tipoDocumento == "Cartera")
                        {
                            foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                            {
                                idDocumento = fila.Cells["IDCARTERA"].Value.ToString();
                                csUtilidades.ejecutarConsulta("UPDATE CARTERA SET RPST_ID_CARTERA=NULL WHERE IDCARTERA=" + idDocumento, false);

                            }
                        }
                    }
                    return;
                }
                else
                {
                    csa3erp a3erp = new csa3erp();
                    csa3erpCartera a3cartera = new csa3erpCartera();



                    csRepasatWebService rpstWS = new csRepasatWebService();

                    bool compras = rbCompras.Checked;
                    bool pedido = false;
                    string idDocumentoA3ERP = "";
                    string idNumDocRPST = "";
                    string idDocERP = "";

                    //DocsVenta
                    if (tipoDocumento == "Pedidos" && !compras) tipoObjeto = "saleorders";
                    if (tipoDocumento == "Albaranes" && !compras) tipoObjeto = "saledeliverynotes";
                    if (tipoDocumento == "Facturas" && !compras) tipoObjeto = "saleinvoices";
                    if (tipoDocumento == "Cartera" && !compras) tipoObjeto = "incomingpayments";
                    if (tipoDocumento == "Impagados" && !compras) tipoObjeto = "incomingpayments";
                    if (tipoDocumento == "Remesas" && !compras) tipoObjeto = "remittances";
                    //DocsCompra
                    if (tipoDocumento == "Pedidos" && compras) tipoObjeto = "purchaseinvoices";
                    if (tipoDocumento == "Albaranes" && compras) tipoObjeto = "purchasedeliverynotes";
                    if (tipoDocumento == "Facturas" && compras) tipoObjeto = "purchaseinvoices";
                    if (tipoDocumento == "Cartera" && compras) tipoObjeto = "outgoingpayments";


                    pedido = tipoDocumento == "Pedidos" ? true : false;


                    DialogResult Resultado = MessageBox.Show("¡ATENCIÓN ESTE PROCESO ES IRREVERSIBLE!!! \n\n" +
                            "Vas a borrar " + dgvRPST.SelectedRows.Count.ToString() + " documentos en A3ERP y borrar el enlace en Repasat \n¿Estás seguro?", "BORRAR DOCUMENTOS ENLAZADOS", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (Resultado == DialogResult.Yes)
                    {
                        a3erp.abrirEnlace();

                        foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                        {

                            idDocumentoA3ERP = fila.Cells["EXTERNALID"].Value.ToString();

                            if (string.IsNullOrEmpty(idDocumentoA3ERP))
                            {
                                "La fila seleccionada no tiene un Identificador Externo, verifique los datos por favor".mb();
                                continue;
                            }

                            

                            //idDocERP = fila.Cells["NUM_FRA"].Value.ToString();
                            idNumDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();

                            if (tipoDocumento == "Pedidos" || tipoDocumento == "Albaranes" || tipoDocumento == "Facturas")
                            {
                                a3erp.borrarDocumento(idDocumentoA3ERP, tipoDocumento, compras);
                                rpstWS.actualizarDocumentoRepasat(tipoObjeto, "codExternoDocumento", idNumDocRPST, "");
                            }
                            else if (tipoDocumento == "Cartera")
                            {
                                if (fila.Cells["AGRUPACION"].Value.ToString() == "SI") continue;

                                borrarSoloEnlace = csGlobal.conexionDB.Contains("REPASATA3ERP") ? true : false;
                                if (borrarSoloEnlace)
                                {
                                    csUtilidades.ejecutarConsulta("UPDATE CARTERA SET RPST_ID_CARTERA=NULL WHERE IDCARTERA=" + idDocumentoA3ERP, false);
                                }
                                else
                                {
                                    a3cartera.anularCobroPago(rbVentas.Checked, idDocumentoA3ERP, Convert.ToInt16(fila.Cells["NUM VTO"].Value.ToString()));
                                }

                                rpstWS.actualizarDocumentoRepasat(tipoObjeto, "codExternoCarteraCobros", idNumDocRPST, "",false,"cancelsync");
                                //rpstWS.actualizarDocumentoRepasat(tipoObjeto, "importeCobradoCarteraCobros", idNumDocRPST, "0");
                                //rpstWS.actualizarDocumentoRepasat(tipoObjeto, "estadoCarteraCobros", idNumDocRPST, "0");
                                //rpstWS.actualizarDocumentoRepasat(tipoObjeto, "refSerie", idNumDocRPST, "0");
                            }
                            else if (tipoDocumento == "Impagados")
                            {
                                a3cartera.anularDevolucion(rbVentas.Checked, idDocumentoA3ERP, Convert.ToInt16(fila.Cells["NUM VTO"].Value.ToString()));
                                rpstWS.actualizarDocumentoRepasat(tipoObjeto, "codExternoCarteraCobros", idNumDocRPST, "");

                            }
                            else if (tipoDocumento == "Remesas")
                            {
                                a3cartera.anularCobroRemesa(Convert.ToInt16(idDocumentoA3ERP));
                                rpstWS.actualizarDocumentoRepasat(tipoObjeto, "codExternoRemesa", idNumDocRPST, "");
                            }

                            else if (tipoDocumento == "")
                            {
                                return;
                            }
                        }

                        a3erp.cerrarEnlace();
                        "Proceso finalizado".mb();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.mb();

            }

        }


        private void borrarSincronizaciónToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvRPST.Rows.Count > 0)
                {
                    bool mainRPST = false;
                    mainRPST = dgvRPST.Columns.Contains("origenDocumento") ? true : false;
                    string origenDatos = mainRPST ? "A3ERP" : "Repasat";
                    borrarSincronizacionDocumentos(origenDatos);
                }
            }
            catch { }

        }



        private void establecerColumnaImagen()
        {
            if (!dgvRPST.Columns.Contains("RPST"))
            {
                DataGridViewImageColumn imagen = new DataGridViewImageColumn();
                imagen.Name = "RPST";
                dgvRPST.Columns.Add(imagen);
                dgvRPST.Columns["RPST"].DisplayIndex = 0;
                dgvRPST.Refresh();
            }

            if (!dgvAccountsRepasat.Columns.Contains("RPST"))
            {
                DataGridViewImageColumn imagen = new DataGridViewImageColumn();
                imagen.Name = "RPST";
                dgvAccountsRepasat.Columns.Add(imagen);
                dgvAccountsRepasat.Columns["RPST"].DisplayIndex = 0;
                dgvAccountsRepasat.Refresh();
                
            }
            //if (!dgvRPST.Columns.Contains("ATR"))
            //{
            //    DataGridViewImageColumn imagen = new DataGridViewImageColumn();
            //    imagen.Name = "ATR";
            //    dgvRPST.Columns.Add(imagen);
            //    dgvRPST.Columns["ATR"].DisplayIndex = 1;
            //}
            dgvRPST.Columns["RPST"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            //dgvRPST.Columns["ATR"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

        }
        /// <summary>
        /// Función para cargar documentos en Datatable de pantalla
        /// </summary>
        /// <param name="loadDocumento"> Indicamos si cargamos los datos para mostrarlos</param>
        /// <param name="syncDocumento">Indicamos si cargamos los datos para sincronizarlos y traspasarlos entre aplicaciones</param>
        /// <param name="mainRPST">Indica si la carga de datos inicial se hace desde Repasat o A3ERP </param>
        /// <param name="tipoDoc">indicamos el tipo de documento que cargamos</param>
        private void gestionarDocumentos(bool loadDocumento, bool syncDocumento, bool mainRPST, string tipoDoc = "Facturas")
        {
            if (string.IsNullOrEmpty(cboxTipoDoc.Text))
            {
                "Debes seleccionar un tipo de documento".mb();
                return;
            }
            ticketBaiToolStripMenuItem.Visible = false;

            string tipoDocumento = cboxTipoDoc.SelectedItem.ToString();
            string filtroFechaIni = "";
            string filtroFechaFin = "";

            var parameterDate = DateTime.ParseExact("01/01/2020", "MM/dd/yyyy", CultureInfo.InvariantCulture);

            filtroFechaIni = dtpFromInvoicesV.Value.Date < parameterDate ? parameterDate.Date.ToString("yyyy-MM-dd") : dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
            filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");

            if (tipoDocumento == "Facturas")
            {
                //Cargar
                if (loadDocumento)
                {
                    dgvRPST.Columns.Clear();
                    if (mainRPST)
                    {
                        dgvRPST.DataSource = cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, rbVentas.Checked, true, filtroDocumentosCarga());
                        establecerColumnaImagen();
                    }
                    else   //Syncronizar hacia A3ERP
                    {
                        string serie = checkBoxSeries.Checked ? cboxSeries.Text : null;
                        dgvRPST.DataSource = docsFromA3ToRPST("saleinvoices", true, filtroDocumentosCarga(), false, serie);
                    }
                }
                else    //Syncronizar
                {
                    if (mainRPST)
                    {
                        //Fras de Repasat to A3ERP
                        cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, "NO");
                    }
                    else
                    {
                        //Traspaso de documentos desde A3ERP a a Repasat
                        docsFromA3ToRPST("saleinvoices", false, "NO");
                    }
                }

            }
            else if (tipoDocumento == "Pedidos")
            {

                if (loadDocumento)
                {
                    if (mainRPST)  //Carga de Datos de Repasat
                    {
                        csGlobal.docDestino = "Pedido";
                        dgvRPST.DataSource = cargarPedidos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, true, filtroDocumentosCarga());
                        establecerColumnaImagen();
                    }
                    else
                    {
                        //cargarPedidosVenta
                        dgvRPST.DataSource = docsFromA3ToRPST(obtenerTipoObjetoRPST(), loadDocumento, filtroDocumentosCarga());
                    }
                }
                else
                {
                    if (mainRPST)         //Traspaso de Datos Repasat a A3ERP
                    {
                        cargarPedidos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, filtroDocumentosCarga());
                    }
                    else    //Traspaso de Datos A3ERP a Repasat
                    {
                        docsFromA3ToRPST(obtenerTipoObjetoRPST(), false, "NO");
                    }
                }
            }

            else if (tipoDocumento == "Albaranes")
            {
                if (loadDocumento)
                {
                    if (mainRPST)  //Carga de Datos de Repasat
                    {
                        csGlobal.docDestino = "Albaran";
                        dgvRPST.DataSource = cargarPedidos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, true, filtroDocumentosCarga());
                        establecerColumnaImagen();
                    }
                    else
                    {
                        //cargarPedidosVenta
                        dgvRPST.DataSource = docsFromA3ToRPST(obtenerTipoObjetoRPST(), loadDocumento, filtroDocumentosCarga());
                    }
                }
                else
                {
                    if (mainRPST)         //Traspaso de Datos Repasat a A3ERP
                    {
                        cargarPedidos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, filtroDocumentosCarga());
                    }
                    else    //Traspaso de Datos A3ERP a Repasat
                    {
                        docsFromA3ToRPST(obtenerTipoObjetoRPST(), false, "NO");
                    }
                }
                //Envio Docs a Repasat
                //if (syncDocumento && !mainRPST) docsFromA3ToRPST("saledeliverynotes");
            }

            else if (tipoDocumento == "Cartera")
            {
                //Cargar Datos
                if (loadDocumento)
                {
                    if (mainRPST)  //Cargar datos de Repasat
                    {
                        dgvRPST.DataSource = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, syncDocumento, filtroDocumentosCarga());
                    }
                    else  //Cargar datos de A3ERP
                    {
                        dgvRPST.DataSource = docsFromA3ToRPST("incomingpayments", true, filtroDocumentosCarga());
                    }

                }
                //Syncronizar hacia A3ERP
                if (syncDocumento)
                {
                    if (mainRPST)   //Efectos
                    {
                        cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, true, filtroDocumentosCarga());
                    }
                    else
                    {
                        DataTable dtA3ERP = docsFromA3ToRPST("incomingpayments", false, filtroDocumentosCarga());
                        DataTable dtRPST = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, "TODOS", 0, false, false, null, null, null, true);
                        Repasat.csUpdateData updateRepasat = new Repasat.csUpdateData();
                        updateDataRPST.gestionarCarteraA3ToRepasat(dtA3ERP, dtRPST);
                    }
                }
                //Envio Docs a Repasat
                //if (syncDocumento && !mainRPST) docsFromA3ToRPST("saleinvoices");

            }
            else if (tipoDocumento == "Impagados" && mainRPST)
            {
                bool accion = syncDocumento ? true : false;
                dgvRPST.DataSource = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, accion, filtroDocumentosCarga(), 0, true);
            }

            else if (tipoDocumento == "Agrupaciones" && mainRPST)
            {
                bool accion = syncDocumento ? true : false;
                dgvRPST.DataSource = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, accion, filtroDocumentosCarga(), 0, false, false, null, null, null, false, true);

            }

            else if (tipoDocumento == "Remesas" && mainRPST)
            {
                bool accion = loadDocumento ? true : false;
                dgvRPST.DataSource = gestionarRemesas(filtroFechaIni, filtroFechaFin, rbVentas.Checked, accion, filtroDocumentosCarga());
            }
            else if (tipoDocumento == "Tarifas")
            {
                docsFromA3ToRPST(obtenerTipoObjetoRPST(), false, "NO");
            }



            informarDetalleGrid();

        }

        private void informarDetalleGrid()
        {
            tssLabelTotalRows.Text = dgvRPST.Rows.Count.ToString();

        }
        private void btnLoadDocsA3ERP_Click(object sender, EventArgs e)
        {
            inicioProceso();

            gestionarDocumentos(true, false, false);
            mostrarLogoDatosOrigen(false, false);
            inicioProceso(false);
        }

        private void btnLoadDocsRPST_Click(object sender, EventArgs e)
        {
            cargarDatosRepasat();

        }

        private void cargarDatosRepasat() { 
            inicioProceso();
            splitContainer8.Panel2Collapsed = true;
            splitContainer8.Panel2.Hide();
            gestionarDocumentos(true, false, true);
            mostrarLogoDatosOrigen(false, true);
            inicioProceso(false);
        
        }

        //función para sobreescribir la funcion ProcessCmdKey y asignar teclas rápidas
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.F5))
            {
                cargarDatosRepasat();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }



        private void btnSyncToA3ERP_Click(object sender, EventArgs e)
        {
            inicioProceso();
            gestionarDocumentos(false, true, true);
            inicioProceso(false);
        }

        private void btnSyncToRPST_Click(object sender, EventArgs e)
        {
            inicioProceso();
            gestionarDocumentos(false, true, false);
            inicioProceso(false);

        }

        private void mostrarBotones(bool mostrar)
        {
            btnSyncToA3ERP.Visible = mostrar;
            btnSyncToRPST.Visible = mostrar;
            btnLoadDocsA3ERP.Visible = mostrar;
            btnLoadDocsRPST.Visible = mostrar;

        }

        private void cboxTipoDoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (inicializarDgvRPST)
            {
                dgvRPST.DataSource = null;
                dgvRPST.Columns.Clear();
                inicializarDgvRPST = true;
            }

            if (string.IsNullOrEmpty(cboxTipoDoc.SelectedItem.ToString()))
            {
                mostrarBotones(false);
            }
            else
            {
                mostrarBotones(true);
            }

            if (cboxTipoDoc.SelectedItem == "Facturas")
            {
                grBoxTicketBai.Visible = csGlobal.ticketBaiActivo ? true : false;
            }
            else {
                grBoxTicketBai.Visible = false;
            }






            if (cboxTipoDoc.SelectedItem == "Cartera")
            {
                grBoxEstadoCobro.Visible = true;
                grBoxRemesado.Visible = true;
                grBoxCuenta.Visible = true;
                grBoxDocPago.Visible = true;
                cboxDocPagos.Visible = false;
                checkBoxDocPagos.Checked = false;
                mostrarSelectorFechasCartera(true);

                if (!csGlobal.conexionDB.Contains("REPASATA3ERP"))
                {
                    if (csGlobal.modoGestionCarteraRPST)
                    {
                        btnSyncToRPST.Visible = false;
                    }
                    else
                    {
                        btnSyncToA3ERP.Visible = false;
                    }
                }

                ctextMenuSync.Items["carteraToolStripMenuItem"].Visible = true;
                ctextMenuSync.Items["remesasToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["documentosToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["ticketBaiToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["a3ERPRPSTToolStripMenuItem"].Visible = false;
            }
            else if (cboxTipoDoc.SelectedItem == "Remesas")
            {
                remesasToolStripMenuItem.Visible = true;
                grBoxEstadoCobro.Visible = true;
                grBoxDocPago.Visible = false;
                grBoxRemesado.Visible = false;
                grBoxCuenta.Visible = false;
                grBoxSeries.Visible = false;

                ctextMenuSync.Items["carteraToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["remesasToolStripMenuItem"].Visible = true;
                ctextMenuSync.Items["documentosToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["ticketBaiToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["a3ERPRPSTToolStripMenuItem"].Visible = false;

                if (!csGlobal.conexionDB.Contains("REPASATA3ERP"))
                {
                    if (csGlobal.modoGestionCarteraRPST)
                    {
                        btnSyncToRPST.Visible = false;
                    }
                    else
                    {
                        btnSyncToA3ERP.Visible = false;
                    }
                }

            }
            else
            {
                grBoxEstadoCobro.Visible = false;
                grBoxRemesado.Visible = false;
                btnSyncToRPST.Visible = true;
                btnSyncToA3ERP.Visible = true;
                //grBoxAccounts.Visible = false;
                grBoxDocPago.Visible = false;
                ctextMenuSync.Items["carteraToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["remesasToolStripMenuItem"].Visible = false;
                ctextMenuSync.Items["documentosToolStripMenuItem"].Visible = true;
                mostrarSelectorFechasCartera(false);
            }
        }

        private void btnPlanContable_Click(object sender, EventArgs e)
        {
            informObjectType("accountingaccounts");
            campoExternoRepasat = "codExtCuentaContable";

            tablaToUpdate = "CUENTAS";
            campoKeyToUpdate = "CUENTA";
            repasatKeyField = "RPST_ID_CTA";

            dgvRepasatData.DataSource = loadRPSTObject.cuentasContablesRepasat();
            cuentasContablesA3ERP();
        }

        private void btnUploadCuentasContables_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las cuentas contables no asignadas, ¿está seguro/a?", "Sincronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarCuentasContablesToRPST();
                dgvRepasatData.DataSource = loadRPSTObject.cuentasContablesRepasat();
                cuentasContablesA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnBanks_Click(object sender, EventArgs e)
        {
            informObjectType("banks");
            tablaToUpdate = "BANCOS";
            campoKeyToUpdate = "CODBAN";
            repasatKeyField = "RPST_ID_BANCO";
            campoExternoRepasat = "bank[codExternoDatosBancarios]";
            Bancos();
            bancosA3ERP();
        }

        private void btnProyectos_Click(object sender, EventArgs e)
        {
            informObjectType("projects");
            tablaToUpdate = "CENTROSC";
            campoKeyToUpdate = "CENTROCOSTE";
            repasatKeyField = "RPST_ID_CENTROC";
            campoExternoRepasat = "codExternoProyecto";
            Proyectos();
            proyectosA3ERP();
        }

        private void btnCrearProyectosA3_Click(object sender, EventArgs e)
        {
            csa3erp a3 = new klsync.csa3erp();
            a3.abrirEnlace();
            a3ERPActiveX.Maestro naxProyecto = new a3ERPActiveX.Maestro();
            naxProyecto.Iniciar("CENTROSC");

            foreach (DataGridViewRow proyectos in dgvRepasatData.SelectedRows)
            {
                naxProyecto.Nuevo();

                naxProyecto.set_AsString("CENTROCOSTE", proyectos.Cells["CODIGOERP"].Value.ToString());
                naxProyecto.set_AsString("DESCCENTRO", proyectos.Cells["DESCRIPCION"].Value.ToString());
                naxProyecto.set_AsString("APLINIVEL1", "F");
                naxProyecto.set_AsString("APLINIVEL2", "F");
                naxProyecto.set_AsString("APLINIVEL3", "T");
                naxProyecto.set_AsString("OBSOLETO", "F");
                naxProyecto.set_AsString("BLOQUEADO", "F");
                naxProyecto.set_AsString("RPST_ID_CENTROC", proyectos.Cells["IDREPASAT"].Value.ToString());

                naxProyecto.Guarda(true);
            }

            string codigo = "";
            naxProyecto.Acabar();

            a3.cerrarEnlace();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            dgvA3ERPData.DataSource = null;
            dgvRepasatData.DataSource = null;
            tipoObjeto = "retentiontypes";
            tablaToUpdate = "CENTROSC";
            campoKeyToUpdate = "CENTROCOSTE";
            repasatKeyField = "RPST_ID_CENTROC";
            campoExternoRepasat = "codExternoProyecto";
            Retenciones();
            proyectosA3ERP();
        }

        private void dgvRPST_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                string[] columnsShortCuts = { "RPST", "NUMFRA_QR", "NUMSERIE_QR", "SERIE_QR","NUMFAC_QR", "REPASAT_ID_DOC" };
                if ( columnsShortCuts.Contains(this.dgvRPST.Columns[e.ColumnIndex].Name))
                {
                    string tipoDocumento = "";
                    string urlProduct = "";
                    string idDocument = "";

                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Facturas" && rbVentas.Checked ? "saleinvoices/show" : tipoDocumento;
                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Facturas" && rbCompras.Checked ? "purchaseinvoices/show" : tipoDocumento;
                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Pedidos" && rbVentas.Checked ? "saleorders/show" : tipoDocumento;
                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Pedidos" && rbCompras.Checked ? "purchaseorders/show" : tipoDocumento;
                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Albaranes" && rbVentas.Checked ? "saledeliverynotes/show" : tipoDocumento;
                    tipoDocumento = cboxTipoDoc.SelectedItem.ToString() == "Albaranes" && rbCompras.Checked ? "purchasedeliverynotes/show" : tipoDocumento;

                    if (dgvRPST["REPASAT_ID_DOC", e.RowIndex].Value.ToString() != "" && !string.IsNullOrEmpty(tipoDocumento))
                    {
                        try
                        {
                            idDocument = dgvRPST["REPASAT_ID_DOC", e.RowIndex].Value.ToString();
                            urlProduct = "https://panel.repasat.com/es/" + tipoDocumento + "/" + idDocument;
                            Process.Start("Chrome.exe", urlProduct);
                        }
                        catch
                        {
                            Process.Start("IExplore.exe", urlProduct);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El artículo no está en la tienda");
                    }
                }
                if (this.dgvRPST.Columns[e.ColumnIndex].Name == "FECHA")
                {
                    dtpFromInvoicesV.Value = Convert.ToDateTime(dgvRPST["FECHA", e.RowIndex].Value.ToString());
                    dtpToInvoicesV.Value = Convert.ToDateTime(dgvRPST["FECHA", e.RowIndex].Value.ToString());
                }
                if (this.dgvRPST.Columns[e.ColumnIndex].Name == "FECHA VTO")
                {
                    dtpFromInvoicesV.Value = Convert.ToDateTime(dgvRPST["FECHA VTO", e.RowIndex].Value.ToString());
                    dtpToInvoicesV.Value = Convert.ToDateTime(dgvRPST["FECHA VTO", e.RowIndex].Value.ToString());
                }
            }
        }

        private void btnTiposImpuestos_Click(object sender, EventArgs e)
        {
            tipoObjeto = "taxoperation";
            campoExternoRepasat = "codExternoDocuPago";

            tablaToUpdate = "TIPOIVA";
            campoKeyToUpdate = "TIPIVA";
            repasatKeyField = "RPST_ID_IVA";

            dgvRepasatData.DataSource = loadRPSTObject.tiposIVARepasat();
            tiposIVAA3ERP();
        }

        private void actualizarEnA3ERPSelecciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idCliente = "";
            foreach (DataGridViewRow dgvselrow in dgvAccountsRepasat.SelectedRows)
            {
                idCliente = dgvselrow.Cells["CODIGOERP"].Value.ToString();
                cargarDatosCuentasRepasat(objetoCliente(), false, idCliente, false, false, true);
            }
        }

        private void recibirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string numDoc = "";
            string numEfecto = "";
            string fecha = "";
            csa3erpCartera a3Cartera = new csa3erpCartera();
            csa3erp a3erp = new csa3erp();
            try
            {
                a3erp.abrirEnlace();
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    numDoc = dgvRPST.SelectedRows[0].Cells["IDA3 FRA"].Value.ToString();
                    numEfecto = dgvRPST.SelectedRows[0].Cells["NUM VTO"].Value.ToString();
                    fecha = dgvRPST.SelectedRows[0].Cells["FECHA PAGO"].Value.ToString();

                    a3Cartera.recibirEfecto(true, numDoc, Convert.ToInt16(numEfecto), fecha);
                }
                a3erp.cerrarEnlace();
            }
            catch { }

        }

        private void actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string idDocA3ERP = "";
                string idDocRPST = "";
                string codCuentaA3 = "";
                string documentos = "";
                string query = "";
                string fechaDoc = "";
                string totalDoc = "";
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    codCuentaA3 = (fila.Cells["CLIENTE"].Value.ToString().Split('(')[1]).Replace(")", string.Empty);
                    idDocA3ERP = fila.Cells["EXTERNALID"].Value.ToString();
                    idDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();
                    fechaDoc = fila.Cells["FECHA"].Value.ToString();
                    totalDoc = fila.Cells["IMPORTE_DOC"].Value.ToString().Replace(",", ".");

                    //query = "SELECT CAST (IDFACV AS INT) AS IDFACV FROM CABEFACV " +
                    //    " WHERE FECHA='" + fechaDoc + "' AND LTRIM(CODCLI)='" + codCuentaA3 + "' AND TOTDOC=" + totalDoc + " AND IDFACV=" + idDocA3ERP +
                    //    " UNION " +
                    //    " SELECT CAST(IDFACV AS INT) AS IDFACV FROM CABEFACV " +
                    //    " WHERE FECHA = '" + fechaDoc + "' AND LTRIM(CODCLI) = '" + codCuentaA3 + "' AND TOTDOC = " + totalDoc;
                    query = "SELECT CAST (IDFACV AS INT) AS IDFACV FROM CABEFACV " +
                        " WHERE FECHA='" + fechaDoc + "' AND LTRIM(CODCLI)='" + codCuentaA3 + "' AND TOTDOC=" + totalDoc + " AND IDFACV=" + idDocA3ERP ;

                    documentos = sql.obtenerCampoTabla(query, false);
                    if (!string.IsNullOrEmpty(documentos))
                    {
                        idDocA3ERP = idDocA3ERP != documentos ? documentos : idDocA3ERP;
                        csUtilidades.ejecutarConsulta("UPDATE CABEFACV SET RPST_ID_FACV=" + idDocRPST + " WHERE IDFACV=" + idDocA3ERP, false);
                    }
                }
                "Actualización Realizada".mb();
            }
            catch { }

        }


        private void resetearCodExterno()
        {
            try
            {
                string SeriePS = "";
                string SerieA3 = "";
                string NumDoc = "";
                string numVto = "";
                string idDocRPST = "";
                string idFrav = "";
                string rpst_id_facv = "";
                string scriptGet = "";
                string scriptUpdate = "";
                string idCartera = "";


                DataTable Objeto = new DataTable();

                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    //SeriePS = (fila.Cells["SERIE"].Value.ToString().Split('('))[1];
                    //SeriePS = SeriePS.Replace(")", "");
                    //SerieA3 = (fila.Cells["SERIE"].Value.ToString().Split('('))[0].Trim();
                    NumDoc = fila.Cells["ID_REPASAT_FRA"].Value.ToString();
                    numVto = fila.Cells["NUMVEN"].Value.ToString();
                    idCartera = fila.Cells["IDCARTERA"].Value.ToString();

                    //idDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();

                    Objeto = cargarEfectos("1900-01-01", "2099-12-31", rbVentas.Checked, false, filtroDocumentosCarga(), 0, false, false, NumDoc, numVto, SeriePS);
                    //scriptGet = "SELECT CABEFACV.RPST_ID_FACV FROM CABEFACV WHERE LTRIM(SERIE)='" + Serie + "' AND NUMDOC=" + NumDoc;
                    if (Objeto.Rows.Count > 0)
                    {
                        idDocRPST = Objeto.Rows[0]["REPASAT_ID_DOC"].ToString();

                        if (!string.IsNullOrEmpty(idDocRPST))
                        {
                            scriptUpdate = "UPDATE CARTERA SET RPST_ID_CARTERA=" + idDocRPST + " WHERE IDCARTERA=" + idCartera;
                            sql.actualizarCampo(scriptUpdate, false);

                        }
                    }



                }
                sql.cerrarConexion();
                "Proceso finalizado".mb();

            }
            catch (Exception ex)
            {
                (ex.Message).mb();
                sql.cerrarConexion();
            }


        }

        private void resetearEnlaceEnRepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetearCodExterno();
        }

        private void cargarCuentas(bool Ventas = true)
        {
            try
            {
                if (checkBoxCuentas.Checked)
                {
                    cboxCuentas.Visible = true;
                    string sqlScript = "";
                    sqlScript = Ventas ? "SELECT '0' AS CODCUENTA,'' AS NOMCUENTA " + " UNION " + " SELECT LTRIM(CODCLI), NOMCLI + '-' +  LTRIM(CODCLI) FROM CLIENTES" :
                        "SELECT '0' AS CODCUENTA,'' AS NOMCUENTA " + " UNION " + " SELECT LTRIM(CODPRO), NOMPRO + '|*|' + LTRIM(CODPRO) FROM PROVEED";
                    SqlConnection dataConnection = new SqlConnection();
                    csSqlConnects sqlConnect = new csSqlConnects();
                    dataConnection.ConnectionString = csGlobal.cadenaConexion;
                    dataConnection.Open();
                    SqlDataAdapter a = new SqlDataAdapter(sqlScript, dataConnection);
                    DataTable t1 = new DataTable();
                    a.Fill(t1);

                    cboxCuentas.ValueMember = "CODCUENTA";
                    cboxCuentas.DisplayMember = "NOMCUENTA";
                    cboxCuentas.DataSource = t1;

                    dataConnection.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void cboxCuentas_CheckedChanged(object sender, EventArgs e)
        {
            cargarCuentas(rbVentas.Checked ? true : false);
            if (!checkBoxCuentas.Checked)
            {
                cboxCuentas.Visible = false;
                cboxCuentas.DataSource = null;
            }
        }

        private void rbVentas_CheckedChanged(object sender, EventArgs e)
        {
            cargarCuentas(rbVentas.Checked ? true : false);
        }

        private void asignarBancoDeSincronizacion()
        {
            try
            {
                string idEfectoRPST = "";
                bool efectoCobrado = false;
                string idBancoEfecto = "";
                string cuentaContableBancoEfecto = "";

                Repasat.csUpdateData updateRepasat = new Repasat.csUpdateData();

                if (dgvRPST.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow fila in dgvRPST.SelectedRows) { }
                }
            }
            catch { }



        }

        private void actualizarCarteraRepasatDesdeA3(bool filasSeleccionadas = false, bool resetearEfectos = false, bool ventas = true, bool updateEfectosPagados = false)
        {
            Repasat.csUpdateData updateRepasat = new Repasat.csUpdateData();
            string filtroFechaIni = "";
            string filtroFechaFin = "";
            string numDoc = "";
            string serieA3 = "";
            string idFraRepasat = "";
            string numVto = "";
            bool enlaceCorrectoFra = true;
            bool enlaceCorrectoCart = true;

            if (filasSeleccionadas && dgvRPST.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    if (resetearEfectos)
                    {
                        anularPago(fila.Cells["REPASAT_ID_DOC"].Value.ToString(), rbVentas.Checked);
                    }
                    else
                    {
                        filtroFechaIni = Convert.ToDateTime(fila.Cells["FECHA VTO"].Value.ToString()).ToString("yyyy-MM-dd");
                        filtroFechaFin = Convert.ToDateTime(fila.Cells["FECHA VTO"].Value.ToString()).ToString("yyyy-MM-dd");
                        numDoc = fila.Cells["NUM_FRA"].Value.ToString();
                        serieA3 = fila.Cells["SERIE"].Value.ToString();
                        numVto = fila.Cells["NUM VTO"].Value.ToString();
                        idFraRepasat = fila.Cells["RPST IDFRA"].Value.ToString();
                        string tablaFra = ventas ? "CABEFACV" : "CABEFACC";
                        string txtProcedeTabla = ventas ? "FV" : "FC";
                        string keyFieldTablaFra = ventas ? "IDFACV" : "IDFACC";
                        string anexoWhere = ventas ? " AND COBPAG='C' " : " AND COBPAG='P' ";

                        //serieA3 = sql.obtenerCampoTabla("SELECT LTRIM(SERIE) AS SERIE FROM SERIES WHERE RPST_ID_SERIE=" + serieA3,false);

                        //Verifico si la factura está enlazada
                        enlaceCorrectoFra = sql.obtenerCampoTabla("SELECT DESLIGCART FROM " + tablaFra + " WHERE LTRIM(SERIE)='" + serieA3 + "' AND NUMDOC= + " + numDoc, false) == "F" ? true : false;
                        enlaceCorrectoCart = sql.obtenerCampoTabla("SELECT CAST(PROCEDEID as int) as PROCEDEID FROM CARTERA WHERE LTRIM(SERIE)='" + serieA3 + "' AND NUMDOC= + " + numDoc + anexoWhere, false) == "0" ? false : true;

                        if (!enlaceCorrectoCart || !enlaceCorrectoFra)
                        {

                            DialogResult Resultado = MessageBox.Show("El Efecto " + serieA3 + " / " + numDoc + " no está enlazado correctamente dentro A3ERP. ¿Quieres enlazar el efecto con la factura manualmente?", "Enlazar Documento Manualmente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (Resultado == DialogResult.Yes)
                            {
                                string idFact = sql.obtenerCampoTabla("SELECT CAST(" + keyFieldTablaFra + " as int) as IDFAC FROM  " + tablaFra + "  WHERE LTRIM(SERIE)='" + serieA3 + "' AND NUMDOC= + " + numDoc, false);
                                csUtilidades.ejecutarConsulta("UPDATE CARTERA SET PROCEDE='" + txtProcedeTabla + "', PROCEDEID=" + idFact + " WHERE LTRIM(SERIE)='" + serieA3 + "' AND NUMDOC= + " + numDoc + anexoWhere, false);
                                "Efecto Enlazado".mb();
                            }


                        }



                        DataTable dtA3ERP = docsFromA3ToRPST("incomingpayments", false, filtroDocumentosCarga(), true, serieA3, numDoc, null, null, true);
                        DataTable dtRPST = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, "NO", 0, false, false, idFraRepasat, numVto, null, false);

                        //Si en A3 no hay filas, no entramos a actualizar
                        if (dtA3ERP.Rows.Count > 0)
                        {
                            updateDataRPST.gestionarCarteraA3ToRepasat(dtA3ERP, dtRPST, rbVentas.Checked, resetearEfectos, updateEfectosPagados);
                        }
                    }
                }
            }
            else
            {
                filtroFechaIni = dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");

                DataTable dtA3ERP = docsFromA3ToRPST("incomingpayments", false, filtroDocumentosCarga(), true);
                DataTable dtRPST = cargarEfectos(filtroFechaIni, filtroFechaFin, rbVentas.Checked, false, "NO");

                updateDataRPST.gestionarCarteraA3ToRepasat(dtA3ERP, dtRPST, rbVentas.Checked);
            }

        }

        private void mostrarLogoDatosOrigen(bool ocultar = false, bool Repasat = true)
        {
            if (ocultar)
            {
                lblShowData.Visible = false;
                picBoxA3.Visible = false;
                picboxA3ERP.Visible = false;
            }
            else
            {
                lblShowData.Visible = true;
                if (Repasat)
                {
                    picBoxA3.Visible = false;
                    picBoxRPST.Visible = true;
                    rdbutFromRPST.Checked = true;
                    rdbutFromA3ERP.Checked = false;
                    rdbutFromRPST.Visible = true;
                    rdbutFromA3ERP.Visible = false;

                }
                else
                {
                    picBoxA3.Visible = true;
                    picBoxRPST.Visible = false;
                    rdbutFromRPST.Checked = false;
                    rdbutFromA3ERP.Checked = true;
                    rdbutFromRPST.Visible = false;
                    rdbutFromA3ERP.Visible = true;
                }
            }


        }

        private void consultaActualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarCarteraRepasatDesdeA3();
        }

        private void filasSeleccionadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarCarteraRepasatDesdeA3(true, false, rbVentas.Checked, true);
        }

        private void resetearEfectoEnRPSTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarCarteraRepasatDesdeA3(true, true, rbVentas.Checked, false);
        }

        private void cargarEfectosRemesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string numRemesa = dgvRPST.CurrentRow.Cells["NUMREMESA"].Value.ToString();
            dgvSyncDocsDetail.DataSource = cargarEfectos("1900-01-01", "2999-12-31", rbVentas.Checked, false, "TODOS", Convert.ToInt16(numRemesa), false, true);
            inicializarDgvRPST = false;
            cboxTipoDoc.SelectedIndex = 3;
            splitContainer8.Panel2Collapsed = false;
            splitContainer8.Panel2.Show();
        }

        private void verificarImportesRemesas(string objeto)
        {

            string scriptTabla = "";
            scriptTabla = objeto == "remesas" ? "SELECT ROUND(IMPORTE,2) FROM __REMESAS WHERE RPST_ID_REMESA= " : scriptTabla;
            scriptTabla = objeto == "facturaV" ? "SELECT ROUND(TOTDOC,2) FROM CABEFACV WHERE RPST_IDFACV= " : scriptTabla;
            scriptTabla = objeto == "facturaC" ? "SELECT ROUND(TOTDOC,2) FROM CABEFACV WHERE RPST_IDFACV= " : scriptTabla;
            scriptTabla = objeto == "efectosV" ? "SELECT ROUND(IMPORTE,2) FROM CARTERA WHERE RPST_IDFACV= " : scriptTabla;

            csSqlConnects sql = new klsync.csSqlConnects();
            string importeDocA3 = "";
            string importeDocRPST = "";
            int numFila = 0;
            string serieDoc = "";
            string numDoc = "";
            string idDocA3 = "";
            string idDocRPST = "";
            string idRemesaRPST = "";
            double diferencia = 0;
            if (dgvRPST.Rows.Count > 0)
            {
                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgvRPST.Rows)
                {
                    idDocA3 = fila.Cells["EXTERNALID"].Value.ToString();
                    idDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();
                    idRemesaRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();

                    if (string.IsNullOrEmpty(idDocA3)) continue;

                    importeDocRPST = fila.Cells["IMPORTE"].Value.ToString();
                    importeDocA3 = sql.obtenerCampoTabla(scriptTabla + idRemesaRPST);
                    importeDocA3 = string.IsNullOrEmpty(importeDocA3) ? "0" : Math.Round(Convert.ToDouble(importeDocA3), 2).ToString();
                    if (importeDocRPST != importeDocA3)
                    {
                        diferencia = Convert.ToDouble(importeDocA3) - Convert.ToDouble(importeDocRPST);
                        diferencia = Math.Round(diferencia, 2);
                        if (diferencia == 0)
                        {
                            fila.Cells["ACCIÓN"].Value = "IMPORTE VERIFICADO";
                        }
                        else
                        {
                            fila.Cells["ACCIÓN"].Value = "REVISAR DOC: IMPORTE EN A3=" + importeDocA3 + "  (Dif: " + diferencia.ToString() + ")"; ;
                        }
                    }
                    else
                    {
                        fila.Cells["ACCIÓN"].Value = "IMPORTE VERIFICADO";
                    }
                }


                sql.cerrarConexion();

            }
        }

        private void verificarImportesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            verificarImportesRemesas("remesas");
        }

        private void verificarImportesDocumentos(string objeto, bool totalDoc = true, bool numLines=false)
        {
            try
            {
                string campoImporte = totalDoc ? "TOTDOC" : "BASE";
                string campoImporteRPST = totalDoc ? "IMPORTE_DOC" : "BASE_DOC";
                string tabla = "";
                string campoId = "";
                string scriptTabla = ""; 
                csSqlConnects sql = new klsync.csSqlConnects();
                string importeDocA3 = "";
                string importeDocRPST = "";
                string numLinesDocRPST = "";
                string numLinesDocA3 = "";
                int numFila = 0;
                string serieDoc = "";
                string numDoc = "";
                string idDocA3 = "";
                string idDocRPST = "";
                string idRemesaRPST = "";
                double diferencia = 0;
                bool ventas = rbVentas.Checked;

                switch (objeto)
                {
                    case "Facturas":
                       tabla = ventas ? "CABEFACV" : "CABEFACC"; 
                       campoId = ventas ? "IDFACV" : "IDFACC";
                        break;
                    case "Pedidos":
                       tabla = ventas ? "CABEPEDV" : "CABEPEDC";
                       campoId = ventas ? "IDPEDV" : "IDPEDC";
                        break;
                    default:
                        return;
                }


                if (!numLines)
                {
                    scriptTabla = "SELECT ROUND(" + campoImporte + ",2) FROM " + tabla + " WHERE " + campoId + "= ";
                }
                else {
                    scriptTabla = "SELECT COUNT(*) FROM LINEPEDI WHERE IDPEDV = " + campoId + " GROUP BY IDPEDV";
                }

               
                if (dgvRPST.Rows.Count > 0)
                {
                    sql.abrirConexion();

                    foreach (DataGridViewRow fila in dgvRPST.Rows)
                    {
                        idDocA3 = fila.Cells["EXTERNALID"].Value.ToString();
                        idDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();
                        if (string.IsNullOrEmpty(idDocA3)) continue;

                        importeDocRPST = fila.Cells[campoImporteRPST].Value.ToString();

                        if (!numLines)
                        {
                            importeDocA3 = sql.obtenerCampoTabla(scriptTabla + idDocA3);
                            importeDocA3 = string.IsNullOrEmpty(importeDocA3) ? "0" : Math.Round(Convert.ToDouble(importeDocA3), 2).ToString();

                            if (importeDocRPST != importeDocA3)
                            {
                                diferencia = Convert.ToDouble(importeDocA3) - Convert.ToDouble(importeDocRPST);
                                diferencia = Math.Round(diferencia, 2);
                                fila.Cells["ACCIÓN"].Value = diferencia == 0 ? "IMPORTE VERIFICADO" : "REVISAR DOC: IMPORTE EN A3=" + importeDocA3 + "  (Dif: " + diferencia.ToString() + ")";
                            }
                            else
                            {
                                fila.Cells["ACCIÓN"].Value = "IMPORTE VERIFICADO";
                            }
                        }
                        else
                        {
                            scriptTabla = "SELECT COUNT(*) AS LINES FROM LINEPEDI WHERE IDPEDV = " + idDocA3 + " GROUP BY IDPEDV";
                            numLinesDocA3 = sql.obtenerCampoTabla(scriptTabla);
                            numLinesDocA3 = string.IsNullOrEmpty(numLinesDocA3) ? "0" : Convert.ToInt32(numLinesDocA3).ToString();

                            numLinesDocRPST = fila.Cells["LINEAS"].Value.ToString();
                            
                            if (numLinesDocA3 != numLinesDocRPST)
                            {
                                diferencia = Convert.ToDouble(numLinesDocA3) - Convert.ToDouble(numLinesDocRPST);
                                diferencia = Math.Round(diferencia, 0);
                                fila.Cells["ACCIÓN"].Value = diferencia == 0 ? "LINEAS OK " : "DIFERENCIAS: LIN. EN A3=" + numLinesDocA3 + "  (Dif: " + diferencia.ToString() + ")";
                            }
                            else
                            {
                                fila.Cells["ACCIÓN"].Value = "LINEAS OK";
                            }
                        }
                    }
                    sql.cerrarConexion();
                }
            }
            catch (Exception ex) { 
            
            }
        }

        private void verificarImportesEfectos()
        {


            string scriptTabla = "";
            scriptTabla = "SELECT ROUND(IMPORTE,2) FROM CARTERA WHERE LTRIM(SERIERPST_ID_REMESA= ";

            csSqlConnects sql = new klsync.csSqlConnects();
            string importeDocA3 = "";
            string importeDocRPST = "";
            int numFila = 0;
            string serieDoc = "";
            string numDoc = "";
            string idDoc = "";
            string idRemesaRPST = "";
            double diferencia = 0;
            string numRemesa = "";
            string numVto = "";

            if (dgvRPST.Rows.Count > 0)
            {
                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgvRPST.Rows)
                {
                    idDoc = fila.Cells["EXTERNALID"].Value.ToString();
                    numRemesa = fila.Cells["NUMREMESA"].Value.ToString();
                    numDoc = fila.Cells["NUM_FRA"].Value.ToString();
                    serieDoc = fila.Cells["SERIE"].Value.ToString();
                    numVto = fila.Cells["NUM VTO"].Value.ToString();

                    if (!string.IsNullOrEmpty(numRemesa))
                    {
                        scriptTabla = "SELECT ROUND(IMPORTE,2) FROM CARTERA WHERE LTRIM(SERIE)='" + serieDoc + "' AND NUMDOC=" + numDoc + " AND NUMVEN=" + numVto;
                    }
                    else
                    {
                        scriptTabla = "SELECT ROUND(IMPORTE,2) FROM CARTERA WHERE LTRIM(SERIE)='" + serieDoc + "' AND NUMDOC=" + numDoc + " AND NUMVEN=" + numVto;
                    }

                    //    if (string.IsNullOrEmpty(idDoc)) continue;

                    importeDocRPST = fila.Cells["IMPORTE_EFECTO"].Value.ToString();
                    importeDocA3 = sql.obtenerCampoTabla(scriptTabla);
                    importeDocA3 = string.IsNullOrEmpty(importeDocA3) ? "0" : Math.Round(Convert.ToDouble(importeDocA3), 2).ToString();
                    if (importeDocRPST != importeDocA3)
                    {
                        diferencia = Convert.ToDouble(importeDocA3) - Convert.ToDouble(importeDocRPST);
                        diferencia = Math.Round(diferencia, 2);
                        if (diferencia == 0)
                        {
                            fila.Cells["ACCIÓN"].Value = "IMPORTE VERIFICADO";
                        }
                        else
                        {
                            fila.Cells["ACCIÓN"].Value = "REVISAR DOC: IMPORTE EN A3=" + importeDocA3 + "  (Dif: " + diferencia.ToString() + ")"; ;
                        }
                    }
                    else
                    {
                        fila.Cells["ACCIÓN"].Value = "IMPORTE VERIFICADO";
                    }
                }

                sql.cerrarConexion();

            }
        }


        private void mostrarSelectorFechasCartera(bool fechaFra = false)
        {
            if (fechaFra)
            {
                rbutFechaFactura.Visible = true;
                rbutFechaVencimiento.Visible = true;
            }
            else
            {
                rbutFechaFactura.Visible = false;
                rbutFechaVencimiento.Visible = false;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            csa3erp a3erp = new csa3erp();
            csa3erpCartera a3car = new csa3erpCartera();

            a3erp.abrirEnlace();
            a3car.crearEfectoManual("2019", "1900001", "7", "20/05/2019", "1250.65", true);
            a3erp.cerrarEnlace();
        }

        private void verificarSituaciónEfectosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            verificarSituacionEfectos(dgvSyncDocsDetail);
        }

        private void verificarIDsSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarIDSincronizacionRemesas();
        }

        private void verificarIDSincronizacionRemesas()
        {
            try
            {
                string idRemesaRPST = "";
                string idRemesaA3ERP = "";
                string numRemesaRPST = "";
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    idRemesaRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();
                    numRemesaRPST = fila.Cells["NUMREMESA"].Value.ToString();
                    idRemesaA3ERP = fila.Cells["EXTERNALID"].Value.ToString();

                    csUtilidades.ejecutarConsulta("UPDATE __REMESAS SET " +
                        " REFERENCIA='REMESA REPASAT Nº " + numRemesaRPST + "' , RPST_ID_REMESA=" + idRemesaRPST + " WHERE IDREMESA=" + idRemesaA3ERP, false);

                }
                "PROCESO FINALIZADO".mb();
            }
            catch
            { }
        }

        private void recibirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string numDoc = "";
            string numEfecto = "";
            string fecha = "";
            csa3erpCartera a3Cartera = new csa3erpCartera();
            csa3erp a3erp = new csa3erp();
            try
            {
                a3erp.abrirEnlace();
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    numDoc = dgvRPST.SelectedRows[0].Cells["IDA3 FRA"].Value.ToString();
                    numEfecto = dgvRPST.SelectedRows[0].Cells["NUM VTO"].Value.ToString();
                    fecha = dgvRPST.SelectedRows[0].Cells["FECHA PAGO"].Value.ToString();

                    a3Cartera.recibirEfecto(true, numDoc, Convert.ToInt16(numEfecto), fecha);
                }
                a3erp.cerrarEnlace();
            }
            catch { }
        }

        private void verificarImportesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            verificarImportesEfectos();
        }

        private void verificarSincronizaciónDeFacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string SeriePS = "";
                string SerieA3 = "";
                string NumDoc = "";
                string idDocRPST = "";
                string idFrav = "";
                string rpst_id_facv = "";
                string scriptGet = "";
                string scriptUpdateV = "";
                string scriptUpdateC = "";


                DataTable factura = new DataTable();

                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {

                    if (dgvRPST.Columns.Contains("origenDocumento")) //si actualizamos desde A3ERP
                    {
                        SeriePS = (fila.Cells["SERIE"].Value.ToString().Split('('))[1];
                        SeriePS = SeriePS.Replace(")", "");
                        SerieA3 = (fila.Cells["SERIE"].Value.ToString().Split('('))[0].Trim();
                        NumDoc = fila.Cells["NUMDOC"].Value.ToString();
                        factura = cargarFacturasFromRPSToA3("1900-01-01", "2099-12-31", rbVentas.Checked, true, filtroDocumentosCarga(), SeriePS, NumDoc);
                    }
                    else //Si actualizamos desde el listado de Repasat hacia A3ERP
                    {
                        SerieA3 = (fila.Cells["SERIE"].Value.ToString().Split('('))[0].Trim();
                        NumDoc = fila.Cells["NUM_FRA"].Value.ToString();
                        idDocRPST = fila.Cells["REPASAT_ID_DOC"].Value.ToString();
                    }

                    if (factura.Rows.Count > 0)
                    {
                        idDocRPST = factura.Rows[0]["REPASAT_ID_DOC"].ToString();
                    }

                    if (!string.IsNullOrEmpty(idDocRPST))
                    {
                        scriptUpdateV = "UPDATE CABEFACV SET RPST_ID_FACV=" + idDocRPST + " WHERE LTRIM(SERIE)='" + SerieA3 + "' AND NUMDOC=" + NumDoc;
                        scriptUpdateC = "UPDATE CABEFACC SET RPST_ID_FACC=" + idDocRPST + " WHERE LTRIM(SERIE)='" + SerieA3 + "' AND NUMDOC=" + NumDoc;
                        sql.actualizarCampo(rbVentas.Checked ? scriptUpdateV : scriptUpdateC, false);

                    }



                }
                sql.cerrarConexion();
                "Proceso finalizado".mb();

            }
            catch (Exception ex)
            {
                (ex.Message).mb();
                sql.cerrarConexion();
            }
        }

        private void totalDocumentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarImportesDocumentos(cboxTipoDoc.Text);
        }

        private void baseDocumentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarImportesDocumentos(cboxTipoDoc.Text, false);
        }

        private void cargarDocumentosPagoRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoDocuPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtPaymentDocs = new DataTable();
            dtPaymentDocs.Columns.Add("DESCRIPCION");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csDocumentosPago[] documentoPago = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "paymentdocuments";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                    documentoPago = new Objetos.csDocumentosPago[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtPaymentDocs.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            documentoPago[ii] = new Objetos.csDocumentosPago();

                            documentoPago[ii].nomDocumentoPago = repasat.data[ii].nomDocuPago;

                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomDocuPago;

                            dtPaymentDocs.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < documentoPago.Length; iiii++)
                        {
                            if (documentoPago[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                        contador = 0;
                    }
                    cboxDocPagos.DataSource = dtPaymentDocs;
                    cboxDocPagos.DisplayMember = "DESCRIPCION";
                    cboxDocPagos.BindingContext = this.BindingContext;

                }
                else
                {
                    "No hay información para descargar".mb();
                }
            }
        }

        private void checkBoxDocPagos_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxDocPagos.Checked)
            {
                cboxDocPagos.Visible = true;
                cargarDocumentosPagoRepasat();
            }
            else
            {
                cboxDocPagos.Visible = false;
                cboxDocPagos.DataSource = null;
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            //Repasat.SyncRepasat.csSyncRepasat syncRepasat = new Repasat.SyncRepasat.csSyncRepasat();
            //syncRepasat.syncroDocs();

            Repasat.LoadObjects.csDtObjects rpstAuxObject = new Repasat.LoadObjects.csDtObjects();
            DataSet test = rpstAuxObject.loadRPSTAuxObjects();
            string hola = "hola";
        }

        private void informaciónSobreEfectoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string numFra = "";
                string numVto = "";
                string serie = "";

                numFra = dgvRPST.SelectedRows[0].Cells["NUM_FRA"].Value.ToString();
                serie = dgvRPST.SelectedRows[0].Cells["SERIE"].Value.ToString();

                string query = "  SELECT CONVERT(VARCHAR(10), FECHAFACTURA, 101) as FECHAFRA ,SERIE, NUMDOC,CONVERT(VARCHAR(10), FECHA, 101) as FECHAVTO, " +
                    " IMPORTE, NUMVEN, IDCARTERA, CASE WHEN COBPAG='C' THEN CODCLI ELSE CODPRO END AS CODIC,NOMBRE FROM CARTERA WHERE NUMDOC=" + numFra + " AND " +
                    " LTRIM(SERIE)='" + serie + "'";

                DataTable dtVencimientos = new DataTable();
                dtVencimientos = sql.obtenerDatosSQLScript(query);

                if (dtVencimientos.Rows.Count > 0)
                {
                    frDataTableModalForm f = new frDataTableModalForm();
                    f.dgvDataTable.DataSource = dtVencimientos;
                    f.ShowDialog(this);
                }
                else
                {
                    ("La factura " + serie + "-" + numFra + " no se encuentra traspasada a A3ERP").mb();
                }
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

        }

        private void forzarTraspasoConCódigoClienteInformadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            syncroData(true, false, false, true);
        }

        private void btnAsignBankRecibos_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "PROCESANDO";
            this.Refresh();
            actualizarBancosEfectosNoInformados();
            toolStripStatusLabel4.Text = "FIN PROCESO";
        }


        private void actualizarBancosEfectosNoInformados()
        {
            try
            {
                string idDomBanca = "";
                string iban = "";
                string banco = "";
                string oficina = "";
                string digitoControl = "";
                string cuenta = "";
                string codIC = "";
                string codICBank = "";
                string idCartera = "";


                DataTable dtCartera = new DataTable();
                dtCartera = sql.obtenerDatosSQLScript("SELECT IDCARTERA, CODCLI, NOMBRE, COBPAG, SERIE, NUMDOC, PAGADO, IDDOMBANCA, IBAN, BANCO, DIGITO, " +
                    " NUMCUENTA, IMPORTE FROM CARTERA WHERE PAGADO = 'F' AND IDDOMBANCA IS NULL  ");

                DataTable dtBancosCLi = new DataTable();
                dtBancosCLi = sql.obtenerDatosSQLScript("SELECT * FROM DOMBANCA WHERE DEFECTO='T'");

                foreach (DataRow dr in dtCartera.Rows)
                {
                    codIC = dr["CODCLI"].ToString();
                    idCartera = dr["IDCARTERA"].ToString().Replace(",0000", "");
                    foreach (DataRow drbank in dtBancosCLi.Rows)
                    {
                        codICBank = drbank["CODCLI"].ToString();
                        if (codICBank == codIC)
                        {
                            idDomBanca = drbank["IDDOMBANCA"].ToString().Replace(",0000", "");
                            iban = drbank["IBAN"].ToString();
                            banco = drbank["BANCO"].ToString();
                            oficina = drbank["AGENCIA"].ToString();
                            digitoControl = drbank["DIGITO"].ToString();
                            cuenta = drbank["NUMCUENTA"].ToString();

                            sql.actualizarCampo("UPDATE CARTERA SET IDDOMBANCA=" + idDomBanca + ", IBAN='" + iban + "', BANCO='" + banco + "', AGENCIA='" +
                                oficina + "', DIGITO='" + digitoControl + "', NUMCUENTA='" + cuenta + "' WHERE IDCARTERA = " + idCartera);


                        }

                    }


                }

            }
            catch
            {

            }
        }

        private void btnConfirmarMandato_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "PROCESANDO";
            this.Refresh();
            sql.actualizarCampo("UPDATE DOMBANCA SET MANDATOCONFIRMADO='T', MANDATOFECHACONFIRMADO='01-01-2020', MANDATOFECHAFIRMA='01-01-2020' " +
                " WHERE MANDATOCONFIRMADO='F'");
            toolStripStatusLabel4.Text = "FIN PROCESO";
        }

        private void btnCheckDombanca_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = "SINCRONIZANDO";
            this.Refresh();
            updateDataRPST.gestionarCuentasBancariasRepasatToA3ERP(true, true, null, false, false, true);
            toolStripStatusLabel4.Text = "FIN SINCRONIZACIÓN";
        }

        /// <summary>
        /// función para forzar la creación de una cuenta en A3ERP de un cliente de Repasat con 
        /// el código externo informado. Se creará el cliente o proveedor en A3ERP con el 
        /// codigo externo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                string cuenta = "";

                bool ventas = rbCompras.Checked ? false : true;


                foreach (DataGridViewRow dr in dgvRPST.SelectedRows)
                {
                    cuenta = dr.Cells["CLIENTE"].Value.ToString().Split("()".ToCharArray())[1];
                    updateData.traspasarCuentasRepasatToA3(ventas, false, null, false, false, false, false, true, cuenta);

                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }

        }



        /// <summary>
        /// proceso para verificar si los códigos de las cuentas son correctos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verificarCódigoDeCuentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string SeriePS = "";
                string SerieA3 = "";
                string NumDoc = "";
                string idDocRPST = "";
                string idFrav = "";
                string rpst_id_facv = "";
                string scriptGet = "";
                string scriptUpdateV = "";
                string scriptUpdateC = "";
                string codCuentaPS = "";
                string codCuentaA3 = "";
                string bloqueado = "";
                bool ventas = rbVentas.Checked ? true : false;


                DataTable factura = new DataTable();

                sql.abrirConexion();
                foreach (DataGridViewRow fila in dgvRPST.Rows)
                {
                    bloqueado = "";
                    SeriePS = fila.Cells["SERIE"].Value.ToString();
                    NumDoc = fila.Cells["NUM_FRA"].Value.ToString();
                    codCuentaPS = (fila.Cells["CLIENTE"].Value.ToString().Split('('))[1].Replace(")", "");
                    codCuentaA3 = sql.obtenerCampoTabla("SELECT LTRIM(" + (ventas ? "CODCLI) FROM CABEFACV " : "CODPRO) FROM CABEFACC ") + "  WHERE LTRIM(SERIE)='" + SeriePS + "' AND NUMDOC=" + NumDoc);
                    bloqueado = sql.obtenerCampoTabla("SELECT BLOQUEADO FROM " + (ventas ? "PROVEED" : "CLIENTES") + " WHERE LTRIM(" + (ventas ? "CODCLI) " : "CODPRO) ") + ")='" + codCuentaA3 + "'");
                    if (codCuentaPS != codCuentaA3)
                    {
                        fila.Cells["ACCIÓN"].Value = "REVISAR CUENTA: PS >>" + codCuentaPS + "    A3 >>" + codCuentaA3 + (bloqueado == "T" ? " BLOQUEADO" : "");
                    }

                }
                sql.cerrarConexion();
                "Proceso finalizado".mb();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }







        private void btnUploadSeries_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat las Series seleccionadas, ¿está seguro/a?", "Sinronizar Rutas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarSeriesA3ToRepasat();
                series();
                seriesA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }
        private void btnCrearSeriesA3_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en A3ERP las Series, ¿está seguro/a?", "Sinronizar Rutas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                try
                {
                    string filtroURL = "";
                    var repasat = new Repasat.csSeries.RootObject();

                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;
                        string tipoObjeto = "seriesdocs";
                        var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=1", "GET", tipoObjeto, filtroURL, "");
                        repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
                    }

                    if (repasat != null && repasat.data != null && repasat.data.Count > 0)
                    {
                        Objetos.csSeries[] series = new Objetos.csSeries[repasat.data.Count];

                        for (int i = 0; i < repasat.data.Count; i++)
                        {
                            series[i] = new Objetos.csSeries
                            {
                                idSerie = repasat.data[i].idSerie.ToString(),
                                nomSerie = repasat.data[i].nomSerie,
                                serie = repasat.data[i].refSerie
                            };
                        }

                        csa3erp a3erp = new csa3erp();
                        csCRUDSeries csSerie = new csCRUDSeries();

                        a3erp.abrirEnlace();
                        csSerie.crearSerieA3(series, tipoObjeto, campoExternoRepasat);
                        a3erp.cerrarEnlace();
                        
                        //Refresca los 2 dgv
                        seriesA3ERP();
                        dgvA3ERPData.Refresh();
                        dgvRepasatData.DataSource = loadRPSTObject.Series();
                        MessageBox.Show("Proceso finalizado.");
                    }
                    else
                    {
                        MessageBox.Show("No se encontraron series.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ocurrió un error: {ex.Message}");
                }
            }
        }

        //060924
        //private void btnCrearSeriesA3_Click(object sender, EventArgs e)

        //{
        //    DialogResult Resultado;

        //    Resultado = MessageBox.Show("Va a dar de alta en A3ERP las Series seleccionadas, ¿está seguro/a?", "Sinronizar Rutas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        //    if (Resultado == DialogResult.Yes)
        //    {
        //        try
        //        {
        //            // Obtener las series desde Repasat
        //            string filtroURL = "";
        //            var repasat = new Repasat.csSeries.RootObject();

        //            // Obtener datos de Repasat
        //            using (WebClient wc = new WebClient())
        //            {
        //                wc.Encoding = Encoding.UTF8;
        //                string tipoObjeto = "seriesdocs";
        //                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=1", "GET", tipoObjeto, filtroURL, "");
        //                repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
        //            }

        //            // Verificar que los datos no sean nulos
        //            if (repasat != null && repasat.data != null && repasat.data.Count > 0)
        //            {
        //                // Crear array de csSeries con los datos obtenidos
        //                Objetos.csSeries[] series = new Objetos.csSeries[repasat.data.Count];

        //                // Asignar los valores desde Repasat a csSeries
        //                for (int i = 0; i < repasat.data.Count; i++)
        //                {
        //                    series[i] = new Objetos.csSeries
        //                    {
        //                        idSerie = repasat.data[i].idSerie.ToString(),
        //                        nomSerie = repasat.data[i].nomSerie,
        //                        //codExternoSerie = repasat.data[i].codExternoSerie.ToString()
        //                    };
        //                }

        //                // Llamar a crearSerieA3 pasándole el array de series
        //                csa3erp a3erp = new csa3erp();
        //                a3erp.abrirEnlace();
        //                crearSerieA3(series);
        //                a3erp.cerrarEnlace();
        //                MessageBox.Show("Proceso finalizado.");
        //            }
        //            else
        //            {
        //                MessageBox.Show("No se encontraron series para descargar.");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show($"Ocurrió un error: {ex.Message}");
        //        }
        //    }
        //}
        //


        private void btnRegStock_Click(object sender, EventArgs e)
        {
            cargarMovStockRPST("2020-07-01", "2020-07-16");
        }


        /// <summary>
        /// Webservice actualizar movimientos de Stock
        /// </summary>
        /// <param name="filtroFechaIni"></param> formato aaaa-MM-dd
        /// <param name="filtroFechaFin"></param> formato aaaa-MM-dd
        /// <param name="ventas"></param>
        /// <param name="consulta"></param>
        /// <param name="sincronizado"></param>
        /// <param name="idSerieRPST"></param>
        /// <param name="numDocumento"></param>
        /// <returns></returns>
        public DataTable cargarMovStockRPST(string filtroFechaIni, string filtroFechaFin, bool movStockActividades = true, bool consulta = false, string sincronizado = "TODOS", string idSerieRPST = null, string numDocumento = null)
        {
            tipoDocumento = "movStock";
            int errorPagina = 0;
            int errorNumDocumento = 0;
            string errorSerieDoc = "";
            string errorIdDocRPST = "";

            int errorLineaDoc = 0;
            string errorArticulo = "";
            int documentosGenerados = 0;
            //bool docCompras = ventas ? false : true;

            // 29-3-2020 parche para incorporar clientes
            DataTable dtCuentas = new DataTable();
            dtCuentas = sql.obtenerDatosSQLScript("select NOMFISCAL, NOMCLI,NIFCLI, CODCLI,RPST_ID_CLI from clientes where (RPST_ID_CLI) IS NOT NULL");

            try
            {
                string filtroFechaURL = "&filter_gte[fecMovimientoStock]=" + filtroFechaIni + "&filter_lte[fecMovimientoStock]=" + filtroFechaFin;
                string filtroExtDocNull = "&filter[codExternoMovimientoStock]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
                string filtroMovStockActividades = "&filter_ne[idActividad]=";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroSerie = "";
                string codArt = "";
                string estadoSincro = "";
                DataTable accounts = new DataTable();

                int numCabeceras = 0;
                int numLineas = 0;

                //filtro para cargar todos los documentos o los pendientes
                if (sincronizado == "TODOS")
                {
                    filtroExtDocNull = "";
                }
                else if (sincronizado == "NO")
                {
                    filtroExtDocNull = "&filter[codExternoMovimientoStock]=";
                }
                else if (sincronizado == "SI")
                {
                    filtroExtDocNull = "&filter_ne[codExternoMovimientoStock]=";
                }


                string filtroNumSerieDoc = "";
                filtroNumSerieDoc = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? "" : "&filter[idSerie] =" + idSerieRPST + "&filter[numSerieDocumento]=" + numDocumento;
                filtroExtDocNull = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? filtroExtDocNull : "";

                string filtroURL = filtroFechaURL + filtroMovStockActividades + filtroExtDocNull + filtroSerie + filtroNumSerieDoc;



                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtInvoices = new DataTable();
                dtInvoices.Columns.Add("ACCIÓN");
                dtInvoices.Columns.Add("TIPO_DOC");
                dtInvoices.Columns.Add("SERIE");
                dtInvoices.Columns.Add("NUM_FRA");
                dtInvoices.Columns.Add("FECHA");
                dtInvoices.Columns.Add("CLIENTE");
                dtInvoices.Columns.Add("LINEAS");
                dtInvoices.Columns.Add("BASE_DOC");
                dtInvoices.Columns.Add("IMPORTE_DOC");
                dtInvoices.Columns.Add("EXTERNALID");
                dtInvoices.Columns.Add("REPASAT_ID_DOC");

                DataTable dtCuentasContables = new DataTable();
                dtCuentasContables.Columns.Add("CUENTA");
                dtCuentasContables.Columns.Add("IMPORTE");

                double totalDocument = 0;
                string tipoObjeto = "";


                //Continuo con la carga de facturas
                csSqlConnects sql = new csSqlConnects();
                Objetos.csMovimientosStock[] movimientos = null;
                csa3erp a3 = new csa3erp();
                int total_lineas = 0, contador = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    tipoObjeto = "stockmovements";

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                    if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                    {

                        var repasat = JsonConvert.DeserializeObject<Repasat.csStockMovements.RootObject>(json);
                        movimientos = new Objetos.csMovimientosStock[repasat.data.Count];

                        for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                        {
                            errorPagina = i;
                            #region kwebfkjwebf

                            /////////////////////////////////////////////////////
                            //////////////////   CABECERAS  ////////////////////
                            ////////////////////////////////////////////////////

                            //Comienzo iteración de las cabeceras
                            for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                            {
                                DataRow rowInvoices = dtInvoices.NewRow();

                                movimientos[numCabeceras] = new Objetos.csMovimientosStock();

                                //gestiono errores
                                errorIdDocRPST = repasat.data[ii].idMovimientoStock.ToString();
                                errorNumDocumento = repasat.data[ii].idArticulo == null ? 0 : Convert.ToInt32(repasat.data[ii].idArticulo.ToString());
                                //fin gestion errores

                                movimientos[numCabeceras].idMovimientoStock = repasat.data[ii].idMovimientoStock;
                                movimientos[numCabeceras].codart = repasat.data[ii].product.refArticulo;
                                movimientos[numCabeceras].almacen = repasat.data[ii].warehouse.codExternoAlmacen.ToString();
                                movimientos[numCabeceras].fechaMov = Convert.ToDateTime(repasat.data[ii].fecMovimientoStock);
                                movimientos[numCabeceras].motivoMov = "Movimiento Tarea";
                                movimientos[numCabeceras].tipoMov = repasat.data[ii].tipoMovimientoStock;
                                movimientos[numCabeceras].unidades = repasat.data[ii].unidades1MovimientoStock;
                                movimientos[numCabeceras].idActividad = repasat.data[ii].idActividad.ToString();


                                //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                                /*
                                estadoSincro = string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento.ToString()) ? " pendiente de traspaso " : " traspasado ";
                                rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].numSerieDocumento + estadoSincro;
                                rowInvoices["SERIE"] = repasat.data[ii].series.nomSerie;
                                rowInvoices["NUM_FRA"] = repasat.data[ii].numSerieDocumento;
                                rowInvoices["FECHA"] = (DateTime.Parse(repasat.data[ii].fecDocumento)).ToString("dd/MM/yyyy");
                                rowInvoices["CLIENTE"] = repasat.data[ii].account.nomCli.ToUpper() + " (" + cabeceras[numCabeceras].codIC + ")";
                                rowInvoices["LINEAS"] = repasat.data[ii].lines.Count;
                                rowInvoices["BASE_DOC"] = repasat.data[ii].totalBaseImponibleDocumento.ToString().Replace('.', ',');
                                rowInvoices["IMPORTE_DOC"] = repasat.data[ii].totalDocumento.ToString().Replace('.', ',');
                                rowInvoices["EXTERNALID"] = repasat.data[ii].codExternoDocumento.ToString();
                                rowInvoices["REPASAT_ID_DOC"] = repasat.data[ii].idDocumento;

                                totalDocument = totalDocument + Convert.ToDouble(repasat.data[ii].totalDocumento.ToString().Replace('.', ','));
                                */

                                dtInvoices.Rows.Add(rowInvoices);

                                numCabeceras++;
                            }

                            #endregion

                            //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                            // json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                            json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                            if (i + 1 <= repasat.last_page)
                            {
                                repasat = JsonConvert.DeserializeObject<Repasat.csStockMovements.RootObject>(json);

                                //Inicializo total_lineas para que cuente las nuevas lineas
                                total_lineas = 0;
                                //Redimensiono el Array de Cabeceras
                                Array.Resize(ref movimientos, movimientos.Length + repasat.data.Count);
                            }

                            //contador = 0;
                        }
                        if (!consulta)
                        {
                            DialogResult Resultado = DialogResult.Yes;
                            if (csGlobal.modoManual)
                            {
                                Resultado = MessageBox.Show("¿Quiere traspasar " + (movimientos.Length) + " movimientos a A3ERP?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            else { Resultado = DialogResult.Yes; }

                            if (Resultado == DialogResult.Yes)
                            {
                                csa3erpStock a3Stock = new csa3erpStock();
                                a3.abrirEnlace();

                                a3Stock.generaDoc(filtroFechaIni, filtroFechaFin, movimientos);

                                a3.cerrarEnlace();
                                if (csGlobal.modoManual)
                                    // MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos traspasados a A3ERP de " + cabeceras.Length.ToString());
                                    return null;
                            }
                        }


                    }
                    else
                    {
                        if (csGlobal.modoManual)
                        {
                            "No hay documentos para descargar entre los periodos seleccionados".mb();
                        }
                        return null;
                    }

                }
                if (consulta)
                {
                    return dtInvoices;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                    "Se ha encontrado un error en el siguiente número de documento de Repasat \n" +
                    "Serie: " + errorSerieDoc.ToString() + "\n" +
                    "Documento: " + errorNumDocumento.ToString() + "\n" +
                    "Id Documento Repasat: " + errorIdDocRPST.ToString() + "\n" +
                    "por favor solucionar para poder traspasar documento";

                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;
            }

        }


        public DataTable cargarInventarioRPST(bool consulta = false, string sincronizado = "TODOS", string idSerieRPST = null, string numDocumento = null)
        {
            tipoDocumento = "stock";
            int errorPagina = 0;
            int errorNumDocumento = 0;
            string errorSerieDoc = "";
            string errorIdDocRPST = "";

            int errorLineaDoc = 0;
            string errorArticulo = "";
            int documentosGenerados = 0;
            //bool docCompras = ventas ? false : true;

            // 29-3-2020 parche para incorporar clientes
            DataTable dtCuentas = new DataTable();
            dtCuentas = sql.obtenerDatosSQLScript("select NOMFISCAL, NOMCLI,NIFCLI, CODCLI,RPST_ID_CLI from clientes where (RPST_ID_CLI) IS NOT NULL");

            try
            {
                //string filtroFechaURL = "&filter_gte[fecMovimientoStock]=" + filtroFechaIni + "&filter_lte[fecMovimientoStock]=" + filtroFechaFin;
                string filtroExtDocNull = "&filter[codExternoMovimientoStock]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
                string filtroMovStockActividades = "&filter_ne[idActividad]=";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroAlmacen = "";
                string filtroSerie = "";
                string codArt = "";
                string estadoSincro = "";
                if (checkBAlmacen.Checked)
                {
                    filtroAlmacen = "&idAlmacen=" + cboxAlmacen.SelectedValue;

                }
                DataTable accounts = new DataTable();

                int numCabeceras = 0;
                int numLineas = 0;

                //filtro para cargar todos los documentos o los pendientes
                if (sincronizado == "TODOS")
                {
                    filtroExtDocNull = "";
                }
                else if (sincronizado == "NO")
                {
                    filtroExtDocNull = "&filter[codExternoMovimientoStock]=";
                }
                else if (sincronizado == "SI")
                {
                    filtroExtDocNull = "&filter_ne[codExternoMovimientoStock]=";
                }


                string filtroNumSerieDoc = "";
                filtroNumSerieDoc = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? "" : "&filter[idSerie] =" + idSerieRPST + "&filter[numSerieDocumento]=" + numDocumento;
                filtroExtDocNull = (string.IsNullOrEmpty(numDocumento) && string.IsNullOrEmpty(idSerieRPST)) ? filtroExtDocNull : "";

                string filtroURL = filtroMovStockActividades + filtroExtDocNull + filtroSerie + filtroNumSerieDoc + filtroAlmacen;



                //Creo un Datatable para registrar las facturas/documentos que se han traspasado
                DataTable dtInventory = new DataTable();
                dtInventory.Columns.Add("ACCIÓN");
                dtInventory.Columns.Add("IDARTICULO");
                dtInventory.Columns.Add("REFARTICULO");
                dtInventory.Columns.Add("NOMBRE");
                dtInventory.Columns.Add("STOCK_RPST");
                dtInventory.Columns.Add("STOCK_A3ERP");
                dtInventory.Columns.Add("DIFERENCIA");
                dtInventory.Columns.Add("ESTADO");
                dtInventory.Columns.Add("CONTROL_STOCK");

                double totalDocument = 0;
                string tipoObjeto = "";


                //Continuo con la carga de facturas
                csSqlConnects sql = new csSqlConnects();
                Objetos.csMovimientosStock[] movimientos = null;
                csa3erp a3 = new csa3erp();
                int total_lineas = 0, contador = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    tipoObjeto = "stock";

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                    if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                    {

                        var repasat = JsonConvert.DeserializeObject<Repasat.csInventory.RootObject>(json);
                        movimientos = new Objetos.csMovimientosStock[repasat.data.Count];

                        for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                        {
                            errorPagina = i;
                            #region kwebfkjwebf

                            for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                            {
                                DataRow rowInvoices = dtInventory.NewRow();

                                movimientos[numCabeceras] = new Objetos.csMovimientosStock();

                                //gestiono errores
                                //errorIdDocRPST = repasat.data[ii].idMovimientoStock.ToString();
                                //errorNumDocumento = repasat.data[ii].idArticulo == null ? 0 : Convert.ToInt32(repasat.data[ii].idArticulo.ToString());
                                //fin gestion errores

                                //movimientos[numCabeceras].idMovimientoStock = repasat.data[ii].idMovimientoStock;
                                //movimientos[numCabeceras].codart = repasat.data[ii].product.refArticulo;
                                //movimientos[numCabeceras].almacen = repasat.data[ii].warehouse.codExternoAlmacen.ToString();
                                //movimientos[numCabeceras].fechaMov = Convert.ToDateTime(repasat.data[ii].fecMovimientoStock);
                                //movimientos[numCabeceras].motivoMov = "Movimiento Tarea";
                                //movimientos[numCabeceras].tipoMov = repasat.data[ii].tipoMovimientoStock;
                                //movimientos[numCabeceras].unidades = repasat.data[ii].unidades1MovimientoStock;
                                //movimientos[numCabeceras].idActividad = repasat.data[ii].idActividad.ToString();

                                //Añado toda la información en el datatable para mostrar al final los documentos que traspaso

                                if (repasat.data[ii].warehouses.Count > 0 && checkBAlmacen.Checked)
                                {
                                    string almacenes = "";
                                    for (int iii = 0; iii < repasat.data[ii].warehouses.Count; iii++)
                                    {
                                        if (repasat.data[ii].warehouses[iii].idAlmacen.ToString() == cboxAlmacen.SelectedValue.ToString())
                                        {
                                            rowInvoices["STOCK_RPST"] = repasat.data[ii].warehouses[iii].pivot.unidades1ArticuloAlmacen.ToString().Replace('.', ',');
                                            almacenA3ERP = repasat.data[ii].warehouses[iii].codExternoAlmacen;
                                        }
                                    }

                                }

                                rowInvoices["ACCIÓN"] = "Doc nº " + repasat.data[ii].idArticulo;
                                rowInvoices["IDARTICULO"] = repasat.data[ii].idArticulo;
                                rowInvoices["REFARTICULO"] = repasat.data[ii].refArticulo; ;
                                rowInvoices["NOMBRE"] = repasat.data[ii].nomArticulo; ;
                                rowInvoices["ESTADO"] = repasat.data[ii].activoArticulo;
                                rowInvoices["CONTROL_STOCK"] = repasat.data[ii].stockArticulo == 1 ? "SI" : "NO";
                                //Si miro por almacén cojo el valor del almacén, si no cojo el valor total
                                rowInvoices["STOCK_RPST"] = checkBAlmacen.Checked ? rowInvoices["STOCK_RPST"] : repasat.data[ii].available_units.ToString().Replace('.', ',');
                                rowInvoices["STOCK_A3ERP"] = "0";
                                rowInvoices["DIFERENCIA"] = Convert.ToDouble(rowInvoices["STOCK_RPST"]) - Convert.ToDouble(rowInvoices["STOCK_A3ERP"]);

                                dtInventory.Rows.Add(rowInvoices);

                                numCabeceras++;
                            }

                            #endregion

                            //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                            // json = updateDataRPST.whileJson(wc, tipoObjeto + string.Format("?codExternoDocumento=null&fecDocumento[]={0}&fecDocumento[]={1}&api_token=", filtroFechaIni, filtroFechaFin) + csGlobal.webServiceKey, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");

                            json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, filtroURL, "");
                            if (i + 1 <= repasat.last_page)
                            {
                                repasat = JsonConvert.DeserializeObject<Repasat.csInventory.RootObject>(json);

                                //Inicializo total_lineas para que cuente las nuevas lineas
                                total_lineas = 0;
                                //Redimensiono el Array de Cabeceras
                                Array.Resize(ref movimientos, movimientos.Length + repasat.data.Count);
                            }

                            //contador = 0;
                        }
                        if (!consulta)
                        {
                            DialogResult Resultado = DialogResult.Yes;
                            if (csGlobal.modoManual)
                            {
                                Resultado = MessageBox.Show("¿Quiere traspasar " + (movimientos.Length) + " movimientos a A3ERP?", "Traspasar Documentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            else { Resultado = DialogResult.Yes; }

                            if (Resultado == DialogResult.Yes)
                            {
                                csa3erpStock a3Stock = new csa3erpStock();
                                //a3.abrirEnlace();

                                //a3Stock.generaDoc(movimientos);

                                //a3.cerrarEnlace();
                                if (csGlobal.modoManual)
                                    // MessageBox.Show("Proceso Finalizado:\n" + documentosGenerados + " documentos traspasados a A3ERP de " + cabeceras.Length.ToString());
                                    return null;
                            }
                        }


                    }
                    else
                    {
                        if (csGlobal.modoManual)
                        {
                            "No hay documentos para descargar entre los periodos seleccionados".mb();
                        }
                        return null;
                    }

                }
                if (consulta)
                {
                    DataTable dtStockA3 = new DataTable();
                    double stockRPST = 0;
                    double stockA3 = 0;
                    string idArticulo = "";
                    string codart = "";
                    int numeroFila = 0;

                    if (checkBAlmacen.Checked)
                    {
                        dtStockA3 = sql.obtenerDatosSQLScript("select LTRIM(CODART) AS CODART, SUM(UNIDADES) AS STOCK from stockalm WHERE LTRIM(CODALM) = '" + almacenA3ERP + "' GROUP BY CODART ");
                    }
                    else
                    {
                        dtStockA3 = sql.obtenerDatosSQLScript("select LTRIM(CODART) AS CODART, SUM(UNIDADES) AS STOCK from stockalm GROUP BY CODART ");
                    }
                    foreach (DataRow filaRPST in dtInventory.Rows)
                    {
                        idArticulo = filaRPST["REFARTICULO"].ToString();
                        stockRPST = Convert.ToDouble(filaRPST["STOCK_RPST"]);
                        foreach (DataRow filaA3 in dtStockA3.Rows)
                        {
                            codart = filaA3["CODART"].ToString();
                            stockA3 = Convert.ToDouble(filaA3["STOCK"]);
                            if (idArticulo == codart)
                            {
                                dtInventory.Rows[dtInventory.Rows.IndexOf(filaRPST)]["STOCK_A3ERP"] = stockA3.ToString();
                                dtInventory.Rows[dtInventory.Rows.IndexOf(filaRPST)]["DIFERENCIA"] = stockRPST - stockA3;
                            }
                        }

                    }


                    return dtInventory;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Pagina: " + errorPagina.ToString() + "\n" +
                    "Se ha encontrado un error en el siguiente número de documento de Repasat \n" +
                    "Serie: " + errorSerieDoc.ToString() + "\n" +
                    "Documento: " + errorNumDocumento.ToString() + "\n" +
                    "Id Documento Repasat: " + errorIdDocRPST.ToString() + "\n" +
                    "por favor solucionar para poder traspasar documento";

                if (csGlobal.modoManual)
                {
                    (errorLog + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return null;
            }

        }


        private Objetos.csMovimientosStock[] generarMovimientos(DataGridViewSelectedRowCollection dr)
        {
            Objetos.csMovimientosStock[] movimientos = new Objetos.csMovimientosStock[dr.Count];

            int numFilas = 0;

            foreach (DataGridViewRow fila in dr)
            {
                movimientos[numFilas] = new Objetos.csMovimientosStock();
                movimientos[numFilas].codart = fila.Cells["REFARTICULO"].Value.ToString();
                movimientos[numFilas].almacen = fila.Cells["REFARTICULO"].Value.ToString();
                movimientos[numFilas].fechaMov = DateTime.Now;
                movimientos[numFilas].motivoMov = "Regularización Stock Repasat";
                movimientos[numFilas].unidades = Convert.ToDouble(fila.Cells["DIFERENCIA"].Value.ToString());
                numFilas++;
            }

            return movimientos;
        }



        private void editarVencimientoEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataTable vencimientos = new DataTable();
            string serieDoc = dgvRPST.SelectedRows[0].Cells["SERIE"].Value.ToString();
            string numDoc = dgvRPST.SelectedRows[0].Cells["NUM_FRA"].Value.ToString();
            string numVto = dgvRPST.SelectedRows[0].Cells["NUM VTO"].Value.ToString();
            vencimientos = sql.obtenerDatosSQLScript("SELECT LTRIM(SERIE) AS SERIE, NUMDOC, IMPORTE, NUMVEN FROM CARTERA WHERE LTRIM(SERIE)='" + serieDoc + "' AND NUMDOC=" + numDoc);

            string detalleFras = "";
            for (int i = 0; i < vencimientos.Rows.Count; i++)
            {
                detalleFras += "SERIE: " + vencimientos.Rows[i]["SERIE"].ToString() +
                               " NUMDOC: " + vencimientos.Rows[i]["NUMDOC"].ToString() +
                               " VTO: " + vencimientos.Rows[i]["NUMVEN"].ToString() + ">> (" + vencimientos.Rows[i]["IMPORTE"].ToString() + ") \n";
            }
            string nuevoNumVto = Microsoft.VisualBasic.Interaction.InputBox("Introducir número de vencimiento: + \n" + detalleFras, "Nuevo Vencimiento", "");

            try
            {

                csUtilidades.ejecutarConsulta("UPDATE CARTERA SET NUMVEN=" + nuevoNumVto + " WHERE LTRIM(SERIE)='" + serieDoc + "' AND NUMDOC=" + numDoc, false);
                MessageBox.Show("Efecto Editado con éxito");
            }
            catch (Exception)
            {
            }
        }



        Utilidades.frLoading loading;

        /// <summary>
        /// Loading imagen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void showLoader()
        {
            loading = new Utilidades.frLoading();
            loading.Show();
        }

        public void hideLoader()
        {
            if (loading != null)
                loading.Close();
        }

        private void btnQueryStockRepasat_Click(object sender, EventArgs e)
        {
            //Cargar Stock
            dgvStock.DataSource = cargarInventarioRPST(true);
        }

        private void btnQueryStockA3ERP_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdateStockToA3ERP_Click(object sender, EventArgs e)
        {
            string fecIni = dtPickerFromStock.Value.ToString("yyyy-MM-dd");
            string fecFin = dtPickerToStock.Value.ToString("yyyy-MM-dd");
            cargarMovStockRPST(fecIni, fecFin);
        }

        private void checkBAlmacen_CheckedChanged(object sender, EventArgs e)
        {
            cboxAlmacen.Visible = checkBAlmacen.Checked ? true : false;
            if (checkBAlmacen.Checked) cargarAlmacenesRPST();
        }

        private void regularizarEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Objetos.csMovimientosStock[] movimientos = generarMovimientos(dgvStock.SelectedRows);

            DataRowView almacenSelected = cboxAlmacen.SelectedItem as DataRowView;

            if (almacenSelected != null)
            {
                almacenA3ERP = almacenSelected.Row[2] as string;
            }

            csa3erp a3 = new csa3erp();
            csa3erpStock a3Stock = new csa3erpStock();

            a3.abrirEnlace();

            a3Stock.generaRegularizaInventario(movimientos, almacenA3ERP);

            a3.cerrarEnlace();
        }

        private void dgvStock_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            formatearCeldaImporte(dgvStock, "STOCK_RPST", e.ColumnIndex, e);
            formatearCeldaImporte(dgvStock, "STOCK_A3ERP", e.ColumnIndex, e);
            formatearCeldaImporte(dgvStock, "DIFERENCIA", e.ColumnIndex, e);
        }

        private void btnContactos_Click(object sender, EventArgs e)
        {
            csa3erp a3erp = new csa3erp();
            try
            {

                a3erp.abrirEnlace();
                Maestro a3maestro = new Maestro();




                //a3maestro.Iniciar("CONTACTOS");
                a3maestro.Iniciar("__CONTACTOS");

                a3maestro.Nuevo();
                string nuevoCodigo = a3maestro.NuevoCodigoNum();
                //a3maestro.AsString["TABLA"] = "ORG";
                a3maestro.AsString["CODIGO"] = nuevoCodigo;
                //a3maestro.AsInteger["NUMCON"] = 111;
                a3maestro.AsString["NOMBRE"] = "JULIETA BENEGAS";
                a3maestro.AsString["EMAIL"] = "julietab@beta.com";
                a3maestro.AsString["TELEFONO1"] = "931234567";
                a3maestro.AsString["TELEFONO2"] = "666555888";
                a3maestro.AsString["RPST_ID_CONTACT"] = "222";

                a3maestro.Guarda(true);
                string codigo = a3maestro.get_AsString("CODIGO");  // PODEMOS UTILIZAR EL ID ES EL ID DEL CONTACTO
                int idContacto = Convert.ToInt32(sql.obtenerCampoTabla("SELECT ID FROM __CONTACTOS WHERE LTRIM(CODIGO)='" + codigo.Trim() + "'"));

                a3maestro.Acabar();
                a3maestro = null;


                IContactoRelacion contactRel = new ContactoRelacion();
                contactRel.Iniciar();
                contactRel.Nuevo(Convert.ToInt32(codigo.Trim()), ContactoRelacionEntidad.tcreCliente, "5");
                contactRel.Guardar();






                a3erp.cerrarEnlace();
            }
            catch (Exception ex)
            {
                string mess = ex.Message.ToString();
                a3erp.cerrarEnlace();

            }
        }

        private void btnCrearDirecciones_Click(object sender, EventArgs e)
        {
            updateDataRPST.gestionarDireccionesA3ToRepasat(null, true, false);
        }

        private void rbutStockActivity_CheckedChanged(object sender, EventArgs e)
        {
            grBoxSelectDatesStock.Visible = rbutStockActivity.Checked ? true : false;
            btnUpdateStockToA3ERP.Visible = rbutStockActivity.Checked ? true : false;
            grBoxAlmacenes.Visible = rbutStockActivity.Checked ? false : true;
            btnQueryStockRepasat.Visible = rbutStockActivity.Checked ? false : true;
            dgvStock.DataSource = null;
        }

        private void activarControlStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cboxMaestros.SelectedItem == "Productos" && dgvAccountsA3ERP.SelectedRows.Count > 0)
            {
                string codart = "";
                foreach (DataGridViewRow fila in dgvAccountsA3ERP.SelectedRows)
                {
                    codart = fila.Cells["CODIGO"].Value.ToString();
                    csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET AFESTOCK='T' WHERE LTRIM(CODART)='" + codart + "'", false);
                }

                MessageBox.Show("PROCESO FINALIZADO");
            }

        }

        //private void resetearCódigoExternoToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    string idRepasat = "";
        //    try
        //    {
        //        csRepasatWebService rpstWebService = new csRepasatWebService();
        //        if (dgvAccountsRepasat.SelectedRows.Count > 0)
        //        {
        //            DialogResult Resultado = MessageBox.Show("Vas a resetear los códigos Externos de " + dgvAccountsRepasat.SelectedRows.Count.ToString() +  " registros en Repasat. \n ¿Está seguro?", "Resetear Codigos Externos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //            if (Resultado == DialogResult.Yes)
        //            {
        //                statusLabel.Text = "PROCESANDO !!!";
        //                this.Refresh();
        //                foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
        //                {
        //                    idRepasat = fila.Cells["IDREPASAT"].Value.ToString();
        //                    rpstWebService.actualizarDocumentoRepasat("products", "codExternoArticulo", idRepasat, string.Empty);

        //                }
        //                statusLabel.Text = "PROCESO FINALIZADO !!!";
        //                this.Refresh();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        (ex.Message).mb();
        //    }

        //}
        /// <summary>
        /// Función para borrar o resetear el Id de Repasat en los registros seleccionados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetearIdRepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string codArt = "";
            try
            {

                if (dgvAccountsA3ERP.SelectedRows.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("Vas a resetear " + dgvAccountsA3ERP.SelectedRows.Count.ToString() + " Identificadores de Repasat  en A3ERP. \n ¿Está seguro?", "Resetear Codigos Externos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        statusLabel.Text = "PROCESANDO !!!";
                        this.Refresh();
                        foreach (DataGridViewRow fila in dgvAccountsA3ERP.SelectedRows)
                        {
                            codArt = fila.Cells["CODIGO"].Value.ToString().Trim();
                            sql.actualizarCampo("UPDATE ARTICULO SET RPST_ID_PROD = NULL WHERE LTRIM(CODART)='" + codArt + "'");
                        }
                        syncroData(false, true, false);
                        statusLabel.Text = "PROCESO FINALIZADO !!!";
                        this.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }

        private void validarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idRepasat = "";
            string refArticuloRPST = "";
            string codArt = "";
            DataTable dtA3Articulos = new DataTable();
            try
            {
                csRepasatWebService rpstWebService = new csRepasatWebService();
                if (dgvAccountsRepasat.SelectedRows.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("Vas a validar la sincronización de " + dgvAccountsRepasat.SelectedRows.Count.ToString() + " registros en Repasat. \n" +
                        "Este proceso verifica si la referencia de estos artículos están en A3ERP, \n" +
                        "Si existen se actualizará el código Externo en Repasat. \n" +
                        "En A3ERP se actualizará el identificador del artículo de Repasat. \n " +
                        "¿Está seguro?", "Resetear Codigos Externos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        dtA3Articulos = sql.obtenerDatosSQLScript("SELECT LTRIM(CODART) AS CODART FROM ARTICULO");
                        statusLabel.Text = "PROCESANDO !!!";
                        this.Refresh();
                        foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                        {
                            idRepasat = fila.Cells["IDREPASAT"].Value.ToString();
                            refArticuloRPST = fila.Cells["REFERENCIA"].Value.ToString();
                            foreach (DataRow articuloA3 in dtA3Articulos.Rows)
                            {
                                codArt = articuloA3["CODART"].ToString();
                                if (refArticuloRPST == codArt)
                                {
                                    rpstWebService.actualizarDocumentoRepasat("products", "codExternoArticulo", idRepasat, codArt);
                                    sql.actualizarCampo("UPDATE ARTICULO SET RPST_ID_PROD = " + idRepasat + " WHERE LTRIM(CODART)='" + codArt + "'");
                                    break;
                                }
                            }

                        }
                        statusLabel.Text = "PROCESO FINALIZADO !!!";
                        this.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }

        private void resetearCódigoExternoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idRepasat = "";
            try
            {
                csRepasatWebService rpstWebService = new csRepasatWebService();
                if (dgvAccountsRepasat.SelectedRows.Count > 0)
                {
                    DialogResult Resultado = MessageBox.Show("Vas a resetear los códigos Externos de " + dgvAccountsRepasat.SelectedRows.Count.ToString() + " registros en Repasat. \n ¿Está seguro?", "Resetear Codigos Externos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        statusLabel.Text = "PROCESANDO !!!";
                        this.Refresh();
                        foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                        {
                            idRepasat = fila.Cells["IDREPASAT"].Value.ToString();
                            rpstWebService.actualizarDocumentoRepasat("products", "codExternoArticulo", idRepasat, string.Empty);
                            this.Refresh();

                        }
                        statusLabel.Text = "PROCESO FINALIZADO !!!";
                        this.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }



        private void enlaceDesdeRepasatA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                inicioProceso();

                string cuentaA3_RPST = "";
                string idCuentaRPST = "";
                int filasUpdated = 0;
                string scriptUpdate = "";
                if (dgvAccountsRepasat.SelectedRows.Count == 0)
                {
                    ("NO HAY FILAS SELECCIONADAS").mb();
                    return;
                }
                else
                {
                    DialogResult Resultado = MessageBox.Show("Se van a actualizar " + dgvAccountsRepasat.SelectedRows.Count + "  códigos de Repasat en A3ERP. \n ¿Está seguro?", "Actualizar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        DataTable dtCuenta = new DataTable();
                        datacommand.Connection = dataconnection;
                        dataconnection.ConnectionString = csGlobal.cadenaConexion;
                        dataconnection.Open();


                        foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                        {
                            cuentaA3_RPST = cboxMaestros.SelectedItem.ToString().Contains("Direcciones") ? fila.Cells["EXTERNALID"].Value.ToString() : fila.Cells["CODIGOERP"].Value.ToString();
                            idCuentaRPST = fila.Cells["IDREPASAT"].Value.ToString();
                            if (!string.IsNullOrEmpty(cuentaA3_RPST))
                            {
                                switch (cboxMaestros.SelectedItem.ToString())
                                {
                                    case "Clientes":
                                        scriptUpdate = "UPDATE __CLIENTES SET RPST_ID_CLI=" + idCuentaRPST + " WHERE LTRIM(CODCLI)='" + cuentaA3_RPST + "'";
                                        break;
                                    case "Proveedores":
                                        scriptUpdate = "UPDATE __PROVEED SET RPST_ID_PROV=" + idCuentaRPST + " WHERE LTRIM(CODPRO)='" + cuentaA3_RPST + "'";
                                        break;
                                    case "Direcciones Clientes":
                                        scriptUpdate = "UPDATE DIRENT SET RPST_ID_DIR=" + idCuentaRPST + " WHERE IDDIRENT=" + cuentaA3_RPST;
                                        break;
                                    case "Direcciones Proveedores":
                                        scriptUpdate = "UPDATE __DIRENTPRO SET RPST_ID_DIRPRO=" + idCuentaRPST + " WHERE IDDIRENT=" + cuentaA3_RPST;
                                        break;
                                }

                                datacommand.CommandText = scriptUpdate;
                                datacommand.ExecuteNonQuery();

                                filasUpdated++;
                            }
                        }
                        dataconnection.Close();
                        ("Se han actualizado " + filasUpdated + " filas").mb();
                        inicioProceso(false);
                    }
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
                dataconnection.Close();

            }
        }


        private void inicioProceso(bool inicio = true)
        {
            try
            {
                if (inicio)
                {
                    statusLabel.Text = "P R O C E S A N D O";
                    statusLabel.BackColor = Color.Orange;
                    this.Refresh();
                }
                else
                {
                    statusLabel.Text = "PROCESO FINALIZADO";
                    statusLabel.BackColor = SystemColors.Window;
                    this.Refresh();
                }
            }
            catch { }

        }

        private void btnAsientosUtils_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                //
            }
            else
            {
                //   Repasat.Forms.frAsientosA3ERP.DefInstance.MdiParent = this;
                Repasat.Forms.frAsientosA3ERP.DefInstance.Show();
                Repasat.Forms.frAsientosA3ERP.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                //
            }
            else
            {
                //   Repasat.Forms.frAsientosA3ERP.DefInstance.MdiParent = this;
                Repasat.Forms.frTraspasoA3ToRPST.DefInstance.Show();
                Repasat.Forms.frTraspasoA3ToRPST.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void btnApex_Click(object sender, EventArgs e)
        {
            Puntes.frMainFormPuntes.DefInstance.Show();

            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                //
            }
            else
            {
                //   Repasat.Forms.frAsientosA3ERP.DefInstance.MdiParent = this;
                Puntes.frMainFormPuntes.DefInstance.Show();
                Puntes.frMainFormPuntes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }



        private void cambiarEstadoSyncronizacion(bool estado, string tipoObjeto)
        {
            inicioProceso();
            csRepasatWebService rpstWS = new csRepasatWebService();
            int codigoExterno = estado ? 1 : 0;
            string idRegistroRepasat = "";
            string campoClaveDGV = "";
            string campoExternoRepasat = "";
            DataGridView dgv = new DataGridView();

            switch (tipoObjeto)
            {
                case "purchaseinvoices":
                case "saleinvoices":
                case "purchaseorders":
                case "saleorders":
                    campoClaveDGV = "REPASAT_ID_DOC";
                    campoExternoRepasat = "sincronizarDocumento";
                    dgv = dgvRPST;
                    break;

                case "accounts":
                    campoClaveDGV = "IDREPASAT";
                    campoExternoRepasat = "sincronizarCuenta";
                    dgv = dgvAccountsRepasat;
                    break;

                case "incomingpayments":
                case "outgoingpayments":
                    campoClaveDGV = "REPASAT_ID_DOC";
                    campoExternoRepasat = "sincronizarCartera";
                    dgv = dgvRPST;
                    break;
                case "remittances":
                    campoClaveDGV = "REPASAT_ID_DOC";
                    campoExternoRepasat = "sincronizarRemesa";
                    dgv = dgvRPST;
                    break;

                default:

                    break;
            }

            foreach (DataGridViewRow fila in dgv.SelectedRows) {
                idRegistroRepasat = fila.Cells[campoClaveDGV].Value.ToString();
                rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, codigoExterno.ToString());
            }
            switch (tipoObjeto)
            {
                case "saleinvoices":
                    gestionarDocumentos(true, false, true);
                    mostrarLogoDatosOrigen(false, true);
                    break;

                case "accounts":
                    statusLabel.Text = "P R O C E S A N D O";
                    statusLabel.BackColor = Color.Orange;
                    this.Refresh();
                    syncroData(false, true, true);
                    statusLabel.Text = "PROCESO FINALIZADO";
                    statusLabel.BackColor = SystemColors.Window;
                    break;

                default:

                    break;
            }



            inicioProceso(false);
        }


        private void noToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambiarEstadoSyncronizacion(false, obtenerTipoObjetoRPST());
        }

        private void síToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambiarEstadoSyncronizacion(true, obtenerTipoObjetoRPST());
        }

        private void dgvAccountsRepasat_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgvAccountsRepasat.Columns.Contains("CODIGOERP") == true)
            {
                dgvAccountsRepasat.Columns["CODIGOERP"].DefaultCellStyle.Format = "N0";
                dgvAccountsRepasat.Columns["CODIGOERP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            //Asignación Imagenes link
            if (dgvAccountsRepasat.Rows.Count > 0)
            {
                if (this.dgvAccountsRepasat.Columns[e.ColumnIndex].Name == "RPST")
                {
                    if (dgvAccountsRepasat.Columns.Contains("IDREPASAT"))
                    {
                        if (!string.IsNullOrEmpty(this.dgvAccountsRepasat["IDREPASAT", e.RowIndex].Value.ToString()))
                        {
                            e.Value = Properties.Resources.Repasat16;
                        }
                        else
                        {
                            e.Value = Properties.Resources.transp16; ;
                        }
                    }
                }
            }
        }

        private void siToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cambiarEstadoSyncronizacion(true, "accounts");
        }

        private void noToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cambiarEstadoSyncronizacion(false, "accounts");
        }

        private void comercialesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sincronizarValorEnRepasat("accounts", "idTrabajador");
        }

        private void sincronizarValorEnRepasat(string tipoObjeto, string campo)
        {
            inicioProceso();
            csRepasatWebService rpstWS = new csRepasatWebService();
            string idRegistroRepasat = "";
            string campoClaveDGV = "";
            string campoExternoRepasat = "";
            DataGridView dgv = new DataGridView();
            string codigoExterno = "";
            string valueToUpdate = "";
            string fieldToUpdate = "";

            switch (campo)
            {
                case "idTrabajador":
                    campoClaveDGV = "RPST_ID_CLI";
                    campoExternoRepasat = "idTrabajador";
                    fieldToUpdate = "CIAL";
                    dgv = dgvAccountsA3ERP;
                    break;

                default:

                    break;
            }

            foreach (DataGridViewRow fila in dgv.SelectedRows)
            {
                valueToUpdate = fila.Cells[fieldToUpdate].Value.ToString().Split('|')[0];
                idRegistroRepasat = fila.Cells[campoClaveDGV].Value.ToString();
                rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, valueToUpdate.ToString());
            }
            //switch (tipoObjeto)
            //{
            //    case "saleinvoices":
            //        gestionarDocumentos(true, false, true);
            //        mostrarLogoDatosOrigen(false, true);
            //        break;

            //    case "accounts":
            //        statusLabel.Text = "P R O C E S A N D O";
            //        statusLabel.BackColor = Color.Orange;
            //        this.Refresh();
            //        syncroData(false, true, true);
            //        statusLabel.Text = "PROCESO FINALIZADO";
            //        statusLabel.BackColor = SystemColors.Window;
            //        break;

            //    default:

            //        break;
            //}



            inicioProceso(false);
        }

        private void comercialesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sincronizarValorEnA3ERP("CLIENTES", "COMERCIALES");
        }

        private void sincronizarValorEnA3ERP(string tipoObjeto, string campo) {

            inicioProceso();
            csRepasatWebService rpstWS = new csRepasatWebService();
            //int codigoExterno = estado ? 1 : 0;
            string idRegistroRepasat = "";
            string campoClaveDGV = "";
            string campoExternoRepasat = "";
            string valorToUpdate = "";
            string fieldToUpdate = "";
            DataGridView dgv = new DataGridView();
            //sql.abrirConexion();

            switch (tipoObjeto)
            {
                case "saleinvoices":
                    campoClaveDGV = "REPASAT_ID_DOC";
                    campoExternoRepasat = "sincronizarDocumento";
                    dgv = dgvRPST;
                    break;

                case "CLIENTES":
                    campoClaveDGV = "CODIGOERP";
                    campoExternoRepasat = "sincronizarCuenta";
                    fieldToUpdate = "CIAL";
                    dgv = dgvAccountsRepasat;
                    break;

                default:

                    break;
            }

            foreach (DataGridViewRow fila in dgv.SelectedRows)
            {
                idRegistroRepasat = fila.Cells[campoClaveDGV].Value.ToString();
                valorToUpdate = fila.Cells[fieldToUpdate].Value.ToString().Split('|')[0].Trim().PadLeft(8, ' ');
                string idOrganizacion = sql.obtenerCampoTabla("SELECT IDORG FROM CLIENTES WHERE LTRIM(CODCLI)='" + idRegistroRepasat + "'");
                sql.actualizarCampo("UPDATE __ORGANIZACION SET CODREP='" + valorToUpdate + "' WHERE IDORG=" + idOrganizacion);
                rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, idRegistroRepasat, "");
            }
            //switch (tipoObjeto)
            //{
            //    case "saleinvoices":
            //        gestionarDocumentos(true, false, true);
            //        mostrarLogoDatosOrigen(false, true);
            //        break;

            //    case "accounts":
            //        statusLabel.Text = "P R O C E S A N D O";
            //        statusLabel.BackColor = Color.Orange;
            //        this.Refresh();
            //        syncroData(false, true, true);
            //        statusLabel.Text = "PROCESO FINALIZADO";
            //        statusLabel.BackColor = SystemColors.Window;
            //        break;

            //    default:

            //        break;
            //}



            inicioProceso(false);

        }

        private void dgvAccountsA3ERP_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string tipoFichero = cboxMaestros.SelectedItem.ToString();
            try
            {
                switch (tipoFichero)
                {
                    case ("Productos"):
                        this.dgvAccountsA3ERP.Columns["idArticulo"].Visible = false;
                        this.dgvAccountsA3ERP.Columns["aliasArticulo"].Visible = false;
                        this.dgvAccountsA3ERP.Columns["codExternoArticulo"].Visible = false;
                        this.dgvAccountsA3ERP.Columns["refArticulo"].Visible = false;
                        break;
                    case ("Clientes"):
                        break;
                }
            }
            catch { }

        }

        private void dgvAccountsA3ERP_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //Asignación Imagenes link
            if (dgvAccountsA3ERP.Rows.Count > 0)
            {
                string tipoFichero = cboxMaestros.SelectedItem.ToString();
                string fieldKeyRPST = "";
                switch (tipoFichero)
                {
                    case ("Productos"):
                        fieldKeyRPST = "REPASAT_ID";
                        break;
                    case ("Clientes"):
                        fieldKeyRPST = "RPST_ID_CLI";
                        break;
                }
                if (this.dgvAccountsA3ERP.Columns[e.ColumnIndex].Name == "RPST")
                {
                    if (dgvAccountsA3ERP.Columns.Contains(fieldKeyRPST))
                    {
                        if (!string.IsNullOrEmpty(this.dgvAccountsA3ERP[fieldKeyRPST, e.RowIndex].Value.ToString()))
                        {
                            e.Value = Properties.Resources.Repasat16;
                        }
                        else
                        {
                            e.Value = Properties.Resources.transp16; ;
                        }
                    }
                }


            }
        }

        private void dgvAccountsA3ERP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            if (e.RowIndex > 0 && e.ColumnIndex > 0)
            {
                string filter = dgvAccountsA3ERP.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().PadLeft(8);
                string codigoA3 = dgvAccountsRepasat.Columns[e.ColumnIndex].Name;

                if (codigoA3 == "CODIGOERP" && filter != "99999")
                {
                    if (cboxMaestros.SelectedItem.ToString() != "Productos")
                    {
                        BindingSource bs = new BindingSource();
                        bs.DataSource = dgvAccountsA3ERP.DataSource;
                        bs.Filter = "CODCLI = '" + filter.PadLeft(8) + "'";
                        dgvAccountsA3ERP.DataSource = bs.DataSource;
                    }
                }
                if (codigoA3 == "NIF")
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = dgvAccountsA3ERP.DataSource;
                    bs.Filter = "NIFCLI = '" + filter + "'";
                    dgvAccountsA3ERP.DataSource = bs.DataSource;
                }
            }
            }catch (Exception ex) {}

        }


        private void verificarSincronizacionEnRepasat() {
            try
            {
                DataTable dtArticulosEnRepasat = new DataTable();
                dtArticulosEnRepasat = (DataTable)(dgvAccountsRepasat.DataSource);
                DataTable dtArticulosEnA3ERP = new DataTable();
                dtArticulosEnA3ERP = (DataTable)(dgvAccountsA3ERP.DataSource);
                string identificadorRPST = "";      // id clave primaria en Repasat
                string identificadorERP = "";       // id clave en A3ERP (codcli, codart, etc.)
                string refExtRPST = "";             // ref externa en Repasat
                string refExtERP = "";              // ref externa en ERP
                bool existeRegistro = false;
                int registrosProcesados = 0;
                string DGVA3_columnaClaveA3ERP = "";
                string DGVA3_columnaClaveRPST = "";
                string DGVRPST_columnaClaveA3ERP = "";
                string DGVRPST_columnaClaveRPST = "";
                string colToUpdate = "";
                string tablaToUpdate = "";
                string fieldToUpdate = "";


                tssLabelTotalRows.Text = dgvAccountsA3ERP.Rows.Count.ToString();


                switch (cboxMaestros.SelectedItem.ToString())
                {
                    case "Clientes":
                        DGVA3_columnaClaveA3ERP = "CODCLI";
                        DGVA3_columnaClaveRPST = "RPST_ID_CLI";
                        DGVRPST_columnaClaveA3ERP = "CODIGOERP";
                        DGVRPST_columnaClaveRPST = "IDREPASAT";
                        colToUpdate = "NIFCLI";
                        tablaToUpdate = "__CLIENTES";
                        fieldToUpdate = "RPST_ID_CLI";
                        break;
                    case "Proveedores":
                        DGVA3_columnaClaveA3ERP = "CODPRO";
                        DGVA3_columnaClaveRPST = "RPST_ID_PROV";
                        break;
                    case "Productos":
                        DGVA3_columnaClaveA3ERP = "CODART";
                        DGVA3_columnaClaveRPST = "RPST_ID";
                        DGVRPST_columnaClaveA3ERP = "CODIGOERP";
                        DGVRPST_columnaClaveRPST = "IDREPASAT";
                        colToUpdate = "nomArticulo";
                        tablaToUpdate = "ARTICULO";
                        fieldToUpdate = "RPST_ID_PROD";
                        break;


                }


                foreach (DataGridViewRow filaERP in dgvAccountsA3ERP.SelectedRows)
                {
                    registrosProcesados++;
                    tssLabelTotalRows.Text = registrosProcesados.ToString() + " / " + dgvAccountsA3ERP.Rows.Count.ToString();

                    existeRegistro = false;

                    identificadorERP = filaERP.Cells[DGVA3_columnaClaveA3ERP].Value.ToString().Trim();
                    refExtERP = filaERP.Cells[DGVA3_columnaClaveRPST].Value.ToString();

                    foreach (DataGridViewRow filaRPST in dgvAccountsRepasat.Rows)
                    {
                        refExtRPST = filaRPST.Cells[DGVRPST_columnaClaveA3ERP].Value.ToString();
                        identificadorRPST = filaRPST.Cells[DGVRPST_columnaClaveRPST].Value.ToString();

                        if (identificadorERP == refExtRPST && identificadorRPST == refExtERP)
                        {
                            filaERP.Cells[colToUpdate].Value = "EXISTE: " + filaERP.Cells[DGVA3_columnaClaveRPST].Value.ToString();
                            dgvAccountsA3ERP.Rows[filaERP.Index].Cells[DGVA3_columnaClaveRPST].Style.BackColor = Color.Aquamarine;
                            existeRegistro = true;
                            break;
                        }

                    }
                    if (!existeRegistro)
                    {
                        filaERP.Cells[colToUpdate].Value = "DELETE: " + filaERP.Cells[DGVA3_columnaClaveRPST].Value.ToString();
                        dgvAccountsA3ERP.Rows[filaERP.Index].Cells[DGVA3_columnaClaveRPST].Style.BackColor = Color.LightPink;
                        sql.actualizarCampo("UPDATE " + tablaToUpdate + " SET " + fieldToUpdate + "= NULL WHERE LTRIM(" + DGVA3_columnaClaveA3ERP + ")='" + identificadorERP + "'");
                        dgvAccountsA3ERP.Rows[filaERP.Index].Cells[DGVA3_columnaClaveRPST].Value = 0;
                    }
                    this.Refresh();
                }



            }
            catch (Exception ex) {



            }



        }

        private void verificarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inicioProceso();
            verificarSincronizacionEnRepasat();
            inicioProceso(false);
        }

        private void borrarRemesaEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int remesa = Convert.ToInt16(dgvRPST.SelectedRows[0].Cells["REPASAT_ID_DOC"].Value.ToString());
            borrarRemesaA3ERP(remesa);
        }

        private void borrarRemesaA3ERP(int idRemesa) {
            csa3erp a3erp = new csa3erp();
            csa3erpCartera a3cartera = new csa3erpCartera();
            a3erp.abrirEnlace();
            a3cartera.borrarRemesa(idRemesa, true);
            a3erp.cerrarEnlace();
        }

        private void actualizarDocsEnRPSTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarDocsTicketBaiEnRepasat();
        }

        private void actualizarDocsTicketBaiEnRepasat()
        {
            try
            {
                inicioProceso();
                string[] serieDoc = new string[2];
                string numSerie = "";
                string numDocA3ERP = "";
                string idNumDocRPST = "";
                string idFacA3ERP = "";
                string idDocERP = "";
                string urlTicketBai = "";
                string messageUpdate = "";
                string messageConfirm = "¿Quieres omitir las confirmaciones?";
                string numVencimiento = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                csRepasatWebService rpstWS = new csRepasatWebService();
                string nomCampoExternoRPST = "";
                int contPreguntas = 0;
                DialogResult resultConfirmacion = new DialogResult();
                DialogResult result = new DialogResult();

                DataTable dtTBai = new DataTable();

                string tipoObjetoRPST = obtenerTipoObjetoRPST();

                foreach (DataGridViewRow dgvRow in dgvRPST.SelectedRows)
                {

                    if (dgvRPST.Columns.Contains("serie"))
                    {
                        serieDoc = dgvRow.Cells["serie"].Value.ToString().Split('(');
                        numSerie = serieDoc[0].Trim();
                    }
                    if (dgvRPST.Columns.Contains("NUM_FRA"))
                    {
                        numDocA3ERP = dgvRow.Cells["NUM_FRA"].Value.ToString();
                        idFacA3ERP = dgvRow.Cells["EXTERNALID"].Value.ToString();
                    }
                    idNumDocRPST = dgvRow.Cells["REPASAT_ID_DOC"].Value.ToString();
                    if (dgvRPST.Columns.Contains("NUM VTO"))
                    {
                        numVencimiento = tipoDocumento.ToUpper() == "CARTERA" ? dgvRow.Cells["NUM VTO"].Value.ToString() : numVencimiento;
                    }

                    dtTBai = sql.obtenerDatosSQLScript("SELECT cast(IDFACV as int) as codExternoDocumento, " +
                                 " CABEFACV.SERIE, SERIES.RPST_ID_SERIE, cast(NUMDOC as int) as NUMDOC, RPST_ID_FACV, BORRADOR, SERIEORIGINALFACTURABORRADOR, RAZON, TBAI_FACTURA.ID, DATOSQR AS 'ticketbai[url]',IDRESPUESTA, REGISTRADO, CABEFACV.FECHAHORAENVIO " +
                                 " FROM CABEFACV " +
                                 " INNER JOIN TBAI_FACTURA ON CABEFACV.SERIE = TBAI_FACTURA.FACTURASERIE AND CABEFACV.NUMDOC = TBAI_FACTURA.FACTURANUMDOC " +
                                 " INNER JOIN TBAI_FACTURAV ON TBAI_FACTURAV.ID = TBAI_FACTURA.ID " +
                                 "  INNER JOIN SERIES ON CABEFACV.SERIE=SERIES.SERIE " +
                                 " WHERE RPST_ID_FACV = " + idNumDocRPST + " OR IDFACV=" + idFacA3ERP);

                    if (dtTBai.Rows.Count > 0)
                    {
                        messageUpdate = "¿Quieres enlazar el documento: " + numSerie + "/" + numDocA3ERP + " ?";

                        contPreguntas++;
                        if (contPreguntas == 2)
                        {
                            resultConfirmacion = MessageBox.Show(messageConfirm, "Confirmación", MessageBoxButtons.YesNoCancel);
                            if (resultConfirmacion == DialogResult.Yes)
                            {
                                resultConfirmacion = DialogResult.Yes;
                            }
                        }

                        if (resultConfirmacion != DialogResult.Yes)
                        {
                            result = MessageBox.Show(messageUpdate, "Actualización", MessageBoxButtons.YesNoCancel);
                        }

                        if (result == DialogResult.Yes || resultConfirmacion == DialogResult.Yes)
                        {

                            nomCampoExternoRPST = "ticketbai[url]";
                            urlTicketBai = dtTBai.Rows[0]["ticketbai[url]"].ToString();
                            idDocERP = dtTBai.Rows[0]["ticketbai[url]"].ToString();
                            rpstWS.actualizarDocumentoRepasat(tipoObjeto, nomCampoExternoRPST, idNumDocRPST, urlTicketBai);
                            nomCampoExternoRPST = "numSerieDocumento";
                            idDocERP = dtTBai.Rows[0]["NUMDOC"].ToString();
                            rpstWS.actualizarDocumentoRepasat(tipoObjeto, nomCampoExternoRPST, idNumDocRPST, idDocERP);
                            nomCampoExternoRPST = "idSerie";
                            idDocERP = dtTBai.Rows[0]["RPST_ID_SERIE"].ToString();
                            rpstWS.actualizarDocumentoRepasat(tipoObjeto, nomCampoExternoRPST, idNumDocRPST, idDocERP);
                        }
                    }


                    inicioProceso(false);

                }
            }
            catch (Exception ex) { }


        }

        private void crearCuentaEnRepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inicioProceso(false);
            Repasat.csUpdateData rpstUpdateData = new Repasat.csUpdateData();
            bool ventas = rbVentas.Checked ? true : false;
            string cuenta = "";

            foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
            {
                cuenta = fila.Cells["CODCLI"].Value.ToString().Trim(); ;
                rpstUpdateData.cargarCuentasA3ToRepasat(ventas, false, cuenta);
            }
            inicioProceso(false);
        }

        private void porCódigoExternoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string codCuentaRPST = "";
            string nifRPST = "";
            string codCuentaERP = "";
            string nifERP = "";
            int contador = 0;

            DataTable dtCuentasRPST = new DataTable();
            DataTable dtCuentasERP = new DataTable();

            dtCuentasRPST = (DataTable)dgvAccountsRepasat.DataSource;
            DataView dtv = new DataView(dtCuentasRPST);
            // dtv.RowFilter = " CODIGOERP <> ''";
            dtv.Sort = " CODIGOERP ASC";

            dtCuentasRPST = dtv.ToTable();

            dtCuentasERP = (DataTable)dgvAccountsA3ERP.DataSource;

            bool cuentaToUpdate = true;

            foreach (DataRow filaRPST in dtCuentasRPST.Rows)
            {
                cuentaToUpdate = true;

                codCuentaRPST = filaRPST["CODIGOERP"].ToString();
                nifRPST = filaRPST["NIF"].ToString();
                if (string.IsNullOrEmpty(codCuentaRPST))
                {
                    continue;
                }

                foreach (DataRow filaERP in dtCuentasERP.Rows)
                {
                    codCuentaERP = filaERP["CODCLI"].ToString().Trim();
                    nifERP = filaERP["NIFCLI"].ToString();

                    if (codCuentaERP == codCuentaRPST)
                    {
                        if (nifERP == nifRPST)
                        {
                            //No hago nada porque está correcto
                            cuentaToUpdate = false;
                            break;
                        }
                        else
                        {
                            //Actualizo ERP o REPASAT
                            ("CLIENTE con Código Externo: " + codCuentaERP + "\n" +
                                "--------------------------------------" + "\n" +
                                "NOMBRE en RPST: " + filaRPST["NOMBRE"].ToString() + "\n" +
                                "RAZON en RPST:  " + filaRPST["RAZON"].ToString() + "\n" +
                                "NOMBRE en ERP:  " + filaERP["NOMCLI"].ToString() + "\n" +
                                "--------------------------------------" + "\n" +
                                "NIF en RPST: " + nifRPST + "\n" +
                                "NIF en ERP: " + nifERP + "\n" + "\n" +
                                "--------------------------------------" + "\n" +
                                "¿Actualizar Razón y NIF en REPASAT?").mb();




                        }
                    }
                }
                if (cuentaToUpdate)
                {
                    filaRPST["NOMBRE"] = "PTE TRASPASO A A3ERP";
                }

            }
            dgvAccountsRepasat.DataSource = dtCuentasRPST;
            this.Refresh();
        }

        private void direccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataSet datasetAccounts = updateDataRPST.traspasarCuentasRepasatToA3(true, true, null, true, false, false);

            string idDireccionRPST = "";
            string codCli = "";
            string idDirentA3 = "";
            DataTable dtDirecciones = new DataTable();
            if (datasetAccounts.Tables.Count > 0)
            {
                dtDirecciones = datasetAccounts.Tables[1];

                foreach (DataRow direccion in dtDirecciones.Rows)
                {
                    codCli = direccion["IDCUENTAA3"].ToString();
                    if (!string.IsNullOrEmpty(codCli))
                    {
                        idDireccionRPST = direccion["IDREPASAT"].ToString();

                        csUtilidades.ejecutarConsulta("UPDATE DIRENT SET RPST_ID_DIR = " + idDireccionRPST + " WHERE LTRIM(CODCLI) = '" + codCli + "' AND DEFECTO = 'T'", false);
                    }
                }
            }

            "Direcciones Actualizadas".mb();

        }

        private void cboxGenerarMovsContables_CheckedChanged(object sender, EventArgs e)
        {
            if (!cboxGenerarMovsContables.Checked)
            {
                DialogResult Resultado = MessageBox.Show("Al desmarcar esta opción, no se van a generar los movimientos contables al traspasar los documentos a A3ERP. \n ¿Está seguro?", "Generación Movimientos Contables", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.No)
                {
                    cboxGenerarMovsContables.Checked = true;
                }
            }
        }

        private void actualizarArticulosEnRPSTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearArticulosDeA3ERPToRepasat(true);
        }

        private void btnLoadValidationData_Click(object sender, EventArgs e)
        {
            statusLabel.Text = "P R O C E S A N D O";
            statusLabel.BackColor = Color.Orange;
            this.Refresh();
            syncroData(false, true, true, false, dgvValidatingData, true);
            tssLabelTotalRowsRPST.Text = dgvAccountsRepasat.Rows.Count.ToString() + " Filas";
            statusLabel.Text = "PROCESO FINALIZADO";
            statusLabel.BackColor = SystemColors.Window;
        }

        private void btnUpdateStockToRepasat_Click(object sender, EventArgs e)
        {

        }


        private void verificarCodigosArticulosEnRepasat() {

            try
            {
                string codigoRPSTenA3ERP = "";
                string codigoRPST = "";
                foreach (DataGridViewRow filaA3 in dgvAccountsA3ERP.Rows)
                {
                    codigoRPSTenA3ERP = filaA3.Cells["REPASAT_ID"].Value.ToString();
                    foreach (DataGridViewRow filaRPST in dgvAccountsRepasat.Rows)
                    {
                        codigoRPST = filaRPST.Cells["IDREPASAT"].Value.ToString();
                        if (codigoRPSTenA3ERP == codigoRPST)
                        {
                            dgvAccountsA3ERP.Rows[filaA3.Index].Cells["nomArticulo"].Value= "OK " + filaA3.Cells["nomArticulo"].Value.ToString();
                            dgvAccountsA3ERP.Rows[filaA3.Index].Cells["REPASAT_ID"].Style.BackColor = Color.Green;
                            break;
                        }

                    }

                }

            }
            catch (Exception ex)
            {

            }

        }

        private void verificarCodigosEnRepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string codigoA3ERPenRPST = "";
                string codigoA3ERP = "";
                foreach (DataGridViewRow fila in dgvRepasatData.Rows) {
                    codigoA3ERPenRPST = fila.Cells["CODIGOERP"].Value.ToString();
                    foreach (DataGridViewRow filaA3 in dgvA3ERPData.Rows) {
                        codigoA3ERP = filaA3.Cells["CODERP"].Value.ToString();
                        if (codigoA3ERP == codigoA3ERPenRPST) {
                            dgvRepasatData.Rows[fila.Index].Cells["CODIGOERP"].Style.BackColor = Color.Green;
                        }
                    }
                }
            }
            catch (Exception ex) {

            }
        }

        private void verificarCódigoDeCuentaEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string codCuentaA3enRPST = "";
                string codCuentaA3 = "";
                string nomCuenta = "";
                bool Existe = false;


                string tipoDeCuenta = "CLIENTES";
                string query = "SELECT LTRIM(CODCLI) AS CODCUENTA FROM CLIENTES";

                if (rbCompras.Checked)
                {
                    tipoDeCuenta = "PROVEED";
                    query = "SELECT LTRIM(CODPRO) AS CODCUENTA FROM PROVEED";
                }

                DataTable dtCuentas = new DataTable();
                dtCuentas = sql.obtenerDatosSQLScript(query);

                foreach (DataGridViewRow fila in dgvRPST.Rows)
                {
                    Existe = false;

                    codCuentaA3enRPST = (fila.Cells["CLIENTE"].Value.ToString().Split('('))[1].Replace(")", "");
                    nomCuenta = fila.Cells["CLIENTE"].Value.ToString();
                    foreach (DataRow filaA3 in dtCuentas.Rows) {
                        if (Existe) break;
                        codCuentaA3 = filaA3["CODCUENTA"].ToString();
                        if (codCuentaA3 == codCuentaA3enRPST)
                        {
                            dgvRPST.Rows[fila.Index].Cells["CLIENTE"].Style.BackColor = Color.Green;
                            dgvRPST.Rows[fila.Index].Cells["CLIENTE"].Style.ForeColor = Color.White;
                            Existe = true;
                            break;
                        }
                    }
                    if (!Existe) {
                        dgvRPST.Rows[fila.Index].Cells["CLIENTE"].Style.BackColor = Color.Red;
                        dgvRPST.Rows[fila.Index].Cells["CLIENTE"].Value = "REVISAR:" + nomCuenta;
                    }


                }

                "Proceso finalizado".mb();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btklstic_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCabeceraFras = new DataTable();
                DataTable dtLineasFras = new DataTable();
                DataTable dtCentrosC = new DataTable();
                DataTable dtArticulos = new DataTable();
                DataTable dtApuntesCont = new DataTable();
                DataTable dtCuentasContables = new DataTable();

                string cuentaVieja = "";
                string cuentaNueva = "";
                string idLinea = "";
                string idApunte = "";
                string idCuenta = null;


                //dtCabeceraFras = sql.cargarDatosScriptEnTabla("SELECT * FROM CABEFACV WHERE FECHA >='2023-01-01'");
                dtLineasFras = sql.cargarDatosScriptEnTabla("SELECT CABEFACV.SERIE, CAST(CABEFACV.NUMAPUNTE AS INT) AS NUMAPUNTE, CABEFACV.NOMCLI, CABEFACV.IDFACV, " +
                    " CABEFACV.FECHA, LINEFACT.CENTROCOSTE2,CTACONL,CAST(CUENTAS.IDCUENTA AS INT) AS IDCUENTA " +
                    " FROM LINEFACT " +
                    " INNER JOIN CABEFACV ON LINEFACT.IDFACV=CABEFACV.IDFACV " +
                    " INNER JOIN CUENTAS ON LINEFACT.CTACONL = CUENTAS.CUENTA " +
                    " WHERE CABEFACV.FECHA >= '2023-01-01' ");


                //dtApuntesCont = sql.cargarDatosScriptEnTabla("SELECT * FROM __ASIENTOS WHERE IDCUENTA in (SELECT CAST(IDCUENTA as int) IDCUENTA "+
                //    " FROM CUENTAS WHERE CUENTA LIKE '70000%' AND PLACON='NPGC')");

                // dtCuentasContables = sql.cargarDatosScriptEnTabla("SELECT CAST(IDCUENTA as int) IDCUENTA, CUENTA, DESCCUE FROM CUENTAS WHERE CUENTA LIKE '70000%' AND PLACON='NPGC'");

                //dtArticulos = sql.cargarDatosScriptEnTabla("SELECT * FROM CABEFACV WHERE FECHA >='2023-01-01'");
                //dtCentrosC = sql.cargarDatosScriptEnTabla("SELECT * FROM CABEFACV WHERE FECHA >='2023-01-01'");

                foreach (DataRow fila in dtLineasFras.Rows) {
                    idCuenta = "5764";
                    cuentaVieja = fila["CTACONL"].ToString();
                    // cuentaNueva = fila["CTACONV"].ToString();
                    //idLinea =fila["IDLIN"].ToString().Replace(",0000", "");
                    idApunte = fila["NUMAPUNTE"].ToString();
                    idCuenta = fila["IDCUENTA"].ToString();
                    //idLinea = Convert.ToInt32(idLinea).ToString();

                    sql.actualizarCampo("UPDATE __ASIENTOS SET IDCUENTA = " + idCuenta + " WHERE IDCUENTA= 5764 AND NUMAPUNTE= " + idApunte);
                }

            }

            catch (Exception ex) {

            }
        }

        private void bt_tbai_Click(object sender, EventArgs e)
        {
            try
            {
                inicioProceso();
                
                ticketBaiToolStripMenuItem.Visible = true;

                TBai.csTBai TBai = new TBai.csTBai();
                splitContainer8.Panel2Collapsed = true;
                splitContainer8.Panel2.Hide();

                dgvRPST.DataSource = null;
                dgvRPST.Rows.Clear();
                dgvRPST.Columns.Clear();

                string filtroFechaIni = dtpFromInvoicesV.Value.Date.ToString("dd-MM-yyyy");
                string filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("dd-MM-yyyy");

                DataTable dtFacturasFromRPST = new DataTable();
                
                filtroFechaIni = dtpFromInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                filtroFechaFin = dtpToInvoicesV.Value.Date.ToString("yyyy-MM-dd");
                dtFacturasFromRPST = cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, rbVentas.Checked, true, filtroDocumentosCarga());

                dgvRPST.DataSource = TBai.facturasTbaiToRpst(dtpFromInvoicesV.Value.Date.ToString("dd-MM-yyyy"), dtpToInvoicesV.Value.Date.ToString("dd-MM-yyyy"), dtFacturasFromRPST);
                informarDetalleGrid();

                inicioProceso(false);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void actualizarDocsDesdeA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                inicioProceso();
                string RPST_serieDoc = "";
                string A3_serie = "";
                string numeroDocumento = "";
                string urlTicketBai = "";
                string idTicketBai = "";
                string idDocRPST = "";
                string RPST_external_doc = "";
                string messageUpdate = "";
                int contPreguntas = 0;
                DialogResult resultConfirmacion = new DialogResult();
                DialogResult result = new DialogResult();
                string messageConfirm = "¿Quieres omitir las confirmaciones?";
                string nomCampoExternoRPST = "";
                csRepasatWebService rpstWS = new csRepasatWebService();
                string tipoObjetoRPST = obtenerTipoObjetoRPST();

                TBai.csTBai csTBai = new TBai.csTBai();
                csTBai.actualizarDocsRPSTDesdeA3(tipoObjetoRPST,dgvRPST);

                inicioProceso(false);
            }
            catch (Exception ex) {
                ex.Message.mb();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Repasat.SyncRepasat.csSyncRepasat repasatSync = new Repasat.SyncRepasat.csSyncRepasat();
            repasatSync.facturasA3ERPToRepasat();
        }

        private void btnCostCenters_Click(object sender, EventArgs e)
        {
            dgvA3ERPData.DataSource = null;
            dgvRepasatData.DataSource = null;
            tipoObjeto = "costcenters";
            campoExternoRepasat = "codExternoCentroC";

            tablaToUpdate = "CENTROSC";
            campoKeyToUpdate = "CENTROCOSTE";
            repasatKeyField = "RPST_ID_CENTROC";

            centrosCosteRepasat();
            centrosCosteA3ERP();
        }

        private void btnUploadCentrosCoste_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("Va a dar de alta en Repasat los centros de coste no asignados, ¿está seguro/a?", "Sinronizar Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                cargarCentrosDeCoste();
                centrosCosteRepasat();
                centrosCosteA3ERP();
                MessageBox.Show("Proceso finalizado");
            }
        }

        private void btnBudget_Click(object sender, EventArgs e)
        {
            //A3ERP.Accounting.frBudget.DefInstance.MdiParent = this;
            A3ERP.Accounting.frBudget.DefInstance.Show();
            A3ERP.Accounting.frBudget.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabUtilidades);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Add(tabUtilidades);
        }

        private void porCIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                inicioProceso();

                string cuentaA3_RPST = "";
                string idCuentaRPST = "";
                string codCuenta_A3 = "";
                string CIF_cuenta_RPST = "";
                string CIF_cuenta_A3ERP = "";
                int filasUpdated = 0;
                int CIF_enA3 = 0;
                string scriptUpdate = "";


                if (dgvAccountsRepasat.SelectedRows.Count == 0)
                {
                    ("NO HAY FILAS SELECCIONADAS").mb();
                    return;
                }
                else
                {
                    DialogResult Resultado = MessageBox.Show("Se van a actualizar " + dgvAccountsRepasat.SelectedRows.Count +
                        "  códigos de Repasat en A3ERP. \n Sólo se validaran cuentas con CIF informado\n¿Está seguro?", "Actualizar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Resultado == DialogResult.Yes)
                    {
                        DataTable dtCuenta = new DataTable();
                        datacommand.Connection = dataconnection;
                        dataconnection.ConnectionString = csGlobal.cadenaConexion;
                        dataconnection.Open();


                        foreach (DataGridViewRow fila in dgvAccountsRepasat.SelectedRows)
                        {
                            CIF_cuenta_RPST = fila.Cells["NIF"].Value.ToString();
                            cuentaA3_RPST = fila.Cells["CODIGOERP"].Value.ToString();

                            if (!string.IsNullOrEmpty(CIF_cuenta_RPST))
                            {

                                foreach (DataGridViewRow filaA3 in dgvAccountsA3ERP.Rows) {
                                    CIF_cuenta_A3ERP = filaA3.Cells["NIFCLI"].Value.ToString();

                                    if (CIF_cuenta_RPST == CIF_cuenta_A3ERP) {
                                        idCuentaRPST = fila.Cells["IDREPASAT"].Value.ToString();
                                        codCuenta_A3 = filaA3.Cells["CODCLI"].Value.ToString().Trim();
                                        CIF_enA3++;
                                    }
                                }
                                if (CIF_enA3 == 1)
                                {
                                    switch (cboxMaestros.SelectedItem.ToString())
                                    {
                                        case "Clientes":
                                            scriptUpdate = "UPDATE __CLIENTES SET RPST_ID_CLI=" + idCuentaRPST + " WHERE LTRIM(CODCLI)='" + codCuenta_A3 + "'";
                                            if (cuentaA3_RPST == "99999")
                                            {

                                                DataTable dtParams = new DataTable();
                                                dtParams.Columns.Add("FIELD");
                                                dtParams.Columns.Add("KEY");
                                                dtParams.Columns.Add("VALUE");

                                                DataTable dtKeys = new DataTable();
                                                dtKeys.Columns.Add("KEY");

                                                csRepasatWebService rpstWS = new csRepasatWebService();

                                                DataRow drParams = dtParams.NewRow();
                                                drParams["FIELD"] = "idConExt";
                                                drParams["KEY"] = idCuentaRPST;
                                                drParams["VALUE"] = csGlobal.id_conexion_externa;
                                                dtParams.Rows.Add(drParams);

                                                drParams = dtParams.NewRow();
                                                drParams["FIELD"] = "tipoValor";
                                                drParams["KEY"] = idCuentaRPST;
                                                drParams["VALUE"] = "CLI";
                                                dtParams.Rows.Add(drParams);

                                                drParams = dtParams.NewRow();
                                                drParams["FIELD"] = "idExterno";
                                                drParams["KEY"] = idCuentaRPST;
                                                drParams["VALUE"] = codCuenta_A3;
                                                dtParams.Rows.Add(drParams);

                                                drParams = dtParams.NewRow();
                                                drParams["FIELD"] = "idRepasat";
                                                drParams["KEY"] = idCuentaRPST;
                                                drParams["VALUE"] = idCuentaRPST;
                                                dtParams.Rows.Add(drParams);

                                                DataRow drKeys = dtKeys.NewRow();
                                                drKeys["KEY"] = idCuentaRPST;
                                                dtKeys.Rows.Add(drKeys);
                                                //Sincronización de cuentas

                                                rpstWS.sincronizarObjetoRepasat("externalcustomers", "PUT", dtKeys, dtParams, "");

                                            }
                                            break;
                                        case "Proveedores":
                                            scriptUpdate = "UPDATE __PROVEED SET RPST_ID_PROV=" + idCuentaRPST + " WHERE LTRIM(CODPRO)='" + codCuenta_A3 + "'";
                                            break;
                                    }
                                    datacommand.CommandText = scriptUpdate;
                                    datacommand.ExecuteNonQuery();
                                    filasUpdated++;
                                }
                                else if (CIF_enA3 > 1) {
                                    //("El CIF: " + CIF_cuenta_A3ERP + " está repetido más de 1 vez en A3ERP" + "\n" + "revisarlo manualmente").mb(); ;
                                }
                                CIF_enA3 = 0;



                            }


                        }
                    }
                    dataconnection.Close();
                    ("Se han actualizado " + filasUpdated + " filas").mb();
                    inicioProceso(false);
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
                dataconnection.Close();

            }
        }

        private void dgvAccountsRepasat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (this.dgvAccountsRepasat.Columns[e.ColumnIndex].Name == "RPST")
                {
                    string tipoFichero = cboxMaestros.SelectedItem.ToString();
                    string urlProduct = "";
                    string idRegistro = "";
                    string fieldKeyRPST = "IDREPASAT";
                    string tipoRegistro = "";

                    switch (tipoFichero)
                    {
                        case ("Productos"):
                            urlProduct = "https://panel.repasat.com/es/admin/viewproduct/" + idRegistro;
                            tipoRegistro = "Artículo";
                            break;
                        case ("Clientes"):
                        case ("Proveedores"):
                            urlProduct = "https://panel.repasat.com/es/accounts/show/" + idRegistro;
                            tipoRegistro = "Cliente";
                            break;
                        case ("Direcciones Clientes"):
                        case ("Direcciones Proveedores"):
                            urlProduct = "https://panel.repasat.com/es/admin/viewclientaddress/" + idRegistro;
                            tipoRegistro = "Direccion";
                            break;

                    }

                    if ((dgvAccountsRepasat[fieldKeyRPST, e.RowIndex].Value.ToString() != "" && dgvAccountsRepasat[fieldKeyRPST, e.RowIndex].Value.ToString() != "0") && !string.IsNullOrEmpty(tipoFichero))
                    {
                        try
                        {
                            if (tipoFichero == "Direcciones Clientes" || tipoFichero == "Direcciones Proveedores")
                            {
                                csRepasatWebService rpst = new csRepasatWebService();

                                idRegistro = dgvAccountsRepasat[fieldKeyRPST, e.RowIndex].Value.ToString();
                                var idCount = rpst.obtenerIdCliJSON(idRegistro);
                                urlProduct = urlProduct + idRegistro +"/"+ idCount;
                                Process.Start("Chrome.exe", urlProduct);
                            }
                            else { 
                                idRegistro = dgvAccountsRepasat[fieldKeyRPST, e.RowIndex].Value.ToString();
                                urlProduct = urlProduct + idRegistro;
                                Process.Start("Chrome.exe", urlProduct);
                            }
                        }
                        catch
                        {
                            Process.Start("IExplore.exe", urlProduct);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El artículo no está en Repasat");
                    }
                }

            }

        }

        private void loadAccountsToReasign() {

            string nom = rbClientes.Checked ? "NOMCLI" : "NOMPRO";
            string cod = rbClientes.Checked ? "CODCLI" : "CODPRO";
            string table = rbClientes.Checked ? "CLIENTES" : "PROVEED";

            try
            {
                //string query = $"select codcli, CUENTA, CTADESC, CTALETRA, CTAPORCUENTA, CTAPROVISION, CTAPROVISIONNPGC from __Clientes where codcli in ({cliOrigen}, {cliDestino})";
                string query = "select " + cod + ", Concat(" + nom + ", ' (', LTRIM(" + cod + "), ')') as " + nom + " from " + table + " order by " + nom;
                DataTable dtListaClientesOrigen = sql.obtenerDatosSQLScript(query);
                cbClienteOrigen.DataSource = null;
                cbClienteOrigen.Items.Clear();
                cbClienteOrigen.ValueMember = cod;
                cbClienteOrigen.DisplayMember = nom;
                cbClienteOrigen.DataSource = dtListaClientesOrigen;

                DataTable dtListaClientesDestino = sql.obtenerDatosSQLScript(query);
                cbClienteDestino.DataSource = null;
                cbClienteDestino.Items.Clear();
                cbClienteDestino.ValueMember = cod;
                cbClienteDestino.DisplayMember = nom;
                cbClienteDestino.DataSource = dtListaClientesDestino;

            }
            catch (Exception ex) { }
        }

        private void validarFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                inicioProceso();

                foreach (DataGridViewRow fila in dgvRPST.SelectedRows)
                {
                    bool checkDocument = true;
                    string serie = fila.Cells["SERIE"].Value.ToString().Split(' ')[0];
                    string numDoc = fila.Cells["NUMDOC"].Value.ToString();

                    docsFromA3ToRPST("saleinvoices", false, filtroDocumentosCarga(), false, serie, numDoc,null,null,false, checkDocument);
                }
                inicioProceso(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void cbClienteOrigen_SelectedIndexChanged(object sender, EventArgs e)
        {

            loadClientesOrigen();
        }
        private void cbClienteDestino_SelectedIndexChanged(object sender, EventArgs e)
        {

            loadClientesDestino();
        }
        private void loadClientesOrigen() {
            try
            {
                if (rbClientes.Checked)
                {
                    if (cbClienteOrigen.DataSource != null)
                    {
                        //Cargamos datos cliente Destino
                        string nom = rbClientes.Checked ? "NOMCLI" : "NOMPRO";
                        string cod = rbClientes.Checked ? "CODCLI" : "CODPRO";
                        string table = rbClientes.Checked ? "CLIENTES" : "PROVEED";
                        string ValorOrigen = cbClienteOrigen.SelectedValue.ToString();
                        string query = "select " + cod + ", Concat(" + nom + ", ' (', LTRIM(" + cod + "), ')') as " + nom + " from " + table + " where codcli not in ('" + ValorOrigen + "') order by " + nom;
                        DataTable dtListaClientesDestino = sql.obtenerDatosSQLScript(query);
                        cbClienteDestino.DataSource = null;
                        cbClienteDestino.Items.Clear();
                        cbClienteDestino.ValueMember = cod;
                        cbClienteDestino.DisplayMember = nom;
                        cbClienteDestino.DataSource = dtListaClientesDestino;

                        string cliOrigen = cbClienteOrigen.SelectedValue.ToString();

                        string queryOrigen = "SELECT __CLIENTES.codcli, " +
                        "cast(C7.IDDIRENT as int) AS IDDIRENT, " +
                        "__CLIENTES.CUENTA, cast(C1.IDCUENTA as int) AS IDCTA1, " +
                        "CTADESC, cast(C2.IDCUENTA as int) AS IDCTA2, " +
                        "CTALETRA, cast(C3.IDCUENTA as int) AS IDCTA3, " +
                        "CTAPORCUENTA, cast(C4.IDCUENTA as int) AS IDCTA4, " +
                        "CTAPROVISION, cast(C5.IDCUENTA as int) AS IDCTA5, " +
                        "CTAPROVISIONNPGC, cast(C6.IDCUENTA as int) AS IDCTA6, " +
                        "cast(IDDOMBANCA as int) AS IDDOMBANCA " +
                        "FROM __Clientes " +
                        "LEFT JOIN CUENTAS C1 ON __CLIENTES.CUENTA = C1.CUENTA " +
                        "LEFT JOIN CUENTAS C2 ON __CLIENTES.CTADESC = C2.CUENTA " +
                        "LEFT JOIN CUENTAS C3 ON __CLIENTES.CTALETRA = C3.CUENTA " +
                        "LEFT JOIN CUENTAS C4 ON __CLIENTES.CTAPORCUENTA = C4.CUENTA " +
                        "LEFT JOIN CUENTAS C5 ON __CLIENTES.CTAPROVISION = C5.CUENTA " +
                        "LEFT JOIN CUENTAS C6 ON __CLIENTES.CTAPROVISIONNPGC = C6.CUENTA " +
                        "LEFT JOIN DIRENT C7 ON __CLIENTES.codcli = C7.codcli " +
                        "LEFT JOIN DOMBANCA C8 ON __CLIENTES.CODCLI = C8.CODcli " +
                        "WHERE __CLIENTES.codcli in ( '" + cliOrigen + "' ) " +
                        "AND C1.PLACON = 'NPGC' AND C2.PLACON = 'NPGC' AND C3.PLACON = 'NPGC' AND C4.PLACON = 'NPGC' AND C7.DEFECTO='T' AND (IDDOMBANCA IS NULL OR C8.NUMDOM=1)";

                        DataTable dtClientesOrigen = sql.obtenerDatosSQLScript(queryOrigen);


                        if (dtClientesOrigen != null)
                        {

                            //Enseñamos datos cliente Origen
                            codCliO.Text = "El codigo del Cliente es: ";
                            tbCodCliOrigen.Text = dtClientesOrigen.Rows[0]["CODCLI"].ToString().Trim();

                            lbIdDomBancaO.Text = "El codigo del IdDomBanca es: ";
                            tbIdDomBancaOrigen.Text = dtClientesOrigen.Rows[0]["IDDOMBANCA"].ToString();

                            IdDirentO.Text = "El codigo de la direccion es: ";
                            tbIdDirentO.Text = dtClientesOrigen.Rows[0]["IDDIRENT"].ToString();

                            cuentaOrigen.Text = "La cuenta es: ";
                            lbCuentaOrigen.Text = dtClientesOrigen.Rows[0]["CUENTA"].ToString();
                            tbCuentaOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA1"].ToString();

                            CTADESCOrigen.Text = "La CTADESC es: ";
                            lbCTADESCOrigen.Text = dtClientesOrigen.Rows[0]["CTADESC"].ToString();
                            tbCTADESCOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA2"].ToString();

                            CTALETRAOrigen.Text = "La CTALETRA es: ";
                            lbCTALETRAOrigen.Text = dtClientesOrigen.Rows[0]["CTALETRA"].ToString();
                            tbCTALETRAOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA3"].ToString();

                            CTAPORCUENTAOrigen.Text = "La CTAPORCUENTA es: ";
                            lbCTAPORCUENTAOrigen.Text = dtClientesOrigen.Rows[0]["CTAPORCUENTA"].ToString();
                            tbCTAPORCUENTAOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA4"].ToString();

                            if (dtClientesOrigen.Rows[0]["CTAPROVISION"].ToString() != null)
                            {
                                CTAPROVISIONOrigen.Text = "La CTAPROVISION es: ";
                                lbCTAPROVISIONOrigen.Text = dtClientesOrigen.Rows[0]["CTAPROVISION"].ToString();
                                tbCTAPROVISIONOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA5"].ToString();
                            }

                            CTAPROVISIONNPGCOrigen.Text = "La CTAPROVISIONNPGC es: ";
                            lbCTAPROVISIONNPGCOrigen.Text = dtClientesOrigen.Rows[0]["CTAPROVISIONNPGC"].ToString();
                            tbCTAPROVISIONNPGCOrigen.Text = dtClientesOrigen.Rows[0]["IDCTA6"].ToString();

                        }
                    }
                }
                else
                {
                    if (cbClienteOrigen.DataSource != null)
                    {

                        string proOrigen = cbClienteOrigen.SelectedValue.ToString();

                        string queryOrigen = "select __PROVEED.CODPRO, " +
                        "cast(C4.IDDIRENT as int) AS IDDIRENT, " +
                        "__PROVEED.CUENTA, cast(C1.IDCUENTA as int) AS IDCTA1, " +
                        "CTALETRA, cast(C2.IDCUENTA as int) AS IDCTA2, " +
                        "CTAANTC, cast(C3.IDCUENTA as int) AS IDCTA3, " +
                        "cast(IDDOMBANCA as int) AS IDDOMBANCA " +
                        "FROM __PROVEED " +
                        "LEFT JOIN CUENTAS C1 ON __PROVEED.CUENTA=C1.CUENTA " +
                        "LEFT JOIN CUENTAS C2 ON __PROVEED.CTALETRA=C2.CUENTA " +
                        "LEFT JOIN CUENTAS C3 ON __PROVEED.CTAANTC=C3.CUENTA " +
                        "LEFT JOIN __DIRENTPRO C4 ON __PROVEED.CODPRO = C4.CODPRO " +
                        "LEFT JOIN DOMBANCA C5 ON __PROVEED.CODPRO = C5.CODPRO  " +
                        "WHERE __PROVEED.CODPRO in ( '" + proOrigen + "' ) " +
                        "AND C1.PLACON='NPGC' AND C2.PLACON='NPGC' AND C3.PLACON='NPGC' AND C4.DEFECTO='T' AND (IDDOMBANCA IS NULL OR C5.NUMDOM=1)";

                        DataTable dtProveedoresOrigen = sql.obtenerDatosSQLScript(queryOrigen);

                        if (dtProveedoresOrigen != null)
                        {

                            //Enseñamos datos proveedor Origen
                            codCliO.Text = "El codigo del Proveedor es: ";
                            tbCodCliOrigen.Text = dtProveedoresOrigen.Rows[0]["CODPRO"].ToString().Trim();

                            lbIdDomBancaO.Text = "El codigo del IdDomBanca es: ";
                            tbIdDomBancaOrigen.Text = dtProveedoresOrigen.Rows[0]["IDDOMBANCA"].ToString();

                            IdDirentO.Text = "El codigo de la direccion es: ";
                            tbIdDirentO.Text = dtProveedoresOrigen.Rows[0]["IDDIRENT"].ToString();

                            cuentaOrigen.Text = "La cuenta es: ";
                            lbCuentaOrigen.Text = dtProveedoresOrigen.Rows[0]["CUENTA"].ToString();
                            tbCuentaOrigen.Text = dtProveedoresOrigen.Rows[0]["IDCTA1"].ToString();

                            CTADESCOrigen.Text = "La CTAANTC es: ";
                            lbCTADESCOrigen.Text = dtProveedoresOrigen.Rows[0]["CTAANTC"].ToString();
                            tbCTADESCOrigen.Text = dtProveedoresOrigen.Rows[0]["IDCTA2"].ToString();

                            CTALETRAOrigen.Text = "La CTALETRA es: ";
                            lbCTALETRAOrigen.Text = dtProveedoresOrigen.Rows[0]["CTALETRA"].ToString();
                            tbCTALETRAOrigen.Text = dtProveedoresOrigen.Rows[0]["IDCTA3"].ToString();

                            CTAPORCUENTAOrigen.Text = "";
                            CTAPROVISIONOrigen.Text = "";
                            CTAPROVISIONNPGCOrigen.Text = "";

                        }
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void validarArticulosDeA3EnRPSTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarCodigosArticulosEnRepasat();
        }

        private void cboxTicketBai_CheckedChanged(object sender, EventArgs e)
        {
                csGlobal.ticketBaiActivo = cboxTicketBai.Checked ? true : false;
        }

        private void loadClientesDestino()
        {
            try
            {
                if (rbClientes.Checked)
                {
                    if (cbClienteDestino.DataSource != null)
                    {

                        string cliDestino = cbClienteDestino.SelectedValue.ToString();

                        string queryDestino = "SELECT __CLIENTES.codcli, " +
                        "cast(C7.IDDIRENT as int) AS IDDIRENT, " +
                        "__CLIENTES.CUENTA, cast(C1.IDCUENTA as int) AS IDCTA1, " +
                        "CTADESC, cast(C2.IDCUENTA as int) AS IDCTA2, " +
                        "CTALETRA, cast(C3.IDCUENTA as int) AS IDCTA3, " +
                        "CTAPORCUENTA, cast(C4.IDCUENTA as int) AS IDCTA4, " +
                        "CTAPROVISION, cast(C5.IDCUENTA as int) AS IDCTA5, " +
                        "CTAPROVISIONNPGC, cast(C6.IDCUENTA as int) AS IDCTA6, " +
                        "cast(IDDOMBANCA as int) AS IDDOMBANCA " +
                        "FROM __Clientes " +
                        "LEFT JOIN CUENTAS C1 ON __CLIENTES.CUENTA = C1.CUENTA " +
                        "LEFT JOIN CUENTAS C2 ON __CLIENTES.CTADESC = C2.CUENTA " +
                        "LEFT JOIN CUENTAS C3 ON __CLIENTES.CTALETRA = C3.CUENTA " +
                        "LEFT JOIN CUENTAS C4 ON __CLIENTES.CTAPORCUENTA = C4.CUENTA " +
                        "LEFT JOIN CUENTAS C5 ON __CLIENTES.CTAPROVISION = C5.CUENTA " +
                        "LEFT JOIN CUENTAS C6 ON __CLIENTES.CTAPROVISIONNPGC = C6.CUENTA " +
                        "LEFT JOIN DIRENT C7 ON __CLIENTES.codcli = C7.codcli " +
                        "LEFT JOIN DOMBANCA C8 ON __CLIENTES.CODCLI = C8.codcli " +
                        "WHERE __CLIENTES.codcli in ( '" + cliDestino + "' ) " +
                        "AND C1.PLACON = 'NPGC' AND C2.PLACON = 'NPGC' AND C3.PLACON = 'NPGC' AND C4.PLACON = 'NPGC' AND C7.DEFECTO='T' AND (IDDOMBANCA IS NULL OR C8.NUMDOM=1)";

                        DataTable dtClientesDestino = sql.obtenerDatosSQLScript(queryDestino);

                        if (dtClientesDestino != null)
                        {

                            //Enseñamos datos cliente Destino
                            codCliD.Text = "El codigo del Cliente es: ";
                            tbCodCliDestino.Text = dtClientesDestino.Rows[0]["CODCLI"].ToString().Trim();

                            lbIdDomBancaD.Text = "El codigo del IdDomBanca es: ";
                            tbIdDomBancaDestino.Text = dtClientesDestino.Rows[0]["IDDOMBANCA"].ToString();

                            IdDirentD.Text = "El codigo de la direccion es: ";
                            tbIdDirentD.Text = dtClientesDestino.Rows[0]["IDDIRENT"].ToString();

                            cuentaDestino.Text = "La cuenta es: ";
                            lbcuentaDestino.Text = dtClientesDestino.Rows[0]["CUENTA"].ToString();
                            tbcuentaDestino.Text = dtClientesDestino.Rows[0]["IDCTA1"].ToString();

                            CTADESCDestino.Text = "La CTADESC es: ";
                            lbCTADESCDestino.Text = dtClientesDestino.Rows[0]["CTADESC"].ToString();
                            tbCTADESCDestino.Text = dtClientesDestino.Rows[0]["IDCTA2"].ToString();

                            CTALETRADestino.Text = "La CTALETRA es: ";
                            lbCTALETRADestino.Text = dtClientesDestino.Rows[0]["CTALETRA"].ToString();
                            tbCTALETRADestino.Text = dtClientesDestino.Rows[0]["IDCTA3"].ToString();

                            CTAPORCUENTADestino.Text = "La CTAPORCUENTA es: ";
                            lbCTAPORCUENTADestino.Text = dtClientesDestino.Rows[0]["CTAPORCUENTA"].ToString();
                            tbCTAPORCUENTADestino.Text = dtClientesDestino.Rows[0]["IDCTA4"].ToString();

                            if (dtClientesDestino.Rows[0]["CTAPROVISION"].ToString() != null)
                            {
                                CTAPROVISIONDestino.Text = "La CTAPROVISION es: ";
                                lbCTAPROVISIONDestino.Text = dtClientesDestino.Rows[0]["CTAPROVISION"].ToString();
                                tbCTAPROVISIONDestino.Text = dtClientesDestino.Rows[0]["IDCTA5"].ToString();
                            }

                            CTAPROVISIONNPGCDestino.Text = "La CTAPROVISIONNPGC es: ";
                            lbCTAPROVISIONNPGCDestino.Text = dtClientesDestino.Rows[0]["CTAPROVISIONNPGC"].ToString();
                            tbCTAPROVISIONNPGCDestino.Text = dtClientesDestino.Rows[0]["IDCTA6"].ToString();
                        }
                    }
                }
                else
                {
                    if (cbClienteDestino.DataSource != null)
                    {
                        string proDestino = cbClienteDestino.SelectedValue.ToString();

                        string queryDestino = "select __PROVEED.CODPRO, " +
                        "cast(C4.IDDIRENT as int) AS IDDIRENT, " +
                        "__PROVEED.CUENTA, cast(C1.IDCUENTA as int) AS IDCTA1, " +
                        "CTALETRA, cast(C2.IDCUENTA as int) AS IDCTA2, " +
                        "CTAANTC, cast(C3.IDCUENTA as int) AS IDCTA3, " +
                        "cast(IDDOMBANCA as int) AS IDDOMBANCA " +
                        "FROM __PROVEED " +
                        "LEFT JOIN CUENTAS C1 ON __PROVEED.CUENTA=C1.CUENTA " +
                        "LEFT JOIN CUENTAS C2 ON __PROVEED.CTALETRA=C2.CUENTA " +
                        "LEFT JOIN CUENTAS C3 ON __PROVEED.CTAANTC=C3.CUENTA " +
                        "LEFT JOIN __DIRENTPRO C4 ON __PROVEED.CODPRO = C4.CODPRO " +
                        "LEFT JOIN DOMBANCA C5 ON __PROVEED.CODPRO = C5.CODPRO " +
                        "WHERE __PROVEED.CODPRO in ( '" + proDestino + "' ) " +
                        "AND C1.PLACON='NPGC' AND C2.PLACON='NPGC' AND C3.PLACON='NPGC' AND C4.DEFECTO='T' AND (IDDOMBANCA IS NULL OR C5.NUMDOM=1) ";

                        DataTable dtProveedoresDestino = sql.obtenerDatosSQLScript(queryDestino);

                        if (dtProveedoresDestino != null)
                        {

                            //Enseñamos datos proveedor Destino
                            codCliD.Text = "El codigo del Proveedor es: ";
                            tbCodCliDestino.Text = dtProveedoresDestino.Rows[0]["CODPRO"].ToString().Trim();

                            lbIdDomBancaD.Text = "El codigo del IdDomBanca es: ";
                            tbIdDomBancaDestino.Text = dtProveedoresDestino.Rows[0]["IDDOMBANCA"].ToString();

                            IdDirentD.Text = "El codigo de la direccion es: ";
                            tbIdDirentD.Text = dtProveedoresDestino.Rows[0]["IDDIRENT"].ToString();

                            cuentaDestino.Text = "La cuenta es: ";
                            lbcuentaDestino.Text = dtProveedoresDestino.Rows[0]["CUENTA"].ToString();
                            tbcuentaDestino.Text = dtProveedoresDestino.Rows[0]["IDCTA1"].ToString();

                            CTADESCDestino.Text = "La CTAANTC es: ";
                            lbCTADESCDestino.Text = dtProveedoresDestino.Rows[0]["CTAANTC"].ToString();
                            tbCTADESCDestino.Text = dtProveedoresDestino.Rows[0]["IDCTA2"].ToString();

                            CTALETRADestino.Text = "La CTALETRA es: ";
                            lbCTALETRADestino.Text = dtProveedoresDestino.Rows[0]["CTALETRA"].ToString();
                            tbCTALETRADestino.Text = dtProveedoresDestino.Rows[0]["IDCTA3"].ToString();

                            CTAPORCUENTADestino.Text = "";
                            CTAPROVISIONDestino.Text = "";
                            CTAPROVISIONNPGCDestino.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rbProveedores_CheckedChanged(object sender, EventArgs e)
        {
            textoComboboxDestino.Text = rbClientes.Checked ? "Cliente Origen" : "Proveedor Origen";
            textoComboboxOrigen.Text = rbClientes.Checked ? "Cliente Destino" : "Proveedor Destino";

            CTADESCDestino.Text = rbClientes.Checked ? "CTADESC" : "CTAANTC";
            CTAPORCUENTADestino.Text = rbClientes.Checked ? "CTAPORCUENTA" : "";
            CTAPROVISIONDestino.Text = rbClientes.Checked ? "CTAPROVISION" : "";
            CTAPROVISIONNPGCDestino.Text = rbClientes.Checked ? "CTAPROVISIONNPGC" : "";

            CTADESCOrigen.Text = rbClientes.Checked ? "CTADESC" : "CTAANTC";
            CTAPORCUENTAOrigen.Text = rbClientes.Checked ? "CTAPORCUENTA" : "";
            CTAPROVISIONOrigen.Text = rbClientes.Checked ? "CTAPROVISION" : "";
            CTAPROVISIONNPGCOrigen.Text = rbClientes.Checked ? "CTAPROVISIONNPGC" : "";

            tbCTAPORCUENTADestino.Visible = rbClientes.Checked ? true : false;
            tbCTAPROVISIONDestino.Visible = rbClientes.Checked ? true : false;
            tbCTAPROVISIONNPGCDestino.Visible = rbClientes.Checked ? true : false;

            tbCTAPORCUENTAOrigen.Visible = rbClientes.Checked ? true : false;
            tbCTAPROVISIONOrigen.Visible = rbClientes.Checked ? true : false;
            tbCTAPROVISIONNPGCOrigen.Visible = rbClientes.Checked ? true : false;

            lbCTAPORCUENTADestino.Visible = rbClientes.Checked ? true : false;
            lbCTAPROVISIONDestino.Visible = rbClientes.Checked ? true : false;
            lbCTAPROVISIONNPGCDestino.Visible = rbClientes.Checked ? true : false;

            lbCTAPORCUENTAOrigen.Visible = rbClientes.Checked ? true : false;
            lbCTAPROVISIONOrigen.Visible = rbClientes.Checked ? true : false;
            lbCTAPROVISIONNPGCOrigen.Visible = rbClientes.Checked ? true : false;

            string nom = rbClientes.Checked ? "NOMCLI" : "NOMPRO";
            string cod = rbClientes.Checked ? "CODCLI" : "CODPRO";
            string table = rbClientes.Checked ? "CLIENTES" : "PROVEED";

            //string query = $"select codcli, CUENTA, CTADESC, CTALETRA, CTAPORCUENTA, CTAPROVISION, CTAPROVISIONNPGC from __Clientes where codcli in ({cliOrigen}, {cliDestino})";
            string query = "select " + cod + ", Concat(" + nom + ", ' (', LTRIM(" + cod + "), ')') as " + nom + " from " + table + " order by " + nom;
            DataTable dtListaClientesOrigen = sql.obtenerDatosSQLScript(query);
            cbClienteOrigen.DataSource = null;
            cbClienteOrigen.Items.Clear();
            cbClienteOrigen.ValueMember = cod;
            cbClienteOrigen.DisplayMember = nom;
            cbClienteOrigen.DataSource = dtListaClientesOrigen;

            DataTable dtListaClientesDestino = sql.obtenerDatosSQLScript(query);
            cbClienteDestino.DataSource = null;
            cbClienteDestino.Items.Clear();
            cbClienteDestino.ValueMember = cod;
            cbClienteDestino.DisplayMember = nom;
            cbClienteDestino.DataSource = dtListaClientesDestino;

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
         {
            if (tabControl1.SelectedTab.Name == "tabReasignacion") {
                loadAccountsToReasign();
            }

        }

        private void btReAsing_Click(object sender, EventArgs e)
        {
            updateCuentas(rbProveedores.Checked);
        }

        private void updateCuentas(bool proveedores=false)
        {
            try
            {
                string anexoQryCartera = "";
                string dombanca = "";
                string COD = "";
                if (tbCodCliDestino.Text == tbCodCliOrigen.Text)
                {
                    MessageBox.Show("Los valores del Origen y el Destino tienen que ser diferentes");
                }
                else
                {
                    string[] documentos=new string[0];
                    string campoToUpdate = proveedores ? "CODPRO" : "CODCLI";
                    if (proveedores) {
                        Array.Resize(ref documentos, 9);
                        documentos[0] = "CABEOFEC";
                        documentos[1] = "CABEPEDC";
                        documentos[2] = "CABEALBC";
                        documentos[3] = "CABEFACC";
                        documentos[4] = "CARTERA";
                        documentos[5] = "__AGRUPACIONES";
                        documentos[6] = "ARTICULO";
                        documentos[7] = "PRCESP";
                        documentos[8] = "DESCUENT";
                        //documentos = { "CABEOFEC", "CABEPEDC", "CABEALBC", "CABEFACC", "CARTERA"};
                    }
                    else {
                        Array.Resize(ref documentos, 11);
                        documentos[0] = "CABEOFEV";
                        documentos[1] = "CABEPEDV";
                        documentos[2] = "CABEALBV";
                        documentos[3] = "CABEFACV";
                        documentos[4] = "CARTERA";
                        documentos[5] = "__CABEAUTO";
                        documentos[6] = "__AGRUPACIONES";
                        documentos[7] = "PRCESP";
                        documentos[8] = "DESCUENT";
                        documentos[9] = "FORMAPAGCLI";
                        documentos[10] = "CABEPROD";
                        //documentos =  [ "CABEOFEV", "CABEPEDV", "CABEALBV", "CABEFACV", "CARTERA", "__CABEAUTO" ];
                    }

                    foreach (var documento in documentos)
                    {

                        string query = "";
                        dombanca = lbcodCliD.Text == "" ? "null" : "'"+lbcodCliD.Text+"'";

                        anexoQryCartera = documento == "CARTERA" ? ", IDDOMBANCA = "+ dombanca : "";
                        
                        COD = csUtilidades.comprobarEsNumero(tbCodCliDestino.Text.Trim()) ? tbCodCliDestino.Text.PadLeft(8) : tbCodCliDestino.Text.Trim();

                        if (documento == "CARTERA" || documento == "__AGRUPACIONES" || documento == "ARTICULO" || documento == "PRCESP" || documento == "DESCUENT" || documento == "FORMAPAGCLI" || documento == "CABEPROD") { 
                            query = "UPDATE " + documento + " SET " + campoToUpdate + " ='" + COD + "'" + anexoQryCartera + " WHERE LTRIM(" + campoToUpdate + ") = '" + tbCodCliOrigen.Text.Trim() + "'";
                            csUtilidades.ejecutarConsulta(query, false);
                        }
                        else
                        {
                            query = "UPDATE " + documento + " SET " + campoToUpdate + " ='" + COD + "', IDDIRENT = " + tbIdDirentD.Text + " WHERE LTRIM(" + campoToUpdate + ") = '" + tbCodCliOrigen.Text.Trim() + "'";
                        }
                        csUtilidades.ejecutarConsulta(query, false);

                        if (documento == "CABEFACV" || documento == "__CABEAUTO" || documento == "CABEFACC")
                        {
                            query = "UPDATE " + documento + " SET " + campoToUpdate + "FAC = '" + COD + "' WHERE LTRIM(" + campoToUpdate + "FAC) = '" + tbCodCliOrigen.Text.Trim() + "'";
                            csUtilidades.ejecutarConsulta(query, false);
                        }

                    }

                    //Actualizamos la tabla "__ASIENTOS" con las filas (Data Row) de un DT (data table) (con el DT dtCuentasCont)
                    //string queryAsientos = "select CUENTA, IDCUENTA from CUENTAS";
                    //DataTable dtCuentas = sql.obtenerDatosSQLScript(queryAsientos);
                    DataTable dtCuentasCont = new DataTable();

                    dtCuentasCont.Columns.Add("CTAOrigen");
                    dtCuentasCont.Columns.Add("IDCTAOrigen");
                    dtCuentasCont.Columns.Add("CTADestino");
                    dtCuentasCont.Columns.Add("IDCTADestino");

                    DataRow drCta = dtCuentasCont.NewRow();
                    drCta["CTAOrigen"] = lbCuentaOrigen.Text;
                    drCta["IDCTAOrigen"] = tbCuentaOrigen.Text;
                    drCta["CTADestino"] = lbcuentaDestino.Text;
                    drCta["IDCTADestino"] = tbcuentaDestino.Text;
                    dtCuentasCont.Rows.Add(drCta);

                    drCta = dtCuentasCont.NewRow();
                    drCta["CTAOrigen"] = lbCTADESCOrigen.Text;
                    drCta["IDCTAOrigen"] = tbCTADESCOrigen.Text;
                    drCta["CTADestino"] = lbCTADESCDestino.Text;
                    drCta["IDCTADestino"] = tbCTADESCDestino.Text;
                    dtCuentasCont.Rows.Add(drCta);

                    drCta = dtCuentasCont.NewRow();
                    drCta["CTAOrigen"] = lbCTALETRAOrigen.Text;
                    drCta["IDCTAOrigen"] = tbCTALETRAOrigen.Text;
                    drCta["CTADestino"] = lbCTALETRADestino.Text;
                    drCta["IDCTADestino"] = tbCTALETRADestino.Text;
                    dtCuentasCont.Rows.Add(drCta);

                    if (!proveedores)
                    {
                        drCta = dtCuentasCont.NewRow();
                        drCta["CTAOrigen"] = lbCTAPORCUENTAOrigen.Text;
                        drCta["IDCTAOrigen"] = tbCTAPORCUENTAOrigen.Text;
                        drCta["CTADestino"] = lbCTAPORCUENTADestino.Text;
                        drCta["IDCTADestino"] = tbCTAPORCUENTADestino.Text;
                        dtCuentasCont.Rows.Add(drCta);

                        if( !string.IsNullOrEmpty(lbCTAPROVISIONOrigen.Text) && !string.IsNullOrEmpty(lbCTAPROVISIONDestino.Text)) { 
                            drCta = dtCuentasCont.NewRow();
                            drCta["CTAOrigen"] = lbCTAPROVISIONOrigen.Text;
                            drCta["IDCTAOrigen"] = tbCTAPROVISIONOrigen.Text;
                            drCta["CTADestino"] = lbCTAPROVISIONDestino.Text;
                            drCta["IDCTADestino"] = tbCTAPROVISIONDestino.Text;
                            dtCuentasCont.Rows.Add(drCta);
                        }else if(lbCTAPROVISIONOrigen.Text != "" || lbCTAPROVISIONDestino.Text != "")
                        {
                            MessageBox.Show("La CTAPROVISION debe tener valor tanto en el destino como en el origen");
                        }
                        drCta = dtCuentasCont.NewRow();
                        drCta["CTAOrigen"] = lbCTAPROVISIONNPGCOrigen.Text;
                        drCta["IDCTAOrigen"] = tbCTAPROVISIONNPGCOrigen.Text;
                        drCta["CTADestino"] = lbCTAPROVISIONNPGCDestino.Text;
                        drCta["IDCTADestino"] = tbCTAPROVISIONNPGCDestino.Text;
                        dtCuentasCont.Rows.Add(drCta);
                    }
                    foreach (DataRow fila in dtCuentasCont.Rows)
                    {
                        string query = "";
                        query = "UPDATE __ASIENTOS SET IDCUENTA = '" + fila["IDCTADestino"] + 
                            "' WHERE IDCUENTA = '" + fila["IDCTAOrigen"] + "'";
                        csUtilidades.ejecutarConsulta(query, false);
                    }
                    MessageBox.Show("La reasignacion se ha finalizado con exito");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnAuxiliaresToRPST_Click(object sender, EventArgs e)
        {
            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
            updateDataRPST.auxiliaresA3ToRepasat();        
        }

        private void checkBoxCanal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSeries.Checked)
            {
                cboxSeries.Visible = true;
                cargarSeriesDocs();
            }
            else
            {
                cboxSeries.Visible = false;
                cboxSeries.DataSource = null;
                cboxSeries.Refresh();
            }
        }

        private void btUpdateA3ERPToRepasat_Click(object sender, EventArgs e)
        {
            string idCliente = "";
            string tipoFichero = cboxMaestros.SelectedItem.ToString();

            if (dgvAccountsRepasat.SelectedRows.Count != 0)
            {
                DialogResult Resultado = MessageBox.Show("Se van a actualizar " + dgvAccountsRepasat.SelectedRows.Count + " registros en A3 con los datos de Repasat. \n ¿Está seguro?", "Actualizar Registros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.Yes)
                {
                    if (tipoFichero == "Productos")
                    {
                        cargarDatosArticulos(dgvAccountsRepasat, false, null, false, true);
                    }
                    else if (tipoFichero == "Clientes")
                    {
                        foreach (DataGridViewRow dgvselrow in dgvAccountsRepasat.SelectedRows)
                        {
                            idCliente = dgvselrow.Cells["CODIGOERP"].Value.ToString();
                            cargarDatosCuentasRepasat(objetoCliente(), false, idCliente, false, false, true);
                        }
                    }

                }
            }
        
            else
            {
                MessageBox.Show("Debes seleccionar los registros que quieres actualizar.", "Actualizar Registros");
            }
        }

        private void updateSyncDate() {
            sql.ejecutarQuery("DELETE FROM KLS_REPASAT_UPDATE WHERE TABLA='accounts' AND FROM_RPST=0 AND DATE_SYNC>='" + dtpDateUpdateClientes.Value.ToString() + "'");
            sql.ejecutarQuery("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + dtpDateUpdateClientes.Value.ToString() + "','accounts',0,0)");
        }

        private void loadSyncDate() {
            string fechaUpdateClientes = sql.obtenerCampoTabla("SELECT MAX(DATE_SYNC) FROM KLS_REPASAT_UPDATE WHERE TABLA='accounts' AND FROM_RPST=0");
            dtpDateUpdateClientes.Value = Convert.ToDateTime(fechaUpdateClientes);
            dtpDateUpdateClientes.Refresh();
        }

        private void borrarSincronizaciónEnRepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Repasat.SyncRepasat.csDeleteSyncRepasat deleteSync = new Repasat.SyncRepasat.csDeleteSyncRepasat();
            deleteSync.deleteSyncRPSTObject(tipoObjeto, dgvRepasatData);
        }

        private void btnTestConnect1_Click(object sender, EventArgs e)
        {
            if (csGlobal.conexionDB.Contains("GARBIN"))
            {
                Utilidades.csConexiones dbConnectEsync = new Utilidades.csConexiones();
                dbConnectEsync.TestConexion();
            }
        }

        public void actualizarArticulosRPST()
        {
            cargarDatosArticulos(dgvAccountsRepasat, false, null, false, true);
        }

        private void numLineasDocsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarImportesDocumentos(cboxTipoDoc.Text, true, true);
        }







        //PRUEBAS MARIADB

        public void ProbarConexionMariaDB()
        {
            try
            {
                string connectionString = "Server=esportissim.com;Database=ps_esportissim;User ID=ps_esportissim;Password=BwgrFYML;Port=3306;";
                string connectionString2 = "Server=soyinformatica.com;Database=prueba_esync;User ID=user_esync;Password=Klosions2014*;Port=3306;";
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MessageBox.Show("Conexión exitosa a la base de datos MariaDB.", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al intentar conectar a la base de datos MariaDB: {ex.Message}\n\nStack Trace:\n{ex.StackTrace}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnMariaDBConn_Click_1(object sender, EventArgs e)
        {
            ProbarConexionMariaDB();
        }

        private void btnCrearCentrosCosteA3_Click(object sender, EventArgs e)
        {
            try
            {
                string filtroURL = "";
                var todosLosCentrosCoste = new List<Repasat.csCostCenters.Datum>();
                string tipoObjeto = "costcenters";
                string apiUrl = "&page=1";
                int currentPage = 1;

                do
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;
                        var json = updateDataRPST.whileJson(wc, tipoObjeto, apiUrl, "GET", tipoObjeto, filtroURL, "");
                        var repasat = JsonConvert.DeserializeObject<Repasat.csCostCenters.RootObject>(json);

                        if (repasat != null && repasat.data != null && repasat.data.Count > 0)
                        {
                            todosLosCentrosCoste.AddRange(repasat.data);

                            currentPage++;
                            apiUrl = $"&page={currentPage}&per_page=20"; 

                            if (repasat.next_page_url == null)
                            {
                                break;
                            }
                        }
                        else
                        {
                            MessageBox.Show("No se encontraron centros de coste.");
                            return; 
                        }
                    }
                } while (true); 

                Objetos.csCostCenters[] centroCosteArray = todosLosCentrosCoste
                    .Select(centro => new Objetos.csCostCenters
                    {
                        idCentroCoste = centro.uuidCentroCoste,
                        refCentroCoste = centro.refCentroCoste,
                        descCentroCoste = centro.descCentroCoste,
                        codExternoCentroCoste = centro.codExternoCentroC,
                        level1 = centro.level1.ToString(),
                        level2 = centro.level2.ToString(),
                        level3 = centro.level3.ToString(),
                        bloqueado = "F",
                        obsoleto = "F",
                        fecModificacion = centro.fecModificacion
                    }).ToArray();

                csa3erp a3erp = new csa3erp();
                csCRUDCentroCoste objCentro = new csCRUDCentroCoste();

                a3erp.abrirEnlace();
                objCentro.crearCentroCosteA3(centroCosteArray, tipoObjeto, campoExternoRepasat);
                a3erp.cerrarEnlace();

                //MUESRTA LOS DATOS EN EL DGV
                centrosCosteA3ERP();
                centrosCosteRepasat();

                MessageBox.Show("Proceso finalizado.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex.Message}");
            }
        }

        private void testcc_Click(object sender, EventArgs e)
        {
            try
            {
                Dismay.csDismay dismay = new klsync.Dismay.csDismay();
                dismay.procedimientoDismay();
                System.Windows.Forms.Application.Exit();
                MessageBox.Show("Tarea realizada");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ctexMenuMaster_Opening(object sender, CancelEventArgs e)
        {

        }

        private void ctextMenuSync_Opening(object sender, CancelEventArgs e)
        {

        }

        private void btnTestConexionesBD_Click(object sender, EventArgs e)
        {
            //RPST - PRESTA
            bool resultadoRepasatOPrestaShop = csConexiones.verificarConexionRepasatOPrestaShop();
            MessageBox.Show(resultadoRepasatOPrestaShop ? "Conexión con " + csGlobal.modeAp + " exitosa." : "Error en la conexión con " + csGlobal.modeAp + ".");

            //A3ERP
            bool resultadoA3erp = csConexiones.verificarConexionA3erp();
            MessageBox.Show(resultadoA3erp ? "Conexión con A3ERP exitosa." : "Error en la conexión con A3ERP.");
        }



        //BOTÓN DE GUARDAR O ACTUALIZAR EL NUEVO REGISTRO DE CRONJOB
        private void btnGuardarConfTareasProgramadas_Click(object sender, EventArgs e)
        {
            try
            {
                cmbFrequency.SelectedIndexChanged -= cmbFrequency_SelectedIndexChanged;
                csCronJobService cronJobService = new csCronJobService();

                string nombreTarea = cmbNameTask.Text;
                string nuevaFrecuencia = cmbFrequency.SelectedItem.ToString();
                TimeSpan? nuevaHoraEjecucion = (cmbFrequency.SelectedItem.ToString().ToUpper() == "DIA") ? dtpStartHour.Value.TimeOfDay : (TimeSpan?)null;

                if (lblModoEdicionTareasProgramadas.Visible)
                {
                    var cronJobEditado = new csCronJobConfig
                    {
                        Tarea = nombreTarea,
                        Ejecutar = true,
                        Frecuencia = nuevaFrecuencia,
                        Conexion = csGlobal.conexionDB,
                        UltimaEjecucion = null,
                        HoraEjecucion = dtpStartHour.Value.TimeOfDay,
                        RepeticionMinutos = (nuevaFrecuencia == "MINUTO") ? (int?)cmbMinutos.SelectedItem : null
                    };
        
                    cronJobService.guardarRegistros(new List<csCronJobConfig> { cronJobEditado });
                    cronJobService.borrarTareaProgramada(nombreTarea);
                    cronJobService.crearTareaProgramada(cronJobEditado);
                }
                else
                {
                    var nuevoCronJob = new csCronJobConfig
                    {
                        Tarea = nombreTarea,
                        Ejecutar = true,
                        Frecuencia = nuevaFrecuencia,
                        Conexion = csGlobal.conexionDB,
                        UltimaEjecucion = null,
                        HoraEjecucion = dtpStartHour.Value.TimeOfDay,
                        RepeticionMinutos = (nuevaFrecuencia == "MINUTO") ? (int?)cmbMinutos.SelectedItem : null
                    };

                    cronJobService.guardarRegistros(new List<csCronJobConfig> { nuevoCronJob });
                    cronJobService.crearTareaProgramada(nuevoCronJob);
                }

                MessageBox.Show("Configuración de tarea programada guardada exitosamente.");

                actualizarDataGridView();
                cargarTareasDisponibles();

                cmbFrequency.SelectedIndex = -1;
                dtpStartHour.Visible = false;
                lblStartHour.Visible = false;
                lblMinutos.Visible = false;
                cmbMinutos.Visible = false;
                lblModoEdicionTareasProgramadas.Visible = false;
                btnSalirModoEdicionTareasProgramadas.Visible = false;


                cmbFrequency.SelectedIndexChanged += cmbFrequency_SelectedIndexChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al guardar la configuración de la tarea programada: {ex.Message}\n\nRevisa que estén todos los campos llenos.");
            }
        }

        //FUNCIÓN PARA MOSTRAR LAS OPCIONES VALIDAS EN EL SELECTOR DE TAREAS SEGUN SI ESTAN YA CREADAS O NO
        private void cargarTareasDisponibles(string tareaSeleccionada = null)
        {
            csCronJobService cronJobService = new csCronJobService();
            List<string> tareasDisponibles = cronJobService.obtenerTareasDisponibles(csGlobal.conexionDB);

            cmbNameTask.DataSource = null;
            cmbNameTask.Items.Clear();

            if (!string.IsNullOrEmpty(tareaSeleccionada) && !tareasDisponibles.Contains(tareaSeleccionada))
            {
                tareasDisponibles.Add(tareaSeleccionada);
            }

            if (tareasDisponibles.Count > 0)
            {
                cmbNameTask.DataSource = tareasDisponibles;
                cmbNameTask.Enabled = true;
                cmbFrequency.Enabled = true;
                btnGuardarConfTareasProgramadas.Enabled = true;
            }
            else
            {
                cmbNameTask.Items.Add("No hay tareas disponibles");
                cmbNameTask.SelectedIndex = 0;
                cmbNameTask.Enabled = false;
                cmbFrequency.Enabled = false;
                btnGuardarConfTareasProgramadas.Enabled = false;
            }
        }

        //FUNCION PARA ACTUALIZAR LOS DATOS QUE SE MUESTRAN EN EL DATAGRIDVIEW DE CRONJOBS
        private void actualizarDataGridView()
        {
            dgvCronJobs.Columns.Clear();
            configurarBotonesDataGridView();

            csCronJobService cronJobService = new csCronJobService();
            List<csCronJobConfig> cronJobs = cronJobService.obtenerTodosLosRegistros();

            dgvCronJobs.DataSource = cronJobs.Select(cj => new
            {
                Tarea = cj.Tarea,
                Frecuencia = cj.Frecuencia,
                HoraEjecucion = cj.HoraEjecucion.HasValue ? cj.HoraEjecucion.Value.ToString(@"hh\:mm") : "-",
                RepeticionMinutos = cj.RepeticionMinutos.HasValue ? cj.RepeticionMinutos.ToString() : "-",
                UltimaEjecucion = cj.UltimaEjecucion.HasValue ? cj.UltimaEjecucion.Value.ToString("yyyy-MM-dd HH:mm") : "No ejecutada"
            }).ToList();

            dgvCronJobs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dgvCronJobs.Columns["Tarea"].HeaderText = "TAREA";
            dgvCronJobs.Columns["Frecuencia"].HeaderText = "FRECUENCIA";
            dgvCronJobs.Columns["HoraEjecucion"].HeaderText = "HORA ESPECÍFICA DE EJECUCIÓN";
            dgvCronJobs.Columns["RepeticionMinutos"].HeaderText = "REPETICIÓN (MIN)";
            dgvCronJobs.Columns["UltimaEjecucion"].HeaderText = "ÚLTIMA EJECUCIÓN";

            var colEditar = dgvCronJobs.Columns["Editar"];
            var colEliminar = dgvCronJobs.Columns["Eliminar"];

            if (colEditar is DataGridViewButtonColumn)
            {
                colEditar.Width = 30;
                colEditar.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                colEditar.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter; 
                colEditar.DisplayIndex = dgvCronJobs.Columns.Count - 1;
            }

            if (colEliminar is DataGridViewButtonColumn)
            {
                colEliminar.Width = 30;
                colEliminar.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                colEliminar.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                colEliminar.DisplayIndex = dgvCronJobs.Columns.Count - 1;
            }

            dgvCronJobs.RowTemplate.Height = 30;
        }

        //CONTROLAR VALOR DEL SELECTOR DE FRECUENCIA
        private void cmbFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbFrequency.SelectedItem != null)
            {
                string frecuenciaSeleccionada = cmbFrequency.SelectedItem.ToString();

                lblStartHour.Visible = true;
                dtpStartHour.Visible = true;
                lblMinutos.Visible = frecuenciaSeleccionada == "MINUTO";
                cmbMinutos.Visible = frecuenciaSeleccionada == "MINUTO";

                if (frecuenciaSeleccionada == "MINUTO")
                {
                    cmbMinutos.DataSource = new List<int> { 5, 10, 15, 30, 45 };
                    cmbMinutos.SelectedIndex = 0;
                }
            }
        }


        //BOTON DE SALIR DEL MODO EDICION
        private void btnSalirModoEdicionTareasProgramadas_Click(object sender, EventArgs e)
        {
            lblModoEdicionTareasProgramadas.Visible = false;
            btnSalirModoEdicionTareasProgramadas.Visible = false;

            cmbNameTask.Enabled = true;
            cmbNameTask.SelectedIndex = -1;
            cargarTareasDisponibles();

            cmbFrequency.SelectedIndex = -1;
            dtpStartHour.Visible = false;
            lblMinutos.Visible = false;
            lblStartHour.Visible = false;
            cmbMinutos.Visible = false;

            cmbFrequency.SelectedIndexChanged -= cmbFrequency_SelectedIndexChanged;
            cmbFrequency.SelectedIndexChanged += cmbFrequency_SelectedIndexChanged;

            actualizarDataGridView();
        }

        //BOTON PARA ABRIR EL PROGRAMADOR DE TAREAS DE WINDOWS
        private void btnAbririProgramadorTareas_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("taskschd.msc");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"No se pudo abrir el Programador de Tareas: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //FUNCION PARA AÑADIR LOS BOTONES DE EDITAR Y ELIMINAR EN EL DGV
        private void configurarBotonesDataGridView()
        {
            DataGridViewButtonColumn colEditar = new DataGridViewButtonColumn
            {
                Name = "Editar",
                HeaderText = "",
                Text = "✏️",
                UseColumnTextForButtonValue = true,
                Width = 30,
                DefaultCellStyle = new DataGridViewCellStyle
                {
                    Alignment = DataGridViewContentAlignment.MiddleCenter,
                    Padding = new Padding(0)
                }
            };

            DataGridViewButtonColumn colEliminar = new DataGridViewButtonColumn
            {
                Name = "Eliminar",
                HeaderText = "",
                Text = "🗑️",
                UseColumnTextForButtonValue = true,
                Width = 30,
                DefaultCellStyle = new DataGridViewCellStyle
                {
                    Alignment = DataGridViewContentAlignment.MiddleCenter,
                    Padding = new Padding(0)
                }
            };

            dgvCronJobs.Columns.Add(colEditar);
            dgvCronJobs.Columns.Add(colEliminar);
        }

        //EVENTO DE CLICK EN EL DGV PARA GESTIONAR EL BOTON DE EDITAR Y BORRAR
        private void dgvCronJobs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (dgvCronJobs.Columns[e.ColumnIndex].Name == "Editar")
                    {
                        string nombreTarea = dgvCronJobs.Rows[e.RowIndex].Cells["Tarea"].Value?.ToString();

                        if (!string.IsNullOrEmpty(nombreTarea))
                        {
                            editarRegistro(nombreTarea);
                        }
                    }
                    else if (dgvCronJobs.Columns[e.ColumnIndex].Name == "Eliminar")
                    {
                        string nombreTarea = dgvCronJobs.Rows[e.RowIndex].Cells["Tarea"].Value?.ToString();

                        if (!string.IsNullOrEmpty(nombreTarea))
                        {
                            eliminarRegistro(nombreTarea);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        //CONTROLAR VALOR DEL SELECTOR DE NOMBRE
        private void cmbNameTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tareaSeleccionada = cmbNameTask.SelectedItem?.ToString();
            if (!string.IsNullOrEmpty(tareaSeleccionada) && tareaSeleccionada != "No hay tareas disponibles")
            {
                Console.WriteLine($"Tarea seleccionada: {tareaSeleccionada}");
            }
        }


        //FUNCION PARA EDITAR EL REGISTRO Y TAREA PROGRAMADA
        private int? filaEnEdicion = null;
        private void editarRegistro(string nombreTarea)
        {
            if (!string.IsNullOrEmpty(nombreTarea))
            {
                lblModoEdicionTareasProgramadas.Visible = true;
                btnSalirModoEdicionTareasProgramadas.Visible = true;
                cmbFrequency.Enabled = true;
                btnGuardarConfTareasProgramadas.Enabled = true;

                var tareasDisponibles = cmbNameTask.DataSource as List<string> ?? new List<string>();

                var listaActualizada = new List<string>(tareasDisponibles);
                if (!listaActualizada.Contains(nombreTarea))
                {
                    listaActualizada.Insert(0, nombreTarea);
                }

                cmbNameTask.DataSource = null;
                cmbNameTask.DataSource = listaActualizada;
                cmbNameTask.SelectedItem = nombreTarea;
                cmbNameTask.Enabled = false;

                csCronJobService cronJobService = new csCronJobService();
                var cronJobs = cronJobService.obtenerTodosLosRegistros();
                var cronJob = cronJobs.FirstOrDefault(cj => cj.Tarea == nombreTarea);

                if (cronJob != null)
                {
                    cmbFrequency.SelectedItem = cronJob.Frecuencia;
                    dtpStartHour.Value = DateTime.Today.Add(cronJob.HoraEjecucion ?? TimeSpan.Zero);

                    if (cronJob.Frecuencia == "MINUTO")
                    {
                        lblMinutos.Visible = true;
                        cmbMinutos.Visible = true;
                        cmbMinutos.DataSource = new List<int> { 5, 10, 15, 30, 45 };
                        cmbMinutos.SelectedItem = cronJob.RepeticionMinutos ?? 5;
                    }
                    else
                    {
                        lblMinutos.Visible = false;
                        cmbMinutos.Visible = false;
                    }
                }

                //PINTAR DE ROJO
                foreach (DataGridViewRow row in dgvCronJobs.Rows)
                {
                    if (row.Cells["Tarea"].Value.ToString() == nombreTarea)
                    {
                        filaEnEdicion = row.Index;
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            cell.Style.ForeColor = Color.Red;
                        }
                        break;
                    }
                }
            }
        }

        //FUNCION PARA ELIMINAR EL REGISTRO Y TAREA PROGRAMADA
        private void eliminarRegistro(string nombreTarea)
        {
            var confirmacion = MessageBox.Show($"¿Estás seguro de que deseas eliminar la tarea '{nombreTarea}'?", "Confirmar eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmacion == DialogResult.Yes)
            {
                csCronJobService cronJobService = new csCronJobService();

                bool eliminadoBD = cronJobService.borrarRegistro(nombreTarea, csGlobal.conexionDB);
                cronJobService.borrarTareaProgramada(nombreTarea);

                if (eliminadoBD)
                {
                    MessageBox.Show($"Tarea '{nombreTarea}' eliminada correctamente.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"No se pudo eliminar la tarea '{nombreTarea}' de la base de datos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                actualizarDataGridView();
                cargarTareasDisponibles();
                lblModoEdicionTareasProgramadas.Visible = false;
                btnSalirModoEdicionTareasProgramadas.Visible = false;
            }
        }
    }
}