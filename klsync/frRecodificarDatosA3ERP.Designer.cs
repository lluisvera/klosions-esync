﻿namespace klsync
{
    partial class frRecodificarDatosA3ERP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpProveedores = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbIdDomBancaDestino = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbIdDombancaOrigen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbIdCuentaDestino = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCuentaDestino = new System.Windows.Forms.TextBox();
            this.tbIdCuentaOrigen = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCuentaOrigen = new System.Windows.Forms.TextBox();
            this.tbDatosProveedoresDestino = new System.Windows.Forms.TextBox();
            this.tbDatosProveedoresOrigen = new System.Windows.Forms.TextBox();
            this.btnCargarProveedorDestino = new System.Windows.Forms.Button();
            this.btnCargarProveedorOrigen = new System.Windows.Forms.Button();
            this.btnRecodificarProveedor = new System.Windows.Forms.Button();
            this.dgvDatosProveedorDestino = new System.Windows.Forms.DataGridView();
            this.dgvDatosProveedorOrigen = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cbProveedorDestino = new System.Windows.Forms.ComboBox();
            this.lblProveedorOrigen = new System.Windows.Forms.Label();
            this.cboxProveedorOrigen = new System.Windows.Forms.ComboBox();
            this.tpClientes = new System.Windows.Forms.TabPage();
            this.btnActualizarListaClientes = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnBorrarCliente = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbIdDombancaCliDestino = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbIdDomBancaCli = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbIdCuentaDestinoCli = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbCuentaDestinoCli = new System.Windows.Forms.TextBox();
            this.tbIdCuentaOrigenCli = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbCuentaOrigenCli = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.btnCargarDatosClienteDestino = new System.Windows.Forms.Button();
            this.btnCargarDatosClienteOrigen = new System.Windows.Forms.Button();
            this.btnRecodificarCliente = new System.Windows.Forms.Button();
            this.dgvDatosClienteDestino = new System.Windows.Forms.DataGridView();
            this.dgvDatosClienteOrigen = new System.Windows.Forms.DataGridView();
            this.label22 = new System.Windows.Forms.Label();
            this.cboxClienteDestino = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cboxClienteOrigen = new System.Windows.Forms.ComboBox();
            this.tbIddirentProveedorOrigen = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbIddirentProveedorDestino = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbIdDirentClienteDestino = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbIdDirentClienteOrigen = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tpProveedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosProveedorDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosProveedorOrigen)).BeginInit();
            this.tpClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosClienteDestino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosClienteOrigen)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpProveedores);
            this.tabControl1.Controls.Add(this.tpClientes);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1171, 719);
            this.tabControl1.TabIndex = 0;
            // 
            // tpProveedores
            // 
            this.tpProveedores.Controls.Add(this.tbIddirentProveedorDestino);
            this.tpProveedores.Controls.Add(this.label25);
            this.tpProveedores.Controls.Add(this.tbIddirentProveedorOrigen);
            this.tpProveedores.Controls.Add(this.label24);
            this.tpProveedores.Controls.Add(this.button2);
            this.tpProveedores.Controls.Add(this.label11);
            this.tpProveedores.Controls.Add(this.button1);
            this.tpProveedores.Controls.Add(this.label10);
            this.tpProveedores.Controls.Add(this.label8);
            this.tpProveedores.Controls.Add(this.label9);
            this.tpProveedores.Controls.Add(this.label7);
            this.tpProveedores.Controls.Add(this.label6);
            this.tpProveedores.Controls.Add(this.tbIdDomBancaDestino);
            this.tpProveedores.Controls.Add(this.label5);
            this.tpProveedores.Controls.Add(this.tbIdDombancaOrigen);
            this.tpProveedores.Controls.Add(this.label4);
            this.tpProveedores.Controls.Add(this.tbIdCuentaDestino);
            this.tpProveedores.Controls.Add(this.label3);
            this.tpProveedores.Controls.Add(this.tbCuentaDestino);
            this.tpProveedores.Controls.Add(this.tbIdCuentaOrigen);
            this.tpProveedores.Controls.Add(this.label2);
            this.tpProveedores.Controls.Add(this.tbCuentaOrigen);
            this.tpProveedores.Controls.Add(this.tbDatosProveedoresDestino);
            this.tpProveedores.Controls.Add(this.tbDatosProveedoresOrigen);
            this.tpProveedores.Controls.Add(this.btnCargarProveedorDestino);
            this.tpProveedores.Controls.Add(this.btnCargarProveedorOrigen);
            this.tpProveedores.Controls.Add(this.btnRecodificarProveedor);
            this.tpProveedores.Controls.Add(this.dgvDatosProveedorDestino);
            this.tpProveedores.Controls.Add(this.dgvDatosProveedorOrigen);
            this.tpProveedores.Controls.Add(this.label1);
            this.tpProveedores.Controls.Add(this.cbProveedorDestino);
            this.tpProveedores.Controls.Add(this.lblProveedorOrigen);
            this.tpProveedores.Controls.Add(this.cboxProveedorOrigen);
            this.tpProveedores.Location = new System.Drawing.Point(4, 22);
            this.tpProveedores.Name = "tpProveedores";
            this.tpProveedores.Padding = new System.Windows.Forms.Padding(3);
            this.tpProveedores.Size = new System.Drawing.Size(1163, 693);
            this.tpProveedores.TabIndex = 0;
            this.tpProveedores.Text = "Proveedores";
            this.tpProveedores.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(995, 608);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(128, 71);
            this.button2.TabIndex = 28;
            this.button2.Text = "Actualizar Lista Proveedores";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(204, 608);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 31);
            this.label11.TabIndex = 27;
            this.label11.Text = "6º";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(249, 608);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(218, 50);
            this.button1.TabIndex = 26;
            this.button1.Text = "Borrar Proveedor";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(204, 552);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 31);
            this.label10.TabIndex = 25;
            this.label10.Text = "5º";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1084, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 31);
            this.label8.TabIndex = 24;
            this.label8.Text = "4º";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1023, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 31);
            this.label9.TabIndex = 23;
            this.label9.Text = "3º";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(428, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 31);
            this.label7.TabIndex = 22;
            this.label7.Text = "2º";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(367, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 31);
            this.label6.TabIndex = 21;
            this.label6.Text = "1º";
            // 
            // tbIdDomBancaDestino
            // 
            this.tbIdDomBancaDestino.Location = new System.Drawing.Point(758, 118);
            this.tbIdDomBancaDestino.Name = "tbIdDomBancaDestino";
            this.tbIdDomBancaDestino.Size = new System.Drawing.Size(79, 20);
            this.tbIdDomBancaDestino.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(632, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "IDDOMBANCA";
            // 
            // tbIdDombancaOrigen
            // 
            this.tbIdDombancaOrigen.Location = new System.Drawing.Point(138, 118);
            this.tbIdDombancaOrigen.Name = "tbIdDombancaOrigen";
            this.tbIdDombancaOrigen.Size = new System.Drawing.Size(79, 20);
            this.tbIdDombancaOrigen.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "IDDOMBANCA";
            // 
            // tbIdCuentaDestino
            // 
            this.tbIdCuentaDestino.Location = new System.Drawing.Point(843, 92);
            this.tbIdCuentaDestino.Name = "tbIdCuentaDestino";
            this.tbIdCuentaDestino.Size = new System.Drawing.Size(79, 20);
            this.tbIdCuentaDestino.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(632, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "CUENTA";
            // 
            // tbCuentaDestino
            // 
            this.tbCuentaDestino.Location = new System.Drawing.Point(713, 92);
            this.tbCuentaDestino.Name = "tbCuentaDestino";
            this.tbCuentaDestino.Size = new System.Drawing.Size(124, 20);
            this.tbCuentaDestino.TabIndex = 14;
            // 
            // tbIdCuentaOrigen
            // 
            this.tbIdCuentaOrigen.Location = new System.Drawing.Point(223, 92);
            this.tbIdCuentaOrigen.Name = "tbIdCuentaOrigen";
            this.tbIdCuentaOrigen.Size = new System.Drawing.Size(79, 20);
            this.tbIdCuentaOrigen.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "CUENTA";
            // 
            // tbCuentaOrigen
            // 
            this.tbCuentaOrigen.Location = new System.Drawing.Point(93, 92);
            this.tbCuentaOrigen.Name = "tbCuentaOrigen";
            this.tbCuentaOrigen.Size = new System.Drawing.Size(124, 20);
            this.tbCuentaOrigen.TabIndex = 11;
            // 
            // tbDatosProveedoresDestino
            // 
            this.tbDatosProveedoresDestino.Location = new System.Drawing.Point(635, 168);
            this.tbDatosProveedoresDestino.Multiline = true;
            this.tbDatosProveedoresDestino.Name = "tbDatosProveedoresDestino";
            this.tbDatosProveedoresDestino.Size = new System.Drawing.Size(490, 100);
            this.tbDatosProveedoresDestino.TabIndex = 10;
            // 
            // tbDatosProveedoresOrigen
            // 
            this.tbDatosProveedoresOrigen.Location = new System.Drawing.Point(15, 168);
            this.tbDatosProveedoresOrigen.Multiline = true;
            this.tbDatosProveedoresOrigen.Name = "tbDatosProveedoresOrigen";
            this.tbDatosProveedoresOrigen.Size = new System.Drawing.Size(452, 100);
            this.tbDatosProveedoresOrigen.TabIndex = 9;
            // 
            // btnCargarProveedorDestino
            // 
            this.btnCargarProveedorDestino.Image = global::klsync.Properties.Resources.magnifier13;
            this.btnCargarProveedorDestino.Location = new System.Drawing.Point(1080, 57);
            this.btnCargarProveedorDestino.Name = "btnCargarProveedorDestino";
            this.btnCargarProveedorDestino.Size = new System.Drawing.Size(45, 47);
            this.btnCargarProveedorDestino.TabIndex = 8;
            this.btnCargarProveedorDestino.UseVisualStyleBackColor = true;
            this.btnCargarProveedorDestino.Click += new System.EventHandler(this.btnCargarProveedorDestino_Click);
            // 
            // btnCargarProveedorOrigen
            // 
            this.btnCargarProveedorOrigen.Image = global::klsync.Properties.Resources.magnifier13;
            this.btnCargarProveedorOrigen.Location = new System.Drawing.Point(423, 58);
            this.btnCargarProveedorOrigen.Name = "btnCargarProveedorOrigen";
            this.btnCargarProveedorOrigen.Size = new System.Drawing.Size(44, 45);
            this.btnCargarProveedorOrigen.TabIndex = 7;
            this.btnCargarProveedorOrigen.UseVisualStyleBackColor = true;
            this.btnCargarProveedorOrigen.Click += new System.EventHandler(this.btnCargarProveedorOrigen_Click);
            // 
            // btnRecodificarProveedor
            // 
            this.btnRecodificarProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecodificarProveedor.Location = new System.Drawing.Point(249, 552);
            this.btnRecodificarProveedor.Name = "btnRecodificarProveedor";
            this.btnRecodificarProveedor.Size = new System.Drawing.Size(218, 50);
            this.btnRecodificarProveedor.TabIndex = 6;
            this.btnRecodificarProveedor.Text = "Recodificar Proveedor >>>";
            this.btnRecodificarProveedor.UseVisualStyleBackColor = true;
            this.btnRecodificarProveedor.Click += new System.EventHandler(this.btnRecodificarProveedor_Click);
            // 
            // dgvDatosProveedorDestino
            // 
            this.dgvDatosProveedorDestino.AllowUserToAddRows = false;
            this.dgvDatosProveedorDestino.AllowUserToDeleteRows = false;
            this.dgvDatosProveedorDestino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDatosProveedorDestino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosProveedorDestino.Location = new System.Drawing.Point(635, 274);
            this.dgvDatosProveedorDestino.Name = "dgvDatosProveedorDestino";
            this.dgvDatosProveedorDestino.ReadOnly = true;
            this.dgvDatosProveedorDestino.Size = new System.Drawing.Size(490, 272);
            this.dgvDatosProveedorDestino.TabIndex = 5;
            // 
            // dgvDatosProveedorOrigen
            // 
            this.dgvDatosProveedorOrigen.AllowUserToAddRows = false;
            this.dgvDatosProveedorOrigen.AllowUserToDeleteRows = false;
            this.dgvDatosProveedorOrigen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosProveedorOrigen.Location = new System.Drawing.Point(15, 274);
            this.dgvDatosProveedorOrigen.Name = "dgvDatosProveedorOrigen";
            this.dgvDatosProveedorOrigen.ReadOnly = true;
            this.dgvDatosProveedorOrigen.Size = new System.Drawing.Size(452, 272);
            this.dgvDatosProveedorOrigen.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(631, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(355, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Proveedor al que se va a traspasar la información";
            // 
            // cbProveedorDestino
            // 
            this.cbProveedorDestino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbProveedorDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProveedorDestino.FormattingEnabled = true;
            this.cbProveedorDestino.Location = new System.Drawing.Point(635, 58);
            this.cbProveedorDestino.Name = "cbProveedorDestino";
            this.cbProveedorDestino.Size = new System.Drawing.Size(427, 28);
            this.cbProveedorDestino.TabIndex = 2;
            // 
            // lblProveedorOrigen
            // 
            this.lblProveedorOrigen.AutoSize = true;
            this.lblProveedorOrigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProveedorOrigen.Location = new System.Drawing.Point(11, 19);
            this.lblProveedorOrigen.Name = "lblProveedorOrigen";
            this.lblProveedorOrigen.Size = new System.Drawing.Size(241, 20);
            this.lblProveedorOrigen.TabIndex = 1;
            this.lblProveedorOrigen.Text = "Proveedor que se desea cambiar";
            // 
            // cboxProveedorOrigen
            // 
            this.cboxProveedorOrigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxProveedorOrigen.FormattingEnabled = true;
            this.cboxProveedorOrigen.Location = new System.Drawing.Point(15, 58);
            this.cboxProveedorOrigen.Name = "cboxProveedorOrigen";
            this.cboxProveedorOrigen.Size = new System.Drawing.Size(391, 28);
            this.cboxProveedorOrigen.TabIndex = 0;
            // 
            // tpClientes
            // 
            this.tpClientes.Controls.Add(this.tbIdDirentClienteDestino);
            this.tpClientes.Controls.Add(this.label26);
            this.tpClientes.Controls.Add(this.tbIdDirentClienteOrigen);
            this.tpClientes.Controls.Add(this.label27);
            this.tpClientes.Controls.Add(this.btnActualizarListaClientes);
            this.tpClientes.Controls.Add(this.label12);
            this.tpClientes.Controls.Add(this.btnBorrarCliente);
            this.tpClientes.Controls.Add(this.label13);
            this.tpClientes.Controls.Add(this.label14);
            this.tpClientes.Controls.Add(this.label15);
            this.tpClientes.Controls.Add(this.label16);
            this.tpClientes.Controls.Add(this.label17);
            this.tpClientes.Controls.Add(this.tbIdDombancaCliDestino);
            this.tpClientes.Controls.Add(this.label18);
            this.tpClientes.Controls.Add(this.tbIdDomBancaCli);
            this.tpClientes.Controls.Add(this.label19);
            this.tpClientes.Controls.Add(this.tbIdCuentaDestinoCli);
            this.tpClientes.Controls.Add(this.label20);
            this.tpClientes.Controls.Add(this.tbCuentaDestinoCli);
            this.tpClientes.Controls.Add(this.tbIdCuentaOrigenCli);
            this.tpClientes.Controls.Add(this.label21);
            this.tpClientes.Controls.Add(this.tbCuentaOrigenCli);
            this.tpClientes.Controls.Add(this.textBox7);
            this.tpClientes.Controls.Add(this.textBox8);
            this.tpClientes.Controls.Add(this.btnCargarDatosClienteDestino);
            this.tpClientes.Controls.Add(this.btnCargarDatosClienteOrigen);
            this.tpClientes.Controls.Add(this.btnRecodificarCliente);
            this.tpClientes.Controls.Add(this.dgvDatosClienteDestino);
            this.tpClientes.Controls.Add(this.dgvDatosClienteOrigen);
            this.tpClientes.Controls.Add(this.label22);
            this.tpClientes.Controls.Add(this.cboxClienteDestino);
            this.tpClientes.Controls.Add(this.label23);
            this.tpClientes.Controls.Add(this.cboxClienteOrigen);
            this.tpClientes.Location = new System.Drawing.Point(4, 22);
            this.tpClientes.Name = "tpClientes";
            this.tpClientes.Padding = new System.Windows.Forms.Padding(3);
            this.tpClientes.Size = new System.Drawing.Size(1163, 693);
            this.tpClientes.TabIndex = 1;
            this.tpClientes.Text = "Clientes";
            this.tpClientes.UseVisualStyleBackColor = true;
            // 
            // btnActualizarListaClientes
            // 
            this.btnActualizarListaClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizarListaClientes.Location = new System.Drawing.Point(1008, 605);
            this.btnActualizarListaClientes.Name = "btnActualizarListaClientes";
            this.btnActualizarListaClientes.Size = new System.Drawing.Size(128, 71);
            this.btnActualizarListaClientes.TabIndex = 57;
            this.btnActualizarListaClientes.Text = "Actualizar Lista Clientes";
            this.btnActualizarListaClientes.UseVisualStyleBackColor = true;
            this.btnActualizarListaClientes.Click += new System.EventHandler(this.btnActualizarListaClientes_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(217, 605);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 31);
            this.label12.TabIndex = 56;
            this.label12.Text = "6º";
            // 
            // btnBorrarCliente
            // 
            this.btnBorrarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrarCliente.Location = new System.Drawing.Point(262, 605);
            this.btnBorrarCliente.Name = "btnBorrarCliente";
            this.btnBorrarCliente.Size = new System.Drawing.Size(218, 50);
            this.btnBorrarCliente.TabIndex = 55;
            this.btnBorrarCliente.Text = "Borrar Cliente";
            this.btnBorrarCliente.UseVisualStyleBackColor = true;
            this.btnBorrarCliente.Click += new System.EventHandler(this.btnBorrarCliente_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(217, 549);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 31);
            this.label13.TabIndex = 54;
            this.label13.Text = "5º";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1097, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 31);
            this.label14.TabIndex = 53;
            this.label14.Text = "4º";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1036, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 31);
            this.label15.TabIndex = 52;
            this.label15.Text = "3º";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(441, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 31);
            this.label16.TabIndex = 51;
            this.label16.Text = "2º";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(380, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 31);
            this.label17.TabIndex = 50;
            this.label17.Text = "1º";
            // 
            // tbIdDombancaCliDestino
            // 
            this.tbIdDombancaCliDestino.Location = new System.Drawing.Point(771, 117);
            this.tbIdDombancaCliDestino.Name = "tbIdDombancaCliDestino";
            this.tbIdDombancaCliDestino.Size = new System.Drawing.Size(79, 20);
            this.tbIdDombancaCliDestino.TabIndex = 49;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(645, 122);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 13);
            this.label18.TabIndex = 48;
            this.label18.Text = "IDDOMBANCA";
            // 
            // tbIdDomBancaCli
            // 
            this.tbIdDomBancaCli.Location = new System.Drawing.Point(151, 115);
            this.tbIdDomBancaCli.Name = "tbIdDomBancaCli";
            this.tbIdDomBancaCli.Size = new System.Drawing.Size(79, 20);
            this.tbIdDomBancaCli.TabIndex = 47;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(25, 122);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "IDDOMBANCA";
            // 
            // tbIdCuentaDestinoCli
            // 
            this.tbIdCuentaDestinoCli.Location = new System.Drawing.Point(856, 89);
            this.tbIdCuentaDestinoCli.Name = "tbIdCuentaDestinoCli";
            this.tbIdCuentaDestinoCli.Size = new System.Drawing.Size(79, 20);
            this.tbIdCuentaDestinoCli.TabIndex = 45;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(645, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "CUENTA";
            // 
            // tbCuentaDestinoCli
            // 
            this.tbCuentaDestinoCli.Location = new System.Drawing.Point(726, 89);
            this.tbCuentaDestinoCli.Name = "tbCuentaDestinoCli";
            this.tbCuentaDestinoCli.Size = new System.Drawing.Size(124, 20);
            this.tbCuentaDestinoCli.TabIndex = 43;
            // 
            // tbIdCuentaOrigenCli
            // 
            this.tbIdCuentaOrigenCli.Location = new System.Drawing.Point(236, 89);
            this.tbIdCuentaOrigenCli.Name = "tbIdCuentaOrigenCli";
            this.tbIdCuentaOrigenCli.Size = new System.Drawing.Size(79, 20);
            this.tbIdCuentaOrigenCli.TabIndex = 42;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 92);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "CUENTA";
            // 
            // tbCuentaOrigenCli
            // 
            this.tbCuentaOrigenCli.Location = new System.Drawing.Point(106, 89);
            this.tbCuentaOrigenCli.Name = "tbCuentaOrigenCli";
            this.tbCuentaOrigenCli.Size = new System.Drawing.Size(124, 20);
            this.tbCuentaOrigenCli.TabIndex = 40;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(648, 165);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(490, 100);
            this.textBox7.TabIndex = 39;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(28, 165);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(452, 100);
            this.textBox8.TabIndex = 38;
            // 
            // btnCargarDatosClienteDestino
            // 
            this.btnCargarDatosClienteDestino.Image = global::klsync.Properties.Resources.magnifier13;
            this.btnCargarDatosClienteDestino.Location = new System.Drawing.Point(1093, 54);
            this.btnCargarDatosClienteDestino.Name = "btnCargarDatosClienteDestino";
            this.btnCargarDatosClienteDestino.Size = new System.Drawing.Size(45, 47);
            this.btnCargarDatosClienteDestino.TabIndex = 37;
            this.btnCargarDatosClienteDestino.UseVisualStyleBackColor = true;
            this.btnCargarDatosClienteDestino.Click += new System.EventHandler(this.btnCargarDatosClienteDestino_Click);
            // 
            // btnCargarDatosClienteOrigen
            // 
            this.btnCargarDatosClienteOrigen.Image = global::klsync.Properties.Resources.magnifier13;
            this.btnCargarDatosClienteOrigen.Location = new System.Drawing.Point(436, 55);
            this.btnCargarDatosClienteOrigen.Name = "btnCargarDatosClienteOrigen";
            this.btnCargarDatosClienteOrigen.Size = new System.Drawing.Size(44, 45);
            this.btnCargarDatosClienteOrigen.TabIndex = 36;
            this.btnCargarDatosClienteOrigen.UseVisualStyleBackColor = true;
            this.btnCargarDatosClienteOrigen.Click += new System.EventHandler(this.btnCargarDatosClienteOrigen_Click);
            // 
            // btnRecodificarCliente
            // 
            this.btnRecodificarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecodificarCliente.Location = new System.Drawing.Point(262, 549);
            this.btnRecodificarCliente.Name = "btnRecodificarCliente";
            this.btnRecodificarCliente.Size = new System.Drawing.Size(218, 50);
            this.btnRecodificarCliente.TabIndex = 35;
            this.btnRecodificarCliente.Text = "Recodificar Cliente >>>";
            this.btnRecodificarCliente.UseVisualStyleBackColor = true;
            this.btnRecodificarCliente.Click += new System.EventHandler(this.btnRecodificarCliente_Click);
            // 
            // dgvDatosClienteDestino
            // 
            this.dgvDatosClienteDestino.AllowUserToAddRows = false;
            this.dgvDatosClienteDestino.AllowUserToDeleteRows = false;
            this.dgvDatosClienteDestino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDatosClienteDestino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosClienteDestino.Location = new System.Drawing.Point(648, 271);
            this.dgvDatosClienteDestino.Name = "dgvDatosClienteDestino";
            this.dgvDatosClienteDestino.ReadOnly = true;
            this.dgvDatosClienteDestino.Size = new System.Drawing.Size(490, 272);
            this.dgvDatosClienteDestino.TabIndex = 34;
            // 
            // dgvDatosClienteOrigen
            // 
            this.dgvDatosClienteOrigen.AllowUserToAddRows = false;
            this.dgvDatosClienteOrigen.AllowUserToDeleteRows = false;
            this.dgvDatosClienteOrigen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosClienteOrigen.Location = new System.Drawing.Point(28, 271);
            this.dgvDatosClienteOrigen.Name = "dgvDatosClienteOrigen";
            this.dgvDatosClienteOrigen.ReadOnly = true;
            this.dgvDatosClienteOrigen.Size = new System.Drawing.Size(452, 272);
            this.dgvDatosClienteOrigen.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(644, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(332, 20);
            this.label22.TabIndex = 32;
            this.label22.Text = "Cliente al que se va a traspasar la información";
            // 
            // cboxClienteDestino
            // 
            this.cboxClienteDestino.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxClienteDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxClienteDestino.FormattingEnabled = true;
            this.cboxClienteDestino.Location = new System.Drawing.Point(648, 55);
            this.cboxClienteDestino.Name = "cboxClienteDestino";
            this.cboxClienteDestino.Size = new System.Drawing.Size(427, 28);
            this.cboxClienteDestino.TabIndex = 31;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(24, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(218, 20);
            this.label23.TabIndex = 30;
            this.label23.Text = "Cliente que se desea cambiar";
            // 
            // cboxClienteOrigen
            // 
            this.cboxClienteOrigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxClienteOrigen.FormattingEnabled = true;
            this.cboxClienteOrigen.Location = new System.Drawing.Point(28, 55);
            this.cboxClienteOrigen.Name = "cboxClienteOrigen";
            this.cboxClienteOrigen.Size = new System.Drawing.Size(391, 28);
            this.cboxClienteOrigen.TabIndex = 29;
            // 
            // tbIddirentProveedorOrigen
            // 
            this.tbIddirentProveedorOrigen.Location = new System.Drawing.Point(138, 142);
            this.tbIddirentProveedorOrigen.Name = "tbIddirentProveedorOrigen";
            this.tbIddirentProveedorOrigen.Size = new System.Drawing.Size(79, 20);
            this.tbIddirentProveedorOrigen.TabIndex = 30;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "IDDIRENT";
            // 
            // tbIddirentProveedorDestino
            // 
            this.tbIddirentProveedorDestino.Location = new System.Drawing.Point(758, 144);
            this.tbIddirentProveedorDestino.Name = "tbIddirentProveedorDestino";
            this.tbIddirentProveedorDestino.Size = new System.Drawing.Size(79, 20);
            this.tbIddirentProveedorDestino.TabIndex = 32;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(632, 147);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 13);
            this.label25.TabIndex = 31;
            this.label25.Text = "IDDIRENT";
            // 
            // tbIdDirentClienteDestino
            // 
            this.tbIdDirentClienteDestino.Location = new System.Drawing.Point(771, 143);
            this.tbIdDirentClienteDestino.Name = "tbIdDirentClienteDestino";
            this.tbIdDirentClienteDestino.Size = new System.Drawing.Size(79, 20);
            this.tbIdDirentClienteDestino.TabIndex = 61;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(645, 146);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 13);
            this.label26.TabIndex = 60;
            this.label26.Text = "IDDIRENT";
            // 
            // tbIdDirentClienteOrigen
            // 
            this.tbIdDirentClienteOrigen.Location = new System.Drawing.Point(151, 141);
            this.tbIdDirentClienteOrigen.Name = "tbIdDirentClienteOrigen";
            this.tbIdDirentClienteOrigen.Size = new System.Drawing.Size(79, 20);
            this.tbIdDirentClienteOrigen.TabIndex = 59;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 144);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 13);
            this.label27.TabIndex = 58;
            this.label27.Text = "IDDIRENT";
            // 
            // frRecodificarDatosA3ERP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1195, 778);
            this.Controls.Add(this.tabControl1);
            this.Name = "frRecodificarDatosA3ERP";
            this.Text = "frRecodificarDatosA3ERP";
            this.Load += new System.EventHandler(this.frRecodificarDatosA3ERP_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpProveedores.ResumeLayout(false);
            this.tpProveedores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosProveedorDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosProveedorOrigen)).EndInit();
            this.tpClientes.ResumeLayout(false);
            this.tpClientes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosClienteDestino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosClienteOrigen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpProveedores;
        private System.Windows.Forms.TabPage tpClientes;
        private System.Windows.Forms.TextBox tbDatosProveedoresDestino;
        private System.Windows.Forms.TextBox tbDatosProveedoresOrigen;
        private System.Windows.Forms.Button btnRecodificarProveedor;
        private System.Windows.Forms.DataGridView dgvDatosProveedorDestino;
        private System.Windows.Forms.DataGridView dgvDatosProveedorOrigen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbProveedorDestino;
        private System.Windows.Forms.Label lblProveedorOrigen;
        private System.Windows.Forms.ComboBox cboxProveedorOrigen;
        private System.Windows.Forms.Button btnCargarProveedorDestino;
        private System.Windows.Forms.Button btnCargarProveedorOrigen;
        private System.Windows.Forms.TextBox tbCuentaOrigen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIdCuentaDestino;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCuentaDestino;
        private System.Windows.Forms.TextBox tbIdCuentaOrigen;
        private System.Windows.Forms.TextBox tbIdDomBancaDestino;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbIdDombancaOrigen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnActualizarListaClientes;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnBorrarCliente;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbIdDombancaCliDestino;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbIdDomBancaCli;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbIdCuentaDestinoCli;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbCuentaDestinoCli;
        private System.Windows.Forms.TextBox tbIdCuentaOrigenCli;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbCuentaOrigenCli;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button btnCargarDatosClienteDestino;
        private System.Windows.Forms.Button btnCargarDatosClienteOrigen;
        private System.Windows.Forms.Button btnRecodificarCliente;
        private System.Windows.Forms.DataGridView dgvDatosClienteDestino;
        private System.Windows.Forms.DataGridView dgvDatosClienteOrigen;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cboxClienteDestino;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cboxClienteOrigen;
        private System.Windows.Forms.TextBox tbIddirentProveedorDestino;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbIddirentProveedorOrigen;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbIdDirentClienteDestino;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbIdDirentClienteOrigen;
        private System.Windows.Forms.Label label27;
    }
}