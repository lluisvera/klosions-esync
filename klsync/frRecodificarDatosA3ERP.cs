﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frRecodificarDatosA3ERP : Form
    {

        private static frRecodificarDatosA3ERP m_FormDefInstance;
        public static frRecodificarDatosA3ERP DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRecodificarDatosA3ERP();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frRecodificarDatosA3ERP()
        {
            InitializeComponent();
        }

        private void frRecodificarDatosA3ERP_Load(object sender, EventArgs e)
        {
            cargarProveedores();
            cargarClientes();
        }

        private void cargarProveedores()
        {

            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("SELECT CODPRO, LTRIM(CODPRO) + '-' + NOMPRO AS NOMPRO FROM PROVEED ORDER BY CODPRO", dataConnection);
            DataTable to = new DataTable();
            DataTable td = new DataTable();
            a.Fill(to);
            a.Fill(td);
            dataConnection.Close();

            cboxProveedorOrigen.DataSource = to;
            cboxProveedorOrigen.ValueMember = "CODPRO";
            cboxProveedorOrigen.DisplayMember = "NOMPRO";

            cbProveedorDestino.DataSource = td;
            cbProveedorDestino.ValueMember = "CODPRO";
            cbProveedorDestino.DisplayMember = "NOMPRO";
        }

        private void cargarClientes()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter("SELECT CODCLI, LTRIM(CODCLI) + '-' + NOMCLI AS NOMCLI FROM CLIENTES ORDER BY CODCLI", dataConnection);
            DataTable to = new DataTable();
            DataTable td = new DataTable();
            a.Fill(to);
            a.Fill(td);
            dataConnection.Close();

            cboxClienteOrigen.DataSource = to;
            cboxClienteOrigen.ValueMember = "CODCLI";
            cboxClienteOrigen.DisplayMember = "NOMCLI";

            cboxClienteDestino.DataSource = td;
            cboxClienteDestino.ValueMember = "CODCLI";
            cboxClienteDestino.DisplayMember = "NOMCLI";
        
        }

        private void cargarDocsProveedor(string proveedor, DataGridView dgv, TextBox cuenta, TextBox idcuenta,TextBox idDomBanca,TextBox idDireccion)
        {

            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            

            string query = "SELECT 'OFERTA' AS TIPODOC, NUMDOC, FECHA FROM CABEOFEC WHERE CODPRO='" + proveedor + "'" +
                            " UNION SELECT 'PEDIDO' AS TIPODOC, NUMDOC, FECHA FROM CABEPEDC WHERE CODPRO='" + proveedor + "'" +
                            " UNION SELECT 'ALBARAN' AS TIPODOC, NUMDOC, FECHA FROM CABEALBC WHERE CODPRO='" + proveedor + "'" +
                            " UNION SELECT 'FACTURA' AS TIPODOC, NUMDOC, FECHA FROM CABEFACC WHERE CODPRO='" + proveedor + "'" +
                            " UNION SELECT 'CARTERA' AS TIPODOC, NUMDOC, FECHA FROM CARTERA WHERE CODPRO='" + proveedor + "'";

            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(query, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            dataConnection.Close();
            
            dgv.DataSource = t;

            string queryDatosContabilidad = "SELECT dbo.PROVEED.CODPRO, dbo.PROVEED.NOMPRO, dbo.PROVEED.CUENTA, dbo.CUENTAS.IDCUENTA " +
                                            " FROM dbo.PROVEED INNER JOIN " +
                                            " dbo.CUENTAS ON dbo.PROVEED.CUENTA = dbo.CUENTAS.CUENTA " +
                                            " WHERE  PLACON='NPGC' AND  (dbo.PROVEED.CODPRO = '" + proveedor + "')";
            
            dataConnection.Open();
            SqlDataAdapter dc = new SqlDataAdapter(queryDatosContabilidad, dataConnection);
            DataTable tdc = new DataTable();
            dc.Fill(tdc);
            dataConnection.Close();

            cuenta.Text = tdc.Rows[0][2].ToString();
            idcuenta.Text = tdc.Rows[0][3].ToString();

            string queryDirecciones = "select iddirent from __DIRENTPRO WHERE CODPRO ='" + proveedor + "'";

            dataConnection.Open();
            SqlDataAdapter adaptDirecciones = new SqlDataAdapter(queryDirecciones, dataConnection);
            DataTable tablaDirecciones = new DataTable();
            adaptDirecciones.Fill(tablaDirecciones);
            dataConnection.Close();

            idDireccion.Text = tablaDirecciones.Rows[0][0].ToString().Replace(",0000","");



            string queryDatosDomBancarias = "SELECT CODCLI, CODPRO, IDDOMBANCA, NOMBAN, NUMCUENTA, NUMDOM, POBBAN, TITULAR " +
                " FROM DOMBANCA WHERE CODPRO IN ('" + proveedor + "')";

            dataConnection.Open();
            SqlDataAdapter datosBanc = new SqlDataAdapter(queryDatosDomBancarias, dataConnection);
            DataTable domiciliaciones = new DataTable();
            datosBanc.Fill(domiciliaciones);
            dataConnection.Close();

            if (domiciliaciones.Rows.Count > 0)
            {
                idDomBanca.Text = domiciliaciones.Rows[0][2].ToString();
            }
            
        
        }



        private void cargarDocsCliente(string cliente, DataGridView dgv, TextBox cuenta, TextBox idcuenta, TextBox idDomBanca, TextBox idDireccion)
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;



            string query = "SELECT 'OFERTA' AS TIPODOC, NUMDOC, FECHA FROM CABEOFEV WHERE CODCLI='" + cliente + "'" +
                            " UNION SELECT 'PEDIDO' AS TIPODOC, NUMDOC, FECHA FROM CABEPEDV WHERE CODCLI='" + cliente + "'" +
                            " UNION SELECT 'ALBARAN' AS TIPODOC, NUMDOC, FECHA FROM CABEALBV WHERE CODCLI='" + cliente + "'" +
                            " UNION SELECT 'FACTURA' AS TIPODOC, NUMDOC, FECHA FROM CABEFACV WHERE CODCLI='" + cliente + "'" +
                            " UNION SELECT 'CARTERA' AS TIPODOC, NUMDOC, FECHA FROM CARTERA WHERE CODCLI='" + cliente + "'";

            dataConnection.Open();
            SqlDataAdapter a = new SqlDataAdapter(query, dataConnection);
            DataTable t = new DataTable();
            a.Fill(t);
            dataConnection.Close();

            dgv.DataSource = t;

            string queryDatosContabilidad = "SELECT dbo.CLIENTES.CODCLI, dbo.CLIENTES.NOMCLI, dbo.CLIENTES.CUENTA, dbo.CUENTAS.IDCUENTA " +
                                            " FROM dbo.CLIENTES INNER JOIN " +
                                            " dbo.CUENTAS ON dbo.CLIENTES.CUENTA = dbo.CUENTAS.CUENTA " +
                                            " WHERE  PLACON='NPGC' AND  (dbo.CLIENTES.CODCLI = '" + cliente + "')";

            dataConnection.Open();
            SqlDataAdapter dc = new SqlDataAdapter(queryDatosContabilidad, dataConnection);
            DataTable tdc = new DataTable();
            dc.Fill(tdc);
            dataConnection.Close();

            cuenta.Text = tdc.Rows[0][2].ToString();
            idcuenta.Text = tdc.Rows[0][3].ToString();

            //string queryDirecciones = "select iddirent from __DIRENTPRO WHERE CODPRO ='" + cliente + "')";
            string queryDirecciones = "select iddirent from DIRENT WHERE CODCLI ='" + cliente + "')";

            dataConnection.Open();
            SqlDataAdapter adaptDirecciones = new SqlDataAdapter(queryDatosContabilidad, dataConnection);
            DataTable tablaDirecciones = new DataTable();
            adaptDirecciones.Fill(tablaDirecciones);
            dataConnection.Close();

            idDireccion.Text = tablaDirecciones.Rows[0][0].ToString();
            
            string queryDatosDomBancarias = "SELECT CODCLI, CODPRO, IDDOMBANCA, NOMBAN, NUMCUENTA, NUMDOM, POBBAN, TITULAR " +
                " FROM DOMBANCA WHERE CODCLI IN ('" + cliente + "')";

            dataConnection.Open();
            SqlDataAdapter datosBanc = new SqlDataAdapter(queryDatosDomBancarias, dataConnection);
            DataTable domiciliaciones = new DataTable();
            datosBanc.Fill(domiciliaciones);
            dataConnection.Close();

            if (domiciliaciones.Rows.Count > 0)
            {
                idDomBanca.Text = domiciliaciones.Rows[0][2].ToString();
            }
            
        
        }

        private void btnCargarProveedorOrigen_Click(object sender, EventArgs e)
        {
            cargarDocsProveedor(cboxProveedorOrigen.SelectedValue.ToString(), dgvDatosProveedorOrigen,tbCuentaOrigen,tbIdCuentaOrigen,tbIdDombancaOrigen,tbIddirentProveedorOrigen);
        }

        private void btnCargarProveedorDestino_Click(object sender, EventArgs e)
        {
            cargarDocsProveedor(cbProveedorDestino.SelectedValue.ToString(), dgvDatosProveedorDestino,tbCuentaDestino,tbIdCuentaDestino,tbIdDomBancaDestino,tbIddirentProveedorDestino);
        }
        private void cargarDatosProveedores()
        {
            cargarDocsProveedor(cboxProveedorOrigen.SelectedValue.ToString(), dgvDatosProveedorOrigen,tbCuentaOrigen,tbIdCuentaOrigen,tbIdDombancaOrigen,tbIddirentProveedorOrigen);
            cargarDocsProveedor(cbProveedorDestino.SelectedValue.ToString(), dgvDatosProveedorDestino,tbCuentaDestino,tbIdCuentaDestino,tbIdDomBancaDestino,tbIddirentProveedorDestino);
        }

        private void btnRecodificarProveedor_Click(object sender, EventArgs e)
        {
            recodificarProveedor(cboxProveedorOrigen.SelectedValue.ToString(), cbProveedorDestino.SelectedValue.ToString(), tbIdCuentaOrigen.Text, tbIdCuentaDestino.Text,tbIdDombancaOrigen.Text,tbIdDomBancaDestino.Text,tbIddirentProveedorOrigen.Text,tbIddirentProveedorDestino.Text);
            cargarDatosProveedores();
        }

        private void recodificarProveedor(string proveedorOrigen, string proveedorDestino, string idCuentaOrigen, string idCuentaDestino,string idDombancaOrigen, string idDomBancaDestino, string idDirentProvOrigen, string idDirentProvDestino)
        {
            csSqlConnects sql = new csSqlConnects();

            try
            {
                sql.actualizarCampo("UPDATE CABEFACC SET CODPRO='" + proveedorDestino + "', CODPROFAC ='" + proveedorDestino + "', IDDIRENT=" + idDirentProvDestino.Replace(" ", "") + "  WHERE CODPRO='" + proveedorOrigen + "'");
                sql.actualizarCampo("UPDATE CARTERA SET CODPRO='" + proveedorDestino + "' WHERE CODPRO='" + proveedorOrigen + "'");
                if (idDomBancaDestino != "")
                {
                    sql.actualizarCampo("UPDATE CARTERA SET IDDOMBANCA=" + idDomBancaDestino.Replace(",0000", "") + " WHERE IDDOMBANCA=" + idDombancaOrigen.Replace(",0000", ""));
                }
                //sql.actualizarCampo("UPDATE DOMBANCA SET CODPRO='" + proveedorDestino + "', idcom WHERE CODPRO='" + proveedorOrigen + "'");
                sql.actualizarCampo("UPDATE __ASIENTOS SET IDCUENTA='" + idCuentaDestino + "' WHERE IDCUENTA='" + idCuentaOrigen + "'");
                sql.actualizarCampo("UPDATE __AGRUPACIONES SET CODPRO='" + proveedorDestino + "' WHERE CODPRO='" + proveedorOrigen + "'");
                sql.actualizarCampo("UPDATE ARTICULO SET CODPRO='" + proveedorDestino + "' WHERE CODPRO='" + proveedorOrigen + "'");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void recodificarCliente(string clienteOrigen, string clienteDestino, string idCuentaOrigen, string idCuentaDestino, string idDombancaOrigen, string idDomBancaDestino)
        {
            csSqlConnects sql = new csSqlConnects();

            try
            {
                sql.actualizarCampo("UPDATE CABEFACV SET CODCLI='" + clienteDestino + "' WHERE CODCLI='" + clienteOrigen + "'");
                sql.actualizarCampo("UPDATE CARTERA SET CODCLI='" + clienteDestino + "' WHERE CODCLI='" + clienteOrigen + "'");
                if (idDomBancaDestino!="")
                {
                sql.actualizarCampo("UPDATE CARTERA SET IDDOMBANCA=" + idDomBancaDestino.Replace(",0000", "") + " WHERE IDDOMBANCA=" + idDombancaOrigen.Replace(",0000", ""));
                }
                //sql.actualizarCampo("UPDATE DOMBANCA SET CODPRO='" + proveedorDestino + "', idcom WHERE CODPRO='" + proveedorOrigen + "'");
                sql.actualizarCampo("UPDATE __ASIENTOS SET IDCUENTA='" + idCuentaDestino + "' WHERE IDCUENTA='" + idCuentaOrigen + "'");
                sql.actualizarCampo("UPDATE __AGRUPACIONES SET CODCLI='" + clienteDestino + "' WHERE CODCLI='" + clienteOrigen + "'");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            csa3erpTercero a3tercero = new csa3erpTercero();
            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            a3tercero.borrarProveedorA3(cboxProveedorOrigen.SelectedValue.ToString());
            a3erp.cerrarEnlace();
            cargarProveedores();
        }

        private void borrarCliente()
        {
            csa3erpTercero a3tercero = new csa3erpTercero();
            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            a3tercero.borrarClienteA3(cboxClienteOrigen.SelectedValue.ToString());
            a3erp.cerrarEnlace();
            cargarClientes();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cargarProveedores();
        }

        private void cargarDatosClienteOrigen()
        {
            cargarDocsCliente(cboxClienteOrigen.SelectedValue.ToString(), dgvDatosClienteOrigen, tbCuentaOrigenCli, tbIdCuentaOrigenCli, tbIdDomBancaCli,tbIdDirentClienteOrigen);
        }

        private void cargarDatosClienteDestino()
        {
            cargarDocsCliente(cboxClienteDestino.SelectedValue.ToString(), dgvDatosClienteDestino, tbCuentaDestinoCli, tbIdCuentaDestinoCli, tbIdDombancaCliDestino,tbIdDirentClienteDestino);
        
        }

        private void btnCargarDatosClienteOrigen_Click(object sender, EventArgs e)
        {
            cargarDatosClienteOrigen();
        }

        private void btnCargarDatosClienteDestino_Click(object sender, EventArgs e)
        {
            cargarDatosClienteDestino();
        }

        private void btnActualizarListaClientes_Click(object sender, EventArgs e)
        {
            cargarClientes();
        }

        private void btnRecodificarCliente_Click(object sender, EventArgs e)
        {
            recodificarCliente(cboxClienteOrigen.SelectedValue.ToString(), cboxClienteDestino.SelectedValue.ToString(), tbIdCuentaOrigenCli.Text, tbIdCuentaDestinoCli.Text,tbIdDombancaOrigen.Text,tbIdDomBancaDestino.Text);
            recodificarProveedor(cboxProveedorOrigen.SelectedValue.ToString(), cbProveedorDestino.SelectedValue.ToString(), tbIdCuentaOrigen.Text, tbIdCuentaDestino.Text,tbIdDombancaOrigen.Text,tbIdDombancaCliDestino.Text,tbIddirentProveedorOrigen.Text,tbIddirentProveedorDestino.Text);
            cargarDatosClienteOrigen();
            cargarDatosClienteDestino();

        }

        private void btnBorrarCliente_Click(object sender, EventArgs e)
        {
            borrarCliente();
        }
    }
}
