﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace klsync
{
    class csSqlScripts
    {
        public string selectDireccionesPS(bool all)
        {
            if (all == true)
            {
                return "  SELECT lastname, id_address, id_customer, postcode, city, address1 " +
                        " FROM ps_address " +
                        " WHERE id_customer IN" +
                        "   (Select  id_customer " +
                        "   From ps_address " +
                        "   Group By ps_address.id_customer " +
                        "   HAVING COUNT(id_customer)>1)";
            }
            else
            {
                return "SELECT lastname, id_address, id_customer FROM ps_address GROUP BY id_customer HAVING COUNT(id_customer) = 1";
            }
        }



        public string stockPSTallasA3SIPSI(string idProduct=null)
        {
            string anexoProduct = "";
            if (!string.IsNullOrEmpty(idProduct))
            {//" where ps_product.reference = 'HE3127' " +
                anexoProduct = " where ps_product.id_product=" + idProduct;

            }

            string consulta = "SELECT ps_stock_available.id_stock_available, " +
                " ps_product.reference, ps_product_attribute.reference as referenceAttr, " +
                " ps_product.id_product, " +
                " ps_product_attribute.id_product_attribute, " +
                " count(ps_product_attribute.id_product_attribute) AS atributos, " +
                " max(ps_product_attribute_combination.id_attribute) AS idAtributo1, " +
                " (select name from ps_attribute_lang where ps_attribute_lang.id_attribute = Min(ps_product_attribute_combination.id_attribute) and ps_attribute_lang.id_lang = 1) as nombreAtr1, " +
                " min(ps_product_attribute_combination.id_attribute) AS idAtributo2, " +
                " (select name from ps_attribute_lang where ps_attribute_lang.id_attribute = Max(ps_product_attribute_combination.id_attribute) and ps_attribute_lang.id_lang = 1) as nombreAtr2, " +
                " ps_stock_available.quantity as STOCK_PS, ps_stock_available.out_of_stock as permitir_compras_PS" +
                " FROM ps_product_attribute_combination " +
                " INNER JOIN ps_product_attribute ON ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute " +
                " INNER JOIN ps_product on ps_product.id_product = ps_product_attribute.id_product " +
                " INNER JOIN ps_attribute_lang on ps_attribute_lang.id_attribute = ps_product_attribute_combination.id_attribute " +
                " LEFT JOIN ps_stock_available on ps_stock_available.id_product = ps_product.id_product and ps_stock_available.id_product_attribute = ps_product_attribute.id_product_attribute " +
                anexoProduct + 
                " GROUP BY ps_product_attribute.id_product_attribute, ps_product_attribute.reference";

            return consulta;
        }

        public string stockPSTallasA3NOPSSI()
        {
            string consulta = "";

            return consulta;
        
        }


        public string stockA3TallasA3SIPSSI(string idProduct=null)
        {
            string almacenes = csUtilidades.getAlmacenWhereIn();
            string anexo="";
            if (csGlobal.activarMonotallas == "Y")
            {
                anexo = " AND dbo.ARTICULO.KLS_MONOATRIBUTO IS NULL ";
            
            }

            string anexoArticulo = "";
            if (!string.IsNullOrEmpty(idProduct))
            {
                anexoArticulo= " AND dbo.ARTICULO.KLS_ID_SHOP=" + idProduct + " ";
            }
            string consulta;
            //22-04-2021 RAUL - Se modifica el IF para que entre en el caso de que este activadas las monotallas en lugar de en funcion del nombre de la conexión
            //if (csGlobal.conexionDB.Contains("INDUSNOW") || csGlobal.conexionDB.Contains("CADCANARIAS"))
            if (csGlobal.activarMonotallas == "Y")
            {
                 consulta = "SELECT dbo.STOCKALM.CODALM, " +
                            " LTRIM(dbo.STOCKALM.CODART) AS CODART, " +
                            " LTRIM(dbo.STOCKALM.CODFAMTALLAH) AS CODFAMTALLAH, " +
                            " LTRIM(dbo.STOCKALM.CODFAMTALLAV) AS CODFAMTALLAV, " +
                            " LTRIM(dbo.STOCKALM.CODTALLAH) AS CODTALLAH, " +
                            " LTRIM(dbo.STOCKALM.CODTALLAV) AS CODTALLAV, " +
                            " dbo.TALLAS.ID, " +
                            " TALLAS_1.ID AS ID2, " +
                            " dbo.STOCKALM.UNIDADES AS STOCK_A3, " +
                            " CASE WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='SI') THEN 1 WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='NO') THEN 0 ELSE 2 END  AS PERMITIR_COMPRAS, " +
                            //19-04-16 Añadido por franco para Indusnow (gestion de stock)
                            " FAMILIATALLA_1.KLS_MONOTALLA AS FAMILIAMONOTALLAV,"+
                            " FAMILIATALLA.KLS_MONOTALLA AS FAMILIAMONOTALLAH"+
                            //hasta aquí
                            " FROM " +
                            " dbo.STOCKALM LEFT OUTER JOIN " +
                            " dbo.TALLAS ON (dbo.STOCKALM.CODTALLAH = dbo.TALLAS.CODTALLA OR " +
                            " dbo.STOCKALM.CODTALLAH IS NULL AND dbo.TALLAS.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA OR " +
                            " dbo.STOCKALM.CODFAMTALLAH IS NULL AND dbo.TALLAS.CODFAMTALLA IS NULL) LEFT OUTER JOIN " +
                            " dbo.ARTICULO LEFT OUTER JOIN " +
                            " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAH = FAMILIATALLA_1.CODFAMTALLA LEFT OUTER JOIN " +
                            " dbo.FAMILIATALLA ON dbo.ARTICULO.CODFAMTALLAV = dbo.FAMILIATALLA.CODFAMTALLA ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART LEFT OUTER JOIN " +
                            " dbo.TALLAS AS TALLAS_1 ON (dbo.STOCKALM.CODTALLAV = TALLAS_1.CODTALLA OR " +
                            " dbo.STOCKALM.CODTALLAV IS NULL AND TALLAS_1.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAV = TALLAS_1.CODFAMTALLA OR " +
                            " dbo.STOCKALM.CODFAMTALLAV IS NULL AND TALLAS_1.CODFAMTALLA IS NULL) " +
                            " WHERE " +
                            " (LTRIM(dbo.STOCKALM.CODALM) IN (" + almacenes + ")) AND (dbo.STOCKALM.UNIDADES > 0) AND (CASE WHEN dbo.ARTICULO.KLS_MONOATRIBUTO IS NULL AND  " +
                            " FAMILIATALLA_1.KLS_MONOTALLA IS NULL AND FAMILIATALLA.KLS_MONOTALLA IS NULL THEN NULL ELSE 'T' END IS NULL) " + anexoArticulo + 
                            " UNION " + //  CONSULTA UNION
                            " SELECT " +
                            "  dbo.STOCKALM.CODALM, LTRIM(dbo.STOCKALM.CODART) AS CODART, LTRIM(dbo.STOCKALM.CODFAMTALLAH) AS CODFAMTALLAH,  " +
                            " LTRIM(dbo.STOCKALM.CODFAMTALLAV) AS CODFAMTALLAV, LTRIM(dbo.STOCKALM.CODTALLAH) AS CODTALLAH, LTRIM(dbo.STOCKALM.CODTALLAV)  " +
                            " AS CODTALLAV, " +
                            //" dbo.TALLAS.ID, " +
                            //" TALLAS_1.ID AS ID2, " +
                            " CASE WHEN dbo.FAMILIATALLA.KLS_MONOTALLA IS NULL THEN TALLAS.ID WHEN dbo.FAMILIATALLA.KLS_MONOTALLA = 'F' THEN TALLAS.ID ELSE TALLAS_1.ID END AS ID, " +
                            "  CASE WHEN FAMILIATALLA_1.KLS_MONOTALLA IS NULL THEN TALLAS_1.ID WHEN FAMILIATALLA_1.KLS_MONOTALLA = 'F' THEN TALLAS_1.ID ELSE TALLAS.ID END AS ID2, " +

                            " dbo.STOCKALM.UNIDADES AS STOCK_A3, CASE WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'SI')  " +
                            " THEN 1 WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'NO') THEN 0 ELSE 2 END AS PERMITIR_COMPRAS, " +
                            //19-04-16 Añadido por franco para Indusnow (gestion de st0ck
                            " FAMILIATALLA_1.KLS_MONOTALLA AS FAMILIAMONOTALLAV," +
                            " FAMILIATALLA.KLS_MONOTALLA AS FAMILIAMONOTALLAH" +
                            //hasta aquí
                            " FROM            dbo.TALLAS AS TALLAS_1 RIGHT OUTER JOIN " +
                            " dbo.ARTICULO LEFT OUTER JOIN " +
                            " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAV = FAMILIATALLA_1.CODFAMTALLA RIGHT OUTER JOIN " +
                            " dbo.STOCKALM LEFT OUTER JOIN " +
                            " dbo.TALLAS ON (dbo.STOCKALM.CODTALLAH = dbo.TALLAS.CODTALLA OR " +
                            " dbo.STOCKALM.CODTALLAH IS NULL AND dbo.TALLAS.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA OR " +
                            " dbo.STOCKALM.CODFAMTALLAH IS NULL AND dbo.TALLAS.CODFAMTALLA IS NULL) ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART ON  " +
                            " (dbo.STOCKALM.CODTALLAV = TALLAS_1.CODTALLA OR " +
                            " dbo.STOCKALM.CODTALLAV IS NULL AND TALLAS_1.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAV = TALLAS_1.CODFAMTALLA OR " +
                            " dbo.STOCKALM.CODFAMTALLAV IS NULL AND TALLAS_1.CODFAMTALLA IS NULL) LEFT OUTER JOIN " +
                            " dbo.FAMILIATALLA ON dbo.ARTICULO.CODFAMTALLAH = dbo.FAMILIATALLA.CODFAMTALLA " +
                            " WHERE " +

                            " (LTRIM(dbo.STOCKALM.CODALM) IN (" + almacenes + ")) AND (dbo.STOCKALM.UNIDADES > 0) AND (dbo.ARTICULO.KLS_ID_SHOP > 0) AND " +
                            " (dbo.FAMILIATALLA.KLS_MONOTALLA IS NULL) AND (FAMILIATALLA_1.KLS_MONOTALLA IS NOT NULL) AND (dbo.ARTICULO.KLS_MONOATRIBUTO IS NULL) " + anexoArticulo +
                            
                            " OR " +
                            " (LTRIM(dbo.STOCKALM.CODALM) IN (" + almacenes + ")) AND (dbo.STOCKALM.UNIDADES > 0) AND (dbo.ARTICULO.KLS_ID_SHOP > 0) AND " +
                            " (dbo.FAMILIATALLA.KLS_MONOTALLA IS NOT NULL) AND (dbo.FAMILIATALLA.KLS_MONOTALLA IS NOT NULL) AND (dbo.ARTICULO.KLS_MONOATRIBUTO IS NULL) " + anexoArticulo +
                            " ORDER BY CODART";
            }
            else
            {

                consulta = "SELECT dbo.STOCKALM.CODALM, " +
                               " LTRIM(dbo.STOCKALM.CODART) AS CODART, " +
                               " LTRIM(dbo.STOCKALM.CODFAMTALLAH) AS CODFAMTALLAH, " +
                               " LTRIM(dbo.STOCKALM.CODFAMTALLAV) AS CODFAMTALLAV, " +
                               " LTRIM(dbo.STOCKALM.CODTALLAH) AS CODTALLAH, " +
                               " LTRIM(dbo.STOCKALM.CODTALLAV) AS CODTALLAV, " +
                               " dbo.TALLAS.ID, " +
                               " TALLAS_1.ID AS ID2, " +
                               " dbo.STOCKALM.UNIDADES AS STOCK_A3, " +
                               " CASE WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='SI') THEN 1 WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='NO') THEN 0 ELSE 2 END  AS PERMITIR_COMPRAS " +
                               " FROM     dbo.TALLAS RIGHT OUTER JOIN " +
                               " dbo.STOCKALM ON (dbo.STOCKALM.CODTALLAH = dbo.TALLAS.CODTALLA OR " +
                               " dbo.STOCKALM.CODTALLAH IS NULL AND dbo.TALLAS.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA OR " +
                               " dbo.STOCKALM.CODFAMTALLAH IS NULL AND dbo.TALLAS.CODFAMTALLA IS NULL) LEFT OUTER JOIN " +
                               " dbo.TALLAS AS TALLAS_1 ON (dbo.STOCKALM.CODTALLAV = TALLAS_1.CODTALLA OR " +
                               " dbo.STOCKALM.CODTALLAV IS NULL AND TALLAS_1.CODTALLA IS NULL) AND (dbo.STOCKALM.CODFAMTALLAV = TALLAS_1.CODFAMTALLA OR " +
                               " dbo.STOCKALM.CODFAMTALLAV IS NULL AND TALLAS_1.CODFAMTALLA IS NULL) LEFT OUTER JOIN " +
                               " dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                               " WHERE  LTRIM(dbo.STOCKALM.CODALM) IN (" + csUtilidades.getAlmacenWhereIn() + ") AND (dbo.STOCKALM.UNIDADES > 0) " + anexo + anexoArticulo +
                               " ORDER BY stockalm.codart asc";
            
            
            
            
            
            }
            return consulta;
        }


        public string stockA3TallasA3NOPSSI(string codart=null)
        {
            string anexoCodart = "";

            if (!string.IsNullOrEmpty(codart))
            {
                anexoCodart = " and LTRIM(KLS_ARTICULO_PADRE)='" + codart + "' ";
            }

            string consulta = "SELECT " +
                                " LTRIM(KLS_ARTICULO_PADRE) AS KLS_ARTICULO_PADRE, dbo.ARTICULO.KLS_ARTICULO_TIENDA, LTRIM(dbo.ARTICULO.CODART) AS CODART,  " +
                                " dbo.ARTICULO.DESCART, dbo.ARTICULO.KLS_ARTICULO_BLOQUEAR AS COMBINACION_VISIBLE, SUM(dbo.STOCKALM.UNIDADES) AS STOCK_A3, " +
                                " CASE WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='SI') THEN 1 WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='NO') THEN 0 ELSE 2 END  AS PERMITIR_COMPRAS " +
                                " FROM " +
                                " dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                                " dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART " +
                                " WHERE " +
                                " (dbo.ARTICULO.KLS_ARTICULO_PADRE IS NOT NULL) AND LTRIM(dbo.STOCKALM.CODALM) IN (" + csUtilidades.getAlmacenWhereIn() + ") " +
                                anexoCodart +
                                " GROUP BY LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.DESCART, dbo.ARTICULO.KLS_ARTICULO_PADRE, dbo.ARTICULO.KLS_ARTICULO_TIENDA,  " +
                                " dbo.ARTICULO.KLS_ARTICULO_BLOQUEAR, " +
                                " CASE WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='SI') THEN 1 WHEN (dbo.ARTICULO.KLS_PERMITIRCOMPRAS='NO') THEN 0 ELSE 2 END";

            return consulta;
        }

        public string consultaStockA3SIPSSI(string almacen, string where_in, string idProduct = null)
        {

            //SE CAMBIA EL SUM POR 
            //SUM(CASE WHEN (dbo.STOCKALM.UNIDADES<0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 
            //YA QUE SI EL STOCK ES NEGATIVO NO LO INCLUIMOS, Y REALMENTE SUMANOS LAS UNIDADES POSITIVAS

            string anexoArticulo = "";

            if (idProduct == null)
            {
                anexoArticulo = " AND KLS_ID_SHOP > 0 ";
            }
            else
            {
                anexoArticulo = " AND KLS_ID_SHOP = " + idProduct;
            }

            string consulta = "SELECT LTRIM(dbo.STOCKALM.CODART) AS REFERENCE, " +
                        " ARTICULO.DESCART, " +
                        " dbo.ARTICULO.TALLAS, " +
                        " dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                        " KLS_ID_SHOP, " +
                        " CASE " +
                        "       WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                        "       WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                        "       ELSE 2 " +
                        "       END AS KLS_PERMITIRCOMPRAS, " +
                        "       SUM(CASE WHEN (dbo.STOCKALM.UNIDADES<0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3  " +
                        " FROM dbo.STOCKALM  WITH (NOLOCK) " +
                        " INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                        " WHERE dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T'  AND dbo.ARTICULO.ESKIT = 'F' " +
                        " AND LTRIM(dbo.STOCKALM.CODALM) IN (" + csUtilidades.getAlmacenWhereIn() + ") " +
                        //" AND KLS_ID_SHOP > 0 " +
                        anexoArticulo + 
                        " GROUP BY LTRIM(dbo.STOCKALM.CODART), " +
                        "         dbo.ARTICULO.TALLAS, " +
                        "         ARTICULO.DESCART, " +
                        "         dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                        "         KLS_ID_SHOP, " +
                        "         KLS_PERMITIRCOMPRAS" + 
                        " UNION " +
                        //AÑADO EL STOCK DISPONIBLE DE LOS KITS 7-9-2017
                        " SELECT   " +
                        " TOP (100) PERCENT dbo.ARTICULO.CODART AS REFERENCE, dbo.ARTICULO.DESCART, dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ARTICULO_TIENDA,  " +
                        " dbo.ARTICULO.KLS_ID_SHOP, " +
                        " CASE WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 ELSE 2 END AS KLS_PERMITIRCOMPRAS, " +
                        " MIN(CASE WHEN dbo.STOCKALM.UNIDADES IS NULL  " +
                        " THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                        " FROM  " +
                        " dbo.ESCANDALLO LEFT OUTER JOIN " +
                        " dbo.STOCKALM ON dbo.ESCANDALLO.CODARTC = dbo.STOCKALM.CODART LEFT OUTER JOIN " +
                        " dbo.ARTICULO ON dbo.ESCANDALLO.CODARTP = dbo.ARTICULO.CODART " +
                        " GROUP BY  " +
                        " dbo.ARTICULO.CODART, dbo.ESCANDALLO.CODALM, dbo.ARTICULO.ESKIT, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.KLS_PERMITIRCOMPRAS,  " +
                        " dbo.ARTICULO.DESCART, dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ARTICULO_TIENDA " +
                        " HAVING  " +
                        " (dbo.ARTICULO.ESKIT = 'T') AND (dbo.ARTICULO.KLS_ID_SHOP > 0) AND (dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T') " +
                        " ORDER BY REFERENCE ";

            return consulta;
        }


        public string consultaStockA3NOPSSI(string almacen, string where_in, string where_in_padre, bool products = false)
        {

            //SE CAMBIA EL SUM POR 
            //SUM(CASE WHEN (dbo.STOCKALM.UNIDADES<0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 
            //YA QUE SI EL STOCK ES NEGATIVO NO LO INCLUIMOS, Y REALMENTE SUMANOS LAS UNIDADES POSITIVAS

            string sb = //"SELECT LTRIM(AP.CODART) AS CODART, " +
                //"       dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                //"       dbo.ARTICULO.KLS_ID_SHOP, " +
                //"       CASE " +
                //"           WHEN AP.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                //"           WHEN AP.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                //"           ELSE 2 " +
                //"       END AS KLS_PERMITIRCOMPRAS, " +
                //"       SUM(CASE WHEN (dbo.STOCKALM.UNIDADES < 0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                //"FROM dbo.STOCKALM WITH (NOLOCK) " +
                //"INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                //"INNER JOIN dbo.ARTICULO AS AP ON dbo.ARTICULO.KLS_ARTICULO_PADRE = AP.CODART " +
                //"WHERE LTRIM(dbo.STOCKALM.CODALM) IN ('" + almacen.Replace(",", "','") + "') " +
                //"  AND (AP.KLS_ID_SHOP > 0) " + where_in +
                //"GROUP BY dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                //"         dbo.ARTICULO.KLS_ID_SHOP, " +
                //"         dbo.ARTICULO.KLS_PERMITIRCOMPRAS, " +
                //"         AP.CODART, " +
                //"         CASE " +
                //"             WHEN AP.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                //"             WHEN AP.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                //"             ELSE 2 " +
                //"         END " +
                //"UNION " +
                //30-10-2018 Edito porque no calcula bien en Slide
                // "SELECT LTRIM(dbo.ARTICULO.CODART) AS CODART, " +
                " SELECT LTRIM(dbo.ARTICULO.KLS_ARTICULO_PADRE) AS CODART, " +
                "       ARTICULO_1.KLS_ARTICULO_TIENDA AS KLS_ARTICULO_TIENDA, " +
                "       ARTICULO_1.KLS_ID_SHOP, " +
                "       CASE " +
                "           WHEN ARTICULO_1.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                "           WHEN ARTICULO_1.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                "           ELSE 2 " +
                "       END AS KLS_PERMITIRCOMPRAS, " +
                "       SUM(CASE WHEN (dbo.STOCKALM.UNIDADES < 0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                " FROM dbo.STOCKALM WITH (NOLOCK) INNER JOIN " +
                " dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART INNER JOIN " +
                " dbo.ARTICULO AS ARTICULO_1 ON dbo.ARTICULO.KLS_ARTICULO_PADRE = ARTICULO_1.CODART " +
                " WHERE (LTRIM(dbo.STOCKALM.CODALM) IN ('" + almacen.Replace(",", "','") + "')) " +
                " AND (ARTICULO_1.KLS_ID_SHOP > 0) " + where_in_padre +
                " GROUP BY LTRIM(dbo.ARTICULO.KLS_ARTICULO_PADRE), " +
                "         ARTICULO_1.KLS_ARTICULO_TIENDA, " +
                "         ARTICULO_1.KLS_ID_SHOP, " +
                "         ARTICULO_1.KLS_PERMITIRCOMPRAS, " +
                "         CASE " +
                "             WHEN ARTICULO_1.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                "             WHEN ARTICULO_1.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                "             ELSE 2 " +
                "         END " +
                " UNION " +
                //AÑADIMOS LOS ARTÍCULOS QUE NO TIENEN PADRES
                " SELECT " +
                " LTRIM(dbo.ARTICULO.CODART) AS CODART, dbo.ARTICULO.KLS_ARTICULO_TIENDA, dbo.ARTICULO.KLS_ID_SHOP, " +
                " CASE WHEN dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 WHEN dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 ELSE 2 END AS KLS_PERMITIRCOMPRAS, " +
                " SUM(CASE WHEN (dbo.STOCKALM.UNIDADES < 0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                " FROM dbo.STOCKALM WITH (NOLOCK)INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                " WHERE LTRIM(dbo.STOCKALM.CODALM) IN ('" + almacen.Replace(",", "','") + "') " +
                " AND(dbo.ARTICULO.KLS_ID_SHOP > 0) AND dbo.ARTICULO.KLS_ARTICULO_PADRE IS NULL " + where_in +
                " GROUP BY LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.KLS_ARTICULO_TIENDA, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.KLS_PERMITIRCOMPRAS, " +
                " CASE WHEN dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 WHEN dbo.ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 ELSE 2 END";
	
            return sb;
        }


        public string repeatedCategories()
        {
            return "select ps_product.id_product, ps_category_product.id_category, ps_product.id_category_default from ps_product " +
                   " left join ps_category_product on ps_category_product.id_product = ps_product.id_product " +
                   " where id_category is not null";
        }

        public string selectAssignCategories()
        {
            return "select ps_category.id_category, id_parent, level_depth, name, ps_category.position, ps_category.active " +
            " from ps_category join ps_category_lang on ps_category_lang.id_category = ps_category.id_category " +
            " where id_lang = " + csUtilidades.idIdiomaDefaultPS() + " order by level_depth asc, id_category asc";
        }

        public string getProductsByCat(int id, string filtro = "")
        {
            string where = "", replicarImagenes = "";

            if (filtro != "")
            {
                where = " where " + filtro;
            }
            if (!csGlobal.replicarImagenes)
            {
                replicarImagenes = " group by reference";
            }


            //int lang = csGlobal.IdiomasEsync.FirstOrDefault(x => x.Value == "CAS").Key;
            //return "select ps_product.reference, ps_category_product.id_product, ps_category_lang.id_category, ps_product_lang.name as product_name, ps_category_lang.name AS category_name" +
            //        " from ps_category_product " +
            //        " join ps_product_lang on ps_product_lang.id_product = ps_category_product.id_product " +
            //        " join ps_category_lang on ps_category_lang.id_category = ps_category_product.id_category " +
            //        " join ps_product on ps_product.id_product = ps_category_product.id_product " +
            //        " where ps_category_lang.id_lang = " + lang.ToString() + " and ps_product_lang.id_lang = " + lang.ToString() + " and ps_category_lang.id_category = " + id + where;
            string consulta = "select distinct ps_product.reference, ps_category_product.id_product, ps_category_lang.id_category, ps_product_lang.name as product_name, ps_category_lang.name AS category_name, ps_image.position as image, ps_product.active " +
                        " from ps_category_product " +
                        " join ps_product_lang on ps_product_lang.id_product = ps_category_product.id_product " +
                        " join ps_category_lang on ps_category_lang.id_category = ps_category_product.id_category " +
                        " join ps_product on ps_product.id_product = ps_category_product.id_product " +
                        " left join ps_image on ps_image.id_product = ps_category_product.id_product " +
                        " where ps_category_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS() + " and ps_product_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS()  + " and ps_category_lang.id_category = " + id + where + replicarImagenes;
            return consulta;
        }

        public string getProductsByName(string filtro = "")
        {
            string where = "";

            if (filtro != "")
            {
                where = " " + filtro;
            }

            string consulta = "select distinct ps_product.reference, ps_category_product.id_product, ps_category_lang.id_category, ps_product_lang.name as product_name, ps_category_lang.name AS category_name, ps_image.position as image, ps_product.active " +
                        " from ps_category_product " +
                        " join ps_product_lang on ps_product_lang.id_product = ps_category_product.id_product " +
                        " join ps_category_lang on ps_category_lang.id_category = ps_category_product.id_category " +
                        " join ps_product on ps_product.id_product = ps_category_product.id_product " +
                        " left join ps_image on ps_image.id_product = ps_category_product.id_product " +
                        " where ps_category_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS() + " and ps_product_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS() + " and " + where;
            return consulta;
        }


        public string selectDireccionesA3(bool vacio)
        {
            string anexo = "";
            if (vacio)
            {
                anexo = " AND ID_PS is NULL";
            }

            return " SELECT dbo.__CLIENTES.KLS_CODCLIENTE, dbo.DIRENT.CODCLI, dbo.DIRENT.NOMENT, dbo.DIRENT.ID_PS, dbo.DIRENT.DTOENT, dbo.DIRENT.POBENT, dbo.DIRENT.DIRENT1, dbo.DIRENT.IDDIRENT " +
                   " FROM dbo.__CLIENTES  WITH (NOLOCK) INNER JOIN dbo.DIRENT ON dbo.__CLIENTES.CODCLI = dbo.DIRENT.CODCLI WHERE KLS_CODCLIENTE <> '' " + anexo + " ORDER BY dbo.DIRENT.CODCLI";
        }

        public string selectEstadoArticuloA3ERP(string codart)
        {

            return " select  KLS_ARTICULO_BLOQUEAR from ARTICULO  WITH (NOLOCK) where KLS_ID_SHOP=" + codart;
        }

        public string selectArticulosSync(string tipoCarga, string consulta = null, string where_in = "", string codMarca = "0", string codColeccion = "0", bool disponible = false, bool enTienda=false)
        {
            string filtroTexto = "";
            string filtroTexto2 = "";
            string filtroMarca = "";
            string filtroColeccion="";
            string filtroStockDisponible = "";
            string filtroArticulosTienda = "";
            string anexo = "";
            bool campos_nuevos = false; // last_update, etc
            bool articulo_padre = false;
            bool coleccion = false;
            

            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_ARTICULO_PADRE"))
            {
                articulo_padre = true;
            }
            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_LASTUPDATE"))
            {
                campos_nuevos = true;
            }

            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_CODCOLECCION"))
            {
                coleccion = true;
            }

            if (disponible)
            {
                filtroStockDisponible = " AND (SELECT SUM(UNIDADES) AS STOCK FROM dbo.STOCKALM WHERE STOCKALM.CODART=ARTICULO.CODART GROUP BY CODART) > 0 ";
            }

            //28-01-2018 MEJORAMOS LA CONSULTA CON UN BUSCADOR AVANZADO
            if (consulta != "" || codMarca!="0" || codColeccion!="0" || enTienda)
            {
                if (codMarca != "0")
                {
                    filtroMarca = " AND (dbo.ARTICULO.KLS_CODMARCA IN ('" +codMarca + "')) ";
                }

                if (codColeccion!="0")
                {
                    filtroColeccion = " AND (dbo.ARTICULO.KLS_CODCOLECCION IN ('" + codColeccion + "')) ";
                }
                if (enTienda)
                {
                    filtroArticulosTienda = " AND (dbo.ARTICULO.KLS_ID_SHOP > 0) ";
                
                }


                filtroTexto = " AND (DESCART LIKE '%" + consulta + "%' OR ltrim(CODART) LIKE  '%" + consulta + "%') " + filtroMarca + filtroColeccion + filtroStockDisponible + filtroArticulosTienda;
                filtroTexto2 = " WHERE (DESCART LIKE '%" + consulta + "%' OR ltrim(CODART) LIKE  '%" + consulta + "%') " + filtroMarca + filtroColeccion + filtroStockDisponible + filtroArticulosTienda;
            }
            switch (tipoCarga)
            {
                case "Actualizar":
                    //anexo = " LEFT JOIN KLS_ARTICULOS_ACTUALIZAR ON LTRIM(ARTICULO.CODART) = LTRIM(KLS_ARTICULOS_ACTUALIZAR.CODART) WHERE KLS_ARTICULOS_ACTUALIZAR.CODART <> ''";
                    anexo = "LEFT OUTER JOIN dbo.KLS_ARTICULOS_ACTUALIZAR ON dbo.ARTICULO.CODART = dbo.KLS_ARTICULOS_ACTUALIZAR.CODART " +
                    " GROUP BY LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.DESCART, dbo.ARTICULO.PRCVENTA, dbo.ARTICULO.KLS_ARTICULO_TIENDA, dbo.ARTICULO.KLS_ID_SHOP, " +
                    " dbo.ARTICULO.KLS_ARTICULO_BLOQUEAR, dbo.ARTICULO.TALLAS, dbo.KLS_ARTICULOS_ACTUALIZAR.CODART " +
                    " HAVING      (dbo.KLS_ARTICULOS_ACTUALIZAR.CODART <> '') " + filtroTexto;
                    break;
                case "Nuevos":
                    anexo = " WHERE (KLS_ID_SHOP IS NULL OR KLS_ID_SHOP =0) AND KLS_ARTICULO_TIENDA='T' " + ((articulo_padre) ? "AND KLS_ARTICULO_PADRE IS  NULL" : "") + filtroTexto + filtroMarca + filtroStockDisponible + filtroArticulosTienda;
                    break;
                case "Todos":
                    
                    anexo = filtroTexto2;
                    if (where_in != "")
                    {
                        anexo = " WHERE LTRIM(CODART) IN (" + where_in + ")" + filtroStockDisponible + filtroArticulosTienda;
                    }
                    else
                    {
                        if (anexo == "")
                        {
                            anexo = anexo + filtroStockDisponible.Replace("AND","WHERE");
                        }
                        else
                        {
                            anexo = anexo + filtroStockDisponible;
                        }
                    }
                    break;
                case "enTienda":
                    anexo = " WHERE KLS_ARTICULO_TIENDA='T' AND KLS_ID_SHOP IS NOT NULL" + filtroTexto + filtroStockDisponible;
                    break;
                case "Bloqueados":
                    anexo = " WHERE KLS_ARTICULO_BLOQUEAR = 'T'" + filtroTexto + filtroStockDisponible;
                    break;

                default:
                    anexo = filtroTexto2 + filtroStockDisponible;
                    break;
            }




            // UPDATE ARTICULO
            // SET ARTICULO.KLS_ARTICULO_TIENDA='T'
            // WHERE CODART IN (SELECT * FROM ARTICULOAUXX)

            string selectScript = "SELECT LTRIM(ARTICULO.CODART) AS CODIGO, DESCART,  ROUND(PRCVENTA,2) AS PRECIO, KLS_ARTICULO_TIENDA AS 'VISIBLE TIENDA', " +
                                  " KLS_ID_SHOP AS 'ID EN TIENDA', KLS_ARTICULO_BLOQUEAR AS BLOQUEAR, TALLAS AS TyC, KLS_MARCAS.kls_descmarca as MARCA, " +
                                  " (SELECT SUM(UNIDADES) AS STOCK  FROM dbo.STOCKALM WHERE STOCKALM.CODART=ARTICULO.CODART GROUP BY CODART) as STOCK, " +
                                  " Convert(varchar(10),CONVERT(date,ARTICULO.FECALTA,106),103) as FECHA_ALTA " + 
                                  ((campos_nuevos) ? " , articulo.KLS_LASTUPDATE AS ACTUALIZACION " : "") +
                                  ((coleccion) ? " , articulo.KLS_CODCOLECCION AS COLECCION " : "") +
                                  " FROM DBO.ARTICULO  WITH (NOLOCK) " +
                                  " LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA " + anexo;
            //" where ltrim(codart) in ('EXC100-010','EXC100-011','EXC100-021','EXC100-021','EXC100-021','EXC100-026','EXC100-060','EXC100-060','EXC100-060','EXC100-071','EXC100-071','EXC100-071','EXC100-074','EXC100-100','EXC100-148','EXC100-152','EXC100-153','EXC100-154','EXC100-176','EXC100-181','EXC100-181','EXC100-182-BK','EXC100-182','EXC100-182','EXC100-182','EXC100-182','EXC100-189','EXC100-191','EXC100-191','EXC100-210','EXC100-210','EXC100-215-BK','EXC100-304','EXC100-310','EXC100-310','EXC100-311','EXC100-311','EXC100-312','EXC100-314','EXC100-335','EXC100-335','EXC100-395','EXC100-395','EXC100-464','EXC100-465','EXC100-500','EXC100-500','EXC100-501','EXC100-501','EXC100-502','EXC100-502','EXC100-503','EXC100-503','EXC100-980','EXC100-980','EXC200-001','EXC200-001','EXC200-002','EXC200-002','EXC200-003','EXC200-003','EXC200-004','EXC200-004','EXC200-005','EXC200-006','EXC200-007','EXC200-008','EXC200-009','EXC200-011','EXC200-012','EXC200-013','EXC200-014','EXC200-015','EXC200-016','EXC200-017','EXC200-019','EXC200-019','EXC200-043','EXC200-044','EXC200-046','EXC200-047','EXC200-048','EXC200-049','EXC200-050','EXC200-052','EXC200-053','EXC200-054','EXC200-055','EXC200-056','EXC200-057','EXC200-058','EXC200-059','EXC200-062','EXC200-063','EXC200-064','EXC200-067','EXC200-069','EXC200-070','EXC200-071','EXC200-087','EXC200-089','EXC200-098','EXC200-099','EXC200-102','EXC200-107','EXC200-108','EXC200-109','EXC200-150','EXC200-151','EXC200-152','EXC200-168','EXC200-169','EXC200-170','EXC200-171','EXC200-172','EXC200-173','EXC200-174','EXC200-175','EXC200-176','EXC200-177','EXC200-178','EXC200-179','EXC200-180','EXC200-181','EXC200-182','EXC200-183','EXC200-184','EXC200-185','EXC200-186','EXC200-187','EXC200-188','EXC200-189','EXC200-190','EXC200-191','EXC200-192','EXC200-216','EXC200-218','EXC200-220','EXC200-222','EXC200-224','EXC200-226','EXC200-227','EXC200-228','EXC200-229','EXC200-236','EXC200-251','EXC200-252','EXC200-253','EXC200-254','EXC200-255','EXC200-256','EXC200-257','EXC200-259','EXC200-261','EXC200-262','EXC200-263','EXC200-264','EXC200-268','EXC200-290','EXC200-300','EXC200-310','EXC200-320','EXC200-324','EXC200-350-SM','EXC200-350','EXC200-363','EXC200-364','EXC200-365','EXC200-366','EXC200-500','EXC200-510','EXC200-520','EXC200-524','EXC200-530','EXC200-552','EXC200-553','EXC200-554','EXC200-555','EXC200-556','EXC200-557','EXC200-558','EXC200-559','EXC200-560','EXC200-561','EXC200-562','EXC200-562','EXC200-566','EXC200-567','EXC200-568','EXC200-569','EXC200-570','EXC200-571','EXC200-571','EXC200-577','EXC200-600','EXC200-610','EXC200-620','EXC200-630','EXC200-634','EXC200-678','EXC200-680','EXC200-682','EXC200-684','EXC200-686','EXC200-688','EXC200-690','EXC200-692','EXC200-694','EXC200-696','EXC200-698','EXC200-699','EXC200-735','EXC200-950','EXC200-951','EXC200-951','EXC200-952','EXC201-001','EXC201-002','EXC201-003','EXC201-005','EXC201-010','EXC201-021','EXC201-022','EXC201-023','EXC201-025','EXC201-041','EXC201-042','EXC201-043','EXC201-045','EXC202-000','EXC202-003','EXC202-004','EXC202-006','EXC204-012','EXC204-300','EXC204-301','EXC204-302','EXC204-303','EXC204-304','EXC204-305','EXC204-306','EXC204-307','EXC204-308','EXC204-309','EXC204-310','EXC204-311','EXC204-320','EXC204-320','EXC204-321','EXC204-321','EXC204-321','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-2466-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-GSBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-4266-WDBN-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-GSBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-42810-WDBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-GSBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXC542-4288-WDBF-BK','EXCBB001MPLGE','EXCBB001MPLGE','EXCBB001MPLGE','EXCBB002MPLGE','EXCBB002MPLGE','EXCBB002MPLGE','EXCBB003MPLGE','EXCBB003MPLGE','EXCBB003MPLGE','EXCBB005MPLGE','EXCBB005MPLGE','EXCBB005MPLGE','EXCWB12.390SG-BK','EXCWB12.390SG-BK','EXCWB12.390SG-BK','EXCWB12.390SG-BK','EXCWB12.390SG-BK','EXCWB12.5SGB','EXCWB12.5SGB','EXCWB12.5SGB','EXCWB12.5SGB','EXCWB12.5SGB','EXCWB12.6SGB','EXCWB12.6SGB','EXCWB12.6SGB','EXCWB12.6SGB','EXCWB12.6SGB','EXCWB15.390SG-BK','EXCWB15.390SG-BK','EXCWB15.390SG-BK','EXCWB15.390SG-BK','EXCWB15.390SG-BK','EXCWB15.5SGB','EXCWB15.5SGB','EXCWB15.5SGB','EXCWB15.5SGB','EXCWB15.5SGB','EXCWB15.6SGB','EXCWB15.6SGB','EXCWB15.6SGB','EXCWB15.6SGB','EXCWB15.6SGB','EXCWB6.390SG-BK','EXCWB6.390SG-BK','EXCWB6.390SG-BK','EXCWB6.390SG-BK','EXCWB6.390SG-BK','EXCWB6.5SGB','EXCWB6.5SGB','EXCWB6.5SGB','EXCWB6.5SGB','EXCWB6.5SGB','EXCWB6.6SGB','EXCWB6.6SGB','EXCWB6.6SGB','EXCWB6.6SGB','EXCWB6.6SGB','EXCWB9.390SG-BK','EXCWB9.390SG-BK','EXCWB9.390SG-BK','EXCWB9.390SG-BK','EXCWB9.390SG-BK','EXCWB9.5SGB','EXCWB9.5SGB','EXCWB9.5SGB','EXCWB9.5SGB','EXCWB9.5SGB','EXCWB9.6SGB','EXCWB9.6SGB','EXCWB9.6SGB','EXCWB9.6SGB','EXCWB9.6SGB','STEGOKTS011','STEGOKTS011','UBIAP-AC-PRO','UBIUAP-AC-PRO','UBIUC-CK','UBIUC-CK','ZYNWA1100-NH','ZYNWA1100-NH','ZYNWA1123-AC','ZYNWA1123-AC','ZYNWA1123-AC','ZYNWA1123-AC')";

            return selectScript;
        }

        public string selectDocsPS(string version, bool soloFras, string fechaDesde, string fechaHasta, bool pendientes, string formasPago)
        {
            string selectDocs = "";
            string anexo = "";

            selectDocs = "select ps_orders.id_order as pedido,current_state as situacion,ps_orders.id_customer as cliente ,dni,id_address_invoice,address1," +
                 " firstname,lastname, company ,payment,total_discounts,ps_orders.total_shipping_tax_excl as portes,total_paid,total_products,postcode, city," +
                 " date_format(invoice_date,'%d/%m/%Y') as invoice_date,ps_orders.invoice_number, kls_invoice_erp.invoice_number as docsA3" +
             " from " +
               csGlobal.databasePS + ".ps_orders " +
             " inner join " + csGlobal.databasePS + ".ps_address " +
             "	on (ps_address.id_address=ps_orders.id_address_invoice and " +
             " ps_address.id_customer=ps_orders.id_customer) " +
             " left join " + csGlobal.databasePS + ".kls_invoice_erp " +
             " on (ps_orders.invoice_number=kls_invoice_erp.invoice_number) " +
             " where  " +
             " current_state<> 6 and " +
             " invoice_date>='" + fechaDesde + "' and invoice_date<='" + fechaHasta + "' " + anexo + " order by ps_orders.invoice_number "; ;

            return selectDocs;
        }

        public string selectDocsPSTest()
        {
            string selectDocs = "";

            selectDocs = "select ps_orders.id_order as pedido,current_state as situacion,ps_orders.id_customer as cliente ,dni,id_address_invoice,address1," +
                 " firstname,lastname, company ,payment,total_discounts,ps_orders.total_shipping_tax_excl as portes,total_paid,total_products,postcode, city," +
                 " date_format(invoice_date,'%d/%m/%Y') as invoice_date,ps_orders.invoice_number, kls_invoice_erp.invoice_number as docsA3" +
             " from " +
               csGlobal.databasePS + ".ps_orders " +
             " inner join " + csGlobal.databasePS + ".ps_address " +
             "	on (ps_address.id_address=ps_orders.id_address_invoice and " +
             " ps_address.id_customer=ps_orders.id_customer) " +
             " left join " + csGlobal.databasePS + ".kls_invoice_erp " +
             " on (ps_orders.invoice_number=kls_invoice_erp.invoice_number) ";

            return selectDocs;
        }

        public string selectDocsPresta(string version, bool soloFras, string fechaDesde, string fechaHasta, bool pendientes, string formasPago)
        {
            string selectDocsV16 = "";
            string selectDocsV15 = "";
            string selectDocsV14 = "";
            string anexo = "";
            if (formasPago != "")
            {
                anexo = " and ps_orders.payment='" + formasPago + "' ";

            }
            if (soloFras)
            {
                anexo = " and ps_orders.invoice_number<>0 ";
            }
            if (pendientes)
            {
                anexo = anexo + " and kls_invoice_erp.invoice_number is null and ps_orders.invoice_number<>0 ";


            }
            selectDocsV16 = "select ps_orders.id_order,id_carrier,ps_orders.id_customer,dni,id_address_invoice,address1," +
              " ps_address.firstname,ps_address.lastname, ps_address.company ,payment,total_discounts,total_paid,total_products,postcode, city," +
              " date_format(invoice_date,'%d/%m/%Y') as invoice_date,ps_orders.invoice_number,id_order_detail ," +
              " product_id,product_reference,product_name,product_quantity,unit_price_tax_incl,total_price_tax_incl, " +
              " round(total_discounts*100/total_products_wt,1) as descuento, kls_invoice_erp.invoice_number as docsA3, ps_customer.kls_a3erp_id " +
              " from " +
                csGlobal.databasePS + ".ps_orders " +
              " inner join " + csGlobal.databasePS + ".ps_order_detail " +
              " on (ps_orders.id_order=ps_order_detail.id_order) " +
              " inner join " + csGlobal.databasePS + ".ps_customer " +
              " on (ps_orders.id_customer=ps_customer.id_customer) " +
              " inner join " + csGlobal.databasePS + ".ps_address " +
              "	on (ps_address.id_address=ps_orders.id_address_invoice and " +
              " ps_address.id_customer=ps_orders.id_customer) " +
              " left join " + csGlobal.databasePS + ".kls_invoice_erp " +
              " on (ps_orders.invoice_number=kls_invoice_erp.invoice_number) " +
              " where  " +
              " current_state<> 6 and " +
              " invoice_date>='" + fechaDesde + "' and invoice_date<='" + fechaHasta + "' " + anexo + " order by ps_orders.invoice_number "; ;

            selectDocsV15 = "select ps_orders.id_order,id_carrier,ps_orders.id_customer,dni,id_address_invoice,address1," +
              " firstname,lastname, company ,payment,total_discounts,total_paid,total_products,postcode, city," +
              " date_format(invoice_date,'%d/%m/%Y') as invoice_date,ps_orders.invoice_number,id_order_detail ," +
              " product_id,product_reference,product_name,product_quantity,product_price, " +
              " round(total_discounts*100/total_products_wt,1) as descuento, kls_invoice_erp.invoice_number as docsA3" +
              " from " +
                csGlobal.databasePS + ".ps_orders " +
              " inner join " + csGlobal.databasePS + ".ps_order_detail " +
              " on (ps_orders.id_order=ps_order_detail.id_order) " +
              " inner join " + csGlobal.databasePS + ".ps_address " +
              "	on (ps_address.id_address=ps_orders.id_address_invoice and " +
              " ps_address.id_customer=ps_orders.id_customer) " +
              " left join " + csGlobal.databasePS + ".kls_invoice_erp " +
              " on (ps_orders.invoice_number=kls_invoice_erp.invoice_number) " +
              " where  " +
              " invoice_date>='" + fechaDesde + "' and invoice_date<='" + fechaHasta + "' " + anexo + " order by ps_orders.invoice_number "; ;

            selectDocsV14 = "select ps_orders.id_order,id_carrier,ps_orders.id_customer,dni,id_address_invoice, address1," +
               " firstname,lastname, company,payment,total_discounts,total_paid,total_products,postcode, city, " +
               " date_format(invoice_date,'%d/%m/%Y') as invoice_date,ps_orders.invoice_number,id_order_detail , " +
               " product_id,product_reference,product_name,product_quantity,product_price, round(product_price*(1+(tax_rate/100)),2) as unit_price_tax_incl, " +
               " round(total_discounts*100/total_products_wt,1) as descuento, kls_invoice_erp.invoice_number as docsA3" +
               " from " +
                csGlobal.databasePS + ".ps_orders " +
              " inner join " + csGlobal.databasePS + ".ps_order_detail " +
              " on (ps_orders.id_order=ps_order_detail.id_order) " +
              " inner join " + csGlobal.databasePS + ".ps_address " +
              "	on (ps_address.id_address=ps_orders.id_address_invoice and " +
              " ps_address.id_customer=ps_orders.id_customer) " +
              " left join " + csGlobal.databasePS + ".kls_invoice_erp " +
              " on (ps_orders.invoice_number=kls_invoice_erp.invoice_number) " +
              " where  " +
                ////" current_state<> 6 and " +
              " invoice_date>='" + fechaDesde + "' and invoice_date<='" + fechaHasta + "' " + anexo + " order by ps_orders.invoice_number "; ;

            if (version == "1.4")
            {
                return selectDocsV14;
            }
            else if (version == "1.5")
            {
                return selectDocsV15;
            }
            else if (version == "1.6")
            {
                return selectDocsV16;
            }
            else
            {
                return selectDocsV16;
            }
        }

        public string selectStocks()
        {

            string selectScript = "SELECT LTRIM(KLS_PRESTA_PRODUCTS.reference) AS codart, dbo.ARTICULO.DESCART, " +
                    " LTRIM(dbo.STOCKALM.CODALM) as codalm, dbo.STOCKALM.UNIDADES, " +
                    " dbo.KLS_PRESTA_PRODUCTS.id_producto " +
                    " FROM dbo.ARTICULO  WITH (NOLOCK) LEFT OUTER JOIN " +
                    " dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART  RIGHT OUTER JOIN " +
                    " dbo.KLS_PRESTA_PRODUCTS ON LTRIM(dbo.ARTICULO.CODART) = dbo.KLS_PRESTA_PRODUCTS.reference" +
            " WHERE (dbo.STOCKALM.CODALM LIKE '%" + csGlobal.almacenA3 + "') OR (dbo.STOCKALM.CODALM IS NULL) ";
            return selectScript;
        }

        //Creación de Tablas en A3
        public string crearTablaArticulosA3()
        {
            string createScript = " CREATE TABLE [dbo].[KLS_PRESTA_PRODUCTS]( " +
                " [id_producto] [int] NOT NULL, " +
                " [reference] [varchar](32) NOT NULL " +
                " ) ON [PRIMARY] ";
            return createScript;
        }

        public string crearTablaEnlacesIdiomas()
        {

            string createScript = " CREATE TABLE [dbo].[KLS_ESYNC_IDIOMAS](" +
                   " [idiomaPS] [int] NOT NULL, " +
                   " [idiomaA3] [varchar](50) NOT NULL " +
                   " ) ON [PRIMARY] ";
            return createScript;

        }

        public string crearTablaTallasyColores()
        {

            string createScript = " CREATE TABLE [dbo].[KLS_ESYNC_TALLASYCOLORES](" +
                    "[PS_GRUPO] [varchar](50) NULL," +
                    "[PS_ATRIBUTO] [varchar](50) NULL," +
                    "[A3_GRUPO] [varchar](50) NULL," +
                    "[A3_ATRIBUTO] [varchar](50) NULL" +
                   " ) ON [PRIMARY] ";
            return createScript;

        }
        public string selectPreciosFichaArticulo()
        {
            string anexo = "";
            if (csGlobal.ivaIncluido.ToUpper() == "SI")
            {
                anexo = ",ROUND(PRCVENTA/(1+(PORIVA)/100),3) AS BASE ";
            }

            string selectScript = "SELECT CODART, DESCART, KLS_ID_SHOP, PRCVENTA AS PRECIO, ARTICULO.TIPIVA, ARTICULO.KLS_ARTICULO_PADRE, " +
                " CASE WHEN ARTICULO.TIPIVA='ORD21' THEN 1 WHEN ARTICULO.TIPIVA='RED10' THEN 2 " +
                " WHEN ARTICULO.TIPIVA='RED4' THEN 3 WHEN ARTICULO.TIPIVA='SRE' THEN 3 ELSE 1 END AS TAXGROUP,PORIVA " + anexo + ", 'PRECIOSFICHA' AS ORIGEN  " +
                " FROM ARTICULO  WITH (NOLOCK) INNER JOIN TIPOIVA ON ARTICULO.TIPIVA=TIPOIVA.TIPIVA WHERE KLS_ID_SHOP > 0";

            if (csGlobal.preciosAtributosVariables)
            {

                selectScript = selectScript + "UNION " +
                    " SELECT CODART, DESCART, KLS_ID_SHOP, PRCVENTA AS PRECIO, ARTICULO.TIPIVA, ARTICULO.KLS_ARTICULO_PADRE,  " +
                    " CASE WHEN ARTICULO.TIPIVA='ORD21' THEN 1 WHEN ARTICULO.TIPIVA='RED10' THEN 2 " +
                    " WHEN ARTICULO.TIPIVA='RED4' THEN 3 WHEN ARTICULO.TIPIVA='SRE' THEN 3 ELSE 1 END AS TAXGROUP,PORIVA " + anexo + ", 'PRECIOSATRIBUTOS' AS ORIGEN  " +
                    " FROM ARTICULO  WITH (NOLOCK) INNER JOIN TIPOIVA ON ARTICULO.TIPIVA=TIPOIVA.TIPIVA WHERE KLS_ARTICULO_PADRE IS NOT NULL ";
            }
            
            
            return selectScript;
        }


        public string selectPreciosArticulos(string tarifa)
        {
            string selectScript = "";
            string anexo = "";

            if (csGlobal.ivaIncluido.ToUpper() == "SI")
            {
                anexo = ",ROUND(dbo.TARIFAVE.PRECIO/(1+(PORIVA)/100),3) AS BASE ";
            }


            if (csGlobal.precioAtributos)
            {
                //Compruebo si la empresa trabaja con precios de atributos o no.
         
                
                selectScript = string.Format("SELECT dbo.ARTICULO.KLS_ID_SHOP, " +
           "       dbo.TARIFAVE.CODART AS Articulo, " +
           "       dbo.ARTICULO.DESCART AS Descripción, " +
           "       dbo.TARIFAVE.TARIFA AS [Codigo Tarifa], " +
           "       dbo.TARIFAS.DESCTARIFA AS Tarifa,poriva," +
           "       CASE " +
           "           WHEN ARTICULO.TIPIVA='ORD21' THEN 1 " +
           "           WHEN ARTICULO.TIPIVA='RED10' THEN 2 " +
           "           WHEN ARTICULO.TIPIVA='RED4' THEN 3 " +
           "           WHEN ARTICULO.TIPIVA='SRE' THEN 3 " +
           "           ELSE 1 " +
           "       END AS TAXGROUP, " +
           "       round(dbo.TARIFAVE.PRECIO,2) AS PRECIO " + anexo +
           //"       ROUND(PRCVENTA/(1+(PORIVA)/100),3) AS BASE " +
           "FROM dbo.TARIFAVE WITH (NOLOCK) " +
           "INNER JOIN dbo.ARTICULO ON dbo.TARIFAVE.CODART = dbo.ARTICULO.CODART " +
           "INNER JOIN dbo.TARIFAS ON dbo.TARIFAVE.TARIFA = dbo.TARIFAS.TARIFA " +
           "INNER JOIN TIPOIVA ON ARTICULO.TIPIVA=TIPOIVA.TIPIVA " +
           "WHERE" +
           "  (tarifave.FECMIN <=GETDATE() " +
           "       and tarifave.FECMAX > getDate()) " +
           " AND TARIFAS.DESCTARIFA = '{0}'", tarifa);

            }
            else {
                selectScript = string.Format("SELECT dbo.ARTICULO.KLS_ID_SHOP, " +
          "       dbo.TARIFAVE.CODART AS Articulo, " +
          "       dbo.ARTICULO.DESCART AS Descripción, " +
          "       dbo.TARIFAVE.TARIFA AS [Codigo Tarifa], " +
          "       dbo.TARIFAS.DESCTARIFA AS Tarifa,poriva," +
          "       CASE " +
          "           WHEN ARTICULO.TIPIVA='ORD21' THEN 1 " +
          "           WHEN ARTICULO.TIPIVA='RED10' THEN 2 " +
          "           WHEN ARTICULO.TIPIVA='RED4' THEN 3 " +
          "           WHEN ARTICULO.TIPIVA='SRE' THEN 3 " +
          "           ELSE 1 " +
          "       END AS TAXGROUP, " +
          "       round(dbo.TARIFAVE.PRECIO,2) AS PRECIO " + anexo +
          //"       ROUND(PRCVENTA/(1+(PORIVA)/100),3) AS BASE " +
          "FROM dbo.TARIFAVE WITH (NOLOCK) " +
          "INNER JOIN dbo.ARTICULO ON dbo.TARIFAVE.CODART = dbo.ARTICULO.CODART " +
          "INNER JOIN dbo.TARIFAS ON dbo.TARIFAVE.TARIFA = dbo.TARIFAS.TARIFA " +
          "INNER JOIN TIPOIVA ON ARTICULO.TIPIVA=TIPOIVA.TIPIVA " +
          "WHERE (KLS_ID_SHOP > 0) " +
          "  AND (tarifave.FECMIN <=GETDATE() " +
          "       and tarifave.FECMAX > getDate()) " +
          " AND TARIFAS.DESCTARIFA = '{0}'", tarifa);
            }
		
       
            return selectScript;
        }

        //CARGAR INFORMACIÓN EN PANTALLA DE IDIOMAS
        public string selectIdiomasA3()
        {
            string selectScript = "SELECT CODIDIOMA, DESCIDIOMA FROM dbo.IDIOMAS WITH (NOLOCK) ";
            return selectScript;
        }


        public string selectClientesFromA3ToRepasat(bool update=false, string updateDate=null, string accountsToUpdate=null, bool cronjob=false)
        {
            string anexoClientesToUpdate = "";
            string anexoJoinReplog = " INNER JOIN dbo.REPLOG ON dbo.CLIENTES.CODCLI = dbo.REPLOG.IDREG1  and dbo.REPLOG.TABLA = 'CLIENTES' and dbo.REPLOG.MOVIMIENTO = 'MOD' ";
            string anexoFechaReplog = " ,max(fecha) as lastupdate";
            string anexoClientesToCreate = "";
            string anexoInnerTablaReplog = "";
            string anexoFiltroTablaReplog = "";
            string anexoInnerTablaCliente = " dbo.CLIENTES INNER JOIN  ";
            string selectScript = "";
            string anexoExternalConnection = csGlobal.sync_adv_mode ? "," + csGlobal.id_conexion_externa + " AS id_conexion_ext " : null;
            string anexoUnidadNegocio = csGlobal.activarUnidadesNegocio ? "," + csGlobal.idUnidadNegocio + " AS id_unidad_negocio " : null;

            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();

            updateDate = update? updateDataRPST.getLastUpdateSyncDate("accounts", false):"";
            anexoClientesToCreate = string.IsNullOrEmpty(accountsToUpdate) ? "": " AND LTRIM(dbo.CLIENTES.CODCLI) IN (" + accountsToUpdate  + ")";

            selectScript = " SELECT " +
                " LTRIM(dbo.CLIENTES.CODCLI) AS codExternoCli, dbo.CLIENTES.NOMCLI AS nomCli, dbo.CLIENTES.NIFCLI AS cifCli, CASE WHEN CLIENTES.BLOQUEADO = 'T' THEN 0 ELSE 1 END AS activoCli,  " +
                " dbo.CLIENTES.DIASPAGO1 AS diaPago1Cli, dbo.CLIENTES.DIASPAGO2 AS diaPago2Cli, dbo.CLIENTES.DIASPAGO3 AS diaPago3Cli, dbo.CLIENTES.OBSCLI AS descCli,  " +
                " REPLACE(dbo.CLIENTES.TELCLI, '.', '') AS tel1, dbo.CLIENTES.RAZON AS razonSocialCli, dbo.CLIENTES.PAGINAWEB AS webCli, dbo.CLIENTES.E_MAIL AS emailCli, 'CLI' AS tipoCli, dbo.RUTAS.RPST_ID_RUTA AS idRuta,  " +
                " dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago, dbo.REPRESEN.RPST_ID_REPRE AS idTrabajador, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago, REPLACE(dbo.CLIENTES.FAXCLI, '.', '') AS fax, " +
                " REPLACE(dbo.CLIENTES.TELCLI2, '.', '') AS tel2, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, dbo.ZONAS.RPST_ID_ZONA AS idZonaGeo " +
                anexoExternalConnection +
                anexoUnidadNegocio +
                " FROM dbo.CLIENTES WITH (NOLOCK)LEFT OUTER JOIN " +
                " dbo.REGIVA ON dbo.CLIENTES.REGIVA = dbo.REGIVA.REGIVA LEFT OUTER JOIN " +
                " dbo.FORMAPAG ON dbo.CLIENTES.FORPAG = dbo.FORMAPAG.FORPAG LEFT OUTER JOIN " +
                " dbo.REPRESEN ON dbo.CLIENTES.CODREP = dbo.REPRESEN.CODREP LEFT OUTER JOIN " +
                " dbo.DOCUPAGO ON dbo.CLIENTES.DOCPAG = dbo.DOCUPAGO.DOCPAG LEFT OUTER JOIN " +
                " dbo.RUTAS ON dbo.CLIENTES.RUTA = dbo.RUTAS.RUTA LEFT OUTER JOIN " +
                " dbo.ZONAS ON dbo.CLIENTES.ZONA = dbo.ZONAS.ZONA " +
                " WHERE (dbo.CLIENTES.RPST_ID_CLI IS NULL OR dbo.CLIENTES.RPST_ID_CLI IN(0)) " +
                " AND (dbo.CLIENTES.RPST_SINCRONIZAR != 0  OR  dbo.CLIENTES.RPST_SINCRONIZAR IS  NULL) " + anexoClientesToCreate + 
                " ORDER BY dbo.CLIENTES.CODCLI  ";

            if (update)
            {
                //Para buscar qué cuentas hay que actualizar tenemos 3 opciones
                //forzar todos los clientes, por lo que prescindimos de la tabla Replog
                //en función de la tabla replog, el parametro accountsToUpdate será null
                //selección de clientes, donde también prescindimos de la tabla Replog
                anexoFiltroTablaReplog = string.IsNullOrEmpty(updateDate) ? "": " AND REPLOG.FECHA > '" + updateDate + "'" ;
                
                if (accountsToUpdate != null && accountsToUpdate!="ALL")
                {
                    anexoClientesToUpdate = " AND (LTRIM(dbo.CLIENTES.CODCLI) IN (" + accountsToUpdate + "))";
                    anexoJoinReplog = "";
                    anexoFechaReplog = "";
                    anexoFiltroTablaReplog = "";
                }
                if ((!cronjob && string.IsNullOrEmpty(updateDate)) || accountsToUpdate == "ALL") {
                    anexoJoinReplog = "";
                    anexoFechaReplog = "";
                    anexoFiltroTablaReplog = "";
                } 
                

                selectScript = "SELECT " +
                    " dbo.CLIENTES.RPST_ID_CLI, dbo.CLIENTES.NOMCLI AS nomCli, dbo.CLIENTES.RAZON AS razonSocialCli, dbo.CLIENTES.CLIALIAS AS aliasCli, dbo.CLIENTES.NIFCLI AS cifCli,  dbo.CLIENTES.E_MAIL AS emailCli, dbo.CLIENTES.PAGINAWEB AS webCli, dbo.CLIENTES.TELCLI AS tel1, dbo.CLIENTES.TELCLI2 AS tel2, dbo.CLIENTES.FAXCLI AS fax,    dbo.CLIENTES.DIASPAGO1 AS diaPago1Cli, dbo.CLIENTES.DIASPAGO2 AS diaPago2Cli, dbo.CLIENTES.DIASPAGO3 AS diaPago3Cli,  LTRIM(dbo.CLIENTES.CODCLI) AS codExternoCli,    dbo.FORMAPAG.RPST_ID_FORMAPAG  AS idFormaPago, dbo.DOCUPAGO.RPST_ID_DOCPAGO  AS idDocuPago, CASE WHEN dbo.CLIENTES.BLOQUEADO_ORG='T' THEN 0 ELSE 1 END AS activoCli,  dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, " +
                    " dbo.ZONAS.RPST_ID_ZONA  AS idZonaGeo, dbo.RUTAS.RPST_ID_RUTA AS idRuta, dbo.REPRESEN.RPST_ID_REPRE  AS idTrabajador  " + anexoFechaReplog +
                    " FROM dbo.CLIENTES " +
                    anexoJoinReplog +
                    " LEFT OUTER JOIN dbo.REPRESEN ON dbo.CLIENTES.CODREP = dbo.REPRESEN.CODREP " +
                    " LEFT OUTER JOIN dbo.RUTAS ON dbo.CLIENTES.RUTA = dbo.RUTAS.RUTA " +
                    " LEFT OUTER JOIN dbo.ZONAS ON dbo.CLIENTES.ZONA = dbo.ZONAS.ZONA " +
                    " LEFT OUTER JOIN dbo.DOCUPAGO ON dbo.CLIENTES.DOCPAG = dbo.DOCUPAGO.DOCPAG" +
                    " LEFT OUTER JOIN dbo.FORMAPAG ON dbo.CLIENTES.FORPAG = dbo.FORMAPAG.FORPAG" +
                    " LEFT OUTER JOIN dbo.REGIVA ON dbo.CLIENTES.REGIVA = dbo.REGIVA.REGIVA" +
                    " WHERE(dbo.CLIENTES.RPST_ID_CLI IS NOT NULL AND dbo.CLIENTES.RPST_ID_CLI NOT IN(0))" +
                    " AND(dbo.CLIENTES.RPST_SINCRONIZAR != 0  OR  dbo.CLIENTES.RPST_SINCRONIZAR IS  NULL)" +
                    anexoClientesToUpdate +
                    anexoFiltroTablaReplog + 
                    " GROUP BY" +
                    " dbo.CLIENTES.NOMCLI, dbo.CLIENTES.RAZON, dbo.CLIENTES.CLIALIAS, dbo.CLIENTES.NIFCLI,  dbo.CLIENTES.E_MAIL, " +
                    " dbo.CLIENTES.PAGINAWEB, dbo.CLIENTES.TELCLI, dbo.CLIENTES.TELCLI2, dbo.CLIENTES.FAXCLI," +
                    " dbo.CLIENTES.DIASPAGO1, dbo.CLIENTES.DIASPAGO2, dbo.CLIENTES.DIASPAGO3,  LTRIM(dbo.CLIENTES.CODCLI) ,    " +
                    " dbo.CLIENTES.RPST_ID_CLI, dbo.FORMAPAG.RPST_ID_FORMAPAG  , dbo.DOCUPAGO.RPST_ID_DOCPAGO ,dbo.CLIENTES.BLOQUEADO_ORG,  " +
                    " dbo.REGIVA.RPST_ID_REGIVA, dbo.ZONAS.RPST_ID_ZONA, dbo.RUTAS.RPST_ID_RUTA, dbo.REPRESEN.RPST_ID_REPRE";

            }

            return selectScript;
        
        }

        public string selectContactosFromA3ToRepasat(bool update = false, string updateDate = null)
        {
            string selectScript = "";

            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
            if (update) { updateDate = updateDataRPST.getLastUpdateSyncDate("accounts", false); }

            selectScript = " SELECT " +
                " dbo.CONTACTOS.CODIGO, dbo.CONTACTOS.EMAIL AS 'contact[emailContacto]',CONVERT(INT,dbo.CONTACTOS.IDCONTACTO) as 'contact[codExternoContacto]' , " +
                " UPPER(SUBSTRING(dbo.CONTACTOS.NOMBRE, 0, CHARINDEX(' ', dbo.CONTACTOS.NOMBRE, 1))) AS 'contact[nombreContacto]', " +
                " UPPER(SUBSTRING(dbo.CONTACTOS.NOMBRE, CHARINDEX(' ', dbo.CONTACTOS.NOMBRE, 1)+1, 99)) AS 'contact[apellidosContacto]', REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO1,'.',''),'-','') AS 'contact[telf1Contacto]',  " +
                " REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO2,'.',''),'-','') AS 'contact[telf2Contacto]', dbo.CONTACTOS.RPST_ID_CONTACT, " +
                " CASE WHEN RPST_ID_CLI IS NULL THEN RPST_ID_PROV ELSE RPST_ID_CLI END AS idCli " +
                " FROM dbo.CONTACTOS LEFT OUTER JOIN " +
                " dbo.PROVEED ON dbo.CONTACTOS.CODIGO = dbo.PROVEED.IDORG LEFT OUTER JOIN " +
                " dbo.CLIENTES ON dbo.CONTACTOS.CODIGO = dbo.CLIENTES.IDORG " +
                " WHERE(dbo.CONTACTOS.TABLA = 'ORG') AND (dbo.CONTACTOS.RPST_ID_CONTACT IS NULL OR dbo.CONTACTOS.RPST_ID_CONTACT = 0) " +
                " AND (CASE WHEN RPST_ID_CLI IS NULL THEN RPST_ID_PROV ELSE RPST_ID_CLI END IS NOT NULL)";

            if (update)
            {
                selectScript = "SELECT  " +
                    " dbo.CONTACTOS.CODIGO, dbo.CONTACTOS.EMAIL AS 'contact[emailContacto]', dbo.CONTACTOS.IDCONTACTO, dbo.CONTACTOS.NUMCON, " +
                    " REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO1,'.',''),'-','') AS 'contact[telf1Contacto]', REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO2,'.',''),'-','') AS 'contact[telf2Contacto]',  " +
                    " dbo.CONTACTOS.RPST_ID_CONTACT, dbo.CONTACTOS.NOMBRE, dbo.REPLOG.FECHA, dbo.REPLOG.IDREG1, dbo.REPLOG.IDREG2, dbo.REPLOG.IDREG3, dbo.REPLOG.MOVIMIENTO, dbo.REPLOG.TABLA " +
                    " FROM            dbo.CONTACTOS INNER JOIN " +
                    " dbo.REPLOG ON dbo.CONTACTOS.CODIGO = dbo.REPLOG.IDREG2 AND dbo.CONTACTOS.NUMCON = dbo.REPLOG.IDREG3 " +
                    " WHERE(dbo.CONTACTOS.TABLA = 'ORG') AND(dbo.REPLOG.MOVIMIENTO = 'MOD') AND(dbo.REPLOG.IDREP IN " +
                    " (SELECT MAX(IDREP) AS Expr1 " +
                    " FROM " +
                    " dbo.REPLOG AS REPLOG_1 " +
                    " WHERE(FECHA >= '" + updateDate + "') AND(TABLA = 'CONTACTOS') AND(MOVIMIENTO = 'MOD') " +
                    " GROUP BY IDREG2, IDREG3)) AND(dbo.CONTACTOS.RPST_ID_CONTACT > 0)";
            }

            return selectScript;

        }


        public string selectBancosFromA3ToRepasat(bool update = false, string updateDate = null, string idCuenta = null, bool ventas = true)
        {
            string selectScript = "";

            string anexoCuenta = "";

            anexoCuenta = string.IsNullOrEmpty(idCuenta) ? "" : (ventas ? " AND LTRIM(dbo.DOMBANCA.CODCLI)='" + idCuenta + "'" : " AND LTRIM(dbo.DOMBANCA.CODPRO) = '" + idCuenta + "'");

            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
            if (update) { updateDate = updateDataRPST.getLastUpdateSyncDate("banks", false); }

            selectScript = " SELECT " +
                " dbo.DOMBANCA.TITULAR AS 'bank[titularCuentaDatosBancarios]', dbo.DOMBANCA.NOMBAN AS 'bank[nomDatosBancarios]', " +
                " dbo.DOMBANCA.IBAN AS 'bank[ibanCuentaDatosBancarios]',  " +
                " dbo.DOMBANCA.BANCO AS 'bank[entidadCuentaDatosBancarios]', dbo.DOMBANCA.AGENCIA AS 'bank[agenciaCuentaDatosBancarios]',  " +
                " dbo.DOMBANCA.DIGITO AS 'bank[dcCuentaDatosBancarios]',  " +
                " CONCAT (dbo.DOMBANCA.IBAN, dbo.DOMBANCA.BANCO,dbo.DOMBANCA.AGENCIA, dbo.DOMBANCA.DIGITO, dbo.DOMBANCA.NUMCUENTA) as 'bank[ibanCompletoDatosBancarios]'," +
                " dbo.DOMBANCA.NUMCUENTA AS 'bank[numCuentaDatosBancarios]',  " +
                " CASE WHEN dbo.__PROVEED.RPST_ID_PROV IS NULL THEN dbo.__CLIENTES.RPST_ID_CLI ELSE dbo.__PROVEED.RPST_ID_PROV END AS idCli, " +
                " CASE WHEN dbo.__PROVEED.RPST_ID_PROV IS NULL THEN 'CLI' ELSE 'PRO' END AS tipo, " +
                " CONVERT(INT,dbo.DOMBANCA.IDDOMBANCA) AS 'bank[codExternoDatosBancarios]', " +
                " CASE WHEN DEFECTO='T' THEN 1 ELSE 0 END AS 'principalDatosBancarios' " +
                " FROM dbo.DOMBANCA LEFT OUTER JOIN " +
                " dbo.__PROVEED ON dbo.DOMBANCA.CODPRO = dbo.__PROVEED.CODPRO LEFT OUTER JOIN " +
                " dbo.__CLIENTES ON dbo.DOMBANCA.CODCLI = dbo.__CLIENTES.CODCLI " +
                " WHERE((CASE WHEN dbo.__PROVEED.RPST_ID_PROV IS NULL THEN dbo.__CLIENTES.RPST_ID_CLI ELSE dbo.__PROVEED.RPST_ID_PROV END) IS NOT NULL) " +
                " AND (RPST_ID_BANCO IS NULL OR RPST_ID_BANCO=0) " + anexoCuenta;

            if (update)
            {
                //selectScript = "SELECT  " +
                //    " dbo.CONTACTOS.CODIGO, dbo.CONTACTOS.EMAIL AS 'contact[emailContacto]', dbo.CONTACTOS.IDCONTACTO, dbo.CONTACTOS.NUMCON, " +
                //    " REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO1,'.',''),'-','') AS 'contact[telf1Contacto]', REPLACE(REPLACE(dbo.CONTACTOS.TELEFONO2,'.',''),'-','') AS 'contact[telf2Contacto]',  " +
                //    " dbo.CONTACTOS.RPST_ID_CONTACT, dbo.CONTACTOS.NOMBRE, dbo.REPLOG.FECHA, dbo.REPLOG.IDREG1, dbo.REPLOG.IDREG2, dbo.REPLOG.IDREG3, dbo.REPLOG.MOVIMIENTO, dbo.REPLOG.TABLA " +
                //    " FROM            dbo.CONTACTOS INNER JOIN " +
                //    " dbo.REPLOG ON dbo.CONTACTOS.CODIGO = dbo.REPLOG.IDREG2 AND dbo.CONTACTOS.NUMCON = dbo.REPLOG.IDREG3 " +
                //    " WHERE(dbo.CONTACTOS.TABLA = 'ORG') AND(dbo.REPLOG.MOVIMIENTO = 'MOD') AND(dbo.REPLOG.IDREP IN " +
                //    " (SELECT MAX(IDREP) AS Expr1 " +
                //    " FROM " +
                //    " dbo.REPLOG AS REPLOG_1 " +
                //    " WHERE(FECHA >= '" + updateDate + "') AND(TABLA = 'CONTACTOS') AND(MOVIMIENTO = 'MOD') " +
                //    " GROUP BY IDREG2, IDREG3)) AND(dbo.CONTACTOS.RPST_ID_CONTACT > 0)";
            }

            return selectScript;

        }
        public string selectProveedoresFromA3ToRepasat(bool update = false, string updateDate = null, string accountsToUpdate = null, bool cronjob=false)
        {
            string anexoCuentasToUpdate = "";
            string anexoClientesToCreate = "";
            string anexoInnerTablaReplog = "";
            string anexoFiltroTablaReplog = "";
            string anexoInnerTablaCliente = " dbo.PROVEED INNER JOIN  ";
            string selectScript = "";

            Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();

            updateDate = update ? updateDataRPST.getLastUpdateSyncDate("accounts", false) : "";
            anexoClientesToCreate = string.IsNullOrEmpty(accountsToUpdate) ? "" : " AND LTRIM(dbo.PROVEED.CODPRO) IN (" + accountsToUpdate + ")";

            selectScript = "  SELECT " +
                 " LTRIM(dbo.PROVEED.CODPRO) AS codExternoCli, dbo.PROVEED.NOMPRO AS nomCli, dbo.PROVEED.NIFPRO AS cifCli, CASE WHEN PROVEED.BLOQUEADO = 'T' THEN 0 ELSE 1 END AS activoCli,   " +
                 " dbo.PROVEED.DIASPAGO1 AS diaPago1Cli, dbo.PROVEED.DIASPAGO2 AS diaPago2Cli, dbo.PROVEED.DIASPAGO3 AS diaPago3Cli, dbo.PROVEED.OBSPRO AS descCli,   " +
                 " REPLACE(dbo.PROVEED.TELPRO, '.', '') AS tel1, dbo.PROVEED.RAZON AS razonSocialCli, dbo.PROVEED.PAGINAWEB AS webCli, dbo.PROVEED.E_MAIL AS emailCli, 'PRO' AS tipoCli, dbo.RUTAS.RPST_ID_RUTA AS idRuta,   " +
                 " dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago, dbo.REPRESEN.RPST_ID_REPRE AS idTrabajador, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago, REPLACE(dbo.PROVEED.FAXPRO, '.', '') AS fax, " +
                 " REPLACE(dbo.PROVEED.TEL2PRO, '.', '') AS tel2, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto " +
                 " FROM dbo.PROVEED WITH (NOLOCK)LEFT OUTER JOIN " +
                 " dbo.REGIVA ON dbo.PROVEED.REGIVA = dbo.REGIVA.REGIVA LEFT OUTER JOIN " +
                 " dbo.FORMAPAG ON dbo.PROVEED.FORPAG = dbo.FORMAPAG.FORPAG LEFT OUTER JOIN " +
                 " dbo.REPRESEN ON dbo.PROVEED.CODREP = dbo.REPRESEN.CODREP LEFT OUTER JOIN " +
                 " dbo.DOCUPAGO ON dbo.PROVEED.DOCPAG = dbo.DOCUPAGO.DOCPAG LEFT OUTER JOIN " +
                 " dbo.RUTAS ON dbo.PROVEED.RUTA = dbo.RUTAS.RUTA LEFT OUTER JOIN " +
                 " dbo.ZONAS ON dbo.PROVEED.ZONA = dbo.ZONAS.ZONA " +
                 " WHERE(dbo.PROVEED.RPST_ID_PROV IS NULL OR dbo.PROVEED.RPST_ID_PROV IN(0)) " +
                 " AND(dbo.PROVEED.RPST_SINCRONIZAR != 0  OR  dbo.PROVEED.RPST_SINCRONIZAR IS  NULL) " + anexoClientesToCreate +
                " ORDER BY dbo.PROVEED.CODPRO ";


            if (update)
            {
                //Para buscar qué cuentas hay que actualizar tenemos 3 opciones
                //forzar todos los clientes, por lo que prescindimos de la tabla Replog
                //en función de la tabla replog, el parametro accountsToUpdate será null
                //selección de clientes, donde también prescindimos de la tabla Replog
                if (accountsToUpdate == "ALL")
                {
                    anexoCuentasToUpdate = "";
                    anexoInnerTablaCliente = " dbo.PROVEED LEFT OUTER JOIN ";
                }
                else if (accountsToUpdate != null && accountsToUpdate != "ALL")
                {
                    anexoCuentasToUpdate = " WHERE (LTRIM(dbo.PROVEED.CODPRO) IN (" + accountsToUpdate + "))";
                    anexoInnerTablaCliente = " dbo.PROVEED LEFT OUTER JOIN ";
                }

                else if (accountsToUpdate == null)
                {
                    anexoInnerTablaReplog = " dbo.REPLOG ON dbo.PROVEED.CODPRO = dbo.REPLOG.IDREG1 LEFT OUTER JOIN  ";
                    anexoFiltroTablaReplog = " WHERE(dbo.REPLOG.IDREP IN  " +
                                                " (SELECT MAX(IDREP) AS Expr1  " +
                                                " FROM " +
                                                " dbo.REPLOG AS REPLOG_1  " +
                                                " WHERE(FECHA >= '" + updateDate + "') AND(TABLA = 'PROVEED') AND(MOVIMIENTO = 'MOD')  " +
                                                " GROUP BY IDREG1))";
                }
                selectScript = "SELECT " +
                    " dbo.PROVEED.NOMPRO AS nomCli, dbo.PROVEED.RAZON AS razonSocialCli, dbo.PROVEED.PROALIAS AS aliasCli, " +
                    " REPLACE(dbo.PROVEED.NIFPRO,'-','') AS cifCli, " +
                    " dbo.PROVEED.E_MAIL AS emailCli, dbo.PROVEED.PAGINAWEB AS webCli, " +
                    " REPLACE(REPLACE(dbo.PROVEED.TELPRO,'.',''),' ','') AS tel1, " +
                    " REPLACE(REPLACE(dbo.PROVEED.TEL2PRO,'.',''),' ','') AS tel2, " +
                    " REPLACE(REPLACE(dbo.PROVEED.FAXPRO,'.',''),' ','') AS fax, " +
                    " dbo.PROVEED.DIASPAGO1 AS diaPago1Cli, dbo.PROVEED.DIASPAGO2 AS diaPago2Cli, dbo.PROVEED.DIASPAGO3 AS diaPago3Cli,  LTRIM(dbo.PROVEED.CODPRO) AS codExternoCli, " +
                    " dbo.PROVEED.RPST_ID_PROV,dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago, dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago, CASE WHEN dbo.PROVEED.BLOQUEADO_ORG = 'T' THEN 0 ELSE 1 END AS activoCli, " +
                    " dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, dbo.ZONAS.RPST_ID_ZONA AS idZonaGeo, dbo.RUTAS.RPST_ID_RUTA AS idRuta, dbo.REPRESEN.RPST_ID_REPRE AS idTrabajador " +
                    " FROM " +
                    anexoInnerTablaCliente +
                    //Lo pasamos como parámetro para seleccionar directamente los proveedores de la tabla de proveedores
                    anexoInnerTablaReplog +
                    " dbo.REPRESEN ON dbo.PROVEED.CODREP = dbo.REPRESEN.CODREP LEFT OUTER JOIN " +
                    " dbo.RUTAS ON dbo.PROVEED.RUTA = dbo.RUTAS.RUTA LEFT OUTER JOIN " +
                    " dbo.ZONAS ON dbo.PROVEED.ZONA = dbo.ZONAS.ZONA LEFT OUTER JOIN " +
                    " dbo.DOCUPAGO ON dbo.PROVEED.DOCPAG = dbo.DOCUPAGO.DOCPAG LEFT OUTER JOIN " +
                    " dbo.FORMAPAG ON dbo.PROVEED.FORPAG = dbo.FORMAPAG.FORPAG LEFT OUTER JOIN " +
                    " dbo.REGIVA ON dbo.PROVEED.REGIVA = dbo.REGIVA.REGIVA " +
                    anexoFiltroTablaReplog + anexoCuentasToUpdate;

            }

            return selectScript;
        }



        public string selectDireccionesFromA3ToRepasat(string cuenta, bool cliente=true, bool update=false, string updateDate = null)
        {
            string selectScript = "";

            if (cliente && !update)
            {
                string anexoCuenta = string.IsNullOrEmpty(cuenta) ? "" : " WHERE LTRIM(DIRENT.CODCLI) = '" + cuenta + "'  and (RPST_ID_DIR IS NULL OR RPST_ID_DIR=0)";
                selectScript = " SELECT LTRIM(DIRENT.CODCLI) AS CODCLI, " +
                                    " CASE WHEN NOMENT IS NULL THEN 'PRINCIPAL' ELSE NOMENT END AS 'address[nomDireccion]',    " +
                                    " DIRENT1 AS 'address[direccion1Direccion]', " +
                                    " DIRENT2 AS 'address[direccion2Direccion]', " +
                                    " DTOENT AS 'address[cpDireccion]', " +
                                    " POBENT AS 'address[poblacionDireccion]', " +
                                    " CONVERT(INT,LTRIM(CODPROVI)) AS 'address[idProvincia]', " +
                                    " 'ESP' AS 'address[idPais]', " +
                                    " CASE WHEN DEFECTO='T' THEN 1 ELSE 0 END AS 'principalDireccion', " +
                                    " CASE WHEN DEFECTO='T' THEN 1 ELSE 0 END AS 'facturacionDireccion', " +
                                    " (SELECT RPST_ID_CLI  FROM __CLIENTES WHERE LTRIM(CODCLI)=ltrim(dirent.CODCLI)) AS idCli, " +
                                    " CONVERT(INT,IDDIRENT) AS 'address[codExternoDireccion]' " +
                                    " FROM DIRENT  INNER JOIN __CLIENTES ON __CLIENTES.CODCLI = DIRENT.CODCLI" + anexoCuenta;
            }
            else if (cliente && update)
            {
                selectScript = "SELECT LTRIM(dbo.DIRENT.CODCLI) AS CODCLI, " +
                                    " CASE WHEN NOMENT IS NULL THEN 'PRINCIPAL' ELSE NOMENT END AS 'address[nomDireccion]', " +
                                    " UPPER(dbo.DIRENT.DIRENT1) AS 'address[direccion1Direccion]', " +
                                    " dbo.DIRENT.DIRENT2 AS 'address[direccion2Direccion]', dbo.DIRENT.DTOENT AS 'address[cpDireccion]', " +
                                    " dbo.DIRENT.POBENT AS 'address[poblacionDireccion]', CONVERT(INT, LTRIM(dbo.DIRENT.CODPROVI)) AS 'address[idProvincia]', " +
                                    " 'ESP' AS 'address[idPais]', CASE WHEN DEFECTO = 'T' THEN 1 ELSE 0 END AS 'principalDireccion', " +
                                    " CASE WHEN DEFECTO = 'T' THEN 1 ELSE 0 END AS 'facturacionDireccion', " +
                                    " (SELECT RPST_ID_CLI FROM dbo.__CLIENTES WHERE(LTRIM(CODCLI) = '')) AS idCli, " +
                                    " CONVERT(INT, dbo.DIRENT.IDDIRENT) AS 'address[codExternoDireccion]', dbo.DIRENT.RPST_ID_DIR as idDireccion " +
                                    " FROM dbo.DIRENT INNER JOIN " +
                                    " dbo.REPLOG ON dbo.DIRENT.CODCLI = dbo.REPLOG.IDREG1 AND dbo.DIRENT.NUMDIR = dbo.REPLOG.IDREG2 " +
                                    " WHERE(dbo.DIRENT.RPST_ID_DIR > 0) AND(dbo.REPLOG.IDREP IN " +
                                    " (SELECT MAX(IDREP) AS ID " +
                                    " FROM dbo.REPLOG AS REPLOG_1 " +
                                    " WHERE(FECHA >= '" + updateDate + "') AND(TABLA = 'DIRENT') AND(MOVIMIENTO = 'MOD') " +
                                    " GROUP BY IDREG1))";
            }
            else if (!cliente && !update)
            {
                selectScript = " SELECT LTRIM(CODPRO) AS CODPRO, " +
                                    " NOMENT AS 'address[nomDireccion]',   " +
                                    " DIRENT1 AS 'address[direccion1Direccion]',  " +
                                    " DIRENT2 AS 'address[direccion2Direccion]',  " +
                                    " DTOENT AS 'address[cpDireccion]',  " +
                                    " POBENT AS 'address[poblacionDireccion]',  " +
                                    " CONVERT(INT,LTRIM(CODPROVI)) AS 'address[idProvincia]',  " +
                                    " 'ESP' AS 'address[idPais]',  " +
                                    " CASE WHEN DEFECTO='T' THEN 1 ELSE 0 END AS 'principalDireccion',  " +
                                    " CASE WHEN DEFECTO='T' THEN 1 ELSE 0 END AS 'facturacionDireccion',  " +
                                    " (SELECT RPST_ID_PROV FROM __PROVEED WHERE LTRIM(CODPRO)='" + cuenta + "') AS idCli,  " +
                                    " CONVERT(INT,IDDIRENT) AS 'address[codExternoDireccion]'  " +
                                    " FROM __direntpro WHERE LTRIM(CODPRO)='" + cuenta + "'";

            }
            else if (!cliente && update)
            {
                selectScript = "SELECT " +
                    " LTRIM(dbo.__DIRENTPRO.CODPRO) AS CODCLI, CASE WHEN NOMENT IS NULL THEN 'PRINCIPAL' ELSE NOMENT END AS 'address[nomDireccion]', " +
                    " dbo.__DIRENTPRO.DIRENT1 AS 'address[direccion1Direccion]', " +
                    " dbo.__DIRENTPRO.DIRENT2 AS 'address[direccion2Direccion]', dbo.__DIRENTPRO.DTOENT AS 'address[cpDireccion]', " +
                    " dbo.__DIRENTPRO.POBENT AS 'address[poblacionDireccion]', " +
                    " CONVERT(INT, LTRIM(dbo.__DIRENTPRO.CODPROVI)) AS 'address[idProvincia]',  " +
                    " 'ESP' AS 'address[idPais]', CASE WHEN DEFECTO = 'T' THEN 1 ELSE 0 END AS 'principalDireccion', " +
                    " CASE WHEN DEFECTO = 'T' THEN 1 ELSE 0 END AS 'facturacionDireccion', " +
                    " (SELECT RPST_ID_PROV FROM dbo.PROVEED WHERE(LTRIM(CODPRO) = '')) AS idCli, " +
                    " CONVERT(INT, dbo.__DIRENTPRO.IDDIRENT) AS 'address[codExternoDireccion]',dbo.DIRENT.RPST_ID_DIRPROV as idDireccion " +
                    " FROM " +
                    " dbo.__DIRENTPRO INNER JOIN " +
                    " dbo.REPLOG ON dbo.__DIRENTPRO.CODPRO = dbo.REPLOG.IDREG1 AND dbo.__DIRENTPRO.NUMDIR = dbo.REPLOG.IDREG2 " +
                    " WHERE(dbo.__DIRENTPRO.RPST_ID_DIRPROV > 0) AND(dbo.REPLOG.IDREP IN " +
                    " (SELECT MAX(IDREP) AS ID  " +
                    " FROM dbo.REPLOG AS REPLOG_1 " +
                    " WHERE(FECHA >= '" + updateDate + "') AND(TABLA = '__DIRENTPRO') AND(MOVIMIENTO = 'MOD') " +
                    " GROUP BY IDREG1))";

            }
            return selectScript;
        
        }


        public string selectProductosFromA3ToRepasat(bool update = false, string updateDate = null)
        {

            string selectScript = "";

            string anexo_select_familia = csGlobal.familia_art == null || csGlobal.familia_art == "" ? "" : " C" + csGlobal.familia_art + ".RPST_ID_CARACTERISTICA AS idFam, ";
            string anexo_where_familia = csGlobal.familia_art == null || csGlobal.familia_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.familia_art + " ON CAR" + csGlobal.familia_art + " = C" + csGlobal.familia_art + ".CODCAR and C" + csGlobal.familia_art + ".TIPCAR='A' AND C" + csGlobal.familia_art + ".NUMCAR='" + csGlobal.familia_art + "' ";
            string anexo_select_subfamilia = csGlobal.subfamilia_art == null || csGlobal.subfamilia_art == "" ? "" : " C" + csGlobal.subfamilia_art + ".RPST_ID_CARACTERISTICA AS idSubFam, ";
            string anexo_where_subfamilia = csGlobal.subfamilia_art == null || csGlobal.subfamilia_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.subfamilia_art + " ON CAR" + csGlobal.subfamilia_art + " = C" + csGlobal.subfamilia_art + ".CODCAR and C" + csGlobal.subfamilia_art + ".TIPCAR='A' AND C" + csGlobal.subfamilia_art + ".NUMCAR='" + csGlobal.subfamilia_art + "' ";
            string anexo_select_marca = csGlobal.marcas_art == null || csGlobal.marcas_art == "" ? "" : " C" + csGlobal.marcas_art + ".RPST_ID_CARACTERISTICA AS uuidMarca, ";
            string anexo_where_marca = csGlobal.marcas_art == null || csGlobal.marcas_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.marcas_art + " ON CAR" + csGlobal.marcas_art + " = C" + csGlobal.marcas_art + ".CODCAR and C" + csGlobal.marcas_art + ".TIPCAR='A' AND C" + csGlobal.marcas_art + ".NUMCAR='" + csGlobal.marcas_art + "' ";
            string anexo_filtroConstante = csGlobal.filtro_art == null || csGlobal.filtro_art == "" ? "" : csGlobal.filtro_art;
            //09/02/2024 AÑADO ANEXO PARA EL TIPO PRODUCTO
            string anexo_select_tipo = csGlobal.tipo_art == null || csGlobal.tipo_art == "" ? "" : " C" + csGlobal.tipo_art + ".RPST_ID_CARACTERISTICA AS idTipoArticulo, ";
            string anexo_where_tipo = csGlobal.tipo_art == null || csGlobal.tipo_art == "" ? "" : " left outer JOIN CARACTERISTICAS C" + csGlobal.tipo_art + " ON CAR" + csGlobal.tipo_art + " = C" + csGlobal.tipo_art + ".CODCAR and C" + csGlobal.tipo_art + ".TIPCAR='A' AND C" + csGlobal.tipo_art + ".NUMCAR='" + csGlobal.tipo_art + "' ";

            string mayusculasAlias = csGlobal.nombreServidor.Contains("PURE") || csGlobal.nombreServidor.Contains("PRUEBAS") ? " (ARTALIAS) as aliasArticulo, " : " UPPER(ARTALIAS) as aliasArticulo, ";
            string mayusculasDesc = csGlobal.nombreServidor.Contains("PURE") || csGlobal.nombreServidor.Contains("PRUEBAS") ? " (A.DESCART) AS nomArticulo, " : " UPPER(A.DESCART) AS nomArticulo, ";

            if (!update)
            {
                selectScript = " SELECT LTRIM(A.CODART) as CODIGO, RPST_ID_PROD AS REPASAT_ID, DESCART as nomArticulo, " +
                 " CASE WHEN A.RPST_SINCRONIZAR=1 then 'SI' ELSE 'NO' END AS SYNC, " +
                    //COMROBAMOS SI HAY UN PRECIO EN LA TARIFA SINO PONEMOS EL ORIGINAL
                    " CASE WHEN T.PRECIO=0 OR T.PRECIO IS NULL then PRCVENTA ELSE T.PRECIO END AS precioV, " +
                    " PRCCOMPRA as precioC, " +
                    anexo_select_familia + anexo_select_subfamilia + anexo_select_tipo + anexo_select_marca +
                    " CASE WHEN ESCOMPRA ='T' THEN '1' ELSE '0' END as compraArticulo,  " +
                    " CASE WHEN ESVENTA ='T' THEN '1' ELSE '0' END as ventaArticulo,  CASE WHEN BLOQUEADO ='T' THEN '0' ELSE '1' END as activoArticulo,  " +
                    " CASE WHEN AFESTOCK ='T' THEN '1' ELSE '0' END as stockArticulo,  CASE WHEN OBSOLETO ='T' THEN 'SI' ELSE 'NO' END as obsoletoArticulo,  FECALTA AS FECHAALTA, " +
                    mayusculasAlias + 
                    " LTRIM(A.CODART) as codExternoArticulo, LTRIM(RPST_ID_PROD) AS idArticulo, LTRIM(A.CODART) AS refArticulo " +
                    " FROM ARTICULO A " +
                    anexo_where_familia +
                    anexo_where_subfamilia +
                    anexo_where_tipo +
                    anexo_where_marca +
                    " left outer JOIN TARIFAVE T ON A.CODART = T.CODART and T.TARIFA = 8 " +
                    " WHERE A.CODART IS NOT NULL "+
                    " AND RPST_ID_PROD IS NULL OR RPST_ID_PROD = 0  " +
                    " ORDER BY A.CODART";
            }
            else
            {
                selectScript="SELECT " +
                    " LTRIM(A.CODART) AS codExternoArticulo, RPST_ID_PROD AS idArticulo, LTRIM(A.CODART) AS refArticulo, "+ mayusculasDesc +
                    " CASE WHEN A.RPST_SINCRONIZAR=1 then 'SI' ELSE 'NO' END AS SYNC, " +
                    mayusculasAlias +
                    " A.ARTPRO AS  referenciaProveedorArticulo, " +
                    " CASE WHEN T.PRECIO=0 OR T.PRECIO IS NULL then PRCVENTA ELSE T.PRECIO END AS precioV, " +
                    " PRCCOMPRA as precioC, " +
                    anexo_select_familia + anexo_select_subfamilia + anexo_select_tipo + anexo_select_marca +
                    " CASE WHEN ESCOMPRA ='T' THEN '1' ELSE '0' END as compraArticulo,  " +
                    " CASE WHEN ESVENTA ='T' THEN '1' ELSE '0' END as ventaArticulo,  CASE WHEN BLOQUEADO ='T' THEN '0' ELSE '1' END as activoArticulo,  " +
                    " CASE WHEN AFESTOCK ='T' THEN '1' ELSE '0' END as stockArticulo,  CASE WHEN OBSOLETO ='T' THEN 'SI' ELSE 'NO' END as obsoletoArticulo,  FECALTA AS FECHAALTA, " +
                    " CASE WHEN BLOQUEADO = 'T' THEN '0' ELSE '1' END AS activoArticulo,  " +
                    " dbo.REPLOG.FECHA, dbo.REPLOG.IDREG1, dbo.REPLOG.TABLA, dbo.REPLOG.MOVIMIENTO " +
                    " FROM " +
                    " dbo.ARTICULO A INNER JOIN dbo.REPLOG ON A.CODART = dbo.REPLOG.IDREG1 " +
                    anexo_where_familia +
                    anexo_where_subfamilia +
                    anexo_where_tipo +
                    anexo_where_marca +
                    " left outer JOIN TARIFAVE T ON A.CODART = T.CODART and T.TARIFA = 8 " +
                    " WHERE A.CODART IS NOT NULL " + anexo_filtroConstante +
                    " AND RPST_ID_PROD>0 and (dbo.REPLOG.IDREP IN " +
                    " (SELECT MAX(IDREP) AS ID " +
                    " FROM dbo.REPLOG AS REPLOG_1 " +
                    " WHERE (FECHA >= '" + updateDate + "') AND (TABLA = 'ARTICULO') AND (MOVIMIENTO = 'MOD') " +
                    " GROUP BY IDREG1))";
            }

            return selectScript;
        
        }

        // 0 = todos
        // 1 = con codigo
        // 2 = sin codigo
        public string selectClientesA3(string estadoSincronizacion="TODOS" , int code=0, string searchText="", string sincronizar="TODOS", string bloqueado="TODOS")
        {
            try
            {
                string filtroQuery = " WHERE CODCLI IS NOT NULL ";

                //PARA ANDREUTOYS
                bool existeColumnaPedidoMinimo = false;
                existeColumnaPedidoMinimo = csUtilidades.existeColumnaSQL("__CLIENTES", "KLS_PEDIDO_MINIMO");
                string anexoPedidoMinimo = existeColumnaPedidoMinimo ? " ,KLS_PEDIDO_MINIMO " : "";

                string filtroTexto = " AND (E_MAIL_DOCS LIKE '%" + searchText + "%' OR " +
                    " NIFCLI LIKE '%" + searchText + "%' OR " +
                    " KLS_EMAIL_USUARIO LIKE '%" + searchText + "%' OR " +
                    " CODCLI LIKE '%" + searchText + "%' OR " +
                    " CONTACTO LIKE '%" + searchText + "%' OR" +
                    " KLS_NOM_USUARIO LIKE '%" + searchText + "%' OR" +
                    " KLS_APELL_USUARIO LIKE '%" + searchText + "%' OR" +
                    " RAZON LIKE '%" + searchText + "%') ";

                filtroTexto = string.IsNullOrEmpty(searchText) ? "" : filtroTexto;

                string condicion = "";

                string filtroEstadoSincronizacion = "";
                filtroEstadoSincronizacion = estadoSincronizacion == "SI" ? " AND KLS_CODCLIENTE > 0 " : filtroEstadoSincronizacion;

                filtroEstadoSincronizacion = estadoSincronizacion == "NO" ? " AND (KLS_CODCLIENTE IS NULL OR KLS_CODCLIENTE = 0 OR KLS_CODCLIENTE = '') " : filtroEstadoSincronizacion;


                filtroEstadoSincronizacion = estadoSincronizacion == "TODOS" ? "" : filtroEstadoSincronizacion;

                string filtroEstadoBloqueo = "";
                filtroEstadoBloqueo = bloqueado == "SI" ? " AND BLOQUEADO = 'T' " : filtroEstadoBloqueo;
                filtroEstadoBloqueo = bloqueado == "NO" ? " AND BLOQUEADO = 'F' " : filtroEstadoBloqueo;


                string filtroMarcaSincronizar = "";
                filtroMarcaSincronizar = bloqueado == "SI" ? " AND KLS_CLIENTE_TIENDA='T' " : filtroMarcaSincronizar;
                //filtroMarcaSincronizar = bloqueado == "NO" ? "AND KLS_CLIENTE_TIENDA='F' " : filtroMarcaSincronizar;
                filtroMarcaSincronizar = bloqueado == "NO" ? "AND (KLS_CLIENTE_TIENDA='F' OR KLS_CLIENTE_TIENDA='') " : filtroMarcaSincronizar;

                string filtroMalFormateado = code == 3 ? " AND (E_MAIL_DOCS = '' OR (E_MAIL_DOCS LIKE '%,%' OR E_MAIL_DOCS LIKE '%;%' OR E_MAIL_DOCS LIKE '%/%')) " : "";

                condicion = filtroQuery + filtroEstadoSincronizacion + filtroEstadoBloqueo + filtroMarcaSincronizar + filtroMalFormateado + filtroTexto;

                string selectScript = " SELECT CLIENTES.CODCLI, NOMCLI AS RAZON, NIFCLI, KLS_EMAIL_USUARIO, KLS_CODCLIENTE, " +
                                      " KLS_CLIENTE_TIENDA, KLS_NOM_USUARIO, KLS_APELL_USUARIO, E_MAIL_DOCS, BLOQUEADO, REGIVA " + anexoPedidoMinimo + 
                                      " FROM dbo.CLIENTES  WITH (NOLOCK) " + condicion + 
                                      " ORDER BY E_MAIL_DOCS desc";

                selectScript = code == 4 ? "SELECT E_MAIL_DOCS AS EMAIL, COUNT(*) AS NUMERO_REPETICIONES FROM dbo.CLIENTES  WITH (NOLOCK) " +
                                    "WHERE E_MAIL_DOCS NOT LIKE '%,%' " +
                                    "  AND E_MAIL_DOCS NOT LIKE '%;%' " +
                                    "  AND BLOQUEADO = 'F' AND (" + filtroTexto + ")" +
                                    "GROUP BY E_MAIL_DOCS HAVING " +
                                    "    COUNT(*) > 1" : selectScript;

                return selectScript;
            }
            catch { return null; }
        }

        public string selectFamilias()
        {
            return "SELECT KLS_CODCATEGORIA as COD_PS, KLS_DESCCATEGORIA AS DESCFAM FROM dbo.KLS_CATEGORIAS WITH (NOLOCK) ";
        }

        // DESCUENTOS
        public string selectDescuento1ArtCli()
        {
            string consulta = "SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, " +
                    " dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, dbo.DESCUENT.FAMART,dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, " +
                    " dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'artcli' AS tipoconsulta " +
                    " FROM " +
                    " dbo.__CLIENTES  WITH (NOLOCK) INNER JOIN dbo.DESCUENT ON dbo.__CLIENTES.CODCLI = dbo.DESCUENT.CODCLI " +
                    " INNER JOIN dbo.ARTICULO ON dbo.DESCUENT.CODART = dbo.ARTICULO.CODART " +
                    " WHERE (dbo.__CLIENTES.KLS_CODCLIENTE <> '') " +
                    " AND (dbo.DESCUENT.DESC1 >= 0) " +
                    " AND (dbo.ARTICULO.KLS_ID_SHOP <> '')  AND (DESCUENT.CODPRO IS NULL) AND(DESCUENT.fecmax > GETDATE())";

            return consulta;
        }

        public string selectDescuento2ArtFamCli()
        {
            string consulta = "SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, " +
                      " LTRIM(dbo.DESCUENT.FAMART) AS FAMART, LTRIM(dbo.DESCUENT.FAMCLI) AS FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, " +
                      " 'artfamcli' AS tipoconsulta " +
                      " FROM         dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                      " dbo.DESCUENT ON dbo.ARTICULO.CODART = dbo.DESCUENT.CODART INNER JOIN " +
                      " dbo.__CLIENTES ON dbo.DESCUENT.FAMCLI = dbo.__CLIENTES.FAMCLIDESC " +
                      " WHERE     (dbo.__CLIENTES.KLS_CODCLIENTE <> '') AND (dbo.DESCUENT.DESC1 > 0) AND (dbo.ARTICULO.KLS_ID_SHOP <> '')";

            return consulta;
        }

        /// <summary>
        /// Para todos los clientes (cuando no tiene familia de clientes especificada). Aunque no tengan familia de clientes se le puede 
        /// </summary>
        /// <returns></returns>
        public string selectDescuento21ArtFamCli()
        {
            /*string consulta = "SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, 'DESCUENTO' AS TIPO, 'artfamcli' AS tipoconsulta " +
                              " FROM dbo.ARTICULO INNER JOIN dbo.DESCUENT ON dbo.ARTICULO.CODART = dbo.DESCUENT.CODART WHERE (dbo.DESCUENT.DESC1 > 0) " +
                              " AND (dbo.ARTICULO.KLS_ID_SHOP <> '')  AND (dbo.DESCUENT.FAMART IS NULL)   AND (dbo.DESCUENT.CODCLI IS NULL)"; */

            string consulta = "SELECT dbo.ARTICULO.CODART, " +
                                "       dbo.ARTICULO.KLS_ID_SHOP, " +
                                "       dbo.ARTICULO.DESCART, " +
                                "       dbo.ARTICULO.FAMARTDESCVEN, " +
                                "       dbo.DESCUENT.DESC1," +
                                "       dbo.DESCUENT.FECMIN, " +
                                "       dbo.DESCUENT.FECMAX, " +
                                "       LTRIM(dbo.DESCUENT.FAMART) AS FAMART, " +
                                "       LTRIM(dbo.DESCUENT.FAMCLI) AS FAMCLI, " +
                                "       dbo.__CLIENTES.KLS_CODCLIENTE, " +
                                "       'DESCUENTO' AS TIPO, " +
                                "       'artfamcli' AS tipoconsulta " +
                                " FROM dbo.ARTICULO WITH (NOLOCK) " +
                                " INNER JOIN dbo.DESCUENT ON dbo.ARTICULO.CODART = dbo.DESCUENT.CODART " +
                                " LEFT JOIN __CLIENTES ON __CLIENTES.CODCLI = DESCUENT.CODCLI " +
                                " WHERE (dbo.DESCUENT.DESC1 > 0) " +
                                "  AND (dbo.ARTICULO.KLS_ID_SHOP <> '') " +
                                "  AND (dbo.DESCUENT.FAMART IS NULL) " +
                                "  AND (dbo.__CLIENTES.KLS_CODCLIENTE IS NULL)";

            return consulta;
        }


        //SELECT PARA CLIENTES CON FAMILIAS DE ARTÍCULOS
        public string selectDescuento3_bis_FamArtCli()
        {

            // 14/10/2016 AÑADIMOS FILTRO FAMART IS NOT NULL
            string consulta = " SELECT " +
                        " dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, LTRIM(dbo.DESCUENT.FAMART) AS FAMART, LTRIM(dbo.DESCUENT.FAMCLI) AS FAMCLI, 'DESCUENTO' AS TIPO, " +
                        "  'famclifamart' AS tipoconsulta, dbo.__CLIENTES.KLS_CODCLIENTE, LTRIM(dbo.DESCUENT.CODCLI) AS CODCLI, 0 AS KLS_ID_SHOP, UNIDADES " +
                        " FROM   " +
                        " dbo.DESCUENT WITH (NOLOCK)  INNER JOIN dbo.__CLIENTES ON dbo.DESCUENT.CODCLI = dbo.__CLIENTES.CODCLI " +
                        " WHERE " +
                        " (dbo.DESCUENT.DESC1 > 0) AND (dbo.DESCUENT.CODCLI > 0) AND (dbo.__CLIENTES.KLS_CODCLIENTE > 0) AND (dbo.DESCUENT.FAMCLI IS NULL)" +
                        " AND (dbo.DESCUENT.FAMART IS NOT NULL) AND (CODPRO IS NULL)";

            

            return consulta;
        }


        // FAMILIA DE ARTICULOS SIN CLIENTES
        public string selectDescuento31_bis_FamArtCli()
        {
            // 14/10/2016 AÑADIMOS FILTRO FAMART IS NOT NULL
            string consulta = " SELECT " +
                                " UNIDADES, DESC1, FECMIN, FECMAX, FAMART, 0 AS KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'famclifamart' AS tipoconsulta, 0 AS KLS_ID_SHOP, LTRIM(FAMCLI) AS FAMCLI" +
                                " FROM " +
                                " dbo.DESCUENT  WITH (NOLOCK) " +
                                " WHERE " +
                                " (DESC1 > 0) AND (CODCLI IS NULL) AND (FAMCLI IS NULL)" +
                                " AND (FAMART IS NOT NULL)  AND (CODPRO IS NULL) AND(DESCUENT.fecmax > GETDATE())";

            return consulta;

        }

        public string selectDescuento3FamArtCli()
        {
            string consulta = " SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX,  " +
                    " LTRIM(dbo.DESCUENT.FAMART) as FAMART, LTRIM(dbo.DESCUENT.FAMCLI) AS FAMCLI, dbo.__CLIENTES.FAMCLIDESC, LTRIM(dbo.__CLIENTES.CODCLI) AS CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, " +
                    " 'famartcli' AS tipoconsulta " +
                    " FROM dbo.__CLIENTES  WITH (NOLOCK) INNER JOIN " +
                    " dbo.DESCUENT ON dbo.__CLIENTES.CODCLI = dbo.DESCUENT.CODCLI INNER JOIN " +
                    " dbo.ARTICULO ON dbo.DESCUENT.FAMART = dbo.ARTICULO.FAMARTDESCVEN " +
                    " WHERE (dbo.__CLIENTES.KLS_CODCLIENTE <> '') AND (dbo.DESCUENT.DESC1 > 0) AND (dbo.ARTICULO.KLS_ID_SHOP <> '')";

            return consulta;
        }

        /// <summary>
        /// Asignar un mismo descuento a un cliente para todos los artículos
        /// </summary>
        /// <returns></returns>
        public string selectDescuento31FamArtCli()
        {
            string consulta = " SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX,  " +
                    " dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, " +
                    " 'famartcli' AS tipoconsulta " +
                    " FROM dbo.__CLIENTES  WITH (NOLOCK) INNER JOIN " +
                    " dbo.DESCUENT ON dbo.__CLIENTES.CODCLI = dbo.DESCUENT.CODCLI INNER JOIN " +
                    " dbo.ARTICULO ON dbo.DESCUENT.FAMART = dbo.ARTICULO.FAMARTDESCVEN " +
                    " WHERE (dbo.__CLIENTES.KLS_CODCLIENTE <> '') AND (dbo.DESCUENT.DESC1 > 0) AND (dbo.ARTICULO.KLS_ID_SHOP <> '')";

            return consulta;
        }

        public string selectDescuento4FamCliFamArt()
        {
            //string consulta =   "SELECT [FAMART] " +
            //                    "      ,[FAMCLI] " +
            //                    "      ,[FECMAX] " +
            //                    "      ,[FECMIN] " +
            //                    "       ,FECMIN " +
            //                    " ,dbo.DESCUENT.DESC1 " +
            //                    "       ,FECMAX " +
            //                    " ',DESCUENTO' AS TIPO " +
            //                    "      ,[UNIDADES] " +
            //                    "      ,[ID] " +
            //                    "  FROM [DESCUENT] " +
            //                    "  WHERE FAMCLI IS NOT NULL AND FAMART IS NOT NULL";

            string consulta = " SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX,  " +
                    " dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO,  " +
                    " 'famclifamart' AS tipoconsulta " +
                    " FROM dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                    " dbo.DESCUENT ON dbo.ARTICULO.FAMARTDESCVEN = dbo.DESCUENT.FAMART INNER JOIN " +
                    " dbo.__CLIENTES ON dbo.DESCUENT.FAMCLI = dbo.__CLIENTES.FAMCLIDESC " +
                    " WHERE (dbo.__CLIENTES.KLS_CODCLIENTE <> '') AND (dbo.DESCUENT.DESC1 > 0) AND (dbo.ARTICULO.KLS_ID_SHOP <> '') AND (DESCUENT.CODCLI IS NULL) AND (dbo.DESCUENT.CODART IS NULL)";

            return consulta;
        }

        public string selectDescuento5_1_FamCliFamArt()
        {
            // 14/10/2016 FILTRO CODPRO IS NULL
            string consulta = " SELECT     CODART, CODCLI, CODPRO, DESC1, DESC2, DESC3, DESC4, FAMART, FAMCLI, FECMAX, FECMIN, ID, TIPREG, CAST(UNIDADES AS INT) AS UNIDADES, 0 AS KLS_ID_SHOP, 0 AS KLS_CODCLIENTE, 'DESCUENTO' AS TIPO  " +
                                " FROM  " +
                                " dbo.DESCUENT  WITH (NOLOCK) " +
                                " WHERE " +
                                " (CODCLI IS NULL) AND (CODART IS NULL) AND (FAMCLI IS NOT NULL) AND (FAMART IS NOT NULL) AND (CODPRO IS NULL) AND(DESCUENT.fecmax > GETDATE())";

            return consulta;
        }
        // FIN DESCUENTOS

        public string selectDescuentosFamCliArticuloCasuisticaDismay()
        {
            string consulta = "SELECT DESCUENT.CODART, " +
                                "       DESCUENT.CODCLI, " +
                                "       DESCUENT.CODPRO, " +
                                "       DESCUENT.DESC1, " +
                                "       DESCUENT.DESC2, " +
                                "       DESCUENT.DESC3, " +
                                "       DESCUENT.DESC4, " +
                                "       DESCUENT.FAMART, " +
                                "       DESCUENT.FAMCLI, " +
                                "       DESCUENT.FECMAX, " +
                                "       DESCUENT.FECMIN, " +
                                "       DESCUENT.ID, " +
                                "       DESCUENT.TIPREG, " +
                                "       DESCUENT.UNIDADES, " +
                                "       ARTICULO.KLS_ID_SHOP, " +
                                "       0 AS KLS_CODCLIENTE, " +
                                "       'DESCUENTO' AS TIPO " +
                                "FROM dbo.DESCUENT   " +
                                "LEFT JOIN ARTICULO ON ARTICULO.CODART = DESCUENT.CODART " +
                                "WHERE (CODCLI IS NULL) " +
                                "  AND (FAMCLI IS NOT NULL) " +
                                "  AND (FAMART IS NULL) " +
                                "  AND (DESCUENT.CODPRO IS NULL) " +
                                "  AND (0 IS NOT NULL) " +
                                "  AND KLS_ID_SHOP > 0  AND(DESCUENT.fecmax > GETDATE())";
            //Se modifica para Dismay por que tenian varios grupos de cliente que se estaban traspasando a la tienda y solo utilizan el BASICO en Prestashop.
            if (csGlobal.nombreServidor.Contains("DISMAY"))
            {
                //consulta += " AND DESCUENT.FAMCLI='BASICO'";
            }

            return consulta;
        }

        public string selectArticuloParaTodosLosClientes()
        {
            string sb = "SELECT LTRIM(descuent.CODART) AS CODART, " +
                                                        "KLS_ID_SHOP, " +
                                                        "descuent.DESC1, " +
                                                        "       FECMIN, " +
                                                        "       FECMAX, " +
                                                        "       FAMART, " +
                                                        "       0 AS KLS_CODCLIENTE, " +
                                                        "       'DESCUENTO' AS TIPO, " +
                                                        "       'famclifamart' AS tipoconsulta, " +
                                                        "       FAMCLI " +
                                                        "FROM dbo.DESCUENT WITH (NOLOCK) " +
                                                        "left join articulo on articulo.codart = descuent.codart " +
                                                        "WHERE (descuent.DESC1 > 0) " +
                                                        "  AND (CODCLI IS NULL) " +
                                                        "  AND (FAMCLI IS NULL) " +
                                                        "  AND (FAMART IS NULL) " +
                                                        "  AND (descuent.CODPRO IS NULL) AND (dbo.ARTICULO.KLS_ID_SHOP > 0) AND(DESCUENT.fecmax > GETDATE()); ";

            return sb;
        }



        public string selectTarifaDescuentosA3()
        {
            string selectScript =
         "SELECT " +
             " dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, " +
             "dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'famclifamart' as tipoconsulta" +
         " FROM " +
             "dbo.__CLIENTES WITH (NOLOCK)  RIGHT OUTER JOIN" +
             " dbo.DESCUENT ON dbo.__CLIENTES.FAMCLIDESC = dbo.DESCUENT.FAMCLI LEFT OUTER JOIN" +
             " dbo.ARTICULO ON dbo.DESCUENT.FAMART = dbo.ARTICULO.FAMARTDESCVEN " +
         "WHERE " +
             "(dbo.__CLIENTES.KLS_CODCLIENTE <> '') AND (dbo.DESCUENT.DESC1 > 0) AND KLS_ID_SHOP <> '' " +
             "union " +
           " SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, " +
           " dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'famclifamart' as tipoconsulta " +
         " FROM dbo.__CLIENTES  WITH (NOLOCK) RIGHT OUTER JOIN " +
           " dbo.DESCUENT ON dbo.__CLIENTES.CODCLI = dbo.DESCUENT.CODCLI LEFT OUTER JOIN " +
           " dbo.ARTICULO ON dbo.DESCUENT.FAMART = dbo.ARTICULO.FAMARTDESCVEN " +
         " WHERE     (dbo.DESCUENT.DESC1 > 0) AND (dbo.__CLIENTES.KLS_CODCLIENTE <> '') " +
        "union " +
           "  SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, " +
           " dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, dbo.__CLIENTES.FAMCLIDESC, dbo.__CLIENTES.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'famclifamart' as tipoconsulta " +
         " FROM dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                      " dbo.DESCUENT ON dbo.ARTICULO.FAMARTDESCVEN = dbo.DESCUENT.FAMART INNER JOIN " +
                      " dbo.__CLIENTES ON dbo.DESCUENT.FAMCLI = dbo.__CLIENTES.FAMCLIDESC " +
         " WHERE     (dbo.DESCUENT.DESC1 > 0)  AND (dbo.ARTICULO.KLS_ID_SHOP<>0) " +
         " UNION " +
         " SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.DESC1, dbo.DESCUENT.FECMIN, dbo.DESCUENT.FECMAX, " +
            " dbo.DESCUENT.FAMART, dbo.DESCUENT.FAMCLI, '' AS FAMCLIDESC, dbo.DESCUENT.CODCLI, dbo.__CLIENTES.KLS_CODCLIENTE, 'DESCUENTO' AS TIPO, 'CLIFAMART' AS tipoconsulta " +
            " FROM dbo.ARTICULO  WITH (NOLOCK) " +
            " INNER JOIN dbo.DESCUENT ON dbo.ARTICULO.FAMARTDESCVEN = dbo.DESCUENT.FAMART " +
            " INNER JOIN dbo.__CLIENTES ON dbo.DESCUENT.CODCLI = dbo.__CLIENTES.CODCLI " +
            " WHERE ARTICULO.KLS_ID_SHOP IS NOT NULL";

            return selectScript;
        }

        public string selectTarifaPreciosEspecialesA3(string filtro = "")
        {
            //BLOQUE 1: Seleccionamos los precios de ARTICULO-FAMILIA CLIENTES
            string sb = "SELECT dbo.articulo.codart, " +
                         "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
                         "dbo.__clientes.codcli, " +
                         "prcesp.fecmin, " +
                         "prcesp.fecmax, " +
                         "Cast(poriva AS INT)                       AS poriva, " +
                         "dbo.articulo.kls_id_shop, " +
                         "dbo.__clientes.kls_codcliente, " +
                         "dbo.prcesp.precio                         AS PRICE, " +
                         "0                                         AS REDUCTION," +
                         "'amount'                                  AS REDUCTION_TYPE," +
                         "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
                         "dbo.prcesp.unidades, " +
                         "dbo.prcesp.famcli " +
                         "FROM   dbo.articulo WITH (nolock) " +
                         "INNER JOIN dbo.prcesp " +
                         "ON dbo.articulo.codart = dbo.prcesp.codart " +
                         " LEFT OUTER JOIN dbo.__clientes " +
                         "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
                         "INNER JOIN tipoiva " +
                         "ON articulo.tipiva = tipoiva.tipiva " +
                         "WHERE  famcli IS NOT NULL  " +
                         "AND ( dbo.articulo.kls_id_shop <> '' ) " +
                         "AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
                         "AND ( dbo.prcesp.codpro IS NULL ) " +
                         "AND ( dbo.__clientes.kls_codcliente IS NULL) " +
                         "AND ( dbo.__clientes.codcli IS NULL) " +
                         " AND(prcesp.fecmax > GETDATE())" +

                         "UNION " +

                         //BLOQUE 2: Seleccionamos los precios de ARTICULO-CLIENTE
                         "SELECT dbo.articulo.codart, " +
                         "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
                         "dbo.__clientes.codcli, " +
                         "prcesp.fecmin, " +
                         "prcesp.fecmax, " +
                         "Cast(poriva AS INT)                       AS poriva, " +
                         "dbo.articulo.kls_id_shop, " +
                         "dbo.__clientes.kls_codcliente, " +
                         "dbo.prcesp.precio                         AS PRICE, " +
                         "0                                         AS REDUCTION," +
                         "'amount'                                  AS REDUCTION_TYPE," +
                         "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
                         "dbo.prcesp.unidades, " +
                         "dbo.prcesp.famcli " +
                         "FROM   dbo.articulo WITH (nolock) " +
                         "INNER JOIN dbo.prcesp " +
                         "ON dbo.articulo.codart = dbo.prcesp.codart " +
                         " LEFT OUTER JOIN dbo.__clientes " +
                         "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
                         "INNER JOIN tipoiva " +
                         "ON articulo.tipiva = tipoiva.tipiva " +
                         "WHERE  famcli IS NULL " +
                         "AND ( dbo.articulo.kls_id_shop <> '' ) " +
                         "AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
                         " AND ( dbo.prcesp.codpro IS NULL ) " +
                         "AND ( dbo.__clientes.kls_codcliente IS NOT NULL) " +
                         "AND ( dbo.__clientes.codcli IS NOT NULL) " +
                         " AND(prcesp.fecmax > GETDATE())" +

                         "UNION " +
                         //BLOQUE 3: Seleccionamos los precios por UNIDAD 
                         "SELECT dbo.articulo.codart, " +
                         "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
                         "dbo.__clientes.codcli, " +
                         "prcesp.fecmin, " +
                         "prcesp.fecmax, " +
                         "Cast(poriva AS INT)                       AS poriva, " +
                         "dbo.articulo.kls_id_shop, " +
                         "dbo.__clientes.kls_codcliente, " +
                         "dbo.prcesp.precio                         AS PRICE, " +
                         "0                                         AS REDUCTION," +
                         "'amount'                                  AS REDUCTION_TYPE," +
                         "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
                         "dbo.prcesp.unidades, " +
                         "dbo.prcesp.famcli " +
                         "FROM   dbo.articulo WITH (nolock) " +
                         "INNER JOIN dbo.prcesp " +
                         "ON dbo.articulo.codart = dbo.prcesp.codart " +
                         " LEFT OUTER JOIN dbo.__clientes " +
                         "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
                         "INNER JOIN tipoiva " +
                         "ON articulo.tipiva = tipoiva.tipiva " +
                         "WHERE  dbo.prcesp.unidades>1 " +
                         "AND ( dbo.articulo.kls_id_shop <> '' ) " +
                         "AND ( dbo.articulo.kls_articulo_tienda = 'T' )" +
                         "AND ( dbo.prcesp.codpro IS NULL ) " +
                         " AND(prcesp.fecmax > GETDATE())" +

                         "UNION " +
                         //BLOQUE 4: Seleccionamos los precios especificos generales. 

                         "SELECT dbo.articulo.codart, " +
                         "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
                         "dbo.__clientes.codcli, " +
                         "prcesp.fecmin, " +
                         "prcesp.fecmax, " +
                         "Cast(poriva AS INT)                       AS poriva, " +
                         "dbo.articulo.kls_id_shop, " +
                         "dbo.__clientes.kls_codcliente, " +
                         "dbo.prcesp.precio                         AS PRICE, " +
                         "0                                         AS REDUCTION," +
                         "'amount'                                  AS REDUCTION_TYPE," +
                         " dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
                         " dbo.prcesp.unidades, " +
                         " dbo.prcesp.famcli " +
                         " FROM   dbo.articulo WITH (nolock) " +
                         " INNER JOIN dbo.prcesp " +
                         " ON dbo.articulo.codart = dbo.prcesp.codart " +
                         " LEFT OUTER JOIN dbo.__clientes " +
                         " ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
                         " INNER JOIN tipoiva " +
                         " ON articulo.tipiva = tipoiva.tipiva " +
                         " WHERE  famcli IS NULL " +
                         " AND ( dbo.articulo.kls_id_shop <> '' ) " +
                         " AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
                         " AND(dbo.prcesp.codpro IS NULL) " +
                         " AND(dbo.__clientes.kls_codcliente IS NULL)" +
                         " AND(dbo.__clientes.CODCLI IS NULL) " +
                         " AND(dbo.prcesp.precio > 0) " +
                         " AND(prcesp.fecmax > GETDATE())";

            ////BLOQUE 1: Seleccionamos los precios de ARTICULO-FAMILIA CLIENTES
            //string sb = "SELECT dbo.articulo.codart, " +
            //             "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +   
            //             "dbo.__clientes.codcli, " +
            //             "prcesp.fecmin, " +
            //             "prcesp.fecmax, " +
            //             "Cast(poriva AS INT)                       AS poriva, " +
            //             "dbo.articulo.kls_id_shop, " +
            //             "dbo.__clientes.kls_codcliente, " +
            //             "dbo.prcesp.precio                         AS PRICE, " +
            //             "0                                         AS REDUCTION," +
            //             "'amount'                                  AS REDUCTION_TYPE," +
            //             "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
            //             "dbo.prcesp.unidades, " +
            //             "dbo.prcesp.famcli " +
            //             "FROM   dbo.articulo WITH (nolock) " +
            //             "INNER JOIN dbo.prcesp " +
            //             "ON dbo.articulo.codart = dbo.prcesp.codart " +
            //             " LEFT OUTER JOIN dbo.__clientes " +
            //             "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
            //             "INNER JOIN tipoiva " +
            //             "ON articulo.tipiva = tipoiva.tipiva " +
            //             "WHERE  famcli IS NOT NULL  " +
            //             "AND ( dbo.articulo.kls_id_shop <> '' ) " +
            //             "AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
            //             "AND ( dbo.prcesp.codpro IS NULL ) " +
            //             "AND ( dbo.__clientes.kls_codcliente IS NULL) " +
            //             "AND ( dbo.__clientes.codcli IS NULL) " +

            //             "UNION " +

            //             //BLOQUE 2: Seleccionamos los precios de ARTICULO-CLIENTE
            //             "SELECT dbo.articulo.codart, " +
            //             "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
            //             "dbo.__clientes.codcli, " +
            //             "prcesp.fecmin, " +
            //             "prcesp.fecmax, " +
            //             "Cast(poriva AS INT)                       AS poriva, " +
            //             "dbo.articulo.kls_id_shop, " +
            //             "dbo.__clientes.kls_codcliente, " +
            //             "dbo.prcesp.precio                         AS PRICE, " +
            //             "0                                         AS REDUCTION," +
            //             "'amount'                                  AS REDUCTION_TYPE," +
            //             "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
            //             "dbo.prcesp.unidades, " +
            //             "dbo.prcesp.famcli " +
            //             "FROM   dbo.articulo WITH (nolock) " +
            //             "INNER JOIN dbo.prcesp " +
            //             "ON dbo.articulo.codart = dbo.prcesp.codart " +
            //             " LEFT OUTER JOIN dbo.__clientes " +
            //             "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
            //             "INNER JOIN tipoiva " +
            //             "ON articulo.tipiva = tipoiva.tipiva " +
            //             "WHERE  famcli IS NULL " +
            //             "AND ( dbo.articulo.kls_id_shop <> '' ) " +
            //             "AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
            //             " AND ( dbo.prcesp.codpro IS NULL ) " +
            //             "AND ( dbo.__clientes.kls_codcliente IS NOT NULL) " +
            //             "AND ( dbo.__clientes.codcli IS NOT NULL) " +


            //             "UNION " +
            //             //BLOQUE 3: Seleccionamos los precios por UNIDAD 
            //             "SELECT dbo.articulo.codart, " +
            //             "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
            //             "dbo.__clientes.codcli, " +
            //             "prcesp.fecmin, " +
            //             "prcesp.fecmax, " +
            //             "Cast(poriva AS INT)                       AS poriva, " +
            //             "dbo.articulo.kls_id_shop, " +
            //             "dbo.__clientes.kls_codcliente, " +
            //             "dbo.prcesp.precio                         AS PRICE, " +
            //             "0                                         AS REDUCTION," +
            //             "'amount'                                  AS REDUCTION_TYPE," +
            //             "dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
            //             "dbo.prcesp.unidades, " +
            //             "dbo.prcesp.famcli " +
            //             "FROM   dbo.articulo WITH (nolock) " +
            //             "INNER JOIN dbo.prcesp " +
            //             "ON dbo.articulo.codart = dbo.prcesp.codart " +
            //             " LEFT OUTER JOIN dbo.__clientes " +
            //             "ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
            //             "INNER JOIN tipoiva " +
            //             "ON articulo.tipiva = tipoiva.tipiva " +
            //             "WHERE  dbo.prcesp.unidades>1 " +
            //             "AND ( dbo.articulo.kls_id_shop <> '' ) " +
            //             "AND ( dbo.articulo.kls_articulo_tienda = 'T' )" +
            //             "AND ( dbo.prcesp.codpro IS NULL ) " +

            //             "UNION " +
            //            //BLOQUE 4: Seleccionamos los precios especificos generales. 

            //             "SELECT dbo.articulo.codart, " +
            //             "dbo.articulo.KLS_ARTICULO_PADRE           AS CODIGOPADRE, " +
            //             "dbo.__clientes.codcli, " +
            //             "prcesp.fecmin, " +
            //             "prcesp.fecmax, " +
            //             "Cast(poriva AS INT)                       AS poriva, " +
            //             "dbo.articulo.kls_id_shop, " +
            //             "dbo.__clientes.kls_codcliente, " +
            //             "dbo.prcesp.precio                         AS PRICE, " +
            //             "0                                         AS REDUCTION," +
            //             "'amount'                                  AS REDUCTION_TYPE," +
            //             " dbo.articulo.prcventa - dbo.prcesp.precio AS IMPORTEDTO, " +
            //             " dbo.prcesp.unidades, " +
            //             " dbo.prcesp.famcli " +
            //             " FROM   dbo.articulo WITH (nolock) " +
            //             " INNER JOIN dbo.prcesp " +
            //             " ON dbo.articulo.codart = dbo.prcesp.codart " +
            //             " LEFT OUTER JOIN dbo.__clientes " +
            //             " ON dbo.prcesp.codcli = dbo.__clientes.codcli " +
            //             " INNER JOIN tipoiva " +
            //             " ON articulo.tipiva = tipoiva.tipiva " +
            //             " WHERE  famcli IS NULL " +
            //             " AND ( dbo.articulo.kls_id_shop <> '' ) " +
            //             " AND ( dbo.articulo.kls_articulo_tienda = 'T' ) " +
            //             " AND(dbo.prcesp.codpro IS NULL) "+
            //             " AND(dbo.__clientes.kls_codcliente IS NULL)" +
            //             " AND(dbo.__clientes.CODCLI IS NULL) "+
            //             " AND(dbo.prcesp.precio > 0) " +
            //             " AND(prcesp.fecmax > GETDATE())";


            return sb;
        }

        public string selectIdiomasPS()
        {
            string selectScript = "SELECT ID_LANG,NAME, ACTIVE, ISO_CODE FROM " + csGlobal.databasePS + ".ps_lang";
            return selectScript;
        }

        public string cargarArticulosConImagenes(bool articulosSinImagen)
        {
            string selectScript = "Select distinct ps_product.id_product, ps_product_lang.name, ps_product.reference,  " +
                                " ps_image.id_image, CASE WHEN ps_product.active = 0 THEN 'No' ELSE 'Si' END as Activo, ps_image.position,  ps_image.cover " +
                                " From " +
                                csGlobal.databasePS + ".ps_image Right Join " +
                                csGlobal.databasePS + ".ps_product On ps_product.id_product = ps_image.id_product " +
                                " inner join ps_product_lang on ps_product_lang.id_product = ps_product.id_product" +
                                ((!articulosSinImagen) ? " where ps_product_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS() : "");

            if (articulosSinImagen)
            {
                selectScript = selectScript + " where (ps_image.id_image is null) or (ps_image.id_image='') and ps_product_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS();
            }

            return selectScript;
        }


        public string selectCategoriasPS(bool orderById=false)
        {
            string anexo = "ps_category_lang.name";

            if (orderById)
            {
                anexo = "ps_category_lang.id_category";
            }

            string selectScript = "Select ps_category.id_category, ps_category_lang.name, ps_category.id_parent From " +
            " ps_category Inner Join " +
            " ps_category_lang On ps_category.id_category = ps_category_lang.id_category " +
            " Where ps_category_lang.id_lang = (Select value from ps_configuration where name='PS_LANG_DEFAULT') order by " + anexo;

            return selectScript;
        }


        public string selectCaracteristicasPS()
        {
            //string selectScript = "Select  ps_feature.id_feature,  ps_feature_lang.name, ps_feature_value_lang.id_feature_value Caracteristica," +
            //    "ps_feature_value_lang.value " +
            //    " From " +
            //    csGlobal.databasePS + ".ps_feature Inner Join " +
            //    csGlobal.databasePS + ".ps_feature_lang On ps_feature_lang.id_feature = ps_feature.id_feature " +
            //    " Inner Join " +
            //    csGlobal.databasePS + ".ps_feature_value On ps_feature.id_feature = ps_feature_value.id_feature " +
            //    " Inner Join " +
            //    " ps_feature_value_lang On ps_feature_value_lang.id_feature_value = " +
            //    " ps_feature_value.id_feature_value " +
            //    " Where " +
            //    " ps_feature_lang.id_lang = 1 and ps_feature_value_lang.id_lang=1 ";
            string selectScript = "Select  ps_feature.id_feature,  ps_feature_lang.name, ps_feature_value_lang.id_feature_value Caracteristica," +
                "ps_feature_value_lang.value " +
                " From " +
                " ps_feature Inner Join " +
                " ps_feature_lang On ps_feature_lang.id_feature = ps_feature.id_feature " +
                " Inner Join " +
                " ps_feature_value On ps_feature.id_feature = ps_feature_value.id_feature " +
                " Inner Join " +
                " ps_feature_value_lang On ps_feature_value_lang.id_feature_value = " +
                " ps_feature_value.id_feature_value " +
                " Where " +
                " ps_feature_lang.id_lang  = (Select value from ps_configuration where name='PS_LANG_DEFAULT') and ps_feature_value_lang.id_lang= (Select value from ps_configuration where name='PS_LANG_DEFAULT') " +
                " order by ps_feature.position";

            return selectScript;
        }

        public string selectIdiomasEnlazados()
        {
            string selectScript = "SELECT idiomaPS,idiomaA3 FROM dbo.KLS_ESYNC_IDIOMAS WITH (NOLOCK) ";
            return selectScript;
        }

        public string selectAtributosPS()
        {
            string selectScript = "Select  ps_attribute.id_attribute_group, ps_attribute_group_lang.name As GroupName," +
                                  "ps_attribute_lang.id_attribute, ps_attribute_lang.name" +
                                  " From " +
                                  csGlobal.databasePS + ".ps_attribute Inner Join " +
                                  csGlobal.databasePS + ".ps_attribute_group_lang On ps_attribute.id_attribute_group = " +
                                  csGlobal.databasePS + ".ps_attribute_group_lang.id_attribute_group Inner Join " +
                                  csGlobal.databasePS + ".ps_attribute_lang On ps_attribute_lang.id_attribute = " +
                                  csGlobal.databasePS + ".ps_attribute.id_attribute" +
                                  " Where " +
                                  csGlobal.databasePS + ".ps_attribute_group_lang.id_lang = 1 And " +
                                  csGlobal.databasePS + ".ps_attribute_lang.id_lang = 1";

            return selectScript;
        }

        public string selectTallasyColoresA3()
        {
            string selectScript = "SELECT dbo.FAMILIATALLA.CODFAMTALLA, " +
                " dbo.FAMILIATALLA.DESCFAMTALLA, dbo.FAMILIATALLA.VERTICAL, " +
                " dbo.TALLAS.CODTALLA, dbo.TALLAS.DESCRIPCION, dbo.TALLAS.ORDENTALLA " +
                " FROM " +
                " dbo.TALLAS  WITH (NOLOCK) INNER JOIN " +
                " dbo.FAMILIATALLA ON dbo.TALLAS.CODFAMTALLA = dbo.FAMILIATALLA.CODFAMTALLA ";
            return selectScript;
        }

        public string selectClientesA3RPST()
        {
            //string selectScript = "SELECT CODCLI, NOMCLI, RAZON, NIFCLI, TELCLI, DIRCLI, DTOCLI, POBCLI, PROVIFISCAL*1 as PROVINCIA, CODPAIS, TELCLI, PARAM1 as IDREPASAT, E_MAIL FROM dbo.CLIENTES WHERE PARAM1<>''";
            string selectScript = "SELECT CODCLI, NOMCLI, RAZON, NIFCLI, TELCLI, DIRCLI, DTOCLI, POBCLI, PROVIFISCAL*1 as PROVINCIA, CODPAIS, TELCLI, PAGINAWEB, KLS_CODCLIENTE as IDREPASAT, E_MAIL FROM dbo.CLIENTES";
            return selectScript;
        }

        public string selectArticulosA3()
        {
            string selectScript = "SELECT CODART, DESCART, PRCVENTA, TEXTO FROM dbo.ARTICULO WITH (NOLOCK) ";
            return selectScript;
        }

        public string selectCategoriasA3()
        {
            string selectScript = "SELECT  KLS_CODCATEGORIA, KLS_DESCCATEGORIA FROM dbo.KLS_CATEGORIAS WITH (NOLOCK) ";
            return selectScript;
        }

        public string selectCaracteristicasA3()
        {
            string selectScript = "SELECT  KLS_CODCARACTERISTICA, KLS_DESCCARACTERISTICA, KLS_VALORCARACTERISTICA,KLS_DESCVALORCARACTERISTICA FROM dbo.KLS_CARACTERISTICAS WITH (NOLOCK) ";
            return selectScript;
        }

        public string selectFabricantesA3()
        {
            string selectScript = "SELECT LTRIM([KLS_CODMARCA]) AS CODIGO,[KLS_DESCMARCA] AS FABRICANTE FROM KLS_MARCAS  WITH (NOLOCK) where ltrim(KLS_CODMARCA)<>''";
            return selectScript;
        }

        public string selectFabricantesPS()
        {
            string selectScript = "Select  * From ps_manufacturer order by name";
            return selectScript;
        }

        public string selectArticulosFabricantesPS()
        {
            string selectScript = "SELECT [CODART],[DESCART],LTRIM([KLS_CODMARCA]) as COD_FABRICANTE,KLS_ID_SHOP FROM ARTICULO  WITH (NOLOCK) where KLS_CODMARCA IS NOT NULL AND KLS_ID_SHOP IS NOT NULL ";
            return selectScript;
        }

        //Esta función devuelve el stock sólo de aquellos artículos que se les ha asignado stock en A3ERP, aquellos que no tienen stock no contempla la combinación
        //Utilizar la función selectStockTallasyColoresFull()
        public string selectStockTallasyColoresPS()
        {
            //Modificado por query de tabla con todas las combinaciones de articulos y tallas y colore
            //string selectScript = "SELECT " +
            //" dbo.STOCKALM.CODART, dbo.STOCKALM.CODFAMTALLAH, dbo.STOCKALM.CODFAMTALLAV, dbo.STOCKALM.CODTALLAH, dbo.STOCKALM.CODTALLAV, " +
            //" dbo.STOCKALM.UNIDADES, dbo.STOCKALM.ID, TALLAS_1.ID AS CODIGO_TALLA_PS, dbo.TALLAS.ID AS CODIGO_COLOR_PS, dbo.ARTICULO.KLS_ID_SHOP " +
            //" FROM " +
            //" dbo.STOCKALM INNER JOIN dbo.TALLAS AS TALLAS_1 " +
            //" ON dbo.STOCKALM.CODFAMTALLAH = TALLAS_1.CODFAMTALLA AND dbo.STOCKALM.CODTALLAH = TALLAS_1.CODTALLA INNER JOIN " +
            //" dbo.TALLAS ON dbo.STOCKALM.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA AND dbo.STOCKALM.CODTALLAV = dbo.TALLAS.CODTALLA  LEFT OUTER JOIN " +
            //" dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
            //" WHERE KLS_ID_SHOP<>''";


            string selectScript = "SELECT " +
                " KLS_ESYNC_STOCK_TALLASYCOLORES.CODART, KLS_ESYNC_STOCK_TALLASYCOLORES.KLS_ID_SHOP, KLS_ESYNC_STOCK_TALLASYCOLORES.DESCART, " +
                " KLS_ESYNC_STOCK_TALLASYCOLORES.CODFAMTALLAH, KLS_ESYNC_STOCK_TALLASYCOLORES.DESCFAMTALLAH, " +
                " KLS_ESYNC_STOCK_TALLASYCOLORES.CODFAMTALLAV, KLS_ESYNC_STOCK_TALLASYCOLORES.DESCFAMTALLAV, " +
                " KLS_ESYNC_STOCK_TALLASYCOLORES.CODIGO_COLOR, VALOR_COLOR, ID_COLOR, ID_TALLA, VALOR_TALLA, " +
                " UNIDADES, KLS_ESYNC_STOCK_TALLASYCOLORES.ID, CODIGO_TALLA, CASE WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 ELSE 2 END AS KLS_PERMITIRCOMPRAS " +
                " FROM " +
                " dbo.KLS_ESYNC_STOCK_TALLASYCOLORES WITH (NOLOCK)  " +
                " INNER JOIN ARTICULO ON ARTICULO.CODART = KLS_ESYNC_STOCK_TALLASYCOLORES.CODART";
            return selectScript;
        }

        public string selectStockTallasyColoresFull()
        {
            string selectScript = "SELECT " +
                    " dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.ARTICULO.CODFAMTALLAH,  " +
                    " dbo.FAMILIATALLA.DESCFAMTALLA AS DESCFAMTALLAH, dbo.FAMILIATALLA.VERTICAL, dbo.ARTICULO.CODFAMTALLAV,  " +
                    " FAMILIATALLA_1.DESCFAMTALLA AS DESCFAMTALLAV, dbo.TALLAS.CODTALLA AS CODIGO_COLOR, dbo.TALLAS.DESCRIPCION AS VALOR_COLOR, dbo.TALLAS.ID AS ID_COLOR,  " +
                    " TALLAS_1.CODTALLA AS CODIGO_TALLA, TALLAS_1.DESCRIPCION AS VALOR_TALLA, TALLAS_1.ID AS ID_TALLA, dbo.STOCKALM.UNIDADES, dbo.STOCKALM.ID AS ID_ALMACEN  " +
                    " FROM " +
                    " dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                    " dbo.FAMILIATALLA ON dbo.ARTICULO.CODFAMTALLAH = dbo.FAMILIATALLA.CODFAMTALLA INNER JOIN " +
                    " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAV = FAMILIATALLA_1.CODFAMTALLA INNER JOIN " +
                    " dbo.TALLAS ON dbo.FAMILIATALLA.CODFAMTALLA = dbo.TALLAS.CODFAMTALLA INNER JOIN " +
                    " dbo.TALLAS AS TALLAS_1 ON FAMILIATALLA_1.CODFAMTALLA = TALLAS_1.CODFAMTALLA LEFT OUTER JOIN " +
                    " dbo.STOCKALM ON dbo.TALLAS.CODTALLA = dbo.STOCKALM.CODTALLAH AND TALLAS_1.CODTALLA = dbo.STOCKALM.CODTALLAV AND  " +
                    " FAMILIATALLA_1.CODFAMTALLA = dbo.STOCKALM.CODFAMTALLAV AND dbo.FAMILIATALLA.CODFAMTALLA = dbo.STOCKALM.CODFAMTALLAH AND  " +
                    " dbo.ARTICULO.CODART = dbo.STOCKALM.CODART " +
                    " WHERE (dbo.ARTICULO.KLS_ID_SHOP <> '')";
            return selectScript;

        }


        public string selectStocTallasyColoresDismay()
        {

            string selectScript = "SELECT " +
                      " LTRIM(dbo.ARTICULO.CODART) AS CODIGO, dbo.ARTICULO.DESCART, dbo.ARTICULO.EAN13, dbo.ARTICULO.KLS_ARTICULO_PADRE, " +
                      " dbo.ARTICULO.KLS_ARTICULO_TIENDA, dbo.ARTICULO.KLS_ID_SHOP, ARTICULO_1.KLS_ID_SHOP AS KLS_ID_SHOP_PARENT, SUM(dbo.STOCKALM.UNIDADES) AS STOCK  " +
                //" CASE WHEN SUM(dbo.STOCKALM.UNIDADES) IS NULL THEN 0 ELSE SUM(dbo.STOCKALM.UNIDADES) END AS STOCK " +
                      " FROM " +
                      " dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                      " dbo.ARTICULO AS ARTICULO_1 ON dbo.ARTICULO.KLS_ARTICULO_PADRE = ARTICULO_1.CODART LEFT OUTER JOIN " +
                      " dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART " +
                      " where ARTICULO_1.KLS_ID_SHOP > 0 " +
                      " GROUP BY LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.DESCART, dbo.ARTICULO.EAN13, dbo.ARTICULO.KLS_ARTICULO_PADRE, dbo.ARTICULO.KLS_ARTICULO_TIENDA,  " +
                      " dbo.ARTICULO.KLS_ID_SHOP, ARTICULO_1.KLS_ID_SHOP " +
                      " HAVING      (dbo.ARTICULO.KLS_ARTICULO_PADRE <> '') " +
                      " ORDER BY KLS_ID_SHOP_PARENT, dbo.ARTICULO.KLS_ID_SHOP";
            //15-3-16 Cancelado porque no funciona si los artículos no tenían stock
            //string selectScript = "SELECT " +
            //        " LTRIM(dbo.ARTICULO.CODART) AS CODIGO, dbo.ARTICULO.DESCART, dbo.ARTICULO.EAN13, dbo.ARTICULO.KLS_ARTICULO_PADRE, " +
            //        " dbo.ARTICULO.KLS_ARTICULO_TIENDA, dbo.ARTICULO.KLS_ID_SHOP, ARTICULO_1.KLS_ID_SHOP AS KLS_ID_SHOP_PARENT, dbo.STOCKALM.UNIDADES,  " +
            //        " CASE WHEN dbo.STOCKALM.UNIDADES IS NULL THEN 0 ELSE dbo.STOCKALM.UNIDADES END AS STOCK " +
            //        " FROM " +
            //        " dbo.ARTICULO INNER JOIN " +
            //        " dbo.ARTICULO AS ARTICULO_1 ON dbo.ARTICULO.KLS_ARTICULO_PADRE = ARTICULO_1.CODART LEFT OUTER JOIN  " +
            //        " dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART " +
            //        " WHERE (dbo.ARTICULO.KLS_ARTICULO_PADRE <> '') " +
            //        " ORDER BY ARTICULO_1.KLS_ID_SHOP,dbo.ARTICULO.KLS_ID_SHOP";
            return selectScript;
        }
        public string selectDefaultLangPS()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string selectScript = "Select  ps_configuration.value From   ps_configuration Where   ps_configuration.name ='PS_LANG_DEFAULT'";
            return selectScript;
        }

        public string selectArticulosInfo(string filtroNombre, string filtroMarca, string filtroCategoria, string filtroCaracteristica, bool caracteristicas, bool peso = false, string txtBusqueda = "")
        {
            string anexo = " ";
            string selectScript = "";
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string defaultIdimoa = mySqlConnect.defaultLangPS().ToString();
            string speso = "";

            if (peso)
            {
                speso = " , ps_product.weight ";
            }
            if (filtroMarca != "")
            {
                anexo = anexo + filtroMarca;

            }
            if (filtroCategoria != "")
            {
                anexo = anexo + filtroCategoria;
            }
            if (filtroNombre != "")
            {
                anexo = anexo + filtroNombre;
            }
            if (filtroCaracteristica != "")
            {
                anexo = anexo + filtroCaracteristica;
            }


            if ( caracteristicas)
            {
                //Modificación para que no salgan las categorías y no se repitan tantas lineas
                //queda más clara la asignación
                selectScript = "Select " +
                                   " ps_product.id_product, " +
                                   " ps_product.id_manufacturer, " +
                                   " ps_manufacturer.name As Fabricante, " +
                                   " ps_product.reference, " +
                                   " ps_product_lang.name, " +
                                   //" ps_category_lang.id_category As Cod_Categoria, " +
                                   //" ps_category_lang.name As Categoria, " +
                                   " ps_feature_product.id_feature, " +
                                   " ps_feature_product.id_feature_value, " +
                                   " ps_feature_value_lang.value " + speso +
                                   " ,ps_product.active, " +
                                   " date_format(ps_product.date_add,'%d/%m/%Y %H:%i:%s') as fecha" +
                                   " From " +
                                   " ps_product Inner Join " +
                                   " ps_product_lang On ps_product.id_product = ps_product_lang.id_product " +
                                   " Left Join " +
                                   " ps_manufacturer On ps_product.id_manufacturer = " +
                                   " ps_manufacturer.id_manufacturer " +
                                   //" Left Join " +
                                   //" ps_category_product On ps_category_product.id_product = ps_product.id_product " +
                                   //" Inner Join " +
                                   //" ps_category_lang On ps_category_lang.id_category = " +
                                   //" ps_category_product.id_category " +
                                   " Left Join " +
                                   " ps_feature_product On ps_feature_product.id_product = ps_product.id_product " +
                                   " Left Join " +
                                   " ps_feature_value_lang On ps_feature_product.id_feature_value = " +
                                   " ps_feature_value_lang.id_feature_value " +
                                   " Where " +
                                   " (ps_feature_value_lang.id_lang = " + defaultIdimoa + " Or ps_feature_value_lang.id_lang Is Null) And " +
                                   " ps_product_lang.id_lang = " + defaultIdimoa + anexo +
                                   " or ps_manufacturer.name like '%" + txtBusqueda + "%')";
                                   

            }
            else
            {
                selectScript = "Select " +
                                  " ps_product.id_product, " +
                                  " ps_product.id_manufacturer, " +
                                  " ps_manufacturer.name As Fabricante, " +
                                  " ps_product.reference, " +
                                  " ps_product_lang.name, " +
                                  " ps_category_lang.id_category As Cod_Categoria, " +
                                  " ps_category_lang.name As Categoria " + speso +
                                  " ,ps_product.active, " +
                                  " date_format(ps_product.date_add,'%d/%m/%Y %H:%i:%s') as fecha" +
                                  " From " +
                                  " ps_product Inner Join " +
                                  " ps_product_lang On ps_product.id_product = ps_product_lang.id_product " +
                                  " Left Join " +
                                  " ps_manufacturer On ps_product.id_manufacturer = " +
                                  "  ps_manufacturer.id_manufacturer Left Join " +
                                  " ps_category_product On ps_category_product.id_product = ps_product.id_product " +
                                  " Inner Join " +
                                  " ps_category_lang On ps_category_lang.id_category = " +
                                  " ps_category_product.id_category " +
                                  " Where " +
                                  " ps_product_lang.id_lang = " + defaultIdimoa + " And  " +
                                  " ps_category_lang.id_lang =  " + defaultIdimoa + anexo +
               " OR ps_category_lang.name like '%" + txtBusqueda + "%' " +
               " or ps_manufacturer.name like '%" + txtBusqueda + "%')";
            }



            //selectScript = MySqlHelper.EscapeString(selectScript);
            //6-10-2015 Quitamos el EscapeString por problemas con el like '%texto_filtro%', escapa los % y los cambia por /%

            return selectScript;

        }

        public string selectProyectosA3()
        {
            string selectScript = "SELECT dbo.CENTROSC.CENTROCOSTE AS PROYECTO, dbo.CENTROSC.DESCCENTRO AS NOMPROYECTO, " +
                  " 1 * SUBSTRING(dbo.CENTROSC.CENTROCOSTE, 2, 4) AS CODCLI, dbo.CLIENTES.NOMCLI, dbo.CLIENTES.TELCLI, dbo.CLIENTES.DIRCLI, dbo.CLIENTES.DTOCLI, " +
                  " dbo.CLIENTES.POBCLI " +
                  " FROM dbo.CENTROSC WITH (NOLOCK)  LEFT OUTER JOIN " +
                  " dbo.CLIENTES ON 1 * SUBSTRING(dbo.CENTROSC.CENTROCOSTE, 2, 4) = dbo.CLIENTES.CODCLI " +
                  " WHERE (dbo.CENTROSC.APLINIVEL1 = 'F') AND (dbo.CENTROSC.APLINIVEL2 = 'F') AND (dbo.CENTROSC.APLINIVEL3 = 'T') AND (dbo.CENTROSC.BLOQUEADO = 'F') AND " +
                  " (dbo.CENTROSC.OBSOLETO = 'F')";
            return selectScript;
        }

        public string seletArticulosConTallasYColores()
        {
            string selectScript = "SELECT " +
                      " dbo.ARTICULO.CODART, dbo.ARTICULO.DESCART, dbo.ARTICULO.CODFAMTALLAH, dbo.ARTICULO.CODFAMTALLAV, " +
                      " dbo.FAMILIATALLA.DESCFAMTALLA AS DESCFAMTALLA_H, FAMILIATALLA_1.DESCFAMTALLA AS DESCFAMTALLA_V, LTRIM(dbo.TALLAS.CODTALLA) AS CODTALLA_H, " +
                      " LTRIM(TALLAS_1.CODTALLA) AS CODTALLA_V " +
                      " FROM dbo.ARTICULO  WITH (NOLOCK) INNER JOIN " +
                      " dbo.FAMILIATALLA ON dbo.ARTICULO.CODFAMTALLAH = dbo.FAMILIATALLA.CODFAMTALLA INNER JOIN " +
                      " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAV = FAMILIATALLA_1.CODFAMTALLA INNER JOIN " +
                      " dbo.TALLAS ON dbo.FAMILIATALLA.CODFAMTALLA = dbo.TALLAS.CODFAMTALLA INNER JOIN " +
                      " dbo.TALLAS AS TALLAS_1 ON FAMILIATALLA_1.CODFAMTALLA = TALLAS_1.CODFAMTALLA ";
            return selectScript;
        }

        public string cargarStockArticulosA3()
        {
            string[] almacenes = null;
            string where = "";
            int ii = 0;

            if (csGlobal.almacenA3.Contains(","))
            {
                almacenes = csGlobal.almacenA3.Split(',');
                for (int i = 0; i < almacenes.Length; i++)
                {
                    if (ii != 0)
                        where += " OR (LTRIM(dbo.STOCKALM.CODALM) = '" + almacenes[i] + "') ";
                    else
                        where += "  (LTRIM(dbo.STOCKALM.CODALM) = '" + almacenes[i] + "') ";
                    ii++;
                }
            }
            else
                where = " (LTRIM(dbo.STOCKALM.CODALM) = '" + csGlobal.almacenA3 + "') ";

            string selectScript = "SELECT " +
                        " LTRIM(dbo.STOCKALM.CODART) AS REFERENCE, SUM(dbo.STOCKALM.UNIDADES) as UNIDADES, dbo.ARTICULO.TALLAS, " +
                        " dbo.ARTICULO.KLS_ARTICULO_TIENDA, KLS_ID_SHOP, " +
                        " CASE WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 ELSE 2 END AS KLS_PERMITIRCOMPRAS " +
                        " FROM " +
                        " dbo.STOCKALM  WITH (NOLOCK) INNER JOIN " +
                        " dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                        " WHERE " +
                        " (dbo.STOCKALM.CODFAMTALLAH IS NULL) AND (dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T') AND " +
                        " (dbo.STOCKALM.CODFAMTALLAV IS NULL) AND (dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T') AND (dbo.STOCKALM.UNIDADES > 0) AND ( " +
                            where +
                        " ) GROUP BY " +
                        " LTRIM(dbo.STOCKALM.CODART), dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ARTICULO_TIENDA, KLS_ID_SHOP, KLS_PERMITIRCOMPRAS";

            return selectScript;
        }

        public string selectKLS_A3ERP_ID()
        {
            String sentencia = "SHOW COLUMNS FROM ps_customer LIKE 'kls_a3erp_id'";
            return sentencia;
        }

        public string selectOrders(string desde, string hasta, string numdoc = "")
        {
            string anexo = "";
            if (numdoc != "")
            {
                anexo = " AND dbo.CABEALBV.NUMDOC = " + numdoc;
            }

            string sb = "                         select FECHA, " +
                        "                         NUMDOC, " +
                        "                         IDALBV, " +
                        "                         SERIE, " +
                        "                         CODCLI, " +
                        "                         NOMCLI, " +
                        "                         NIFCLI, " +
                        "                         REFERENCIA, " +
                        "                         SITUACION, " +
                        "                         CODPAIS " +
                        "FROM dbo.CABEALBV " +
                        "WHERE dbo.CABEALBV.FECHA >= '" + desde + "' AND dbo.CABEALBV.FECHA <= '" + hasta + "' " + anexo + " ORDER BY dbo.CABEALBV.NUMDOC";
		
            return sb;
        }

        /*public string selectOrders(string desde, string hasta, string numdoc = "")
        {
            string anexo = "";
            if (numdoc != "")
            {
                anexo = " AND dbo.CABEALBV.NUMDOC = " + numdoc;
            }

            string consulta2 = "SELECT dbo.PROVINCI.NOMPROVI, dbo.CABEALBV.NUMDOC, dbo.CABEALBV.FECHA, dbo.CABEALBV.CODCLI, dbo.CABEALBV.NOMCLI, dbo.CABEALBV.NIFCLI, dbo.DIRENT.TELENT1 AS TELCLI, " +
                      " dbo.CABEALBV.REFERENCIA, dbo.LINEALBA.CODART, dbo.ARTICULO.ARTALIAS, dbo.LINEALBA.DESCLIN, dbo.LINEALBA.UNIDADES, dbo.LINEALBA.ORDLIN, dbo.CABEALBV.SITUACION, " +
                      " dbo.DIRENT.DIRENT1 AS DIRCLI, dbo.DIRENT.DTOENT AS DTOCLI, dbo.DIRENT.POBENT AS POBCLI, dbo.ARTICULO.CODPRO " +
                       " FROM dbo.DIRENT INNER JOIN " +
                      " dbo.PROVINCI ON dbo.DIRENT.CODPROVI = dbo.PROVINCI.CODPROVI INNER JOIN " +
                      " dbo.LINEALBA INNER JOIN " +
                      " dbo.CABEALBV ON dbo.LINEALBA.IDALBV = dbo.CABEALBV.IDALBV INNER JOIN " +
                      " dbo.ARTICULO ON dbo.LINEALBA.CODART = dbo.ARTICULO.CODART ON dbo.DIRENT.IDDIRENT = dbo.CABEALBV.IDDIRENT " +
                      " WHERE dbo.CABEALBV.FECHA >= '" + desde + "' AND dbo.CABEALBV.FECHA <= '" + hasta + "' " + anexo + " ORDER BY dbo.CABEALBV.NUMDOC, dbo.LINEALBA.ORDLIN";

            return consulta2;
        }*/

        /// <summary>
        /// Algunos clientes quieren la direccion de facturacion
        /// y otros la de envio. Por esa razon se ha creado esta funcion
        /// </summary>
        /// <returns>string del join correcto</returns>
        private string joinPedidoFactura()
        {
            return ((csGlobal.docDestino.ToUpper() == "PEDIDO") ?
            " On ps_address.id_address = ps_orders.id_address_delivery "
            :
            " On ps_address.id_address = ps_orders.id_address_invoice "
            );
        }

        //Función para Exportar la información de los datos de Contacto
        public string selectDocumentosPS(string version, bool soloFras, string fechaDesde, string fechaHasta, bool pendientes, string formasPago, string filtroArticulos, bool PtesPago, bool porFechaPedido = false)
        {
            string selectDocs = "";
            string selectDocsV14 = "";
            string anexo = "";
            string anexoPPago = "";
            string filtroCampoFecha = "";
            string moduloRefPedido = "";
            string anexoFilter = "";
            bool kls_reference = csUtilidades.existeColumnaMySQL(csGlobal.databasePS, "ps_fmm_custom_userdata", "field_value");
             
            filtroCampoFecha = porFechaPedido ? " ps_orders.date_add " : " ps_orders.invoice_date ";

            if (csGlobal.moduloReferenciaPedidos)
            {
                moduloRefPedido = " ,kls_reference as Ref_Cliente ";
            }

            if (formasPago != "")
            {
                if (formasPago == "Transferencia bancaria")
                {
                    formasPago = "Trasferencia bancaria";
                }
                anexo = " and ps_orders.payment='" + formasPago + "' ";
            }
            if (soloFras)
            {
                anexo = " and kls_invoice_erp.invoice_number_erp<>0 ";
            }
            if (pendientes)
            {
                //Activamos la opción para poder bajar pedidos de nuevo
                if (csGlobal.modeDebug)
                {
                    anexo = anexo + " and  ps_orders.invoice_number > 0 ";
                }
                else
                {
                    anexo = anexo + " and (kls_invoice_erp.invoice_number_erp is null or kls_invoice_erp.invoice_number_erp = 0)  and ps_orders.invoice_number > 0 ";
                }
            }
            if (filtroArticulos != "")
            {
                anexo = anexo + " and ps_order_detail.product_reference in (" + filtroArticulos + ")";
            }

            if (csGlobal.usaProntoPago.ToUpper() == "SI")
            {
                anexoPPago = " ,ps_cart_rule.kls_a3erp_pronto_pago as DtoPPago ";
            }
            else
            {
                anexoPPago = " , 0 as DtoPPago ";
            }

            if (kls_reference)
            {
                anexoPPago += " , (select field_value from ps_fmm_custom_userdata where id_order = ps_orders.id_order) as kls_reference ";
            }

            // Estructura thagson
            if (csGlobal.databaseA3.Contains("THAGSON"))
            {
                anexoPPago += " , origin ";
            }

            if (csGlobal.perfilAdminEsync)
            {
                csGlobal.estadosPedidosBloqueados = "99999";
            }
            if (csGlobal.conexionDB.Contains("MIMASA"))
            {

                selectDocs = " select date_format(ps_orders.invoice_date,'%d/%m/%Y') as invoice_date, ps_orders.id_order,kls_invoice_erp.invoice_number_erp as docsA3, ps_orders.id_customer, ps_customer.kls_a3erp_id, " +
                " dni,ps_address.phone, ps_address.phone_mobile, id_address_invoice,address1, address2, ps_address.firstname,ps_address.lastname, " +
                " ps_address.company ,payment,total_discounts,total_paid,total_products,postcode, id_carrier," +
                " city, ps_orders.invoice_number, " +
                " ps_orders.total_shipping as Portes, (select max(ps_order_cart_rule.free_shipping) from ps_order_cart_rule where ps_order_cart_rule.id_order=ps_orders.id_order group by ps_order_cart_rule.id_order)AS PortesGratis, ps_country.iso_code as pais, " +
                "(select max(reduction_percent) from ps_order_cart_rule LEFT JOIN ps_cart_rule ON ps_order_cart_rule.id_cart_rule = ps_cart_rule.id_cart_rule where ps_order_cart_rule.id_order = ps_orders.id_order group by (ps_order_cart_rule.id_order)) AS Dto," + 
                "ps_orders.id_address_delivery,ps_orders.reference, module, ps_state.id_state, ps_country.id_country, " +
                " case when ps_state.iso_code in ('ES-TF','ES-ML','ES-CE','ES-GC') THEN '1' ELSE '0' END AS OPECCM, " + //AÑADIMOS LINEA PARA DETECTAR SI ES CANARIAS CEUTA Y MELILLA
                " ps_customer.email, ps_customer.kls_a3erp_fam_id as famcli, (select id_reference from ps_carrier where id_carrier = ps_orders.id_carrier) as carrier_Ref " + anexoPPago + moduloRefPedido +
                " from ps_customer Inner Join ps_orders On ps_orders.id_customer = ps_customer.id_customer Inner Join ps_address " +

                joinPedidoFactura() +

                " And ps_address.id_customer = ps_orders.id_customer " +
                " Left Join " +
                " kls_invoice_erp On ps_orders.invoice_number = kls_invoice_erp.invoice_number " +
                " inner Join " +
                " ps_country On ps_address.id_country = ps_country.id_country " +
                " left join ps_state " +
                " on ps_address.id_state=ps_state.id_state " +
                " where current_state not in (" + csGlobal.estadosPedidosBloqueados + ") " +
                " and (" + filtroCampoFecha + " between '" + fechaDesde + "' and '" + fechaHasta + "') " + anexo + " order by ps_orders.invoice_number";
            }
            else
            {
                selectDocs = " select date_format(ps_orders.invoice_date,'%d/%m/%Y') as invoice_date, ps_orders.id_order,kls_invoice_erp.invoice_number_erp as docsA3, ps_orders.id_customer, ps_customer.kls_a3erp_id, " +
                " dni,ps_address.phone, ps_address.phone_mobile, id_address_invoice,address1, address2, ps_address.firstname,ps_address.lastname, " +
                " ps_address.company ,payment,total_discounts,total_paid,total_products,postcode, id_carrier," +
                " city, ps_orders.invoice_number, " +
                " ps_orders.total_shipping as Portes, ps_order_cart_rule.free_shipping as PortesGratis, ps_country.iso_code as pais, " +
                " ps_cart_rule.reduction_percent as Dto, ps_orders.id_address_delivery,ps_orders.reference, module, ps_state.id_state, ps_country.id_country, " +
                " case when ps_state.iso_code in ('ES-TF','ES-ML','ES-CE','ES-GC') THEN '1' ELSE '0' END AS OPECCM, " + //AÑADIMOS LINEA PARA DETECTAR SI ES CANARIAS CEUTA Y MELILLA
                " ps_customer.email, (select id_reference from ps_carrier where id_carrier = ps_orders.id_carrier) as carrier_Ref, ps_orders.gift, total_wrapping_tax_excl, total_wrapping_tax_incl " + anexoPPago + moduloRefPedido + ","+
                " (SELECT COUNT(ps_address.id_address) FROM ps_address WHERE ps_address.id_customer = ps_customer.id_customer) AS num_direcciones" +
                " from ps_customer Inner Join ps_orders On ps_orders.id_customer = ps_customer.id_customer Inner Join ps_address " +

                joinPedidoFactura() +
                //MODIFICACION RAUL 23/09/20
                //Se modifica esta linea debido a que al hacer un inner join de address, estaba recuperando todas las direcciones del cliente independientemente de si aparecian en el pedido. Hemos modificado el OR por un AND
                //ANTIGUO
                //" or ps_address.id_customer = ps_orders.id_customer " 
                //NUEVO 
                " and ps_address.id_customer = ps_orders.id_customer " +
                " Left Join " +
                " kls_invoice_erp On ps_orders.invoice_number = kls_invoice_erp.invoice_number " +
                " Left Join " +
                " ps_order_cart_rule On ps_order_cart_rule.id_order = ps_orders.id_order " +
                " Left Join " +
                " ps_cart_rule On ps_order_cart_rule.id_cart_rule = ps_cart_rule.id_cart_rule " +
                " inner Join " +
                " ps_country On ps_address.id_country = ps_country.id_country " +
                " left join ps_state " +
                " on ps_address.id_state=ps_state.id_state " +
                " where current_state not in (" + csGlobal.estadosPedidosBloqueados + ") " +
                " and (" + filtroCampoFecha + " between '" + fechaDesde + "' and '" + fechaHasta + "') " + anexo +
                //MODIFICACION RAUL 23/09/20
                //Se modifica el group by para que se incluyan todos los campos del SELECT, ya que sino da error en las bases de datos que tienen activado el sql_mode=ONLY_FULL_GROUP_BY
                //ANTIGUO
                //" group by ps_orders.invoice_number order by ps_orders.invoice_number"+
                //NUEVO 
                "GROUP BY ps_orders.invoice_number, ps_orders.invoice_date, ps_orders.id_order, kls_invoice_erp.invoice_number_erp, ps_orders.id_customer, ps_customer.kls_a3erp_id, dni,ps_address.phone, ps_address.phone_mobile, id_address_invoice,address1, address2, ps_address.firstname,ps_address.lastname, ps_address.company,payment,total_discounts,total_paid,total_products,postcode, id_carrier, city, ps_orders.invoice_number, ps_orders.total_shipping, ps_order_cart_rule.free_shipping, ps_country.iso_code, ps_cart_rule.reduction_percent, ps_orders.id_address_delivery,ps_orders.reference, module, ps_state.id_state, ps_country.id_country,ps_state.iso_code, ps_customer.email, ps_orders.gift, total_wrapping_tax_excl, total_wrapping_tax_incl  order by ps_orders.invoice_number";
            }

            if (version == "1.4")
            {
                selectDocsV14 = "Select " +
                                " ps_orders.id_order, " +
                                " ps_orders.id_carrier, " +
                                " ps_orders.id_customer, " +
                                " ps_orders.id_address_invoice, " +
                                " ps_orders.payment, " +
                                " ps_orders.total_paid As `Total Pagado`, " +
                                " ps_address.firstname As `DF Nombre`, " +
                                " ps_address.lastname As `DF Apellidos`, " +
                                " ps_address.company As `DF Empresa`, " +
                                " ps_address.address1 As `DF Dirección`, " +
                                " CONCAT('CP ',ps_address.postcode) As `DF CP`, " +
                                " ps_address.city As `DF Población`, " +
                                " ps_address.id_state As `DF Provincia`, " +
                                " ps_address.phone As `DF Teléfono`, " +
                                " ps_address.phone_mobile As `DF Móvil`, " +
                                " ps_address.dni As `DF DNI`, " +
                                " Date_Format(ps_orders.invoice_date, '%d/%m/%Y') As `Fecha Fra`, " +
                                " ps_orders.invoice_number As `Numero Fra`, " +
                                " ps_order_detail.product_reference, " +
                                " ps_order_detail.product_name, " +
                                " ps_order_detail.product_quantity As `Cantidad`, " +
                                " ps_customer.email, " +
                                " ps_address1.company As `DE Empresa`, " +
                                " ps_address1.firstname As `DEnv Nombre`, " +
                                " ps_address1.lastname As `DEnv Apellidos`, " +
                                " ps_address1.address1 As `DEnv Dirección`, " +
                                " CONCAT('CP ',ps_address1.postcode) As `DEnv CP`, " +
                                " ps_address1.city As `DEnv Población`, " +
                                " ps_address1.id_state As `DEnv Provincia`, " +
                                " ps_address1.phone As `DEnv Teléfono`, " +
                                " ps_address1.phone_mobile As `DEnv Móvil`, " +
                                " ps_address1.dni As `DEnv DNI`, " +
                                " ps_order_history.date_add As `fecha Pedido`,  ps_order_history.id_order_state  As `Situación`" +
                                " From " +
                                 csGlobal.databasePS + ".ps_orders Inner Join " +
                                 csGlobal.databasePS + ".ps_order_history on (ps_orders.id_order=ps_order_history.id_order) Inner Join " +
                                 csGlobal.databasePS + ".ps_order_detail On ps_orders.id_order = ps_order_detail.id_order Inner Join " +
                                 csGlobal.databasePS + ".ps_address On ps_address.id_address = ps_orders.id_address_invoice And " +
                                " ps_address.id_customer = ps_orders.id_customer Left Join " +
                                 csGlobal.databasePS + ".kls_invoice_erp On ps_orders.invoice_number = kls_invoice_erp.invoice_number " +
                                " Inner Join " +
                                 csGlobal.databasePS + ".ps_customer On ps_customer.id_customer = ps_orders.id_customer Inner Join " +
                                 csGlobal.databasePS + ".ps_address ps_address1 On ps_orders.id_customer = ps_address1.id_customer And " +
                                " ps_orders.id_address_delivery = ps_address1.id_address " +
                                " where ";

                if (PtesPago)
                {
                    selectDocsV14 = selectDocsV14 + "ps_orders.invoice_number=0 and ps_order_history.date_add>='" + fechaDesde + "' and ps_order_history.date_add<='" + fechaHasta + "' " + anexo +
                     " group by ps_orders.id_order ,ps_order_detail.product_id " +
                     " order by ps_orders.invoice_number ";
                }
                else
                {
                    selectDocsV14 = selectDocsV14 + " ps_orders.invoice_date>='" + fechaDesde + "' and ps_orders.invoice_date<='" + fechaHasta + "' " + anexo +
                     " group by ps_orders.id_order ,ps_order_detail.product_id " +
                    " order by ps_orders.invoice_number ";
                }
            }

            if (version == "1.4")
            {
                return selectDocsV14;
            }
            else
            {
                return selectDocs;
            }
        }

        public string selectArticulosDeDocumentosPS(bool soloFras, string fechaDesde, string fechaHasta, bool pendientes, string formasPago, string filtroArticulos)
        {
            string selectDocs = "";
            string anexo = "";
            if (formasPago != "")
            {
                anexo = " and ps_orders.payment='" + formasPago + "' ";
            }
            if (soloFras)
            {
                anexo = " and ps_orders.invoice_number<>0 ";
            }
            if (pendientes)
            {
                anexo = anexo + " and kls_invoice_erp.invoice_number is null and ps_orders.invoice_number<>0 ";
            }
            if (filtroArticulos != "")
            {
                anexo = anexo + " and ps_order_detail.product_reference in (" + filtroArticulos + ")";


            }

            selectDocs = "Select distinct" +
                            " ps_order_detail.product_reference, " +
                            " From " +
                             csGlobal.databasePS + ".ps_orders Inner Join " +
                             csGlobal.databasePS + ".ps_order_detail On ps_orders.id_order = ps_order_detail.id_order " +
                            " Where " +
                            " invoice_date>='" + fechaDesde + "' and invoice_date<='" + fechaHasta + "' " + anexo + " order by ps_order_detail.product_reference "; ;


            return selectDocs;

        }


        public string selectClientesConCamposA3(string campos)
        {
            return "SELECT " + campos + " FROM dbo.CLIENTES  WITH (NOLOCK) ";
        }

        public string selectArticulosA3conIdDuplicado()
        { 
            string query="SELECT " +
                        " CODART, DESCART, KLS_ID_SHOP FROM ARTICULO WHERE KLS_ID_SHOP IN ( " +
                        " SELECT  KLS_ID_SHOP " +
                        " FROM  dbo.ARTICULO  " +
                        " WHERE KLS_ID_SHOP<>0 " +
                        " GROUP BY dbo.ARTICULO.KLS_ID_SHOP " +
                        " HAVING    COUNT(KLS_ID_SHOP) >1) " +
                        " ORDER BY KLS_ID_SHOP" ;
            return query;
        }

        public string selectAtributosImagenA3(string articulo, string color)
        {
            string query = "SELECT " +
                " dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                " dbo.TALLAS.CODTALLA, dbo.TALLAS.ID, dbo.ARTICULO.KLS_ID_SHOP " +
                " FROM            dbo.ARTICULO INNER JOIN " +
                " dbo.TALLAS ON dbo.ARTICULO.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA " +
                " WHERE " +
                " (LTRIM(dbo.ARTICULO.CODART) = '" + articulo + "') AND (LTRIM(dbo.TALLAS.CODTALLA) = '" + color + "')";

            return query;
        }



    }
}
