﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using klsync.TRS;
using klsync.Updates;
using System.Net;

namespace klsync
{
    public partial class frMainForm : Form
    {
        public frMainForm()
        {
            InitializeComponent();

            menuStrip1.Visible = false;

            setTitleVersion();   
        }

        private void esconder_menus_thagson_dismay()
        {
            if (csGlobal.conexionDB.ToUpper().Contains("THAGSON") || csGlobal.conexionDB.ToUpper().Contains("DISMAY") || csGlobal.conexionDB.ToUpper().Contains("ANDREUTOYS"))
            {
                listadoDeDocumentosToolStripMenuItem.Visible = false;
                informePedidosToolStripMenuItem.Visible = false;
                códigosDeBarrasToolStripMenuItem.Visible = false;
                //incidenciasToolStripMenuItem.Visible = false;
                envíoDeDocumentosToolStripMenuItem.Visible = false;
                seurToolStripMenuItem.Visible = false;
                datosDeParametrizaciónToolStripMenuItem.Visible = false;
                generadorDocumentosA3ERPToolStripMenuItem.Visible = false;
                graellaDeTallasYColoresToolStripMenuItem.Visible = false;
                tsMenuPresupuestos.Visible = false;
                resumenToolStripMenuItem.Visible = false;
                toolStripMenuItem2.Visible = false;
                webservicePSToolStripMenuItem.Visible = false;
                arreglarTriggersToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                alphaToolStripMenuItem.Visible = false;
                asignaciónDeCategoríasToolStripMenuItem.Visible = false;


                //administraciónToolStripMenuItem.Visible = false;
            }
            if (csGlobal.conexionDB.ToUpper().Contains("TEST") )
            {
                datosDeParametrizaciónToolStripMenuItem.Visible = true;
            }
        }

        private void setTitleVersion()
        {
      
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            this.Text = string.Format("KLOSIONS eSYNC - {0} ", version);
        }


        private void sincronizarDocsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frSincronizar.DefInstance.MdiParent = this;
                frSincronizar.DefInstance.Show();
                frSincronizar.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void datosDeParametrizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frConfig.DefInstance.MdiParent = this;
            frConfig.DefInstance.Show();
            frConfig.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private bool checkIp()
        {
            string externalip = new System.Net.WebClient().DownloadString("http://icanhazip.com");

            return (externalip.Trim() == @"62.15.38.132") ? true : false;
        }

        private void managingMenus()
        {
            if (csGlobal.modeDebug) csUtilidades.log("mainform 1");
            this.menuStrip1.Items[4].Visible = false;
            this.alexPedidosToolStripMenuItem.Visible = false;
            //SI ES DE DISMAY LO VISUALIZAMOS
            recoveryAttributesDismayToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("DISMAY") ? true : false;
            exportarDatosPSToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("DISMAY") ? true : false;

            if (csGlobal.modeAp.ToUpper() == "REPASAT")
            {
                csTitulos.tituloSincronizarDocs(this);

                this.menuStrip1.Items[4].Visible = true;
            }
            else
            {
                csTitulos.tituloSincronizarDocs(this);

                this.menuStrip1.Items[4].Visible = false;
            }
            
            if (csGlobal.conexionDB.Contains("SGI") || csGlobal.conexionDB.Contains("BASTIDE") || csGlobal.conexionDB.Contains("BASIC"))
            {
                alexPedidosToolStripMenuItem.Visible = true;
            }
            if (csGlobal.modeDebug) csUtilidades.log("mainform2");
            if (csGlobal.reminderPayment)
            {
                recordatoriosPagoToolStripMenuItem.Visible = true;
            }

            //MIRO
            amigosFundacióMiróToolStripMenuItem.Visible= csGlobal.conexionDB.Contains("FMIRO") ? true : false;
            ///TRS
            ///
            ///TRS
            mantenimientosToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            gestiónToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            configurarToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            repasatToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            accesosDirectosToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            admanToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;
            administraciónToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? false : true;

            if (csGlobal.conexionDB.Contains("TRS"))
            {
                foreach (ToolStripItem item in utilidadesPSToolStripMenuItem.DropDownItems)
                {
                    item.Visible = false;
                }

                admanToolStripMenuItem.Visible = true;
            }

            generarPedidosVentaTRSToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? true : false;
            actualizarDatosIntrastatToolStripMenuItem.Visible= csGlobal.conexionDB.Contains("TRS") ? true : false;
            recordatoriosPagoToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("TRS") ? true : false;
            ///MIMASA
            mimasaIfigenToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("MIMASA") ?true: false;

            presupuestosToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("BUDGET") ? true : false;


            if (csGlobal.conexionDB.Contains("ADMAN") || csGlobal.conexionDB.Contains("BUDGET"))
            {
                foreach (ToolStripItem item in menuStrip1.Items)
                {
                    item.Visible = false;
                }

                admanToolStripMenuItem.Visible = csGlobal.conexionDB.Contains("ADMAN") ? true : false;
                presupuestosToolStripMenuItem1.Visible = csGlobal.conexionDB.Contains("BUDGET") ? true : false;
            }
            else
            {
                admanToolStripMenuItem.Visible = false;
                presupuestosToolStripMenuItem1.Visible = false;
            }

            if (csGlobal.conexionDB.Contains("BUDGET")) {
                presupuestosToolStripMenuItem1.Visible = true;
            }

            if (csGlobal.modeDebug) csUtilidades.log("mainForm 3");
            gastosFPCToolStripMenuItem.Visible = false;
            euroline();
            vilardell();
            fundaciocat();
            menuPuntes();
            showa();

            menuStrip1.Visible = true;

            esconder_menus_thagson_dismay();

            repasat();
        }

        private void repasat()
        {
            if (csGlobal.modeAp.ToUpper() == "REPASAT")
            {
                csDismay.DefInstance.MdiParent = this;
                csDismay.DefInstance.Show();
                csDismay.DefInstance.WindowState = FormWindowState.Maximized;
                menuStrip1.Visible = false;
            }
        }
        private void fundaciocat()
        {
            if (csGlobal.conexionDB.ToUpper().Contains("FUNDACIOCAT"))
            {
                foreach (ToolStripMenuItem item in menuStrip1.Items)
                {
                    item.Visible = false;
                }

                administraciónToolStripMenuItem.Text = "FUNDACIO.CAT";
                administraciónToolStripMenuItem.Visible = true;
                accederToolStripMenuItem.Visible = false;
                reportarUnProblemaToolStripMenuItem.Visible = false;
                webservicePSToolStripMenuItem.Visible = false;
                arreglarTriggersToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                asignaciónDeCategoríasToolStripMenuItem.Visible = false;
                gastosFPCToolStripMenuItem.Visible = true ;
                csUtilidades.log("FUNDACIOCAT OCULTAR");
            }
            else
            {
                alphaToolStripMenuItem.Visible = false;
            }
        }


        private void menuPuntes() {
            if (csGlobal.conexionDB.ToUpper().Contains("PUNTES"))
            {
                foreach (ToolStripMenuItem item in menuStrip1.Items)
                {
                    item.Visible = false;
                }

                administraciónToolStripMenuItem.Text = "PUNTES";
                administraciónToolStripMenuItem.Visible = true;
                accederToolStripMenuItem.Visible = false;
                reportarUnProblemaToolStripMenuItem.Visible = false;
                webservicePSToolStripMenuItem.Visible = false;
                arreglarTriggersToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                asignaciónDeCategoríasToolStripMenuItem.Visible = false;
                gastosFPCToolStripMenuItem.Visible = false;
                puntesToolStripMenuItem.Visible = true;
                csUtilidades.log("PUNTES OCULTAR");
            }
            else
            {
                alphaToolStripMenuItem.Visible = false;
            }


        }

        private void vilardell()
        {
            if (csGlobal.conexionDB.ToUpper().Contains("VILARDELL"))
            {
                foreach (ToolStripMenuItem item in menuStrip1.Items)
                {
                    item.Visible = false;
                }

                administraciónToolStripMenuItem.Text = "Vilardell";
                administraciónToolStripMenuItem.Visible = true;
                accederToolStripMenuItem.Visible = false;
                reportarUnProblemaToolStripMenuItem.Visible = false;
                webservicePSToolStripMenuItem.Visible = false;
                arreglarTriggersToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                asignaciónDeCategoríasToolStripMenuItem.Visible = false;
            }
            else
            {
                alphaToolStripMenuItem.Visible = false;
            }
        }
        private void fundacioCat()
        {
            {
                if (csGlobal.conexionDB.ToUpper().Contains("FUNDACIOCAT"))
                {
                    foreach (ToolStripMenuItem item in menuStrip1.Items)
                    {
                        item.Visible = false;
                    }

                    administraciónToolStripMenuItem.Text = "FUNDACIO";
                    administraciónToolStripMenuItem.Visible = true;
                    accederToolStripMenuItem.Visible = false;
                    reportarUnProblemaToolStripMenuItem.Visible = false;
                    webservicePSToolStripMenuItem.Visible = false;
                    arreglarTriggersToolStripMenuItem.Visible = false;
                    testingToolStripMenuItem.Visible = false;
                    asignaciónDeCategoríasToolStripMenuItem.Visible = false;
                }
                else
                {
                    alphaToolStripMenuItem.Visible = false;
                }
            }

        }

        private void showa()
        {

            if (csGlobal.conexionDB.Contains("SHOWA"))
            {
                listadoDeDocumentosToolStripMenuItem.Visible = false;
                informePedidosToolStripMenuItem.Visible = false;
                códigosDeBarrasToolStripMenuItem.Visible = false;
                incidenciasToolStripMenuItem.Visible = false;
                envíoDeDocumentosToolStripMenuItem.Visible = false;
                seurToolStripMenuItem.Visible = false;
                datosDeParametrizaciónToolStripMenuItem.Visible = false;
                generadorDocumentosA3ERPToolStripMenuItem.Visible = false;
                graellaDeTallasYColoresToolStripMenuItem.Visible = false;
                tsMenuPresupuestos.Visible = false;
                resumenToolStripMenuItem.Visible = false;
                toolStripMenuItem2.Visible = false;
                webservicePSToolStripMenuItem.Visible = false;
                arreglarTriggersToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                alphaToolStripMenuItem.Visible = false;
            }
          
        }

        private void euroline()
        {
            if (csGlobal.conexionDB.Contains("EUROLINE"))
            {
                foreach (ToolStripMenuItem item in menuStrip1.Items)
                {
                    item.Visible = false;
                }

                utilidadesPSToolStripMenuItem.Visible = true;
                utilidadesPSToolStripMenuItem.Text = "Programas";
                artículosToolStripMenuItem1.Visible = false;
                imágenesToolStripMenuItem.Visible = false;
                informePedidosToolStripMenuItem.Visible = false;
                tsMenuPresupuestos.Visible = false;
                alexPedidosToolStripMenuItem.Visible = false;
                códigosDeBarrasToolStripMenuItem.Visible = false;
                testingToolStripMenuItem.Visible = false;
                asignaciónDeCategoríasToolStripMenuItem.Visible = false;
            }
            else
            {
                eurolineToolStripMenuItem.Visible = false;
            }
        }

        private void frMainForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;

                //Creamos una funcion que haga un control de versiones (mira la actual y la mas nueva si no son iguales pone un MessageBox que dice quieres actalizar tu version, si le das a si te lleva/descarga el link)
                //controlVersiones();

                new_login();

                managingMenus();
            }
            catch (Exception)
            {
                csUtilidades.salirDelPrograma();
            }
        }

        private void new_login()
        {
            // Seleccionamos enlace 
            // de momento usamos el anterior
            frSelectEnlace.DefInstance.ShowDialog();
            frSelectEnlace.DefInstance.WindowState = FormWindowState.Normal;

            // Si la ip es la misma que la nuestra,
            // cargamos todos los nodos
            //if (checkIp())
            //{

            //}
            //else
            //{
            //    // si la ip es otra, cargamos otra cosa
            //}
        }

        private void controlVersiones()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string versionActual = fvi.FileVersion;

            //Obyenemos la version mas nueva del esync (de momento a mano)
            string versionNueva = "2023.12.01.01";

            if (versionActual != versionNueva)
            {
                DialogResult resultado = MessageBox.Show("Hay una nueva versión disponible. ¿Deseas actualizar tu versión?", "Actualizar Versión", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (resultado == DialogResult.Yes)
                {
                    try
                    {
                        using (WebClient cliente = new WebClient())
                        {
                            MessageBox.Show("Descargando la nueva versión...");
                            cliente.DownloadFile("https://www.esync.es/kls_esync_app/KLS_Esync_Setup.rar", "KLS_Esync_Setup.rar");
                            MessageBox.Show("Descarga completa. Instala la nueva versión.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error al descargar la nueva versión: {ex.Message}");
                    }
                }
            }
        }

        private void utilidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frUtility.DefInstance.MdiParent = this;
                frUtility.DefInstance.Show();
                frUtility.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void sincronizarStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frStock.DefInstance.MdiParent = this;
                frStock.DefInstance.Show();
                frStock.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void ficherosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frArticulos.DefInstance.MdiParent = this;
                frArticulos.DefInstance.Show();
                frArticulos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void seleccionarEnlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public void showErrorEnlace()
        {
            //MessageBox.Show("NO HAY UN ENLACE SELECCIONADO \n\nDEBE SELECIONAR PREVIAMENTE UN ENLACE.", "SELECCIONAR ENLACE", //MessageBoxButtons.OK, //MessageBoxIcon.Error);
        }


        private void idiomasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frIdiomas.DefInstance.MdiParent = this;
                frIdiomas.DefInstance.Show();
                frIdiomas.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void tallasYColoresToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                //frTallasyColores_old.DefInstance.MdiParent = this;
                //frTallasyColores_old.DefInstance.Show();
                //frTallasyColores_old.DefInstance.WindowState = FormWindowState.Maximized;
            }

        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frRepasatClientes.DefInstance.MdiParent = this;
                frRepasatClientes.DefInstance.Show();
                frRepasatClientes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void proyectosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frRepasatProyectos.DefInstance.MdiParent = this;
                frRepasatProyectos.DefInstance.Show();
                frRepasatProyectos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void artículosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frRepasatArticulos.DefInstance.MdiParent = this;
                frRepasatArticulos.DefInstance.Show();
                frRepasatArticulos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void categoríasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frNewCategorias.DefInstance.MdiParent = this;
                frNewCategorias.DefInstance.Show();
                frNewCategorias.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void sincronizarDoc2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frSincronizarDocs.DefInstance.MdiParent = this;
                frSincronizarDocs.DefInstance.Show();
                frSincronizarDocs.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void característicasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frFeatures.DefInstance.MdiParent = this;
                frFeatures.DefInstance.Show();
                frFeatures.DefInstance.WindowState = FormWindowState.Maximized;

            }



        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frClientes.DefInstance.MdiParent = this;
                frClientes.DefInstance.Show();
                frClientes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void actividadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frRepasatActividades.DefInstance.MdiParent = this;
                frRepasatActividades.DefInstance.Show();
                frRepasatActividades.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }

        private void listadoDeDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frInformes.DefInstance.MdiParent = this;
                frInformes.DefInstance.Show();
                frInformes.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }

        private void fabricantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frManufacturers.DefInstance.MdiParent = this;
                frManufacturers.DefInstance.Show();
                frManufacturers.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }

        private void artículosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPrestaTools.DefInstance.MdiParent = this;
                frPrestaTools.DefInstance.Show();
                frPrestaTools.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }

        private void preciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPrecios.DefInstance.MdiParent = this;
                frPrecios.DefInstance.Show();
                frPrecios.DefInstance.WindowState = FormWindowState.Maximized;

            }
        }





        private void tiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Chrome.exe", csGlobal.nodeTienda);
            }
            catch
            {
                System.Diagnostics.Process.Start(csGlobal.nodeTienda);
            }
        }

        private void backOfficeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Chrome.exe", csGlobal.nodeBackOffice);
            }
            catch
            {
                System.Diagnostics.Process.Start(csGlobal.nodeBackOffice);
            }
        }


        private void imágenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frImagenes.DefInstance.MdiParent = this;
                frImagenes.DefInstance.Show();
                frImagenes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void programadorDeTareasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frTaskScheduler.DefInstance.MdiParent = this;
                frTaskScheduler.DefInstance.Show();
                frTaskScheduler.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void sincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void enlaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //frSelectEnlace.DefInstance.MdiParent = this;
            frSelectEnlace.DefInstance.ShowDialog();
            frSelectEnlace.DefInstance.WindowState = FormWindowState.Normal;
        }

        private void webserviceToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void webservicePSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPSWebService.DefInstance.MdiParent = this;
                frPSWebService.DefInstance.Show();
                frPSWebService.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void resumenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frSummary.DefInstance.MdiParent = this;
                frSummary.DefInstance.Show();
                frSummary.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("notepad.exe", "errorLog.txt");
        }

        private void generadorDocumentosA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frGenerarDocsA3.DefInstance.MdiParent = this;
                frGenerarDocsA3.DefInstance.Show();
                frGenerarDocsA3.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void envíoDeDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                //   if (Microsoft.VisualBasic.Interaction.InputBox("Acceso: ", "", "") == "klosions2014")
                // {
                frEnvioDocumentos.DefInstance.MdiParent = this;
                frEnvioDocumentos.DefInstance.Show();
                frEnvioDocumentos.DefInstance.WindowState = FormWindowState.Maximized;
                //}                
            }
        }

        private void borrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea borrar el fichero de Log?", "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                    File.WriteAllText(csGlobal.rutaFicConfig + @"\errorLog.txt", String.Empty);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void seurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frSeur.DefInstance.MdiParent = this;
                frSeur.DefInstance.Show();
                frSeur.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void facturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frRita.DefInstance.MdiParent = this;
            frRita.DefInstance.Show();
            frRita.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void paypalAdmanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPaypal.DefInstance.MdiParent = this;
                frPaypal.DefInstance.Show();
                frPaypal.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void captioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frCaptio.DefInstance.MdiParent = this;
                frCaptio.DefInstance.Show();
                frCaptio.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void recodificaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frRecodificarDatosA3ERP.DefInstance.MdiParent = this;
                frRecodificarDatosA3ERP.DefInstance.Show();
                frRecodificarDatosA3ERP.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void marcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frSincronizarMarcas.DefInstance.MdiParent = this;
                frSincronizarMarcas.DefInstance.Show();
                frSincronizarMarcas.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void informePedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frInformePedidosPS.DefInstance.MdiParent = this;
                frInformePedidosPS.DefInstance.Show();
                frInformePedidosPS.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void accederToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (csUtilidades.ventanaPassword())
            {
                configuraciónBDToolStripMenuItem.Enabled = true;
                testingToolStripMenuItem.Visible = true;
            }
        }

        private void reportarUnProblemaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frReport report = new frReport();
            report.Show();
            report.StartPosition = FormStartPosition.CenterScreen;
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frGruposClientes.DefInstance.MdiParent = this;
                frGruposClientes.DefInstance.Show();
                frGruposClientes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void alexPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPedidos.DefInstance.MdiParent = this;
                frPedidos.DefInstance.Show();
                frPedidos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }


        private void presupuestosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPresupuestos.DefInstance.MdiParent = this;
                frPresupuestos.DefInstance.Show();
                frPresupuestos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void asientosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frAsientos.DefInstance.MdiParent = this;
                frAsientos.DefInstance.Show();
                frAsientos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void repasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                csDismay.DefInstance.MdiParent = this;
                csDismay.DefInstance.Show();
                csDismay.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void arreglarTriggersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*if (csUtilidades.ventanaPassword())
            {
                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mysql = new csMySqlConnect();
                frConfig config = new frConfig();

                if (!csUtilidades.existeTablaMySQL("kls_invoice_erp"))
                {
                    config.crearTablasAuxiliaresPS();

                    MessageBox.Show("Las tablas auxiliares de prestashop han sido creadas");
                }
                config.tablasAuxiliaresA3();
                if (!sql.consultaExiste("SELECT count(*) FROM KLS_ESYNC_IDIOMAS"))
                {
                    MessageBox.Show("Idiomas no configurados");
                }
                if (!csUtilidades.existeColumnaSQL("PROVINCI", "KLS_ID_PS"))
                {
                    config.actualizarProvinciasPaisesA3();
                }
                if (!sql.consultaExiste("select * from sys.triggers where name = 'FECHA_ALTA_MOVIMIENTO'"))
                {
                    if (MessageBox.Show("Sigue las instrucciones de la página que se va a abrir a continuacion", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk) == DialogResult.OK)
                    {
                        System.Diagnostics.Process.Start("http://www.klosions.com/a3erp-como-anadir-fecha-de-alta-y-ultima-modificacion/");
                    }
                }


            }*/

            //frCheckings.DefInstance.MdiParent = this;
            frCheckings.DefInstance.Show();
            //frCheckings.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void frMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            csUtilidades.salirDelPrograma();
        }

        private void característicasToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frFeatures.DefInstance.MdiParent = this;
                frFeatures.DefInstance.Show();
                frFeatures.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void familiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frFamilias.DefInstance.MdiParent = this;
                frFamilias.DefInstance.Show();
                frFamilias.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void toolStripMenuItemEnlaces_Click(object sender, EventArgs e)
        {
            frSelectEnlace enlace = new frSelectEnlace();
            enlace.Show();
        }

        private void graellaDeTallasYColoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frGraellaTyC.DefInstance.MdiParent = this;
            frGraellaTyC.DefInstance.Show();
            frGraellaTyC.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void tsMenuPresupuestos_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPresupuestos.DefInstance.MdiParent = this;
                frPresupuestos.DefInstance.Show();
                frPresupuestos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void stockAvanzadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frStockAvanzado.DefInstance.MdiParent = this;
            frStockAvanzado.DefInstance.Show();
            frStockAvanzado.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void atributosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frAtributos.DefInstance.MdiParent = this;
                frAtributos.DefInstance.Show();
                frAtributos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frTesting.DefInstance.MdiParent = this;
            frTesting.DefInstance.Show();
            frTesting.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void alphaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (csUtilidades.ventanaPassword())
            //{
            frVilardell.DefInstance.MdiParent = this;
            frVilardell.DefInstance.Show();
            frVilardell.DefInstance.WindowState = FormWindowState.Maximized;
            //}
        }

        private void preciosClientesArticulosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frPreciosFamiliasClientes.DefInstance.MdiParent = this;
            frPreciosFamiliasClientes.DefInstance.Show();
            frPreciosFamiliasClientes.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void tablasAuxiliaresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frTablasAuxiliares.DefInstance.MdiParent = this;
                frTablasAuxiliares.DefInstance.Show();
                frTablasAuxiliares.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void caracteristicasOLDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frFeatures.DefInstance.MdiParent = this;
                frFeatures.DefInstance.Show();
                frFeatures.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void alexPedidosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frPedidos.DefInstance.MdiParent = this;
                frPedidos.DefInstance.Show();
                frPedidos.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void webservicePSToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frPSWebService.DefInstance.MdiParent = this;
            frPSWebService.DefInstance.Show();
            frPSWebService.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void códigosDeBarrasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frCodBarr.DefInstance.MdiParent = this;
            frCodBarr.DefInstance.Show();
            frCodBarr.DefInstance.WindowState = FormWindowState.Maximized;

        }

        private void paginationToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frImagenesTyC.DefInstance.MdiParent = this;
            frImagenesTyC.DefInstance.Show();
            frImagenesTyC.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void preciosFamClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frPreciosFamiliasClientes.DefInstance.MdiParent = this;
            frPreciosFamiliasClientes.DefInstance.Show();
            frPreciosFamiliasClientes.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void eurolineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frEuroline.DefInstance.MdiParent = this;
                frEuroline.DefInstance.Show();
                frEuroline.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void verificacionesStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frStock.DefInstance.MdiParent = this;
            frStock.DefInstance.Show();
            frStock.DefInstance.WindowState = FormWindowState.Maximized;
        }


        private void artículosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frIncidencias.DefInstance.MdiParent = this;
                frIncidencias.DefInstance.Show();
                frIncidencias.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void stocksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frStock.DefInstance.MdiParent = this;
                frStock.DefInstance.Show();
                frStock.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void asignaciónDeCategoríasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frShowaCategoryAsign.DefInstance.MdiParent = this;
                frShowaCategoryAsign.DefInstance.Show();
                frShowaCategoryAsign.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void configurarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (csGlobal.perfilAdminEsync)
            {
                datosDeParametrizaciónToolStripMenuItem.Visible = true;
            }
        }

        private void mimasaIfigenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                Mimasa.frMimasa.DefInstance.MdiParent = this;
                Mimasa.frMimasa.DefInstance.Show();
                Mimasa.frMimasa.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void recordatoriosPagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
               frReminderPayment.DefInstance.MdiParent = this;
               frReminderPayment.DefInstance.Show();
               frReminderPayment.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void Caracteristicas_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {
                frCaracteristicas.DefInstance.MdiParent = this;
                frCaracteristicas.DefInstance.Show();
                frCaracteristicas.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }



        private void gastosFPCToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Fundacio.frGastosFundacio.DefInstance.MdiParent = this;
            Fundacio.frGastosFundacio.DefInstance.Show();
            Fundacio.frGastosFundacio.DefInstance.WindowState = FormWindowState.Maximized;
        

        }

        private void amigosFundacióMiróToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.amigos_miro();
            "Amigos procesados".mb();
        }

        private void recoveryAttributesDismayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dismay.csDismay dismay = new Dismay.csDismay();
            dismay.procedimientoDismay();
        }
        private void importaciónFacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            adMan.frFacturasVentaAdMan.DefInstance.MdiParent = this;
            adMan.frFacturasVentaAdMan.DefInstance.Show();
            adMan.frFacturasVentaAdMan.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void admanFrasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            adMan.frFacturasVentaAdMan.DefInstance.MdiParent = this;
            adMan.frFacturasVentaAdMan.DefInstance.Show();
            adMan.frFacturasVentaAdMan.DefInstance.WindowState = FormWindowState.Maximized;
        }


        private void generarPedidosVentaTRSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {

                frGenerarDocsA3.DefInstance.MdiParent = this;
                frGenerarDocsA3.DefInstance.Show();
                frGenerarDocsA3.DefInstance.WindowState = FormWindowState.Maximized;
                

            }
        }

        private void actualizarDatosIntrastatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {

                frCompletarDeclaracionIntrastat.DefInstance.MdiParent = this;
                frCompletarDeclaracionIntrastat.DefInstance.Show();
                frCompletarDeclaracionIntrastat.DefInstance.WindowState = FormWindowState.Maximized;


            }
        }

        private void puntesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace();
            }
            else
            {

                Puntes.frMainFormPuntes.DefInstance.MdiParent = this;
                Puntes.frMainFormPuntes.DefInstance.Show();
                Puntes.frMainFormPuntes.DefInstance.WindowState = FormWindowState.Maximized;
            }
        }

        private void presupuestosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            A3ERP.Accounting.frBudget.DefInstance.MdiParent = this;
            A3ERP.Accounting.frBudget.DefInstance.Show();
            A3ERP.Accounting.frBudget.DefInstance.WindowState = FormWindowState.Maximized;
        }

        private void mantenimientosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exportarDatosPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(csGlobal.conexionDB))
            {
                showErrorEnlace(); 
            }
            else
            {
                frExportDataToExcel frmExport = new frExportDataToExcel();
                frmExport.MdiParent = this; 
                frmExport.Show(); 
                frmExport.WindowState = FormWindowState.Maximized; 
            }
        }
    }
}
