﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frShowaCategoryAsign : Form
    {

        private csMySqlConnect mysql = new csMySqlConnect();

        private static frShowaCategoryAsign m_FormDefInstance;
        public static frShowaCategoryAsign DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frShowaCategoryAsign();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frShowaCategoryAsign()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            cargarCategorias();
            cargarCaracteristicas();
        }

        private void cargarCategorias()
        {
            string scriptCategories = "select ps_category.id_category,ps_category_lang.name as categoria,ps_category.id_parent as idPadre,ps_category_lang_1.name as Padre " +
                                " from ps_category inner join ps_category_lang on ps_category.id_category=ps_category_lang.id_category " +
                                " left outer join ps_category as ps_category_1 on ps_category.id_parent=ps_category_1.id_category " +
                                "  inner join ps_category_lang as ps_category_lang_1 on ps_category_1.id_category=ps_category_lang_1.id_category " +
                                " where ps_category_lang.id_lang=1 and ps_category_lang_1.id_lang=1";

            csUtilidades.addDataSource(dgvCategories, mysql.cargarTabla(scriptCategories));
        }

        private void cargarCaracteristicas()
        {
            string scriptFeatures = "select id_feature, name, 'S' as ocultar from ps_feature_lang where id_lang=1";

            csUtilidades.addDataSource(dgvFeatures, mysql.cargarTabla(scriptFeatures));
        }

        private void cargarCaracteristicasExcluidas(string category)
        {
            string feature = "";
            string scriptExcludedFeatures="select id_feature from kls_features_categories_exclusions where id_category=" + category + " order by id_feature" ;
            DataTable excludedFeatures = mysql.cargarTabla(scriptExcludedFeatures);


            if (excludedFeatures.hasRows())
            {
                foreach (DataRow drExcluded in excludedFeatures.Rows)
                {
                    feature = drExcluded["id_feature"].ToString();

                    foreach (DataGridViewRow fila in dgvFeatures.Rows)
                    {
                        if (fila.Cells["id_feature"].Value.ToString() == feature)
                        {
                            fila.Cells["ocultar"].Value = "N";
                        }
                    }
                }
            }

            pintarDeRojo(dgvFeatures);
        
        }

        private void dgvCategories_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                dgvFeatures.ClearSelection();
                cargarCaracteristicas();
                cargarCaracteristicasExcluidas(dgvCategories.Rows[e.RowIndex].Cells["id_category"].Value.ToString());
            }
        }

        private void excluirCaracterísticasSeleccionadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mostrarCaracteristicasSeleccionadas();
            

        }

        private void mostrarCaracterísticasSeleccionadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ocultarCaracteristicasSeleccionadas();
        }


        private void ocultarCaracteristicasSeleccionadas()
        {
            string categoria = "";
            string feature = "";

            foreach (DataGridViewRow fila in dgvFeatures.SelectedRows)
            {
                categoria = dgvCategories.SelectedRows[0].Cells["id_category"].Value.ToString();
                feature = fila.Cells["id_feature"].Value.ToString();
                csUtilidades.ejecutarConsulta("insert into kls_features_categories_exclusions (id_feature,id_category) VALUES (" + feature + "," + categoria + ")", true);
            }

            dgvFeatures.ClearSelection();
            cargarCaracteristicas();
            cargarCaracteristicasExcluidas(dgvCategories.SelectedRows[0].Cells["id_category"].Value.ToString());
        
        }


        private void mostrarCaracteristicasSeleccionadas()
        {
            string categoria = "";
            string feature = "";

            foreach (DataGridViewRow fila in dgvFeatures.SelectedRows)
            {
                categoria = dgvCategories.SelectedRows[0].Cells["id_category"].Value.ToString();
                feature = fila.Cells["id_feature"].Value.ToString();
                csUtilidades.ejecutarConsulta("delete from kls_features_categories_exclusions where id_feature= " + feature + " and id_category=" + categoria, true);
            }

            dgvFeatures.ClearSelection();
            cargarCaracteristicas();
            cargarCaracteristicasExcluidas(dgvCategories.SelectedRows[0].Cells["id_category"].Value.ToString());
        }

        private void pintarDeRojo(DataGridView dgv)
        {
            if (dgv.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Cells["ocultar"].Value.ToString() == "N")
                    {
                        row.DefaultCellStyle.ForeColor = Color.Green;
                    }
                    if (row.Cells["ocultar"].Value.ToString() == "S")
                    {
                        row.DefaultCellStyle.ForeColor = Color.Red;
                    }
                }
            }
        }

    }
}
