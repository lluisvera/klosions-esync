﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Data;

namespace klsync
{
    class csMail
    {
        public void sendMail(string subject, string messageBody, string adjunto = "", string destinatario = "", string emailOrigen = "", DataTable dt = null, bool html = false, bool notificacionLectura = false, bool fechaNombrePedido = false)
        {
            try
            {
                string emailDestino = "";
                string emailFrom = "";

                
                if (dt != null)
                {
                    emailDestino = dt.Rows[0]["email"].ToString();
                }
                else if (destinatario != "" && dt == null)
                {
                    emailDestino = destinatario;
                }
                else
                {
                    emailDestino = csGlobal.EmailDestino;
                }

                if (emailOrigen != "")
                {
                    emailFrom = emailOrigen;
                }
                else
                {
                    emailFrom = csGlobal.EmailEnvio;
                }

                if (csGlobal.perfilAdminEsync)
                {
                    emailDestino = "lluisvera@klosions.com";
                }
                

                MailMessage mail = new MailMessage(emailFrom, emailDestino);
                //List<string> attachments = new List<string>();
                //string attachment = "";
                //valido si el dataTable tiene lineas
                if (dt != null)
                {
                    if (dt.Rows[0].ItemArray[1].ToString() != "")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //attachment = dt.Rows[i].ItemArray[1].ToString();
                            //attachments.Add(attachment);

                            mail.Attachments.Add(new System.Net.Mail.Attachment(dt.Rows[i].ItemArray[1].ToString()));
                        }

                        //csUtilidades.zipping("facturas.zip", attachments);
                        //mail.Attachments.Add(new System.Net.Mail.Attachment("facturas.zip"));
                    }
                }
                
                if (adjunto.Length > 1)
                {
                    string fecha = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string nombre = "c:/docs/"+ ((fechaNombrePedido)?fecha:"pedido") + ".xml";
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(adjunto);
                    xdoc.Save(nombre);

                    MemoryStream mStrm = new MemoryStream(Encoding.UTF8.GetBytes(adjunto));
                    System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Xml);
                    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(mStrm, ct);
                    //attach.ContentDisposition.FileName = "c:/docs/pedido.xml";
                    attach.ContentDisposition.FileName = ((fechaNombrePedido) ? fecha : "pedido")  + ".xml";

                    mail.Attachments.Add(attach);
                }

                if (html)
                    mail.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();

                if (csGlobal.EmailSMTP.Contains("gmail") || csGlobal.EmailSMTP.Contains("hotmail"))
                {
                    client.Port = 587;
                    client.EnableSsl = true;
                }
                else
                {
                    client.Port = 25;
                }

                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = csGlobal.EmailSMTP;
                client.Timeout = 25000;
                client.Credentials = new System.Net.NetworkCredential(csGlobal.UsuarioEmail, csGlobal.UsuarioPass);
                mail.Subject = subject;
                mail.Body = messageBody;

                
                if (notificacionLectura)
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                
                client.Send(mail);
            }
            catch (SmtpFailedRecipientException ex)
            {
                Program.guardarErrorFichero(ex.Message.ToString());            
            }
        }
    }
}
