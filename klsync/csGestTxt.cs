﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csGestTxt
    {
        public string[] segmentarTxt(string TexToSegment)
        {
            char[] delimitedchars = { '-' };
            string[] palabras = TexToSegment.Split(delimitedchars);
            int numpalabras = palabras.Count();
            return palabras;
        }

        public string codisegmento(string TexToSegment)
        {
            char[] delimitedchars = { '-' };
            string[] palabras = TexToSegment.Split(delimitedchars);
            int numpalabras = palabras.Count();
            return palabras[0];
        }

        public string descsegmento(string TexToSegment)
        {
            char[] delimitedchars = { '-' };
            string[] palabras = TexToSegment.Split(delimitedchars);
            int numpalabras = palabras.Count();
            return palabras[1];
        }



        public string rellenarIzq(string texto, int tamañoTotal, string relleno)
        {
            int largoIni = texto.Length;
            int largoFin = tamañoTotal - largoIni;
            string textoFin = "";
            for (int i = 1; i <= largoFin; i++)
            {
                textoFin = textoFin + relleno;
            }
            textoFin = textoFin + texto;
            return textoFin;
        }

    }
}
