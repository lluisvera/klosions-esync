﻿namespace klsync
{
    partial class frRita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frRita));
            this.contextWebservice = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.articulosRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exportarAExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusWebservice = new System.Windows.Forms.StatusStrip();
            this.tssLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssNumeroFilas = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelRita = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelProgreso = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBarRita = new System.Windows.Forms.ToolStripProgressBar();
            this.splitContainerAdman = new System.Windows.Forms.SplitContainer();
            this.gbCuatro = new System.Windows.Forms.GroupBox();
            this.btnSetExtNULLAdvInvoice = new System.Windows.Forms.Button();
            this.btnSetExtNULLInvoices = new System.Windows.Forms.Button();
            this.btnSetExtThridInvoice = new System.Windows.Forms.Button();
            this.btnAdvInvoices2 = new System.Windows.Forms.Button();
            this.btnThirdInvoices2 = new System.Windows.Forms.Button();
            this.btnInvoices2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbTres = new System.Windows.Forms.GroupBox();
            this.btnAdvInvoices = new System.Windows.Forms.Button();
            this.btnThirdInvoices = new System.Windows.Forms.Button();
            this.btnInvoices = new System.Windows.Forms.Button();
            this.gbDos = new System.Windows.Forms.GroupBox();
            this.any = new System.Windows.Forms.NumericUpDown();
            this.gbUno = new System.Windows.Forms.GroupBox();
            this.labelPasoUno = new System.Windows.Forms.Label();
            this.contextWebservice.SuspendLayout();
            this.statusWebservice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdman)).BeginInit();
            this.splitContainerAdman.Panel1.SuspendLayout();
            this.splitContainerAdman.SuspendLayout();
            this.gbCuatro.SuspendLayout();
            this.gbTres.SuspendLayout();
            this.gbDos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.any)).BeginInit();
            this.gbUno.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextWebservice
            // 
            this.contextWebservice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.articulosRPSTToolStripMenuItem,
            this.clientesRPSTToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exportarAExcelToolStripMenuItem});
            this.contextWebservice.Name = "contextWebservice";
            this.contextWebservice.Size = new System.Drawing.Size(156, 76);
            // 
            // articulosRPSTToolStripMenuItem
            // 
            this.articulosRPSTToolStripMenuItem.Name = "articulosRPSTToolStripMenuItem";
            this.articulosRPSTToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.articulosRPSTToolStripMenuItem.Text = "Articulos RPST";
            // 
            // clientesRPSTToolStripMenuItem
            // 
            this.clientesRPSTToolStripMenuItem.Name = "clientesRPSTToolStripMenuItem";
            this.clientesRPSTToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.clientesRPSTToolStripMenuItem.Text = "Clientes RPST";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 6);
            // 
            // exportarAExcelToolStripMenuItem
            // 
            this.exportarAExcelToolStripMenuItem.Name = "exportarAExcelToolStripMenuItem";
            this.exportarAExcelToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exportarAExcelToolStripMenuItem.Text = "Exportar a Excel";
            // 
            // statusWebservice
            // 
            this.statusWebservice.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssLabel,
            this.tssNumeroFilas,
            this.toolStripStatusLabelInfo,
            this.toolStripStatusLabelRita,
            this.toolStripStatusLabelProgreso,
            this.toolStripProgressBarRita});
            this.statusWebservice.Location = new System.Drawing.Point(0, 671);
            this.statusWebservice.Name = "statusWebservice";
            this.statusWebservice.Size = new System.Drawing.Size(996, 26);
            this.statusWebservice.TabIndex = 7;
            this.statusWebservice.Text = "statusStrip1";
            // 
            // tssLabel
            // 
            this.tssLabel.Name = "tssLabel";
            this.tssLabel.Size = new System.Drawing.Size(0, 21);
            // 
            // tssNumeroFilas
            // 
            this.tssNumeroFilas.Name = "tssNumeroFilas";
            this.tssNumeroFilas.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusLabelInfo
            // 
            this.toolStripStatusLabelInfo.Name = "toolStripStatusLabelInfo";
            this.toolStripStatusLabelInfo.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusLabelRita
            // 
            this.toolStripStatusLabelRita.Name = "toolStripStatusLabelRita";
            this.toolStripStatusLabelRita.Size = new System.Drawing.Size(772, 21);
            this.toolStripStatusLabelRita.Spring = true;
            // 
            // toolStripStatusLabelProgreso
            // 
            this.toolStripStatusLabelProgreso.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabelProgreso.Name = "toolStripStatusLabelProgreso";
            this.toolStripStatusLabelProgreso.Size = new System.Drawing.Size(82, 21);
            this.toolStripStatusLabelProgreso.Text = "Progreso ";
            // 
            // toolStripProgressBarRita
            // 
            this.toolStripProgressBarRita.Name = "toolStripProgressBarRita";
            this.toolStripProgressBarRita.Size = new System.Drawing.Size(125, 20);
            // 
            // splitContainerAdman
            // 
            this.splitContainerAdman.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAdman.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAdman.Name = "splitContainerAdman";
            // 
            // splitContainerAdman.Panel1
            // 
            this.splitContainerAdman.Panel1.Controls.Add(this.gbCuatro);
            this.splitContainerAdman.Panel1.Controls.Add(this.gbTres);
            this.splitContainerAdman.Panel1.Controls.Add(this.gbDos);
            this.splitContainerAdman.Panel1.Controls.Add(this.gbUno);
            this.splitContainerAdman.Panel2Collapsed = true;
            this.splitContainerAdman.Size = new System.Drawing.Size(996, 671);
            this.splitContainerAdman.SplitterDistance = 858;
            this.splitContainerAdman.TabIndex = 11;
            // 
            // gbCuatro
            // 
            this.gbCuatro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCuatro.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.gbCuatro.Controls.Add(this.btnSetExtNULLAdvInvoice);
            this.gbCuatro.Controls.Add(this.btnSetExtNULLInvoices);
            this.gbCuatro.Controls.Add(this.btnSetExtThridInvoice);
            this.gbCuatro.Controls.Add(this.btnAdvInvoices2);
            this.gbCuatro.Controls.Add(this.btnThirdInvoices2);
            this.gbCuatro.Controls.Add(this.btnInvoices2);
            this.gbCuatro.Controls.Add(this.label1);
            this.gbCuatro.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.gbCuatro.ForeColor = System.Drawing.Color.Red;
            this.gbCuatro.Location = new System.Drawing.Point(4, 401);
            this.gbCuatro.Name = "gbCuatro";
            this.gbCuatro.Size = new System.Drawing.Size(990, 267);
            this.gbCuatro.TabIndex = 3;
            this.gbCuatro.TabStop = false;
            this.gbCuatro.Text = "*** ATENCIÓN: VOLVER A DESCARGAR FACTURAS";
            // 
            // btnSetExtNULLAdvInvoice
            // 
            this.btnSetExtNULLAdvInvoice.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSetExtNULLAdvInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnSetExtNULLAdvInvoice.Location = new System.Drawing.Point(282, 176);
            this.btnSetExtNULLAdvInvoice.Name = "btnSetExtNULLAdvInvoice";
            this.btnSetExtNULLAdvInvoice.Size = new System.Drawing.Size(182, 56);
            this.btnSetExtNULLAdvInvoice.TabIndex = 10;
            this.btnSetExtNULLAdvInvoice.Text = "Borrar external id";
            this.btnSetExtNULLAdvInvoice.UseVisualStyleBackColor = false;
            this.btnSetExtNULLAdvInvoice.Click += new System.EventHandler(this.btnSetExtNULLAdvInvoice_Click);
            // 
            // btnSetExtNULLInvoices
            // 
            this.btnSetExtNULLInvoices.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSetExtNULLInvoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnSetExtNULLInvoices.Location = new System.Drawing.Point(24, 177);
            this.btnSetExtNULLInvoices.Name = "btnSetExtNULLInvoices";
            this.btnSetExtNULLInvoices.Size = new System.Drawing.Size(104, 56);
            this.btnSetExtNULLInvoices.TabIndex = 9;
            this.btnSetExtNULLInvoices.Text = "Borrar external id";
            this.btnSetExtNULLInvoices.UseVisualStyleBackColor = false;
            this.btnSetExtNULLInvoices.Click += new System.EventHandler(this.btnSetExtNULLInvoices_Click);
            // 
            // btnSetExtThridInvoice
            // 
            this.btnSetExtThridInvoice.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSetExtThridInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnSetExtThridInvoice.Location = new System.Drawing.Point(134, 177);
            this.btnSetExtThridInvoice.Name = "btnSetExtThridInvoice";
            this.btnSetExtThridInvoice.Size = new System.Drawing.Size(141, 56);
            this.btnSetExtThridInvoice.TabIndex = 8;
            this.btnSetExtThridInvoice.Text = "Borrar external id";
            this.btnSetExtThridInvoice.UseVisualStyleBackColor = false;
            this.btnSetExtThridInvoice.Click += new System.EventHandler(this.btnSetExtThridInvoice_Click);
            // 
            // btnAdvInvoices2
            // 
            this.btnAdvInvoices2.Image = ((System.Drawing.Image)(resources.GetObject("btnAdvInvoices2.Image")));
            this.btnAdvInvoices2.Location = new System.Drawing.Point(281, 84);
            this.btnAdvInvoices2.Name = "btnAdvInvoices2";
            this.btnAdvInvoices2.Size = new System.Drawing.Size(183, 86);
            this.btnAdvInvoices2.TabIndex = 7;
            this.btnAdvInvoices2.Text = "Advertisers Invoices";
            this.btnAdvInvoices2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdvInvoices2.UseVisualStyleBackColor = true;
            this.btnAdvInvoices2.Click += new System.EventHandler(this.btnAdvInvoices2_Click);
            // 
            // btnThirdInvoices2
            // 
            this.btnThirdInvoices2.Image = ((System.Drawing.Image)(resources.GetObject("btnThirdInvoices2.Image")));
            this.btnThirdInvoices2.Location = new System.Drawing.Point(133, 84);
            this.btnThirdInvoices2.Name = "btnThirdInvoices2";
            this.btnThirdInvoices2.Size = new System.Drawing.Size(142, 86);
            this.btnThirdInvoices2.TabIndex = 6;
            this.btnThirdInvoices2.Text = "Third Invoices";
            this.btnThirdInvoices2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThirdInvoices2.UseVisualStyleBackColor = true;
            this.btnThirdInvoices2.Click += new System.EventHandler(this.btnThirdInvoices2_Click);
            // 
            // btnInvoices2
            // 
            this.btnInvoices2.Image = ((System.Drawing.Image)(resources.GetObject("btnInvoices2.Image")));
            this.btnInvoices2.Location = new System.Drawing.Point(23, 84);
            this.btnInvoices2.Name = "btnInvoices2";
            this.btnInvoices2.Size = new System.Drawing.Size(104, 86);
            this.btnInvoices2.TabIndex = 5;
            this.btnInvoices2.Text = "Invoices";
            this.btnInvoices2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnInvoices2.UseVisualStyleBackColor = true;
            this.btnInvoices2.Click += new System.EventHandler(this.btnInvoices2_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(19, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(675, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "Se volverán a generar las facturas en A3ERP";
            // 
            // gbTres
            // 
            this.gbTres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTres.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gbTres.Controls.Add(this.btnAdvInvoices);
            this.gbTres.Controls.Add(this.btnThirdInvoices);
            this.gbTres.Controls.Add(this.btnInvoices);
            this.gbTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.gbTres.Location = new System.Drawing.Point(4, 216);
            this.gbTres.Name = "gbTres";
            this.gbTres.Size = new System.Drawing.Size(990, 179);
            this.gbTres.TabIndex = 2;
            this.gbTres.TabStop = false;
            this.gbTres.Text = "3. DESCARGAR FACTURAS NUEVAS";
            // 
            // btnAdvInvoices
            // 
            this.btnAdvInvoices.Image = ((System.Drawing.Image)(resources.GetObject("btnAdvInvoices.Image")));
            this.btnAdvInvoices.Location = new System.Drawing.Point(282, 32);
            this.btnAdvInvoices.Name = "btnAdvInvoices";
            this.btnAdvInvoices.Size = new System.Drawing.Size(183, 86);
            this.btnAdvInvoices.TabIndex = 4;
            this.btnAdvInvoices.Text = "Advertisers Invoices";
            this.btnAdvInvoices.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdvInvoices.UseVisualStyleBackColor = true;
            this.btnAdvInvoices.Click += new System.EventHandler(this.btnAdvInvoices_Click);
            // 
            // btnThirdInvoices
            // 
            this.btnThirdInvoices.Image = ((System.Drawing.Image)(resources.GetObject("btnThirdInvoices.Image")));
            this.btnThirdInvoices.Location = new System.Drawing.Point(134, 32);
            this.btnThirdInvoices.Name = "btnThirdInvoices";
            this.btnThirdInvoices.Size = new System.Drawing.Size(142, 86);
            this.btnThirdInvoices.TabIndex = 3;
            this.btnThirdInvoices.Text = "Third Invoices";
            this.btnThirdInvoices.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThirdInvoices.UseVisualStyleBackColor = true;
            this.btnThirdInvoices.Click += new System.EventHandler(this.btnThirdInvoices_Click);
            // 
            // btnInvoices
            // 
            this.btnInvoices.Image = ((System.Drawing.Image)(resources.GetObject("btnInvoices.Image")));
            this.btnInvoices.Location = new System.Drawing.Point(24, 32);
            this.btnInvoices.Name = "btnInvoices";
            this.btnInvoices.Size = new System.Drawing.Size(104, 86);
            this.btnInvoices.TabIndex = 0;
            this.btnInvoices.Text = "Invoices";
            this.btnInvoices.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnInvoices.UseVisualStyleBackColor = true;
            this.btnInvoices.Click += new System.EventHandler(this.btnInvoices_Click);
            // 
            // gbDos
            // 
            this.gbDos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDos.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gbDos.Controls.Add(this.any);
            this.gbDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.gbDos.Location = new System.Drawing.Point(4, 101);
            this.gbDos.Name = "gbDos";
            this.gbDos.Size = new System.Drawing.Size(990, 109);
            this.gbDos.TabIndex = 1;
            this.gbDos.TabStop = false;
            this.gbDos.Text = "2. FILTRAR POR AÑO";
            // 
            // any
            // 
            this.any.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.any.Location = new System.Drawing.Point(24, 46);
            this.any.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.any.Minimum = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.any.Name = "any";
            this.any.Size = new System.Drawing.Size(137, 26);
            this.any.TabIndex = 0;
            this.any.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.any.Value = new decimal(new int[] {
            2017,
            0,
            0,
            0});
            // 
            // gbUno
            // 
            this.gbUno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbUno.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gbUno.Controls.Add(this.labelPasoUno);
            this.gbUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.gbUno.Location = new System.Drawing.Point(3, 14);
            this.gbUno.Name = "gbUno";
            this.gbUno.Size = new System.Drawing.Size(990, 81);
            this.gbUno.TabIndex = 0;
            this.gbUno.TabStop = false;
            this.gbUno.Text = "1. INFORMACIÓN GENERAL";
            // 
            // labelPasoUno
            // 
            this.labelPasoUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPasoUno.Location = new System.Drawing.Point(20, 35);
            this.labelPasoUno.Name = "labelPasoUno";
            this.labelPasoUno.Size = new System.Drawing.Size(724, 43);
            this.labelPasoUno.TabIndex = 0;
            this.labelPasoUno.Text = "Sólo se pueden descargar facturas cuyo cliente o proveedor tengan el fiscal ident" +
    "ity (nif) informado";
            // 
            // frRita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 697);
            this.Controls.Add(this.splitContainerAdman);
            this.Controls.Add(this.statusWebservice);
            this.Name = "frRita";
            this.Text = "Adman";
            this.Load += new System.EventHandler(this.frRita_Load);
            this.contextWebservice.ResumeLayout(false);
            this.statusWebservice.ResumeLayout(false);
            this.statusWebservice.PerformLayout();
            this.splitContainerAdman.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAdman)).EndInit();
            this.splitContainerAdman.ResumeLayout(false);
            this.gbCuatro.ResumeLayout(false);
            this.gbTres.ResumeLayout(false);
            this.gbDos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.any)).EndInit();
            this.gbUno.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextWebservice;
        private System.Windows.Forms.ToolStripMenuItem articulosRPSTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesRPSTToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusWebservice;
        private System.Windows.Forms.ToolStripStatusLabel tssLabel;
        private System.Windows.Forms.ToolStripStatusLabel tssNumeroFilas;
        private System.Windows.Forms.ToolStripMenuItem exportarAExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelInfo;
        private System.Windows.Forms.SplitContainer splitContainerAdman;
        private System.Windows.Forms.GroupBox gbUno;
        private System.Windows.Forms.Label labelPasoUno;
        private System.Windows.Forms.GroupBox gbDos;
        private System.Windows.Forms.GroupBox gbCuatro;
        private System.Windows.Forms.GroupBox gbTres;
        private System.Windows.Forms.NumericUpDown any;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInvoices;
        private System.Windows.Forms.Button btnAdvInvoices;
        private System.Windows.Forms.Button btnThirdInvoices;
        private System.Windows.Forms.Button btnAdvInvoices2;
        private System.Windows.Forms.Button btnThirdInvoices2;
        private System.Windows.Forms.Button btnInvoices2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRita;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarRita;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProgreso;
        private System.Windows.Forms.Button btnSetExtThridInvoice;
        private System.Windows.Forms.Button btnSetExtNULLInvoices;
        private System.Windows.Forms.Button btnSetExtNULLAdvInvoice;


    }
}