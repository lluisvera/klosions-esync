﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using System.Threading;

namespace klsync
{
    public partial class frImagenes : Form
    {

        private static frImagenes m_FormDefInstance;
        public static frImagenes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frImagenes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frImagenes()
        {
            InitializeComponent();
        }

        private void cargarImagenes()
        {
            bool articulosSinImagen = false;
            if (rbutArticulosSinImagen.Checked)
            {
                articulosSinImagen = true;
            }

            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(mySqlScript.cargarArticulosConImagenes(articulosSinImagen), dgvImagenes);
            tssLbl.Text = "Número de Filas: " + dgvImagenes.Rows.Count.ToString();
        }

        private void conectarDB(string sqlScript, DataGridView dgv)
        {

            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                //MySqlConnection conn = new MySqlConnection(csGlobal.cadenaConexionPS);
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;


            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }

        private void rbutArticulosSinImagen_CheckedChanged(object sender, EventArgs e)
        {
            cargarImagenes();
        }

        private void rbutTodosArticulos_CheckedChanged(object sender, EventArgs e)
        {
            cargarImagenes();
        }



        private void subirImagen(string articulo, bool multiple)
        {
            csImagenesPS imagenesPS = new csImagenesPS();
            string ficOrigen = "";
            string ficDestino = "";
            string fichero = "";

            //string fichero = "";
            string nombreDestino = "";
            string ruta = "";
            string extension = "";
            string nombreImagenSinExtension = "";

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            bool renombrarImagen = false;

            if (!multiple)
            {
                Stream myStream = null;
                openFileDialog1.InitialDirectory = csGlobal.rutaImagenes;
                openFileDialog1.FileName = "*" + articulo + "*";
                openFileDialog1.Filter = "txt files (*.jpg)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;
                //indico que se pueden coger varios ficheros a la vez
                openFileDialog1.Multiselect = false;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            renombrarImagen = false;

                            fichero = openFileDialog1.FileName;
                            nombreDestino = System.IO.Path.GetFileName(fichero);
                            ruta = System.IO.Path.GetDirectoryName(fichero);
                            extension = System.IO.Path.GetExtension(fichero);
                            imagenesPS.subirImagenPS(fichero, myStream);
                        }
                    }
                }

            }
            else if (multiple)
            {
                ficOrigen = csGlobal.rutaImagenes + "\\" + articulo + ".jpg";
                if (File.Exists(ficOrigen))
                {
                    fichero = ficOrigen;
                    Thread imagen = new Thread(delegate() { imagenesPS.subirImagenPS(fichero); });
                    imagen.Start();
                }

            }

        }

        


        private void subirImagenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csImagenesPS imagenesPS = new csImagenesPS();
            imagenesPS.subirImagenes();
            cargarImagenes();
        }

        private void frImagenes_Load(object sender, EventArgs e)
        {
            rbutTodosArticulos.Checked = true;
            cargarImagenes();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            csUtilidades.codificarImagenesPrestashop();
        }

        private void borrarImagenesSeleccionadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ("¿¿Seguro que quieres borrar las imagenes seleccionadas de la tienda??".what() && csUtilidades.ventanaPassword())
            {
                csPrestashop ps = new csPrestashop();

                foreach (DataGridViewRow item in dgvImagenes.SelectedRows)
                {
                    string id_product = item.Cells["id_product"].Value.ToString();
                    string id_image = item.Cells["id_image"].Value.ToString();

                    if (id_image != "")
                    {
                        ps.deleteImage(id_image, id_product);
                    }
                }

                cargarImagenes();

                "** Las imágenes se han borrado con éxito **".mb();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }
    }
}





