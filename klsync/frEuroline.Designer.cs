﻿namespace klsync
{
    partial class frEuroline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsLabelYear = new System.Windows.Forms.ToolStripLabel();
            this.tsCBoxYear = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxSerie = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxOferta = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxTarifa = new System.Windows.Forms.ToolStripComboBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabP0 = new System.Windows.Forms.TabPage();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.btShow = new System.Windows.Forms.Button();
            this.tabP1Info = new System.Windows.Forms.TabPage();
            this.lblIdOferta = new System.Windows.Forms.Label();
            this.idDocumento = new System.Windows.Forms.Label();
            this.rbPrintPreciosNO = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.rbPrintPreciosSI = new System.Windows.Forms.RadioButton();
            this.lblCodCli = new System.Windows.Forms.Label();
            this.btHide = new System.Windows.Forms.Button();
            this.dgvInfoDoc = new System.Windows.Forms.DataGridView();
            this.btAsignLineas = new System.Windows.Forms.Button();
            this.lblSituacion = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.lblOferta = new System.Windows.Forms.Label();
            this.lblNomCli = new System.Windows.Forms.Label();
            this.lblReferencia = new System.Windows.Forms.Label();
            this.tbP2Prices = new System.Windows.Forms.TabPage();
            this.btnPendiente = new System.Windows.Forms.Button();
            this.btAprobar = new System.Windows.Forms.Button();
            this.btnServirDoc = new System.Windows.Forms.Button();
            this.btHide2 = new System.Windows.Forms.Button();
            this.comboOperacion = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCambiarPVP = new System.Windows.Forms.Button();
            this.btnCambiarPrecio1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCambiarPrecio2 = new System.Windows.Forms.Button();
            this.txtMargenPrecio1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMargenPrecio2 = new System.Windows.Forms.TextBox();
            this.labelAcciones = new System.Windows.Forms.Label();
            this.comboBoxPrecios = new System.Windows.Forms.ComboBox();
            this.btnCopiarPrecios = new System.Windows.Forms.Button();
            this.tbP3Proveedores = new System.Windows.Forms.TabPage();
            this.btHide3 = new System.Windows.Forms.Button();
            this.btAsignarProveedor = new System.Windows.Forms.Button();
            this.cboxProveedores = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvProveedores = new System.Windows.Forms.DataGridView();
            this.btnResumenProveedores = new System.Windows.Forms.Button();
            this.tbColumns = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.lbVisualizarColumns = new System.Windows.Forms.Label();
            this.clboxColumns = new System.Windows.Forms.CheckedListBox();
            this.statusStripBottom = new System.Windows.Forms.StatusStrip();
            this.tssInfoProceso = new System.Windows.Forms.ToolStripStatusLabel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabP0.SuspendLayout();
            this.tabP1Info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfoDoc)).BeginInit();
            this.tbP2Prices.SuspendLayout();
            this.tbP3Proveedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedores)).BeginInit();
            this.tbColumns.SuspendLayout();
            this.statusStripBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabelYear,
            this.tsCBoxYear,
            this.toolStripLabel2,
            this.toolStripComboBoxSerie,
            this.toolStripLabel3,
            this.toolStripComboBoxOferta,
            this.toolStripButton1,
            this.toolStripLabel1,
            this.toolStripComboBoxTarifa});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(10);
            this.toolStrip1.Size = new System.Drawing.Size(1248, 49);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsLabelYear
            // 
            this.tsLabelYear.Name = "tsLabelYear";
            this.tsLabelYear.Size = new System.Drawing.Size(38, 26);
            this.tsLabelYear.Text = "Año";
            // 
            // tsCBoxYear
            // 
            this.tsCBoxYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsCBoxYear.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsCBoxYear.Name = "tsCBoxYear";
            this.tsCBoxYear.Size = new System.Drawing.Size(121, 29);
            this.tsCBoxYear.SelectedIndexChanged += new System.EventHandler(this.tsCBoxYear_SelectedIndexChanged);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(52, 26);
            this.toolStripLabel2.Text = "Series";
            // 
            // toolStripComboBoxSerie
            // 
            this.toolStripComboBoxSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxSerie.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripComboBoxSerie.Name = "toolStripComboBoxSerie";
            this.toolStripComboBoxSerie.Size = new System.Drawing.Size(121, 29);
            this.toolStripComboBoxSerie.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxSerie_SelectedIndexChanged);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(61, 26);
            this.toolStripLabel3.Text = "Ofertas";
            // 
            // toolStripComboBoxOferta
            // 
            this.toolStripComboBoxOferta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxOferta.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripComboBoxOferta.Name = "toolStripComboBoxOferta";
            this.toolStripComboBoxOferta.Size = new System.Drawing.Size(150, 29);
            this.toolStripComboBoxOferta.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxOferta_SelectedIndexChanged);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::klsync.Properties.Resources.magnifying_glass;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(77, 26);
            this.toolStripButton1.Text = "Cargar";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(56, 26);
            this.toolStripLabel1.Text = "Tarifas";
            // 
            // toolStripComboBoxTarifa
            // 
            this.toolStripComboBoxTarifa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxTarifa.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripComboBoxTarifa.Name = "toolStripComboBoxTarifa";
            this.toolStripComboBoxTarifa.Size = new System.Drawing.Size(121, 29);
            this.toolStripComboBoxTarifa.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxTarifa_SelectedIndexChanged);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(874, 636);
            this.dgv.TabIndex = 1;
            this.dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 57);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv);
            this.splitContainer1.Size = new System.Drawing.Size(1224, 660);
            this.splitContainer1.SplitterDistance = 346;
            this.splitContainer1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabP0);
            this.tabControl1.Controls.Add(this.tabP1Info);
            this.tabControl1.Controls.Add(this.tbP2Prices);
            this.tabControl1.Controls.Add(this.tbP3Proveedores);
            this.tabControl1.Controls.Add(this.tbColumns);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(340, 637);
            this.tabControl1.TabIndex = 19;
            // 
            // tabP0
            // 
            this.tabP0.Controls.Add(this.checkedListBox1);
            this.tabP0.Controls.Add(this.btShow);
            this.tabP0.Location = new System.Drawing.Point(23, 4);
            this.tabP0.Name = "tabP0";
            this.tabP0.Size = new System.Drawing.Size(313, 629);
            this.tabP0.TabIndex = 3;
            this.tabP0.UseVisualStyleBackColor = true;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(69, 111);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(8, 4);
            this.checkedListBox1.TabIndex = 24;
            // 
            // btShow
            // 
            this.btShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btShow.BackgroundImage = global::klsync.Properties.Resources.magnifying_glass;
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btShow.Location = new System.Drawing.Point(284, 3);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(26, 28);
            this.btShow.TabIndex = 23;
            this.btShow.UseVisualStyleBackColor = true;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // tabP1Info
            // 
            this.tabP1Info.Controls.Add(this.lblIdOferta);
            this.tabP1Info.Controls.Add(this.idDocumento);
            this.tabP1Info.Controls.Add(this.rbPrintPreciosNO);
            this.tabP1Info.Controls.Add(this.label5);
            this.tabP1Info.Controls.Add(this.rbPrintPreciosSI);
            this.tabP1Info.Controls.Add(this.lblCodCli);
            this.tabP1Info.Controls.Add(this.btHide);
            this.tabP1Info.Controls.Add(this.dgvInfoDoc);
            this.tabP1Info.Controls.Add(this.btAsignLineas);
            this.tabP1Info.Controls.Add(this.lblSituacion);
            this.tabP1Info.Controls.Add(this.lblEstado);
            this.tabP1Info.Controls.Add(this.labelInfo);
            this.tabP1Info.Controls.Add(this.lblOferta);
            this.tabP1Info.Controls.Add(this.lblNomCli);
            this.tabP1Info.Controls.Add(this.lblReferencia);
            this.tabP1Info.Location = new System.Drawing.Point(23, 4);
            this.tabP1Info.Name = "tabP1Info";
            this.tabP1Info.Padding = new System.Windows.Forms.Padding(3);
            this.tabP1Info.Size = new System.Drawing.Size(313, 629);
            this.tabP1Info.TabIndex = 0;
            this.tabP1Info.Text = "Info Documento";
            this.tabP1Info.UseVisualStyleBackColor = true;
            // 
            // lblIdOferta
            // 
            this.lblIdOferta.AutoSize = true;
            this.lblIdOferta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblIdOferta.Location = new System.Drawing.Point(74, 286);
            this.lblIdOferta.Name = "lblIdOferta";
            this.lblIdOferta.Size = new System.Drawing.Size(14, 18);
            this.lblIdOferta.TabIndex = 31;
            this.lblIdOferta.Text = "-";
            // 
            // idDocumento
            // 
            this.idDocumento.AutoSize = true;
            this.idDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.idDocumento.Location = new System.Drawing.Point(7, 286);
            this.idDocumento.Name = "idDocumento";
            this.idDocumento.Size = new System.Drawing.Size(73, 18);
            this.idDocumento.TabIndex = 30;
            this.idDocumento.Text = "IdOferta:";
            // 
            // rbPrintPreciosNO
            // 
            this.rbPrintPreciosNO.AutoSize = true;
            this.rbPrintPreciosNO.Location = new System.Drawing.Point(159, 257);
            this.rbPrintPreciosNO.Name = "rbPrintPreciosNO";
            this.rbPrintPreciosNO.Size = new System.Drawing.Size(41, 17);
            this.rbPrintPreciosNO.TabIndex = 29;
            this.rbPrintPreciosNO.Text = "NO";
            this.rbPrintPreciosNO.UseVisualStyleBackColor = true;
            this.rbPrintPreciosNO.CheckedChanged += new System.EventHandler(this.rbPrintPreciosNO_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(7, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 18);
            this.label5.TabIndex = 28;
            this.label5.Text = "Impr. Precios:";
            // 
            // rbPrintPreciosSI
            // 
            this.rbPrintPreciosSI.AutoSize = true;
            this.rbPrintPreciosSI.Checked = true;
            this.rbPrintPreciosSI.Location = new System.Drawing.Point(124, 257);
            this.rbPrintPreciosSI.Name = "rbPrintPreciosSI";
            this.rbPrintPreciosSI.Size = new System.Drawing.Size(35, 17);
            this.rbPrintPreciosSI.TabIndex = 27;
            this.rbPrintPreciosSI.TabStop = true;
            this.rbPrintPreciosSI.Text = "SI";
            this.rbPrintPreciosSI.UseVisualStyleBackColor = true;
            this.rbPrintPreciosSI.CheckedChanged += new System.EventHandler(this.rbPrintPreciosSI_CheckedChanged);
            // 
            // lblCodCli
            // 
            this.lblCodCli.AutoSize = true;
            this.lblCodCli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblCodCli.Location = new System.Drawing.Point(7, 100);
            this.lblCodCli.Name = "lblCodCli";
            this.lblCodCli.Size = new System.Drawing.Size(124, 18);
            this.lblCodCli.TabIndex = 26;
            this.lblCodCli.Text = "Código Cliente:";
            // 
            // btHide
            // 
            this.btHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide.Location = new System.Drawing.Point(280, 6);
            this.btHide.Name = "btHide";
            this.btHide.Size = new System.Drawing.Size(27, 28);
            this.btHide.TabIndex = 25;
            this.btHide.UseVisualStyleBackColor = true;
            this.btHide.Click += new System.EventHandler(this.btHide_Click);
            // 
            // dgvInfoDoc
            // 
            this.dgvInfoDoc.AllowUserToAddRows = false;
            this.dgvInfoDoc.AllowUserToDeleteRows = false;
            this.dgvInfoDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInfoDoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvInfoDoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInfoDoc.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvInfoDoc.Location = new System.Drawing.Point(9, 324);
            this.dgvInfoDoc.Name = "dgvInfoDoc";
            this.dgvInfoDoc.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInfoDoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvInfoDoc.Size = new System.Drawing.Size(298, 299);
            this.dgvInfoDoc.TabIndex = 20;
            // 
            // btAsignLineas
            // 
            this.btAsignLineas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAsignLineas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAsignLineas.Location = new System.Drawing.Point(212, 275);
            this.btAsignLineas.Name = "btAsignLineas";
            this.btAsignLineas.Size = new System.Drawing.Size(95, 43);
            this.btAsignLineas.TabIndex = 19;
            this.btAsignLineas.Text = "Resumen Documento";
            this.btAsignLineas.UseVisualStyleBackColor = true;
            this.btAsignLineas.Click += new System.EventHandler(this.btAsignLineas_Click);
            // 
            // lblSituacion
            // 
            this.lblSituacion.AutoSize = true;
            this.lblSituacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblSituacion.Location = new System.Drawing.Point(7, 223);
            this.lblSituacion.Name = "lblSituacion";
            this.lblSituacion.Size = new System.Drawing.Size(66, 18);
            this.lblSituacion.TabIndex = 18;
            this.lblSituacion.Text = "Estado:";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblEstado.Location = new System.Drawing.Point(6, 193);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(66, 18);
            this.lblEstado.TabIndex = 17;
            this.lblEstado.Text = "Estado:";
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(6, 43);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(104, 20);
            this.labelInfo.TabIndex = 0;
            this.labelInfo.Text = "Información";
            // 
            // lblOferta
            // 
            this.lblOferta.AutoSize = true;
            this.lblOferta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblOferta.Location = new System.Drawing.Point(7, 71);
            this.lblOferta.Name = "lblOferta";
            this.lblOferta.Size = new System.Drawing.Size(60, 18);
            this.lblOferta.TabIndex = 1;
            this.lblOferta.Text = "Oferta:";
            // 
            // lblNomCli
            // 
            this.lblNomCli.AutoSize = true;
            this.lblNomCli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblNomCli.Location = new System.Drawing.Point(7, 129);
            this.lblNomCli.Name = "lblNomCli";
            this.lblNomCli.Size = new System.Drawing.Size(127, 18);
            this.lblNomCli.TabIndex = 2;
            this.lblNomCli.Text = "Nombre cliente:";
            // 
            // lblReferencia
            // 
            this.lblReferencia.AutoSize = true;
            this.lblReferencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblReferencia.Location = new System.Drawing.Point(6, 159);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(94, 18);
            this.lblReferencia.TabIndex = 3;
            this.lblReferencia.Text = "Referencia:";
            // 
            // tbP2Prices
            // 
            this.tbP2Prices.Controls.Add(this.btnPendiente);
            this.tbP2Prices.Controls.Add(this.btAprobar);
            this.tbP2Prices.Controls.Add(this.btnServirDoc);
            this.tbP2Prices.Controls.Add(this.btHide2);
            this.tbP2Prices.Controls.Add(this.comboOperacion);
            this.tbP2Prices.Controls.Add(this.label1);
            this.tbP2Prices.Controls.Add(this.btnCambiarPVP);
            this.tbP2Prices.Controls.Add(this.btnCambiarPrecio1);
            this.tbP2Prices.Controls.Add(this.label3);
            this.tbP2Prices.Controls.Add(this.btnCambiarPrecio2);
            this.tbP2Prices.Controls.Add(this.txtMargenPrecio1);
            this.tbP2Prices.Controls.Add(this.label2);
            this.tbP2Prices.Controls.Add(this.txtMargenPrecio2);
            this.tbP2Prices.Controls.Add(this.labelAcciones);
            this.tbP2Prices.Controls.Add(this.comboBoxPrecios);
            this.tbP2Prices.Controls.Add(this.btnCopiarPrecios);
            this.tbP2Prices.Location = new System.Drawing.Point(23, 4);
            this.tbP2Prices.Name = "tbP2Prices";
            this.tbP2Prices.Padding = new System.Windows.Forms.Padding(3);
            this.tbP2Prices.Size = new System.Drawing.Size(313, 629);
            this.tbP2Prices.TabIndex = 1;
            this.tbP2Prices.Text = "Acciones Precios";
            this.tbP2Prices.UseVisualStyleBackColor = true;
            // 
            // btnPendiente
            // 
            this.btnPendiente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnPendiente.Location = new System.Drawing.Point(104, 338);
            this.btnPendiente.Name = "btnPendiente";
            this.btnPendiente.Size = new System.Drawing.Size(106, 29);
            this.btnPendiente.TabIndex = 33;
            this.btnPendiente.Text = "Volver a Pte";
            this.btnPendiente.UseVisualStyleBackColor = true;
            this.btnPendiente.Click += new System.EventHandler(this.btnPendiente_Click);
            // 
            // btAprobar
            // 
            this.btAprobar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btAprobar.Location = new System.Drawing.Point(16, 338);
            this.btAprobar.Name = "btAprobar";
            this.btAprobar.Size = new System.Drawing.Size(82, 29);
            this.btAprobar.TabIndex = 32;
            this.btAprobar.Text = "Aceptar";
            this.btAprobar.UseVisualStyleBackColor = true;
            this.btAprobar.Click += new System.EventHandler(this.btAprobar_Click);
            // 
            // btnServirDoc
            // 
            this.btnServirDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnServirDoc.Location = new System.Drawing.Point(16, 373);
            this.btnServirDoc.Name = "btnServirDoc";
            this.btnServirDoc.Size = new System.Drawing.Size(127, 29);
            this.btnServirDoc.TabIndex = 30;
            this.btnServirDoc.Text = "Servir Doc";
            this.btnServirDoc.UseVisualStyleBackColor = true;
            this.btnServirDoc.Click += new System.EventHandler(this.btnServirDoc_Click);
            // 
            // btHide2
            // 
            this.btHide2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide2.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHide2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide2.Location = new System.Drawing.Point(280, 6);
            this.btHide2.Name = "btHide2";
            this.btHide2.Size = new System.Drawing.Size(27, 28);
            this.btHide2.TabIndex = 29;
            this.btHide2.UseVisualStyleBackColor = true;
            this.btHide2.Click += new System.EventHandler(this.btHide2_Click);
            // 
            // comboOperacion
            // 
            this.comboOperacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboOperacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboOperacion.FormattingEnabled = true;
            this.comboOperacion.Items.AddRange(new object[] {
            "Todo el documento",
            "Por selección"});
            this.comboOperacion.Location = new System.Drawing.Point(16, 226);
            this.comboOperacion.Name = "comboOperacion";
            this.comboOperacion.Size = new System.Drawing.Size(271, 28);
            this.comboOperacion.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Operación:";
            // 
            // btnCambiarPVP
            // 
            this.btnCambiarPVP.Enabled = false;
            this.btnCambiarPVP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambiarPVP.Location = new System.Drawing.Point(13, 172);
            this.btnCambiarPVP.Name = "btnCambiarPVP";
            this.btnCambiarPVP.Size = new System.Drawing.Size(144, 28);
            this.btnCambiarPVP.TabIndex = 23;
            this.btnCambiarPVP.Text = "Cambiar precio PVP";
            this.btnCambiarPVP.UseVisualStyleBackColor = true;
            this.btnCambiarPVP.Click += new System.EventHandler(this.btnCambiarPVP_Click);
            // 
            // btnCambiarPrecio1
            // 
            this.btnCambiarPrecio1.Enabled = false;
            this.btnCambiarPrecio1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnCambiarPrecio1.Location = new System.Drawing.Point(13, 104);
            this.btnCambiarPrecio1.Name = "btnCambiarPrecio1";
            this.btnCambiarPrecio1.Size = new System.Drawing.Size(144, 28);
            this.btnCambiarPrecio1.TabIndex = 21;
            this.btnCambiarPrecio1.Text = "Cambiar precio 1";
            this.btnCambiarPrecio1.UseVisualStyleBackColor = true;
            this.btnCambiarPrecio1.Click += new System.EventHandler(this.btnCambiarPrecio1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(15, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 28;
            this.label3.Text = "Copiar precios";
            // 
            // btnCambiarPrecio2
            // 
            this.btnCambiarPrecio2.Enabled = false;
            this.btnCambiarPrecio2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnCambiarPrecio2.Location = new System.Drawing.Point(13, 138);
            this.btnCambiarPrecio2.Name = "btnCambiarPrecio2";
            this.btnCambiarPrecio2.Size = new System.Drawing.Size(144, 28);
            this.btnCambiarPrecio2.TabIndex = 22;
            this.btnCambiarPrecio2.Text = "Cambiar precio 2";
            this.btnCambiarPrecio2.UseVisualStyleBackColor = true;
            this.btnCambiarPrecio2.Click += new System.EventHandler(this.btnCambiarPrecio2_Click);
            // 
            // txtMargenPrecio1
            // 
            this.txtMargenPrecio1.Enabled = false;
            this.txtMargenPrecio1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtMargenPrecio1.Location = new System.Drawing.Point(207, 108);
            this.txtMargenPrecio1.Name = "txtMargenPrecio1";
            this.txtMargenPrecio1.Size = new System.Drawing.Size(68, 26);
            this.txtMargenPrecio1.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(203, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Margen";
            // 
            // txtMargenPrecio2
            // 
            this.txtMargenPrecio2.Enabled = false;
            this.txtMargenPrecio2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtMargenPrecio2.Location = new System.Drawing.Point(207, 144);
            this.txtMargenPrecio2.Name = "txtMargenPrecio2";
            this.txtMargenPrecio2.Size = new System.Drawing.Size(68, 26);
            this.txtMargenPrecio2.TabIndex = 26;
            // 
            // labelAcciones
            // 
            this.labelAcciones.AutoSize = true;
            this.labelAcciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAcciones.Location = new System.Drawing.Point(3, 3);
            this.labelAcciones.Name = "labelAcciones";
            this.labelAcciones.Size = new System.Drawing.Size(82, 20);
            this.labelAcciones.TabIndex = 18;
            this.labelAcciones.Text = "Acciones";
            // 
            // comboBoxPrecios
            // 
            this.comboBoxPrecios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrecios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboBoxPrecios.FormattingEnabled = true;
            this.comboBoxPrecios.Items.AddRange(new object[] {
            "1 > 3",
            "2 > 3",
            "2 > 1",
            "3 > 1",
            "3 > 2"});
            this.comboBoxPrecios.Location = new System.Drawing.Point(16, 54);
            this.comboBoxPrecios.Name = "comboBoxPrecios";
            this.comboBoxPrecios.Size = new System.Drawing.Size(141, 28);
            this.comboBoxPrecios.TabIndex = 19;
            // 
            // btnCopiarPrecios
            // 
            this.btnCopiarPrecios.Enabled = false;
            this.btnCopiarPrecios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnCopiarPrecios.Location = new System.Drawing.Point(163, 53);
            this.btnCopiarPrecios.Name = "btnCopiarPrecios";
            this.btnCopiarPrecios.Size = new System.Drawing.Size(127, 29);
            this.btnCopiarPrecios.TabIndex = 20;
            this.btnCopiarPrecios.Text = "Copiar precios";
            this.btnCopiarPrecios.UseVisualStyleBackColor = true;
            this.btnCopiarPrecios.Click += new System.EventHandler(this.btnCopiarPrecios_Click);
            // 
            // tbP3Proveedores
            // 
            this.tbP3Proveedores.Controls.Add(this.btHide3);
            this.tbP3Proveedores.Controls.Add(this.btAsignarProveedor);
            this.tbP3Proveedores.Controls.Add(this.cboxProveedores);
            this.tbP3Proveedores.Controls.Add(this.label4);
            this.tbP3Proveedores.Controls.Add(this.dgvProveedores);
            this.tbP3Proveedores.Controls.Add(this.btnResumenProveedores);
            this.tbP3Proveedores.Location = new System.Drawing.Point(23, 4);
            this.tbP3Proveedores.Name = "tbP3Proveedores";
            this.tbP3Proveedores.Size = new System.Drawing.Size(313, 629);
            this.tbP3Proveedores.TabIndex = 2;
            this.tbP3Proveedores.Text = "Proveedores";
            this.tbP3Proveedores.UseVisualStyleBackColor = true;
            // 
            // btHide3
            // 
            this.btHide3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide3.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHide3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide3.Location = new System.Drawing.Point(277, 9);
            this.btHide3.Name = "btHide3";
            this.btHide3.Size = new System.Drawing.Size(27, 28);
            this.btHide3.TabIndex = 35;
            this.btHide3.UseVisualStyleBackColor = true;
            this.btHide3.Click += new System.EventHandler(this.btHide3_Click);
            // 
            // btAsignarProveedor
            // 
            this.btAsignarProveedor.Location = new System.Drawing.Point(151, 92);
            this.btAsignarProveedor.Name = "btAsignarProveedor";
            this.btAsignarProveedor.Size = new System.Drawing.Size(75, 23);
            this.btAsignarProveedor.TabIndex = 34;
            this.btAsignarProveedor.Text = "Asignar";
            this.btAsignarProveedor.UseVisualStyleBackColor = true;
            this.btAsignarProveedor.Click += new System.EventHandler(this.btAsignarProveedor_Click);
            // 
            // cboxProveedores
            // 
            this.cboxProveedores.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboxProveedores.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxProveedores.FormattingEnabled = true;
            this.cboxProveedores.Location = new System.Drawing.Point(12, 58);
            this.cboxProveedores.Name = "cboxProveedores";
            this.cboxProveedores.Size = new System.Drawing.Size(214, 28);
            this.cboxProveedores.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 20);
            this.label4.TabIndex = 32;
            this.label4.Text = "Proveedores";
            // 
            // dgvProveedores
            // 
            this.dgvProveedores.AllowUserToAddRows = false;
            this.dgvProveedores.AllowUserToDeleteRows = false;
            this.dgvProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProveedores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProveedores.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProveedores.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvProveedores.Location = new System.Drawing.Point(12, 327);
            this.dgvProveedores.Name = "dgvProveedores";
            this.dgvProveedores.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProveedores.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvProveedores.Size = new System.Drawing.Size(298, 299);
            this.dgvProveedores.TabIndex = 22;
            // 
            // btnResumenProveedores
            // 
            this.btnResumenProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResumenProveedores.Location = new System.Drawing.Point(12, 278);
            this.btnResumenProveedores.Name = "btnResumenProveedores";
            this.btnResumenProveedores.Size = new System.Drawing.Size(95, 43);
            this.btnResumenProveedores.TabIndex = 21;
            this.btnResumenProveedores.Text = "Resumen Proveedores";
            this.btnResumenProveedores.UseVisualStyleBackColor = true;
            this.btnResumenProveedores.Click += new System.EventHandler(this.btnResumenProveedores_Click);
            // 
            // tbColumns
            // 
            this.tbColumns.Controls.Add(this.button1);
            this.tbColumns.Controls.Add(this.lbVisualizarColumns);
            this.tbColumns.Controls.Add(this.clboxColumns);
            this.tbColumns.Location = new System.Drawing.Point(23, 4);
            this.tbColumns.Name = "tbColumns";
            this.tbColumns.Size = new System.Drawing.Size(313, 629);
            this.tbColumns.TabIndex = 4;
            this.tbColumns.Text = "Columnas";
            this.tbColumns.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(105, 431);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Mostrar Todo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbVisualizarColumns
            // 
            this.lbVisualizarColumns.AutoSize = true;
            this.lbVisualizarColumns.Location = new System.Drawing.Point(19, 32);
            this.lbVisualizarColumns.Name = "lbVisualizarColumns";
            this.lbVisualizarColumns.Size = new System.Drawing.Size(100, 13);
            this.lbVisualizarColumns.TabIndex = 1;
            this.lbVisualizarColumns.Text = "Visualizar Columnas";
            // 
            // clboxColumns
            // 
            this.clboxColumns.FormattingEnabled = true;
            this.clboxColumns.Location = new System.Drawing.Point(22, 61);
            this.clboxColumns.Name = "clboxColumns";
            this.clboxColumns.Size = new System.Drawing.Size(172, 364);
            this.clboxColumns.TabIndex = 0;
            this.clboxColumns.SelectedIndexChanged += new System.EventHandler(this.clboxColumns_SelectedIndexChanged);
            // 
            // statusStripBottom
            // 
            this.statusStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssInfoProceso});
            this.statusStripBottom.Location = new System.Drawing.Point(0, 707);
            this.statusStripBottom.Name = "statusStripBottom";
            this.statusStripBottom.Size = new System.Drawing.Size(1248, 22);
            this.statusStripBottom.TabIndex = 3;
            this.statusStripBottom.Text = "statusStrip1";
            // 
            // tssInfoProceso
            // 
            this.tssInfoProceso.Name = "tssInfoProceso";
            this.tssInfoProceso.Size = new System.Drawing.Size(0, 17);
            // 
            // frEuroline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 729);
            this.Controls.Add(this.statusStripBottom);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frEuroline";
            this.Text = "Euroline";
            this.Load += new System.EventHandler(this.frEuroline_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabP0.ResumeLayout(false);
            this.tabP1Info.ResumeLayout(false);
            this.tabP1Info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfoDoc)).EndInit();
            this.tbP2Prices.ResumeLayout(false);
            this.tbP2Prices.PerformLayout();
            this.tbP3Proveedores.ResumeLayout(false);
            this.tbP3Proveedores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedores)).EndInit();
            this.tbColumns.ResumeLayout(false);
            this.tbColumns.PerformLayout();
            this.statusStripBottom.ResumeLayout(false);
            this.statusStripBottom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxTarifa;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxSerie;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxOferta;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblReferencia;
        private System.Windows.Forms.Label lblNomCli;
        private System.Windows.Forms.Label lblOferta;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblSituacion;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.StatusStrip statusStripBottom;
        private System.Windows.Forms.ToolStripStatusLabel tssInfoProceso;
        private System.Windows.Forms.ToolStripLabel tsLabelYear;
        private System.Windows.Forms.ToolStripComboBox tsCBoxYear;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabP1Info;
        private System.Windows.Forms.TabPage tbP2Prices;
        private System.Windows.Forms.ComboBox comboOperacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCambiarPVP;
        private System.Windows.Forms.Button btnCambiarPrecio1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCambiarPrecio2;
        private System.Windows.Forms.TextBox txtMargenPrecio1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMargenPrecio2;
        private System.Windows.Forms.Label labelAcciones;
        private System.Windows.Forms.ComboBox comboBoxPrecios;
        private System.Windows.Forms.Button btnCopiarPrecios;
        private System.Windows.Forms.Button btAsignLineas;
        private System.Windows.Forms.DataGridView dgvInfoDoc;
        private System.Windows.Forms.TabPage tbP3Proveedores;
        private System.Windows.Forms.DataGridView dgvProveedores;
        private System.Windows.Forms.Button btnResumenProveedores;
        private System.Windows.Forms.Button btAsignarProveedor;
        private System.Windows.Forms.ComboBox cboxProveedores;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabP0;
        private System.Windows.Forms.Button btShow;
        private System.Windows.Forms.Button btHide;
        private System.Windows.Forms.Button btHide2;
        private System.Windows.Forms.Button btHide3;
        private System.Windows.Forms.Button btnServirDoc;
        private System.Windows.Forms.Label lblCodCli;
        private System.Windows.Forms.RadioButton rbPrintPreciosNO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbPrintPreciosSI;
        private System.Windows.Forms.Label lblIdOferta;
        private System.Windows.Forms.Label idDocumento;
        private System.Windows.Forms.Button btAprobar;
        private System.Windows.Forms.Button btnPendiente;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TabPage tbColumns;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbVisualizarColumns;
        private System.Windows.Forms.CheckedListBox clboxColumns;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}