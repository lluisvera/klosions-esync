﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csCarriers
    {
        public class Datum
        {
            public int idTransportista { get; set; }
            public int idEntidadTransportista { get; set; }
            public string nomTransportista { get; set; }
            public int codTransportista { get; set; }
            public int? autorTransportista { get; set; }
            public int activoTransportista { get; set; }
            public string fecAltaTransportista { get; set; }
            public string codExternoTransportista { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
