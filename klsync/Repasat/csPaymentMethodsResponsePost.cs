﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csPaymentMethodsResponsePost
    {
        public class Resource
        {
            public int idFormaPago { get; set; }
            public string nomFormaPago { get; set; }
            public int codFormaPago { get; set; }
            public int activoFormaPago { get; set; }
            //public int? autorFormaPago { get; set; }
            public string fecAltaFormaPago { get; set; }
            public int numVenFormaPago { get; set; }
            public int primerLapsoFormaPago { get; set; }
            public int siguientesLapsosFormaPago { get; set; }
            public int calculoVenFormaPago { get; set; }
            public object repartoProFormaPago { get; set; }
            public int pagadoFormaPago { get; set; }
            public int contratoRepasatTarjetaFormaPago { get; set; }
            public string codExternoFormaPago { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
