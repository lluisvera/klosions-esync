﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csWarehouses
    {
        public class Datum
        {
            public int idAlmacen { get; set; }
            public int idEntidadAlmacen { get; set; }
            public string refAlmacen { get; set; }
            public string nomAlmacen { get; set; }
            public int? idContacto { get; set; }
            public int? idDireccion { get; set; }
            public string fecAltaAlmacen { get; set; }
            public int activoAlmacen { get; set; }
            public string codExternoAlmacen { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
