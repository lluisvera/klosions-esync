﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csEmployees
    {
        public class Datum
        {
            public int idTrabajador { get; set; }
            public int? idResponsable { get; set; }
            public int codTrabajador { get; set; }
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string aliasTrabajador { get; set; }
            public string nifTrabajador { get; set; }
            public object idResponsableExt { get; set; }
            public string tipoRecursoTrabajador { get; set; }
            public string sexoTrabajador { get; set; }
            public int? fotoTrabajador { get; set; }
            public int? idDpto { get; set; }
            public int? idUsuario { get; set; }
            public string fecCadNif { get; set; }
            public object numSegSoc { get; set; }
            public string telf1 { get; set; }
            public string telf2 { get; set; }
            public string emailTrabajador { get; set; }
            public string fecNac { get; set; }
            public int? idEstadoCivil { get; set; }
            public object numHijos { get; set; }
            public object carnetConducirTrabajador { get; set; }
            public string paisNac { get; set; }
            public int? idTarifa { get; set; }
            public object idTurnos { get; set; }
            public int? idCalendario { get; set; }
            public string fecAntig { get; set; }
            public int? categoriaTrabajador { get; set; }
            public double? bolsaHorasTrabajador { get; set; }
            public int activoTrabajador { get; set; }
            public string fecAltaTrabajador { get; set; }
            public object fecBajaTrabajador { get; set; }
            public string colorCalendarioTrabajador { get; set; }
            public string prefTelf { get; set; }
            public object gradoDiscapacidad { get; set; }
            public object email2Trabajador { get; set; }
            public object cuentaContableTrabajador { get; set; }
            public object rangoSueldoTrabajador { get; set; }
            public string codExternoTrabajador { get; set; }
            public string nomCompleto { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

    }
}
