﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csRoutasResponsePost
    {
        public class Resource
        {
            public string nomRuta { get; set; }
            public string codExternoRuta { get; set; }
            public int idEntidadRuta { get; set; }
            public int codRuta { get; set; }
            public int idRuta { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }

    }
}
