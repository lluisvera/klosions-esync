﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csMarcaArtResponsePost
    {
        public class Resource
        {
            public string nomMarca { get; set; }
            public string codExternoMarca { get; set; }
            public int idEntidadMarca { get; set; }
            public int codMarca { get; set; }
            public string uuidMarca { get; set; }
            
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }

        }

    }
}
