﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.Repasat
{
    class csIncomingPayments
    {
        public class Customer
        {
            public int? idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int? idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public string porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public string cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public int? idTipoRetencion { get; set; }
            public int? idEmpresaAccesoExterno { get; set; }
            public int? idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public string representadoCli { get; set; }
            public string comisionCli { get; set; }
            public string descuentoCli { get; set; }
            public int? idFormatoImpresionPresupuesto { get; set; }
            public int? idFormatoImpresionPedidoVenta { get; set; }
            public int? idFormatoImpresionPedidoCompra { get; set; }
            public int? idFormatoImpresionAlbaranVenta { get; set; }
            public int? idFormatoImpresionAlbaranCompra { get; set; }
            public int? idFormatoImpresionFacturaVenta { get; set; }
            public int? idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int? idRuta { get; set; }
            public string fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public int? idTarifaDescuentos { get; set; }
            public int? idSector { get; set; }
            public int? idMotivoBaja { get; set; }
            public int? idTamanoEmpresa { get; set; }
        }

        public class PaymentDocument
        {
            public int? idDocuPago { get; set; }
            public string nomDocuPago { get; set; }
            public int? idDatosBancarios { get; set; }
            public string descDocuPago { get; set; }
            public int codDocuPago { get; set; }
            public int remesableDocuPago { get; set; }
            public int envioRecDocuPago { get; set; }
            public int efectivoDocuPago { get; set; }
            public string fecAltaDocuPago { get; set; }
            public string autorDocuPago { get; set; }
            public int activoDocuPago { get; set; }
            public int contratoRepasatTarjetaDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
        }

        public class Series
        {
            public int idSerie { get; set; }
            public string nomSerie { get; set; }
            public int? contSerie { get; set; }
            public int? contFacturasSerie { get; set; }
            public int? contAbonosSerie { get; set; }
            public int? contFacturasCompraSerie { get; set; }
            public int? contAbonosCompraSerie { get; set; }
            public int? contPedidosSerie { get; set; }
            public int? contRecibosSerie { get; set; }
            public int? serieAplicacionSerie { get; set; }
            public int? autorSerie { get; set; }
            public int? contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public int? activoSerie { get; set; }
            public int? contPedidosVentaSerie { get; set; }
            public int? contPedidosCompraSerie { get; set; }
            public int? contAlbaranesVentaSerie { get; set; }
            public int? contAlbaranesCompraSerie { get; set; }
            public int? contAlbaranesRegularizacionSerie { get; set; }
            public int? contAnticiposSerie { get; set; }
            public int? contTareasSerie { get; set; }
            public string codExternoSerie { get; set; }
            public string refSerie { get; set; }
        }

        public class Invoice
        {
            public int? idDocumento { get; set; }
            public string tipoDocumento { get; set; }
            public int? idCli { get; set; }
            public int? idProyecto { get; set; }
            public int? idDireccionEnvio { get; set; }
            public int? idDireccionFacturacion { get; set; }
            public int? idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public string refDocumento { get; set; }
            public int? idFormaPago { get; set; }
            public int? idCam { get; set; }
            public double? totalBaseImponibleDocumento { get; set; }
            public double? totalImpuestosDocumento { get; set; }
            public double? totalDocumento { get; set; }
            public double? totalPagadoDocumento { get; set; }
            public int anticipoDocumento { get; set; }
            public int porcentajeRetencionDocumento { get; set; }
            public int totalRetencionDocumento { get; set; }
            public string domiciliacionBancariaDocumento { get; set; }
            public int? idContratoCli { get; set; }
            public string mesContratoCliDocumento { get; set; }
            public string anoContratoCliDocumento { get; set; }
            public string fecDocumento { get; set; }
            public string fecContableDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public int? idTrabajador { get; set; }
            public int? idUsuario { get; set; }
            public int? idDocuPago { get; set; }
            public string portesDocumento { get; set; }
            public int? idTipoImpuestoPortes { get; set; }
            public string totalImpuestosPortesDocumento { get; set; }
            public string totalPortesDocumento { get; set; }
            public int porcentajeDescuentoDocumento { get; set; }
            public string codExternoDocumento { get; set; }
            public int? idTarifa { get; set; }
            public int? idAlmacen { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public int? idTipoRetencion { get; set; }
            public string totalRecargoEquivalenciaDocumento { get; set; }
            public string totalRetencionesDocumento { get; set; }
            public string totalBaseRetencionesDocumento { get; set; }
            public string totalRecargoEquivalenciaPortesDocumento { get; set; }
            public string fecEnvioMailDocumento { get; set; }
            public int? idRepresentado { get; set; }
            public string observacionesCabeceraDocumento { get; set; }
            public string observacionesPieDocumento { get; set; }
            public int? idTipoImpuesto { get; set; }
            public int? idTransportista { get; set; }
            public int? idTarifaDescuentos { get; set; }
            public string totalCosteDocumento { get; set; }
            public string margenComercialDocumento { get; set; }
            public int? idContacto { get; set; }
            public string origenDocumento { get; set; }
            public Series series { get; set; }
        }

        public class Bank
        {
            public int idDatosBancarios { get; set; }
            public string nomDatosBancarios { get; set; }
            public string titularCuentaDatosBancarios { get; set; }
            public string ibanCuentaDatosBancarios { get; set; }
            public string entidadCuentaDatosBancarios { get; set; }
            public string agenciaCuentaDatosBancarios { get; set; }
            public string dcCuentaDatosBancarios { get; set; }
            public string numCuentaDatosBancarios { get; set; }
            public string bicCuentaDatosBancarios { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaDatosBancarios { get; set; }
            public string cuentaContableDatosBancarios { get; set; }
            public string sufijoRemesaCobro { get; set; }
            public int? efectivoDatosBancarios { get; set; }
            public string codExternoDatosBancarios { get; set; }
            public int? idCuentaContable { get; set; }
        }

        public class Remittance
        {
            public int idRemesa { get; set; }
            public int idRefSerie { get; set; }
            public string fechaGeneracion { get; set; }
            public string fechaCobro { get; set; }
            public int estadoCobro { get; set; }
            public string importeRemesaTotal { get; set; }
            public int idBancoRemesa { get; set; }
            public double gastosRemesa { get; set; }
            public int tipoCarteraRemesa { get; set; }
            public string codExternoRemesa { get; set; }
            public string fecAltaRemesa { get; set; }
            public string fecModificacionRemesa { get; set; }
        }

        public class Datum
        {
            public int? idCarteraCobros { get; set; }
            public int? idDocumento { get; set; }
            public int? idCli { get; set; }
            public int? idEntidadCarteraCobros { get; set; }
            public int numVencimientoCarteraCobros { get; set; }
            public string fecVencimientoCarteraCobros { get; set; }
            public string fecImpagadoCarteraCobros { get; set; }
            public string fecCobroCarteraCobros { get; set; }
            public double? gastosCobroCarteraCobros { get; set; }
            public string procedenciaCarteraCobros { get; set; }
            public string numGrupoCarteraCobros { get; set; }
            public double importeCarteraCobros { get; set; }
            public double importeCobradoCarteraCobros { get; set; }
            public int? idDocuPago { get; set; }
            public int? idDatosBancarios { get; set; }
            public int impagadoCarteraCobros { get; set; }
            public double? gastosImpagadoCarteraCobros { get; set; }
            public int? numeroRemesaCarteraCobros { get; set; }
            public int estadoCarteraCobros { get; set; }
            public int situacionCarteraCobros { get; set; }
            public string fecAltaCarteraCobros { get; set; }
            public string refSerie { get; set; }
            public int? idRepresentado { get; set; }
            public double? despesasImpagado { get; set; }
            public double? importeCobroOriginal { get; set; }
            public string vencimientoOriginal { get; set; }
            public int? idDocuImpagado { get; set; }
            public int anticipoCarteraCobros { get; set; }
            public int? idSerieAnticipo { get; set; }
            public string referenciaAnticipo { get; set; }
            public string numDocumentoAnticipo { get; set; }
            public int estadoAnticipo { get; set; }
            public string numAgrupacionCarteraCobros { get; set; }
            public bool padreAgrupacionCarteraCobros { get; set; }
            public string codExternoCarteraCobros { get; set; }
            public string refAgrupacionCarteraCobros { get; set; }
            public int? idRemesa { get; set; }
            public int? codExternoAgrupacion { get; set; }
            public bool? sincronizarCartera { get; set; }
            public int? efectoSincronizado { get; set; }

            public Customer customer { get; set; }
            public PaymentDocument payment_document { get; set; }
            public Invoice invoice { get; set; }
            public Bank bank { get; set; }
            public Remittance remittance { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }



        public DataSet cargarEfectosA3ToRepasat(DataTable dt, bool resetear=false)
        {
            try
            {
                string rowKey = "";
                DataSet datasetRPST = new DataSet();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow dr in dt.Rows)
                    {
                        rowKey =  dr["idCarteraCobros"].ToString();

                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {

                            //if (dtcol.ColumnName == "idRuta")
                            //{
                            //    string stop = "";
                            //}
                            if (string.IsNullOrEmpty(dr[dtcol.ColumnName].ToString()))
                            {
                                continue;
                            }
                            else
                            {
                                if (dtcol.ColumnName == "DOCPAG" || dtcol.ColumnName == "FORPAG")
                                {
                                    continue;
                                }
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcol.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = dr[dtcol.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);

                    return datasetRPST;
                }
                else
                {
                    return null;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
