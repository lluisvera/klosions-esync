﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.Repasat
{
    class csObjetoRepasat
    {


        public DataSet cargarObjetoERPToRepasat(string tipoObjeto)
        {
            try
            {
                string rowKey = "";
                string query = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csRPSTSqlScripts sqlScript = new csRPSTSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();

                if (tipoObjeto == "RUTAS") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    query = "select NOMRUTA AS nomRuta, ltrim(RUTA) AS codExternoRuta, ltrim(RUTA) AS codExternoObjeto  from rutas where RPST_ID_RUTA is null or RPST_ID_RUTA=0";
                }

                if (tipoObjeto == "REPRESENTANTES") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    query = "SELECT LTRIM(CODREP) AS codExternoTrabajador, " +
                        " SUBSTRING(UPPER(NOMREP), 1, CASE CHARINDEX(' ', NOMREP) WHEN 0 THEN LEN(NOMREP) ELSE CHARINDEX(' ', UPPER(NOMREP)) - 1 END) AS nombreTrabajador, " +
                        " SUBSTRING(UPPER(NOMREP), CASE CHARINDEX(' ', NOMREP) WHEN 0 THEN 1 ELSE CHARINDEX(' ', UPPER(NOMREP)) + 1 END, 1000) AS apellidosTrabajador, " +
                        " UPPER(NOMREP) as aliasTrabajador, " +
                        " REPLACE(TELREP,'.','') AS telf1, REPLACE(TEL2REP,'.','') as telf2, NIFREP as nifTrabajador, E_MAIL as emailTrabajador, " +
                        " LTRIM(CODREP) AS codExternoObjeto, '1' AS activoTrabajador " +
                        " FROM REPRESEN" +
                        " WHERE RPST_ID_REPRE IS NULL OR  RPST_ID_REPRE=0";
                }

                if (tipoObjeto == "FORMASPAG") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    query = "SELECT " +
                        " UPPER(DESCFOR) AS nomFormaPago,ltrim(FORPAG) as codExternoFormaPago,'1' as activoFormaPago, " +
                        " NUMVENCIM AS numVenFormaPago,PRIMLAPSO AS primerLapsoFormaPago,SIGLAPSO as siguientesLapsosFormaPago, " +
                        " CASE WHEN COBPAGAUTO='T' THEN 1 ELSE 0 END  as pagadoFormaPago, LTRIM(FORPAG) AS codExternoObjeto " +
                        " FROM FORMAPAG " +
                        " WHERE RPST_ID_FORMAPAG IS NULL OR  RPST_ID_FORMAPAG=0";
                }

                if (tipoObjeto == "DOCSPAG") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    query = "SELECT UPPER(DESCDOC) AS nomDocuPago, LTRIM(DOCPAG) AS codExternoDocuPago, REMESABLE AS remesableDocuPago, " +
                        " Recibir as envioRecDocuPago, metalico as efectivoDocuPago, '1' as activoDocuPago, LTRIM(DOCPAG) AS codExternoObjeto " +
                        " from docupago " +
                        " WHERE RPST_ID_DOCPAGO IS NULL OR  RPST_ID_DOCPAGO=0";
                }

                if (tipoObjeto == "ZONAS")
                {
                    query = "SELECT UPPER(NOMZONA) AS nomZonaGeo, LTRIM(ZONA) AS codExternoZonaGeo,  " +
                       " LTRIM(ZONA) AS codExternoObjeto " +
                       " from zonas " +
                       " WHERE RPST_ID_ZONA IS NULL OR  RPST_ID_ZONA=0";
                }

                if (tipoObjeto == "SERIES")
                {
                    query = "SELECT LTRIM(UPPER(SERIE)) AS nomSerie, LTRIM(SERIE) AS codExternoSerie,  " +
                       " LTRIM(SERIE) AS codExternoObjeto " +
                       " from SERIES " +
                       " WHERE RPST_ID_SERIE IS NULL OR  RPST_ID_SERIE=0";
                }

                if (tipoObjeto == "CUENTASCONTABLES") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    query = "SELECT UPPER(DESCCUE) AS nomCuentaContable, CUENTA AS numCuentaContable, CUENTA AS codExtCuentaContable, " +
                        " CUENTA AS codExternoObjeto, CASE WHEN LEFT(CUENTA,1)<=5  THEN 'bal' ELSE 'pyg' END AS partidaCuentaContable " +
                        " from CUENTAS " +
                        " WHERE (RPST_ID_CTA IS NULL OR  RPST_ID_CTA=0) AND NIVEL=5 AND PLACON='NPGC' ORDER BY CUENTA";
                }

                if (tipoObjeto == "CENTROSC") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    string anexoUnidadeNegocio = csGlobal.activarUnidadesNegocio ? ", " + csGlobal.idUnidadNegocio + " AS idUnidadNegocio " : "";
                    query = "SELECT ltrim(CENTROCOSTE) as codExternoObjeto ,ltrim(CENTROCOSTE) as codExternoCentroC ,DESCCENTRO AS descCentroCoste, RPST_ID_CENTROC AS ID_REPASAT," +
                        " CASE WHEN APLINIVEL1 = 'T' THEN 1 ELSE 0 END AS level1 , " +
                        " CASE WHEN APLINIVEL2 = 'T' THEN 1 ELSE 0 END AS level2 , " +
                        " CASE WHEN APLINIVEL3 = 'T' THEN 1 ELSE 0 END AS level3,  " +
                        " ltrim(CENTROCOSTE) as refCentroCoste  " + anexoUnidadeNegocio +
                        " FROM CENTROSC " +
                        " WHERE BLOQUEADO = 'F' AND (RPST_ID_CENTROC IS NULL OR  RPST_ID_CENTROC='' OR  RPST_ID_CENTROC='0') ORDER BY CENTROCOSTE";
                }

                if (tipoObjeto == "FAMILIAART") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    string fam = csGlobal.familia_art;
                    query = "select LTRIM(CODCAR) AS codExternoFamilia, '1' as activoFam, '0' as tipoFam, UPPER(DESCCAR) AS nomFam, LTRIM(CODCAR) AS codExternoObjeto " +
                        "from CARACTERISTICAS where LTRIM(NUMCAR) = '" + fam + "' and TIPCAR = 'A' and " +
                        "(RPST_ID_CARACTERISTICA is null or RPST_ID_CARACTERISTICA='');";
                }

                if (tipoObjeto == "SUBFAMILIAART") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    string subfam = csGlobal.subfamilia_art;
                    query = "select LTRIM(CODCAR) AS codExternoFamilia, '1' as activoFam, '1' as tipoFam, UPPER(DESCCAR) AS nomFam, LTRIM(CODCAR) AS codExternoObjeto " +
                        "from CARACTERISTICAS where LTRIM(NUMCAR) = '" + subfam + "' and TIPCAR = 'A' and " +
                        "(RPST_ID_CARACTERISTICA is null or RPST_ID_CARACTERISTICA='');";
                }

                if (tipoObjeto == "MARCAART") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    string marca = csGlobal.marcas_art;
                    query = "select LTRIM(CODCAR) AS codExternoMarca, '1' as activoMarca, UPPER(DESCCAR) AS nomMarca, LTRIM(CODCAR) AS codExternoObjeto " +
                        "from CARACTERISTICAS where LTRIM(NUMCAR) = '" + marca + "' and TIPCAR = 'A' and" +
                        "(RPST_ID_CARACTERISTICA is null or RPST_ID_CARACTERISTICA='');";
                }

                if (tipoObjeto == "TIPOART") // OBJETOS SIEMPRE EN MAYÚSCULAS
                {
                    string tipoArt = csGlobal.tipo_art;
                    query = "select LTRIM(CODCAR) AS codExternoTipoArticulo, '1' as activoTipoArticulo, UPPER(DESCCAR) AS nomTipoArticulo, LTRIM(CODCAR) AS codExternoObjeto " +
                        "from CARACTERISTICAS where LTRIM(NUMCAR) = '" + tipoArt + "' and TIPCAR = 'A' and" +
                        "(RPST_ID_CARACTERISTICA is null or RPST_ID_CARACTERISTICA=0);";
                }

                dt = sqlConnect.obtenerDatosSQLScript(query);

                Objetos.csTercero cliente = new Objetos.csTercero();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow dr in dt.Rows)
                    {
                        rowKey = dr["codExternoObjeto"].ToString();
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {
                            if (dtcol.ColumnName != "codExternoObjeto")
                            { 
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcol.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = dr[dtcol.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);
                    //csRepasatWebService rpstWS = new csRepasatWebService();
                    //rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "");

                    return datasetRPST;
                }
                else
                {
                    return null;
                }
            }

            catch (Exception)
            {
                return null;
            }
        }

    }
}
