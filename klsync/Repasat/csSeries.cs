﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csSeries
    {
        public class Datum
        {
            public int? idSerie { get; set; }
            public string nomSerie { get; set; }
            public string contSerie { get; set; }
            public long? contFacturasSerie { get; set; }
            public long? contAbonosSerie { get; set; }
            public long? contFacturasCompraSerie { get; set; }
            public long? contAbonosCompraSerie { get; set; }
            public long? contPedidosSerie { get; set; }
            public long? contRecibosSerie { get; set; }
            public long? serieAplicacionSerie { get; set; }
            public long? autorSerie { get; set; }
            public long? contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public long? activoSerie { get; set; }
            public long? contPedidosVentaSerie { get; set; }
            public long? contPedidosCompraSerie { get; set; }
            public long? contAlbaranesVentaSerie { get; set; }
            public long? contAlbaranesCompraSerie { get; set; }
            public long? contAlbaranesRegularizacionSerie { get; set; }
            public long? contAnticiposSerie { get; set; }
            public long? contTareasSerie { get; set; }
            public string codExternoSerie { get; set; }
            /// <summary>
            /// Se utiliza esta variable para informar que serie utilizar en caso de Facturas Rectificativas
            /// </summary>
            public string refSerie { get; set; } 
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
