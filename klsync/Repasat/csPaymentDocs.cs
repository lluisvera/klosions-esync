﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csPaymentDocs
    {

        public class Datum
        {
            public int idDocuPago { get; set; }
            public int idEntidadDocuPago { get; set; }
            public string nomDocuPago { get; set; }
            public int? idDatosBancarios { get; set; }
            public string descDocuPago { get; set; }
            public int codDocuPago { get; set; }
            public int? remesableDocuPago { get; set; }
            public int? envioRecDocuPago { get; set; }
            public int? efectivoDocuPago { get; set; }
            public string fecAltaDocuPago { get; set; }
            public int? autorDocuPago { get; set; }
            public int activoDocuPago { get; set; }
            public int? contratoRepasatTarjetaDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
            public object fecSincronizacionDocuPago { get; set; }
            public int procesarEnNotaGasto { get; set; }
            public List<AuxiliarExterno> auxiliar_externo { get; set; }
        }
        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
            public Pivot pivot { get; set; }
        }
        public class AuxiliarExterno
        {
            public int idConExt { get; set; }
            public int idUsuario { get; set; }
            public object idUsuarioUpdate { get; set; }
            public string created_at { get; set; }
            public object updated_at { get; set; }
            public string tipoConexion { get; set; }
            public string nomConexion { get; set; }
            public object observacionesConexion { get; set; }
            public Pivot pivot { get; set; }
        }
        public class Pivot
        {
            public int idRepasat { get; set; }
            public int idConExt { get; set; }
            public int idEntidadAuxiliaresConExterna { get; set; }
            public string tipoValor { get; set; }
            public string idExterno { get; set; }
        }
    }
}
