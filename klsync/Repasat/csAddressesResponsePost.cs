﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csAddressesResponsePost
    {

        public class Account
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public object descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public object idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            //public object idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int? activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public string diaPago1Cli { get; set; }
            public string diaPago2Cli { get; set; }
            public string diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public int porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public int? idTipoRetencion { get; set; }
            public int? idEmpresaAccesoExterno { get; set; }
            public int? idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public string representadoCli { get; set; }
            public int? comisionCli { get; set; }
            public int? descuentoCli { get; set; }
            public int? idFormatoImpresionPresupuesto { get; set; }
            public int? idFormatoImpresionPedidoVenta { get; set; }
            public int? idFormatoImpresionPedidoCompra { get; set; }
            public int? idFormatoImpresionAlbaranVenta { get; set; }
            public int? idFormatoImpresionAlbaranCompra { get; set; }
            public int? idFormatoImpresionFacturaVenta { get; set; }
            public int? idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int? idRuta { get; set; }
            public string fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
        }

        public class Address
        {
            public int idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public object refDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int idProvincia { get; set; }
            public string idPais { get; set; }
            public object idUsuario { get; set; }
            public object latitudDireccion { get; set; }
            public object longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
            public object horarioDireccion { get; set; }
            public object idRuta { get; set; }
            public string fecModificacionDireccion { get; set; }
        }

        public class Resource
        {
            public int idDireccionCli { get; set; }
            public int idDireccion { get; set; }
            public int idCli { get; set; }
            public int principalDireccion { get; set; }
            public int facturacionDireccion { get; set; }
            public int? idContacto { get; set; }
            public string tipoDireccion { get; set; }
            public int codExternoDireccion { get; set; }
            public Account account { get; set; }
            public Address address { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }

    }
}
