﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using klsync.Utilidades;
using klsync.Objetos;

namespace klsync.Repasat
{
    public class csUpdateData
    {
        csSqlConnects sql = new csSqlConnects();
        csRepasatUtilities rpstUtil = new csRepasatUtilities();
        csLogErrores logProceso = new csLogErrores();
        csLogErrores errorLog = new csLogErrores();
        csMapearDatosClientes mapper = new csMapearDatosClientes();


        /// <summary>
        /// Función que devuelve la última fecha de syncronización
        /// </summary>
        /// <param name="objeto"></param>
        /// <returns></returns>
        public string getLastUpdateSyncDate(string objeto, bool origenRepasat)
        {
            string fromRepasat = origenRepasat ? "1" : "0";
            string fechaUltimaSyncro = "";
            DateTime fechaSync = DateTime.Now;

            fechaUltimaSyncro =  sql.obtenerCampoTabla("SELECT MAX(DATE_SYNC) FROM KLS_REPASAT_UPDATE WHERE TABLA='" + objeto + "' AND FROM_RPST=" + fromRepasat);

            if (string.IsNullOrEmpty(fechaUltimaSyncro))
            {
                //fechaSync = DateTime.Now;    
                if (objeto == "accounts")
                {
                    fechaSync = Convert.ToDateTime(sql.obtenerCampoTabla("select min(fecalta) from CLIENTES"));
                }

            }
            else
            {
                fechaSync = Convert.ToDateTime(fechaUltimaSyncro).AddHours(-1);
            }

            fechaUltimaSyncro = origenRepasat ? fechaSync.ToString("yyyy-MM-dd HH:mm:ss") : fechaSync.ToString("dd-MM-yy HH:mm:ss");
            return fechaUltimaSyncro;
        }

        public void resetLastUpdateSyncDate(DateTime fechaDesdeReset)
        {
            csUtilidades.ejecutarConsulta("DELETE FROM KLS_REPASAT_UPDATE", false);

            //string fechaActualizacion = DateTime.Now.ToString("dd-MM-yy HH:mm:ss");
            string fechaActualizacion = fechaDesdeReset.ToString("dd-MM-yy HH:mm:ss");

            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','accounts',0,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','accounts',1,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','products',0,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','products',1,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','contacts',0,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','contacts',1,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','address',0,0)", false);
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','address',1,0)", false);
        }

        public void actualizarFechaSync(string objeto,int numeroRegistros,bool fromA3ERP=false)
        {
            string fechaActualizacion = DateTime.Now.ToString("dd-MM-yy HH:mm:ss");
            string desdeRepasat = fromA3ERP ? "0" : "1";
            csUtilidades.ejecutarConsulta("INSERT INTO KLS_REPASAT_UPDATE (DATE_SYNC,TABLA, FROM_RPST,NUMREGISTROS) VALUES ('" + fechaActualizacion + "','" + objeto + "'," + desdeRepasat + "," + numeroRegistros.ToString() + ")", false);

        }

        public void cargarCuentasA3ToRepasat(bool clientes = true, bool update = false, string accountsToUpdate = null, bool cronjob = false)
        {
            try
            {
                string metodoCRUD = update ? "PUT" : "POST";
                Repasat.csAccounts accountRPST = new Repasat.csAccounts();
                DataSet dtsAccounts = accountRPST.cargarCuentasA3ToRepasat(clientes, update,accountsToUpdate,cronjob);
                if (dtsAccounts != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("accounts", metodoCRUD, dtsAccounts.Tables["dtKeys"], dtsAccounts.Tables["dtParams"], "", clientes);
                }
                //cuentasA3ERP(rbClientes.Checked);
                if (csGlobal.modoManual)
                {
                    logProceso.guardarLogProceso("Fin Proceso csAccounts funcion cargarCuentasA3ToRepasat");
                }

            }
            catch (InvalidOperationException ex) { }
        }

        //PRUEBA REFACTORIZACION
        public void actualizarCuentasA3ToRepasat(bool clientes = true, bool update = false, string accountsToUpdate = null, bool cronjob = false, string dateFrom = null)
        {
            try
            {
                string metodoCRUD = DeterminarMetodoCRUD(update);
                DataSet dtsAccounts = CargarDatosCuentas(clientes, update, accountsToUpdate, cronjob, dateFrom);

                if (dtsAccounts != null)
                {
                    SincronizarCuentas(dtsAccounts, metodoCRUD, clientes);
                }
                else
                {
                    RegistrarMensaje("No se encontraron nuevas cuentas para sincronizar.");
                }
            }
            catch (InvalidOperationException ex)
            {
                RegistrarError(ex, "Error al actualizar cuentas de a3ERP a Repasat");
            }
            catch (Exception ex)
            {
                RegistrarError(ex, "Error inesperado al actualizar cuentas de a3ERP a Repasat");
            }
        }

        private string DeterminarMetodoCRUD(bool update)
        {
            return update ? "PUT" : "POST";
        }

        private DataSet CargarDatosCuentas(bool clientes, bool update, string accountsToUpdate, bool cronjob, string dateFrom)
        {
            Repasat.csAccounts accountRPST = new Repasat.csAccounts();
            return accountRPST.cargarCuentasA3ToRepasat(clientes, update, accountsToUpdate, cronjob, dateFrom);
        }

        private void SincronizarCuentas(DataSet dtsAccounts, string metodoCRUD, bool clientes)
        {
            DataTable dtKeys = dtsAccounts.Tables["dtKeys"];
            DataTable dtParams = dtsAccounts.Tables["dtParams"];
            csRepasatWebService rpstWS = new csRepasatWebService();
            rpstWS.sincronizarObjetoRepasat("accounts", metodoCRUD, dtKeys, dtParams, "", clientes, true, null, true);
        }

        private void RegistrarError(Exception ex, string mensajePersonalizado)
        {
            string mensajeError = $"{mensajePersonalizado}: {ex.ToString()}";
            Utilidades.csLogErrores errorLog = new Utilidades.csLogErrores();
            errorLog.guardarErrorFichero(mensajeError);
        }

        private void RegistrarMensaje(string mensaje)
        {
            Utilidades.csLogErrores errorLog = new Utilidades.csLogErrores();
            errorLog.guardarErrorFichero(mensaje);
        }

        //SE COMENTA PARA MODIFICAR LA FUNCION A UNA NUEVA 
        //public void actualizarCuentasA3ToRepasat(bool clientes = true, bool update = false, string accountsToUpdate = null, bool cronjob = false, string dateFrom=null)
        //{
        //    try
        //    {
        //        string metodoCRUD = update ? "PUT" : "POST";
        //        Repasat.csAccounts accountRPST = new Repasat.csAccounts();
        //        DataSet dtsAccounts = accountRPST.cargarCuentasA3ToRepasat(clientes, update, accountsToUpdate,cronjob,dateFrom);

        //        if (dtsAccounts != null)
        //        {
        //            DataTable dtKeys = dtsAccounts.Tables["dtKeys"];
        //            DataTable dtParams = dtsAccounts.Tables["dtParams"];
        //            csRepasatWebService rpstWS = new csRepasatWebService();
        //            rpstWS.sincronizarObjetoRepasat("accounts", metodoCRUD, dtKeys, dtParams, "", clientes,true,null,true);
        //        }
        //        //cuentasA3ERP(rbClientes.Checked);

        //    }
        //    catch (InvalidOperationException ex) { }
        //}

        public void gestionarCarteraA3ToRepasat(DataTable dtA3ERP, DataTable dtRepasat, bool ventas=true, bool resetearEfectoRPST=false, bool updateEfectosPagados = false)
        {
            string metodoWebService = "";
            string repasatFunction = ""; // función adicional para anular un pago o cobro
            string objeto = ventas ? "incomingpayments" : "outgoingpayments";
            DataSet dtstCartera = new DataSet();
            Repasat.csIncomingPayments efectos = new csIncomingPayments();
            //1 - Obtengo los efectos que tengo que actualizar, pero no sé el id del efecto en Repasat, por lo que tengo que averiguarlo
            string idFraRepasat = "";       //Solo actualizaré aquellos documentos/facturas que están subidos a Repasat
            string idSerieRepasat = "";
            string idNumVencimiento = "";
            int idEfectoRepasat = 0;
            
            //El documento de Referencia en Repasat es el Id de la factura de A3
            string idDocumentoReferenciaA3 = "";
            string idDocumentoReferenciaRPST = "";
            string numVencimientoReferenciaA3 = "";
            string numVencimientoReferenciaRPST = "";

            //Variabler para verificar importes de efectos en A3 y repasat
            double importeEfectoRPST = 0;
            double importeEfectoA3 = 0;


            //Preparo el Datatable que utilizaré para actualizar
            DataTable dtEfectosToRPST = new DataTable();
            dtEfectosToRPST.Columns.Add("idCarteraCobros");
            dtEfectosToRPST.Columns.Add("estadoCarteraCobros");
            dtEfectosToRPST.Columns.Add("importeCobradoCarteraCobros");
            dtEfectosToRPST.Columns.Add("refSerie");
            dtEfectosToRPST.Columns.Add("codExternoCarteraCobros");
            dtEfectosToRPST.Columns.Add("fecCobroCarteraCobros");
            dtEfectosToRPST.Columns.Add("idDatosBancarios");

            //2- Tengo que actualizar en el Datatable el id del Efecto para pasárselo como key

            if (dtRepasat==null || dtA3ERP==null || dtRepasat.Rows.Count == 0 || dtA3ERP.Rows.Count == 0) return;

            foreach (DataRow filaA3 in dtA3ERP.Rows)
            {
                idDocumentoReferenciaA3 = filaA3["PROCEDEID"].ToString();
                numVencimientoReferenciaA3 = filaA3["NUMVEN"].ToString();
                importeEfectoA3=Convert.ToDouble(filaA3["IMPORTECOB"].ToString());

                foreach (DataRow filaRPST in dtRepasat.Rows)
                {
                    idDocumentoReferenciaRPST = filaRPST["IDA3 FRA"].ToString();
                    numVencimientoReferenciaRPST = filaRPST["NUM VTO"].ToString();
                    importeEfectoRPST = Convert.ToDouble(filaRPST["IMPORTE_EFECTO"].ToString());

                    if (updateEfectosPagados && importeEfectoRPST != importeEfectoA3)
                    {
                        continue;
                    }

                    if (idDocumentoReferenciaA3 == idDocumentoReferenciaRPST  && numVencimientoReferenciaA3==numVencimientoReferenciaRPST)
                    {
                        DataRow dr = dtEfectosToRPST.NewRow();
                        dr["idCarteraCobros"] = filaRPST["REPASAT_ID_DOC"].ToString();
                        dr["estadoCarteraCobros"] = resetearEfectoRPST ? "0" : "1";
                        dr["importeCobradoCarteraCobros"] = resetearEfectoRPST ? "" : filaA3["IMPORTECOB"].ToString().Replace(",",".");
                        dr["refSerie"] = resetearEfectoRPST ? "" : filaA3["REMESA"].ToString();
                        dr["codExternoCarteraCobros"] = resetearEfectoRPST ? "" : filaA3["IDCARTERA"].ToString();
                        dr["fecCobroCarteraCobros"] = resetearEfectoRPST ? "" : Convert.ToDateTime(filaA3["FECHAVALOR"].ToString()).ToString("yyyy-MM-dd");
                        dr["idDatosBancarios"] = resetearEfectoRPST ? "" : filaA3["RPST_ID_BANCO"].ToString();
                        dtEfectosToRPST.Rows.Add(dr);
                    }
                }
            }
            if (dtEfectosToRPST.Rows.Count == 0) return;

            dtstCartera= efectos.cargarEfectosA3ToRepasat(dtEfectosToRPST,resetearEfectoRPST);
            //Repasat.csAccounts accountRPST = new Repasat.csAccounts();
            //DataSet dtsAccounts = accountRPST.cargarCuentasA3ToRepasat(clientes, update, accountsToUpdate);

            csRepasatWebService rpstWS = new csRepasatWebService();
            metodoWebService = resetearEfectoRPST ? "GET" : "PUT";
            repasatFunction = resetearEfectoRPST ? "cancelPayment" : null;
            rpstWS.sincronizarObjetoRepasat(objeto, metodoWebService , dtstCartera.Tables["dtKeys"], dtstCartera.Tables["dtParams"], "", true, false, repasatFunction);


        }

        public string obtenerIdEfectoRepasat(string idSerie, string idNumDocumento, string numVencimiento)
        {
            string idEfecto = "";
            return idEfecto;

        }

        public void gestionarDireccionesA3ToRepasat(string codCuenta=null, bool cliente = true, bool update = false)
        {
            try
            {
                Repasat.csAddresses direccionesRPST = new Repasat.csAddresses();

                string updateDate = "";
                updateDate=update?getLastUpdateSyncDate("addresses", false):"";
                DataSet dtsDirecciones = direccionesRPST.cargarDireccionesA3ToRepasat(codCuenta, cliente, update,updateDate);
                if (dtsDirecciones != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    if (!update)
                    {
                        rpstWS.sincronizarObjetoRepasat("address", "POST", dtsDirecciones.Tables["dtKeys"], dtsDirecciones.Tables["dtParams"], "", cliente, false);
                    }
                    else
                    {
                        rpstWS.sincronizarObjetoRepasat("address", "PUT", dtsDirecciones.Tables["dtKeys"], dtsDirecciones.Tables["dtParams"], "", cliente,true);
                        actualizarFechaSync("address", dtsDirecciones.Tables["dtKeys"].Rows.Count, true);
                    }
                }
                //cuentasA3ERP(rbClientes.Checked);

            }
            catch (InvalidOperationException ex) { }

        }

        public void gestionarContactosA3ToRepasat(bool clientes = true, bool update = false)
        {
            try
            {
                Repasat.csContacts cuentasRPST = new Repasat.csContacts();
                DataSet dtsContactos = cuentasRPST.cargarContactosA3ToRepasat(clientes, update);
                if (dtsContactos != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    if (!update)
                    {
                        rpstWS.sincronizarObjetoRepasat("contacts", "POST", dtsContactos.Tables["dtKeys"], dtsContactos.Tables["dtParams"], "", clientes);
                    }
                    else
                    {
                        rpstWS.sincronizarObjetoRepasat("contacts", "PUT", dtsContactos.Tables["dtKeys"], dtsContactos.Tables["dtParams"], "", clientes);
                        actualizarFechaSync("contacts", dtsContactos.Tables["dtKeys"].Rows.Count, true);
                    }
                }
                //cuentasA3ERP(rbClientes.Checked);

            }
            catch (InvalidOperationException ex) { }

        }


        public void gestionarCuentasBancariasA3ToRepasat(bool clientes = true, bool update = false, string idCuenta = null)
        {
            try
            {
                Repasat.csBancs bancosRPST = new Repasat.csBancs();
                DataSet dtsContactos = bancosRPST.cargarBancosA3ToRepasat(clientes, update, idCuenta);
                if (dtsContactos != null)
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    if (!update)
                    {
                        rpstWS.sincronizarObjetoRepasat("banks", "POST", dtsContactos.Tables["dtKeys"], dtsContactos.Tables["dtParams"], "", clientes);
                    }
                    else
                    {
                        rpstWS.sincronizarObjetoRepasat("banks", "PUT", dtsContactos.Tables["dtKeys"], dtsContactos.Tables["dtParams"], "", clientes);
                        actualizarFechaSync("banks", dtsContactos.Tables["dtKeys"].Rows.Count, true);
                    }
                }
                //cuentasA3ERP(rbClientes.Checked);

            }
            catch (InvalidOperationException ex) { }
        }
        /// <summary>
        /// Proceso para revisar las cuentas bancarias de clientes y proveedores
        /// atención a si procede del alta de una cuenta de cliente o si directamente estamos dando de alta cuentas bancarias
        /// </summary>
        /// <param name="clientes"></param>
        /// <param name="consulta"></param>
        /// <param name="idAccount"></param>
        /// <param name="pendientes"></param>
        /// <param name="update"></param>
        /// <param name="checkDombanca"></param>
        /// <param name="codRPSTAccount"></param>
        /// <param name="altaCuenta"></param> Procede del procedimiento de alta de cliente
        /// <returns></returns>
        public DataTable gestionarCuentasBancariasRepasatToA3ERP(bool clientes, bool consulta = false, string idAccount = null, bool pendientes = false, bool update = false, bool checkDombanca = false, string codRPSTAccount = null, bool altaCuenta = false)
        {
            try
            {
                string filtroDomiciliacionesNoSyncro = "&filter[codExternoDatosBancarios]=";
                string filtroTipoCuenta = "&filter_has[customers]=1";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";
                string filtroCodigoClienteRPST = string.IsNullOrEmpty(codRPSTAccount) ? "" : "&filter[customers][codCli]=" + codRPSTAccount;
                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("contacts", true);
                    filtroFechaLatUpdate = "&filter_gte[fecModificacionContacto]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";
                }

                filtroDomiciliacionesNoSyncro = pendientes ? "&filter[codExternoContacto]=" : filtroDomiciliacionesNoSyncro = "";

                Objetos.csDomBanca[] dombanca = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();
                csa3erpTercero tercero = new csa3erpTercero();

                string tipoObjeto = "banks";
                string tipoCuenta = "";
                int totalLineas = 0;
                int contador = 0;
                bool checkAddresses = true;
                int numRegistro = 0;


                //creo un DataTable para almacenar las direcciones 
                DataTable dtDomiciliaciones = new DataTable();

                dtDomiciliaciones.Columns.Add("IDREPASAT");
                dtDomiciliaciones.Columns.Add("NOMBRE");
                dtDomiciliaciones.Columns.Add("TITULAR");
                dtDomiciliaciones.Columns.Add("IBAN");
                dtDomiciliaciones.Columns.Add("ENTIDAD");
                dtDomiciliaciones.Columns.Add("AGENCIA");
                dtDomiciliaciones.Columns.Add("DIGITOC");
                dtDomiciliaciones.Columns.Add("NUMCUENTA");
                dtDomiciliaciones.Columns.Add("BIC");
                dtDomiciliaciones.Columns.Add("IDCUENTAA3");
                dtDomiciliaciones.Columns.Add("FULL_IBAN");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newBanks = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    tipoCuenta = clientes ? "CLI" : "PRO";
                    tipoCuenta = "";

                    filtroTipoCuenta = filtroTipoCuenta + tipoCuenta;

                    string filtroURL = filtroDomiciliacionesNoSyncro + filtroTipoCuenta + filtroFechaLatUpdate+ filtroCodigoClienteRPST;

                    var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csBancs.RootObject>(json);

                    dombanca = new Objetos.csDomBanca[repasat.data.Count];

                    for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    {

                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            dombanca[contador] = new Objetos.csDomBanca();
                            dombanca[contador].nomTercero = repasat.data[ii].nomDatosBancarios.ToString().ToUpper();
                            dombanca[contador].nomBanco = string.IsNullOrWhiteSpace(repasat.data[ii].nomDatosBancarios) ? "" : repasat.data[ii].nomDatosBancarios.ToString().ToUpper();
                            dombanca[contador].iban = string.IsNullOrWhiteSpace(repasat.data[ii].ibanCuentaDatosBancarios) ? "" : repasat.data[ii].ibanCuentaDatosBancarios.ToString().ToUpper();
                            dombanca[contador].banco = string.IsNullOrWhiteSpace(repasat.data[ii].entidadCuentaDatosBancarios) ? "" : repasat.data[ii].entidadCuentaDatosBancarios.ToString();
                            dombanca[contador].agencia = string.IsNullOrWhiteSpace(repasat.data[ii].agenciaCuentaDatosBancarios.ToString()) ? "" : repasat.data[ii].agenciaCuentaDatosBancarios.ToString();
                            dombanca[contador].digitoControl = string.IsNullOrWhiteSpace(repasat.data[ii].dcCuentaDatosBancarios.ToString()) ? "" : repasat.data[ii].dcCuentaDatosBancarios.ToString();
                            dombanca[contador].numcuenta = string.IsNullOrWhiteSpace(repasat.data[ii].numCuentaDatosBancarios.ToString()) ? "" : repasat.data[ii].numCuentaDatosBancarios.ToString();
                            dombanca[contador].cuentaext = string.IsNullOrWhiteSpace(repasat.data[ii].numCuentaDatosBancarios.ToString()) ? "" : repasat.data[ii].numCuentaDatosBancarios.ToString();
                            //
                            dombanca[contador].titular = string.IsNullOrWhiteSpace(repasat.data[ii].titularCuentaDatosBancarios.ToString()) ? "" : repasat.data[ii].titularCuentaDatosBancarios.ToString();
                            //
                            dombanca[contador].cuentaext = string.IsNullOrWhiteSpace(repasat.data[ii].customers[0].codExternoCli) ? "" : repasat.data[ii].customers[0].codExternoCli.ToString();
                            dombanca[contador].bancoPrincipal = string.IsNullOrWhiteSpace(repasat.data[ii].customers[0].pivot.principalDatosBancarios.ToString()) ? "" : repasat.data[ii].customers[0].pivot.principalDatosBancarios.ToString();
                            dombanca[contador].codTercero = repasat.data[ii].customers[0].codExternoCli.ToString();
                            dombanca[contador].rpstIdBanco = repasat.data[ii].idDatosBancarios.ToString();
                            dombanca[contador].full_iban_account = dombanca[contador].iban + dombanca[contador].banco + dombanca[contador].agencia + dombanca[contador].digitoControl + dombanca[contador].numcuenta;
                            dombanca[contador].proveedor = clientes ? false : true;

                            //Guardo las direcciones en un Datatable si las voy a revisar
                            DataRow filaDomiciliacion = dtDomiciliaciones.NewRow();

                            filaDomiciliacion["IDREPASAT"] = string.IsNullOrEmpty(repasat.data[ii].idDatosBancarios.ToString()) ? "" : repasat.data[ii].idDatosBancarios.ToString();
                            filaDomiciliacion["NOMBRE"] = string.IsNullOrEmpty(repasat.data[ii].nomDatosBancarios) ? "" : repasat.data[ii].nomDatosBancarios.ToString();
                            filaDomiciliacion["TITULAR"] = string.IsNullOrEmpty(repasat.data[ii].titularCuentaDatosBancarios) ? "" : repasat.data[ii].titularCuentaDatosBancarios.ToString();
                            filaDomiciliacion["IBAN"] = string.IsNullOrEmpty(repasat.data[ii].ibanCuentaDatosBancarios) ? "" : repasat.data[ii].ibanCuentaDatosBancarios.ToString();
                            filaDomiciliacion["ENTIDAD"] = string.IsNullOrEmpty(repasat.data[ii].entidadCuentaDatosBancarios) ? "" : repasat.data[ii].entidadCuentaDatosBancarios.ToString();
                            filaDomiciliacion["AGENCIA"] = string.IsNullOrEmpty(repasat.data[ii].agenciaCuentaDatosBancarios) ? "" : repasat.data[ii].agenciaCuentaDatosBancarios.ToString();
                            filaDomiciliacion["DIGITOC"] = string.IsNullOrEmpty(repasat.data[ii].dcCuentaDatosBancarios) ? "" : repasat.data[ii].dcCuentaDatosBancarios.ToString();
                            filaDomiciliacion["NUMCUENTA"] = string.IsNullOrEmpty(repasat.data[ii].numCuentaDatosBancarios) ? "" : repasat.data[ii].numCuentaDatosBancarios.ToString();
                            filaDomiciliacion["BIC"] = string.IsNullOrEmpty(repasat.data[ii].bicCuentaDatosBancarios) ? "" : repasat.data[ii].bicCuentaDatosBancarios.ToString();
                            filaDomiciliacion["IDCUENTAA3"] = string.IsNullOrEmpty(repasat.data[ii].customers[0].codExternoCli) ? "" : repasat.data[ii].customers[0].codExternoCli.ToString();
                            filaDomiciliacion["FULL_IBAN"] = filaDomiciliacion["IBAN"].ToString() + filaDomiciliacion["ENTIDAD"] + filaDomiciliacion["ENTIDAD"] + filaDomiciliacion["DIGITOC"] + filaDomiciliacion["NUMCUENTA"];

                            dtDomiciliaciones.Rows.Add(filaDomiciliacion);

                            contador++;
                        }
                        json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csBancs.RootObject>(json);

                            Array.Resize(ref dombanca, dombanca.Length + repasat.data.Count);


                        }
                    }



                    if (!consulta && dombanca.Length > 0)
                    {
                        if (!altaCuenta) { a3.abrirEnlace(); }   //si viene del alta de cliente no tocamos la conexión

                        if (update && dombanca.Length > 0)
                        {
                            tercero.crearDomiciliacion(dombanca, clientes);
                        }
                        else if (!update && dombanca.Length > 0)
                        {
                            tercero.crearDomiciliacion(dombanca, clientes);
                        }
                        if (!altaCuenta) { a3.cerrarEnlace(); }     //si viene del alta de cliente no tocamos la conexión
                    }
                    if (checkDombanca)
                    {
                        //a3.abrirEnlace();
                        tercero.verificarDomiciliaciones(dombanca, false);
                        //a3.cerrarEnlace();

                    }

                    if (consulta)
                    { return dtDomiciliaciones; }
                    else return null;

                }
            }
            catch (System.Runtime.InteropServices.COMException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (JsonException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (Exception ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }

        }

        public DataTable gestionarContactosRepasatToA3(bool clientes, bool consulta = false, string idAccount = null, bool pendientes = false, bool update = false)
        {
            try
            {
                string filtroContactosNoSyncro = "&filter[codExternoContacto]=";
                //string filtroTipoCuenta = "&filter[tipoCli]=";
                string filtroTipoCuenta = "&filter_has[customers]=1";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";

                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("contacts", true);
                    filtroFechaLatUpdate = "&filter_gte[fecModificacionContacto]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";
                }

                filtroContactosNoSyncro = pendientes ? "&filter[codExternoContacto]=" : filtroContactosNoSyncro = "";

                Objetos.csContactos[] contacts = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();
                csa3erpTercero tercero = new csa3erpTercero();

                string tipoObjeto = "contacts";
                string tipoCuenta = "";
                int totalLineas = 0;
                int contador = 0;
                bool checkAddresses = true;
                int numRegistro = 0;


                //creo un DataTable para almacenar las direcciones 
                DataTable dtContacts = new DataTable();

                dtContacts.Columns.Add("IDREPASAT");
                dtContacts.Columns.Add("NOMBRE");
                dtContacts.Columns.Add("APELLIDOS");
                dtContacts.Columns.Add("TELF1");
                dtContacts.Columns.Add("TELF2");
                dtContacts.Columns.Add("EMAIL");
                dtContacts.Columns.Add("NOMBRECLIENTE");
                dtContacts.Columns.Add("IDCUENTAA3");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newContacts = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    tipoCuenta = clientes ? "CLI" : "PRO";
                    tipoCuenta = "";

                    filtroTipoCuenta = filtroTipoCuenta + tipoCuenta;

                    string filtroURL = filtroContactosNoSyncro + filtroTipoCuenta + filtroFechaLatUpdate;

                    var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csContacts.RootObject>(json);

                    contacts = new Objetos.csContactos[repasat.data.Count];

                    for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    {

                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            contacts[contador] = new Objetos.csContactos();
                            contacts[contador].nombre = repasat.data[ii].nombreCompleto.ToString().ToUpper();
                            contacts[contador].email = string.IsNullOrWhiteSpace(repasat.data[ii].emailContacto) ? "" : repasat.data[ii].emailContacto.ToString().ToUpper();
                            contacts[contador].telf1 = string.IsNullOrWhiteSpace(repasat.data[ii].telf1Contacto) ? "" : repasat.data[ii].telf1Contacto.ToString().ToUpper();
                            contacts[contador].telf2 = string.IsNullOrWhiteSpace(repasat.data[ii].telf2Contacto) ? "" : repasat.data[ii].telf2Contacto.ToString();
                            contacts[contador].idContactoRepasat = string.IsNullOrWhiteSpace(repasat.data[ii].idContacto.ToString()) ? "" : repasat.data[ii].idContacto.ToString();
                            contacts[contador].nombreCliente = string.IsNullOrEmpty(repasat.data[ii].customers[0].nomCli) ? "" : repasat.data[ii].customers[0].nomCli.ToString();
                            contacts[contador].razonSocialCuenta = string.IsNullOrEmpty(repasat.data[ii].customers[0].nomCli) ? "" : repasat.data[ii].customers[0].nomCli.ToString();


                            contacts[contador].codClienteERP = string.IsNullOrWhiteSpace(repasat.data[ii].customers[0].codExternoCli) ? "" : repasat.data[ii].customers[0].codExternoCli.ToString();
                            contacts[contador].idClienteRepasat = repasat.data[ii].customers[0].idCli.ToString();

                            //Guardo las direcciones en un Datatable si las voy a revisar
                            DataRow filaContact = dtContacts.NewRow();

                            filaContact["IDREPASAT"] = string.IsNullOrEmpty(repasat.data[ii].idContacto.ToString()) ? "": repasat.data[ii].idContacto.ToString();
                            filaContact["NOMBRE"] = string.IsNullOrEmpty(repasat.data[ii].nombreContacto) ? "" : repasat.data[ii].nombreContacto.ToString();
                            filaContact["APELLIDOS"] = string.IsNullOrEmpty(repasat.data[ii].apellidosContacto) ? "" : repasat.data[ii].apellidosContacto.ToString();
                            filaContact["TELF1"] = string.IsNullOrEmpty(repasat.data[ii].telf1Contacto) ? "" : repasat.data[ii].telf1Contacto.ToString();
                            filaContact["TELF2"] = string.IsNullOrEmpty(repasat.data[ii].telf2Contacto) ? "" : repasat.data[ii].telf2Contacto.ToString();
                            filaContact["EMAIL"] = string.IsNullOrEmpty(repasat.data[ii].emailContacto) ? "" : repasat.data[ii].emailContacto.ToString();
                            filaContact["NOMBRECLIENTE"] = string.IsNullOrEmpty(repasat.data[ii].customers[0].nomCli) ? "" : repasat.data[ii].customers[0].nomCli.ToString();
                            //filaAddress["IDCUENTAA3"] = repasat.data[ii]..codExternoCli;

                            dtContacts.Rows.Add(filaContact);

                            contador++;
                        }
                        json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csContacts.RootObject>(json);

                            Array.Resize(ref contacts, contacts.Length + repasat.data.Count);

                        }
                    }



                    if (!consulta && contacts.Length > 0)
                    {
                        a3.abrirEnlace();

                        if (update && contacts.Length > 0)
                        {
                            updateA3.actualizarContactos(contacts,true);
                        }
                        else if (!update && contacts.Length > 0)
                        {
                            updateA3.actualizarContactos(contacts);
                        }
                        a3.cerrarEnlace();
                    }

                    if (consulta)
                    { return dtContacts; }
                    else return null;

                }
            }
            catch (System.Runtime.InteropServices.COMException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (JsonException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (Exception ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }

        }

        public string whileJson(WebClient wc, string type, string page, string metodo, string tipoObjeto, string filtros, string id, string repasatFunction=null)
        {
            // No parará hasta que conecte

            string rt = null;
            bool ok = false;

            while (!ok)
            {
                try
                {
                    csRepasatWebService rpstWS = new csRepasatWebService();
                    string url = rpstWS.urlRepasatWS(tipoObjeto, metodo, filtros, id, page,repasatFunction);

                    //22-2-2023 añadida esta linea tras actualización del servidor de Repasat porque daba el siguiente error
                    //Anulada la solicitud: No se puede crear un canal seguro SSL/TLS
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    //11-06-2024 añadimos el user agent (nos comenta Dinahosting, que para que no pete el tema de seguridad, hay que añadir un user-agent a la petición web"
                    wc.Headers.Add("user-agent", "KLOSIONS ESYNC ");


                    rt = wc.DownloadString(url);
                    if (rt != null)
                    {
                        ok = true;
                    }
                }
                catch (Exception ex)
                {
                    if (csGlobal.modeDebug)
                    {
                        csUtilidades.log("NO SE ESTÁ CARGANDO EL WEBSERVICE, REVISAR APIKEY");
                    }
                    if (csGlobal.modoManual)
                    {
                        MessageBox.Show("NO SE ESTÁ CARGANDO EL WEBSERVICE, REVISAR APIKEY");
                    }
                    break;

                }
            }

            return rt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientes"></param>
        /// <param name="consulta"></param>
        /// <param name="idAccount"></param>
        /// <param name="checkDirecciones"></param>
        /// <param name="pendientes"></param>
        /// <param name="update"></param>
        /// <param name="sincronizados"></param>
        /// <param name="forceSync"></param>
        /// <param name="externalId"></param>
        /// <param name="validationProcess"></param>
        /// <returns></returns>
        public DataSet traspasarCuentasRepasatToA3(bool clientes, bool consulta = false, string idAccount = null, bool checkDirecciones = false, bool pendientes = false, bool update = false, bool sincronizados = false, bool forceSync = false, string externalId = null, bool validationProcess = false)
        {

            int errorNumPagina = 0;
            int errorRegPagina = 0;
            int errorRegCabecera = 0;
            int errorRegLinea = 0;
            string tipoCuenta = clientes ? "CLI" : "PRO";
            try
            {
                DataSet datasetCuentas = new DataSet();

                string filtroCuentasNoSyncro = "&filter[codExternoCli]=";
                string filtroEstadoSyncro = "";
                string filtroSync = "";
                string filtroTipoCuenta = "&filter[tipoCli]=";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";


                filtroSync = consulta ? "" : "&filter[sincronizarCuenta]=1";

                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("accounts", true);

                    filtroSync = consulta ? "" : "&filter[sincronizarCuenta]=1";

                    filtroFechaLatUpdate = "&filter_gte[fecModificacionCli]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";

                    filtroFechaLatUpdate = string.IsNullOrEmpty(idAccount) ? filtroFechaLatUpdate : "";
                }

                filtroEstadoSyncro = pendientes ? "&filter[codExternoCli]=" : filtroEstadoSyncro;
                filtroEstadoSyncro = sincronizados ? "&filter_ne[codExternoCli]=" : filtroEstadoSyncro;

                filtroCuentasNoSyncro = pendientes ? "&filter[codExternoCli]=" : filtroCuentasNoSyncro = "";


                if (forceSync)
                {
                    if (string.IsNullOrEmpty(idAccount) && string.IsNullOrEmpty(externalId))
                    {
                        filtroCuentasNoSyncro = "";
                        filtroEstadoSyncro = "&filter_ne[codExternoCli]=";
                    }
                    else
                    {
                        filtroCuentasNoSyncro = "";
                        filtroEstadoSyncro = string.IsNullOrEmpty(externalId) ? "" : "&filter[codExternoCli]=" + externalId;
                    }

                }

                Objetos.csTercero[] accounts = null;

                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();
                csa3erpTercero tercero = new csa3erpTercero();
                Objetos.csDireccion[] direcciones = null;
                Objetos.csContactos[] contactos = null;

                string tipoObjeto = "accounts";
                int totalLineas = 0;
                int totalContactos = 0;
                int contador = 0;
                int contadorContactos = 0;
                bool checkAddresses = true;
                int numRegistro = 0;

                //creo un DataTable para almacenar los Clientes creados
                DataTable dtAccounts = new DataTable();

                dtAccounts.Columns.Add("IDREPASAT");
                dtAccounts.Columns.Add("NOMBRE");
                dtAccounts.Columns.Add("RAZON");
                dtAccounts.Columns.Add("NIF");
                if (csGlobal.activarUnidadesNegocio) dtAccounts.Columns.Add("CONEXIONES");
                if (validationProcess) dtAccounts.Columns.Add("CODCLI RPST");

                DataColumn colInt32 = new DataColumn("CODIGOERP");
                colInt32.DataType = System.Type.GetType("System.Int32");
                dtAccounts.Columns.Add(colInt32);

                dtAccounts.Columns.Add("TIPOCUENTA");
                dtAccounts.Columns.Add("SYNC");
                dtAccounts.Columns.Add("CIAL");

                if (!clientes)
                {
                    dtAccounts.Columns.Add("CTA CONTABLE");
                    dtAccounts.Columns.Add("TIPO PROV");
                }

                //creo un DataTable para almacenar las direcciones 
                DataTable dtAddresses = new DataTable();

                if (checkDirecciones)
                {
                    dtAddresses.Columns.Add("IDREPASAT");
                    dtAddresses.Columns.Add("NOMBRE");
                    dtAddresses.Columns.Add("DIRECCION");
                    dtAddresses.Columns.Add("CP");
                    dtAddresses.Columns.Add("POBLACION");
                    dtAddresses.Columns.Add("EXTERNALID");
                    dtAddresses.Columns.Add("PRINCIPAL");
                    dtAddresses.Columns.Add("IDCUENTAA3");
                }



                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newCustomers = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    filtroTipoCuenta = filtroTipoCuenta + tipoCuenta;

                    string filtroURL = filtroSync + filtroCuentasNoSyncro + filtroTipoCuenta + filtroEstadoSyncro + filtroFechaLatUpdate;

                    if (!string.IsNullOrEmpty(idAccount))
                    {
                        if (forceSync && string.IsNullOrEmpty(externalId))
                        {
                            filtroURL = forceSync ? filtroURL + "&filter[idCli]=" + idAccount : "";
                        }
                        else if (!forceSync && string.IsNullOrEmpty(externalId))
                        {
                            filtroURL = "&filter[idCli]=" + idAccount;
                        }
                    }

                    //Para forzar actualización en A3ERP, añado este filtro
                    //en l afunción hay que pasar el codExterno de la cuenta
                    if (update && !string.IsNullOrEmpty(idAccount))
                    {
                        filtroURL = "&filter[codExternoCli]=" + idAccount + filtroTipoCuenta;
                    }

                    var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csAccounts.RootObject>(json);
                    accounts = new Objetos.csTercero[repasat.data.Count];

                    //Reviso el número de direcciones de las cuentas en el total de los nodos
                    //Si un documento tiene 1 linea y otro tiene 4 líneas "total_lineas=5"
                    for (int ii = 0; ii < repasat.data.Count; ii++)
                    {
                        totalLineas += repasat.data[ii].addresses.Count;
                    }

                    direcciones = new Objetos.csDireccion[totalLineas];

                    //Reviso el número de contactos

                    for (int c = 0; c < repasat.data.Count; c++)
                    {
                        totalContactos += repasat.data[c].contacts.Count;
                    }
                    contactos = new Objetos.csContactos[totalContactos];

                    //17/04/2018 modifico el last page y le quito el =
                    //for (int i = 2; i <= repasat.last_page + 1; i++) //Explicar porque i comienza en 2
                    for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    {


                        errorRegPagina = i;
                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {

                            errorRegCabecera = numRegistro;

                            //21-12-2019 añadimos la opción de que si forzamos la sincronización entramos en el bucle
                            if ((string.IsNullOrEmpty(repasat.data[ii].codExternoCli) && repasat.data[ii].tipoCli == tipoCuenta) || !string.IsNullOrEmpty(idAccount) || consulta || update || forceSync)
                            {
                                newCustomers = true;
                                accounts[numRegistro] = new Objetos.csTercero();
                                if (csGlobal.activarUnidadesNegocio)
                                {
                                    accounts[numRegistro].codIC = "";

                                    for (int i_con = 0; i_con < repasat.data[ii].conexion_externa.Count; i_con++)
                                    {
                                        if (repasat.data[ii].conexion_externa[i_con].idConExt.ToString() == csGlobal.id_conexion_externa)
                                        {
                                            accounts[numRegistro].codIC = repasat.data[ii].conexion_externa[0].idExterno.ToString();
                                        }
                                    }

                                }
                                else
                                {
                                    accounts[numRegistro].codIC = repasat.data[ii].codExternoCli;
                                }
                                accounts[numRegistro].nombre = repasat.data[ii].nomCli.ToUpper();
                                accounts[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasCli) ? "" : repasat.data[ii].aliasCli.ToUpper();
                                accounts[numRegistro].apellidos = string.Empty;
                                accounts[numRegistro].idRepasat = repasat.data[ii].idCli.ToString();
                                accounts[numRegistro].email = repasat.data[ii].emailCli;

                                //valido que la Razón Social esté informada, si no le pongo el nombre del cliente
                                if (repasat.data[ii].razonSocialCli != null)
                                {
                                    accounts[numRegistro].razonSocial = repasat.data[ii].razonSocialCli.ToUpper();
                                }
                                else
                                {
                                    accounts[numRegistro].razonSocial = repasat.data[ii].nomCli.ToUpper();
                                }
                                accounts[numRegistro].telf = repasat.data[ii].tel1;
                                accounts[numRegistro].telf2 = repasat.data[ii].tel2;
                                accounts[numRegistro].fax = repasat.data[ii].fax;
                                accounts[numRegistro].web = repasat.data[ii].webCli;
                                accounts[numRegistro].nifcif = repasat.data[ii].cifCli;
                                accounts[numRegistro].active_Status = repasat.data[ii].activoCli.ToString();
                                accounts[numRegistro].tipoCliente = repasat.data[ii].tipoCli;
                                accounts[numRegistro].id_customer_Ext = repasat.data[ii].codCli.ToString();
                                accounts[numRegistro].repasatID = repasat.data[ii].idCli.ToString();
                                accounts[numRegistro].diaPago1 = repasat.data[ii].diaPago1Cli.ToString();
                                accounts[numRegistro].diaPago2 = repasat.data[ii].diaPago2Cli.ToString();
                                accounts[numRegistro].diaPago3 = repasat.data[ii].diaPago3Cli.ToString();
                                accounts[numRegistro].repasatID = repasat.data[ii].idCli.ToString();
                                accounts[numRegistro].codRepasat = repasat.data[ii].codCli.ToString();
                                accounts[numRegistro].ruta = string.IsNullOrWhiteSpace(repasat.data[ii].idRuta.ToString()) ? "" : repasat.data[ii].route.codExternoRuta.ToString().ToString();
                                accounts[numRegistro].zona = string.IsNullOrWhiteSpace(repasat.data[ii].idZonaGeo.ToString()) ? "" : repasat.data[ii].geozone.codExternoZonaGeo.ToString().ToString();
                                accounts[numRegistro].cuentaContable = string.IsNullOrWhiteSpace(repasat.data[ii].idCuentaContable) ? "" : repasat.data[ii].accounting_account.numCuentaContable.ToString();

                                if (!string.IsNullOrEmpty(accounts[numRegistro].cuentaContable) && string.IsNullOrEmpty(accounts[numRegistro].codIC) && accounts[numRegistro].cuentaContable.Length > 5)
                                {
                                    string cuentaContable = accounts[numRegistro].cuentaContable;
                                    cuentaContable = cuentaContable.Substring(3, cuentaContable.Length - 3);
                                    cuentaContable = Convert.ToInt32(cuentaContable).ToString();
                                    accounts[numRegistro].codIC = cuentaContable;
                                }
                                accounts[numRegistro].active_Status = repasat.data[ii].activoCli.ToString();
                                accounts[numRegistro].cuentaGenerica = repasat.data[ii].clienteGenerico.ToString();
                                accounts[numRegistro].tipoProveedor = repasat.data[ii].tipoCliPro.ToString();

                                //Datos de Facturación
                                //customers[numRegistro].regimenIva = repasat.data[ii].regimenIVA;
                                accounts[numRegistro].codFormaPago = string.IsNullOrWhiteSpace(repasat.data[ii].idFormaPago.ToString()) ? "" : repasat.data[ii].payment_method.codExternoFormaPago.ToString();
                                accounts[numRegistro].codDocPago = string.IsNullOrWhiteSpace(repasat.data[ii].idDocuPago.ToString()) ? "" : repasat.data[ii].payment_document.codExternoDocuPago.ToString();
                                accounts[numRegistro].moneda = "EURO";
                                accounts[numRegistro].regimenIva = rpstUtil.obtenerRegIVA(repasat.data[ii].idRegimenImpuesto.ToString());



                                //añado el cliente a la fila del datatable
                                DataRow filaCustomer = dtAccounts.NewRow();

                                double numericValue;
                                bool isNumber = true;
                                filaCustomer["IDREPASAT"] = repasat.data[ii].idCli.ToString();
                                filaCustomer["NOMBRE"] = repasat.data[ii].nomCli;
                                filaCustomer["RAZON"] = string.IsNullOrWhiteSpace(repasat.data[ii].razonSocialCli) ? "" : repasat.data[ii].razonSocialCli.ToString();
                                filaCustomer["NIF"] = repasat.data[ii].cifCli;
                                if (validationProcess)
                                {
                                    filaCustomer["CODCLI RPST"] = repasat.data[ii].codCli;
                                }
                                if (csGlobal.activarUnidadesNegocio)
                                {
                                    filaCustomer["CONEXIONES"] = repasat.data[ii].conexion_externa.Count;
                                }

                                isNumber = double.TryParse(accounts[numRegistro].codIC, out numericValue);
                                filaCustomer["CODIGOERP"] = isNumber ? accounts[numRegistro].codIC : "99999";
                                filaCustomer["TIPOCUENTA"] = repasat.data[ii].tipoCli;
                                filaCustomer["SYNC"] = repasat.data[ii].sincronizarCuenta ? "SI" : "NO";
                                filaCustomer["CIAL"] = repasat.data[ii].idTrabajador == null ? "" : repasat.data[ii].responsible.codExternoTrabajador + " | " + repasat.data[ii].responsible.nomCompleto;

                                if (!clientes)
                                {
                                    filaCustomer["CTA CONTABLE"] = accounts[numRegistro].cuentaContable;
                                    filaCustomer["TIPO PROV"] = repasat.data[ii].tipoCliPro.ToString();
                                }



                                //Gestiono las direcciones del cliente
                                if (checkAddresses)
                                {
                                    for (int iii = 0; iii < repasat.data[ii].addresses.Count; iii++) // lineas
                                    {
                                        errorRegLinea = contador;

                                        direcciones[contador] = new Objetos.csDireccion();
                                        direcciones[contador].nombreDireccion = repasat.data[ii].addresses[iii].nomDireccion.ToString();
                                        direcciones[contador].direccion1 = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].direccion1Direccion) ? "" : repasat.data[ii].addresses[iii].direccion1Direccion.ToString();
                                        direcciones[contador].direccion2 = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].direccion2Direccion) ? "" : repasat.data[ii].addresses[iii].direccion2Direccion.ToString();
                                        direcciones[contador].poblacion = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].poblacionDireccion) ? "" : repasat.data[ii].addresses[iii].poblacionDireccion.ToString();
                                        direcciones[contador].codigoPostal = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].cpDireccion) ? "" : repasat.data[ii].addresses[iii].cpDireccion.ToString();

                                        if (string.IsNullOrEmpty(repasat.data[ii].addresses[iii].idPais))
                                        {
                                            repasat.data[ii].addresses[iii].idPais = "ESP";
                                        }
                                        if (repasat.data[ii].addresses[iii].idPais != null)
                                        {
                                            direcciones[contador].pais = rpstUtil.obtenerPais(repasat.data[ii].addresses[iii].idPais.ToString());
                                        }
                                        direcciones[contador].pais = repasat.data[ii].addresses[iii].idPais.ToString().Substring(0, 2);

                                        direcciones[contador].codProvincia = repasat.data[ii].addresses[iii].idProvincia != null ? rpstUtil.obtenerProvincia(repasat.data[ii].addresses[iii].idProvincia.ToString(), direcciones[contador].pais) : null;

                                        direcciones[contador].idClienteDireccionRepasat = repasat.data[ii].idCli.ToString();
                                        direcciones[contador].idDireccionRepasat = repasat.data[ii].addresses[iii].idDireccion.ToString();
                                        direcciones[contador].direccionfiscal = false;
                                        direcciones[contador].codExternoDireccion = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].codExternoDireccion) ? "" : repasat.data[ii].addresses[iii].codExternoDireccion.ToString();

                                        if (repasat.data[ii].addresses[iii].pivot.facturacionDireccion) //La dirección principal la informo en la cuenta
                                        {
                                            accounts[numRegistro].direccion = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].direccion1Direccion) ? "" : repasat.data[ii].addresses[iii].direccion1Direccion.ToString();
                                            accounts[numRegistro].poblacion = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].poblacionDireccion) ? "" : repasat.data[ii].addresses[iii].poblacionDireccion.ToString();
                                            accounts[numRegistro].codigoPostal = string.IsNullOrWhiteSpace(repasat.data[ii].addresses[iii].cpDireccion) ? "" : repasat.data[ii].addresses[iii].cpDireccion.ToString();
                                            accounts[numRegistro].codprovincia = rpstUtil.obtenerProvincia(repasat.data[ii].addresses[iii].idProvincia.ToString(), repasat.data[ii].addresses[iii].idPais.ToString().Substring(0, 2));
                                            accounts[numRegistro].codPais = rpstUtil.obtenerPais(repasat.data[ii].addresses[iii].idPais.ToString());
                                            direcciones[contador].direccionfiscal = true;

                                        }

                                        //Guardo las direcciones en un Datatable si las voy a revisar
                                        if (checkDirecciones)
                                        {
                                            DataRow filaAddress = dtAddresses.NewRow();

                                            filaAddress["IDREPASAT"] = repasat.data[ii].addresses[iii].idDireccion.ToString();
                                            filaAddress["NOMBRE"] = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].nomDireccion) ? "" : repasat.data[ii].addresses[iii].nomDireccion.ToString();
                                            filaAddress["DIRECCION"] = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].direccion1Direccion) ? "" : repasat.data[ii].addresses[iii].direccion1Direccion.ToString();
                                            filaAddress["CP"] = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].cpDireccion) ? "" : repasat.data[ii].addresses[iii].cpDireccion.ToString();
                                            filaAddress["POBLACION"] = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].poblacionDireccion) ? "" : repasat.data[ii].addresses[iii].poblacionDireccion.ToString();
                                            filaAddress["EXTERNALID"] = string.IsNullOrEmpty(repasat.data[ii].addresses[iii].codExternoDireccion) ? "" : repasat.data[ii].addresses[iii].codExternoDireccion.ToString();
                                            filaAddress["PRINCIPAL"] = direcciones[contador].direccionfiscal;
                                            filaAddress["IDCUENTAA3"] = string.IsNullOrEmpty(repasat.data[ii].codExternoCli) ? "" : repasat.data[ii].codExternoCli;

                                            dtAddresses.Rows.Add(filaAddress);
                                        }

                                        contador++;

                                    }
                                    //checkAddresses = false;
                                }
                                //Reviso Contactos


                                for (int numContacts = 0; numContacts < repasat.data[ii].contacts.Count; numContacts++) // lineas
                                {
                                    //errorRegLinea = contador;
                                    contactos[contadorContactos] = new Objetos.csContactos();
                                    contactos[contadorContactos].nombre = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].nombreContacto) ? "" : repasat.data[ii].contacts[numContacts].nombreContacto.ToString();
                                    contactos[contadorContactos].apellidos = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].apellidosContacto) ? "" : repasat.data[ii].contacts[numContacts].apellidosContacto.ToString();
                                    contactos[contadorContactos].telf1 = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].telf1Contacto) ? "" : repasat.data[ii].contacts[numContacts].telf1Contacto.ToString();
                                    contactos[contadorContactos].telf2 = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].telf2Contacto) ? "" : repasat.data[ii].contacts[numContacts].telf2Contacto.ToString();
                                    contactos[contadorContactos].email = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].emailContacto) ? "" : repasat.data[ii].contacts[numContacts].emailContacto.ToString();
                                    contactos[contadorContactos].codExternoContacto = string.IsNullOrEmpty(repasat.data[ii].contacts[numContacts].codExternoContacto) ? "" : repasat.data[ii].contacts[numContacts].codExternoContacto.ToString();
                                    contactos[contadorContactos].idClienteRepasat = repasat.data[ii].idCli.ToString();
                                    contactos[contadorContactos].idContactoRepasat = repasat.data[ii].contacts[numContacts].idContacto.ToString();
                                    contactos[contadorContactos].razonSocialCuenta = string.IsNullOrEmpty(repasat.data[ii].razonSocialCli) ? "" : repasat.data[ii].razonSocialCli.ToString();
                                    contactos[contadorContactos].codClienteERP = string.IsNullOrEmpty(repasat.data[ii].codExternoCli) ? "" : repasat.data[ii].codExternoCli.ToString();

                                    contadorContactos++;

                                }

                                dtAccounts.Rows.Add(filaCustomer);
                                numRegistro++;
                            }
                        }

                        json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csAccounts.RootObject>(json);

                            //Redimensiono Array de Direcciones
                            for (int ii = 0; ii < repasat.data.Count; ii++)
                            {
                                totalLineas += repasat.data[ii].addresses.Count;
                            }

                            //Redimensiono Array de Contactos
                            for (int c = 0; c < repasat.data.Count; c++)
                            {
                                totalContactos += repasat.data[c].contacts.Count;
                            }

                            Array.Resize(ref accounts, accounts.Length + repasat.data.Count);
                            Array.Resize(ref direcciones, totalLineas);
                            Array.Resize(ref contactos, totalContactos);


                        }
                    }
                }

                if (!consulta && newCustomers || forceSync)
                {
                    DialogResult Resultado;

                    if (!string.IsNullOrEmpty(idAccount))
                    {
                        Resultado = DialogResult.Yes;
                    }
                    else
                    {
                        Resultado = DialogResult.Yes;

                        //LLUIS
                        // Resultado = MessageBox.Show("¿Se van a crear " + accounts.Count() + (clientes ? " clientes ": " proveedores ") + " nuevos?", "Crear cuentas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    if (Resultado == DialogResult.Yes)
                    {
                        if (accounts.Length > 0)
                        {
                            a3.abrirEnlace();
                            if (update)
                            {
                                updateA3.actualizarCuentas(accounts);
                            }
                            else
                            {
                                tercero.crearCuentasA3Objects(accounts, direcciones, null, clientes, forceSync, contactos);
                            }

                            a3.cerrarEnlace();
                        }
                    }
                }

                if (consulta)
                {
                    datasetCuentas.Tables.Add(dtAccounts);
                    datasetCuentas.Tables.Add(dtAddresses);
                    return datasetCuentas;
                }
                else
                {
                    return null;
                }

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
            catch (JsonException ex)
            {
                string errorLog = "Numpagina: " + errorRegPagina + "\n!" +
                                    "Error Registro: " + errorRegCabecera.ToString() + "\n!";

                if (csGlobal.modoManual)
                {
                    MessageBox.Show(errorLog + ex.Message);
                }

                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError("Registro: " + errorRegCabecera.ToString() + "/n" + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        //public DataSet traspasarCuentasRepasatToA3(bool clientes, bool consulta = false, string idAccount = null, bool checkDirecciones = false, bool pendientes = false, bool update = false, bool sincronizados = false, bool forceSync = false, string externalId = null, bool validationProcess = false)
        //{
        //    try
        //    {
        //        DataSet datasetCuentas = new DataSet();
        //        DataTable dtAccounts = mapper.crearDataTableCuentas(validationProcess, clientes);
        //        DataTable dtAddresses = checkDirecciones ? mapper.crearDataTableDirecciones() : null;

        //        string filtroURL = csConstruirFiltros.construirFiltroURL(clientes, consulta, idAccount, checkDirecciones, pendientes, update, sincronizados, forceSync, externalId);

        //        var cuentasRepasat = obtenerCuentasRepasat(filtroURL);
        //        var accounts = mapper.mapearCuentas(cuentasRepasat, dtAccounts, dtAddresses, checkDirecciones, validationProcess, clientes, consulta, idAccount, update, forceSync);

        //        if (!consulta && (accounts.Length > 0 || forceSync))
        //        {
        //            actualizarCuentasEnA3ERP(accounts, dtAddresses, clientes, forceSync, consulta);
        //        }

        //        if (consulta)
        //        {
        //            datasetCuentas.Tables.Add(dtAccounts);
        //            if (dtAddresses != null)
        //            {
        //                datasetCuentas.Tables.Add(dtAddresses);
        //            }
        //        }

        //        return consulta ? datasetCuentas : null;
        //    }
        //    catch (Exception ex)
        //    {
        //        errorLog.registrarError(ex, "Error en función 'traspasarCuentasRepasatToA3'");
        //        return null;
        //    }
        //}

        private csAccounts.RootObject obtenerCuentasRepasat(string filtroURL)
        {
            using (var wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                var json = whileJson(wc, "accounts", "&page=1", "GET", "accounts", filtroURL, "");
                return JsonConvert.DeserializeObject<csAccounts.RootObject>(json);
            }
        }

        private void actualizarCuentasEnA3ERP(csTercero[] accounts, DataTable dtAddresses, bool clientes, bool forceSync, bool consulta)
        {
            var a3 = new csa3erp();
            var updateA3 = new A3ERP.updateA3ERP();
            var tercero = new csa3erpTercero();

            a3.abrirEnlace();

            if (consulta)
            {
                updateA3.actualizarCuentas(accounts);
            }
            else
            {
                csDireccion[] direcciones = null;
                if (dtAddresses != null)
                {
                    direcciones = mapper.mapearDireccionesDataTable(dtAddresses);
                }

                tercero.crearCuentasA3Objects(accounts, direcciones, null, clientes, forceSync, null);
            }

            a3.cerrarEnlace();
        }
        /////////////////////////////////////////////////////////////////






        public DataSet traspasarArticulosRepasatToA3(bool consulta = false, string idProduct = null, bool pendientes = false, bool update = false, bool sincronizados = false, bool forzeSync = false, string externalId = null, DataTable dtArticulos = null)
        {
            int errorRegPagina = 0;
            int errorRegCabecera = 0;
            int errorRegLinea = 0;
            string codart = "";
            string idartps = "";
            try
            {
                DataSet datasetArticulos = new DataSet();

                string filtroArticulosNoSyncro = "&filter[codExternoCli]=";
                string filtroEstadoSyncro = "";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";


                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("products", true);

                    filtroFechaLatUpdate = "&filter_gte[fecModificacionArt]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";

                    //filtroFechaLatUpdate = string.IsNullOrEmpty(idAccount) ? filtroFechaLatUpdate : "";
                }
                if (pendientes)
                {
                    filtroEstadoSyncro = "&filter[codExternoArticulo]=";
                }
                if (sincronizados)
                {
                    filtroEstadoSyncro = "&filter_ne[codExternoArticulo]=";
                }

                filtroArticulosNoSyncro = pendientes ? "&filter[codExternoArticulo]=" : filtroArticulosNoSyncro = "";


                /* if (forzeSync)
                 {
                     if (string.IsNullOrEmpty(idProduct) && string.IsNullOrEmpty(externalId))
                     {
                         filtroArticulosNoSyncro = "";
                         filtroEstadoSyncro = "&filter_ne[codExternoArticulo]=";
                     }
                     else
                     {
                         filtroArticulosNoSyncro = "";
                         filtroEstadoSyncro = "&filter[codExternoArticulo]=" + externalId;
                     }

                 }*/

                Objetos.csArticulo[] products = null;

                /* csa3erp a3 = new csa3erp();
                 A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();
                 csa3erpTercero tercero = new csa3erpTercero();
                 Objetos.csDireccion[] direcciones = null;

                 Objetos.csArticulo products = new Objetos.csArticulo();*/

                string tipoObjeto = "products";
                int totalLineas = 0;
                int contador = 0;
                int numRegistro = 0;

                //creo un DataTable para almacenar los Clientes creados
                DataTable dtProducts = new DataTable();

                dtProducts.Columns.Add("IDREPASAT");
                dtProducts.Columns.Add("NOMBRE");
                dtProducts.Columns.Add("CODIGOERP");

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    string filtroURL = filtroArticulosNoSyncro + filtroEstadoSyncro + filtroFechaLatUpdate;

                    if (!string.IsNullOrEmpty(idProduct))
                    {
                        if (forzeSync && string.IsNullOrEmpty(externalId))
                        {
                            filtroURL = forzeSync ? "" : "&filter[idProduct]=" + idProduct;
                        }
                    }

                    //Para forzar actualización en A3ERP, añado este filtro
                    //en l afunción hay que pasar el codExterno de la cuenta
                    /*if (update && !string.IsNullOrEmpty(idAccount))
                    {
                        filtroURL = "&filter[codExternoCli]=" + idAccount;
                    }*/

                    var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);
                    products = new Objetos.csArticulo[repasat.data.Count];
                    bool articuloParaA3ERP = false;


                    //17/04/2018 modifico el last page y le quito el =
                    //for (int i = 2; i <= repasat.last_page + 1; i++) //Explicar porque i comienza en 2
                    for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    {
                        errorRegPagina = i;
                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            errorRegCabecera = numRegistro;
                            articuloParaA3ERP = false;
                            codart = repasat.data[ii].refArticulo;
                            idartps = repasat.data[ii].idArticulo.ToString();

                            //21-12-2019 añadimos la opción de que si forzamos la sincronización entramos en el bucle
                            if ((string.IsNullOrEmpty(repasat.data[ii].codExternoArticulo) || !string.IsNullOrEmpty(idProduct) || consulta || update || forzeSync))
                            {
                                if (dtArticulos != null)
                                {
                                    if (dtArticulos.Rows.Count > 0 && forzeSync)
                                    {
                                        foreach (DataRow fila in dtArticulos.Rows)
                                        {
                                            if (fila["CODART"].ToString() == repasat.data[ii].refArticulo)
                                            {
                                                articuloParaA3ERP = true;
                                            }
                                        }
                                        if (!articuloParaA3ERP) continue;

                                    }
                                }


                                products[numRegistro] = new Objetos.csArticulo();
                                products[numRegistro].codart = repasat.data[ii].refArticulo;
                                products[numRegistro].descArt = repasat.data[ii].nomArticulo.ToUpper();
                                products[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasArticulo) ? "" : repasat.data[ii].aliasArticulo.ToUpper();
                                products[numRegistro].esVenta = repasat.data[ii].ventaArticulo != null ? "F" : (repasat.data[ii].ventaArticulo.ToString() == "1" ? "T" : "F");
                                products[numRegistro].esCompra = repasat.data[ii].compraArticulo != null ? "F" : (repasat.data[ii].compraArticulo.ToString() == "1" ? "T" : "F");
                                products[numRegistro].esStock = repasat.data[ii].stockArticulo != null ? "F" : (repasat.data[ii].stockArticulo.ToString() == "1" ? "T" : "F");
                                products[numRegistro].precioSinIVA = repasat.data[ii].precioVentaArticulo == null ? "0" : repasat.data[ii].precioVentaArticulo.ToString();
                                products[numRegistro].precioCompra = repasat.data[ii].precioCompraArticulo == null ? "0" : repasat.data[ii].precioCompraArticulo.ToString();
                                products[numRegistro].precioCoste = repasat.data[ii].preciocostecompraArticulo == null ? "0" : repasat.data[ii].preciocostecompraArticulo.ToString();
                                products[numRegistro].id_product_PS = repasat.data[ii].idArticulo.ToString();
                                products[numRegistro].bloqueado = repasat.data[ii].bloqueadoArticulo != null ? "F" : (repasat.data[ii].bloqueadoArticulo.ToString() == "1" ? "T" : "F");
                                //products[numRegistro].tipoIVAVenta = repasat.data[ii].idTipoImpuesto.ToString();
                                //products[numRegistro].tipoIVACompra = repasat.data[ii].idTipoImpuestoCompra.ToString();
                                products[numRegistro].refProveedor = repasat.data[ii].referenciaProveedorArticulo == null ? "" : repasat.data[ii].referenciaProveedorArticulo.ToString();

                                DataRow filaProduct = dtProducts.NewRow();
                                filaProduct["IDREPASAT"] = repasat.data[ii].idArticulo.ToString();
                                filaProduct["NOMBRE"] = repasat.data[ii].nomArticulo.ToUpper();
                                filaProduct["CODIGOERP"] = repasat.data[ii].refArticulo;

                                dtProducts.Rows.Add(filaProduct);
                            }

                            numRegistro++;
                        }

                        json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);

                            Array.Resize(ref products, products.Length + repasat.data.Count);


                        }
                    }
                }


                if (!consulta || forzeSync)
                {
                    csa3erp a3 = new klsync.csa3erp();
                    csa3erpItm a3erpitem = new csa3erpItm();
                    a3.abrirEnlace();

                    if (update)
                    {
                        a3erpitem.actualizarArticulos(products);
                    }
                    else
                    {
                        a3erpitem.crearArticuloEnA3ERP(products, true);
                    }
                    a3.cerrarEnlace();
                }

                if (consulta)
                {
                    datasetArticulos.Tables.Add(dtProducts);
                    return datasetArticulos;
                }
                else
                {
                    return null;
                }

            }
            catch (System.Runtime.InteropServices.COMException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (JsonException ex)
            {
                string errorLog = "Numpagina: " + errorRegPagina + "\n!" +
                                    "Error Registro: " + errorRegCabecera.ToString() + "\n!";

                if (csGlobal.modoManual)
                {
                    MessageBox.Show(errorLog + ex.Message);
                }

                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError("Registro: " + errorRegCabecera.ToString() + "/n" + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }


        public DataTable gestionarDireccionesRepasatToA3(bool clientes, bool consulta = false, string idAccount = null, bool pendientes = false, bool update = false)
        {
            try
            {

                bool checkDirecciones = false;
                string filtroCuentasNoSyncro = "&filter[codExternoDireccion]=";
                //string filtroTipoCuenta = "&filter[tipoCli]=";
                string filtroTipoCuenta = "&filter_has[customers]=1";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";

                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("addresses", true);
                    filtroFechaLatUpdate = "&filter_gte[fecModificacionDireccion]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";
                }

                filtroCuentasNoSyncro = pendientes ? "&filter[codExternoCli]=" : filtroCuentasNoSyncro = "";

                Objetos.csTercero[] accounts = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();
                csa3erpTercero tercero = new csa3erpTercero();
                Objetos.csDireccion[] direcciones = null;

                string tipoObjeto = "addresses";
                string tipoCuenta = "";
                int totalLineas = 0;
                int contador = 0;
                bool checkAddresses = true;
                int numRegistro = 0;


                //creo un DataTable para almacenar las direcciones 
                DataTable dtAddresses = new DataTable();

                dtAddresses.Columns.Add("IDREPASAT");
                dtAddresses.Columns.Add("NOMBRE");
                dtAddresses.Columns.Add("DIRECCION");
                dtAddresses.Columns.Add("CP");
                dtAddresses.Columns.Add("POBLACION");
                dtAddresses.Columns.Add("EXTERNALID");
                dtAddresses.Columns.Add("PRINCIPAL");
                dtAddresses.Columns.Add("IDCUENTAA3");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newCustomers = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    tipoCuenta = clientes ? "CLI" : "PRO";
                    tipoCuenta = "";

                    filtroTipoCuenta = filtroTipoCuenta + tipoCuenta;

                    string filtroURL = filtroCuentasNoSyncro + filtroTipoCuenta + filtroFechaLatUpdate;

                    var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csAddresses.RootObject>(json);

                    direcciones = new Objetos.csDireccion[repasat.data.Count];

                    for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    {

                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            direcciones[contador] = new Objetos.csDireccion();
                            direcciones[contador].nombreDireccion = repasat.data[ii].nomDireccion.ToString().ToUpper();
                            direcciones[contador].direccion1 = string.IsNullOrWhiteSpace(repasat.data[ii].direccion1Direccion) ? "" : repasat.data[ii].direccion1Direccion.ToString().ToUpper();
                            direcciones[contador].direccion2 = string.IsNullOrWhiteSpace(repasat.data[ii].direccion2Direccion) ? "" : repasat.data[ii].direccion2Direccion.ToString().ToUpper();
                            direcciones[contador].poblacion = string.IsNullOrWhiteSpace(repasat.data[ii].poblacionDireccion) ? "" : repasat.data[ii].poblacionDireccion.ToString();
                            direcciones[contador].codigoPostal = string.IsNullOrWhiteSpace(repasat.data[ii].cpDireccion) ? "" : repasat.data[ii].cpDireccion.ToString();
                            direcciones[contador].codProvincia = rpstUtil.obtenerProvincia(repasat.data[ii].idProvincia.ToString(), repasat.data[ii].idPais.ToString().Substring(0, 2));
                            if (string.IsNullOrEmpty(repasat.data[ii].idPais))
                            {
                                repasat.data[ii].idPais = "ESP";
                            }
                            if (repasat.data[ii].idPais != null)
                            {
                                direcciones[contador].pais = rpstUtil.obtenerPais(repasat.data[ii].idPais.ToString());
                            }
                            direcciones[contador].codPais = repasat.data[ii].idPais.ToString().Substring(0, 2);
                            //direcciones[contador].idClienteDireccionRepasat = repasat.data[ii].idCli.ToString();
                            direcciones[contador].idDireccionRepasat = repasat.data[ii].idDireccion.ToString();

                            //PENDIENTE DE SOLUCIONAR EN EL WEBSERVICE
                            direcciones[contador].direccionfiscal = repasat.data[ii].customers[0].pivot.principaldireccion? true:false;

                            //Guardo las direcciones en un Datatable si las voy a revisar
                            DataRow filaAddress = dtAddresses.NewRow();

                            filaAddress["IDREPASAT"] = repasat.data[ii].idDireccion.ToString();
                            filaAddress["NOMBRE"] = repasat.data[ii].nomDireccion.ToString();
                            filaAddress["DIRECCION"] = repasat.data[ii].direccion1Direccion.ToString();
                            filaAddress["CP"] = repasat.data[ii].cpDireccion.ToString();
                            filaAddress["POBLACION"] = repasat.data[ii].poblacionDireccion.ToString();
                            filaAddress["EXTERNALID"] = repasat.data[ii].codExternoDireccion.ToString();
                            filaAddress["PRINCIPAL"] = direcciones[contador].direccionfiscal;
                            //filaAddress["IDCUENTAA3"] = repasat.data[ii]..codExternoCli;

                            dtAddresses.Rows.Add(filaAddress);

                            contador++;
                        }
                        json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csAddresses.RootObject>(json);

                            Array.Resize(ref direcciones, direcciones.Length + repasat.data.Count);


                        }
                    }



                    if (!consulta && direcciones.Length>0)
                    {
                        a3.abrirEnlace();

                        if (update && direcciones.Length>0)
                        {
                            updateA3.actualizarDirecciones(direcciones);
                        }
                        else
                        {
                            //tercero.crearCuentasA3Objects(accounts, direcciones, null, clientes);
                        }
                        a3.cerrarEnlace();
                    }

                    if (consulta)
                    { return dtAddresses; }
                    else return null;

                }
            }
            catch (System.Runtime.InteropServices.COMException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (JsonException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (Exception ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }

    }

        public DataTable gestionarArticulosRepasatToA3(bool consulta = false, string idProduct = null, bool pendientes = false, bool update = false, bool sincronizado=false)
        {
            try
            {

                bool checkDirecciones = false;
                string filtroProductosNoSyncro = "&filter_ne[codExternoArticulo]=";
                string filtroProductosSyncro = " & filter[codExternoArticulo] = ";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";

                if (update)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = rpstUpdate.getLastUpdateSyncDate("products", true);
                    filtroFechaLatUpdate = "&filter_gte[fecModificacionArticulo]=" + fechaUltSync;

                    filtroFechaLatUpdate = update ? filtroFechaLatUpdate : "";
                }

                filtroProductosNoSyncro = pendientes ? "&filter[codExternoCli]=" : filtroProductosNoSyncro = "";

                Objetos.csArticulo[] articulos = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();

                string tipoObjeto = "products";
                int totalLineas = 0;
                int contador = 0;
                int numRegistro = 0;


                //creo un DataTable para almacenar las direcciones 
                DataTable dtAddresses = new DataTable();

                dtAddresses.Columns.Add("IDREPASAT");
                dtAddresses.Columns.Add("NOMBRE");
                dtAddresses.Columns.Add("DIRECCION");
                dtAddresses.Columns.Add("CP");
                dtAddresses.Columns.Add("POBLACION");
                dtAddresses.Columns.Add("EXTERNALID");
                dtAddresses.Columns.Add("PRINCIPAL");
                dtAddresses.Columns.Add("IDCUENTAA3");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newCustomers = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    //tipoCuenta = clientes ? "CLI" : "PRO";
                    //tipoCuenta = "";

                    //filtroTipoCuenta = filtroTipoCuenta + tipoCuenta;

                    //string filtroURL = filtroProductosNoSyncro + filtroTipoCuenta + filtroFechaLatUpdate;

                    //var json = whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    //var repasat = JsonConvert.DeserializeObject<Repasat.csAddresses.RootObject>(json);

                    //direcciones = new Objetos.csDireccion[repasat.data.Count];

                    //for (int i = 1; i <= repasat.last_page; i++) //Explicar porque i comienza en 2
                    //{

                    //    for (int ii = 0; ii < repasat.data.Count; ii++)
                    //    {
                    //        direcciones[contador] = new Objetos.csDireccion();
                    //        direcciones[contador].nombreDireccion = repasat.data[ii].nomDireccion.ToString().ToUpper();
                    //        direcciones[contador].direccion1 = string.IsNullOrWhiteSpace(repasat.data[ii].direccion1Direccion) ? "" : repasat.data[ii].direccion1Direccion.ToString().ToUpper();
                    //        direcciones[contador].direccion2 = string.IsNullOrWhiteSpace(repasat.data[ii].direccion2Direccion) ? "" : repasat.data[ii].direccion2Direccion.ToString().ToUpper();
                    //        direcciones[contador].poblacion = string.IsNullOrWhiteSpace(repasat.data[ii].poblacionDireccion) ? "" : repasat.data[ii].poblacionDireccion.ToString();
                    //        direcciones[contador].codigoPostal = string.IsNullOrWhiteSpace(repasat.data[ii].cpDireccion) ? "" : repasat.data[ii].cpDireccion.ToString();
                    //        direcciones[contador].codProvincia = obtenerProvincia(repasat.data[ii].idProvincia.ToString(), repasat.data[ii].idPais.ToString().Substring(0, 2));
                    //        if (string.IsNullOrEmpty(repasat.data[ii].idPais))
                    //        {
                    //            repasat.data[ii].idPais = "ESP";
                    //        }
                    //        if (repasat.data[ii].idPais != null)
                    //        {
                    //            direcciones[contador].pais = obtenerPais(repasat.data[ii].idPais.ToString());
                    //        }
                    //        direcciones[contador].codPais = repasat.data[ii].idPais.ToString().Substring(0, 2);
                    //        //direcciones[contador].idClienteDireccionRepasat = repasat.data[ii].idCli.ToString();
                    //        direcciones[contador].idDireccionRepasat = repasat.data[ii].idDireccion.ToString();

                    //        //PENDIENTE DE SOLUCIONAR EN EL WEBSERVICE
                    //        direcciones[contador].direccionfiscal = repasat.data[ii].customers[0].pivot.principaldireccion ? true : false;

                    //        //Guardo las direcciones en un Datatable si las voy a revisar
                    //        DataRow filaAddress = dtAddresses.NewRow();

                    //        filaAddress["IDREPASAT"] = repasat.data[ii].idDireccion.ToString();
                    //        filaAddress["NOMBRE"] = repasat.data[ii].nomDireccion.ToString();
                    //        filaAddress["DIRECCION"] = repasat.data[ii].direccion1Direccion.ToString();
                    //        filaAddress["CP"] = repasat.data[ii].cpDireccion.ToString();
                    //        filaAddress["POBLACION"] = repasat.data[ii].poblacionDireccion.ToString();
                    //        filaAddress["EXTERNALID"] = repasat.data[ii].codExternoDireccion.ToString();
                    //        filaAddress["PRINCIPAL"] = direcciones[contador].direccionfiscal;
                    //        //filaAddress["IDCUENTAA3"] = repasat.data[ii]..codExternoCli;

                    //        dtAddresses.Rows.Add(filaAddress);

                    //        contador++;
                    //    }
                    //    json = whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                    //    if (i + 1 <= repasat.last_page)
                    //    {
                    //        repasat = JsonConvert.DeserializeObject<Repasat.csAddresses.RootObject>(json);

                    //        Array.Resize(ref direcciones, direcciones.Length + repasat.data.Count);


                    //    }
                    //}



                    //if (!consulta && direcciones.Length > 0)
                    //{
                    //    a3.abrirEnlace();

                    //    if (update && direcciones.Length > 0)
                    //    {
                    //        updateA3.actualizarDirecciones(direcciones);
                    //    }
                    //    else
                    //    {
                    //        //tercero.crearCuentasA3Objects(accounts, direcciones, null, clientes);
                    //    }
                    //    a3.cerrarEnlace();
                    //}

                    if (consulta)
                    { return dtAddresses; }
                    else return null;

                }
            }
            catch (System.Runtime.InteropServices.COMException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (JsonException ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
            catch (Exception ex) { csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name); return null; }
        }





        //SE HAN MOVIDO LAS DOS FUNCIONES A klsync.Utilidades.csRepasatUtilities.cs, SE DEBE PODER BORRAR
        //public string obtenerProvincia(string provincia, string pais, string nomProvincia = null)
        //{
        //    try
        //    {
        //        DataView dv = new DataView(csGlobal.dtProvinciasA3);
        //        dv.RowFilter = "RPST_ID_PROV=" + provincia ;

        //  //      if (string.IsNullOrEmpty(nomProvincia)) { return string.Empty; }

        //        if (dv.Count > 0 || pais=="ES")
        //        { 
        //            string provinciaEdit = provincia;

        //            if (csGlobal.modeAp.ToUpper() == "REPASAT" && provincia.Length == 1 && pais == "ES")
        //            {
        //                return provinciaEdit = "0" + provincia;
        //            }
        //            return provinciaEdit;
        //        }
        //        else
        //        {
        //            csUtilidades.ejecutarConsulta("INSERT INTO PROVINCI (CODPROVI, NOMPROVI) VALUES ('" + provincia + "','" + provincia + "')",false);
        //            return provincia;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }

        //}

        //public string obtenerPais(string pais)
        //{

        //    string paisERP = "";

        //    if (csGlobal.modeAp.ToUpper() == "REPASAT")
        //    {
        //        paisERP = sql.obtenerCampoTabla("SELECT CODPAIS FROM PAISES WHERE CODISO3166A3='" + pais + "'");
        //    }
        //    if (string.IsNullOrEmpty(paisERP))
        //    {
        //        paisERP = "ES";
        //    }
          
        //    return paisERP;
        //}

        public void syncronizarRegistro(DataGridViewSelectedRowCollection dgv, string tabla, bool syncronizar = false)
        {

            string codregistro = "";
            string tableToUpdate = "";
            string fieldKey = "";
            string fieldKeyValue = "";
            
            sql.abrirConexion();

            for (int i = 0; i < dgv.Count; i++)
            {
                codregistro = dgv[i].Cells[1].Value.ToString().Trim();

                //por defecto el valor es 
                string valorSyncro = "0";
                valorSyncro = syncronizar ? "1" : "0";
                switch (tabla)
                {
                    case ("Clientes"):
                        fieldKey = "CODCLI";
                        tableToUpdate = "__CLIENTES";
                        codregistro = dgv[i].Cells[1].Value.ToString().Trim();
                        break;
                    case ("Proveedores"):
                        fieldKey = "CODPRO";
                        tableToUpdate = "__PROVEED";
                        codregistro = dgv[i].Cells[1].Value.ToString().Trim();
                        break;
                    case ("Productos"):
                        fieldKey = "CODART";
                        tableToUpdate = "ARTICULO";
                        codregistro = dgv[i].Cells["CODIGO"].Value.ToString().Trim();
                        break;

                }


                string query = " UPDATE " + tableToUpdate + " SET RPST_SINCRONIZAR=" + valorSyncro + " WHERE LTRIM(" + fieldKey + ")='" + codregistro + "'";
                sql.actualizarCampo(query,false);
            }

            sql.cerrarConexion();

        }

        public void auxiliaresA3ToRepasat()
        {
            DialogResult Resultado = DialogResult.Yes;
            if (csGlobal.modoManual)
            {
                Resultado = MessageBox.Show("¿Quiere traspasar los datos no sincronizados en A3ERP a Repasat de las siguientes tablas?\r\n\r\n Documentos de pago\r\n Metodos de pago\r\n Rutas\r\n Zonas\r\n Centros de Coste\r\n Marcas", "Traspasar Datos A3 -> RPST", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            else { Resultado = DialogResult.Yes; }

            if (Resultado == DialogResult.Yes)
            {
                try
                {
                    csDismay rpst = new csDismay();
                    //Faltan (Transportistas, Almacenes, Proyectos, Bancos y los de Impuestos/Retenciones)
                    rpst.cargarDocumentosPago();
                    rpst.cargarMetodosPago();
                    //cargarEmpleadosA3ToRepasat();
                    rpst.cargarRutasA3ToRepasat();
                    rpst.cargarZonasA3ToRepasat();
                    //cargarSeriesA3ToRepasat();
                    rpst.cargarCentrosDeCoste();

                    rpst.cargarMarcasA3ToRepasat();

                    if (csGlobal.modoManual)
                        MessageBox.Show("Proceso Finalizado:\n Los datos se han traspasados a Repasat correctamente ");
                }
                catch (Exception ex)
                {
                    if (csGlobal.modoManual) { MessageBox.Show(ex.Message); }
                    csUtilidades.log(ex.Message); 
                }
            }
        }


        public void actualizarStockA3ERPToRepasat(bool selectProductsMovsChecked, bool allProductsChecked, int numUpdownMovDays)
        {
            Utilidades.csLogErrores logProceso = new Utilidades.csLogErrores();
            try
            {
                //
                logProceso.guardarLogProceso("Iniciando la actualización de stock...");

                csSqlConnects sql = new csSqlConnects();
                DataTable dtStock = new DataTable();
                DataSet datasetRPST = new DataSet();
                string rowKey = "";
                string anexoQuery = string.Empty;
                string[] almacenes = null;
                string anexoAlmacenes = "";
                int totalRows = 0;

                // Definición del anexo de almacenes según el formato en csGlobal.almacenA3
                if (csGlobal.almacenA3.Contains(","))
                {
                    almacenes = csGlobal.almacenA3.Split(',');
                    anexoAlmacenes += " and (LTRIM(STOCKALM.CODALM) in (";
                    for (int i = 0; i < almacenes.Length; i++)
                    {
                        anexoAlmacenes += " '" + almacenes[i] + "'";
                        if (i < almacenes.Length - 1)
                        {
                            anexoAlmacenes += ", ";
                        }
                    }
                    anexoAlmacenes += ")) ";
                }
                else
                {
                    anexoAlmacenes = " and (LTRIM(STOCKALM.CODALM) in ('" + csGlobal.almacenA3 + "')) ";
                }

                if (!csGlobal.modoManual)
                {
                    anexoQuery = @"INNER JOIN (
                            SELECT CODART
                            FROM MOVSTOCKS
                            WHERE FECDOC > GETDATE()-5
                            GROUP BY CODART
                        ) AS MovimientosRecientes ON dbo.STOCKALM.CODART = MovimientosRecientes.CODART";
                }
                else if (csGlobal.modoManual && selectProductsMovsChecked)
                {
                    anexoQuery = @"INNER JOIN (
                            SELECT CODART
                            FROM MOVSTOCKS
                            WHERE FECDOC > GETDATE()-" + numUpdownMovDays + @"
                            GROUP BY CODART
                        ) AS MovimientosRecientes ON dbo.STOCKALM.CODART = MovimientosRecientes.CODART";
                }
                else
                {
                    anexoQuery = "";
                }

                string query = @"SELECT
                            dbo.STOCKALM.CODART,
                            dbo.ARTICULO.RPST_ID_PROD,
                            SUM(dbo.STOCKALM.UNIDADES) AS cantidadArticulo
                        FROM
                            dbo.STOCKALM
                        INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART
                        " + anexoQuery + @"
                        WHERE
                            dbo.ARTICULO.RPST_ID_PROD > 0
                            AND dbo.ARTICULO.BLOQUEADO = 'F' " + anexoAlmacenes + @"
                        GROUP BY
                            dbo.STOCKALM.CODART,
                            dbo.ARTICULO.RPST_ID_PROD";

                dtStock = sql.obtenerDatosSQLScript(query);

                if (csGlobal.modoManual && allProductsChecked)
                {
                    dtStock = sql.obtenerDatosSQLScript("select ARTICULO.CODART, dbo.ARTICULO.RPST_ID_PROD, SUM(ISNULL(STOCKALM.UNIDADES, 0)) AS cantidadArticulo " +
                        " from ARTICULO left outer join STOCKALM  on ARTICULO.codart = STOCKALM.CODART " + anexoAlmacenes +
                        " where (dbo.ARTICULO.RPST_ID_PROD > 0) AND dbo.ARTICULO.BLOQUEADO = 'F' GROUP BY ARTICULO.CODART, ARTICULO.RPST_ID_PROD; ");
                }

                if (dtStock != null)
                {
                    totalRows = dtStock.Rows.Count;
                    logProceso.guardarLogProceso($"Total de líneas obtenidas: {totalRows}");

                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow drFilaStock in dtStock.Rows)
                    {
                        rowKey = drFilaStock["RPST_ID_PROD"].ToString();
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        DataRow drParams = dtParams.NewRow();
                        drParams["FIELD"] = "cantidadArticulo";
                        drParams["KEY"] = rowKey;
                        drParams["VALUE"] = drFilaStock["cantidadArticulo"];
                        dtParams.Rows.Add(drParams);
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);

                    csRepasatWebService rpstWS = new csRepasatWebService();
                    rpstWS.sincronizarObjetoRepasat("products", "PUT", datasetRPST.Tables["dtKeys"], datasetRPST.Tables["dtParams"], "");
                }
                else
                {
                    logProceso.guardarLogProceso("No se obtuvieron datos de stock." + "\n Variable dtStock: " + dtStock.Rows.Count);
                }
            }
            catch (Exception ex)
            {
                logProceso.guardarLogProceso($"Error en la actualización de stock: {ex.ToString()}");
            }
        }


    }
}
