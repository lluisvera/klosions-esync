﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.Repasat
{
    class csContacts
    {

        public class Pivot
        {
            public int? idContacto { get; set; }
            public int? idCli { get; set; }
            public int? idEntidadContactoCli { get; set; }
        }

        public class Customer
        {
            public int? idCli { get; set; }
            public int? codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int? idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int? activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public double porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public string cuentaContableProveedorCli { get; set; }
            public string idRegimenImpuesto { get; set; }
            public string idTipoRetencion { get; set; }
            public int? idEmpresaAccesoExterno { get; set; }
            public int? idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public string representadoCli { get; set; }
            public string comisionCli { get; set; }
            public string descuentoCli { get; set; }
            public int? idFormatoImpresionPresupuesto { get; set; }
            public int? idFormatoImpresionPedidoVenta { get; set; }
            public int? idFormatoImpresionPedidoCompra { get; set; }
            public int? idFormatoImpresionAlbaranVenta { get; set; }
            public int? idFormatoImpresionAlbaranCompra { get; set; }
            public int? idFormatoImpresionFacturaVenta { get; set; }
            public int? idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int? idRuta { get; set; }
            public string fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public int? idTarifaDescuentos { get; set; }
            public Pivot pivot { get; set; }
        }

        public class Datum
        {
            public int? idContacto { get; set; }
            public string nombreContacto { get; set; }
            public string apellidosContacto { get; set; }
            public string telf1Contacto { get; set; }
            public string extensionTelf1Contacto { get; set; }
            public string telf2Contacto { get; set; }
            public string extensionTelf2Contacto { get; set; }
            public string emailContacto { get; set; }
            public string email2Contacto { get; set; }
            public string cargoContacto { get; set; }
            public int? idUsuario { get; set; }
            public string fecNacContacto { get; set; }
            public string parejaContacto { get; set; }
            public string facebookContacto { get; set; }
            public string twitterContacto { get; set; }
            public string linkedinContacto { get; set; }
            public string skypeContacto { get; set; }
            public string comentsContacto { get; set; }
            public string fecAltaContacto { get; set; }
            public int? facturacionContacto { get; set; }
            public string codExternoContacto { get; set; }
            public string fecModificacionContacto { get; set; }
            public int? idUsuarioLogin { get; set; }
            public string nombreCompleto { get; set; }
            public List<Customer> customers { get; set; }
            public List<string> leads { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int? total { get; set; }
            public int? per_page { get; set; }
            public int? current_page { get; set; }
            public int? last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }


        public DataSet cargarContactosA3ToRepasat(bool clientes, bool update = false)
        {
            try
            {
                string rowKey = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();
                if (clientes)
                {
                    dt = sqlConnect.obtenerDatosSQLScript(sqlScript.selectContactosFromA3ToRepasat(update));
                }
                else
                {
                    dt = sqlConnect.obtenerDatosSQLScript(sqlScript.selectContactosFromA3ToRepasat());
                }
                Objetos.csTercero cliente = new Objetos.csTercero();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (update)
                        {
                            rowKey = dr["RPST_ID_CLI"].ToString();
                        }
                        else
                        {
                            rowKey = dr["contact[codExternoContacto]"].ToString();
                        }
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {

                            //if (dtcol.ColumnName == "idRuta")
                            //{
                            //    string stop = "";
                            //}
                            if (string.IsNullOrEmpty(dr[dtcol.ColumnName].ToString()))
                            {
                                continue;
                            }
                            else
                            {
                                if (dtcol.ColumnName == "CODIGO" || dtcol.ColumnName == "FORPAG")
                                {
                                    continue;
                                }
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcol.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = dr[dtcol.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);
                    //csRepasatWebService rpstWS = new csRepasatWebService();
                    //rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "");

                    return datasetRPST;
                }
                else
                {
                    return null;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }


        
    }
}
