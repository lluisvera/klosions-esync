﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace klsync.Repasat.LoadObjects
{
    class csDtObjects
    {
        Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
        bool check = false;
        public DataTable documentosPagoRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoDocuPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtPaymentDocs = new DataTable();
            dtPaymentDocs.TableName = "PaymentDocs";
            dtPaymentDocs.Columns.Add("IDREPASAT");
            dtPaymentDocs.Columns.Add("DESCRIPCION");
            dtPaymentDocs.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csDocumentosPago[] documentoPago = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "paymentdocuments";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                    documentoPago = new Objetos.csDocumentosPago[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtPaymentDocs.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            documentoPago[ii] = new Objetos.csDocumentosPago();

                            documentoPago[ii].idDocumentoPago = repasat.data[ii].idDocuPago.ToString();
                            documentoPago[ii].codExternoDocuPago = repasat.data[ii].codExternoDocuPago;
                            documentoPago[ii].nomDocumentoPago = repasat.data[ii].nomDocuPago;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idDocuPago.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomDocuPago;
                            rowMethod["CODIGOERP"] = repasat.data[ii].codExternoDocuPago;

                            dtPaymentDocs.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < documentoPago.Length; iiii++)
                        {
                            if (documentoPago[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                        contador = 0;
                    }
                  
                    
                        return dtPaymentDocs;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }
        }

        public DataTable metodosPagoRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoFormaPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtPaymentMethods = new DataTable();
            dtPaymentMethods.TableName = "PaymentMethods";
            dtPaymentMethods.Columns.Add("IDREPASAT");
            dtPaymentMethods.Columns.Add("DESCRIPCION");
            dtPaymentMethods.Columns.Add("CODIGOERP");

            //if (csGlobal.sync_adv_mode) {
            //    dtPaymentMethods.Columns.Add("CODIGOERP_ADV");
            //}

            csSqlConnects sql = new csSqlConnects();
            Objetos.csMetodoPago[] metodoPago = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "paymentmethods";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentMethods.RootObject>(json);
                    metodoPago = new Objetos.csMetodoPago[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtPaymentMethods.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            metodoPago[ii] = new Objetos.csMetodoPago();

                            metodoPago[ii].idFormaPagoRepasat = repasat.data[ii].idFormaPago.ToString();
                            metodoPago[ii].codExternoFormaPago = repasat.data[ii].codExternoFormaPago;
                            metodoPago[ii].nomFormaPago = repasat.data[ii].nomFormaPago;

                           


                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idFormaPago.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomFormaPago;
                            rowMethod["CODIGOERP"] = loadColumnRPSTCodeA3ERP(repasat, ii);
                            //Añado columna para external 
                          
                            dtPaymentMethods.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < metodoPago.Length; iiii++)
                        {
                            if (metodoPago[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csPaymentMethods.RootObject>(json);
                        contador = 0;
                    }
                   return dtPaymentMethods;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }

        }

        public DataTable transportistas()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoFormaPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtCarriers = new DataTable();
            dtCarriers.TableName = "carriers";
            dtCarriers.Columns.Add("IDREPASAT");
            dtCarriers.Columns.Add("DESCRIPCION");
            dtCarriers.Columns.Add("CODIGOERP");

            //if (csGlobal.sync_adv_mode) {
            //    dtPaymentMethods.Columns.Add("CODIGOERP_ADV");
            //}

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTransportistas[] transportista = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "carriers";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                    transportista = new Objetos.csTransportistas[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtCarriers.NewRow();

                            transportista[ii] = new Objetos.csTransportistas();

                            transportista[ii].idTransportista = repasat.data[ii].idTransportista.ToString();
                            transportista[ii].codExternoTransportista = repasat.data[ii].codExternoTransportista;
                            transportista[ii].nomTransportista = repasat.data[ii].nomTransportista;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idTransportista.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomTransportista;
                            rowMethod["CODIGOERP"] = repasat.data[ii].codExternoTransportista;
                            //Añado columna para external 

                            dtCarriers.Rows.Add(rowMethod);
                        }

                        check = false;
                        for (int iiii = 0; iiii < transportista.Length; iiii++)
                        {
                            if (transportista[iiii] != null)
                                check = true;
                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                        contador = 0;
                    }
                    return dtCarriers;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }

        }

        public string loadColumnRPSTCodeA3ERP(Repasat.csPaymentMethods.RootObject repasat,int ii) {
            try
            {
                string codeA3ERP="";

                if (csGlobal.sync_adv_mode && repasat.data[ii].auxiliar_externo.Count > 0)
                {
                    for (int cont = 0; cont < repasat.data[ii].auxiliar_externo.Count; cont++)
                    {
                        if (repasat.data[ii].auxiliar_externo[cont].idConExt.ToString() == csGlobal.id_conexion_externa)
                        {
                            codeA3ERP = repasat.data[ii].auxiliar_externo[cont].pivot.idExterno;
                        }
                    }
                }
                else if (csGlobal.sync_adv_mode && repasat.data[ii].auxiliar_externo.Count == 0)
                {
                    codeA3ERP = null;
                }
                else
                {
                    codeA3ERP = repasat.data[ii].codExternoFormaPago;
                }

                return codeA3ERP;
            }
            catch (Exception ex) {
                return null;
            }
        }

        public DataTable empleadosRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoTrabajador]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtEmployees = new DataTable();

            dtEmployees.TableName = "dtEmployees";

            dtEmployees.Columns.Add("IDREPASAT");
            dtEmployees.Columns.Add("DESCRIPCION");
            dtEmployees.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csRepresentante[] representante = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "employees";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csEmployees.RootObject>(json);
                    representante = new Objetos.csRepresentante[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtEmployees.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            representante[ii] = new Objetos.csRepresentante();

                            representante[ii].idRepresentante = repasat.data[ii].idTrabajador.ToString();
                            representante[ii].codExternoTrabajador = repasat.data[ii].codExternoTrabajador;
                            representante[ii].nombreRepresentante = repasat.data[ii].nombreTrabajador + " " + repasat.data[ii].apellidosTrabajador;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idTrabajador.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nombreTrabajador + " " + repasat.data[ii].apellidosTrabajador; ;
                            rowMethod["CODIGOERP"] = repasat.data[ii].codExternoTrabajador;

                            dtEmployees.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < representante.Length; iiii++)
                        {
                            if (representante[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csEmployees.RootObject>(json);
                        contador = 0;
                    }
                   return dtEmployees;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable Rutas()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoRuta]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtRoute = new DataTable();
            dtRoute.TableName = "dtRoutes";
            dtRoute.Columns.Add("IDREPASAT");
            dtRoute.Columns.Add("DESCRIPCION");
            dtRoute.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csRuta[] ruta = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "routes";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csRoutes.RootObject>(json);
                    ruta = new Objetos.csRuta[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtRoute.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            ruta[ii] = new Objetos.csRuta();

                            ruta[ii].idRuta = repasat.data[ii].idRuta.ToString();
                            ruta[ii].codExternoRuta = repasat.data[ii].codExternoRuta;
                            ruta[ii].nomRuta = repasat.data[ii].nomRuta;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idRuta.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomRuta;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoRuta;

                            dtRoute.Rows.Add(rowCarrier);
                        }

                        check = false;
                        for (int iiii = 0; iiii < ruta.Length; iiii++)
                        {
                            if (ruta[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csRoutes.RootObject>(json);
                    }
                    return dtRoute;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable zonas()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoZona]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtZona = new DataTable();
            dtZona.TableName = "dtZones";
            dtZona.Columns.Add("IDREPASAT");
            dtZona.Columns.Add("DESCRIPCION");
            dtZona.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csZonaGeografica[] zona = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "geozones";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeo.RootObject>(json);
                    zona = new Objetos.csZonaGeografica[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtZona.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            zona[ii] = new Objetos.csZonaGeografica();

                            zona[ii].idZona = repasat.data[ii].idZonaGeo.ToString();
                            zona[ii].codExternoZona = repasat.data[ii].codExternoZonaGeo.ToString();
                            zona[ii].nomZona = repasat.data[ii].nomZonaGeo;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idZonaGeo.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomZonaGeo;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoZonaGeo;

                            dtZona.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < zona.Length; iiii++)
                        {
                            if (zona[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeo.RootObject>(json);
                    }
                    return dtZona;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }

        }

        public DataTable Series()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoSerie]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtSerie = new DataTable();
            dtSerie.TableName = "dtSeries";
            dtSerie.Columns.Add("IDREPASAT");
            dtSerie.Columns.Add("DESCRIPCION");
            dtSerie.Columns.Add("CODIGOERP");
            dtSerie.Columns.Add("SERIERECT");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csSeries[] serieDoc = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "seriesdocs";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
                    serieDoc = new Objetos.csSeries[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowSerie = dtSerie.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            serieDoc[ii] = new Objetos.csSeries();

                            serieDoc[ii].idSerie = repasat.data[ii].idSerie.ToString();
                            serieDoc[ii].codExternoSerie = repasat.data[ii].codExternoSerie;
                            serieDoc[ii].nomSerie = repasat.data[ii].nomSerie;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowSerie["IDREPASAT"] = repasat.data[ii].idSerie.ToString();
                            rowSerie["DESCRIPCION"] = repasat.data[ii].nomSerie;
                            rowSerie["CODIGOERP"] = repasat.data[ii].codExternoSerie;
                            rowSerie["SERIERECT"] = repasat.data[ii].refSerie;

                            dtSerie.Rows.Add(rowSerie);
                        }



                        check = false;
                        for (int iiii = 0; iiii < serieDoc.Length; iiii++)
                        {
                            if (serieDoc[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csSeries.RootObject>(json);
                    }
                   return dtSerie;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable Almacenes()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoAlmacen]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtWarehouses = new DataTable();
            dtWarehouses.TableName = "dtWarehouses";
            dtWarehouses.Columns.Add("IDREPASAT");
            dtWarehouses.Columns.Add("DESCRIPCION");
            dtWarehouses.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csWarehouses[] almacen = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "warehouses";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    almacen = new Objetos.csWarehouses[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtWarehouses.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            almacen[ii] = new Objetos.csWarehouses();

                            almacen[ii].idAlmacen = repasat.data[ii].idAlmacen.ToString();
                            almacen[ii].codExternoAlmacen = repasat.data[ii].codExternoAlmacen;
                            almacen[ii].nomAlmacen = repasat.data[ii].nomAlmacen;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idAlmacen.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomAlmacen;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoAlmacen;

                            dtWarehouses.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < almacen.Length; iiii++)
                        {
                            if (almacen[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    }
                    return dtWarehouses;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable FamiliaArt(Boolean subfamilia = false)
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoFamilia]=";  //filtro para cargar sólo los documentos no descargados en A3ERP  
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;


            //3-1-24 Modificado por Iker quitado campo filtroExtDocNull porque no funciona
            //string filtroURL = subfamilia ? "&filter_ne[idPadreFam]=" + filtroExtDocNull : "&filter[idPadreFam]=" + filtroExtDocNull; 
            string filtroURL = subfamilia ? "&filter_ne[idPadreFam]=" : "&filter[idPadreFam]="; //Filtrplas subfamilias o familias


            DataTable dtFamiliaArt = new DataTable();
            dtFamiliaArt.TableName = "dtFamiliaArt";
            dtFamiliaArt.Columns.Add("IDREPASAT");
            dtFamiliaArt.Columns.Add("DESCRIPCION");
            dtFamiliaArt.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csFamiliaArt[] familiaArt = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "families";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csFamiliaArt.RootObject>(json);
                    familiaArt = new Objetos.csFamiliaArt[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtFamiliaArt.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            familiaArt[ii] = new Objetos.csFamiliaArt();

                            familiaArt[ii].idFam = repasat.data[ii].idFam.ToString();
                            familiaArt[ii].codExternoFamilia = repasat.data[ii].codExternoFamilia;
                            familiaArt[ii].nomFam = repasat.data[ii].nomFam;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idFam.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomFam;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoFamilia;

                            dtFamiliaArt.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < familiaArt.Length; iiii++)
                        {
                            if (familiaArt[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csFamiliaArt.RootObject>(json);
                    }
                    return dtFamiliaArt;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable MarcaArt()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoFamilia]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;

            //4-1-24 Modificado por Iker quitado campo filtroExtDocNull porque no funciona
            //string filtroURL = filtroExtDocNull;
            string filtroURL = "";

            DataTable dtMarcaArt = new DataTable();
            dtMarcaArt.TableName = "dtMarcaArt";
            dtMarcaArt.Columns.Add("IDREPASAT");
            dtMarcaArt.Columns.Add("DESCRIPCION");
            dtMarcaArt.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csMarcaArt[] familiaArt = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "brands";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csMarcasArt.RootObject>(json);
                    familiaArt = new Objetos.csMarcaArt[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtMarcaArt.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            familiaArt[ii] = new Objetos.csMarcaArt();

                            familiaArt[ii].uuidMarca = repasat.data[ii].uuidMarca.ToString();
                            familiaArt[ii].codExternoMarca = repasat.data[ii].codExternoMarca;
                            familiaArt[ii].nomMarca = repasat.data[ii].nomMarca;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].uuidMarca.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomMarca;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoMarca;

                            dtMarcaArt.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < familiaArt.Length; iiii++)
                        {
                            if (familiaArt[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csMarcasArt.RootObject>(json);
                    }
                    return dtMarcaArt;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable TipoArt()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoTipoArticulo]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;

            //4-1-24 Modificado por Iker quitado campo filtroExtDocNull porque no funciona
            //string filtroURL = filtroExtDocNull;
            string filtroURL = "";

            DataTable dtTipoArt = new DataTable();
            dtTipoArt.TableName = "dtTipoArt";
            dtTipoArt.Columns.Add("IDREPASAT");
            dtTipoArt.Columns.Add("DESCRIPCION");
            dtTipoArt.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTipoArt[] tipoArt = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "producttypes";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csTipoArt.RootObject>(json);
                    tipoArt = new Objetos.csTipoArt[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtTipoArt.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            tipoArt[ii] = new Objetos.csTipoArt();

                            tipoArt[ii].idTipoArticulo = repasat.data[ii].idTipoArticulo;
                            tipoArt[ii].codExternoTipoArticulo = repasat.data[ii].codExternoTipoArticulo;
                            tipoArt[ii].nomTipoArticulo = repasat.data[ii].nomTipoArticulo;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idTipoArticulo.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomTipoArticulo;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoTipoArticulo;

                            dtTipoArt.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < tipoArt.Length; iiii++)
                        {
                            if (tipoArt[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csTipoArt.RootObject>(json);
                    }
                    return dtTipoArt;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }


        }

        public DataTable cuentasContablesRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExtCuentaContable]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            filtroExtDocNull = ""; //Cargo todas las cuentas
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtAccount = new DataTable();
            dtAccount.TableName = "dtAccounts";
            dtAccount.Columns.Add("IDREPASAT");
            dtAccount.Columns.Add("DESCRIPCION");
            dtAccount.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csCuentasContables[] cuenta = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "accountingaccounts";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csAccountingAccounts.RootObject>(json);
                    cuenta = new Objetos.csCuentasContables[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowAccount = dtAccount.NewRow();

                            cuenta[ii] = new Objetos.csCuentasContables();

                            cuenta[ii].idCuenta = repasat.data[ii].idCuentaContable.ToString();
                            cuenta[ii].codExternoCuentaContable = repasat.data[ii].codExtCuentaContable;
                            cuenta[ii].nomCuenta = repasat.data[ii].nomCuentaContable;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowAccount["IDREPASAT"] = repasat.data[ii].idCuentaContable.ToString();
                            rowAccount["DESCRIPCION"] = repasat.data[ii].numCuentaContable + "-" + repasat.data[ii].nomCuentaContable;
                            rowAccount["CODIGOERP"] = repasat.data[ii].codExtCuentaContable;

                            dtAccount.Rows.Add(rowAccount);
                        }

                        check = false;
                        for (int iiii = 0; iiii < cuenta.Length; iiii++)
                        {
                            if (cuenta[iiii] != null)
                                check = true;
                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csAccountingAccounts.RootObject>(json);
                        contador = 0;
                    }
                    return dtAccount;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }
        }

        public DataTable regimenImpuestosRepasat()
        {
            string tipoObjeto = "";
            //hay que cargar los regimenes generales y los de la empresa personalizados
            //string filtroExtDocNull = "&filter[codExternoDocuPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            //string filtroURL = filtroExtDocNull;
            string filtroURL = "";

            DataTable dtTaxOperations = new DataTable();
            dtTaxOperations.TableName = "dtTaxOperations";
            dtTaxOperations.Columns.Add("IDREPASAT");
            dtTaxOperations.Columns.Add("DESCRIPCION");
            //dtPaymentDocs.Columns.Add("CODIGOERP"); //La asignación se hace en A3ERP

            csSqlConnects sql = new csSqlConnects();
            Objetos.csRegimenImpuesto[] tipoOperacion = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "taxoperations";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csTaxOperations.RootObject>(json);
                    tipoOperacion = new Objetos.csRegimenImpuesto[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtTaxOperations.NewRow();


                            tipoOperacion[ii] = new Objetos.csRegimenImpuesto();

                            tipoOperacion[ii].idRegimenImpuesto = repasat.data[ii].idRegimenImpuesto.ToString();
                            tipoOperacion[ii].codExterno = repasat.data[ii].codExternoRegimenesImpuestos;
                            //documentoPago[ii].nomDocumentoPago = repasat.data[ii].nomDocuPago;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idRegimenImpuesto.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomRegimenImpuesto;
                            //rowMethod["CODIGOERP"] = repasat.data[ii].codExternoDocuPago;

                            dtTaxOperations.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < tipoOperacion.Length; iiii++)
                        {
                            if (tipoOperacion[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csTaxOperations.RootObject>(json);
                        contador = 0;
                    }
                    return dtTaxOperations;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }

        }


        public DataTable tiposIVARepasat()
        {
            string tipoObjeto = "";
            //hay que cargar los regimenes generales y los de la empresa personalizados
            //string filtroExtDocNull = "&filter[codExternoDocuPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            //string filtroURL = filtroExtDocNull;
            string filtroURL = "";

            DataTable dtTaxTypes = new DataTable();
            dtTaxTypes.TableName = "dtTaxTypes";
            dtTaxTypes.Columns.Add("IDREPASAT");
            dtTaxTypes.Columns.Add("DESCRIPCION");
            //dtPaymentDocs.Columns.Add("CODIGOERP"); //La asignación se hace en A3ERP

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTiposIVA[] tipoIVA = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "taxtypes";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csTaxTypes.RootObject>(json);
                    tipoIVA = new Objetos.csTiposIVA[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtTaxTypes.NewRow();

                            tipoIVA[ii] = new Objetos.csTiposIVA();

                            tipoIVA[ii].idTipoIVA = repasat.data[ii].idTipoImpuesto.ToString();
                            tipoIVA[ii].codExterno = repasat.data[ii].codExternoTipoImpuesto;
                            //documentoPago[ii].nomDocumentoPago = repasat.data[ii].nomDocuPago;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idTipoImpuesto.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomTipoImpuesto;
                            //rowMethod["CODIGOERP"] = repasat.data[ii].codExternoDocuPago;

                            dtTaxTypes.Rows.Add(rowMethod);
                        }



                        check = false;
                        for (int iiii = 0; iiii < tipoIVA.Length; iiii++)
                        {
                            if (tipoIVA[iiii] != null)
                                check = true;
                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csTaxTypes.RootObject>(json);
                        contador = 0;
                    }
                    return dtTaxTypes;

                }
                else
                {
                    "No hay información para descargar".mb();
                    return null;
                }
            }

        }

        public DataSet loadRPSTAuxObjects()
        {
            try
            {
                DataSet dtsAuxObjects = new DataSet();
                dtsAuxObjects.Tables.Add(documentosPagoRepasat());
                dtsAuxObjects.Tables.Add(metodosPagoRepasat());
                dtsAuxObjects.Tables.Add(transportistas());
                dtsAuxObjects.Tables.Add(empleadosRepasat());
                dtsAuxObjects.Tables.Add(Rutas());
                dtsAuxObjects.Tables.Add(zonas());
                dtsAuxObjects.Tables.Add(Series());
                dtsAuxObjects.Tables.Add(Almacenes());
                dtsAuxObjects.Tables.Add(regimenImpuestosRepasat());
                dtsAuxObjects.Tables.Add(tiposIVARepasat());

                return dtsAuxObjects;



            }
            catch (Exception ex) {
                return null;
            }

        }

        public DataTable datosDocumentosPagoRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoDocuPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtPaymentDocs = new DataTable();
            dtPaymentDocs.TableName = "PaymentDocs";
            dtPaymentDocs.Columns.Add("IDREPASAT");
            dtPaymentDocs.Columns.Add("DESCRIPCION");
            dtPaymentDocs.Columns.Add("CODDOCUPAGO");
            dtPaymentDocs.Columns.Add("RECIBIR");
            dtPaymentDocs.Columns.Add("REMESABLE");
            dtPaymentDocs.Columns.Add("EFECTIVO");
            dtPaymentDocs.Columns.Add("CODEXTERNO");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csDocumentosPago[] documentoPago = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "paymentdocuments";

                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                    documentoPago = new Objetos.csDocumentosPago[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtPaymentDocs.NewRow();

                            documentoPago[ii] = new Objetos.csDocumentosPago();

                            documentoPago[ii].idDocumentoPago = repasat.data[ii].idDocuPago.ToString();
                            documentoPago[ii].codExternoDocuPago = repasat.data[ii].codExternoDocuPago;
                            documentoPago[ii].nomDocumentoPago = repasat.data[ii].nomDocuPago;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idDocuPago.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomDocuPago;
                            rowMethod["CODDOCUPAGO"] = repasat.data[ii].codDocuPago.ToString().PadLeft(8, ' ');
                            rowMethod["RECIBIR"] = repasat.data[ii].envioRecDocuPago;
                            rowMethod["REMESABLE"] = repasat.data[ii].remesableDocuPago;
                            rowMethod["EFECTIVO"] = repasat.data[ii].efectivoDocuPago;
                            rowMethod["CODEXTERNO"] = repasat.data[ii].codExternoDocuPago;

                            dtPaymentDocs.Rows.Add(rowMethod);
                        }

                        check = false;
                        for (int iiii = 0; iiii < documentoPago.Length; iiii++)
                        {
                            if (documentoPago[iiii] != null)
                                check = true;
                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocs.RootObject>(json);
                        contador = 0;
                    }

                    return dtPaymentDocs;
                }
                else
                {
                    "No hay información para traspasar".mb();
                    return null;
                }
            }
        }

        public DataTable datosTransportistasRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoFormaPago]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtCarriers = new DataTable();
            dtCarriers.TableName = "carriers";
            dtCarriers.Columns.Add("IDREPASAT");
            dtCarriers.Columns.Add("DESCRIPCION");
            dtCarriers.Columns.Add("CODTRANSPORT");
            dtCarriers.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTransportistas[] transportista = null;

            int total_lineas = 0, contador = 0;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "carriers";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                    transportista = new Objetos.csTransportistas[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowMethod = dtCarriers.NewRow();

                            transportista[ii] = new Objetos.csTransportistas();

                            transportista[ii].idTransportista = repasat.data[ii].idTransportista.ToString();
                            transportista[ii].codExternoTransportista = repasat.data[ii].codExternoTransportista;
                            transportista[ii].nomTransportista = repasat.data[ii].nomTransportista;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowMethod["IDREPASAT"] = repasat.data[ii].idTransportista.ToString();
                            rowMethod["DESCRIPCION"] = repasat.data[ii].nomTransportista;
                            rowMethod["CODTRANSPORT"] = repasat.data[ii].codTransportista;
                            rowMethod["CODIGOERP"] = repasat.data[ii].codExternoTransportista;
                            //Añado columna para external 

                            dtCarriers.Rows.Add(rowMethod);
                        }

                        check = false;
                        for (int iiii = 0; iiii < transportista.Length; iiii++)
                        {
                            if (transportista[iiii] != null)
                                check = true;
                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csCarriers.RootObject>(json);
                        contador = 0;
                    }
                    return dtCarriers;

                }
                else
                {
                    "No hay información para traspasar".mb();
                    return null;
                }
            }

        }
        public DataTable datosRutasRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoRuta]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtRoute = new DataTable();
            dtRoute.TableName = "dtRoutes";
            dtRoute.Columns.Add("IDREPASAT");
            dtRoute.Columns.Add("DESCRIPCION");
            dtRoute.Columns.Add("CODRUTA");
            dtRoute.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csRuta[] ruta = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "routes";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csRoutes.RootObject>(json);
                    ruta = new Objetos.csRuta[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii<repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtRoute.NewRow();
                            ruta[ii] = new Objetos.csRuta();

                            ruta[ii].idRuta = repasat.data[ii].idRuta.ToString();
                            ruta[ii].codExternoRuta = repasat.data[ii].codExternoRuta;
                            ruta[ii].nomRuta = repasat.data[ii].nomRuta;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idRuta.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomRuta;
                            rowCarrier["CODRUTA"] = repasat.data[ii].codRuta;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoRuta;

                            dtRoute.Rows.Add(rowCarrier);
                        }

                        check = false;
                        for (int iiii = 0; iiii < ruta.Length; iiii++)
                        {
                            if (ruta[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csRoutes.RootObject>(json);
                    }
                    return dtRoute;
                }
                else
                {
                    "No hay información para traspasar".mb();
                    return null;
                }
            }
        }

        public DataTable datosZonasRepasat()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoZona]=";   //filtro para cargar sólo los documentos no descargados en A3ERP
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtZona = new DataTable();
            dtZona.TableName = "dtZones";
            dtZona.Columns.Add("IDREPASAT");
            dtZona.Columns.Add("DESCRIPCION");
            dtZona.Columns.Add("CODZONA");
            dtZona.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csZonaGeografica[] zona = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "geozones";

                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeo.RootObject>(json);
                    zona = new Objetos.csZonaGeografica[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtZona.NewRow();

                            zona[ii] = new Objetos.csZonaGeografica();

                            zona[ii].idZona = repasat.data[ii].idZonaGeo.ToString();
                            zona[ii].codExternoZona = repasat.data[ii].codExternoZonaGeo.ToString();
                            zona[ii].nomZona = repasat.data[ii].nomZonaGeo;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idZonaGeo.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomZonaGeo;
                            rowCarrier["CODZONA"] = repasat.data[ii].codZonaGeo;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoZonaGeo;

                            dtZona.Rows.Add(rowCarrier);
                        }

                        check = false;
                        for (int iiii = 0; iiii < zona.Length; iiii++)
                        {
                            if (zona[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }

                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeo.RootObject>(json);
                    }
                    return dtZona;
                }
                else
                {
                    "No hay información para traspasar".mb();
                    return null;
                }
            }
        }

        public DataTable datosAlmacenes()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoAlmacen]=";   
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtWarehouses = new DataTable();
            dtWarehouses.TableName = "dtWarehouses";
            dtWarehouses.Columns.Add("IDREPASAT");
            dtWarehouses.Columns.Add("DESCRIPCION");
            dtWarehouses.Columns.Add("CODALMACEN");
            dtWarehouses.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csWarehouses[] almacen = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "warehouses";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    almacen = new Objetos.csWarehouses[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtWarehouses.NewRow();

                            almacen[ii] = new Objetos.csWarehouses();

                            almacen[ii].idAlmacen = repasat.data[ii].idAlmacen.ToString();
                            almacen[ii].codExternoAlmacen = repasat.data[ii].codExternoAlmacen;
                            almacen[ii].nomAlmacen = repasat.data[ii].nomAlmacen;

                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idAlmacen.ToString();
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomAlmacen;
                            rowCarrier["CODALMACEN"] = repasat.data[ii].refAlmacen;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoAlmacen;

                            dtWarehouses.Rows.Add(rowCarrier);
                        }

                        check = false;
                        for (int iiii = 0; iiii < almacen.Length; iiii++)
                        {
                            if (almacen[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csWarehouses.RootObject>(json);
                    }
                    return dtWarehouses;
                }
                else
                {
                    "No hay información para traspasar".mb();
                    return null;
                }
            }
        }

        public DataTable datosTipoArt()
        {
            string tipoObjeto = "";
            string filtroExtDocNull = "&filter[codExternoTipoArticulo]=";
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroURL = filtroExtDocNull;

            DataTable dtTipoArt = new DataTable();
            dtTipoArt.TableName = "dtTipoArt";
            dtTipoArt.Columns.Add("IDREPASAT");
            dtTipoArt.Columns.Add("CODREPASAT");
            dtTipoArt.Columns.Add("DESCRIPCION");
            dtTipoArt.Columns.Add("CODIGOERP");

            csSqlConnects sql = new csSqlConnects();
            Objetos.csTipoArt[] tipoArt = null;

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                tipoObjeto = "producttypes";


                var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.csTipoArt.RootObject>(json);
                    tipoArt = new Objetos.csTipoArt[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {
                        //Comienzo iteración de los elementos
                        for (int ii = 0; ii < repasat.data.Count; ii++) // cabeceras
                        {
                            DataRow rowCarrier = dtTipoArt.NewRow();

                            ////Valido que el documento no esté bajado o que no esté marcado para on bajar
                            ////También valido si he marcado el cliente como que no se descargue (si tiene una X el cliente no se debe bajar)
                            ////Cuando se borra un codExternoDocumento, el campo se pone a 0
                            //if ((!string.IsNullOrEmpty(repasat.data[ii].codExternoDocumento) && repasat.data[ii].codExternoDocumento != "0") || repasat.data[ii].codExternoDocumento == "X" || repasat.data[ii].account.codExternoCli == "X")
                            //    continue;
                            tipoArt[ii] = new Objetos.csTipoArt();

                            tipoArt[ii].idTipoArticulo = repasat.data[ii].idTipoArticulo;
                            tipoArt[ii].codExternoTipoArticulo = repasat.data[ii].codExternoTipoArticulo;
                            tipoArt[ii].codTipoArticulo = repasat.data[ii].codTipoArticulo;
                            tipoArt[ii].nomTipoArticulo = repasat.data[ii].nomTipoArticulo;


                            //Añado toda la información en el datatable para mostrar al final los documentos que traspaso
                            rowCarrier["IDREPASAT"] = repasat.data[ii].idTipoArticulo.ToString();
                            rowCarrier["CODREPASAT"] = repasat.data[ii].codTipoArticulo;
                            rowCarrier["DESCRIPCION"] = repasat.data[ii].nomTipoArticulo;
                            rowCarrier["CODIGOERP"] = repasat.data[ii].codExternoTipoArticulo;

                            dtTipoArt.Rows.Add(rowCarrier);
                        }



                        check = false;
                        for (int iiii = 0; iiii < tipoArt.Length; iiii++)
                        {
                            if (tipoArt[iiii] != null)
                                check = true;
                        }

                        if (check)
                        {

                        }


                        //Una vez he revisado la primera página miro si hay una segunda página o posteriores (bucle)
                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1).ToString(), "GET", tipoObjeto, "", "");
                        if (i + 1 <= repasat.last_page)
                            repasat = JsonConvert.DeserializeObject<Repasat.csTipoArt.RootObject>(json);
                    }
                    return dtTipoArt;

                }
                else
                {
                    //toolStripStatusLabel1.Text = "No hay documentos para descargar entre los periodos seleccionados";
                    "No hay información para descargar".mb();
                    return null;
                }
            }
        }

    }
}
