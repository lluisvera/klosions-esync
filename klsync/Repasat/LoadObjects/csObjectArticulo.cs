﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Repasat.LoadObjects
{
    class csObjectArticulo
    {
        public DataGridView dgv { get; set; }
        public bool consulta { get; set; }
        public string productId { get; set; }
        public bool pendientes { get; set; }
        public bool update { get; set; }
        public bool syncAllData { get; set; }
        public bool syncYesData { get; set; }
        public bool syncNoData { get; set; }
        public string accountId { get; set; }
    }
}
