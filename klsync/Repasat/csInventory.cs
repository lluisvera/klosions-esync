﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csInventory
    {

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Pivot
        {
            public int idArticulo { get; set; }
            public int idAlmacen { get; set; }
            public string unidades1ArticuloAlmacen { get; set; }
            public string unidades2ArticuloAlmacen { get; set; }
        }

        public class Warehouse
        {
            public int idAlmacen { get; set; }
            public int idEntidadAlmacen { get; set; }
            public string refAlmacen { get; set; }
            public string nomAlmacen { get; set; }
            public object idContacto { get; set; }
            public object idDireccion { get; set; }
            public string fecAltaAlmacen { get; set; }
            public int activoAlmacen { get; set; }
            public string codExternoAlmacen { get; set; }
            public Pivot pivot { get; set; }
        }

        public class Entity
        {
            public string nomEntidad { get; set; }
            public string cifEntidad { get; set; }
        }

        public class Datum
        {
            public int idArticulo { get; set; }
            public int codArticulo { get; set; }
            public string nomArticulo { get; set; }
            public string refArticulo { get; set; }
            public string descArticulo { get; set; }
            public int? idTipoArticulo { get; set; }
            public int? fotoArticulo { get; set; }
            public double precioVentaArticulo { get; set; }
            public double precioCompraArticulo { get; set; }
            public int cantidadArticulo { get; set; }
            public int activoArticulo { get; set; }
            public string codExternoArticulo { get; set; }
            public string fecAltaArticulo { get; set; }
            public object fecBajaArticulo { get; set; }
            public object idFabricante { get; set; }
            public int? idFam { get; set; }
            public int? idSubFam { get; set; }
            public object idUni { get; set; }
            public object idArtAlt { get; set; }
            public int? idResp { get; set; }
            public int? bloqueadoArticulo { get; set; }
            public string motivoArticulo { get; set; }
            public int? compraArticulo { get; set; }
            public int? ventaArticulo { get; set; }
            public int? stockArticulo { get; set; }
            public int? planificableArticulo { get; set; }
            public object pesoArticulo { get; set; }
            public object volumenArticulo { get; set; }
            public object descuentoMaxArticulo { get; set; }
            public object codigobarrasArticulo { get; set; }
            public string preciocostecompraArticulo { get; set; }
            public object fechaentregacompraArticulo { get; set; }
            public object fechaentregaventaArticulo { get; set; }
            public string alarmapresucompraArticulo { get; set; }
            public string alarmapresuventaArticulo { get; set; }
            public object alarmapedidoscompraArticulo { get; set; }
            public string alarmapedidosventaArticulo { get; set; }
            public string alarmaalbaranescompraArticulo { get; set; }
            public string alarmaalbaranesventaArticulo { get; set; }
            public string alarmafacturascompraArticulo { get; set; }
            public string alarmafacturasventaArticulo { get; set; }
            public object largoArticulo { get; set; }
            public object anchoArticulo { get; set; }
            public object gruesoArticulo { get; set; }
            public object embalajeArticulo { get; set; }
            public object embalajesPorCaja { get; set; }
            public object unidadesPorPale { get; set; }
            public string referenciaProveedorArticulo { get; set; }
            public string aliasArticulo { get; set; }
            public int? idTipoImpuesto { get; set; }
            public int? cuentaContableCompraArticulo { get; set; }
            public long? cuentaContableVentaArticulo { get; set; }
            public int? idTipoImpuestoCompra { get; set; }
            public int tieneNumSerieArticulo { get; set; }
            public string fecModificacionArticulo { get; set; }
            public object idCuentaContableVenta { get; set; }
            public object idCuentaContableCompra { get; set; }
            public object idUbicacion { get; set; }
            public int esEpi { get; set; }
            public object fecSincronizacionArticulo { get; set; }
            public int? idRepresentado { get; set; }
            public object urlExternaArticulo { get; set; }
            public int paraNotaGasto { get; set; }
            public double available_units { get; set; }
            public List<Warehouse> warehouses { get; set; }
            public Entity entity { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }



    }
}
