﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csTipoArt
    {
        public class Datum
        {
            public int idTipoArticulo { get; set; }
            public int codTipoArticulo { get; set; }
            public string nomTipoArticulo { get; set; }
            public int? activoTipoArticulo { get; set; }
            public string fecAltaTipoArticulo { get; set; }
            public int? autorTipoArticulo { get; set; }
            public string codExternoTipoArticulo { get; set; }

        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
