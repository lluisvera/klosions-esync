﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csEmpty
    {
        public class RootObject
        {
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public object from { get; set; }
            public object to { get; set; }
            public List<object> data { get; set; }
        }
    }
}
