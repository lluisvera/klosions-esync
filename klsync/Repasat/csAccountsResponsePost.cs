﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csAccountsResponsePost
    {

        public class Resource
        {
            public string codExternoCli { get; set; }
            public string nomCli { get; set; }
            public string cifCli { get; set; }
            public string activoCli { get; set; }
            public string diaPago1Cli { get; set; }
            public string diaPago2Cli { get; set; }
            public string diaPago3Cli { get; set; }
            public string descCli { get; set; }
            public string tel1 { get; set; }
            public string razonSocialCli { get; set; }
            public string webCli { get; set; }
            public string emailCli { get; set; }
            public string tipoCli { get; set; }
            public int codCli { get; set; }
            public int idCli { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
