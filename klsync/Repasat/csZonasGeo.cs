﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csZonasGeo
    {
        public class Datum
        {
            public int idZonaGeo { get; set; }
            public int idEntidadZonaGeo { get; set; }
            public int codZonaGeo { get; set; }
            public string nomZonaGeo { get; set; }
            public int? autorZonaGeo { get; set; }
            public int activoZonaGeo { get; set; }
            public string fecAltaZonaGeo { get; set; }
            public string codExternoZonaGeo { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
