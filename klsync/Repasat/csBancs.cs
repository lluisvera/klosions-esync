﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.Repasat
{
    class csBancs
    {

        public class Pivot
        {
            public int idDatosBancarios { get; set; }
            public int idCli { get; set; }
            public int idEntidadDatosBancariosCli { get; set; }
            public int principalDatosBancarios { get; set; }
        }

        public class Customer
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public object descCli { get; set; }
            public string tipoCli { get; set; }
            public int?  idZonaGeo { get; set; }
            public int?  idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public string idEmpresaClienteAplicacionCli { get; set; }
            public int?  idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int?  idTrabajador { get; set; }
            public int?  idDocuPago { get; set; }
            public int?  idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public double porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public string cuentaContableProveedorCli { get; set; }
            public int?  idRegimenImpuesto { get; set; }
            public string idTipoRetencion { get; set; }
            public string idEmpresaAccesoExterno { get; set; }
            public string idClasificacion { get; set; }
            public string idSerie { get; set; }
            public string representadoCli { get; set; }
            public string comisionCli { get; set; }
            public int? descuentoCli { get; set; }
            public string idFormatoImpresionPresupuesto { get; set; }
            public string idFormatoImpresionPedidoVenta { get; set; }
            public string idFormatoImpresionPedidoCompra { get; set; }
            public string idFormatoImpresionAlbaranVenta { get; set; }
            public string idFormatoImpresionAlbaranCompra { get; set; }
            public string idFormatoImpresionFacturaVenta { get; set; }
            public string idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int?  idRuta { get; set; }
            public object fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public object idTarifaDescuentos { get; set; }
            public Pivot pivot { get; set; }
        }

        public class Datum
        {
            public int idDatosBancarios { get; set; }
            public string nomDatosBancarios { get; set; }
            public string titularCuentaDatosBancarios { get; set; }
            public string ibanCuentaDatosBancarios { get; set; }
            public string entidadCuentaDatosBancarios { get; set; }
            public string agenciaCuentaDatosBancarios { get; set; }
            public string dcCuentaDatosBancarios { get; set; }
            public string numCuentaDatosBancarios { get; set; }
            public string bicCuentaDatosBancarios { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaDatosBancarios { get; set; }
            public string cuentaContableDatosBancarios { get; set; }
            public string sufijoRemesaCobro { get; set; }
            public string efectivoDatosBancarios { get; set; }
            public string codExternoDatosBancarios { get; set; }
            public List<Customer> customers { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

        public DataSet cargarBancosA3ToRepasat(bool ventas, bool update = false, string idCuenta=null)
        {
            try
            {
                string updateDate = null;
                string rowKey = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();

                dt = sqlConnect.obtenerDatosSQLScript(sqlScript.selectBancosFromA3ToRepasat(update, updateDate, idCuenta, ventas));
                
                Objetos.csTercero cliente = new Objetos.csTercero();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (update)
                        {
                            rowKey = dr["RPST_ID_CLI"].ToString();
                        }
                        else
                        {
                            rowKey = dr["bank[codExternoDatosBancarios]"].ToString();
                        }
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {

                            //if (dtcol.ColumnName == "idRuta")
                            //{
                            //    string stop = "";
                            //}
                            if (string.IsNullOrEmpty(dr[dtcol.ColumnName].ToString()))
                            {
                                continue;
                            }
                            else
                            {
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcol.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = dr[dtcol.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);
                    //csRepasatWebService rpstWS = new csRepasatWebService();
                    //rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "");

                    return datasetRPST;
                }
                else
                {
                    return null;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
