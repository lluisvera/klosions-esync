﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csSeriesdocsResponsePost
    {
        public class Resource
        {
            public string nomSerie { get; set; }
            public int idEntidadZonaGeo { get; set; }
            public int idSerie { get; set; }
            public string codExternoSerie { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
