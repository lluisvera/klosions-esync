﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csEmployeesResponsePost
    {

        public class Resource
        {
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string codExternoTrabajador { get; set; }
            //public int __invalid_name__trabajadores.idEntidadTrabajador { get; set; }
            public int codTrabajador { get; set; }
            public int idTrabajador { get; set; }
            public string nomCompleto { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
