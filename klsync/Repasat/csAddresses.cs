﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace klsync.Repasat
{
    class csAddresses
    {
        public class Datum
        {
            public int idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int? idProvincia { get; set; }
            public string idPais { get; set; }
            public int? idUsuario { get; set; }
            public double? latitudDireccion { get; set; }
            public double? longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
            public string horarioDireccion { get; set; }
            public int? idRuta { get; set; }
            public string fecModificacionDireccion { get; set; }
            public List<Customer> customers { get; set; }
            public List<object> leads { get; set; }
        }

        public class Pivot
        { 
            public int idCli { get; set; }
            public int idDireccion { get; set; }
            public bool principaldireccion { get; set; }
            public bool facturacionDireccion { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

        public class Customer
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public object descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public object idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public object tel2 { get; set; }
            public object fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public object idEmpresaClienteAplicacionCli { get; set; }
            public object idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public int porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public object idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public object descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int? idRuta { get; set; }
            public object fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public object idTarifaDescuentos { get; set; }
            public Pivot pivot { get; set; }
        }

        public DataSet cargarDireccionesA3ToRepasat(string codCli, bool esCliente=true, bool isUpdate=false, string updateDate=null)
        {
            try
            {
                string rowKey = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();
                dt = sqlConnect.obtenerDatosSQLScript(sqlScript.selectDireccionesFromA3ToRepasat(codCli,esCliente,isUpdate,updateDate));
                Objetos.csTercero cliente = new Objetos.csTercero();


                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");

                foreach (DataRow dr in dt.Rows)
                {
                    rowKey = dr["address[codExternoDireccion]"].ToString();
                    rowKey = isUpdate? dr["idDireccion"].ToString(): dr["address[codExternoDireccion]"].ToString();
                    DataRow drKeys = dtKeys.NewRow();
                    drKeys["KEY"] = rowKey;
                    dtKeys.Rows.Add(drKeys);

                    foreach (DataColumn dtcol in dt.Columns)
                    {
                        
                        DataRow drParams = dtParams.NewRow();
                        drParams["FIELD"] = dtcol.ColumnName;
                        drParams["KEY"] = rowKey;
                        drParams["VALUE"] = dr[dtcol.ColumnName];
                        dtParams.Rows.Add(drParams);

                    }
                }

                datasetRPST.Tables.Add(dtKeys);
                datasetRPST.Tables.Add(dtParams);
                csRepasatWebService rpstWS = new csRepasatWebService();

                string metodoCRUD = isUpdate ? "PUT" : "POST" ;
                rpstWS.sincronizarObjetoRepasat("addresses", metodoCRUD, dtKeys, dtParams, "", esCliente, isUpdate);

                csUpdateData updateData = new csUpdateData();
                updateData.actualizarFechaSync("addresses", dtKeys.Rows.Count, false);
                return datasetRPST;
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }



}
