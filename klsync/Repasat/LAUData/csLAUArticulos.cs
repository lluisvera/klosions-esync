﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using a3ERPActiveX;
using MySql.Data.MySqlClient;
//using System.Xaml;


namespace klsync.Repasat.LoadData
{
    class 
        csLoadArticulos
    {
        Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
//        public void cargarDatosArticulos(DataGridView dgv, bool consulta = false, string productId = null, bool pendientes = false, bool update = false)

        public DataTable cargarDatosArticulosOld(Repasat.LoadObjects.csObjectArticulo objArticulo )
        {
            string errorProducto = "";

            try
            {

                string filtroItemsNoSyncro = "&filter[codExternoArticulo]=";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;


                Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();


                string fechaUltSync = "";
                fechaUltSync = objArticulo.update ? rpstUpdate.getLastUpdateSyncDate("products", true) : "";
                string filtroFechaLatUpdate = "&filter_gte[fecModificacionArticulo]=" + fechaUltSync;

                filtroFechaLatUpdate = objArticulo.update ? filtroFechaLatUpdate : "";

                if (objArticulo.syncNoData || objArticulo.pendientes)
                {
                    filtroItemsNoSyncro = "&filter[codExternoArticulo]=";
                }
                else if (objArticulo.syncYesData)
                {
                    filtroItemsNoSyncro = "&filter_ne[codExternoArticulo]=";
                }
                else if (objArticulo.syncAllData)
                {
                    filtroItemsNoSyncro = "";
                }

                //analizar si se puede borrar 
                objArticulo.dgv.DataSource = null;
                objArticulo.dgv.Refresh();
                //hasta aquí

                csSqlConnects sql = new csSqlConnects();
                Objetos.csArticulo[] products = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();


                csa3erpItm A3Products = new csa3erpItm();

                string tipoObjeto = "products";
                int numRegistro = 0;

                //creo un DataTable para almacenar los Clientes creados
                DataTable dtProducts = new DataTable();

                dtProducts.Columns.Add("IDREPASAT");
                dtProducts.Columns.Add("REFERENCIA");
                dtProducts.Columns.Add("DESCART");
                dtProducts.Columns.Add("CODIGOERP");
                dtProducts.Columns.Add("ACTIVO");
                //dtProducts.Columns.Add("NIF");
                //dtProducts.Columns.Add("CODIGOERP");
                //dtProducts.Columns.Add("TIPOCUENTA");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newProducts = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    string filtroURL = filtroApiKey + filtroItemsNoSyncro + filtroFechaLatUpdate;

                    if (!string.IsNullOrEmpty(objArticulo.accountId))
                    {
                        filtroURL = "&filter[idCli]=" + objArticulo.accountId;
                    }

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);
                    products = new Objetos.csArticulo[repasat.data.Count];

                    for (int i = 1; i <= repasat.last_page; i++)
                    {
                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            if (repasat.data[ii].codExternoArticulo == null || objArticulo.consulta || objArticulo.update)
                            {
                                if (repasat.data[ii].codExternoArticulo != null)
                                {
                                    if (string.IsNullOrEmpty(repasat.data[ii].refArticulo))
                                    {
                                        repasat.data[ii].refArticulo = "SIN_REF";
                                    }
                                }
                                newProducts = true;
                                products[numRegistro] = new Objetos.csArticulo();
                                errorProducto = repasat.data[ii].codExternoArticulo == null ? "0" : repasat.data[ii].refArticulo.ToString().ToUpper();
                                products[numRegistro].codart = repasat.data[ii].codExternoArticulo == null ? "0" : repasat.data[ii].refArticulo.ToString().ToUpper();
                                products[numRegistro].codExternoArt = repasat.data[ii].idArticulo.ToString();
                                products[numRegistro].bloqueado = (repasat.data[ii].activoArticulo.ToString() == "1") ? "F" : "T";
                                if (csGlobal.nombreServidor.Contains("PURE") || csGlobal.nombreServidor.Contains("PRUEBAS"))
                                {
                                    products[numRegistro].descArt = repasat.data[ii].nomArticulo;
                                    products[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasArticulo) ? "" : repasat.data[ii].aliasArticulo.ToString();

                                }
                                else
                                {
                                    products[numRegistro].descArt = repasat.data[ii].nomArticulo.ToUpper();
                                    products[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasArticulo) ? "" : repasat.data[ii].aliasArticulo.ToString().ToUpper();

                                }
                                products[numRegistro].refProveedor = string.IsNullOrWhiteSpace(repasat.data[ii].referenciaProveedorArticulo) ? "" : repasat.data[ii].referenciaProveedorArticulo.ToString().ToUpper();
                                products[numRegistro].esVenta = repasat.data[ii].ventaArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].esCompra = repasat.data[ii].compraArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].esStock = repasat.data[ii].stockArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].precioSinIVA = string.IsNullOrWhiteSpace(repasat.data[ii].precioVentaArticulo.ToString()) ? "0" : repasat.data[ii].precioVentaArticulo.ToString().Replace(".", ",");
                                products[numRegistro].precioCompra = string.IsNullOrWhiteSpace(repasat.data[ii].precioCompraArticulo.ToString()) ? "0" : repasat.data[ii].precioCompraArticulo.ToString().Replace(".", ",");
                                products[numRegistro].precioCoste = string.IsNullOrWhiteSpace(repasat.data[ii].preciocostecompraArticulo) ? "0" : repasat.data[ii].preciocostecompraArticulo.ToString().Replace(".", ",");

                                //pendientes los tipos de iva
                                // products[numRegistro].tipoIVAVenta = repasat.data[ii].idTipoImpuesto.ToString();
                                //products[numRegistro].tipoIVACompra = repasat.data[ii].idTipoImpuestoCompra.ToString();

                                //añado el cliente a la fila del datatable
                                DataRow filaProducto = dtProducts.NewRow();

                                filaProducto["IDREPASAT"] = repasat.data[ii].idArticulo.ToString();
                                filaProducto["REFERENCIA"] = string.IsNullOrWhiteSpace(repasat.data[ii].refArticulo) ? "" : repasat.data[ii].refArticulo.ToString();
                                filaProducto["DESCART"] = string.IsNullOrWhiteSpace(repasat.data[ii].nomArticulo) ? "" : repasat.data[ii].nomArticulo.ToString();


                                //if (string.IsNullOrWhiteSpace(repasat.data[ii].codExternoArticulo)) {
                                //    filaProducto["CODIGOERP"] = "0";
                                //}
                                //else {
                                //    int n;
                                //    filaProducto["CODIGOERP"] = int.TryParse(repasat.data[ii].codExternoArticulo.ToString(), out n);
                                //}
                                filaProducto["CODIGOERP"] = string.IsNullOrWhiteSpace(repasat.data[ii].codExternoArticulo) ? "" : repasat.data[ii].codExternoArticulo.ToString();
                                filaProducto["ACTIVO"] = repasat.data[ii].activoArticulo == 1 ? "SI" : "NO";

                                dtProducts.Rows.Add(filaProducto);
                                numRegistro++;
                            }
                        }

                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);

                            Array.Resize(ref products, products.Length + repasat.data.Count);
                        }
                    }
                }

                //objArticulo.dgv.DataSource = dtProducts;

                if (!objArticulo.consulta && newProducts)
                {
                    a3.abrirEnlace();

                    if (objArticulo.update)
                    {
                        updateA3.actualizarArticulos(products);

                    }
                    else
                    {
                        A3Products.crearArticuloEnA3ERP(products);
                    }
                    a3.cerrarEnlace();
                }
                return dtProducts;

            }
            catch (System.Runtime.InteropServices.COMException ex) { 
                MessageBox.Show(ex.Message);
                return null;
            }
            catch (JsonException ex) { 
                MessageBox.Show(ex.Message);
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + errorProducto);
                return null;
            }

        }





        /////////////////////////////
        ///


        public DataTable cargarDatosArticulos(Repasat.LoadObjects.csObjectArticulo objArticulo)
        {
            string errorProducto = "";

            try
            {

                DataSet datasetArticulos = new DataSet();

                string filtroArticulosNoSyncro = "&filter[codExternoCli]=";
                string filtroEstadoSyncro = "";
                string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
                string filtroFechaLatUpdate = "";

                if (objArticulo.consulta)
                {
                    Repasat.csUpdateData rpstUpdate = new Repasat.csUpdateData();
                    string fechaUltSync = objArticulo.update ? rpstUpdate.getLastUpdateSyncDate("products", true) : "";

                    filtroFechaLatUpdate = "&filter_gte[fecModificacionArt]=" + fechaUltSync;

                    filtroFechaLatUpdate = objArticulo.update ? filtroFechaLatUpdate : "";

                    //filtroFechaLatUpdate = string.IsNullOrEmpty(idAccount) ? filtroFechaLatUpdate : "";
                }
                if (objArticulo.pendientes)
                {
                    filtroEstadoSyncro = "&filter[codExternoArticulo]=";
                }
                if (objArticulo.syncAllData)
                {
                    filtroEstadoSyncro = "&filter_ne[codExternoArticulo]=";
                }

                filtroArticulosNoSyncro = objArticulo.pendientes ? "&filter[codExternoArticulo]=" : filtroArticulosNoSyncro = "";

                //analizar si se puede borrar 
                objArticulo.dgv.DataSource = null;
                objArticulo.dgv.Refresh();
                //hasta aquí

                csSqlConnects sql = new csSqlConnects();
                Objetos.csArticulo[] products = null;
                csa3erp a3 = new csa3erp();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();


                csa3erpItm A3Products = new csa3erpItm();

                string tipoObjeto = "products";
                int numRegistro = 0;
                //SE AÑADE
                int totalLineas = 0;
                int contador = 0;


                //creo un DataTable para almacenar los Clientes creados
                DataTable dtProducts = new DataTable();

                dtProducts.Columns.Add("IDREPASAT");
                dtProducts.Columns.Add("REFERENCIA");
                dtProducts.Columns.Add("DESCART");
                dtProducts.Columns.Add("CODIGOERP");
                dtProducts.Columns.Add("ACTIVO");
                //dtProducts.Columns.Add("NIF");
                //dtProducts.Columns.Add("CODIGOERP");
                //dtProducts.Columns.Add("TIPOCUENTA");

                //Añado un control para ver si tengo que crear clientes nuevos o no
                bool newProducts = false;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;

                    //REVISAR LA VARIABLE filtroApiKey porque en la otra funcion se usa la variable filtroArticulosNoSyncro
                    string filtroURL = filtroApiKey + filtroEstadoSyncro + filtroFechaLatUpdate;

                    if (!string.IsNullOrEmpty(objArticulo.accountId))
                    {
                        filtroURL = "&filter[idCli]=" + objArticulo.accountId;
                    }

                    var json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + 1, "GET", tipoObjeto, filtroURL, "");
                    var repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);
                    products = new Objetos.csArticulo[repasat.data.Count];

                    for (int i = 1; i <= repasat.last_page; i++)
                    {
                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            if (repasat.data[ii].codExternoArticulo == null || objArticulo.consulta || objArticulo.update)
                            {
                                if (repasat.data[ii].codExternoArticulo != null)
                                {
                                    if (string.IsNullOrEmpty(repasat.data[ii].refArticulo))
                                    {
                                        repasat.data[ii].refArticulo = "SIN_REF";
                                    }
                                }
                                newProducts = true;
                                products[numRegistro] = new Objetos.csArticulo();
                                errorProducto = repasat.data[ii].codExternoArticulo == null ? "0" : repasat.data[ii].refArticulo.ToString().ToUpper();
                                products[numRegistro].codart = repasat.data[ii].codExternoArticulo == null ? "0" : repasat.data[ii].refArticulo.ToString().ToUpper();
                                products[numRegistro].codExternoArt = repasat.data[ii].idArticulo.ToString();
                                products[numRegistro].bloqueado = (repasat.data[ii].activoArticulo.ToString() == "1") ? "F" : "T";
                                if (csGlobal.nombreServidor.Contains("PURE") || csGlobal.nombreServidor.Contains("PRUEBAS"))
                                {
                                    products[numRegistro].descArt = repasat.data[ii].nomArticulo;
                                    products[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasArticulo) ? "" : repasat.data[ii].aliasArticulo.ToString();

                                }
                                else
                                {
                                    products[numRegistro].descArt = repasat.data[ii].nomArticulo.ToUpper();
                                    products[numRegistro].alias = string.IsNullOrWhiteSpace(repasat.data[ii].aliasArticulo) ? "" : repasat.data[ii].aliasArticulo.ToString().ToUpper();

                                }
                                products[numRegistro].refProveedor = string.IsNullOrWhiteSpace(repasat.data[ii].referenciaProveedorArticulo) ? "" : repasat.data[ii].referenciaProveedorArticulo.ToString().ToUpper();
                                products[numRegistro].esVenta = repasat.data[ii].ventaArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].esCompra = repasat.data[ii].compraArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].esStock = repasat.data[ii].stockArticulo.ToString() == "1" ? "T" : "F";
                                products[numRegistro].precioSinIVA = string.IsNullOrWhiteSpace(repasat.data[ii].precioVentaArticulo.ToString()) ? "0" : repasat.data[ii].precioVentaArticulo.ToString().Replace(".", ",");
                                products[numRegistro].precioCompra = string.IsNullOrWhiteSpace(repasat.data[ii].precioCompraArticulo.ToString()) ? "0" : repasat.data[ii].precioCompraArticulo.ToString().Replace(".", ",");
                                products[numRegistro].precioCoste = string.IsNullOrWhiteSpace(repasat.data[ii].preciocostecompraArticulo) ? "0" : repasat.data[ii].preciocostecompraArticulo.ToString().Replace(".", ",");

                                //pendientes los tipos de iva
                                // products[numRegistro].tipoIVAVenta = repasat.data[ii].idTipoImpuesto.ToString();
                                //products[numRegistro].tipoIVACompra = repasat.data[ii].idTipoImpuestoCompra.ToString();

                                //añado el cliente a la fila del datatable
                                DataRow filaProducto = dtProducts.NewRow();

                                filaProducto["IDREPASAT"] = repasat.data[ii].idArticulo.ToString();
                                filaProducto["REFERENCIA"] = string.IsNullOrWhiteSpace(repasat.data[ii].refArticulo) ? "" : repasat.data[ii].refArticulo.ToString();
                                filaProducto["DESCART"] = string.IsNullOrWhiteSpace(repasat.data[ii].nomArticulo) ? "" : repasat.data[ii].nomArticulo.ToString();


                                //if (string.IsNullOrWhiteSpace(repasat.data[ii].codExternoArticulo)) {
                                //    filaProducto["CODIGOERP"] = "0";
                                //}
                                //else {
                                //    int n;
                                //    filaProducto["CODIGOERP"] = int.TryParse(repasat.data[ii].codExternoArticulo.ToString(), out n);
                                //}
                                filaProducto["CODIGOERP"] = string.IsNullOrWhiteSpace(repasat.data[ii].codExternoArticulo) ? "" : repasat.data[ii].codExternoArticulo.ToString();
                                filaProducto["ACTIVO"] = repasat.data[ii].activoArticulo == 1 ? "SI" : "NO";

                                dtProducts.Rows.Add(filaProducto);
                                numRegistro++;
                            }
                        }

                        json = updateDataRPST.whileJson(wc, tipoObjeto, "&page=" + (i + 1), "GET", tipoObjeto, filtroURL, "");
                        if (i + 1 <= repasat.last_page)
                        {
                            repasat = JsonConvert.DeserializeObject<Repasat.csProducts.RootObject>(json);

                            Array.Resize(ref products, products.Length + repasat.data.Count);
                        }
                    }
                }

                //objArticulo.dgv.DataSource = dtProducts;

                if (!objArticulo.consulta && newProducts)
                {
                    a3.abrirEnlace();

                    if (objArticulo.update)
                    {
                        updateA3.actualizarArticulos(products);

                    }
                    else
                    {
                        A3Products.crearArticuloEnA3ERP(products);
                    }
                    a3.cerrarEnlace();
                }
                return dtProducts;

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            catch (JsonException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + errorProducto);
                return null;
            }

        }


    }
}
