﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using klsync.A3ERP.Auxiliares;
using a3ERPActiveX;
using Newtonsoft.Json;

namespace klsync.Repasat.LAUData
{
    //CLASE PARA GESTIONAR EL TRASPASO DE CLIENTES DE RPST A A3
    class csLAUClientes
    {
        private readonly csUpdateData updateDataRPST;

        //CONSTRUCTOR
        public csLAUClientes(csUpdateData updateDataRPST)
        {
            this.updateDataRPST = updateDataRPST;
        }

        public void TraspasarCuentasToA3(DataGridView dgv, bool clientes,  bool consulta,  bool forceSync = false)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgv.SelectedRows)
                {
                    string idCliente = row.Cells["IDREPASAT"].Value.ToString();
                    //TraspasarCuentaToA3(clientes, consulta, idCliente, forceSync);
                    //Se cambia y se añade en la funcion de abajo una nueva variable 'string codExt', si no se impleemnta, quitarlo
                    TraspasarCuentaToA3(clientes, consulta, idCliente, forceSync, row.Cells.ToString());
                }
            }
            else
            {
                TraspasarCuentaToA3(clientes, consulta, null, forceSync, null);
            }
        }

        private void TraspasarCuentaToA3(bool clientes,bool consulta, string idCliente, bool forceSync, string codExt)
        {
            updateDataRPST.traspasarCuentasRepasatToA3(clientes, false, idCliente, true, true, false, false, forceSync, codExt);
        }

        public void SincronizarClienteDesdeRepasatA3ERP(string clienteJson)
        {
            var clienteRepasat = JsonConvert.DeserializeObject<csAccounts.Datum>(clienteJson);


        }
    }
}

