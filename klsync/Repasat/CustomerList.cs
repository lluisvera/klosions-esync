﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class CustomerList
    {
        public class Data
        {
            public Datum resource { get; set; }
        }
        public class Datum
        {
            public int idCli { get; set; }
            public int? idGrupoEmpresarial { get; set; }
            public int? idTamanoFacturas { get; set; }
            public int? idTamanoEmpleados { get; set; }
            public int? idOrigen { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int sincronizarCuenta { get; set; }
            public object idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int idTrabajador { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public string porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public object idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public object descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public object idRuta { get; set; }
            public object fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public object idUsuarioCreador { get; set; }
            public object idUsuarioEditor { get; set; }
            public object idTarifaDescuentos { get; set; }
            public object idSector { get; set; }
            public object idMotivoBaja { get; set; }
            public object fecBaja { get; set; }
            public object idTamanoEmpresa { get; set; }
            public object idCuentaContable { get; set; }
            public object fecSincronizacionCli { get; set; }
            public int correspondenciaEmailCli { get; set; }
            public int correspondenciaTelfCli { get; set; }
            public string liquidacionDocumentos { get; set; }
            public object codigoAfiliadoCli { get; set; }
            public object idTransportista { get; set; }
            public int portesPagados { get; set; }
            public object costeTransporte { get; set; }
            public object costeParaGratis { get; set; }
            public object aliasExportDoc { get; set; }
            public int clienteGenerico { get; set; }
            public object idAdjunto { get; set; }
            public string tipoCliPro { get; set; }
            public List<Address> addresses { get; set; }
            public List<ConexionExterna> conexion_externa { get; set; }
        }

        public class RootObject
        {
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public int from { get; set; }
            public int to { get; set; }
            public List<Datum> data { get; set; }
        }

        public class Pivot
        {
            public int idCli { get; set; }
            public int idDireccion { get; set; }
            public int idEntidadDireccionCli { get; set; }
            public int principalDireccion { get; set; }
            public int facturacionDireccion { get; set; }
        }

        public class ConexionExterna
        {
            public int idCliExt { get; set; }
            public int idConExt { get; set; }
            public string tipoValor { get; set; }
            public string idExterno { get; set; }
            public int idRepasat { get; set; }
            public object idUsuario { get; set; }
            public object idUsuarioUpdate { get; set; }
            public string created_at { get; set; }
            public string updated_at { get; set; }
            public object activoInnuva { get; set; }
        }

        public class Address
        {
            public int idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public object refDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int? idProvincia { get; set; }
            public string idPais { get; set; }
            public object idUsuario { get; set; }
            public object latitudDireccion { get; set; }
            public object longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
            public object horarioDireccion { get; set; }
            public object idRuta { get; set; }
            public string fecModificacionDireccion { get; set; }
            public object jefeContactoDireccion { get; set; }
            public object fecSincronizacionDireccion { get; set; }
            public Pivot pivot { get; set; }
        }
    }
}
