﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csAccountingAccounts
    {
        public class Datum
        {
            public int idCuentaContable { get; set; }
            public int codCuentaContable { get; set; }
            public string nomCuentaContable { get; set; }
            public string numCuentaContable { get; set; }
            public string tipoCuentaContable { get; set; }
            public string partidaCuentaContable { get; set; }
            public string codExtCuentaContable { get; set; }
            public int? autorCuentaContable { get; set; }
            public string fecAltaCuentaContable { get; set; }
            public string fecModificacionCuentaContable { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
