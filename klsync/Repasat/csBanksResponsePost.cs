﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csBanksResponsePost
    {


        public class Resource
        {
            public string nomDatosBancarios { get; set; }
            public string titularCuentaDatosBancarios { get; set; }
            public string ibanCuentaDatosBancarios { get; set; }
            public string entidadCuentaDatosBancarios { get; set; }
            public string agenciaCuentaDatosBancarios { get; set; }
            public string dcCuentaDatosBancarios { get; set; }
            public string numCuentaDatosBancarios { get; set; }
            public string codExternoDatosBancarios { get; set; }
            public int idDatosBancarios { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
