﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csAccountingAccountsResponsePost
    {
        public class Resource
        {
            public string nomCuentaContable { get; set; }
            public string numCuentaContable { get; set; }
            public string codExtCuentaContable { get; set; }
            public int codCuentaContable { get; set; }
            public string fecModificacionCuentaContable { get; set; }
            public string fecAltaCuentaContable { get; set; }
            public int idCuentaContable { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }

    }
}
