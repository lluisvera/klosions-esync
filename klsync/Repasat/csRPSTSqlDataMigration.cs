﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace klsync.Repasat
{
    class csRPSTSqlDataMigration
    {
        SqlConnection dataconnection = new SqlConnection();

        public DataTable dtLoadDocsFromA3ToRPST(string objeto, bool ventas, string desde, string hasta, bool consulta = false, string syncDocumento = "TODOS", string estadoCobro = "TODOS", string codCuenta = null, bool forceUpdate=false, string serieA3=null, string numDoc=null, bool filtrarCarteraFechaFactura=false, bool updateEfectosPagados = false, bool procesoAuto = false, bool checkDoc=false)
        {
            try
            {
              
              
                string dateFilter = " WHERE FECHA >='" + desde + "' AND FECHA <='" + hasta + "'";
                string filterEstadoCobro = "";
                string filterSyncData = "";
                string filterSincronizar = "";
                string filterCuenta = "";
                string filterProcede = ventas ? " AND PROCEDE = 'FV'" : " AND PROCEDE = 'FC' ";
                bool sincronizar = consulta ? false : true;
                string filterPagCob = ventas ? " AND COBPAG = 'C' " : " AND COBPAG='P' ";
                string filterNumDoc = string.IsNullOrEmpty(numDoc) ? "" : " AND LTRIM(CARTERA.SERIE)='" + serieA3+ "' AND CARTERA.NUMDOC=" + numDoc + " ";
                string filterSerie = string.IsNullOrEmpty(serieA3) ? "" : " AND LTRIM(SERIE)='" + serieA3 + "' ";
                string filtroCampoFechaCartera = "";
                string filterCamposObligatoriosCabeceraV = "AND dbo.__CLIENTES.RPST_ID_CLI IS NOT NULL AND dbo.DOCUPAGO.RPST_ID_DOCPAGO IS NOT NULL AND dbo.FORMAPAG.RPST_ID_FORMAPAG IS NOT NULL ";

                //Modificamos para excluir Cartera de esta actualización de
                syncDocumento = consulta ? syncDocumento : "NO";

                csSqlConnects sql = new klsync.csSqlConnects();
                DataTable dtPedidos = new DataTable();
                DataTable dtCabeceras = new DataTable();
                DataTable dtLineas = new DataTable();
                Repasat.csRPSTSqlScripts sqlScript = new Repasat.csRPSTSqlScripts();

                string selectCab = "";
                string selectLin = "";

                if (objeto == "saleorders")
                {
                    filterSyncData = " AND (RPST_ID_PEDV IS NULL OR RPST_ID_PEDV=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_PEDV IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;

                    selectCab = sqlScript.obtenerCabeceraPed(ventas, dateFilter + filterSyncData + filterEstadoCobro + filterSerie);
                    selectLin = sqlScript.obtenerLineasPed(ventas, dateFilter.Replace(" WHERE ", " AND "));
                }
                if (objeto == "purchaseorders")
                {
                    filterSyncData = " AND (RPST_ID_PEDC IS NULL OR RPST_ID_PEDC=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_PEDC IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;

                    selectCab = sqlScript.obtenerCabeceraPed(ventas, dateFilter + filterSyncData + filterEstadoCobro + filterSerie);
                    selectLin = sqlScript.obtenerLineasPed(ventas, dateFilter.Replace(" WHERE "," AND "));
                }

                else if (objeto == "saledeliverynotes")
                {
                    filterSyncData = " AND (RPST_ID_ALBV IS NULL OR RPST_ID_ALBV=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_ALBV IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;

                    selectCab = sqlScript.obtenerCabeceraAlb(true, dateFilter + filterSyncData + filterEstadoCobro + filterSerie);
                    selectLin = sqlScript.obtenerLineasAlb(true);
                }
                else if (objeto == "saleinvoices")
                {
                    filterSyncData = " AND (RPST_ID_FACV IS NULL OR RPST_ID_FACV=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_FACV IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;
                    filterSerie = filterSerie.Replace("SERIE", "CABEFACV.SERIE");
                    filterNumDoc = string.IsNullOrEmpty(numDoc) ? "" : " AND CABEFACV.NUMDOC=" + numDoc + " ";
                    filterCamposObligatoriosCabeceraV = !consulta ? filterCamposObligatoriosCabeceraV : string.Empty;

                    selectCab = sqlScript.obtenerCabeceraFra(ventas, dateFilter + filterSyncData + filterEstadoCobro + filterSerie + filterNumDoc + filterCamposObligatoriosCabeceraV ,consulta);
                    selectLin = sqlScript.obtenerLineasFra(ventas, dateFilter + filterSyncData + filterEstadoCobro, consulta);
                }
                else if (objeto == "rates")
                {
                    filterSyncData = " AND (RPST_ID_FACV IS NULL OR RPST_ID_FACV=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_FACV IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;
                    filterSerie = filterSerie.Replace("SERIE", "CABEFACV.SERIE");
                    filterNumDoc = string.IsNullOrEmpty(numDoc) ? "" : " AND CABEFACV.NUMDOC=" + numDoc + " ";
                    filterCamposObligatoriosCabeceraV = !consulta ? filterCamposObligatoriosCabeceraV : string.Empty;

                    selectCab = sqlScript.obtenerCabeceraTarifaPrecios(ventas,null);
                    selectLin = sqlScript.obtenerLineasTarifaPrecios(ventas,null);
                }
                else if (objeto == "incomingpayments")
                {
                    filtroCampoFechaCartera = filtrarCarteraFechaFactura ? " CARTERA.FECHAFACTURA " : " CARTERA.FECHA ";
                    dateFilter = " WHERE " + filtroCampoFechaCartera + " >='" + desde + "' AND " + filtroCampoFechaCartera + " <='" + hasta + "'";

                    filterCuenta = !string.IsNullOrEmpty(codCuenta) && ventas ? " AND LTRIM(CARTERA.CODCLI)='" + codCuenta + "' " : "";
                    filterCuenta = !string.IsNullOrEmpty(codCuenta) && !ventas ? " AND LTRIM(CARTERA.CODPRO)='" + codCuenta + "' " : filterCuenta;
                    filterCuenta = string.IsNullOrEmpty(codCuenta) ? "" : filterCuenta;


                    //FILTRO SI ESTÁN SINCRONIZADOS
                    /* 26-01-2021 Deshabilitamos para poder actualizar el estado de cobro de los efectos en Repasat
                    filterSyncData = " AND (RPST_ID_CARTERA IS NULL OR RPST_ID_CARTERA=0)";
                    filterSyncData = syncDocumento == "SI" ? " AND (RPST_ID_CARTERA IS NOT NULL) " : filterSyncData;
                    filterSyncData = syncDocumento == "TODOS" ? "" : filterSyncData;
                    */


                    filterSincronizar = sincronizar ? " AND (CASE WHEN CARTERA.CODCLI IS NULL THEN  CABEFACC.RPST_ID_FACC ELSE CABEFACV.RPST_ID_FACV END) IS NOT NULL  AND RPST_ID_CARTERA IS NULL " : "";
                    filterSincronizar = forceUpdate ? "" : filterSincronizar;
                    filterProcede = forceUpdate ? "" : filterProcede;


                    dateFilter = forceUpdate && !string.IsNullOrEmpty(numDoc)? " WHERE CARTERA.FECHA >= '01-01-1900' AND CARTERA.FECHA <= '31-12-2099' " : dateFilter;

                    //FILTRO SI ESTÁN COBRADOS/PAGADOS
                    filterEstadoCobro = estadoCobro == "SI" ? " AND PAGADO='T' " : filterEstadoCobro;
                    filterEstadoCobro = estadoCobro == "NO" ? " AND PAGADO='F' " : filterEstadoCobro;
                    filterEstadoCobro = !consulta ? " AND PAGADO='T' " : filterEstadoCobro;
                    filterEstadoCobro = string.IsNullOrEmpty(numDoc) ? filterEstadoCobro : "";

                    filterEstadoCobro= updateEfectosPagados ? " AND PAGADO='T' " : filterEstadoCobro;

                    dateFilter = dateFilter + filterPagCob;
                    selectCab = sqlScript.obtenerCarteraEfectos(true, dateFilter + filterSyncData + filterEstadoCobro + filterSincronizar + filterCuenta + filterProcede + filterNumDoc);
                    //selectLin = sqlScript.obtenerLineasFra(true);
                }


                


                dtCabeceras = sql.obtenerDatosSQLScript(selectCab);
                if ((consulta && !checkDoc) || objeto == "incomingpayments" )
                {
                    return dtCabeceras;

                }
                dtLineas = sql.obtenerDatosSQLScript(selectLin);

                DataTable dtLineasCheck= sql.obtenerDatosSQLScript(selectLin.Replace("[lines[XXLIN][", "").Replace("]]",""));

                string tipoImpuesto = dtLineas.Columns[3].ColumnName;

                //VALIDACIÓN DE DATOS
                //Verifico si los campos "clave" como tipo de impuesto, formas de pago están sincronizados con Repasat
                //Si falta algún valor, no se realiza la sincronización

                DataView dvComprobacion = new DataView(dtLineasCheck);

                
                //dvComprobacion.RowFilter = "idTipoImpuesto=''";
                //int filasNoValidas = dvComprobacion.Count;




                if (dtCabeceras.Rows.Count > 0)
                {
                    if (!procesoAuto)
                    {
                        string action = checkDoc ? "verificar " : "traspasar ";
                    
                        if (MessageBox.Show("Se van a " + action + dtCabeceras.Rows.Count.ToString() + " documentos a Repasat. ¿Está seguro?", "Atencion", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            SyncDocsFromA3ToRPST(objeto, dtCabeceras, dtLineas,checkDoc);
                        }
                    }
                    else {
                        SyncDocsFromA3ToRPST(objeto, dtCabeceras, dtLineas);
                    }
                    
                }
                return dtCabeceras;
            }
            catch (Exception ex)
            {
                csUtilidades.log(ex.Message);
                return null;
            }

        }

        private void SyncDocsFromA3ToRPST(string objeto, DataTable dtCabeceras, DataTable dtLineas, bool checkDocument=false)
        {
            try
            {
                string numCabecera = "";
                string numLinea = "";
                string rowKey = "";
                string columnKey =  "IDDOC" ;
                string idDocCab = "";
                string idDocLin = "";
                string numLineaToRPST = "";
                DataSet datasetRPST = new DataSet();

                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();

                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");



                foreach (DataRow drCab in dtCabeceras.Rows)
                {
                    rowKey = drCab[columnKey].ToString();
                    idDocCab = rowKey;
                    DataRow drKeys = dtKeys.NewRow();
                    drKeys["KEY"] = rowKey;
                    dtKeys.Rows.Add(drKeys);

                    // Añado los campos de la cabecera
                    foreach (DataColumn dtcolCab in dtCabeceras.Columns)
                    {
                        if (string.IsNullOrEmpty(drCab[dtcolCab.ColumnName].ToString()))  //Si la columna está en blanco me lo salto
                        {
                            continue;
                        }
                        else
                        {
                            if (dtcolCab.ColumnName == "DOCPAG" || dtcolCab.ColumnName == "FORPAG" || dtcolCab.ColumnName == "CODCLI" || dtcolCab.ColumnName == "REGIVA" || dtcolCab.ColumnName == "IDDOC") // Si el nombre de la columna de sql es uno de estos, me lo salto
                            {
                                continue;
                            }
                            DataRow drParams = dtParams.NewRow();
                            drParams["FIELD"] = dtcolCab.ColumnName;
                            drParams["KEY"] = rowKey;
                            drParams["VALUE"] = drCab[dtcolCab.ColumnName];
                            dtParams.Rows.Add(drParams);
                        }
                    }

                    //Añado los campos de las lineas
                    foreach (DataRow drLin in dtLineas.Rows)
                    {
                        idDocLin = drLin["IDDOC"].ToString();

                        if (idDocCab == idDocLin)
                        {
                            //Guardo el número de linea para actualizar cada registro
                            numLineaToRPST = drLin["ORDLIN"].ToString();

                            // Añado los campos de las lineas
                            foreach (DataColumn dtcolLin in dtLineas.Columns)
                            {
                                if (string.IsNullOrEmpty(drLin[dtcolLin.ColumnName].ToString()))
                                {
                                    continue;
                                }
                                else
                                {
                                    if (dtcolLin.ColumnName == "NUMLINPED" || dtcolLin.ColumnName == "ORDLIN" || dtcolLin.ColumnName == "IDDOC")
                                    {
                                        continue;
                                    }
                                    DataRow drParams = dtParams.NewRow();
                                    drParams["FIELD"] = dtcolLin.ColumnName.Replace("XXLIN", numLineaToRPST);
                                    drParams["KEY"] = rowKey;
                                    drParams["VALUE"] = drLin[dtcolLin.ColumnName];
                                    dtParams.Rows.Add(drParams);
                                }

                            }
                        }
                    }
                }

                datasetRPST.Tables.Add(dtKeys);
                datasetRPST.Tables.Add(dtParams);
                //csRepasatWebService rpstWS = new csRepasatWebService();
                //rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "");

                bool esCliente = true;


                switch (objeto)
                {
                    case "purchaseinvoices":
                    case "purchaseorders":
                    case "purchasedeliverynote":
                        esCliente = true;
                        break;
                }



                csRepasatWebService rpstWS = new csRepasatWebService();
                // rpstWS.sincronizarObjetoRepasat("saleorders", "POST", dtsAccounts.Tables["dtKeys"], dtsAccounts.Tables["dtParams"], "", clientes);
                rpstWS.sincronizarObjetoRepasat(objeto, "POST", dtKeys, dtParams, "", esCliente, false, checkDocument ? "checkdocument" : "");


            }
            catch (Exception ex)
            {
            }
        }
    }
}
