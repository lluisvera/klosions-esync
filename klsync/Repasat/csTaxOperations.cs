﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csTaxOperations
    {
        public class Datum
        {
            public int idRegimenImpuesto { get; set; }
            public string nomRegimenImpuesto { get; set; }
            public int recargoEquivalenciaRegimenImpuesto { get; set; }
            public int igicRegimenImpuesto { get; set; }
            public int ivaRegimenImpuesto { get; set; }
            public int exentoRegimenImpuesto { get; set; }
            public int noSujetoRegimenImpuesto { get; set; }
            public int intracomunitarioRegimenImpuesto { get; set; }
            public string autorRegimenImpuesto { get; set; }
            public string fecAltaRegimenImpuesto { get; set; }
            public string codExternoRegimenesImpuestos { get; set; }
            public int contratoRepasatTarjetaRegimenImpuesto { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
