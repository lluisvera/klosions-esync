﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace klsync.Repasat
{
    public class csAccounts
    {
        Utilidades.csLogErrores logProcesos = new Utilidades.csLogErrores();
        public class Type
        {
            public int? idTipoCli { get; set; }
            public int? idEntidadTipoCli { get; set; }
            public int codTipoCli { get; set; }
            public string nomTipoCli { get; set; }
            public int? autorTipoCli { get; set; }
            public int activoTipoCli { get; set; }
            public string fecAltaTipoCli { get; set; }
        }

        public class Responsible
        {
            public int? idTrabajador { get; set; }
            public int? idResponsable { get; set; }
            public int codTrabajador { get; set; }
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string aliasTrabajador { get; set; }
            public object nifTrabajador { get; set; }
            public object idResponsableExt { get; set; }
            public string tipoRecursoTrabajador { get; set; }
            public string sexoTrabajador { get; set; }
            public int? fotoTrabajador { get; set; }
            public int? idDpto { get; set; }
            public int? idUsuario { get; set; }
            public object fecCadNif { get; set; }
            public object numSegSoc { get; set; }
            public string telf1 { get; set; }
            public string telf2 { get; set; }
            public string emailTrabajador { get; set; }
            public object fecNac { get; set; }
            public object idEstadoCivil { get; set; }
            public object numHijos { get; set; }
            public object carnetConducirTrabajador { get; set; }
            public string paisNac { get; set; }
            public int? idTarifa { get; set; }
            public object idTurnos { get; set; }
            public int? idCalendario { get; set; }
            public object fecAntig { get; set; }
            public int? categoriaTrabajador { get; set; }
            public double? bolsaHorasTrabajador { get; set; }
            public int activoTrabajador { get; set; }
            public string fecAltaTrabajador { get; set; }
            public object fecBajaTrabajador { get; set; }
            public string colorCalendarioTrabajador { get; set; }
            public string prefTelf { get; set; }
            public object gradoDiscapacidad { get; set; }
            public object email2Trabajador { get; set; }
            public object cuentaContableTrabajador { get; set; }
            public object rangoSueldoTrabajador { get; set; }
            public string codExternoTrabajador { get; set; }
            public string nomCompleto { get; set; }
        }

        public class addresses
        {
            public int? idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int? idProvincia { get; set; }
            public string idPais { get; set; }
            public int? idUsuario { get; set; }
            public double? latitudDireccion { get; set; }
            public double? longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
            public pivot pivot { get; set; }
        }

        public class Geozone
        {
            public int? idZonaGeo { get; set; }
            public int? idEntidadZonaGeo { get; set; }
            public int codZonaGeo { get; set; }
            public string nomZonaGeo { get; set; }
            public int? autorZonaGeo { get; set; }
            public int activoZonaGeo { get; set; }
            public string fecAltaZonaGeo { get; set; }
            public string codExternoZonaGeo { get; set; }
        }

        public class Campaign
        {
            public int? idCam { get; set; }
            public int? idEntidadCam { get; set; }
            public int codCam { get; set; }
            public string nomCam { get; set; }
            public int? autorCam { get; set; }
            public int activoCam { get; set; }
            public string fecAltaCam { get; set; }
        }

        public class AccountingAccount
        {
            public int idCuentaContable { get; set; }
            public int? codCuentaContable { get; set; }
            public string nomCuentaContable { get; set; }
            public string numCuentaContable { get; set; }
            public string tipoCuentaContable { get; set; }
            public string partidaCuentaContable { get; set; }
            public int? codExtCuentaContable { get; set; }
            public int? autorCuentaContable { get; set; }
            public string fecAltaCuentaContable { get; set; }
            public string fecModificacionCuentaContable { get; set; }
            public string fecSincronizacionCuentaContable { get; set; }
        }

        public class Route
        {
            public int idRuta { get; set; }
            public int idEntidadRuta { get; set; }
            public int codRuta { get; set; }
            public string nomRuta { get; set; }
            public int pernoctaRuta { get; set; }
            public object autorRuta { get; set; }
            public string fecAltaRuta { get; set; }
            public string codExternoRuta { get; set; }
        }

        public class Datum
        {
            public int? idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idRuta { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public bool sincronizarCuenta { get; set; }
            public object idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string fecModificacionCli { get; set; }
            public string idIdioma { get; set; }
            public string idCuentaContable { get; set; }
            public double? porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public string cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public int? idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public double? descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public string tipoCliPro { get; set; }
            public Type type { get; set; }
            public Responsible responsible { get; set; }
            public Geozone geozone { get; set; }
            public Campaign campaign { get; set; }
           // public addresses addresses { get; set; }
            public List<addresses> addresses { get; set; }
            public PaymentMethod payment_method { get; set; }
            public PaymentDocument payment_document { get; set; }
            public AccountingAccount accounting_account { get; set; }

            public Route route { get; set; }
            public List<Contact> contacts { get; set; }
            public int  clienteGenerico { get; set; }
            public List<ConexionExterna> conexion_externa { get; set; }
        }

        public class Contact
        {
            public int? idContacto { get; set; }
            public int codContacto { get; set; }
            public string nombreContacto { get; set; }
            public string apellidosContacto { get; set; }
            public string telf1Contacto { get; set; }
            public string extensionTelf1Contacto { get; set; }
            public string telf2Contacto { get; set; }
            public string extensionTelf2Contacto { get; set; }
            public string emailContacto { get; set; }
            public string email2Contacto { get; set; }
            public string cargoContacto { get; set; }
            public int? idUsuario { get; set; }
            public object fecNacContacto { get; set; }
            public string parejaContacto { get; set; }
            public string facebookContacto { get; set; }
            public string twitterContacto { get; set; }
            public string linkedinContacto { get; set; }
            public string skypeContacto { get; set; }
            public string comentsContacto { get; set; }
            public string fecAltaContacto { get; set; }
            public int? facturacionContacto { get; set; }
            public string codExternoContacto { get; set; }
            public string fecModificacionContacto { get; set; }
            public int? idUsuarioLogin { get; set; }
            public string nifContacto { get; set; }
            public string fecSincronizacionContacto { get; set; }
            public int correspondenciaEmailContacto { get; set; }
            public int correspondenciaTelfContacto { get; set; }
            public int activoContacto { get; set; }
            public string nombreCompleto { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

        public class pivot
        {
            public int? idCli { get; set; }
            public int? idDireccion { get; set; }
            public int? idEntidadDireccionCli { get; set; }
            public bool principaldireccion { get; set; }
            public bool facturacionDireccion { get; set; }
        }

        public class PaymentMethod
        {
            public int idFormaPago { get; set; }
            public string nomFormaPago { get; set; }
            public int codFormaPago { get; set; }
            public int activoFormaPago { get; set; }
            public int? autorFormaPago { get; set; }
            public string fecAltaFormaPago { get; set; }
            public int numVenFormaPago { get; set; }
            public int primerLapsoFormaPago { get; set; }
            public int siguientesLapsosFormaPago { get; set; }
            public int calculoVenFormaPago { get; set; }
            public object repartoProFormaPago { get; set; }
            public int pagadoFormaPago { get; set; }
            public int contratoRepasatTarjetaFormaPago { get; set; }
            public string codExternoFormaPago { get; set; }
        }

        public class PaymentDocument
        {
            public int? idDocuPago { get; set; }
            public string nomDocuPago { get; set; }
            public int? idDatosBancarios { get; set; }
            public string descDocuPago { get; set; }
            public int? codDocuPago { get; set; }
            public int? remesableDocuPago { get; set; }
            public int envioRecDocuPago { get; set; }
            public int efectivoDocuPago { get; set; }
            public string fecAltaDocuPago { get; set; }
            public int? autorDocuPago { get; set; }
            public int activoDocuPago { get; set; }
            public int contratoRepasatTarjetaDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
        }

        public class ConexionExterna
        {
            public int idCliExt { get; set; }
            public int idConExt { get; set; }
            public string tipoValor { get; set; }
            public string idExterno { get; set; }
            public int idRepasat { get; set; }
            public int? idUsuario { get; set; }
            public int? idUsuarioUpdate { get; set; }
            public string created_at { get; set; }
            public string updated_at { get; set; }
            public int? activoInnuva { get; set; }
        }



        public DataSet cargarCuentasA3ToRepasat(bool clientes, bool update=false, string accountsToUpdate=null, bool cronjob = false, string dateFrom=null)
        {
            try
            {
                string rowKey = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();

                string selectCuentas = clientes ? sqlScript.selectClientesFromA3ToRepasat(update, dateFrom, accountsToUpdate,cronjob) : sqlScript.selectProveedoresFromA3ToRepasat(update, dateFrom, accountsToUpdate,cronjob);
                dt = sqlConnect.obtenerDatosSQLScript(selectCuentas);
                Objetos.csTercero cliente = new Objetos.csTercero();

                if (dt.Rows.Count > 0)
                {
                    if (csGlobal.modoManual) {
                        logProcesos.guardarLogProceso("Inicio Proceso csAccounts funcion cargarCuentasA3ToRepasat");
                        logProcesos.guardarLogProceso("Se van a procesar " + dt.Rows.Count.ToString() + " filas");
                    }

                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (update)
                        {
                            rowKey = clientes? dr["RPST_ID_CLI"].ToString(): dr["RPST_ID_PROV"].ToString();
                        }
                        else
                        {
                            rowKey = dr["codExternoCli"].ToString();
                        }
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {

                            //if (dtcol.ColumnName == "idRuta")
                            //{
                            //    string stop = "";
                            //}
                            if (string.IsNullOrEmpty(dr[dtcol.ColumnName].ToString()))
                            {
                                continue;
                            }
                            else
                            {
                                if (dtcol.ColumnName == "DOCPAG" || dtcol.ColumnName == "FORPAG")
                                {
                                    continue;
                                }
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcol.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = dr[dtcol.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);
                    csRepasatWebService rpstWS = new csRepasatWebService();
                 //   rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "",clientes);

                    return datasetRPST;
                }
                else
                {
                    return null;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
