﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csPaymentMethods
    {
        public class Datum
        {
            public int idFormaPago { get; set; }
            public int idEntidadFormaPago { get; set; }
            public string nomFormaPago { get; set; }
            public int codFormaPago { get; set; }
            public int activoFormaPago { get; set; }
            public int? autorFormaPago { get; set; }
            public string fecAltaFormaPago { get; set; }
            public int numVenFormaPago { get; set; }
            public int primerLapsoFormaPago { get; set; }
            public int siguientesLapsosFormaPago { get; set; }
            public int calculoVenFormaPago { get; set; }
            public object repartoProFormaPago { get; set; }
            public int pagadoFormaPago { get; set; }
            public int contratoRepasatTarjetaFormaPago { get; set; }
            public string codExternoFormaPago { get; set; }
            public object fecSincronizacionFormaPago { get; set; }
            public List<AuxiliarExterno> auxiliar_externo { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

        public class AuxiliarExterno
        {
            public int idConExt { get; set; }
            public int idUsuario { get; set; }
            public object idUsuarioUpdate { get; set; }
            public string created_at { get; set; }
            public object updated_at { get; set; }
            public string tipoConexion { get; set; }
            public string nomConexion { get; set; }
            public object observacionesConexion { get; set; }
            public Pivot pivot { get; set; }
        }

        public class Pivot
        {
            public int idRepasat { get; set; }
            public int idConExt { get; set; }
            public int idEntidadAuxiliaresConExterna { get; set; }
            public string tipoValor { get; set; }
            public string idExterno { get; set; }
        }

    }
}
