﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csSaleInvoiceCheck
    {
        public class Data
        {
            public Resource resource { get; set; }
        }

        public class Resource
        {
            public string CheckDocument { get; set; }
            public string CheckDocumentInfo { get; set; }
            public string Cuenta { get; set; }
            public string SerieDocumento { get; set; }
            public string Direccion { get; set; }
            public int? idDireccion { get; set; }
            public string Docupago { get; set; }
            public string Formapago { get; set; }
            public string RegImpuestos { get; set; }
            public string documentoSubido { get; set; }
         
            public int? idDocumento { get; set; }
            public int? codExternoDocumento { get; set; }
            
            public int? idDocA3ERP { get; set; }
            public string CuentaA3 { get; set; }
            
        }

        public class Root
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }

    }
}
