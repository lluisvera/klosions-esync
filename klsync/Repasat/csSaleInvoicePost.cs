﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csSaleInvoicePost
    {
        public class Series
        {
            public int?  idSerie { get; set; }
            public string nomSerie { get; set; }
            public int contSerie { get; set; }
            public int contFacturasSerie { get; set; }
            public int contAbonosSerie { get; set; }
            public int contFacturasCompraSerie { get; set; }
            public int contAbonosCompraSerie { get; set; }
            public object contPedidosSerie { get; set; }
            public object contRecibosSerie { get; set; }
            public int serieAplicacionSerie { get; set; }
            public int? autorSerie { get; set; }
            public int contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public int activoSerie { get; set; }
            public int contPedidosVentaSerie { get; set; }
            public int contPedidosCompraSerie { get; set; }
            public int contAlbaranesVentaSerie { get; set; }
            public int contAlbaranesCompraSerie { get; set; }
            public int contAlbaranesRegularizacionSerie { get; set; }
            public int contAnticiposSerie { get; set; }
            public int contTareasSerie { get; set; }
            public string codExternoSerie { get; set; }
        }

        public class Taxtype
        {
            public int?  idTipoImpuesto { get; set; }
            public string codTipoImpuesto { get; set; }
            public string nomTipoImpuesto { get; set; }
            public int impuestosTipoImpuesto { get; set; }
            public double recargoEquivalenciaTipoImpuesto { get; set; }
            public string idPais { get; set; }
            public int activoTipoImpuesto { get; set; }
            public object autorTipoImpuesto { get; set; }
            public string fecAltaTipoImpuesto { get; set; }
            public string codExternoTipoImpuesto { get; set; }
        }

        public class Line
        {
            public int?  idLineaDocumento { get; set; }
            public int?  idDocumento { get; set; }
            public int?  idArticulo { get; set; }
            public object capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public object descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int?  idTipoImpuesto { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object idDocumentoOrigen { get; set; }
            public object idLineaPresupuesto { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public object idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object idActividad { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
            public int tieneNumSerieArticuloLineaDocumento { get; set; }
            public string descuento2LineaDocumento { get; set; }
            public string costeLineaDocumento { get; set; }
            public string descuentoCosteLineaDocumento { get; set; }
            public string totalCosteLineaDocumento { get; set; }
            public string observacionesLineaDocumento { get; set; }
            public string margenComercialLineaDocumeto { get; set; }
            public Taxtype taxtype { get; set; }
        }

        public class Account
        {
            public int?  idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public object descCli { get; set; }
            public string tipoCli { get; set; }
            public object idZonaGeo { get; set; }
            public object idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public object tel2 { get; set; }
            public object fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public object idEmpresaClienteAplicacionCli { get; set; }
            public object idTipoCli { get; set; }
            public int activoCli { get; set; }
            public object idTrabajador { get; set; }
            public int?  idDocuPago { get; set; }
            public int?  idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public int? diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public string porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int?  idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public object idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public object descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int?  idRuta { get; set; }
            public object fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public object idTarifaDescuentos { get; set; }
            public object idSector { get; set; }
            public object idMotivoBaja { get; set; }
        }

        public class PaymentMethod
        {
            public int?  idFormaPago { get; set; }
            public string nomFormaPago { get; set; }
            public int codFormaPago { get; set; }
            public int activoFormaPago { get; set; }
            public object autorFormaPago { get; set; }
            public string fecAltaFormaPago { get; set; }
            public int numVenFormaPago { get; set; }
            public int primerLapsoFormaPago { get; set; }
            public int siguientesLapsosFormaPago { get; set; }
            public int calculoVenFormaPago { get; set; }
            public object repartoProFormaPago { get; set; }
            public int pagadoFormaPago { get; set; }
            public int contratoRepasatTarjetaFormaPago { get; set; }
            public string codExternoFormaPago { get; set; }
        }

        public class PaymentDocument
        {
            public int?  idDocuPago { get; set; }
            public string nomDocuPago { get; set; }
            public object idDatosBancarios { get; set; }
            public object descDocuPago { get; set; }
            public int codDocuPago { get; set; }
            public int remesableDocuPago { get; set; }
            public int envioRecDocuPago { get; set; }
            public int efectivoDocuPago { get; set; }
            public string fecAltaDocuPago { get; set; }
            public object autorDocuPago { get; set; }
            public int activoDocuPago { get; set; }
            public int contratoRepasatTarjetaDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
        }

        public class BillingAddress
        {
            public int?  idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public object refDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int?  idProvincia { get; set; }
            public string idPais { get; set; }
            public object idUsuario { get; set; }
            public object latitudDireccion { get; set; }
            public object longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
            public object horarioDireccion { get; set; }
            public object idRuta { get; set; }
            public string fecModificacionDireccion { get; set; }
        }

        public class Taxoperation
        {
            public int?  idRegimenImpuesto { get; set; }
            public string codRegimenImpuesto { get; set; }
            public string nomRegimenImpuesto { get; set; }
            public string tipoRegimenImpuesto { get; set; }
            public int recargoEquivalenciaRegimenImpuesto { get; set; }
            public int igicRegimenImpuesto { get; set; }
            public int ivaRegimenImpuesto { get; set; }
            public int exentoRegimenImpuesto { get; set; }
            public int noSujetoRegimenImpuesto { get; set; }
            public int intracomunitarioRegimenImpuesto { get; set; }
            public object autorRegimenImpuesto { get; set; }
            public string fecAltaRegimenImpuesto { get; set; }
            public string codExternoRegimenesImpuestos { get; set; }
            public int contratoRepasatTarjetaRegimenImpuesto { get; set; }
            public string mostrarNombre { get; set; }
        }

        public class Resource
        {
            public int?  idDocumento { get; set; }
            public string tipoDocumento { get; set; }
            public int?  idCli { get; set; }
            public object idProyecto { get; set; }
            public object idDireccionEnvio { get; set; }
            public int?  idDireccionFacturacion { get; set; }
            public int?  idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public object refDocumento { get; set; }
            public int?  idFormaPago { get; set; }
            public object idCam { get; set; }
            public double totalBaseImponibleDocumento { get; set; }
            public double totalImpuestosDocumento { get; set; }
            public double totalDocumento { get; set; }
            public object totalPagadoDocumento { get; set; }
            public int anticipoDocumento { get; set; }
            public int porcentajeRetencionDocumento { get; set; }
            public int totalRetencionDocumento { get; set; }
            public object domiciliacionBancariaDocumento { get; set; }
            public object idContratoCli { get; set; }
            public object mesContratoCliDocumento { get; set; }
            public object anoContratoCliDocumento { get; set; }
            public string fecDocumento { get; set; }
            public object fecContableDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public object idTrabajador { get; set; }
            public object idUsuario { get; set; }
            public int?  idDocuPago { get; set; }
            public double portesDocumento { get; set; }
            public int? idTipoImpuestoPortes { get; set; }
            public double totalImpuestosPortesDocumento { get; set; }
            public double totalPortesDocumento { get; set; }
            public double porcentajeDescuentoDocumento { get; set; }
            public string codExternoDocumento { get; set; }
            public int? idTarifa { get; set; }
            public int? idAlmacen { get; set; }
            public int?  idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public string totalRecargoEquivalenciaDocumento { get; set; }
            public string totalRetencionesDocumento { get; set; }
            public string totalBaseRetencionesDocumento { get; set; }
            public string totalRecargoEquivalenciaPortesDocumento { get; set; }
            public string fecEnvioMailDocumento { get; set; }
            public int? idRepresentado { get; set; }
            public string observacionesCabeceraDocumento { get; set; }
            public string observacionesPieDocumento { get; set; }
            public int? idTipoImpuesto { get; set; }
            public int? idTransportista { get; set; }
            public int? idTarifaDescuentos { get; set; }
            public double totalCosteDocumento { get; set; }
            public string margenComercialDocumento { get; set; }
            public int? idContacto { get; set; }
            public string origenDocumento { get; set; }
            public Series series { get; set; }
            public List<Line> lines { get; set; }
            public Account account { get; set; }
            public PaymentMethod payment_method { get; set; }
            public PaymentDocument payment_document { get; set; }
            public object delivery_address { get; set; }
            public BillingAddress billing_address { get; set; }
            public object carrier { get; set; }
            public Taxoperation taxoperation { get; set; }
            public object employee { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }



    }
}
