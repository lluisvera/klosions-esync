﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csTaxTypes
    {
        public class Datum
        {
            public int idTipoImpuesto { get; set; }
            public string codTipoImpuesto { get; set; }
            public string nomTipoImpuesto { get; set; }
            public double impuestosTipoImpuesto { get; set; }
            public double recargoEquivalenciaTipoImpuesto { get; set; }
            public string idPais { get; set; }
            public int activoTipoImpuesto { get; set; }
            public object autorTipoImpuesto { get; set; }
            public string fecAltaTipoImpuesto { get; set; }
            public string codExternoTipoImpuesto { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
