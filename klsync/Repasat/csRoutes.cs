﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csRoutes
    {
        public class Datum
        {
            public int idRuta { get; set; }
            public int idEntidadRuta { get; set; }
            public int codRuta { get; set; }
            public string nomRuta { get; set; }
            public int pernoctaRuta { get; set; }
            public object autorRuta { get; set; }
            public string fecAltaRuta { get; set; }
            public string codExternoRuta { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }


    }
}
