﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csPaymentDocsResponsePost
    {


        public class Resource
        {
            public string nomDocuPago { get; set; }
            public string activoDocuPago { get; set; }
            public string remesableDocuPago { get; set; }
            public string envioRecDocuPago { get; set; }
            public string efectivoDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
            public int codDocuPago { get; set; }
            public int idDocuPago { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
