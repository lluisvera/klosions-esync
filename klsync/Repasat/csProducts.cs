﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace klsync.Repasat
{
    class csProducts
    {
        public class Datum
        {
            public int idArticulo { get; set; }
            public int codArticulo { get; set; }
            public string nomArticulo { get; set; }
            public string refArticulo { get; set; }
            public string descArticulo { get; set; }
            public int? idTipoArticulo { get; set; }
            public int? fotoArticulo { get; set; }
            public double precioVentaArticulo { get; set; }
            public double precioCompraArticulo { get; set; }
            public int cantidadArticulo { get; set; }
            public int activoArticulo { get; set; }
            public string codExternoArticulo { get; set; }
            public string fecAltaArticulo { get; set; }
            public object fecBajaArticulo { get; set; }
            public object idFabricante { get; set; }
            public int? idFam { get; set; }
            public int? idSubFam { get; set; }
            public int? idUni { get; set; }
            public object idArtAlt { get; set; }
            public int? idResp { get; set; }
            public int? bloqueadoArticulo { get; set; }
            public string motivoArticulo { get; set; }
            public int? compraArticulo { get; set; }
            public int? ventaArticulo { get; set; }
            public int? stockArticulo { get; set; }
            public int? planificableArticulo { get; set; }
            public object pesoArticulo { get; set; }
            public object volumenArticulo { get; set; }
            public object descuentoMaxArticulo { get; set; }
            public object codigobarrasArticulo { get; set; }
            public string preciocostecompraArticulo { get; set; }
            public object fechaentregacompraArticulo { get; set; }
            public object fechaentregaventaArticulo { get; set; }
            public string alarmapresucompraArticulo { get; set; }
            public string alarmapresuventaArticulo { get; set; }
            public object alarmapedidoscompraArticulo { get; set; }
            public string alarmapedidosventaArticulo { get; set; }
            public string alarmaalbaranescompraArticulo { get; set; }
            public string alarmaalbaranesventaArticulo { get; set; }
            public string alarmafacturascompraArticulo { get; set; }
            public string alarmafacturasventaArticulo { get; set; }
            public object largoArticulo { get; set; }
            public object anchoArticulo { get; set; }
            public object gruesoArticulo { get; set; }
            public object embalajeArticulo { get; set; }
            public string referenciaProveedorArticulo { get; set; }
            public string aliasArticulo { get; set; }
            public int? idTipoImpuesto { get; set; }
            public object cuentaContableCompraArticulo { get; set; }
            public object cuentaContableVentaArticulo { get; set; }
            public int? idTipoImpuestoCompra { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }




        public DataSet uploadProductosA3ToRepasat(bool update = false, string fechaActualizacion=null, DataTable products=null)
        {
            try
            {
                string rowKey = "";
                string value = "";
                DataSet datasetRPST = new DataSet();
                DataTable dt = new DataTable();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sqlConnect = new csSqlConnects();

                //dt = sqlConnect.obtenerDatosSQLScript(sqlScript.selectProductosFromA3ToRepasat(update,fechaActualizacion));
                dt = products;

                //Si no devuelve datos devolvemos Null y nos ahorramos pasos
                if (!dt.hasRows())
                {
                    return null;
                }

                Objetos.csArticulo productos = new Objetos.csArticulo();


                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");

                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        rowKey = update ? dr["idArticulo"].ToString() : dr["codExternoArticulo"].ToString();
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        foreach (DataColumn dtcol in dt.Columns)
                        {
                            if (dtcol.ColumnName == "TABLA" || dtcol.ColumnName == "MOVIMIENTO" || dtcol.ColumnName == "FECHA" || dtcol.ColumnName == "IDREG1")
                            {
                                continue;
                            }
                            DataRow drParams = dtParams.NewRow();
                            dtcol.ColumnName = (dtcol.ColumnName == "precioV") ? "precioVentaArticulo" : dtcol.ColumnName;
                            dtcol.ColumnName = (dtcol.ColumnName == "precioC") ? "precioCompraArticulo" : dtcol.ColumnName;
                            drParams["FIELD"] = dtcol.ColumnName;
                            drParams["KEY"] = rowKey;
                            value= dr[dtcol.ColumnName].ToString();
                            drParams["VALUE"] = dtcol.ColumnName.Contains("precio") ? value.Replace(",", ".") : value;
                            dtParams.Rows.Add(drParams);

                        }
                    }

                    datasetRPST.Tables.Add(dtKeys);
                    datasetRPST.Tables.Add(dtParams);
                    csRepasatWebService rpstWS = new csRepasatWebService();
                   // rpstWS.sincronizarObjetoRepasat("products", "POST", dtKeys, dtParams, "");
                }

                return datasetRPST;
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
