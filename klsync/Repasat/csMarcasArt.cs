﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csMarcasArt
    {
        public class Datum
        {
            public string uuidMarca { get; set; }
            public int codMarca { get; set; }
            public string nomMarca { get; set; }
            public string urlMarca { get; set; }
            public int? imgMarca { get; set; }
            public int? activoMarca { get; set; }
            public string fechaAltaMarca { get; set; }
            public int? idUsuarioMarca { get; set; }
            public string codExternoMarca { get; set; }

        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
