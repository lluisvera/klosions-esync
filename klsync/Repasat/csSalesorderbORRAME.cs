﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csSalesorderbORRAME
    {

        public class Series
        {
            public int idSerie { get; set; }
            public int idEntidadSerie { get; set; }
            public string nomSerie { get; set; }
            public int contSerie { get; set; }
            public int contFacturasSerie { get; set; }
            public int contAbonosSerie { get; set; }
            public int contFacturasCompraSerie { get; set; }
            public int contAbonosCompraSerie { get; set; }
            public object contPedidosSerie { get; set; }
            public object contRecibosSerie { get; set; }
            public int serieAplicacionSerie { get; set; }
            public int? autorSerie { get; set; }
            public int contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public int activoSerie { get; set; }
            public int contPedidosVentaSerie { get; set; }
            public int contPedidosCompraSerie { get; set; }
            public int contAlbaranesVentaSerie { get; set; }
            public int contAlbaranesCompraSerie { get; set; }
            public int contAlbaranesRegularizacionSerie { get; set; }
            public int contAnticiposSerie { get; set; }
            public int contTareasSerie { get; set; }
        }

        public class Line
        {
            public int idLineaDocumento { get; set; }
            public int idEntidadLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int idArticulo { get; set; }
            public string capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public string descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object fecEntregaLineaDocumento { get; set; }
            public string unidadesServidasArticuloLineaDocumento { get; set; }
            public string unidadesAnuladasArticuloLineaDocumento { get; set; }
            public string unidadesPendientesArticuloLineaDocumento { get; set; }
            public object idLineaPresupuesto { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public object idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
        }

        public class Account
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public object aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public object idZonaGeo { get; set; }
            public object idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public object idEmpresaClienteAplicacionCli { get; set; }
            public int idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int idTrabajador { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public object diaPago1Cli { get; set; }
            public object diaPago2Cli { get; set; }
            public object diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public int porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public int idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public object descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
        }

        public class Resource
        {
            public int idDocumento { get; set; }
            public int idEntidadDocumento { get; set; }
            public int idCli { get; set; }
            public object idCliPot { get; set; }
            public object idProyecto { get; set; }
            public int idDireccionEnvio { get; set; }
            public int idDireccionFacturacion { get; set; }
            public int idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public string refDocumento { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public object idCam { get; set; }
            public double totalBaseImponibleDocumento { get; set; }
            public double totalImpuestosDocumento { get; set; }
            public double totalDocumento { get; set; }
            public object totalPagadoDocumento { get; set; }
            public object domiciliacionBancariaDocumento { get; set; }
            public int idTrabajador { get; set; }
            public string fecDocumento { get; set; }
            public object fecCierreDocumento { get; set; }
            public object fecCaducidadDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public int idUsuario { get; set; }
            public object fecEntregaDocumento { get; set; }
            public int portesDocumento { get; set; }
            public int idTipoImpuestoPortes { get; set; }
            public double totalImpuestosPortesDocumento { get; set; }
            public double totalPortesDocumento { get; set; }
            public object idTransportista { get; set; }
            public string servidoDocumento { get; set; }
            public int porcentajeDescuentoDocumento { get; set; }
            public object idTarifa { get; set; }
            public object idAlmacen { get; set; }
            public int idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public string totalRecargoEquivalenciaDocumento { get; set; }
            public string totalRetencionesDocumento { get; set; }
            public string totalBaseRetencionesDocumento { get; set; }
            public string totalRecargoEquivalenciaPortesDocumento { get; set; }
            public object idRepresentado { get; set; }
            public string observacionesCabeceraDocumento { get; set; }
            public string observacionesPieDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public string tipoGestion { get; set; }
            public int codExternoDocumento { get; set; }
            public Series series { get; set; }
            public List<Line> lines { get; set; }
            public Account account { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
