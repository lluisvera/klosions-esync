﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csRetentionTypes
    {
        public class Datum
        {
            public int idTipoRetencion { get; set; }
            public int idEntidadTipoRetencion { get; set; }
            public string nomTipoRetencion { get; set; }
            public string codTipoRetencion { get; set; }
            public string idPais { get; set; }
            public int? impuestosTipoRetencion { get; set; }
            public int? activoTipoRetencion { get; set; }
            public string ventaCompraTipoRetencion { get; set; }
            public string tipoTipoRetencion { get; set; }
            public string fecAltaTipoRetencion { get; set; }
            public string fecModificacionTipoRetencion { get; set; }
            public object country { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
