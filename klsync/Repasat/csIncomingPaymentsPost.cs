﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csIncomingPaymentsPost
    {
        public class Resource
        {
            public int idCarteraCobros { get; set; }
            public int idDocumento { get; set; }
            public int idCli { get; set; }
            public int idEntidadCarteraCobros { get; set; }
            public int numVencimientoCarteraCobros { get; set; }
            public string fecVencimientoCarteraCobros { get; set; }
            public string fecCobroCarteraCobros { get; set; }
            public object gastosCobroCarteraCobros { get; set; }
            public string procedenciaCarteraCobros { get; set; }
            public object numGrupoCarteraCobros { get; set; }
            public double importeCarteraCobros { get; set; }
            public string importeCobradoCarteraCobros { get; set; }
            public int idDocuPago { get; set; }
            public string idDatosBancarios { get; set; }
            public int impagadoCarteraCobros { get; set; }
            public object gastosImpagadoCarteraCobros { get; set; }
            public object numeroRemesaCarteraCobros { get; set; }
            public string estadoCarteraCobros { get; set; }
            public int situacionCarteraCobros { get; set; }
            public string fecAltaCarteraCobros { get; set; }
            public object refSerie { get; set; }
            public object idRepresentado { get; set; }
            public object despesasImpagado { get; set; }
            public object importeCobroOriginal { get; set; }
            public object vencimientoOriginal { get; set; }
            public object idDocuImpagado { get; set; }
            public int anticipoCarteraCobros { get; set; }
            public object idSerieAnticipo { get; set; }
            public string referenciaAnticipo { get; set; }
            public object numDocumentoAnticipo { get; set; }
            public int estadoAnticipo { get; set; }
            public object numAgrupacionCarteraCobros { get; set; }
            public int padreAgrupacionCarteraCobros { get; set; }
            public string codExternoCarteraCobros { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
