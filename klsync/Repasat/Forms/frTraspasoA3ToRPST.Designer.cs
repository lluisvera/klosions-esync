﻿namespace klsync.Repasat.Forms
{
    partial class frTraspasoA3ToRPST
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnAddAsiento = new System.Windows.Forms.Button();
            this.btnDeleteRows = new System.Windows.Forms.Button();
            this.lblSkipLines = new System.Windows.Forms.Label();
            this.tbSkipLines = new System.Windows.Forms.TextBox();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.btnAddAccount = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 572);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1164, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnAddAccount);
            this.splitContainer1.Panel1.Controls.Add(this.btnAddAsiento);
            this.splitContainer1.Panel1.Controls.Add(this.btnDeleteRows);
            this.splitContainer1.Panel1.Controls.Add(this.lblSkipLines);
            this.splitContainer1.Panel1.Controls.Add(this.tbSkipLines);
            this.splitContainer1.Panel1.Controls.Add(this.btnLoadFile);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvDatos);
            this.splitContainer1.Size = new System.Drawing.Size(1164, 572);
            this.splitContainer1.SplitterDistance = 269;
            this.splitContainer1.TabIndex = 1;
            // 
            // btnAddAsiento
            // 
            this.btnAddAsiento.BackgroundImage = global::klsync.adMan.Resources.verified9;
            this.btnAddAsiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddAsiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAsiento.Location = new System.Drawing.Point(22, 224);
            this.btnAddAsiento.Name = "btnAddAsiento";
            this.btnAddAsiento.Size = new System.Drawing.Size(164, 42);
            this.btnAddAsiento.TabIndex = 4;
            this.btnAddAsiento.Text = "Crear Asientos";
            this.btnAddAsiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddAsiento.UseVisualStyleBackColor = true;
            this.btnAddAsiento.Click += new System.EventHandler(this.btnAddAsiento_Click);
            // 
            // btnDeleteRows
            // 
            this.btnDeleteRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteRows.Location = new System.Drawing.Point(152, 175);
            this.btnDeleteRows.Name = "btnDeleteRows";
            this.btnDeleteRows.Size = new System.Drawing.Size(75, 28);
            this.btnDeleteRows.TabIndex = 3;
            this.btnDeleteRows.Text = "Borrar";
            this.btnDeleteRows.UseVisualStyleBackColor = true;
            this.btnDeleteRows.Click += new System.EventHandler(this.btnDeleteRows_Click);
            // 
            // lblSkipLines
            // 
            this.lblSkipLines.AutoSize = true;
            this.lblSkipLines.Location = new System.Drawing.Point(19, 180);
            this.lblSkipLines.Name = "lblSkipLines";
            this.lblSkipLines.Size = new System.Drawing.Size(68, 13);
            this.lblSkipLines.TabIndex = 2;
            this.lblSkipLines.Text = "Saltar Lineas";
            // 
            // tbSkipLines
            // 
            this.tbSkipLines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSkipLines.Location = new System.Drawing.Point(93, 177);
            this.tbSkipLines.Name = "tbSkipLines";
            this.tbSkipLines.Size = new System.Drawing.Size(53, 26);
            this.tbSkipLines.TabIndex = 1;
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.BackgroundImage = global::klsync.Properties.Resources.excel;
            this.btnLoadFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLoadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadFile.Location = new System.Drawing.Point(22, 113);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(164, 41);
            this.btnLoadFile.TabIndex = 0;
            this.btnLoadFile.Text = "Cargar Excel";
            this.btnLoadFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoadFile.UseVisualStyleBackColor = true;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // dgvDatos
            // 
            this.dgvDatos.AllowUserToAddRows = false;
            this.dgvDatos.AllowUserToDeleteRows = false;
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDatos.Location = new System.Drawing.Point(0, 0);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.ReadOnly = true;
            this.dgvDatos.Size = new System.Drawing.Size(891, 572);
            this.dgvDatos.TabIndex = 0;
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.BackgroundImage = global::klsync.adMan.Resources.verified9;
            this.btnAddAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAccount.Location = new System.Drawing.Point(22, 285);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(164, 51);
            this.btnAddAccount.TabIndex = 5;
            this.btnAddAccount.Text = "Crear Cuentas Contables";
            this.btnAddAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddAccount.UseVisualStyleBackColor = true;
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click);
            // 
            // frTraspasoA3ToRPST
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 594);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "frTraspasoA3ToRPST";
            this.Text = "frTraspasoA3ToRPST";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.Label lblSkipLines;
        private System.Windows.Forms.TextBox tbSkipLines;
        private System.Windows.Forms.Button btnDeleteRows;
        private System.Windows.Forms.Button btnAddAsiento;
        private System.Windows.Forms.Button btnAddAccount;
    }
}