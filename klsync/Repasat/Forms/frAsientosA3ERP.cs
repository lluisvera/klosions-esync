﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Repasat.Forms
{
    public partial class frAsientosA3ERP : Form
    {
        private static frAsientosA3ERP m_FormDefInstance;
        public static frAsientosA3ERP DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frAsientosA3ERP();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        

        csSqlConnects sql = new csSqlConnects();
        DataTable dtListaCentrosCoste = new DataTable();
        

        public frAsientosA3ERP()
        {
            InitializeComponent();
        }

        private void frAsientosA3ERP_Load(object sender, EventArgs e)
        {
            cargarDGV();
            cargarCentrosCoste();
            dtListaCentrosCoste = sql.obtenerDatosSQLScript("SELECT CENTROCOSTE, CONCAT('(',LTRIM(CENTROCOSTE),') ',DESCCENTRO) AS DESCRIP FROM CENTROSC WHERE OBSOLETO='F'");
        }

        private void cargarDGV(DataTable dtFras=null) {

            try {

                string tipoCarga = rbutFullAsiento.Checked ? "": " AND LEFT(CUENTAS.CUENTA,1) IN (6,7) " ;
                string fechaFrom = dtpFromInvoicesV.Value.ToShortDateString();
                string fechaTo = dtpToInvoicesV.Value.ToShortDateString();

                string scriptCargaAsientos = "SELECT FECHA, CAST( NUMASIENTO AS INT) AS NUMASIENTO, CAST (IDAPUNTE AS INT) AS IDAPUNTE, NUMLIN, CUENTA, " +
                    " CUENTAS.DESCCUE, DEBE, HABER, CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, PROCEDE,  CAST( PROCEDEID AS INT) AS PROCEDEID " +
                    " FROM __ASIENTOS INNER JOIN CUENTAS ON CUENTAS.IDCUENTA = __ASIENTOS.IDCUENTA " +
                    " WHERE FECHA>= '" + fechaFrom + "' AND  FECHA<= '" + fechaTo + "' AND PROCEDE IN ('FC','FV') " + tipoCarga + 
                    " ORDER BY FECHA,NUMASIENTO,NUMLIN";

                DataTable dtAsientos = sql.obtenerDatosSQLScript(scriptCargaAsientos);
                dgvAsientos.DataSource = dtAsientos;

                this.dgvAsientos.Columns["DEBE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvAsientos.Columns["HABER"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvAsientos.AutoResizeColumns();


            }
            catch (Exception ex) {

            }
        }


        private void cargarDoc(string partida, string idDocumento)
        {
            try
            {

                string tabla = (partida == "FC") ? " CABEFACC." : " CABEFACV.";
                string documento = (partida == "FC") ? " CABEFACC.IDFACC =" + idDocumento : " CABEFACV.IDFACV =" + idDocumento; 

                string scriptCargaDocsC  = " SELECT CAST(CABEFACC.IDFACC AS INT) AS IDFACC, CAST(IDLIN AS INT) AS IDLIN,FORMAT (CABEFACC.FECHA, 'dd/MM/yyyy ') AS FECHA, SERIE, CAST(NUMDOC AS BIGINT) AS NUMDOC, " +
                        " CABEFACC.CODPRO, NOMPRO, LINEFACT.CODART, DESCLIN, " +
                        " UNIDADES, PRECIO,LINEFACT.BASE, LINEFACT.TIPIVA, LINEFACT.CTACONL, " +
                        " LINEFACT.CENTROCOSTE AS CC1DOC,ARTICULO.CENTROCOSTEC AS CC1ART, " +
                        " LINEFACT.CENTROCOSTE2 AS CC2DOC,ARTICULO.CENTROCOSTEC2 AS CC2ART, " +
                        " LINEFACT.CENTROCOSTE3 AS CC3DOC,ARTICULO.CENTROCOSTEC3 AS CC3ART " +
                        " FROM CABEFACC INNER JOIN LINEFACT ON CABEFACC.IDFACC = LINEFACT.IDFACC " +
                        " LEFT JOIN ARTICULO ON LINEFACT.CODART = ARTICULO.CODART " +
                        " WHERE CABEFACC.IDFACC = " + idDocumento;

                string scriptCargaDocsV = " SELECT CAST(CABEFACV.IDFACV AS INT) AS IDFACV,  CAST(IDLIN AS INT) AS IDLIN,FORMAT (CABEFACV.FECHA, 'dd/MM/yyyy ') AS FECHA, SERIE, CAST(NUMDOC AS BIGINT) AS NUMDOC, " +
                        " CABEFACV.CODCLI, NOMCLI, LINEFACT.CODART, DESCLIN, " +
                        " UNIDADES, PRECIO,LINEFACT.BASE, LINEFACT.TIPIVA, LINEFACT.CTACONL, " +
                        " LINEFACT.CENTROCOSTE AS CC1DOC,ARTICULO.CENTROCOSTEC AS CC1ART, " +
                        " LINEFACT.CENTROCOSTE2 AS CC2DOC,ARTICULO.CENTROCOSTEC2 AS CC2ART, " +
                        " LINEFACT.CENTROCOSTE3 AS CC3DOC,ARTICULO.CENTROCOSTEC3 AS CC3ART " +
                        " FROM CABEFACV INNER JOIN LINEFACT ON CABEFACV.IDFACV = LINEFACT.IDFACV " +
                        " LEFT JOIN ARTICULO ON LINEFACT.CODART = ARTICULO.CODART " +
                        " WHERE CABEFACV.IDFACV = " + idDocumento;

                string scriptDocs = (partida == "FC") ? scriptCargaDocsC : scriptCargaDocsV;

                DataTable dtDocs = sql.obtenerDatosSQLScript(scriptDocs);
                dgvFras.DataSource = dtDocs;
                dgvFras.AutoResizeColumns();


            }
            catch (Exception ex)
            {
            }

        }

        private void rbutFullAsiento_CheckedChanged(object sender, EventArgs e)
        {
            cargarDGV();
        }


        private void cargarCentrosCoste()
        {
            try
            {
                //CC1
                cboxCC1.Items.Clear();
                cboxCC1.ValueMember = "CENTROCOSTE";
                cboxCC1.DisplayMember = "DESCRIP";
                cboxCC1.DataSource = sql.obtenerDatosSQLScript(" SELECT CENTROCOSTE, CONCAT('(',LTRIM(CENTROCOSTE),') ',DESCCENTRO) AS DESCRIP FROM CENTROSC WHERE APLINIVEL1='T' AND OBSOLETO='F' ");
                //CC2
                cboxCC2.Items.Clear();
                cboxCC2.ValueMember = "CENTROCOSTE";
                cboxCC2.DisplayMember = "DESCRIP";
                cboxCC2.DataSource = sql.obtenerDatosSQLScript(" SELECT CENTROCOSTE, CONCAT('(',LTRIM(CENTROCOSTE),') ',DESCCENTRO) AS DESCRIP FROM CENTROSC WHERE APLINIVEL2='T' AND OBSOLETO='F' ");
                //CC3
                cboxCC3.Items.Clear();
                cboxCC3.ValueMember = "CENTROCOSTE";
                cboxCC3.DisplayMember = "DESCRIP";
                cboxCC3.DataSource = sql.obtenerDatosSQLScript(" SELECT CENTROCOSTE, CONCAT('(',LTRIM(CENTROCOSTE),') ',DESCCENTRO) AS DESCRIP FROM CENTROSC WHERE APLINIVEL3='T' AND OBSOLETO='F' ");
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        private void dgvAsientos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //Cargar Factura
                string tipoDoc = dgvAsientos.Rows[e.RowIndex].Cells["PROCEDE"].Value.ToString();
                string idDoc = dgvAsientos.Rows[e.RowIndex].Cells["PROCEDEID"].Value.ToString();
                idDoc = idDoc.Replace(",0000", "");
                cargarDoc(tipoDoc, idDoc);

                //Identificar Centros de Coste de la linea
                identificarCentrosCoste(e.RowIndex);

                //Formaterar colorear lineas mismo Asiento
                string numAsiento = dgvAsientos.Rows[e.RowIndex].Cells["NUMASIENTO"].Value.ToString();
                string numApunte = dgvAsientos.Rows[e.RowIndex].Cells["IDAPUNTE"].Value.ToString();
                foreach (DataGridViewRow row in dgvAsientos.Rows)
                {
                    if (row.Cells["NUMASIENTO"].Value.ToString() == numAsiento)
                    {
                        if (row.Cells["IDAPUNTE"].Value.ToString() == numApunte)
                        {
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            row.DefaultCellStyle.BackColor = Color.LawnGreen;
                        }
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.White;
                    }
                }

                //Actualizar Fechas
                if (this.dgvAsientos.Columns[e.ColumnIndex].Name == "FECHA")
                {
                    dtpFromInvoicesV.Value = Convert.ToDateTime(dgvAsientos["FECHA", e.RowIndex].Value.ToString());
                    dtpToInvoicesV.Value = Convert.ToDateTime(dgvAsientos["FECHA", e.RowIndex].Value.ToString());
                    this.Refresh();
                }

            }
            catch (Exception ex) { }
        }


        private void identificarCentrosCoste(int fila)
        {
            string centroCoste1 = dgvAsientos.Rows[fila].Cells["CENTROCOSTE"].Value.ToString();
            string centroCoste2 = dgvAsientos.Rows[fila].Cells["CENTROCOSTE2"].Value.ToString();
            string centroCoste3 = dgvAsientos.Rows[fila].Cells["CENTROCOSTE3"].Value.ToString();

            foreach (DataRow filaCC in dtListaCentrosCoste.Rows)
            {
                if (filaCC["CENTROCOSTE"].ToString() == centroCoste1)
                {
                    cboxCC1.SelectedValue = centroCoste1;
                }
                if (filaCC["CENTROCOSTE"].ToString() == centroCoste2)
                {
                    cboxCC2.SelectedValue = centroCoste2;
                }
                if (filaCC["CENTROCOSTE"].ToString() == centroCoste3)
                {
                    cboxCC3.SelectedValue = centroCoste3;
                }
            }


        }

        private void btnUpdateAsiento_Click(object sender, EventArgs e)
        {


            try
            {
                string numAsiento = "";
                string centroCoste1 = cboxCC1.SelectedValue.ToString();
                string centroCoste2 = cboxCC2.SelectedValue.ToString();
                string centroCoste3 = cboxCC3.SelectedValue.ToString();
                string scriptUpdate = "";

                string messageUpdate = "¿Quiere actualizar los datos analiticos de contabilidad?\n\n\n" +
                    "Si no selecciona ninguna fila, se actualizará la fila activa\n" +
                    "Si quiere actualizar más de una fila, debe seleccionar las filas completas que quiera actualizar.";
                DialogResult Resultado;
                Resultado = MessageBox.Show(messageUpdate, "Actualizar Centros de Coste", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.Yes)
                {
                    if (dgvAsientos.SelectedRows.Count == 0)
                    {
                        numAsiento = dgvAsientos.Rows[dgvAsientos.CurrentRow.Index].Cells["IDAPUNTE"].Value.ToString();
                        centroCoste1 = cboxCC1.SelectedValue.ToString();
                        centroCoste2 = cboxCC2.SelectedValue.ToString();
                        centroCoste3 = cboxCC3.SelectedValue.ToString();

                        scriptUpdate = "UPDATE __ASIENTOS SET " +
                            " CENTROCOSTE='" + centroCoste1 + "', " +
                            " CENTROCOSTE2='" + centroCoste2 + "', " +
                            " CENTROCOSTE3='" + centroCoste3 + "' " +
                            " WHERE IDAPUNTE = " + numAsiento;

                        sql.actualizarCampo(scriptUpdate);
                    }
                    else
                    {
                        foreach (DataGridViewRow filaToUpdate in dgvAsientos.SelectedRows)
                        {
                            numAsiento = filaToUpdate.Cells["IDAPUNTE"].Value.ToString();
                            centroCoste1 = cboxCC1.SelectedValue.ToString();
                            centroCoste2 = cboxCC2.SelectedValue.ToString();
                            centroCoste3 = cboxCC3.SelectedValue.ToString();

                            scriptUpdate = "UPDATE __ASIENTOS SET " +
                               " CENTROCOSTE='" + centroCoste1 + "', " +
                               " CENTROCOSTE2='" + centroCoste2 + "', " +
                               " CENTROCOSTE3='" + centroCoste3 + "' " +
                               " WHERE IDAPUNTE = " + numAsiento;

                            sql.actualizarCampo(scriptUpdate);

                        }

                    }

                    cargarDGV();
                }

            }
            catch
            {

            }
        }

        private void btnUpdateDocs_Click(object sender, EventArgs e)
        {
            try
            {
                string numLinea = "";
                string centroCoste1 = cboxCC1.SelectedValue.ToString();
                string centroCoste2 = cboxCC2.SelectedValue.ToString();
                string centroCoste3 = cboxCC3.SelectedValue.ToString();
                string scriptUpdate = "";

                string messageUpdate = "¿Quiere actualizar los datos analiticos de las lineas de documentos?\n\n\n" +
                    "Si no selecciona ninguna fila, se actualizará la fila activa\n" +
                    "Si quiere actualizar más de una fila, debe seleccionar las filas completas que quiera actualizar.";
                DialogResult Resultado;
                Resultado = MessageBox.Show(messageUpdate, "Actualizar Centros de Coste", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Resultado == DialogResult.Yes)
                {

                    if (dgvFras.SelectedRows.Count == 0)
                    {
                        numLinea = dgvFras.Rows[dgvFras.CurrentRow.Index].Cells["IDLIN"].Value.ToString();
                        centroCoste1 = cboxCC1.SelectedValue.ToString();
                        centroCoste2 = cboxCC2.SelectedValue.ToString();
                        centroCoste3 = cboxCC3.SelectedValue.ToString();

                        scriptUpdate = "UPDATE LINEFACT SET " +
                            " CENTROCOSTE='" + centroCoste1 + "', " +
                            " CENTROCOSTE2='" + centroCoste2 + "', " +
                            " CENTROCOSTE3='" + centroCoste3 + "' " +
                            " WHERE IDLIN = " + numLinea;

                        sql.actualizarCampo(scriptUpdate);
                    }
                    else
                    {
                        foreach (DataGridViewRow filaToUpdate in dgvAsientos.SelectedRows)
                        {
                            numLinea = filaToUpdate.Cells["IDLIN"].Value.ToString();
                            centroCoste1 = cboxCC1.SelectedValue.ToString();
                            centroCoste2 = cboxCC2.SelectedValue.ToString();
                            centroCoste3 = cboxCC3.SelectedValue.ToString();

                            scriptUpdate = "UPDATE LINEFACT SET " +
                               " CENTROCOSTE='" + centroCoste1 + "', " +
                               " CENTROCOSTE2='" + centroCoste2 + "', " +
                               " CENTROCOSTE3='" + centroCoste3 + "' " +
                               " WHERE IDLIN = " + numLinea;

                            sql.actualizarCampo(scriptUpdate);

                        }

                    }

                    string tipoDoc = dgvAsientos.Rows[dgvAsientos.CurrentRow.Index].Cells["PROCEDE"].Value.ToString();
                    string idDoc = dgvAsientos.Rows[dgvAsientos.CurrentRow.Index].Cells["PROCEDEID"].Value.ToString();
                    idDoc = idDoc.Replace(",0000", "");
                    cargarDoc(tipoDoc, idDoc);
                }

            }
            catch
            {

            }

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            cargarDGV();
        }
    }
}
