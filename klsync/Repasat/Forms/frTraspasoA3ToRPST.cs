﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExcelDataReader;
using System.Collections;
using a3ERPActiveX;

namespace klsync.Repasat.Forms
{
    public partial class frTraspasoA3ToRPST : Form
    {
        private static frTraspasoA3ToRPST m_FormDefInstance;


        public static frTraspasoA3ToRPST DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frTraspasoA3ToRPST();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frTraspasoA3ToRPST()
        {
            InitializeComponent();
        }

        private string rutaArchivo;
        private DataTable dtAsientos;
        private DataTable dtCuentas;
        private DataSet dtsResult;
        private DataTable dtGastos;

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            if (dgvDatos.Rows.Count > 0)
            {
                this.dgvDatos.DataSource = null;
                dgvDatos.Rows.Clear();
            }
          

            Stream myStream;
            OpenFileDialog abrirArchivo = new OpenFileDialog();

            abrirArchivo.InitialDirectory = "c:\\";
            abrirArchivo.Filter = "CSV files (*.csv)|*.csv|Excel Files|*.xls;*.xlsx";
            abrirArchivo.FilterIndex = 2;
            abrirArchivo.RestoreDirectory = true;

            if (abrirArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                rutaArchivo = abrirArchivo.FileName;
                try
                {
                    if ((myStream = abrirArchivo.OpenFile()) != null)
                    {
                        //Dataset completo
                        dtsResult = leerExcel(myStream);

                        dtAsientos = this.dtsResult.Tables[0];

                        dtGastos = this.dtsResult.Tables[1];
                        // dtFacturas = this.dtsResult.Tables[2];

                        //infoComercial();
                        //transformarFacturas();
                        //transformarGastos();
                        //dateFormat();

                        //dgvFacturas.DataSource = this.dtFacturas;
                        dgvDatos.DataSource = dtGastos;    
                    //dgvGastos.DataSource = this.dtGastos;
                        //cellFomat();

                        //textBox1.Text = abrirArchivo.SafeFileName;
                        //button2.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cierre el documento previamente. ");
                }
            }
        }

        private DataSet leerExcel(Stream stream)
        {
            var result = new DataSet();

            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                result = reader.AsDataSet();
                return result;
            }
        }

        private void btnDeleteRows_Click(object sender, EventArgs e)
        {
            for (int i = 6; i >= 0; i--)
            {
                //renombro columnas
                if (i == 6)
                {
                    foreach (DataColumn col in dtAsientos.Columns)
                    {
                        col.ColumnName = dtAsientos.Rows[i][col].ToString();

                    }

                }
                dtAsientos.Rows.RemoveAt(i);
            }
            dgvDatos.DataSource = dtAsientos;


        }

        private void btnAddAsiento_Click(object sender, EventArgs e)
        {

            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();

            a3ERPActiveX.Asiento asientoA3 = new a3ERPActiveX.Asiento();

            asientoA3.Iniciar();

            asientoA3.NuevoNax("N", "1", "GENERAL", "", "09/05/2021");
            asientoA3.Importando = true;
          
            asientoA3.AApunteNax("", "GASTOS VARIOS", "1", "1", "1", "EURO", "57200000", 100, 100, 0, 0, "TEXTO", "07/05/2021");
          
            asientoA3.AApunteNax("", "GASTOS VARIOS", "1", "1", "1", "EURO", "62800003", 0, 0, 100, 100, "TEXTO", "07/05/2021");
            decimal numeroAsiento = asientoA3.Anade();
          
            asientoA3.Acabar();


            a3erp.cerrarEnlace();



        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {

            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
            maestro.Iniciar("cuentas");
            maestro.Nuevo();
            maestro.set_AsString("CUENTA", "57200011");
            maestro.set_AsString("DESCCUE", "BANCO FICTICIO 11");
            //maestro.set_AsString("ACTPASS", "A");
            maestro.set_AsString("DETALLE", "T");
            maestro.set_AsString("BLOQUEADO", "F");
            maestro.set_AsString("OBSOLETO", "F");
            string idcuenta = maestro.get_AsString("IDCUENTA");
            string codcuenta = maestro.get_AsString("CUENTA");
            maestro.Guarda(true);
            maestro.Acabar();
            a3erp.cerrarEnlace();
        }
    }
}
