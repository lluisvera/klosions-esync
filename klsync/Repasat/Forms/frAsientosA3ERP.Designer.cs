﻿namespace klsync.Repasat.Forms
{
    partial class frAsientosA3ERP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grBoxCentrosC = new System.Windows.Forms.GroupBox();
            this.btnUpdateDocs = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAsientos = new System.Windows.Forms.Label();
            this.btnUpdateAsiento = new System.Windows.Forms.Button();
            this.lblCC3 = new System.Windows.Forms.Label();
            this.cboxCC3 = new System.Windows.Forms.ComboBox();
            this.lblCC2 = new System.Windows.Forms.Label();
            this.cboxCC2 = new System.Windows.Forms.ComboBox();
            this.lblCC1 = new System.Windows.Forms.Label();
            this.cboxCC1 = new System.Windows.Forms.ComboBox();
            this.grBoxShowLines = new System.Windows.Forms.GroupBox();
            this.rbutOnlyPyG = new System.Windows.Forms.RadioButton();
            this.rbutFullAsiento = new System.Windows.Forms.RadioButton();
            this.grBoxFechas = new System.Windows.Forms.GroupBox();
            this.dtpFromInvoicesV = new System.Windows.Forms.DateTimePicker();
            this.dtpToInvoicesV = new System.Windows.Forms.DateTimePicker();
            this.lbDesde = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgvAsientos = new System.Windows.Forms.DataGridView();
            this.dgvFras = new System.Windows.Forms.DataGridView();
            this.btnLoad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grBoxCentrosC.SuspendLayout();
            this.grBoxShowLines.SuspendLayout();
            this.grBoxFechas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFras)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grBoxCentrosC);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxShowLines);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxFechas);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1125, 563);
            this.splitContainer1.SplitterDistance = 170;
            this.splitContainer1.TabIndex = 0;
            // 
            // grBoxCentrosC
            // 
            this.grBoxCentrosC.Controls.Add(this.btnUpdateDocs);
            this.grBoxCentrosC.Controls.Add(this.label1);
            this.grBoxCentrosC.Controls.Add(this.lblAsientos);
            this.grBoxCentrosC.Controls.Add(this.btnUpdateAsiento);
            this.grBoxCentrosC.Controls.Add(this.lblCC3);
            this.grBoxCentrosC.Controls.Add(this.cboxCC3);
            this.grBoxCentrosC.Controls.Add(this.lblCC2);
            this.grBoxCentrosC.Controls.Add(this.cboxCC2);
            this.grBoxCentrosC.Controls.Add(this.lblCC1);
            this.grBoxCentrosC.Controls.Add(this.cboxCC1);
            this.grBoxCentrosC.Location = new System.Drawing.Point(12, 266);
            this.grBoxCentrosC.Name = "grBoxCentrosC";
            this.grBoxCentrosC.Size = new System.Drawing.Size(127, 290);
            this.grBoxCentrosC.TabIndex = 42;
            this.grBoxCentrosC.TabStop = false;
            this.grBoxCentrosC.Text = "Centros Coste";
            // 
            // btnUpdateDocs
            // 
            this.btnUpdateDocs.Location = new System.Drawing.Point(18, 246);
            this.btnUpdateDocs.Name = "btnUpdateDocs";
            this.btnUpdateDocs.Size = new System.Drawing.Size(103, 36);
            this.btnUpdateDocs.TabIndex = 9;
            this.btnUpdateDocs.Text = "Actualizar >>>";
            this.btnUpdateDocs.UseVisualStyleBackColor = true;
            this.btnUpdateDocs.Click += new System.EventHandler(this.btnUpdateDocs_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Documentos";
            // 
            // lblAsientos
            // 
            this.lblAsientos.AutoSize = true;
            this.lblAsientos.Location = new System.Drawing.Point(9, 163);
            this.lblAsientos.Name = "lblAsientos";
            this.lblAsientos.Size = new System.Drawing.Size(47, 13);
            this.lblAsientos.TabIndex = 7;
            this.lblAsientos.Text = "Asientos";
            // 
            // btnUpdateAsiento
            // 
            this.btnUpdateAsiento.Location = new System.Drawing.Point(18, 179);
            this.btnUpdateAsiento.Name = "btnUpdateAsiento";
            this.btnUpdateAsiento.Size = new System.Drawing.Size(103, 36);
            this.btnUpdateAsiento.TabIndex = 6;
            this.btnUpdateAsiento.Text = "Actualizar >>>";
            this.btnUpdateAsiento.UseVisualStyleBackColor = true;
            this.btnUpdateAsiento.Click += new System.EventHandler(this.btnUpdateAsiento_Click);
            // 
            // lblCC3
            // 
            this.lblCC3.AutoSize = true;
            this.lblCC3.Location = new System.Drawing.Point(9, 112);
            this.lblCC3.Name = "lblCC3";
            this.lblCC3.Size = new System.Drawing.Size(77, 13);
            this.lblCC3.TabIndex = 5;
            this.lblCC3.Text = "Centro Coste 3";
            // 
            // cboxCC3
            // 
            this.cboxCC3.FormattingEnabled = true;
            this.cboxCC3.Location = new System.Drawing.Point(6, 128);
            this.cboxCC3.Name = "cboxCC3";
            this.cboxCC3.Size = new System.Drawing.Size(115, 21);
            this.cboxCC3.TabIndex = 4;
            // 
            // lblCC2
            // 
            this.lblCC2.AutoSize = true;
            this.lblCC2.Location = new System.Drawing.Point(9, 68);
            this.lblCC2.Name = "lblCC2";
            this.lblCC2.Size = new System.Drawing.Size(77, 13);
            this.lblCC2.TabIndex = 3;
            this.lblCC2.Text = "Centro Coste 2";
            // 
            // cboxCC2
            // 
            this.cboxCC2.FormattingEnabled = true;
            this.cboxCC2.Location = new System.Drawing.Point(6, 84);
            this.cboxCC2.Name = "cboxCC2";
            this.cboxCC2.Size = new System.Drawing.Size(115, 21);
            this.cboxCC2.TabIndex = 2;
            // 
            // lblCC1
            // 
            this.lblCC1.AutoSize = true;
            this.lblCC1.Location = new System.Drawing.Point(9, 24);
            this.lblCC1.Name = "lblCC1";
            this.lblCC1.Size = new System.Drawing.Size(77, 13);
            this.lblCC1.TabIndex = 1;
            this.lblCC1.Text = "Centro Coste 1";
            // 
            // cboxCC1
            // 
            this.cboxCC1.FormattingEnabled = true;
            this.cboxCC1.Location = new System.Drawing.Point(6, 40);
            this.cboxCC1.Name = "cboxCC1";
            this.cboxCC1.Size = new System.Drawing.Size(115, 21);
            this.cboxCC1.TabIndex = 0;
            // 
            // grBoxShowLines
            // 
            this.grBoxShowLines.Controls.Add(this.rbutOnlyPyG);
            this.grBoxShowLines.Controls.Add(this.rbutFullAsiento);
            this.grBoxShowLines.Location = new System.Drawing.Point(12, 174);
            this.grBoxShowLines.Name = "grBoxShowLines";
            this.grBoxShowLines.Size = new System.Drawing.Size(127, 86);
            this.grBoxShowLines.TabIndex = 41;
            this.grBoxShowLines.TabStop = false;
            this.grBoxShowLines.Text = "Mostrar Lineas";
            // 
            // rbutOnlyPyG
            // 
            this.rbutOnlyPyG.AutoSize = true;
            this.rbutOnlyPyG.Location = new System.Drawing.Point(18, 50);
            this.rbutOnlyPyG.Name = "rbutOnlyPyG";
            this.rbutOnlyPyG.Size = new System.Drawing.Size(69, 17);
            this.rbutOnlyPyG.TabIndex = 1;
            this.rbutOnlyPyG.Text = "Sólo PyG";
            this.rbutOnlyPyG.UseVisualStyleBackColor = true;
            // 
            // rbutFullAsiento
            // 
            this.rbutFullAsiento.AutoSize = true;
            this.rbutFullAsiento.Checked = true;
            this.rbutFullAsiento.Location = new System.Drawing.Point(18, 27);
            this.rbutFullAsiento.Name = "rbutFullAsiento";
            this.rbutFullAsiento.Size = new System.Drawing.Size(69, 17);
            this.rbutFullAsiento.TabIndex = 0;
            this.rbutFullAsiento.TabStop = true;
            this.rbutFullAsiento.Text = "Completo";
            this.rbutFullAsiento.UseVisualStyleBackColor = true;
            this.rbutFullAsiento.CheckedChanged += new System.EventHandler(this.rbutFullAsiento_CheckedChanged);
            // 
            // grBoxFechas
            // 
            this.grBoxFechas.Controls.Add(this.btnLoad);
            this.grBoxFechas.Controls.Add(this.dtpFromInvoicesV);
            this.grBoxFechas.Controls.Add(this.dtpToInvoicesV);
            this.grBoxFechas.Controls.Add(this.lbDesde);
            this.grBoxFechas.Controls.Add(this.label2);
            this.grBoxFechas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxFechas.Location = new System.Drawing.Point(12, 12);
            this.grBoxFechas.Name = "grBoxFechas";
            this.grBoxFechas.Size = new System.Drawing.Size(127, 156);
            this.grBoxFechas.TabIndex = 40;
            this.grBoxFechas.TabStop = false;
            this.grBoxFechas.Text = "Fechas";
            // 
            // dtpFromInvoicesV
            // 
            this.dtpFromInvoicesV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromInvoicesV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromInvoicesV.Location = new System.Drawing.Point(12, 41);
            this.dtpFromInvoicesV.Name = "dtpFromInvoicesV";
            this.dtpFromInvoicesV.Size = new System.Drawing.Size(103, 26);
            this.dtpFromInvoicesV.TabIndex = 3;
            // 
            // dtpToInvoicesV
            // 
            this.dtpToInvoicesV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpToInvoicesV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToInvoicesV.Location = new System.Drawing.Point(12, 89);
            this.dtpToInvoicesV.Name = "dtpToInvoicesV";
            this.dtpToInvoicesV.Size = new System.Drawing.Size(102, 26);
            this.dtpToInvoicesV.TabIndex = 5;
            // 
            // lbDesde
            // 
            this.lbDesde.AutoSize = true;
            this.lbDesde.Location = new System.Drawing.Point(9, 22);
            this.lbDesde.Name = "lbDesde";
            this.lbDesde.Size = new System.Drawing.Size(49, 16);
            this.lbDesde.TabIndex = 6;
            this.lbDesde.Text = "Desde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Hasta";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvAsientos);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvFras);
            this.splitContainer2.Size = new System.Drawing.Size(951, 563);
            this.splitContainer2.SplitterDistance = 342;
            this.splitContainer2.TabIndex = 0;
            // 
            // dgvAsientos
            // 
            this.dgvAsientos.AllowUserToAddRows = false;
            this.dgvAsientos.AllowUserToDeleteRows = false;
            this.dgvAsientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAsientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAsientos.Location = new System.Drawing.Point(0, 0);
            this.dgvAsientos.Name = "dgvAsientos";
            this.dgvAsientos.ReadOnly = true;
            this.dgvAsientos.Size = new System.Drawing.Size(951, 342);
            this.dgvAsientos.TabIndex = 0;
            this.dgvAsientos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAsientos_CellClick);
            // 
            // dgvFras
            // 
            this.dgvFras.AllowUserToAddRows = false;
            this.dgvFras.AllowUserToDeleteRows = false;
            this.dgvFras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFras.Location = new System.Drawing.Point(0, 0);
            this.dgvFras.Name = "dgvFras";
            this.dgvFras.ReadOnly = true;
            this.dgvFras.Size = new System.Drawing.Size(951, 217);
            this.dgvFras.TabIndex = 0;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(11, 121);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(103, 29);
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Buscar";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // frAsientosA3ERP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 563);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frAsientosA3ERP";
            this.Text = "Asientos Contables";
            this.Load += new System.EventHandler(this.frAsientosA3ERP_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.grBoxCentrosC.ResumeLayout(false);
            this.grBoxCentrosC.PerformLayout();
            this.grBoxShowLines.ResumeLayout(false);
            this.grBoxShowLines.PerformLayout();
            this.grBoxFechas.ResumeLayout(false);
            this.grBoxFechas.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFras)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dgvAsientos;
        private System.Windows.Forms.DataGridView dgvFras;
        private System.Windows.Forms.RadioButton rbutOnlyPyG;
        private System.Windows.Forms.RadioButton rbutFullAsiento;
        private System.Windows.Forms.GroupBox grBoxShowLines;
        private System.Windows.Forms.GroupBox grBoxFechas;
        private System.Windows.Forms.DateTimePicker dtpFromInvoicesV;
        private System.Windows.Forms.DateTimePicker dtpToInvoicesV;
        private System.Windows.Forms.Label lbDesde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grBoxCentrosC;
        private System.Windows.Forms.Label lblCC3;
        private System.Windows.Forms.ComboBox cboxCC3;
        private System.Windows.Forms.Label lblCC2;
        private System.Windows.Forms.ComboBox cboxCC2;
        private System.Windows.Forms.Label lblCC1;
        private System.Windows.Forms.ComboBox cboxCC1;
        private System.Windows.Forms.Button btnUpdateAsiento;
        private System.Windows.Forms.Button btnUpdateDocs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAsientos;
        private System.Windows.Forms.Button btnLoad;
    }
}