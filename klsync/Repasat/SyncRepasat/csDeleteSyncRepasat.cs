﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Repasat.SyncRepasat
{
    class csDeleteSyncRepasat
    {

        public void deleteSyncRPSTObject(string objectType, DataGridView dgvRegistros)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("¿Quiere borrar la sincronización del registro en REPASAT?", "Borrar Sincronización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            string campoExterno = "";
            switch (objectType)
            {
                case "paymentdocuments":
                    campoExterno = "codExternoDocuPago";
                    break;
                case "paymentmethods":
                    campoExterno = "codExternoFormaPago";
                    break;
                case "employees":
                    campoExterno = "codExternoTrabajador";
                    break;
                case "carriers":
                    campoExterno = "codExternoTransportista";
                    break;
                case "routes":
                    campoExterno = "codExternoRuta";
                    break;
                case "geozones":
                    campoExterno = "codExternoZonaGeo";
                    break;
                case "seriesdocs":
                    campoExterno = "codExternoSerie";
                    break;
                case "warehouses":
                    campoExterno = "codExternoAlmacen";
                    break;
                case "families":
                    campoExterno = "codExternoFamilia";
                    break;
                case "brands":
                    campoExterno = "codExternoMarca";
                    break;
                case "producttypes":
                    campoExterno = "codExternoTipoArticulo";
                    break;
                case "accountingaccounts":
                    campoExterno = "codExtCuentaContable";
                    break;
                case "projects":
                    campoExterno = "codExternoProyecto";
                    break;
                case "costcenters":
                    campoExterno = "codExternoCentroC";
                    break;
                case "banks":
                    campoExterno = "bank[codExternoDatosBancarios]";
                    break;
                default:
                    // code block
                    break;
            }



            if (Resultado == DialogResult.Yes && !string.IsNullOrEmpty(campoExterno))
            {
                int contador = 1;
                string idRegistro = "";
                csRepasatWebService rpstWS = new klsync.csRepasatWebService();
                foreach (DataGridViewRow fila in dgvRegistros.SelectedRows)
                {
                    idRegistro = fila.Cells["IDREPASAT"].Value.ToString();
                    rpstWS.actualizarDocumentoRepasat(objectType, campoExterno, idRegistro, "");
                    contador++;
                }
                "Proceso finalizado".mb();
            }

        }
    }
}
