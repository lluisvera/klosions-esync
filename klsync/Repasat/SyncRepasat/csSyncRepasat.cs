﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat.SyncRepasat
{
    class csSyncRepasat
    {

        public void syncroDocs()
        {
      
            DateTime fechaActual = DateTime.Now;
            //DateTime fechaFrom = fechaActual.AddMonths(-1);
            //DateTime fechaTo = fechaActual.AddMonths(1);
            DateTime fechaFrom = fechaActual.AddDays(-5);
            DateTime fechaTo = fechaActual.AddDays(5);
            string filtroFechaIni = fechaFrom.ToString("yyyy-MM-dd");
            string filtroFechaFin = fechaTo.ToString("yyyy-MM-dd");

            // Facturas - Albaranes - Pedidos - Cartera - Remesas - Impagados

            string tipoDocumento = "";
            tipoDocumento = "Facturas";

            csDismay docsRepasat = new klsync.csDismay();
            docsRepasat.cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, true, false,"NO"); //FACTURAS DE VENTA
            docsRepasat.cargarFacturasFromRPSToA3(filtroFechaIni, filtroFechaFin, false, false, "NO"); //FACTURAS DE COMPRA

            //PEDIDOS
            csGlobal.docDestino = "PEDIDO";
            docsRepasat.cargarPedidos(filtroFechaIni, filtroFechaFin, true, false, "NO");


            fechaFrom = fechaActual.AddMonths(-6);
            fechaTo = fechaActual.AddMonths(2);
            filtroFechaIni = fechaFrom.ToString("yyyy-MM-dd");
            filtroFechaFin = fechaTo.ToString("yyyy-MM-dd");

            //CARTERA
            docsRepasat.cargarEfectos(filtroFechaIni, filtroFechaFin, true, true, "NO");
            docsRepasat.cargarEfectos(filtroFechaIni, filtroFechaFin, false, true, "NO");

            //REMESAS
            docsRepasat.gestionarRemesas(filtroFechaIni, filtroFechaFin, true, false, "NO");

        }

        public void facturasA3ERPToRepasat(string serie=null)
        {
            try
            {
                //para añadir en parametros de ejecución automática
                //TEST_ESYNC_KLS facturasA3ERPToRepasat 23A


                bool forceUpdate = false;
                string serieA3 = serie;
                string numDoc = null;
                bool updateEfectosPagados = false;
                bool filtrarPorFechaFactura = false;
                string syncDocumento = "NO";
                string filtroEstadoCobro = "TODOS";
                string filtroCodCuenta = null;


                DateTime currentDateTime = DateTime.Now;
                string FechaHoy = currentDateTime.Date.ToString("dd-MM-yyyy");

                var fechaInicio = DateTime.ParseExact("05/01/2023", "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                string filtroFechaIni = fechaInicio.Date.ToString("dd-MM-yyyy");

                Repasat.csRPSTSqlDataMigration dataMig = new Repasat.csRPSTSqlDataMigration();
                dataMig.dtLoadDocsFromA3ToRPST("saleinvoices", true, filtroFechaIni, FechaHoy, false, syncDocumento, filtroEstadoCobro, filtroCodCuenta, forceUpdate, serieA3, numDoc, filtrarPorFechaFactura, updateEfectosPagados, true);

            }
            catch
            {

            }

        }


        public void clientesRepasatToA3ERP()
        {
            try
            {
                Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
                updateDataRPST.traspasarCuentasRepasatToA3(true, false, null, true, true, false, false, false);
            }
            catch (Exception ex) { }
            
        }

        public void facturasVRepasatToA3ERP(string serie = null)
        {
            try
            {
                //para añadir en parametros de ejecución automática
                //TEST_ESYNC_KLS facturasRepasatToA3ERP 23A

                bool docsVenta = true;
                bool consulta = false;

                DateTime currentDateTime = DateTime.Now;
                string FechaHoy = currentDateTime.Date.ToString("yyyy-MM-dd");

                csDismay repasat = new klsync.csDismay();
                repasat.cargarFacturasFromRPSToA3(FechaHoy, FechaHoy, docsVenta, false, "NO");

            }
            catch (Exception ex) { }

        }

        //public void actualizarStockA3ERPToRepasat()
        //{
        //    Utilidades.csLogErrores logProceso = new Utilidades.csLogErrores();
        //    try {
        //        //TEST_ESYNC_KLS actualizarStockA3ERPToRepasat
        //        csDismay repasat = new klsync.csDismay();
        //        repasat.actualizarStockA3ERPToRepasat();

        //        logProceso.guardarLogProceso("actualizar stock correcto");
        //    }
        //    catch (Exception ex) {

        //        logProceso.guardarLogProceso("actualizar stock error");
        //    }
        //}
    }
}
