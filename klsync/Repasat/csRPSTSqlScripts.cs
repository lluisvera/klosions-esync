﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csRPSTSqlScripts
    {
        public string obtenerCabeceraPed(bool ventas, string filtro)
        {

                //Select para ventas
                string scriptV = "SELECT " +
                            " dbo.SERIES.RPST_ID_SERIE AS idSerie, CAST(dbo.CABEPEDV.NUMDOC AS int) AS numSerieDocumento, dbo.CABEPEDV.REFERENCIA AS refDocumento, " +
                            " CONVERT(VARCHAR(10), dbo.CABEPEDV.FECHA, 126) AS fecDocumento,  " +
                            " dbo.CABEPEDV.CODCLI, dbo.CABEPEDV.NOMCLI AS NOMBRE_CUENTA, dbo.__CLIENTES.RPST_ID_CLI AS idCli,  " +
                            " CAST(dbo.CABEPEDV.IDDIRENT AS int) AS idDireccionFacturacion, dbo.CABEPEDV.DOCPAG, dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago,  " +
                            " dbo.CABEPEDV.FORPAG, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago,  " +
                            " dbo.CABEPEDV.REGIVA, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, replace(dbo.CABEPEDV.BASEPOR,',','.') AS portesDocumento, " +
                            " CAST(dbo.CABEPEDV.IDPEDV AS int) as IDDOC, CAST(dbo.CABEPEDV.IDPEDV AS int) AS codExternoDocumento, 'A3ERP' as origenDocumento, " +
                            " dbo.CABEPEDV.RPST_ID_PEDV " +
                            " FROM  " +
                            " dbo.CABEPEDV INNER JOIN " +
                            " dbo.__CLIENTES ON dbo.CABEPEDV.CODCLI = dbo.__CLIENTES.CODCLI INNER JOIN " +
                            " dbo.FORMAPAG ON dbo.CABEPEDV.FORPAG = dbo.FORMAPAG.FORPAG INNER JOIN " +
                            " dbo.DOCUPAGO ON dbo.CABEPEDV.DOCPAG = dbo.DOCUPAGO.DOCPAG  " +
                            " INNER JOIN dbo.SERIES ON dbo.CABEPEDV.SERIE = dbo.SERIES.SERIE " +
                            " INNER JOIN dbo.REGIVA ON dbo.CABEPEDV.REGIVA = dbo.REGIVA.REGIVA" + filtro;

                //Select para compras
                string scriptC = "SELECT " +
                            " dbo.SERIES.RPST_ID_SERIE AS idSerie, CAST(dbo.CABEPEDC.NUMDOC AS int) AS numSerieDocumento, dbo.CABEPEDC.REFERENCIA AS refDocumento, " +
                            " CONVERT(VARCHAR(10), dbo.CABEPEDC.FECHA, 126) AS fecDocumento,  " +
                            " dbo.CABEPEDC.CODPRO, dbo.CABEPEDC.NOMPRO AS NOMBRE_CUENTA, dbo.__PROVEED.RPST_ID_PROV AS idCli,  " +
                            " CAST(dbo.CABEPEDC.IDDIRENT AS int) AS idDireccionFacturacion, dbo.CABEPEDC.DOCPAG, dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago,  " +
                            " dbo.CABEPEDC.FORPAG, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago,  " +
                            " dbo.CABEPEDC.REGIVA, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, replace(dbo.CABEPEDC.BASEPOR,',','.') AS portesDocumento, " +
                            " CAST(dbo.CABEPEDC.IDPEDC AS int) as IDDOC, CAST(dbo.CABEPEDC.IDPEDC AS int) AS codExternoDocumento, 'A3ERP' as origenDocumento,  " +
                            " dbo.CABEPEDC.RPST_ID_PEDC " +
                            " FROM  " +
                            " dbo.CABEPEDC INNER JOIN " +
                            " dbo.__PROVEED ON dbo.CABEPEDC.CODPRO = dbo.__PROVEED.CODPRO INNER JOIN " +
                            " dbo.FORMAPAG ON dbo.CABEPEDC.FORPAG = dbo.FORMAPAG.FORPAG INNER JOIN " +
                            " dbo.DOCUPAGO ON dbo.CABEPEDC.DOCPAG = dbo.DOCUPAGO.DOCPAG  " +
                            " INNER JOIN dbo.SERIES ON dbo.CABEPEDC.SERIE = dbo.SERIES.SERIE " +
                            " INNER JOIN dbo.REGIVA ON dbo.CABEPEDC.REGIVA = dbo.REGIVA.REGIVA" + filtro;

                string script = ventas ? scriptV : scriptC;


                return script;
            
        }

        public string obtenerLineasPed(bool ventas, string filtro)
        {
            filtro = ventas ? filtro.Replace("FECHA", "CABEPEDV.FECHA") : filtro.Replace("FECHA", "CABEPEDC.FECHA");
            string script = "SELECT " +
                " dbo.ARTICULO.RPST_ID_PROD AS [lines[XXLIN]][idArticulo]]], LTRIM(dbo.LINEPEDI.CODART) AS [lines[XXLIN]][refArticuloLineaDocumento]]], " +
                " dbo.LINEPEDI.DESCLIN AS [lines[XXLIN]][nomArticuloLineaDocumento]]], REPLACE(dbo.LINEPEDI.UNIDADES,',','.') AS[lines[XXLIN]][unidadesArticuloLineaDocumento]]],  " +
                " REPLACE(dbo.LINEPEDI.PRECIO,',','.') AS [lines[XXLIN]][precioVentaArticuloLineaDocumento]]], " +
                " 'PER' AS [lines[XXLIN]][tipoDescuentoLineaDocumento]]], REPLACE(dbo.LINEPEDI.DESC1,',','.') AS[lines[XXLIN]][descuentoLineaDocumento]]], " +
                " dbo.TIPOIVA.RPST_ID_IVA AS [lines[XXLIN]][idTipoImpuesto]]], dbo.LINEPEDI.NUMLINPED, dbo.LINEPEDI.ORDLIN, " +  
                " CAST ( " + (ventas ? " dbo.LINEPEDI.IDPEDV " : " dbo.LINEPEDI.IDPEDC ") + " AS int) AS IDDOC " +
                " FROM " +
                " dbo.LINEPEDI INNER JOIN dbo.ARTICULO" +
                " ON dbo.LINEPEDI.CODART = dbo.ARTICULO.CODART " +
                " INNER JOIN dbo.TIPOIVA " +
                " ON dbo.LINEPEDI.TIPIVA = dbo.TIPOIVA.TIPIVA " +
                (ventas ? "INNER JOIN CABEPEDV ON CABEPEDV.IDPEDC = LINEPEDI.IDPEDV" : "INNER JOIN CABEPEDC ON CABEPEDC.IDPEDC=LINEPEDI.IDPEDC" )+
                " ​WHERE " + (ventas ? "LINEPEDI.IDPEDC" : "LINEPEDI.IDPEDV") + " IS NULL" + filtro;

            //script = ventas ? script : script.Replace("dbo.IDPEDV", "dbo.IDPEDC");

            return script;
        }

        public string obtenerCabeceraAlb(bool ventas, string filtro)
        {
            string script = "SELECT " +
                        " dbo.SERIES.RPST_ID_SERIE AS idSerie,  CAST(dbo.CABEALBV.NUMDOC AS int) AS numSerieDocumento, dbo.CABEALBV.REFERENCIA AS refDocumento, CONVERT(VARCHAR(10), dbo.CABEALBV.FECHA, 126) AS fecDocumento,  " +
                        " dbo.CABEALBV.CODCLI, dbo.__CLIENTES.RPST_ID_CLI AS idCli,  " +
                        " CAST(dbo.DIRENT.RPST_ID_DIR AS int) AS idDireccionFacturacion, dbo.CABEALBV.DOCPAG, dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago,  " +
                        " dbo.CABEALBV.FORPAG, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago,  " +
                        " dbo.CABEALBV.REGIVA, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, replace(dbo.CABEALBV.BASEPOR,',','.') AS portesDocumento, " +
                        " CAST(dbo.CABEALBV.IDALBV AS int) as IDDOC, CAST(dbo.CABEALBV.IDALBV AS int) AS codExternoDocumento, 'A3ERP' as origenDocumento " +
                        " FROM  " +
                        " dbo.CABEALBV INNER JOIN " +
                        " dbo.__CLIENTES ON dbo.CABEALBV.CODCLI = dbo.__CLIENTES.CODCLI " +
                        " INNER JOIN dbo.FORMAPAG " +
                        " ON dbo.CABEALBV.FORPAG = dbo.FORMAPAG.FORPAG " +
                        " INNER JOIN dbo.DOCUPAGO " +
                        " ON dbo.CABEALBV.DOCPAG = dbo.DOCUPAGO.DOCPAG " +
                        " INNER JOIN dbo.REGIVA " +
                        " ON dbo.CABEALBV.REGIVA = dbo.REGIVA.REGIVA" +
                        " INNER JOIN dbo.SERIES ON dbo.CABEPEDV.SERIE = dbo.SERIES.SERIE " +
                        " INNER JOIN dbo.DIRENT ON dbo.CABEALBV.IDDIRENT = dbo.DIRENT.IDDIRENT" + filtro;

            return script;

        }
        public string obtenerLineasAlb(bool ventas)
        {
            string script = "SELECT " +
              " dbo.ARTICULO.RPST_ID_PROD AS [lines[XXLIN]][idArticulo]]], LTRIM(dbo.LINEALBA.CODART) AS [lines[XXLIN]][refArticuloLineaDocumento]]], " +
              " dbo.LINEALBA.DESCLIN AS [lines[XXLIN]][nomArticuloLineaDocumento]]], dbo.LINEALBA.UNIDADES AS[lines[XXLIN]][unidadesArticuloLineaDocumento]]],  " +
              " REPLACE(dbo.LINEALBA.PRECIO,',','.') AS [lines[XXLIN]][precioVentaArticuloLineaDocumento]]], " +
              " 'PER' AS [lines[XXLIN]][tipoDescuentoLineaDocumento]]], REPLACE(dbo.LINEALBA.DESC1,',','.') AS[lines[XXLIN]][descuentoLineaDocumento]]], " +
              " dbo.TIPOIVA.RPST_ID_IVA AS [lines[XXLIN]][idTipoImpuesto]]], dbo.LINEALBA.NUMLINALB, dbo.LINEALBA.ORDLIN,  CAST (dbo.LINEALBA.IDALBV AS int) AS IDDOC " +
              " FROM " +
              " dbo.LINEALBA INNER JOIN dbo.ARTICULO" +
              " ON dbo.LINEALBA.CODART = dbo.ARTICULO.CODART " +
              " INNER JOIN dbo.TIPOIVA " +
              " ON dbo.LINEALBA.TIPIVA = dbo.TIPOIVA.TIPIVA " +
              " ​WHERE IDALBC IS NULL";





            return script;

        }


        public string obtenerCabeceraFra(bool ventas, string filtro, bool consulta=false, string syncronizados="TODOS")
        {
            string scriptSyncV = "";
            string scriptLoadV = "";
            string scriptSyncC = "";
            string scriptLoadC = "";
            string scriptQuery = "";

            scriptSyncV = "SELECT " +
                        " dbo.SERIES.RPST_ID_SERIE AS idSerie, CAST(dbo.CABEFACV.NUMDOC AS int) AS numSerieDocumento, 0 as generarRecibos, " +
                        " dbo.CABEFACV.REFERENCIA AS refDocumento, CONVERT(VARCHAR(10), dbo.CABEFACV.FECHA, 126) AS fecDocumento,  " +
                        " dbo.CABEFACV.CODCLI, dbo.__CLIENTES.RPST_ID_CLI AS idCli,  " +
                        " CAST(dbo.DIRENT.RPST_ID_DIR AS int) AS idDireccionFacturacion, dbo.CABEFACV.DOCPAG, dbo.DOCUPAGO.RPST_ID_DOCPAGO AS idDocuPago,  " +
                        " dbo.CABEFACV.FORPAG, dbo.FORMAPAG.RPST_ID_FORMAPAG AS idFormaPago,  " +
                        " dbo.CABEFACV.REGIVA, dbo.REGIVA.RPST_ID_REGIVA AS idRegimenImpuesto, replace(dbo.CABEFACV.BASEPOR,',','.') AS portesDocumento, " +
                        " CAST(dbo.CABEFACV.IDFACV AS int) as IDDOC, CASE WHEN RECTIFICATIVA ='T' THEN 'CREDIT_NOTE' ELSE 'INVOICE' END AS tipoDocumento,   " +
                        " CAST(dbo.CABEFACV.IDFACV AS int) as codExternoDocumento, 'A3ERP' as origenDocumento, dbo.REPRESEN.RPST_ID_REPRE as idTrabajador " +
                        " FROM  " +
                        " dbo.CABEFACV INNER JOIN " +
                        " dbo.__CLIENTES ON dbo.CABEFACV.CODCLI = dbo.__CLIENTES.CODCLI " +
                        " INNER JOIN dbo.FORMAPAG " +
                        " ON dbo.CABEFACV.FORPAG = dbo.FORMAPAG.FORPAG " +
                        " INNER JOIN dbo.DOCUPAGO " +
                        " ON dbo.CABEFACV.DOCPAG = dbo.DOCUPAGO.DOCPAG " +
                        " INNER JOIN dbo.REGIVA " +
                        " ON dbo.CABEFACV.REGIVA = dbo.REGIVA.REGIVA" +
                        " INNER JOIN dbo.SERIES ON dbo.CABEFACV.SERIE = dbo.SERIES.SERIE " +
                        " INNER JOIN dbo.DIRENT ON dbo.CABEFACV.IDDIRENT = dbo.DIRENT.IDDIRENT "+
                        " LEFT OUTER JOIN dbo.REPRESEN ON CABEFACV.CODREP = dbo.REPRESEN.CODREP " + filtro;

            scriptLoadV = "SELECT " +
                        " CASE WHEN RPST_ID_FACV =0 THEN 'Pendiente Traspaso'  WHEN RPST_ID_FACV IS NOT NULL THEN 'Documento Traspasado' else 'Pendiente Traspaso' end as ACCION, " +
                        " FECHA, TIPOCONT AS TC, " +
                        " CONCAT(CABEFACV.SERIE,' (', SERIES.RPST_ID_SERIE, ')') AS SERIE, " +
                        " CAST(NUMDOC as int) AS NUMDOC, RPST_ID_FACV AS RPST_ID, " +
                        " CABEFACV.CODCLI, " +
                        " CONCAT (NOMCLI, ' (', __CLIENTES.RPST_ID_CLI,')') AS NOMCLI, " +
                        " BASE, TOTIVA, TOTDOC,  " +
                        " CONCAT(LTRIM(CABEFACV.FORPAG),' ( ', FORMAPAG.RPST_ID_FORMAPAG, ' )') AS FORMAPAGO, " +
                        " CONCAT(LTRIM(CABEFACV.DOCPAG),' ( ', DOCUPAGO.RPST_ID_DOCPAGO, ' )') AS DOCPAGO, " +
                        " CONCAT(CABEFACV.REGIVA,' ( ', REGIVA.RPST_ID_REGIVA, ' )') AS REGIVA, " +
                        " CAST(IDFACV AS int) as IDFACV,  CAST(NUMCARTERA AS int) AS NUMCARTERA, " +
                        " CONCAT(CABEFACV.CODREP,' (', REPRESEN.RPST_ID_REPRE, ')') AS CIAL, " + 
                        " 'A3ERP' as origenDocumento " +
                        " FROM " +
                        " CABEFACV INNER JOIN SERIES ON " +
                        " CABEFACV.SERIE = SERIES.SERIE " +
                        " INNER JOIN __CLIENTES ON " +
                        " CABEFACV.CODCLI = __CLIENTES.CODCLI " +
                        " INNER JOIN FORMAPAG ON " +
                        " CABEFACV.FORPAG = FORMAPAG.FORPAG " +
                        " INNER JOIN REGIVA ON " +
                        " CABEFACV.REGIVA = REGIVA.REGIVA " +
                        " INNER JOIN DOCUPAGO ON " +
                        " CABEFACV.DOCPAG = DOCUPAGO.DOCPAG " +
                        " LEFT JOIN REPRESEN ON CABEFACV.CODREP=REPRESEN.CODREP " + filtro +
                        " ORDER BY CABEFACV.SERIE, CABEFACV.NUMDOC";

            scriptLoadC = "SELECT " +
                         " dbo.CABEFACC.FECHA, dbo.CABEFACC.TIPOCONT AS TC, " +
                         " CONCAT(dbo.CABEFACC.SERIE, ' (', dbo.SERIES.RPST_ID_SERIE, ')')  AS SERIE, CAST(dbo.CABEFACC.NUMDOC AS int) AS NUMDOC, " +
                         " dbo.CABEFACC.RPST_ID_FACC AS RPST_ID, dbo.CABEFACC.CODPRO, dbo.CABEFACC.NOMPRO, dbo.CABEFACC.BASE, dbo.CABEFACC.TOTIVA, dbo.CABEFACC.TOTDOC,  CONCAT(LTRIM(dbo.CABEFACC.FORPAG), ' ( ', " +
                         " dbo.FORMAPAG.RPST_ID_FORMAPAG, ' )')  AS FORMAPAGO, CONCAT(LTRIM(dbo.CABEFACC.DOCPAG), ' ( ', dbo.DOCUPAGO.RPST_ID_DOCPAGO, ' )')  AS DOCPAGO, CONCAT(dbo.CABEFACC.REGIVA, ' ( ', " +
                         " dbo.REGIVA.RPST_ID_REGIVA, ' )')  AS REGIVA, CAST(dbo.CABEFACC.IDFACC AS int) AS IDFACC, CAST(dbo.CABEFACC.NUMCARTERA AS int) AS NUMCARTERA, 'A3ERP' AS origenDocumento " +
                         " FROM dbo.CABEFACC INNER JOIN " +
                         " dbo.SERIES ON dbo.CABEFACC.SERIE = dbo.SERIES.SERIE INNER JOIN " +
                         " dbo.__PROVEED ON dbo.CABEFACC.CODPRO = dbo.__PROVEED.CODPRO INNER JOIN " +
                         " dbo.FORMAPAG ON dbo.CABEFACC.FORPAG = dbo.FORMAPAG.FORPAG INNER JOIN " +
                         " dbo.REGIVA ON dbo.CABEFACC.REGIVA = dbo.REGIVA.REGIVA INNER JOIN " +
                         " dbo.DOCUPAGO ON dbo.CABEFACC.DOCPAG = dbo.DOCUPAGO.DOCPAG " + filtro +
                         " ORDER BY SERIE, NUMDOC";

            if (ventas)
            {
                return scriptQuery = consulta ? scriptLoadV : scriptSyncV;
            }
            else
            {
                return scriptQuery = consulta ? scriptLoadC : scriptSyncC;
            }


        }

        public string obtenerCarteraEfectos(bool ventas, string filtro)
        {
            string script = "SELECT " +
                " COBPAG, PAGADO, CASE WHEN CARTERA.CODCLI IS NULL THEN LTRIM(CARTERA.CODPRO) ELSE LTRIM(CARTERA.CODCLI) END AS CODIC, " +
                " NOMBRE,  CARTERA.CODBAN,CTABAN,CARTERA.DOCPAG,CARTERA.FECHA, CARTERA.FECHAVALOR, CARTERA.FECHAFACTURA, " +
                " CAST(IDCARTERA AS int) AS IDCARTERA, CAST(IDDOMBANCA AS int) AS IDDOMBANCA,  IMPORTE,IMPORTECOB, " +
                " CAST (CARTERA.NUMCARTERA AS int) AS NUMCARTERA,  CARTERA.TIPOCONT, CARTERA.SERIE, CAST (CARTERA.NUMDOC AS int) AS NUMDOC, " +
                " NUMVEN, CAST (PADREREME AS int) AS REMESA, PROCEDE, " +
                " CAST(PROCEDEID AS int) AS PROCEDEID, CASE WHEN CARTERA.CODCLI IS NULL THEN  CABEFACC.RPST_ID_FACC ELSE CABEFACV.RPST_ID_FACV END AS ID_REPASAT_FRA, " +
                " RPST_ID_CARTERA , BANCOS.RPST_ID_BANCO, 'A3ERP' as origenDocumento " +
                " FROM dbo.CARTERA LEFT OUTER JOIN " +
                " CABEFACV ON dbo.CARTERA.PROCEDEID = dbo.CABEFACV.IDFACV LEFT OUTER JOIN " +
                " CABEFACC ON dbo.CARTERA.PROCEDEID = dbo.CABEFACC.IDFACC LEFT OUTER JOIN " +
                " BANCOS ON dbo.CARTERA.CODBAN = dbo.BANCOS.CODBAN  " + filtro;

            return script;
        }

        public string obtenerCabeceraTarifaPrecios(bool ventas, string filtro)
        {
            string script = "SELECT LTRIM(tarifas.TARIFA) AS codExternoTarifaPrecio, upper(desctarifa) as nomTarifa, 1 as activoTarifa, " +
                " case WHEN tipo = 'V' then 'VEN' else 'COM' end as tipoTarifa, RPST_ID_TARIFA AS idTarifa, " +
                " FORMAT(max, 'yyyy-MM-dd') as fecMaxTarifa, FORMAT(min, 'yyyy-MM-dd') as fecMinTarifa, tarifas.Tarifa as IDDOC " +
                " FROM TARIFAS " +
                " inner join(select tarifa, max(tarifave.fecmax) max, min(fecmin) min " +
                " from TARIFAVE group by TARIFA) as tar " +
                " on tarifas.tarifa = tar.TARIFA " + filtro;

            return script;
        }

        public string obtenerLineasTarifaPrecios(bool ventas, string filtro)
        {
            string script = "select ltrim(tarifave.TARIFA) AS IDDOC, CAST(IDTARIFAV AS INT) AS ORDLIN, ltrim(tarifave.CODART) as CODART ,RPST_ID_PROD [lintarifa[XXLIN]][idArticulo]]], " +
                " FORMAT (fecmin, 'yyyy-MM-dd') [lintarifa[XXLIN]][fecDesdeLinTarifa]]],FORMAT (fecmax, 'yyyy-MM-dd') [lintarifa[XXLIN]][fecHastaLinTarifa]]], " +
                " unidades [lintarifa[XXLIN]][unidadesLinTarifa]]] from TARIFAVE " +
                " inner join articulo on articulo.codart = tarifave.CODART " +
                " inner join tarifas on tarifas.tarifa = tarifave.TARIFA " +
                " where RPST_ID_PROD is not null" + filtro;

            return script;
        }

        public string obtenerLineasFra(bool ventas, string filtro, bool consulta = false, string syncronizados = "TODOS")
        {

            filtro = ventas ? filtro.Replace("FECHA", "CABEFACV.FECHA") : filtro.Replace("FECHA", "CABEFACC.FECHA");
            string orden = ventas ? " ORDER BY dbo.LINEFACT.IDFACV, dbo.LINEFACT.ORDLIN " : " ORDER BY dbo.LINEFACT.IDFACC, dbo.LINEFACT.ORDLIN ";
                 
           string script = "SELECT " +
            " dbo.ARTICULO.RPST_ID_PROD AS [lines[XXLIN]][idArticulo]]], LTRIM(dbo.LINEFACT.CODART) AS [lines[XXLIN]][refArticuloLineaDocumento]]], " +
            " CASE WHEN dbo.LINEFACT.DESCLIN='' THEN '-' ELSE dbo.LINEFACT.DESCLIN END AS [lines[XXLIN]][nomArticuloLineaDocumento]]], dbo.LINEFACT.UNIDADES AS[lines[XXLIN]][unidadesArticuloLineaDocumento]]],  " +
            " REPLACE(dbo.LINEFACT.PRECIO,',','.') AS [lines[XXLIN]][precioVentaArticuloLineaDocumento]]], " +
            " 'PER' AS [lines[XXLIN]][tipoDescuentoLineaDocumento]]], " +
            " REPLACE(dbo.LINEFACT.DESC1,',','.') AS[lines[XXLIN]][descuentoLineaDocumento]]], " +
            " REPLACE(dbo.LINEFACT.DESC2,',','.') AS[lines[XXLIN]][descuento2LineaDocumento]]], " +
            " dbo.TIPOIVA.RPST_ID_IVA AS [lines[XXLIN]][idTipoImpuesto]]], dbo.LINEFACT.NUMLINFAC, dbo.LINEFACT.ORDLIN,  CAST (dbo.LINEFACT.IDFACV AS int) AS IDDOC " +
            " FROM " +
            " dbo.LINEFACT INNER JOIN dbo.ARTICULO" +
            " ON dbo.LINEFACT.CODART = dbo.ARTICULO.CODART " +
            " INNER JOIN dbo.TIPOIVA " +
            " ON dbo.LINEFACT.TIPIVA = dbo.TIPOIVA.TIPIVA " +
            (ventas ? " INNER JOIN CABEFACV ON dbo.LINEFACT.IDFACV = CABEFACV.IDFACV " : " INNER JOIN CABEFACC ON dbo.LINEFACT.IDFACC = CABEFACV.IDFACC ") +
             filtro +
            (ventas ? " ​AND IDFACC IS NULL" : " ​AND IDFACV IS NULL ") +
            orden;

            return script;

        }






    }
}
