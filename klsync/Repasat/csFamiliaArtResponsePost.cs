﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csFamiliaArtResponsePost
    {
        public class Resource
        {
            public string nomFam { get; set; }
            public string codExternoFamilia { get; set; }
            public int idEntidadFam { get; set; }
            public int codFam { get; set; }
            public int idFam { get; set; }
            public int tipoFam { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }

    }
}
