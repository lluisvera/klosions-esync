﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csRemittances
    {
        public class Bank
        {
            public int idDatosBancarios { get; set; }
            public string nomDatosBancarios { get; set; }
            public string titularCuentaDatosBancarios { get; set; }
            public string ibanCuentaDatosBancarios { get; set; }
            public string entidadCuentaDatosBancarios { get; set; }
            public string agenciaCuentaDatosBancarios { get; set; }
            public string dcCuentaDatosBancarios { get; set; }
            public string numCuentaDatosBancarios { get; set; }
            public string bicCuentaDatosBancarios { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaDatosBancarios { get; set; }
            public string cuentaContableDatosBancarios { get; set; }
            public string sufijoRemesaCobro { get; set; }
            public int? efectivoDatosBancarios { get; set; }
            public string codExternoDatosBancarios { get; set; }
        }

        public class Datum
        {
            public int idRemesa { get; set; }
            public int idRefSerie { get; set; }
            public int idEntidadRemesa { get; set; }
            public string fechaGeneracion { get; set; }
            public string fechaCobro { get; set; }
            public int estadoCobro { get; set; }
            public string importeRemesaTotal { get; set; }
            public int idBancoRemesa { get; set; }
            public string gastosRemesa { get; set; }
            public string codExternoRemesa { get; set; }
            public int tipoCarteraRemesa { get; set; }
            public bool sincronizarRemesa { get; set; }
            public string efectos { get; set; }
            public Bank bank { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
