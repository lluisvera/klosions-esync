﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csPurchaseOrders
    {
        public class Series
        {
            public int idSerie { get; set; }
            public string nomSerie { get; set; }
            public int contSerie { get; set; }
            public int contFacturasSerie { get; set; }
            public int contAbonosSerie { get; set; }
            public int contFacturasCompraSerie { get; set; }
            public int contAbonosCompraSerie { get; set; }
            public object contPedidosSerie { get; set; }
            public object contRecibosSerie { get; set; }
            public int serieAplicacionSerie { get; set; }
            public int? autorSerie { get; set; }
            public int contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public int activoSerie { get; set; }
            public int contPedidosVentaSerie { get; set; }
            public int contPedidosCompraSerie { get; set; }
            public int contAlbaranesVentaSerie { get; set; }
            public int contAlbaranesCompraSerie { get; set; }
            public int contAlbaranesRegularizacionSerie { get; set; }
            public int contAnticiposSerie { get; set; }
            public int contTareasSerie { get; set; }
        }

        public class Taxtype
        {
            public int idTipoImpuesto { get; set; }
            public string codTipoImpuesto { get; set; }
            public string nomTipoImpuesto { get; set; }
            public int impuestosTipoImpuesto { get; set; }
            public double recargoEquivalenciaTipoImpuesto { get; set; }
            public string idPais { get; set; }
            public int activoTipoImpuesto { get; set; }
            public object autorTipoImpuesto { get; set; }
            public string fecAltaTipoImpuesto { get; set; }
            public string codExternoTipoImpuesto { get; set; }
        }

        public class Line
        {
            public int idLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int? idArticulo { get; set; }
            public string capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public string descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string descuento2LineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object fecEntregaLineaDocumento { get; set; }
            public string unidadesServidasArticuloLineaDocumento { get; set; }
            public string unidadesAnuladasArticuloLineaDocumento { get; set; }
            public string unidadesPendientesArticuloLineaDocumento { get; set; }
            public int? idLineaPresupuesto { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public int? idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
            public Taxtype taxtype { get; set; }
        }

        public class Account
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public object idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public int? diaPago2Cli { get; set; }
            public object diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public double porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public int? descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
        }

        public class PaymentMethod
        {
            public int idFormaPago { get; set; }
            public string nomFormaPago { get; set; }
            public int codFormaPago { get; set; }
            public int activoFormaPago { get; set; }
            public int? autorFormaPago { get; set; }
            public string fecAltaFormaPago { get; set; }
            public int numVenFormaPago { get; set; }
            public int primerLapsoFormaPago { get; set; }
            public int siguientesLapsosFormaPago { get; set; }
            public int calculoVenFormaPago { get; set; }
            public object repartoProFormaPago { get; set; }
            public int pagadoFormaPago { get; set; }
            public int contratoRepasatTarjetaFormaPago { get; set; }
            public string codExternoFormaPago { get; set; }
        }

        public class PaymentDocument
        {
            public int idDocuPago { get; set; }
            public string nomDocuPago { get; set; }
            public int? idDatosBancarios { get; set; }
            public string descDocuPago { get; set; }
            public int codDocuPago { get; set; }
            public int remesableDocuPago { get; set; }
            public int envioRecDocuPago { get; set; }
            public int efectivoDocuPago { get; set; }
            public string fecAltaDocuPago { get; set; }
            public int? autorDocuPago { get; set; }
            public int activoDocuPago { get; set; }
            public int contratoRepasatTarjetaDocuPago { get; set; }
            public string codExternoDocuPago { get; set; }
        }

        public class DeliveryAddress
        {
            public int idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int? idProvincia { get; set; }
            public string idPais { get; set; }
            public int? idUsuario { get; set; }
            public double? latitudDireccion { get; set; }
            public double? longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
        }

        public class BillingAddress
        {
            public int idDireccion { get; set; }
            public int codDireccion { get; set; }
            public string nomDireccion { get; set; }
            public string direccion1Direccion { get; set; }
            public string direccion2Direccion { get; set; }
            public string cpDireccion { get; set; }
            public string poblacionDireccion { get; set; }
            public int? idProvincia { get; set; }
            public string idPais { get; set; }
            public int? idUsuario { get; set; }
            public double? latitudDireccion { get; set; }
            public double? longitudDireccion { get; set; }
            public string fecAltaDireccion { get; set; }
            public int geolocalizadaDireccion { get; set; }
            public string codExternoDireccion { get; set; }
        }

        public class Carrier
        {
            public int idTransportista { get; set; }
            public int idEntidadTransportista { get; set; }
            public string nomTransportista { get; set; }
            public int codTransportista { get; set; }
            public int? autorTransportista { get; set; }
            public int activoTransportista { get; set; }
            public string fecAltaTransportista { get; set; }
            public string codExternoTransportista { get; set; }
        }

        public class Taxoperation
        {
            public int? idRegimenImpuesto { get; set; }
            public string nomRegimenImpuesto { get; set; }
            public int recargoEquivalenciaRegimenImpuesto { get; set; }
            public int igicRegimenImpuesto { get; set; }
            public int ivaRegimenImpuesto { get; set; }
            public int exentoRegimenImpuesto { get; set; }
            public int noSujetoRegimenImpuesto { get; set; }
            public int intracomunitarioRegimenImpuesto { get; set; }
            public object autorRegimenImpuesto { get; set; }
            public string fecAltaRegimenImpuesto { get; set; }
            public string codExternoRegimenesImpuestos { get; set; }
        }

        public class Employee
        {
            public int idTrabajador { get; set; }
            public int? idResponsable { get; set; }
            public int codTrabajador { get; set; }
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string aliasTrabajador { get; set; }
            public string nifTrabajador { get; set; }
            public object idResponsableExt { get; set; }
            public string tipoRecursoTrabajador { get; set; }
            public string sexoTrabajador { get; set; }
            public int? fotoTrabajador { get; set; }
            public int? idDpto { get; set; }
            public int? idUsuario { get; set; }
            public string fecCadNif { get; set; }
            public object numSegSoc { get; set; }
            public string telf1 { get; set; }
            public object telf2 { get; set; }
            public string emailTrabajador { get; set; }
            public string fecNac { get; set; }
            public object idEstadoCivil { get; set; }
            public object numHijos { get; set; }
            public object carnetConducirTrabajador { get; set; }
            public string paisNac { get; set; }
            public int? idTarifa { get; set; }
            public object idTurnos { get; set; }
            public int? idCalendario { get; set; }
            public string fecAntig { get; set; }
            public int? categoriaTrabajador { get; set; }
            public double? bolsaHorasTrabajador { get; set; }
            public int activoTrabajador { get; set; }
            public string fecAltaTrabajador { get; set; }
            public object fecBajaTrabajador { get; set; }
            public string colorCalendarioTrabajador { get; set; }
            public string prefTelf { get; set; }
            public object gradoDiscapacidad { get; set; }
            public object email2Trabajador { get; set; }
            public object cuentaContableTrabajador { get; set; }
            public object rangoSueldoTrabajador { get; set; }
            public string codExternoTrabajador { get; set; }
            public string nomCompleto { get; set; }
        }

        public class Datum
        {
            public int idDocumento { get; set; }
            public int idCli { get; set; }
            public object idCliPot { get; set; }
            public int? idProyecto { get; set; }
            public int? idDireccionEnvio { get; set; }
            public int idDireccionFacturacion { get; set; }
            public int idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public string refDocumento { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public int? idCam { get; set; }
            public double totalBaseImponibleDocumento { get; set; }
            public double totalImpuestosDocumento { get; set; }
            public double totalDocumento { get; set; }
            public object totalPagadoDocumento { get; set; }
            public object domiciliacionBancariaDocumento { get; set; }
            public int? idTrabajador { get; set; }
            public string fecDocumento { get; set; }
            public object fecCierreDocumento { get; set; }
            public object fecCaducidadDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public int? idUsuario { get; set; }
            public object fecEntregaDocumento { get; set; }
            public double portesDocumento { get; set; }
            public int? idTipoImpuestoPortes { get; set; }
            public double totalImpuestosPortesDocumento { get; set; }
            public double totalPortesDocumento { get; set; }
            public int? idTransportista { get; set; }
            public string servidoDocumento { get; set; }
            public int porcentajeDescuentoDocumento { get; set; }
            public int? idTarifa { get; set; }
            public int? idAlmacen { get; set; }
            public int idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public string totalRecargoEquivalenciaDocumento { get; set; }
            public string totalRetencionesDocumento { get; set; }
            public string totalBaseRetencionesDocumento { get; set; }
            public string totalRecargoEquivalenciaPortesDocumento { get; set; }
            public object idRepresentado { get; set; }
            public string observacionesCabeceraDocumento { get; set; }
            public string observacionesPieDocumento { get; set; }
            public int? idTipoImpuesto { get; set; }
            public string tipoGestion { get; set; }
            public int? codExternoDocumento { get; set; }
            public bool sincronizarDocumento { get; set; }
            public int? clienteGenerico { get; set; }
            public int? direccionGenerica { get; set; }
            public string razonSocial { get; set; }
            public string nomCli { get; set; }

            public string cifCli { get; set; }

            //Direccion Fiscal
            public string nomDirCli { get; set; }
            public string direccion1Cli { get; set; }
            public string direccion2Cli { get; set; }
            public string cpCli { get; set; }
            public string poblacionCli { get; set; }
            public string idProvinciaCli { get; set; }
            public string idPaisCli { get; set; }
            //Direccion Envio
            public string nomCliEnt { get; set; }
            public string direccion1CliEnt { get; set; }
            public string direccion2CliEnt { get; set; }
            public string cpCliEnt { get; set; }
            public string poblacionCliEnt { get; set; }
            public string idProvinciaCliEnt { get; set; }
            public string idPaisCliEnt { get; set; }

            public Series series { get; set; }
            public List<Line> lines { get; set; }
            public Account account { get; set; }
            public PaymentMethod payment_method { get; set; }
            public PaymentDocument payment_document { get; set; }
            public DeliveryAddress delivery_address { get; set; }
            public BillingAddress billing_address { get; set; }
            public Carrier carrier { get; set; }
            public Taxoperation taxoperation { get; set; }
            public Employee employee { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }

    }
}
