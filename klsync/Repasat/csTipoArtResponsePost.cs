﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csTipoArtResponsePost
    {
        public class Resource
        {
            public string nomTipoArticulo { get; set; }
            public string codExternoTipoArticulo { get; set; }
            public int idEntidadTipoArticulo { get; set; }
            public int codTipoArticulo { get; set; }
            public int idTipoArticulo { get; set; }
            
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }

        }

    }
}
