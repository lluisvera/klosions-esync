﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csZonasGeoResponsePost
    {
        public class Resource
        {
            public string nomZonaGeo { get; set; }
            public string activoZonaGeo { get; set; }
            public int idEntidadZonaGeo { get; set; }
            public int codZonaGeo { get; set; }
            public int idZonaGeo { get; set; }
            public string codExternoZonaGeo { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
