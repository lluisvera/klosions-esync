﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csPurchaseOrdersPost
    {

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Resource
        {
            public int idDocumento { get; set; }
            public int idCli { get; set; }
            public object idCliPot { get; set; }
            public object idProyecto { get; set; }
            public object idDireccionEnvio { get; set; }
            public int idDireccionFacturacion { get; set; }
            public int idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public object refDocumento { get; set; }
            public int idDocuPago { get; set; }
            public int idFormaPago { get; set; }
            public object idCam { get; set; }
            public double totalBaseImponibleDocumento { get; set; }
            public double totalImpuestosDocumento { get; set; }
            public double totalDocumento { get; set; }
            public object totalPagadoDocumento { get; set; }
            public object domiciliacionBancariaDocumento { get; set; }
            public object idTrabajador { get; set; }
            public string fecDocumento { get; set; }
            public object fecCierreDocumento { get; set; }
            public object fecCaducidadDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public object idUsuario { get; set; }
            public object fecEntregaDocumento { get; set; }
            public Double portesDocumento { get; set; }
            public object idTipoImpuestoPortes { get; set; }
            public Double totalImpuestosPortesDocumento { get; set; }
            public Double totalPortesDocumento { get; set; }
            public object idTransportista { get; set; }
            public string servidoDocumento { get; set; }
            public int porcentajeDescuentoDocumento { get; set; }
            public object idTarifa { get; set; }
            public object idAlmacen { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public Double totalRecargoEquivalenciaDocumento { get; set; }
            public Double totalRetencionesDocumento { get; set; }
            public Double totalBaseRetencionesDocumento { get; set; }
            public Double totalRecargoEquivalenciaPortesDocumento { get; set; }
            public object idRepresentado { get; set; }
            public object observacionesCabeceraDocumento { get; set; }
            public object observacionesPieDocumento { get; set; }
            public int? idTipoImpuesto { get; set; }
            public object tipoGestion { get; set; }
            public object codExternoDocumento { get; set; }
            public object idTarifaDescuentos { get; set; }
            public string totalCosteDocumento { get; set; }
            public string margenComercialDocumento { get; set; }
            public object idContacto { get; set; }

        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }


    }
}
