﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csProjects
    {
        public class Customer
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string tel2 { get; set; }
            public string fax { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int? idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int? activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int diaPago1Cli { get; set; }
            public int diaPago2Cli { get; set; }
            public int diaPago3Cli { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public string porcentajeDescuentoCli { get; set; }
            public int? idTarifa { get; set; }
            public int? idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public string cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public int? idTipoRetencion { get; set; }
            public int? idEmpresaAccesoExterno { get; set; }
            public int? idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public object representadoCli { get; set; }
            public string comisionCli { get; set; }
            public string descuentoCli { get; set; }
            public int? idFormatoImpresionPresupuesto { get; set; }
            public int? idFormatoImpresionPedidoVenta { get; set; }
            public int? idFormatoImpresionPedidoCompra { get; set; }
            public int? idFormatoImpresionAlbaranVenta { get; set; }
            public int? idFormatoImpresionAlbaranCompra { get; set; }
            public int? idFormatoImpresionFacturaVenta { get; set; }
            public int? idFormatoImpresionFacturaCompra { get; set; }
            public string tipoEnvioCorreo { get; set; }
            public int? idRuta { get; set; }
            public string fecConversionCli { get; set; }
            public string fecModificacionCli { get; set; }
            public int? idTarifaDescuentos { get; set; }
            public int? idSector { get; set; }
            public int? idMotivoBaja { get; set; }
            public int? idTamanoEmpresa { get; set; }
        }

        public class Pivot
        {
            public int idProyecto { get; set; }
            public int idTrabajador { get; set; }
        }

        public class Employee
        {
            public int idTrabajador { get; set; }
            public object idResponsable { get; set; }
            public int codTrabajador { get; set; }
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string aliasTrabajador { get; set; }
            public string nifTrabajador { get; set; }
            public int? idResponsableExt { get; set; }
            public string tipoRecursoTrabajador { get; set; }
            public string sexoTrabajador { get; set; }
            public int? idDpto { get; set; }
            public int? idUsuario { get; set; }
            public string fecCadNif { get; set; }
            public string numSegSoc { get; set; }
            public string telf1 { get; set; }
            public string telf2 { get; set; }
            public string emailTrabajador { get; set; }
            public string fecNac { get; set; }
            public int? idEstadoCivil { get; set; }
            public string numHijos { get; set; }
            public object carnetConducirTrabajador { get; set; }
            public object paisNac { get; set; }
            public object idTarifa { get; set; }
            public object idTurnos { get; set; }
            public object idCalendario { get; set; }
            public object fecAntig { get; set; }
            public object categoriaTrabajador { get; set; }
            public double? bolsaHorasTrabajador { get; set; }
            public int activoTrabajador { get; set; }
            public string fecAltaTrabajador { get; set; }
            public object fecBajaTrabajador { get; set; }
            public object colorCalendarioTrabajador { get; set; }
            public object prefTelf { get; set; }
            public object gradoDiscapacidad { get; set; }
            public object email2Trabajador { get; set; }
            public object cuentaContableTrabajador { get; set; }
            public object rangoSueldoTrabajador { get; set; }
            public string codExternoTrabajador { get; set; }
            public int turnoManana { get; set; }
            public int turnoTarde { get; set; }
            public int turnoNoche { get; set; }
            public object numRegistroTrabajador { get; set; }
            public string nomCompleto { get; set; }
            public Pivot pivot { get; set; }
        }

        public class Datum
        {
            public int? idProyecto { get; set; }
            public int? idEntidadProyecto { get; set; }
            public int? codProyecto { get; set; }
            public string nomProyecto { get; set; }
            public int? idProyectoPadre { get; set; }
            public int? idCli { get; set; }
            public int enSeguimientoProyecto { get; set; }
            public object idCalendario { get; set; }
            public string fecIniProyecto { get; set; }
            public string fecFinProyecto { get; set; }
            public int? idTarifa { get; set; }
            public string docRef { get; set; }
            public string jefeProyecto { get; set; }
            public string descProyecto { get; set; }
            public string telf1 { get; set; }
            public string contacto { get; set; }
            public string centroCoste { get; set; }
            public string responsable { get; set; }
            public int visibleAsociadoProyecto { get; set; }
            public string codExternoProyecto { get; set; }
            public int activoProyecto { get; set; }
            public string fecAltaProyecto { get; set; }
            public string fecBajaProyecto { get; set; }
            public int? idCuentaCertificador { get; set; }
            public string porcentajeDescuentoProyecto { get; set; }
            public int? idTipoProyecto { get; set; }
            public int? idEstado { get; set; }
            public Customer customer { get; set; }
            public List<Employee> employees { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
