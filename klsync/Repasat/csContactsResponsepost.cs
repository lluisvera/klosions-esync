﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csContactsResponsepost
    {
        public class Resource
        {
            public int idContacto { get; set; }
            public string nombreContacto { get; set; }
            public string apellidosContacto { get; set; }
            public string telf1Contacto { get; set; }
            public string extensionTelf1Contacto { get; set; }
            public string telf2Contacto { get; set; }
            public string extensionTelf2Contacto { get; set; }
            public string emailContacto { get; set; }
            public string email2Contacto { get; set; }
            public string cargoContacto { get; set; }
            public int idUsuario { get; set; }
            public string fecNacContacto { get; set; }
            public string parejaContacto { get; set; }
            public string facebookContacto { get; set; }
            public string twitterContacto { get; set; }
            public string linkedinContacto { get; set; }
            public string skypeContacto { get; set; }
            public string comentsContacto { get; set; }
            public string fecAltaContacto { get; set; }
            public int facturacionContacto { get; set; }
            public string codExternoContacto { get; set; }
            public string fecModificacionContacto { get; set; }
            public string nombreCompleto { get; set; }
            public List<object> customers { get; set; }
            public List<object> leads { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
