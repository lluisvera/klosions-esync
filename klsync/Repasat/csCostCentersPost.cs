﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csCostCentersPost
    {

        public class Resource
        {
            public string uuidCentroCoste { get; set; }
            public string codCentrosCoste { get; set; }
            public int autorCentroCoste { get; set; }
            public string descCentroCoste { get; set; }
            public string fecAltaCentroCoste { get; set; }
            public int level1 { get; set; }
            public int level2 { get; set; }
            public int level3 { get; set; }
            public string codExternoCentroC { get; set; }
            public object idUnidadNegocio { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
      
        }
    }
}
