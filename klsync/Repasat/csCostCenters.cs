﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csCostCenters
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
        public class Datum
        {
            public string uuidCentroCoste { get; set; }
            public string codCentrosCoste { get; set; }
            public int autorCentroCoste { get; set; }
            public string descCentroCoste { get; set; }
            public string refCentroCoste { get; set; }
            public string fecAltaCentroCoste { get; set; }
            public int level1 { get; set; }
            public int level2 { get; set; }
            public int level3 { get; set; }
            public string codExternoCentroC { get; set; }
            public object idUnidadNegocio { get; set; }
            public string fecModificacion { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
