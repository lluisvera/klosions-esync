﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Repasat
{
    class csEmptyResponse
    {
        public class Resource
        {
            public string codExternoArticulo { get; set; }
            public string refArticulo { get; set; }
            public string nomArticulo { get; set; }
            public string precioVentaArticulo { get; set; }
            public string precioCompraArticulo { get; set; }
            public string compraArticulo { get; set; }
            public string ventaArticulo { get; set; }
            public string activoArticulo { get; set; }
            public string stockArticulo { get; set; }
            public int codArticulo { get; set; }
            public int idArticulo { get; set; }
        }

        public class Data
        {
            public Resource resource { get; set; }
        }

        public class RootObject
        {
            public bool status { get; set; }
            public Data data { get; set; }
        }
    }
}
