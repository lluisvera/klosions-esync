﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csFamiliaArt
    {
        public class Datum
        {
            public int idFam { get; set; }
            public int idEntidadFam { get; set; }
            public string nomFam { get; set; }
            public int codFam { get; set; }            
            public int activoFam { get; set; }
            public int tipoFam { get; set; }
            public int? idPadreFam { get; set; }
            public int? autorFam { get; set; }
            public string codExternoFamilia { get; set; }
            public string fecSincronizacionFam { get; set; }

        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public object next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
