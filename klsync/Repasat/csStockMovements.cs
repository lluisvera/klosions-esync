﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Repasat
{
    class csStockMovements
    {

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Warehouse
        {
            public int idAlmacen { get; set; }
            public int idEntidadAlmacen { get; set; }
            public string refAlmacen { get; set; }
            public string nomAlmacen { get; set; }
            public object idContacto { get; set; }
            public object idDireccion { get; set; }
            public string fecAltaAlmacen { get; set; }
            public int activoAlmacen { get; set; }
            public object codExternoAlmacen { get; set; }

        }

        public class Product
        {
            public int idArticulo { get; set; }
            public int codArticulo { get; set; }
            public string nomArticulo { get; set; }
            public string refArticulo { get; set; }
            public string descArticulo { get; set; }
            public int? idTipoArticulo { get; set; }
            public int? fotoArticulo { get; set; }
            public double precioVentaArticulo { get; set; }
            public double precioCompraArticulo { get; set; }
            public int cantidadArticulo { get; set; }
            public int activoArticulo { get; set; }
            public string codExternoArticulo { get; set; }
            public string fecAltaArticulo { get; set; }
            public object fecBajaArticulo { get; set; }
            public int? idFabricante { get; set; }
            public int? idFam { get; set; }
            public int? idSubFam { get; set; }
            public int? idUni { get; set; }
            public object idArtAlt { get; set; }
            public int? idResp { get; set; }
            public int? bloqueadoArticulo { get; set; }
            public string motivoArticulo { get; set; }
            public int? compraArticulo { get; set; }
            public int? ventaArticulo { get; set; }
            public int? stockArticulo { get; set; }
            public int? planificableArticulo { get; set; }
            public object pesoArticulo { get; set; }
            public object volumenArticulo { get; set; }
            public object descuentoMaxArticulo { get; set; }
            public object codigobarrasArticulo { get; set; }
            public string preciocostecompraArticulo { get; set; }
            public int? fechaentregacompraArticulo { get; set; }
            public object fechaentregaventaArticulo { get; set; }
            public string alarmapresucompraArticulo { get; set; }
            public string alarmapresuventaArticulo { get; set; }
            public object alarmapedidoscompraArticulo { get; set; }
            public string alarmapedidosventaArticulo { get; set; }
            public string alarmaalbaranescompraArticulo { get; set; }
            public string alarmaalbaranesventaArticulo { get; set; }
            public string alarmafacturascompraArticulo { get; set; }
            public string alarmafacturasventaArticulo { get; set; }
            public object largoArticulo { get; set; }
            public object anchoArticulo { get; set; }
            public object gruesoArticulo { get; set; }
            public object embalajeArticulo { get; set; }
            public object embalajesPorCaja { get; set; }
            public object unidadesPorPale { get; set; }
            public string referenciaProveedorArticulo { get; set; }
            public string aliasArticulo { get; set; }
            public int? idTipoImpuesto { get; set; }
            public object cuentaContableCompraArticulo { get; set; }
            public object cuentaContableVentaArticulo { get; set; }
            public int? idTipoImpuestoCompra { get; set; }
            public int tieneNumSerieArticulo { get; set; }
            public string fecModificacionArticulo { get; set; }
            public object idCuentaContableVenta { get; set; }
            public object idCuentaContableCompra { get; set; }
            public object idUbicacion { get; set; }
            public int esEpi { get; set; }
            public object fecSincronizacionArticulo { get; set; }
            public object idRepresentado { get; set; }
            public object urlExternaArticulo { get; set; }
            public int paraNotaGasto { get; set; }

        }

        public class SaleDeliveryNoteLine
        {
            public int idLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int idArticulo { get; set; }
            public object capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public string descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object idLineaPresupuesto { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public string unidadesPendientesArticuloLineaDocumento { get; set; }
            public string unidadesServidasArticuloLineaDocumento { get; set; }
            public string unidadesAnuladasArticuloLineaDocumento { get; set; }
            public int idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object idActividad { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
            public int tieneNumSerieArticuloLineaDocumento { get; set; }
            public string descuento2LineaDocumento { get; set; }
            public string descuento3LineaDocumento { get; set; }
            public string costeLineaDocumento { get; set; }
            public string descuentoCosteLineaDocumento { get; set; }
            public string totalCosteLineaDocumento { get; set; }
            public string observacionesLineaDocumento { get; set; }
            public string margenComercialLineaDocumeto { get; set; }
            public object idCuentaContable { get; set; }
            public object comision1LineaDocumento { get; set; }
            public object totalComision1LineaDocumento { get; set; }
            public object comision2LineaDocumento { get; set; }
            public object totalComision2LineaDocumento { get; set; }
            public object comision3LineaDocumento { get; set; }
            public object totalComision3LineaDocumento { get; set; }
            public object idLineaNotaGasto { get; set; }

        }

        public class PurchaseDeliveryNoteLine
        {
            public int idLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int idArticulo { get; set; }
            public object idContratoClienteDocumento { get; set; }
            public object capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public string descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public string unidadesPendientesArticuloLineaDocumento { get; set; }
            public string unidadesServidasArticuloLineaDocumento { get; set; }
            public string unidadesAnuladasArticuloLineaDocumento { get; set; }
            public int idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object idProyecto { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
            public int tieneNumSerieArticuloLineaDocumento { get; set; }
            public string descuento2LineaDocumento { get; set; }
            public string descuento3LineaDocumento { get; set; }
            public string costeLineaDocumento { get; set; }
            public string descuentoCosteLineaDocumento { get; set; }
            public string totalCosteLineaDocumento { get; set; }
            public string observacionesLineaDocumento { get; set; }
            public string margenComercialLineaDocumeto { get; set; }
            public object idCuentaContable { get; set; }
            public object idLineaNotaGasto { get; set; }

        }

        public class PurchaseInvoiceLine
        {
            public int idLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int idArticulo { get; set; }
            public object capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public object idTipoArticulo { get; set; }
            public object descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object idDocumentoOrigen { get; set; }
            public object idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public int idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object idProyecto { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
            public int tieneNumSerieArticuloLineaDocumento { get; set; }
            public string descuento2LineaDocumento { get; set; }
            public string descuento3LineaDocumento { get; set; }
            public string costeLineaDocumento { get; set; }
            public string descuentoCosteLineaDocumento { get; set; }
            public string totalCosteLineaDocumento { get; set; }
            public string observacionesLineaDocumento { get; set; }
            public string margenComercialLineaDocumeto { get; set; }
            public object idCuentaContable { get; set; }
            public object idLineaNotaGasto { get; set; }

        }

        public class StockRegularization
        {
            public int idDocumento { get; set; }
            public int idEntidadDocumento { get; set; }
            public int idSerie { get; set; }
            public string fecDocumento { get; set; }
            public int idAlmacen { get; set; }
            public string comentariosDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public int numSerieDocumento { get; set; }
            public object idAlmacenDestino { get; set; }
            public string tipoDocumento { get; set; }

        }

        public class Datum
        {
            public int idMovimientoStock { get; set; }
            public string fecMovimientoStock { get; set; }
            public int idArticulo { get; set; }
            public int idAlmacen { get; set; }
            public int idEntidadMovimientoStock { get; set; }
            public string tipoMovimientoStock { get; set; }
            public double unidades1MovimientoStock { get; set; }
            public double unidades2MovimientoStock { get; set; }
            public object idLineaAlbaranVenta { get; set; }
            public object idLineaAlbaranCompra { get; set; }
            public object idLineaFacturaVenta { get; set; }
            public object idLineaFacturaCompra { get; set; }
            public object idAlbaranRegularizacion { get; set; }
            public int idActividad { get; set; }
            public object codExternoMovimientoStock { get; set; }
            public Warehouse warehouse { get; set; }
            public Product product { get; set; }
            public object sale_delivery_note_line { get; set; }
            public object purchase_delivery_note_line { get; set; }
            public object sale_invoice_line { get; set; }
            public object purchase_invoice_line { get; set; }
            public object stock_regularization { get; set; }

        }

        public class RootObject
        {
            public bool status { get; set; }
            public int total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }

        }


    }
}
