﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Set the TextBox's PasswordChar property
// to X or something at design time.

namespace klsync
{
    public partial class frDialogPassword : Form
    {
        public frDialogPassword()
        {
            InitializeComponent();
        }

        // Validate the password.
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text == "klosions2014" || txtPassword.Text.ToUpper()==csGlobal.customerPassword.ToUpper())
            {
                // The password is ok.
                //MessageBox.Show("Permisos administración concedidos");
                this.DialogResult = DialogResult.OK;
                csGlobal.perfilAdminEsync = true;
            }
            else
            {
                // The password is invalid.
                txtPassword.Clear();
                txtPassword.Focus();
                MessageBox.Show("Permiso denegado");
            }
        }
    }
}
