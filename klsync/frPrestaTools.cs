﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frPrestaTools : Form
    {
        csSqlConnects sql = new csSqlConnects();
        private static frPrestaTools m_FormDefInstance;
        public static frPrestaTools DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPrestaTools();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frPrestaTools()
        {
            InitializeComponent();
        }

        private void btCargarArticulos_Click(object sender, EventArgs e)
        {
            cargarArticulosInfoPS();
        }

        private string filtroDescripcion()
        {

            string filtroDescripcion = "";
            filtroDescripcion = " and (ps_product_lang.name like '%" + tstbNombreArticulo.Text + "%' or ps_product.reference like '%" + tstbNombreArticulo.Text + "%' ";
            return filtroDescripcion;
        }

        private bool selectConCaracteristicas()
        {
            bool activo = false;

            if (tscboxFeatures.SelectedItem.ToString() == "SI")
            {
                activo = true;
            }
            return activo;
        }







        private string filtroMarcas()
        {
            string filtroMarcas = "";
            string[] nomMarca;
            string id_manufacturer = "";
            if (clboxMarcasFilter.CheckedItems.Count > 0)
            {
                for (int i = 0; i < clboxMarcasFilter.CheckedItems.Count; i++)
                {
                    if (i > 0)
                    {
                        filtroMarcas = filtroMarcas + ",";
                    }

                    id_manufacturer = clboxMarcasFilter.CheckedItems[i].ToString();
                    nomMarca = id_manufacturer.Split(new Char[] { '-' });
                    filtroMarcas += nomMarca[1];
                }
                filtroMarcas = " and ps_manufacturer.id_manufacturer in (" + filtroMarcas + ") ";
            }

            return filtroMarcas;


        }

        private string filtroCategorias()
        {
            string filtroCategorias = "";
            string categoria = "";
            string[] nomCategoria;
            if (clboxCategoriasFilter.CheckedItems.Count > 0)
            {
                for (int i = 0; i < clboxCategoriasFilter.CheckedItems.Count; i++)
                {
                    if (i > 0)
                    {
                        filtroCategorias = filtroCategorias + ",";
                    }
                    categoria = clboxCategoriasFilter.CheckedItems[i].ToString();
                    nomCategoria = categoria.Split(new Char[] { '-' });
                    filtroCategorias = filtroCategorias + "'" + nomCategoria[1] + "'";

                }
                filtroCategorias = " and ps_category_lang.id_category in (" + filtroCategorias + ") ";
            }

            return filtroCategorias;
        }

        private string filtroCaracteristica()
        {

            string filtroCaracteristicas = "";
            string caracteristica = "";
            string[] nomCategoria;
            if (clboxCaracteristicasFilter.CheckedItems.Count > 0)
            {
                for (int i = 0; i < clboxCaracteristicasFilter.CheckedItems.Count; i++)
                {
                    if (i > 0)
                    {
                        filtroCaracteristicas = filtroCaracteristicas + ",";
                    }
                    caracteristica = clboxCaracteristicasFilter.CheckedItems[i].ToString();
                    nomCategoria = caracteristica.Split(new Char[] { '-' });
                    filtroCaracteristicas = filtroCaracteristicas + "'" + nomCategoria[3] + "'";

                }
                filtroCaracteristicas = " and ps_feature_value_lang.id_feature_value in (" + filtroCaracteristicas + ") ";
            }

            return filtroCaracteristicas;

        }




        private void cargarArticulosInfoPS()
        {
            try
            {
                //bool peso = (rbPesoSi.Checked) ? true : false;
                bool peso = (tscboxPeso.Text=="SI") ? true : false;
                csMySqlConnect mysql = new csMySqlConnect();
                // MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
                DataTable table = new DataTable();
                if (csGlobal.isODBCConnection)
                {
                    OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    csSqlScripts mySqlScript = new csSqlScripts();
                    OdbcDataAdapter MyDA = new OdbcDataAdapter();

                    MyDA.SelectCommand = new OdbcCommand(mySqlScript.selectArticulosInfo(filtroDescripcion(), filtroMarcas(), filtroCategorias(), filtroCaracteristica(), selectConCaracteristicas(), peso, tstbNombreArticulo.Text), Odbcconnection);

                    MyDA.Fill(table);
                }
                else
                {
                    MySqlConnection conn = new MySqlConnection(csGlobal.cadenaConexionPS);
                    conn.Open();

                    csSqlScripts mySqlScript = new csSqlScripts();
                    MySqlDataAdapter MyDA = new MySqlDataAdapter();

                    MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectArticulosInfo(filtroDescripcion(), filtroMarcas(), filtroCategorias(), filtroCaracteristica(), selectConCaracteristicas(), peso, tstbNombreArticulo.Text), conn);

                    MyDA.Fill(table);
                }

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvArticulos.DataSource = bSource;
                tstbNombreArticulo.Focus();
            }
            catch (Exception ex)
            {
            }
        }



        private void cargarMarcasPS()
        {
            string marca = "";
            csMySqlConnect mysql = new csMySqlConnect();            
            DataTable table = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                OdbcDataAdapter MyDA = new OdbcDataAdapter();

                MyDA.SelectCommand = new OdbcCommand(mySqlScript.selectFabricantesPS(), Odbcconnection);

                MyDA.Fill(table);
            }
            else
            {
                MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
                conn.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectFabricantesPS(), conn);

                MyDA.Fill(table);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                marca = table.Rows[i].ItemArray[1].ToString() + "-" + table.Rows[i].ItemArray[0].ToString();
                clboxMarcasFilter.Items.Add(table.Rows[i].ItemArray[1].ToString() + "-" + table.Rows[i].ItemArray[0].ToString());
                clboxMarcas.Items.Add(marca);
            }
        }

        private void cargarCategoriasPS()
        {
            clboxCategoriasFilter.Items.Clear();
            clboxCategorias.Items.Clear();
            string categoria = "";

            csMySqlConnect mysql = new csMySqlConnect();
            DataTable table = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                OdbcDataAdapter MyDA = new OdbcDataAdapter();

                MyDA.SelectCommand = new OdbcCommand(mySqlScript.selectCategoriasPS(cboxOrderByID.Checked), Odbcconnection);

                MyDA.Fill(table);
            }
            else
            {
                MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
                conn.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectCategoriasPS(cboxOrderByID.Checked), conn);

                MyDA.Fill(table);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                categoria = table.Rows[i].ItemArray[1].ToString() + "-" + table.Rows[i].ItemArray[0].ToString();
                //clboxCategoriasFilter.Items.Add(table.Rows[i].ItemArray[1].ToString());
                clboxCategoriasFilter.Items.Add(categoria);
                clboxCategorias.Items.Add(categoria);

            }


        }

        private void cargarCaracteristicasPS()
        {
            string caracteristica = "";

            csMySqlConnect mysql = new csMySqlConnect();
            DataTable table = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                OdbcDataAdapter MyDA = new OdbcDataAdapter();

                MyDA.SelectCommand = new OdbcCommand(mySqlScript.selectCaracteristicasPS(), Odbcconnection);

                MyDA.Fill(table);
            }
            else
            {
                MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());

                conn.Open();

                csSqlScripts mySqlScript = new csSqlScripts();
                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectCaracteristicasPS(), conn);

                MyDA.Fill(table);
            }


            for (int i = 0; i < table.Rows.Count; i++)
            {
                caracteristica = table.Rows[i].ItemArray[0].ToString() + "|" + table.Rows[i].ItemArray[1].ToString() + "|" + table.Rows[i].ItemArray[3].ToString() + "|" + table.Rows[i].ItemArray[2].ToString();
                clboxCaracteristicasFilter.Items.Add(caracteristica);
                clboxCaracteristicas.Items.Add(caracteristica);
            }
        }

        private void frPrestaTools_Load(object sender, EventArgs e)
        {
            cargarMarcasPS();
            cargarCategoriasPS();
            cargarCaracteristicasPS();
        }

        private void btDesmarcarFiltroCategorias_Click(object sender, EventArgs e)
        {
            desmarcarChecksListBox(clboxCategoriasFilter);
            
        }

        private void btDesmarcarFiltroMarcas_Click(object sender, EventArgs e)
        {
            desmarcarChecksListBox(clboxMarcasFilter);
        }

        private void DesmarcarFiltroCaracteristicas()
        {
            desmarcarChecksListBox(clboxCaracteristicasFilter);
        }

        private void AsignarCategorias()
        {
            if (dgvArticulos.SelectedRows.Count > 0 && clboxCategorias.CheckedItems.Count>0)
            {
                csRepLog rl = new csRepLog();

                string categoriaAsignada = "";
                string[] seleccion;
                string categoria = "";
               
                csMySqlConnect conectorMySql = new csMySqlConnect();
                string valores = "";

                //Explicar el check
                bool check = false;
                
                //Itero cada línea de producto
                for (int i = 0; i < dgvArticulos.SelectedRows.Count; i++)
                {
                    //Obtengo la referencia o código de A3ERP
                    string reference = dgvArticulos.SelectedRows[i].Cells["reference"].Value.ToString();

                    for (int ii=0; ii<clboxCategorias.CheckedItems.Count; ii++)
                    {
                        categoriaAsignada = clboxCategorias.CheckedItems[ii].ToString();
                        seleccion = categoriaAsignada.Split('-');
                        categoria = seleccion[1];

                        if (csGlobal.A3ERPConnection == "Y")
                        {
                            if (!sql.consultaExiste("SELECT KLS_COD_PS FROM  KLS_CATEGORIAS WHERE KLS_COD_PS=" + categoria))
                            {
                                string id_category = categoria;
                                string name = seleccion[0];
                                string id_parent = "";
                                csUtilidades.ejecutarConsulta("insert into kls_categorias (KLS_COD_PS, KLS_CODCATEGORIA, KLS_DESCCATEGORIA, KLS_ID_CATPARENT) " +
                                            " VALUES ('" + id_category + "','" + id_category + "','" + name + "', '" + id_parent + "')", false);
                            }
                        }
                        //Para cada categoría marcada en el ListBox verificamos si existe en PS

                        if (!isCategoryProductExist(categoria, dgvArticulos.SelectedRows[i].Cells["id_product"].Value.ToString()))
                        {
                            if (ii > 0 && check)
                            {
                                valores = valores + ",";
                            }

                            check = true;
                            valores = valores + "(" + categoria + "," + dgvArticulos.SelectedRows[i].Cells["id_product"].Value.ToString() + ",0)";

                            //Primero miramos si existe la categoría en A3ERP que vamos a modificar

                            // Actualizamos en A3

                            //Añadido para poder utilizar Esync sin A3ERP
                            if (csGlobal.A3ERPConnection == "Y")
                            {
                                string codart = "(SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + reference + "')";
                                csUtilidades.ejecutarConsulta("INSERT KLS_CATEGORIAS_ART VALUES (" + codart + ", '" + categoria + "', NULL)", false);
                                rl.insertarRegistroRegLog(reference, "ARTICULO", rl.MOD);
                            }
                            //Actualizamos en PS
                            
                        }
                    }
                    if (valores.Length > 0)
                    {
                        conectorMySql.InsertValoresEnTabla("ps_category_product", "(id_category, id_product,position)", valores, "", true, false);
                        valores = "";
                    }
                }
                //if (clboxCategorias.CheckedItems.Count > 0 && check)
                //{
                    
                //}
            }
        }

        private bool isCategoryProductExist(string id_category, string id_product)
        {
            csMySqlConnect mysql = new csMySqlConnect();

            if (mysql.existeTabla("ps_category_product where id_category = " + id_category + " and id_product = " + id_product))
            {
                return true;
            }

            return false;
        }


        private void asignarMarcas(bool quitarMarca)
        {
            if (dgvArticulos.SelectedRows.Count > 0)
            {
                csRepLog rl = new csRepLog();

                string marcaAsignada = "";
                string articulo = "", reference = "";
                string[] seleccion;
                string marca = "";
                marcaAsignada = clboxMarcas.CheckedItems[0].ToString();
                seleccion = marcaAsignada.Split('-');
                marca = seleccion[1];
                csMySqlConnect conectorMySql = new csMySqlConnect();

                if (clboxMarcas.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < dgvArticulos.SelectedRows.Count; i++)
                    {
                        articulo = dgvArticulos.SelectedRows[i].Cells["id_product"].Value.ToString();
                        reference = dgvArticulos.SelectedRows[i].Cells["reference"].Value.ToString();
                        if (quitarMarca)
                        {
                            marca = "0";
                        }
                        conectorMySql.asignarMarcaProducto(marca, articulo);

                        // Actualizamos A3
                        csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_CODMARCA = '" + marca + "' WHERE LTRIM(CODART) = '" + reference + "'", false);
                        rl.insertarRegistroRegLog(reference, "ARTICULO", rl.MOD);
                    }
                }
            }
            else
            {
                //MessageBox.Show("No hay filas seleccionadas para actualizar");        
            }


        }


        private void AsignarCaracteristicas()
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();
                if (dgvArticulos.SelectedRows.Count > 0 && clboxCaracteristicas.CheckedItems.Count > 0)
                {
                    foreach (DataGridViewRow fila in dgvArticulos.SelectedRows)
                    {
                        for (int i = 0; i < clboxCaracteristicas.Items.Count; i++)
                        {
                            if (clboxCaracteristicas.GetItemChecked(i))
                            {
                                string caracteristicaAsignada = "";
                                string[] seleccion;
                                string caracteristica = "";
                                string valorCaracteristica = "";
                                string articulo = "";
                                caracteristicaAsignada = (string)clboxCaracteristicas.Items[i];
                                seleccion = caracteristicaAsignada.Split('|');
                                caracteristica = seleccion[0];
                                valorCaracteristica = seleccion[3];
                                csMySqlConnect conectorMySql = new csMySqlConnect();
                                string valores = "";
                                articulo = fila.Cells["id_product"].Value.ToString();
                                valores = "(" + caracteristica + "," + articulo + "," + valorCaracteristica + ")";

                                string consulta = "delete from ps_feature_product where id_feature = " + caracteristica + " and id_product = " + articulo;
                                csUtilidades.ejecutarConsulta(consulta, true);
                                conectorMySql.InsertValoresEnTabla("ps_feature_product", "(id_feature, id_product, id_feature_value)", valores, "", true, false);

                                // Actualizamos A3
                                csRepLog rl = new csRepLog();
                                csSqlConnects sql = new csSqlConnects();
                                string script = "SELECT CODART FROM ARTICULO WHERE LTRIM(KLS_ID_SHOP)=" + articulo;
                                string codart = sql.obtenerCampoTabla(script);
                                //si está mal y no nos da artículo lo arreglamos
                                if (string.IsNullOrEmpty(codart))
                                {
                                    codart = mysql.obtenerDatoFromQuery("select reference from ps_product where id_product=" + articulo);
                                    csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_ID_SHOP=" + articulo + " WHERE LTRIM(CODART)='" + codart + "'",false);
                                }

                                csUtilidades.ejecutarConsulta("INSERT INTO KLS_CARACTERISTICAS_ART VALUES('" + codart + "'," + valorCaracteristica + ")", false);
                                rl.insertarRegistroRegLog(articulo, "ARTICULO", rl.MOD);
                            }
                        }
                    }
                }
                else
                {
                    //MessageBox.Show("No hay filas seleccionadas para actualizar");
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void borrarCategorias()
        {
            if (dgvArticulos.SelectedRows.Count > 0)
            {
                string categoriaAsignada = "";
                string articuloAsignado = "";
                string codart = "";
                string filtro = "";
                string query = "";
                //articuloAsignado = clboxCategorias.CheckedItems[0].ToString();
                //categoriaAsignada = clboxCategorias.CheckedItems[5].ToString();
                csMySqlConnect conectorMySql = new csMySqlConnect();

                for (int i = 0; i < dgvArticulos.SelectedRows.Count; i++)
                {
                    categoriaAsignada = dgvArticulos.SelectedRows[i].Cells["Cod_Categoria"].Value.ToString();
                    articuloAsignado = dgvArticulos.SelectedRows[i].Cells["id_product"].Value.ToString();
                    codart = dgvArticulos.SelectedRows[i].Cells["reference"].Value.ToString().Trim();

                    // MYSQL
                    query = string.Format("delete from ps_category_product where id_product = {0} and id_category = {1}", articuloAsignado, categoriaAsignada);
                    csUtilidades.ejecutarConsulta(query, true);
                    // SQL

                    if (csGlobal.A3ERPConnection == "Y")
                    {
                        query = string.Format("delete from kls_categorias_art where ltrim(codart) = '{0}' and kls_codcategoria = '{1}'", codart, categoriaAsignada);
                        csUtilidades.ejecutarConsulta(query,false);
                    }
                }
            }
            else
            {
                //MessageBox.Show("No hay filas seleccionadas para actualizar");
            }
        }

        private void borrarCaracteristica()
        {
            //try
            //{
            //    if (dgvArticulos.SelectedRows.Count == 0 && clboxCaracteristicas.CheckedItems.Count > 0)
            //    {
            //        csMySqlConnect mysql = new csMySqlConnect();
            //        foreach (object itemChecked in clboxCaracteristicas.CheckedItems)
            //        {
            //            string id = itemChecked.ToString().Substring(0, 1);
            //            mysql.ejecutarConsulta("delete from ps_feature_lang where id_feature = " + id);
            //            mysql.ejecutarConsulta("delete from ps_feature where id_feature = " + id);
            //            mysql.ejecutarConsulta("delete from ps_feature_value where id_feature = " + id);
            //            mysql.ejecutarConsulta("delete from ps_feature_value_lang where id_feature = " + id);
            //        }

            //        cargarCaracteristicasPS();
            //        MessageBox.Show("Características borradas");
            //    }
            //    if (dgvArticulos.SelectedRows.Count == 1 && clboxCaracteristicas.CheckedItems.Count == 1)
            //    {

            //    }
            //    else
            //    {
            //        MessageBox.Show("Selecciona un artículo y a continuación las características que quieras borrar");
            //    }
            //}
            //catch (Exception) { }


            if (dgvArticulos.SelectedRows.Count > 0)
            {
                string caracteristicaSeleccionada = "";
                string valorCaracteristicaSeleccionada = "";
                string articuloSeleccionado = "";
                string filtro = "";

                csMySqlConnect conectorMySql = new csMySqlConnect();

                for (int i = 0; i < dgvArticulos.SelectedRows.Count; i++)
                {
                    caracteristicaSeleccionada = dgvArticulos.SelectedRows[i].Cells["id_feature"].Value.ToString();
                    articuloSeleccionado = dgvArticulos.SelectedRows[i].Cells["id_product"].Value.ToString();
                    valorCaracteristicaSeleccionada = dgvArticulos.SelectedRows[i].Cells["id_feature_value"].Value.ToString();
                    filtro = " id_feature=" + caracteristicaSeleccionada + " and id_product=" + articuloSeleccionado + " and id_feature_value=" + valorCaracteristicaSeleccionada;
                    conectorMySql.borrar("ps_feature_product", filtro);
                }
            }
            else
            {
                //MessageBox.Show("No hay filas seleccionadas para actualizar");
            }
        }


        private void btAsignarCategoria_Click(object sender, EventArgs e)
        {
            AsignarCategorias();
            cargarArticulosInfoPS();

        }

        private void btSustituirCategorias_Click(object sender, EventArgs e)
        {
            AsignarCategorias();
            borrarCategorias();
            cargarArticulosInfoPS();
        }

        private void btBorrarCategorias_Click(object sender, EventArgs e)
        {
            borrarCategorias();
            cargarArticulosInfoPS();
        }

        private bool verificarExisteCategoriaArticulo(string producto, string categoria)
        {
            bool Existe = false;

            csMySqlConnect mySqlConnect = new csMySqlConnect();
            if (mySqlConnect.existeRegistro("ps_category_product", " id_product=" + producto))
            {
                Existe = true;
            }

            return Existe;

        }

        private void btAsignarCaracteristica_Click(object sender, EventArgs e)
        {
            AsignarCaracteristicas();
            cargarArticulosInfoPS();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            borrarCaracteristica();
            cargarArticulosInfoPS();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            csGlobal.categoriaArbol = "";

            string filtroCategorias = "";
            string categoria = "";
            string[] nomCategoria;

            if (clboxCategoriasFilter.CheckedItems.Count == 1)
            {
                for (int i = 0; i < clboxCategoriasFilter.CheckedItems.Count; i++)
                {

                    categoria = clboxCategoriasFilter.CheckedItems[i].ToString();
                    nomCategoria = categoria.Split(new Char[] { '-' });
                    filtroCategorias = nomCategoria[1];

                }

                csGlobal.categoriaArbol = filtroCategorias;
                frCategoriasTree formularioTree = new frCategoriasTree();
                formularioTree.ShowDialog();
            }

        }



        private void mostrarArbolCategoria(CheckedListBox cListBox)
        {
            if (cListBox.SelectedItems.Count == 1)
            {
                csGlobal.categoriaArbol = "";
                string categoria = "";
                string filtroCategorias = "";
                string[] nomCategoria;

                categoria = cListBox.SelectedItem.ToString();
                nomCategoria = categoria.Split(new Char[] { '-' });
                filtroCategorias = nomCategoria[1];

                csGlobal.categoriaArbol = filtroCategorias;
                frCategoriasTree formularioTree = new frCategoriasTree();
                formularioTree.ShowDialog();
            }
            else
            {
                MessageBox.Show("No hay ninguna categoría seleccionada");
            }


        }

        private void btDesmarcarFiltroCaracteristicas_Click(object sender, EventArgs e)
        {
            DesmarcarFiltroCaracteristicas();
        }

        private void añadirPesoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvArticulos.SelectedRows.Count > 0)
                addWeight();

            cargarArticulosInfoPS();
        }

        private void addWeight()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string idWhere = "";
            string peso = Microsoft.VisualBasic.Interaction.InputBox("Peso a añadir: ", "Peso", "");

            foreach (DataGridViewRow fila in dgvArticulos.SelectedRows)
            {
                idWhere = fila.Cells[0].Value.ToString();

                mySqlConnect.actualizarValoresEnTabla("ps_product", "weight", peso, "id_product", idWhere);
            }
        }

        private void btBorrarMarca_Click(object sender, EventArgs e)
        {
            asignarMarcas(true);
            cargarArticulosInfoPS();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            asignarMarcas(false);
            cargarArticulosInfoPS();
        }

        private void árbolDeCategoríaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            mostrarArbolCategoria(clboxCategoriasFilter);
        }

        private void árbolDeCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mostrarArbolCategoria(clboxCategorias);
        }

        private void actualizarFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvArticulos.Rows.Count > 0)
            {
                if (dgvArticulos.SelectedRows.Count > 0)
                {
                    frDialogDateTime fechaInput = new frDialogDateTime();

                    foreach (DataGridViewRow item in dgvArticulos.SelectedRows)
                    {
                        if (fechaInput.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            string dt = Convert.ToDateTime(csGlobal.fechaFormValue).ToString("yyyy-MM-dd 00:00:00");
                            csUtilidades.ejecutarConsulta(
                                string.Format("update ps_product set date_add = '{0}' where id_product = {1}", dt,item.Cells["id_product"].Value.ToString()),true);
                            csUtilidades.ejecutarConsulta(
                                string.Format("update ps_product_shop set date_add = '{0}' where id_product = {1}", dt, item.Cells["id_product"].Value.ToString()),true);
                        }
                    }

                    "Fechas actualizadas".mb();
                }
            }
            
        }

        private void btnPesoBastide_Click(object sender, EventArgs e)
        {
            frPesoBastide basti = new frPesoBastide();
            basti.Show();
        }

        private void btDesmarcarCategorias_Click(object sender, EventArgs e)
        {
            desmarcarChecksListBox(clboxCategorias);
        }


        //Función para desmarcar todos los checks de ListCheckbox
        private void desmarcarChecksListBox(CheckedListBox lb)
        {
            foreach (int i in lb.CheckedIndices)
            {
                lb.SetItemCheckState(i, CheckState.Unchecked);
            }
        
        
        }

        private void btDesmarcarCaracteristicas_Click(object sender, EventArgs e)
        {
            desmarcarChecksListBox(clboxCaracteristicas);
        }

        private void btArbolCategorias_Click(object sender, EventArgs e)
        {
            cargarArbolCategorias();
        }

        private void cargarArbolCategorias()
        {
            csGlobal.categoriaArbol = "";

            string filtroCategorias = "";
            string categoria = "";
            string[] nomCategoria;

            if (clboxCategoriasFilter.CheckedItems.Count == 1)
            {
                for (int i = 0; i < clboxCategoriasFilter.CheckedItems.Count; i++)
                {

                    categoria = clboxCategoriasFilter.CheckedItems[i].ToString();
                    nomCategoria = categoria.Split(new Char[] { '-' });
                    filtroCategorias = nomCategoria[1];

                }

                csGlobal.categoriaArbol = filtroCategorias;
                frCategoriasTree formularioTree = new frCategoriasTree();
                formularioTree.ShowDialog();
            }
            else 
            {
                "Debes marcar una sola categoría para mostrar el arbol de categoría".mb();
            }
        }

        private void btDescarcarMarcas_Click(object sender, EventArgs e)
        {
            desmarcarChecksListBox(clboxMarcas);
        }

        private void tsbtnCargarArticulos_Click(object sender, EventArgs e)
        {
            cargarArticulosInfoPS();
        }

        private void tsbtBorrarTextoFiltro_Click(object sender, EventArgs e)
        {
            tstbNombreArticulo.Text = "";
        }

        private void tstbNombreArticulo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)13)
            //{
            //    cargarArticulosInfoPS();
            //}

          

        }

        private void frPrestaTools_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                tstbNombreArticulo.Text = "";
                tstbNombreArticulo.Focus();
            }

            if (e.KeyCode == Keys.Return)
            {
                cargarArticulosInfoPS();
                tstbNombreArticulo.Focus();
            }
        }

        private void tsbtnPesoAtributos_Click(object sender, EventArgs e)
        {
            frPesoBastide basti = new frPesoBastide();
            basti.Show();
        }

        private void cboxOrderByID_CheckedChanged(object sender, EventArgs e)
        {
            cargarCategoriasPS();
        }

        private void actualizarStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string codart = "";
            string idproduct = "";
            csStocks stock = new klsync.csStocks();

            if (csGlobal.activarMonotallas == "Y")
            {
                csTallasYColoresV2 tyc = new csTallasYColoresV2();
                tyc.marcarArticulosMonoatributoEnA3ERP("");
            }

            foreach (DataGridViewRow fila in dgvArticulos.SelectedRows)
            {
                codart = fila.Cells["reference"].Value.ToString();
                idproduct = fila.Cells["id_product"].Value.ToString();
                stock.stock(idproduct, codart);

            }
        }

        private void resetearAtributosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            tyc.resetearAtributos(dgvArticulos,true);
        }
    }  

}
