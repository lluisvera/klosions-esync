﻿namespace klsync
{
    partial class frRepasatActividades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvActividades = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportarAExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btLoadActividades = new System.Windows.Forms.Button();
            this.dtpFecIni = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecFin = new System.Windows.Forms.DateTimePicker();
            this.cboxTrabajadores = new System.Windows.Forms.ComboBox();
            this.cboxFiltroPorEmpleado = new System.Windows.Forms.CheckBox();
            this.btTraspasatA3 = new System.Windows.Forms.Button();
            this.dgvLineasActividades = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActividades)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasActividades)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvActividades
            // 
            this.dgvActividades.AllowUserToAddRows = false;
            this.dgvActividades.AllowUserToDeleteRows = false;
            this.dgvActividades.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvActividades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActividades.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvActividades.Location = new System.Drawing.Point(12, 101);
            this.dgvActividades.Name = "dgvActividades";
            this.dgvActividades.ReadOnly = true;
            this.dgvActividades.Size = new System.Drawing.Size(1146, 315);
            this.dgvActividades.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportarAExcelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(156, 26);
            // 
            // exportarAExcelToolStripMenuItem
            // 
            this.exportarAExcelToolStripMenuItem.Name = "exportarAExcelToolStripMenuItem";
            this.exportarAExcelToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exportarAExcelToolStripMenuItem.Text = "&Exportar a Excel";
            this.exportarAExcelToolStripMenuItem.Click += new System.EventHandler(this.exportarAExcelToolStripMenuItem_Click);
            // 
            // btLoadActividades
            // 
            this.btLoadActividades.Location = new System.Drawing.Point(405, 57);
            this.btLoadActividades.Name = "btLoadActividades";
            this.btLoadActividades.Size = new System.Drawing.Size(109, 33);
            this.btLoadActividades.TabIndex = 1;
            this.btLoadActividades.Text = "Cargar Actividades";
            this.btLoadActividades.UseVisualStyleBackColor = true;
            this.btLoadActividades.Click += new System.EventHandler(this.btLoadActividades_Click);
            // 
            // dtpFecIni
            // 
            this.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecIni.Location = new System.Drawing.Point(63, 46);
            this.dtpFecIni.Name = "dtpFecIni";
            this.dtpFecIni.Size = new System.Drawing.Size(97, 20);
            this.dtpFecIni.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Desde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hasta";
            // 
            // dtpFecFin
            // 
            this.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecFin.Location = new System.Drawing.Point(63, 72);
            this.dtpFecFin.Name = "dtpFecFin";
            this.dtpFecFin.Size = new System.Drawing.Size(97, 20);
            this.dtpFecFin.TabIndex = 4;
            // 
            // cboxTrabajadores
            // 
            this.cboxTrabajadores.FormattingEnabled = true;
            this.cboxTrabajadores.Location = new System.Drawing.Point(253, 69);
            this.cboxTrabajadores.Name = "cboxTrabajadores";
            this.cboxTrabajadores.Size = new System.Drawing.Size(121, 21);
            this.cboxTrabajadores.TabIndex = 7;
            // 
            // cboxFiltroPorEmpleado
            // 
            this.cboxFiltroPorEmpleado.AutoSize = true;
            this.cboxFiltroPorEmpleado.Location = new System.Drawing.Point(253, 46);
            this.cboxFiltroPorEmpleado.Name = "cboxFiltroPorEmpleado";
            this.cboxFiltroPorEmpleado.Size = new System.Drawing.Size(117, 17);
            this.cboxFiltroPorEmpleado.TabIndex = 8;
            this.cboxFiltroPorEmpleado.Text = "Filtro Por Empleado";
            this.cboxFiltroPorEmpleado.UseVisualStyleBackColor = true;
            this.cboxFiltroPorEmpleado.CheckedChanged += new System.EventHandler(this.cboxFiltroPorEmpleado_CheckedChanged);
            // 
            // btTraspasatA3
            // 
            this.btTraspasatA3.BackgroundImage = global::klsync.Properties.Resources.Refresh;
            this.btTraspasatA3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btTraspasatA3.Location = new System.Drawing.Point(535, 56);
            this.btTraspasatA3.Name = "btTraspasatA3";
            this.btTraspasatA3.Size = new System.Drawing.Size(48, 34);
            this.btTraspasatA3.TabIndex = 9;
            this.btTraspasatA3.UseVisualStyleBackColor = true;
            this.btTraspasatA3.Click += new System.EventHandler(this.btTraspasatA3_Click);
            // 
            // dgvLineasActividades
            // 
            this.dgvLineasActividades.AllowUserToAddRows = false;
            this.dgvLineasActividades.AllowUserToDeleteRows = false;
            this.dgvLineasActividades.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLineasActividades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLineasActividades.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvLineasActividades.Location = new System.Drawing.Point(12, 437);
            this.dgvLineasActividades.Name = "dgvLineasActividades";
            this.dgvLineasActividades.ReadOnly = true;
            this.dgvLineasActividades.Size = new System.Drawing.Size(1146, 144);
            this.dgvLineasActividades.TabIndex = 10;
            // 
            // frRepasatActividades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1170, 607);
            this.Controls.Add(this.dgvLineasActividades);
            this.Controls.Add(this.btTraspasatA3);
            this.Controls.Add(this.cboxFiltroPorEmpleado);
            this.Controls.Add(this.cboxTrabajadores);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpFecFin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpFecIni);
            this.Controls.Add(this.btLoadActividades);
            this.Controls.Add(this.dgvActividades);
            this.Name = "frRepasatActividades";
            this.Text = "frRepasatActividades";
            this.Load += new System.EventHandler(this.frRepasatActividades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvActividades)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasActividades)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvActividades;
        private System.Windows.Forms.Button btLoadActividades;
        private System.Windows.Forms.DateTimePicker dtpFecIni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFecFin;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportarAExcelToolStripMenuItem;
        private System.Windows.Forms.ComboBox cboxTrabajadores;
        private System.Windows.Forms.CheckBox cboxFiltroPorEmpleado;
        private System.Windows.Forms.Button btTraspasatA3;
        private System.Windows.Forms.DataGridView dgvLineasActividades;
    }
}