﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace klsync
{
    class csPrecios
    {
        csSqlConnects sql = new csSqlConnects();
        csMySqlConnect mysql = new csMySqlConnect();

        public string productPrice(string codart)
        {
            string descripcion = "";
            string precio = "";
            csSqlConnects valorArticulo = new csSqlConnects();
            descripcion = valorArticulo.obtenerPrecioArticuloA3(codart, "PRCVENTA");
            //Modificación para Esportissim porque los precios de la ficha de artículo son con IVA incluido y se han de dividir por el IVA
            if (csGlobal.ivaIncluido.ToUpper() == "SI")
            {
                descripcion = Math.Round(Convert.ToDouble(descripcion) / 1.21, 3).ToString();
            }
            else
            {
                descripcion = Math.Round(Convert.ToDouble(descripcion), 3).ToString();
            
            }
            precio = descripcion.Replace(",", ".");

            return precio;
        }

        public void actualiarFamiliasArticulosEnPS()
        {
            string productPS = "";
            string productA3 = "";
            string familiaA3 = "";

            DataTable dtA3 = sql.cargarDatosTablaA3("SELECT KLS_ID_SHOP, FAMARTDESCVEN FROM dbo.ARTICULO WHERE (KLS_ID_SHOP > 0) AND (FAMARTDESCVEN IS NOT NULL)");
            DataTable dtPS = mysql.cargarTabla("select id_product, reference, kls_a3erp_fam_id from ps_product");

            foreach (DataRow fila in dtPS.Rows)
            {
                productPS = fila["id_product"].ToString();
                foreach (DataRow filaA3 in dtA3.Rows)
                {
                    productA3 = filaA3["KLS_ID_SHOP"].ToString();
                    familiaA3 = filaA3["FAMARTDESCVEN"].ToString();
                    {
                        if (productA3 == productPS)
                        {
                            mysql.actualizarGenerica("Update ps_product set kls_a3erp_fam_id='" + familiaA3 + "' where id_product=" + productPS);

                        }
                    }
                }
            }
        }


        public void actualizarFamiliasClientesPS()
        {
            string clientePS = "";
            string clienteA3 = "";
            string familiaA3 = "";

            DataTable dtA3 = sql.cargarDatosTablaA3("SELECT CODCLI, FAMCLIESP, KLS_CODCLIENTE FROM dbo.__CLIENTES WHERE (KLS_CODCLIENTE > 0) AND (FAMCLIESP IS NOT NULL)");
            DataTable dtPS = mysql.cargarTabla("select id_customer, kls_a3erp_fam_id from ps_customer");

            foreach (DataRow fila in dtPS.Rows)
            {
                clientePS = fila["id_customer"].ToString();
                foreach (DataRow filaA3 in dtA3.Rows)
                {
                    clienteA3 = filaA3["KLS_CODCLIENTE"].ToString();
                    familiaA3 = filaA3["FAMCLIESP"].ToString();
                    {
                        if (clienteA3 == clientePS)
                        {
                            mysql.actualizarGenerica("Update ps_customer set kls_a3erp_fam_id='" + familiaA3 + "' where id_customer=" + clientePS);
                        }
                    }
                }
            }
        }

        public void descuentos()
        {
            try
            {
                //Borramos sólo los precios que no procedan de una regla introducida desde Prestashop
                csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='percentage'", true);
                csSqlScripts sqlScript = new csSqlScripts();

                // OPERATIVO OCTUBRE 2016
                // ARTICULO - CLIENTE
                DataTable dt1 = sql.cargarDatosTablaA3(sqlScript.selectDescuento1ArtCli());
                introducirDescuentosPorPartesV2("1", dt1);

                //COMBINACIÓN DE CLIENTES FAMILIAS DE ARTÍCULOS - HEBO

                DataTable dt3_bis = sql.cargarDatosTablaA3(sqlScript.selectDescuento3_bis_FamArtCli());
                introducirDescuentosPorPartesV2("3", dt3_bis, true);

                //COMBINACIÓN DE FAMILIAS DE ARTÍCULOS SIN CLIENTES
                DataTable dt31_bis = sql.cargarDatosTablaA3(sqlScript.selectDescuento31_bis_FamArtCli());
                introducirDescuentosPorPartesV2("3", dt31_bis, true);

                //COMBINACIÓN DE FAMILIAS CLIENTES Y FAMILIAS DE ARTÍCULOS - DISMAY
                DataTable dt51 = sql.cargarDatosTablaA3(sqlScript.selectDescuento5_1_FamCliFamArt());
                introducirDescuentosPorPartesV2("5", dt51, true);
            }
            catch (Exception ex) { }
        }

        private void introducirDescuentosPorPartesV2(string parte, DataTable dt, bool descuentosfinales = false)
        {
            if (dt.Rows.Count > 0)
            {
                List<string> valuesList = new List<string>();

                // HEBO 1.6.0.9 (no tiene el campo reduction_tax) - DISMAY 1.6.0.1.1 (tiene el campo reduction_tax)
                int versionInstalada = Int32.Parse(csGlobal.versionPS.Replace(".", ""));
                string campos = "(id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to" + ((versionInstalada == 16011) ? ",reduction_tax" : "") + ")";

                string articulo = "0";
                string id_shop = "0";
                string cliente = "0"; // variable (customer)
                string precio = "-1";
                string descuento = "";
                string desdeCantidad = "1";
                string tipoDescuento = "percentage";
                string famcli = "";
                string famart = "";
                DateTime fechaDesde = new DateTime();
                DateTime fechaHasta = new DateTime();
                string Lineas = "";
                string LineasLang = "";
                string LineasShop = "";
                int contador = 0;
                string lineas_descuentos = "";
                string campos_descuentos = "(id_specific_price_rule, id_product, id_shop, id_customer, price, from_quantity, reduction, " +
                                            " reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli, kls_famart, " +
                                            " id_cart, id_shop_group, id_currency, id_country, id_group, id_product_attribute" + ((versionInstalada >= 16011) ? ",reduction_tax" : "") + ")";
                // Añadimos los campos de la última fila porque los pide al hacer el insert

                csMySqlConnect mysql = new csMySqlConnect();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    precio = "-1";

                    if (!descuentosfinales)
                    {
                        articulo = dt.Rows[i]["KLS_ID_SHOP"].ToString().Trim();
                        cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString();
                    }

                    descuento = dt.Rows[i]["DESC1"].ToString();
                    if (dt.Rows[i]["TIPO"].ToString() == "DESCUENTO")
                    {
                        tipoDescuento = "percentage";
                    }
                    else
                    {
                        tipoDescuento = "amount";
                        //precio = dgvPreciosA3.SelectedRows[i].Cells[4].Value.ToString();
                        precio = precio.Replace(",", ".");
                        descuento = "0";
                    }
                    if (cliente == "")
                    {
                        cliente = "0";
                    }

                    fechaDesde = Convert.ToDateTime(dt.Rows[i]["FECMIN"].ToString());
                    fechaHasta = Convert.ToDateTime(dt.Rows[i]["FECMAX"].ToString());

                    double dcost = Convert.ToDouble(descuento);
                    dcost = (dcost / 100);
                    descuento = dcost.ToString().Replace(",", ".");

                    if (descuentosfinales)
                    {
                        famart = dt.Rows[i]["FAMART"].ToString().Trim();
                        famcli = dt.Rows[i]["FAMCLI"].ToString().Trim();
                        if (famcli == "")
                        {
                            famcli = "0";
                        }
                        if (famart == "")
                        {
                            famart = "0";
                        }

                        cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString().Trim();

                        lineas_descuentos = "(" + articulo + ",0," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                            + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + "','"
                            + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "', '" + famcli + "', '"
                            + famart + "',0,0,0,0,0,0" + ((versionInstalada == 16011) ? ",1" : "") + ")";

                        valuesList.Add(lineas_descuentos);
                    }
                    else
                    {
                        Lineas = "(" + articulo + "," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                        + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + "','"
                        + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "'" + ((versionInstalada >= 16011) ? ",1" : "") + ")";

                        valuesList.Add(Lineas);
                    }
                }

                csUtilidades.WriteListToDatabase("ps_specific_price", ((descuentosfinales) ? campos_descuentos : campos), valuesList);
            }

        }

    }
}
