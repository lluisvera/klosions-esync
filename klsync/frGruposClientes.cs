﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace klsync
{
    public partial class frGruposClientes : Form
    {
        private Stopwatch sw = new Stopwatch(); 
        private static frGruposClientes m_FormDefInstance;
        public static frGruposClientes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frGruposClientes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frGruposClientes()
        {
            InitializeComponent();
        }

        private void toolStripButtonCargarGrupos_Click(object sender, EventArgs e)
        {
            sw.Start();

            csMySqlConnect mysql = new csMySqlConnect();
            string idioma = mysql.defaultLangPS().ToString();

            DataTable grupos = mysql.cargarTabla("select id_group, name from ps_group_lang where id_lang = " + idioma);
            dgvGruposClientes.DataSource = grupos;
            dgvGruposClientes.Columns[0].Visible = false;
            dgvGruposDestino.DataSource = grupos;
            dgvGruposDestino.Columns[0].Visible = false;

            dgvGruposDestino.DataSource = mysql.cargarTabla("select id_group, name from ps_group_lang where id_lang = " + idioma);
            dgvGruposDestino.Columns[0].Visible = false;

            DataTable clientes = mysql.cargarTabla("select id_customer, company, email from ps_customer where id_lang = " + idioma);
            dgvClientes.DataSource = clientes;
            dgvClientes.Columns[0].Visible = false;

            csUtilidades.contarFilasGrid((DataTable)dgvClientes.DataSource, sw, toolStripStatusLabelContarClientes);
        }

        private void btnaddcustomergroup_Click(object sender, EventArgs e)
        {
            if (dgvGruposClientes.SelectedRows.Count == 1 && dgvClientes.SelectedRows.Count > 0)
            {
                string clientes = "", grupo_seleccionado = "", nombre_grupo_seleccionado ="";
                DataTable dt = csUtilidades.dgv2dtSelectedRows(dgvClientes);

                clientes = csUtilidades.concatenarValoresQueryFromDataTable(dt, "id_customer");

                grupo_seleccionado = dgvGruposDestino.SelectedRows[0].Cells["id_group"].Value.ToString();
                nombre_grupo_seleccionado = dgvGruposDestino.SelectedRows[0].Cells["id_group"].Value.ToString();


                csUtilidades.ejecutarConsulta(" INSERT INTO ps_customer_group " +
                                              " SELECT ps_customer_group.id_customer, " +
                                              grupo_seleccionado +
                                              " FROM ps_customer_group " +
                                              " WHERE ps_customer_group.id_customer NOT IN " +
                                              " (SELECT ps_customer_group.id_customer " +
                                              " FROM ps_customer_group " +
                                              " WHERE ps_customer_group.id_group = " + grupo_seleccionado + ") and ps_customer_group.id_customer in(" + clientes + ")", true);

                csUtilidades.ejecutarConsulta("update ps_customer set ps_customer.id_default_group = " + grupo_seleccionado + " where ps_customer.id_customer in(" + clientes + ")", true);

                MessageBox.Show("Clientes añadidos al grupo " + nombre_grupo_seleccionado);
            }
            else
            {
                MessageBox.Show("Sigue los pasos para añadir los clientes a los grupos");
            }
        }

        private void toolStripButtonBusquedaGruposClientes_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SendKeys.SendWait("^f");
        }

        private void buscarLupa(string busqueda)
        {
            sw.Start();
            csMySqlConnect mysql = new csMySqlConnect();
            string idioma = mysql.defaultLangPS().ToString();

            dgvClientes.DataSource = mysql.cargarTabla("select id_customer, company, email from ps_customer where " +
                " (company like '%" + busqueda + "%' OR" +
                " email like '%" + busqueda + "%') AND" +
                " id_lang = " + idioma);

            csUtilidades.contarFilasGrid((DataTable)dgvClientes.DataSource, sw, toolStripStatusLabelContarClientes);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                csSqlConnects sql = new csSqlConnects();
                string search = Microsoft.VisualBasic.Interaction.InputBox("", "Búsqueda", "");
                if (search != "")
                {
                    buscarLupa(search);
                }
                else
                {
                    sw.Start();
                    csMySqlConnect mysql = new csMySqlConnect();
                    string idioma = mysql.defaultLangPS().ToString();

                    dgvClientes.DataSource = mysql.cargarTabla("select id_customer, company, email from ps_customer where id_lang = " + idioma);
                    csUtilidades.contarFilasGrid((DataTable)dgvClientes.DataSource, sw, toolStripStatusLabelContarClientes);
                }

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void dgvGruposClientes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            cargarGrupoClienteSeleccionado();
        }

        private void cargarGrupoClienteSeleccionado()
        {
            sw.Start();
            csMySqlConnect mysql = new csMySqlConnect();
            string idioma = mysql.defaultLangPS().ToString();
            string grupo_seleccionado = dgvGruposClientes.SelectedRows[0].Cells["id_group"].Value.ToString();

            dgvClientes.DataSource = mysql.cargarTabla("select ps_customer.id_customer, company, email from ps_customer left join ps_customer_group on ps_customer_group.id_customer = ps_customer.id_customer where ps_customer_group.id_group = " + grupo_seleccionado + " and ps_customer.id_lang = " + idioma);

            csUtilidades.contarFilasGrid((DataTable)dgvClientes.DataSource, sw, toolStripStatusLabelContarClientes);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (dgvGruposClientes.SelectedRows.Count == 1)
            {
                cargarGrupoClienteSeleccionado();
            }
        }

        private void dgvClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            string grupos_clientes = "El usuario pertenece a los grupos: ";

            DataTable dt = mysql.cargarTabla("select distinct name from ps_customer_group left join ps_group_lang on ps_group_lang.id_group = ps_customer_group.id_group where id_customer = " + dgvClientes.SelectedRows[0].Cells["id_customer"].Value.ToString());
            grupos_clientes += csUtilidades.concatenarValoresQueryFromDataTable(dt, "name");
            dgvClientes.SelectedRows[0].Cells["company"].ToolTipText = grupos_clientes;
            dgvClientes.SelectedRows[0].Cells["email"].ToolTipText = grupos_clientes;
        }
    }
}
