﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a3ERPActiveX;
using System.Data;

namespace klsync
{
    class csa3erpStock
    {


        public void generaDoc(string fechaInicio, string fechaFin, Objetos.csMovimientosStock[] movStock)
        {

            string errorNumRegistro = "";
            string errorFechaRegistro = "";

            try
            {
                DataTable dtAlmacenes = new DataTable();
                csSqlConnects sql = new klsync.csSqlConnects();
                dtAlmacenes = sql.obtenerDatosSQLScript("SELECT LTRIM(CODALM) AS ALMACEN,DESCALM, RPST_ID_ALM from almacen WHERE RPST_ID_ALM IS NOT NULL");
                string almacenA3 = "";
                //Inicializamos el objeto
                a3ERPActiveX.Regularizacion naxReg = new a3ERPActiveX.Regularizacion();
                naxReg.Iniciar();

                bool albaranAbierto = true;
                bool diaConMovimientos = false;

                DataTable dtMovimientos = new DataTable();
                dtMovimientos.Columns.Add("idMovimiento");
                dtMovimientos.Columns.Add("codExternoMovimientoStock");


                //Inicializamos fecha del albarán regularización
                DateTime fechaRegularizacion = new DateTime();

                //Miro los días de diferencia entre la fecha final y la inicial
                //Por cada día se generará un Albarán de Regularización
                TimeSpan dias = new TimeSpan();

                int franjaDias = Convert.ToInt16(dias.TotalDays);

                foreach (DataRow almacen in dtAlmacenes.Rows)
                {
                    fechaRegularizacion = Convert.ToDateTime(fechaInicio);
                    dias = Convert.ToDateTime(fechaFin) - Convert.ToDateTime(fechaInicio);

                    almacenA3 = almacen["ALMACEN"].ToString();

                    for (int i = 0; i <= franjaDias; i++)
                    {
                        //Reviso todos los movimientos y por cada día los añado al Albarán
                        foreach (Objetos.csMovimientosStock movimiento in movStock)
                        {
                            if (movimiento.almacen != almacenA3) continue;

                            DateTime fechaX = movimiento.fechaMov;
                            //Valido si existe movimientos en ese día
                            if (fechaX == fechaRegularizacion)
                            {
                                if (movimiento.unidades == 0)
                                {
                                    continue;
                                }

                                errorFechaRegistro = fechaRegularizacion.ToShortDateString();
                                errorNumRegistro = i.ToString();

                                diaConMovimientos = true;
                                if (albaranAbierto)
                                {
                                    naxReg.Nuevo(fechaRegularizacion.ToString("dd/MM/yyyy"), almacenA3);
                                    naxReg.set_AsStringCab("MOTIVO", "Regularización Artículos Tarea");
                                    albaranAbierto = false;
                                }
                                //Añado las lineas
                                naxReg.NuevaLineaArt(movimiento.codart, movimiento.unidades);
                                naxReg.set_AsStringLin("TEXTO", "Actividad Id: " + movimiento.idActividad);
                                naxReg.AnadirLinea();
                                DataRow filaMov = dtMovimientos.NewRow();
                                filaMov["idMovimiento"] = movimiento.idMovimientoStock;
                                filaMov["codExternoMovimientoStock"] = i.ToString();
                                dtMovimientos.Rows.Add(filaMov);
                            }
                        }

                        //Guardo el documento
                        if (diaConMovimientos)
                        {
                            decimal documento = naxReg.Anade();

                            foreach (DataRow fila in dtMovimientos.Rows)
                            {
                                if (fila["codExternoMovimientoStock"].ToString() == i.ToString())
                                {
                                    fila["codExternoMovimientoStock"] = documento;
                                }
                            }
                            diaConMovimientos = false;
                            albaranAbierto = true;
                        }
                        fechaRegularizacion = fechaRegularizacion.AddDays(1);
                    }
                }






                /*naxfactura.set_AsStringCab("dtocli", dtocli);
                naxfactura.set_AsStringCab("pobcli", pobcli);
                naxfactura.NuevaLineaArt(codart, unidades);
                naxfactura.set_AsFloatLin("Desc1", 0);
                //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                naxfactura.set_AsFloatLin("Prcmonedamasiva", precio);
                naxfactura.set_AsStringLin("CentroCoste", "C1");
                naxfactura.set_AsStringLin("CentroCoste2", "C2");
                naxfactura.set_AsStringLin("CentroCoste3", "C3");*/



                naxReg.Acabar();

                //Actualizar Movimientos en Repasat

                if (dtMovimientos.Rows.Count > 0)
                {
                    DataSet datasetRPST = new DataSet();
                    string rowKey = "";
                    string objeto = "stockmovements";
                    DataTable dtParams = new DataTable();
                    dtParams.TableName = "dtParams";
                    dtParams.Columns.Add("FIELD");
                    dtParams.Columns.Add("KEY");
                    dtParams.Columns.Add("VALUE");

                    DataTable dtKeys = new DataTable();
                    dtKeys.TableName = "dtKeys";
                    dtKeys.Columns.Add("KEY");

                    foreach (DataRow drCab in dtMovimientos.Rows)
                    {
                        rowKey = drCab["idMovimiento"].ToString();
                        //idDocCab = rowKey;
                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = rowKey;
                        dtKeys.Rows.Add(drKeys);

                        // Añado los campos de la cabecera
                        foreach (DataColumn dtcolCab in dtMovimientos.Columns)
                        {
                            if (string.IsNullOrEmpty(drCab[dtcolCab.ColumnName].ToString()))  //Si la columna está en blanco me lo salto
                            {
                                continue;
                            }
                            else
                            {
                                if (dtcolCab.ColumnName == "idMovimiento" || dtcolCab.ColumnName == "FORPAG" || dtcolCab.ColumnName == "CODCLI" || dtcolCab.ColumnName == "REGIVA" || dtcolCab.ColumnName == "IDDOC") // Si el nombre de la columna de sql es uno de estos, me lo salto
                                {
                                    continue;
                                }
                                DataRow drParams = dtParams.NewRow();
                                drParams["FIELD"] = dtcolCab.ColumnName;
                                drParams["KEY"] = rowKey;
                                drParams["VALUE"] = drCab[dtcolCab.ColumnName];
                                dtParams.Rows.Add(drParams);
                            }
                        }
                    }



                        datasetRPST.Tables.Add(dtKeys);
                        datasetRPST.Tables.Add(dtParams);
                        //csRepasatWebService rpstWS = new csRepasatWebService();
                        //rpstWS.sincronizarObjetoRepasat("accounts", "POST", dtKeys, dtParams, "");


                        csRepasatWebService rpstWS = new csRepasatWebService();
                        // rpstWS.sincronizarObjetoRepasat("saleorders", "POST", dtsAccounts.Tables["dtKeys"], dtsAccounts.Tables["dtParams"], "", clientes);
                        rpstWS.sincronizarObjetoRepasat(objeto, "PUT", dtKeys, dtParams, "", false);



                }
            }
            catch (Exception ex)
            {

                string txt = ex.Message + "\n" +
                    errorFechaRegistro + "\n" +
                    "num Registro:" + errorNumRegistro;
                txt.mb();
            }

        }


        public void generaRegularizaInventario(Objetos.csMovimientosStock[] movStock, string almacen)
        {
            try
            {
                //Inicializamos el objeto
                a3ERPActiveX.Regularizacion naxReg = new a3ERPActiveX.Regularizacion();
                naxReg.Iniciar();

                bool albaranAbierto = false;
                bool diaConMovimientos = false;

                DataTable dtMovimientos = new DataTable();
                dtMovimientos.Columns.Add("idMovimiento");
                dtMovimientos.Columns.Add("codExternoMovimientoStock");


                string fecha = DateTime.Now.ToString("aaaa-MM-dd");
                //Inicializamos fecha del albarán regularización
                //DateTime fechaRegularizacion = Convert.ToDateTime(fechaInicio);


                //Miro los días de diferencia entre la fecha final y la inicial
                //Por cada día se generará un Albarán de Regularización
                //TimeSpan dias = Convert.ToDateTime(fechaFin) - Convert.ToDateTime(fechaInicio);

                //int franjaDias = Convert.ToInt16(dias.TotalDays);

                //  for (int i = 0; i <= franjaDias; i++)
                //{
                //Reviso todos los movimientos y por cada día los añado al Albarán
                foreach (Objetos.csMovimientosStock movimiento in movStock)
                {
                    //Solo añado aquellos movimientos diferentes de 0
                    if (movimiento.unidades != 0)
                    {
                       
                        DateTime fechaX = movimiento.fechaMov;
                        if (!albaranAbierto)
                        {
                            naxReg.Nuevo(DateTime.Now.ToString("dd/MM/yyyy"), almacen);
                            naxReg.set_AsStringCab("MOTIVO", "REGULARIZACIÓN STOCK RPST");
                        }
                        albaranAbierto = true;

                        //Añado las lineas
                        naxReg.NuevaLineaArt(movimiento.codart, movimiento.unidades);
                        naxReg.set_AsStringLin("TEXTO", "Regularización Repasat");
                        naxReg.AnadirLinea();
                        DataRow filaMov = dtMovimientos.NewRow();
                        filaMov["idMovimiento"] = movimiento.idMovimientoStock;
                    }
                }

                //Guardo el documento
                decimal documento = naxReg.Anade();
                naxReg.Acabar();

                //Actualizar Movimientos en Repasat

            }
            catch (Exception ex)
            {
                string txt = ex.Message;
            }

        }
    }
}
