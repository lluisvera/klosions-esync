﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;
using System.Net;
using System.Windows.Forms;
using System.Data;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace klsync.PaymentReminder
{
    class csPaymentReminder
    {


        csSqlConnects sql = new csSqlConnects();
        csCarteraScripts carteraScript = new csCarteraScripts();
        csCarteraCartaFiles cartaFiles = new csCarteraCartaFiles();

     

        public void obtenerListaEfectos(bool efectosVencidos=false )
        {

            string datosClientes = "";
            string codCli = "";
            string nomCli = "";
            double importeCarta = 0;

            string scriptQuery = efectosVencidos ? carteraScript.scriptRecibosVencidos(csGlobal.reminderDaysAfter.ToString()) : carteraScript.scriptRecibosPendientes(csGlobal.reminderDaysBefore.ToString());

            DataTable dt = sql.obtenerDatosSQLScript(scriptQuery);

            ("DataTable con " + dt.Rows.Count.ToString() + " filas").mb();

            if (dt.Rows.Count > 0)
            {
                codCli = dt.Rows[0]["CODCLI"].ToString();
                foreach (DataRow dr in dt.Rows)
                {
                    if (codCli == dr["CODCLI"].ToString())
                    {
                        importeCarta = importeCarta + Convert.ToDouble(dr["IMPORT"].ToString());
                        nomCli = dr["NOMBRE"].ToString();
                        //codCli = dr["CODCLI"].ToString();
                        datosClientes = datosClientes + "< tr > " +
                           "<td style=\"text-align: center; \">" + dr["SERIE"].ToString() + "</ td >" +
                           "<td style=\"text-align: right; \">" + dr["NUMDOC"].ToString() + "</ td >" +
                           "<td style=\"text-align: center; \">" + Convert.ToDateTime(dr["FECHAFACTURA"].ToString()).ToString("dd-MM-yyyy") + "</ td >" +
                           "<td style=\"text-align: center; \">" + Convert.ToDateTime(dr["FECHA"].ToString()).ToString("dd-MM-yyyy") + "</ td >" +
                           "<td style=\"text-align: right; \">" + Math.Round(Convert.ToDouble(dr["IMPORT"].ToString()), 2).ToString("#,##0.00") + "</ td ></ tr>";
                    }
                    else
                    {
                        datosClientes = datosClientes + "<tr><th colspan=\"4\"; style=\"text-align: right; \">Total / Amount  </th >" +
                            "<th style=\"text-align: right; \">" + Math.Round(importeCarta).ToString("#,##0.00") + "</th></tr>";
                        sendReminderCart(datosClientes, nomCli, codCli);
                        datosClientes = "";
                        importeCarta = 0;
                        importeCarta = importeCarta + Convert.ToDouble(dr["IMPORT"].ToString());
                        datosClientes = datosClientes + "< tr > " +
                           "<td style=\"text-align: center; \">" + dr["SERIE"].ToString() + "</ td >" +
                           "<td style=\"text-align: right; \">" + dr["NUMDOC"].ToString() + "</ td >" +
                           "<td style=\"text-align: center; \">" + Convert.ToDateTime(dr["FECHAFACTURA"].ToString()).ToString("dd-MM-yyyy") + "</ td >" +
                           "<td style=\"text-align: center; \">" + Convert.ToDateTime(dr["FECHA"].ToString()).ToString("dd-MM-yyyy") + "</ td >" +
                           "<td style=\"text-align: right; \">" + Math.Round(Convert.ToDouble(dr["IMPORT"].ToString()), 2).ToString("#,##0.00") + "</ td ></ tr>";
                        codCli = dr["CODCLI"].ToString();
                        nomCli = dr["NOMBRE"].ToString();

                    }


                }

                //Último Envío
                datosClientes = datosClientes + "<tr><th colspan=\"4\"; style=\"text-align: right; \">Total / Amoun</th >" +
                            "<th style=\"text-align: right; \">" + Math.Round(importeCarta,2).ToString("#,##0.00") + "</th></tr>";
                sendReminderCart(datosClientes, nomCli, codCli);
            }
        }

        public void sendReminderCart(string detalleEfectos, string customerName = null, string codCli = null)
        {
            MailMessage mail = new MailMessage();
            var doc = new Document();
            string emailCli = "";
            try
            {

                doc = new Document();

                PdfWriter writer = PdfWriter.GetInstance(doc,
                            new FileStream(csGlobal.reminderExportPath +  @"\reminder.pdf", FileMode.Create));

                MemoryStream memoryStream = new MemoryStream();
                PdfWriter.GetInstance(doc, memoryStream);

                doc.Open();

                string logoHtml = cartaFiles.cabeceraLogoReminderFile();
                string customerHtml = "<p><span style=\"color: #993300;\"><span style=\"color: #000000;\">Cliente</span> / Customer: </span>" + customerName + "  (" + codCli + ")" + "</p>";
                string headHtml = cartaFiles.cabeceraReminderFile();
                string bodyHtml = detalleEfectos;
                string pieHtml = cartaFiles.footerReminder();
                using (var documentoHtml = new StringReader(logoHtml + customerHtml + headHtml + bodyHtml + pieHtml))
                    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, documentoHtml);

                doc.Add(new Paragraph(DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt")));

                doc.Close();
                memoryStream.Close();
                writer.Close();

                //mail = new MailMessage("reminder@repasat.com", "lluisvera@klosions.com");
                mail = new MailMessage();
                mail.From = new MailAddress(csGlobal.mailerFrom);
                mail.Subject = "Reminder / Recordatorio: " + customerName;
                mail.IsBodyHtml = true;
                mail.Body = cartaFiles.bodyReminderMail();
                mail.Attachments.Add(new Attachment(csGlobal.reminderExportPath + @"\reminder.pdf"));


                string[] emailEmpresa = csGlobal.emailNotifyEmpresa.Split(';');
                foreach (string email in emailEmpresa)
                {
                    mail.Bcc.Add(email);
                }

                if (csGlobal.remindSendToCustomer)
                {
                    string[] destinatariosCliente = sql.obtenerDatosEnArray(carteraScript.scriptContactosNotificacion(codCli.Trim()));

                    if (destinatariosCliente == null)
                    {
                        mail.Subject = "INCIDENCIA ENVÍO EN EL CLIENTE: " + customerName + " SIN EMAIL DE CONTACTO ASIGNADO";
                    }
                    else
                    {
                        foreach (string destinatario in destinatariosCliente)
                        {
                            emailCli = destinatario;
                            emailCli = emailCli.Replace(" ","").Replace(",","").Replace(";", "");
                            mail.To.Add(emailCli);
                        }
                    }
                }


                var client = new SmtpClient();
                //client.Port = 25;
                client.Port = 587;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = csGlobal.mailerHost;
                client.Credentials = new NetworkCredential(csGlobal.mailerUserName,csGlobal.mailerPassword);

                client.Send(mail);
                //Liberamos los documentos adjuntos
                foreach (Attachment attachment in mail.Attachments)
                {
                    attachment.Dispose();
                }
                mail.Attachments.Dispose();
                mail = null;

                //"mensaje enviado".mb();

                ////Borramos el fichero generado
                //if (MessageBox.Show("Borramos el fichero?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                //{
                if (File.Exists(csGlobal.reminderExportPath + @"\reminder.pdf"))
                {
                    // If file found, delete it    
                    File.Delete(csGlobal.reminderExportPath + @"\reminder.pdf");
                    //Console.WriteLine("File deleted.");
                }

                //MessageBox.Show("File borrado");
                //}
                //else
                //{
                //    MessageBox.Show("Operación cancelada");
                //}
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + "Cliente: " + codCli.ToString());
                doc.Close();

                foreach (Attachment attachment in mail.Attachments)
                {
                    attachment.Dispose();
                }
                mail.Attachments.Dispose();
                mail = null;
            }
        }

    }
}
