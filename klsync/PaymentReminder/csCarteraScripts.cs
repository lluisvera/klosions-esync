﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.PaymentReminder
{
    class csCarteraScripts
    {

        public string scriptRecibosPendientes(string numdias = "7")
        {
            string script = "SELECT " +
                " LTRIM(CODCLI) AS CODCLI, NOMBRE, SERIE, NUMDOC, (IMPORTE-IMPORTECOB) AS IMPORT,CONVERT(VARCHAR(10), FECHAFACTURA, 111) as FECHAFACTURA, CONVERT(VARCHAR(10), FECHA, 111) as FECHA, DATEDIFF(DAY,FECHA, getdate()) " +
                " FROM CARTERA WHERE PAGADO ='F' AND COBPAG='C' and (DATEDIFF(DAY,FECHA, getdate())<=0 and DATEDIFF(DAY,FECHA, getdate())>=-" + numdias + " ) order by CODCLI, FECHA";
            return script;
        }

        public string scriptRecibosVencidos(string numdias = "7")
        {
            string script = "SELECT " +
               " CODCLI,NOMBRE, SERIE, NUMDOC, (IMPORTE-IMPORTECOB) AS IMPORT, CONVERT(VARCHAR(10), FECHAFACTURA, 111) as FECHAFACTURA, CONVERT(VARCHAR(10), FECHA, 111) as FECHA, DATEDIFF(DAY,FECHA, getdate()) " +
               " FROM CARTERA WHERE PAGADO ='F' AND COBPAG='C' and (DATEDIFF(DAY,FECHA, getdate())>0 and DATEDIFF(DAY,FECHA, getdate())<=" + numdias + ") order by FECHA";
            return script;
        }

        public string scriptContactosNotificacion(string codCli=null)
        {
            string anexoCliente = "";
            anexoCliente = string.IsNullOrEmpty(codCli) ? "" : " AND EMAIL<>'' AND LTRIM(__CLIENTES.CODCLI)='" + codCli + "'";

             string script = "SELECT EMAIL " +
                " from CONTACTOS inner join __cargos on contactos.IDCARGO=__CARGOS.ID INNER JOIN __CLIENTES ON CONTACTOS.CODIGO=__CLIENTES.IDORG " +
                " WHERE  DESCRIPCION LIKE '%REMINDER%' " + anexoCliente;
            return script;

        }


    }
}
