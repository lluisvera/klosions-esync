﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.PaymentReminder
{
    class csCarteraCartaFiles
    {
        public string cabeceraReminderFile()
        {
            try
            {
                string pathFile = @"C:\Program Files (x86)\Klosions eSync\CabeceraReminder.txt";
                // string rutaCabeceraFile = csGlobal.rutaFicConfig + "CabeceraReminder.txt";
                string text = System.IO.File.ReadAllText(pathFile);

                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string cabeceraLogoReminderFile()
        {
            try
            {
                string pathFile = @"C:\Program Files (x86)\Klosions eSync\CabeceraLogoReminder.txt";
                // string rutaCabeceraFile = csGlobal.rutaFicConfig + "CabeceraReminder.txt";
                string text = System.IO.File.ReadAllText(pathFile);

                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public string bodyReminderMail()
        {
            try
            {
                string pathFile = @"C:\Program Files (x86)\Klosions eSync\BodyReminder.txt";
                // string rutaCabeceraFile = csGlobal.rutaFicConfig + "BodyReminder.txt";
                string text = System.IO.File.ReadAllText(pathFile);

                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string footerReminder()
        {
            try
            {
                string pathFile = @"C:\Program Files (x86)\Klosions eSync\PieReminder.txt";
                // string rutaCabeceraFile = csGlobal.rutaFicConfig + "BodyReminder.txt";
                string text = System.IO.File.ReadAllText(pathFile);

                return text;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
