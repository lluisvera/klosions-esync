﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace klsync
{
    public partial class frInformePedidosPS : Form
    {
        private ToolStripControlHost tsdtIni;
        private ToolStripControlHost tsdtFin;
        private ToolStripControlHost tslblIni;
        private ToolStripControlHost tslblFin;
        private ToolStripControlHost tslRadioPedido;
        private ToolStripControlHost tslRadioFactura;
        private ToolStripControlHost tslLabelFacturado;
        private ToolStripControlHost tslRadioFacturadoSiNo;

        ComboBox FacturadoSiNo = new ComboBox();
        RadioButton rbPedido = new RadioButton();
        RadioButton rbFactura = new RadioButton();
        DateTimePicker dtpDesde = new DateTimePicker();
        DateTimePicker dtpHasta = new DateTimePicker();
        ToolStripButton btnCargarDocumentos = new ToolStripButton();
        ToolStripButton btnEnviarDocumento = new ToolStripButton();

        private static frInformePedidosPS m_FormDefInstance;
        public static frInformePedidosPS DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frInformePedidosPS();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frInformePedidosPS()
        {
            InitializeComponent();


            Label lblDesde = new Label();
            lblDesde.Text = "Desde:";
            lblDesde.TextAlign = ContentAlignment.MiddleCenter;
            lblDesde.BackColor = Color.Transparent;
            lblDesde.Padding = new Padding(10);

            Label lblFacturado = new Label();
            lblFacturado.Text = "Facturado: ";
            lblFacturado.TextAlign = ContentAlignment.MiddleCenter;
            lblFacturado.BackColor = Color.Transparent;
            lblFacturado.Padding = new Padding(10);

            dtpDesde.Format = DateTimePickerFormat.Short;
            dtpHasta.Format = DateTimePickerFormat.Short;

            Label lblHasta = new Label();
            lblHasta.Text = "Hasta:";
            lblHasta.TextAlign = ContentAlignment.MiddleCenter;
            lblHasta.BackColor = Color.Transparent;
            lblHasta.Padding = new Padding(20);

            rbPedido.Text = "Pedido";
            rbFactura.Text = "Factura";
            rbFactura.Checked = true;

            FacturadoSiNo.Items.AddRange(new object[] { "Todos", "Si", "No" });
            FacturadoSiNo.SelectedIndex = 0;

            btnCargarDocumentos.Image = klsync.Properties.Resources.magnifying_glass;
            btnCargarDocumentos.ImageScaling = ToolStripItemImageScaling.None;
            btnCargarDocumentos.Text = "Cargar los documentos";
            btnCargarDocumentos.TextImageRelation = TextImageRelation.ImageAboveText;
            btnCargarDocumentos.Padding = new Padding(20);
            btnCargarDocumentos.Click += new EventHandler(cargarDocumentos);

            btnEnviarDocumento.Image = klsync.Properties.Resources.microsoft_excel;
            btnEnviarDocumento.ImageScaling = ToolStripItemImageScaling.None;
            btnEnviarDocumento.Text = "Exportar a Excel";
            btnEnviarDocumento.TextImageRelation = TextImageRelation.ImageAboveText;
            btnEnviarDocumento.Padding = new Padding(20);
            btnEnviarDocumento.Click += new EventHandler(exportarExcel);

            tslblIni = new ToolStripControlHost(lblDesde);
            tsdtIni = new ToolStripControlHost(dtpDesde);
            tslblFin = new ToolStripControlHost(lblHasta);
            tsdtFin = new ToolStripControlHost(dtpHasta);
            tslRadioFactura = new ToolStripControlHost(rbFactura);
            tslRadioPedido = new ToolStripControlHost(rbPedido);
            tslLabelFacturado = new ToolStripControlHost(lblFacturado);
            tslRadioFacturadoSiNo = new ToolStripControlHost(FacturadoSiNo);

            toolStripInformePedidos.Items.Add(tslblIni);                                    // 0
            toolStripInformePedidos.Items.Add(tsdtIni);                                     // 1
            toolStripInformePedidos.Items.Add(tslblFin);                                    // 2
            toolStripInformePedidos.Items.Add(tsdtFin);                                     // 3
            toolStripInformePedidos.Items.Add(new ToolStripSeparator());                    // 4
            toolStripInformePedidos.Items[4].Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            toolStripInformePedidos.Items.Add(tslRadioFactura);                             // 5
            toolStripInformePedidos.Items.Add(tslRadioPedido);                              // 6
            toolStripInformePedidos.Items.Add(new ToolStripSeparator());                    // 7
            toolStripInformePedidos.Items[7].Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            toolStripInformePedidos.Items.Add(tslLabelFacturado);                           // 8
            toolStripInformePedidos.Items.Add(tslRadioFacturadoSiNo);                       // 9
            toolStripInformePedidos.Items.Add(new ToolStripSeparator());                    // 10
            toolStripInformePedidos.Items[10].Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            toolStripInformePedidos.Items.Add(btnCargarDocumentos);                         // 11
            toolStripInformePedidos.Items.Add(btnEnviarDocumento);                          // 12

            toolStripInformePedidos.CanOverflow = false;
            toolStripInformePedidos.CanOverflow = true;
        }

        public void exportarExcel(object sender, EventArgs e)
        {
            if (dgvInformePedidos.Rows.Count > 0)
            {
                csUtilidades.DataTable2CSV((DataTable)dgvInformePedidos.DataSource);
            }
        }

        public void cargarDocumentos(object sender, EventArgs e)
        {
            Stopwatch watch = Stopwatch.StartNew();
            csMySqlConnect mysql = new csMySqlConnect();
            DateTime FechaDesde = dtpDesde.Value;
            DateTime FechaHasta = dtpHasta.Value;
            string strFechaDesde = FechaDesde.ToString("yyyy-MM-dd 00:00:00");
            string strFechaHasta = FechaHasta.ToString("yyyy-MM-dd 23:59:59");

            string facturado = "";
            if (FacturadoSiNo.Text == "Si")
            {
                facturado = " AND ps_order_state.invoice = 1";
            }
            else if (FacturadoSiNo.Text == "No")
            {
                facturado = " AND ps_order_state.invoice = 0";
            }

            string invoiceOrDelivery = (rbFactura.Checked) ? "invoice_date" : "ps_orders.date_add";
            string invoiceOrDeliveryNumber = (rbFactura.Checked) ? "invoice_number" : "delivery_number";

            string consulta = "select " +
                " id_order, invoice_date, ps_orders.date_add, ps_customer.id_customer, firstname, lastname, email, invoice_number, delivery_number, " +
                " total_discounts, total_discounts_tax_incl, total_discounts_tax_excl, " +
                " total_paid, total_paid_tax_incl, total_paid_tax_excl, total_shipping, total_shipping_tax_incl, total_shipping_tax_excl " +
                " from ps_orders " +
                " left join ps_customer on ps_customer.id_customer = ps_orders.id_customer " +
                " LEFT JOIN ps_order_state on ps_order_state.id_order_state = ps_orders.current_state " +
                " where " + invoiceOrDelivery + " >= '" + strFechaDesde + "' and " + invoiceOrDelivery + " <= '" + strFechaHasta + "' " +
                facturado;

            dgvInformePedidos.DataSource = mysql.cargarTabla(consulta);


            dgvInformePedidos.Columns["total_paid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_paid"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_paid_tax_incl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_paid_tax_incl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_paid_tax_excl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_paid_tax_excl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_paid_tax_excl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_shipping"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_shipping"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_shipping_tax_incl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_shipping_tax_incl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_shipping_tax_excl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_shipping_tax_excl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_discounts"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_discounts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_discounts_tax_incl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_discounts_tax_incl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvInformePedidos.Columns["total_discounts_tax_excl"].DefaultCellStyle.Format = "N2";
            dgvInformePedidos.Columns["total_discounts_tax_excl"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            double total_paid = 0;
            double total_paid_tax_incl = 0;
            double total_paid_tax_excl = 0;
            double total_shipping = 0;
            double total_shipping_tax_incl = 0;
            double total_shipping_tax_excl = 0;
            foreach (DataGridViewRow item in dgvInformePedidos.Rows)
            {
                total_paid += Math.Round(Convert.ToDouble(item.Cells["total_paid"].Value.ToString()), 2);
                total_paid_tax_incl += Math.Round(Convert.ToDouble(item.Cells["total_paid_tax_incl"].Value.ToString()), 2);
                total_paid_tax_excl += Math.Round(Convert.ToDouble(item.Cells["total_paid_tax_excl"].Value.ToString()), 2);
                total_shipping += Math.Round(Convert.ToDouble(item.Cells["total_shipping"].Value.ToString()), 2);
                total_shipping_tax_incl += Math.Round(Convert.ToDouble(item.Cells["total_shipping_tax_incl"].Value.ToString()), 2);
                total_shipping_tax_excl += Math.Round(Convert.ToDouble(item.Cells["total_shipping_tax_excl"].Value.ToString()), 2);
            }

            DataTable dataTable = (DataTable)dgvInformePedidos.DataSource;
            DataRow drToAdd = dataTable.NewRow();

            drToAdd["total_paid"] = total_paid;
            drToAdd["total_paid_tax_incl"] = total_paid_tax_incl;
            drToAdd["total_paid_tax_excl"] = total_paid_tax_excl;
            drToAdd["total_shipping"] = total_shipping;
            drToAdd["total_shipping_tax_incl"] = total_shipping_tax_incl;
            drToAdd["total_shipping_tax_excl"] = total_shipping_tax_excl;
            dataTable.Rows.Add(drToAdd);
            dataTable.AcceptChanges();

            csUtilidades.contarFilasGrid((DataTable)dgvInformePedidos.DataSource, watch, toolStripStatusLabelNumeroFilas);
        }
    }
}
