﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frDialogCombo : Form
    {
        public frDialogCombo(DataTable dt, string campoComboBox, string label, bool cadena = false)
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
            lblSeleccionar.Text = label;
            csUtilidades.poblarCombo(combo, dt, campoComboBox, cadena);
        }

        private void btnAceptar_Click_1(object sender, EventArgs e)
        {
            if (combo.SelectedValue != null)
            {
                this.DialogResult = DialogResult.OK;

                csGlobal.comboFormValue = combo.Text;
                csGlobal.comboFormValueMember = combo.ValueMember.ToString();
            }
            else
            {
                csGlobal.comboFormValue = "";
            }

            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                this.Close();

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
