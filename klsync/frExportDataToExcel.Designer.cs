﻿namespace klsync
{
    partial class frExportDataToExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExportDataToExcel = new System.Windows.Forms.Button();
            this.cmbExportarDatosMarca = new System.Windows.Forms.ComboBox();
            this.lblExportarDatosMarca = new System.Windows.Forms.Label();
            this.lblSeleccionarMarca = new System.Windows.Forms.Label();
            this.lblExportMarcas = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnExportDataToExcel
            // 
            this.btnExportDataToExcel.Location = new System.Drawing.Point(268, 108);
            this.btnExportDataToExcel.Name = "btnExportDataToExcel";
            this.btnExportDataToExcel.Size = new System.Drawing.Size(75, 23);
            this.btnExportDataToExcel.TabIndex = 0;
            this.btnExportDataToExcel.Text = "Exportar";
            this.btnExportDataToExcel.UseVisualStyleBackColor = true;
            this.btnExportDataToExcel.Click += new System.EventHandler(this.btnExportDataToExcel_Click);
            // 
            // cmbExportarDatosMarca
            // 
            this.cmbExportarDatosMarca.FormattingEnabled = true;
            this.cmbExportarDatosMarca.Location = new System.Drawing.Point(141, 108);
            this.cmbExportarDatosMarca.Name = "cmbExportarDatosMarca";
            this.cmbExportarDatosMarca.Size = new System.Drawing.Size(121, 21);
            this.cmbExportarDatosMarca.TabIndex = 1;
            // 
            // lblExportarDatosMarca
            // 
            this.lblExportarDatosMarca.AutoSize = true;
            this.lblExportarDatosMarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportarDatosMarca.Location = new System.Drawing.Point(27, 28);
            this.lblExportarDatosMarca.Name = "lblExportarDatosMarca";
            this.lblExportarDatosMarca.Size = new System.Drawing.Size(300, 25);
            this.lblExportarDatosMarca.TabIndex = 2;
            this.lblExportarDatosMarca.Text = "EXPORTAR DATOS EXCEL";
            // 
            // lblSeleccionarMarca
            // 
            this.lblSeleccionarMarca.AutoSize = true;
            this.lblSeleccionarMarca.Location = new System.Drawing.Point(29, 111);
            this.lblSeleccionarMarca.Name = "lblSeleccionarMarca";
            this.lblSeleccionarMarca.Size = new System.Drawing.Size(106, 13);
            this.lblSeleccionarMarca.TabIndex = 3;
            this.lblSeleccionarMarca.Text = "Selecciona la marca:";
            // 
            // lblExportMarcas
            // 
            this.lblExportMarcas.AutoSize = true;
            this.lblExportMarcas.Location = new System.Drawing.Point(29, 85);
            this.lblExportMarcas.Name = "lblExportMarcas";
            this.lblExportMarcas.Size = new System.Drawing.Size(244, 13);
            this.lblExportMarcas.TabIndex = 4;
            this.lblExportMarcas.Text = "EXPORTAR DATOS PRODUCTOS POR MARCA";
            // 
            // frExportDataToExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblExportMarcas);
            this.Controls.Add(this.lblSeleccionarMarca);
            this.Controls.Add(this.lblExportarDatosMarca);
            this.Controls.Add(this.cmbExportarDatosMarca);
            this.Controls.Add(this.btnExportDataToExcel);
            this.Name = "frExportDataToExcel";
            this.Text = "frExportDataToExcel";
            this.Load += new System.EventHandler(this.frExportDataToExcel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExportDataToExcel;
        private System.Windows.Forms.ComboBox cmbExportarDatosMarca;
        private System.Windows.Forms.Label lblExportarDatosMarca;
        private System.Windows.Forms.Label lblSeleccionarMarca;
        private System.Windows.Forms.Label lblExportMarcas;
    }
}