﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Net;

namespace klsync
{
    class csIdiomas
    {
        public string[] idiomasActivosPS()
        {

            csSqlConnects sqlConnect = new csSqlConnects();

            string[] codIdiomas = new string[sqlConnect.obtenerListaDatos("KLS_ESYNC_IDIOMAS", "", "IDIOMAPS").Count()];
            codIdiomas = sqlConnect.obtenerListaDatos("KLS_ESYNC_IDIOMAS", "", "IDIOMAPS");
            return codIdiomas;


        }

        public void fixLanguages()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Clear();
            dict.Add("de", "ALE");
            dict.Add("es", "CAS");
            dict.Add("ca", "CAT");
            dict.Add("us", "ING");
            dict.Add("gb", "ING");
            dict.Add("en", "ING");
            dict.Add("it", "ITA");
            dict.Add("pt", "POR");

            DataTable ps = new DataTable();

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string consulta = "", tabla = "KLS_ESYNC_IDIOMAS", idiomaps = "";


            sql.borrarDatosSqlTabla(tabla);
            ps = mysql.cargarTabla("select id_lang, iso_code from ps_lang where active = 1");
            foreach (DataRow dr in ps.Rows)
            {
                foreach (KeyValuePair<string, string> entry in dict)
                {
                    idiomaps = dr["iso_code"].ToString();
                    if (entry.Key == idiomaps)
                    {
                        consulta = "insert into " + tabla + " (IDIOMAA3, IDIOMAPS) values ('" + entry.Value + "'," + dr["id_lang"].ToString() + ")";
                        csUtilidades.ejecutarConsulta(consulta, false);
                        break;
                    }
                }
            }
        }

        public string getIdioma(string codIdioma)
        {
            string id = "";

            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                if (valuePair.Key.ToString() == codIdioma)
                {
                    id = valuePair.Value.ToString();
                }
            }

            return id;
        }

        public string productName(string codart)
        {
            string descripcion = "";
            csSqlConnects sql = new csSqlConnects();
            string traduccion = "<name>";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                //descripcion = valorArticulo.obtenerTraduccionArticuloA3(codart, "DESCART", valuePair.Value.ToString());
                descripcion = sql.obtenerTraduccionArticuloA3(codart, valuePair.Value.ToString());
                //descripcion = descripcion.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                descripcion = descripcion.Replace("&", "&amp;").Replace("<", "").Replace(">", "").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-").Replace("#", "").Replace("Ø", "").Replace("=", "");
                traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion + "</language>";
            }
            traduccion = traduccion + "</name>";
            return traduccion;
        }

        public string linkRewrite(string codart)
        {
            string descripcion = "";
            csSqlConnects valorArticulo = new csSqlConnects();

            string traduccion = "<link_rewrite>";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                descripcion = valorArticulo.obtenerUrlArticuloA3(codart, "KLS_URL", valuePair.Value.ToString());
                traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion.Replace("&", "&amp;").Replace("<", "").Replace(">", "").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-").Replace("#", "").Replace("Ø", "").Replace("=", "") +"</language>";
            }
            traduccion = traduccion + "</link_rewrite>";
            traduccion=traduccion.Replace(".","");
            return traduccion;
        }

        //public string shortDescription(string codart, DataTable clientToUpdate = null)
        //{
        //    string descripcion = "";
        //    string texto = "";
        //    csSqlConnects valorArticulo = new csSqlConnects();
        //    string traduccion = "<description_short>";
        //    foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
        //    {
        //        int idiomaToUpdate = Convert.ToInt32(valuePair.Key.ToString());
        //        if (clientToUpdate != null)
        //        {
        //            texto = clientToUpdate.Rows[idiomaToUpdate]["description_short"].ToString();
        //            //texto = "<![CDATA[ " + texto + " ]]>";
        //            texto = csGlobal.formatCData(texto);
        //            traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + texto + "</language>";
        //            //traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + "hola hola" + "</language>";
        //        }
        //        else
        //        {
        //            descripcion = valorArticulo.obtenerTraduccionArticuloA3(codart, "TEXTO", valuePair.Value.ToString());
        //            descripcion = WebUtility.HtmlEncode((descripcion.Replace("&", "&amp;")).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-"));
        //            if (descripcion.Length >= 400)
        //            {
        //                if (descripcion != null || descripcion.Length > 400 || descripcion.IndexOf(" ", 400) != -1)
        //                {
        //                    descripcion = descripcion.Substring(0, descripcion.IndexOf(" ", 385)) + "(...)";
        //                }
        //            }

        //            traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion + "</language>";
        //        }

        //    }
        //    traduccion = traduccion + "</description_short>";
        //    return traduccion;
        //}


        public string shortDescription(string codart, DataTable clientToUpdate = null)
        {
            string descripcion = "";
            string texto = "";
            csSqlConnects valorArticulo = new csSqlConnects();
            int maxCaracteres = csUtilidades.obtenerShortDescriptionLength();
            string traduccion = "<description_short>";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                int idiomaToUpdate = Convert.ToInt32(valuePair.Key.ToString());
                if (clientToUpdate != null)
                {
                    texto = clientToUpdate.Rows[idiomaToUpdate]["description_short"].ToString();
                    texto = csGlobal.formatCData(texto);
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + texto + "</language>";
                }
                else
                {
                    descripcion = valorArticulo.obtenerTraduccionArticuloA3(codart, "TEXTO", valuePair.Value.ToString());
                    descripcion = WebUtility.HtmlEncode((descripcion.Replace("&", "&amp;")).Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-"));
                    if (descripcion.Length >= maxCaracteres)
                    {
                        if (descripcion != null || descripcion.Length > maxCaracteres || descripcion.IndexOf(" ", maxCaracteres) != -1)
                        {
                            descripcion = descripcion.Substring(0, descripcion.IndexOf(" ", (maxCaracteres - 10))) + "(...)";
                        }
                    }

                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion + "</language>";
                }

            }
            traduccion = traduccion + "</description_short>";
            return traduccion;
        }


        public string description(string codart, DataTable clientToUpdate = null)
        {
            string descripcion = "";
            string texto = "";
            csSqlConnects valorArticulo = new csSqlConnects();
            string traduccion = "<description>";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                int idiomaToUpdate = Convert.ToInt32(valuePair.Key.ToString());
                if (clientToUpdate != null)
                {
                    texto = clientToUpdate.Rows[idiomaToUpdate]["description"].ToString();
                    texto = csGlobal.formatCData(texto);
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + texto + "</language>";
                }
                else
                {
                    descripcion = valorArticulo.obtenerTraduccionArticuloA3(codart, "KLS_DESC_LARGA", WebUtility.HtmlEncode((valuePair.Value.ToString())).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-"));
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion + "</language>";
                }

                if (csGlobal.conexionDB == "Thagson")
                {
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descripcion + "</language>"; 
                }
            }
            traduccion = traduccion + "</description>";
            return traduccion;
        }

        public string idiomasDefinidos(DataTable clienToUpdate = null)
        {
            csSqlConnects valorArticulo = new csSqlConnects();
            string traduccion = "";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\"></language>";
            }


            return traduccion;
        }

        public string idiomasDefinidos(string descfinal)
        {
            csSqlConnects valorArticulo = new csSqlConnects();
            string traduccion = "";
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\">" + descfinal + "</language>";
            }

            return traduccion;
        }

        public string articuloCategoria(string codart, DataTable articuloToUpdate)
        {
            string traduccion = "<categories>";
            string[] categoria;
            csSqlConnects categoriasArticulo = new csSqlConnects();

            if (articuloToUpdate.Rows.Count > 0)
            {
                string categoriasPs = articuloToUpdate.Rows[0]["categorias"].ToString();
                categoria = categoriasPs.Split(',');

            }
            else
            {
                categoria = new string[categoriasArticulo.obtenerCategoriasArticulo(codart).Count()];
                categoria = categoriasArticulo.obtenerCategoriasArticulo(codart);
            }
            foreach (string idCategoria in categoria)
            {
                traduccion = traduccion + "<category><id>" + idCategoria + "</id></category>";
            }
            traduccion = traduccion + "</categories>";
            return traduccion;
        }
    }
}
