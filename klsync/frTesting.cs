﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Collections.Specialized;
using System.Threading.Tasks;
using RestSharp;
using System.Diagnostics;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;

//Añadido para test Json to REpasat
//using System.Net.Http.HttpMessageInvoker;
using System.Net.Http;
//using RestSharp;
//using Bukimedia.PrestaSharp.Factories;

namespace klsync
{
    public partial class frTesting : Form
    {
        #region Direcciones
        string consulta_a3 = "SELECT DIRENT.DIRENT1 AS DIR, DIRENT.DTOENT AS ZIPCODE, CLIENTES.KLS_CODCLIENTE AS CUSTOMER, DIRENT.IDDIRENT AS ID_PS1 " +
        " FROM DIRENT " +
        " LEFT JOIN CLIENTES ON LTRIM(CLIENTES.CODCLI) = LTRIM(DIRENT.CODCLI) " +
        " WHERE ID_PS IS NOT NULL AND ID_PS <> '0' ORDER BY CUSTOMER ASC";

        string consulta_ps = "select address1 as DIR, postcode AS ZIPCODE, id_customer AS CUSTOMER, kls_id_dirent AS ID_PS2, id_address as ID from ps_address order by id_customer asc";
        string consulta_stock_a3 = "";

        csSqlConnects sql = new csSqlConnects();
        csMySqlConnect mysql = new csMySqlConnect();
        private static frTesting m_FormDefInstance;
        public static frTesting DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frTesting();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frTesting()
        {
            InitializeComponent();


/*            if (csGlobal.conexionDB.ToUpper().Contains("THAGSON"))
            {
                txtThagsonCaracteristicas.Visible = false;
                btnCaracteristicasThagson.Visible = false;
            }*/
            //csUtilidades.cargarDGV(dgvA3, consulta_a3, csUtilidades.SQL);
            //csUtilidades.cargarDGV(dgvPS, consulta_ps, csUtilidades.MYSQL);
        }



        private void actualizarDesdeAbajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string customerA3 = "", customerPS = "", dirA3 = "", dirPS = "", idps_PS, id_address = "", consulta = "";
            //string idps_A3 = "";
            //foreach (DataRow a3 in ((DataTable)dgvA3.DataSource).Rows)
            //{
            //    dirA3 = a3["DIR"].ToString();
            //    customerA3 = a3["CUSTOMER"].ToString();
            //    idps_A3 = Convert.ToDouble(a3["ID_PS1"].ToString()).ToString();
            //    foreach (DataRow ps in ((DataTable)dgvPS.DataSource).Rows)
            //    {
            //        dirPS = ps["DIR"].ToString();
            //        customerPS = ps["CUSTOMER"].ToString();
            //        idps_PS = ps["ID_PS2"].ToString();
            //        id_address = ps["ID"].ToString();
            //        if (dirA3 == dirPS && customerA3 == customerPS && idps_A3 != idps_PS && Convert.ToInt32(idps_A3) > 0)
            //        {
            //            consulta = "update ps_address set kls_id_dirent = " + idps_A3 + " where id_address = " + id_address;
            //            csUtilidades.ejecutarConsulta(consulta, true);
            //            break;
            //        }
            //    }
            //}
        }
        #endregion

        private void btnCodificarImg_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            codificarImagensFMiro();
        }

        private void codificarImagensFMiro()
        {
            MessageBox.Show("Selecciona el csv con las imagenes codificadas");
            DataTable dt = csUtilidades.cargarCSV();

            var codart = "";
            var origen_imagenes = new FolderBrowserDialog();
            var destino_imagenes = new FolderBrowserDialog();
            MessageBox.Show("Selecciona el origen de las fotos");
            origen_imagenes.ShowDialog();
            MessageBox.Show("Selecciona donde quieres que se guarden las fotos codificadas");
            destino_imagenes.ShowDialog();

            // Si el fichero de las imagenes no esta vacio y el csv tenia filas
            if (!string.IsNullOrEmpty(origen_imagenes.SelectedPath) && !string.IsNullOrEmpty(destino_imagenes.SelectedPath) && dt.Rows.Count > 0)
            {
                // Iteramos los ficheros de la carpeta
                string[] files = Directory.GetFiles(origen_imagenes.SelectedPath, "*.jpg", SearchOption.AllDirectories);

                foreach (string file in files)
                {
                    var neatFile = Path.GetFileName(file);

                    DataRow[] dr = dt.Select("FOTO = '" + neatFile + "'");

                    if (dr.Length > 0)
                    {
                        codart = dr[0]["CODART"].ToString().Trim();
                        File.Copy(file, destino_imagenes.SelectedPath + "\\" + codart + ".jpg", true);

                        csUtilidades.ejecutarConsulta(
                            "UPDATE ARTICULO SET KLS_ARTICULO_TIENDA = 'T' WHERE LTRIM(CODART) = '" + codart + "'", false);
                    }
                }

                MessageBox.Show("Completado!");
            }
        }

        private DataTable stockDeArticulosDeLaTienda()
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            List<string> codarts = new List<string>();

            DataTable productos_PS = mysql.cargarTabla("select id_product, reference from ps_product");

            // Recorremos cada uno de ellos y 
            foreach (DataRow producto in productos_PS.Rows)
            {
                codarts.Add(producto["reference"].ToString());
            }

            // Hacemos un string con todos los productos
            string where_in = "";
            codarts = codarts.Distinct().ToList(); // Que no haya repetidos
            string last = codarts[codarts.Count - 1];
            foreach (string codart in codarts)
            {
                if (codart == last)
                {
                    where_in += "'" + codart + "'";
                }
                else
                {
                    where_in += "'" + codart + "',";
                }
            }

            // Mirar si usa tallas
            csSqlScripts scripts = new csSqlScripts();
            DataTable a3 = sql.cargarDatosTablaA3(scripts.consultaStockA3SIPSSI(csGlobal.almacenA3, where_in, null));

            return a3;
        }

        private void toolStripButtonInsertar_Click(object sender, EventArgs e)
        {
            insertarStock();
        }

        private void toolStripButtonBorrar_Click(object sender, EventArgs e)
        {
            borrarStock();
        }

        private void borrarStock()
        {
            if (MessageBox.Show("Vas a borrar el stock, ok?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                csUtilidades.ejecutarConsulta("delete from ps_stock_available where id_product_attribute = 0", true);

                MessageBox.Show("Stock borrado");
            }
            else
            {
                MessageBox.Show("Operación cancelada");
            }
        }

        public void actualizarStock()
        {
            DataTable productos_PS = mysql.cargarTabla("select id_product, reference from ps_product");
            Dictionary<string, string> myDic = new Dictionary<string, string>();
            DataTable a3 = stockDeArticulosDeLaTienda();
            string reference = "";
            if (a3.Rows.Count > 0)
            {
                // Recorremos cada uno de ellos y 
                foreach (DataRow producto in productos_PS.Rows)
                {
                    myDic.Add(producto["id_product"].ToString(), producto["reference"].ToString());
                }

                string unidades = "", id_product_a3 = "";
                List<string> values = new List<string>();
                foreach (DataRow fila in a3.Rows)
                {
                    foreach (KeyValuePair<string, string> entry in myDic)
                    {
                        reference = fila["REFERENCE"].ToString();
                        if (entry.Value == reference)
                        {
                            unidades = fila["UNIDADES"].ToString();
                            id_product_a3 = fila["KLS_ID_SHOP"].ToString();
                            values.Add(" quantity = " + unidades + " where id_product = " + entry.Key + " ");
                        }
                    }
                }

                csUtilidades.UpdateDataBaseWithList("ps_stock_available", values, true);
                //MessageBox.Show("YA HE TERMINADO");
            }
        }

        private void insertarStock()
        {
            borrarStock();

            DataTable productos_PS = mysql.cargarTabla("select id_product, reference from ps_product");
            Dictionary<string, string> myDic = new Dictionary<string, string>();

            // Recorremos cada uno de ellos y 
            foreach (DataRow producto in productos_PS.Rows)
            {
                myDic.Add(producto["id_product"].ToString(), producto["reference"].ToString());
            }

            DataTable a3 = stockDeArticulosDeLaTienda();
            if (a3.Rows.Count > 0)
            {
                string campos = "(id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)";
                string unidades = "", id_product_a3 = "", out_of_stock = "";
                List<string> values = new List<string>();
                string reference = "";
                foreach (DataRow fila in a3.Rows)
                {
                    foreach (KeyValuePair<string, string> entry in myDic)
                    {
                        reference = fila["REFERENCE"].ToString();
                        unidades = fila["UNIDADES"].ToString();
                        out_of_stock = fila["KLS_PERMITIRCOMPRAS"].ToString();

                        if (entry.Value == reference)
                        {
                            values.Add("( " + entry.Key + ",0,1,0, " + unidades + ",0," + out_of_stock + ")");
                        }
                    }
                }

                csUtilidades.WriteListToDatabase("ps_stock_available", campos, values, true);

                MessageBox.Show("PROCESO TERMINADO");
            }
        }

        private void toolStripButtonActualizar_Click(object sender, EventArgs e)
        {
            actualizarStock();
        }

        private void btnActualizarStockTyC_Click(object sender, EventArgs e)
        {
            frStockAvanzado stock = new frStockAvanzado();
            stock.actualizar_stock_tyc();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvStock_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            checkStock();
        }

        private int checkStock()
        {
            int contador = 0;

            //foreach (DataGridViewRow row in dgvStock.Rows)
            //{
            //    string stock_a3 = row.Cells["STOCK_A3"].Value.ToString();
            //    string stock_ps = row.Cells["STOCK_PS"].Value.ToString();

            //    if (stock_a3 != "" && stock_a3 != stock_ps)
            //    {
            //        contador++;
            //        row.DefaultCellStyle.ForeColor = Color.Red;
            //    }
            //}

            return contador;
        }

        private void tsbGuardarRepLog_Click(object sender, EventArgs e)
        {

        }

        private void tsbMostrarDiferenciaRepLog_Click(object sender, EventArgs e)
        {

        }

        private void tsbComparar_Click(object sender, EventArgs e)
        {
        }

        private void toolStripButtonRepLogPrueba_Click(object sender, EventArgs e)
        {
            csRepLog rep = new csRepLog();

            rep.cambiosTabla("ARTICULO");
        }

        private void btnBorrarCat_Click(object sender, EventArgs e)
        {
            sql.borrarDatosSqlTabla("KLS_CATEGORIAS_ART");
            sql.borrarDatosSqlTabla("KLS_CATEGORIAS");

            MessageBox.Show("COMPLETADO");
        }

        private void btnBorrarCar_Click(object sender, EventArgs e)
        {
            sql.borrarDatosSqlTabla("KLS_CARACTERISTICAS_ART");
            sql.borrarDatosSqlTabla("KLS_CARACTERISTICAS");

            MessageBox.Show("COMPLETADO");
        }

        private void btnBorrarMarcas_Click(object sender, EventArgs e)
        {
            csUtilidades.ejecutarConsulta("update articulo set kls_codmarca = null", false);

            MessageBox.Show("COMPLETADO");
        }

        private void bajarCats(string where_in = "")
        {
            try
            {
                // Si no hay categorias, las bajamos
                int a3_cat = sql.cargarDatosTablaA3("select * from kls_categorias").Rows.Count;
                if (a3_cat == 0)
                {
                    // Creamos las categorias en KLS_CATEGORIAS
                    DataTable cat = mysql.cargarTabla("select ps_category.id_category as KLS_COD_PS, ps_category.id_parent as KLS_ID_PARENT, ps_category_lang.name AS KLS_DESCCATEGORIA from ps_category left join ps_category_lang on ps_category.id_category = ps_category_lang.id_category WHERE ps_category_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS() + ((where_in != "") ? " and ps_category.id_category in (" + where_in + ") " : ""));
                    if (csUtilidades.verificarDt(cat))
                    {
                        foreach (DataRow item in cat.Rows)
                        {
                            string id_category = item["KLS_COD_PS"].ToString();
                            string name = item["KLS_DESCCATEGORIA"].ToString();
                            string id_parent = item["KLS_ID_PARENT"].ToString();
                            csUtilidades.ejecutarConsulta("insert into kls_categorias (KLS_COD_PS, KLS_CODCATEGORIA, KLS_DESCCATEGORIA, KLS_ID_CATPARENT) " +
                                        " VALUES ('" + id_category + "','" + id_category + "','" + name.Replace("'", "") + "', '" + id_parent + "')", false);
                        }
                    }
                }

                // Bajamos todas las categorias en un datatable
                DataTable cat_product = mysql.cargarTabla("select reference, ps_category_product.id_category from ps_category_product left join ps_product_lang on ps_product_lang.id_product = ps_category_product.id_product left join ps_product on ps_product.id_product = ps_category_product.id_product where id_lang = " + csUtilidades.idIdiomaDefaultPS() + " and reference is not null order by reference asc");
                string codart = "", campos = "", consulta = "", valores = "";
                List<string> values = new List<string>();
                //csSingletonSQL single = new csSingletonSQL(csGlobal.ServerA3, csGlobal.databaseA3, csGlobal.userA3, csGlobal.passwordA3);
                if (csUtilidades.verificarDt(cat_product))
                {
                    foreach (DataRow item in cat_product.Rows)
                    {
                        string reference = item["reference"].ToString();
                        if (!String.IsNullOrEmpty(reference))
                        {
                            //codart = sql.obtenerCampoTabla("(SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + reference + "')");
                            codart = "(SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + item["reference"].ToString() + "')";
                            //codart = single.obtenerCampoTabla("SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + item["reference"].ToString() + "'");
                            campos = "(CODART, KLS_CODCATEGORIA)";
                            valores = "(" + codart + ", " + Convert.ToInt32(item["id_category"].ToString()) + ")";

                            consulta = "INSERT INTO kls_categorias_art " + campos + " VALUES " + valores;
                            //single.ejecutarConsulta(consulta);
                            csUtilidades.ejecutarConsulta(consulta, false);
                        }
                    }

                    //csUtilidades.WriteListToDatabase("kls_categorias_art", campos, values, csUtilidades.SQL, true);
                }
            }
            catch (Exception ex)
            {
            }
        }

        // FUNCIONA - CATEGORIAS
        private void btnDownCat_Click(object sender, EventArgs e)
        {
            bajarCats();
        }

        private void bw_caracteristicas(object sender, DoWorkEventArgs e)
        {
            // Creamos las caracteristicas en KLS_CARACTERISTICAS
            string consulta_ps = "select ps_feature.id_feature as KLS_COD_PS, ps_feature_value_lang.id_feature_value KLS_CODCARACTERISTICA, ps_feature_lang.name AS KLS_DESCCARACTERISTICA, ps_feature_value_lang.value AS KLS_DESCVALORCARACTERISTICA From ps_feature Inner Join ps_feature_lang On ps_feature_lang.id_feature = ps_feature.id_feature Inner Join ps_feature_value On ps_feature.id_feature = ps_feature_value.id_feature Inner Join ps_feature_value_lang On ps_feature_value_lang.id_feature_value = ps_feature_value.id_feature_value Where ps_feature_lang.id_lang = (Select value from ps_configuration where name='PS_LANG_DEFAULT') and ps_feature_value_lang.id_lang= (Select value from ps_configuration where name='PS_LANG_DEFAULT')";
            DataTable dt = mysql.cargarTabla(consulta_ps);

            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string kls_cod_ps = item["KLS_COD_PS"].ToString();
                    string kls_codcaracteristica = item["KLS_CODCARACTERISTICA"].ToString();
                    string kls_valorcaracteristica = kls_codcaracteristica;
                    string kls_desccaracteristica = item["KLS_DESCCARACTERISTICA"].ToString();
                    string kls_descvalorcaracteristica = item["KLS_DESCVALORCARACTERISTICA"].ToString();

                    csUtilidades.ejecutarConsulta("insert into kls_caracteristicas (KLS_COD_PS, KLS_CODCARACTERISTICA, KLS_DESCCARACTERISTICA, KLS_DESCVALORCARACTERISTICA, KLS_VALORCARACTERISTICA) " +
                                " VALUES (" + Convert.ToInt32(kls_cod_ps) + ",'" + kls_codcaracteristica + "','" + kls_desccaracteristica.Replace("'", "") + "','" + kls_descvalorcaracteristica + "','" + kls_valorcaracteristica + "')", false);
                }
            }

            // Bajamos todas las caracteristicas en un datatable
            DataTable feature_product = mysql.cargarTabla("SELECT reference, " +
                    "       ps_feature_product.id_feature " +
                    "FROM ps_feature_product " +
                    "LEFT JOIN ps_product_lang ON ps_product_lang.id_product = ps_feature_product.id_product " +
                    "LEFT JOIN ps_product ON ps_product.id_product = ps_feature_product.id_product " +
                    "WHERE id_lang = " +
                    "    (SELECT value " +
                    "     FROM ps_configuration " +
                    "     WHERE name='PS_LANG_DEFAULT') " +
                    "  AND reference IS NOT NULL " +
                    "  and ps_feature_product.id_feature is not null and ps_feature_product.id_feature <> 0 " +
                    " ORDER BY reference ASC ");

            List<string> values = new List<string>();
            string campos = "(CODART, KLS_VALORCARACTERISTICA)";
            if (csUtilidades.verificarDt(feature_product))
            {
                foreach (DataRow item in feature_product.Rows)
                {
                    string reference = item["reference"].ToString();
                    if (!String.IsNullOrEmpty(reference))
                    {
                        string consulta = "insert into kls_caracteristicas_art SELECT CODART, " + item["id_feature"].ToString() + " FROM ARTICULO WHERE LTRIM(CODART) ='" + item["reference"].ToString() + "'";
                        csUtilidades.ejecutarConsulta(consulta, false);
                    }
                }

                // csUtilidades.WriteListToDatabase("kls_caracteristicas_art", campos, values, csUtilidades.SQL, true);
            }
        }

        // FUNCIONA - CARACTERISTICAS
        private void btnDownCar_Click(object sender, EventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 50;
            btnDownCar.Enabled = false;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += bw_caracteristicas;
            bw.RunWorkerCompleted += bw_caracteristicasCompleted;
            bw.RunWorkerAsync();
        }

        private void bw_caracteristicasCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.MarqueeAnimationSpeed = 0;
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Value = progressBar1.Maximum;

            btnDownCar.Enabled = true;

            MessageBox.Show("CARACTERISTICAS ASIGNADAS");
        }

        // FUNCIONA - MARCAS
        private void btnDownMarcas_Click(object sender, EventArgs e)
        {
            DataTable dt = mysql.cargarTabla("select id_manufacturer as KLS_CODMARCA, name AS KLS_DESCMARCA from ps_manufacturer where active = 1");

            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string codmarca = item["KLS_CODMARCA"].ToString();
                    string descmarca = item["KLS_DESCMARCA"].ToString();
                    csUtilidades.ejecutarConsulta("insert into kls_marcas (KLS_CODMARCA, " +
                                " KLS_DESCMARCA) VALUES ('" + codmarca + "', '" + descmarca + "')", false);
                }
            }

            // Bajamos todas las caracteristicas en un datatable
            DataTable manufacturer_product = mysql.cargarTabla("select reference, id_manufacturer from ps_product where id_manufacturer <> 0");

            if (csUtilidades.verificarDt(manufacturer_product))
            {
                foreach (DataRow item in manufacturer_product.Rows)
                {
                    string consulta = "UPDATE ARTICULO SET KLS_CODMARCA = '" + item["id_manufacturer"].ToString() + "' WHERE LTRIM(CODART) = '" + item["reference"].ToString() + "'";

                    csUtilidades.ejecutarConsulta(consulta, false);
                }

                MessageBox.Show("COMPLETADO");
            }
        }


        //Función para actualizar todas las marcas o fabricantes de A3ERP a Prestashop
        private void actualizarMarcasDeA3aPS()
        {
            DataTable dt = sql.obtenerDatosSQLScript("select codart, kls_id_shop as idproduct, kls_codmarca from articulo where kls_id_shop >0");
            string id_product="";
            string id_manufacturer="";

            foreach (DataRow item in dt.Rows)
            {
                id_product = item["idproduct"].ToString();
                id_manufacturer=item["KLS_CODMARCA"].ToString();
                    string consulta = "update ps_product set id_manufacturer=" + id_manufacturer + " where id_product=" + id_product;

                    csUtilidades.ejecutarConsulta(consulta, true);
            }
            MessageBox.Show("COMPLETADO");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            actualizarMarcasDeA3aPS();
        }
        
        private void btnUpdateStock_Click(object sender, EventArgs e)
        {

            btnUpdateStock.Enabled = false;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 50;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            csStocks stock = new csStocks();
            stock.stock();
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.MarqueeAnimationSpeed = 0;
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Value = progressBar1.Maximum;


            btnUpdateStock.Enabled = true;
            MessageBox.Show("STOCK ACTUALIZADO");
        }

        private void btnDeleteProductAttribute_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseas borrar las tablas ps_product_attribute y ps_product_attribute_shop??", "Atencion", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                csUtilidades.ejecutarConsulta("delete from ps_product_attribute", true);
                csUtilidades.ejecutarConsulta("delete from ps_product_attribute_shop", true);
                csUtilidades.ejecutarConsulta("delete from ps_stock_available where id_product_attribute <> 0", true);

                MessageBox.Show("TABLAS BORRADAS");
            }
            else
            {
                MessageBox.Show("Operacion cancelada");
            }
        }

        private void btnSetProductAttribute_Click(object sender, EventArgs e)
        {
            frDialogInput input = new frDialogInput("Introduce el id_product que quieres setear: ");
            if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string id_product = csGlobal.comboFormValue;

                csTallasYColoresV2 TyC = new csTallasYColoresV2();
                TyC.inicializarTallasYColores(id_product);

                MessageBox.Show("Tallas y colores inicializadas para el id_product " + id_product);
            }

        }

        private void btnIdParent_Click(object sender, EventArgs e)
        {
            DataTable dt = sql.cargarDatosTablaA3("select * from kls_categorias");

            foreach (DataRow item in dt.Rows)
            {
                string id_category = item["kls_cod_ps"].ToString();
                string id_parent = item["kls_id_catparent"].ToString();

                csUtilidades.ejecutarConsulta("update ps_category set id_parent = " + id_parent + " where id_category " + id_category,
                    true);
            }
        }

        /// <summary>
        /// Ocultar los productos de la tienda que no tengan imagen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProductsWithNoImage_Click(object sender, EventArgs e)
        {
            DataTable images = mysql.cargarTabla("select id_product from ps_image");
            DataTable products = mysql.cargarTabla("select id_product from ps_product where active = 1");

            DataTable final = csUtilidades.getDiffDataTables(products, images);

            if (csUtilidades.verificarDt(final))
            {
                foreach (DataRow item in final.Rows)
                {
                    csUtilidades.ejecutarConsulta("update ps_product set active = 0 where id_product = " + item["id_product"].ToString(), true);
                    csUtilidades.ejecutarConsulta("update ps_product_shop set active = 0 where id_product = " + item["id_product"].ToString(), true);
                }

                MessageBox.Show("LOS ARTICULOS SIN IMAGEN HAN SIDO OCULTADOS");
            }
            else
            {
                MessageBox.Show("NO HAY ARTICULOS SIN IMAGEN");
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            csRepLog replog = new csRepLog();
            replog.cambiosTabla("TALLAS");
        }

        private void btnUpdateCategories_Click(object sender, EventArgs e)
        {
            actualizarCategorias();
        }

        public void actualizarCategorias()
        {
            DataTable ps = mysql.cargarTabla("select ltrim(id_category) as id_category from ps_category");
            DataTable a3 = sql.cargarDatosTablaA3("select ltrim(kls_cod_ps) as id_category from kls_categorias");

            DataTable final = csUtilidades.getDiffDataTables(ps, a3);
            string where_in = "";

            // Si hay resultados significa que hay nuevas categorias para bajar
            if (csUtilidades.verificarDt(final))
            {
                foreach (DataRow item in final.Rows)
                {
                    where_in += "'" + item["id_category"].ToString() + "',";
                }

                where_in = where_in.TrimEnd(',');

                bajarCats(where_in);
            }
        }

        private void btAsignarColores_Click(object sender, EventArgs e)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable dtAttributeGroupPS = new DataTable();
            DataTable dtAttributesPS = new DataTable();
            DataTable dtAttributeGroupA3 = new DataTable();
            DataTable dtAttributesA3 = new DataTable();

            string idGroupPS = "";
            string idGroupA3 = "";
            string tipoGrupo = "";
            string idAttribute = "";
            string colorHexadec = "";
            string anexoColor = "";

            dtAttributeGroupA3 = sqlConnect.obtenerDatosSQLScript("select LTRIM(CODFAMTALLA) AS CODFAMTALLA, KLS_GROUP_TYPE from FAMILIATALLA where LTRIM(codfamtalla) not in ('0')");
            dtAttributesA3 = sqlConnect.obtenerDatosSQLScript("select ID,KLS_CODCOLOR FROM TALLAS WHERE KLS_CODCOLOR IS NOT NULL");

            foreach (DataRow drGroupA3 in dtAttributeGroupA3.Rows)
            {
                tipoGrupo = drGroupA3["KLS_GROUP_TYPE"].ToString();
                idGroupA3 = drGroupA3["CODFAMTALLA"].ToString();
                try
                {
                    if (tipoGrupo == "color")
                    {
                        anexoColor = " is_color_group = 1 , ";
                    }
                    else
                    {
                        anexoColor = " is_color_group = 0 , ";
                    }

                    csUtilidades.ejecutarConsulta("update ps_attribute_group set " + anexoColor + " group_type='" + tipoGrupo + "' where kls_codFamiliaA3='" + idGroupA3 + "'", true);
                }
                catch (Exception es)
                { }
            }

            foreach (DataRow drAttributeA3 in dtAttributesA3.Rows)
            {
                idAttribute = drAttributeA3["ID"].ToString();
                colorHexadec = drAttributeA3["KLS_CODCOLOR"].ToString();
                try
                {
                    csUtilidades.ejecutarConsulta("update ps_attribute set color='" + colorHexadec + "' where id_attribute=" + idAttribute, true);
                }
                catch (Exception es)
                { }
            }

        }

        private void btSetColorsPSToA3_Click(object sender, EventArgs e)
        {

            csSqlConnects sqlConnect = new csSqlConnects();
            csMySqlConnect mySqlConnect = new csMySqlConnect();

            DataTable dtAttributeGroupPS = new DataTable();
            DataTable dtAttributesPS = new DataTable();
            DataTable dtAttributeGroupA3 = new DataTable();
            DataTable dtAttributesA3 = new DataTable();

            string idGroupPS = "";
            string idGroupA3 = "";
            string tipoGrupo = "";
            string idAttribute = "";
            string colorHexadec = "";

            dtAttributeGroupPS = mySqlConnect.obtenerDatosPS("select kls_codFamiliaA3,group_type from ps_attribute_group");
            dtAttributesPS = mySqlConnect.obtenerDatosPS("select id_attribute,color from ps_attribute where color is not null");

            foreach (DataRow drGroupA3 in dtAttributeGroupPS.Rows)
            {
                tipoGrupo = drGroupA3["group_type"].ToString();
                idGroupA3 = drGroupA3["kls_codFamiliaA3"].ToString();
                try
                {
                    csUtilidades.ejecutarConsulta("update FAMILIATALLA set KLS_GROUP_TYPE ='" + tipoGrupo + "' where LTRIM(CODFAMTALLA)='" + idGroupA3 + "'", false);
                }
                catch (Exception es)
                { }
            }

            foreach (DataRow drAttributeA3 in dtAttributesPS.Rows)
            {
                idAttribute = drAttributeA3["id_attribute"].ToString();
                colorHexadec = drAttributeA3["color"].ToString();
                try
                {
                    csUtilidades.ejecutarConsulta("update TALLAS set KLS_CODCOLOR='" + colorHexadec + "' where ID=" + idAttribute, false);
                }
                catch (Exception es)
                { }
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            csUtilidades.ejecutarConsulta("delete from KLS_ATRIBUTOS_ART",false);
            csUtilidades.ejecutarConsulta("delete from KLS_ATRIBUTOS",false);

            MessageBox.Show("ATRIBUTOS DE SQL BORRADOS");
        }


        private void btnFranchutes_Click(object sender, EventArgs e)
        {
            // 1. Cargar en un datatable las referencias de todos los productos
            DataTable articulos = sql.cargarDatosTablaA3("select ltrim(codart) as codart from articulo where ltrim(codart) <> '0'");

            // 2. Cargar en un string todos los ficheros de una determinada carpeta
            var fbd = new FolderBrowserDialog();
            var fbd2 = new FolderBrowserDialog();
            fbd.SelectedPath = @"C:\Users\Alex\Desktop\images\franchutes\catalog";
            fbd2.SelectedPath = @"C:\Users\Alex\Desktop\images\franchutes\catalog\final";
            int contador = 1;
            if (!string.IsNullOrEmpty(fbd.SelectedPath) && !string.IsNullOrEmpty(fbd2.SelectedPath))
            {
                string[] files = Directory.GetFiles(fbd.SelectedPath, "*.jpg", SearchOption.AllDirectories);

                // Verificar que hay articulos
                if (csUtilidades.verificarDt(articulos))
                {
                    foreach (DataRow dr in articulos.Rows)
                    {
                        foreach (string file in files)
                        {
                            string codart = dr["codart"].ToString();
                            string filename = Path.GetFileName(file);
                            // Si el fichero contiene el codart en el nombre
                            if (filename.Contains(codart))
                            {
                                contador = 0;
                                string fullpath = Path.GetFullPath(file);

                                try
                                {
                                    contador++;
                                    File.Copy(fullpath, Path.Combine(fbd2.SelectedPath, codart + "-" + contador + ".jpg"), false);
                                }
                                catch (IOException ex)
                                {
                                    for (int i = 2; i < 50; i++)
                                    {
                                        // Parte clave del algoritmo
                                        if (!File.Exists(Path.Combine(fbd2.SelectedPath, codart + "-" + i.ToString() + ".jpg")))
                                        {
                                            File.Copy(fullpath, Path.Combine(fbd2.SelectedPath, codart + "-" + i.ToString() + ".jpg"), false);
                                            break;
                                        }
                                    }
                                }

                            }

                        }
                    }
                }

                MessageBox.Show("Completado");
            }
        }

        private void btnSubirCatA3toPS_Click(object sender, EventArgs e)
        {
            if ("¿Quieres subir todas las categorias a Prestashop?".what())
            {
                try
                {
                    DataTable ps = mysql.cargarTabla("select id_category, id_product from ps_category_product");
                    DataTable a3 = sql.cargarDatosTablaA3("SELECT KLS_CODCATEGORIA, KLS_ID_SHOP FROM KLS_CATEGORIAS_ART LEFT JOIN ARTICULO ON ARTICULO.CODART = KLS_CATEGORIAS_ART.CODART WHERE KLS_ID_SHOP > 0");
                    if (a3.hasRows() && ps.hasRows())
                    {
                        foreach (DataRow item in a3.Rows)
                        {
                            string id_category = item["KLS_CODCATEGORIA"].ToString().Trim();
                            string id_product = item["KLS_ID_SHOP"].ToString().Trim();

                            DataTable search = ps.buscar(string.Format("id_product = {0} and id_category = {1}", id_product, id_category));

                            if (!search.hasRows())
                            {
                                string consulta = string.Format("insert into ps_category_product (id_category,id_product) values({0}, {1})", id_category, id_product);
                                csUtilidades.ejecutarConsulta(consulta, true);
                            }
                        }

                        MessageBox.Show("Operacion completada");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("La operacion no se ha podido llevar a cabo.\n\n Error: " + ex.Message);
                }
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void btnSubirImagen_Click(object sender, EventArgs e)
        {
            frSeleccionTallaColor TyC = new frSeleccionTallaColor("294");
            TyC.Show();
        }

        private void btReorganizarAttributos_Click(object sender, EventArgs e)
        {
            DataTable dtAttributeGroups = mysql.cargarTabla("select id_attribute_group from ps_attribute_group");
            string grupo = "";
            string attribute = "";
            int position = 0;
            foreach (DataRow fila in dtAttributeGroups.Rows)
            {
                position = 0;
                grupo = fila["id_attribute_group"].ToString();
                DataTable dtAttribues = mysql.cargarTabla("select * from ps_attribute where id_attribute_group=" + grupo + " order by position");
                foreach (DataRow filaAttributos in dtAttribues.Rows)
                {
                    attribute = filaAttributos["id_attribute"].ToString();
                    mysql.actualizarGenerica("update ps_attribute set position =" + position + " where id_attribute=" + attribute);
                    position++;
                }
            }
            MessageBox.Show("Reordenación Completada");
        }

        public void resetearTyC()
        {
            string ps_product_attribute = "delete from ps_product_attribute";
            string ps_product_attribute_combination = "delete from ps_product_attribute_combination";
            string ps_product_attribute_shop = "delete from ps_product_attribute_shop";
            string ps_stock_available = "delete from ps_stock_available where id_product_attribute>0";

            csUtilidades.ejecutarConsulta(ps_product_attribute, true);
            csUtilidades.ejecutarConsulta(ps_product_attribute_combination, true);
            csUtilidades.ejecutarConsulta(ps_product_attribute_shop, true);
            csUtilidades.ejecutarConsulta(ps_stock_available, true);
        }

        private void btResetAttributes_Click(object sender, EventArgs e)
        {
            string preguntaSeguridad = "Vas a borrar todas las combinaciones de Atributos de Prestashop" + "\n" +
                "¿Estás seguro?";
            DialogResult result = MessageBox.Show(preguntaSeguridad, "Resetear Atributos", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                resetearTyC();
                MessageBox.Show("Proceso terminado");
            }
        }


        private void cargarPedidos()
        {
            string consulta = "SELECT DISTINCT id_order, " +
                            "                reference, " +
                            "                firstname, " +
                            "                lastname, " +
                            "                name, " +
                            "                payment, " +
                            "                invoice_date, " +
                            "                delivery_date, " +
                            "                ps_orders.date_add, " +
                            "                ps_orders.date_upd " +
                            "FROM ps_orders " +
                            "LEFT JOIN ps_order_state_lang ON ps_order_state_lang.id_order_state = ps_orders.current_state " +
                            "left join ps_customer on ps_customer.id_customer = ps_orders.id_customer " +
                            "where ps_order_state_lang.id_lang = " + csUtilidades.idIdiomaDefaultPS();

            csUtilidades.addDataSource(dgvPedidos, mysql.cargarTabla(consulta));
        }

        private void borrarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvPedidos.Rows.Count > 0)
            {
                int seleccion = dgvPedidos.SelectedRows.Count;
                string id_order = "";
                if (seleccion > 0)
                {
                    if (string.Format("VAS A BORRAR {0} PEDIDOS, ESTÁS SEGURO???", seleccion).what())
                    {
                        csPrestashop presta = new csPrestashop();
                        foreach (DataGridViewRow item in dgvPedidos.SelectedRows)
                        {
                            id_order = item.Cells["id_order"].Value.ToString();
                            presta.borrarPedido(id_order);
                        }

                        string.Format("{0} pedidos borrados", seleccion).mb();
                        cargarPedidos();
                    }
                }
            }
        }


        //Cambiar estado de pedidos
        private void cambiarEstadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataTable dt = mysql.cargarTabla("select id_order_state, name from ps_order_state_lang where id_lang = " + csUtilidades.idIdiomaDefaultPS());

            frDialogCombo combo = new frDialogCombo(dt, "name", "Estado", true);

            if (combo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string name = csGlobal.comboFormValue;
                string id = dt.buscar(string.Format("name = '{0}'", name)).Rows[0]["id_order_state"].ToString();

                if (string.Format("Estas seguro de querer cambiar el estado a {0}??", name).what())
                {
                    csPrestashop presta = new csPrestashop();

                    foreach (DataGridViewRow item in dgvPedidos.SelectedRows)
                    {
                        string id_order = item.Cells["id_order"].Value.ToString();
                        presta.cambiarEstadoPedido(id_order, id);
                    }

                    "Estados cambiados con exito".mb();
                }
            }
        }

        private void btCheckProductCategory_Click(object sender, EventArgs e)
        {
            string id_product = "";
            string id_product_Category = "";
            string id_category_def = "";
            string id_category_product = "";
            DataTable dtProductsPS = new DataTable();
            DataTable dtCategoryProductsPS = new DataTable();

            DataTable dtCategoryUpdate = new DataTable();
            dtCategoryUpdate.Columns.Add("id_product");
            dtCategoryUpdate.Columns.Add("id_category");
            dtCategoryUpdate.Columns.Add("id_category_old");


            csMySqlConnect mySqlconnect = new csMySqlConnect();
            dtProductsPS = mySqlconnect.obtenerDatosPS("Select id_product,id_category_default from ps_product order by id_product");
            dtCategoryProductsPS = mySqlconnect.obtenerDatosPS("Select id_product,id_category from ps_category_product order by id_product");

            foreach (DataRow dr in dtProductsPS.Rows)
            {
                id_product = dr["id_product"].ToString();
                id_category_def = dr["id_category_default"].ToString();

                foreach (DataRow dr2 in dtCategoryProductsPS.Rows)
                {
                    id_product_Category = dr2["id_product"].ToString();
                    id_category_product = dr2["id_category"].ToString();
                    if (id_product == id_product_Category)
                    {
                        if (id_category_def == id_category_product)
                        {
                            break;
                        }
                        else
                        {
                            DataRow fila = dtCategoryUpdate.NewRow();
                            fila["id_product"] = dr["id_product"].ToString();
                            fila["id_category"] = dr["id_category_default"].ToString();
                            fila["id_category_old"] = dr2["id_category"].ToString();
                            dtCategoryUpdate.Rows.Add(fila);
                            break;
                        }
                    }
                }
            }

            foreach (DataRow filaUpd in dtCategoryUpdate.Rows)
            {
                mySqlconnect.actualizarGenerica("Update ps_category_product set id_category=" + filaUpd["id_category"] + " where id_product=" + filaUpd["id_product"] + " and id_category=" + filaUpd["id_category_old"]);
            }

            MessageBox.Show("hola");
        }

        private void btnMoverImagenes_Click(object sender, EventArgs e)
        {
            "Vamos a mover todos los ficheros de todas las subcarpetas y moverlas a la carpeta de destino".mb();

            var fbd = new FolderBrowserDialog();
            var fbd2 = new FolderBrowserDialog();
            "Origen de los archivos:".mb();
            var result = fbd.ShowDialog();
            "Selecciona donde quieres que se guarden los ficheros:".mb();
            var result2 = fbd2.ShowDialog();

            if (!string.IsNullOrEmpty(fbd.SelectedPath) && !string.IsNullOrEmpty(fbd2.SelectedPath))
            {
                try
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories);

                    foreach (string file in files)
                    {
                        string filename = Path.GetFileName(file);

                        string dir = Path.GetFileName(file).ToString();
                        File.Move(file, Path.Combine(fbd2.SelectedPath, dir));
                    }
                }
                catch (Exception ex)
                {
                }


                MessageBox.Show("Completado");
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }
        private void btActualizarFechaRecepcion_Click(object sender, EventArgs e)
        {
            actualizarFechaRecepcionArticulos();
        }

        private void btnCSVMover_Click(object sender, EventArgs e)
        {
            "Vamos a abrir un csv y buscar todos los productos de una determinada carpeta, y luego moverlos a otra carpeta de destino".mb();

            var fbd = new FolderBrowserDialog();
            var fbd2 = new FolderBrowserDialog();
            var fbd3 = new OpenFileDialog();
            "Origen de los archivos:".mb();
            var result = fbd.ShowDialog();
            "Selecciona donde quieres que se guarden los ficheros:".mb();
            var result2 = fbd2.ShowDialog();

            "CSV: ".mb();
            var csv = fbd3.ShowDialog();

            MessageBox.Show("hola");



            if (!string.IsNullOrEmpty(fbd.SelectedPath) && !string.IsNullOrEmpty(fbd2.SelectedPath) && (fbd3 != null))
            {
                try
                {
                    DataTable dt = csUtilidades.ConvertCSVtoDataTable(fbd3.FileName, false);
                    List<string> ficheros = new List<string>();
                    string[] files = Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories);

                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (var file in files)
                        {
                            string n = Path.GetFileNameWithoutExtension(file);
                            string m = row["codarts"].ToString();
                            if (n == m)
                            {
                                File.Copy(file, Path.Combine(fbd2.SelectedPath, Path.GetFileName(file.ToString())), true);
                            }
                        }
                    }

                    MessageBox.Show("Completado");

                }
                catch (Exception ex)
                {
                }

            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        //Función para actualizar la fecha de recepción de los artículos en base a los pedidos de compra de A3ERP
        //Se actualizan las tablas de Prestashop 
        //(sin atributos --> ps_product (campo available_date)
        //(con atributos --> ps_product_attribute (campo available_date)
        private void actualizarFechaRecepcionArticulos()
        {
            DateTime fechaRecepcion;
            string id_product = "";
            string updateString = "";
            string consulta_a3 = "SELECT " +
                " dbo.CABEPEDC.FECHA, dbo.CABEPEDC.NUMDOC, dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.DESCART, dbo.LINEPEDI.ORDLIN, " +
                " dbo.LINEPEDI.UNIDADES, dbo.LINEPEDI.FECENTREGA, dbo.LINEPEDI.SITUACION, dbo.LINEPEDI.UNISERVIDA, " +
                " dbo.LINEPEDI.CODFAMTALLAH, dbo.LINEPEDI.CODTALLAH, dbo.LINEPEDI.CODFAMTALLAV, dbo.LINEPEDI.CODTALLAV, dbo.ARTICULO.TALLAS " +
                " FROM " +
                " dbo.LINEPEDI " +
                " INNER JOIN dbo.ARTICULO ON dbo.LINEPEDI.CODART = dbo.ARTICULO.CODART INNER JOIN " +
                " dbo.CABEPEDC ON dbo.LINEPEDI.IDPEDC = dbo.CABEPEDC.IDPEDC " +
                " WHERE (dbo.LINEPEDI.SITUACION = 'A') AND (dbo.ARTICULO.KLS_ID_SHOP > 0)";

            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(consulta_a3, dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);

            for (int i = 0; i < t.Rows.Count; i++)
            {
                fechaRecepcion = Convert.ToDateTime(t.Rows[i]["FECENTREGA"].ToString());
                id_product = t.Rows[i]["KLS_ID_SHOP"].ToString();
                updateString = "update ps_product set available_date='" + fechaRecepcion.ToString("yyyy/MM/dd") + "' where ps_product.id_product=" + id_product;
                csUtilidades.ejecutarConsulta(updateString, true);
            }
        }

        private void btAlbRegularizacion_Click(object sender, EventArgs e)
        {
            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            a3erp.generarAlbaranRegularizacion();
            a3erp.cerrarEnlace();
        }

        private void tssCombinaciones_Click(object sender, EventArgs e)
        {
            string query_a3 = "select cast(KLS_ID_SHOP as integer) as id_product from articulo where tallas = 'T' and kls_id_shop > 0 order by kls_id_shop desc";
            string query_ps = "select distinct id_product as id_product from ps_product_attribute order by id_product desc";
            string id_product = "";

            var dt = new System.Data.DataTable("combinaciones");
            dt.Columns.Add("combinaciones", typeof(string));

            DataTable a3 = sql.cargarDatosTablaA3(query_a3);
            DataTable ps = mysql.cargarTabla(query_ps);

            foreach (DataRow dr_a3 in a3.Rows)
            {
                string kls_id_shop = dr_a3["id_product"].ToString();
                foreach (DataRow dr_ps in ps.Rows)
                {
                    id_product = dr_ps["id_product"].ToString();

                    // Si coinciden salimos del bucle
                    if (id_product == kls_id_shop)
                    {
                        break;
                    }
                }

                // Si sale del bucle significa que ha pateado 
                // todo el dt y no ha encontrado el articulo
                if (id_product != kls_id_shop)
                {
                    dt.Rows.Add(new Object[]{
                        kls_id_shop
                   });
                }
            }

            string where_in = csUtilidades.delimitarPorComasDataTable(dt, "combinaciones");
            DataTable final = mysql.cargarTabla("select ps_product.id_product, ps_product.reference, ps_product.active as ps_product_active, ps_product_shop.active as ps_product_shop_active from  ps_product  left join ps_product_shop on ps_product_shop.id_product = ps_product.id_product where ps_product.id_product in (" + where_in + ")");
            //DataTable diff = csUtilidades.getDiffDataTables(a3, ps);
            //string where_in = csUtilidades.delimitarPorComasDataTable(diff);
            //csUtilidades.addDataSource(dgvCombinaciones, diff);
            csUtilidades.addDataSource(dgvCombinaciones, final);
            //"Estos articulos tienen tallas pero sus combinaciones no existen en prestashop".mb();
        }

        private void btnActivoInactivo_Click(object sender, EventArgs e)
        {
            cargarActivoInactivo();
        }

        private void cargarActivoInactivo()
        {
            DataTable dt_activoInactivo = mysql.cargarTabla("select ps_product.id_product, ps_product.reference, ps_product.active as ps_product_active, ps_product_shop.active as ps_product_shop_active " +
                                                            "from  ps_product  " +
                                                            "left join ps_product_shop on ps_product_shop.id_product = ps_product.id_product " +
                                                            "where ps_product.active <> ps_product_shop.active ");

            csUtilidades.addDataSource(dgvActivoInactivo, dt_activoInactivo);

            string.Format("Hay {0} registros", dt_activoInactivo.Rows.Count).mb();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (dgvActivoInactivo.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in dgvActivoInactivo.SelectedRows)
                {
                    activarDesactivarProduct("ps_product_shop", dr.Cells["id_product"].Value.ToString(), 1);
                }
            }

            cargarActivoInactivo();
        }

        private void desactivarPsproductshopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvActivoInactivo.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in dgvActivoInactivo.SelectedRows)
                {
                    activarDesactivarProduct("ps_product_shop", dr.Cells["id_product"].Value.ToString(), 0);
                }
            }

            cargarActivoInactivo();
        }

        private void activarDesactivarProduct(string table, string id_product, int active = 1)
        {
            csUtilidades.ejecutarConsulta(string.Format("update {2} set active = {0} where id_product = {1}", active, id_product, table), true);
        }

        private void activarPsproductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvActivoInactivo.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in dgvActivoInactivo.SelectedRows)
                {
                    activarDesactivarProduct("ps_product", dr.Cells["id_product"].Value.ToString(), 1);
                }
            }

            cargarActivoInactivo();
        }

        private void desactivarPsproductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvActivoInactivo.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in dgvActivoInactivo.SelectedRows)
                {
                    activarDesactivarProduct("ps_product", dr.Cells["id_product"].Value.ToString(), 0);
                }
            }

            cargarActivoInactivo();
        }

        private void dgvActivoInactivo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Process.Start("Chrome.exe", csGlobal.nodeTienda + "/index.php?controller=product&id_product=" + dgvActivoInactivo["id_product", e.RowIndex].Value.ToString());
            }
            catch { }
        }

        private void dgvCombinaciones_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Process.Start("Chrome.exe", csGlobal.nodeTienda + "/index.php?controller=product&id_product=" + dgvCombinaciones["id_product", e.RowIndex].Value.ToString());
            }
            catch { }
        }

        private void artículoSeleccionadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvCombinaciones.Rows.Count > 0)
            {
                int productos_seleccionados = dgvCombinaciones.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de sincronizar {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgvCombinaciones.SelectedRows)
                        {
                            TyC.sincronizarAtributosArticulos(item.Cells["id_product"].Value.ToString());
                        }

                        string.Format("{0} productos sincronizados con éxito", productos_seleccionados).mb();
                    }
                }
            }
            else
            {
                "Operación cancelada".mb();
            }
        }

        private void todosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            tyc.sincronizarAtributosArticulos("");
            MessageBox.Show("ARTÍCULOS SINCRONIZADO");
        }

        private void resetearAtributosSelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetAndSyncAttributes();
        }

        private void resetAndSyncAttributes()
        {
            if (dgvCombinaciones.Rows.Count > 0)
            {
                int productos_seleccionados = dgvCombinaciones.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de resetear {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgvCombinaciones.SelectedRows)
                        {
                            TyC.resetearTyCSlow(item.Cells["id_product"].Value.ToString());
                        }

                        "Producto reseteado con exito".mb();

                        if ("Quieres sincronizar estos atributos?".what())
                        {
                            foreach (DataGridViewRow item in dgvCombinaciones.SelectedRows)
                            {
                                TyC.sincronizarAtributosArticulos(item.Cells["id_product"].Value.ToString());
                            }

                            "Atributos sincronizados".mb();
                        }
                        else
                        {
                            "Operación cancelada".mb();
                        }
                    }
                    else
                    {
                        "Operación cancelada".mb();
                    }
                }
            }
        }

        private void generarCombinacionVisibleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarCombinacionConCodart();
        }

        private void generarCombinacionConCodart()
        {
            DataTable combinacionesA3 = new DataTable();
            csTallasYColoresV2 TyC = new csTallasYColoresV2();

            if (dgvCombinaciones.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dr in dgvCombinaciones.SelectedRows)
                {
                    string codart = dr.Cells["reference"].Value.ToString().Trim();
                    string id_product = dr.Cells["id_product"].Value.ToString().Trim();
                    csUtilidades.ejecutarConsulta(string.Format("delete from KLS_COMBINACIONES_VISIBLES where ltrim(codart) = '{0}'", codart), false);

                    combinacionesA3 = TyC.cargarCombinacionesArticulosA3(id_product, true);
                    sql.insertarTodasCombinacionesAtributosA3(combinacionesA3);
                }

            }
        }

        private void btnRegenerarImagenes_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csImagenesPS imagenesPS = new csImagenesPS();
            imagenesPS.regenerarImagenes();
        }

       /* private void btnSincFamilias_Click(object sender, EventArgs e)
        {
            frArticulos art = new frArticulos();
            Thread t = new Thread(art.SyncFamilias);
            t.Start();
        }*/

        private void btnUpdateShortDescription_Click(object sender, EventArgs e)
        {
            frArticulos a = new frArticulos();
            a.actualizarPS_TAG(progressBarTags);
        }

        private void btnPSProductTag_Click(object sender, EventArgs e)
        {
            frArticulos a = new frArticulos();
            a.actualizarPS_PRODUCT_TAG(progressBarTags);
        }

        private void btnCaracteristicasThagson_Click(object sender, EventArgs e)
        {
            if ("¿Quieres proceder con la copia de caracteristicas/categorias?".what())
            {
                List<string> values = new List<string>();
                string fields = "(codart, kls_codcategoria)";
                // Listado de caracteristicas de la tabla articulo
                DataTable articulo_caracteristicas = sql.cargarDatosTablaA3("select car1, car2, car3, codart from articulo where kls_id_shop > 0");
                // Listado de cracteristicas con su id de la categoria correspondiente
                DataTable caracteristicas2 = sql.cargarDatosTablaA3("select codcar, catps from caracteristicas2 where catps is not null");
                // Listado de las categorias existentes en A3, que tocará revisar por si ya existen
                DataTable kls_categorias_art = sql.cargarDatosTablaA3("select * from kls_categorias_art");
                DataTable kls_categorias = sql.cargarDatosTablaA3("select * from kls_categorias");

                if (articulo_caracteristicas.hasRows() && caracteristicas2.hasRows())
                {
                    foreach (DataRow dr in articulo_caracteristicas.Rows)
                    {
                        string car1 = dr["car1"].ToString();
                        string car2 = dr["car2"].ToString();
                        string car3 = dr["car3"].ToString();
                        string codart = dr["codart"].ToString();

                        foreach (DataRow dr2 in caracteristicas2.Rows)
                        {
                            string codcar = dr2["codcar"].ToString();
                            string catps = dr2["catps"].ToString();

                            DataTable search = kls_categorias_art.buscar(string.Format("kls_codcategoria = '{0}' and codart = '{1}'", catps, codart));
                            DataTable search2 = kls_categorias.buscar(string.Format("KLS_CODCATEGORIA = {0}", catps));

                            if (!search.hasRows() && search2.hasRows())
                            {
                                if (car1 == codcar)
                                {
                                    values.Add(string.Format("('{0}',{1})", codart, catps));
                                }
                                if (car2 == codcar)
                                {
                                    values.Add(string.Format("('{0}',{1})", codart, catps));
                                }
                                if (car3 == codcar)
                                {
                                    values.Add(string.Format("('{0}',{1})", codart, catps));
                                }
                            }
                        }
                    }

                    if (values.Count > 0)
                    {
                        csUtilidades.WriteListToDatabase("kls_categorias_art", fields, values, false);
                    }

                    "Operacion finalizada".mb();
                }
                else
                {
                    csUtilidades.operacionCancelada();
                }
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void generarCombinaciónYSincronizarReseteandoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generarCombinacionConCodart();
            resetAndSyncAttributes();
        }

        

        private void button5_Click(object sender, EventArgs e)
        {
              csRepasatWebService rpstWS=new csRepasatWebService();
              //rpstWS.sincronizarObjetoRepasat("products");
        }


        private void btnResetTYCPrestashop_Click(object sender, EventArgs e)
        {
            borrarTallasYColores();
        }



        private void borrarTallasYColores()
        {

            if (MessageBox.Show("Vas a borrar todo lo relacionado con Atributos en Prestashop, ¿Estás seguro?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                csTallasYColoresV2 tyc = new csTallasYColoresV2();
                tyc.vaciarTablasTallasyColores();
                MessageBox.Show("Stock borrado");
            }
            else
            {
                MessageBox.Show("Operación cancelada");
            }
        
        }

        private void button6_Click(object sender, EventArgs e)
        {
         
                csTallasYColoresV2 TyC = new csTallasYColoresV2();
                TyC.inicializarTallasYColores("");

                MessageBox.Show("Tallas y colores inicializadas para el id_product ");
        }

        private void btCheckProductCategory_Click_1(object sender, EventArgs e)
        {
            asignarCategoriaDefault();
        }

        private void asignarCategoriaDefault()
        {
            string idProduct = "";
            string idCategory = "";
            string query="select ps_product.id_product,ps_category_product.id_category " +
                " from ps_product left outer join ps_category_product " +
                " on ps_product.id_product=ps_category_product.id_product where ps_category_product.id_category is null";
            DataTable dt=new DataTable();
            dt = mysql.obtenerDatosPS(query);

            foreach (DataRow dr in dt.Rows)
            {
                idProduct = dr["id_product"].ToString();
                idCategory = dr["id_category"].ToString();
                csUtilidades.ejecutarConsulta("insert into ps_category_product (id_product,id_category) values (" + idProduct + "," + 2 + ")", true);
            }

        
        
        }

        private void btNextAvailableDate_Click(object sender, EventArgs e)
        {
            csStocks stock = new csStocks();
            stock.updateFechaProximaRecepcion();
        }

        private void btReorgLevelCategories_Click(object sender, EventArgs e)
        {
            int i=0;
            int nivel = 0;
            string parentCat = "";
            string cat = "";
            DataTable dt = new DataTable();
            string script="select ps_category.id_category, id_parent, level_depth, name, ps_category.position, ps_category.active " +
                            " from ps_category join ps_category_lang on ps_category_lang.id_category = ps_category.id_category  " +
                            " where id_lang = (Select value from ps_configuration where name='PS_LANG_DEFAULT') and ps_category.id_category>1 " +
                            " order by level_depth asc";
            dt = mysql.cargarTabla(script);

            foreach (DataRow dr in dt.Rows)
            {
                cat = dr["id_category"].ToString();
                if (cat=="20")
                {
                    string hola="";
                }
                parentCat = dr["id_category"].ToString();
                while (parentCat != "1")
                {
                    parentCat= tieneCategoriaPadre(parentCat);
                    nivel++;
                }
                csUtilidades.ejecutarConsulta("update ps_category set level_depth=" + nivel + " where id_category=" + dr["id_category"].ToString(),true);
                nivel = 0;
            }

            




        }

        private string tieneCategoriaPadre(string idCat)
        {
            string parentCat = "";
            DataTable dt = new DataTable();
            string script = "select id_parent " +
                            " from ps_category   " +
                            " where ps_category.id_category=" + idCat;
            dt = mysql.cargarTabla(script);
            parentCat= dt.Rows[0].ItemArray[0].ToString();
            return parentCat;
                
        }

        private void button7_Click(object sender, EventArgs e)
        {
            csBastide bastide = new csBastide();
            bastide.generarFicheroTXTAlmacenLogistica();
        }


        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            cargarPedidos();
        }


        private void btActualizarRecEquivalenciaVilardell(object sender, EventArgs e)
        {
             string query = "insert into ps_surchage_eq_customers " +
                                        " select ps_customer.id_customer, ps_customer.equivalence_surcharge, ps_surchage_eq_customers.id_customer, " +
                                        " ps_surchage_eq_customers.active " +
                                        " from ps_customer left join ps_surchage_eq_customers " +
                                        " on ps_customer.id_customer=ps_surchage_eq_customers.id_customer" +
                                        " where ps_surchage_eq_customers.id_customer is null ";
                        csUtilidades.ejecutarConsulta(query, true);
        }

        private void btCombinacionesVisibles_Click(object sender, EventArgs e)
        {
            btCombinacionesVisibles.Enabled = false;

            if (MessageBox.Show("¿Quieres borrar las combinaciones existentes en la tabla?","Combinaciones Visibles", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                csUtilidades.ejecutarConsulta("DELETE FROM KLS_COMBINACIONES_VISIBLES", false);

            }
            csTallasYColoresV2 TyC = new csTallasYColoresV2();
            TyC.generarTodasLasCombinacionesA3("");
            btCombinacionesVisibles.Enabled = true;
            "Proceso finalizado".mb();
        }

        private void btnReminder_Click(object sender, EventArgs e)
        {
        //    Program.guardarErrorFichero("hola");
            PaymentReminder.csPaymentReminder carteraReminder = new PaymentReminder.csPaymentReminder();
            carteraReminder.obtenerListaEfectos(false);
            carteraReminder.obtenerListaEfectos(true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream("hola.pdf", FileMode.Create));
            doc.Open();

            Paragraph title = new Paragraph();
            title.Font = FontFactory.GetFont(FontFactory.TIMES, 18f, BaseColor.BLUE);
            title.Add("Hola Mundo!!");
            doc.Add(title);

            doc.Add(new Paragraph("Hola Mundo!!"));
            doc.Add(new Paragraph("Parrafo 1"));
            doc.Add(new Paragraph("Parrafo 2"));
            doc.Close();
        }

        private void btnSincFamilias_Click_1(object sender, EventArgs e)
        {

            csUtilidades.ejecutarConsulta("delete from ps_specific_price where reduction_type = 'amount' and id_product_attribute>0", true);

            //TODO:ACTUALZIAR TARIFAS DE HIJOS
            //Seleccionamos los artículos que estan en 
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            DataTable atributosPS = new DataTable();
            DataTable tarifasA3 = new DataTable();
            List<string> values = new List<string>();
            string campos = "(id_product, id_shop, id_customer,id_product_attribute, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to)";
            string idproduct = "", customer = "", precio = "", from = "", to = "", idAttribute = "";

            string selectAtributos = "select * from ps_product_attribute";

            string selectTarifaA3 = "SELECT tarifas.desctarifa, " +
                                    "clientes.kls_codcliente, " +
                                    "clientes.codcli, " +
                                    "articulo.kls_id_shop, " +
                                    "articulo.KLS_ARTICULO_PADRE, " +
                                    "tarifave.* " +
                                    "FROM tarifave " +
                                    "LEFT JOIN tarifas " +
                                    "ON tarifas.tarifa = tarifave.tarifa " +
                                    "INNER JOIN clientes " +
                                    "ON clientes.tarifa = tarifas.tarifa " +
                                    "INNER JOIN articulo " +
                                    "ON articulo.codart = tarifave.codart " +
                                    "WHERE precio > 0 " +
                                    "AND fecmax >= Getdate() " +
                                    "AND kls_codcliente > 0 " +
                                    "AND KLS_ARTICULO_PADRE IS NOT NULL " +
                                    "AND KLS_ID_SHOP IS NULL";

            atributosPS = mysql.cargarTabla(selectAtributos);


            tarifasA3 = sql.cargarDatosTablaA3(selectTarifaA3);

            foreach (DataRow fila in atributosPS.Rows)
            {
                foreach (DataRow fila2 in tarifasA3.Rows)
                {
                    if (fila["reference"].ToString() == fila2["CODART"].ToString().Trim())
                    {

                        idproduct = fila["id_product"].ToString();
                        idAttribute = fila["id_product_attribute"].ToString();
                        customer = fila2["KLS_CODCLIENTE"].ToString();
                        precio = fila2["PRECIO"].ToString();
                        from = Convert.ToDateTime(fila2["FECMIN"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        to = Convert.ToDateTime(fila2["FECMAX"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");

                        if (csGlobal.ivaIncluido.ToUpper() == "SI")
                        {
                            precio = Math.Round(Convert.ToDouble(precio) / 1.21, 3).ToString();
                        }
                        precio = precio.Replace(",", ".");

                        values.Add("(" + idproduct + ", 1, " + customer + ", " + idAttribute +", " + precio + ", 0, 0, 'amount', '" + from + "', '" + to + "')");

                    }

                }

            }
            csUtilidades.WriteListToDatabase("ps_specific_price", campos, values, true);




        }

       

    }
}
