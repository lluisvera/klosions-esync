﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Threading;
using System.IO.Compression;
using RestSharp;
using System.ComponentModel;
using System.Web;

namespace klsync
{
    class csIpFinder
    {

        public class IpFinder
        {
            public static IPAddress GetExternalIp()
            {
                try
                {
                    string whatIsMyIp = "http://whatismyip.com";
                    string getIpRegex = @"(?<=<TITLE>.*)\d*\.\d*\.\d*\.\d*(?=</TITLE>)";
                    WebClient wc = new WebClient();
                    UTF8Encoding utf8 = new UTF8Encoding();
                    string requestHtml = "";
                    try
                    {
                        requestHtml = utf8.GetString(wc.DownloadData(whatIsMyIp));
                    }
                    catch (WebException we)
                    {
                        // do something with exception
                        Console.Write(we.ToString());
                    }
                    Regex r = new Regex(getIpRegex);
                    Match m = r.Match(requestHtml);
                    IPAddress externalIp = null;
                    if (m.Success)
                    {
                        externalIp = IPAddress.Parse(m.Value);
                    }
                    return externalIp;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        //public static IPAddress GetExternalIPAddress()
        //{
        //    IPHostEntry myIPHostEntry = Dns.GetHostEntry(Dns.GetHostName());

        //    foreach (IPAddress myIPAddress in myIPHostEntry.AddressList)
        //    {
        //        byte[] ipBytes = myIPAddress.GetAddressBytes();

        //        if (myIPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        //        {
        //            if (!IsPrivateIP(myIPAddress))
        //            {
        //                return myIPAddress;
        //            }
        //        }
        //    }

        //    return null;
        //}


        //public bool IsPrivateIP(IPAddress myIPAddress)
        //{
        //    if (myIPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        //    {
        //        byte[] ipBytes = myIPAddress.GetAddressBytes();

        //        // 10.0.0.0/24 
        //        if (ipBytes[0] == 10)
        //        {
        //            return true;
        //        }
        //        // 172.16.0.0/16
        //        else if (ipBytes[0] == 172 && ipBytes[1] == 16)
        //        {
        //            return true;
        //        }
        //        // 192.168.0.0/16
        //        else if (ipBytes[0] == 192 && ipBytes[1] == 168)
        //        {
        //            return true;
        //        }
        //        // 169.254.0.0/16
        //        else if (ipBytes[0] == 169 && ipBytes[1] == 254)
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}


        //private bool CompareIpAddress(IPAddress IPAddress1, IPAddress IPAddress2)
        //{
        //    byte[] b1 = IPAddress1.GetAddressBytes();
        //    byte[] b2 = IPAddress2.GetAddressBytes();

        //    if (b1.Length == b2.Length)
        //    {
        //        for (int i = 0; i < b1.Length; ++i)
        //        {
        //            if (b1[i] != b2[i])
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //    return true;
        //}
    }
}
