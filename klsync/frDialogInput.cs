﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frDialogInput : Form
    {
        public frDialogInput(string label, string input = "", string titulo = "")
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.CenterScreen;
            lblSeleccionar.Text = label;
            txtInput.Text = input;
            
            if (titulo != "")
            {
                this.Text = titulo;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                this.Close();

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtInput.Text != "")
            {
                this.DialogResult = DialogResult.OK;

                csGlobal.comboFormValue = txtInput.Text;
            }
            else
            {
                csGlobal.comboFormValue = "";
            }

            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
