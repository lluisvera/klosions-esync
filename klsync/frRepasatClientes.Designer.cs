﻿namespace klsync
{
    partial class frRepasatClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvClientesRPST = new System.Windows.Forms.DataGridView();
            this.menuContextual1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.seleccionarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarARepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btCargarClientes = new System.Windows.Forms.Button();
            this.btCargarClientesA3 = new System.Windows.Forms.Button();
            this.btSincronizarClientes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientesRPST)).BeginInit();
            this.menuContextual1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvClientesRPST
            // 
            this.dgvClientesRPST.AllowUserToAddRows = false;
            this.dgvClientesRPST.AllowUserToDeleteRows = false;
            this.dgvClientesRPST.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClientesRPST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientesRPST.ContextMenuStrip = this.menuContextual1;
            this.dgvClientesRPST.Location = new System.Drawing.Point(9, 63);
            this.dgvClientesRPST.Name = "dgvClientesRPST";
            this.dgvClientesRPST.ReadOnly = true;
            this.dgvClientesRPST.Size = new System.Drawing.Size(1018, 492);
            this.dgvClientesRPST.TabIndex = 0;
            // 
            // menuContextual1
            // 
            this.menuContextual1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionarTodoToolStripMenuItem,
            this.exportarARepasatToolStripMenuItem,
            this.borrarClienteToolStripMenuItem});
            this.menuContextual1.Name = "menuContextual1";
            this.menuContextual1.Size = new System.Drawing.Size(171, 70);
            // 
            // seleccionarTodoToolStripMenuItem
            // 
            this.seleccionarTodoToolStripMenuItem.Name = "seleccionarTodoToolStripMenuItem";
            this.seleccionarTodoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.seleccionarTodoToolStripMenuItem.Text = "Seleccionar &Todo";
            this.seleccionarTodoToolStripMenuItem.Click += new System.EventHandler(this.seleccionarTodoToolStripMenuItem_Click);
            // 
            // exportarARepasatToolStripMenuItem
            // 
            this.exportarARepasatToolStripMenuItem.Name = "exportarARepasatToolStripMenuItem";
            this.exportarARepasatToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.exportarARepasatToolStripMenuItem.Text = "&Exportar a Repasat";
            this.exportarARepasatToolStripMenuItem.Click += new System.EventHandler(this.exportarARepasatToolStripMenuItem_Click);
            // 
            // borrarClienteToolStripMenuItem
            // 
            this.borrarClienteToolStripMenuItem.Name = "borrarClienteToolStripMenuItem";
            this.borrarClienteToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.borrarClienteToolStripMenuItem.Text = "&Borrar Cliente";
            this.borrarClienteToolStripMenuItem.Click += new System.EventHandler(this.borrarClienteToolStripMenuItem_Click);
            // 
            // btCargarClientes
            // 
            this.btCargarClientes.Location = new System.Drawing.Point(9, 23);
            this.btCargarClientes.Name = "btCargarClientes";
            this.btCargarClientes.Size = new System.Drawing.Size(110, 34);
            this.btCargarClientes.TabIndex = 1;
            this.btCargarClientes.Text = "Clientes REPASAT";
            this.btCargarClientes.UseVisualStyleBackColor = true;
            this.btCargarClientes.Click += new System.EventHandler(this.btCargarClientes_Click);
            // 
            // btCargarClientesA3
            // 
            this.btCargarClientesA3.Location = new System.Drawing.Point(141, 23);
            this.btCargarClientesA3.Name = "btCargarClientesA3";
            this.btCargarClientesA3.Size = new System.Drawing.Size(110, 34);
            this.btCargarClientesA3.TabIndex = 2;
            this.btCargarClientesA3.Text = "Clientes A3ERP";
            this.btCargarClientesA3.UseVisualStyleBackColor = true;
            this.btCargarClientesA3.Click += new System.EventHandler(this.btCargarClientesA3_Click);
            // 
            // btSincronizarClientes
            // 
            this.btSincronizarClientes.BackgroundImage = global::klsync.Properties.Resources.Refresh;
            this.btSincronizarClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btSincronizarClientes.Location = new System.Drawing.Point(267, 23);
            this.btSincronizarClientes.Name = "btSincronizarClientes";
            this.btSincronizarClientes.Size = new System.Drawing.Size(48, 34);
            this.btSincronizarClientes.TabIndex = 3;
            this.btSincronizarClientes.UseVisualStyleBackColor = true;
            this.btSincronizarClientes.Click += new System.EventHandler(this.btSincronizarClientes_Click);
            // 
            // frRepasatClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1039, 555);
            this.Controls.Add(this.btSincronizarClientes);
            this.Controls.Add(this.btCargarClientesA3);
            this.Controls.Add(this.btCargarClientes);
            this.Controls.Add(this.dgvClientesRPST);
            this.Name = "frRepasatClientes";
            this.Text = "frRepasatClientes";
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientesRPST)).EndInit();
            this.menuContextual1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvClientesRPST;
        private System.Windows.Forms.Button btCargarClientes;
        private System.Windows.Forms.Button btCargarClientesA3;
        private System.Windows.Forms.ContextMenuStrip menuContextual1;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarARepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarClienteToolStripMenuItem;
        private System.Windows.Forms.Button btSincronizarClientes;
    }
}