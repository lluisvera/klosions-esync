﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using System.Net;

namespace klsync
{

    /*
     * Formato que debe tener el fichero: txt
     * Requerimientos técnicos (xml, fichero plano, campos, etc.): fichero plano
     * Forma de envío (email, ftp, etc.): ftp
     */
    public partial class frSeur : Form
    {
        List<string> lista = new List<string>();
        private const string CODIGO_CLIENTE = "676";
        private ToolStripControlHost tsdtIni;
        private ToolStripControlHost tsdtFin;
        private ToolStripControlHost tslblIni;
        private ToolStripControlHost tslblFin;
        private ToolStripControlHost tsComboOrigen;
        private ToolStripControlHost tsTxtFiltro;
        private ToolStripTextBox txtFiltro = new ToolStripTextBox();
        private ComboBox comboFiltro = new ComboBox();
        private DateTimePicker dtpDesde = new DateTimePicker();
        private DateTimePicker dtpHasta = new DateTimePicker();
        private ToolStripButton btnCargarDocumentos = new ToolStripButton();
        private ToolStripButton btnEnviarDocumento = new ToolStripButton();

        private static frSeur m_FormDefInstance;
        public static frSeur DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSeur();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        //private void Form1_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        //{
        //    // if textbox is focused and Enter key was pressed
        //    if (this.txtFiltro.Focused && e.KeyChar == '\r')
        //    {
        //        // click the Go button
        //        this.btnCargarDocumentos.PerformClick();
        //        // don't allow the Enter key to pass to textbox
        //        e.Handled = true;
        //    }
        //}

        public frSeur()
        {
            InitializeComponent();

            Label lblDesde = new Label();
            lblDesde.Text = "Desde:";
            lblDesde.TextAlign = ContentAlignment.MiddleCenter;
            lblDesde.BackColor = Color.Transparent;
            lblDesde.Margin = new Padding(10, 0, 10, 0);

            dtpDesde.Format = DateTimePickerFormat.Short;
            dtpHasta.Format = DateTimePickerFormat.Short;
            dtpDesde.Width = 150;
            dtpHasta.Width = 150;

            comboFiltro.Items.AddRange(new object[] { "Filtrar por cliente", "Filtrar por serie" });
            comboFiltro.SelectedIndex = 0;
            comboFiltro.Width = 150;

            Label lblHasta = new Label();
            lblHasta.Text = "Hasta:";
            lblHasta.TextAlign = ContentAlignment.MiddleCenter;
            lblHasta.BackColor = Color.Transparent;
            lblHasta.Margin = new Padding(10, 0, 10, 0);

            btnCargarDocumentos.Image = klsync.Properties.Resources.magnifying_glass;
            btnCargarDocumentos.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnCargarDocumentos.ImageScaling = ToolStripItemImageScaling.None;
            btnCargarDocumentos.Text = "Cargar facturas";
            btnCargarDocumentos.TextImageRelation = TextImageRelation.ImageAboveText;
            btnCargarDocumentos.Margin = new Padding(10, 0, 10, 0);
            btnCargarDocumentos.Click += new EventHandler(cargarDocumentos); // !

            btnEnviarDocumento.Image = klsync.Properties.Resources.document199;
            btnEnviarDocumento.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            btnEnviarDocumento.ImageScaling = ToolStripItemImageScaling.None;
            btnEnviarDocumento.Text = "Generar documento";
            btnEnviarDocumento.TextImageRelation = TextImageRelation.ImageAboveText;
            btnEnviarDocumento.Margin = new Padding(10, 0, 10, 0);
            btnEnviarDocumento.Click += new EventHandler(generarDocumentos);

            tsComboOrigen = new ToolStripControlHost(comboFiltro);
            txtFiltro.Margin = new Padding(10, 0, 10, 0);
            txtFiltro.BorderStyle = BorderStyle.FixedSingle;
            txtFiltro.Font = new Font(txtFiltro.Font.FontFamily, 12);
            tsTxtFiltro = txtFiltro;
            tslblIni = new ToolStripControlHost(lblDesde);
            tsdtIni = new ToolStripControlHost(dtpDesde);
            tslblFin = new ToolStripControlHost(lblHasta);
            tsdtFin = new ToolStripControlHost(dtpHasta);

            toolstripSeur.Items.Add(tsComboOrigen);
            toolstripSeur.Items.Add(tsTxtFiltro);
            toolstripSeur.Items.Add(tslblIni);
            toolstripSeur.Items.Add(tsdtIni);
            toolstripSeur.Items.Add(tslblFin);
            toolstripSeur.Items.Add(tsdtFin);
            toolstripSeur.Items.Add(btnCargarDocumentos);
            toolstripSeur.Items.Add(btnEnviarDocumento);
        }

        public void cargarDocumentos(object sender, EventArgs e)
        {
            cargarDocumentos();
        }

        private void cargarDocumentos()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string filtroA = comboFiltro.Text;
            string filtroB = txtFiltro.Text;
            string where = "";

            if (filtroA.Trim() != "" && filtroB.Trim() != "")
            {
                where = " AND LTRIM(" + ((filtroA == "Filtrar por cliente") ? "CLIENTES.CODCLI" : "SERIE") + ") = '" + filtroB + "'";
            }

            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            DateTime FechaDesde = dtpDesde.Value;
            DateTime FechaHasta = dtpHasta.Value;
            string strFechaDesde = FechaDesde.ToString("yyyy-MM-dd 00:00:00");
            string strFechaHasta = FechaHasta.ToString("yyyy-MM-dd 23:55:00");
            string poblacion = "", codigopostal = "", provincia = "", pais = "", contacto = "", razonsocial = "", telefono = "", dirent = "", id_state = "";

            string consulta = "SELECT " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER((LTRIM(DIRENT.NOMENT))) " +
            "       END AS NOMENT, " +
            "       dbo.CABEFACV.FECHA, " +
            "       dbo.CABEFACV.CODMON, " +
            "       dbo.CABEFACV.IDFACV, " +
            "       dbo.CABEFACV.NUMDOC, " +
            "       dbo.CABEFACV.CODCLI, " +
            "       dbo.CABEFACV.TOTDOC AS TOTAL, " +
            "       dbo.CABEFACV.TIPIVA1, " +
            "       dbo.CABEFACV.SERIE, " +
            "       dbo.CABEFACV.PARAM9, " +
            "       dbo.LINEFACT.CODALM, " +
            "       dbo.LINEFACT.AFESTOCK, " +
            "       ARTICULO.ESKIT, " + 
            "       dbo.DIRENT.IDDIRENT, " +
            "       dbo.CLIENTES.GENERICO, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER(LTRIM(DIRENT.CONTACTO)) " +
            "       END AS CONTACTO, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE (LTRIM(REPLACE(DIRENT.TELENT1, ' ', ''))) " +
            "       END AS TELEFONO, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE (LTRIM(DIRENT.DTOENT)) " +
            "       END AS ZIPCODE, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER(LTRIM(DIRENT.DIRENT1)) " +
            "       END AS DIRENT, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE (LTRIM(DIRENT.CODPAIS)) " +
            "       END AS CODPAIS, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE dbo.OBTENER_PROVINCIA(LTRIM(DIRENT.CODPROVI)) " +
            "       END AS PROVINCIA, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER(LTRIM(DIRENT.POBENT)) " +
            "       END AS POBLACION " +
            "FROM dbo.CABEFACV " +
            "INNER JOIN dbo.LINEFACT ON dbo.CABEFACV.IDFACV = dbo.LINEFACT.IDFACV " +
            "INNER JOIN dbo.DIRENT ON LTRIM(dbo.CABEFACV.CODCLI) = LTRIM(dbo.DIRENT.CODCLI) " +
            "LEFT OUTER JOIN dbo.PROVINCI ON dbo.CABEFACV.CODPROVI = dbo.PROVINCI.CODPROVI " +
            "AND LTRIM(dbo.PROVINCI.CODPROVI) = LTRIM(dbo.CABEFACV.CODPROVI) " +
            "LEFT OUTER JOIN dbo.CLIENTES ON LTRIM(dbo.CLIENTES.CODCLI) = LTRIM(dbo.CABEFACV.CODCLI) " +
            "LEFT OUTER JOIN dbo.__ORGANIZACION ON LTRIM(dbo.CLIENTES.IDORG) = LTRIM(dbo.__ORGANIZACION.IDORG) " +
            " LEFT JOIN ARTICULO ON ARTICULO.CODART = LINEFACT.CODART " +
            " WHERE (CABEFACV.FECHA BETWEEN '" + Convert.ToDateTime(strFechaDesde).ToString() + "' AND '" +
             Convert.ToDateTime(strFechaHasta).ToString() + "') " + where +
            "GROUP BY       dbo.PROVINCI.NOMPROVI, " +
            "               dbo.CABEFACV.FECHA, " +
            "               dbo.CABEFACV.CODMON, " +
            "               dbo.CABEFACV.IDFACV, " +
            "               dbo.CABEFACV.NUMDOC, " +
            "               dbo.CABEFACV.CODCLI, " +
            "               dbo.CABEFACV.TOTDOC, " +
            "               dbo.CABEFACV.TIPIVA1, " +
            "               dbo.CABEFACV.SERIE, " +
            "               dbo.CABEFACV.PARAM9, " +
            "               dbo.LINEFACT.CODALM, " +
            "               dbo.LINEFACT.AFESTOCK, " +
            "               ARTICULO.ESKIT, " +
            "               dbo.DIRENT.IDDIRENT, " +
            "               dbo.DIRENT.DEFECTO, " +
            "               dbo.DIRENT.DIRENT1, " +
            "               dbo.CLIENTES.GENERICO, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER((LTRIM(DIRENT.CONTACTO))) " +
            "       END, " +
            "               CASE " +
            "                   WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "                   ELSE (LTRIM(DIRENT.CODPAIS)) " +
            "               END, " +
            "               CASE " +
            "    WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "    ELSE dbo.OBTENER_PROVINCIA(LTRIM(DIRENT.CODPROVI)) " +
            "    END, " +
            "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE UPPER(LTRIM(DIRENT.POBENT)) " +
            "       END," +
            "               CASE " +
            "            WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "            ELSE UPPER(LTRIM(DIRENT.DIRENT1)) " +
            "        END, " +
            "        CASE " +
            "    WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "    ELSE UPPER((LTRIM(DIRENT.NOMENT))) " +
            "    END, " +
           "       CASE " +
            "           WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "           ELSE (LTRIM(REPLACE(DIRENT.TELENT1, ' ', ''))) " +
            "       END, " +
            "               CASE " +
            "                   WHEN CLIENTES.GENERICO = 'T' THEN '' " +
            "                   ELSE (LTRIM(DIRENT.DTOENT)) " +
            "               END HAVING (LTRIM(dbo.LINEFACT.CODALM) = '1') " +
            " AND ((dbo.LINEFACT.AFESTOCK = 'T')  OR (ARTICULO.ESKIT = 'T')) " +
            "AND (dbo.DIRENT.DEFECTO = 'T') ";

            //////////////////////////////////////////////////////////////
            // Obtener la direccion de envío a partir del id de la factura
            // cargamos todos los datos en un datatable
            DataTable primera_fase = sql.cargarDatosTablaA3(consulta);
            if (csUtilidades.verificarDt(primera_fase))
            {
                var results = from myRow in primera_fase.AsEnumerable()
                              where myRow.Field<string>("GENERICO") == "T"
                              select myRow;

                if (results.Count() > 0)
                {
                    // Filtramos sólo los genéricos
                    DataTable segunda_fase = results.CopyToDataTable();

                    DataView dv = new DataView(segunda_fase);

                    // obtenemos en otro datatable todos los idfacv
                    DataTable tercera_fase = dv.ToTable(true, "IDFACV");

                    // los separamos por comas en una string
                    string result = csUtilidades.delimitarPorComasDataTable(tercera_fase, "IDFACV");

                    // consulta mysql para obtener los datos de las facturas que nos interesan
                    string consulta_mysql = " select kls_invoice_erp.invoice_number_erp as idfacv, ps_address.company,ps_address.firstname, " +
                    " ps_address.lastname, ps_address.address1, ps_address.postcode, ps_address.id_state, ps_address.id_country, ps_address.city,ps_address.phone" +
                                            " from kls_invoice_erp " +
                                            " left join ps_orders on kls_invoice_erp.invoice_number = ps_orders.invoice_number " +
                                            " left join ps_address on ps_address.id_address = ps_orders.id_address_delivery " +
                                            " where invoice_number_erp in(" + result + ")";

                    // y la cargamos en un datatable
                    DataTable cuarta_fase = mysql.cargarTabla(consulta_mysql);

                    // iteramos sobre el primer datatable (a3) y el último (el de mysql)
                    // para obtener los campos y emparejarlos con el datatable de a3
                    foreach (DataRow dr in primera_fase.Rows)
                    {
                        string idA3 = Convert.ToDouble(dr["idfacv"].ToString()).ToString();
                        foreach (DataRow dr2 in cuarta_fase.Rows)
                        {
                            string idPS = Convert.ToDouble(dr2["idfacv"].ToString()).ToString();

                            razonsocial = dr2["company"].ToString().Trim().ToUpper();
                            telefono = dr2["phone"].ToString().Trim().ToUpper().Replace(" ", "").Replace("-", "");
                            poblacion = dr2["city"].ToString().Trim().ToUpper();
                            contacto = dr2["firstname"].ToString().Trim().ToUpper() + " " + dr2["lastname"].ToString().Trim().ToUpper();
                            dirent = dr2["address1"].ToString().Trim().ToUpper();
                            codigopostal = dr2["postcode"].ToString().Trim().ToUpper();
                            id_state = dr2["id_state"].ToString().ToUpper();

                            if (idPS == idA3)
                            {
                                provincia = sql.obtenerCampoTabla("select nomprovi from provinci where kls_idps = " + id_state );
                                pais = sql.obtenerCampoTabla("select codpais from paises where kls_codpais_ps = " + dr2["id_country"].ToString().ToUpper());

                                string company = dr2["company"].ToString().Trim().ToUpper();
                                if (company == "")
                                    dr["NOMENT"] = contacto;
                                else
                                    dr["NOMENT"] = razonsocial;

                                dr["ZIPCODE"] = codigopostal;
                                dr["DIRENT"] = dirent;
                                dr["CONTACTO"] = contacto;
                                dr["POBLACION"] = poblacion;
                                dr["PROVINCIA"] = provincia;
                                dr["CODPAIS"] = pais;
                                dr["TELEFONO"] = telefono;

                                break;
                            }
                        }
                    }
                    // Fin de obtencion de direccion de envío
                    ////////////////////////////////////////
                }
            }

            csUtilidades.addDataSource(dgvSeur, primera_fase);

            changeColor(dgvSeur);

            if (csUtilidades.verificarDt(primera_fase))
            {
                dgvSeur.Columns["TIPIVA1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvSeur.Columns["TOTAL"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvSeur.Columns["NUMDOC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvSeur.Columns["IDFACV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvSeur.Columns["CODCLI"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvSeur.Columns["TIPIVA1"].DefaultCellStyle.Format = "N2";
                dgvSeur.Columns["TOTAL"].DefaultCellStyle.Format = "N2";
                dgvSeur.Columns["NUMDOC"].DefaultCellStyle.Format = "N0";
                dgvSeur.Columns["IDFACV"].DefaultCellStyle.Format = "N0";
                dgvSeur.Columns["CODCLI"].DefaultCellStyle.Format = "N0";
                dgvSeur.Columns["PARAM9"].Visible = false;

                csUtilidades.contarFilasGrid((DataTable)dgvSeur.DataSource, sw, numFilas);
            }
        }

        private void changeColor(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["PARAM9"].Value.ToString() == "T")
                {
                    row.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
        }

        /*
         ***  SISLOG - Manual de Integración v10
         ***************************************
         ***  PUNTO 4. CABECERAS - ESTRUCTURA GENERAL DE UN FICHERO DE INTERCAMBIO
         ***  PUNTO 6.2 LINEAS   - CONFIRMACIÓN DE LÍNEAS DE PEDIDOS DE SALIDA 
         */
        public void generarDocumentos(object sender, EventArgs e)
        {
            // Cargamos cabecera de albaranes de venta en un dt
            //DataTable headers = csUtilidades.dgv2dtSelectedRows(dgvSeur);
            DataTable headers = csUtilidades.fillDataTableFromDataGridView(dgvSeur, true);

            string year_now = DateTime.Now.Year.ToString();
            string month_now = DateTime.Now.ToString("MM");
            string day_now = DateTime.Now.ToString("dd");
            string hour_now = DateTime.Now.Hour.ToString();
            string minutes_now = DateTime.Now.Minute.ToString();
            string seconds_now = DateTime.Now.ToString("ss");

            

            // Si se ha seleccionado alguna cabecera
            if (headers.Rows.Count > 0)
            {
                csSqlConnects sql = new csSqlConnects();
                string query_lines = "", cadena = "", year = "", month = "", day = "", hour = "", minutes = "";

                for (int i = 0; i < headers.Rows.Count; i++)
                {
                    if (headers.Rows.Count - 1 == i)
                        query_lines += Convert.ToDouble(headers.Rows[i]["IDFACV"].ToString()).ToString();
                    else
                        query_lines += Convert.ToDouble(headers.Rows[i]["IDFACV"].ToString()).ToString() + ",";
                }


                string consulta_lines = "SELECT * FROM LINEFACT left join articulo on articulo.codart = linefact.codart LEFT JOIN ALMACEN ON ALMACEN.CODALM = LINEFACT.CODALM " + ((query_lines != "") ? " WHERE IDFACV IN (" + query_lines + ") " : "") + " AND UNIDADES > 0 AND ALMACEN.DESCALM IN ('SEUR')   AND (LINEFACT.ESCOMPONENTE = 'F' OR LINEFACT.ESKIT = 'T')";
                // Lineas de los albaranes de venta
                DataTable lines = sql.cargarDatosTablaA3(consulta_lines);

                // Instanciamos el objeto Seur
                csSeur seur = new csSeur();

                // Comienzo de cabecera de fichero
                seur.InicioCabeceraFicheroComFic = "CC";
                seur.NombreFicheroComFic = ("sip" + day_now + month_now + year_now + hour_now + minutes_now + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 
                seur.CodigoClienteComFic = CODIGO_CLIENTE.PadLeft(6, '0'); // Valor proporcionado por SIL 
                seur.ResponsableTransmisionComFic = csGlobal.conexionDB.PadRight(40); // Responsable del fichero
                seur.FechaHoraTransmisionComFic = (day_now + "/" + month_now + "/" + year_now + " " + hour_now + ":" + minutes_now).PadRight(16); // DD/MM/AAAA HH:MM 

                // Escribimos en la cadena
                cadena += seur.InicioCabeceraFicheroComFic;
                cadena += seur.NombreFicheroComFic;
                cadena += seur.CodigoClienteComFic;
                cadena += seur.ResponsableTransmisionComFic;
                cadena += seur.FechaHoraTransmisionComFic;

                // Comienzo de cabecera de registro
                seur.InicioCabeceraRegistroComReg = "\nCF";
                seur.NombreFicheroComReg = ("sip" + day_now + month_now + year_now + hour_now + minutes_now + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 
                seur.NumeroRegistrosComReg = (headers.Rows.Count).ToString().PadLeft(7, '0'); // Número de registros de datos 

                // Escribimos en la cadena
                cadena += seur.InicioCabeceraRegistroComReg;
                cadena += seur.NombreFicheroComReg;
                cadena += seur.NumeroRegistrosComReg;

                #region Cabecera

                foreach (DataRow header in headers.Rows)
                {
                    lista.Add(Convert.ToDouble(header["IDFACV"].ToString()).ToString());

                    string fecha = header["FECHA"].ToString();
                    year = Convert.ToDateTime(fecha).Year.ToString();
                    month = Convert.ToDateTime(fecha).ToString("MM");
                    day = Convert.ToDateTime(fecha).ToString("dd");
                    hour = Convert.ToDateTime(fecha).Hour.ToString();
                    minutes = Convert.ToDateTime(fecha).Minute.ToString();
                    string cabecera = Convert.ToDouble(header["IDFACV"].ToString()).ToString();
                    string nombre_destinatario = "", apellidos = "";

                    string lcStart = header["NOMENT"].ToString();
                    string lcRest = "";
                    int lnSpace = lcStart.IndexOf(' ');
                    if (lnSpace > -1)
                    {
                        string lcFirst = lcStart.Substring(0, lnSpace);
                        lcRest = lcStart.Substring(lnSpace + 1);
                    }

                    string contacto = header["CONTACTO"].ToString();
                    string telefono = header["TELEFONO"].ToString();
                    string codpais = header["CODPAIS"].ToString();
                    string poblacion = header["POBLACION"].ToString().ToUpper();
                    string provincia = header["PROVINCIA"].ToString().ToUpper();
                    string zipcode = header["ZIPCODE"].ToString();
                    string dirent = header["DIRENT"].ToString().ToUpper();

                    if (header["NOMENT"].ToString().Contains(' '))
                    {
                        nombre_destinatario = header["NOMENT"].ToString().Split(' ').First();
                        apellidos = header["NOMENT"].ToString().Split(' ')[1];
                    }
                    else
                    {
                        nombre_destinatario = header["NOMENT"].ToString();
                        apellidos = nombre_destinatario;
                    }

                    if (lcRest.Trim() == "")
                    {
                        lcRest = nombre_destinatario;
                    }

                    #region Pedidos
                    //////////////////////////
                    // PEDIDOS DE SALIDA (5.5)
                    //////////////////////////

                    seur.RegistroDatosPedido = "\nRD";
                    seur.TipoOperacionPedido = "A"; // A,B,M (Alta, Baja, Modificación) || ** CONFIRMAR **
                    seur.NumeroPedidoPedido = Convert.ToDouble(cabecera).ToString().PadRight(15); // Número  de  pedido  del  cliente.  Debe  ser  único  e irrepetible 
                    seur.CodigoClientePedido = CODIGO_CLIENTE.PadRight(3); //Código  de  cliente  en  el  sistema  Informático  de  Seur. Valor Fijo.   
                    seur.CodigoDivisionPedido = "000001".PadRight(6);
                    seur.CodigoAccionPedido = "000001".PadRight(6);
                    seur.CodigoAlmacenPedido = "94".PadRight(3);
                    seur.TipoPedido = "PN".PadRight(2); // PN (Pedidos normales) CD (Cross Docking) 
                    seur.TipoUrgenciaPedido = "01";
                    seur.FechaPedido = (day_now + "/" + month_now + "/" + year_now + " " + hour_now + ":" + minutes_now + ":" + seconds_now).PadRight(19);  // Fecha de envío del pedido: DD/MM/YYYY HH:MI:SS

                    cadena += seur.RegistroDatosPedido;
                    cadena += seur.TipoOperacionPedido;
                    cadena += seur.NumeroPedidoPedido;
                    cadena += seur.CodigoClientePedido;
                    cadena += seur.CodigoDivisionPedido;
                    cadena += seur.CodigoAccionPedido;
                    cadena += seur.CodigoAlmacenPedido;
                    cadena += seur.TipoPedido;
                    cadena += seur.TipoUrgenciaPedido;
                    cadena += seur.FechaPedido;

                    seur.CodigoFARSPedido = "".PadRight(2);
                    seur.SolicitanteNombrePedido = "".PadRight(30);
                    seur.SolicitanteApellido1Pedido = "".PadRight(30);
                    seur.SolicitanteApellido2Pedido = "".PadRight(30);
                    seur.CodigoCargoPedido = "".PadRight(2);
                    seur.DepartamentoPedido = "".PadRight(40);
                    seur.DestinatarioCodigoPedido = "".PadRight(15);
                    seur.DestinatarioRazonSocialPedido = "".PadRight(40);
                    seur.DestinatarioNombrePedido = resolverPadRight(nombre_destinatario, 30);
                    seur.DestinatarioApellido1Pedido = resolverPadRight(lcRest, 30);
                    seur.DestinatarioApellido2Pedido = "".PadRight(30);
                    seur.DestinatarioNIFPedido = "".PadRight(15);
                    seur.CodigoCategoriaPedido = "".PadRight(2);
                    seur.NombreCategoriaPedido = "".PadRight(40);
                    seur.CodigoTipoViaPedido = "".PadRight(2);
                    seur.NombreViaPedido = resolverPadRight(dirent, 40);
                    seur.NumeroPedido = "".PadRight(6);
                    seur.EscaleraPedido = "".PadRight(2);
                    seur.PisoPedido = "".PadRight(3);
                    seur.PuertaPedido = "".PadRight(2);
                    seur.DireccionAbreviadaPedido = resolverPadRight(dirent, 24);
                    seur.PrefijoTelefonoPedido = "".PadRight(5);
                    seur.TelefonoPedido = resolverPadRight(telefono, 15);
                    seur.PrefijoFaxPedido = "".PadRight(5);
                    seur.FaxPedido = "".PadRight(15);
                    seur.CodigoPaisPedido = codpais.PadRight(3);
                    seur.CodigoPostalPedido = zipcode.Replace(" ", "").PadRight(7);
                    seur.PoblacionPedido = resolverPadRight(poblacion, 40);
                    seur.ProvinciaPedido = resolverPadRight(provincia, 40);
                    seur.EmpresaTransportePedido = "908".PadRight(3);
                    seur.ServicioPedido = "1".PadRight(5);
                    seur.TipoReembolsoPedido = "".PadRight(1);
                    seur.ImporteReembolsoPedido = "".PadRight(11, '0');
                    seur.TipoPODPedido = "".PadRight(1);
                    seur.TipoSeguroPedido = "".PadRight(1);
                    seur.ImporteAseguradoPedido = "".PadRight(11, '0');
                    seur.TipoPortePedido = "P".PadRight(1);
                    seur.ObservacionesPedido = "".PadRight(60);
                    seur.GastosEnvioPedido = "".PadRight(11, '0');
                    seur.BultosPedido = "".PadRight(3, '0');
                    seur.KilosPedido = "".PadRight(8, '0');
                    seur.VolumenPedido = "".PadRight(8, '0');
                    seur.PedidoDestinatarioPedido = "".PadRight(20);
                    seur.TipoManipulacionPedido = "N".PadRight(2);
                    seur.NombreDestinatarioFinalPedido = resolverPadRight(contacto.ToUpper(), 40);
                    seur.DireccionDestinatarioFinalPedido = "".PadRight(40);
                    seur.PoblacionDestinatarioFinalPedido = "".PadRight(40);
                    seur.CodigoPostalDestinatarioFinalPedido = "".PadRight(7);
                    seur.PaisDestinatarioFinalPedido = "".PadRight(3);
                    seur.CodigoEmpresaPedido = "".PadRight(2);
                    seur.CodigoCentroPedido = "".PadRight(4, '0');
                    seur.CodigoDepartamentoPedido = "".PadRight(3, '0');
                    seur.CodigoAlbaranPedido = "".PadRight(7, '0');
                    seur.NAD_IV_Pedido = "".PadRight(17);
                    seur.NAD_DP_Pedido = "".PadRight(17);
                    seur.NAD_BY_Pedido = "".PadRight(17);
                    seur.NAD_SU_Pedido = "".PadRight(17);
                    seur.NAD_MS_Pedido = "".PadRight(17);
                    seur.NAD_MR_Pedido = "".PadRight(17);
                    seur.IndicadorSeurCambioPedido = "".PadRight(1);
                    seur.EmailPedido = "".PadRight(100);
                    seur.ObservacionesAlmacenPedido = "".PadRight(256);

                    cadena += seur.CodigoFARSPedido;
                    cadena += seur.SolicitanteNombrePedido;
                    cadena += seur.SolicitanteApellido1Pedido;
                    cadena += seur.SolicitanteApellido2Pedido;
                    cadena += seur.CodigoCargoPedido;
                    cadena += seur.DepartamentoPedido;
                    cadena += seur.DestinatarioCodigoPedido;
                    cadena += seur.DestinatarioRazonSocialPedido;
                    cadena += seur.DestinatarioNombrePedido;
                    cadena += seur.DestinatarioApellido1Pedido;
                    cadena += seur.DestinatarioApellido2Pedido;
                    cadena += seur.DestinatarioNIFPedido;
                    cadena += seur.CodigoCategoriaPedido;
                    cadena += seur.NombreCategoriaPedido;
                    cadena += seur.CodigoTipoViaPedido;
                    cadena += seur.NombreViaPedido;
                    cadena += seur.NumeroPedido;
                    cadena += seur.EscaleraPedido;
                    cadena += seur.PisoPedido;
                    cadena += seur.PuertaPedido;
                    cadena += seur.DireccionAbreviadaPedido;
                    cadena += seur.PrefijoTelefonoPedido;
                    cadena += seur.TelefonoPedido;
                    cadena += seur.PrefijoFaxPedido;
                    cadena += seur.FaxPedido;
                    cadena += seur.CodigoPaisPedido;
                    cadena += seur.CodigoPostalPedido;
                    cadena += seur.PoblacionPedido;
                    cadena += seur.ProvinciaPedido;
                    cadena += seur.EmpresaTransportePedido;
                    cadena += seur.ServicioPedido;
                    cadena += seur.TipoReembolsoPedido;
                    cadena += seur.ImporteReembolsoPedido;
                    cadena += seur.TipoPODPedido;
                    cadena += seur.TipoSeguroPedido;
                    cadena += seur.ImporteAseguradoPedido;
                    cadena += seur.TipoPortePedido;
                    cadena += seur.ObservacionesPedido;
                    cadena += seur.GastosEnvioPedido;
                    cadena += seur.BultosPedido;
                    cadena += seur.KilosPedido;
                    cadena += seur.VolumenPedido;
                    cadena += seur.PedidoDestinatarioPedido;
                    cadena += seur.TipoManipulacionPedido;
                    cadena += seur.NombreDestinatarioFinalPedido;
                    cadena += seur.DireccionDestinatarioFinalPedido;
                    cadena += seur.PoblacionDestinatarioFinalPedido;
                    cadena += seur.CodigoPostalDestinatarioFinalPedido;
                    cadena += seur.PaisDestinatarioFinalPedido;
                    cadena += seur.CodigoEmpresaPedido;
                    cadena += seur.CodigoCentroPedido;
                    cadena += seur.CodigoDepartamentoPedido;
                    cadena += seur.CodigoAlbaranPedido;
                    cadena += seur.NAD_IV_Pedido;
                    cadena += seur.NAD_DP_Pedido;
                    cadena += seur.NAD_BY_Pedido;
                    cadena += seur.NAD_SU_Pedido;
                    cadena += seur.NAD_MS_Pedido;
                    cadena += seur.NAD_MR_Pedido;
                    cadena += seur.IndicadorSeurCambioPedido;
                    cadena += seur.EmailPedido;
                    cadena += seur.ObservacionesAlmacenPedido;
                    #endregion
                }

                #endregion

                // Fin de cabecera de registro
                seur.FinCabeceraRegistroFinReg = "\nFF";
                seur.NombreFicheroFinReg = ("sip" + day + month + year + hour + minutes + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 

                // Escribimos en la cadena
                cadena += seur.FinCabeceraRegistroFinReg;
                cadena += seur.NombreFicheroFinReg;

                // Comienzo de cabecera de registro
                seur.InicioCabeceraRegistroComReg = "\nCF";
                seur.NombreFicheroComReg = ("sip" + day + month + year + hour + minutes + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 
                seur.NumeroRegistrosComReg = (lines.Rows.Count).ToString().PadLeft(7, '0'); // Número de registros de datos 

                // Escribimos en la cadena
                cadena += seur.InicioCabeceraRegistroComReg;
                cadena += seur.NombreFicheroComReg;
                cadena += seur.NumeroRegistrosComReg;

                #region Lineas
                foreach (DataRow header in headers.Rows)
                {
                    //////////////////////// 
                    ////////////////////////
                    // Recorremos las lineas
                    string linea = "";
                    int contador = 1;
                    foreach (DataRow line in lines.Rows)
                    {
                        string idfacv_line = line["IDFACV"].ToString();
                        string idfacv_header = header["IDFACV"].ToString();
                        if (idfacv_line == idfacv_header)
                        {
                            year = Convert.ToDateTime(line["FECHA"].ToString()).Year.ToString();
                            month = Convert.ToDateTime(line["FECHA"].ToString()).ToString("MM");
                            day = Convert.ToDateTime(line["FECHA"].ToString()).ToString("dd");
                            hour = Convert.ToDateTime(line["FECHA"].ToString()).Hour.ToString();
                            minutes = Convert.ToDateTime(line["FECHA"].ToString()).Minute.ToString();

                            // Lineas 
                            seur.RegistroDatosLinea = "\nRD";
                            seur.NumeroPedidoLinea = Convert.ToDouble(idfacv_line).ToString().PadRight(15); // Número de pedido del cliente. Debe ser único e irrepetible 
                            seur.CodigoClienteLinea = CODIGO_CLIENTE.PadRight(3); // Código de cliente en el sistema Informático de Seur. Valor Fijo. (Este dato lo proporciona Seur). 
                            seur.CodigoDivisionPedido = "000001".PadRight(6);
                            seur.CodigoAccionPedido = "000001".PadRight(6);
                            seur.CodigoAlmacenLinea = "94".PadRight(3); // Identifica el Almacén donde se encuentra la mercancía del Cliente. Valor a proporcionar por  SEUR - 01. Getafe,  46. Valencia, 94. Barcelona
                            seur.NumeroLinea = contador.ToString().PadLeft(3, '0'); // Secuenciador de líneas de artículos. 
                            seur.CodigoArticuloLinea = line["CODART"].ToString().Trim().PadRight(35); // Código del artículo del  cliente. No tiene que existir para otro artículo del cliente
                            seur.CodigoArticuloV1Linea = "0".PadRight(3); // Clasificaciones dentro del mismo artículo. Crea otro artículo nuevo. Ej. Versión: 0
                            seur.CodigoArticuloV2Linea = "0".PadRight(2); // Clasificaciones dentro del mismo artículo. Crea otro artículo nuevo. Ej. Talla: 0
                            seur.CodigoArticuloVL = "0".PadRight(1); // Clasificaciones dentro del mismo artículo. Crea otro artículo nuevo. Ej. Color: 0
                            seur.Lote = "".PadRight(20);
                            seur.NumeroSerie = "".PadRight(20);
                            seur.CantidadPedidaLinea = line["UNIDADES"].ToString().PadRight(6); // Cantidad*
                            seur.CantidadLote = "0".PadRight(8); // Precio*
                            seur.CantidadServidaLinea = "".PadRight(256);

                            // Escribimos en la cadena
                            linea = seur.RegistroDatosLinea;
                            linea += seur.NumeroPedidoLinea;
                            linea += seur.CodigoClienteLinea;
                            linea += seur.CodigoDivisionPedido;
                            linea += seur.CodigoAccionPedido;
                            linea += seur.CodigoAlmacenLinea;
                            linea += seur.NumeroLinea;
                            linea += seur.CodigoArticuloLinea;
                            linea += seur.CodigoArticuloV1Linea;
                            linea += seur.CodigoArticuloV2Linea;
                            linea += seur.CodigoArticuloVL;
                            linea += seur.Lote;
                            linea += seur.NumeroSerie;
                            linea += seur.CantidadPedidaLinea;
                            linea += seur.CantidadLote;
                            linea += seur.CantidadServidaLinea;
                            linea += seur.CantidadServidaLinea;

                            cadena += linea;
                            contador++;
                        }
                    }
                    ////////////////////////
                    //    FIN LINEAS      //    
                    ////////////////////////
                }
                #endregion

                // Fin de cabecera de registro
                seur.FinCabeceraRegistroFinReg = "\nFF";
                seur.NombreFicheroFinReg = ("sip" + day_now + month_now + year_now + hour_now + minutes_now + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 

                // Escribimos en la cadena
                cadena += seur.FinCabeceraRegistroFinReg;
                cadena += seur.NombreFicheroFinReg;

                // Fin de cabecera de fichero
                seur.FinCabeceraFicheroFinFic = "\nCC";
                seur.NombreFicheroFinFic = ("sip" + day + month + year + hour + minutes + ".dat").PadRight(19); // XxxDDMMAAAAHHMI.xxx 

                // Escribimos en la cadena
                cadena += seur.FinCabeceraFicheroFinFic;
                cadena += seur.NombreFicheroFinFic;

                try
                {
                    string[] lineas = File.ReadAllLines("SEUR.txt");
                    string usuario = lineas[0];
                    string password = lineas[1];
                    string host = lineas[2];
                    string path = lineas[3];
                    string nombre = "sip" + day_now + month_now + year_now + hour_now + minutes_now + ".261";
                    using (SaveFileDialog savefile = new SaveFileDialog())
                    {
                        savefile.Filter = "Data Files (*.dat)|*.dat";
                        savefile.DefaultExt = "dat";
                        savefile.AddExtension = false;
                        savefile.FileName = nombre;
                        savefile.Filter = "Files (*.*)|*.|All files (*.*)|*.*";

                        Directory.CreateDirectory(@path);
                        File.WriteAllText(@path + savefile.FileName, cadena, Encoding.Default);
                    }

                    if (MessageBox.Show("¿Quieres enviar el fichero a Seur?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Upload(usuario, password, host, @path + nombre, nombre);

                        for (int i = 0; i < lista.Count; i++)
                        {
                            csUtilidades.ejecutarConsulta("update cabefacv set param9 = 'T' where idfacv = '" + lista[i] + "'", false);
                        }

                        cargarDocumentos();
                    }
                    else
                    {
                        lista.Clear();
                        MessageBox.Show("El fichero no se ha enviado, pero se ha guardado en la carpeta " + @path + "\n\nPara enviarlo, genéralo de nuevo");
                        csUtilidades.openFolder(@path);
                    }
                }
                catch (Exception ex) { }
            }
            else
            {
                MessageBox.Show("Selecciona una linea como mínimo para exportar");
            }
        }

        private void Upload(string username, string password, string hostFTP, string pathOfTheFile, string file)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(username, password);
                    client.UploadFile(hostFTP + "/in/" + file, "STOR", pathOfTheFile);

                    MessageBox.Show("El fichero se ha enviado correctamente");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error 500. El fichero no se ha podido enviar a Seur");
            }
        }

        private string resolverPadRight(string word, int size, bool padright = true)
        {
            if (word.Length > size)
                return word.Substring(0, size);
            else
                return ((padright) ? word.PadRight(size) : word.PadLeft(size));
        }

        private void dgvSeur_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            changeColor(dgvSeur);
        }

        private void dgvSeur_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvSeur.SelectedRows.Count > 0)
            {
                numFilas.Text = dgvSeur.SelectedRows.Count.ToString() + " filas seleccionadas";
            }
            else 
            {
                numFilas.Text = dgvSeur.Rows.Count.ToString() + " resultados";
            }
        }
    }
}
