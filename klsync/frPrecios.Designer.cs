﻿namespace klsync
{
    partial class frPrecios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frPrecios));
            this.dgvPreciosA3 = new System.Windows.Forms.DataGridView();
            this.contextTraspasar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aCCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.traspasarPreciosEspecialesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traspasarPreciosConReducciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.fILTROSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.filtrarPorClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssRows = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssInfoPrecios = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelProgreso = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnCargarPreciosFicha = new System.Windows.Forms.ToolStripButton();
            this.btnActualizarPreciosFicha = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.comboTarifas = new System.Windows.Forms.ToolStripComboBox();
            this.brnActualizarTarifas = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btnTarifasClientes = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCargarPreciosEspeciales = new System.Windows.Forms.ToolStripButton();
            this.btnActualizarPreciosEspeciales = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPreciosEspecialesConReduccion = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.btnActualizarDescuentos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.btnActualizarFamiliasCliArt = new System.Windows.Forms.ToolStripButton();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLoadSpecialPricesReduction = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoadPrices = new System.Windows.Forms.Button();
            this.btnUpdateCustomerRates = new System.Windows.Forms.Button();
            this.btnLoadSpecialPrices = new System.Windows.Forms.Button();
            this.btnUpdateSpecialPrices = new System.Windows.Forms.Button();
            this.btnUpdateRates = new System.Windows.Forms.Button();
            this.comboRates = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnUpdateDiscounts = new System.Windows.Forms.Button();
            this.btnPrioridades = new System.Windows.Forms.Button();
            this.btnPreciosMiro = new System.Windows.Forms.Button();
            this.btnPreciosBastide = new System.Windows.Forms.Button();
            this.btnUpdatePrices = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPreciosA3)).BeginInit();
            this.contextTraspasar.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPreciosA3
            // 
            this.dgvPreciosA3.AllowUserToAddRows = false;
            this.dgvPreciosA3.AllowUserToDeleteRows = false;
            this.dgvPreciosA3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPreciosA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPreciosA3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPreciosA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPreciosA3.ContextMenuStrip = this.contextTraspasar;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPreciosA3.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPreciosA3.Location = new System.Drawing.Point(0, 3);
            this.dgvPreciosA3.Name = "dgvPreciosA3";
            this.dgvPreciosA3.ReadOnly = true;
            this.dgvPreciosA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPreciosA3.Size = new System.Drawing.Size(1093, 446);
            this.dgvPreciosA3.TabIndex = 4;
            // 
            // contextTraspasar
            // 
            this.contextTraspasar.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextTraspasar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCCIONESToolStripMenuItem,
            this.toolStripMenuItem3,
            this.traspasarPreciosEspecialesToolStripMenuItem,
            this.traspasarPreciosConReducciónToolStripMenuItem,
            this.toolStripMenuItem1,
            this.fILTROSToolStripMenuItem,
            this.toolStripMenuItem2,
            this.filtrarPorClienteToolStripMenuItem,
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem});
            this.contextTraspasar.Name = "contextTraspasar";
            this.contextTraspasar.Size = new System.Drawing.Size(383, 178);
            // 
            // aCCIONESToolStripMenuItem
            // 
            this.aCCIONESToolStripMenuItem.Enabled = false;
            this.aCCIONESToolStripMenuItem.Name = "aCCIONESToolStripMenuItem";
            this.aCCIONESToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.aCCIONESToolStripMenuItem.Text = "ACCIONES";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(379, 6);
            // 
            // traspasarPreciosEspecialesToolStripMenuItem
            // 
            this.traspasarPreciosEspecialesToolStripMenuItem.Name = "traspasarPreciosEspecialesToolStripMenuItem";
            this.traspasarPreciosEspecialesToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.traspasarPreciosEspecialesToolStripMenuItem.Text = "Traspasar Precios Especiales";
            this.traspasarPreciosEspecialesToolStripMenuItem.Click += new System.EventHandler(this.traspasarPreciosEspecialesToolStripMenuItem_Click);
            // 
            // traspasarPreciosConReducciónToolStripMenuItem
            // 
            this.traspasarPreciosConReducciónToolStripMenuItem.Name = "traspasarPreciosConReducciónToolStripMenuItem";
            this.traspasarPreciosConReducciónToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.traspasarPreciosConReducciónToolStripMenuItem.Text = "Traspasar Precios con Reducción";
            this.traspasarPreciosConReducciónToolStripMenuItem.Click += new System.EventHandler(this.traspasarPreciosConReducciónToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(379, 6);
            // 
            // fILTROSToolStripMenuItem
            // 
            this.fILTROSToolStripMenuItem.Enabled = false;
            this.fILTROSToolStripMenuItem.Name = "fILTROSToolStripMenuItem";
            this.fILTROSToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.fILTROSToolStripMenuItem.Text = "PRECIOS ESPECIALES";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(379, 6);
            // 
            // filtrarPorClienteToolStripMenuItem
            // 
            this.filtrarPorClienteToolStripMenuItem.Name = "filtrarPorClienteToolStripMenuItem";
            this.filtrarPorClienteToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.filtrarPorClienteToolStripMenuItem.Text = "Filtrar por cliente";
            this.filtrarPorClienteToolStripMenuItem.Click += new System.EventHandler(this.filtrarPorClienteToolStripMenuItem_Click);
            // 
            // actualizarFamDescuentosAClientesEnPSToolStripMenuItem
            // 
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem.Name = "actualizarFamDescuentosAClientesEnPSToolStripMenuItem";
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem.Text = "Actualizar Fam. Descuentos a Clientes en PS";
            this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem.Click += new System.EventHandler(this.actualizarFamDescuentosAClientesEnPSToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.statusStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssRows,
            this.toolStripStatusLabel1,
            this.tssInfoPrecios,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabelProgreso});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1096, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssRows
            // 
            this.tssRows.Name = "tssRows";
            this.tssRows.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // tssInfoPrecios
            // 
            this.tssInfoPrecios.Name = "tssInfoPrecios";
            this.tssInfoPrecios.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1027, 17);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // toolStripStatusLabelProgreso
            // 
            this.toolStripStatusLabelProgreso.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripStatusLabelProgreso.Name = "toolStripStatusLabelProgreso";
            this.toolStripStatusLabelProgreso.Size = new System.Drawing.Size(54, 17);
            this.toolStripStatusLabelProgreso.Text = "Progreso";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCargarPreciosFicha,
            this.btnActualizarPreciosFicha,
            this.toolStripSeparator18,
            this.toolStripSeparator3,
            this.comboTarifas,
            this.brnActualizarTarifas,
            this.toolStripSeparator19,
            this.toolStripSeparator8,
            this.btnTarifasClientes,
            this.toolStripSeparator17,
            this.toolStripSeparator14,
            this.btnCargarPreciosEspeciales,
            this.btnActualizarPreciosEspeciales,
            this.toolStripSeparator15,
            this.btnPreciosEspecialesConReduccion,
            this.toolStripSeparator20,
            this.toolStripSeparator16,
            this.btnActualizarDescuentos,
            this.toolStripSeparator22,
            this.toolStripSeparator21,
            this.btnActualizarFamiliasCliArt});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(10);
            this.toolStrip2.Size = new System.Drawing.Size(1096, 59);
            this.toolStrip2.TabIndex = 10;
            this.toolStrip2.Text = "toolStrip2";
            this.toolStrip2.Visible = false;
            // 
            // btnCargarPreciosFicha
            // 
            this.btnCargarPreciosFicha.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCargarPreciosFicha.Image = ((System.Drawing.Image)(resources.GetObject("btnCargarPreciosFicha.Image")));
            this.btnCargarPreciosFicha.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnCargarPreciosFicha.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCargarPreciosFicha.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnCargarPreciosFicha.Name = "btnCargarPreciosFicha";
            this.btnCargarPreciosFicha.Size = new System.Drawing.Size(36, 36);
            this.btnCargarPreciosFicha.Text = "toolStripButton10";
            this.btnCargarPreciosFicha.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCargarPreciosFicha.ToolTipText = "Cargar Precios Ficha";
            this.btnCargarPreciosFicha.Click += new System.EventHandler(this.btnCargarPreciosFicha_Click);
            // 
            // btnActualizarPreciosFicha
            // 
            this.btnActualizarPreciosFicha.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnActualizarPreciosFicha.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarPreciosFicha.Image")));
            this.btnActualizarPreciosFicha.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnActualizarPreciosFicha.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActualizarPreciosFicha.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnActualizarPreciosFicha.Name = "btnActualizarPreciosFicha";
            this.btnActualizarPreciosFicha.Size = new System.Drawing.Size(36, 36);
            this.btnActualizarPreciosFicha.Text = "toolStripButton10";
            this.btnActualizarPreciosFicha.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnActualizarPreciosFicha.ToolTipText = "Actualizar Precios Ficha";
            this.btnActualizarPreciosFicha.Click += new System.EventHandler(this.btnActualizarPreciosFicha_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // comboTarifas
            // 
            this.comboTarifas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTarifas.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.comboTarifas.Items.AddRange(new object[] {
            ""});
            this.comboTarifas.Margin = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.comboTarifas.Name = "comboTarifas";
            this.comboTarifas.Size = new System.Drawing.Size(121, 39);
            this.comboTarifas.SelectedIndexChanged += new System.EventHandler(this.comboTarifas_SelectedIndexChanged);
            // 
            // brnActualizarTarifas
            // 
            this.brnActualizarTarifas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.brnActualizarTarifas.Image = ((System.Drawing.Image)(resources.GetObject("brnActualizarTarifas.Image")));
            this.brnActualizarTarifas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.brnActualizarTarifas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.brnActualizarTarifas.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.brnActualizarTarifas.Name = "brnActualizarTarifas";
            this.brnActualizarTarifas.Size = new System.Drawing.Size(36, 36);
            this.brnActualizarTarifas.Text = "toolStripButton10";
            this.brnActualizarTarifas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.brnActualizarTarifas.ToolTipText = "Actualizar Tarifas";
            this.brnActualizarTarifas.Click += new System.EventHandler(this.brnActualizarTarifas_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
            // 
            // btnTarifasClientes
            // 
            this.btnTarifasClientes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnTarifasClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnTarifasClientes.Image")));
            this.btnTarifasClientes.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnTarifasClientes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTarifasClientes.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnTarifasClientes.Name = "btnTarifasClientes";
            this.btnTarifasClientes.Size = new System.Drawing.Size(36, 36);
            this.btnTarifasClientes.Text = "toolStripButton10";
            this.btnTarifasClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTarifasClientes.ToolTipText = "Actualizar Tarifas Clientes";
            this.btnTarifasClientes.Click += new System.EventHandler(this.btnTarifasClientes_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 39);
            // 
            // btnCargarPreciosEspeciales
            // 
            this.btnCargarPreciosEspeciales.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCargarPreciosEspeciales.Image = ((System.Drawing.Image)(resources.GetObject("btnCargarPreciosEspeciales.Image")));
            this.btnCargarPreciosEspeciales.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnCargarPreciosEspeciales.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCargarPreciosEspeciales.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnCargarPreciosEspeciales.Name = "btnCargarPreciosEspeciales";
            this.btnCargarPreciosEspeciales.Size = new System.Drawing.Size(36, 36);
            this.btnCargarPreciosEspeciales.Text = "toolStripButton10";
            this.btnCargarPreciosEspeciales.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCargarPreciosEspeciales.ToolTipText = "Cargar Precios Especiales";
            this.btnCargarPreciosEspeciales.Click += new System.EventHandler(this.btnCargarPreciosEspeciales_Click);
            // 
            // btnActualizarPreciosEspeciales
            // 
            this.btnActualizarPreciosEspeciales.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnActualizarPreciosEspeciales.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarPreciosEspeciales.Image")));
            this.btnActualizarPreciosEspeciales.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnActualizarPreciosEspeciales.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActualizarPreciosEspeciales.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnActualizarPreciosEspeciales.Name = "btnActualizarPreciosEspeciales";
            this.btnActualizarPreciosEspeciales.Size = new System.Drawing.Size(36, 36);
            this.btnActualizarPreciosEspeciales.Text = "toolStripButton10";
            this.btnActualizarPreciosEspeciales.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnActualizarPreciosEspeciales.ToolTipText = "Actualizar Precios Especiales";
            this.btnActualizarPreciosEspeciales.Click += new System.EventHandler(this.btnActualizarPreciosEspeciales_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 39);
            // 
            // btnPreciosEspecialesConReduccion
            // 
            this.btnPreciosEspecialesConReduccion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPreciosEspecialesConReduccion.Image = ((System.Drawing.Image)(resources.GetObject("btnPreciosEspecialesConReduccion.Image")));
            this.btnPreciosEspecialesConReduccion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPreciosEspecialesConReduccion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPreciosEspecialesConReduccion.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnPreciosEspecialesConReduccion.Name = "btnPreciosEspecialesConReduccion";
            this.btnPreciosEspecialesConReduccion.Size = new System.Drawing.Size(36, 36);
            this.btnPreciosEspecialesConReduccion.Text = "toolStripButton10";
            this.btnPreciosEspecialesConReduccion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPreciosEspecialesConReduccion.ToolTipText = "Cargar Precios Especiales con Reducción";
            this.btnPreciosEspecialesConReduccion.Click += new System.EventHandler(this.btnPreciosEspecialesConReduccion_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 39);
            // 
            // btnActualizarDescuentos
            // 
            this.btnActualizarDescuentos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnActualizarDescuentos.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarDescuentos.Image")));
            this.btnActualizarDescuentos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnActualizarDescuentos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActualizarDescuentos.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnActualizarDescuentos.Name = "btnActualizarDescuentos";
            this.btnActualizarDescuentos.Size = new System.Drawing.Size(36, 36);
            this.btnActualizarDescuentos.Text = "toolStripButton10";
            this.btnActualizarDescuentos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnActualizarDescuentos.ToolTipText = "Actualizar Descuentos";
            this.btnActualizarDescuentos.Click += new System.EventHandler(this.btnActualizarDescuentos_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(6, 39);
            // 
            // btnActualizarFamiliasCliArt
            // 
            this.btnActualizarFamiliasCliArt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnActualizarFamiliasCliArt.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizarFamiliasCliArt.Image")));
            this.btnActualizarFamiliasCliArt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnActualizarFamiliasCliArt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActualizarFamiliasCliArt.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.btnActualizarFamiliasCliArt.Name = "btnActualizarFamiliasCliArt";
            this.btnActualizarFamiliasCliArt.Size = new System.Drawing.Size(36, 36);
            this.btnActualizarFamiliasCliArt.Text = "toolStripButton10";
            this.btnActualizarFamiliasCliArt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnActualizarFamiliasCliArt.ToolTipText = "Actualizar Familias Cliente / Artículo";
            this.btnActualizarFamiliasCliArt.Click += new System.EventHandler(this.btnActualizarFamiliasCliArt_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 3);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.dgvPreciosA3);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer.Size = new System.Drawing.Size(1096, 629);
            this.splitContainer.SplitterDistance = 452;
            this.splitContainer.TabIndex = 11;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Controls.Add(this.btnLoadSpecialPricesReduction, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnLoadPrices, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdateCustomerRates, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnLoadSpecialPrices, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdateSpecialPrices, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdateRates, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboRates, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdateDiscounts, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnPrioridades, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPreciosMiro, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnPreciosBastide, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnUpdatePrices, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button2, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1090, 167);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnLoadSpecialPricesReduction
            // 
            this.btnLoadSpecialPricesReduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLoadSpecialPricesReduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadSpecialPricesReduction.Location = new System.Drawing.Point(623, 58);
            this.btnLoadSpecialPricesReduction.Name = "btnLoadSpecialPricesReduction";
            this.btnLoadSpecialPricesReduction.Size = new System.Drawing.Size(149, 49);
            this.btnLoadSpecialPricesReduction.TabIndex = 18;
            this.btnLoadSpecialPricesReduction.Text = "Cargar precios especiales reduccion";
            this.btnLoadSpecialPricesReduction.UseVisualStyleBackColor = true;
            this.btnLoadSpecialPricesReduction.Click += new System.EventHandler(this.btnLoadSpecialPricesReduction_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(778, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 55);
            this.label7.TabIndex = 17;
            this.label7.Text = "DESCUENTOS";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(623, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 55);
            this.label5.TabIndex = 4;
            this.label5.Text = "PRECIOS ESP. REDUCCION";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(468, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 55);
            this.label4.TabIndex = 3;
            this.label4.Text = "PRECIOS ESPECIALES";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(313, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 55);
            this.label3.TabIndex = 2;
            this.label3.Text = "TARIFAS CLIENTES";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(158, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 55);
            this.label2.TabIndex = 1;
            this.label2.Text = "TARIFAS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 55);
            this.label1.TabIndex = 0;
            this.label1.Text = "PRECIOS FICHA";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoadPrices
            // 
            this.btnLoadPrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLoadPrices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadPrices.Location = new System.Drawing.Point(3, 58);
            this.btnLoadPrices.Name = "btnLoadPrices";
            this.btnLoadPrices.Size = new System.Drawing.Size(149, 49);
            this.btnLoadPrices.TabIndex = 6;
            this.btnLoadPrices.Text = "Cargar precios ficha";
            this.btnLoadPrices.UseVisualStyleBackColor = true;
            this.btnLoadPrices.Click += new System.EventHandler(this.btnLoadPrices_Click);
            // 
            // btnUpdateCustomerRates
            // 
            this.btnUpdateCustomerRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateCustomerRates.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCustomerRates.Location = new System.Drawing.Point(468, 113);
            this.btnUpdateCustomerRates.Name = "btnUpdateCustomerRates";
            this.btnUpdateCustomerRates.Size = new System.Drawing.Size(149, 51);
            this.btnUpdateCustomerRates.TabIndex = 8;
            this.btnUpdateCustomerRates.Text = "Actualizar precios especiales";
            this.btnUpdateCustomerRates.UseVisualStyleBackColor = true;
            this.btnUpdateCustomerRates.Click += new System.EventHandler(this.btnUpdateCustomerRates_Click);
            // 
            // btnLoadSpecialPrices
            // 
            this.btnLoadSpecialPrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLoadSpecialPrices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadSpecialPrices.Location = new System.Drawing.Point(468, 58);
            this.btnLoadSpecialPrices.Name = "btnLoadSpecialPrices";
            this.btnLoadSpecialPrices.Size = new System.Drawing.Size(149, 49);
            this.btnLoadSpecialPrices.TabIndex = 9;
            this.btnLoadSpecialPrices.Text = "Cargar precios especiales";
            this.btnLoadSpecialPrices.UseVisualStyleBackColor = true;
            this.btnLoadSpecialPrices.Click += new System.EventHandler(this.btnLoadSpecialPrices_Click);
            // 
            // btnUpdateSpecialPrices
            // 
            this.btnUpdateSpecialPrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateSpecialPrices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateSpecialPrices.Location = new System.Drawing.Point(623, 113);
            this.btnUpdateSpecialPrices.Name = "btnUpdateSpecialPrices";
            this.btnUpdateSpecialPrices.Size = new System.Drawing.Size(149, 51);
            this.btnUpdateSpecialPrices.TabIndex = 10;
            this.btnUpdateSpecialPrices.Text = "Actualizar precios especiales reduccion";
            this.btnUpdateSpecialPrices.UseVisualStyleBackColor = true;
            this.btnUpdateSpecialPrices.Click += new System.EventHandler(this.btnUpdateSpecialPrices_Click);
            // 
            // btnUpdateRates
            // 
            this.btnUpdateRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateRates.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateRates.Location = new System.Drawing.Point(313, 113);
            this.btnUpdateRates.Name = "btnUpdateRates";
            this.btnUpdateRates.Size = new System.Drawing.Size(149, 51);
            this.btnUpdateRates.TabIndex = 11;
            this.btnUpdateRates.Text = "Actualizar tarifas clientes";
            this.btnUpdateRates.UseVisualStyleBackColor = true;
            this.btnUpdateRates.Click += new System.EventHandler(this.btnUpdateRates_Click);
            // 
            // comboRates
            // 
            this.comboRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboRates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRates.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboRates.FormattingEnabled = true;
            this.comboRates.Location = new System.Drawing.Point(158, 58);
            this.comboRates.Name = "comboRates";
            this.comboRates.Size = new System.Drawing.Size(149, 28);
            this.comboRates.TabIndex = 15;
            this.comboRates.SelectedIndexChanged += new System.EventHandler(this.comboRates_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(933, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 55);
            this.label6.TabIndex = 16;
            this.label6.Text = "FAMILIA ART. Y CLI.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdateDiscounts
            // 
            this.btnUpdateDiscounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateDiscounts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateDiscounts.Location = new System.Drawing.Point(778, 113);
            this.btnUpdateDiscounts.Name = "btnUpdateDiscounts";
            this.btnUpdateDiscounts.Size = new System.Drawing.Size(149, 51);
            this.btnUpdateDiscounts.TabIndex = 13;
            this.btnUpdateDiscounts.Text = "Actualizar descuentos";
            this.btnUpdateDiscounts.UseVisualStyleBackColor = true;
            this.btnUpdateDiscounts.Click += new System.EventHandler(this.btnUpdateDiscounts_Click);
            // 
            // btnPrioridades
            // 
            this.btnPrioridades.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrioridades.Location = new System.Drawing.Point(933, 58);
            this.btnPrioridades.Name = "btnPrioridades";
            this.btnPrioridades.Size = new System.Drawing.Size(154, 49);
            this.btnPrioridades.TabIndex = 20;
            this.btnPrioridades.Text = "Actualizar prioridades";
            this.btnPrioridades.UseVisualStyleBackColor = true;
            this.btnPrioridades.Click += new System.EventHandler(this.btnPrioridades_Click);
            // 
            // btnPreciosMiro
            // 
            this.btnPreciosMiro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPreciosMiro.Location = new System.Drawing.Point(778, 58);
            this.btnPreciosMiro.Name = "btnPreciosMiro";
            this.btnPreciosMiro.Size = new System.Drawing.Size(149, 49);
            this.btnPreciosMiro.TabIndex = 21;
            this.btnPreciosMiro.Text = "Actualizar Precio Amigo";
            this.btnPreciosMiro.UseVisualStyleBackColor = true;
            this.btnPreciosMiro.Visible = false;
            this.btnPreciosMiro.Click += new System.EventHandler(this.btnPreciosMiro_Click);
            // 
            // btnPreciosBastide
            // 
            this.btnPreciosBastide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreciosBastide.Location = new System.Drawing.Point(313, 58);
            this.btnPreciosBastide.Name = "btnPreciosBastide";
            this.btnPreciosBastide.Size = new System.Drawing.Size(149, 49);
            this.btnPreciosBastide.TabIndex = 22;
            this.btnPreciosBastide.Text = "Precios Impacto Bastide";
            this.btnPreciosBastide.UseVisualStyleBackColor = true;
            this.btnPreciosBastide.Visible = false;
            this.btnPreciosBastide.Click += new System.EventHandler(this.btnPreciosBastide_Click);
            // 
            // btnUpdatePrices
            // 
            this.btnUpdatePrices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdatePrices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdatePrices.Location = new System.Drawing.Point(3, 113);
            this.btnUpdatePrices.Name = "btnUpdatePrices";
            this.btnUpdatePrices.Size = new System.Drawing.Size(149, 51);
            this.btnUpdatePrices.TabIndex = 11;
            this.btnUpdatePrices.Text = "Actualizar precios ficha artículo";
            this.btnUpdatePrices.UseVisualStyleBackColor = true;
            this.btnUpdatePrices.Click += new System.EventHandler(this.btnUpdatePrices_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(158, 113);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(149, 51);
            this.button2.TabIndex = 19;
            this.button2.Text = "Actualizar Precios Tarifa";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frPrecios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1096, 661);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.statusStrip1);
            this.Name = "frPrecios";
            this.Text = "Precios";
            this.Load += new System.EventHandler(this.frPrecios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPreciosA3)).EndInit();
            this.contextTraspasar.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPreciosA3;
        private System.Windows.Forms.ContextMenuStrip contextTraspasar;
        private System.Windows.Forms.ToolStripMenuItem traspasarPreciosEspecialesToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssRows;
        private System.Windows.Forms.ToolStripMenuItem traspasarPreciosConReducciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tssInfoPrecios;
        private System.Windows.Forms.ToolStripMenuItem aCCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fILTROSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProgreso;
        private System.Windows.Forms.ToolStripMenuItem filtrarPorClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnCargarPreciosFicha;
        private System.Windows.Forms.ToolStripButton btnActualizarPreciosFicha;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripComboBox comboTarifas;
        private System.Windows.Forms.ToolStripButton brnActualizarTarifas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton btnTarifasClientes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton btnCargarPreciosEspeciales;
        private System.Windows.Forms.ToolStripButton btnActualizarPreciosEspeciales;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton btnPreciosEspecialesConReduccion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripButton btnActualizarDescuentos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripButton btnActualizarFamiliasCliArt;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoadPrices;
        private System.Windows.Forms.Button btnUpdateCustomerRates;
        private System.Windows.Forms.Button btnLoadSpecialPrices;
        private System.Windows.Forms.Button btnUpdateSpecialPrices;
        private System.Windows.Forms.Button btnUpdateRates;
        private System.Windows.Forms.Button btnUpdatePrices;
        private System.Windows.Forms.Button btnUpdateDiscounts;
        private System.Windows.Forms.ComboBox comboRates;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLoadSpecialPricesReduction;
        private System.Windows.Forms.Button btnPrioridades;
        private System.Windows.Forms.Button btnPreciosMiro;
        private System.Windows.Forms.Button btnPreciosBastide;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem actualizarFamDescuentosAClientesEnPSToolStripMenuItem;
    }
}