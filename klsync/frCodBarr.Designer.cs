﻿namespace klsync
{
    partial class frCodBarr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvArticulos = new System.Windows.Forms.DataGridView();
            this.dgvCodigosBarra = new System.Windows.Forms.DataGridView();
            this.lblArticulos = new System.Windows.Forms.Label();
            this.lbCodigosBarra = new System.Windows.Forms.Label();
            this.btBuscar = new System.Windows.Forms.Button();
            this.btLoadAll = new System.Windows.Forms.Button();
            this.btCreateCodBarr = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.tbPais = new System.Windows.Forms.TextBox();
            this.lblPais = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEmpresa = new System.Windows.Forms.TextBox();
            this.tbBarrcodeGen = new System.Windows.Forms.TextBox();
            this.lbLastBarcode = new System.Windows.Forms.Label();
            this.tbLastBarcode = new System.Windows.Forms.TextBox();
            this.lbTallas = new System.Windows.Forms.Label();
            this.dgvColores = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvTallas = new System.Windows.Forms.DataGridView();
            this.lbContador = new System.Windows.Forms.Label();
            this.tbContador = new System.Windows.Forms.TextBox();
            this.cMenuCodBarr = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarCódigoDeBarrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btCrearCodBarr = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodigosBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTallas)).BeginInit();
            this.cMenuCodBarr.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvArticulos
            // 
            this.dgvArticulos.AllowUserToAddRows = false;
            this.dgvArticulos.AllowUserToDeleteRows = false;
            this.dgvArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvArticulos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulos.Location = new System.Drawing.Point(32, 146);
            this.dgvArticulos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvArticulos.MultiSelect = false;
            this.dgvArticulos.Name = "dgvArticulos";
            this.dgvArticulos.ReadOnly = true;
            this.dgvArticulos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulos.Size = new System.Drawing.Size(803, 659);
            this.dgvArticulos.TabIndex = 0;
            this.dgvArticulos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArticulos_CellClick);
            // 
            // dgvCodigosBarra
            // 
            this.dgvCodigosBarra.AllowUserToAddRows = false;
            this.dgvCodigosBarra.AllowUserToDeleteRows = false;
            this.dgvCodigosBarra.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCodigosBarra.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCodigosBarra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCodigosBarra.ContextMenuStrip = this.cMenuCodBarr;
            this.dgvCodigosBarra.Location = new System.Drawing.Point(867, 602);
            this.dgvCodigosBarra.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvCodigosBarra.Name = "dgvCodigosBarra";
            this.dgvCodigosBarra.ReadOnly = true;
            this.dgvCodigosBarra.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCodigosBarra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCodigosBarra.Size = new System.Drawing.Size(586, 203);
            this.dgvCodigosBarra.TabIndex = 1;
            // 
            // lblArticulos
            // 
            this.lblArticulos.AutoSize = true;
            this.lblArticulos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArticulos.Location = new System.Drawing.Point(49, 118);
            this.lblArticulos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblArticulos.Name = "lblArticulos";
            this.lblArticulos.Size = new System.Drawing.Size(87, 25);
            this.lblArticulos.TabIndex = 2;
            this.lblArticulos.Text = "Articulos";
            // 
            // lbCodigosBarra
            // 
            this.lbCodigosBarra.AutoSize = true;
            this.lbCodigosBarra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodigosBarra.Location = new System.Drawing.Point(1159, 11);
            this.lbCodigosBarra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCodigosBarra.Name = "lbCodigosBarra";
            this.lbCodigosBarra.Size = new System.Drawing.Size(165, 25);
            this.lbCodigosBarra.TabIndex = 3;
            this.lbCodigosBarra.Text = "Códigos Creados";
            // 
            // btBuscar
            // 
            this.btBuscar.Image = global::klsync.Properties.Resources.magnifying_glass;
            this.btBuscar.Location = new System.Drawing.Point(127, 15);
            this.btBuscar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btBuscar.Name = "btBuscar";
            this.btBuscar.Size = new System.Drawing.Size(83, 58);
            this.btBuscar.TabIndex = 4;
            this.btBuscar.UseVisualStyleBackColor = true;
            this.btBuscar.Click += new System.EventHandler(this.btBuscar_Click);
            // 
            // btLoadAll
            // 
            this.btLoadAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadAll.Location = new System.Drawing.Point(36, 15);
            this.btLoadAll.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btLoadAll.Name = "btLoadAll";
            this.btLoadAll.Size = new System.Drawing.Size(83, 58);
            this.btLoadAll.TabIndex = 5;
            this.btLoadAll.Text = "Todos";
            this.btLoadAll.UseVisualStyleBackColor = true;
            this.btLoadAll.Click += new System.EventHandler(this.btLoadAll_Click);
            // 
            // btCreateCodBarr
            // 
            this.btCreateCodBarr.Image = global::klsync.Properties.Resources.product_barcode;
            this.btCreateCodBarr.Location = new System.Drawing.Point(217, 15);
            this.btCreateCodBarr.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btCreateCodBarr.Name = "btCreateCodBarr";
            this.btCreateCodBarr.Size = new System.Drawing.Size(83, 58);
            this.btCreateCodBarr.TabIndex = 6;
            this.btCreateCodBarr.UseVisualStyleBackColor = true;
            this.btCreateCodBarr.Click += new System.EventHandler(this.btCreateCodBarr_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Location = new System.Drawing.Point(867, 574);
            this.lblMensaje.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(64, 25);
            this.lblMensaje.TabIndex = 7;
            this.lblMensaje.Text = "label1";
            // 
            // tbPais
            // 
            this.tbPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPais.Location = new System.Drawing.Point(404, 41);
            this.tbPais.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbPais.Name = "tbPais";
            this.tbPais.Size = new System.Drawing.Size(132, 30);
            this.tbPais.TabIndex = 8;
            this.tbPais.Text = "84";
            this.tbPais.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPais
            // 
            this.lblPais.AutoSize = true;
            this.lblPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPais.Location = new System.Drawing.Point(399, 11);
            this.lblPais.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPais.Name = "lblPais";
            this.lblPais.Size = new System.Drawing.Size(50, 25);
            this.lblPais.TabIndex = 9;
            this.lblPais.Text = "País";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(552, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 25);
            this.label1.TabIndex = 11;
            this.label1.Text = "Empresa";
            // 
            // tbEmpresa
            // 
            this.tbEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEmpresa.Location = new System.Drawing.Point(557, 41);
            this.tbEmpresa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbEmpresa.Name = "tbEmpresa";
            this.tbEmpresa.Size = new System.Drawing.Size(132, 30);
            this.tbEmpresa.TabIndex = 10;
            this.tbEmpresa.Text = "35148";
            this.tbEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbBarrcodeGen
            // 
            this.tbBarrcodeGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBarrcodeGen.Location = new System.Drawing.Point(1161, 39);
            this.tbBarrcodeGen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbBarrcodeGen.Name = "tbBarrcodeGen";
            this.tbBarrcodeGen.Size = new System.Drawing.Size(281, 30);
            this.tbBarrcodeGen.TabIndex = 12;
            // 
            // lbLastBarcode
            // 
            this.lbLastBarcode.AutoSize = true;
            this.lbLastBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastBarcode.Location = new System.Drawing.Point(715, 11);
            this.lbLastBarcode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbLastBarcode.Name = "lbLastBarcode";
            this.lbLastBarcode.Size = new System.Drawing.Size(99, 25);
            this.lbLastBarcode.TabIndex = 14;
            this.lbLastBarcode.Text = "Último CB";
            // 
            // tbLastBarcode
            // 
            this.tbLastBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLastBarcode.Location = new System.Drawing.Point(720, 41);
            this.tbLastBarcode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLastBarcode.Name = "tbLastBarcode";
            this.tbLastBarcode.Size = new System.Drawing.Size(208, 30);
            this.tbLastBarcode.TabIndex = 13;
            this.tbLastBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbTallas
            // 
            this.lbTallas.AutoSize = true;
            this.lbTallas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTallas.Location = new System.Drawing.Point(877, 118);
            this.lbTallas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTallas.Name = "lbTallas";
            this.lbTallas.Size = new System.Drawing.Size(65, 25);
            this.lbTallas.TabIndex = 16;
            this.lbTallas.Text = "Tallas";
            // 
            // dgvColores
            // 
            this.dgvColores.AllowUserToAddRows = false;
            this.dgvColores.AllowUserToDeleteRows = false;
            this.dgvColores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvColores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColores.Location = new System.Drawing.Point(1071, 146);
            this.dgvColores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvColores.Name = "dgvColores";
            this.dgvColores.ReadOnly = true;
            this.dgvColores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvColores.Size = new System.Drawing.Size(199, 425);
            this.dgvColores.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1071, 118);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 25);
            this.label2.TabIndex = 18;
            this.label2.Text = "Colores";
            // 
            // dgvTallas
            // 
            this.dgvTallas.AllowUserToAddRows = false;
            this.dgvTallas.AllowUserToDeleteRows = false;
            this.dgvTallas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTallas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTallas.Location = new System.Drawing.Point(867, 146);
            this.dgvTallas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvTallas.Name = "dgvTallas";
            this.dgvTallas.ReadOnly = true;
            this.dgvTallas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTallas.Size = new System.Drawing.Size(196, 425);
            this.dgvTallas.TabIndex = 17;
            // 
            // lbContador
            // 
            this.lbContador.AutoSize = true;
            this.lbContador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbContador.Location = new System.Drawing.Point(960, 11);
            this.lbContador.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbContador.Name = "lbContador";
            this.lbContador.Size = new System.Drawing.Size(93, 25);
            this.lbContador.TabIndex = 20;
            this.lbContador.Text = "Contador";
            // 
            // tbContador
            // 
            this.tbContador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbContador.Location = new System.Drawing.Point(965, 41);
            this.tbContador.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbContador.Name = "tbContador";
            this.tbContador.Size = new System.Drawing.Size(132, 30);
            this.tbContador.TabIndex = 19;
            this.tbContador.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cMenuCodBarr
            // 
            this.cMenuCodBarr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarCódigoDeBarrasToolStripMenuItem});
            this.cMenuCodBarr.Name = "cMenuCodBarr";
            this.cMenuCodBarr.Size = new System.Drawing.Size(239, 28);
            // 
            // borrarCódigoDeBarrasToolStripMenuItem
            // 
            this.borrarCódigoDeBarrasToolStripMenuItem.Name = "borrarCódigoDeBarrasToolStripMenuItem";
            this.borrarCódigoDeBarrasToolStripMenuItem.Size = new System.Drawing.Size(238, 24);
            this.borrarCódigoDeBarrasToolStripMenuItem.Text = "&Borrar Código de Barras";
            this.borrarCódigoDeBarrasToolStripMenuItem.Click += new System.EventHandler(this.borrarCódigoDeBarrasToolStripMenuItem_Click);
            // 
            // btCrearCodBarr
            // 
            this.btCrearCodBarr.Image = global::klsync.Properties.Resources.product_barcode;
            this.btCrearCodBarr.Location = new System.Drawing.Point(1278, 146);
            this.btCrearCodBarr.Margin = new System.Windows.Forms.Padding(4);
            this.btCrearCodBarr.Name = "btCrearCodBarr";
            this.btCrearCodBarr.Size = new System.Drawing.Size(83, 58);
            this.btCrearCodBarr.TabIndex = 22;
            this.btCrearCodBarr.UseVisualStyleBackColor = true;
            this.btCrearCodBarr.Click += new System.EventHandler(this.btCrearCodBarr_Click);
            // 
            // frCodBarr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1460, 905);
            this.Controls.Add(this.btCrearCodBarr);
            this.Controls.Add(this.lbContador);
            this.Controls.Add(this.tbContador);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvTallas);
            this.Controls.Add(this.lbTallas);
            this.Controls.Add(this.lbLastBarcode);
            this.Controls.Add(this.tbLastBarcode);
            this.Controls.Add(this.tbBarrcodeGen);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbEmpresa);
            this.Controls.Add(this.lblPais);
            this.Controls.Add(this.tbPais);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.btCreateCodBarr);
            this.Controls.Add(this.btLoadAll);
            this.Controls.Add(this.btBuscar);
            this.Controls.Add(this.lbCodigosBarra);
            this.Controls.Add(this.lblArticulos);
            this.Controls.Add(this.dgvCodigosBarra);
            this.Controls.Add(this.dgvArticulos);
            this.Controls.Add(this.dgvColores);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frCodBarr";
            this.Text = "frCodBarr";
            this.Load += new System.EventHandler(this.frCodBarr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCodigosBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTallas)).EndInit();
            this.cMenuCodBarr.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvArticulos;
        private System.Windows.Forms.DataGridView dgvCodigosBarra;
        private System.Windows.Forms.Label lblArticulos;
        private System.Windows.Forms.Label lbCodigosBarra;
        private System.Windows.Forms.Button btBuscar;
        private System.Windows.Forms.Button btLoadAll;
        private System.Windows.Forms.Button btCreateCodBarr;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.TextBox tbPais;
        private System.Windows.Forms.Label lblPais;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEmpresa;
        private System.Windows.Forms.TextBox tbBarrcodeGen;
        private System.Windows.Forms.Label lbLastBarcode;
        private System.Windows.Forms.TextBox tbLastBarcode;
        private System.Windows.Forms.Label lbTallas;
        private System.Windows.Forms.DataGridView dgvColores;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvTallas;
        private System.Windows.Forms.Label lbContador;
        private System.Windows.Forms.TextBox tbContador;
        private System.Windows.Forms.ContextMenuStrip cMenuCodBarr;
        private System.Windows.Forms.ToolStripMenuItem borrarCódigoDeBarrasToolStripMenuItem;
        private System.Windows.Forms.Button btCrearCodBarr;
    }
}