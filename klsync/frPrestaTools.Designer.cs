﻿namespace klsync
{
    partial class frPrestaTools
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frPrestaTools));
            this.dgvArticulos = new System.Windows.Forms.DataGridView();
            this.contextPeso = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.añadirPesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clboxCategorias = new System.Windows.Forms.CheckedListBox();
            this.cMenuCategoryAction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.árbolDeCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cMenuCategoryFilter = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.árbolDeCategoríaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.clboxMarcas = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btSustituirCategorias = new System.Windows.Forms.Button();
            this.btBorrarCategorias = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.clboxCaracteristicas = new System.Windows.Forms.CheckedListBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btAsignarCaracteristica = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btBorrarMarca = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.clboxCaracteristicasFilter = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.clboxMarcasFilter = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.clboxCategoriasFilter = new System.Windows.Forms.CheckedListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btDescarcarMarcas = new System.Windows.Forms.Button();
            this.btArbolCategorias = new System.Windows.Forms.Button();
            this.btDesmarcarCaracteristicas = new System.Windows.Forms.Button();
            this.btDesmarcarFiltroCaracteristicas = new System.Windows.Forms.Button();
            this.btDesmarcarCategorias = new System.Windows.Forms.Button();
            this.btDesmarcarFiltroMarcas = new System.Windows.Forms.Button();
            this.btDesmarcarFiltroCategorias = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tstbNombreArticulo = new System.Windows.Forms.ToolStripTextBox();
            this.tsbtBorrarTextoFiltro = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnCargarArticulos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.tscboxFeatures = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tscboxPeso = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnPesoAtributos = new System.Windows.Forms.ToolStripButton();
            this.btAsignarCategoria = new System.Windows.Forms.Button();
            this.cboxOrderByID = new System.Windows.Forms.CheckBox();
            this.actualizarStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearAtributosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).BeginInit();
            this.contextPeso.SuspendLayout();
            this.cMenuCategoryAction.SuspendLayout();
            this.cMenuCategoryFilter.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvArticulos
            // 
            this.dgvArticulos.AllowUserToAddRows = false;
            this.dgvArticulos.AllowUserToDeleteRows = false;
            this.dgvArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArticulos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulos.ContextMenuStrip = this.contextPeso;
            this.dgvArticulos.Location = new System.Drawing.Point(221, 98);
            this.dgvArticulos.Name = "dgvArticulos";
            this.dgvArticulos.ReadOnly = true;
            this.dgvArticulos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulos.Size = new System.Drawing.Size(694, 664);
            this.dgvArticulos.TabIndex = 3;
            // 
            // contextPeso
            // 
            this.contextPeso.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.añadirPesoToolStripMenuItem,
            this.actualizarFechaToolStripMenuItem,
            this.actualizarStockToolStripMenuItem,
            this.resetearAtributosToolStripMenuItem});
            this.contextPeso.Name = "contextPeso";
            this.contextPeso.Size = new System.Drawing.Size(171, 114);
            // 
            // añadirPesoToolStripMenuItem
            // 
            this.añadirPesoToolStripMenuItem.Name = "añadirPesoToolStripMenuItem";
            this.añadirPesoToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.añadirPesoToolStripMenuItem.Text = "Añadir Peso";
            this.añadirPesoToolStripMenuItem.Click += new System.EventHandler(this.añadirPesoToolStripMenuItem_Click);
            // 
            // actualizarFechaToolStripMenuItem
            // 
            this.actualizarFechaToolStripMenuItem.Name = "actualizarFechaToolStripMenuItem";
            this.actualizarFechaToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.actualizarFechaToolStripMenuItem.Text = "Actualizar fecha";
            this.actualizarFechaToolStripMenuItem.Click += new System.EventHandler(this.actualizarFechaToolStripMenuItem_Click);
            // 
            // clboxCategorias
            // 
            this.clboxCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clboxCategorias.ContextMenuStrip = this.cMenuCategoryAction;
            this.clboxCategorias.FormattingEnabled = true;
            this.clboxCategorias.HorizontalScrollbar = true;
            this.clboxCategorias.Location = new System.Drawing.Point(921, 131);
            this.clboxCategorias.Name = "clboxCategorias";
            this.clboxCategorias.Size = new System.Drawing.Size(325, 169);
            this.clboxCategorias.TabIndex = 5;
            // 
            // cMenuCategoryAction
            // 
            this.cMenuCategoryAction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.árbolDeCategoriaToolStripMenuItem});
            this.cMenuCategoryAction.Name = "cMenuCategoryAction";
            this.cMenuCategoryAction.Size = new System.Drawing.Size(174, 26);
            // 
            // árbolDeCategoriaToolStripMenuItem
            // 
            this.árbolDeCategoriaToolStripMenuItem.Name = "árbolDeCategoriaToolStripMenuItem";
            this.árbolDeCategoriaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.árbolDeCategoriaToolStripMenuItem.Text = "Árbol de Categoría";
            this.árbolDeCategoriaToolStripMenuItem.Click += new System.EventHandler(this.árbolDeCategoriaToolStripMenuItem_Click);
            // 
            // cMenuCategoryFilter
            // 
            this.cMenuCategoryFilter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.árbolDeCategoríaToolStripMenuItem1});
            this.cMenuCategoryFilter.Name = "cMenuOpciones";
            this.cMenuCategoryFilter.Size = new System.Drawing.Size(174, 26);
            // 
            // árbolDeCategoríaToolStripMenuItem1
            // 
            this.árbolDeCategoríaToolStripMenuItem1.Name = "árbolDeCategoríaToolStripMenuItem1";
            this.árbolDeCategoríaToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.árbolDeCategoríaToolStripMenuItem1.Text = "Árbol de Categoría";
            this.árbolDeCategoríaToolStripMenuItem1.Click += new System.EventHandler(this.árbolDeCategoríaToolStripMenuItem1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(921, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "CATEGORÍAS";
            // 
            // clboxMarcas
            // 
            this.clboxMarcas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clboxMarcas.FormattingEnabled = true;
            this.clboxMarcas.Location = new System.Drawing.Point(921, 368);
            this.clboxMarcas.Name = "clboxMarcas";
            this.clboxMarcas.Size = new System.Drawing.Size(325, 139);
            this.clboxMarcas.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(922, 345);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "MARCAS";
            // 
            // btSustituirCategorias
            // 
            this.btSustituirCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSustituirCategorias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSustituirCategorias.Image = ((System.Drawing.Image)(resources.GetObject("btSustituirCategorias.Image")));
            this.btSustituirCategorias.Location = new System.Drawing.Point(1034, 303);
            this.btSustituirCategorias.Name = "btSustituirCategorias";
            this.btSustituirCategorias.Size = new System.Drawing.Size(103, 37);
            this.btSustituirCategorias.TabIndex = 19;
            this.btSustituirCategorias.Text = "Sustituir";
            this.btSustituirCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSustituirCategorias.UseVisualStyleBackColor = true;
            this.btSustituirCategorias.Click += new System.EventHandler(this.btSustituirCategorias_Click);
            // 
            // btBorrarCategorias
            // 
            this.btBorrarCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBorrarCategorias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBorrarCategorias.Image = ((System.Drawing.Image)(resources.GetObject("btBorrarCategorias.Image")));
            this.btBorrarCategorias.Location = new System.Drawing.Point(1143, 303);
            this.btBorrarCategorias.Name = "btBorrarCategorias";
            this.btBorrarCategorias.Size = new System.Drawing.Size(103, 37);
            this.btBorrarCategorias.TabIndex = 20;
            this.btBorrarCategorias.Text = "Borrar";
            this.btBorrarCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btBorrarCategorias.UseVisualStyleBackColor = true;
            this.btBorrarCategorias.Click += new System.EventHandler(this.btBorrarCategorias_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(922, 557);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 20);
            this.label8.TabIndex = 25;
            this.label8.Text = "CARACTERÍSTICAS";
            // 
            // clboxCaracteristicas
            // 
            this.clboxCaracteristicas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clboxCaracteristicas.FormattingEnabled = true;
            this.clboxCaracteristicas.HorizontalScrollbar = true;
            this.clboxCaracteristicas.Location = new System.Drawing.Point(924, 580);
            this.clboxCaracteristicas.Name = "clboxCaracteristicas";
            this.clboxCaracteristicas.Size = new System.Drawing.Size(325, 139);
            this.clboxCaracteristicas.TabIndex = 24;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(1146, 725);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 37);
            this.button3.TabIndex = 31;
            this.button3.Text = "Borrar";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(1037, 725);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 37);
            this.button4.TabIndex = 30;
            this.button4.Text = "Sustituir";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // btAsignarCaracteristica
            // 
            this.btAsignarCaracteristica.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAsignarCaracteristica.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAsignarCaracteristica.Image = ((System.Drawing.Image)(resources.GetObject("btAsignarCaracteristica.Image")));
            this.btAsignarCaracteristica.Location = new System.Drawing.Point(924, 725);
            this.btAsignarCaracteristica.Name = "btAsignarCaracteristica";
            this.btAsignarCaracteristica.Size = new System.Drawing.Size(103, 37);
            this.btAsignarCaracteristica.TabIndex = 29;
            this.btAsignarCaracteristica.Text = "Asignar";
            this.btAsignarCaracteristica.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAsignarCaracteristica.UseVisualStyleBackColor = true;
            this.btAsignarCaracteristica.Click += new System.EventHandler(this.btAsignarCaracteristica_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(921, 513);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 37);
            this.button1.TabIndex = 33;
            this.button1.Text = "Asignar";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btBorrarMarca
            // 
            this.btBorrarMarca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBorrarMarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBorrarMarca.Image = ((System.Drawing.Image)(resources.GetObject("btBorrarMarca.Image")));
            this.btBorrarMarca.Location = new System.Drawing.Point(1030, 513);
            this.btBorrarMarca.Name = "btBorrarMarca";
            this.btBorrarMarca.Size = new System.Drawing.Size(103, 37);
            this.btBorrarMarca.TabIndex = 34;
            this.btBorrarMarca.Text = "Borrar";
            this.btBorrarMarca.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btBorrarMarca.UseVisualStyleBackColor = true;
            this.btBorrarMarca.Click += new System.EventHandler(this.btBorrarMarca_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 557);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(160, 20);
            this.label7.TabIndex = 22;
            this.label7.Text = "CARACTERÍSTICAS";
            // 
            // clboxCaracteristicasFilter
            // 
            this.clboxCaracteristicasFilter.FormattingEnabled = true;
            this.clboxCaracteristicasFilter.Location = new System.Drawing.Point(5, 580);
            this.clboxCaracteristicasFilter.Name = "clboxCaracteristicasFilter";
            this.clboxCaracteristicasFilter.Size = new System.Drawing.Size(210, 184);
            this.clboxCaracteristicasFilter.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 345);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "MARCAS";
            // 
            // clboxMarcasFilter
            // 
            this.clboxMarcasFilter.FormattingEnabled = true;
            this.clboxMarcasFilter.Location = new System.Drawing.Point(5, 368);
            this.clboxMarcasFilter.Name = "clboxMarcasFilter";
            this.clboxMarcasFilter.Size = new System.Drawing.Size(210, 169);
            this.clboxMarcasFilter.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "CATEGORÍAS";
            // 
            // clboxCategoriasFilter
            // 
            this.clboxCategoriasFilter.ContextMenuStrip = this.cMenuCategoryFilter;
            this.clboxCategoriasFilter.Location = new System.Drawing.Point(8, 131);
            this.clboxCategoriasFilter.Name = "clboxCategoriasFilter";
            this.clboxCategoriasFilter.Size = new System.Drawing.Size(208, 199);
            this.clboxCategoriasFilter.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(2, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(170, 29);
            this.label9.TabIndex = 36;
            this.label9.Text = "FILTRAR POR";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(1109, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 29);
            this.label10.TabIndex = 37;
            this.label10.Text = "ACCIONES";
            // 
            // btDescarcarMarcas
            // 
            this.btDescarcarMarcas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDescarcarMarcas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDescarcarMarcas.BackgroundImage")));
            this.btDescarcarMarcas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDescarcarMarcas.Location = new System.Drawing.Point(1216, 337);
            this.btDescarcarMarcas.Name = "btDescarcarMarcas";
            this.btDescarcarMarcas.Size = new System.Drawing.Size(27, 28);
            this.btDescarcarMarcas.TabIndex = 41;
            this.btDescarcarMarcas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btDescarcarMarcas, "Borrar Selección");
            this.btDescarcarMarcas.UseVisualStyleBackColor = true;
            this.btDescarcarMarcas.Click += new System.EventHandler(this.btDescarcarMarcas_Click);
            // 
            // btArbolCategorias
            // 
            this.btArbolCategorias.BackgroundImage = global::klsync.Properties.Resources.diagram;
            this.btArbolCategorias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btArbolCategorias.Location = new System.Drawing.Point(149, 98);
            this.btArbolCategorias.Name = "btArbolCategorias";
            this.btArbolCategorias.Size = new System.Drawing.Size(30, 30);
            this.btArbolCategorias.TabIndex = 40;
            this.btArbolCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolTip1.SetToolTip(this.btArbolCategorias, "Mostrar Arbol de Categorías");
            this.btArbolCategorias.UseVisualStyleBackColor = true;
            this.btArbolCategorias.Click += new System.EventHandler(this.btArbolCategorias_Click);
            // 
            // btDesmarcarCaracteristicas
            // 
            this.btDesmarcarCaracteristicas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDesmarcarCaracteristicas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDesmarcarCaracteristicas.BackgroundImage")));
            this.btDesmarcarCaracteristicas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDesmarcarCaracteristicas.Location = new System.Drawing.Point(1216, 549);
            this.btDesmarcarCaracteristicas.Name = "btDesmarcarCaracteristicas";
            this.btDesmarcarCaracteristicas.Size = new System.Drawing.Size(27, 28);
            this.btDesmarcarCaracteristicas.TabIndex = 39;
            this.btDesmarcarCaracteristicas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btDesmarcarCaracteristicas, "Borrar Selección");
            this.btDesmarcarCaracteristicas.UseVisualStyleBackColor = true;
            this.btDesmarcarCaracteristicas.Click += new System.EventHandler(this.btDesmarcarCaracteristicas_Click);
            // 
            // btDesmarcarFiltroCaracteristicas
            // 
            this.btDesmarcarFiltroCaracteristicas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDesmarcarFiltroCaracteristicas.BackgroundImage")));
            this.btDesmarcarFiltroCaracteristicas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDesmarcarFiltroCaracteristicas.Location = new System.Drawing.Point(186, 549);
            this.btDesmarcarFiltroCaracteristicas.Name = "btDesmarcarFiltroCaracteristicas";
            this.btDesmarcarFiltroCaracteristicas.Size = new System.Drawing.Size(30, 30);
            this.btDesmarcarFiltroCaracteristicas.TabIndex = 23;
            this.toolTip1.SetToolTip(this.btDesmarcarFiltroCaracteristicas, "Borrar Selección");
            this.btDesmarcarFiltroCaracteristicas.UseVisualStyleBackColor = true;
            this.btDesmarcarFiltroCaracteristicas.Click += new System.EventHandler(this.btDesmarcarFiltroCaracteristicas_Click);
            // 
            // btDesmarcarCategorias
            // 
            this.btDesmarcarCategorias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDesmarcarCategorias.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDesmarcarCategorias.BackgroundImage")));
            this.btDesmarcarCategorias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDesmarcarCategorias.Location = new System.Drawing.Point(1213, 98);
            this.btDesmarcarCategorias.Name = "btDesmarcarCategorias";
            this.btDesmarcarCategorias.Size = new System.Drawing.Size(30, 30);
            this.btDesmarcarCategorias.TabIndex = 18;
            this.btDesmarcarCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btDesmarcarCategorias, "Borrar Selección");
            this.btDesmarcarCategorias.UseVisualStyleBackColor = true;
            this.btDesmarcarCategorias.Click += new System.EventHandler(this.btDesmarcarCategorias_Click);
            // 
            // btDesmarcarFiltroMarcas
            // 
            this.btDesmarcarFiltroMarcas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btDesmarcarFiltroMarcas.BackgroundImage")));
            this.btDesmarcarFiltroMarcas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDesmarcarFiltroMarcas.Location = new System.Drawing.Point(186, 335);
            this.btDesmarcarFiltroMarcas.Name = "btDesmarcarFiltroMarcas";
            this.btDesmarcarFiltroMarcas.Size = new System.Drawing.Size(30, 30);
            this.btDesmarcarFiltroMarcas.TabIndex = 16;
            this.toolTip1.SetToolTip(this.btDesmarcarFiltroMarcas, "Borrar Selección");
            this.btDesmarcarFiltroMarcas.UseVisualStyleBackColor = true;
            this.btDesmarcarFiltroMarcas.Click += new System.EventHandler(this.btDesmarcarFiltroMarcas_Click);
            // 
            // btDesmarcarFiltroCategorias
            // 
            this.btDesmarcarFiltroCategorias.BackgroundImage = global::klsync.Properties.Resources.delete;
            this.btDesmarcarFiltroCategorias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDesmarcarFiltroCategorias.Location = new System.Drawing.Point(185, 98);
            this.btDesmarcarFiltroCategorias.Name = "btDesmarcarFiltroCategorias";
            this.btDesmarcarFiltroCategorias.Size = new System.Drawing.Size(30, 30);
            this.btDesmarcarFiltroCategorias.TabIndex = 15;
            this.btDesmarcarFiltroCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolTip1.SetToolTip(this.btDesmarcarFiltroCategorias, "Borrar Selección");
            this.btDesmarcarFiltroCategorias.UseVisualStyleBackColor = true;
            this.btDesmarcarFiltroCategorias.Click += new System.EventHandler(this.btDesmarcarFiltroCategorias_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tstbNombreArticulo,
            this.tsbtBorrarTextoFiltro,
            this.toolStripSeparator2,
            this.tsbtnCargarArticulos,
            this.toolStripSeparator1,
            this.toolStripLabel3,
            this.tscboxFeatures,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.tscboxPeso,
            this.toolStripSeparator4,
            this.tsbtnPesoAtributos});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1258, 54);
            this.toolStrip1.TabIndex = 42;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(169, 51);
            this.toolStripLabel1.Text = "Filtrar por Nombre";
            // 
            // tstbNombreArticulo
            // 
            this.tstbNombreArticulo.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tstbNombreArticulo.Name = "tstbNombreArticulo";
            this.tstbNombreArticulo.Size = new System.Drawing.Size(300, 54);
            this.tstbNombreArticulo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tstbNombreArticulo_KeyPress);
            // 
            // tsbtBorrarTextoFiltro
            // 
            this.tsbtBorrarTextoFiltro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tsbtBorrarTextoFiltro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtBorrarTextoFiltro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtBorrarTextoFiltro.Name = "tsbtBorrarTextoFiltro";
            this.tsbtBorrarTextoFiltro.Size = new System.Drawing.Size(23, 51);
            this.tsbtBorrarTextoFiltro.Text = "tsbtLimpiarTextoFiltro";
            this.tsbtBorrarTextoFiltro.ToolTipText = "Borrar Texto de Filtro";
            this.tsbtBorrarTextoFiltro.Click += new System.EventHandler(this.tsbtBorrarTextoFiltro_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 54);
            // 
            // tsbtnCargarArticulos
            // 
            this.tsbtnCargarArticulos.Image = global::klsync.Properties.Resources.load_data1;
            this.tsbtnCargarArticulos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnCargarArticulos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCargarArticulos.Name = "tsbtnCargarArticulos";
            this.tsbtnCargarArticulos.Size = new System.Drawing.Size(96, 51);
            this.tsbtnCargarArticulos.Text = "&Cargar Artículos";
            this.tsbtnCargarArticulos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtnCargarArticulos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnCargarArticulos.Click += new System.EventHandler(this.tsbtnCargarArticulos_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(134, 51);
            this.toolStripLabel3.Text = "Características";
            // 
            // tscboxFeatures
            // 
            this.tscboxFeatures.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tscboxFeatures.Items.AddRange(new object[] {
            "NO",
            "SI"});
            this.tscboxFeatures.Name = "tscboxFeatures";
            this.tscboxFeatures.Size = new System.Drawing.Size(75, 54);
            this.tscboxFeatures.Text = "NO";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(51, 51);
            this.toolStripLabel2.Text = "Peso";
            // 
            // tscboxPeso
            // 
            this.tscboxPeso.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tscboxPeso.Items.AddRange(new object[] {
            "NO",
            "SI"});
            this.tscboxPeso.Name = "tscboxPeso";
            this.tscboxPeso.Size = new System.Drawing.Size(75, 54);
            this.tscboxPeso.Text = "NO";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 54);
            // 
            // tsbtnPesoAtributos
            // 
            this.tsbtnPesoAtributos.Image = global::klsync.Properties.Resources.weight;
            this.tsbtnPesoAtributos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnPesoAtributos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnPesoAtributos.Name = "tsbtnPesoAtributos";
            this.tsbtnPesoAtributos.Size = new System.Drawing.Size(88, 51);
            this.tsbtnPesoAtributos.Text = "&Peso Atributos";
            this.tsbtnPesoAtributos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbtnPesoAtributos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnPesoAtributos.Click += new System.EventHandler(this.tsbtnPesoAtributos_Click);
            // 
            // btAsignarCategoria
            // 
            this.btAsignarCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAsignarCategoria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btAsignarCategoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAsignarCategoria.Image = global::klsync.Properties.Resources.plus1;
            this.btAsignarCategoria.Location = new System.Drawing.Point(921, 303);
            this.btAsignarCategoria.Name = "btAsignarCategoria";
            this.btAsignarCategoria.Size = new System.Drawing.Size(103, 38);
            this.btAsignarCategoria.TabIndex = 17;
            this.btAsignarCategoria.Text = "Asignar";
            this.btAsignarCategoria.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAsignarCategoria.UseVisualStyleBackColor = true;
            this.btAsignarCategoria.Click += new System.EventHandler(this.btAsignarCategoria_Click);
            // 
            // cboxOrderByID
            // 
            this.cboxOrderByID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxOrderByID.AutoSize = true;
            this.cboxOrderByID.Location = new System.Drawing.Point(1111, 108);
            this.cboxOrderByID.Name = "cboxOrderByID";
            this.cboxOrderByID.Size = new System.Drawing.Size(96, 17);
            this.cboxOrderByID.TabIndex = 43;
            this.cboxOrderByID.Text = "Ordenar por ID";
            this.cboxOrderByID.UseVisualStyleBackColor = true;
            this.cboxOrderByID.CheckedChanged += new System.EventHandler(this.cboxOrderByID_CheckedChanged);
            // 
            // actualizarStockToolStripMenuItem
            // 
            this.actualizarStockToolStripMenuItem.Name = "actualizarStockToolStripMenuItem";
            this.actualizarStockToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.actualizarStockToolStripMenuItem.Text = "Actualizar Stock";
            this.actualizarStockToolStripMenuItem.Click += new System.EventHandler(this.actualizarStockToolStripMenuItem_Click);
            // 
            // resetearAtributosToolStripMenuItem
            // 
            this.resetearAtributosToolStripMenuItem.Name = "resetearAtributosToolStripMenuItem";
            this.resetearAtributosToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.resetearAtributosToolStripMenuItem.Text = "&Resetear Atributos";
            this.resetearAtributosToolStripMenuItem.Click += new System.EventHandler(this.resetearAtributosToolStripMenuItem_Click);
            // 
            // frPrestaTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1258, 775);
            this.Controls.Add(this.cboxOrderByID);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btDescarcarMarcas);
            this.Controls.Add(this.btArbolCategorias);
            this.Controls.Add(this.btDesmarcarCaracteristicas);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btBorrarMarca);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btAsignarCaracteristica);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.clboxCaracteristicas);
            this.Controls.Add(this.btDesmarcarFiltroCaracteristicas);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.clboxCaracteristicasFilter);
            this.Controls.Add(this.btBorrarCategorias);
            this.Controls.Add(this.btSustituirCategorias);
            this.Controls.Add(this.btDesmarcarCategorias);
            this.Controls.Add(this.btAsignarCategoria);
            this.Controls.Add(this.btDesmarcarFiltroMarcas);
            this.Controls.Add(this.btDesmarcarFiltroCategorias);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.clboxMarcasFilter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.clboxCategoriasFilter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.clboxMarcas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.clboxCategorias);
            this.Controls.Add(this.dgvArticulos);
            this.KeyPreview = true;
            this.Name = "frPrestaTools";
            this.Text = "Utilidades Prestashop";
            this.Load += new System.EventHandler(this.frPrestaTools_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frPrestaTools_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).EndInit();
            this.contextPeso.ResumeLayout(false);
            this.cMenuCategoryAction.ResumeLayout(false);
            this.cMenuCategoryFilter.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvArticulos;
        private System.Windows.Forms.CheckedListBox clboxCategorias;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox clboxMarcas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btAsignarCategoria;
        private System.Windows.Forms.Button btDesmarcarCategorias;
        private System.Windows.Forms.Button btSustituirCategorias;
        private System.Windows.Forms.Button btBorrarCategorias;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox clboxCaracteristicas;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btAsignarCaracteristica;
        private System.Windows.Forms.ContextMenuStrip contextPeso;
        private System.Windows.Forms.ToolStripMenuItem añadirPesoToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btBorrarMarca;
        private System.Windows.Forms.Button btDesmarcarFiltroMarcas;
        private System.Windows.Forms.Button btDesmarcarFiltroCaracteristicas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox clboxCaracteristicasFilter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox clboxMarcasFilter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox clboxCategoriasFilter;
        private System.Windows.Forms.ContextMenuStrip cMenuCategoryFilter;
        private System.Windows.Forms.ToolStripMenuItem árbolDeCategoríaToolStripMenuItem1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ContextMenuStrip cMenuCategoryAction;
        private System.Windows.Forms.ToolStripMenuItem árbolDeCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarFechaToolStripMenuItem;
        private System.Windows.Forms.Button btDesmarcarFiltroCategorias;
        private System.Windows.Forms.Button btDesmarcarCaracteristicas;
        private System.Windows.Forms.Button btArbolCategorias;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btDescarcarMarcas;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox tstbNombreArticulo;
        private System.Windows.Forms.ToolStripButton tsbtnCargarArticulos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtBorrarTextoFiltro;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox tscboxFeatures;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox tscboxPeso;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbtnPesoAtributos;
        private System.Windows.Forms.CheckBox cboxOrderByID;
        private System.Windows.Forms.ToolStripMenuItem actualizarStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearAtributosToolStripMenuItem;
    }
}