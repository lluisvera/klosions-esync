﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using klsync.Configuracion;
using System.Net.Mail;

namespace klsync.GestionErrores
{
    class csGestionErrores
    {
        public string errorSerieDocumento { get; set; }
        public string errorNumeroDocumento { get; set; }
        public string errorNumeroLinea { get; set; }
        public string errorCodClienteA3ERP { get; set; }
        public string errorNomClienteA3ERP { get; set; }
        public string errorNumVencimiento { get; set; }
        public string errorFechaDoc { get; set; }
        public string errorFechaCobro { get; set; }
        public string errorTipoDocumento { get; set; }
        public string errorCodBanco { get; set; }
        public string errorNomBanco { get; set; }
        public string errorCodArticulo { get; set; }
        public string errorNomArticulo { get; set; }
        public string errorImporteDocumento { get; set; }
        public string errorImporteCobrado { get; set; }


        //NACHO 03/07/24: Función para clasificar los errores por id según el contenido del mensaje de error.
        //public int ObtenerIdError(Exception ex)
        //{
        //    string mensaje = ex.Message;

        //    if (mensaje.Contains("tabla 'dbo.DIRENT', column 'IDDIRENT'"))
        //    {
        //        return 1; 
        //    }
        //    else if (mensaje.Contains(""))
        //    {
        //        return 2; 
        //    }
        //    return -1;
        //}

        //NACHO 03/07/24: Función para enviar un correo electronico segun los parametros establecidos en la clase ConfiguracionCorreo.
        //public void EnviarCorreoError(string asunto, string cuerpo)
        //{
        //    try
        //    {
        //        using (MailMessage mail = new MailMessage())
        //        {
        //            mail.From = new MailAddress(ConfiguracionCorreo.Remitente);
        //            mail.To.Add(ConfiguracionCorreo.Destinatario);
        //            mail.Subject = asunto;
        //            mail.Body = cuerpo;
        //            mail.IsBodyHtml = false;

        //            using (SmtpClient smtp = new SmtpClient(ConfiguracionCorreo.ServidorSmtp, ConfiguracionCorreo.PuertoSmtp))
        //            {
        //                smtp.Credentials = new NetworkCredential(ConfiguracionCorreo.Usuario, ConfiguracionCorreo.Contraseña);
        //                smtp.EnableSsl = true;
        //                smtp.Send(mail);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Program.guardarErrorFichero(ex.ToString());
        //    }
        //}
    }
}
