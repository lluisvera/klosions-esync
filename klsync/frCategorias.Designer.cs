﻿﻿namespace klsync

{

     partial class frFamilias

    {

        /// <summary>

        /// Required designer variable.

        /// </summary>

        private System.ComponentModel.IContainer components = null;



        /// <summary>

        /// Clean up any resources being used.

        /// </summary>

        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        protected override void Dispose(bool disposing)

        {

            if (disposing && (components != null))

            {

                components.Dispose();

            }

            base.Dispose(disposing);

        }



        #region Windows Form Designer generated code



        /// <summary>

        /// Required method for Designer support  do not modify

        /// the contents of this method with the code editor.

        /// </summary>

        private void InitializeComponent()

        {

            this.components = new System.ComponentModel.Container();

            this.dgvFamCat = new System.Windows.Forms.DataGridView();

            this.cntxExportarFam = new System.Windows.Forms.ContextMenuStrip(this.components);

            this.exportarFilaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.btnCargarFam = new System.Windows.Forms.Button();

            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();

            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCat)).BeginInit();

            this.cntxExportarFam.SuspendLayout();

            this.tableLayoutPanel1.SuspendLayout();

            this.SuspendLayout();

            // 

            // dgvFamCat

            // 

            this.dgvFamCat.AllowUserToAddRows = false;

            this.dgvFamCat.AllowUserToDeleteRows = false;

            this.dgvFamCat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 

            | System.Windows.Forms.AnchorStyles.Left) 

            | System.Windows.Forms.AnchorStyles.Right)));

            this.dgvFamCat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;

            this.dgvFamCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            this.dgvFamCat.ContextMenuStrip = this.cntxExportarFam;

            this.dgvFamCat.Location = new System.Drawing.Point(3, 45);

            this.dgvFamCat.Name = "dgvFamCat";

            this.dgvFamCat.ReadOnly = true;

            this.dgvFamCat.Size = new System.Drawing.Size(781, 425);

            this.dgvFamCat.TabIndex = 2;

            // 

            // cntxExportarFam

            // 

            this.cntxExportarFam.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {

            this.exportarFilaToolStripMenuItem});

            this.cntxExportarFam.Name = "cntxExportarFam";

            this.cntxExportarFam.Size = new System.Drawing.Size(139, 26);

            // 

            // exportarFilaToolStripMenuItem

            // 

            this.exportarFilaToolStripMenuItem.Name = "exportarFilaToolStripMenuItem";

            this.exportarFilaToolStripMenuItem.Size = new System.Drawing.Size(138, 22);

            this.exportarFilaToolStripMenuItem.Text = "&Exportar Fila";

            this.exportarFilaToolStripMenuItem.Click += new System.EventHandler(this.exportarFilaToolStripMenuItem_Click);

            // 

            // btnCargarFam

            // 

            this.btnCargarFam.Dock = System.Windows.Forms.DockStyle.Fill;

            this.btnCargarFam.Location = new System.Drawing.Point(3, 3);

            this.btnCargarFam.Name = "btnCargarFam";

            this.btnCargarFam.Size = new System.Drawing.Size(781, 36);

            this.btnCargarFam.TabIndex = 3;

            this.btnCargarFam.Text = "Cargar Familias";

            this.btnCargarFam.UseVisualStyleBackColor = true;

            this.btnCargarFam.Click += new System.EventHandler(this.btnCargarFam_Click);

            // 

            // tableLayoutPanel1

            // 

            this.tableLayoutPanel1.ColumnCount = 1;

            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));

            this.tableLayoutPanel1.Controls.Add(this.btnCargarFam, 0, 0);

            this.tableLayoutPanel1.Controls.Add(this.dgvFamCat, 0, 1);

            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;

            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);

            this.tableLayoutPanel1.Name = "tableLayoutPanel1";

            this.tableLayoutPanel1.RowCount = 2;

            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.879493F));

            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.12051F));

            this.tableLayoutPanel1.Size = new System.Drawing.Size(787, 473);

            this.tableLayoutPanel1.TabIndex = 4;

            // 

            // frFamCat

            // 

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);

            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            this.ClientSize = new System.Drawing.Size(787, 473);

            this.Controls.Add(this.tableLayoutPanel1);

            this.Name = "frFamCat";

            this.Text = "Familias > Categorias";

            this.Load += new System.EventHandler(this.frFamCat_Load);

            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCat)).EndInit();

            this.cntxExportarFam.ResumeLayout(false);

            this.tableLayoutPanel1.ResumeLayout(false);

            this.ResumeLayout(false);



        }



        #endregion



        private System.Windows.Forms.DataGridView dgvFamCat;

        private System.Windows.Forms.Button btnCargarFam;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

        private System.Windows.Forms.ContextMenuStrip cntxExportarFam;

        private System.Windows.Forms.ToolStripMenuItem exportarFilaToolStripMenuItem;

    }

}