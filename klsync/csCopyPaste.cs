﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;

namespace klsync
{
    class csCopyPaste
    {
        //Sobre Datatable
        public void pasteClipboardDT(DataGridView dgv, bool withHeaders)
        {
            DataObject o = (DataObject)Clipboard.GetDataObject();
            int numFila = 0;
            int numColumnas = 0;

            //Pruebas
            DataTable hojaExcel = new DataTable();

            //hojaExcel.Columns.Add("centrocoste1");

            if (o.GetDataPresent(DataFormats.Text))
            {
                if (dgv.RowCount > 0)
                    dgv.Rows.Clear();

                if (dgv.ColumnCount > 0)
                    dgv.Columns.Clear();

                bool columnsAdded = false;

                //Traspaso todo el portapapeles a un array de filas
                string[] pastedRows = Regex.Split(o.GetData(DataFormats.Text).ToString().TrimEnd("\r\n".ToCharArray()), "\r\n");
                int j = 0;

                //Pateo todas las filas
                foreach (string pastedRow in pastedRows)
                {
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });

                    //Si es la primera fila añado las columnas
                    if (numFila == 0)
                    {
                        //Creo las columnas
                        foreach (string item in pastedRowCells)
                        {
                            hojaExcel.Columns.Add(item);
                        }
                        numFila++;

                    }
                    else
                    {
                        DataRow filaExcel = hojaExcel.NewRow();
                        foreach (string item in pastedRowCells)
                        {
                            filaExcel[numColumnas] = item;
                            numColumnas++;
                        }

                        hojaExcel.Rows.Add(filaExcel);
                        numColumnas = 0;

                    }


                }
                dgv.DataSource = hojaExcel;
                dgv.Refresh();
            }
        }

        public void PasteClipboard(DataGridView dgv)
        {
            DataObject o = (DataObject)Clipboard.GetDataObject();
           
            if (o.GetDataPresent(DataFormats.Text))
            {
                if (dgv.RowCount > 0)
                    dgv.Rows.Clear();

                if (dgv.ColumnCount > 0)
                    dgv.Columns.Clear();

                bool columnsAdded = false;
                string[] pastedRows = Regex.Split(o.GetData(DataFormats.Text).ToString().TrimEnd("\r\n".ToCharArray()), "\r\n");
                int j = 0;

              
                foreach (string pastedRow in pastedRows)
                {   
                    string[] pastedRowCells = pastedRow.Split(new char[] { '\t' });

                    if (!columnsAdded)
                    {
                        for (int i = 0; i < pastedRowCells.Length; i++)
                            dgv.Columns.Add(pastedRowCells[i], pastedRowCells[i]);

                        columnsAdded = true;
                        continue;
                    }

                    dgv.Rows.Add();
                    
                   // dgv.Refresh();
                    int myRowIndex = dgv.Rows.Count - 1;

                    using (DataGridViewRow myDataGridViewRow = dgv.Rows[j])
                    {
                        for (int i = 0; i < pastedRowCells.Length; i++)
                            myDataGridViewRow.Cells[i].Value = pastedRowCells[i];
                    }
                    j++;
                }
                dgv.Refresh();
            }
        }
    }
}
