﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using OutLook = Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Interop.Excel;
using System.Reflection; //para el valor missing
using System.Windows.Forms;
using System.Globalization;

namespace klsync
{
    class csOfficeIntegration
    {
        public void ExportToExcel(System.Windows.Forms.DataGridView dgvDatos)
        {
            try
            {
                string temp;
                Microsoft.Office.Interop.Excel.Application xlsApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Worksheet xlsSheet;
                Microsoft.Office.Interop.Excel.Workbook xlsBook;
                xlsApp.Visible = false;

                System.Globalization.CultureInfo CurrentCI = System.Threading.Thread.CurrentThread.CurrentCulture;

                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                xlsBook = xlsApp.Workbooks.Add(true);
                xlsSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlsBook.ActiveSheet;

                int iCol = 0;
                foreach (DataGridViewColumn column in dgvDatos.Columns)
                {
                    if (column.Visible)
                        xlsSheet.Cells[1, ++iCol] = column.HeaderText;
                }

                foreach (DataGridViewRow row in dgvDatos.Rows)
                {
                    iCol = 0;
                    foreach (DataGridViewColumn column in dgvDatos.Columns)
                    {
                        if (column.Visible)
                        {
                            temp = dgvDatos[column.Index, row.Index].FormattedValue.ToString();
                            xlsSheet.Cells[row.Index + 2, ++iCol] = temp;
                        }
                    }
                }

                xlsSheet.Columns.AutoFit();
                xlsApp.Visible = true;
            }
            catch (Exception e)
            { throw e; }
        }
    }
}

