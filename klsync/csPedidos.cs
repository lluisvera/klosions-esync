﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;

namespace klsync
{
    class csPedidos
    {
        public void exportarFicheroXMLSGI(string numdoc)
        {
            csSqlConnects sql = new csSqlConnects();

            DataTable dt = sql.cargarDatosTablaA3(string.Format("SELECT dbo.PROVINCI.NOMPROVI, " +
                                                    "       dbo.CABEALBV.NUMDOC, " +
                                                    "       dbo.CABEALBV.FECHA, " +
                                                    "       dbo.CABEALBV.CODCLI, " +
                                                    "       dbo.CABEALBV.NOMCLI, " +
                                                    "       dbo.CABEALBV.NIFCLI, " +
                                                    "       dbo.DIRENT.TELENT1 AS TELCLI, " +
                                                    "       dbo.CABEALBV.REFERENCIA, " +
                                                    "       dbo.LINEALBA.CODART, " +
                                                    "       dbo.ARTICULO.ARTALIAS, " +
                                                    "       dbo.LINEALBA.DESCLIN, " +
                                                    "       dbo.LINEALBA.UNIDADES, " +
                                                    "       dbo.LINEALBA.ORDLIN, " +
                                                    "       dbo.CABEALBV.SITUACION, " +
                                                    "       dbo.DIRENT.DIRENT1 AS DIRCLI, " +
                                                    "       dbo.DIRENT.DTOENT AS DTOCLI, " +
                                                    "       dbo.DIRENT.POBENT AS POBCLI, " +
                                                    "       dbo.ARTICULO.CODPRO " +
                                                    "FROM dbo.DIRENT " +
                                                    "INNER JOIN dbo.PROVINCI ON dbo.DIRENT.CODPROVI = dbo.PROVINCI.CODPROVI " +
                                                    "INNER JOIN dbo.LINEALBA " +
                                                    "INNER JOIN dbo.CABEALBV ON dbo.LINEALBA.IDALBV = dbo.CABEALBV.IDALBV " +
                                                    "INNER JOIN dbo.ARTICULO ON dbo.LINEALBA.CODART = dbo.ARTICULO.CODART ON dbo.DIRENT.IDDIRENT = dbo.CABEALBV.IDDIRENT " +
                                                    "  AND dbo.CABEALBV.NUMDOC = {0} " +
                                                    "ORDER BY dbo.CABEALBV.NUMDOC, " +
                                                    "         dbo.LINEALBA.ORDLIN", Convert.ToDouble(numdoc)));


            if (dt.hasRows())
            {
                try
                {

                    //Si no existe el directorio lo crea
                    System.IO.FileInfo file = new System.IO.FileInfo("c:/albaranes/");
                    file.Directory.Create(); // If the directory already exists, this method does nothing.
                    
                    string numero_pedido = "";
                    string numero_pedido_detalle = "";

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.IndentChars = "  ";
                    settings.NewLineChars = "\r\n";
                    settings.NewLineHandling = NewLineHandling.Replace;
                    
                    XmlWriter writer = XmlWriter.Create("c:/albaranes/pedido-" + Convert.ToDouble(numdoc).ToString() + ".xml", settings);
                    writer.WriteStartElement("Pedidos");

                    foreach (DataRow row in dt.Rows)
                    {
                        if (numero_pedido != row[0].ToString())
                        {
                            numero_pedido = row[0].ToString();
                            writer.WriteStartElement("Pedido");
                            writer.WriteElementString("numDoc", Convert.ToInt32(row["NUMDOC"]).ToString());
                            writer.WriteElementString("fecha", row["FECHA"].ToString());
                            writer.WriteElementString("codcli", row["CODCLI"].ToString().Trim());
                            writer.WriteElementString("nomcli", row["NOMCLI"].ToString());
                            writer.WriteElementString("nifcli", row["NIFCLI"].ToString());
                            writer.WriteElementString("telcli", row["TELCLI"].ToString());
                            writer.WriteElementString("referencia", row["REFERENCIA"].ToString());

                            writer.WriteElementString("dirCli", row["DIRCLI"].ToString());
                            writer.WriteElementString("dtoCli", row["DTOCLI"].ToString());
                            writer.WriteElementString("pobCli", row["POBCLI"].ToString());
                            writer.WriteElementString("codPro", row["CODPRO"].ToString());
                            writer.WriteElementString("Provincia", row["NOMPROVI"].ToString().Trim());

                            writer.WriteStartElement("Lineas");

                            foreach (DataRow detalle in dt.Rows)
                            {
                                numero_pedido_detalle = detalle[0].ToString();

                                if (numero_pedido == numero_pedido_detalle)
                                {
                                    writer.WriteStartElement("Linea");
                                    writer.WriteAttributeString("id", detalle["ORDLIN"].ToString());
                                    writer.WriteElementString("codart", detalle["CODART"].ToString().Trim());
                                    writer.WriteElementString("artalias", detalle["ARTALIAS"].ToString());
                                    writer.WriteElementString("desclin", detalle["DESCLIN"].ToString());
                                    writer.WriteElementString("unidades", detalle["UNIDADES"].ToString());
                                    writer.WriteEndElement();
                                }
                            }

                            writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }

                    writer.WriteEndElement();
                    writer.Close();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                }
            }
        }

        public string exportarFicheroXMLSGIStreaming(string numdoc)
        {
            csSqlConnects sql = new csSqlConnects();
            string txt = string.Empty;
            DataTable dt = sql.cargarDatosTablaA3(string.Format("SELECT dbo.PROVINCI.NOMPROVI, " +
                                                    "       dbo.CABEALBV.NUMDOC, " +
                                                    "       dbo.CABEALBV.FECHA, " +
                                                    "       dbo.CABEALBV.CODCLI, " +
                                                    "       dbo.CABEALBV.NOMCLI, " +
                                                    "       dbo.CABEALBV.NIFCLI, " +
                                                    "       dbo.DIRENT.TELENT1 AS TELCLI, " +
                                                    "       dbo.CABEALBV.REFERENCIA, " +
                                                    "       dbo.LINEALBA.CODART, " +
                                                    "       dbo.ARTICULO.ARTALIAS, " +
                                                    "       dbo.LINEALBA.DESCLIN, " +
                                                    "       dbo.LINEALBA.UNIDADES, " +
                                                    "       dbo.LINEALBA.ORDLIN, " +
                                                    "       dbo.CABEALBV.SITUACION, " +
                                                    "       dbo.DIRENT.DIRENT1 AS DIRCLI, " +
                                                    "       dbo.DIRENT.DTOENT AS DTOCLI, " +
                                                    "       dbo.DIRENT.POBENT AS POBCLI, " +
                                                    "       dbo.ARTICULO.CODPRO " +
                                                    "FROM dbo.DIRENT " +
                                                    "INNER JOIN dbo.PROVINCI ON dbo.DIRENT.CODPROVI = dbo.PROVINCI.CODPROVI " +
                                                    "INNER JOIN dbo.LINEALBA " +
                                                    "INNER JOIN dbo.CABEALBV ON dbo.LINEALBA.IDALBV = dbo.CABEALBV.IDALBV " +
                                                    "INNER JOIN dbo.ARTICULO ON dbo.LINEALBA.CODART = dbo.ARTICULO.CODART ON dbo.DIRENT.IDDIRENT = dbo.CABEALBV.IDDIRENT " +
                                                    "  AND dbo.CABEALBV.NUMDOC = {0} " +
                                                    "ORDER BY dbo.CABEALBV.NUMDOC, " +
                                                    "         dbo.LINEALBA.ORDLIN", Convert.ToDouble(numdoc)));


            if (dt.hasRows())
            {
                try
                {
                    string numero_pedido = "";
                    string numero_pedido_detalle = "";
                    using (var sw = new StringWriter())
                    {
                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Indent = true;
                        settings.IndentChars = "  ";
                        settings.NewLineChars = "\r\n";
                        settings.NewLineHandling = NewLineHandling.Replace;

                        using (var xw = XmlWriter.Create(sw, settings))
                        {
                            xw.WriteStartElement("Pedidos");

                            foreach (DataRow row in dt.Rows)
                            {
                                if (numero_pedido != row[0].ToString())
                                {
                                    numero_pedido = row[0].ToString();
                                    xw.WriteStartElement("Pedido");
                                    xw.WriteElementString("numDoc", Convert.ToInt32(row["NUMDOC"]).ToString());
                                    xw.WriteElementString("fecha", row["FECHA"].ToString());
                                    xw.WriteElementString("codcli", row["CODCLI"].ToString().Trim());
                                    xw.WriteElementString("nomcli", row["NOMCLI"].ToString());
                                    xw.WriteElementString("nifcli", row["NIFCLI"].ToString());
                                    xw.WriteElementString("telcli", row["TELCLI"].ToString());
                                    xw.WriteElementString("referencia", row["REFERENCIA"].ToString());

                                    xw.WriteElementString("dirCli", row["DIRCLI"].ToString());
                                    xw.WriteElementString("dtoCli", row["DTOCLI"].ToString());
                                    xw.WriteElementString("pobCli", row["POBCLI"].ToString());
                                    xw.WriteElementString("codPro", row["CODPRO"].ToString());
                                    xw.WriteElementString("Provincia", row["NOMPROVI"].ToString().Trim());

                                    xw.WriteStartElement("Lineas");

                                    foreach (DataRow detalle in dt.Rows)
                                    {
                                        numero_pedido_detalle = detalle[0].ToString();

                                        if (numero_pedido == numero_pedido_detalle)
                                        {
                                            xw.WriteStartElement("Linea");
                                            xw.WriteAttributeString("id", detalle["ORDLIN"].ToString());
                                            xw.WriteElementString("codart", detalle["CODART"].ToString().Trim());
                                            xw.WriteElementString("artalias", detalle["ARTALIAS"].ToString());
                                            xw.WriteElementString("desclin", detalle["DESCLIN"].ToString());
                                            xw.WriteElementString("unidades", detalle["UNIDADES"].ToString());
                                            xw.WriteEndElement();
                                        }
                                    }

                                    xw.WriteEndElement();
                                    xw.WriteEndElement();
                                }
                            }

                            xw.WriteEndElement();
                            xw.Close();

                            return sw.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                }
            }

            return txt;
        }
    }
}
