﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Data;

namespace klsync
{

    //Definimos las variables globales que serán accesibles desde toda la aplicación
    public static class csGlobal
    {

        public const string ipKlosions = "83.43.139.240";

        public static string rutaFicConfig
        {
            get;

            set;
        }

        public static bool localConfig { get; set; }
        public static string cadenaConexionPS
        {
            get
            {
                return "SERVER=" + csGlobal.ServerPS + ";" + "DATABASE=" + csGlobal.databasePS.Replace("`", "") + ";" + ((csGlobal.PortPS != "") ? "PORT=" + csGlobal.PortPS + ";" : "") + "UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Convert Zero Datetime=True";
            }
            set
            {

            }

        }

        public static string cadenaConexion
        {
            get
            {
                SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
                csb.DataSource = csGlobal.ServerA3;
                csb.InitialCatalog = csGlobal.databaseA3;
                csb.IntegratedSecurity = csGlobal.integratedSecurity;
                csb.UserID = csGlobal.userA3;
                csb.Password = csGlobal.passwordA3;
                return csb.ConnectionString;

            }
            set
            {

            }
        }

        public static Dictionary<string, string> a3tiposIVA = new Dictionary<string, string>()
        {

        };

        public static Dictionary<string, string> a3RegimenesImpto = new Dictionary<string, string>()
        {

        };

        public static Dictionary<string, string> formas_pago
        {
            get;
            set;
        }
        public static Dictionary<string, string> transportistas
        {
            get;
            set;
        }


        //   Dictionary<string, string> dyTaxTypes = new Dictionary<string, string>();




        /// <summary>
        /// Defino el diccionario para guardar los régimenes de impuestos de A3ERP
        /// En csXMLconfig informo de los valores
        /// funcion obtenerRegimenImpuestos()
        /// </summary>
        public static Dictionary<string, string> regimenImpuesto = new Dictionary<string, string>()
        {
        };

        public static bool ps_visible_combinations
        {
            get;
            set;
        }

        public static string description_short
        {
            get;
            set;
        }

        public static string log
        {
            get;
            set;
        }

        public static DateTime expiration_date
        {
            get;
            set;
        }

        public static string description
        {
            get;
            set;
        }

        public static bool replicarImagenes
        {
            get;
            set;
        }

        public static bool ticketBaiActivo
        {
            get;
            set;
        }

        public static string ticketBaiFechaDesde
        {
            get;
            set;
        }

        public static string globalXML
        {
            get;
            set;
        }


        public static string comboFormValue
        {
            get;
            set;
        }

        public static string comboFormValueMember
        {
            get;
            set;
        }

        public static string horaFormValue
        {
            get;
            set;
        }

        public static string fechaFormValue
        {
            get;
            set;
        }

        public static int multiOptionFormValue
        {
            get;
            set;
        }

        public static bool perfilAdminEsync
        {
            get;
            set;
        }

        public static bool tieneTallas
        {
            get;
            set;
        }

        public static bool modeDebug
        {
            get;
            set;
        }

        public static int tipoContable
        {
            get;
            set;
        }

        public static bool moduloReferenciaPedidos
        {
            get;

            set;
        }

        public static string defaultLang
        {
            get;
            set;
        }

        public static string defaultIdLangPS
        {
            get;
            set;
        }


        public static string nombreServidor
        {
            get;
            set;

        }
        public static string rutaRemotaImagenes
        {
            get;
            set;

        }

        public static string modeAp
        {
            get;
            set;
        }

        public static bool ocultarArticuloSiStock0
        {
            get;
            set;
        }


        public static string modeExternalConnection
        {
            get;
            set;
        }


        public static string conexionDB
        {
            get;
            set;
        }
        public static string ServerPS
        {
            get;
            set;
        }

        public static string PortPS
        {
            get;
            set;
        }
        public static string databasePS
        {
            get;
            set;
        }

        public static string userPS
        {
            get;
            set;
        }

        public static string passwordPS
        {
            get;
            set;
        }


        public static string APIUrl
        {
            get;
            set;

        }
        public static string ServerA3
        {
            get;
            set;
        }

        public static string databaseA3
        {
            get;
            set;
        }

        public static string nombreEmpresaA3
        {

            get;
            set;
        }

        public static string userA3
        {
            get;
            set;
        }

        public static string passwordA3
        {
            get;
            set;
        }
        public static string userFTP
        {
            get;
            set;
        }
        public static string passwordFTP
        {
            get;
            set;
        }
        public static string versionPS
        {
            get;
            set;
        }
        public static string analitica
        {
            get;
            set;
        }
        public static string nivelesAnalitica
        {
            get;
            set;
        }
        public static string nivel1Analitica
        {
            get;
            set;
        }
        public static string nivel2Analitica
        {
            get;
            set;
        }
        public static string nivel3Analitica
        {
            get;
            set;
        }

        public static string nivelProyecto
        {
            get;
            set;
        }
        public static string docDestino
        {
            get;
            set;
        }
        public static string clienteGenerico
        {
            get;
            set;
        }
        public static string serie
        {
            get;
            set;
        }
        public static string rutaA3
        {
            get;
            set;
        }

        public static string rutaImagenes
        {
            get;
            set;
        }
        public static string rutaFTP
        {
            get;
            set;
        }


        public static string usarClienteGenerico
        {
            get;
            set;
        }

        public static bool preciosAtributosVariables
        {
            get;
            set;
        }


        public static string categoria
        {
            get;
            set;
        }
        public static string ecommerce
        {
            get;
            set;
        }
        public static string almacenA3
        {
            get;
            set;
        }
        public static string id_tax_group
        {
            get;
            set;
        }
        public static string ivaIncluido
        {
            get;
            set;
        }
        public static Dictionary<int, string> IdiomasEsync
        {
            get;
            set;
        }
        public static string webServiceKey
        {
            get;
            set;
        }
        public static string rutaFTPImagenes
        {
            get;
            set;

        }
        public static bool integratedSecurity
        {
            get;
            set;

        }
        public static string nodeTienda
        {
            get;
            set;

        }
        public static string nodeBackOffice
        {
            get;
            set;
        }
        public static string ModTxt
        {
            get;
            set;
        }

        public static string fpa3
        {
            get;
            set;
        }


        public static string ServirPedido
        {
            get;
            set;
        }

        public static string EmailEnvio
        {
            get;
            set;
        }

        public static string EmailDestino
        {
            get;
            set;
        }

        public static string EmailSMTP
        {
            get;
            set;
        }

        public static string UsuarioEmail
        {
            get;
            set;
        }

        public static string UsuarioPass
        {
            get;
            set;
        }

        public static string UserWS
        {
            get;
            set;
        }

        public static string PassWS
        {
            get;
            set;
        }

        public static string categoriaArbol
        {
            get;
            set;
        }

        public static string defCustomerGroup
        {
            get;
            set;
        }

        public static string usaProntoPago
        {
            get;
            set;
        }

        public static string estadosPedidosBloqueados
        {
            get;
            set;
        }

        public static bool actualizarDescripcionesArticulo
        {
            get;
            set;
        }

        public static bool actualizarPrecioArticulo
        {
            get;
            set;
        }

        public static string versionPSInstalada
        {
            get;
            set;
        }

        public static string modoTallasYColores
        {
            get;
            set;
        }

        public static string separadorImagen
        {
            get;
            set;
        }

        public static string formatCData(string texto)
        {
            texto = "<![CDATA[ " + texto + " ]]>";

            return texto;
        }

        public static string tipoEjecución
        {

            get;
            set;
        }

        public static string emailNotificacionCliente
        {

            get;
            set;
        }

        //Variable para indicar si estamos trabajando en modo manual o tarea programada
        public static bool modoManual
        {
            get;
            set;
        }

        public static string articuloRegalo
        {
            get;
            set;
        }

        //Variable para guardar la ruta de exportación por defecto de los ficheros 
        public static string fileExportPath
        {
            get;
            set;
        }

        public static string A3ERPConnection
        {
            get;
            set;
        }

        public static string activarMonotallas
        {
            get;
            set;
        }

        public enum tipoMetodo
        {
            POST,
            PUT
        }

        public static string customerPassword
        {
            get;
            set;
        }

        public static bool cupon_as_line
        {
            get;
            set;
        }

        public static string productCupon
        {
            get;
            set;
        }
        public static bool gestionPreciosHijos
        {
            get;
            set;
        }
        public static bool modoGestionCarteraRPST
        {
            get;
            set;

        }
        public static bool precioAtributos
        {
            get;
            set;

        }
        public static bool productosConPeso
        {
            get;
            set;

        }
        public static bool reminderPayment
        {
            get;
            set;

        }
        public static bool remindSendToCustomer
        {
            get;
            set;

        }
        public static string emailNotifyEmpresa
        {
            get;
            set;
        }
        public static string mailerHost
        {
            get;
            set;
        }
        public static string mailerFrom
        {
            get;
            set;
        }
        public static string mailerTo
        {
            get;
            set;
        }
        public static string mailerUserName
        {
            get;
            set;
        }
        public static string mailerPassword
        {
            get;
            set;
        }

        public static int reminderDaysBefore
        {
            get;
            set;
        }
        public static int reminderDaysAfter
        {
            get;
            set;
        }

        public static string reminderExportPath
        {
            get;
            set;
        }

        public static string codart_regalo
        {
            get;
            set;
        }

        /// <summary>
        /// Indicamos si es el valor es Y
        /// que en A3 pasamos el precio base de Prestashop y los descuentos por cliente
        /// </summary>
        public static bool a3_gestion_precio_dto
        {
            get;
            set;
        }

        public static int versionA3ERP
        {
            get;
            set;
        }
        public static DataTable dtProvinciasA3
        {
            get;
            set;
        }
        /// <summary>
        /// Variable para indicar si se traspasan los pedidos aunque haya articulos que no existan en A3. (Se ignorará las lineas de los articulos que no existan)
        /// </summary>
        public static bool gestionArticulosPedidos
        {
            get;
            set;
        }

        /// <summary>
        /// Variable para indicar si se usa el documento de compras de repasat como referencia en A3ERP
        /// </summary>
        public static bool usarDocProRefA3
        {
            get;
            set;
        }

        public static bool caracterCuota
        {
            get;
            set;
        }

        public static bool combinacionesVisibles
        {
            get;
            set;
        }

        public static bool medidasA3
        {
            get;
            set;
        }

        public static bool activarUnidadesNegocio
        {
            get;
            set;
        }
        public static string idUnidadNegocio
        {
            get;
            set;
        }

        public static bool sync_adv_mode {
            get;
            set;
        }

        public static string id_conexion_externa
        {
            get;
            set;
        }

        public static bool isODBCConnection
        {
            get;
            set;
        }

        public static bool isMySQLConnection
        {
            get;
            set;
        }

        public static bool isSqlServerConnection
        {
            get;
            set;
        }

        public static bool preciosA3
        {
            get;
            set;
        }
        public static bool FormaPagoA3
        {
            get;
            set;
        }
        public static bool DocPagoA3
        {
            get;
            set;
        }
        public static string familia_art{get; set; } //Para sincronizar con A3ERP 
        public static string subfamilia_art { get; set; } //Para sincronizar con A3ERP 
        public static string marcas_art { get; set; } //Para sincronizar con A3ERP 
        public static string filtro_art { get; set; } //Para filtrar la sincronizacion con A3ERP 
        public static string tipo_art { get; set; } //Para filtrar la sincronizacion con A3ERP 
        public static string filtro_fecha
        {
            get;
            set;
        }

    }

    public static class Extensions
    {
        public static bool ToBool(this string s)
        {
            return s == "0" ? false : true;
        }
    }

   
}
