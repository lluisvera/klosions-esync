﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data.Sql;
using a3ERPActiveX;



namespace klsync
{
    public partial class frSincronizar : Form
    {

        private static frSincronizar m_FormDefInstance;
        public static frSincronizar DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSincronizar();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frSincronizar()
        {
            InitializeComponent();
        }

        //Acciones al Abrir el formulario
        private void frSincronizar_Load(object sender, EventArgs e)
        {

            cbFormaPago.Visible = false;
            var now = DateTime.Now;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            dtpDesde.Value = startOfMonth;
            dtpHasta.Value = DateTime.Today;


        }

        private void conectarDB(string sqlScript,bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();
            
                MySqlDataAdapter MyDA = new MySqlDataAdapter();
                //MyDA.SelectCommand = new MySqlCommand("SELECT reference,payment,module,total_paid FROM `ps_orders`", conn);
                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);
                //MyDA.SelectCommand = new MySqlCommand("SELECT * FROM `ps_category_product`", conn);
                //MyDA.SelectCommand = new MySqlCommand("Show tables;", conn);
                //MyDA.SelectCommand = new MySqlCommand("SHOW COLUMNS FROM ps_orders;", conn);
                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {

                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }

                    }
                    numeroDocumentos = table.Rows.Count - contador;
                    ////MessageBox.Show("hay " + table.Rows.Count.ToString() + " filas en el grid");
                    ////MessageBox.Show("Hay " + numeroDocumentos);
                }
               

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }
        
        }


        //PARA BORRAR???
        private void btConnectSQL_Click(object sender, EventArgs e)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            ////MessageBox.Show("Conexión Establecida");
            dataConnection.Open();
            //SqlCommand dataCommand = new SqlCommand();
            //dataCommand.Connection = dataconnection;
            //dataCommand.CommandText = textBox1.Text;
            SqlDataAdapter a = new SqlDataAdapter("select * from cabefacv", dataConnection);
 
            DataTable t = new DataTable();
            a.Fill(t);
            dgvDocumentosPS.DataSource = t;


        }

        //Cargo los documentos de Prestashop en función de los parámetros
       private void btLoadSelection_Click(object sender, EventArgs e)
       {
           seleccionarDocsPS();
       }

       private void seleccionarDocsPS()
       {
            csSqlScripts sqlScript = new csSqlScripts();
            string formasPago = "";
            bool selection = false;
            bool pendientes = false;
            
           if (cboxFormasPago.Checked)
           {
               formasPago = cbFormaPago.Text;
           }

           if (rbQueryDocs.Checked)
           {
               selection = true;
           }

           if (rbDocsPtes.Checked)
           {
               pendientes = true;
           }
           
           conectarDB(sqlScript.selectDocsPresta(csGlobal.versionPS, selection, dtpDesde.Value.ToString("yy-MM-dd"), dtpHasta.Value.ToString("yy-MM-dd 23:59:59"), pendientes, formasPago), true, dgvDocumentosPS);
        }

       
       //Crear los documentos en A3ERP en función de la conexión seleccionada
        private void btCrearDocsA3_Click(object sender, EventArgs e)
        {
            if (dgvDocumentosPS.Rows.Count < 1)
            {
                //MessageBox.Show("No hay documentos para traspasar");
            }
            else
            {
               
                if (cboxTraspasarDocsA3.Checked)
                {
                    string[,] documentos = new string[dgvDocumentosPS.Rows.Count, 16];
                    //Repaso todas las líneas a traspasar
                    for (int i = 0; i < dgvDocumentosPS.Rows.Count; i++)
                    {
                        documentos[i, 0] = dgvDocumentosPS.Rows[i].Cells["invoice_date"].Value.ToString();
                        documentos[i, 1] = dgvDocumentosPS.Rows[i].Cells["dni"].Value.ToString();
                        documentos[i, 2] = dgvDocumentosPS.Rows[i].Cells["firstname"].Value.ToString();
                        documentos[i, 3] = dgvDocumentosPS.Rows[i].Cells["lastname"].Value.ToString();
                        documentos[i, 4] = dgvDocumentosPS.Rows[i].Cells["company"].Value.ToString();
                        documentos[i, 5] = dgvDocumentosPS.Rows[i].Cells["address1"].Value.ToString();
                        documentos[i, 6] = dgvDocumentosPS.Rows[i].Cells["product_reference"].Value.ToString();
                        documentos[i, 7] = dgvDocumentosPS.Rows[i].Cells["product_quantity"].Value.ToString();
                        documentos[i, 8] = dgvDocumentosPS.Rows[i].Cells["product_price"].Value.ToString();
                        documentos[i, 9] = dgvDocumentosPS.Rows[i].Cells["invoice_number"].Value.ToString();
                        documentos[i, 10] = dgvDocumentosPS.Rows[i].Cells["descuento"].Value.ToString();
                        documentos[i, 11] = dgvDocumentosPS.Rows[i].Cells["lastname"].Value.ToString();
                        documentos[i, 12] = dgvDocumentosPS.Rows[i].Cells["payment"].Value.ToString();
                        documentos[i, 13] = dgvDocumentosPS.Rows[i].Cells["postcode"].Value.ToString();
                        documentos[i, 14] = dgvDocumentosPS.Rows[i].Cells["city"].Value.ToString();
                        //documentos[i, 15] = dgvDocumentosPS.Rows[i].Cells["kls_a3erp_id"].Value.ToString();
                    }
                    csa3erp A3Func = new csa3erp();
                    A3Func.abrirEnlace();
                    A3Func.generaDocA3(documentos);
                    A3Func.cerrarEnlace();
                    seleccionarDocsPS();
                }
                else
                {
                    string[,] documentos = new string[dgvDocumentosPS.SelectedRows.Count, 16];
                   //documentos.resi = new string[dgvDocumentosPS.SelectedRows.Count, 16];
                    //Repaso todas las líneas a traspasar
                    for (int i = 0; i < dgvDocumentosPS.SelectedRows.Count; i++)
                    {
                        documentos[i, 0] = dgvDocumentosPS.SelectedRows[i].Cells["invoice_date"].Value.ToString();
                        documentos[i, 1] = dgvDocumentosPS.SelectedRows[i].Cells["dni"].Value.ToString();
                        documentos[i, 2] = dgvDocumentosPS.SelectedRows[i].Cells["firstname"].Value.ToString();
                        documentos[i, 3] = dgvDocumentosPS.SelectedRows[i].Cells["lastname"].Value.ToString();
                        documentos[i, 4] = dgvDocumentosPS.SelectedRows[i].Cells["company"].Value.ToString();
                        documentos[i, 5] = dgvDocumentosPS.SelectedRows[i].Cells["address1"].Value.ToString();
                        documentos[i, 6] = dgvDocumentosPS.SelectedRows[i].Cells["product_reference"].Value.ToString();
                        documentos[i, 7] = dgvDocumentosPS.SelectedRows[i].Cells["product_quantity"].Value.ToString();
                        documentos[i, 8] = dgvDocumentosPS.SelectedRows[i].Cells["product_price"].Value.ToString();
                        documentos[i, 9] = dgvDocumentosPS.SelectedRows[i].Cells["invoice_number"].Value.ToString();
                        documentos[i, 10] = dgvDocumentosPS.SelectedRows[i].Cells["descuento"].Value.ToString();
                        documentos[i, 11] = dgvDocumentosPS.SelectedRows[i].Cells["lastname"].Value.ToString();
                        documentos[i, 12] = dgvDocumentosPS.SelectedRows[i].Cells["payment"].Value.ToString();
                        documentos[i, 13] = dgvDocumentosPS.SelectedRows[i].Cells["postcode"].Value.ToString();
                        documentos[i, 14] = dgvDocumentosPS.SelectedRows[i].Cells["city"].Value.ToString();
                        //documentos[i, 15] = dgvDocumentosPS.Rows[i].Cells["kls_a3erp_id"].Value.ToString();
                    }
                    csa3erp A3Func = new csa3erp();
                    A3Func.abrirEnlace();
                    A3Func.generaDocA3(documentos);
                    A3Func.cerrarEnlace();
                    seleccionarDocsPS();
                
                
                }

               

            }

        }

        private void lbHasta_Click(object sender, EventArgs e)
        {
            dtpHasta.Value = dtpDesde.Value;
        }



        private void cboxFormasPago_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxFormasPago.Checked)
            {
                csMySqlConnect mySql=new csMySqlConnect();
                cbFormaPago.DataSource = mySql.select();
                cbFormaPago.Visible = true;
            }
            else
            {
                cbFormaPago.Visible = false;
            }
        }

        private void rbDocsPtes_CheckedChanged(object sender, EventArgs e)
        {
            mostrarBotonSync();

        }

    //Visualizo el botón únicamente si está marcada la opcion de pendientes
        private void mostrarBotonSync()
        {
            if (rbDocsPtes.Checked)
            {
                btCrearDocsA3.Visible = true;
            }
            else
            {
                btCrearDocsA3.Visible = false;
            }
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            csTaskProcess taskProcess = new csTaskProcess();
            taskProcess.CrearDocsA3();
        }

        private void btXMLOrdere_Click(object sender, EventArgs e)
        {
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScripts sqlScript=new csSqlScripts();
            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();
               
                MyDA.SelectCommand = new MySqlCommand(sqlScript.selectDocsPSTest(), conn);
               
                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvDocumentosPS.DataSource = bSource;

                
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }


        }

        private void borrarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            foreach (DataGridViewRow fila in dgvDocumentosPS.SelectedRows)
            {
                mySql.borrar("kls_invoice_erp", "invoice_number", fila.Cells["docsA3"].Value.ToString());
            }
            seleccionarDocsPS();
            
        }   

 
    }
}
