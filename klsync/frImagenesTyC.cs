﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync
{
    public partial class frImagenesTyC : Form
    {
        List<string> images = new List<string>();
        List<int> visibleItems = new List<int>();
        csSqlConnects sql = new csSqlConnects();

        private static frImagenesTyC m_FormDefInstance;
        public static frImagenesTyC DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frImagenesTyC();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frImagenesTyC()
        {
            InitializeComponent();
        }

        private void toolStripButtonCargarProductos_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable products = mysql.cargarTabla("select distinct id_product from ps_product_attribute");
        }
    }
}
