﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frRepasatArticulos : Form
    {
        private static frRepasatArticulos m_FormDefInstance;
        public static frRepasatArticulos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRepasatArticulos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frRepasatArticulos()
        {
            InitializeComponent();
        }




        private void cargarArticulosRPST()
        {
            csSqlScriptRepasat csScriptRPST = new csSqlScriptRepasat();
            conectarDB(csScriptRPST.selectArticulosRPST(), false, dgvArticulos);
        }

        private void conectarDB(string sqlScript, bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {

                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }

                    }
                    numeroDocumentos = table.Rows.Count - contador;

                }


            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }



        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = csGlobal.ServerA3;
            csb.InitialCatalog = csGlobal.databaseA3;
            csb.IntegratedSecurity = true;
            csb.UserID = csGlobal.userA3;
            csb.Password = csGlobal.passwordA3;
            return csb.ConnectionString;
        }

        private void cargarArticulosA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = CadenaConexion();

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectArticulosA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvArticulos.DataSource = t;


        }

        private void InsertarArticulos(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();

            foreach (DataGridViewRow fila in Filas)
            {
                mysqlconect.insertItems(scriptRepasat.insertArticulosRPST(fila.Cells["CODART"].Value.ToString(), fila.Cells["DESCART"].Value.ToString(), fila.Cells["TEXTO"].Value.ToString(),
                    fila.Cells["PRCVENTA"].Value.ToString()));

            }
        }


        private void BorrarArticulos(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();

            foreach (DataGridViewRow fila in Filas)
            {
                mysqlconect.insertItems(scriptRepasat.borrarArticulosRPST(fila.Cells["idArticulo"].Value.ToString()));

            }

        }



        private void seleccionarTodoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            dgvArticulos.SelectAll();
        }

        private void exportarARepasatToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            InsertarArticulos(dgvArticulos.SelectedRows);
        }

        private void borrarArtículosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BorrarArticulos(dgvArticulos.SelectedRows);
            cargarArticulosRPST();
        }

        private void btCargarArtRPST_Click(object sender, EventArgs e)
        {
            cargarArticulosRPST();
        }

        private void btCargarArtA3_Click(object sender, EventArgs e)
        {
            cargarArticulosA3();
        }



     
    }
}
