﻿namespace klsync
{
    partial class frVilardell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPagePedidos = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btLoadPedidos = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbutDocStatusPend = new System.Windows.Forms.RadioButton();
            this.rbutDocsStatusAll = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbutDocDirect = new System.Windows.Forms.RadioButton();
            this.rbutDocsTransfer = new System.Windows.Forms.RadioButton();
            this.rbutDocsAll = new System.Windows.Forms.RadioButton();
            this.btHideDocsPanel = new System.Windows.Forms.Button();
            this.btShowDocsPanel = new System.Windows.Forms.Button();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.cmsPedidos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eliminarPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarManualmenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripPedidos = new System.Windows.Forms.ToolStrip();
            this.btDownloadOfertas = new System.Windows.Forms.ToolStripButton();
            this.btDownloadPedidos = new System.Windows.Forms.ToolStripButton();
            this.tsbtAllDocs = new System.Windows.Forms.ToolStripButton();
            this.tabPageClientes = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnDeleteText = new System.Windows.Forms.Button();
            this.tbSearchText = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbutEmailNO = new System.Windows.Forms.RadioButton();
            this.rbutEmailSi = new System.Windows.Forms.RadioButton();
            this.rbutEmailAll = new System.Windows.Forms.RadioButton();
            this.btLoadCustomers = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbutPrestashop = new System.Windows.Forms.RadioButton();
            this.rbutAll = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbutCustomers = new System.Windows.Forms.RadioButton();
            this.rbutCustomerLead = new System.Windows.Forms.RadioButton();
            this.rbutCustomerAll = new System.Windows.Forms.RadioButton();
            this.btHide = new System.Windows.Forms.Button();
            this.btShow = new System.Windows.Forms.Button();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.ctexMenuClientes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarEnlaceClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subirAB2BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripClientes = new System.Windows.Forms.ToolStrip();
            this.btnSubir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnBajar = new System.Windows.Forms.ToolStripButton();
            this.tabPagePrecios = new System.Windows.Forms.TabPage();
            this.dgvPrecios = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonLoadPrecios = new System.Windows.Forms.ToolStripButton();
            this.tabAgentes = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.btAsignarAgente = new System.Windows.Forms.Button();
            this.btCargarClientesByAgente = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cboxAgentes = new System.Windows.Forms.ComboBox();
            this.rButPorAgente = new System.Windows.Forms.RadioButton();
            this.rbutTodosAgentes = new System.Windows.Forms.RadioButton();
            this.dgvAgentes = new System.Windows.Forms.DataGridView();
            this.ctexMenuAgentes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.syncronizarAsignacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabMayoristas = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.insertarMayorista = new System.Windows.Forms.Button();
            this.borrarMayorista = new System.Windows.Forms.Button();
            this.btnLoadMayoristas = new System.Windows.Forms.Button();
            this.dgvMayoristas = new System.Windows.Forms.DataGridView();
            this.statusStripAlpha = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelDash = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl1.SuspendLayout();
            this.tabPagePedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.cmsPedidos.SuspendLayout();
            this.toolStripPedidos.SuspendLayout();
            this.tabPageClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.ctexMenuClientes.SuspendLayout();
            this.toolStripClientes.SuspendLayout();
            this.tabPagePrecios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecios)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tabAgentes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgentes)).BeginInit();
            this.ctexMenuAgentes.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabMayoristas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMayoristas)).BeginInit();
            this.statusStripAlpha.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPagePedidos);
            this.tabControl1.Controls.Add(this.tabPageClientes);
            this.tabControl1.Controls.Add(this.tabPagePrecios);
            this.tabControl1.Controls.Add(this.tabAgentes);
            this.tabControl1.Controls.Add(this.tabMayoristas);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1276, 692);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPagePedidos
            // 
            this.tabPagePedidos.Controls.Add(this.splitContainer2);
            this.tabPagePedidos.Controls.Add(this.toolStripPedidos);
            this.tabPagePedidos.Location = new System.Drawing.Point(4, 28);
            this.tabPagePedidos.Margin = new System.Windows.Forms.Padding(5);
            this.tabPagePedidos.Name = "tabPagePedidos";
            this.tabPagePedidos.Padding = new System.Windows.Forms.Padding(5);
            this.tabPagePedidos.Size = new System.Drawing.Size(1268, 660);
            this.tabPagePedidos.TabIndex = 1;
            this.tabPagePedidos.Text = "Pedidos";
            this.tabPagePedidos.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(8, 68);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btLoadPedidos);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer2.Panel1.Controls.Add(this.btHideDocsPanel);
            this.splitContainer2.Panel1.Controls.Add(this.btShowDocsPanel);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvPedidos);
            this.splitContainer2.Size = new System.Drawing.Size(1252, 567);
            this.splitContainer2.SplitterDistance = 224;
            this.splitContainer2.TabIndex = 2;
            // 
            // btLoadPedidos
            // 
            this.btLoadPedidos.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadPedidos.Image = global::klsync.Properties.Resources.magnifying_glass;
            this.btLoadPedidos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadPedidos.Location = new System.Drawing.Point(20, 292);
            this.btLoadPedidos.Name = "btLoadPedidos";
            this.btLoadPedidos.Size = new System.Drawing.Size(185, 54);
            this.btLoadPedidos.TabIndex = 8;
            this.btLoadPedidos.Text = "Cargar Pedidos";
            this.btLoadPedidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btLoadPedidos.UseVisualStyleBackColor = true;
            this.btLoadPedidos.Click += new System.EventHandler(this.btLoadPedidos_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbutDocStatusPend);
            this.groupBox4.Controls.Add(this.rbutDocsStatusAll);
            this.groupBox4.Location = new System.Drawing.Point(17, 187);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(188, 90);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Situación";
            // 
            // rbutDocStatusPend
            // 
            this.rbutDocStatusPend.AutoSize = true;
            this.rbutDocStatusPend.Checked = true;
            this.rbutDocStatusPend.Location = new System.Drawing.Point(31, 58);
            this.rbutDocStatusPend.Name = "rbutDocStatusPend";
            this.rbutDocStatusPend.Size = new System.Drawing.Size(99, 23);
            this.rbutDocStatusPend.TabIndex = 1;
            this.rbutDocStatusPend.TabStop = true;
            this.rbutDocStatusPend.Text = "Pendientes";
            this.rbutDocStatusPend.UseVisualStyleBackColor = true;
            // 
            // rbutDocsStatusAll
            // 
            this.rbutDocsStatusAll.AutoSize = true;
            this.rbutDocsStatusAll.Location = new System.Drawing.Point(31, 29);
            this.rbutDocsStatusAll.Name = "rbutDocsStatusAll";
            this.rbutDocsStatusAll.Size = new System.Drawing.Size(65, 23);
            this.rbutDocsStatusAll.TabIndex = 0;
            this.rbutDocsStatusAll.Text = "Todos";
            this.rbutDocsStatusAll.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbutDocDirect);
            this.groupBox3.Controls.Add(this.rbutDocsTransfer);
            this.groupBox3.Controls.Add(this.rbutDocsAll);
            this.groupBox3.Location = new System.Drawing.Point(17, 49);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 117);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo Documento";
            // 
            // rbutDocDirect
            // 
            this.rbutDocDirect.AutoSize = true;
            this.rbutDocDirect.Location = new System.Drawing.Point(31, 87);
            this.rbutDocDirect.Name = "rbutDocDirect";
            this.rbutDocDirect.Size = new System.Drawing.Size(81, 23);
            this.rbutDocDirect.TabIndex = 2;
            this.rbutDocDirect.Text = "Directos";
            this.rbutDocDirect.UseVisualStyleBackColor = true;
            // 
            // rbutDocsTransfer
            // 
            this.rbutDocsTransfer.AutoSize = true;
            this.rbutDocsTransfer.Location = new System.Drawing.Point(31, 58);
            this.rbutDocsTransfer.Name = "rbutDocsTransfer";
            this.rbutDocsTransfer.Size = new System.Drawing.Size(80, 23);
            this.rbutDocsTransfer.TabIndex = 1;
            this.rbutDocsTransfer.Text = "Transfer";
            this.rbutDocsTransfer.UseVisualStyleBackColor = true;
            // 
            // rbutDocsAll
            // 
            this.rbutDocsAll.AutoSize = true;
            this.rbutDocsAll.Checked = true;
            this.rbutDocsAll.Location = new System.Drawing.Point(31, 29);
            this.rbutDocsAll.Name = "rbutDocsAll";
            this.rbutDocsAll.Size = new System.Drawing.Size(65, 23);
            this.rbutDocsAll.TabIndex = 0;
            this.rbutDocsAll.TabStop = true;
            this.rbutDocsAll.Text = "Todos";
            this.rbutDocsAll.UseVisualStyleBackColor = true;
            // 
            // btHideDocsPanel
            // 
            this.btHideDocsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHideDocsPanel.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHideDocsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHideDocsPanel.Location = new System.Drawing.Point(161, 3);
            this.btHideDocsPanel.Name = "btHideDocsPanel";
            this.btHideDocsPanel.Size = new System.Drawing.Size(27, 28);
            this.btHideDocsPanel.TabIndex = 5;
            this.btHideDocsPanel.UseVisualStyleBackColor = true;
            this.btHideDocsPanel.Click += new System.EventHandler(this.btHideDocsPanel_Click);
            // 
            // btShowDocsPanel
            // 
            this.btShowDocsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btShowDocsPanel.BackgroundImage = global::klsync.Properties.Resources.magnifying_glass;
            this.btShowDocsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btShowDocsPanel.Location = new System.Drawing.Point(194, 3);
            this.btShowDocsPanel.Name = "btShowDocsPanel";
            this.btShowDocsPanel.Size = new System.Drawing.Size(26, 28);
            this.btShowDocsPanel.TabIndex = 4;
            this.btShowDocsPanel.UseVisualStyleBackColor = true;
            this.btShowDocsPanel.Click += new System.EventHandler(this.btShowDocsPanel_Click);
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidos.ContextMenuStrip = this.cmsPedidos;
            this.dgvPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPedidos.Location = new System.Drawing.Point(0, 0);
            this.dgvPedidos.Margin = new System.Windows.Forms.Padding(5);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.ReadOnly = true;
            this.dgvPedidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPedidos.Size = new System.Drawing.Size(1024, 567);
            this.dgvPedidos.TabIndex = 1;
            this.dgvPedidos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvPedidos_KeyDown);
            // 
            // cmsPedidos
            // 
            this.cmsPedidos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsPedidos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eliminarPedidoToolStripMenuItem,
            this.sincronizarManualmenteToolStripMenuItem,
            this.borrarSincronizaciónToolStripMenuItem});
            this.cmsPedidos.Name = "cmsPedidos";
            this.cmsPedidos.Size = new System.Drawing.Size(259, 82);
            // 
            // eliminarPedidoToolStripMenuItem
            // 
            this.eliminarPedidoToolStripMenuItem.Name = "eliminarPedidoToolStripMenuItem";
            this.eliminarPedidoToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.eliminarPedidoToolStripMenuItem.Text = "Eliminar pedido";
            this.eliminarPedidoToolStripMenuItem.Click += new System.EventHandler(this.eliminarPedidoToolStripMenuItem_Click);
            // 
            // sincronizarManualmenteToolStripMenuItem
            // 
            this.sincronizarManualmenteToolStripMenuItem.Name = "sincronizarManualmenteToolStripMenuItem";
            this.sincronizarManualmenteToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.sincronizarManualmenteToolStripMenuItem.Text = "Sincronizar manualmente";
            this.sincronizarManualmenteToolStripMenuItem.Click += new System.EventHandler(this.sincronizarManualmenteToolStripMenuItem_Click);
            // 
            // borrarSincronizaciónToolStripMenuItem
            // 
            this.borrarSincronizaciónToolStripMenuItem.Name = "borrarSincronizaciónToolStripMenuItem";
            this.borrarSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.borrarSincronizaciónToolStripMenuItem.Text = "Borrar sincronización";
            this.borrarSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem_Click);
            // 
            // toolStripPedidos
            // 
            this.toolStripPedidos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripPedidos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripPedidos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btDownloadOfertas,
            this.btDownloadPedidos,
            this.tsbtAllDocs});
            this.toolStripPedidos.Location = new System.Drawing.Point(5, 5);
            this.toolStripPedidos.Name = "toolStripPedidos";
            this.toolStripPedidos.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripPedidos.Size = new System.Drawing.Size(1258, 60);
            this.toolStripPedidos.TabIndex = 0;
            this.toolStripPedidos.Text = "toolStrip1";
            // 
            // btDownloadOfertas
            // 
            this.btDownloadOfertas.Image = global::klsync.Properties.Resources.green_cloud_storage_download;
            this.btDownloadOfertas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btDownloadOfertas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDownloadOfertas.Name = "btDownloadOfertas";
            this.btDownloadOfertas.Size = new System.Drawing.Size(65, 57);
            this.btDownloadOfertas.Text = "Ofertas";
            this.btDownloadOfertas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDownloadOfertas.Click += new System.EventHandler(this.btDownloadOfertas_Click);
            // 
            // btDownloadPedidos
            // 
            this.btDownloadPedidos.Image = global::klsync.Properties.Resources.blue_cloud_storage_download;
            this.btDownloadPedidos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btDownloadPedidos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDownloadPedidos.Name = "btDownloadPedidos";
            this.btDownloadPedidos.Size = new System.Drawing.Size(68, 57);
            this.btDownloadPedidos.Text = "Pedidos";
            this.btDownloadPedidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btDownloadPedidos.Click += new System.EventHandler(this.btDownloadPedidos_Click);
            // 
            // tsbtAllDocs
            // 
            this.tsbtAllDocs.Image = global::klsync.adMan.Resources.download;
            this.tsbtAllDocs.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtAllDocs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtAllDocs.Name = "tsbtAllDocs";
            this.tsbtAllDocs.Size = new System.Drawing.Size(47, 57);
            this.tsbtAllDocs.Text = "Todo";
            this.tsbtAllDocs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtAllDocs.Click += new System.EventHandler(this.tsbtAllDocs_Click);
            // 
            // tabPageClientes
            // 
            this.tabPageClientes.Controls.Add(this.splitContainer1);
            this.tabPageClientes.Controls.Add(this.toolStripClientes);
            this.tabPageClientes.Location = new System.Drawing.Point(4, 28);
            this.tabPageClientes.Margin = new System.Windows.Forms.Padding(5);
            this.tabPageClientes.Name = "tabPageClientes";
            this.tabPageClientes.Size = new System.Drawing.Size(1268, 660);
            this.tabPageClientes.TabIndex = 2;
            this.tabPageClientes.Text = "Clientes";
            this.tabPageClientes.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 62);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox7);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox6);
            this.splitContainer1.Panel1.Controls.Add(this.btLoadCustomers);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.btHide);
            this.splitContainer1.Panel1.Controls.Add(this.btShow);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvClientes);
            this.splitContainer1.Size = new System.Drawing.Size(1268, 598);
            this.splitContainer1.SplitterDistance = 218;
            this.splitContainer1.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnDeleteText);
            this.groupBox7.Controls.Add(this.tbSearchText);
            this.groupBox7.Location = new System.Drawing.Point(10, 160);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(188, 62);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Texto";
            // 
            // btnDeleteText
            // 
            this.btnDeleteText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteText.BackgroundImage = global::klsync.Properties.Resources.delete;
            this.btnDeleteText.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteText.Location = new System.Drawing.Point(162, 2);
            this.btnDeleteText.Name = "btnDeleteText";
            this.btnDeleteText.Size = new System.Drawing.Size(20, 20);
            this.btnDeleteText.TabIndex = 10;
            this.btnDeleteText.UseVisualStyleBackColor = true;
            this.btnDeleteText.Click += new System.EventHandler(this.btnDeleteText_Click);
            // 
            // tbSearchText
            // 
            this.tbSearchText.Location = new System.Drawing.Point(6, 29);
            this.tbSearchText.Name = "tbSearchText";
            this.tbSearchText.Size = new System.Drawing.Size(176, 27);
            this.tbSearchText.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rbutEmailNO);
            this.groupBox6.Controls.Add(this.rbutEmailSi);
            this.groupBox6.Controls.Add(this.rbutEmailAll);
            this.groupBox6.Location = new System.Drawing.Point(10, 228);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(188, 62);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Email";
            // 
            // rbutEmailNO
            // 
            this.rbutEmailNO.AutoSize = true;
            this.rbutEmailNO.Location = new System.Drawing.Point(124, 26);
            this.rbutEmailNO.Name = "rbutEmailNO";
            this.rbutEmailNO.Size = new System.Drawing.Size(45, 23);
            this.rbutEmailNO.TabIndex = 2;
            this.rbutEmailNO.Text = "No";
            this.rbutEmailNO.UseVisualStyleBackColor = true;
            // 
            // rbutEmailSi
            // 
            this.rbutEmailSi.AutoSize = true;
            this.rbutEmailSi.Location = new System.Drawing.Point(80, 26);
            this.rbutEmailSi.Name = "rbutEmailSi";
            this.rbutEmailSi.Size = new System.Drawing.Size(38, 23);
            this.rbutEmailSi.TabIndex = 1;
            this.rbutEmailSi.Text = "Si";
            this.rbutEmailSi.UseVisualStyleBackColor = true;
            // 
            // rbutEmailAll
            // 
            this.rbutEmailAll.AutoSize = true;
            this.rbutEmailAll.Checked = true;
            this.rbutEmailAll.Location = new System.Drawing.Point(16, 26);
            this.rbutEmailAll.Name = "rbutEmailAll";
            this.rbutEmailAll.Size = new System.Drawing.Size(65, 23);
            this.rbutEmailAll.TabIndex = 0;
            this.rbutEmailAll.TabStop = true;
            this.rbutEmailAll.Text = "Todos";
            this.rbutEmailAll.UseVisualStyleBackColor = true;
            // 
            // btLoadCustomers
            // 
            this.btLoadCustomers.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadCustomers.Image = global::klsync.adMan.Resources.people;
            this.btLoadCustomers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadCustomers.Location = new System.Drawing.Point(10, 392);
            this.btLoadCustomers.Name = "btLoadCustomers";
            this.btLoadCustomers.Size = new System.Drawing.Size(190, 56);
            this.btLoadCustomers.TabIndex = 6;
            this.btLoadCustomers.Text = "&Cargar Clientes";
            this.btLoadCustomers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btLoadCustomers.UseVisualStyleBackColor = true;
            this.btLoadCustomers.Click += new System.EventHandler(this.btLoadCustomers_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbutPrestashop);
            this.groupBox2.Controls.Add(this.rbutAll);
            this.groupBox2.Location = new System.Drawing.Point(12, 296);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 90);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ecommerce";
            // 
            // rbutPrestashop
            // 
            this.rbutPrestashop.AutoSize = true;
            this.rbutPrestashop.Location = new System.Drawing.Point(31, 58);
            this.rbutPrestashop.Name = "rbutPrestashop";
            this.rbutPrestashop.Size = new System.Drawing.Size(119, 23);
            this.rbutPrestashop.TabIndex = 1;
            this.rbutPrestashop.Text = "En Prestashop";
            this.rbutPrestashop.UseVisualStyleBackColor = true;
            // 
            // rbutAll
            // 
            this.rbutAll.AutoSize = true;
            this.rbutAll.Checked = true;
            this.rbutAll.Location = new System.Drawing.Point(31, 29);
            this.rbutAll.Name = "rbutAll";
            this.rbutAll.Size = new System.Drawing.Size(65, 23);
            this.rbutAll.TabIndex = 0;
            this.rbutAll.TabStop = true;
            this.rbutAll.Text = "Todos";
            this.rbutAll.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbutCustomers);
            this.groupBox1.Controls.Add(this.rbutCustomerLead);
            this.groupBox1.Controls.Add(this.rbutCustomerAll);
            this.groupBox1.Location = new System.Drawing.Point(10, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 117);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo Clientes";
            // 
            // rbutCustomers
            // 
            this.rbutCustomers.AutoSize = true;
            this.rbutCustomers.Location = new System.Drawing.Point(31, 87);
            this.rbutCustomers.Name = "rbutCustomers";
            this.rbutCustomers.Size = new System.Drawing.Size(80, 23);
            this.rbutCustomers.TabIndex = 2;
            this.rbutCustomers.Text = "Clientes";
            this.rbutCustomers.UseVisualStyleBackColor = true;
            // 
            // rbutCustomerLead
            // 
            this.rbutCustomerLead.AutoSize = true;
            this.rbutCustomerLead.Location = new System.Drawing.Point(31, 58);
            this.rbutCustomerLead.Name = "rbutCustomerLead";
            this.rbutCustomerLead.Size = new System.Drawing.Size(102, 23);
            this.rbutCustomerLead.TabIndex = 1;
            this.rbutCustomerLead.Text = "Potenciales";
            this.rbutCustomerLead.UseVisualStyleBackColor = true;
            // 
            // rbutCustomerAll
            // 
            this.rbutCustomerAll.AutoSize = true;
            this.rbutCustomerAll.Checked = true;
            this.rbutCustomerAll.Location = new System.Drawing.Point(31, 29);
            this.rbutCustomerAll.Name = "rbutCustomerAll";
            this.rbutCustomerAll.Size = new System.Drawing.Size(65, 23);
            this.rbutCustomerAll.TabIndex = 0;
            this.rbutCustomerAll.TabStop = true;
            this.rbutCustomerAll.Text = "Todos";
            this.rbutCustomerAll.UseVisualStyleBackColor = true;
            // 
            // btHide
            // 
            this.btHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide.Location = new System.Drawing.Point(155, 3);
            this.btHide.Name = "btHide";
            this.btHide.Size = new System.Drawing.Size(27, 28);
            this.btHide.TabIndex = 3;
            this.btHide.UseVisualStyleBackColor = true;
            this.btHide.Click += new System.EventHandler(this.btHide_Click);
            // 
            // btShow
            // 
            this.btShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btShow.BackgroundImage = global::klsync.Properties.Resources.magnifying_glass;
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btShow.Location = new System.Drawing.Point(188, 3);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(26, 28);
            this.btShow.TabIndex = 2;
            this.btShow.UseVisualStyleBackColor = true;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // dgvClientes
            // 
            this.dgvClientes.AllowUserToAddRows = false;
            this.dgvClientes.AllowUserToDeleteRows = false;
            this.dgvClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvClientes.ContextMenuStrip = this.ctexMenuClientes;
            this.dgvClientes.Location = new System.Drawing.Point(0, 0);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.ReadOnly = true;
            this.dgvClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvClientes.Size = new System.Drawing.Size(1041, 573);
            this.dgvClientes.TabIndex = 1;
            // 
            // ctexMenuClientes
            // 
            this.ctexMenuClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarEnlaceClienteToolStripMenuItem,
            this.subirAB2BToolStripMenuItem});
            this.ctexMenuClientes.Name = "ctexMenuClientes";
            this.ctexMenuClientes.Size = new System.Drawing.Size(184, 48);
            // 
            // borrarEnlaceClienteToolStripMenuItem
            // 
            this.borrarEnlaceClienteToolStripMenuItem.Name = "borrarEnlaceClienteToolStripMenuItem";
            this.borrarEnlaceClienteToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.borrarEnlaceClienteToolStripMenuItem.Text = "Borrar Enlace Cliente";
            this.borrarEnlaceClienteToolStripMenuItem.Click += new System.EventHandler(this.borrarEnlaceClienteToolStripMenuItem_Click);
            // 
            // subirAB2BToolStripMenuItem
            // 
            this.subirAB2BToolStripMenuItem.Name = "subirAB2BToolStripMenuItem";
            this.subirAB2BToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.subirAB2BToolStripMenuItem.Text = "&Subir a B2B";
            this.subirAB2BToolStripMenuItem.Click += new System.EventHandler(this.subirAB2BToolStripMenuItem_Click);
            // 
            // toolStripClientes
            // 
            this.toolStripClientes.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripClientes.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSubir,
            this.toolStripSeparator1,
            this.btnBajar});
            this.toolStripClientes.Location = new System.Drawing.Point(0, 0);
            this.toolStripClientes.Name = "toolStripClientes";
            this.toolStripClientes.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripClientes.Size = new System.Drawing.Size(1268, 62);
            this.toolStripClientes.TabIndex = 0;
            this.toolStripClientes.Text = "toolStrip1";
            // 
            // btnSubir
            // 
            this.btnSubir.Enabled = false;
            this.btnSubir.Image = global::klsync.adMan.Resources.uploadPrices;
            this.btnSubir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSubir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSubir.Name = "btnSubir";
            this.btnSubir.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnSubir.Size = new System.Drawing.Size(74, 59);
            this.btnSubir.Text = "Subir";
            this.btnSubir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSubir.Click += new System.EventHandler(this.btnSubir_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 62);
            // 
            // btnBajar
            // 
            this.btnBajar.Image = global::klsync.adMan.Resources.download;
            this.btnBajar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnBajar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBajar.Name = "btnBajar";
            this.btnBajar.Size = new System.Drawing.Size(143, 59);
            this.btnBajar.Text = "Bajar pendientes";
            this.btnBajar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBajar.Click += new System.EventHandler(this.btnBajar_Click);
            // 
            // tabPagePrecios
            // 
            this.tabPagePrecios.Controls.Add(this.dgvPrecios);
            this.tabPagePrecios.Controls.Add(this.toolStrip1);
            this.tabPagePrecios.Location = new System.Drawing.Point(4, 28);
            this.tabPagePrecios.Margin = new System.Windows.Forms.Padding(5);
            this.tabPagePrecios.Name = "tabPagePrecios";
            this.tabPagePrecios.Size = new System.Drawing.Size(1268, 660);
            this.tabPagePrecios.TabIndex = 3;
            this.tabPagePrecios.Text = "Precios";
            this.tabPagePrecios.UseVisualStyleBackColor = true;
            // 
            // dgvPrecios
            // 
            this.dgvPrecios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPrecios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrecios.Location = new System.Drawing.Point(10, 64);
            this.dgvPrecios.Margin = new System.Windows.Forms.Padding(5);
            this.dgvPrecios.Name = "dgvPrecios";
            this.dgvPrecios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrecios.Size = new System.Drawing.Size(901, 424);
            this.dgvPrecios.TabIndex = 3;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonLoadPrecios});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(1268, 59);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonLoadPrecios
            // 
            this.toolStripButtonLoadPrecios.Font = new System.Drawing.Font("Calibri", 14.25F);
            this.toolStripButtonLoadPrecios.Image = global::klsync.Properties.Resources.Refresh;
            this.toolStripButtonLoadPrecios.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLoadPrecios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadPrecios.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripButtonLoadPrecios.Name = "toolStripButtonLoadPrecios";
            this.toolStripButtonLoadPrecios.Size = new System.Drawing.Size(65, 59);
            this.toolStripButtonLoadPrecios.Text = "Cargar";
            this.toolStripButtonLoadPrecios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tabAgentes
            // 
            this.tabAgentes.Controls.Add(this.splitContainer3);
            this.tabAgentes.Controls.Add(this.toolStrip2);
            this.tabAgentes.Location = new System.Drawing.Point(4, 28);
            this.tabAgentes.Name = "tabAgentes";
            this.tabAgentes.Padding = new System.Windows.Forms.Padding(3);
            this.tabAgentes.Size = new System.Drawing.Size(1268, 660);
            this.tabAgentes.TabIndex = 4;
            this.tabAgentes.Text = "Agentes";
            this.tabAgentes.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer3.Location = new System.Drawing.Point(6, 66);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.btAsignarAgente);
            this.splitContainer3.Panel1.Controls.Add(this.btCargarClientesByAgente);
            this.splitContainer3.Panel1.Controls.Add(this.groupBox5);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.dgvAgentes);
            this.splitContainer3.Size = new System.Drawing.Size(1259, 569);
            this.splitContainer3.SplitterDistance = 241;
            this.splitContainer3.TabIndex = 2;
            // 
            // btAsignarAgente
            // 
            this.btAsignarAgente.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAsignarAgente.Image = global::klsync.adMan.Resources.people;
            this.btAsignarAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAsignarAgente.Location = new System.Drawing.Point(19, 232);
            this.btAsignarAgente.Name = "btAsignarAgente";
            this.btAsignarAgente.Size = new System.Drawing.Size(190, 56);
            this.btAsignarAgente.TabIndex = 9;
            this.btAsignarAgente.Text = "Asignar Agente";
            this.btAsignarAgente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btAsignarAgente.UseVisualStyleBackColor = true;
            // 
            // btCargarClientesByAgente
            // 
            this.btCargarClientesByAgente.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCargarClientesByAgente.Image = global::klsync.adMan.Resources.people;
            this.btCargarClientesByAgente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCargarClientesByAgente.Location = new System.Drawing.Point(19, 170);
            this.btCargarClientesByAgente.Name = "btCargarClientesByAgente";
            this.btCargarClientesByAgente.Size = new System.Drawing.Size(190, 56);
            this.btCargarClientesByAgente.TabIndex = 8;
            this.btCargarClientesByAgente.Text = "Cargar Clientes";
            this.btCargarClientesByAgente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCargarClientesByAgente.UseVisualStyleBackColor = true;
            this.btCargarClientesByAgente.Click += new System.EventHandler(this.btCargarClientesByAgente_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cboxAgentes);
            this.groupBox5.Controls.Add(this.rButPorAgente);
            this.groupBox5.Controls.Add(this.rbutTodosAgentes);
            this.groupBox5.Location = new System.Drawing.Point(19, 15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(188, 149);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Selección";
            // 
            // cboxAgentes
            // 
            this.cboxAgentes.FormattingEnabled = true;
            this.cboxAgentes.Location = new System.Drawing.Point(50, 87);
            this.cboxAgentes.Name = "cboxAgentes";
            this.cboxAgentes.Size = new System.Drawing.Size(121, 27);
            this.cboxAgentes.TabIndex = 2;
            this.cboxAgentes.Visible = false;
            // 
            // rButPorAgente
            // 
            this.rButPorAgente.AutoSize = true;
            this.rButPorAgente.Location = new System.Drawing.Point(31, 58);
            this.rButPorAgente.Name = "rButPorAgente";
            this.rButPorAgente.Size = new System.Drawing.Size(98, 23);
            this.rButPorAgente.TabIndex = 1;
            this.rButPorAgente.Text = "Por Agente";
            this.rButPorAgente.UseVisualStyleBackColor = true;
            this.rButPorAgente.CheckedChanged += new System.EventHandler(this.rButPorAgente_CheckedChanged);
            // 
            // rbutTodosAgentes
            // 
            this.rbutTodosAgentes.AutoSize = true;
            this.rbutTodosAgentes.Checked = true;
            this.rbutTodosAgentes.Location = new System.Drawing.Point(31, 29);
            this.rbutTodosAgentes.Name = "rbutTodosAgentes";
            this.rbutTodosAgentes.Size = new System.Drawing.Size(65, 23);
            this.rbutTodosAgentes.TabIndex = 0;
            this.rbutTodosAgentes.TabStop = true;
            this.rbutTodosAgentes.Text = "Todos";
            this.rbutTodosAgentes.UseVisualStyleBackColor = true;
            this.rbutTodosAgentes.CheckedChanged += new System.EventHandler(this.rbutTodosAgentes_CheckedChanged);
            // 
            // dgvAgentes
            // 
            this.dgvAgentes.AllowUserToAddRows = false;
            this.dgvAgentes.AllowUserToDeleteRows = false;
            this.dgvAgentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAgentes.ContextMenuStrip = this.ctexMenuAgentes;
            this.dgvAgentes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgentes.Location = new System.Drawing.Point(0, 0);
            this.dgvAgentes.Name = "dgvAgentes";
            this.dgvAgentes.ReadOnly = true;
            this.dgvAgentes.Size = new System.Drawing.Size(1014, 569);
            this.dgvAgentes.TabIndex = 0;
            // 
            // ctexMenuAgentes
            // 
            this.ctexMenuAgentes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syncronizarAsignacionesToolStripMenuItem});
            this.ctexMenuAgentes.Name = "ctexMenuAgentes";
            this.ctexMenuAgentes.Size = new System.Drawing.Size(209, 26);
            // 
            // syncronizarAsignacionesToolStripMenuItem
            // 
            this.syncronizarAsignacionesToolStripMenuItem.Name = "syncronizarAsignacionesToolStripMenuItem";
            this.syncronizarAsignacionesToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.syncronizarAsignacionesToolStripMenuItem.Text = "Syncronizar Asignaciones";
            this.syncronizarAsignacionesToolStripMenuItem.Click += new System.EventHandler(this.syncronizarAsignacionesToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip2.Size = new System.Drawing.Size(1262, 60);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::klsync.adMan.Resources.syncA3ERP;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(138, 57);
            this.toolStripButton1.Text = "Asignar a Clientes";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tabMayoristas
            // 
            this.tabMayoristas.Controls.Add(this.splitContainer4);
            this.tabMayoristas.Location = new System.Drawing.Point(4, 28);
            this.tabMayoristas.Name = "tabMayoristas";
            this.tabMayoristas.Padding = new System.Windows.Forms.Padding(3);
            this.tabMayoristas.Size = new System.Drawing.Size(1268, 660);
            this.tabMayoristas.TabIndex = 5;
            this.tabMayoristas.Text = "Mayoristas";
            this.tabMayoristas.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(3, 3);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.insertarMayorista);
            this.splitContainer4.Panel1.Controls.Add(this.borrarMayorista);
            this.splitContainer4.Panel1.Controls.Add(this.btnLoadMayoristas);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.dgvMayoristas);
            this.splitContainer4.Size = new System.Drawing.Size(1262, 654);
            this.splitContainer4.SplitterDistance = 237;
            this.splitContainer4.TabIndex = 0;
            // 
            // insertarMayorista
            // 
            this.insertarMayorista.Location = new System.Drawing.Point(21, 126);
            this.insertarMayorista.Name = "insertarMayorista";
            this.insertarMayorista.Size = new System.Drawing.Size(201, 56);
            this.insertarMayorista.TabIndex = 10;
            this.insertarMayorista.Text = "Insertar/Actualizar";
            this.insertarMayorista.UseVisualStyleBackColor = true;
            this.insertarMayorista.Click += new System.EventHandler(this.insertarMayorista_Click);
            // 
            // borrarMayorista
            // 
            this.borrarMayorista.Location = new System.Drawing.Point(21, 200);
            this.borrarMayorista.Name = "borrarMayorista";
            this.borrarMayorista.Size = new System.Drawing.Size(201, 56);
            this.borrarMayorista.TabIndex = 1;
            this.borrarMayorista.Text = "Borrar Mayorista";
            this.borrarMayorista.UseVisualStyleBackColor = true;
            this.borrarMayorista.Click += new System.EventHandler(this.borrarMayorista_Click_1);
            // 
            // btnLoadMayoristas
            // 
            this.btnLoadMayoristas.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadMayoristas.Image = global::klsync.adMan.Resources.people;
            this.btnLoadMayoristas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoadMayoristas.Location = new System.Drawing.Point(21, 14);
            this.btnLoadMayoristas.Name = "btnLoadMayoristas";
            this.btnLoadMayoristas.Size = new System.Drawing.Size(201, 56);
            this.btnLoadMayoristas.TabIndex = 9;
            this.btnLoadMayoristas.Text = "Cargar Mayoristas";
            this.btnLoadMayoristas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLoadMayoristas.UseVisualStyleBackColor = true;
            this.btnLoadMayoristas.Click += new System.EventHandler(this.btnLoadMayoristas_Click);
            // 
            // dgvMayoristas
            // 
            this.dgvMayoristas.AllowUserToDeleteRows = false;
            this.dgvMayoristas.AllowUserToOrderColumns = true;
            this.dgvMayoristas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMayoristas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMayoristas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMayoristas.Location = new System.Drawing.Point(0, 0);
            this.dgvMayoristas.Name = "dgvMayoristas";
            this.dgvMayoristas.Size = new System.Drawing.Size(1021, 654);
            this.dgvMayoristas.TabIndex = 0;
            // 
            // statusStripAlpha
            // 
            this.statusStripAlpha.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStripAlpha.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelDash,
            this.tsslInfo,
            this.progressBar});
            this.statusStripAlpha.Location = new System.Drawing.Point(0, 666);
            this.statusStripAlpha.Name = "statusStripAlpha";
            this.statusStripAlpha.Padding = new System.Windows.Forms.Padding(2, 0, 23, 0);
            this.statusStripAlpha.Size = new System.Drawing.Size(1276, 26);
            this.statusStripAlpha.TabIndex = 1;
            this.statusStripAlpha.Text = "statusStrip1";
            // 
            // toolStripStatusLabelDash
            // 
            this.toolStripStatusLabelDash.Name = "toolStripStatusLabelDash";
            this.toolStripStatusLabelDash.Size = new System.Drawing.Size(1076, 21);
            this.toolStripStatusLabelDash.Spring = true;
            // 
            // tsslInfo
            // 
            this.tsslInfo.Name = "tsslInfo";
            this.tsslInfo.Size = new System.Drawing.Size(73, 21);
            this.tsslInfo.Text = "Progreso";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 20);
            // 
            // frVilardell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 692);
            this.Controls.Add(this.statusStripAlpha);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frVilardell";
            this.Text = " ";
            this.Load += new System.EventHandler(this.frVilardell_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPagePedidos.ResumeLayout(false);
            this.tabPagePedidos.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.cmsPedidos.ResumeLayout(false);
            this.toolStripPedidos.ResumeLayout(false);
            this.toolStripPedidos.PerformLayout();
            this.tabPageClientes.ResumeLayout(false);
            this.tabPageClientes.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.ctexMenuClientes.ResumeLayout(false);
            this.toolStripClientes.ResumeLayout(false);
            this.toolStripClientes.PerformLayout();
            this.tabPagePrecios.ResumeLayout(false);
            this.tabPagePrecios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecios)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabAgentes.ResumeLayout(false);
            this.tabAgentes.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgentes)).EndInit();
            this.ctexMenuAgentes.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabMayoristas.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMayoristas)).EndInit();
            this.statusStripAlpha.ResumeLayout(false);
            this.statusStripAlpha.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPagePedidos;
        private System.Windows.Forms.TabPage tabPageClientes;
        private System.Windows.Forms.ToolStrip toolStripPedidos;
        private System.Windows.Forms.ToolStrip toolStripClientes;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.TabPage tabPagePrecios;
        private System.Windows.Forms.DataGridView dgvPrecios;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadPrecios;
        private System.Windows.Forms.StatusStrip statusStripAlpha;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDash;
        private System.Windows.Forms.ToolStripStatusLabel tsslInfo;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.ContextMenuStrip cmsPedidos;
        private System.Windows.Forms.ToolStripMenuItem eliminarPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnSubir;
        private System.Windows.Forms.ToolStripButton btnBajar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btHide;
        private System.Windows.Forms.Button btShow;
        private System.Windows.Forms.Button btLoadCustomers;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbutPrestashop;
        private System.Windows.Forms.RadioButton rbutAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbutCustomers;
        private System.Windows.Forms.RadioButton rbutCustomerLead;
        private System.Windows.Forms.RadioButton rbutCustomerAll;
        private System.Windows.Forms.ToolStripButton btDownloadPedidos;
        private System.Windows.Forms.ToolStripButton btDownloadOfertas;
        private System.Windows.Forms.ContextMenuStrip ctexMenuClientes;
        private System.Windows.Forms.ToolStripMenuItem borrarEnlaceClienteToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbutDocStatusPend;
        private System.Windows.Forms.RadioButton rbutDocsStatusAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbutDocDirect;
        private System.Windows.Forms.RadioButton rbutDocsTransfer;
        private System.Windows.Forms.RadioButton rbutDocsAll;
        private System.Windows.Forms.Button btHideDocsPanel;
        private System.Windows.Forms.Button btShowDocsPanel;
        private System.Windows.Forms.Button btLoadPedidos;
        private System.Windows.Forms.ToolStripMenuItem sincronizarManualmenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.TabPage tabAgentes;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button btAsignarAgente;
        private System.Windows.Forms.Button btCargarClientesByAgente;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cboxAgentes;
        private System.Windows.Forms.RadioButton rButPorAgente;
        private System.Windows.Forms.RadioButton rbutTodosAgentes;
        private System.Windows.Forms.DataGridView dgvAgentes;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip ctexMenuAgentes;
        private System.Windows.Forms.ToolStripMenuItem syncronizarAsignacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbtAllDocs;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbutEmailSi;
        private System.Windows.Forms.RadioButton rbutEmailAll;
        private System.Windows.Forms.RadioButton rbutEmailNO;
        private System.Windows.Forms.ToolStripMenuItem subirAB2BToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnDeleteText;
        private System.Windows.Forms.TextBox tbSearchText;
        private System.Windows.Forms.TabPage tabMayoristas;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button btnLoadMayoristas;
        private System.Windows.Forms.DataGridView dgvMayoristas;
        private System.Windows.Forms.Button borrarMayorista;
        private System.Windows.Forms.Button insertarMayorista;
    }
}