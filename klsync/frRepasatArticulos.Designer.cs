﻿namespace klsync
{
    partial class frRepasatArticulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvArticulos = new System.Windows.Forms.DataGridView();
            this.menuContextual1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.seleccionarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarARepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarArtículosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btCargarArtRPST = new System.Windows.Forms.Button();
            this.btCargarArtA3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).BeginInit();
            this.menuContextual1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvArticulos
            // 
            this.dgvArticulos.AllowUserToAddRows = false;
            this.dgvArticulos.AllowUserToDeleteRows = false;
            this.dgvArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulos.ContextMenuStrip = this.menuContextual1;
            this.dgvArticulos.Location = new System.Drawing.Point(12, 86);
            this.dgvArticulos.Name = "dgvArticulos";
            this.dgvArticulos.ReadOnly = true;
            this.dgvArticulos.Size = new System.Drawing.Size(914, 408);
            this.dgvArticulos.TabIndex = 0;
            // 
            // menuContextual1
            // 
            this.menuContextual1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionarTodoToolStripMenuItem,
            this.exportarARepasatToolStripMenuItem,
            this.borrarArtículosToolStripMenuItem});
            this.menuContextual1.Name = "menuContextual1";
            this.menuContextual1.Size = new System.Drawing.Size(171, 70);
            // 
            // seleccionarTodoToolStripMenuItem
            // 
            this.seleccionarTodoToolStripMenuItem.Name = "seleccionarTodoToolStripMenuItem";
            this.seleccionarTodoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.seleccionarTodoToolStripMenuItem.Text = "Seleccionar &Todo";
            this.seleccionarTodoToolStripMenuItem.Click += new System.EventHandler(this.seleccionarTodoToolStripMenuItem_Click_1);
            // 
            // exportarARepasatToolStripMenuItem
            // 
            this.exportarARepasatToolStripMenuItem.Name = "exportarARepasatToolStripMenuItem";
            this.exportarARepasatToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.exportarARepasatToolStripMenuItem.Text = "&Exportar a Repasat";
            this.exportarARepasatToolStripMenuItem.Click += new System.EventHandler(this.exportarARepasatToolStripMenuItem_Click_1);
            // 
            // borrarArtículosToolStripMenuItem
            // 
            this.borrarArtículosToolStripMenuItem.Name = "borrarArtículosToolStripMenuItem";
            this.borrarArtículosToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.borrarArtículosToolStripMenuItem.Text = "&Borrar Artículos";
            this.borrarArtículosToolStripMenuItem.Click += new System.EventHandler(this.borrarArtículosToolStripMenuItem_Click);
            // 
            // btCargarArtRPST
            // 
            this.btCargarArtRPST.Location = new System.Drawing.Point(12, 57);
            this.btCargarArtRPST.Name = "btCargarArtRPST";
            this.btCargarArtRPST.Size = new System.Drawing.Size(124, 23);
            this.btCargarArtRPST.TabIndex = 1;
            this.btCargarArtRPST.Text = "Cargar Articulos RPST";
            this.btCargarArtRPST.UseVisualStyleBackColor = true;
            this.btCargarArtRPST.Click += new System.EventHandler(this.btCargarArtRPST_Click);
            // 
            // btCargarArtA3
            // 
            this.btCargarArtA3.Location = new System.Drawing.Point(142, 57);
            this.btCargarArtA3.Name = "btCargarArtA3";
            this.btCargarArtA3.Size = new System.Drawing.Size(124, 23);
            this.btCargarArtA3.TabIndex = 2;
            this.btCargarArtA3.Text = "Cargar Articulos A3";
            this.btCargarArtA3.UseVisualStyleBackColor = true;
            this.btCargarArtA3.Click += new System.EventHandler(this.btCargarArtA3_Click);
            // 
            // frRepasatArticulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(938, 506);
            this.Controls.Add(this.btCargarArtA3);
            this.Controls.Add(this.btCargarArtRPST);
            this.Controls.Add(this.dgvArticulos);
            this.Name = "frRepasatArticulos";
            this.Text = "frRepasatArticulos";
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulos)).EndInit();
            this.menuContextual1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvArticulos;
        private System.Windows.Forms.Button btCargarArtRPST;
        private System.Windows.Forms.Button btCargarArtA3;
        private System.Windows.Forms.ContextMenuStrip menuContextual1;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarARepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarArtículosToolStripMenuItem;
    }
}