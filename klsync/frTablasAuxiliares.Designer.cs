﻿namespace klsync
{
    partial class frTablasAuxiliares
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frTablasAuxiliares));
            this.splitContainerTabAux = new System.Windows.Forms.SplitContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButtonA3ERPtoPS = new System.Windows.Forms.ToolStripButton();
            this.dgvA3 = new System.Windows.Forms.DataGridView();
            this.cmsTablasAuxiliares = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.limpiarTransportistasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarFormasPagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonPStoA3ERP = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.dgvPS = new System.Windows.Forms.DataGridView();
            this.toolStripAuxiliaryTables = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSincronizar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonFabricantes = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonCategorias = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonCaracteristicas = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonLanguages = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAtributos = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolTransportistas = new System.Windows.Forms.ToolStripButton();
            this.toolFormasPago = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTabAux)).BeginInit();
            this.splitContainerTabAux.Panel1.SuspendLayout();
            this.splitContainerTabAux.Panel2.SuspendLayout();
            this.splitContainerTabAux.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).BeginInit();
            this.cmsTablasAuxiliares.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).BeginInit();
            this.toolStripAuxiliaryTables.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerTabAux
            // 
            this.splitContainerTabAux.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerTabAux.Location = new System.Drawing.Point(0, 105);
            this.splitContainerTabAux.Name = "splitContainerTabAux";
            // 
            // splitContainerTabAux.Panel1
            // 
            this.splitContainerTabAux.Panel1.Controls.Add(this.toolStrip1);
            this.splitContainerTabAux.Panel1.Controls.Add(this.dgvA3);
            // 
            // splitContainerTabAux.Panel2
            // 
            this.splitContainerTabAux.Panel2.Controls.Add(this.toolStrip2);
            this.splitContainerTabAux.Panel2.Controls.Add(this.dgvPS);
            this.splitContainerTabAux.Size = new System.Drawing.Size(1092, 461);
            this.splitContainerTabAux.SplitterDistance = 522;
            this.splitContainerTabAux.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripButtonA3ERPtoPS});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(5);
            this.toolStrip1.Size = new System.Drawing.Size(522, 38);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(56, 25);
            this.toolStripLabel1.Text = "A3ERP";
            // 
            // toolStripButtonA3ERPtoPS
            // 
            this.toolStripButtonA3ERPtoPS.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonA3ERPtoPS.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStripButtonA3ERPtoPS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonA3ERPtoPS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonA3ERPtoPS.Image")));
            this.toolStripButtonA3ERPtoPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonA3ERPtoPS.Name = "toolStripButtonA3ERPtoPS";
            this.toolStripButtonA3ERPtoPS.Size = new System.Drawing.Size(97, 25);
            this.toolStripButtonA3ERPtoPS.Text = "A3ERP > PS";
            this.toolStripButtonA3ERPtoPS.Click += new System.EventHandler(this.toolStripButtonA3ERPtoPS_Click);
            // 
            // dgvA3
            // 
            this.dgvA3.AllowUserToAddRows = false;
            this.dgvA3.AllowUserToDeleteRows = false;
            this.dgvA3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvA3.ContextMenuStrip = this.cmsTablasAuxiliares;
            this.dgvA3.Location = new System.Drawing.Point(12, 66);
            this.dgvA3.Name = "dgvA3";
            this.dgvA3.ReadOnly = true;
            this.dgvA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvA3.Size = new System.Drawing.Size(507, 383);
            this.dgvA3.TabIndex = 0;
            // 
            // cmsTablasAuxiliares
            // 
            this.cmsTablasAuxiliares.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsTablasAuxiliares.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.limpiarTransportistasToolStripMenuItem,
            this.limpiarFormasPagoToolStripMenuItem,
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem,
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem});
            this.cmsTablasAuxiliares.Name = "cmsTablasAuxiliares";
            this.cmsTablasAuxiliares.Size = new System.Drawing.Size(348, 130);
            // 
            // limpiarTransportistasToolStripMenuItem
            // 
            this.limpiarTransportistasToolStripMenuItem.Enabled = false;
            this.limpiarTransportistasToolStripMenuItem.Name = "limpiarTransportistasToolStripMenuItem";
            this.limpiarTransportistasToolStripMenuItem.Size = new System.Drawing.Size(347, 26);
            this.limpiarTransportistasToolStripMenuItem.Text = "Resetear valores transportistas";
            this.limpiarTransportistasToolStripMenuItem.Click += new System.EventHandler(this.limpiarTransportistasToolStripMenuItem_Click);
            // 
            // limpiarFormasPagoToolStripMenuItem
            // 
            this.limpiarFormasPagoToolStripMenuItem.Enabled = false;
            this.limpiarFormasPagoToolStripMenuItem.Name = "limpiarFormasPagoToolStripMenuItem";
            this.limpiarFormasPagoToolStripMenuItem.Size = new System.Drawing.Size(347, 26);
            this.limpiarFormasPagoToolStripMenuItem.Text = "Resetear valores Formas Pago";
            this.limpiarFormasPagoToolStripMenuItem.Click += new System.EventHandler(this.limpiarFormasPagoToolStripMenuItem_Click);
            // 
            // sincronizarArticulosCategoriasEnA3ToolStripMenuItem
            // 
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem.Name = "sincronizarArticulosCategoriasEnA3ToolStripMenuItem";
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem.Size = new System.Drawing.Size(347, 26);
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem.Text = "Sincronizar Articulos-Categorias en A3";
            this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem.Click += new System.EventHandler(this.sincronizarArticulosCategoriasEnA3ToolStripMenuItem_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPStoA3ERP,
            this.toolStripLabel2});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Padding = new System.Windows.Forms.Padding(5);
            this.toolStrip2.Size = new System.Drawing.Size(566, 38);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonPStoA3ERP
            // 
            this.toolStripButtonPStoA3ERP.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStripButtonPStoA3ERP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonPStoA3ERP.Enabled = false;
            this.toolStripButtonPStoA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPStoA3ERP.Image")));
            this.toolStripButtonPStoA3ERP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPStoA3ERP.Name = "toolStripButtonPStoA3ERP";
            this.toolStripButtonPStoA3ERP.Size = new System.Drawing.Size(97, 25);
            this.toolStripButtonPStoA3ERP.Text = "PS > A3ERP";
            this.toolStripButtonPStoA3ERP.Click += new System.EventHandler(this.toolStripButtonPStoA3ERP_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(104, 25);
            this.toolStripLabel2.Text = "PRESTASHOP";
            // 
            // dgvPS
            // 
            this.dgvPS.AllowUserToAddRows = false;
            this.dgvPS.AllowUserToDeleteRows = false;
            this.dgvPS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPS.Location = new System.Drawing.Point(3, 66);
            this.dgvPS.Name = "dgvPS";
            this.dgvPS.ReadOnly = true;
            this.dgvPS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPS.Size = new System.Drawing.Size(551, 383);
            this.dgvPS.TabIndex = 1;
            // 
            // toolStripAuxiliaryTables
            // 
            this.toolStripAuxiliaryTables.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripAuxiliaryTables.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripAuxiliaryTables.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSincronizar,
            this.toolStripSeparator7,
            this.toolStripSeparator6,
            this.toolStripSeparator4,
            this.toolStripButtonFabricantes,
            this.toolStripSeparator3,
            this.toolStripButtonCategorias,
            this.toolStripSeparator2,
            this.toolStripButtonCaracteristicas,
            this.toolStripSeparator1,
            this.toolStripButtonLanguages,
            this.toolStripSeparator5,
            this.toolStripButtonAtributos,
            this.toolStripButton1,
            this.toolStripProgressBar,
            this.toolTransportistas,
            this.toolFormasPago,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStripAuxiliaryTables.Location = new System.Drawing.Point(0, 0);
            this.toolStripAuxiliaryTables.Name = "toolStripAuxiliaryTables";
            this.toolStripAuxiliaryTables.Padding = new System.Windows.Forms.Padding(5);
            this.toolStripAuxiliaryTables.Size = new System.Drawing.Size(1092, 102);
            this.toolStripAuxiliaryTables.TabIndex = 1;
            this.toolStripAuxiliaryTables.Text = "toolStrip1";
            // 
            // toolStripButtonSincronizar
            // 
            this.toolStripButtonSincronizar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripButtonSincronizar.Enabled = false;
            this.toolStripButtonSincronizar.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButtonSincronizar.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSincronizar.Image")));
            this.toolStripButtonSincronizar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonSincronizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSincronizar.Name = "toolStripButtonSincronizar";
            this.toolStripButtonSincronizar.Size = new System.Drawing.Size(94, 89);
            this.toolStripButtonSincronizar.Text = "Sincronizar";
            this.toolStripButtonSincronizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonSincronizar.Click += new System.EventHandler(this.toolStripButtonSincronizar_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripButtonFabricantes
            // 
            this.toolStripButtonFabricantes.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonFabricantes.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFabricantes.Image")));
            this.toolStripButtonFabricantes.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonFabricantes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonFabricantes.Name = "toolStripButtonFabricantes";
            this.toolStripButtonFabricantes.Size = new System.Drawing.Size(156, 89);
            this.toolStripButtonFabricantes.Text = "Fabricantes / Marcas";
            this.toolStripButtonFabricantes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonFabricantes.Click += new System.EventHandler(this.toolStripButtonFabricantes_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripButtonCategorias
            // 
            this.toolStripButtonCategorias.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonCategorias.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCategorias.Image")));
            this.toolStripButtonCategorias.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCategorias.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCategorias.Name = "toolStripButtonCategorias";
            this.toolStripButtonCategorias.Size = new System.Drawing.Size(88, 89);
            this.toolStripButtonCategorias.Text = "Categorías";
            this.toolStripButtonCategorias.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonCategorias.Click += new System.EventHandler(this.toolStripButtonCategorias_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripButtonCaracteristicas
            // 
            this.toolStripButtonCaracteristicas.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonCaracteristicas.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCaracteristicas.Image")));
            this.toolStripButtonCaracteristicas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCaracteristicas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCaracteristicas.Name = "toolStripButtonCaracteristicas";
            this.toolStripButtonCaracteristicas.Size = new System.Drawing.Size(114, 89);
            this.toolStripButtonCaracteristicas.Text = "Características";
            this.toolStripButtonCaracteristicas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonCaracteristicas.Click += new System.EventHandler(this.toolStripButtonCaracteristicas_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 92);
            // 
            // toolStripButtonLanguages
            // 
            this.toolStripButtonLanguages.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonLanguages.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLanguages.Image")));
            this.toolStripButtonLanguages.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLanguages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLanguages.Name = "toolStripButtonLanguages";
            this.toolStripButtonLanguages.Size = new System.Drawing.Size(69, 89);
            this.toolStripButtonLanguages.Text = "Idiomas";
            this.toolStripButtonLanguages.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonLanguages.Click += new System.EventHandler(this.toolStripButtonLanguages_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 92);
            this.toolStripSeparator5.Visible = false;
            // 
            // toolStripButtonAtributos
            // 
            this.toolStripButtonAtributos.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonAtributos.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAtributos.Image")));
            this.toolStripButtonAtributos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonAtributos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAtributos.Name = "toolStripButtonAtributos";
            this.toolStripButtonAtributos.Size = new System.Drawing.Size(78, 89);
            this.toolStripButtonAtributos.Text = "Atributos";
            this.toolStripButtonAtributos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonAtributos.Click += new System.EventHandler(this.toolStripButtonAtributos_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(103, 89);
            this.toolStripButton1.Text = "Inicializar tyc";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 89);
            // 
            // toolTransportistas
            // 
            this.toolTransportistas.BackColor = System.Drawing.SystemColors.Control;
            this.toolTransportistas.Image = ((System.Drawing.Image)(resources.GetObject("toolTransportistas.Image")));
            this.toolTransportistas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolTransportistas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolTransportistas.Name = "toolTransportistas";
            this.toolTransportistas.Size = new System.Drawing.Size(111, 89);
            this.toolTransportistas.Text = "Transportistas";
            this.toolTransportistas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolTransportistas.Click += new System.EventHandler(this.toolTransportistas_Click);
            // 
            // toolFormasPago
            // 
            this.toolFormasPago.BackColor = System.Drawing.SystemColors.Control;
            this.toolFormasPago.Image = ((System.Drawing.Image)(resources.GetObject("toolFormasPago.Image")));
            this.toolFormasPago.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolFormasPago.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolFormasPago.Name = "toolFormasPago";
            this.toolFormasPago.Size = new System.Drawing.Size(104, 57);
            this.toolFormasPago.Text = "Formas Pago";
            this.toolFormasPago.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolFormasPago.Click += new System.EventHandler(this.toolFormasPago_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(157, 57);
            this.toolStripButton2.Text = "Sinc. Representantes";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(80, 57);
            this.toolStripButton3.Text = "Vaciar tyc";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.ToolTipText = "Vaciar tyc";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // sincronizarArticulosMarcasEnA3ToolStripMenuItem
            // 
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem.Name = "sincronizarArticulosMarcasEnA3ToolStripMenuItem";
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem.Size = new System.Drawing.Size(347, 26);
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem.Text = "Sincronizar Articulos-Marcas en A3";
            this.sincronizarArticulosMarcasEnA3ToolStripMenuItem.Click += new System.EventHandler(this.sincronizarArticulosMarcasEnA3ToolStripMenuItem_Click);
            // 
            // frTablasAuxiliares
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 566);
            this.Controls.Add(this.toolStripAuxiliaryTables);
            this.Controls.Add(this.splitContainerTabAux);
            this.Name = "frTablasAuxiliares";
            this.Text = "Tablas Auxiliares";
            this.Load += new System.EventHandler(this.frTablasAuxiliares_Load);
            this.splitContainerTabAux.Panel1.ResumeLayout(false);
            this.splitContainerTabAux.Panel1.PerformLayout();
            this.splitContainerTabAux.Panel2.ResumeLayout(false);
            this.splitContainerTabAux.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTabAux)).EndInit();
            this.splitContainerTabAux.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).EndInit();
            this.cmsTablasAuxiliares.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).EndInit();
            this.toolStripAuxiliaryTables.ResumeLayout(false);
            this.toolStripAuxiliaryTables.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerTabAux;
        private System.Windows.Forms.DataGridView dgvA3;
        private System.Windows.Forms.DataGridView dgvPS;
        private System.Windows.Forms.ToolStrip toolStripAuxiliaryTables;
        private System.Windows.Forms.ToolStripButton toolStripButtonFabricantes;
        private System.Windows.Forms.ToolStripButton toolStripButtonCategorias;
        private System.Windows.Forms.ToolStripButton toolStripButtonCaracteristicas;
        private System.Windows.Forms.ToolStripButton toolStripButtonAtributos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonSincronizar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButtonA3ERPtoPS;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButtonPStoA3ERP;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonLanguages;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripButton toolTransportistas;
        private System.Windows.Forms.ToolStripButton toolFormasPago;
        private System.Windows.Forms.ContextMenuStrip cmsTablasAuxiliares;
        private System.Windows.Forms.ToolStripMenuItem limpiarTransportistasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarFormasPagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripMenuItem sincronizarArticulosCategoriasEnA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarArticulosMarcasEnA3ToolStripMenuItem;
    }
}