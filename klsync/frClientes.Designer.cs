﻿namespace klsync
{
    partial class frClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frClientes));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextClientes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.actualizarCamposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.sincronizarIDsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.bloquearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desbloquearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activarClienteParaPrestashopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.asignarGrupoClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tscboxCustomerGroup = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.crearDireccionesEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.crearClienteEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarDireccionesFiscalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarFamiliaDeClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarManualmenteCodA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextDirPS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pasarCódigoAA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.crearDirecciónEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcarDirecciónFiscalEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ponerANULLIdDireccionPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rossoSincDirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextProviA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sincronizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextProviPS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sincronizarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripClientes = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonExportar = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.grBoxBloqueados = new System.Windows.Forms.GroupBox();
            this.rbBlockTodos = new System.Windows.Forms.RadioButton();
            this.rbBlockNo = new System.Windows.Forms.RadioButton();
            this.rbBlockSi = new System.Windows.Forms.RadioButton();
            this.grBoxSincronizar = new System.Windows.Forms.GroupBox();
            this.rbSincTodos = new System.Windows.Forms.RadioButton();
            this.rbSincNo = new System.Windows.Forms.RadioButton();
            this.rbSincSi = new System.Windows.Forms.RadioButton();
            this.grBoxEnPrestashop = new System.Windows.Forms.GroupBox();
            this.rbEnPSTodos = new System.Windows.Forms.RadioButton();
            this.rbEnPSNo = new System.Windows.Forms.RadioButton();
            this.rbEnPSSi = new System.Windows.Forms.RadioButton();
            this.btClearTextSearch = new System.Windows.Forms.Button();
            this.lbFilterby = new System.Windows.Forms.Label();
            this.cboxClientes = new System.Windows.Forms.ComboBox();
            this.rbLoadPS = new System.Windows.Forms.RadioButton();
            this.rbLoadA3ERP = new System.Windows.Forms.RadioButton();
            this.tbDefaultCustomerGroup = new System.Windows.Forms.TextBox();
            this.lbDefaultCustomers = new System.Windows.Forms.Label();
            this.btAdvancedSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btHide = new System.Windows.Forms.Button();
            this.btShow = new System.Windows.Forms.Button();
            this.splitContCustomers = new System.Windows.Forms.SplitContainer();
            this.tabClientes = new System.Windows.Forms.TabControl();
            this.tabCustomers = new System.Windows.Forms.TabPage();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.tabDirecciones = new System.Windows.Forms.TabControl();
            this.tabAddreses = new System.Windows.Forms.TabPage();
            this.dgvDirecciones = new System.Windows.Forms.DataGridView();
            this.statusStripClientes = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelBusquedaCtrlF = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelEspacioClientes = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelClientesContadorFilas = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.contextClientes.SuspendLayout();
            this.contextDirPS.SuspendLayout();
            this.contextProviA3.SuspendLayout();
            this.contextProviPS.SuspendLayout();
            this.toolStripClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grBoxBloqueados.SuspendLayout();
            this.grBoxSincronizar.SuspendLayout();
            this.grBoxEnPrestashop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContCustomers)).BeginInit();
            this.splitContCustomers.Panel1.SuspendLayout();
            this.splitContCustomers.Panel2.SuspendLayout();
            this.splitContCustomers.SuspendLayout();
            this.tabClientes.SuspendLayout();
            this.tabCustomers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.tabDirecciones.SuspendLayout();
            this.tabAddreses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDirecciones)).BeginInit();
            this.statusStripClientes.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextClientes
            // 
            this.contextClientes.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem,
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem,
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.actualizarCamposToolStripMenuItem,
            this.toolStripMenuItem4,
            this.sincronizarIDsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.bloquearClienteToolStripMenuItem,
            this.desbloquearClienteToolStripMenuItem,
            this.activarClienteParaPrestashopToolStripMenuItem,
            this.toolStripSeparator1,
            this.asignarGrupoClienteToolStripMenuItem,
            this.toolStripSeparator2,
            this.crearDireccionesEnPSToolStripMenuItem,
            this.toolStripMenuItem5,
            this.toolStripSeparator3,
            this.crearClienteEnA3ERPToolStripMenuItem,
            this.actualizarDireccionesFiscalesToolStripMenuItem,
            this.utilidadesToolStripMenuItem});
            this.contextClientes.Name = "contextClientes";
            this.contextClientes.Size = new System.Drawing.Size(337, 426);
            this.contextClientes.Opening += new System.ComponentModel.CancelEventHandler(this.contextClientes_Opening);
            // 
            // ponerANULLElKLSCODCLIENTEToolStripMenuItem
            // 
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem.Name = "ponerANULLElKLSCODCLIENTEToolStripMenuItem";
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem.Text = "Poner a NULL el KLS_COD_CLIENTE";
            this.ponerANULLElKLSCODCLIENTEToolStripMenuItem.Click += new System.EventHandler(this.ponerANULLElKLSCODCLIENTEToolStripMenuItem_Click);
            // 
            // pSPonerANULLCodClienteA3ERPToolStripMenuItem
            // 
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem.Name = "pSPonerANULLCodClienteA3ERPToolStripMenuItem";
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem.Text = "PS-Poner a NULL Cod. Cliente A3ERP";
            this.pSPonerANULLCodClienteA3ERPToolStripMenuItem.Click += new System.EventHandler(this.pSPonerANULLCodClienteA3ERPToolStripMenuItem_Click);
            // 
            // actualizarCodigoKlosionsTiendaToolStripMenuItem
            // 
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem.Name = "actualizarCodigoKlosionsTiendaToolStripMenuItem";
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem.Text = "Actualizar código klosions tienda";
            this.actualizarCodigoKlosionsTiendaToolStripMenuItem.Click += new System.EventHandler(this.actualizarCodigoKlosionsTiendaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(333, 6);
            // 
            // actualizarCamposToolStripMenuItem
            // 
            this.actualizarCamposToolStripMenuItem.Name = "actualizarCamposToolStripMenuItem";
            this.actualizarCamposToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.actualizarCamposToolStripMenuItem.Text = "Actualizar campos";
            this.actualizarCamposToolStripMenuItem.Click += new System.EventHandler(this.actualizarCamposToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(333, 6);
            // 
            // sincronizarIDsToolStripMenuItem
            // 
            this.sincronizarIDsToolStripMenuItem.Name = "sincronizarIDsToolStripMenuItem";
            this.sincronizarIDsToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.sincronizarIDsToolStripMenuItem.Text = "Sincronizar IDs por EMAIL";
            this.sincronizarIDsToolStripMenuItem.Click += new System.EventHandler(this.sincronizarIDsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(333, 6);
            // 
            // bloquearClienteToolStripMenuItem
            // 
            this.bloquearClienteToolStripMenuItem.Name = "bloquearClienteToolStripMenuItem";
            this.bloquearClienteToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.bloquearClienteToolStripMenuItem.Text = "Bloquear cliente";
            this.bloquearClienteToolStripMenuItem.Click += new System.EventHandler(this.bloquearClienteToolStripMenuItem_Click);
            // 
            // desbloquearClienteToolStripMenuItem
            // 
            this.desbloquearClienteToolStripMenuItem.Name = "desbloquearClienteToolStripMenuItem";
            this.desbloquearClienteToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.desbloquearClienteToolStripMenuItem.Text = "Desbloquear cliente";
            this.desbloquearClienteToolStripMenuItem.Click += new System.EventHandler(this.desbloquearClienteToolStripMenuItem_Click);
            // 
            // activarClienteParaPrestashopToolStripMenuItem
            // 
            this.activarClienteParaPrestashopToolStripMenuItem.Name = "activarClienteParaPrestashopToolStripMenuItem";
            this.activarClienteParaPrestashopToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.activarClienteParaPrestashopToolStripMenuItem.Text = "Activar Cliente para Prestashop";
            this.activarClienteParaPrestashopToolStripMenuItem.Click += new System.EventHandler(this.activarClienteParaPrestashopToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(333, 6);
            // 
            // asignarGrupoClienteToolStripMenuItem
            // 
            this.asignarGrupoClienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscboxCustomerGroup});
            this.asignarGrupoClienteToolStripMenuItem.Name = "asignarGrupoClienteToolStripMenuItem";
            this.asignarGrupoClienteToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.asignarGrupoClienteToolStripMenuItem.Text = "Asignar Grupo Cliente";
            this.asignarGrupoClienteToolStripMenuItem.MouseHover += new System.EventHandler(this.asignarGrupoClienteToolStripMenuItem_MouseHover);
            // 
            // tscboxCustomerGroup
            // 
            this.tscboxCustomerGroup.Name = "tscboxCustomerGroup";
            this.tscboxCustomerGroup.Size = new System.Drawing.Size(121, 23);
            this.tscboxCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.tscboxCustomerGroup_SelectedIndexChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(333, 6);
            // 
            // crearDireccionesEnPSToolStripMenuItem
            // 
            this.crearDireccionesEnPSToolStripMenuItem.Name = "crearDireccionesEnPSToolStripMenuItem";
            this.crearDireccionesEnPSToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.crearDireccionesEnPSToolStripMenuItem.Text = "Crear Direcciones en PS";
            this.crearDireccionesEnPSToolStripMenuItem.Click += new System.EventHandler(this.crearDireccionesEnPSToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(336, 26);
            this.toolStripMenuItem5.Text = "Actualizar id direcciones";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(333, 6);
            // 
            // crearClienteEnA3ERPToolStripMenuItem
            // 
            this.crearClienteEnA3ERPToolStripMenuItem.Name = "crearClienteEnA3ERPToolStripMenuItem";
            this.crearClienteEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.crearClienteEnA3ERPToolStripMenuItem.Text = "Crear Cliente en A3ERP";
            this.crearClienteEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.crearClienteEnA3ERPToolStripMenuItem_Click);
            // 
            // actualizarDireccionesFiscalesToolStripMenuItem
            // 
            this.actualizarDireccionesFiscalesToolStripMenuItem.Name = "actualizarDireccionesFiscalesToolStripMenuItem";
            this.actualizarDireccionesFiscalesToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.actualizarDireccionesFiscalesToolStripMenuItem.Text = "Actualizar direcciones fiscales en PS";
            this.actualizarDireccionesFiscalesToolStripMenuItem.Click += new System.EventHandler(this.actualizarDireccionesFiscalesToolStripMenuItem_Click);
            // 
            // utilidadesToolStripMenuItem
            // 
            this.utilidadesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem,
            this.actualizarFamiliaDeClientesToolStripMenuItem,
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem,
            this.actualizarManualmenteCodA3ERPToolStripMenuItem});
            this.utilidadesToolStripMenuItem.Name = "utilidadesToolStripMenuItem";
            this.utilidadesToolStripMenuItem.Size = new System.Drawing.Size(336, 26);
            this.utilidadesToolStripMenuItem.Text = "Utilidades";
            this.utilidadesToolStripMenuItem.Click += new System.EventHandler(this.utilidadesToolStripMenuItem_Click);
            // 
            // actualizarImportePedidoMínimoEnPSToolStripMenuItem
            // 
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem.Name = "actualizarImportePedidoMínimoEnPSToolStripMenuItem";
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem.Size = new System.Drawing.Size(412, 26);
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem.Text = "Actualizar Importe Pedido Minimo PS";
            this.actualizarImportePedidoMínimoEnPSToolStripMenuItem.Click += new System.EventHandler(this.actualizarImportePedidoMínimoEnPSToolStripMenuItem_Click);
            // 
            // actualizarFamiliaDeClientesToolStripMenuItem
            // 
            this.actualizarFamiliaDeClientesToolStripMenuItem.Name = "actualizarFamiliaDeClientesToolStripMenuItem";
            this.actualizarFamiliaDeClientesToolStripMenuItem.Size = new System.Drawing.Size(412, 26);
            this.actualizarFamiliaDeClientesToolStripMenuItem.Text = "Actualizar familia de precios especiales";
            this.actualizarFamiliaDeClientesToolStripMenuItem.Click += new System.EventHandler(this.actualizarFamiliaDeClientesToolStripMenuItem_Click);
            // 
            // actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem
            // 
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem.Name = "actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem";
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem.Size = new System.Drawing.Size(412, 26);
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem.Text = "Actualizar CodA3ERP si esta vacío y existe en A3";
            this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem.Click += new System.EventHandler(this.actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem_Click);
            // 
            // actualizarManualmenteCodA3ERPToolStripMenuItem
            // 
            this.actualizarManualmenteCodA3ERPToolStripMenuItem.Name = "actualizarManualmenteCodA3ERPToolStripMenuItem";
            this.actualizarManualmenteCodA3ERPToolStripMenuItem.Size = new System.Drawing.Size(412, 26);
            this.actualizarManualmenteCodA3ERPToolStripMenuItem.Text = "Actualizar manualmente CodA3ERP";
            this.actualizarManualmenteCodA3ERPToolStripMenuItem.Click += new System.EventHandler(this.actualizarManualmenteCodA3ERPToolStripMenuItem_Click);
            // 
            // contextDirPS
            // 
            this.contextDirPS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pasarCódigoAA3ToolStripMenuItem,
            this.toolStripMenuItem3,
            this.crearDirecciónEnPSToolStripMenuItem,
            this.marcarDirecciónFiscalEnPSToolStripMenuItem,
            this.ponerANULLIdDireccionPSToolStripMenuItem,
            this.rossoSincDirToolStripMenuItem});
            this.contextDirPS.Name = "contextDirPS";
            this.contextDirPS.Size = new System.Drawing.Size(280, 136);
            // 
            // pasarCódigoAA3ToolStripMenuItem
            // 
            this.pasarCódigoAA3ToolStripMenuItem.Enabled = false;
            this.pasarCódigoAA3ToolStripMenuItem.Name = "pasarCódigoAA3ToolStripMenuItem";
            this.pasarCódigoAA3ToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.pasarCódigoAA3ToolStripMenuItem.Text = "Pasar código a A3";
            this.pasarCódigoAA3ToolStripMenuItem.Click += new System.EventHandler(this.pasarCódigoAA3ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(279, 22);
            this.toolStripMenuItem3.Text = "Sincronizar Direccion";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // crearDirecciónEnPSToolStripMenuItem
            // 
            this.crearDirecciónEnPSToolStripMenuItem.Name = "crearDirecciónEnPSToolStripMenuItem";
            this.crearDirecciónEnPSToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.crearDirecciónEnPSToolStripMenuItem.Text = "Crear dirección en PS";
            this.crearDirecciónEnPSToolStripMenuItem.Click += new System.EventHandler(this.crearDirecciónEnPSToolStripMenuItem_Click);
            // 
            // marcarDirecciónFiscalEnPSToolStripMenuItem
            // 
            this.marcarDirecciónFiscalEnPSToolStripMenuItem.Name = "marcarDirecciónFiscalEnPSToolStripMenuItem";
            this.marcarDirecciónFiscalEnPSToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.marcarDirecciónFiscalEnPSToolStripMenuItem.Text = "Marcar Dirección Fiscal en PS";
            this.marcarDirecciónFiscalEnPSToolStripMenuItem.Click += new System.EventHandler(this.marcarDirecciónFiscalEnPSToolStripMenuItem_Click);
            // 
            // ponerANULLIdDireccionPSToolStripMenuItem
            // 
            this.ponerANULLIdDireccionPSToolStripMenuItem.Name = "ponerANULLIdDireccionPSToolStripMenuItem";
            this.ponerANULLIdDireccionPSToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.ponerANULLIdDireccionPSToolStripMenuItem.Text = "Poner a NULL Id Direccion PS";
            this.ponerANULLIdDireccionPSToolStripMenuItem.Click += new System.EventHandler(this.ponerANULLIdDireccionPSToolStripMenuItem_Click);
            // 
            // rossoSincDirToolStripMenuItem
            // 
            this.rossoSincDirToolStripMenuItem.Name = "rossoSincDirToolStripMenuItem";
            this.rossoSincDirToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.rossoSincDirToolStripMenuItem.Text = "Sinc Direcciones A3-PS (Una dirección)";
            this.rossoSincDirToolStripMenuItem.Click += new System.EventHandler(this.rossoSincDirToolStripMenuItem_Click);
            // 
            // contextProviA3
            // 
            this.contextProviA3.Enabled = false;
            this.contextProviA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarToolStripMenuItem});
            this.contextProviA3.Name = "contextProviA3";
            this.contextProviA3.Size = new System.Drawing.Size(133, 26);
            this.contextProviA3.Text = "A3";
            // 
            // sincronizarToolStripMenuItem
            // 
            this.sincronizarToolStripMenuItem.Enabled = false;
            this.sincronizarToolStripMenuItem.Name = "sincronizarToolStripMenuItem";
            this.sincronizarToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.sincronizarToolStripMenuItem.Text = "Sincronizar";
            // 
            // contextProviPS
            // 
            this.contextProviPS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarToolStripMenuItem1});
            this.contextProviPS.Name = "contextProviA3";
            this.contextProviPS.Size = new System.Drawing.Size(133, 26);
            this.contextProviPS.Text = "PS";
            this.contextProviPS.Opening += new System.ComponentModel.CancelEventHandler(this.contextProviPS_Opening);
            // 
            // sincronizarToolStripMenuItem1
            // 
            this.sincronizarToolStripMenuItem1.Name = "sincronizarToolStripMenuItem1";
            this.sincronizarToolStripMenuItem1.Size = new System.Drawing.Size(132, 22);
            this.sincronizarToolStripMenuItem1.Text = "Sincronizar";
            // 
            // toolStripClientes
            // 
            this.toolStripClientes.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonExportar});
            this.toolStripClientes.Location = new System.Drawing.Point(0, 0);
            this.toolStripClientes.Name = "toolStripClientes";
            this.toolStripClientes.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripClientes.Size = new System.Drawing.Size(1215, 60);
            this.toolStripClientes.TabIndex = 11;
            this.toolStripClientes.Text = "toolStrip1";
            // 
            // toolStripButtonExportar
            // 
            this.toolStripButtonExportar.Image = global::klsync.adMan.Resources.paper11;
            this.toolStripButtonExportar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExportar.Name = "toolStripButtonExportar";
            this.toolStripButtonExportar.Size = new System.Drawing.Size(103, 57);
            this.toolStripButtonExportar.Text = "Clientes a PS";
            this.toolStripButtonExportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonExportar.ToolTipText = "Exportar clientes seleccionados";
            this.toolStripButtonExportar.Click += new System.EventHandler(this.toolStripButtonExportar_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 60);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxBloqueados);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxSincronizar);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxEnPrestashop);
            this.splitContainer1.Panel1.Controls.Add(this.btClearTextSearch);
            this.splitContainer1.Panel1.Controls.Add(this.lbFilterby);
            this.splitContainer1.Panel1.Controls.Add(this.cboxClientes);
            this.splitContainer1.Panel1.Controls.Add(this.rbLoadPS);
            this.splitContainer1.Panel1.Controls.Add(this.rbLoadA3ERP);
            this.splitContainer1.Panel1.Controls.Add(this.tbDefaultCustomerGroup);
            this.splitContainer1.Panel1.Controls.Add(this.lbDefaultCustomers);
            this.splitContainer1.Panel1.Controls.Add(this.btAdvancedSearch);
            this.splitContainer1.Panel1.Controls.Add(this.tbSearch);
            this.splitContainer1.Panel1.Controls.Add(this.btHide);
            this.splitContainer1.Panel1.Controls.Add(this.btShow);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContCustomers);
            this.splitContainer1.Size = new System.Drawing.Size(1215, 586);
            this.splitContainer1.SplitterDistance = 275;
            this.splitContainer1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Buscar por:";
            // 
            // grBoxBloqueados
            // 
            this.grBoxBloqueados.Controls.Add(this.rbBlockTodos);
            this.grBoxBloqueados.Controls.Add(this.rbBlockNo);
            this.grBoxBloqueados.Controls.Add(this.rbBlockSi);
            this.grBoxBloqueados.Location = new System.Drawing.Point(47, 243);
            this.grBoxBloqueados.Name = "grBoxBloqueados";
            this.grBoxBloqueados.Size = new System.Drawing.Size(188, 50);
            this.grBoxBloqueados.TabIndex = 15;
            this.grBoxBloqueados.TabStop = false;
            this.grBoxBloqueados.Text = "Estado Bloqueo";
            // 
            // rbBlockTodos
            // 
            this.rbBlockTodos.AutoSize = true;
            this.rbBlockTodos.Checked = true;
            this.rbBlockTodos.Location = new System.Drawing.Point(99, 19);
            this.rbBlockTodos.Name = "rbBlockTodos";
            this.rbBlockTodos.Size = new System.Drawing.Size(55, 17);
            this.rbBlockTodos.TabIndex = 2;
            this.rbBlockTodos.TabStop = true;
            this.rbBlockTodos.Text = "Todos";
            this.rbBlockTodos.UseVisualStyleBackColor = true;
            // 
            // rbBlockNo
            // 
            this.rbBlockNo.AutoSize = true;
            this.rbBlockNo.Location = new System.Drawing.Point(58, 19);
            this.rbBlockNo.Name = "rbBlockNo";
            this.rbBlockNo.Size = new System.Drawing.Size(39, 17);
            this.rbBlockNo.TabIndex = 1;
            this.rbBlockNo.Text = "No";
            this.rbBlockNo.UseVisualStyleBackColor = true;
            // 
            // rbBlockSi
            // 
            this.rbBlockSi.AutoSize = true;
            this.rbBlockSi.Location = new System.Drawing.Point(17, 19);
            this.rbBlockSi.Name = "rbBlockSi";
            this.rbBlockSi.Size = new System.Drawing.Size(34, 17);
            this.rbBlockSi.TabIndex = 0;
            this.rbBlockSi.Text = "Si";
            this.rbBlockSi.UseVisualStyleBackColor = true;
            // 
            // grBoxSincronizar
            // 
            this.grBoxSincronizar.Controls.Add(this.rbSincTodos);
            this.grBoxSincronizar.Controls.Add(this.rbSincNo);
            this.grBoxSincronizar.Controls.Add(this.rbSincSi);
            this.grBoxSincronizar.Location = new System.Drawing.Point(47, 187);
            this.grBoxSincronizar.Name = "grBoxSincronizar";
            this.grBoxSincronizar.Size = new System.Drawing.Size(188, 50);
            this.grBoxSincronizar.TabIndex = 14;
            this.grBoxSincronizar.TabStop = false;
            this.grBoxSincronizar.Text = "Sincronizar";
            // 
            // rbSincTodos
            // 
            this.rbSincTodos.AutoSize = true;
            this.rbSincTodos.Checked = true;
            this.rbSincTodos.Location = new System.Drawing.Point(99, 19);
            this.rbSincTodos.Name = "rbSincTodos";
            this.rbSincTodos.Size = new System.Drawing.Size(55, 17);
            this.rbSincTodos.TabIndex = 2;
            this.rbSincTodos.TabStop = true;
            this.rbSincTodos.Text = "Todos";
            this.rbSincTodos.UseVisualStyleBackColor = true;
            // 
            // rbSincNo
            // 
            this.rbSincNo.AutoSize = true;
            this.rbSincNo.Location = new System.Drawing.Point(58, 19);
            this.rbSincNo.Name = "rbSincNo";
            this.rbSincNo.Size = new System.Drawing.Size(39, 17);
            this.rbSincNo.TabIndex = 1;
            this.rbSincNo.Text = "No";
            this.rbSincNo.UseVisualStyleBackColor = true;
            // 
            // rbSincSi
            // 
            this.rbSincSi.AutoSize = true;
            this.rbSincSi.Location = new System.Drawing.Point(17, 19);
            this.rbSincSi.Name = "rbSincSi";
            this.rbSincSi.Size = new System.Drawing.Size(34, 17);
            this.rbSincSi.TabIndex = 0;
            this.rbSincSi.Text = "Si";
            this.rbSincSi.UseVisualStyleBackColor = true;
            // 
            // grBoxEnPrestashop
            // 
            this.grBoxEnPrestashop.Controls.Add(this.rbEnPSTodos);
            this.grBoxEnPrestashop.Controls.Add(this.rbEnPSNo);
            this.grBoxEnPrestashop.Controls.Add(this.rbEnPSSi);
            this.grBoxEnPrestashop.Location = new System.Drawing.Point(47, 130);
            this.grBoxEnPrestashop.Name = "grBoxEnPrestashop";
            this.grBoxEnPrestashop.Size = new System.Drawing.Size(188, 50);
            this.grBoxEnPrestashop.TabIndex = 13;
            this.grBoxEnPrestashop.TabStop = false;
            this.grBoxEnPrestashop.Text = "En Prestashop";
            // 
            // rbEnPSTodos
            // 
            this.rbEnPSTodos.AutoSize = true;
            this.rbEnPSTodos.Checked = true;
            this.rbEnPSTodos.Location = new System.Drawing.Point(99, 19);
            this.rbEnPSTodos.Name = "rbEnPSTodos";
            this.rbEnPSTodos.Size = new System.Drawing.Size(55, 17);
            this.rbEnPSTodos.TabIndex = 2;
            this.rbEnPSTodos.TabStop = true;
            this.rbEnPSTodos.Text = "Todos";
            this.rbEnPSTodos.UseVisualStyleBackColor = true;
            // 
            // rbEnPSNo
            // 
            this.rbEnPSNo.AutoSize = true;
            this.rbEnPSNo.Location = new System.Drawing.Point(58, 19);
            this.rbEnPSNo.Name = "rbEnPSNo";
            this.rbEnPSNo.Size = new System.Drawing.Size(39, 17);
            this.rbEnPSNo.TabIndex = 1;
            this.rbEnPSNo.Text = "No";
            this.rbEnPSNo.UseVisualStyleBackColor = true;
            // 
            // rbEnPSSi
            // 
            this.rbEnPSSi.AutoSize = true;
            this.rbEnPSSi.Location = new System.Drawing.Point(17, 19);
            this.rbEnPSSi.Name = "rbEnPSSi";
            this.rbEnPSSi.Size = new System.Drawing.Size(34, 17);
            this.rbEnPSSi.TabIndex = 0;
            this.rbEnPSSi.Text = "Si";
            this.rbEnPSSi.UseVisualStyleBackColor = true;
            // 
            // btClearTextSearch
            // 
            this.btClearTextSearch.BackgroundImage = global::klsync.adMan.Resources.borrar;
            this.btClearTextSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btClearTextSearch.Location = new System.Drawing.Point(182, 48);
            this.btClearTextSearch.Name = "btClearTextSearch";
            this.btClearTextSearch.Size = new System.Drawing.Size(18, 17);
            this.btClearTextSearch.TabIndex = 12;
            this.btClearTextSearch.UseVisualStyleBackColor = true;
            this.btClearTextSearch.Click += new System.EventHandler(this.btClearTextSearch_Click);
            // 
            // lbFilterby
            // 
            this.lbFilterby.AutoSize = true;
            this.lbFilterby.Location = new System.Drawing.Point(47, 296);
            this.lbFilterby.Name = "lbFilterby";
            this.lbFilterby.Size = new System.Drawing.Size(50, 13);
            this.lbFilterby.TabIndex = 11;
            this.lbFilterby.Text = "Filtrar por";
            // 
            // cboxClientes
            // 
            this.cboxClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxClientes.FormattingEnabled = true;
            this.cboxClientes.Items.AddRange(new object[] {
            "",
            "Repetidos",
            "Mal formateados",
            "Id incorrectas"});
            this.cboxClientes.Location = new System.Drawing.Point(47, 312);
            this.cboxClientes.Name = "cboxClientes";
            this.cboxClientes.Size = new System.Drawing.Size(153, 28);
            this.cboxClientes.TabIndex = 10;
            // 
            // rbLoadPS
            // 
            this.rbLoadPS.AutoSize = true;
            this.rbLoadPS.Location = new System.Drawing.Point(120, 98);
            this.rbLoadPS.Name = "rbLoadPS";
            this.rbLoadPS.Size = new System.Drawing.Size(39, 17);
            this.rbLoadPS.TabIndex = 9;
            this.rbLoadPS.Text = "PS";
            this.rbLoadPS.UseVisualStyleBackColor = true;
            // 
            // rbLoadA3ERP
            // 
            this.rbLoadA3ERP.AutoSize = true;
            this.rbLoadA3ERP.Checked = true;
            this.rbLoadA3ERP.Location = new System.Drawing.Point(47, 98);
            this.rbLoadA3ERP.Name = "rbLoadA3ERP";
            this.rbLoadA3ERP.Size = new System.Drawing.Size(60, 17);
            this.rbLoadA3ERP.TabIndex = 8;
            this.rbLoadA3ERP.TabStop = true;
            this.rbLoadA3ERP.Text = "A3ERP";
            this.rbLoadA3ERP.UseVisualStyleBackColor = true;
            this.rbLoadA3ERP.CheckedChanged += new System.EventHandler(this.rbLoadA3ERP_CheckedChanged);
            // 
            // tbDefaultCustomerGroup
            // 
            this.tbDefaultCustomerGroup.Enabled = false;
            this.tbDefaultCustomerGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDefaultCustomerGroup.Location = new System.Drawing.Point(47, 369);
            this.tbDefaultCustomerGroup.Name = "tbDefaultCustomerGroup";
            this.tbDefaultCustomerGroup.Size = new System.Drawing.Size(188, 26);
            this.tbDefaultCustomerGroup.TabIndex = 7;
            // 
            // lbDefaultCustomers
            // 
            this.lbDefaultCustomers.AutoSize = true;
            this.lbDefaultCustomers.Location = new System.Drawing.Point(44, 353);
            this.lbDefaultCustomers.Name = "lbDefaultCustomers";
            this.lbDefaultCustomers.Size = new System.Drawing.Size(136, 13);
            this.lbDefaultCustomers.TabIndex = 6;
            this.lbDefaultCustomers.Text = "Grupo Clientes por defecto:";
            // 
            // btAdvancedSearch
            // 
            this.btAdvancedSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btAdvancedSearch.Image = ((System.Drawing.Image)(resources.GetObject("btAdvancedSearch.Image")));
            this.btAdvancedSearch.Location = new System.Drawing.Point(211, 66);
            this.btAdvancedSearch.Name = "btAdvancedSearch";
            this.btAdvancedSearch.Size = new System.Drawing.Size(27, 26);
            this.btAdvancedSearch.TabIndex = 5;
            this.btAdvancedSearch.UseVisualStyleBackColor = true;
            this.btAdvancedSearch.Click += new System.EventHandler(this.btAdvancedSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSearch.Location = new System.Drawing.Point(47, 66);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(153, 26);
            this.tbSearch.TabIndex = 4;
            // 
            // btHide
            // 
            this.btHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide.Image = ((System.Drawing.Image)(resources.GetObject("btHide.Image")));
            this.btHide.Location = new System.Drawing.Point(211, 6);
            this.btHide.Name = "btHide";
            this.btHide.Size = new System.Drawing.Size(27, 28);
            this.btHide.TabIndex = 3;
            this.btHide.UseVisualStyleBackColor = true;
            this.btHide.Click += new System.EventHandler(this.btHide_Click);
            // 
            // btShow
            // 
            this.btShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btShow.Image = ((System.Drawing.Image)(resources.GetObject("btShow.Image")));
            this.btShow.Location = new System.Drawing.Point(245, 6);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(26, 28);
            this.btShow.TabIndex = 2;
            this.btShow.UseVisualStyleBackColor = true;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // splitContCustomers
            // 
            this.splitContCustomers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContCustomers.Location = new System.Drawing.Point(0, 0);
            this.splitContCustomers.Name = "splitContCustomers";
            this.splitContCustomers.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContCustomers.Panel1
            // 
            this.splitContCustomers.Panel1.Controls.Add(this.tabClientes);
            // 
            // splitContCustomers.Panel2
            // 
            this.splitContCustomers.Panel2.Controls.Add(this.tabDirecciones);
            this.splitContCustomers.Size = new System.Drawing.Size(936, 586);
            this.splitContCustomers.SplitterDistance = 362;
            this.splitContCustomers.TabIndex = 13;
            // 
            // tabClientes
            // 
            this.tabClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabClientes.Controls.Add(this.tabCustomers);
            this.tabClientes.Location = new System.Drawing.Point(3, 6);
            this.tabClientes.Name = "tabClientes";
            this.tabClientes.SelectedIndex = 0;
            this.tabClientes.Size = new System.Drawing.Size(930, 353);
            this.tabClientes.TabIndex = 12;
            // 
            // tabCustomers
            // 
            this.tabCustomers.Controls.Add(this.dgvClientes);
            this.tabCustomers.Location = new System.Drawing.Point(4, 22);
            this.tabCustomers.Name = "tabCustomers";
            this.tabCustomers.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomers.Size = new System.Drawing.Size(922, 327);
            this.tabCustomers.TabIndex = 0;
            this.tabCustomers.Text = "CLIENTES";
            this.tabCustomers.UseVisualStyleBackColor = true;
            // 
            // dgvClientes
            // 
            this.dgvClientes.AllowUserToAddRows = false;
            this.dgvClientes.AllowUserToDeleteRows = false;
            this.dgvClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.ContextMenuStrip = this.contextClientes;
            this.dgvClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvClientes.Location = new System.Drawing.Point(3, 3);
            this.dgvClientes.Name = "dgvClientes";
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvClientes.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvClientes.Size = new System.Drawing.Size(916, 321);
            this.dgvClientes.TabIndex = 11;
            this.dgvClientes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_CellClick);
            this.dgvClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_CellContentClick);
            this.dgvClientes.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_CellEndEdit);
            // 
            // tabDirecciones
            // 
            this.tabDirecciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDirecciones.Controls.Add(this.tabAddreses);
            this.tabDirecciones.Location = new System.Drawing.Point(3, 3);
            this.tabDirecciones.Name = "tabDirecciones";
            this.tabDirecciones.SelectedIndex = 0;
            this.tabDirecciones.Size = new System.Drawing.Size(926, 178);
            this.tabDirecciones.TabIndex = 13;
            // 
            // tabAddreses
            // 
            this.tabAddreses.Controls.Add(this.dgvDirecciones);
            this.tabAddreses.Location = new System.Drawing.Point(4, 22);
            this.tabAddreses.Name = "tabAddreses";
            this.tabAddreses.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddreses.Size = new System.Drawing.Size(918, 152);
            this.tabAddreses.TabIndex = 0;
            this.tabAddreses.Text = "DIRECCIONES";
            this.tabAddreses.UseVisualStyleBackColor = true;
            // 
            // dgvDirecciones
            // 
            this.dgvDirecciones.AllowUserToAddRows = false;
            this.dgvDirecciones.AllowUserToDeleteRows = false;
            this.dgvDirecciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDirecciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDirecciones.ContextMenuStrip = this.contextDirPS;
            this.dgvDirecciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDirecciones.Location = new System.Drawing.Point(3, 3);
            this.dgvDirecciones.Name = "dgvDirecciones";
            this.dgvDirecciones.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDirecciones.Size = new System.Drawing.Size(912, 146);
            this.dgvDirecciones.TabIndex = 12;
            this.dgvDirecciones.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDirecciones_CellEndEdit);
            // 
            // statusStripClientes
            // 
            this.statusStripClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelBusquedaCtrlF,
            this.toolStripStatusLabelEspacioClientes,
            this.toolStripStatusLabelClientesContadorFilas,
            this.toolStripProgressBar});
            this.statusStripClientes.Location = new System.Drawing.Point(0, 622);
            this.statusStripClientes.Name = "statusStripClientes";
            this.statusStripClientes.Size = new System.Drawing.Size(1215, 24);
            this.statusStripClientes.TabIndex = 13;
            this.statusStripClientes.Text = "statusStrip1";
            this.statusStripClientes.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStripClientes_ItemClicked);
            // 
            // toolStripStatusLabelBusquedaCtrlF
            // 
            this.toolStripStatusLabelBusquedaCtrlF.BackColor = System.Drawing.Color.Silver;
            this.toolStripStatusLabelBusquedaCtrlF.Name = "toolStripStatusLabelBusquedaCtrlF";
            this.toolStripStatusLabelBusquedaCtrlF.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripStatusLabelBusquedaCtrlF.Size = new System.Drawing.Size(179, 19);
            this.toolStripStatusLabelBusquedaCtrlF.Text = "Ayuda: F1 - Búsqueda: CTRL + F";
            // 
            // toolStripStatusLabelEspacioClientes
            // 
            this.toolStripStatusLabelEspacioClientes.Name = "toolStripStatusLabelEspacioClientes";
            this.toolStripStatusLabelEspacioClientes.Size = new System.Drawing.Size(882, 19);
            this.toolStripStatusLabelEspacioClientes.Spring = true;
            // 
            // toolStripStatusLabelClientesContadorFilas
            // 
            this.toolStripStatusLabelClientesContadorFilas.Name = "toolStripStatusLabelClientesContadorFilas";
            this.toolStripStatusLabelClientesContadorFilas.Size = new System.Drawing.Size(37, 19);
            this.toolStripStatusLabelClientesContadorFilas.Text = "0 filas";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 18);
            // 
            // frClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1215, 646);
            this.Controls.Add(this.statusStripClientes);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStripClientes);
            this.KeyPreview = true;
            this.Name = "frClientes";
            this.Text = "Clientes";
            this.Load += new System.EventHandler(this.frClientes_Load);
            this.contextClientes.ResumeLayout(false);
            this.contextDirPS.ResumeLayout(false);
            this.contextProviA3.ResumeLayout(false);
            this.contextProviPS.ResumeLayout(false);
            this.toolStripClientes.ResumeLayout(false);
            this.toolStripClientes.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.grBoxBloqueados.ResumeLayout(false);
            this.grBoxBloqueados.PerformLayout();
            this.grBoxSincronizar.ResumeLayout(false);
            this.grBoxSincronizar.PerformLayout();
            this.grBoxEnPrestashop.ResumeLayout(false);
            this.grBoxEnPrestashop.PerformLayout();
            this.splitContCustomers.Panel1.ResumeLayout(false);
            this.splitContCustomers.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContCustomers)).EndInit();
            this.splitContCustomers.ResumeLayout(false);
            this.tabClientes.ResumeLayout(false);
            this.tabCustomers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.tabDirecciones.ResumeLayout(false);
            this.tabAddreses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDirecciones)).EndInit();
            this.statusStripClientes.ResumeLayout(false);
            this.statusStripClientes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextClientes;
        private System.Windows.Forms.ToolStripMenuItem ponerANULLElKLSCODCLIENTEToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextDirPS;
        private System.Windows.Forms.ToolStripMenuItem pasarCódigoAA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarCamposToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextProviA3;
        private System.Windows.Forms.ContextMenuStrip contextProviPS;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem sincronizarIDsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem bloquearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desbloquearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarCodigoKlosionsTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStripClientes;
        private System.Windows.Forms.ToolStripButton toolStripButtonExportar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.StatusStrip statusStripClientes;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelBusquedaCtrlF;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelEspacioClientes;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelClientesContadorFilas;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.Button btHide;
        private System.Windows.Forms.Button btShow;
        private System.Windows.Forms.Button btAdvancedSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.TextBox tbDefaultCustomerGroup;
        private System.Windows.Forms.Label lbDefaultCustomers;
        private System.Windows.Forms.RadioButton rbLoadPS;
        private System.Windows.Forms.RadioButton rbLoadA3ERP;
        private System.Windows.Forms.ComboBox cboxClientes;
        private System.Windows.Forms.Label lbFilterby;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem asignarGrupoClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox tscboxCustomerGroup;
        private System.Windows.Forms.Button btClearTextSearch;
        private System.Windows.Forms.DataGridView dgvDirecciones;
        private System.Windows.Forms.SplitContainer splitContCustomers;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem crearDireccionesEnPSToolStripMenuItem;
        private System.Windows.Forms.TabControl tabClientes;
        private System.Windows.Forms.TabPage tabCustomers;
        private System.Windows.Forms.TabControl tabDirecciones;
        private System.Windows.Forms.TabPage tabAddreses;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem crearClienteEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pSPonerANULLCodClienteA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activarClienteParaPrestashopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem crearDirecciónEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marcarDirecciónFiscalEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarDireccionesFiscalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ponerANULLIdDireccionPSToolStripMenuItem;
        private System.Windows.Forms.GroupBox grBoxEnPrestashop;
        private System.Windows.Forms.RadioButton rbEnPSTodos;
        private System.Windows.Forms.RadioButton rbEnPSNo;
        private System.Windows.Forms.RadioButton rbEnPSSi;
        private System.Windows.Forms.GroupBox grBoxBloqueados;
        private System.Windows.Forms.RadioButton rbBlockTodos;
        private System.Windows.Forms.RadioButton rbBlockNo;
        private System.Windows.Forms.RadioButton rbBlockSi;
        private System.Windows.Forms.GroupBox grBoxSincronizar;
        private System.Windows.Forms.RadioButton rbSincTodos;
        private System.Windows.Forms.RadioButton rbSincNo;
        private System.Windows.Forms.RadioButton rbSincSi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem utilidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarImportePedidoMínimoEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem actualizarFamiliaDeClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rossoSincDirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarManualmenteCodA3ERPToolStripMenuItem;
    }
}