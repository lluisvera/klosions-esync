﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace klsync
{
    class csTallasYColores
    {

        public void procedimientoActualizarStockTallasyColores()
        {
            if (csGlobal.conexionDB != "dismayNuevo")
            {
                vaciarTablasTallasyColores();
                sincronizarFamiliasTallasyColores();
                sincronizarTallasyColores();
                asignarTallasyColoresArticulos();
                borrarStockTallasyColores();
                actualizarStockTallasyColores();
            }
            else
            {
                sincronizarTallasyColoresDismay();
                asignarTallasyColoresArticulosDismay();
                borrarStockTallasyColores();
                actualizarStockTallasyColoresDismay();
            }

        }

        public void procedimientoActualizarStockTallasyColores2()
        {
            try
            {
                string consulta_borrar = " DELETE FROM ps_stock_available " + 
                                         " WHERE id_product IN (SELECT id_product FROM ps_category_product WHERE id_category IN (467, 468, 469, 470,472))";
                string consulta_insertar = "INSERT INTO ps_stock_available(id_product, id_product_attribute, id_shop, out_of_stock) " +
                                           " SELECT DISTINCT id_product, 0, 1, 1 FROM ps_category_product WHERE id_category IN (467, 468, 469, 470,472)";

                bool dismay = (csGlobal.conexionDB.ToUpper().Contains("DISMAY")) ? true : false;

                if (!dismay)
                {
                    vaciarTablasTallasyColores();
                    sincronizarFamiliasTallasyColores();
                    sincronizarTallasyColores(); 
                    //asignarTallasyColoresArticulos();
                    borrarStockTallasyColores();
                    actualizarStockTallasyColores();
                }
                else
                {
                    sincronizarTallasyColoresDismay();
                    asignarTallasyColoresArticulosDismay();
                    borrarStockTallasyColores();
                    actualizarStockTallasyColoresDismay();
                }

                if (csGlobal.conexionDB == "esportissim")
                {
                    //csUtilidades.ejecutarConsulta("UPDATE ps_stock_available " +
                    //                              "SET out_of_stock = 1 " +
                    //                              "WHERE id_product IN (SELECT id_product FROM ps_category_product WHERE id_category IN (467, 468, 469, 470));", true, csUtilidades.UPDATE);

                    csUtilidades.ejecutarConsulta(consulta_borrar, true);
                    csUtilidades.ejecutarConsulta(consulta_insertar, true);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        public void actualizarTablaGralTallasyColores()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            csSqlScripts sqlScript = new csSqlScripts();
            //Vaciar los datos de la tabla
            sqlConnect.borrarDatosSqlTabla("KLS_ESYNC_STOCK_TALLASYCOLORES");
            //Inicializo el indice
            sqlConnect.inicializarIndiceTabla("KLS_ESYNC_STOCK_TALLASYCOLORES");
            DataTable datosTallasyColores = new DataTable();
            datosTallasyColores = sqlConnect.obtenerDatosSQLScript(sqlScript.selectStockTallasyColoresFull());
            SqlBulkCopy bulkCopy = new SqlBulkCopy(csGlobal.cadenaConexion);
            bulkCopy.ColumnMappings.Add("CODART", "CODART");
            bulkCopy.ColumnMappings.Add("KLS_ID_SHOP", "KLS_ID_SHOP");
            bulkCopy.ColumnMappings.Add("DESCART", "DESCART");
            bulkCopy.ColumnMappings.Add("CODFAMTALLAH", "CODFAMTALLAH");
            bulkCopy.ColumnMappings.Add("DESCFAMTALLAH", "DESCFAMTALLAH");
            bulkCopy.ColumnMappings.Add("CODFAMTALLAV", "CODFAMTALLAV");
            bulkCopy.ColumnMappings.Add("DESCFAMTALLAV", "DESCFAMTALLAV");
            bulkCopy.ColumnMappings.Add("CODIGO_COLOR", "CODIGO_COLOR");
            bulkCopy.ColumnMappings.Add("VALOR_COLOR", "VALOR_COLOR");
            bulkCopy.ColumnMappings.Add("CODIGO_TALLA", "CODIGO_TALLA");
            bulkCopy.ColumnMappings.Add("VALOR_TALLA", "VALOR_TALLA");
            bulkCopy.ColumnMappings.Add("ID_COLOR", "ID_COLOR");
            bulkCopy.ColumnMappings.Add("ID_TALLA", "ID_TALLA");
            bulkCopy.ColumnMappings.Add("UNIDADES", "UNIDADES");
            bulkCopy.ColumnMappings.Add("ID_ALMACEN", "ID_STOCKALM");


            bulkCopy.DestinationTableName = "KLS_ESYNC_STOCK_TALLASYCOLORES";
            bulkCopy.WriteToServer(datosTallasyColores);


            //for (int i = 0; i < datosTallasyColores.Rows.Count; i++)
            //{
            //    //http://dotnetmentors.com/c-sharp/bulk-upload-into-sql-server-using-sqlbulkcopy-and-c-sharp.aspx


            //}
        }

        private void vaciarTablasTallasyColores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_attribute_group");
            mySqlConnect.borrar("ps_attribute_group_lang");
            mySqlConnect.borrar("ps_attribute_group_shop");
            mySqlConnect.borrar("ps_attribute");
            mySqlConnect.borrar("ps_attribute_lang");
            mySqlConnect.borrar("ps_attribute_shop");
        }

        private void vaciarTablasTallasyColoresDismay()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_attribute");
            mySqlConnect.borrar("ps_attribute_lang");
            mySqlConnect.borrar("ps_attribute_shop");

        }

        private void sincronizarFamiliasTallasyColores()
        {
            try
            {
                //Con esta función cargo las familias de A3 y las traspaso a PS
                //Imprescindible que los códigos de familias de A3 sean numéricos, si no da error
                //ya que utilizamos la misma codificación
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute_group
                string LineasLang = "";
                string LineasShop = "";
                string codigoGrupo = "";
                int position = 1;
                string nuevoCodigo = "";

                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("TallasColores");
                //15-9-6 Modificación para coger todos los artículos con tallas y colores, aunque no tengan stock
                familiaTallasYColores = sqlConnect.cargarFamiliasTallasyColores();
                //Primero insertamos en las tablas que no están afectadas por idiomas
                position = 1;

                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    codigoGrupo = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasShop = LineasShop + ",";
                    }
                    Lineas = Lineas + "(" + codigoGrupo + ",0,'select')";
                    LineasShop = LineasShop + "(" + codigoGrupo + ",1)";
                    position++;
                }

                mySqlConnect.InsertValoresEnTabla("ps_attribute_group", "(id_attribute_group, is_color_group, group_type)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_attribute_group_shop", "(id_attribute_group, id_shop)", LineasShop, "");

                //Para la tabla afectada por idiomas, tenemos que repetirlo para cada idioma
                foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                {
                    for (int iLang = 0; iLang < familiaTallasYColores.Rows.Count; iLang++)
                    {
                        codigoGrupo = familiaTallasYColores.Rows[iLang].ItemArray[0].ToString().Replace(" ", "");
                        if (iLang > 0)
                        {
                            LineasLang = LineasLang + ",";
                        }
                        LineasLang = LineasLang + "(" + codigoGrupo + "," + valuePair.Key.ToString() + ",'" + familiaTallasYColores.Rows[iLang].ItemArray[1].ToString() + "','" + familiaTallasYColores.Rows[iLang].ItemArray[1].ToString() + "')";
                    }

                    mySqlConnect.InsertValoresEnTabla("ps_attribute_group_lang", "(id_attribute_group, id_lang,name,public_name)", LineasLang, "");
                    LineasLang = "";
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("sincronizarFamiliasTallasyColores: " + ex.ToString());
            }

        }

        private void sincronizarTallasyColores() // tallas
        {
            try
            {
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string codigoGrupo = "";
                string descripcion = "";
                string idCaracteristica = "";
                string position = "";

                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("TallasColores");
                string consulta = "select * from tallas left join familiatalla on tallas.codfamtalla = familiatalla.codfamtalla";
                familiaTallasYColores = sqlConnect.cargarDatosTablaA3(consulta);
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    codigoGrupo = familiaTallasYColores.Rows[i]["KLS_IDGROUP"].ToString().Replace(" ", "");
                    descripcion = familiaTallasYColores.Rows[i]["CODTALLA"].ToString().Replace(" ", "");
                    idCaracteristica = familiaTallasYColores.Rows[i]["ID"].ToString().Replace(" ", "");
                    position = familiaTallasYColores.Rows[i]["ORDENTALLA"].ToString().Replace(" ", "");

                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasShop = LineasShop + ",";
                    }
                    Lineas = Lineas + "(" + idCaracteristica + "," + codigoGrupo + ", " + position + ")";
                    LineasShop = LineasShop + "(" + idCaracteristica + ",1)";
                }

                mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");
                //MessageBox.Show("Exportación Finalizada");

                //Para la tabla afectada por idiomas, tenemos que repetirlo para cada idioma
                foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                {
                    for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                    {
                        codigoGrupo = familiaTallasYColores.Rows[i]["KLS_IDGROUP"].ToString().Replace(" ", "");
                        descripcion = familiaTallasYColores.Rows[i]["CODTALLA"].ToString().Replace(" ", "");
                        idCaracteristica = familiaTallasYColores.Rows[i]["ID"].ToString().Replace(" ", "");
                        //position = familiaTallasYColores.Rows[i].ItemArray[3].ToString();

                        if (i > 0)
                        {
                            LineasLang = LineasLang + ",";
                        }

                        LineasLang = LineasLang + "(" + idCaracteristica + "," + valuePair.Key.ToString() + ",'" + descripcion + "')";
                    }

                    mySqlConnect.InsertValoresEnTabla("ps_attribute_lang", "(id_attribute, id_lang,name)", LineasLang, "");
                    LineasLang = "";
                }
                //MessageBox.Show("Exportación Finalizada");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("sincronizarTallasyColores: " + ex.ToString());
            }

        }

        private void sincronizarTallasyColoresDismay()
        {

            int errorFila = 0;

            try
            {
                vaciarTablasTallasyColoresDismay();
                int orden = 0, contador = 0;
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string codigoGrupo = "";
                string descripcion = "";
                string idCaracteristica = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("TallasColores");
                familiaTallasYColores = sqlConnect.cargarAtributosDismay();
                string defaultLangCode = mySqlConnect.defaultLangPS().ToString();
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    orden++;
                    codigoGrupo = "1";
                    descripcion = familiaTallasYColores.Rows[i].ItemArray[1].ToString();

                    descripcion = descripcion.Replace("'", @"\'");
                    //Utilizo el id_product como identificador del atributo

                    idCaracteristica = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (contador > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasShop = LineasShop + ",";
                    }

                    Lineas = Lineas + "('" + idCaracteristica + "'," + codigoGrupo + "," + orden + ")";
                    LineasShop = LineasShop + "('" + idCaracteristica + "',1)";

                    contador++;

                    if (contador > 500)
                    {
                        Lineas = Lineas.TrimEnd(',');
                        LineasShop = LineasShop.TrimEnd(',');
                        mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
                        mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");
                        Lineas = "";
                        LineasShop = "";

                        contador = 0;
                    }
                }

                Lineas = Lineas.TrimEnd(',');
                LineasShop = LineasShop.TrimEnd(',');
                mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");

                //MessageBox.Show("Exportación Finalizada");

                //Para la tabla afectada por idiomas, tenemos que repetirlo para cada idioma
                foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                {
                    for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                    {
                        errorFila = i;
                        codigoGrupo = "1";
                        descripcion = familiaTallasYColores.Rows[i].ItemArray[1].ToString();
                        descripcion = descripcion.Replace("'", @"\'");
                        idCaracteristica = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                        if (i > 0)
                        {
                            LineasLang = LineasLang + ",";
                        }
                        LineasLang = LineasLang + "(" + idCaracteristica + "," + defaultLangCode + ",'" + descripcion + "')";
                    }
                    mySqlConnect.InsertValoresEnTabla("ps_attribute_lang", "(id_attribute, id_lang,name)", LineasLang, "");
                    LineasLang = "";
                }
            }
            catch (Exception ex) {
                ("Fila " + errorFila + "\n" + ex.Message).mb();

            }
        }

        private void asignarTallasyColoresArticulos()
        {
            try
            {
                csCheckVersion checkVersion = new csCheckVersion();
                borrarAsignacionTallasycolores();
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string idCombinacion = "";
                string atributoTalla = "";
                string atributoColor = "";
                string articulo = "";
                string defaultAtributoArticulo = "";
                string default_on = "0";

                csSqlScripts sqlScript = new csSqlScripts();

                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
                familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStockTallasyColoresPS(), "");
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    //articulo = familiaTallasYColores.Rows[i].ItemArray[1].ToString().Replace(" ", "");
                    articulo = familiaTallasYColores.Rows[i].ItemArray[1].ToString();
                    if (defaultAtributoArticulo != articulo)
                    {
                        default_on = "1";
                    }
                    else
                    {
                        default_on = "0";
                    }


                    idCombinacion = familiaTallasYColores.Rows[i].ItemArray[13].ToString().Replace(" ", "");
                    atributoColor = familiaTallasYColores.Rows[i].ItemArray[9].ToString().Replace(" ", "");
                    atributoTalla = familiaTallasYColores.Rows[i].ItemArray[10].ToString().Replace(" ", "");
                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasLang = LineasLang + ",";
                        LineasShop = LineasShop + ",";
                    }
                    Lineas = Lineas + "(" + idCombinacion + "," + articulo + ")";
                    LineasLang = LineasLang + "(" + atributoTalla + "," + idCombinacion + "),(" + atributoColor + "," + idCombinacion + ")";

                    if (checkVersion.checkVersión("1.6.1"))
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + "," + articulo + ")";
                    }
                    else
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + ")";
                    }
                    defaultAtributoArticulo = articulo;
                }
                
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", LineasLang, "");
                if (checkVersion.checkVersión("1.6.1"))
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product)", LineasShop, "");
                }
                else
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", LineasShop, "");
                }
                //MessageBox.Show("Exportación Finalizada");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("asignarTallasyColoresArticulos: " + ex.ToString());
            }

        }

        //Personalización para Dismay
        //Hibrido de Tallas y Colores. A3ERP no trabaja con Tallas y Colores
        private void asignarTallasyColoresArticulosDismay()
        {
            try
            {
                csCheckVersion checkVersion = new csCheckVersion();
                borrarAsignacionTallasycolores();
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string idCombinacion = "";
                string atributoTalla = "";
                string articulo = "";
                string defaultAtributoArticulo = "";
                string default_on = "0";
                string reference = "";

                csSqlScripts sqlScript = new csSqlScripts();

                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
                familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStocTallasyColoresDismay(), "");
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    //articulo = familiaTallasYColores.Rows[i].ItemArray[1].ToString().Replace(" ", "");
                    //articulo = familiaTallasYColores.Rows[i].ItemArray[9].ToString();
                    articulo = familiaTallasYColores.Rows[i].ItemArray[6].ToString().Replace(" ", "");
                    reference = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (defaultAtributoArticulo != articulo)
                    {
                        default_on = "1";
                    }
                    else
                    {
                        default_on = "Null";
                    }


                    idCombinacion = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    //atributoColor = familiaTallasYColores.Rows[i].ItemArray[7].ToString().Replace(" ", "");
                    atributoTalla = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasLang = LineasLang + ",";
                        LineasShop = LineasShop + ",";
                    }

                    if (articulo != "")
                    { 
                    }
                    Lineas = Lineas + "(" + idCombinacion + "," + articulo + ",'" + reference + "','','','','')";
                    LineasLang = LineasLang + "(" + atributoTalla + "," + idCombinacion + ")";
                    
                    if (checkVersion.checkVersión("1.6.1"))
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + "," + articulo + ")";
                    }
                    else
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + ")";
                    }
                    
                    defaultAtributoArticulo = articulo;
                }

                Lineas = Lineas.TrimEnd(',');
                LineasLang = LineasLang.TrimEnd(',');

                mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product,reference,supplier_reference,location,ean13,upc)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", LineasLang, "");
                //mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", LineasShop, "");
                if (checkVersion.checkVersión("1.6.1"))
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product)", LineasShop, "");
                }
                else
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", LineasShop, "");
                }
                //MessageBox.Show("Exportación Finalizada");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("actualizarStockTallasyColores: " + ex.ToString());
            }

        }

        private void borrarStockTallasyColores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_stock_available", " id_product_attribute<>0 ");
        }

        private void actualizarStockTallasyColores()
        {
            try
            {
                borrarStockTallasyColores();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sql = new csSqlConnects();
                int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

                string Lineas = "";

                string idCombinacion = "";
                string cantidad = "";
                string articulo = "";
                string stock_disponible = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
                familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStockTallasyColoresPS(), "");

                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    articulo = familiaTallasYColores.Rows[i]["KLS_ID_SHOP"].ToString().Replace(" ", "");
                    idCombinacion = familiaTallasYColores.Rows[i]["ID"].ToString().Replace(" ", "");
                    cantidad = familiaTallasYColores.Rows[i]["UNIDADES"].ToString().Replace(" ", "");
                    stock_disponible = familiaTallasYColores.Rows[i]["KLS_PERMITIRCOMPRAS"].ToString().Replace(" ", "");

                    if (cantidad == "")
                    {
                        cantidad = "0";
                    }
                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                    }

                    Lineas = Lineas + "(" + ultimoId.ToString() + "," + articulo + "," + idCombinacion + ",1," + cantidad + ",0," + stock_disponible + ")";
                    ultimoId = ultimoId + 1;
                }

                mySqlConnect.InsertValoresEnTabla("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,quantity, depends_on_stock, out_of_stock)", Lineas, "");
                //MessageBox.Show("Stock Actualizado");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("actualizarStockTallasyColores: " + ex.ToString());
            }

        }

        private void actualizarStockTallasyColoresDismay()
        {
            borrarStockTallasyColores();
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csSqlScripts sqlScript = new csSqlScripts();
            int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

            string Lineas = "";

            string idCombinacion = "";
            string cantidad = "";
            string articulo = "";

            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
            familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStocTallasyColoresDismay(), "");
            for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
            {
                articulo = familiaTallasYColores.Rows[i].ItemArray[6].ToString().Replace(" ", "");
                //articulo = "1520";
                idCombinacion = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                cantidad = familiaTallasYColores.Rows[i].ItemArray[7].ToString().Replace(" ", "");
                if (cantidad == "")
                {
                    cantidad = "0";
                }
                if (i > 0)
                {
                    Lineas = Lineas + ",";

                }
                Lineas = Lineas + "(" + ultimoId.ToString() + "," + articulo + "," + idCombinacion + ",1," + cantidad + ",0,2)";
                ultimoId = ultimoId + 1;
            }

            mySqlConnect.InsertValoresEnTabla("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,quantity, depends_on_stock, out_of_stock)", Lineas, "");
            //MessageBox.Show("Stock Actualizado");
        }

        private void borrarAsignacionTallasycolores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_product_attribute");
            mySqlConnect.borrar("ps_product_attribute_combination");
            mySqlConnect.borrar("ps_product_attribute_shop");


        }
    }
}
