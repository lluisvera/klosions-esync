﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using MySql.Data.MySqlClient;

namespace klsync
{
    public partial class frConfig : Form
    {

        private static frConfig m_FormDefInstance;
        public static frConfig DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frConfig();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frConfig()
        {
            InitializeComponent();
        }

        private void frConfig_Load(object sender, EventArgs e)
        {
            controlesNuevoEnlace(false);
            cbConexiones.DataSource = null;
            cbConexiones.Items.Clear();
            csXMLConfig xmlConfig = new csXMLConfig();
            cbConexiones.DataSource = xmlConfig.cargarConexionesXML();
            cbConexiones.SelectedItem = csGlobal.conexionDB;
        }

        private void btTestA3_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                //MessageBox.Show("Conexión Establecida Correctamente");
                dataConnection.Close();
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private bool btTestA3_Click()
        {
            bool isConn = false;

            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                dataConnection.Close();
                isConn = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            return isConn;
        }

        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = tbServerA3.Text;
            csb.InitialCatalog = tbDatabaseA3.Text;
            csb.IntegratedSecurity = csGlobal.integratedSecurity;
            csb.UserID = tbUserA3.Text;
            csb.Password = tbPasswordA3.Text;
            return csb.ConnectionString;
        }

        private void btSelectClienteGen_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("select ltrim(codcli) as codcli from clientes", dataConnection);
                DataTable t = new DataTable();
                a.Fill(t);
                cbClienteGenerico.ValueMember = "codcli";
                cbClienteGenerico.DisplayMember = "codcli";
                cbClienteGenerico.DataSource = t;
                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
        private void cargarAlmacenes()
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("select ltrim(codalm) as codalm from almacen", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cbAlmacenA3.ValueMember = "codalm";
                cbAlmacenA3.DisplayMember = "codalm";
                cbAlmacenA3.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        private void cargarDatosAnalitica()
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("select ltrim(centrocoste) as centrocoste from centrosc", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);
                DataTable t2 = new DataTable();
                a.Fill(t2);
                DataTable t3 = new DataTable();
                a.Fill(t3);
                cbNivel1.ValueMember = "centrocoste";
                cbNivel1.DisplayMember = "centrocoste";
                cbNivel1.DataSource = t1;
                cbNivel2.ValueMember = "centrocoste";
                cbNivel2.DisplayMember = "centrocoste";
                cbNivel2.DataSource = t2;
                cbNivel3.ValueMember = "centrocoste";
                cbNivel3.DisplayMember = "centrocoste";
                cbNivel3.DataSource = t3;
                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }


        }



        private void btSelectSerie_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = CadenaConexion();
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("select serie from series", dataConnection);
                DataTable t = new DataTable();
                a.Fill(t);
                cbSerie.ValueMember = "serie";
                cbSerie.DisplayMember = "serie";
                cbSerie.DataSource = t;
                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }



        protected void CrearDocumentoXML()
        {
            System.Xml.Linq.XDocument miXML = new XDocument(
            new XDeclaration("1.0", "utf-8", "yes"),
            new XComment("Configuración Enlace A3ERP Prestashop"),
            new XElement("Conexiones",
                                new XElement("conexion",
                                new XAttribute("id", "maslejos"),
                                    new XElement("ServidorPS", "212.36.85.76"),
                                    new XElement("DatabasePS", "invenio2"),
                                    new XElement("UsuarioPS", "invenio2"),
                                    new XElement("PasswordPS", "invenio2012"),
                                    new XElement("ServidorA3", tbServerA3.Text),
                                    new XElement("DatabaseA3", tbDatabaseA3.Text),
                                    new XElement("UsuarioA3", tbUserA3.Text),
                                    new XElement("PasswordA3", tbPasswordA3.Text),
                                    new XElement("ClienteGenerico", cbClienteGenerico.Text.Trim()),
                                    new XElement("SerieDestino", cbSerie.Text.Trim()),
                                    new XElement("VersionPS", "1.5"),
                                    new XElement("IvaIncluido", "S"),
                                    new XElement("DocDestino", cbDocDestino.Text),
                                    new XElement("RutaA3", tbRutaA3.Text),
                                    new XElement("Analitica", "S,3,C1,C2,C3")

                                    //new XElement("VersionPS", cbVersionPS.Text)
                                    ),

                                 new XElement("conexion",
                                 new XAttribute("id", "fll"),
                                    new XElement("ServidorPS", "212.36.85.76"),
                                    new XElement("DatabasePS", "invenio"),
                                    new XElement("UsuarioPS", "invenio"),
                                    new XElement("PasswordPS", "invenio2012"),
                                    new XElement("ServidorA3", tbServerA3.Text),
                                    new XElement("DatabaseA3", tbDatabaseA3.Text),
                                    new XElement("UsuarioA3", tbUserA3.Text),
                                    new XElement("PasswordA3", tbPasswordA3.Text),
                                    new XElement("ClienteGenerico", cbClienteGenerico.Text.Trim()),
                                    new XElement("SerieDestino", cbSerie.Text.Trim()),
                                    new XElement("VersionPS", "1.4"),
                                    new XElement("IvaIncluido", "S"),
                                    new XElement("DocDestino", cbDocDestino.Text),
                                    new XElement("RutaA3", tbRutaA3.Text),
                                    new XElement("Analitica", "S,3,C1,C2,C3"))
                //new XElement("VersionPS", cbVersionPS.Text)
                                    )

            );
            miXML.Save(@"miXML.xml");
            //MessageBox.Show("XML Creado");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CrearDocumentoXML();
        }

        private void visualizarAnalitica()
        {
            if (cboxAnalitica.Checked)
            {
                tbNivelesAnalitica.Visible = true;
                cbNivel1.Visible = true;
                cbNivel2.Visible = true;
                cbNivel3.Visible = true;
                btCargarAnalitica.Visible = true;
                lblNivelesAnalitica.Visible = true;
            }
            else
            {
                tbNivelesAnalitica.Visible = false;
                cbNivel1.Visible = false;
                cbNivel2.Visible = false;
                cbNivel3.Visible = false;
                btCargarAnalitica.Visible = false;
                lblNivelesAnalitica.Visible = false;
            }

        }


        private void cargarConfiguracionConexionesXML()
        {


            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");

            XmlNodeList conexiones = xDoc.GetElementsByTagName("conexion");
            XmlNodeList lista = xDoc.GetElementsByTagName("conexion");
            foreach (XmlElement nodo in lista)
            {
                if (nodo.GetAttribute("id").ToString() == cbConexiones.Text)
                {

                    tbNombreConexion.Text = cbConexiones.Text;
                    cboxModeAp.Text = csGlobal.tipoEjecución;
                    cboxModeAp.Text = csGlobal.modeAp;
                    tbServerPS.Text = csGlobal.ServerPS;
                    tbDatabasePS.Text =  csGlobal.databasePS;
                    tbUserPS.Text = csGlobal.userPS;
                    tbPasswordPS.Text = csGlobal.passwordPS;
                    tbAPIUrl.Text = csGlobal.APIUrl;
                    tbWebServicekey.Text = csGlobal.webServiceKey;
                    tbUserFTP.Text = csGlobal.userFTP;
                    tbPasswordFTP.Text = csGlobal.passwordFTP;
                    tbEmailCustomerNotify.Text = csGlobal.emailNotificacionCliente;

                    tbServerA3.Text = csGlobal.ServerA3;
                    tbDatabaseA3.Text = csGlobal.databaseA3;
                    tbUserA3.Text = csGlobal.userA3;
                    tbPasswordA3.Text = csGlobal.passwordA3;
                    if (csGlobal.usarClienteGenerico == "SI")
                    {
                        cboxClienteGenerico.Checked = true;
                    }
                    else { cboxClienteGenerico.Checked = false; }
                    cbClienteGenerico.Text = csGlobal.clienteGenerico;
                    cbSerie.Text = csGlobal.serie;
                    cbVersionPS.Text = csGlobal.versionPS;
                    cbDocDestino.Text = csGlobal.docDestino;
                    ////Cargo los valores de la analítica
                    //string[] analitica = nodo.GetElementsByTagName("Analitica")[0].InnerText.Split(',');
                    //if (analitica[0].ToUpper() == "SI") cboxAnalitica.Checked = true;
                    //else cboxAnalitica.Checked = false;
                    //visualizarAnalitica();
                    //tbNivelesAnalitica.Text = analitica[1];
                    //cbNivel1.Text = analitica[2];
                    //cbNivel2.Text = analitica[3];
                    //cbNivel3.Text = analitica[4];
                    //tbRutaA3.Text = nodo.GetElementsByTagName("RutaA3")[0].InnerText;
                    //tbRutaFicConfig.Text = nodo.GetElementsByTagName("RutaFicConfig")[0].InnerText;
                    //tbRutaImagenes.Text = nodo.GetElementsByTagName("RutaImagenes")[0].InnerText;
                    //tbRutaFTPImagenes.Text = nodo.GetElementsByTagName("RutaFTPImagenes")[0].InnerText;
                    //tbNombreServidor.Text = nodo.GetElementsByTagName("NombreServidor")[0].InnerText;
                    //tbRutaRemotaImg.Text = nodo.GetElementsByTagName("RutaRemotaImagenes")[0].InnerText;
                    //cbAlmacenA3.Text = nodo.GetElementsByTagName("AlmacenA3")[0].InnerText;
                    //cbCategoria.Text = nodo.GetElementsByTagName("Categoria")[0].InnerText;
                    //cbCaractEnlace.Text = nodo.GetElementsByTagName("Ecommerce")[0].InnerText;
                    //cbIvaIncluido.Text = nodo.GetElementsByTagName("IvaIncluido")[0].InnerText;
                    //cboxDefaultLang.Text = nodo.GetElementsByTagName("DefaultLang")[0].InnerText;
                    //cboxIntegratedSecurity.Text = nodo.GetElementsByTagName("IntegratedSecurity")[0].InnerText;
                    //txtTienda.Text = nodo.GetElementsByTagName("Tienda")[0].InnerText;
                    //txtBackOffice.Text = nodo.GetElementsByTagName("BackOffice")[0].InnerText;

                    //comboServirPedido.Text = nodo.GetElementsByTagName("ServirPedido")[0].InnerText;
                    //textEmailEnvio.Text = nodo.GetElementsByTagName("EmailEnvio")[0].InnerText;
                    //textEmailDestino.Text = nodo.GetElementsByTagName("EmailDestino")[0].InnerText;
                    //textEmailSMTP.Text = nodo.GetElementsByTagName("EmailSMTP")[0].InnerText;
                    //textUsuarioEmail.Text = nodo.GetElementsByTagName("UsuarioEmail")[0].InnerText;
                    //textUsuarioPass.Text = nodo.GetElementsByTagName("UsuarioPass")[0].InnerText;*/
                }
            }

        }



        private void btLoadConfig_Click(object sender, EventArgs e)
        {
            cargarConfiguracionConexionesXML();
        }



        private void btCrearConfiguracion_Click(object sender, EventArgs e)
        {
            csXMLConfig xmlConfig = new csXMLConfig();
            string usarCliGen = "";
            if (cboxClienteGenerico.Checked)
            {
                usarCliGen = "Si";
            }
            else
            {
                usarCliGen = "No";
            }
            if (xmlConfig.comprobarIdEnlace(tbNombreConexion.Text))
            {
                string valoresAnalitica = "S," + tbNivelesAnalitica.Text + "," + cbNivel1.Text + "," + cbNivel2.Text + "," + cbNivel3.Text;
                csXMLConfig xmlConfigurador = new csXMLConfig();
                xmlConfigurador.añadirNodo(cboxModeAp.Text, tbNombreConexion.Text, tbServerPS.Text, tbDatabasePS.Text, tbUserPS.Text, tbPasswordPS.Text, tbAPIUrl.Text, tbUserFTP.Text, tbPasswordFTP.Text, tbServerA3.Text,
                    tbDatabaseA3.Text, tbUserA3.Text, tbPasswordA3.Text, usarCliGen, cbClienteGenerico.Text, cbSerie.Text, cbVersionPS.Text, cbDocDestino.Text,
                    valoresAnalitica, tbRutaA3.Text, tbRutaFicConfig.Text, tbRutaImagenes.Text, tbRutaFTPImagenes.Text, tbNombreServidor.Text, tbRutaRemotaImg.Text, cbAlmacenA3.Text, cbCaractEnlace.Text, cbCategoria.Text,
                    cbIvaIncluido.Text, cboxDefaultLang.Text, true, cboxDefCustomerGroup.Text, cboxUsaProntoPago.Text, tbEstadosPedidosBloqueados.Text,cboxModoTyC.Text);
                //MessageBox.Show("Se ha guardado el enlace " + tbNombreConexion.Text);
                controlesNuevoEnlace(false);
            }

        }

        private void btBorrarEnlace_Click(object sender, EventArgs e)
        {
            DialogResult dialogo = MessageBox.Show("¿Desea borrar el siguiente enlace? \n No podrá utilizar este enlace", "Borrar Enlace", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dialogo == DialogResult.Yes)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");

                //XmlNode nodo = xDoc.SelectSingleNode("/Conexiones/conexion[DatabasePS='invenio']");
                XmlNode nodo = xDoc.SelectSingleNode("/Conexiones/conexion[@id='" + cbConexiones.Text + "']");
                nodo.ParentNode.RemoveChild(nodo);
                xDoc.Load(csGlobal.rutaFicConfig + "miXML.xml");

                foreach (Control c in this.Controls)
                {
                    if (c is TextBox || c is ComboBox)
                    {
                        c.Text = "";
                        c.Refresh();
                    }
                    if (c is GroupBox)
                    {
                        foreach (Control g in c.Controls)
                        {
                            if (g is TextBox)
                            {
                                g.Text = "";
                                g.Refresh();
                            }
                        }


                    }

                }
                this.Refresh();
                //MessageBox.Show("Se ha borrado el enlace");



            }
        }

        private void btNuevoEnlace_Click(object sender, EventArgs e)
        {
            controlesNuevoEnlace(true);
        }

        private void controlesNuevoEnlace(bool visible)
        {
            lbNombreConexion.Visible = visible;
            tbNombreConexion.Visible = visible;
            btCrearConfiguracion.Visible = visible;
            btCrearConfiguracion.Visible = visible;
        }

        private void cbConexiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarConfiguracionConexionesXML();
            desmarcarComprobacion();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            mysql.Insert();
        }

        private void tbTablasAuxiliares_Click(object sender, EventArgs e)
        {
            crearTablasAuxiliaresPS();
        }

        public void crearTablasAuxiliaresPS()
        {
            csGlobal.conexionDB = cbConexiones.Text;
            csMySqlConnect mySql = new csMySqlConnect();
            string query = "";
            csXMLConfig xml = new csXMLConfig();
            xml.cargarConfiguracionConexionesXML();
            //Script para crear la tabla de facturas auxiliares
            query = "CREATE TABLE `kls_invoice_erp` (`invoice_number` int(11) NOT NULL, " +
            " `invoice_number_erp` int(11) NOT NULL,`date_sync` date NOT NULL, " +
            " `total_doc_erp` decimal(17,2) DEFAULT NULL,PRIMARY KEY (`invoice_number`,`invoice_number_erp`), " +
            " UNIQUE KEY `invoice_number_UNIQUE` (`invoice_number`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";

            mySql.crearTablasAuxiliares(query);

            query = "CREATE TABLE `kls_stock_erp` (`id` int(11) NOT NULL AUTO_INCREMENT, " +
            " `reference` varchar(45) NOT NULL,`unidades` int(11) DEFAULT '0', " +
            " PRIMARY KEY (`id`,`reference`)) ENGINE=InnoDB AUTO_INCREMENT=625 DEFAULT CHARSET=utf8";
            mySql.crearTablasAuxiliares(query);

            lblTablasAux.Text = "Tablas auxiliares de PS creadas con éxito";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            csFileSystem fileSystem = new csFileSystem();
            tbRutaA3.Text = fileSystem.rutaA3ERP();
        }



        private void btCargarAnalitica_Click(object sender, EventArgs e)
        {
            cargarDatosAnalitica();
        }

        private void btTablasAuxA3_Click(object sender, EventArgs e)
        {
            tablasAuxiliaresA3();
        }

        public void tablasAuxiliaresA3()
        {
            csSqlConnects sqlConect = new csSqlConnects();
            csSqlScripts sqlScript = new csSqlScripts();

            sqlConect.crearTablasAuxiliares(sqlScript.crearTablaArticulosA3());
            cboxProducts.Checked = true;
            cboxProducts.ForeColor = System.Drawing.Color.Green;
            sqlConect.crearTablasAuxiliares(sqlScript.crearTablaEnlacesIdiomas());
            cboxIdiomas.Checked = true;
            cboxIdiomas.ForeColor = System.Drawing.Color.Green;
            sqlConect.crearTablasAuxiliares(sqlScript.crearTablaTallasyColores());
            cboxTallasyColores.Checked = true;
            cboxTallasyColores.ForeColor = System.Drawing.Color.Green;
        }

        private void btGrabarCambios_Click(object sender, EventArgs e)
        {

            string usarCliGen = "";
            csAnalitica stringAnalitica = new csAnalitica();
            if (cboxClienteGenerico.Checked) usarCliGen = "Si"; else usarCliGen = "No";


            csXMLConfig xmlConfig = new csXMLConfig();
            xmlConfig.modificarNodos(cbConexiones.Text, "ServidorPS", tbServerPS.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "DatabasePS", tbDatabasePS.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "UsuarioPS", tbUserPS.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "PasswordPS", tbPasswordPS.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "APIUrl", tbAPIUrl.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "WebserviceKey", tbWebServicekey.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "UsuarioFTP", tbUserFTP.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "PasswordFTP", tbPasswordFTP.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "ServidorA3", tbServerA3.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "DatabaseA3", tbDatabaseA3.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "UsuarioA3", tbUserA3.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "PasswordA3", tbPasswordA3.Text);

            xmlConfig.modificarNodos(cbConexiones.Text, "UsarClienteGenerico", usarCliGen);

            xmlConfig.modificarNodos(cbConexiones.Text, "ClienteGenerico", cbClienteGenerico.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "SerieDestino", cbSerie.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "VersionPS", cbVersionPS.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "IvaIncluido", cbIvaIncluido.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "DocDestino", cbDocDestino.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "RutaA3", tbRutaA3.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "RutaFicConfig", tbRutaFicConfig.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "RutaImagenes", tbRutaImagenes.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "RutaFTPImagenes", tbRutaFTPImagenes.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "NombreServidor", tbNombreServidor.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "RutaRemotaImagenes", tbRutaRemotaImg.Text);


            //La analitica tiene 4 factores  <Analitica>S,3,C1,C2,C3</Analitica> 
            //Primero indica si se usa o no, luego número de niveles, finalmente los 3 niveles.
            xmlConfig.modificarNodos(cbConexiones.Text, "Analitica", stringAnalitica.xmlAnalitica(cboxAnalitica.Checked, tbNivelesAnalitica.Text, cbNivel1.Text, cbNivel2.Text, cbNivel3.Text));
            xmlConfig.modificarNodos(cbConexiones.Text, "AlmacenA3", cbAlmacenA3.Text);
            //indico el campo que indica que un articulo se vincula con la tienda
            xmlConfig.modificarNodos(cbConexiones.Text, "Ecommerce", cbCaractEnlace.Text);
            //indico el campo que indica la categoría a la que pertenece un articulo
            xmlConfig.modificarNodos(cbConexiones.Text, "Categoria", cbCategoria.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "DefaultLang", cboxDefaultLang.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "IntegratedSecurity", cboxIntegratedSecurity.Text); // alex
            xmlConfig.modificarNodos(cbConexiones.Text, "Tienda", txtTienda.Text); // alex
            xmlConfig.modificarNodos(cbConexiones.Text, "BackOffice", txtBackOffice.Text); // alex

            //(comboServirPedido.SelectedItem.ToString() == "No") ? xmlConfig.modificarNodos(cbConexiones.Text, "EmailEnvio", "No") : xmlConfig.modificarNodos(cbConexiones.Text, "EmailEnvio", "Si");
            xmlConfig.modificarNodos(cbConexiones.Text, "EmailEnvio", textEmailEnvio.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "EmailDestino", textEmailDestino.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "EmailSMTP", textEmailSMTP.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "UsuarioEmail", textUsuarioEmail.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "UsuarioPass", textUsuarioPass.Text);
            xmlConfig.modificarNodos(cbConexiones.Text, "DefaultCustomerGroup", cboxDefCustomerGroup.Text);

            cargarConfiguracionConexionesXML();
        }


        private int analiticaNiveles()
        {
            return 1;
        }


        private void guardarCambios()
        {
            string enlace = cbConexiones.Text;
            csXMLConfig xmlConfig = new csXMLConfig();
            xmlConfig.modificarNodos(enlace, "SerieDestino", cbSerie.Text);
            xmlConfig.modificarNodos(enlace, "SerieDestino", cbSerie.Text);
            xmlConfig.modificarNodos(enlace, "SerieDestino", cbSerie.Text);

        }

        private void btBuscarDirectorioA3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogoDirectorios = new FolderBrowserDialog();
            if (dialogoDirectorios.ShowDialog() == DialogResult.OK)
            {
                tbRutaA3.Text = dialogoDirectorios.SelectedPath;
            }
        }

        private void btBuscarDirectorioConfig_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogoDirectorios = new FolderBrowserDialog();
            if (dialogoDirectorios.ShowDialog() == DialogResult.OK)
            {
                tbRutaFicConfig.Text = dialogoDirectorios.SelectedPath;
            }
        }

        private void btBuscarRutaImagenes_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogoDirectorios = new FolderBrowserDialog();
            if (dialogoDirectorios.ShowDialog() == DialogResult.OK)
            {
                tbRutaImagenes.Text = dialogoDirectorios.SelectedPath;
            }
        }

        private void btBuscarAlmacenes_Click(object sender, EventArgs e)
        {
            cargarAlmacenes();
        }

        private void cboxAnalitica_CheckedChanged(object sender, EventArgs e)
        {
            visualizarAnalitica();
        }

        private void btTestPS_Click(object sender, EventArgs e)
        {
            csMySqlConnect conector = new csMySqlConnect();
            bool isConn = false;

            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();
                isConn = true;
                //MessageBox.Show("Conexión Realizada con éxito");


            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }
        }

        private bool btTestPS_Click()
        {
            csMySqlConnect conector = new csMySqlConnect();
            bool isConn = false;

            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();
                isConn = true;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

            return isConn;
        }

        private bool comprobarIdiomas(String sentencia)
        {
            bool existe = false;

            try
            {
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = new SqlDataAdapter(sentencia, dataConnection);

                DataTable t = new DataTable();

                a.Fill(t);
                if (t.Rows.Count > 0)
                {
                    existe = true;
                }
            }
            catch (Exception ex)
            {
            }

            return existe;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {

            // idiomas
            if (comprobarIdiomas("SELECT count(*) FROM dbo.KLS_ESYNC_IDIOMAS") == true)
            {
                cbIdiomas.Checked = true;
            }
            else
            {
                cbIdiomas.Checked = false;
            }

            // addon
            if (comprobar_AddonPS() == true)
            {
                cbAddonPS.Checked = true;
            }
            else
            {
                cbAddonPS.Checked = false;
            }

            if (btTestA3_Click() == true)
            {
                cbA3.Checked = true;
            }
            else
            {
                cbA3.Checked = false;
            }
            if (btTestPS_Click() == true)
            {
                cbPrestashop.Checked = true;
            }
            else
            {
                cbPrestashop.Checked = false;
            }

            csMySqlConnect mysql = new csMySqlConnect();
            if (mysql.existeTabla("ps_webservice_account"))
            {
                cb_Webservice.Checked = true;
            }
            else
            {
                cb_Webservice.Checked = false;
            }
        }

        private void desmarcarComprobacion()
        {
            cbXML.Checked = false;
            cbIdiomas.Checked = false;
            cbAddonPS.Checked = false;
            cbPrestashop.Checked = false;
            cbA3.Checked = false;
        }

        private void comprobar_XML()
        {
            csXMLUtilities xml = new csXMLUtilities();
        }

        // comprobar que existe el campo kls_a3erp_id
        // creado en PrestaShop (MySQL)
        private bool comprobar_AddonPS()
        {
            bool activo = false;
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScripts sqlScript = new csSqlScripts();
            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript.selectKLS_A3ERP_ID(), conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                if (table.Rows.Count > 0)
                {
                    activo = true;
                }
            }
            catch (Exception ex)
            {
            }

            return activo;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            csXMLConfig xmlConfig = new csXMLConfig();
            //xmlConfig.modificarNodos(cbConexiones.Text, "ServirPedido", comboServirPedido.SelectedItem.ToString());

            cargarConfiguracionConexionesXML();
        }

        public void actualizarProvinciasPaisesA3()
        {
            DataTable dt = cargarCSV();
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable paises = mysql.cargarTabla("select id_country, iso_code from ps_country");
            DataTable provincias = mysql.cargarTabla("select id_state, iso_code from ps_state where iso_code like 'ES-%'");
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    csUtilidades.ejecutarConsulta("UPDATE PROVINCI SET KLS_ISOCODE = '" + dr["4"].ToString() + "' WHERE LTRIM(CODPROVI) = '" + dr["2"].ToString().Trim() + "'", false);
                }

                foreach (DataRow dr in provincias.Rows)
                {
                    csUtilidades.ejecutarConsulta("UPDATE PROVINCI SET KLS_IDPS = " + dr["id_state"].ToString() + " WHERE KLS_ISOCODE = '" + dr["iso_code"].ToString() + "'", false);
                }

                foreach (DataRow dr in paises.Rows)
                {
                    csUtilidades.ejecutarConsulta("UPDATE PAISES SET KLS_CODPAIS_PS = '" + dr["id_country"].ToString()
                        + "' WHERE CODPAIS = '" + dr["iso_code"].ToString() + "'", false);
                }

                MessageBox.Show("PROVINCIAS Y PAISES ACTUALIZADOS :-)");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("Error: " + ex.ToString());
            }
        }

        private void btnActualizarProvincias_Click(object sender, EventArgs e)
        {
            actualizarProvinciasPaisesA3();
        }

        private DataTable cargarCSV()
        {
            DataTable dt = null;
            Stream myStream = null;
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Abrir archivo";
            theDialog.Filter = "CSV Files|*.csv*";
            //theDialog.InitialDirectory = @"C:\Users\Alex\Desktop";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = theDialog.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            dt = csUtilidades.ConvertCSVtoDataTable(theDialog.FileName);
                        }
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("Cierra el documento para importar el csv", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception)
                {
                }
            }

            return dt;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            testEmailNotificacionErrores();
        }



        private void testEmailNotificacionErrores()
        {

            Program.guardarErrorFichero("Test Envío Email notificación de errores de Klosions Esync");
        
        }

        private void btnUpdateCountry_Click(object sender, EventArgs e)
        {
            DataTable dtPaisPS = new DataTable();
            DataTable dtPaisA3 = new DataTable();
            csMySqlConnect mySqlConnect = new klsync.csMySqlConnect();
            csSqlConnects sqlConnect = new klsync.csSqlConnects();
            dtPaisPS = mySqlConnect.obtenerDatosPS("select * from ps_country");
            dtPaisA3 = sqlConnect.obtenerDatosSQLScript("select * from paises");

            string isoCodePaisPS = "";
            string idPaisPS = "";
            string isoCodePaisA3 = "";

            foreach (DataRow fila in dtPaisPS.Rows)
            {
                isoCodePaisPS = fila["iso_code"].ToString();
                foreach (DataRow filaA3 in dtPaisA3.Rows)
                {
                    isoCodePaisA3 = filaA3["CODISO31A266"].ToString();
                    if (isoCodePaisA3 == isoCodePaisPS)
                    {
                        csUtilidades.ejecutarConsulta("UPDATE PAISES SET KLS_CODPAIS_PS='" + idPaisPS + "'", false);
                        break;
                    }
                }
            }
        }
    }
}
