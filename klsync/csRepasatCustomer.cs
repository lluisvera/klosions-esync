﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csRepasatCustomer
    {
        public class Type
        {
            public int idTipoCli { get; set; }
            public int idEntidadTipoCli { get; set; }
            public int codTipoCli { get; set; }
            public string nomTipoCli { get; set; }
            public int? autorTipoCli { get; set; }
            public int activoTipoCli { get; set; }
            public string fecAltaTipoCli { get; set; }
        }

        public class Responsible
        {
            public int idTrabajador { get; set; }
            public int idEntidadTrabajador { get; set; }
            public int? idResponsable { get; set; }
            public int codTrabajador { get; set; }
            public string nombreTrabajador { get; set; }
            public string apellidosTrabajador { get; set; }
            public string aliasTrabajador { get; set; }
            public string nifTrabajador { get; set; }
            public object idResponsableExt { get; set; }
            public string tipoRecursoTrabajador { get; set; }
            public string sexoTrabajador { get; set; }
            public int? fotoTrabajador { get; set; }
            public int? idDpto { get; set; }
            public int idUsuario { get; set; }
            public string fecCadNif { get; set; }
            public object numSegSoc { get; set; }
            public string telf1 { get; set; }
            public object telf2 { get; set; }
            public string emailTrabajador { get; set; }
            public string fecNac { get; set; }
            public object idEstadoCivil { get; set; }
            public int? numHijos { get; set; }
            public object carnetConducirTrabajador { get; set; }
            public string paisNac { get; set; }
            public int? idTarifa { get; set; }
            public object idTurnos { get; set; }
            public object idCalendario { get; set; }
            public string fecAntig { get; set; }
            public int? categoriaTrabajador { get; set; }
            public double? bolsaHorasTrabajador { get; set; }
            public int activoTrabajador { get; set; }
            public string fecAltaTrabajador { get; set; }
            public object fecBajaTrabajador { get; set; }
            public string colorCalendarioTrabajador { get; set; }
            public object prefTelf { get; set; }
            public string gradoDiscapacidad { get; set; }
            public string email2Trabajador { get; set; }
            public object cuentaContableTrabajador { get; set; }
            public object rangoSueldoTrabajador { get; set; }
            public string nomCompleto { get; set; }
        }

        public class Geozone
        {
            public int idZonaGeo { get; set; }
            public int idEntidadZonaGeo { get; set; }
            public int codZonaGeo { get; set; }
            public string nomZonaGeo { get; set; }
            public int? autorZonaGeo { get; set; }
            public int activoZonaGeo { get; set; }
            public string fecAltaZonaGeo { get; set; }
        }

        public class Campaign
        {
            public int idCam { get; set; }
            public int idEntidadCam { get; set; }
            public int codCam { get; set; }
            public string nomCam { get; set; }
            public int? autorCam { get; set; }
            public int activoCam { get; set; }
            public string fecAltaCam { get; set; }
        }

        public class Datum
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int clienteAplicacionCli { get; set; }
            public int? idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public object diaPago2Cli { get; set; }
            public object diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public int porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public object cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public object descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
            public Type type { get; set; }
            public Responsible responsible { get; set; }
            public Geozone geozone { get; set; }
            public Campaign campaign { get; set; }
        }

        public class RootObject
        {
            public double total { get; set; }
            public int per_page { get; set; }
            public int current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
