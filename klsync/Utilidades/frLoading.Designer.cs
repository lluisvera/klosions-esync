﻿namespace klsync.Utilidades
{
    partial class frLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictLoading = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // pictLoading
            // 
            this.pictLoading.Image = global::klsync.Properties.Resources.loading;
            this.pictLoading.Location = new System.Drawing.Point(10, 7);
            this.pictLoading.Name = "pictLoading";
            this.pictLoading.Size = new System.Drawing.Size(135, 134);
            this.pictLoading.TabIndex = 0;
            this.pictLoading.TabStop = false;
            // 
            // frLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(152, 148);
            this.Controls.Add(this.pictLoading);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frLoading";
            this.Opacity = 0.7D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frLoading";
            this.Load += new System.EventHandler(this.frLoading_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictLoading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictLoading;
    }
}