﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Utilidades
{
    public class csRepasatUtilities
    {
        csSqlConnects sql = new csSqlConnects();
        public string obtenerTipoImpuesto(string idImpuesto)
        {
            try
            {
                string codImpuesto = "";
                codImpuesto = csGlobal.a3tiposIVA[idImpuesto];
                return codImpuesto;
            }
            catch
            {
                csUtilidades.registrarLogError("Error en asignacion de tipos de impuestos", "obtenerTipoImpuesto");
                return null;
            }
        }

        public string obtenerRegIVA(string idRegimen)
        {
            try
            {
                string codRegIVA = "";
                codRegIVA = csGlobal.a3RegimenesImpto[idRegimen];
                return codRegIVA;
            }
            catch
            {
                csUtilidades.registrarLogError("Error en asignacion de regimenes de IVA", "obtenerRegIVA");
                return null;
            }
        }

        public string obtenerProvincia(string provincia, string pais, string nomProvincia = null)
        {
            try
            {
                DataView dv = new DataView(csGlobal.dtProvinciasA3);
                dv.RowFilter = "RPST_ID_PROV=" + provincia;

                if (dv.Count > 0 || pais == "ES")
                {
                    string provinciaEdit = provincia;

                    if (csGlobal.modeAp.ToUpper() == "REPASAT" && provincia.Length == 1 && pais == "ES")
                    {
                        return provinciaEdit = "0" + provincia;
                    }
                    return provinciaEdit;
                }
                else
                {
                    bool resultado = csUtilidades.existeRegistroSQL("SELECT * FROM PROVINCI WHERE CODPROVI ='" + provincia + "';");

                    if (!resultado)
                    {
                        csUtilidades.ejecutarConsulta("INSERT INTO PROVINCI (CODPROVI, NOMPROVI) VALUES ('" + provincia + "','" + nomProvincia + "')", false);
                    }
                    return provincia;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string obtenerPais(string pais)
        {

            string paisERP = "";

            if (csGlobal.modeAp.ToUpper() == "REPASAT")
            {
                paisERP = sql.obtenerCampoTabla("SELECT CODPAIS FROM PAISES WHERE CODISO3166A3='" + pais + "'");
            }
            if (string.IsNullOrEmpty(paisERP))
            {
                paisERP = "ES";
            }

            return paisERP;
        }
    }
}
