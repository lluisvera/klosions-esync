﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace klsync.Utilidades
{
    class csLogErrores
    {
        public void guardarErrorFichero(string texto)
        {
            const string fic = @"errorLog.txt";
            System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true);
            swq.WriteLine("_____________________________________________________________ " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString() + " __________________________________________________________________________");
            swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString());
            swq.WriteLine();
            swq.WriteLine();
            swq.WriteLine();
            swq.Close();


            List<string> ccs = new List<string>();
            string asunto = "";
            string bodyMail = "";
            string adjunto = "";
            string from = "";
            string passwordMail = "";
            string servidorSMTP = "";



            ccs.Add("lluisvera@klosions.com");
            ccs.Add("ofi@repasat.com");
            if (csGlobal.emailNotificacionCliente != "")
            {
                ccs.Add(csGlobal.emailNotificacionCliente);
            }

            //Composición de Email
            asunto = "(ESYNC) ERROR";
            bodyMail = "NOMBRE DE CONEXIÓN:" + csGlobal.conexionDB + " \n" + "NOMBRE DE EQUIPO: (" + Environment.MachineName + ")" + " \n" + "Te envía la siguiente notificación: " + " \n" + texto;
            from = "notificaciones@repasat.com";
            passwordMail = "KL0S10N$";
            servidorSMTP = "mail.repasat.com";


            // csUtilidades.enviarMail(asunto,bodyMail, ccs,adjunto,"dev@klosions.com",from, passwordMail,servidorSMTP,false, false);

        }

        public void guardarLogProceso(string texto)
        {
            const string fic = @"LogProceso.txt";
            System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true);
            swq.WriteLine("______ " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString() + " _________");
            swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString());
            swq.WriteLine();
            swq.Close();

        }

        public void registrarError(Exception ex, string mensajePersonalizado)
        {
            string mensajeError = $"{mensajePersonalizado}: {ex}";
            guardarErrorFichero(mensajeError);
        }
    }
}
