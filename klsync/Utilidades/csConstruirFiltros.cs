﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Utilidades
{
    public static class csConstruirFiltros
    {
        public static string construirFiltroURL(bool clientes, bool consulta, string idAccount, bool checkDirecciones, bool pendientes, bool update, bool sincronizados, bool forceSync, string externalId)
        {
            string tipoCuenta = clientes ? "CLI" : "PRO";
            string filtroSync = consulta ? "" : "&filter[sincronizarCuenta]=1";
            string filtroCuentasNoSyncro = pendientes ? "&filter[codExternoCli]=" : "";
            string filtroEstadoSyncro = sincronizados ? "&filter_ne[codExternoCli]=" : "";
            string filtroTipoCuenta = "&filter[tipoCli]=" + tipoCuenta;
            string filtroApiKey = "&api_token=" + csGlobal.webServiceKey;
            string filtroFechaLatUpdate = "";

            if (update)
            {
                string fechaUltSync = new Repasat.csUpdateData().getLastUpdateSyncDate("accounts", true);
                filtroFechaLatUpdate = "&filter_gte[fecModificacionCli]=" + fechaUltSync;
            }

            if (forceSync)
            {
                if (string.IsNullOrEmpty(idAccount) && string.IsNullOrEmpty(externalId))
                {
                    filtroEstadoSyncro = "&filter_ne[codExternoCli]=";
                }
                else
                {
                    filtroEstadoSyncro = !string.IsNullOrEmpty(externalId) ? "&filter[codExternoCli]=" + externalId : "";
                }
            }

            if (!string.IsNullOrEmpty(idAccount))
            {
                filtroEstadoSyncro = update && !string.IsNullOrEmpty(idAccount) ? "&filter[codExternoCli]=" + idAccount + filtroTipoCuenta : "";
            }

            return filtroSync + filtroCuentasNoSyncro + filtroEstadoSyncro + filtroTipoCuenta + filtroFechaLatUpdate + filtroApiKey;
        }
    }
}