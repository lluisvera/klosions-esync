﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using klsync.Utilidades;
using klsync.Objetos;
using klsync.Repasat;

namespace klsync.Utilidades
{
    public class csMapearDatosClientes
    {
        csRepasatUtilities rpstUtil = new csRepasatUtilities();
        public DataTable crearDataTableCuentas(bool validationProcess, bool clientes)
        {
            var dtAccounts = new DataTable();
            dtAccounts.Columns.Add("IDREPASAT");
            dtAccounts.Columns.Add("NOMBRE");
            dtAccounts.Columns.Add("RAZON");
            dtAccounts.Columns.Add("NIF");
            if (csGlobal.activarUnidadesNegocio) dtAccounts.Columns.Add("CONEXIONES");
            if (validationProcess) dtAccounts.Columns.Add("CODCLI RPST");

            var colInt32 = new DataColumn("CODIGOERP") { DataType = System.Type.GetType("System.Int32") };
            dtAccounts.Columns.Add(colInt32);

            dtAccounts.Columns.Add("TIPOCUENTA");
            dtAccounts.Columns.Add("SYNC");
            dtAccounts.Columns.Add("CIAL");

            if (!clientes)
            {
                dtAccounts.Columns.Add("CTA CONTABLE");
                dtAccounts.Columns.Add("TIPO PROV");
            }

            return dtAccounts;
        }

        public DataTable crearDataTableDirecciones()
        {
            var dtAddresses = new DataTable();
            dtAddresses.Columns.Add("IDREPASAT");
            dtAddresses.Columns.Add("NOMBRE");
            dtAddresses.Columns.Add("DIRECCION");
            dtAddresses.Columns.Add("CP");
            dtAddresses.Columns.Add("POBLACION");
            dtAddresses.Columns.Add("EXTERNALID");
            dtAddresses.Columns.Add("PRINCIPAL");
            dtAddresses.Columns.Add("IDCUENTAA3");
            return dtAddresses;
        }

        public csTercero[] mapearCuentas(csAccounts.RootObject repasat, DataTable dtAccounts, DataTable dtAddresses, bool checkDirecciones, bool validationProcess, bool clientes, bool consulta, string idAccount, bool update, bool forceSync)
        {
            var accounts = new csTercero[repasat.data.Count];
            int totalLineas = 0, totalContactos = 0, numRegistro = 0, contador = 0, contadorContactos = 0;
            bool newCustomers = false;

            foreach (var item in repasat.data)
            {
                totalLineas += item.addresses.Count;
                totalContactos += item.contacts.Count;
            }

            var direcciones = new csDireccion[totalLineas];
            var contactos = new csContactos[totalContactos];

            foreach (var cliente in repasat.data)
            {
                if (esNuevoCliente(cliente, clientes, consulta, idAccount, update, forceSync))
                {
                    newCustomers = true;
                    var account = crearCuenta(cliente, validationProcess, clientes);
                    accounts[numRegistro] = account;

                    if (checkDirecciones)
                    {
                        mapearDirecciones(cliente, direcciones, ref contador, account);
                    }

                    mapearContactos(cliente, contactos, ref contadorContactos);
                    dtAccounts.Rows.Add(crearDataRowCliente(account, validationProcess, clientes, cliente, dtAccounts));
                    numRegistro++;
                }
            }

            return accounts;
        }

        public bool esNuevoCliente(csAccounts.Datum cliente, bool clientes, bool consulta, string idAccount, bool update, bool forceSync)
        {
            string tipoCuenta = clientes ? "CLI" : "PRO";
            return (string.IsNullOrEmpty(cliente.codExternoCli) && cliente.tipoCli == tipoCuenta) || !string.IsNullOrEmpty(idAccount) || consulta || update || forceSync;
        }

        public csTercero crearCuenta(csAccounts.Datum cliente, bool validationProcess, bool clientes)
        {
            var account = new csTercero
            {
                codIC = csGlobal.activarUnidadesNegocio ? obtenerCodIC(cliente) : cliente.codExternoCli,
                nombre = cliente.nomCli.ToUpper(),
                alias = string.IsNullOrWhiteSpace(cliente.aliasCli) ? "" : cliente.aliasCli.ToUpper(),
                apellidos = string.Empty,
                idRepasat = cliente.idCli.ToString(),
                email = cliente.emailCli,
                razonSocial = !string.IsNullOrEmpty(cliente.razonSocialCli) ? cliente.razonSocialCli.ToUpper() : cliente.nomCli.ToUpper(),
                telf = cliente.tel1,
                telf2 = cliente.tel2,
                fax = cliente.fax,
                web = cliente.webCli,
                nifcif = cliente.cifCli,
                active_Status = cliente.activoCli.ToString(),
                tipoCliente = cliente.tipoCli,
                id_customer_Ext = cliente.codCli.ToString(),
                repasatID = cliente.idCli.ToString(),
                diaPago1 = cliente.diaPago1Cli.ToString(),
                diaPago2 = cliente.diaPago2Cli.ToString(),
                diaPago3 = cliente.diaPago3Cli.ToString(),
                codRepasat = cliente.codCli.ToString(),
                ruta = string.IsNullOrWhiteSpace(cliente.idRuta.ToString()) ? "" : cliente.route.codExternoRuta.ToString(),
                zona = string.IsNullOrWhiteSpace(cliente.idZonaGeo.ToString()) ? "" : cliente.geozone.codExternoZonaGeo.ToString(),
                cuentaContable = string.IsNullOrWhiteSpace(cliente.idCuentaContable) ? "" : cliente.accounting_account.numCuentaContable.ToString(),
                cuentaGenerica = cliente.clienteGenerico.ToString(),
                tipoProveedor = cliente.tipoCliPro.ToString(),
                codFormaPago = string.IsNullOrWhiteSpace(cliente.idFormaPago.ToString()) ? "" : cliente.payment_method.codExternoFormaPago.ToString(),
                codDocPago = string.IsNullOrWhiteSpace(cliente.idDocuPago.ToString()) ? "" : cliente.payment_document.codExternoDocuPago.ToString(),
                moneda = "EURO",
                regimenIva = rpstUtil.obtenerRegIVA(cliente.idRegimenImpuesto.ToString())
            };

            if (!string.IsNullOrEmpty(account.cuentaContable) && string.IsNullOrEmpty(account.codIC) && account.cuentaContable.Length > 5)
            {
                account.codIC = obtenerCodICFromCuentaContable(account.cuentaContable);
            }

            return account;
        }

        private string obtenerCodIC(csAccounts.Datum cliente)
        {
            foreach (var conexion in cliente.conexion_externa)
            {
                if (conexion.idConExt.ToString() == csGlobal.id_conexion_externa)
                {
                    return conexion.idExterno.ToString();
                }
            }

            return string.Empty;
        }

        private string obtenerCodICFromCuentaContable(string cuentaContable)
        {
            cuentaContable = cuentaContable.Substring(3, cuentaContable.Length - 3);
            return Convert.ToInt32(cuentaContable).ToString();
        }

        public void mapearDirecciones(csAccounts.Datum cliente, csDireccion[] direcciones, ref int contador, csTercero account)
        {
            foreach (var direccion in cliente.addresses)
            {
                var direccionObj = new csDireccion
                {
                    nombreDireccion = direccion.nomDireccion,
                    direccion1 = direccion.direccion1Direccion,
                    direccion2 = direccion.direccion2Direccion,
                    poblacion = direccion.poblacionDireccion,
                    codigoPostal = direccion.cpDireccion,
                    pais = obtenerPais(direccion.idPais),
                    codProvincia = obtenerProvincia(direccion.idProvincia?.ToString(), direccion.idPais.Substring(0, 2)),
                    idClienteDireccionRepasat = cliente.idCli.ToString(),
                    idDireccionRepasat = direccion.idDireccion.ToString(),
                    direccionfiscal = direccion.pivot.facturacionDireccion,
                    codExternoDireccion = direccion.codExternoDireccion
                };

                if (direccion.pivot.facturacionDireccion)
                {
                    account.direccion = direccion.direccion1Direccion;
                    account.poblacion = direccion.poblacionDireccion;
                    account.codigoPostal = direccion.cpDireccion;
                    account.codprovincia = obtenerProvincia(direccion.idProvincia?.ToString(), direccion.idPais.Substring(0, 2));
                    account.codPais = obtenerPais(direccion.idPais);
                }

                direcciones[contador++] = direccionObj;
            }
        }

        public void mapearContactos(csAccounts.Datum cliente, csContactos[] contactos, ref int contadorContactos)
        {
            foreach (var contacto in cliente.contacts)
            {
                contactos[contadorContactos++] = new csContactos
                {
                    nombre = contacto.nombreContacto,
                    apellidos = contacto.apellidosContacto,
                    telf1 = contacto.telf1Contacto,
                    telf2 = contacto.telf2Contacto,
                    email = contacto.emailContacto,
                    codExternoContacto = contacto.codExternoContacto,
                    idClienteRepasat = cliente.idCli.ToString(),
                    idContactoRepasat = contacto.idContacto.ToString(),
                    razonSocialCuenta = cliente.razonSocialCli,
                    codClienteERP = cliente.codExternoCli
                };
            }
        }

        public DataRow crearDataRowCliente(csTercero account, bool validationProcess, bool clientes, csAccounts.Datum cliente, DataTable dtAccounts)
        {
            var filaCustomer = dtAccounts.NewRow();
            filaCustomer["IDREPASAT"] = cliente.idCli.ToString();
            filaCustomer["NOMBRE"] = cliente.nomCli;
            filaCustomer["RAZON"] = cliente.razonSocialCli;
            filaCustomer["NIF"] = cliente.cifCli;
            if (validationProcess) filaCustomer["CODCLI RPST"] = cliente.codCli;
            if (csGlobal.activarUnidadesNegocio) filaCustomer["CONEXIONES"] = cliente.conexion_externa.Count;
            filaCustomer["CODIGOERP"] = double.TryParse(account.codIC, out double numericValue) ? account.codIC : "99999";
            filaCustomer["TIPOCUENTA"] = cliente.tipoCli;
            filaCustomer["SYNC"] = cliente.sincronizarCuenta ? "SI" : "NO";
            filaCustomer["CIAL"] = cliente.idTrabajador == null ? "" : cliente.responsible.codExternoTrabajador + " | " + cliente.responsible.nomCompleto;
            if (!clientes)
            {
                filaCustomer["CTA CONTABLE"] = account.cuentaContable;
                filaCustomer["TIPO PROV"] = cliente.tipoCliPro;
            }

            return filaCustomer;
        }

        public csDireccion[] mapearDireccionesDataTable(DataTable dtAddresses)
        {
            return dtAddresses.Rows.Cast<DataRow>().Select(row => new csDireccion
            {
                nombreDireccion = row["NOMBRE"].ToString(),
                direccion1 = row["DIRECCION"].ToString(),
                codigoPostal = row["CP"].ToString(),
                poblacion = row["POBLACION"].ToString(),
                codExternoDireccion = row["EXTERNALID"].ToString(),
                direccionfiscal = Convert.ToBoolean(row["PRINCIPAL"]),
                idClienteDireccionRepasat = row["IDREPASAT"].ToString(),
                idDireccionRepasat = row["IDCUENTAA3"].ToString()
            }).ToArray();
        }

        private string obtenerPais(string idPais)
        {
            return idPais == null ? "ESP" : idPais.Substring(0, 2);
        }

        private string obtenerProvincia(string idProvincia, string pais)
        {
            return idProvincia != null ? rpstUtil.obtenerProvincia(idProvincia, pais) : null;
        }
    }
}