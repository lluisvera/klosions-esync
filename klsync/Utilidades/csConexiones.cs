﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Utilidades
{
    class csConexiones
    {
        private void connectEsyncDB()
        {

        }

        public string cadenaConexionEsyncDB()
        {
            string servidor = "esync.es"; //Nombre o ip del servidor de MySQL
            string bd = "configesync"; //Nombre de la base de datos
            string usuario = "configesyncuser"; //Usuario de acceso a MySQL
            string password = "Kl0s10ns2014"; //Contraseña de usuario de acceso a MySQL
            string datos = null; //Variable para almacenar el resultado
            string allowDateTime = "True";
            string port = "3306";

            //Crearemos la cadena de conexión concatenando las variables
            string connection_string = "Database=" + bd + "; Port=" + port + "; Data Source=" + servidor + "; User Id=" + usuario + "; Password=" + password + ";Allow Zero Datetime=" + allowDateTime + "";

            return connection_string;
        }

        public static bool verificarConexionRepasatOPrestaShop()
        {
            try
            {
                if (csGlobal.modeAp.ToUpper() == "REPASAT")
                {
                    var mySqlConnect = new csMySqlConnect();
                    return mySqlConnect.probarConexionRepasatOPresta(); 
                }
                else if (csGlobal.modeAp.ToUpper() == "ESYNC")
                {
                    var mySqlConnect = new csMySqlConnect();
                    return mySqlConnect.probarConexionRepasatOPresta();
                }
                else
                {
                    throw new InvalidOperationException("Modo de aplicación no reconocido.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al conectar: {ex.Message}");
                return false;
            }
        }

        public static bool verificarConexionA3erp()
        {
            try
            {
                var SqlConnect = new csSqlConnects();
                return SqlConnect.probarConexionA3();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al conectar con A3ERP: {ex.Message}");
                return false;
            }
        }




        public void TestConexion()
        {
            try
            {
                string datos = null; //Variable para almacenar el resultado
                
                //Instancia para conexión a MySQL, recibe la cadena de conexión
                MySqlConnection conexionBD = new MySqlConnection(cadenaConexionEsyncDB());
                MySqlDataReader reader = null; //Variable para leer el resultado de la consulta

                //Agregamos try-catch para capturar posibles errores de conexión o sintaxis.
                try
                {
                    string consulta = "SHOW DATABASES"; //Consulta a MySQL (Muestra las bases de datos que tiene el servidor)
                    MySqlCommand comando = new MySqlCommand(consulta); //Declaración SQL para ejecutar contra una base de datos MySQL
                    comando.Connection = conexionBD; //Establece la MySqlConnection utilizada por esta instancia de MySqlCommand
                    conexionBD.Open(); //Abre la conexión
                    reader = comando.ExecuteReader(); //Ejecuta la consulta y crea un MySqlDataReader

                    while (reader.Read()) //Avanza MySqlDataReader al siguiente registro
                    {
                        datos += reader.GetString(0) + "\n"; //Almacena cada registro con un salto de linea
                    }

                    MessageBox.Show(datos); //Imprime en cuadro de dialogo el resultado
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show(ex.Message); //Si existe un error aquí muestra el mensaje
                }
                finally
                {
                    conexionBD.Close(); //Cierra la conexión a MySQL
                }
            }
            catch
            {

            }
        }
    }
}
