﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Utilidades
{
    public partial class frLoading : Form
    {
        public frLoading()
        {
            InitializeComponent();
        }

        private void frLoading_Load(object sender, EventArgs e)
        {
            pictLoading.Load("Resources//loading.gif");
            pictLoading.Location = new Point(this.Width/2 - pictLoading.Width/2, this.Height/2 - pictLoading.Height/2);
        }
    }
}
