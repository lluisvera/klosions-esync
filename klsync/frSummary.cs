﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace klsync
{
    public partial class frSummary : Form
    {

        private static frSummary m_FormDefInstance;
        public static frSummary DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSummary();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frSummary()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;

            loadSummaryA3();
            loadSummaryPS();
        }
        

        private void toolStripButtonA3_Click(object sender, EventArgs e)
        {
            treeViewSummaryA3.Nodes.Clear();
            loadSummaryA3();
        }

        private void toolStripButtonPS_Click(object sender, EventArgs e)
        {
            treeViewSummaryPS.Nodes.Clear(); 
            loadSummaryPS();
        }

        private void toolStripButtonReload_Click(object sender, EventArgs e)
        {
            treeViewSummaryA3.Nodes.Clear();
            treeViewSummaryPS.Nodes.Clear();
            loadSummaryA3();
            loadSummaryPS();
        }

        private void loadSummaryA3()
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                TreeNode t = new TreeNode();

                t.Text = "Resumen A3";
                t.Nodes.Add("Articulos: " + sql.contarTabla("ARTICULO"));
                t.Nodes[0].Nodes.Add("Bloqueados: " + sql.contarTabla("ARTICULO", " where BLOQUEADO = 'T'"));
                t.Nodes.Add("Clientes: " + sql.contarTabla("__CLIENTES"));
                t.Nodes[1].Nodes.Add("Bloqueados: " + sql.contarTabla("__CLIENTES", " where BLOQUEADO = 'T'"));
                t.Nodes.Add("Proveedores: " + sql.contarTabla("__PROVEED"));
                t.Nodes[2].Nodes.Add("Bloqueados: " + sql.contarTabla("__PROVEED", " where BLOQUEADO = 'T'"));
                t.Nodes.Add("Pedidos");
                t.Nodes[3].Nodes.Add("Compra: " + sql.contarTabla("CABEPEDC"));
                t.Nodes[3].Nodes.Add("Venta: " + sql.contarTabla("CABEPEDV"));
                t.Nodes.Add("Facturas");
                t.Nodes[4].Nodes.Add("Compra: " + sql.contarTabla("CABEFACC"));
                t.Nodes[4].Nodes.Add("Venta: " + sql.contarTabla("CABEFACV"));
                t.Nodes.Add("Albaranes");
                t.Nodes[5].Nodes.Add("Compra: " + sql.contarTabla("CABEALBC"));
                t.Nodes[5].Nodes.Add("Venta: " + sql.contarTabla("CABEALBV"));
                treeViewSummaryA3.Nodes.Add(t);
                treeViewSummaryA3.ExpandAll();
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void loadSummaryPS()
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                TreeNode t = new TreeNode();

                t.Text = "Resumen Prestashop";
                t.Nodes.Add("Productos: " + mysql.contarTabla("ps_product"));
                t.Nodes[0].Nodes.Add("Activos: " + mysql.contarTabla("ps_product", " where active = 1"));
                t.Nodes[0].Nodes.Add("Inactivos: " + mysql.contarTabla("ps_product", " where active = 0"));
                t.Nodes.Add("Clientes: " + mysql.contarTabla("ps_customer"));
                t.Nodes[1].Nodes.Add("Activos: " + mysql.contarTabla("ps_customer", " where active = 1"));
                t.Nodes[1].Nodes.Add("Inactivos: " + mysql.contarTabla("ps_customer", " where active = 0"));
                t.Nodes.Add("Proveedores: " + mysql.contarTabla("ps_supplier"));
                t.Nodes[2].Nodes.Add("Activos: " + mysql.contarTabla("ps_supplier", " where active = 1"));
                t.Nodes[2].Nodes.Add("Inactivos: " + mysql.contarTabla("ps_supplier", " where active = 0"));
                t.Nodes.Add("Pedidos: " + mysql.contarTabla("ps_orders"));
                t.Nodes.Add("Facturas: " + mysql.contarTabla("ps_order_invoice"));
                
                treeViewSummaryPS.Nodes.Add(t);
                treeViewSummaryPS.ExpandAll();
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }
    }
}
