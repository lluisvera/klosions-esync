﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Web;

namespace klsync
{
    public partial class frVilardell : Form
    {

        csSqlConnects sql = new csSqlConnects();
        csMySqlConnect mysql = new csMySqlConnect();
        private static frVilardell m_FormDefInstance;

        ToolStripButton btSyncA3ERP = new ToolStripButton();
        ToolStripButton btOfertasA3ERP = new ToolStripButton();

        public static frVilardell DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frVilardell();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frVilardell()
        {
            InitializeComponent();

            //// Inicializo los componentes del ToolStrip
            //ToolStripButton btLoad = new ToolStripButton();
            ////btLoad.Image = klsync.Properties.Resources.loadData;
            //btLoad.Text = "Cargar Datos";
            //btLoad.TextImageRelation = TextImageRelation.ImageAboveText;
            //btLoad.BackColor = Color.Transparent;
            //btLoad.ImageScaling = ToolStripItemImageScaling.None;
            //btLoad.Click += new EventHandler(cargarDatosPedido); // !

            ////btSyncA3ERP.Image = klsync.Properties.Resources.syncA3ERP;
            //btSyncA3ERP.Text = "Crear pedido";
            //btSyncA3ERP.TextImageRelation = TextImageRelation.ImageAboveText;
            //btSyncA3ERP.BackColor = Color.Transparent;
            //btSyncA3ERP.ImageScaling = ToolStripItemImageScaling.None;
            //btSyncA3ERP.Click += new EventHandler(crearFicheroPedido);

            ////btOfertasA3ERP.Image = klsync.Properties.Resources.syncA3ERP;
            //btOfertasA3ERP.Text = "Bajar ofertas";
            //btOfertasA3ERP.TextImageRelation = TextImageRelation.ImageAboveText;
            //btOfertasA3ERP.BackColor = Color.Transparent;
            //btOfertasA3ERP.ImageScaling = ToolStripItemImageScaling.None;
            //btOfertasA3ERP.Click += new EventHandler(crearFicheroOfertas);

            //ToolStripSeparator Separator1 = new ToolStripSeparator();
            //ToolStripSeparator Separator2 = new ToolStripSeparator();
            //ToolStripSeparator Separator3 = new ToolStripSeparator();
            //ToolStripSeparator Separator4 = new ToolStripSeparator();
            //ToolStripSeparator Separator5 = new ToolStripSeparator();
            //ToolStripSeparator Separator6 = new ToolStripSeparator();
            //ToolStripSeparator Separator7 = new ToolStripSeparator();

            //toolStripPedidos.Items.Add(btLoad);
            //toolStripPedidos.Items.Add(btSyncA3ERP);
            //toolStripPedidos.Items.Add(btOfertasA3ERP);
        }

        private void toolStripButtonLoadArticulos_Click(object sender, EventArgs e)
        {
            //csUtilidades.addDataSource(dgvArticulos, sql.cargarDatosTablaA3("select descripcion, articuloid, UnidadesPorBultoPrimario from e_articulo where descripcion like '%SUPOSITORIOS GLICERINA VILARDELL %'"));
        }

        private void toolStripButtonLoadClientes_Click(object sender, EventArgs e)
        {
            cargarClientes();
        }



        private void toolStripButtonLoadPedidos_Click(object sender, EventArgs e)
        {
            string query = "SELECT ps_order_detail.id_order_detail, " +
                        "       ps_orders.id_order, " +
                        "       ps_orders.id_customer, " +
                        "       ps_customer.firstname, " +
                        "       ps_customer.lastname, " +
                        "       ps_customer.company, " +
                        "       ps_customer.email, " +
                        "       ps_order_detail.product_reference, " +
                        "       ps_orders.total_paid, " +
                        "       ps_orders.total_products, " +
                        "       ps_orders.invoice_date, " +
                        "       ps_orders.id_address_delivery, " +
                        "       ps_address.id_country, " +
                        "       ps_address.id_state, " +
                        "       ps_address.city, " +
                        "       ps_address.address1, " +
                        "       ps_address.address2, " +
                        "       ps_address.postcode, " +
                        "       ps_address.phone, " +
                        "       ps_address.dni " +
                        "FROM ps_orders " +
                        "LEFT JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer " +
                        "LEFT JOIN ps_address ON ps_address.id_address = ps_orders.id_address_delivery " +
                        "LEFT JOIN ps_order_detail ON ps_order_detail.id_order = ps_orders.id_order " +
                        "ORDER BY id_order ASC ";



            // PENDIENTE - MIRAR EPPLUS
            // http://epplus.codeplex.com/wikipage?title=ContentSheetExample
            // Extra: https://stackoverflow.com/questions/151005/create-excel-xls-and-xlsx-file-from-c-sharp/2603625#2603625
        }

        /* ACCIONES */

        // CARGAR DATOS PEDIDOS
        public void cargarDatosPedido(object sender, EventArgs e)
        {
            cdPedido();
        }

        private void cdPedido()  //2
        {
            csUtilidades.addDataSource(dgvPedidos, mysql.cargarTabla(selectDocsVilardell(rbutDocsAll.Checked, rbutDocStatusPend.Checked, rbutDocsAll.Checked, rbutDocDirect.Checked, rbutDocsTransfer.Checked)));
        }


        private string selectDocsVilardell(bool todosPedidos, bool pedidosPendientes, bool todosTiposPedidos, bool PedidosDirectos, bool pedidosTransfer)
        {
            string querySelectDocs = "";

            string situacionPedido = "";
            string tipoPedido = "";
            int condiciones = 0;
            string condicionWhere = "";

            //Valido si cargo todos los pedidos o sólo los pendientes
            if (todosPedidos)
            {
                situacionPedido = "";

            }

            if (pedidosPendientes)
            {
                situacionPedido = " ps_orders.kls_checked IS NULL and kls_a3erp_id IS NOT NULL and current_state=3  ";
                condiciones++;
                condicionWhere = "where " + situacionPedido;
            }

            //Valido el tipo de Pedido que debo cargar
            if (todosTiposPedidos)
            {
                tipoPedido = "";
            }
            if (PedidosDirectos)
            {
                tipoPedido = " order_type = 'DIRECT'  ";
                condiciones++;
            }
            if (pedidosTransfer)
            {
                tipoPedido = " order_type = 'TRANSFER'  ";
                condiciones++;
            }

            //Construyo el Where

            if (condiciones == 2)
            {
                condicionWhere = condicionWhere + " and " + tipoPedido;

            }
            else if (condiciones == 1 && !condicionWhere.Contains("where"))
            {
                condicionWhere = " where " + tipoPedido;
            }

            //string queryDescuentoLinea = " (select ps_orders.id_order,ps_order_detail.id_order_detail,ps_order_cart_rule.id_order_cart_rule," +
            //    " ps_order_cart_rule.id_cart_rule,product_id, ps_cart_rule.reduction_percent,ps_category_product.id_category " +
            //    " from ps_order_detail left outer join ps_orders on ps_order_detail.id_order = ps_orders.id_order " +
            //    " left outer join ps_category_product on ps_order_detail.product_id = ps_category_product.id_product " +
            //    " left outer join ps_order_cart_rule on ps_orders.id_order = ps_order_cart_rule.id_order " +
            //    " left join ps_cart_rule on ps_cart_rule.id_cart_rule = ps_order_cart_rule.id_cart_rule " +
            //    " left join ps_cart_rule_product_rule_value on ps_cart_rule_product_rule_value.id_item = ps_category_product.id_category " +
            //    " left join ps_cart_rule_product_rule on ps_cart_rule_product_rule.id_product_rule = ps_cart_rule_product_rule_value.id_product_rule " +
            //    " inner join ps_cart_rule_product_rule_group on ps_cart_rule_product_rule_group.id_product_rule_group = ps_cart_rule_product_rule.id_product_rule_group " +
            //    " and ps_cart_rule_product_rule_group.id_cart_rule = ps_order_cart_rule.id_cart_rule " +
            //    " and ps_cart_rule_product_rule_group.id_cart_rule = ps_cart_rule.id_cart_rule " +
            //    " order by ps_orders.id_order,ps_order_detail.id_order_detail) as v on ps_order_detail.id_order_detail = v.id_order_detail ";

            //Lanzo la Query
            string query = "SELECT ps_order_detail.id_order_detail, " +
                        "       ps_orders.id_order, " +
                        //"      ps_orders.invoice_date as order_date, " +
                        //"      date_format(ps_orders.date_add,'%d/%m/%Y') as order_date, " +
                        "       date_format(ps_orders.date_add,'%Y/%m/%d') as order_date, " +
                        "       ps_orders.kls_checked, " +
                        "       ps_orders.id_customer, ps_customer.kls_a3erp_id, " +
                        "       ps_customer.customer_type, " +
                        "       ps_orders.order_type, " +
                        "       ps_customer.firstname, " +
                        "       ps_customer.lastname, " +
                        "       ps_customer.company, " +
                        "       ps_customer.email, " +
                        "       ps_customer.repre_cli, " +
                        "       ps_order_detail.product_reference, " +
                        "       ps_order_detail.product_name, " +
                        "       ps_order_detail.product_quantity, " +
                        "       ps_order_detail.unit_price_tax_excl as precio, " +
                        "       ps_orders.total_discounts, " +
                        "       ps_orders.total_paid, " +
                        "       ps_orders.id_wholesaler as mayorista, " +
                        "       ps_orders.payment_fee_percent, " +
                        "       ps_orders.total_products, " +
                        "       ps_orders.id_address_delivery, " +
                        "       ps_address.kls_id_dirent as dirEntrega, " +
                        "       '' as 'FormaPago', " +
                        "       '0' as 'Portes', " +
                        //"       ps_orders.total_discounts AS 'descuento_comercial_cabecera', " +
                        "       'CIAL' AS 'descuento_comercial_cabecera', " +
                        "       ps_orders.total_products_wt AS 'descuento_comercial_cabecera_valor', " +
                        //"     ps_order_detail.reduction_amount as 'descuento_comercial_linea', " +
                        "       CASE WHEN  ps_order_detail.descuento=0 THEN '' ELSE 'CIAL' END AS 'descuento_comercial_linea'," +
                        //"     ps_order_detail.reduction_percent as 'descuento_comercial_linea_valor', " +
                        //"     v.reduction_percent as 'descuento_comercial_linea_valor', " +
                        "       CASE WHEN ps_order_detail.descuento=0 THEN '' ELSE ps_order_detail.descuento END as 'descuento_comercial_linea_valor', " +
                        "       '' as 'descuento_pronto_pago', " +              //'CIAL' as 'descuento_pronto_pago', " 
                        "       '' as 'descuento_pronto_pago_valor', " +        //'0' as 'descuento_pronto_pago_valor', " 
                        "       '' as 'descuento_financiero', " +               //'CIAL' as 'descuento_financiero', " 
                        "       '' as 'descuento_financiero_valor', " +         //'0' as 'descuento_financiero_valor', " 
                        "       ps_country.iso_code as pais_iso_code, " +
                        "       ps_state.iso_code as provincia_iso_code, " +
                        "       ps_address.city as ciudad, " +
                        "       ps_address.address1 as dir1, " +
                        "       ps_address.address2 as dir2, " +
                        "       ps_address.postcode as codigopostal, " +
                        "       ps_address.phone as tel, " +
                        "       ps_address.dni, " +
                        // "       v.reduction_percent,  " +
                        "       ps_order_detail.descuento, " +
                        "       ps_orders.sent_order_to_wholesaler,  " +
                        "       ps_orders.date_sent_order_to_wholesaler,  " +
                        "       ps_orders.wholesaler_customer_number as refParaMayorista " +
                        "FROM ps_orders " +
                        "JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer " +
                        "LEFT JOIN ps_address ON ps_address.id_address = ps_orders.id_address_delivery " +
                        "LEFT JOIN ps_order_detail ON ps_order_detail.id_order = ps_orders.id_order " +
                        "LEFT JOIN ps_country ON ps_country.id_country = ps_address.id_country " +
                        "LEFT JOIN ps_state ON ps_state.id_state = ps_address.id_state " +
                        //añado función para calcular descuento de linea
                        //" LEFT JOIN " + //queryDescuentoLinea +
                        condicionWhere +
                        "ORDER BY id_order ASC ";

            return query;
        }
        public string selectDevoluciones() {
            string query = 
              "  select ps_order_detail.id_order_detail, " +
              "  ps_orders.kls_checked, "+
              "  ps_orders.id_order, "+
              "  date_format(ps_orders.date_add, '%Y/%m/%d') as order_date, "+ 
              "  ps_customer.id_customer, "+
              "  ps_customer.kls_a3erp_id, "+
              "  ps_customer.customer_type,"+
              "  ps_orders.order_type,"+
              "  ps_customer.firstname,"+
              "  ps_customer.lastname,"+
              "  ps_customer.company, "+
              "  ps_customer.email," +
              "  ps_customer.repre_cli,"+
              "  ps_order_detail.product_reference, " +
              "  ps_order_detail.product_name," +
              "  ps_velsof_rm_order.quantity * -1 as product_quantity,"+
              "  ps_order_detail.unit_price_tax_excl as precio,"+
              "  ps_orders.total_discounts,"+
              "  ps_orders.total_paid,"+
              "  ps_orders.id_wholesaler as mayorista,"+
              "  ps_orders.payment_fee_percent,"+
              "  ps_orders.total_products,"+
              "  ps_orders.id_address_delivery, "+
              "  ps_address.kls_id_dirent as dirEntrega, "+
              "  '' as 'FormaPago', "+
              "  '0' as 'Portes',"+ 
              "  'CIAL' AS 'descuento_comercial_cabecera',"+
              "  ps_orders.total_products_wt AS 'descuento_comercial_cabecera_valor',"+
              "  CASE WHEN  ps_order_detail.descuento = 0 THEN '' ELSE 'CIAL' END AS 'descuento_comercial_linea',"+
              "  CASE WHEN ps_order_detail.descuento = 0 THEN '' ELSE ps_order_detail.descuento END as 'descuento_comercial_linea_valor',"+
              "  '' as 'descuento_pronto_pago',"+
              "  '' as 'descuento_pronto_pago_valor',"+
              "  '' as 'descuento_financiero',"+
              "  '' as 'descuento_financiero_valor',"+
              "  ps_country.iso_code as pais_iso_code,"+
              "  ps_state.iso_code as provincia_iso_code,"+
              "  ps_address.city as ciudad,"+
              "  ps_address.address1 as dir1,"+
              "  ps_address.address2 as dir2,"+
              "  ps_address.postcode as codigopostal,"+
              "  ps_address.phone as tel,"+
              "  ps_address.dni,"+
              "  ps_order_detail.descuento,"+
              "  ps_orders.sent_order_to_wholesaler,"+
              "  ps_orders.date_sent_order_to_wholesaler,"+
              "  ps_orders.wholesaler_customer_number as refParaMayorista"+
              "  from ps_velsof_rm_order"+
              "  inner join ps_customer on ps_customer.id_customer = ps_velsof_rm_order.id_customer"+
              "  left"+
              "  join ps_orders on ps_orders.id_order = ps_velsof_rm_order.id_order"+
              "  left"+
              "  join ps_address on ps_address.id_address = ps_orders.id_address_delivery"+
              "  left"+
              "  join ps_order_detail on ps_order_detail.id_order_detail = ps_velsof_rm_order.id_order_detail" +
              "  left"+
              "  join ps_country on ps_country.id_country = ps_address.id_address"+
              "  left "+
              "  join ps_state on ps_state.id_state = ps_address.id_state "+
              "  WHERE ps_velsof_rm_order.active=2 AND (ps_velsof_rm_order.kls_rm_checked IS NULL || ps_velsof_rm_order.kls_rm_checked=0) ";

            return query;
        }
        public void marcarDevoluciones()
        {
            mysql.ejecutarConsulta("update ps_velsof_rm_order set kls_rm_checked=1 where active=2 and (kls_rm_checked=0 or kls_rm_checked IS NULL)");
        }
        public void crearFicheroOfertas()
        {
            try
            {

                DataTable dt = mysql.cargarTabla(selectDocsVilardell(false, true, false, false, true));

                DataTable tablaDevoluciones = mysql.cargarTabla(selectDevoluciones());
               

                DataTable dtv = new DataTable();

                //Creo un Dataview para los pedidos transfer a los que hay que enviar email con los pedidos
                DataView dv = new DataView(dt);
                dv.RowFilter = "sent_order_to_wholesaler=0";

                dtv = dv.ToTable(true, "id_customer", "id_order", "company", "dir1", "codigopostal", "ciudad", "tel", "dni", "email", "provincia_iso_code", "refParaMayorista", "mayorista");

                if (dt != null)
                {
                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["product_reference"].ToString().Contains("."))
                        {
                            dt.Rows[i]["product_reference"] = "'" + dr["product_reference"];

                        }
                        string totalDescuentoDocumento = dr["total_discounts"].ToString();
                        if (Convert.ToDouble(dr["total_discounts"].ToString()) > 0)
                        {
                            string DescuentoComercialLinea = obtenerDescuentoLinea(dr["id_order"].ToString());
                            dt.Rows[i]["descuento_comercial_linea_valor"] = DescuentoComercialLinea;
                            dt.Rows[i]["descuento_comercial_cabecera_valor"] = "0";
                        }


                        i++;
                    }

                    //Envío por mail
                    if (dtv.hasRows())
                    {
                        enviarDocsMayorista(dtv, dt);
                    }

                    if (dt.hasRows())
                    {
                        DataTable tablaClonada = dt.Clone();
                        tablaClonada.Columns["product_quantity"].DataType = typeof(Int64);

                        foreach (DataRow row in dt.Rows)
                        {
                            tablaClonada.ImportRow(row);
                        }


                        tablaClonada.Merge(tablaDevoluciones);
                       
                        string fechaHoy = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
                        tablaClonada.exportToExcel("ofertas");

                        if (csGlobal.modoManual)
                        {
                            csUtilidades.openFolder(csGlobal.fileExportPath);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);

            }
        }

        private string selectDevVilardell(bool todosPedidos, bool pedidosPendientes, bool todosTiposPedidos, bool PedidosDirectos, bool pedidosTransfer)
        {
            string querySelectDocs = "";

            string situacionPedido = "";
            string tipoPedido = "";
            int condiciones = 0;
            string condicionWhere = "";

           
            string query = "SELECT ps_order_detail.id_order_detail, " +
                        "       ps_orders.id_order, " +
                        //"      ps_orders.invoice_date as order_date, " +
                        //"      date_format(ps_orders.date_add,'%d/%m/%Y') as order_date, " +
                        "       date_format(ps_orders.date_add,'%Y/%m/%d') as order_date, " +
                        "       ps_orders.kls_checked, " +
                        "       ps_orders.id_customer, ps_customer.kls_a3erp_id, " +
                        "       ps_customer.customer_type, " +
                        "       ps_orders.order_type, " +
                        "       ps_customer.firstname, " +
                        "       ps_customer.lastname, " +
                        "       ps_customer.company, " +
                        "       ps_customer.email, " +
                        "       ps_customer.repre_cli, " +
                        "       ps_order_detail.product_reference, " +
                        "       ps_order_detail.product_name, " +
                        "       ps_order_detail.product_quantity, " +
                        "       ps_order_detail.unit_price_tax_excl as precio, " +
                        "       ps_orders.total_discounts, " +
                        "       ps_orders.total_paid, " +
                        "       ps_orders.id_wholesaler as mayorista, " +
                        "       ps_orders.payment_fee_percent, " +
                        "       ps_orders.total_products, " +
                        "       ps_orders.id_address_delivery, " +
                        "       ps_address.kls_id_dirent as dirEntrega, " +
                        "       '' as 'FormaPago', " +
                        "       '0' as 'Portes', " +
                        //"       ps_orders.total_discounts AS 'descuento_comercial_cabecera', " +
                        "       'CIAL' AS 'descuento_comercial_cabecera', " +
                        "       ps_orders.total_products_wt AS 'descuento_comercial_cabecera_valor', " +
                        //"     ps_order_detail.reduction_amount as 'descuento_comercial_linea', " +
                        "       CASE WHEN  ps_order_detail.descuento=0 THEN '' ELSE 'CIAL' END AS 'descuento_comercial_linea'," +
                        //"     ps_order_detail.reduction_percent as 'descuento_comercial_linea_valor', " +
                        //"     v.reduction_percent as 'descuento_comercial_linea_valor', " +
                        "       CASE WHEN ps_order_detail.descuento=0 THEN '' ELSE ps_order_detail.descuento END as 'descuento_comercial_linea_valor', " +
                        "       '' as 'descuento_pronto_pago', " +              //'CIAL' as 'descuento_pronto_pago', " 
                        "       '' as 'descuento_pronto_pago_valor', " +        //'0' as 'descuento_pronto_pago_valor', " 
                        "       '' as 'descuento_financiero', " +               //'CIAL' as 'descuento_financiero', " 
                        "       '' as 'descuento_financiero_valor', " +         //'0' as 'descuento_financiero_valor', " 
                        "       ps_country.iso_code as pais_iso_code, " +
                        "       ps_state.iso_code as provincia_iso_code, " +
                        "       ps_address.city as ciudad, " +
                        "       ps_address.address1 as dir1, " +
                        "       ps_address.address2 as dir2, " +
                        "       ps_address.postcode as codigopostal, " +
                        "       ps_address.phone as tel, " +
                        "       ps_address.dni, " +
                        // "       v.reduction_percent,  " +
                        "       ps_order_detail.descuento, " +
                        "       ps_orders.sent_order_to_wholesaler,  " +
                        "       ps_orders.date_sent_order_to_wholesaler,  " +
                        "       ps_orders.wholesaler_customer_number as refParaMayorista " +
                        "FROM ps_orders " +
                        "JOIN ps_customer ON ps_customer.id_customer = ps_orders.id_customer " +
                        "LEFT JOIN ps_address ON ps_address.id_address = ps_orders.id_address_delivery " +
                        "LEFT JOIN ps_order_detail ON ps_order_detail.id_order = ps_orders.id_order " +
                        "LEFT JOIN ps_country ON ps_country.id_country = ps_address.id_country " +
                        "LEFT JOIN ps_state ON ps_state.id_state = ps_address.id_state " +
                        //añado función para calcular descuento de linea
                        //" LEFT JOIN " + //queryDescuentoLinea +
                        condicionWhere +
                        "ORDER BY id_order ASC ";

            return query;
        }

        private void enviarDocsMayorista(DataTable docsToSend, DataTable detalleDocs)
        {
            try
            {
                csMail mailing = new csMail();
                string numPedidoTransferToSend = "";
                string numPedidoTransfer = "";
                string idMayorista = "";
                string emailMayorista = "test@klosions.com";
                string cabeceraMaterialEntregarMayorista = "";
                string nomCliente = "";
                string direccionEnt = "";
                string codCli = "";
                string cPostal = "";
                string poblacion = "";
                string provincia = "";
                string telCli = "";
                string nifCli = "";
                string emailCliente = "";

                //Lineas
                string codigoArticulo = "";
                string descArticulo = "";
                string cantidad = "";
                string precio = "";
                string descuento = "";
                string totalLinea = "";
                string htmlLinea = "";

                foreach (DataRow fila in docsToSend.Rows)
                {
                    numPedidoTransferToSend = fila["id_order"].ToString();
                    nomCliente = fila["company"].ToString();
                    direccionEnt = fila["dir1"].ToString();
                    codCli = fila["refParaMayorista"].ToString();
                    cPostal = fila["codigopostal"].ToString();
                    poblacion = fila["ciudad"].ToString();
                    provincia = fila["provincia_iso_code"].ToString();
                    telCli = fila["tel"].ToString();
                    nifCli = fila["dni"].ToString();
                    emailCliente = fila["email"].ToString();
                    decimal totalLineaDoc = 0;
                    idMayorista = fila["mayorista"].ToString();
                    emailMayorista = mysql.obtenerDatoFromQuery("select kls_email_mayorista from kls_mayoristas where kls_id_mayorista=" + idMayorista);

                    cabeceraMaterialEntregarMayorista = "<div class=\"contentEditable\" align =\"center\" > " +
                            "<img src=\"http://vilardellps.soyinformatica.com/img/mailimg/logo.png\" height=\"85\" alt =\"Logo Vilardell\" />" +
                            "</div>" + "<br>" + "</div>" + "<br>" +
                            "<div style = \"text-align: left\" ><span style=\"color: #2b6344;\"><strong>Solicitud de Pedido</strong></span></div><br>" +
                            "<span style=\"color: #2b6344;\"> Solicitamos preparen el pedido número " + numPedidoTransferToSend + " de nuestro cliente:</span>" + "<br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Número de documento: " + numPedidoTransferToSend + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Nombre del Cliente :" + nomCliente + "(" + codCli + ")" + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Dirección Entrega: " + direccionEnt + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">CP: " + cPostal + " " + poblacion + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Población: " + poblacion + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Provincia: " + provincia + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Teléfono: " + telCli + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Nif: " + nifCli + "</span><br>" + "\r\n" +
                            "<span style=\"color: #2b6344;\">Correo: " + emailCliente + "</span><br>" + "\r\n" +
                            "<br><br>" + "\r\n" + "\r\n" + "\r\n" +
                            //"<table style=\"border: 1px solid black; \" border=\"1px solid black\" width =\"80%;\" cellspacing =\"0\" > " +
                            //"<tr><th style=\"border: 1px solid black;\">CODIGO</th><th style=\"border: 1px solid black;\">PRODUCTO</th><th style=\"border: 1px solid black;\">UNIDADES</th><th style=\"border: 1px solid black;\">PRECIO</th><th style=\"border: 1px solid black;\">DESCUENTO</th><th style=\"border: 1px solid black;\">TOTAL</th></tr>" + "\r\n";

                            "<table style=\"background-color:#d9d9d9;\" width =\"900px%;\" cellspacing =\"0\" > " +
                            "<tr><th><span style=\"color: #2b6344;\">CODIGO</span></th>" +
                            "<th><span style=\"color: #2b6344;\">PRODUCTO</span></th>" +
                            "<th><span style=\"color: #2b6344;\">UNIDADES</span></th>" +
                            "<th><span style=\"color: #2b6344;\">PRECIO</span></th>" +
                            "<th><span style=\"color: #2b6344;\">DESCUENTO</span></th>" +
                            "<th><span style=\"color: #2b6344;\">TOTAL</span></th></tr>" + "\r\n";


                    foreach (DataRow filaDocsTransfer in detalleDocs.Rows)
                    {
                        numPedidoTransfer = filaDocsTransfer["id_order"].ToString();
                        if (numPedidoTransfer == numPedidoTransferToSend)
                        {
                            codigoArticulo = filaDocsTransfer["product_reference"].ToString();
                            codigoArticulo = codigoArticulo.Replace("'", "");
                            descArticulo = filaDocsTransfer["product_name"].ToString();
                            cantidad = filaDocsTransfer["product_quantity"].ToString();
                            precio = filaDocsTransfer["precio"].ToString();
                            if (!string.IsNullOrEmpty(filaDocsTransfer["descuento"].ToString()))
                            {
                                descuento = filaDocsTransfer["descuento"].ToString();
                                descuento = Math.Round(Convert.ToDecimal(descuento), 0).ToString();
                            }
                            else
                            {
                                descuento = "0";
                            }
                            totalLineaDoc = Convert.ToDecimal(cantidad) * Convert.ToDecimal(precio) * (1 - Convert.ToDecimal(descuento) / 100);
                            totalLinea = Math.Round(totalLineaDoc, 2).ToString();


                            htmlLinea = htmlLinea + "<tr><td><span style=\"color: #2b6344;\">" +
                                codigoArticulo + "</span></td><td><span style=\"color: #2b6344;\">" +
                                descArticulo + "</span></td><td style=\"text-align: right;\"><span style=\"color: #2b6344;\">" +
                                cantidad + "</span></td><td style=\"text-align: right;\"><span style=\"color: #2b6344; text-align: right;\">" +
                                precio + "</span></td><td style=\"text-align: right;\"><span style=\"color: #2b6344; text-align: right;\">" +
                                descuento + "</span></td><td style=\"text-align: right;\"><span style=\"color: #2b6344; text-align: right;\">" +
                                totalLinea + "</span></td></tr>" + "\r\n";
                        }
                    }
                    htmlLinea = htmlLinea + "</table>";
                    htmlLinea = htmlLinea + "<br><br>" +
                                            "Saludos cordiales" + "<br>" + "\r\n" +
                                            "<strong>LaboratoriosVilardell,S.A<strong>" +
                                            "<br><br>" +
                                            "<div class=\"contentEditableContainer contentImageEditable\" > " +
                                            "<div class=\"contentEditable\">" +
                                            "<img src = \"http://vilardellps.soyinformatica.com/img/cms/Footer.png\" alt =\"footer Cabecera Mail\" />" +
                                            "</div>" +
                                            "</div>";
                    string bodyEmail = cabeceraMaterialEntregarMayorista + htmlLinea;
                    mailing.sendMail("Pedido Vilardell Nº: " + numPedidoTransferToSend + " - " + nomCliente, bodyEmail, "", emailMayorista, "", null, true, false, true);
                    htmlLinea = "";
                }

            }
            catch (Exception ex)
            {

            }


        }




        // CREAR PEDIDO
        public void crearFicheroPedido() //1
        {
            double descuentoFormaPago = 0;

            DataTable dt = mysql.cargarTabla(selectDocsVilardell(false, true, false, true, false));
            if (dt != null)
            {
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["product_reference"].ToString().Contains("."))
                    {
                        dt.Rows[i]["product_reference"] = "'" + dr["product_reference"];

                    }
                    if (Convert.ToDouble(dr["total_discounts"].ToString()) > 0)
                    {
                        //dt.Rows[i]["descuento_comercial_linea_valor"] = obtenerDescuentoLinea(dr["id_order"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dr["payment_fee_percent"].ToString()))
                    {
                        if (Convert.ToDecimal(dr["payment_fee_percent"].ToString()) != 0)
                        {
                            descuentoFormaPago = Convert.ToDouble(dr["payment_fee_percent"].ToString());
                            descuentoFormaPago = descuentoFormaPago * -1;
                            dt.Rows[i]["descuento_comercial_cabecera_valor"] = descuentoFormaPago.ToString();
                        }
                        else
                        {
                            dt.Rows[i]["descuento_comercial_cabecera_valor"] = "0";
                        }

                    }
                    else
                    {
                        dt.Rows[i]["descuento_comercial_cabecera_valor"] = "0";
                    }
                    i++;
                }
            }



            if (dt.hasRows())
            {
                string fechaHoy = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
                dt.exportToExcel("pedidos");
                //csUtilidades.openFolder(csGlobal.fileExportPath + @"\Vilardell");
                if (csGlobal.modoManual)
                {
                    csUtilidades.openFolder(csGlobal.fileExportPath);
                }
            }
        }

        public void crearFicheroDevoluciones() {
            double descuentoFormaPago = 0;

            DataTable dt = mysql.cargarTabla(selectDocsVilardell(false, true, false, true, false));
            if (dt != null)
            {
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["product_reference"].ToString().Contains("."))
                    {
                        dt.Rows[i]["product_reference"] = "'" + dr["product_reference"];

                    }
                    if (Convert.ToDouble(dr["total_discounts"].ToString()) > 0)
                    {
                        //dt.Rows[i]["descuento_comercial_linea_valor"] = obtenerDescuentoLinea(dr["id_order"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dr["payment_fee_percent"].ToString()))
                    {
                        if (Convert.ToDecimal(dr["payment_fee_percent"].ToString()) != 0)
                        {
                            descuentoFormaPago = Convert.ToDouble(dr["payment_fee_percent"].ToString());
                            descuentoFormaPago = descuentoFormaPago * -1;
                            dt.Rows[i]["descuento_comercial_cabecera_valor"] = descuentoFormaPago.ToString();
                        }
                        else
                        {
                            dt.Rows[i]["descuento_comercial_cabecera_valor"] = "0";
                        }

                    }
                    else
                    {
                        dt.Rows[i]["descuento_comercial_cabecera_valor"] = "0";
                    }
                    i++;
                }
            }



            if (dt.hasRows())
            {
                string fechaHoy = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
                dt.exportToExcel("pedidos");
                //csUtilidades.openFolder(csGlobal.fileExportPath + @"\Vilardell");
                if (csGlobal.modoManual)
                {
                    csUtilidades.openFolder(csGlobal.fileExportPath);
                }
            }


        }
        private string obtenerDescuentoLinea(string id_Order)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            string descuento = "";
            string query = "select ps_cart_rule.reduction_percent " +
                " from ps_cart_rule  inner join ps_order_cart_rule on " +
                " ps_order_cart_rule.id_cart_rule=ps_cart_rule.id_cart_rule " +
                " where ps_order_cart_rule.id_order=" + id_Order;
            descuento = mysql.obtenerDatoFromQuery(query);
            if (string.IsNullOrEmpty(descuento))
            {
                descuento = "0";
            }
            return descuento;


        }

        /// <summary>
        /// Eliminar pedido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eliminarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (csUtilidades.ventanaPassword())
            {
                if (dgvPedidos.Rows.Count > 0)
                {
                    if (dgvPedidos.SelectedRows.Count > 0)
                    {
                        csPrestashop ps = new csPrestashop();

                        foreach (DataGridViewRow item in dgvPedidos.SelectedRows)
                        {
                            string id_order = item.Cells["id_order"].Value.ToString();
                            ps.borrarPedido(id_order);
                        }

                        cdPedido();
                    }
                }
            }
        }

        public void btnBajar_Click(object sender, EventArgs e)
        {
            verificarClientes();
            csPrestashop ps = new csPrestashop();
            ps.bajarClientesPendientes();
        }

        private void actualizarDocumentosDescargados()
        {
            try
            {
                string cadenaID = "";
                string proyectoID = "";
                string idDocumentoPS = "";
                DataTable dtOrders = mysql.cargarTabla("select " +
                    " id_order,kls_checked,ps_orders.id_wholesaler,ps_customer.company,ps_customer.id_mayorista_alfa " +
                    " from " +
                    " ps_orders left join ps_customer on ps_orders.id_wholesaler = ps_customer.id_customer " +
                    " where kls_checked is null");

                foreach (DataRow dr in dtOrders.Rows)
                {
                    idDocumentoPS = dr["id_order"].ToString();
                    cadenaID = dr["id_mayorista_alfa"].ToString();
                    csUtilidades.ejecutarConsulta("UPDATE V_OFERTA SET PROYECTOID=3, CADENAID=" + cadenaID + "  WHERE OfertaSegunCliente='" + idDocumentoPS + "'", false);
                }

                if (dtOrders.Rows.Count > 0)
                {
                    string filterOrdes = "";
                    string query = "";
                    filterOrdes = csUtilidades.delimitarPorComasDataTable(dtOrders, "id_order");
                    filterOrdes = filterOrdes.Replace(",", "','");


                    query = "SELECT " +
                        " 'PedDirect' as TipoDoc,PedidoSegunCliente,PedidoID,CompaniaID,ContadorEntidadID,NumeroPedido,ClienteID,FechaPedido,RepresentanteID,NetoMoneda,FechaUltAct " +
                        " FROM v_Pedido " +
                        " where PedidoSegunCliente <> '' and CompaniaID=2 and PedidoSegunCliente in ('" + filterOrdes + "')" +
                        " union " +
                        "SELECT " +
                        " 'PedTransf' as TipoDoc, OfertaSegunCliente,OfertaID,CompaniaID,ContadorEntidadID,NumeroOferta,ClienteID,FechaOferta,RepresentanteID,NetoMoneda,FechaUltAct " +
                        " FROM v_Oferta " +
                        " where OfertaSegunCliente <> '' and CompaniaID=2 and OfertaSegunCliente in ('" + filterOrdes + "')";


                    string oderId = "";
                    string ERP_OrderId = "";
                    DataTable dt = sql.cargarDatosTablaA3(query);

                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            oderId = dr["PedidoSegunCliente"].ToString();
                            ERP_OrderId = dr["PedidoID"].ToString();
                            csUtilidades.ejecutarConsulta("update ps_orders set kls_checked=" + ERP_OrderId + " where id_order=" + oderId, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);

            }
        }


        private void exportarClientesPS()
        {
            csPSWebService psWebService = new csPSWebService();

            try
            {
                int clientesSubidos = 0;
                List<string> clientesQueYaExisten = new List<string>();
                csMySqlConnect mysql = new csMySqlConnect();
                string clientes = "Los clientes siguientes ya existen en la tienda: \n\n";

                DataTable dt = new DataTable();
                foreach (DataGridViewColumn column in dgvClientes.Columns)
                    dt.Columns.Add(column.Name); //better to have cell type


                for (int i = 0; i < dgvClientes.SelectedRows.Count; i++)
                {
                    dt.Rows.Add();
                    for (int j = 0; j < dgvClientes.Columns.Count; j++)
                    {
                        dt.Rows[i][j] = dgvClientes.SelectedRows[i].Cells[j].Value;
                        //^^^^^^^^^^^
                    }
                }

                try
                {
                    foreach (DataRow fila in dt.Rows)
                    {
                        string kls_codcliente = fila["IdentificacionComoProveedor"].ToString();
                        // Si el KLS_CODCLIENTE está vacío o si KLS_EMAIL_USUARIO contiene una arroba
                        if (kls_codcliente == "" && csUtilidades.emailIsValid(fila["EMAIL"].ToString()))// && fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString().Contains("@"))
                        {
                            string existeEmail = "select count(email) from ps_customer where email = '" + fila["EMAIL"].ToString() + "'";
                            string existeCodigoCliente = "select count(kls_a3erp_id) from ps_customer where kls_a3erp_id = '" + fila["CodigoCliente"].ToString().Trim() + "'";
                            if (!mysql.existeCampo(existeEmail) || !mysql.existeCampo(existeCodigoCliente))
                            {
                                psWebService.cdPSWebService("POST", "customers", "", fila["CodigoCliente"].ToString(), true, "", null, fila, 0, false); //en minúsculas el objeto

                                clientesSubidos++;

                            }
                            else
                            {
                                clientesQueYaExisten.Add(fila["EMAIL"].ToString());
                            }
                        }
                    }

                    if (clientesQueYaExisten.Count > 0)
                    {
                        foreach (var item in clientesQueYaExisten)
                        {
                            clientes += ">> " + item + "\n";
                        }

                        MessageBox.Show(clientes);

                        if (MessageBox.Show("¿Sincronizar clientes?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            // syncBetweenPSA3_by_Email();

                            MessageBox.Show("Clientes sincronizados con éxito");
                        }
                    }
                    if (clientesSubidos > 0 && csGlobal.modoManual)
                    {
                        actualizarRecEquiv(dt);

                        verificarClientes(true, dt);
                        actualizarDatosRegistroPS(dt);
                        ("Se han procesado " + clientesSubidos + "  clientes").mb();
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void actualizarRecEquiv(DataTable dtClientes)
        {
            string idcliente = "";
            string recEquivalencia = "";
            string insertScript = "";

            foreach (DataRow fila in dtClientes.Rows)
            {
                recEquivalencia = fila["TRATAMIENTONRE"].ToString();
                idcliente = fila["CODIGOCLIENTE"].ToString();

                insertScript = "insert into ps_surchage_eq_customers (id_customer,active) select id_customer," + recEquivalencia + " from ps_customer where kls_a3erp_id='" + idcliente + "'";
                csUtilidades.ejecutarConsulta(insertScript, true);
            }


        }

        /// <summary>
        /// Función que actaliza los datos del formulario de registro para aquellos clientes que se han subido directamente desde el ERP
        /// </summary>
        /// <param name="dtClientes"></param>
        private void actualizarDatosRegistroPS(DataTable dtClientes)
        {
            string idcliente = "";
            string recEquivalencia = "";
            string insertScript = "";
            string updateCustomerData = "";
            string scriptWhereCustomer = "";

            string customer_type = "";
            string nif = "";
            string companyName = "";
            string address1 = "";
            string city = "";
            string postcode = "";
            string id_state = "";
            string phone = "";
            string contactEmail = "";
            string account_Iban = "ES00";
            string account_Entity = "0000";
            string account_office = "0000";
            string account_CD = "00";
            string account_number = "0000000000";
            string tipoPersona = "";
            string status = "ACE";
            string debtsName = "";
            string debtsNif = "";
            string id_customer = "";

            int idRequestCustomer = 0;

            string deudorID = "";



            scriptWhereCustomer = "(" + csUtilidades.concatenarValoresQueryFromDataTable(dtClientes, "idDeudor") + ")";

            DataTable dtBancoCliente = new DataTable();







            foreach (DataRow fila in dtClientes.Rows)
            {
                idcliente = fila["CODIGOCLIENTE"].ToString();
                deudorID = fila["DEUDORID"].ToString();



                dtBancoCliente = sql.obtenerDatosSQLScript("SELECT [ps_Direccion].[DeudorID] ,[PaisID] " +
                       " ,[CCCPais]      ,[CCCDigitoIBAN]      ,[CCCBancoID]      ,[CCCAgencia]      ,[CCCDigito]      ,[CCCCuenta] " +
                       " ,[FormatoLibreCta]      , v_Cliente.Nombre      , IdentificacionComoProveedor  FROM[VILARDELL].[dbo].[ps_Direccion] " +
                       " right join v_Cliente on v_Cliente.DeudorID = ps_Direccion.DeudorID " +
                       " where[ps_Direccion].Baja = 0 and TipoDireccion = 1 and[ps_Direccion].[DeudorID] = " + deudorID);

                id_customer = dtBancoCliente.Rows[0]["IdentificacionComoProveedor"].ToString();
                nif = fila["NIF"].ToString();
                companyName = fila["NOMBRECLIENTE"].ToString();
                address1 = fila["NOMBREVIAPUBLICA"].ToString();
                city = fila["NOMBREMUNICIPIO"].ToString();
                postcode = fila["CPOSTAL"].ToString();
                id_state = fila["CodigoProvincia"].ToString(); //REVISAR ESTA LÍNEA 
                phone = fila["TELEFONO"].ToString();
                contactEmail = fila["EMAIL"].ToString();
                account_Iban = dtBancoCliente.Rows[0]["CCCDigitoIBAN"].ToString();
                account_Entity = dtBancoCliente.Rows[0]["CCCBancoID"].ToString();
                account_office = dtBancoCliente.Rows[0]["CCCAgencia"].ToString();
                account_CD = dtBancoCliente.Rows[0]["CCCDigito"].ToString();
                account_number = dtBancoCliente.Rows[0]["CCCCuenta"].ToString();
                tipoPersona = fila["PERSONAJURIDICA"].ToString();
                status = status;
                debtsName = fila["NOMBRECLIENTE"].ToString();
                debtsNif = fila["NIF"].ToString();
                customer_type = "DIRECT";

                insertScript = "insert into ps_registration_requests (id_customer,nif,company,address1,city,postcode,id_state,phone,contact_Email," +
                    " account_Iban,account_Entity,account_office,account_dc,account_number," +
                    " tipo_persona,status,debts_name,debts_nif,customer_type) values (" +
                     id_customer + ",'" +
                    nif + "','" +
                        companyName + "','" +
                        address1 + "','" +
                        city + "','" +
                        postcode + "','" +
                        id_state + "','" +
                        phone + "','" +
                        contactEmail + "','" +
                        account_Iban + "','" +
                        account_Entity + "','" +
                        account_office + "','" +
                        account_CD + "','" +
                        account_number + "','" +
                        tipoPersona + "','" +
                        status + "','" +
                        debtsName + "','" +
                        debtsNif + "','" +
                        customer_type + "');" +
                        "select last_insert_id();";


                idRequestCustomer = mysql.ObtenerIdInsertado(insertScript);

                scriptWhereCustomer = "update ps_customer set id_registration_request=" + idRequestCustomer + " ,id_registration_request_accepted =" + idRequestCustomer + " where id_customer=" + id_customer;
                csUtilidades.ejecutarConsulta(scriptWhereCustomer, true);
            }

        }



        private void btnSubir_Click(object sender, EventArgs e) // PENDIENTE
        {
            exportarClientesPS();
        }

        private string obtenerNombre(string texto)
        {
            string txt = texto;

            if (texto.Contains(","))
            {
                txt = texto.Split(',')[1];
            }

            return txt.Replace(",", "");
        }

        private string obtenerApellido(string texto)
        {
            string txt = texto;

            if (texto.Contains(","))
            {
                txt = texto.Split(',')[0];
            }

            return txt.Replace(",", "");
        }

        //private void cargarClientes()
        //{
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    dgvClientes.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing; //or even better .DisableResizing. Most time consumption enum is DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        //    dgvClientes.RowHeadersVisible = false;

        //    DataTable dt = sql.cargarDatosTablaA3("select vwa_613_uclientes.*, dbo.funa_613_ultimaventa " +
        //        " (vwa_613_uclientes.clienteid,vwa_613_uclientes.companiaid) as ultimaventa from vwa_613_uclientes " +
        //        " with (nolock) where companiaid = 2 and baja=convert(bit,0) and REL_DireccionFiscalEmail " +
        //        " is not null and REL_DireccionFiscalEmail <> '' and nif <> '' and nif is not null and baja = 0 order by codigocliente");
        //    csUtilidades.addDataSource(dgvClientes, dt);
        //    csUtilidades.contarFilasGrid(dt, sw, tsslInfo);
        //}

        private void cargarClientes()
        {
            string anexoPresta = filtroPrestashop();
            string anexoTipoCustomer = filtroTipoCliente();
            //Busqueda segun lo que introduzcas
            string anexoTexto = " and dbo.ps_Direccion.Nombre like '%" + tbSearchText.Text + "%' or " +
                " dbo.v_Cliente.CodigoCliente like '%" + tbSearchText.Text + "%' or " +
                " dbo.ps_Direccion.NIF like '%" + tbSearchText.Text + "%' or " +
                " dbo.ps_Direccion.Nombre like '%" + tbSearchText.Text + "%' or " +
                " dbo.ps_Direccion.DireccionEMail like '%" + tbSearchText.Text + "%' ";

            if (string.IsNullOrEmpty(tbSearchText.Text))
            {
                anexoTexto = null;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string consultaAlfa = string.Format("SELECT  " +
                        " dbo.v_Cliente.ClienteID, dbo.v_Cliente.CodigoCliente, dbo.v_Cliente.IdentificacionComoProveedor, dbo.ps_Direccion.Nombre AS NombreCliente, " +
                        " dbo.ps_Direccion.DireccionEMail AS EMAIL, dbo.ps_Direccion.NIF, dbo.ps_Direccion.NombreViaPublica + ' ' + dbo.ps_Direccion.NumeroCasa AS NOMBREVIAPUBLICA,    " +
                        " dbo.v_Cliente.TipoCliente, dbo.ps_Direccion.DeudorID, dbo.v_Cliente.TratamientoNRE, dbo.v_Cliente.DireccionFiscalID, " +
                        " dbo.v_Cliente.RepresentanteID, dbo.v_Representante.CodigoRepresentante, dbo.v_Representante.Nombre AS NombreRepresentante, " +
                        " dbo.ps_Direccion.DireccionID, dbo.ps_Direccion.DeudorID AS IdDeudor, dbo.ps_Direccion.Nombre,   " +
                        " dbo.ps_Direccion.CPostal, dbo.ps_Direccion.MunicipioID, dbo.ps_Direccion.NombreMunicipio, dbo.ps_GeoProvincia.CodigoProvincia, dbo.ps_Direccion.PaisID,   " +
                        " dbo.ps_Direccion.CCCPais, dbo.ps_Direccion.CCCDigitoIBAN, dbo.ps_Direccion.CCCBancoID, dbo.ps_Direccion.CCCAgencia, dbo.ps_Direccion.CCCDigito,   " +
                        " dbo.ps_Direccion.CCCCuenta, dbo.ps_Direccion.FormatoLibreCta, dbo.ps_Direccion.Telefono,  dbo.v_Cliente.CodigoCliente AS CODCLI,  " +
                        " dbo.ps_Direccion.DireccionCompletaSelector, dbo.ps_Direccion.Baja,'VILARDELL123' AS PASSWORD,'NOMBRE' AS NOMBRE,'APELLIDOS' AS APELLIDO, dbo.ps_Direccion.Nombre AS RAZON,  " +
                        " CASE WHEN dbo.ps_Direccion.PersonaJuridica = 'true' THEN 'J' ELSE 'F' END AS PersonaJuridica, dbo.v_Cliente.ActividadPrincipalId" +
                        " FROM            dbo.ps_Direccion INNER JOIN  " +
                        " dbo.v_Cliente ON dbo.ps_Direccion.DireccionID = dbo.v_Cliente.DireccionFiscalID  " +
                        " INNER JOIN dbo.ps_GeoProvincia " +
                        " ON dbo.ps_Direccion.ProvinciaID = dbo.ps_GeoProvincia.ProvinciaID" +
                        " LEFT OUTER JOIN " +
                        " dbo.v_Representante ON dbo.v_Cliente.RepresentanteID = dbo.v_Representante.RepresentanteID AND " +
                        " dbo.v_Cliente.CompaniaID = dbo.v_Representante.CompaniaID " +
                        " WHERE dbo.v_Cliente.CompaniaID = 2 and dbo.v_Cliente.baja=0  " + anexoTexto + anexoPresta + anexoTipoCustomer);
            DataTable dt = sql.cargarDatosTablaA3(consultaAlfa);
            csUtilidades.addDataSource(dgvClientes, dt);
            csUtilidades.contarFilasGrid(dt, sw, tsslInfo);
        }

        private void btLoadCustomers_Click(object sender, EventArgs e)
        {
            cargarClientes();
            if (dgvClientes.Rows.Count > 0)
            {
                btnSubir.Enabled = true;
            }
            else
            {
                btnSubir.Enabled = false;
            }
        }

        private string filtroPrestashop()
        {
            //En este campo "IdentificacionComoProveedor" es donde almacenamos el id de prestashop
            string anexoPresta = "";
            if (rbutPrestashop.Checked)
            {
                anexoPresta = " and IdentificacionComoProveedor <>'' ";
            }
            return anexoPresta;
        }

        private string filtroTipoCliente()
        {
            string anexoEmail = "";
            string anextoTipoCustomer = "";

            if (rbutEmailSi.Checked)
            {
                anexoEmail = " and dbo.ps_Direccion.DireccionEMail<>'' ";
            }

            if (rbutEmailNO.Checked)
            {
                anexoEmail = " and dbo.ps_Direccion.DireccionEMail='' ";
            }

            if (rbutCustomerLead.Checked) // potenciales
            {
                //anextoTipoCustomer = " and TipoCliente= 1 ";
                anextoTipoCustomer = " and left(codigoCliente,1)='P' ";
            }
            if (rbutCustomers.Checked)
            {
                //anextoTipoCustomer = " and TipoCliente = 0 and left(codigoCliente,1)<>'P'";
                anextoTipoCustomer = " and left(codigoCliente,1)<>'P'";
            }
            return anextoTipoCustomer + anexoEmail;
        }

        private void btLoadFormasCobro_Click(object sender, EventArgs e)
        {
            cargarFormasCobro();
        }

        private void cargarFormasCobro()
        {
            string anexoPresta = filtroPrestashop();
            string anexoTipoCustomer = filtroTipoCliente();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            string consultaAlfa = string.Format(" SELECT  FormaCobroPagoID as ID,CodigoFormaCobroPago as Cod,Descripcion,DesplazamientoPrimerVencimiento as Vto,NumeroVencimientos " +
                                                " FROM " +
                                                " e_EntornoContableFormasCobroPago " +
                                                "  where companiaId=2 ");
            DataTable dt = sql.cargarDatosTablaA3(consultaAlfa);
            csUtilidades.addDataSource(dgvClientes, dt);
            csUtilidades.contarFilasGrid(dt, sw, tsslInfo);


        }

        private void btLoadPedidosPS_Click(object sender, EventArgs e)
        {
            cdPedido();
        }

        private void btDownloadOfertas_Click(object sender, EventArgs e)
        {
            actualizarDocumentosDescargados();
            verificarClientes();
            crearFicheroOfertas();
            marcarDevoluciones();
        }

        private void btDownloadPedidos_Click(object sender, EventArgs e)
        {
            actualizarDocumentosDescargados();
            verificarClientes();
            crearFicheroPedido();

        }




        /// <summary>
        /// Función para verificar los datos del cliente entre el ERP y Prestashop en función de quien envía la información a quien
        /// 
        /// </summary>
        /// 
        /// <param name="origenERP"></param>

        public void verificarClientes(bool origenERP = false, DataTable dtClientes = null)
        {
            try
            {
                string customerIdPS = "";

                string customerPsEnAlfa = "";
                string customerIdAlfa = "";
                string deudorIdAlfa = "";
                string recargoEquivalendiaAlfa = "";
                string recargoEquivalenciaPS = "";
                string codigoDireccionAlfa = "";
                string personaJuridica = "";
                string tipoClientePS = "";      //Actualizo el campo Tipo de cliente (1 - farmacia, 7 - parafarmacia o 3 - mayorista)

                string customerId_PS = "";
                string codCli_ERP = "";
                string tipoCliente = "";        //Actualizo el campo ActividadPrincipalID (1-farmacia, 7-parafarmacia o 3-mayorista)
                string clienteSerie = "";       //Actualizo el campo ClienteSErieId (1-Directo o 2-Transfer)


                csVilardell vilardell = new klsync.csVilardell();



                //string query = "SELECT " +
                //                " CLIENTEID, V_CLIENTE.COMPANIAID, CODIGOCLIENTE, V_CLIENTE.NOMBRE,V_CLIENTE.REPRESENTANTEID, V_CLIENTE.FECHAULTACT, V_CLIENTE.DEUDORID, " +
                //                " IDENTIFICACIONCOMOPROVEEDOR, TRATAMIENTONRE, CODIGODIRECCION, ACTIVIDADPRINCIPALID " +
                //                " FROM " +
                //                " DBO.V_CLIENTE INNER JOIN " +
                //                " DBO.PS_DIRECCION ON DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND  " +
                //                " DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID " +
                //                " WHERE " +
                //                " (IDENTIFICACIONCOMOPROVEEDOR <> '' AND V_CLIENTE.COMPANIAID = 2 and TipoDireccion=0) ";
                //string where = "";


                string query = "";
                string where = "";

                if (origenERP)
                {
                    query = vilardell.selectClientes(dtClientes, true);
                }
                else
                {
                    query = vilardell.selectClientes(null, false);
                }

                DataTable dt = sql.cargarDatosScriptEnTabla(query, where);

                csUtilidades.registroProceso("Paso 1", System.Reflection.MethodBase.GetCurrentMethod().Name);


                //Cargo los clientes en Prestashop que no tienen el ID de Alfa Asignado
                string queryCustomers = "select " +
                                " ps_customer.id_customer,kls_a3erp_id, tipo_persona, customer_type, type, " +
                                " case when ps_surchage_eq_customers.active is null then 0 else ps_surchage_eq_customers.active end as active " +
                                " from " +
                                " ps_customer left join ps_surchage_eq_customers " +
                                " on ps_customer.id_customer=ps_surchage_eq_customers.id_customer " +
                                " where  kls_a3erp_id is null ";

                DataTable dtCustomersPS = mysql.cargarTabla(queryCustomers);

                csUtilidades.registroProceso("Paso 2", System.Reflection.MethodBase.GetCurrentMethod().Name);


                foreach (DataRow dr in dtCustomersPS.Rows)
                {
                    recargoEquivalenciaPS = dr["active"].ToString();
                    customerIdPS = dr["id_customer"].ToString();

                    personaJuridica = "0";
                    if (dr["tipo_persona"].ToString() == "J")
                    {
                        personaJuridica = "1";
                    }


                    //ClienteSErieId
                    if (dr["customer_type"].ToString() == "DIRECT")
                    { clienteSerie = "1"; }
                    else { clienteSerie = "2"; }

                    //ActividadPrincipalID
                    if (dr["type"].ToString() == "F")
                    { tipoCliente = "1"; }
                    else if (dr["type"].ToString() == "P")
                    { tipoCliente = "2"; }
                    else { tipoCliente = "3"; }


                    foreach (DataRow drAlfa in dt.Rows)
                    {
                        customerIdAlfa = drAlfa["CLIENTEID"].ToString();
                        customerPsEnAlfa = drAlfa["IDENTIFICACIONCOMOPROVEEDOR"].ToString();
                        codigoDireccionAlfa = drAlfa["CodigoDireccion"].ToString();
                        deudorIdAlfa = drAlfa["DEUDORID"].ToString();
                        recargoEquivalendiaAlfa = drAlfa["TRATAMIENTONRE"].ToString();
                        codCli_ERP = drAlfa["codigocliente"].ToString();

                        if (customerPsEnAlfa == customerIdPS)
                        {
                            if (drAlfa["actividadprincipalid"].ToString() == "3")
                            {
                                tipoClientePS = "MF";
                            }
                            else if (drAlfa["actividadprincipalid"].ToString() == "1")
                            {
                                tipoClientePS = "F";
                            }
                            else { tipoClientePS = "P"; }

                            csUtilidades.ejecutarConsulta("update ps_customer set kls_a3erp_id=" + customerIdAlfa + " where id_customer=" + customerIdPS, true);
                            csUtilidades.ejecutarConsulta("update ps_address set kls_id_dirent='" + codigoDireccionAlfa + "' where id_customer=" + customerIdPS, true);
                            //CONSULTAR CON LLUIS CODIGO=1 consutlar si retiramos este update que yo hago mas abajo y que aqui no se esta haciendo.

                            csUtilidades.ejecutarConsulta("UPDATE v_Cliente SET ActividadPrincipalID=" + tipoCliente +
                            " , Actividad2ID = " + tipoCliente +
                            " , Actividad3ID = " + tipoCliente +
                            " , clienteSerieID=" + clienteSerie + " where clienteID='" + customerIdAlfa + "'", false);

                            csUtilidades.ejecutarConsulta("update ps_Direccion set PersonaJuridica=" + personaJuridica + " where DeudorID =" + deudorIdAlfa, false);


                            //Verifico si el cliente tiene activado Recargo de EQuivalencia y actualizo Alfa
                            if (recargoEquivalendiaAlfa != recargoEquivalenciaPS)
                            {
                                csUtilidades.ejecutarConsulta("update v_Cliente set TratamientoNRE=" + recargoEquivalenciaPS + " where clienteID=" + customerIdAlfa, false);
                            }


                            insertarBanco(customerIdAlfa);
                            actualizarAbonos(customerIdAlfa);
                            actualizarDatosEstadistica();

                        }
                    }
                }

                //ACORDARSE DE CAMBIAR LA CONSULTA QUE TENGO DENTRO DE ACTUALIZAR DIRECCIONS PARA QUE BUSQUE ENTTRE TODOS LOS CLIENTES.
                actualizarDirecciones();
                csUtilidades.registroProceso("Paso 3", System.Reflection.MethodBase.GetCurrentMethod().Name);

                csUtilidades.ejecutarConsulta("UPDATE v_ClienteFormaPago SET importehasta=999999999 where ImporteHasta=0", false);

            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        private void borrarEnlaceClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string codigoCliente = "";
            foreach (DataGridViewRow dgv in dgvClientes.SelectedRows)
            {
                codigoCliente = dgv.Cells["codigoCliente"].Value.ToString();
                csUtilidades.ejecutarConsulta("update v_cliente set IdentificacionComoProveedor=null where codigoCliente='" + codigoCliente + "'", false);

            }
            cargarClientes();
        }

        private void btHideDocsPanel_Click(object sender, EventArgs e)
        {

        }

        private void btShowDocsPanel_Click(object sender, EventArgs e)
        {

        }

        private void btHide_Click(object sender, EventArgs e)
        {

        }

        private void btShow_Click(object sender, EventArgs e)
        {

        }

        private void btLoadPedidos_Click(object sender, EventArgs e)
        {
            cdPedido();
        }

        private void sincronizarManualmenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de que quieres sincronizar manualmente este documento? \n No podrás descargarlo en el ERP", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                string numeroPedido = dgvPedidos.SelectedRows[0].Cells["id_order"].Value.ToString();
                string queryUpdate = "update ps_orders set kls_checked='99999999' where id_order=" + numeroPedido;
                csUtilidades.ejecutarConsulta(queryUpdate, true);

                MessageBox.Show("Documentos actualizados con éxito");
                cdPedido();
            }



        }

        private void borrarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de que quieres borrar la sincronización de este documento?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                string numeroPedido = dgvPedidos.SelectedRows[0].Cells["id_order"].Value.ToString();
                string queryUpdate = "update ps_orders set kls_checked=null where id_order=" + numeroPedido;
                csUtilidades.ejecutarConsulta(queryUpdate, true);

                MessageBox.Show("Documentos actualizados con éxito");
                cdPedido();
            }
        }

        private void frVilardell_Load(object sender, EventArgs e)
        {
            btnSubir.Enabled = false;
        }



        private void cargarClientesByAgente(string codRepre = null)
        {
            string whereQuery = "";
            if (!string.IsNullOrEmpty(codRepre))
            {
                whereQuery = " where cod_repre='" + codRepre + "' or repre_cli='" + codRepre + "'";
            }

            string queryRepres = "select " +
                                " id_customer, id_default_group,company,firstname, lastname,email,date_add,customer_type, " +
                                " kls_a3erp_id as CodigoERP, kls_id_manager,kls_payment_method_id,repre_cli, cod_repre " +
                                " from ps_customer" + whereQuery;

            csUtilidades.addDataSource(dgvAgentes, mysql.cargarTabla(queryRepres));
        }

        private void btCargarClientesByAgente_Click(object sender, EventArgs e)
        {
            string agente = null;
            if (cboxAgentes.Visible)
            {
                agente = cboxAgentes.SelectedValue.ToString();
            }

            cargarClientesByAgente(agente);
        }

        private void syncronizarAsignacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DataTable dtAgentes = new DataTable();
            DataTable dtClientes = new DataTable();
            dtAgentes = mysql.cargarTabla("select id_customer, cod_repre from ps_customer where cod_repre is not null");
            dtClientes = mysql.cargarTabla("select id_customer, repre_cli from ps_customer where repre_cli is not null");

            string idCliente = "";
            string idagente = "";

            foreach (DataRow cliente in dtClientes.Rows)
            {
                idCliente = cliente["id_customer"].ToString();
                idagente = cliente["repre_cli"].ToString();

                foreach (DataRow agente in dtAgentes.Rows)
                {
                    if (idagente == agente["cod_repre"].ToString())
                    {
                        csUtilidades.ejecutarConsulta("update ps_customer set kls_id_manager=" + agente["id_customer"].ToString() + " where id_customer =" + idCliente, true);
                    }
                    if (idCliente == agente["id_customer"].ToString())
                    {
                        csUtilidades.ejecutarConsulta("update ps_customer set kls_id_manager=null where id_customer =" + idCliente, true);
                    }
                }
            }
            cargarClientesByAgente();
        }

        private void cargarRepresentantes()
        {
            try
            {

                DataTable dt = sql.cargarDatosTablaA3("SELECT CODIGOREPRESENTANTE,NOMBRE FROM V_REPRESENTANTE " +
                                        " WHERE COMPANIAID=2 AND BAJA=0");

                cboxAgentes.ValueMember = "CODIGOREPRESENTANTE";
                cboxAgentes.DisplayMember = "NOMBRE";
                cboxAgentes.DataSource = dt;

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void rButPorAgente_CheckedChanged(object sender, EventArgs e)
        {
            if (rButPorAgente.Checked)
            {
                cboxAgentes.Visible = true;
                cargarRepresentantes();
            }
        }

        private void rbutTodosAgentes_CheckedChanged(object sender, EventArgs e)
        {
            if (rbutTodosAgentes.Checked)
            {
                cboxAgentes.Visible = false;

            }
        }

        private void tsbtAllDocs_Click(object sender, EventArgs e)
        {
            //clientes
            verificarClientes();
            csPrestashop ps = new csPrestashop();
            ps.bajarClientesPendientes();
            //Documentos Comerciales
            actualizarDocumentosDescargados();
            verificarClientes();
            crearFicheroOfertas();
            crearFicheroPedido();
        }

        private void actualizarBancoCliente(string codCliente, string codDireccion)
        {

            String queryDatosCliDirERP = "select * from v_cliente where ClienteID=" + codCliente;
            /*
            string queryDatosCliDirERP = " SELECT " +
                " dbo.v_Cliente.ClienteID, dbo.v_Cliente.CompaniaID, dbo.v_Cliente.DeudorID, dbo.v_Cliente.CodigoCliente, dbo.v_Cliente.Nombre, dbo.v_Cliente.TipoCliente, " +
                " dbo.v_Cliente.TratamientoNRE, dbo.ps_Direccion.DireccionID, dbo.ps_Direccion.CodigoDireccion " +
                " FROM " +
                " dbo.v_Cliente INNER JOIN " +
                " dbo.ps_Direccion ON dbo.v_Cliente.DeudorID = dbo.ps_Direccion.DeudorID AND dbo.v_Cliente.DeudorID = dbo.ps_Direccion.DeudorID AND " +
                " dbo.v_Cliente.DeudorID = dbo.ps_Direccion.DeudorID AND dbo.v_Cliente.DeudorID = dbo.ps_Direccion.DeudorID " +
                " WHERE " +
                " dbo.ps_Direccion.CodigoDireccion = '" + codDireccion + "' and dbo.v_Cliente.ClienteID = '" + codCliente + "' " +
                " ORDER BY dbo.v_Cliente.DeudorID";*/

            string queryDatosCliDirPS = " select " +
               " ps_customer.id_customer,ps_customer.active,ps_customer.date_upd,kls_a3erp_id, " +
               " account_iban,account_entity,account_office,account_dc,account_number,account_bank_name,account_swift_bic, " +
               " ps_address.kls_id_dirent " +
               " from ps_customer inner join ps_address on ps_customer.id_customer=ps_address.id_customer " +
               " where ps_address.main=1 and ps_address.deleted=0 and kls_a3erp_id='" + codCliente + "'";

            string idDeudorERP = "";

            string ccPais = "ES";
            string iban = "";
            string ccBanco = "";
            string ccOficina = "";
            string ccDC = "";
            string ccCuenta = "";
            string ccCuentaIBAN = "";

            DataTable dtERP = sql.cargarDatosTablaA3(queryDatosCliDirERP);
            DataTable dtPS = mysql.cargarTabla(queryDatosCliDirPS);

            iban = dtPS.Rows[0]["account_iban"].ToString();
            ccBanco = dtPS.Rows[0]["account_entity"].ToString();
            ccOficina = dtPS.Rows[0]["account_office"].ToString();
            ccDC = dtPS.Rows[0]["account_dc"].ToString();
            ccCuenta = dtPS.Rows[0]["account_number"].ToString();
            ccCuentaIBAN = iban + ccBanco + ccOficina + ccDC + ccCuenta;

            idDeudorERP = dtERP.Rows[0]["DeudorID"].ToString();

            string updateBancoERP = "update ps_direccion set " +
                " CCCPais='" + ccPais + "', " +
                " CCCDigitoIBAN='" + iban + "', " +
                " CCCBancoID='" + ccBanco + "', " +
                " CCCAgencia='" + ccOficina + "', " +
                " CCCDigito='" + ccDC + "', " +
                " CCCCuenta='" + ccCuenta + "', " +
                " FormatoLibreCta='" + ccCuentaIBAN + "' " +
                " WHERE " +
                " dbo.ps_Direccion.CodigoDireccion = '10' AND DeudorID = '" + idDeudorERP + "'";


            csUtilidades.ejecutarConsulta(updateBancoERP, false);


        }
        private void insertarBanco(String customerIdAlfa)
        {

            String codigoDireccion = "10";
            String tipoDireccion = "1";
            String paisID = "1";

            string idDeudorERP = "";
            string nombre = "";



            string ccPais = "ES";
            string iban = "";
            string ccBanco = "";
            string ccOficina = "";
            string ccDC = "";
            string ccCuenta = "";
            string ccCuentaIBAN = "";




            //Seleccionamos los datos del cliente de SQL
            String datosClienteSQL = "Select * from dbo.v_Cliente where ClienteId=" + customerIdAlfa;
            DataTable dtERP = sql.cargarDatosTablaA3(datosClienteSQL);

            //Seleccionamos los datos bancarios de MySQL
            String datosClientesMySQL = "Select firstname,account_iban,account_entity,account_office,account_dc,account_number from ps_customer where id_customer=" + dtERP.Rows[0]["IdentificacionComoProveedor"];
            DataTable dtMySql = mysql.cargarTabla(datosClientesMySQL);

            //Datos Sql
            idDeudorERP = dtERP.Rows[0]["DeudorID"].ToString();
            nombre = dtERP.Rows[0]["Nombre"].ToString();

            //datos mysql
            iban = dtMySql.Rows[0]["account_iban"].ToString();
            ccBanco = dtMySql.Rows[0]["account_entity"].ToString();
            ccOficina = dtMySql.Rows[0]["account_office"].ToString();
            ccDC = dtMySql.Rows[0]["account_dc"].ToString();
            ccCuenta = dtMySql.Rows[0]["account_number"].ToString();
            ccCuentaIBAN = iban + ccBanco + ccOficina + ccDC + ccCuenta;





            String insertarBanco = "insert into ps_Direccion (DeudorID,CodigoDireccion,TipoDireccion," +
                                    "Nombre,PaisID,CCCPais,CCCDigitoIBAN,CCCBancoID,CCCAgencia,CCCDigito,CCCCuenta,FormatoLibreCta)" +
                                    "values(" + idDeudorERP + "," + codigoDireccion + "," + tipoDireccion + " , " + "'" + nombre + "'" + " , " + paisID + " , " + "'" + ccPais + "'" + " , " + iban + " , " + ccBanco + " , "
                                    + ccOficina + " , " + ccDC + " , " + ccCuenta + " , " + ccCuentaIBAN + " )";


            csUtilidades.ejecutarConsulta(insertarBanco, false);
        }





        private void subirAB2BToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exportarClientesPS();
        }

        private void btnDeleteText_Click(object sender, EventArgs e)
        {
            tbSearchText.Text = "";
        }


        public void actualziarDir(String codigoClienteALFA)
        {
            //Datos ERP
            String consultaERP = "select IdentificacionComoProveedor,DireccionFiscalID, DireccionMercaderiaID from v_cliente where ClienteID=" + codigoClienteALFA;
            DataTable datosCliERP = sql.cargarDatosTablaA3(consultaERP);

            //Datos PS
            String consultaPS = "select id_customer, type from ps_customer where id_customer=" + datosCliERP.Rows[0]["IdentificacionComoProveedor"].ToString();
            DataTable datosCliPS = mysql.cargarTabla(consultaPS);

            //Podria usar el "identificadorcomoprooveedor que es lo mismo
            String id_customerPS = datosCliPS.Rows[0]["id_customer"].ToString();

            //String para los updates:
            String update_dir = "";

            if (datosCliPS.Rows[0]["type"].ToString() == "F" || datosCliPS.Rows[0]["type"].ToString() == "P")
            {

                consultaPS = "select * from ps_address where id_customer=" + id_customerPS + " and main=1 and deleted=0";
                datosCliPS = mysql.cargarTabla(consultaPS);

                update_dir = "update ps_direccion set NombreViaPublica= '" + datosCliPS.Rows[0]["address1"].ToString() +
                             "' ,CPostal= '" + datosCliPS.Rows[0]["postcode"].ToString() + "' ,NombreMunicipio='" + datosCliPS.Rows[0]["city"].ToString() +
                             "' ,Telefono= " + datosCliPS.Rows[0]["phone"].ToString() + " where DireccionID=" + datosCliERP.Rows[0]["DireccionFiscalID"].ToString() +
                             " or DIreccionID=" + datosCliERP.Rows[0]["DireccionMercaderiaID"].ToString();

                csUtilidades.ejecutarConsulta(update_dir, false);


            }

            //MAYORISTAS

            /*
            else if (datosCliPS.Rows[0]["type"].ToString() == "MF" || datosCliPS.Rows[0]["type"].ToString() == "MP")
            {
                consultaPS = "select * from ps_address where id_customer=" + id_customerPS;
                datosCliERP = mysql.cargarTabla(consultaPS);

                foreach (DataRow row in datosCliERP.Rows)
                {
                    if (row["is_warehouse"].ToString() == "0" && row["main"].ToString() == "1")
                    {

                        update_dir = "update ps_direccion set NombreViaPublica=" + row["address1"].ToString() +
                           "CPostal=" + row["postcode"].ToString() + "NombreMunicipio=" + row["city"].ToString() +
                           "Telefono" + row["phone"].ToString() + "where DireccionID=" + datosCliERP.Rows[0]["DireccionFiscalID"].ToString();
                        csUtilidades.ejecutarConsulta(update_dir, false);

                    }
                    else

                    {
                        


                    }

                }





            }*/



        }

        public void actualizarDirecciones()
        {
            String consultaPS = "select id_customer,company,kls_a3erp_id,actualizar_banco_erp,actualizar_direccion_erp from ps_customer where actualizar_banco_erp!=0 or actualizar_direccion_erp!=0";
            String consultaERP = "";

            String actualizar_banco_erp = "update ps_customer set actualizar_banco_erp=0 where id_customer=";
            String actualizar_direccion_erp = "update ps_customer set actualizar_direccion_erp=0 where id_customer=";


            DataTable clientesPS = mysql.cargarTabla(consultaPS);
            DataTable clientesERP;
            if (clientesPS.Rows.Count > 0)
            {

                foreach (DataRow row in clientesPS.Rows)

                {
                    consultaERP = "select DeudorId, DireccionFiscalID from v_cliente where ClienteID=" + row["kls_a3erp_id"];
                    if (row["actualizar_banco_erp"].ToString() == "1")
                    {


                        clientesERP = sql.cargarDatosTablaA3(consultaERP);

                        actualizarBancoCliente(row["kls_a3erp_id"].ToString(), clientesERP.Rows[0]["DeudorId"].ToString());

                        csUtilidades.ejecutarConsulta(actualizar_banco_erp + row["id_customer"].ToString(), true);


                    }

                    if (row["actualizar_direccion_erp"].ToString() == "1")
                    {
                        actualziarDir(row["kls_a3erp_id"].ToString());



                        csUtilidades.ejecutarConsulta(actualizar_direccion_erp + row["id_customer"].ToString(), true);

                    }
                }

            }


        }
        public void actualizarAbonos(String codigoClienteALFA)
        {
            String consultaAlfa = "Select DeudorId from v_Cliente where ClienteId=" + codigoClienteALFA;
            String deudorId = sql.obtenerCampoTabla(consultaAlfa);


            String dirBancoID = sql.obtenerCampoTabla("select DireccionID from ps_Direccion where TipoDireccion=1 and DeudorID=" + deudorId);

            String updateAbonos = "update v_ClienteFormaPago set DireccionID= " + dirBancoID + ", ImporteHasta= 999999999 where Sentido=-1 and ClienteID=" + codigoClienteALFA;
            csUtilidades.ejecutarConsulta(updateAbonos, false);


        }
        public void actualizarCodigoProyecto()
        {
            String consultaAlfa = "select * from v_pedido where PedidoSegunCliente != ' ' and proyectoID IS NULL";
            //Mañana crear consulta para recuperar  los datos de Prestashop ( el representante unicamente) e introducirlo en alfa.


        }

        public void actualizarDatosEstadistica()
        {
            String selectAlfa = "select ClienteID,Nombre,Actividad2ID, IdentificacionComoProveedor from v_cliente where Actividad2ID IS NOT NULL and IdentificacionComoProveedor !=''";
            DataTable datosAlfa = sql.cargarDatosTablaA3(selectAlfa);

            foreach (DataRow row in datosAlfa.Rows)
            {
                String update = "update v_cliente set Actividad2ID= null where ClienteID=" + row["ClienteID"];
                csUtilidades.ejecutarConsulta(update, false);
            }

        }

        private void btnLoadMayoristas_Click(object sender, EventArgs e)
        {
            cargarMayoristas();
        }

        private void cargarMayoristas()
        {
            DataTable tabla = new DataTable();
            string queryMayoristas = "select kls_id_mayorista as id_mayorista, kls_codigo_mayorista as Codigo_Mayorista, kls_nombre_mayorista as Nombre_Mayorista, kls_email_mayorista as Email_Mayorista from kls_mayoristas";

            tabla = mysql.cargarTabla(queryMayoristas);



            csUtilidades.addDataSource(dgvMayoristas, tabla);
            dgvMayoristas.Columns["id_mayorista"].ReadOnly = true;




        }




        private void dgvPedidos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string id_customer = "0";
                string codigo_mayorista = "";
                string nombre_mayorista = "";
                string email_mayorista = "";






                if (MessageBox.Show("Se van a actualizar los datos en la Base de datos. \n ¿Está seguro?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    //id_customer = dgvMayoristas["id_mayorista", e.RowIndex].Value.ToString().Equals("") ? "0" : dgvMayoristas["id_mayorista", e.RowIndex].Value.ToString();

                    //codigo_mayorista = dgvMayoristas["Codigo_Mayorista", e.RowIndex].Value.ToString();

                    codigo_mayorista = dgvMayoristas.SelectedRows[0].Cells[0].Value.ToString();
                    /*
                    nombre_mayorista = dgvMayoristas["Nombre_Mayorista", e.RowIndex].Value.ToString();
                    email_mayorista = dgvMayoristas["Email_Mayorista", e.RowIndex].Value.ToString();
                    */
                    string consultaInsert = " insert into kls_mayoristas (kls_id_mayorista, kls_codigo_mayorista ,kls_nombre_mayorista,kls_email_mayorista) VALUES" +
                                            "(" + id_customer + "," + codigo_mayorista + ", " + nombre_mayorista + "," + email_mayorista + ") ON DUPLICATE KEY UPDATE kls_id_mayorista ="
                                            + id_customer + ", kls_codigo_mayorista" + codigo_mayorista + ",kls_nombre_mayorista" + nombre_mayorista + ", kls_email_mayorista " + email_mayorista;


                    csUtilidades.ejecutarConsulta(consultaInsert, true);
                    cargarMayoristas();

                }

            }
        }

        private void insertarMayorista_Click(object sender, EventArgs e)
        {
            string id_customer = "0";
            string codigo_mayorista = "''";
            string nombre_mayorista = "''";
            string email_mayorista = "''";


            if (dgvMayoristas.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Se van a actualizar los datos en la Base de datos. \n ¿Está seguro?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {



                    codigo_mayorista = dgvMayoristas.SelectedRows[0].Cells[1].Value.ToString();
                    nombre_mayorista = dgvMayoristas.SelectedRows[0].Cells[2].Value.ToString();
                    email_mayorista = dgvMayoristas.SelectedRows[0].Cells[3].Value.ToString();
                    string consultaInsert = " insert into kls_mayoristas (kls_id_mayorista, kls_codigo_mayorista ,kls_nombre_mayorista,kls_email_mayorista) VALUES" +
                                            "(" + id_customer + ",'" + codigo_mayorista + "', '" + nombre_mayorista + "','" + email_mayorista + "') ON DUPLICATE KEY UPDATE kls_id_mayorista ="
                                            + id_customer + ", kls_codigo_mayorista='" + codigo_mayorista + "', kls_nombre_mayorista= '" + nombre_mayorista + "', kls_email_mayorista= '" + email_mayorista + "'";

                    csUtilidades.ejecutarConsulta(consultaInsert, true);
                    MessageBox.Show("Mayorista Actualizado");

                    cargarMayoristas();

                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una linea.");
            }
        }

        private void borrarMayorista_Click_1(object sender, EventArgs e)
        {
            if (dgvMayoristas.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Se va a borrar el siguiente mayorista de prestashop. \n ¿Está seguro?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string id = dgvMayoristas.SelectedRows[0].Cells[0].Value.ToString();

                    string consultaDelete = "delete from kls_mayoristas where kls_id_mayorista=" + id;

                    csUtilidades.ejecutarConsulta(consultaDelete, true);
                    cargarMayoristas();
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una linea.");
            }
        }


    }
}