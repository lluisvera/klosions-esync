﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace klsync
{
    class csXMLUtilities
    {

        public XmlDocument xmlDoc(string textXML)
        {

            String rawXml = textXML;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(rawXml);
            return xmlDoc;
        }

        public void AbrirXML(string rutaXML)
        {
            XmlDocument ficheroXML = new XmlDocument();
            ficheroXML.Load(rutaXML.ToString());
        }

        public string buscarValorXML(string nombreNodo)
        {
            string valorXML = "";
            if (File.Exists("config.XML"))
            {
                XmlDocument ficheroXML = new XmlDocument();
                ficheroXML.Load("config.XML");
                XmlElement elementoRaiz = ficheroXML.DocumentElement;
                XmlNodeList listaNodos = ficheroXML.GetElementsByTagName(nombreNodo);
                foreach (XmlNode nodo in listaNodos)
                {
                    if (nodo.Name.ToString() == nombreNodo)
                    {
                        valorXML = nodo.InnerText.ToString();
                    }
                }
            }
            return valorXML;
        }

        public string buscarValorXML(string nombreNodo, XmlElement xmlElemento)
        {
            string valorXML = "";


            XmlNodeList listaNodos = xmlElemento.GetElementsByTagName(nombreNodo);
            foreach (XmlNode nodo in listaNodos)
            {
                if (nodo.Name.ToString() == nombreNodo)
                {
                    valorXML = nodo.InnerText.ToString();
                }
            }

            return valorXML;
        }

        public string buscarValorXML(string nombreNodo, XmlDocument xml)
        {
        
          string valorXML = "";


            XmlNodeList listaNodos = xml.GetElementsByTagName(nombreNodo);
            foreach (XmlNode nodo in listaNodos)
            {
                if (nodo.Name.ToString() == nombreNodo)
                {
                    valorXML = nodo.InnerText.ToString();
                }
            }

            return valorXML;
        
        
        }

        public string[] buscarListaXML(string nombreNodo)
        {
            string[] ListaXML;
            ListaXML = new string[0];
            int i = 0;
            if (File.Exists("config.XML"))
            {
                XmlDocument ficheroXML = new XmlDocument();
                ficheroXML.Load("config.XML");
                XmlElement elementoRaiz = ficheroXML.DocumentElement;
                XmlNodeList listaNodos = ficheroXML.GetElementsByTagName(nombreNodo);
                int dimension = listaNodos.Count;
                ListaXML = new string[dimension];
                foreach (XmlNode nodo in listaNodos)
                {
                    if (nodo.Name.ToString() == nombreNodo)
                    {
                        ListaXML[i] = nodo.InnerText.ToString();
                        i++;
                    }
                }
            }
            return ListaXML;
        }

        public void crearConfigXML()
        {
            XmlTextWriter escribeXML = new XmlTextWriter("config.xml", System.Text.Encoding.UTF8);
            escribeXML.Formatting = System.Xml.Formatting.Indented;
            escribeXML.WriteStartDocument();
            escribeXML.WriteStartElement(null, "configuracion", null);

            escribeXML.WriteStartElement(null, "ConexionBD", null);

            escribeXML.WriteStartElement(null, "ServerName", null);
            escribeXML.WriteString("LVGNOTEBOOK\\SQLEXPRESS");
            escribeXML.WriteEndElement();

            escribeXML.WriteStartElement(null, "Database", null);
            escribeXML.WriteString("GESTIONDOCS");
            escribeXML.WriteEndElement();

            escribeXML.WriteStartElement(null, "Usuario", null);
            escribeXML.WriteString("sa");
            escribeXML.WriteEndElement();

            escribeXML.WriteStartElement(null, "Password", null);
            escribeXML.WriteString("");
            escribeXML.WriteEndElement();

            escribeXML.WriteEndElement();
            escribeXML.WriteEndElement();
            escribeXML.Close();
        }

        public void borrarNodoXML(string fichero, string nombreNodo)
        {
            XmlDocument docXML = new XmlDocument();
            docXML.Load(fichero);
            XmlElement root = docXML.DocumentElement;
            XmlNodeList ListaNodos = docXML.GetElementsByTagName(nombreNodo);
            XmlNode NodoObjetivo = ListaNodos[0];
            foreach (XmlNode Nodo in ListaNodos)
            {
                if (Nodo.Name == nombreNodo)
                {
                    NodoObjetivo = Nodo;
                }
            }
            NodoObjetivo.ParentNode.RemoveChild(NodoObjetivo);
            docXML.Save(fichero);

        }

        public string XMLtoJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return JsonConvert.SerializeXmlNode(doc);
        }

        
    }
}
