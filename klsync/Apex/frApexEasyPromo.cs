﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.OleDb;

namespace klsync.Apex
{
    public partial class frApexEasyPromo : Form
    {

        private static frApexEasyPromo m_FormDefInstance;
        public static frApexEasyPromo DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frApexEasyPromo();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frApexEasyPromo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OdbcConnection DbConnection = new OdbcConnection("DSN=ApexOdbc");
            string connetionString = null;
            DataTable dt = new DataTable();
            DataTable dtClients = new DataTable();
            //OdbcConnection cnn;
            //connetionString = "Driver={Microsoft Access Driver (*.mdb)};DBQ=yourdatabasename.mdb;";
            //DbConnection = new OdbcConnection(connetionString);
            try
            {
                DbConnection.Open();
                MessageBox.Show("Connection Open ! ");
                DataTable schema = DbConnection.GetSchema("Tables");
                List<string> TableNames = new List<string>();
                foreach (DataRow row in schema.Rows)
                {
                    TableNames.Add(row[2].ToString());
                }
               // string strSQL = "SELECT * FROM CLI_Clients";
                string strSQL = "SELECT * FROM dac_pro_LAB_Laboratoires";
                

                OdbcCommand command = new OdbcCommand(strSQL, DbConnection);
                command.CommandType = CommandType.Text;
                OdbcDataReader dr = command.ExecuteReader();
                dtClients.Load(dr);


                DbConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
            }

        }
    }
}
