﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace klsync
{
    class csModulesCheck
    {

        csMySqlConnect mysqlConnect = new csMySqlConnect();
        private MySqlConnection connection;

        public bool checkModuloReferenciaCheckou()
        {
            bool moduloActivo = false;
            connection = new MySqlConnection(mysqlConnect.conexionDestino());
            connection.Open();

            try
            {
                string query = "select name,active from ps_module where name='klscheckoutreference' and active=1";

                MySqlCommand cmd = new MySqlCommand(query, connection);
                if (cmd.ExecuteScalar() != null)
                {
                    moduloActivo = true;
                }
            }
            finally
            {
                connection.Close();
            }

            return moduloActivo;
        }
    }
}
