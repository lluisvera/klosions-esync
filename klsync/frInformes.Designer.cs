﻿namespace klsync
{
    partial class frInformes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frInformes));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.clBoxArticulos = new System.Windows.Forms.CheckedListBox();
            this.cboxArticulos = new System.Windows.Forms.CheckBox();
            this.cboxFormasPago = new System.Windows.Forms.CheckBox();
            this.cbFormaPago = new System.Windows.Forms.ComboBox();
            this.rbDocsPtes = new System.Windows.Forms.RadioButton();
            this.rbQueryDocs = new System.Windows.Forms.RadioButton();
            this.rbQueryFras = new System.Windows.Forms.RadioButton();
            this.lbHasta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.btLoadSelection = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvDocumentosPS = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportarAExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.rbDocsPtesPago = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPS)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(480, 39);
            this.label1.TabIndex = 63;
            this.label1.Text = "LISTADO DE DOCUMENTOS";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbDocsPtesPago);
            this.groupBox1.Controls.Add(this.clBoxArticulos);
            this.groupBox1.Controls.Add(this.cboxArticulos);
            this.groupBox1.Controls.Add(this.cboxFormasPago);
            this.groupBox1.Controls.Add(this.cbFormaPago);
            this.groupBox1.Controls.Add(this.rbDocsPtes);
            this.groupBox1.Controls.Add(this.rbQueryDocs);
            this.groupBox1.Controls.Add(this.rbQueryFras);
            this.groupBox1.Controls.Add(this.lbHasta);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpHasta);
            this.groupBox1.Controls.Add(this.dtpDesde);
            this.groupBox1.Controls.Add(this.btLoadSelection);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(19, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1000, 193);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selección";
            // 
            // clBoxArticulos
            // 
            this.clBoxArticulos.FormattingEnabled = true;
            this.clBoxArticulos.Location = new System.Drawing.Point(443, 22);
            this.clBoxArticulos.Name = "clBoxArticulos";
            this.clBoxArticulos.Size = new System.Drawing.Size(203, 124);
            this.clBoxArticulos.TabIndex = 44;
            // 
            // cboxArticulos
            // 
            this.cboxArticulos.AutoSize = true;
            this.cboxArticulos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxArticulos.Location = new System.Drawing.Point(348, 24);
            this.cboxArticulos.Name = "cboxArticulos";
            this.cboxArticulos.Size = new System.Drawing.Size(89, 24);
            this.cboxArticulos.TabIndex = 43;
            this.cboxArticulos.Text = "Artículos";
            this.cboxArticulos.UseVisualStyleBackColor = true;
            this.cboxArticulos.CheckedChanged += new System.EventHandler(this.cboxArticulos_CheckedChanged);
            // 
            // cboxFormasPago
            // 
            this.cboxFormasPago.AutoSize = true;
            this.cboxFormasPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxFormasPago.Location = new System.Drawing.Point(40, 147);
            this.cboxFormasPago.Name = "cboxFormasPago";
            this.cboxFormasPago.Size = new System.Drawing.Size(145, 24);
            this.cboxFormasPago.TabIndex = 42;
            this.cboxFormasPago.Text = "Formas de Pago";
            this.cboxFormasPago.UseVisualStyleBackColor = true;
            // 
            // cbFormaPago
            // 
            this.cbFormaPago.DropDownWidth = 200;
            this.cbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFormaPago.ForeColor = System.Drawing.Color.Navy;
            this.cbFormaPago.FormattingEnabled = true;
            this.cbFormaPago.Items.AddRange(new object[] {
            "LaCaixa",
            "Transferencia bancaria"});
            this.cbFormaPago.Location = new System.Drawing.Point(190, 143);
            this.cbFormaPago.Name = "cbFormaPago";
            this.cbFormaPago.Size = new System.Drawing.Size(137, 28);
            this.cbFormaPago.TabIndex = 41;
            // 
            // rbDocsPtes
            // 
            this.rbDocsPtes.AutoSize = true;
            this.rbDocsPtes.Checked = true;
            this.rbDocsPtes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDocsPtes.ForeColor = System.Drawing.Color.Navy;
            this.rbDocsPtes.Location = new System.Drawing.Point(40, 32);
            this.rbDocsPtes.Name = "rbDocsPtes";
            this.rbDocsPtes.Size = new System.Drawing.Size(242, 24);
            this.rbDocsPtes.TabIndex = 38;
            this.rbDocsPtes.Text = "Docs Pendientes Traspaso A3";
            this.rbDocsPtes.UseVisualStyleBackColor = true;
            // 
            // rbQueryDocs
            // 
            this.rbQueryDocs.AutoSize = true;
            this.rbQueryDocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbQueryDocs.ForeColor = System.Drawing.Color.Navy;
            this.rbQueryDocs.Location = new System.Drawing.Point(40, 117);
            this.rbQueryDocs.Name = "rbQueryDocs";
            this.rbQueryDocs.Size = new System.Drawing.Size(136, 24);
            this.rbQueryDocs.TabIndex = 37;
            this.rbQueryDocs.Text = "Todos los Docs";
            this.rbQueryDocs.UseVisualStyleBackColor = true;
            // 
            // rbQueryFras
            // 
            this.rbQueryFras.AutoSize = true;
            this.rbQueryFras.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbQueryFras.ForeColor = System.Drawing.Color.Navy;
            this.rbQueryFras.Location = new System.Drawing.Point(40, 92);
            this.rbQueryFras.Name = "rbQueryFras";
            this.rbQueryFras.Size = new System.Drawing.Size(95, 24);
            this.rbQueryFras.TabIndex = 36;
            this.rbQueryFras.Text = "Sólo Fras";
            this.rbQueryFras.UseVisualStyleBackColor = true;
            // 
            // lbHasta
            // 
            this.lbHasta.AutoSize = true;
            this.lbHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHasta.ForeColor = System.Drawing.Color.Navy;
            this.lbHasta.Location = new System.Drawing.Point(722, 70);
            this.lbHasta.Name = "lbHasta";
            this.lbHasta.Size = new System.Drawing.Size(58, 20);
            this.lbHasta.TabIndex = 29;
            this.lbHasta.Text = "Hasta*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(722, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Desde";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(814, 65);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(137, 26);
            this.dtpHasta.TabIndex = 27;
            this.dtpHasta.Value = new System.DateTime(2013, 12, 31, 0, 0, 0, 0);
            // 
            // dtpDesde
            // 
            this.dtpDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(814, 30);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(137, 26);
            this.dtpDesde.TabIndex = 26;
            this.dtpDesde.Value = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            // 
            // btLoadSelection
            // 
            this.btLoadSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadSelection.ForeColor = System.Drawing.Color.Navy;
            this.btLoadSelection.Image = ((System.Drawing.Image)(resources.GetObject("btLoadSelection.Image")));
            this.btLoadSelection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadSelection.Location = new System.Drawing.Point(814, 111);
            this.btLoadSelection.Name = "btLoadSelection";
            this.btLoadSelection.Size = new System.Drawing.Size(137, 35);
            this.btLoadSelection.TabIndex = 3;
            this.btLoadSelection.Text = "Cargar Datos";
            this.btLoadSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLoadSelection.UseVisualStyleBackColor = true;
            this.btLoadSelection.Click += new System.EventHandler(this.btLoadSelection_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(19, 232);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1137, 506);
            this.tabControl1.TabIndex = 60;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvDocumentosPS);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1129, 480);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DOCUMENTOS PRESTASHOP";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvDocumentosPS
            // 
            this.dgvDocumentosPS.AllowUserToAddRows = false;
            this.dgvDocumentosPS.AllowUserToDeleteRows = false;
            this.dgvDocumentosPS.AllowUserToOrderColumns = true;
            this.dgvDocumentosPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocumentosPS.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvDocumentosPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocumentosPS.Location = new System.Drawing.Point(3, 3);
            this.dgvDocumentosPS.Name = "dgvDocumentosPS";
            this.dgvDocumentosPS.ReadOnly = true;
            this.dgvDocumentosPS.Size = new System.Drawing.Size(1123, 474);
            this.dgvDocumentosPS.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportarAExcelToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(156, 26);
            // 
            // exportarAExcelToolStripMenuItem
            // 
            this.exportarAExcelToolStripMenuItem.Name = "exportarAExcelToolStripMenuItem";
            this.exportarAExcelToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exportarAExcelToolStripMenuItem.Text = "&Exportar a Excel";
            this.exportarAExcelToolStripMenuItem.Click += new System.EventHandler(this.exportarAExcelToolStripMenuItem_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.BackgroundImage")));
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbLogo.Location = new System.Drawing.Point(1067, 12);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(89, 76);
            this.pbLogo.TabIndex = 64;
            this.pbLogo.TabStop = false;
            // 
            // rbDocsPtesPago
            // 
            this.rbDocsPtesPago.AutoSize = true;
            this.rbDocsPtesPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDocsPtesPago.ForeColor = System.Drawing.Color.Navy;
            this.rbDocsPtesPago.Location = new System.Drawing.Point(40, 62);
            this.rbDocsPtesPago.Name = "rbDocsPtesPago";
            this.rbDocsPtesPago.Size = new System.Drawing.Size(189, 24);
            this.rbDocsPtesPago.TabIndex = 45;
            this.rbDocsPtesPago.Text = "Docs Pendientes Pago";
            this.rbDocsPtesPago.UseVisualStyleBackColor = true;
            // 
            // frInformes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1168, 750);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Name = "frInformes";
            this.Text = "Listado de Documentos";
            this.Load += new System.EventHandler(this.frInformes_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPS)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cboxFormasPago;
        private System.Windows.Forms.ComboBox cbFormaPago;
        private System.Windows.Forms.RadioButton rbDocsPtes;
        private System.Windows.Forms.RadioButton rbQueryDocs;
        private System.Windows.Forms.RadioButton rbQueryFras;
        private System.Windows.Forms.Label lbHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.Button btLoadSelection;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvDocumentosPS;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportarAExcelToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox clBoxArticulos;
        private System.Windows.Forms.CheckBox cboxArticulos;
        private System.Windows.Forms.RadioButton rbDocsPtesPago;
    }
}