﻿namespace klsync
{
    partial class frFeatures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvCaracteristicasPS = new System.Windows.Forms.DataGridView();
            this.dgvCaracteristicasA3 = new System.Windows.Forms.DataGridView();
            this.btLoadCaractPS = new System.Windows.Forms.Button();
            this.btLoadCaractA3 = new System.Windows.Forms.Button();
            this.btCopyCaractToA3 = new System.Windows.Forms.Button();
            this.dgvFeaturesProducts = new System.Windows.Forms.DataGridView();
            this.contextFeature = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertarFeatureEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFeatureProduct = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeaturesProducts)).BeginInit();
            this.contextFeature.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCaracteristicasPS
            // 
            this.dgvCaracteristicasPS.AllowUserToAddRows = false;
            this.dgvCaracteristicasPS.AllowUserToDeleteRows = false;
            this.dgvCaracteristicasPS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCaracteristicasPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaracteristicasPS.Location = new System.Drawing.Point(12, 77);
            this.dgvCaracteristicasPS.Name = "dgvCaracteristicasPS";
            this.dgvCaracteristicasPS.ReadOnly = true;
            this.dgvCaracteristicasPS.Size = new System.Drawing.Size(631, 241);
            this.dgvCaracteristicasPS.TabIndex = 0;
            // 
            // dgvCaracteristicasA3
            // 
            this.dgvCaracteristicasA3.AllowUserToAddRows = false;
            this.dgvCaracteristicasA3.AllowUserToDeleteRows = false;
            this.dgvCaracteristicasA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCaracteristicasA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaracteristicasA3.Location = new System.Drawing.Point(12, 360);
            this.dgvCaracteristicasA3.Name = "dgvCaracteristicasA3";
            this.dgvCaracteristicasA3.ReadOnly = true;
            this.dgvCaracteristicasA3.Size = new System.Drawing.Size(631, 252);
            this.dgvCaracteristicasA3.TabIndex = 1;
            // 
            // btLoadCaractPS
            // 
            this.btLoadCaractPS.Location = new System.Drawing.Point(12, 41);
            this.btLoadCaractPS.Name = "btLoadCaractPS";
            this.btLoadCaractPS.Size = new System.Drawing.Size(113, 30);
            this.btLoadCaractPS.TabIndex = 2;
            this.btLoadCaractPS.Text = "Características PS";
            this.btLoadCaractPS.UseVisualStyleBackColor = true;
            this.btLoadCaractPS.Click += new System.EventHandler(this.btLoadCaractPS_Click);
            // 
            // btLoadCaractA3
            // 
            this.btLoadCaractA3.Location = new System.Drawing.Point(12, 324);
            this.btLoadCaractA3.Name = "btLoadCaractA3";
            this.btLoadCaractA3.Size = new System.Drawing.Size(113, 30);
            this.btLoadCaractA3.TabIndex = 3;
            this.btLoadCaractA3.Text = "Características A3";
            this.btLoadCaractA3.UseVisualStyleBackColor = true;
            this.btLoadCaractA3.Click += new System.EventHandler(this.btLoadCaractA3_Click);
            // 
            // btCopyCaractToA3
            // 
            this.btCopyCaractToA3.Location = new System.Drawing.Point(131, 324);
            this.btCopyCaractToA3.Name = "btCopyCaractToA3";
            this.btCopyCaractToA3.Size = new System.Drawing.Size(89, 30);
            this.btCopyCaractToA3.TabIndex = 4;
            this.btCopyCaractToA3.Text = "V V V V V V";
            this.btCopyCaractToA3.UseVisualStyleBackColor = true;
            this.btCopyCaractToA3.Click += new System.EventHandler(this.btCopyCaractToA3_Click);
            // 
            // dgvFeaturesProducts
            // 
            this.dgvFeaturesProducts.AllowUserToAddRows = false;
            this.dgvFeaturesProducts.AllowUserToDeleteRows = false;
            this.dgvFeaturesProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFeaturesProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFeaturesProducts.ContextMenuStrip = this.contextFeature;
            this.dgvFeaturesProducts.Location = new System.Drawing.Point(649, 77);
            this.dgvFeaturesProducts.Name = "dgvFeaturesProducts";
            this.dgvFeaturesProducts.ReadOnly = true;
            this.dgvFeaturesProducts.Size = new System.Drawing.Size(615, 535);
            this.dgvFeaturesProducts.TabIndex = 5;
            // 
            // contextFeature
            // 
            this.contextFeature.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertarFeatureEnA3ToolStripMenuItem});
            this.contextFeature.Name = "contextFeature";
            this.contextFeature.Size = new System.Drawing.Size(189, 26);
            // 
            // insertarFeatureEnA3ToolStripMenuItem
            // 
            this.insertarFeatureEnA3ToolStripMenuItem.Name = "insertarFeatureEnA3ToolStripMenuItem";
            this.insertarFeatureEnA3ToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.insertarFeatureEnA3ToolStripMenuItem.Text = "Insertar Feature en A3";
            this.insertarFeatureEnA3ToolStripMenuItem.Click += new System.EventHandler(this.insertarFeatureEnA3ToolStripMenuItem_Click);
            // 
            // btnFeatureProduct
            // 
            this.btnFeatureProduct.Location = new System.Drawing.Point(1151, 41);
            this.btnFeatureProduct.Name = "btnFeatureProduct";
            this.btnFeatureProduct.Size = new System.Drawing.Size(113, 30);
            this.btnFeatureProduct.TabIndex = 6;
            this.btnFeatureProduct.Text = "Cargar";
            this.btnFeatureProduct.UseVisualStyleBackColor = true;
            this.btnFeatureProduct.Click += new System.EventHandler(this.btnFeatureProduct_Click);
            // 
            // frFeatures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1342, 624);
            this.Controls.Add(this.btnFeatureProduct);
            this.Controls.Add(this.dgvFeaturesProducts);
            this.Controls.Add(this.btCopyCaractToA3);
            this.Controls.Add(this.btLoadCaractA3);
            this.Controls.Add(this.btLoadCaractPS);
            this.Controls.Add(this.dgvCaracteristicasA3);
            this.Controls.Add(this.dgvCaracteristicasPS);
            this.Name = "frFeatures";
            this.Text = "Características";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeaturesProducts)).EndInit();
            this.contextFeature.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCaracteristicasPS;
        private System.Windows.Forms.DataGridView dgvCaracteristicasA3;
        private System.Windows.Forms.Button btLoadCaractPS;
        private System.Windows.Forms.Button btLoadCaractA3;
        private System.Windows.Forms.Button btCopyCaractToA3;
        private System.Windows.Forms.DataGridView dgvFeaturesProducts;
        private System.Windows.Forms.Button btnFeatureProduct;
        private System.Windows.Forms.ContextMenuStrip contextFeature;
        private System.Windows.Forms.ToolStripMenuItem insertarFeatureEnA3ToolStripMenuItem;
    }
}