﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace klsync
{
    public partial class frPresupuestos : Form
    {
        csSqlConnects sql = new csSqlConnects();
        private static frPresupuestos m_FormDefInstance;
        public static frPresupuestos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPresupuestos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frPresupuestos()
        {
            InitializeComponent();
            toolStripButtonPasarPresupuestoA3.Enabled = false;
            toolStripButtonHelp.ToolTipText = "Para importar el presupuesto es necesario copiar las columnas con el siguiente formato: " + "\n" +
                "NOMBRES COLUMNAS: " + "\n" +
                "- centrocoste1" + "\n" + 
                "- centrocoste2" + "\n" + 
                "- centrocoste3" + "\n" + 
                "- 8D (Cuenta Contable 8 dígitos)" + "\n" + 
                "- SUM (Importe Presupuestado)" + "\n" + 
                "- EJERCICIO" + "\n" +
                "- MONTH" + "\n" +
                "- TYPE " + "\n" + 
                " M (Mensual) o Y (Año)";
        }
        
        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            paste2DGV();
        }
        
        private void paste2DGV()
        {
            limpiarDatagridview();

            csCopyPaste copyPaste = new csCopyPaste();
            //copyPaste.PasteClipboard(dgvPresupuestos);
            copyPaste.pasteClipboardDT(dgvPresupuestos,true);
            if (dgvPresupuestos.Rows.Count > 0)
            {
                toolStripButtonPasarPresupuestoA3.Enabled = true;
                numFilas.Text = dgvPresupuestos.Rows.Count + " filas";
            }
        }
        
        private void limpiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            limpiarDatagridview();
            toolStripButtonPasarPresupuestoA3.Enabled = false;
            numFilas.Text = "0 filas";
        }
        
        private void limpiarDatagridview()
        {
            try
            {
                dgvPresupuestos.Rows.Clear();
                dgvPresupuestos.Columns.Clear();
            }
            catch (Exception)
            {
                dgvPresupuestos.DataSource = null;
            }

        }
        
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.V))
            {
                paste2DGV();

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        
        private DataTable fillDataTableFromDataGridView(DataGridView dgv)
        {
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                dt.Columns.Add(col.HeaderText);
            }

            foreach (DataGridViewRow row in dgv.Rows)
            {
                DataRow dRow = dt.NewRow();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRow[cell.ColumnIndex] = cell.Value;
                }
                dt.Rows.Add(dRow);
            }

            return dt;
        }
        
        private void toolStripButtonPasarPresupuestoA3_Click(object sender, EventArgs e)
        {
            try
            {
                int contador = 0;
                string cuentaAfectada = "";
                string cuentaPresupuesto = "";
                string tipoPresupuesto = "";        //M Mensual o Y Anual
                int mes = 0;
                string ejercicio = "";
                string values_lineas = "";
                string consulta_lineas = "";
                string centro_costes = "";
                double importeLinPresupuesto = 0;
                double totalImporteAnual = 0;
                int cuentasExistentes = 0;

                
                if (dgvPresupuestos.Rows.Count > 0)
                {
                    string cuentaBudget = "";
                    string cuentaA3 = "";
                    DataTable cuentasPresupuesto = new DataTable();
                    cuentasPresupuesto.Columns.Add("CUENTA");
                    cuentasPresupuesto.Columns.Add("EXISTE");
                    //COL1 --> CUENTA CONTABLE
                    string col1 = "", col2 = "", query_values = "";
                    double precio = 0, sumatorio = 0;
                    DataTable dt = fillDataTableFromDataGridView(dgvPresupuestos);
                    //Busco las cuentas afectadas por el presupuesto
                    DataView view = new DataView(dt);
                    DataTable distinctValues = view.ToTable(true, "8D");
                    
                    //Traspaso las cuentas a una tabla temporal para validar si existen o no
                    foreach (DataRow fila in distinctValues.Rows)
                    {
                        cuentasPresupuesto.Rows.Add(fila[0], "");
                    }
                    
                    DataTable cuentas = sql.cargarDatosTablaA3("SELECT cuenta FROM CUENTAS WHERE LEFT(CUENTA,1) IN (6,7,9) AND PLACON='NPGC' AND NIVEL=5");

                    foreach (DataRow filaVerificar in cuentasPresupuesto.Rows)
                    {
                        cuentaBudget = filaVerificar["CUENTA"].ToString();
                        foreach (DataRow filaCuentasA3 in cuentas.Rows)
                        {
                            if (cuentaBudget == filaCuentasA3[0].ToString())
                            {
                                filaVerificar["EXISTE"] = "OK";
                                break;
                            }
                        }
                    }

                    DataView cuentasParaCrear = new DataView(cuentasPresupuesto);
                    DataTable cuentas_por_crear = cuentasParaCrear.ToTable(true, "CUENTA","EXISTE");

                    for (int i = cuentas_por_crear.Rows.Count-1; i >= 0; i--)
                    {

                        if (cuentas_por_crear.Rows[i]["EXISTE"].ToString() == "OK")
                        {
                            cuentas_por_crear.Rows[i].Delete();
                        }
                    }

                    // Comprobar que las cuentas existen
                    //query_values = csUtilidades.concatenarValoresQueryFromDataTable(distinctValues, "8D");
                    
                    //DataTable cuentas_por_crear = csUtilidades.getDiffDataTables(distinctValues, cuentas);

                    // Comprobar que no queda ninguna cuenta por crear
                    if (cuentas_por_crear.Rows.Count == 0)
                    {
                        //Itero por cada cuenta afectada 
                        foreach (DataRow fila in distinctValues.Rows)
                        {
                            cuentaAfectada = fila["8D"].ToString();
                            
                            foreach (DataRow dr in dt.Rows)
                            {
                                cuentaPresupuesto = dr["8D"].ToString();
                                if (cuentaAfectada == cuentaPresupuesto)
                                {
                                    mes = Convert.ToInt32(dr["MONTH"].ToString());
                                    tipoPresupuesto = dr["TYPE"].ToString();
                                    ejercicio = dr["EJERCICIO"].ToString();
                                    col1 = dr["8D"].ToString();
                                    //col2 = distinctValues.Rows[0]["8D"].ToString();
                                    centro_costes = "'       1'" + "," + "'       1'" + "," + "'       1'";
                                   
                                    //Valido importe de la línea
                                    if (dr["SUM"].ToString() == "")
                                    {
                                        importeLinPresupuesto =0;
                                    }
                                    else
                                    {
                                        importeLinPresupuesto=Convert.ToDouble(dr["SUM"].ToString());
                                    }

                                    //Para registros mensuales
                                    if (tipoPresupuesto == "M")
                                    {
                                        values_lineas = centro_costes + ",'EURO', '" + col1 + "', '" + ejercicio + "'," + importeLinPresupuesto.ToString().Replace(",", ".") + "," + mes.ToString() + ",1,1";
                                        consulta_lineas = "INSERT INTO LINEPRES (CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, CODMON, CUENTA, EJERCICIO, IMPORTE, MES, PESO, TIPOCONT) VALUES (" + values_lineas + ")";

                                        csUtilidades.ejecutarConsulta(consulta_lineas, false);
                                        totalImporteAnual = totalImporteAnual + importeLinPresupuesto;
                                    }
                                    else // Si el tipo es año
                                    {
                                        double price_monthly = Convert.ToDouble(importeLinPresupuesto) / 12;
                                        for (int i = 1; i <= 12; i++)
                                        {
                                            values_lineas = centro_costes + ",'EURO', '" + col1 + "', '" + ejercicio + "'," + price_monthly.ToString().Replace(",", ".") + "," + i.ToString() + ",1,1";
                                            consulta_lineas = "INSERT INTO LINEPRES (CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, CODMON, CUENTA, EJERCICIO, IMPORTE, MES, PESO, TIPOCONT) VALUES (" + values_lineas + ")";

                                            csUtilidades.ejecutarConsulta(consulta_lineas, false);
                                            totalImporteAnual = totalImporteAnual + importeLinPresupuesto;
                                        }

                                        price_monthly = 0;
                                    }

                                }
                            }
                            
                            //Inserto Totales

                            string values = centro_costes + ",'EURO', '" + cuentaAfectada + "', '" + ejercicio + "',NULL, " + totalImporteAnual.ToString().Replace(",", ".") + ",NULL,NULL,NULL,1";
                            string consulta = "INSERT INTO PRESUP (CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, CODMON, CUENTA, EJERCICIO, EJERCICIOORI, IMPORTE, INCREMENTO, ORIIMPORTE, ORIPESO, TIPOCONT) VALUES (" + values + ")";
                            csUtilidades.ejecutarConsulta(consulta, false);
                            

                        }

                        listBoxPresupuestos.Items.Clear();
                        listBoxPresupuestos.Items.Add("Inserción de presupuestos finalizada");
                    }
                    else
                    {
                        listBoxPresupuestos.Items.Clear();
                        listBoxPresupuestos.Items.Add("Las cuentas contables siguientes deben ser creadas antes:");
                        foreach (DataRow dr in cuentas_por_crear.Rows)
                        {
                            listBoxPresupuestos.Items.Add("   " + dr["CUENTA"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }
        
        private void toolStripButtonBorrarCabeceras_Click(object sender, EventArgs e)
        {
            DataTable dt = sql.cargarDatosTablaA3("SELECT DISTINCT EJERCICIO FROM PRESUP");
            frDialogCombo combo = new frDialogCombo(dt, "EJERCICIO", "Selecciona el ejercicio a borrar");

            if (combo.ShowDialog() == DialogResult.OK)
            {
                csUtilidades.ejecutarConsulta("DELETE FROM PRESUP WHERE EJERCICIO = '" + csGlobal.comboFormValue + "'",false);
                csUtilidades.ejecutarConsulta("DELETE FROM LINEPRES WHERE EJERCICIO = '" + csGlobal.comboFormValue + "'",false);

                listBoxPresupuestos.Items.Clear();
                listBoxPresupuestos.Items.Add("Los presupuestos del ejercicio " + csGlobal.comboFormValue + " han sido borrados");
            }
        }

        private void abrirDesdeCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            limpiarDatagridview();

            Stream myStream = null;
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Abrir archivo";
            theDialog.Filter = "All files|*.*";
            //theDialog.InitialDirectory = @"C:\Users\Alex\Desktop";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = theDialog.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            dgvPresupuestos.DataSource = csUtilidades.ConvertCSVtoDataTable(theDialog.FileName);
                        }
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Cierra el documento para importar el csv", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (DuplicateNameException ex)
                {
                }
            }

            if (dgvPresupuestos.Rows.Count > 0)
                toolStripButtonPasarPresupuestoA3.Enabled = true;
        }


    }
}
