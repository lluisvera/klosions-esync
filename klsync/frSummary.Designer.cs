﻿namespace klsync
{
    partial class frSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripResumen = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.treeViewSummaryA3 = new System.Windows.Forms.TreeView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeViewSummaryPS = new System.Windows.Forms.TreeView();
            this.toolStripButtonReload = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonA3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripResumen.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripResumen
            // 
            this.toolStripResumen.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonReload,
            this.toolStripSeparator1,
            this.toolStripButtonA3,
            this.toolStripSeparator2,
            this.toolStripButtonPS,
            this.toolStripSeparator3});
            this.toolStripResumen.Location = new System.Drawing.Point(0, 0);
            this.toolStripResumen.Name = "toolStripResumen";
            this.toolStripResumen.Size = new System.Drawing.Size(1048, 54);
            this.toolStripResumen.TabIndex = 1;
            this.toolStripResumen.Text = "Resumen A3";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Padding = new System.Windows.Forms.Padding(20, 20, 0, 0);
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 54);
            // 
            // treeViewSummaryA3
            // 
            this.treeViewSummaryA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewSummaryA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeViewSummaryA3.Location = new System.Drawing.Point(0, 0);
            this.treeViewSummaryA3.Name = "treeViewSummaryA3";
            this.treeViewSummaryA3.Size = new System.Drawing.Size(494, 510);
            this.treeViewSummaryA3.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 54);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeViewSummaryA3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.treeViewSummaryPS);
            this.splitContainer1.Size = new System.Drawing.Size(1048, 510);
            this.splitContainer1.SplitterDistance = 494;
            this.splitContainer1.TabIndex = 3;
            // 
            // treeViewSummaryPS
            // 
            this.treeViewSummaryPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewSummaryPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeViewSummaryPS.Location = new System.Drawing.Point(0, 0);
            this.treeViewSummaryPS.Name = "treeViewSummaryPS";
            this.treeViewSummaryPS.Size = new System.Drawing.Size(550, 510);
            this.treeViewSummaryPS.TabIndex = 3;
            // 
            // toolStripButtonReload
            // 
            this.toolStripButtonReload.Image = global::klsync.Properties.Resources.reload2;
            this.toolStripButtonReload.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonReload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReload.Name = "toolStripButtonReload";
            this.toolStripButtonReload.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolStripButtonReload.Size = new System.Drawing.Size(116, 51);
            this.toolStripButtonReload.Text = "Actualizar todos";
            this.toolStripButtonReload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonReload.Click += new System.EventHandler(this.toolStripButtonReload_Click);
            // 
            // toolStripButtonA3
            // 
            this.toolStripButtonA3.Image = global::klsync.Properties.Resources._40625281;
            this.toolStripButtonA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonA3.Name = "toolStripButtonA3";
            this.toolStripButtonA3.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.toolStripButtonA3.Size = new System.Drawing.Size(89, 51);
            this.toolStripButtonA3.Text = "Act. A3";
            this.toolStripButtonA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonA3.Click += new System.EventHandler(this.toolStripButtonA3_Click);
            // 
            // toolStripButtonPS
            // 
            this.toolStripButtonPS.Image = global::klsync.Properties.Resources.marker201;
            this.toolStripButtonPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPS.Name = "toolStripButtonPS";
            this.toolStripButtonPS.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.toolStripButtonPS.Size = new System.Drawing.Size(68, 51);
            this.toolStripButtonPS.Text = "Act. PS";
            this.toolStripButtonPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonPS.ToolTipText = "Resumen Prestashop";
            this.toolStripButtonPS.Click += new System.EventHandler(this.toolStripButtonPS_Click);
            // 
            // frSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 564);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStripResumen);
            this.Name = "frSummary";
            this.Text = "Resumen";
            this.toolStripResumen.ResumeLayout(false);
            this.toolStripResumen.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripResumen;
        private System.Windows.Forms.ToolStripButton toolStripButtonReload;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonA3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonPS;
        private System.Windows.Forms.TreeView treeViewSummaryA3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeViewSummaryPS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}