﻿namespace klsync
{
    partial class frSeur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolstripSeur = new System.Windows.Forms.ToolStrip();
            this.dgvSeur = new System.Windows.Forms.DataGridView();
            this.statusStripSeur = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.numFilas = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeur)).BeginInit();
            this.statusStripSeur.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolstripSeur
            // 
            this.toolstripSeur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolstripSeur.Location = new System.Drawing.Point(0, 0);
            this.toolstripSeur.Name = "toolstripSeur";
            this.toolstripSeur.Padding = new System.Windows.Forms.Padding(0, 5, 1, 0);
            this.toolstripSeur.Size = new System.Drawing.Size(1014, 25);
            this.toolstripSeur.TabIndex = 0;
            this.toolstripSeur.Text = "toolStrip1";
            // 
            // dgvSeur
            // 
            this.dgvSeur.AllowUserToAddRows = false;
            this.dgvSeur.AllowUserToDeleteRows = false;
            this.dgvSeur.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSeur.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSeur.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSeur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSeur.Location = new System.Drawing.Point(12, 74);
            this.dgvSeur.Name = "dgvSeur";
            this.dgvSeur.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvSeur.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSeur.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSeur.Size = new System.Drawing.Size(990, 591);
            this.dgvSeur.TabIndex = 1;
            this.dgvSeur.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSeur_ColumnHeaderMouseClick);
            this.dgvSeur.SelectionChanged += new System.EventHandler(this.dgvSeur_SelectionChanged);
            // 
            // statusStripSeur
            // 
            this.statusStripSeur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.statusStripSeur.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.numFilas});
            this.statusStripSeur.Location = new System.Drawing.Point(0, 678);
            this.statusStripSeur.Name = "statusStripSeur";
            this.statusStripSeur.Size = new System.Drawing.Size(1014, 26);
            this.statusStripSeur.TabIndex = 3;
            this.statusStripSeur.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(948, 21);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // numFilas
            // 
            this.numFilas.Name = "numFilas";
            this.numFilas.Size = new System.Drawing.Size(51, 21);
            this.numFilas.Text = "0 filas";
            // 
            // frSeur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 704);
            this.Controls.Add(this.statusStripSeur);
            this.Controls.Add(this.dgvSeur);
            this.Controls.Add(this.toolstripSeur);
            this.KeyPreview = true;
            this.Name = "frSeur";
            this.Text = "Seur";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSeur)).EndInit();
            this.statusStripSeur.ResumeLayout(false);
            this.statusStripSeur.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolstripSeur;
        private System.Windows.Forms.DataGridView dgvSeur;
        private System.Windows.Forms.StatusStrip statusStripSeur;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel numFilas;
    }
}