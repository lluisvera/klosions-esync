﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frStock : Form
    {

        private static frStock m_FormDefInstance;
        public static frStock DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frStock();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frStock()
        {
            InitializeComponent();
        }




        private void consultaStockShop()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            conectarDB("SELECT " +
                    " ps_product.id_product,reference ,name,ps_stock_available.quantity,price " +
                    " FROM  " +
                    csGlobal.databasePS + ".ps_product," + csGlobal.databasePS + ".ps_stock_available," + csGlobal.databasePS + ".ps_product_lang " +
                    " WHERE " +
                    " ps_product.id_product=ps_stock_available.id_product and " +
                    " ps_product.id_product=ps_product_lang.id_product and  " +
                    " ps_product_lang.id_lang=" + mysql.defaultLangPS(), false, dgvStock);

        }

        private void conectarDB(string sqlScript, bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();
                // - DEBUG 
                // //MessageBox.Show("Connection successful!"); 
                MySqlDataAdapter MyDA = new MySqlDataAdapter();
                //MyDA.SelectCommand = new MySqlCommand("SELECT reference,payment,module,total_paid FROM `ps_orders`", conn);
                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);
                //MyDA.SelectCommand = new MySqlCommand("SELECT * FROM `ps_category_product`", conn);
                //MyDA.SelectCommand = new MySqlCommand("Show tables;", conn);
                //MyDA.SelectCommand = new MySqlCommand("SHOW COLUMNS FROM ps_orders;", conn);
                DataSet ds = new DataSet();

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {
                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }
                    }
                    numeroDocumentos = table.Rows.Count - contador;
                    ////MessageBox.Show("hay " + table.Rows.Count.ToString() + " filas en el grid");
                    ////MessageBox.Show("Hay " + numeroDocumentos);
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            consultaStockShop();
        }

        //Actualizo el stock de Prestashop
        private void grabaStockA3enPS()
        {
            bool check = true;
            string reference = "";
            string cantidad = "";
            string id_product = "";
            string id_store = "";
            DateTime fecha = DateTime.Now;
            string script = "";

            csStocks stock = new csStocks();
            DataTable tablaStock = new DataTable();
            tablaStock = stock.consultaStockA3();

            foreach (DataRow fila in tablaStock.Rows)
            {


                reference = fila.ItemArray[0].ToString();
                cantidad = fila.ItemArray[3].ToString();
                if (cantidad == "")
                {
                    cantidad = "0";
                }

                id_product = fila.ItemArray[4].ToString();
                id_store = fila.ItemArray[2].ToString();
                if (id_store == "")
                {
                    id_store = "1";
                }
                if (cantidad == "")
                {
                    cantidad = "0";
                }
                if (id_store == "")
                {
                    id_store = "0";
                }

                if (check == true)
                {
                    script = "insert into kls_stock_erp (reference,unidades,id) " +
                   " values ('" + reference + "'," + cantidad + "," + id_product + ")";

                    check = false;
                }
                else
                {
                    script += ",('" + reference + "'," + cantidad + "," + id_product + ")";
                }
            }

            csMySqlConnect mySql = new csMySqlConnect();
            mySql.actualizaStock(script);
        }


        //private void consultaStockA3()
        //{ 
        //    csSqlConnects sqlConnect = new csSqlConnects();
        //    SqlConnection dataConnection = new SqlConnection();
        //    dataConnection.ConnectionString = sqlConnect.CadenaConexion();
        //    dataConnection.Open();
        //    SqlDataAdapter a = new SqlDataAdapter("SELECT ltrim(dbo.STOCKALM.CODART) as codart, dbo.ARTICULO.DESCART, dbo.STOCKALM.CODALM, dbo.STOCKALM.UNIDADES " +
        //    " FROM dbo.ARTICULO INNER JOIN dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART where dbo.ARTICULO.CAR4=1", dataConnection);
        //    DataTable t = new DataTable();
        //    a.Fill(t);
        //    dgvStock.DataSource = t;
        //}

        private void compruebaStock()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            conectarDB("SELECT s.reference as CodigoA3,p.id_product as CodigoPS,l.name as Nombre, s.unidades as StockA3,sa.quantity as StockPS " +
            " FROM " + csGlobal.databasePS + ".ps_product as p left join " +
            csGlobal.databasePS + ".kls_stock_erp as s on (s.reference=p.reference) inner join " +
            csGlobal.databasePS + ".ps_product_lang l on (p.id_product=l.id_product) inner join " +
            csGlobal.databasePS + ".ps_stock_available as sa on (sa.id_product=p.id_product) " +
            " where id_lang=" + mysql.defaultLangPS(), false, dgvStock);

        }


        private void actualizaStockPS()
        {
            int id_product;
            int cantidad;
            csStocks stock = new csStocks();
            DataTable tablaStock = new DataTable();
            tablaStock = stock.consultaStockA3();
            //no tengo el id producto
            foreach (DataRow fila in tablaStock.Rows)
            {
                id_product = Convert.ToInt32(fila.ItemArray[4].ToString());
                if (fila.ItemArray[3] is DBNull)
                {
                    cantidad = 0;
                }
                else
                {
                    cantidad = Convert.ToInt32(fila.ItemArray[3].ToString());
                }
                string script = "update ps_stock_available set quantity=" + cantidad + " where id_product=" + id_product;
                csMySqlConnect mySql = new csMySqlConnect();
                mySql.actualizaStock(script);
            }
            ////MessageBox.Show("Stock Actualizado");
            compruebaStock();
        }

        private void borrarStockTemp()
        {
            csMySqlConnect conector = new csMySqlConnect();
            MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
            conn.Open();
            string sqlScript = "delete from " + csGlobal.databasePS + ".kls_stock_erp where unidades<9999";
            MySqlCommand cmd = new MySqlCommand(sqlScript, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }







   

      

        private void exportarExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csOfficeIntegration officeInt = new csOfficeIntegration();
            officeInt.ExportToExcel(dgvStock);
        }

        private void dgvStock_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int cantidadPS = 0;
            int cantidadA3 = 0;

            if (this.dgvStock.Columns[e.ColumnIndex].Name == "StockA3")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            if (this.dgvStock.Columns[e.ColumnIndex].Name == "StockPS")
            {
                try
                {
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    cantidadPS = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockPS"].Value);
                    cantidadA3 = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockA3"].Value);
                    if (cantidadA3 != cantidadPS)
                    {

                        e.CellStyle.ForeColor = Color.Red;
                    }

                }
                catch (Exception ex)
                { }
            }


        }


        private void btLoadIncidenciaStock_Click(object sender, EventArgs e)
        {
            cargarIncidenciasStock();
        }

        private void cargarIncidenciasStock()
        {
            string stockTotal = "";
            string stockTotalAttributes = "";
            
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable dtStockTotales = new DataTable();
            DataTable dtStockAttributes = new DataTable();
            DataTable dtIncidenciasStock = new DataTable();
            dtIncidenciasStock.Columns.Add("id_product");
            dtIncidenciasStock.Columns.Add("stock");
            dtIncidenciasStock.Columns.Add("stock_Att");

            //Cargo en 2 datatables, primero el total de cada artículo, y en el otro sumo las combinaciones
            //el objetivo es comparar y ver que diferencias de artículos hay
            //dtStockTotales = mysql.cargarTabla("select concat(id_product,'-',quantity) as stock from ps_stock_available where id_product_attribute=0 order by id_product");
            //dtStockAttributes = mysql.cargarTabla("select convert(concat(id_product,'-',sum(quantity)), char(50)) as stock from ps_stock_available where id_product_attribute<>0 group by id_product order by id_product");

            dtStockTotales = mysql.cargarTabla("select id_product,quantity as stock from ps_stock_available where id_product_attribute=0 order by id_product");
            dtStockAttributes = mysql.cargarTabla("select id_product,sum(quantity) as stock from ps_stock_available where id_product_attribute<>0 group by id_product order by id_product");

            //Busco si hay Familias de Atributos nuevos creados en A3ERP, si encuentra los guarda en el datatable
            //dtIncidenciasStock = csUtilidades.getDiffDataTables(dtStockTotales, dtStockAttributes);
            //

            foreach (DataRow fila in dtStockAttributes.Rows)
            {
                stockTotalAttributes = fila["stock"].ToString();

                foreach (DataRow filaStockTotal in dtStockTotales.Rows)
                { 
                    stockTotal = filaStockTotal["stock"].ToString();
                    if (fila["id_product"].ToString() == filaStockTotal["id_product"].ToString())
                    {
                        if (stockTotalAttributes!=stockTotal)
                        {
                            DataRow filaIncidencia=dtIncidenciasStock.NewRow();

                            filaIncidencia["id_product"] = filaStockTotal["id_product"];
                            filaIncidencia["stock"] = stockTotal;
                            filaIncidencia["stock_Att"] = stockTotalAttributes;
                            dtIncidenciasStock.Rows.Add(filaIncidencia);
                        }
                    }
                }

            }
            dgvStock.DataSource = dtIncidenciasStock;
            lblNumeroFilas.Text = dgvStock.Rows.Count.ToString();
        }









    }
}
