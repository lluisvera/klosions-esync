﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Windows.Forms;

namespace klsync.SQL
{
    public class ConexionSingleton
    {
        private static ConexionSingleton instancia;
        private static readonly object bloqueo = new object();
        private OdbcConnection odbcConnection;
        private MySqlConnection mysqlConnection;
        private SqlConnection sqlServerConnection;

        //// Constructor privado para evitar que se creen instancias desde fuera
        private ConexionSingleton()
        {
            if (csGlobal.isODBCConnection)
            {
                odbcConnection = new OdbcConnection("DSN=ps_shop");
            }
            else if (csGlobal.isMySQLConnection)
            {
                string cadenaConexionMysql = conexionDestino();
                mysqlConnection = new MySqlConnection(cadenaConexionMysql);
            }
            else if (csGlobal.isSqlServerConnection)  // Comprobamos si es SQL Server
            {
                sqlServerConnection = new SqlConnection(csGlobal.cadenaConexion);  // Usamos la cadena de conexión de SQL Server
            }
        }

        // Obtener la instancia Singleton
        public static ConexionSingleton ObtenerInstancia()
        {
            lock (bloqueo)
            {
                if (instancia == null)
                {
                    instancia = new ConexionSingleton();
                }
                return instancia;
            }
        }

        // Métodos públicos para acceder a las conexiones de forma controlada
        public OdbcConnection GetOdbcConnection() => odbcConnection;
        public MySqlConnection GetMySqlConnection() => mysqlConnection;
        public SqlConnection GetSqlServerConnection() => sqlServerConnection;


        // Abrir conexión
        public void AbrirConexion()
        {
            lock (bloqueo) // Bloqueamos el acceso para evitar condiciones de carrera
            {
                if (csGlobal.isODBCConnection)
                {
                    if (odbcConnection.State == System.Data.ConnectionState.Closed)
                    {
                        odbcConnection.Open();
                    }
                }
                else if (csGlobal.isMySQLConnection)  // MySQL
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Closed)
                    {
                        mysqlConnection.Open();
                    }
                }
                else if (csGlobal.isSqlServerConnection)  // SQL Server
                {
                    if (sqlServerConnection.State == System.Data.ConnectionState.Closed)
                    {
                        sqlServerConnection.Open();
                    }
                }
            }
        }

        // Cerrar conexión
        public void CerrarConexion()
        {
            lock (bloqueo)
            {
                if (csGlobal.isODBCConnection)
                {
                    if (odbcConnection.State == System.Data.ConnectionState.Open)
                    {
                        odbcConnection.Close();
                    }
                }
                else if (csGlobal.isMySQLConnection)  // MySQL
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                    {
                        mysqlConnection.Close();
                    }
                }
                else if (csGlobal.isSqlServerConnection)  // SQL Server
                {
                    if (sqlServerConnection.State == System.Data.ConnectionState.Open)
                    {
                        sqlServerConnection.Close();
                    }
                }
            }
        }


        // Crear la cadena de conexión
        private string conexionDestino()
        {
            string puertoMySQL = "PORT=" + csGlobal.PortPS + ";";

            if (csGlobal.PortPS == "")
            {
                puertoMySQL = "PORT=3306;";
            }

            string connectionString = "SERVER=" + csGlobal.ServerPS + ";" + puertoMySQL + "DATABASE=" + csGlobal.databasePS + ";" + "UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";
            return connectionString;
        }

        // Función que replica la de csUtilidades (llama a csMySqlConnect o csSqlConnects)
        public void ejecutarConsulta(string consulta, bool MySQL = true, bool gestionarConexiones = true, object sqlConnection = null)
        {
            try
            {
                // Obtener la instancia única del Singleton
                var conexion = ConexionSingleton.ObtenerInstancia();

                if (MySQL)
                {
                    // Pasar la conexión MySQL del Singleton a csMySqlConnect
                    var mysql = new csMySqlConnect(conexion.mysqlConnection);
                    //mysql.actualizaStock(consulta, gestionarConexiones);
                    mysql.actualizaStock(consulta);
                    mysql.CloseConnection();
                }
                else
                {
                    // Pasar la conexión ODBC del Singleton a csSqlConnects
                    var sql = new csSqlConnects(conexion.odbcConnection);
                    sql.actualizarCampo(consulta, gestionarConexiones);
                    sql.cerrarConexion();
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    // Muestra el mensaje de error o lo loggea
                    //MessageBox.Show(ex.Message);
                }
            }
        }
    }
}


