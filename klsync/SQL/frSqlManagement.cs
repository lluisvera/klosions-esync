﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;

namespace klsync.SQL
{
    public partial class frSqlManagement : Form
    {
        public frSqlManagement()
        {
            InitializeComponent();
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            string database = "DEMOREPASAT";
            string extensionFile = ".bak";
            string filename = DateTime.Now.ToString("yyyyMMdd_HHmm") + " Copia" ;
            string pathfile = "D://sqlbackup//";
            SqlConnection con = new SqlConnection();

            //SqlConnection dataConnection = new SqlConnection();
            con.ConnectionString = csGlobal.cadenaConexion;
            try
            {
                string cmd = "BACKUP DATABASE [" + database + "] TO DISK= '" + pathfile + filename + extensionFile + "'";
                using (SqlCommand command = new SqlCommand(cmd, con))
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    command.ExecuteNonQuery();
                    con.Close();

                    using (ZipArchive zipfile = ZipFile.Open("D://sqlbackup//" + filename + ".zip", ZipArchiveMode.Create))
                    {
                        zipfile.CreateEntryFromFile(pathfile + filename + extensionFile, filename);
                    }

                    MessageBox.Show("database backup done succesfully");

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
    }
}
