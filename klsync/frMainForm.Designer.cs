﻿namespace klsync
{
    partial class frMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frMainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mantenimientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ficherosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preciosFamClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.tablasAuxiliaresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilidadesPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artículosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.imágenesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informePedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuPresupuestos = new System.Windows.Forms.ToolStripMenuItem();
            this.alexPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.códigosDeBarrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eurolineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incidenciasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artículosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recoveryAttributesDismayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignaciónDeCategoríasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Caracteristicas = new System.Windows.Forms.ToolStripMenuItem();
            this.mimasaIfigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recordatoriosPagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amigosFundacióMiróToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarPedidosVentaTRSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarDatosIntrastatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarDocsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarDoc2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listadoDeDocumentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.envíoDeDocumentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónBDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datosDeParametrizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programadorDeTareasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.resumenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generadorDocumentosA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graellaDeTallasYColoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paginationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accesosDirectosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backOfficeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.paypalAdmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.captioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recodificaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presupuestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importaciónFacturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accederToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportarUnProblemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.webservicePSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arreglarTriggersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alphaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosFPCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.puntesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.presupuestosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarDatosPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mantenimientosToolStripMenuItem,
            this.utilidadesPSToolStripMenuItem,
            this.gestiónToolStripMenuItem,
            this.configurarToolStripMenuItem,
            this.repasatToolStripMenuItem,
            this.accesosDirectosToolStripMenuItem,
            this.admanToolStripMenuItem,
            this.administraciónToolStripMenuItem,
            this.presupuestosToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1499, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mantenimientosToolStripMenuItem
            // 
            this.mantenimientosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficherosToolStripMenuItem,
            this.clientesToolStripMenuItem1,
            this.preciosToolStripMenuItem,
            this.preciosFamClientesToolStripMenuItem,
            this.toolStripMenuItem4,
            this.tablasAuxiliaresToolStripMenuItem});
            this.mantenimientosToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mantenimientosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mantenimientosToolStripMenuItem.Image")));
            this.mantenimientosToolStripMenuItem.Name = "mantenimientosToolStripMenuItem";
            this.mantenimientosToolStripMenuItem.Size = new System.Drawing.Size(96, 36);
            this.mantenimientosToolStripMenuItem.Text = "&Ficheros";
            this.mantenimientosToolStripMenuItem.Click += new System.EventHandler(this.mantenimientosToolStripMenuItem_Click);
            // 
            // ficherosToolStripMenuItem
            // 
            this.ficherosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ficherosToolStripMenuItem.Image")));
            this.ficherosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ficherosToolStripMenuItem.Name = "ficherosToolStripMenuItem";
            this.ficherosToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.ficherosToolStripMenuItem.Text = "&Artículos";
            this.ficherosToolStripMenuItem.Click += new System.EventHandler(this.ficherosToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem1
            // 
            this.clientesToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("clientesToolStripMenuItem1.Image")));
            this.clientesToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.clientesToolStripMenuItem1.Name = "clientesToolStripMenuItem1";
            this.clientesToolStripMenuItem1.Size = new System.Drawing.Size(241, 38);
            this.clientesToolStripMenuItem1.Text = "C&lientes";
            this.clientesToolStripMenuItem1.Click += new System.EventHandler(this.clientesToolStripMenuItem1_Click);
            // 
            // preciosToolStripMenuItem
            // 
            this.preciosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("preciosToolStripMenuItem.Image")));
            this.preciosToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.preciosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.preciosToolStripMenuItem.Name = "preciosToolStripMenuItem";
            this.preciosToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.preciosToolStripMenuItem.Text = "&Precios";
            this.preciosToolStripMenuItem.Click += new System.EventHandler(this.preciosToolStripMenuItem_Click);
            // 
            // preciosFamClientesToolStripMenuItem
            // 
            this.preciosFamClientesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("preciosFamClientesToolStripMenuItem.Image")));
            this.preciosFamClientesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.preciosFamClientesToolStripMenuItem.Name = "preciosFamClientesToolStripMenuItem";
            this.preciosFamClientesToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.preciosFamClientesToolStripMenuItem.Text = "Precios Fam. Clientes";
            this.preciosFamClientesToolStripMenuItem.Click += new System.EventHandler(this.preciosFamClientesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(238, 6);
            // 
            // tablasAuxiliaresToolStripMenuItem
            // 
            this.tablasAuxiliaresToolStripMenuItem.DoubleClickEnabled = true;
            this.tablasAuxiliaresToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tablasAuxiliaresToolStripMenuItem.Image")));
            this.tablasAuxiliaresToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tablasAuxiliaresToolStripMenuItem.Name = "tablasAuxiliaresToolStripMenuItem";
            this.tablasAuxiliaresToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.tablasAuxiliaresToolStripMenuItem.Text = "Tablas Auxiliares";
            this.tablasAuxiliaresToolStripMenuItem.Click += new System.EventHandler(this.tablasAuxiliaresToolStripMenuItem_Click);
            // 
            // utilidadesPSToolStripMenuItem
            // 
            this.utilidadesPSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artículosToolStripMenuItem1,
            this.imágenesToolStripMenuItem,
            this.informePedidosToolStripMenuItem,
            this.tsMenuPresupuestos,
            this.alexPedidosToolStripMenuItem,
            this.códigosDeBarrasToolStripMenuItem,
            this.eurolineToolStripMenuItem,
            this.incidenciasToolStripMenuItem,
            this.asignaciónDeCategoríasToolStripMenuItem,
            this.Caracteristicas,
            this.mimasaIfigenToolStripMenuItem,
            this.recordatoriosPagoToolStripMenuItem,
            this.amigosFundacióMiróToolStripMenuItem,
            this.generarPedidosVentaTRSToolStripMenuItem,
            this.actualizarDatosIntrastatToolStripMenuItem,
            this.exportarDatosPSToolStripMenuItem});
            this.utilidadesPSToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.utilidadesPSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("utilidadesPSToolStripMenuItem.Image")));
            this.utilidadesPSToolStripMenuItem.Name = "utilidadesPSToolStripMenuItem";
            this.utilidadesPSToolStripMenuItem.Size = new System.Drawing.Size(129, 36);
            this.utilidadesPSToolStripMenuItem.Text = "&Utilidades PS";
            // 
            // artículosToolStripMenuItem1
            // 
            this.artículosToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("artículosToolStripMenuItem1.Image")));
            this.artículosToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.artículosToolStripMenuItem1.Name = "artículosToolStripMenuItem1";
            this.artículosToolStripMenuItem1.Size = new System.Drawing.Size(290, 38);
            this.artículosToolStripMenuItem1.Text = "&Artículos";
            this.artículosToolStripMenuItem1.Click += new System.EventHandler(this.artículosToolStripMenuItem1_Click);
            // 
            // imágenesToolStripMenuItem
            // 
            this.imágenesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("imágenesToolStripMenuItem.Image")));
            this.imágenesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.imágenesToolStripMenuItem.Name = "imágenesToolStripMenuItem";
            this.imágenesToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.imágenesToolStripMenuItem.Text = "&Imágenes";
            this.imágenesToolStripMenuItem.Click += new System.EventHandler(this.imágenesToolStripMenuItem_Click);
            // 
            // informePedidosToolStripMenuItem
            // 
            this.informePedidosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("informePedidosToolStripMenuItem.Image")));
            this.informePedidosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.informePedidosToolStripMenuItem.Name = "informePedidosToolStripMenuItem";
            this.informePedidosToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.informePedidosToolStripMenuItem.Text = "Informe Pedidos";
            this.informePedidosToolStripMenuItem.Click += new System.EventHandler(this.informePedidosToolStripMenuItem_Click);
            // 
            // tsMenuPresupuestos
            // 
            this.tsMenuPresupuestos.Image = ((System.Drawing.Image)(resources.GetObject("tsMenuPresupuestos.Image")));
            this.tsMenuPresupuestos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsMenuPresupuestos.Name = "tsMenuPresupuestos";
            this.tsMenuPresupuestos.Size = new System.Drawing.Size(290, 38);
            this.tsMenuPresupuestos.Text = "&Presupuestos";
            this.tsMenuPresupuestos.Click += new System.EventHandler(this.tsMenuPresupuestos_Click);
            // 
            // alexPedidosToolStripMenuItem
            // 
            this.alexPedidosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("alexPedidosToolStripMenuItem.Image")));
            this.alexPedidosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.alexPedidosToolStripMenuItem.Name = "alexPedidosToolStripMenuItem";
            this.alexPedidosToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.alexPedidosToolStripMenuItem.Text = "Pedidos / Albaranes";
            this.alexPedidosToolStripMenuItem.Click += new System.EventHandler(this.alexPedidosToolStripMenuItem_Click_1);
            // 
            // códigosDeBarrasToolStripMenuItem
            // 
            this.códigosDeBarrasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("códigosDeBarrasToolStripMenuItem.Image")));
            this.códigosDeBarrasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.códigosDeBarrasToolStripMenuItem.Name = "códigosDeBarrasToolStripMenuItem";
            this.códigosDeBarrasToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.códigosDeBarrasToolStripMenuItem.Text = "&Códigos de Barras";
            this.códigosDeBarrasToolStripMenuItem.Click += new System.EventHandler(this.códigosDeBarrasToolStripMenuItem_Click);
            // 
            // eurolineToolStripMenuItem
            // 
            this.eurolineToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("eurolineToolStripMenuItem.Image")));
            this.eurolineToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.eurolineToolStripMenuItem.Name = "eurolineToolStripMenuItem";
            this.eurolineToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.eurolineToolStripMenuItem.Text = "Euroline";
            this.eurolineToolStripMenuItem.Click += new System.EventHandler(this.eurolineToolStripMenuItem_Click);
            // 
            // incidenciasToolStripMenuItem
            // 
            this.incidenciasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artículosToolStripMenuItem,
            this.stocksToolStripMenuItem,
            this.recoveryAttributesDismayToolStripMenuItem});
            this.incidenciasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("incidenciasToolStripMenuItem.Image")));
            this.incidenciasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.incidenciasToolStripMenuItem.Name = "incidenciasToolStripMenuItem";
            this.incidenciasToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.incidenciasToolStripMenuItem.Text = "Incidencias";
            // 
            // artículosToolStripMenuItem
            // 
            this.artículosToolStripMenuItem.Name = "artículosToolStripMenuItem";
            this.artículosToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.artículosToolStripMenuItem.Text = "Artículos";
            this.artículosToolStripMenuItem.Click += new System.EventHandler(this.artículosToolStripMenuItem_Click_1);
            // 
            // stocksToolStripMenuItem
            // 
            this.stocksToolStripMenuItem.Name = "stocksToolStripMenuItem";
            this.stocksToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.stocksToolStripMenuItem.Text = "Stocks";
            this.stocksToolStripMenuItem.Click += new System.EventHandler(this.stocksToolStripMenuItem_Click);
            // 
            // recoveryAttributesDismayToolStripMenuItem
            // 
            this.recoveryAttributesDismayToolStripMenuItem.Name = "recoveryAttributesDismayToolStripMenuItem";
            this.recoveryAttributesDismayToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.recoveryAttributesDismayToolStripMenuItem.Text = "Recovery Attributes Dismay";
            this.recoveryAttributesDismayToolStripMenuItem.Click += new System.EventHandler(this.recoveryAttributesDismayToolStripMenuItem_Click);
            // 
            // asignaciónDeCategoríasToolStripMenuItem
            // 
            this.asignaciónDeCategoríasToolStripMenuItem.Image = global::klsync.adMan.Resources.change;
            this.asignaciónDeCategoríasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.asignaciónDeCategoríasToolStripMenuItem.Name = "asignaciónDeCategoríasToolStripMenuItem";
            this.asignaciónDeCategoríasToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.asignaciónDeCategoríasToolStripMenuItem.Text = "Visualización Características";
            this.asignaciónDeCategoríasToolStripMenuItem.Click += new System.EventHandler(this.asignaciónDeCategoríasToolStripMenuItem_Click);
            // 
            // Caracteristicas
            // 
            this.Caracteristicas.Image = ((System.Drawing.Image)(resources.GetObject("Caracteristicas.Image")));
            this.Caracteristicas.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Caracteristicas.Name = "Caracteristicas";
            this.Caracteristicas.Size = new System.Drawing.Size(290, 38);
            this.Caracteristicas.Text = "Características";
            this.Caracteristicas.Click += new System.EventHandler(this.Caracteristicas_Click);
            // 
            // mimasaIfigenToolStripMenuItem
            // 
            this.mimasaIfigenToolStripMenuItem.Name = "mimasaIfigenToolStripMenuItem";
            this.mimasaIfigenToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.mimasaIfigenToolStripMenuItem.Text = "Mimasa Ifigen";
            this.mimasaIfigenToolStripMenuItem.Click += new System.EventHandler(this.mimasaIfigenToolStripMenuItem_Click);
            // 
            // recordatoriosPagoToolStripMenuItem
            // 
            this.recordatoriosPagoToolStripMenuItem.Name = "recordatoriosPagoToolStripMenuItem";
            this.recordatoriosPagoToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.recordatoriosPagoToolStripMenuItem.Text = "Recordatorios Pago";
            this.recordatoriosPagoToolStripMenuItem.Click += new System.EventHandler(this.recordatoriosPagoToolStripMenuItem_Click);
            // 
            // amigosFundacióMiróToolStripMenuItem
            // 
            this.amigosFundacióMiróToolStripMenuItem.Name = "amigosFundacióMiróToolStripMenuItem";
            this.amigosFundacióMiróToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.amigosFundacióMiróToolStripMenuItem.Text = "Amigos Fundació Miró";
            this.amigosFundacióMiróToolStripMenuItem.Click += new System.EventHandler(this.amigosFundacióMiróToolStripMenuItem_Click);
            // 
            // generarPedidosVentaTRSToolStripMenuItem
            // 
            this.generarPedidosVentaTRSToolStripMenuItem.Name = "generarPedidosVentaTRSToolStripMenuItem";
            this.generarPedidosVentaTRSToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.generarPedidosVentaTRSToolStripMenuItem.Text = "Generar Pedidos Venta TRS";
            this.generarPedidosVentaTRSToolStripMenuItem.Click += new System.EventHandler(this.generarPedidosVentaTRSToolStripMenuItem_Click);
            // 
            // actualizarDatosIntrastatToolStripMenuItem
            // 
            this.actualizarDatosIntrastatToolStripMenuItem.Name = "actualizarDatosIntrastatToolStripMenuItem";
            this.actualizarDatosIntrastatToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.actualizarDatosIntrastatToolStripMenuItem.Text = "Actualizar Datos Intrastat";
            this.actualizarDatosIntrastatToolStripMenuItem.Click += new System.EventHandler(this.actualizarDatosIntrastatToolStripMenuItem_Click);
            // 
            // gestiónToolStripMenuItem
            // 
            this.gestiónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarDocsToolStripMenuItem,
            this.sincronizarDoc2ToolStripMenuItem,
            this.listadoDeDocumentosToolStripMenuItem,
            this.envíoDeDocumentosToolStripMenuItem,
            this.seurToolStripMenuItem});
            this.gestiónToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestiónToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("gestiónToolStripMenuItem.Image")));
            this.gestiónToolStripMenuItem.Name = "gestiónToolStripMenuItem";
            this.gestiónToolStripMenuItem.Size = new System.Drawing.Size(116, 36);
            this.gestiónToolStripMenuItem.Text = "&Sincronizar";
            // 
            // sincronizarDocsToolStripMenuItem
            // 
            this.sincronizarDocsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sincronizarDocsToolStripMenuItem.Image")));
            this.sincronizarDocsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.sincronizarDocsToolStripMenuItem.Name = "sincronizarDocsToolStripMenuItem";
            this.sincronizarDocsToolStripMenuItem.Size = new System.Drawing.Size(267, 38);
            this.sincronizarDocsToolStripMenuItem.Text = "Sincronizar Docs Invenio";
            this.sincronizarDocsToolStripMenuItem.Visible = false;
            this.sincronizarDocsToolStripMenuItem.Click += new System.EventHandler(this.sincronizarDocsToolStripMenuItem_Click);
            // 
            // sincronizarDoc2ToolStripMenuItem
            // 
            this.sincronizarDoc2ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sincronizarDoc2ToolStripMenuItem.Image")));
            this.sincronizarDoc2ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.sincronizarDoc2ToolStripMenuItem.Name = "sincronizarDoc2ToolStripMenuItem";
            this.sincronizarDoc2ToolStripMenuItem.Size = new System.Drawing.Size(267, 38);
            this.sincronizarDoc2ToolStripMenuItem.Text = "Sincronizar Docs";
            this.sincronizarDoc2ToolStripMenuItem.Click += new System.EventHandler(this.sincronizarDoc2ToolStripMenuItem_Click);
            // 
            // listadoDeDocumentosToolStripMenuItem
            // 
            this.listadoDeDocumentosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("listadoDeDocumentosToolStripMenuItem.Image")));
            this.listadoDeDocumentosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.listadoDeDocumentosToolStripMenuItem.Name = "listadoDeDocumentosToolStripMenuItem";
            this.listadoDeDocumentosToolStripMenuItem.Size = new System.Drawing.Size(267, 38);
            this.listadoDeDocumentosToolStripMenuItem.Text = "&Listado de Documentos";
            this.listadoDeDocumentosToolStripMenuItem.Click += new System.EventHandler(this.listadoDeDocumentosToolStripMenuItem_Click);
            // 
            // envíoDeDocumentosToolStripMenuItem
            // 
            this.envíoDeDocumentosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("envíoDeDocumentosToolStripMenuItem.Image")));
            this.envíoDeDocumentosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.envíoDeDocumentosToolStripMenuItem.Name = "envíoDeDocumentosToolStripMenuItem";
            this.envíoDeDocumentosToolStripMenuItem.Size = new System.Drawing.Size(267, 38);
            this.envíoDeDocumentosToolStripMenuItem.Text = "&Envío de Documentos";
            this.envíoDeDocumentosToolStripMenuItem.Click += new System.EventHandler(this.envíoDeDocumentosToolStripMenuItem_Click);
            // 
            // seurToolStripMenuItem
            // 
            this.seurToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("seurToolStripMenuItem.Image")));
            this.seurToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.seurToolStripMenuItem.Name = "seurToolStripMenuItem";
            this.seurToolStripMenuItem.Size = new System.Drawing.Size(267, 38);
            this.seurToolStripMenuItem.Text = "&Seur";
            this.seurToolStripMenuItem.Click += new System.EventHandler(this.seurToolStripMenuItem_Click);
            // 
            // configurarToolStripMenuItem
            // 
            this.configurarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraciónBDToolStripMenuItem,
            this.datosDeParametrizaciónToolStripMenuItem,
            this.programadorDeTareasToolStripMenuItem,
            this.toolStripMenuItem2,
            this.resumenToolStripMenuItem,
            this.generadorDocumentosA3ERPToolStripMenuItem,
            this.graellaDeTallasYColoresToolStripMenuItem,
            this.paginationToolStripMenuItem,
            this.tESTToolStripMenuItem});
            this.configurarToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("configurarToolStripMenuItem.Image")));
            this.configurarToolStripMenuItem.Name = "configurarToolStripMenuItem";
            this.configurarToolStripMenuItem.Size = new System.Drawing.Size(113, 36);
            this.configurarToolStripMenuItem.Text = "&Configurar";
            this.configurarToolStripMenuItem.Click += new System.EventHandler(this.configurarToolStripMenuItem_Click);
            // 
            // configuraciónBDToolStripMenuItem
            // 
            this.configuraciónBDToolStripMenuItem.Name = "configuraciónBDToolStripMenuItem";
            this.configuraciónBDToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.configuraciónBDToolStripMenuItem.Text = "Configuración BD";
            this.configuraciónBDToolStripMenuItem.Visible = false;
            // 
            // datosDeParametrizaciónToolStripMenuItem
            // 
            this.datosDeParametrizaciónToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("datosDeParametrizaciónToolStripMenuItem.Image")));
            this.datosDeParametrizaciónToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.datosDeParametrizaciónToolStripMenuItem.Name = "datosDeParametrizaciónToolStripMenuItem";
            this.datosDeParametrizaciónToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.datosDeParametrizaciónToolStripMenuItem.Text = "Datos de &Parametrización";
            this.datosDeParametrizaciónToolStripMenuItem.Click += new System.EventHandler(this.datosDeParametrizaciónToolStripMenuItem_Click);
            // 
            // programadorDeTareasToolStripMenuItem
            // 
            this.programadorDeTareasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("programadorDeTareasToolStripMenuItem.Image")));
            this.programadorDeTareasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.programadorDeTareasToolStripMenuItem.Name = "programadorDeTareasToolStripMenuItem";
            this.programadorDeTareasToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.programadorDeTareasToolStripMenuItem.Text = "Programador de Tareas";
            this.programadorDeTareasToolStripMenuItem.Click += new System.EventHandler(this.programadorDeTareasToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(282, 6);
            // 
            // resumenToolStripMenuItem
            // 
            this.resumenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("resumenToolStripMenuItem.Image")));
            this.resumenToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.resumenToolStripMenuItem.Name = "resumenToolStripMenuItem";
            this.resumenToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.resumenToolStripMenuItem.Text = "Resumen";
            this.resumenToolStripMenuItem.Click += new System.EventHandler(this.resumenToolStripMenuItem_Click);
            // 
            // generadorDocumentosA3ERPToolStripMenuItem
            // 
            this.generadorDocumentosA3ERPToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("generadorDocumentosA3ERPToolStripMenuItem.Image")));
            this.generadorDocumentosA3ERPToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.generadorDocumentosA3ERPToolStripMenuItem.Name = "generadorDocumentosA3ERPToolStripMenuItem";
            this.generadorDocumentosA3ERPToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.generadorDocumentosA3ERPToolStripMenuItem.Text = "Generador Documentos A3";
            this.generadorDocumentosA3ERPToolStripMenuItem.Click += new System.EventHandler(this.generadorDocumentosA3ERPToolStripMenuItem_Click);
            // 
            // graellaDeTallasYColoresToolStripMenuItem
            // 
            this.graellaDeTallasYColoresToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("graellaDeTallasYColoresToolStripMenuItem.Image")));
            this.graellaDeTallasYColoresToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.graellaDeTallasYColoresToolStripMenuItem.Name = "graellaDeTallasYColoresToolStripMenuItem";
            this.graellaDeTallasYColoresToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.graellaDeTallasYColoresToolStripMenuItem.Text = "Selección Tallas y Colores";
            this.graellaDeTallasYColoresToolStripMenuItem.Click += new System.EventHandler(this.graellaDeTallasYColoresToolStripMenuItem_Click);
            // 
            // paginationToolStripMenuItem
            // 
            this.paginationToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("paginationToolStripMenuItem.Image")));
            this.paginationToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.paginationToolStripMenuItem.Name = "paginationToolStripMenuItem";
            this.paginationToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            this.paginationToolStripMenuItem.Text = "Imagenes TyC";
            this.paginationToolStripMenuItem.Visible = false;
            this.paginationToolStripMenuItem.Click += new System.EventHandler(this.paginationToolStripMenuItem_Click_1);
            // 
            // tESTToolStripMenuItem
            // 
            this.tESTToolStripMenuItem.Name = "tESTToolStripMenuItem";
            this.tESTToolStripMenuItem.Size = new System.Drawing.Size(285, 38);
            // 
            // repasatToolStripMenuItem
            // 
            this.repasatToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repasatToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("repasatToolStripMenuItem.Image")));
            this.repasatToolStripMenuItem.Name = "repasatToolStripMenuItem";
            this.repasatToolStripMenuItem.Size = new System.Drawing.Size(93, 36);
            this.repasatToolStripMenuItem.Text = "&Repasat";
            this.repasatToolStripMenuItem.Click += new System.EventHandler(this.repasatToolStripMenuItem_Click);
            // 
            // accesosDirectosToolStripMenuItem
            // 
            this.accesosDirectosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiendaToolStripMenuItem,
            this.backOfficeToolStripMenuItem});
            this.accesosDirectosToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accesosDirectosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("accesosDirectosToolStripMenuItem.Image")));
            this.accesosDirectosToolStripMenuItem.Name = "accesosDirectosToolStripMenuItem";
            this.accesosDirectosToolStripMenuItem.Size = new System.Drawing.Size(152, 36);
            this.accesosDirectosToolStripMenuItem.Text = "Accesos directos";
            // 
            // tiendaToolStripMenuItem
            // 
            this.tiendaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tiendaToolStripMenuItem.Image")));
            this.tiendaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tiendaToolStripMenuItem.Name = "tiendaToolStripMenuItem";
            this.tiendaToolStripMenuItem.Size = new System.Drawing.Size(173, 38);
            this.tiendaToolStripMenuItem.Text = "&Tienda";
            this.tiendaToolStripMenuItem.Click += new System.EventHandler(this.tiendaToolStripMenuItem_Click);
            // 
            // backOfficeToolStripMenuItem
            // 
            this.backOfficeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("backOfficeToolStripMenuItem.Image")));
            this.backOfficeToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.backOfficeToolStripMenuItem.Name = "backOfficeToolStripMenuItem";
            this.backOfficeToolStripMenuItem.Size = new System.Drawing.Size(173, 38);
            this.backOfficeToolStripMenuItem.Text = "&Back Office";
            this.backOfficeToolStripMenuItem.Click += new System.EventHandler(this.backOfficeToolStripMenuItem_Click);
            // 
            // admanToolStripMenuItem
            // 
            this.admanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.facturasToolStripMenuItem,
            this.toolStripMenuItem3,
            this.paypalAdmanToolStripMenuItem,
            this.captioToolStripMenuItem,
            this.recodificaciónToolStripMenuItem,
            this.presupuestosToolStripMenuItem,
            this.asientosToolStripMenuItem,
            this.importaciónFacturasToolStripMenuItem});
            this.admanToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.admanToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("admanToolStripMenuItem.Image")));
            this.admanToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.admanToolStripMenuItem.Name = "admanToolStripMenuItem";
            this.admanToolStripMenuItem.Size = new System.Drawing.Size(81, 36);
            this.admanToolStripMenuItem.Text = "&Rita";
            // 
            // facturasToolStripMenuItem
            // 
            this.facturasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("facturasToolStripMenuItem.Image")));
            this.facturasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.facturasToolStripMenuItem.Name = "facturasToolStripMenuItem";
            this.facturasToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.facturasToolStripMenuItem.Text = "Facturas";
            this.facturasToolStripMenuItem.Click += new System.EventHandler(this.facturasToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(238, 6);
            // 
            // paypalAdmanToolStripMenuItem
            // 
            this.paypalAdmanToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("paypalAdmanToolStripMenuItem.Image")));
            this.paypalAdmanToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.paypalAdmanToolStripMenuItem.Name = "paypalAdmanToolStripMenuItem";
            this.paypalAdmanToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.paypalAdmanToolStripMenuItem.Text = "Módulo Paypal";
            this.paypalAdmanToolStripMenuItem.Click += new System.EventHandler(this.paypalAdmanToolStripMenuItem_Click);
            // 
            // captioToolStripMenuItem
            // 
            this.captioToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("captioToolStripMenuItem.Image")));
            this.captioToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.captioToolStripMenuItem.Name = "captioToolStripMenuItem";
            this.captioToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.captioToolStripMenuItem.Text = "Captio";
            this.captioToolStripMenuItem.Click += new System.EventHandler(this.captioToolStripMenuItem_Click);
            // 
            // recodificaciónToolStripMenuItem
            // 
            this.recodificaciónToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("recodificaciónToolStripMenuItem.Image")));
            this.recodificaciónToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.recodificaciónToolStripMenuItem.Name = "recodificaciónToolStripMenuItem";
            this.recodificaciónToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.recodificaciónToolStripMenuItem.Text = "Recodificación";
            this.recodificaciónToolStripMenuItem.Click += new System.EventHandler(this.recodificaciónToolStripMenuItem_Click);
            // 
            // presupuestosToolStripMenuItem
            // 
            this.presupuestosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("presupuestosToolStripMenuItem.Image")));
            this.presupuestosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.presupuestosToolStripMenuItem.Name = "presupuestosToolStripMenuItem";
            this.presupuestosToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.presupuestosToolStripMenuItem.Text = "Presupuestos";
            this.presupuestosToolStripMenuItem.Click += new System.EventHandler(this.presupuestosToolStripMenuItem_Click_1);
            // 
            // asientosToolStripMenuItem
            // 
            this.asientosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("asientosToolStripMenuItem.Image")));
            this.asientosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.asientosToolStripMenuItem.Name = "asientosToolStripMenuItem";
            this.asientosToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.asientosToolStripMenuItem.Text = "Asientos";
            this.asientosToolStripMenuItem.Click += new System.EventHandler(this.asientosToolStripMenuItem_Click);
            // 
            // importaciónFacturasToolStripMenuItem
            // 
            this.importaciónFacturasToolStripMenuItem.Name = "importaciónFacturasToolStripMenuItem";
            this.importaciónFacturasToolStripMenuItem.Size = new System.Drawing.Size(241, 38);
            this.importaciónFacturasToolStripMenuItem.Text = "&Importación Facturas";
            this.importaciónFacturasToolStripMenuItem.Click += new System.EventHandler(this.importaciónFacturasToolStripMenuItem_Click);
            // 
            // administraciónToolStripMenuItem
            // 
            this.administraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accederToolStripMenuItem,
            this.reportarUnProblemaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.webservicePSToolStripMenuItem,
            this.arreglarTriggersToolStripMenuItem,
            this.testingToolStripMenuItem,
            this.alphaToolStripMenuItem,
            this.gastosFPCToolStripMenuItem,
            this.puntesToolStripMenuItem});
            this.administraciónToolStripMenuItem.Name = "administraciónToolStripMenuItem";
            this.administraciónToolStripMenuItem.Size = new System.Drawing.Size(127, 36);
            this.administraciónToolStripMenuItem.Text = "&Administración";
            // 
            // accederToolStripMenuItem
            // 
            this.accederToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("accederToolStripMenuItem.Image")));
            this.accederToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.accederToolStripMenuItem.Name = "accederToolStripMenuItem";
            this.accederToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.accederToolStripMenuItem.Text = "A&cceder";
            this.accederToolStripMenuItem.Click += new System.EventHandler(this.accederToolStripMenuItem_Click);
            // 
            // reportarUnProblemaToolStripMenuItem
            // 
            this.reportarUnProblemaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportarUnProblemaToolStripMenuItem.Image")));
            this.reportarUnProblemaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.reportarUnProblemaToolStripMenuItem.Name = "reportarUnProblemaToolStripMenuItem";
            this.reportarUnProblemaToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.reportarUnProblemaToolStripMenuItem.Text = "&Reportar un problema";
            this.reportarUnProblemaToolStripMenuItem.Click += new System.EventHandler(this.reportarUnProblemaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(247, 6);
            // 
            // webservicePSToolStripMenuItem
            // 
            this.webservicePSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("webservicePSToolStripMenuItem.Image")));
            this.webservicePSToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.webservicePSToolStripMenuItem.Name = "webservicePSToolStripMenuItem";
            this.webservicePSToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.webservicePSToolStripMenuItem.Text = "Webservice PS";
            this.webservicePSToolStripMenuItem.Click += new System.EventHandler(this.webservicePSToolStripMenuItem_Click_1);
            // 
            // arreglarTriggersToolStripMenuItem
            // 
            this.arreglarTriggersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("arreglarTriggersToolStripMenuItem.Image")));
            this.arreglarTriggersToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.arreglarTriggersToolStripMenuItem.Name = "arreglarTriggersToolStripMenuItem";
            this.arreglarTriggersToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.arreglarTriggersToolStripMenuItem.Text = "Comprobaciones";
            this.arreglarTriggersToolStripMenuItem.Click += new System.EventHandler(this.arreglarTriggersToolStripMenuItem_Click);
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.testingToolStripMenuItem_Click);
            // 
            // alphaToolStripMenuItem
            // 
            this.alphaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("alphaToolStripMenuItem.Image")));
            this.alphaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.alphaToolStripMenuItem.Name = "alphaToolStripMenuItem";
            this.alphaToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.alphaToolStripMenuItem.Text = "Gestión";
            this.alphaToolStripMenuItem.Click += new System.EventHandler(this.alphaToolStripMenuItem_Click);
            // 
            // gastosFPCToolStripMenuItem
            // 
            this.gastosFPCToolStripMenuItem.Name = "gastosFPCToolStripMenuItem";
            this.gastosFPCToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.gastosFPCToolStripMenuItem.Text = "Gestión de gastos";
            this.gastosFPCToolStripMenuItem.Click += new System.EventHandler(this.gastosFPCToolStripMenuItem_Click_1);
            // 
            // puntesToolStripMenuItem
            // 
            this.puntesToolStripMenuItem.Name = "puntesToolStripMenuItem";
            this.puntesToolStripMenuItem.Size = new System.Drawing.Size(250, 38);
            this.puntesToolStripMenuItem.Text = "&Puntes";
            this.puntesToolStripMenuItem.Click += new System.EventHandler(this.puntesToolStripMenuItem_Click);
            // 
            // presupuestosToolStripMenuItem1
            // 
            this.presupuestosToolStripMenuItem1.Name = "presupuestosToolStripMenuItem1";
            this.presupuestosToolStripMenuItem1.Size = new System.Drawing.Size(115, 36);
            this.presupuestosToolStripMenuItem1.Text = "&Presupuestos";
            this.presupuestosToolStripMenuItem1.Click += new System.EventHandler(this.presupuestosToolStripMenuItem1_Click);
            // 
            // exportarDatosPSToolStripMenuItem
            // 
            this.exportarDatosPSToolStripMenuItem.Name = "exportarDatosPSToolStripMenuItem";
            this.exportarDatosPSToolStripMenuItem.Size = new System.Drawing.Size(290, 38);
            this.exportarDatosPSToolStripMenuItem.Text = "Exportar Datos PS";
            this.exportarDatosPSToolStripMenuItem.Click += new System.EventHandler(this.exportarDatosPSToolStripMenuItem_Click);
            // 
            // frMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1499, 616);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frMainForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frMainForm_FormClosed);
            this.Load += new System.EventHandler(this.frMainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gestiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarDocsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mantenimientosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ficherosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarDoc2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listadoDeDocumentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilidadesPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accesosDirectosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backOfficeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programadorDeTareasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artículosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem imágenesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem generadorDocumentosA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paypalAdmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem captioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recodificaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informePedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem envíoDeDocumentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datosDeParametrizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónBDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accederToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportarUnProblemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presupuestosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asientosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem arreglarTriggersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graellaDeTallasYColoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsMenuPresupuestos;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alphaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem webservicePSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tablasAuxiliaresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alexPedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem códigosDeBarrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paginationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preciosFamClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eurolineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incidenciasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artículosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignaciónDeCategoríasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mimasaIfigenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordatoriosPagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Caracteristicas;
        private System.Windows.Forms.ToolStripMenuItem gastosFPCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amigosFundacióMiróToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recoveryAttributesDismayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importaciónFacturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarPedidosVentaTRSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarDatosIntrastatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem puntesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem presupuestosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportarDatosPSToolStripMenuItem;
    }
}