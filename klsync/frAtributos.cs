﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frAtributos : Form
    {
        private static frAtributos m_FormDefInstance;
        
        csSqlConnects sqlConnects=new csSqlConnects();
        csMySqlConnect mySqlConnect=new csMySqlConnect();
        csCheckVersion checkVersion = new csCheckVersion();

        public static frAtributos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frAtributos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frAtributos()
        {
            InitializeComponent();
        }

        private void btnCopiarAtributosToA3_Click(object sender, EventArgs e)
        {
            copiarAtributosToA3();
        }

        public void copiarAtributosToA3()
        {
            copiarAtributosPS_A3(cargarTablaAtributosPS());
        }

        private DataTable cargarTablaAtributosPS()
        {
            string scriptSql="select " +
                " ps_attribute.id_attribute_group, ps_attribute_group_lang.name as Familia,ps_attribute.id_attribute, ps_attribute_lang.name as Atributo,  ps_attribute_lang.id_lang " +
                " from ps_attribute " +
                " inner join " +
                " ps_attribute_lang on " +
                " ps_attribute_lang.id_attribute=ps_attribute.id_attribute " +
                " inner join " +
                " ps_attribute_group_lang on " +
                " ps_attribute_group_lang.id_attribute_group=ps_attribute.id_attribute_group " +
                " where  ps_attribute_lang.id_lang=1 and  ps_attribute_group_lang.id_lang=1";
            
            DataTable dtAtributos=mySqlConnect.cargarTabla(scriptSql);

            return dtAtributos;
        
        }

        private DataTable cargarTablaATributosA3(string codart="")
        {
            string anexo = "";
            if (codart != "")
            {
                anexo = " AND LTRIM(ARTICULO.CODART)='" + codart + "'";
            }


            string scriptSql = "SELECT " +
                    " dbo.ARTICULO.CODART, dbo.ARTICULO.DESCART, dbo.KLS_ATRIBUTOS_ART.KLS_COD_ATRIBUTO, dbo.KLS_ATRIBUTOS.KLS_DESCGROUP_PS,  " +
                    " dbo.KLS_ATRIBUTOS.KLS_IDGROUP_PS, dbo.KLS_ATRIBUTOS.KLS_VALOR_ATRIBUTO, ARTICULO_1.ACTALARMAOFE, ARTICULO_1.DESCART AS ARTICULOPADRE,  " +
                    " ARTICULO_1.KLS_ID_SHOP AS KLS_ID_SHOP_PATER, ARTICULO_1.CODART AS CODART_PATER " +
                    " FROM " +
                    " dbo.ARTICULO INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS_ART ON dbo.ARTICULO.CODART = dbo.KLS_ATRIBUTOS_ART.CODART INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS ON dbo.KLS_ATRIBUTOS_ART.KLS_COD_ATRIBUTO = dbo.KLS_ATRIBUTOS.KLS_COD_ATRIBUTO INNER JOIN " +
                    " dbo.ARTICULO AS ARTICULO_1 ON dbo.ARTICULO.KLS_ARTICULO_PADRE = ARTICULO_1.CODART " +
                    " WHERE (dbo.ARTICULO.KLS_ARTICULO_PADRE IS NOT NULL)" + anexo;

            DataTable dtAtributos = sqlConnects.obtenerDatosSQLScript(scriptSql);

            return dtAtributos;
        }

        private DataTable cargarArticulosConAtributos()
        {
            string scriptSql = "SELECT " +
                            " dbo.ARTICULO.KLS_ARTICULO_PADRE, dbo.ARTICULO.CODART, ARTICULO_1.KLS_ID_SHOP, SUM(dbo.STOCKALM.UNIDADES) AS STOCK " +
                            " FROM " +
                            " dbo.ARTICULO INNER JOIN " +
                            " dbo.ARTICULO AS ARTICULO_1 ON dbo.ARTICULO.KLS_ARTICULO_PADRE = ARTICULO_1.CODART LEFT OUTER JOIN " +
                            " dbo.STOCKALM ON dbo.ARTICULO.CODART = dbo.STOCKALM.CODART " +
                            " GROUP BY dbo.ARTICULO.KLS_ARTICULO_PADRE, dbo.ARTICULO.CODART, ARTICULO_1.KLS_ID_SHOP " +
                            " HAVING (dbo.ARTICULO.KLS_ARTICULO_PADRE IS NOT NULL) AND (ARTICULO_1.KLS_ID_SHOP > 0) ";

            DataTable dtAtributos = sqlConnects.obtenerDatosSQLScript(scriptSql);

            return dtAtributos;
        
        }

        private void copiarAtributosPS_A3(DataTable dt)
        {
            string valores = "";
            string idFamilia, idAtributo, descFamilia, descAtributo="";
            string scriptInsert = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            { 
                idFamilia=dt.Rows[i]["id_attribute_group"].ToString();
                idAtributo=dt.Rows[i]["id_attribute"].ToString();
                descFamilia="'" + dt.Rows[i]["Familia"].ToString() +"'";
                descAtributo= "'" + dt.Rows[i]["Atributo"].ToString() +"'";
                if (i == 0)
                {
                    valores = "(" + idAtributo + "," + descFamilia + "," + idFamilia + "," + descAtributo + ")";
                }
                else 
                {
                    valores = valores + "," + "(" + idAtributo + "," + descFamilia + "," + idFamilia + "," + descAtributo + ")";
                }
           
            }  
            
            scriptInsert = "INSERT INTO KLS_ATRIBUTOS (KLS_IDGROUP_PS, KLS_DESCGROUP_PS, KLS_COD_ATRIBUTO, KLS_VALOR_ATRIBUTO) VALUES " + valores;

                sqlConnects.insertarValoresEnTabla("KLS_ATRIBUTOS", "(KLS_COD_ATRIBUTO , KLS_DESCGROUP_PS,KLS_IDGROUP_PS , KLS_VALOR_ATRIBUTO)", valores);
               //MessageBox.Show("VALORES INSERTADOS");
        }



        private bool verificarCombinacionArticulo(string id_product, string[] atributos)
        {
            bool existe = false;
            var result = string.Join(",", atributos);
            string atributos2Check = result.ToString();

            string script = "select  ps_product_attribute_combination.id_product_attribute,count(ps_product_attribute_combination.id_product_attribute) " +
                            " from ps_product_attribute_combination " +
                            " inner join ps_product_attribute " +
                            " on ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute " +
                            " where ps_product_attribute.id_product=" + id_product +
                            " and ps_product_attribute_combination.id_attribute in (" + atributos2Check + ") " +
                            " group by ps_product_attribute_combination.id_product_attribute " +
                            " having count(ps_product_attribute_combination.id_product_attribute)=" + atributos.Length;

            if ( mySqlConnect.obtenerValorNumericoQuery(script).ToString() != "")
            {
                existe=true;
            }

            return existe;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            verificarCombinacionArticulo("60", obtenerStringAtributos("SONPRODUCT2"));
        }


        private void insertarAtributoA3_PS(string id_product, string[] atributosProducto)
        {
            //0 Verificar si la combinación está creada en prestashop
            //SI
                //1 Obtener número de Atributo
                //4 ActualizarInsertar stock en ps_stock_available
            //NO
                //1 Obtener ultimo id de ps_product_attribute y sumarle 1
                //2 Insertar Atributos en ps_product_attribute_combination
                //3 Insertar combinación en ps_product_attribute_shop
                //4 Insertar stock en ps_stock_available
        
        }

        public void sincronizarAtributosArticulos()
        {
            //Esta función crea en Prestashop los atributos de artículos Hijo de A3ERP siempre que no existan previamente

            int idCombinacion = 1;
            string articuloPadre_DT1 = "";          //articulo padre
            string articuloCombinacion = "";    //articulo hijo de A3
            string idPSArticuloPadre_DT1 = "";
            string articuloHijoRef_DT1 = "";
            string atributo_DT2 = "";
            string cantidad_DT1 = "0";
            string LineasComb = ""; //ps_attribute
            string LineasProd = "";
            string LineasShop = "";
            string LineasStock = "";
            int i = 0;
            string default_on = "NULL";
            string stock_disponible = "0";
            int numeroFila = 0;
            bool existeCombinacion=false;


            DataTable articulosConAtributos_DT1 =new DataTable();
            DataTable atributosArticulosA3_DT2 = new DataTable();

            articulosConAtributos_DT1 = cargarArticulosConAtributos();
            atributosArticulosA3_DT2 = cargarTablaATributosA3();

            //Obtengo el último ID y le sumo 1
            //Al crearse un artículo se crea una línea de stock (aunque sea 0)
            int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

            //obtengo la última combinación
            int ultimaCombinacion = mySqlConnect.selectID("ps_product_attribute_combination", "id_product_attribute") + 1;
            idCombinacion = ultimaCombinacion;


            //itero la tabla de artículos con atributos
            foreach (DataRow filaArticulo_DT1 in articulosConAtributos_DT1.Rows)
            {
                numeroFila++;
                
                articuloHijoRef_DT1 = filaArticulo_DT1["CODART"].ToString().Trim();

                cantidad_DT1 = filaArticulo_DT1["STOCK"].ToString();
                string kls_id_shop_DT1 = filaArticulo_DT1["KLS_ID_SHOP"].ToString();
                //Verifico que si la combinación Existen en Prestashop
                existeCombinacion = verificarCombinacionArticulo(kls_id_shop_DT1, obtenerStringAtributos(articuloHijoRef_DT1));

                if (existeCombinacion)
                {
                    //Si existe la combinación salto a la siguiente linea
                    continue;
                }

                string articulo_padre_DT1 = filaArticulo_DT1["KLS_ARTICULO_PADRE"].ToString();
                // Cuando no existe la combinación
                if (articuloPadre_DT1 != articulo_padre_DT1)
                {
                    articuloPadre_DT1 = articulo_padre_DT1;

                    idPSArticuloPadre_DT1 = kls_id_shop_DT1;
                }

                foreach (DataRow filaAtributo_DT2 in atributosArticulosA3_DT2.Rows)
                {
                    string articulo_padre_DT2 = filaAtributo_DT2["CODART_PATER"].ToString();
                    string articulo_hijo_DT2 = filaAtributo_DT2["CODART"].ToString().Trim();
                    if (articulo_padre_DT2 == articuloPadre_DT1 && articulo_hijo_DT2 == articuloHijoRef_DT1)
                    {
                        
                        atributo_DT2 = filaAtributo_DT2["KLS_COD_ATRIBUTO"].ToString();

                        if (i > 0)
                        {
                            LineasComb = LineasComb + ",";
                            //LineasProd = LineasProd + ",";
                            //LineasShop = LineasShop + ",";
                        }

                        LineasComb = LineasComb + "(" + atributo_DT2 + "," + idCombinacion + ")";
                        if (checkVersion.checkVersión("1.6.1"))
                        {
                            LineasShop = "(" + idCombinacion + ",1," + default_on + "," + idPSArticuloPadre_DT1 + ")";
                        }
                        else
                        {
                            LineasShop = "(" + idCombinacion + ",1," + default_on + ")";
                        }
                        LineasProd = "(" + idCombinacion + "," + idPSArticuloPadre_DT1 + ",'" + articuloHijoRef_DT1 + "')";
                        LineasStock = "(" + ultimoId.ToString() + "," + idPSArticuloPadre_DT1 + "," + idCombinacion + ",1,0," + cantidad_DT1 + ",0," + stock_disponible + ")";
                        
                        i++;
                    }
                }
                ultimoId = ultimoId + 1;
                idCombinacion = idCombinacion + 1;
                i = 0;
                insertarAtributosArticulo(LineasComb, LineasProd, LineasShop, LineasStock);
                LineasComb = "";
                LineasShop = "";
                LineasProd = "";
                LineasStock = "";
            }
        }

        private void insertarAtributosArticulo(string lineasComb, string lineasProd, string lineasShop, string lineasStock)
        {
            mySqlConnect.InsertValoresEnTabla("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", lineasComb, "");
            mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product, reference)", lineasProd, "");

            if (checkVersion.checkVersión("1.6.1"))
            {
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product)", lineasShop, "");
            }

            else
            {
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", lineasShop, "");
            }

            //vigilar versiones porque pide el id_shop_group
            mySqlConnect.InsertValoresEnTabla("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,id_shop_group,quantity, depends_on_stock, out_of_stock)", lineasStock, "");


        }

        private void button2_Click(object sender, EventArgs e)
        {
            sincronizarAtributosArticulos();
        }

        private string [] obtenerStringAtributos(string codart)
        {
            //Función para obtener un string con los atributos de un artículo a verificar su existencia
            string stringAtributos = "";
           
            DataTable atributosReferencia = new DataTable();
            atributosReferencia = cargarTablaATributosA3(codart);

            string[] listaAtributos = new string[atributosReferencia.Rows.Count];
            int numFilas = 0;
            //Convierto los valores del datatable a un string
            foreach (DataRow fila in atributosReferencia.Rows)
            {
                listaAtributos[numFilas] = fila["KLS_COD_ATRIBUTO"].ToString();
               numFilas++;
            }
            return listaAtributos;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            obtenerStringAtributos("SONPRODUCT2");
        }


    }
}
