﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frFeatures : Form
    {
        private static frFeatures m_FormDefInstance;
        public static frFeatures DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frFeatures();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frFeatures()
        {
            InitializeComponent();
        }

        private void btLoadCaractPS_Click(object sender, EventArgs e)
        {
            cargarCaracteristicasPS();
        }

        private void cargarCaracteristicasPS()
        {
            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(mySqlScript.selectCaracteristicasPS(), dgvCaracteristicasPS);

        }

        private void conectarDB(string sqlScript, DataGridView dgv)
        {

            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;


            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }


        private void btCopyCaractToA3_Click(object sender, EventArgs e)
        {
            CopiarCaracteristicasToA3();
        }

        private void CopiarCaracteristicasToA3()
        {

            csSqlConnects SqlConnects = new csSqlConnects();
            csNetUtilities utilidades = new csNetUtilities();

            SqlConnects.insertarCaracteristicasA3(utilidades.PopulateDataTable(dgvCaracteristicasPS));
            cargarCaracteristicasA3();

        }

        private void cargarCategoriasA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectCategoriasA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvCaracteristicasA3.DataSource = t;

        }

        private void cargarCaracteristicasA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectCaracteristicasA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvCaracteristicasA3.DataSource = t;
        
        }



        private void btnFeatureProduct_Click(object sender, EventArgs e)
        {
            string consulta = "Select ps_product.id_product, ps_feature_product.id_product As id_product1, ps_feature_product.id_feature_value, ps_feature_product.id_feature, ps_product.reference From ps_product Inner Join ps_feature_product On ps_product.id_product = ps_feature_product.id_product";
            csSqlScripts mySqlScript = new csSqlScripts();

            conectarDB(consulta, dgvFeaturesProducts);
        }

        private void insertarFeatureEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                sql.borrarDatosSqlTabla("KLS_CARACTERISTICAS_ART");
                foreach (DataGridViewRow fila in dgvFeaturesProducts.Rows)
                {
                    sql.insertarCaracteristicasArticuloA3(fila);
                }

                MessageBox.Show("Caracteristicas traspasadas a A3");
            }
            catch (Exception ex) { MessageBox.Show("Error al traspasar las caracteristicas a A3"); }
        }

        private void btLoadCaractA3_Click(object sender, EventArgs e)
        {
            cargarCaracteristicasA3();
        }


        


    }
}
