﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csSeur
    {
        // CABECERA 4
        /////////////
        // COMIENZO DE CABECERA DE FICHERO 
        public string InicioCabeceraFicheroComFic { set; get; }
        public string NombreFicheroComFic { set; get; }
        public string CodigoClienteComFic { set; get; }
        public string ResponsableTransmisionComFic { set; get; }
        public string FechaHoraTransmisionComFic { set; get; }

        // FIN DE CABECERA DE FICHERO
        public string FinCabeceraFicheroFinFic { set; get; }
        public string NombreFicheroFinFic { set; get; }

        // COMIENZO DE CABECERA DE REGISTRO
        public string InicioCabeceraRegistroComReg { set; get; }
        public string NombreFicheroComReg { set; get; }
        public string NumeroRegistrosComReg { set; get; }

        // FIN DE CABECERA DE REGISTRO
        public string FinCabeceraRegistroFinReg { set; get; }
        public string NombreFicheroFinReg { set; get; }
        ////////////////

        ////////////////
        // PEDIDOS 5.5
        ////////////////
        public string RegistroDatosPedido { set; get; } 
        public string TipoOperacionPedido { set; get; }
        public string NumeroPedidoPedido { set; get; } 
        public string CodigoClientePedido { set; get; }
        public string CodigoDivisionPedido { set; get; }
        public string CodigoAccionPedido { set; get; }
        public string CodigoAlmacenPedido { set; get; }
        public string TipoPedido { set; get; }
        public string TipoUrgenciaPedido { set; get; }
        public string FechaPedido { set; get; }
        public string CodigoFARSPedido { set; get; }
        public string SolicitanteNombrePedido { set; get; }
        public string SolicitanteApellido1Pedido { set; get; }
        public string SolicitanteApellido2Pedido { set; get; }
        public string CodigoCargoPedido { set; get; }
        public string DepartamentoPedido { set; get; }
        public string DestinatarioCodigoPedido { set; get; }
        public string DestinatarioRazonSocialPedido { set; get; }
        public string DestinatarioNombrePedido { set; get; }
        public string DestinatarioApellido1Pedido { set; get; }
        public string DestinatarioApellido2Pedido { set; get; }
        public string DestinatarioNIFPedido { set; get; }
        public string CodigoCategoriaPedido { set; get; }
        public string NombreCategoriaPedido { set; get; }
        public string CodigoTipoViaPedido { set; get; }
        public string NombreViaPedido { set; get; }
        public string NumeroPedido { set; get; }
        public string EscaleraPedido { set; get; }
        public string PisoPedido { set; get; }
        public string PuertaPedido { set; get; }
        public string DireccionAbreviadaPedido { set; get; }
        public string PrefijoTelefonoPedido { set; get; }
        public string TelefonoPedido { set; get; }
        public string PrefijoFaxPedido { set; get; }
        public string FaxPedido { set; get; }
        public string CodigoPaisPedido { set; get; }
        public string CodigoPostalPedido { set; get; }
        public string PoblacionPedido { set; get; }
        public string ProvinciaPedido { set; get; }
        public string EmpresaTransportePedido { set; get; }
        public string ServicioPedido { set; get; }
        public string TipoReembolsoPedido { set; get; }
        public string ImporteReembolsoPedido { set; get; }
        public string TipoPODPedido { set; get; }
        public string TipoSeguroPedido { set; get; }
        public string ImporteAseguradoPedido { set; get; }
        public string TipoPortePedido { set; get; }
        public string ObservacionesPedido { set; get; }
        public string GastosEnvioPedido { set; get; }
        public string BultosPedido { set; get; }
        public string KilosPedido { set; get; }
        public string VolumenPedido { set; get; }
        public string PedidoDestinatarioPedido { set; get; }
        public string TipoManipulacionPedido { set; get; }
        public string NombreDestinatarioFinalPedido { set; get; }
        public string DireccionDestinatarioFinalPedido { set; get; }
        public string PoblacionDestinatarioFinalPedido { set; get; }
        public string CodigoPostalDestinatarioFinalPedido { set; get; }
        public string PaisDestinatarioFinalPedido { set; get; }
        public string CodigoEmpresaPedido { set; get; }
        public string CodigoCentroPedido { set; get; }
        public string CodigoDepartamentoPedido { set; get; }
        public string CodigoAlbaranPedido { set; get; }
        public string NAD_IV_Pedido { set; get; }
        public string NAD_DP_Pedido { set; get; }
        public string NAD_BY_Pedido { set; get; }
        public string NAD_SU_Pedido { set; get; }
        public string NAD_MS_Pedido { set; get; }
        public string NAD_MR_Pedido { set; get; }
        public string IndicadorSeurCambioPedido { set; get; }
        public string EmailPedido { set; get; }
        public string ObservacionesAlmacenPedido { set; get; }
        
        // LINEAS
        public string RegistroDatosLinea { set; get; }
        public string NumeroPedidoLinea { set; get; }
        public string CodigoClienteLinea { set; get; }
        public string CodigoAlmacenLinea { set; get; }
        public string NumeroLinea { set; get; }
        public string CodigoArticuloLinea { set; get; }
        public string CodigoArticuloV1Linea { set; get; }
        public string CodigoArticuloV2Linea { set; get; }
        public string CodigoArticuloVL { set; get; }
        public string FechaSalidaLinea { set; get; }
        public string CantidadPedidaLinea { set; get; }
        public string CantidadServidaLinea { set; get; }

        // LOTES
        public string RegistroDatosLote { set; get; }
        public string NumeroPedidoLote { set; get; }
        public string CodigoClienteLote { set; get; }
        public string NumeroLineaLote { set; get; }
        public string CodigoArticuloLote { set; get; }
        public string CantidadLote { set; get; }
        public string Lote { set; get; }

        // NÚMEROS DE SERIE
        public string RegistroDatosNumeroSerie { set; get; }
        public string NumeroPedidoNumeroSerie { set; get; }
        public string CodigoClienteNumeroSerie { set; get; }
        public string NumeroLineaNumeroSerie { set; get; }
        public string CodigoArticuloNumeroSerie { set; get; }
        public string NumeroSerie { set; get; }
    }
}
