﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frCategoriasTree : Form
    {
        DataTable tablaArbol = new DataTable();
        DataRow filaRama;

        public frCategoriasTree()
        {
            InitializeComponent();
        }

      
        private void cargarCategoriasPS()
        {
            string categoriaParaAnalizar = "";
            
            tablaArbol.Columns.Add("IdCategoria");
            tablaArbol.Columns.Add("Categoria");
            tablaArbol.Columns.Add("Parent");

            csMySqlConnect mysql = new csMySqlConnect();
            MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
            conn.Open();

            csSqlScripts mySqlScript = new csSqlScripts();
            MySqlDataAdapter MyDA = new MySqlDataAdapter();

            MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectCategoriasPS(), conn);

            DataTable table = new DataTable();
            MyDA.Fill(table);

            categoriaParaAnalizar = csGlobal.categoriaArbol;
            while (categoriaParaAnalizar != "1")
            {
                categoriaParaAnalizar = estructuraArbol(table, categoriaParaAnalizar);
            }

            int totalNodos=tablaArbol.Rows.Count;
            
            for (int i = 0; i < tablaArbol.Rows.Count; i++)
            {
                
                //treeVCategorias.Nodes.Add(tablaArbol.Rows[totalNodos-1].ItemArray[0].ToString() + "-" + tablaArbol.Rows[totalNodos-1].ItemArray[1].ToString());
                TreeNode nuevoNodo = new TreeNode(tablaArbol.Rows[totalNodos - 1].ItemArray[0].ToString() + "-" + tablaArbol.Rows[totalNodos - 1].ItemArray[1].ToString());
                nuevoNodo.Expand();
                treeVCategorias.Nodes.Add(nuevoNodo);
                totalNodos = totalNodos - 1;
            }

            
           
        }


        public string estructuraArbol(DataTable Categorias, string idCategoria)
        {
            string idParent = "";

            for (int i = 1; i < Categorias.Rows.Count; i++)
            {
                if (Categorias.Rows[i].ItemArray[0].ToString() == idCategoria)
                {
                    filaRama = tablaArbol.NewRow();
                    filaRama["IdCategoria"] = Categorias.Rows[i].ItemArray[0].ToString();
                    filaRama["Categoria"] = Categorias.Rows[i].ItemArray[1].ToString();
                    filaRama["Parent"] = Categorias.Rows[i].ItemArray[2].ToString();
                    idParent = Categorias.Rows[i].ItemArray[2].ToString();
                    tablaArbol.Rows.Add(filaRama);
                }
            }



            
            return idParent;
        
        
        
        }

        private void frCategoriasTree_Load(object sender, EventArgs e)
        {
            cargarCategoriasPS();
        }



    }
}
