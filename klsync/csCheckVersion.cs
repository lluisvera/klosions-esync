﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csCheckVersion
    {


        public bool checkVersión(string versionToCheck)
        {
            //Devuelvo Verdadero si la versión a verificar es menor a la instalada
            string v1 = versionToCheck;
            string v2 = csGlobal.versionPS;

            var version1 = new Version(v1);
            var version2 = new Version(v2);

            var result = version1.CompareTo(version2);
            if (result > 0)
            {
             //Version1 is greater
                return false;
            }
            else if (result < 0)
            {
                //Version2 is greater
                return true;
            }
            else
            {
                //Version are the same
                return true;
            }

        }


    }
}
