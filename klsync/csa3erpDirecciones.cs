﻿using System;
using a3ERPActiveX;

namespace klsync
{
    class csa3erpDirecciones
    {
        // vv vv vv SAGRADO vv vv vv
        public void crearDireccion(string direccion, string nombre_direccion, string codcli, string contacto, string ciudad, string zipcode, string id_ps, string email, string codProvi=null)
        {
            string codigo = "";
            Maestro a3maestro = new Maestro();
            try
            {
                a3maestro.Iniciar("DIRENT");

                a3maestro.Nuevo();
                a3maestro.AsString["DIRENT1"] = nombre_direccion;
                a3maestro.AsString["NOMENT"] = direccion;
                a3maestro.AsString["ID_PS"] = id_ps;
                a3maestro.AsString["CODCLI"] = codcli;
                a3maestro.AsString["CONTACTO"] = contacto;
                a3maestro.AsString["POBENT"] = ciudad;
                a3maestro.AsString["DTOENT"] = zipcode;
                a3maestro.AsString["EMAIL"] = email;

                a3maestro.Guarda(true);

                codigo = a3maestro.get_AsString("IDDIRENT");
                
                //Actualizo Prestashop y le asigno el codigo de direccion de A3 creado
                csUtilidades.ejecutarConsulta("update ps_address set kls_id_dirent='"  + codigo +  "' where id_address=" + id_ps,true);
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
        }
        // ^^ ^^ ^^ SAGRADO ^^ ^^ ^^ 

        public void crearDireccion(Objetos.csTercero tercero)
        {
            Maestro a3maestro = new Maestro();
            string idDireccion = "";
            try
            {
                a3maestro.Iniciar("DIRENT");

                a3maestro.Nuevo();
                a3maestro.AsString["DIRENT1"] = tercero.direccion;
                a3maestro.AsString["NOMENT"] = tercero.nomIC;
                a3maestro.AsString["ID_PS"] = tercero.external_id;
                a3maestro.AsString["CODCLI"] = tercero.codIC;
                a3maestro.AsString["CONTACTO"] = tercero.nombre;
                a3maestro.AsString["POBENT"] = tercero.poblacion;
                a3maestro.AsString["DTOENT"] = tercero.codigoPostal;
                a3maestro.AsString["EMAIL"] = tercero.email;
                a3maestro.AsString["TELENT1"] = tercero.telf;
                a3maestro.AsString["KLS_LASTUPDATE"] = DateTime.Now.ToShortDateString();
                //a3maestro.AsString["NIFCLIF"] = tercero.nifcif;

                if (tercero.provincia == null)
                {
                    if (csUtilidades.isAnAddressFromSpain(tercero.external_id))
                    {
                        csSqlConnects sql = new csSqlConnects();
                        a3maestro.AsString["CODPROVI"] = tercero.codigoPostal.Substring(0, 2);
                    }
                    else
                    {
                        a3maestro.AsString["CODPROVI"] = "";
                    }
                }
                else
                    a3maestro.AsString["CODPROVI"] = tercero.codigoPostal.Substring(0, 2);

                idDireccion = a3maestro.get_AsString("IDDIRENT");
                a3maestro.Guarda(true);
                csUtilidades.ejecutarConsulta("update ps_address set kls_id_dirent='" + idDireccion + "' where id_address=" + tercero.external_id, true);

            }
            catch (Exception e) {
                
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
        
        }

        public void crearDireccionRepasat(Objetos.csDireccion[] direcciones, string codTercero=null, bool clientes=true)
        {
            Maestro a3maestro = new Maestro();
            try
            {
               // string[] codigo = new string[2];
                string codigo = "";
                a3maestro.Iniciar("DIRENT");

                for (int i = 0; i < direcciones.Length; i++)
                {
                    if (direcciones[i].idClienteDireccionRepasat == codTercero)
                    {
                        if (!direcciones[i].direccionfiscal)
                        {
                            //9-9-2020 LVG Comento linea porque no entra y no crea las direcciones adicionales
                            //if (direcciones[i].codCli != null)
                            //{
                                a3maestro.Nuevo();
                                //a3maestro..NuevoCodigoNum();
                                a3maestro.AsString["DIRENT1"] = direcciones[i].direccion1;
                                a3maestro.AsString["NOMENT"] = direcciones[i].nombreDireccion;
                                a3maestro.AsString["RPST_ID_DIR"] = direcciones[i].idDireccionRepasat;
                                a3maestro.AsString["CODCLI"] = direcciones[i].codCli;
                                //a3maestro.AsString["CONTACTO"] = direcciones[i].nombre;
                                a3maestro.AsString["CODPROVI"] = direcciones[i].codProvincia;
                                a3maestro.AsString["POBENT"] = direcciones[i].poblacion;
                                a3maestro.AsString["DTOENT"] = direcciones[i].codigoPostal;
                                a3maestro.AsString["EMAIL"] = direcciones[i].email;
                                a3maestro.AsString["TELENT1"] = direcciones[i].telefono;


                                a3maestro.Guarda(true);
                                codigo = a3maestro.get_AsString("IDDIRENT");
                                csRepasatWebService rpstWS = new csRepasatWebService();
                                rpstWS.actualizarDocumentoRepasat("accountaddresses", "address[codExternoDireccion]", direcciones[i].idDireccionRepasat, codigo);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                a3maestro.Acabar();
                a3maestro = null;
                if (csGlobal.modoManual)
                {
                    ex.Message.mb();
                }
            }

        }

        
    }
}
