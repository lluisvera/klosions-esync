﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using a3ERPActiveX;

namespace klsync.Puntes
{
    class csPuntes
    {
        //OdbcConnection DbConnection = new OdbcConnection("DSN=PUNTES;UID=ADMIN;PWD=");
        OdbcConnection DbConnection = new OdbcConnection("DSN=PUNTES 2.0;UID=Puntes;PWD=5kiTo34po3@L");
        SqlConnection cnn;
        string connectionString = @"Data Source=KLSBRAIN\SQLEXPRESS;Initial Catalog=KLSTIC;User ID=sa;Password=klosions";

        public DataTable loadProjects(string tipoCarga, string idProyecto = null)
        {
            try
            {
                string anexoQuery = string.IsNullOrEmpty(idProyecto) ? "" : " AND PR_IdProyecto = '" + idProyecto + "'";

                string anexo = "";
                if (tipoCarga == "PEND") anexo = " AND PR_IdPedidoERP IS NULL";
                if (tipoCarga == "SYNC") anexo = " AND PR_IdPedidoERP >0";

                DataTable dtProjects = new DataTable();
                DataRow Fila;

                string scriptProjects = "SELECT PR_IdProyecto, PR_IdPedidoERP, PR_NombreCliente, PR_FechaSalida, PR_IdCliente, PR_SerieERP, PR_NumPedidoERP, " +
                    " PR_NombreProyecto, PR_DireccionEntrega, PR_RefCliente, PR_RefFacturacion  as Referencia,  PR_Observaciones, PR_RefCodPedido, " +
                    " PR_ObsEntrega, PR_Espesor, PR_AvisoAlarma, PR_Alarmas, PR_Espejo, PR_IDDirEnt, PR_TipoContERP, PR_PeriodoSalida, PR_Material, PR_NumDir, " +
                    " PR_Recib_Inter, PR_Fecha_Entrega_Inter " +
                    " from proyectos where PR_IdCliente is not null " + anexo + anexoQuery;
                

                //Abrimos conexión con Easy Promo
                DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"

                OdbcCommand command = new OdbcCommand(scriptProjects, DbConnection);
                command.CommandType = CommandType.Text;
                OdbcDataReader dr = command.ExecuteReader();



                using (DataSet ds = new DataSet() { EnforceConstraints = false })
                {
                    ds.Tables.Add(dtProjects);
                    dtProjects.Load(dr, LoadOption.OverwriteChanges); // Da error de FileMaker: No cursor open for statement
                    DbConnection.Close();
                    return dtProjects;
                }
            }
            catch (Exception ex)
            {
                if (DbConnection.State == ConnectionState.Open) DbConnection.Close();
                return null;
            }
        }

        public DataTable loadOrders(string tipoCarga, string idProyecto = null)
        {
            try
            {
                //string anexoQuery = string.IsNullOrEmpty(idProyecto) ? "" : " AND PR_IdProyecto = '" + idProyecto + "'";
                string anexoQuery = string.Empty;

                string anexo = "";
                if (tipoCarga == "PEND") anexo = " AND PR_IdPedidoERP IS NULL";
                if (tipoCarga == "SYNC") anexo = " AND PR_IdPedidoERP >0";
                if (!string.IsNullOrEmpty(idProyecto)) anexoQuery = " AND Ped_Num=" + idProyecto;

                DataTable dtProjects = new DataTable();
                DataRow Fila;

                string scriptProjects = "SELECT Ped_Num as NumPed, PR_IdPedidoERP, PR_FechaSalida, PR_IdCliente, Ped_Cliente as Cliente, PR_NumPedidoERP, " +
                    " PR_NombreProyecto,  PR_RefCliente, PR_RefFacturacion  as Referencia,   PR_Observaciones, PR_RefCodPedido, " +
                    " PR_ObsEntrega, Ped_Espejo, PR_IDDirEnt, Ped_DireccionEntrega as DirEntrega, PR_PeriodoSalida, PR_Material, PR_Mercado " +
                    " PR_Recib_Inter, PR_Fecha_Entrega_Inter, PR_NumDir, PR_AvisoAlarma, PR_Alarmas " +
                    " from Ta_d_Pedidos where PR_IdCliente is not null " + anexo + anexoQuery;

                //Abrimos conexión con Easy Promo
                DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"

                OdbcCommand command = new OdbcCommand(scriptProjects, DbConnection);
                command.CommandType = CommandType.Text;
                OdbcDataReader dr = command.ExecuteReader();



                using (DataSet ds = new DataSet() { EnforceConstraints = false })
                {
                    ds.Tables.Add(dtProjects);
                    dtProjects.Load(dr, LoadOption.OverwriteChanges); // Da error de FileMaker: No cursor open for statement
                    DbConnection.Close();
                    return dtProjects;
                }
            }
            catch (Exception ex)
            {
                if (DbConnection.State == ConnectionState.Open) DbConnection.Close();
                return null;
            }
        }



        public DataTable loadTasks(string idDocumento=null)
        {
            try
            {
                //string anexoQuery = string.IsNullOrEmpty(idDocumento) ? "" : " and TA_IdProyecto = '" + idDocumento + "'";
                //string anexoQuery = string.IsNullOrEmpty(idDocumento) ? "" : " and Tar_NumPed = " + idDocumento ;
                string anexoQuery = string.IsNullOrEmpty(idDocumento) ? "" : " and PedLin_NumPed = " + idDocumento;
                DataTable dtTasks = new DataTable();
                DataRow Fila;

                //string scriptTasks = "SELECT TA_IdProyecto, TA_IdTarea as Linea, TA_IdArticulo as Codigo,  TA_UnidadTarea,TA_NombreUsuario, " +
                //    " TA_Cantidad, TA_idLineaERP, TA_Ancho, TA_Alto, TA_CodAlm, TA_ARTICULOName from Tareas where TA_SiBillable=1 " + 
                //    anexoQuery + " order by TA_IdProyecto, TA_IdTarea ";


                //string scriptTasks = "SELECT Tar_NumPed, Tar_IdTarea as Linea, TA_IdArticulo as Codigo, " +
                //    " TA_Cantidad,  TA_Ancho, TA_Alto, TA_CodAlm, TA_ARTICULOName from TA_d_Tareas where Tar_SiBillable=1 " +
                //    anexoQuery + " order by Tar_IdTarea ";

                //Version PedidosLineas
                string scriptTasks = "SELECT PedLin_NumPed AS NumPed, PedLin_TaskSecuence as Linea, Ta_d_Articulos.Art_RefART as Codigo, PedLin_Cantidad as Cantidad, " +
                    " 'x' as TA_idLineaERP, PedLin_Alto Ancho, Pedlin_Ancho as Alto, PedLin_CodAlm as CodAlm, Ta_d_Articulos.Art_NombreArticulo as ArticuloName " +
                    " from Ta_d_PedidosLineas " +
                    " inner join Ta_d_Articulos on Ta_d_PedidosLineas.PedLin_RK_Art = Ta_d_Articulos.Art_PK" +
                    " where Ta_d_Articulos.Art_SiBillable=1 " + anexoQuery;

                //Abrimos conexión con Easy Promo
                DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"

                OdbcCommand command = new OdbcCommand(scriptTasks, DbConnection);
                command.CommandType = CommandType.Text;
                OdbcDataReader dr = command.ExecuteReader();



                using (DataSet ds = new DataSet() { EnforceConstraints = false })
                {
                    ds.Tables.Add(dtTasks);
                    dtTasks.Load(dr, LoadOption.OverwriteChanges); // Da error de FileMaker: No cursor open for statement
                    DbConnection.Close();
                    return dtTasks;
                }
            }
            catch (Exception ex)
            {
                if (DbConnection.State == ConnectionState.Open) DbConnection.Close();
                return null;
            }
        }

        public void updateDocA3Project(string projectFM, string docA3, bool resetear=false)
        {
            try
            {
                string numLinea = "";
                string numLineaFM = "";
                string updateLinea = "";
                string scriptTasks = "";
                DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"


                DataTable dtPedido = new DataTable();
                csSqlConnects sql = new csSqlConnects();
                if (!resetear)
                {
                    dtPedido = sql.obtenerDatosSQLScript("select TIPOCONT, LTRIM(SERIE) AS SERIE, cast(NUMDOC as int) as NUMDOC, IDPEDV from cabepedv WHERE IDPEDV =" + docA3);
                    string tipoContable = dtPedido.Rows[0]["TIPOCONT"].ToString();
                    string serie = dtPedido.Rows[0]["SERIE"].ToString();
                    string numdoc = dtPedido.Rows[0]["NUMDOC"].ToString();
                    scriptTasks  = "UPDATE Ta_d_Pedidos SET PR_IdPedidoERP=" + docA3 +
                        // ",  PR_TipoContERP= '" + tipoContable + "'" +
                        // ",  PR_SerieERP='" + serie + 
                        // "'  WHERE PR_NumPedidoERP=" + projectFM;
                        //"  WHERE Ped_Codigo=" + projectFM;
                        "  WHERE Ped_Num=" + projectFM;

                    //PR_TipoContERP, PR_SerieERP  missing
                }

                scriptTasks = resetear ? "UPDATE PROYECTOS SET " +
                    " PR_IdPedidoERP=NULL, PR_TipoContERP=NULL " +
                    " WHERE Ped_Num=" + docA3 : scriptTasks;

                DataTable lineasPedA3ERP = new DataTable();
                lineasPedA3ERP = sql.obtenerDatosSQLScript("SELECT IDPEDV,CODART, DESCLIN, UNIDADES, PRECIO, IDLIN, NUMLINPED, ORDLIN, REMOTOID FROM LINEPEDI  WHERE IDPEDV=" + docA3);

                OdbcCommand command = new OdbcCommand(scriptTasks, DbConnection);
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();

                if (resetear) {
                    string scriptResetLines = "UPDATE TAREAS SET TA_IdLineaERP= NULL WHERE TA_IdProyecto='" + projectFM + "'";
                    command = new OdbcCommand(scriptResetLines, DbConnection);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }

                if (lineasPedA3ERP.Rows.Count > 0)
                {
                    foreach (DataRow fila in lineasPedA3ERP.Rows) {
                        
                        
                        numLinea = fila["NUMLINPED"].ToString();
                        numLineaFM = fila["REMOTOID"].ToString().Replace(",0000", "");
                        updateLinea = "UPDATE TAREAS SET TA_IdLineaERP= " + numLinea + " WHERE  TA_IdTarea = '" + numLineaFM + "' AND  TA_IdProyecto='" + projectFM + "'";
                        command = new OdbcCommand(updateLinea, DbConnection);
                        command.CommandType = CommandType.Text;
                        //command.ExecuteNonQuery();
                    }
                }



                DbConnection.Close();
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual) ex.Message.mb();
                DbConnection.Close();
            }
        }

        public void borrarSincronizacionDocumentos(DataTable documentos, bool borrarSoloEnlace = false)
        {
            try
            {
               string tipoDocumento = "Pedido";

               csa3erp a3erp = new csa3erp();
                bool pedido = false;
                string idDocumentoA3ERP = "";
                string idNumDocRPST = "";
                string idDocERP = "";
                string idProjectFM = "";
                
                DialogResult Resultado = MessageBox.Show("¡ATENCIÓN ESTE PROCESO ES IRREVERSIBLE!!! \n\n" +
                        "Vas a borrar " + documentos.Rows.Count.ToString() + " documentos en A3ERP y borrar el enlace en File Maker \n¿Estás seguro?", "BORRAR DOCUMENTOS ENLAZADOS", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Resultado == DialogResult.Yes)
                {
                    a3erp.abrirEnlace();

                    foreach (DataRow fila in documentos.Rows)
                    {
                        idDocumentoA3ERP = fila["numDoc"].ToString();
                        idProjectFM = fila["numProjectFM"].ToString();

                        if (string.IsNullOrEmpty(idDocumentoA3ERP))
                        {
                            "La fila seleccionada no tiene un Identificador Externo, verifique los datos por favor".mb();
                            continue;
                        }


                        a3erp.borrarDocumento(idDocumentoA3ERP, "Pedidos", false);
                        updateDocA3Project(idProjectFM, idDocumentoA3ERP,true);

                    }

                    a3erp.cerrarEnlace();
                }
        }
        catch (Exception ex)
        {
            ex.Message.mb();

        }

        }

        public void deleteOrderERP(string idOrder = null, bool modoManual = true)
        {
            try
            {
                if (string.IsNullOrEmpty(idOrder)) return;
                string tipoDocumento = "Pedido";

                csa3erp a3erp = new csa3erp();
                bool pedido = false;
                string idDocumentoA3ERP = "";
                string idNumDocRPST = "";
                string idDocERP = "";
                string idProjectFM = "";

                a3erp.abrirEnlace();
                a3erp.borrarDocumento(idOrder, "Pedidos", false);
                if (modoManual)
                {
                    updateDocA3Project(idOrder, idDocumentoA3ERP, true);
                }
                else
                {
                    updateDocA3Project(idOrder, idOrder, true);
                }
                a3erp.cerrarEnlace();
            }
            catch (Exception ex)
            {
                //ex.Message.mb();
            }

        }

        public void createOrderA3ERP(string idOrder = null, bool modoManual = true, bool editOrder=false)
        {
            DataTable cabeceraDocs = new DataTable();
            DataTable lineasDocs = new DataTable();
            csSqlConnects sql = new csSqlConnects();
            try
            {
                //si no recibo un idOrder, salgo de la función
                if (string.IsNullOrEmpty(idOrder))
                {
                    return;
                }


                //Si le paso un id de Proyecto de FM, cargo los datos de ese documento
                if (!string.IsNullOrEmpty(idOrder))
                {
                    cabeceraDocs = loadOrders("", idOrder);
                    lineasDocs = loadTasks(idOrder);
                }

                Objetos.csCabeceraDoc[] cabecera = new Objetos.csCabeceraDoc[cabeceraDocs.Rows.Count];
                Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[lineasDocs.Rows.Count];


               // MessageBox.Show("Inicio procedimiento");

                try
                {
                    int numDoc = 0;
                    int numLin = 0;
                    string queryExistDoc = string.Empty;
                    if (cabeceraDocs.Rows.Count > 0 && lineasDocs.Rows.Count > 0)
                    {
                        

                        for (int i = 0; i < cabeceraDocs.Rows.Count; i++)
                        {
                            //MessageBox.Show("Inicio bucle cabecera");

                            //Verifico si existe el documento/proyecto en A3ERP
                            /*string queryExistDoc = "SELECT NUMDOC FROM CABEPEDV WHERE TIPOCONT=1 AND SERIE IS NULL " +
                                " AND NUMDOC = " + cabeceraDocs.Rows[i]["PR_IdProyecto"].ToString();*/
                            if (!string.IsNullOrEmpty(cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString()))
                            {

                                queryExistDoc = "SELECT NUMDOC FROM CABEPEDV WHERE SERIE IS NULL " +
                                    " AND NUMDOC = " + cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString();

                                if (sql.obtenerExisteDocumentoEnA3(queryExistDoc))
                                {
                                    //  MessageBox.Show("verificacion existe doc");
                                    //MessageBox.Show(queryExistDoc);

                                    //MessageBox.Show("YA EXISTE UN PEDIDO CON EL NÚMERO " + cabeceraDocs.Rows[i]["PR_IdProyecto"].ToString() + " EN A3ERP");
                                    //continue;
                                    editOrder = true;
                                }
                            }

                            //MessageBox.Show(queryExistDoc);

                          

                            //if (!string.IsNullOrEmpty(cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString())  && !editOrder)
                            //{
                            //    continue;
                            //}
                            //if (!string.IsNullOrEmpty(cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString()))
                            //{
                            //    editOrder = true;
                            //}

                           // MessageBox.Show("Inicio asignación valores cabecera");

                            editOrder = string.IsNullOrEmpty(cabeceraDocs.Rows[i]["PR_IdPedidoERP"].ToString()) ? false : true;
                            cabecera[numDoc] = new Objetos.csCabeceraDoc();
                            cabecera[numDoc].codIC = cabeceraDocs.Rows[i]["PR_IdCliente"].ToString().Trim();
                            cabecera[numDoc].nombreIC = cabeceraDocs.Rows[i]["Cliente"].ToString();
                            cabecera[numDoc].referencia = cabeceraDocs.Rows[i]["Referencia"].ToString();
                            cabecera[numDoc].comentariosCabecera = cabeceraDocs.Rows[i]["PR_Observaciones"].ToString();
                            cabecera[numDoc].external_id = cabeceraDocs.Rows[i]["PR_IdPedidoERP"].ToString();
                            //cabecera[numDoc].serieDoc = csGlobal.serie;
                            //cabecera[numDoc].numDocA3 = cabeceraDocs.Rows[i]["NumPed"].ToString();
                            cabecera[numDoc].numDocA3 = cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString();
                            cabecera[numDoc].extNumdDoc = cabeceraDocs.Rows[i]["NumPed"].ToString();
                            cabecera[numDoc].SOLU_TITULO = cabeceraDocs.Rows[i]["PR_NombreProyecto"].ToString();
                            cabecera[numDoc].SOLU_FC_ENTREGA = cabeceraDocs.Rows[i]["PR_FechaSalida"].ToString();
                            // cabecera[numDoc].SOLU_ENTREGA = cabeceraDocs.Rows[i]["PR_HoraPedido"].ToString();
                            cabecera[numDoc].SOLU_DIR_ENTREGA = cabeceraDocs.Rows[i]["PR_ObsEntrega"].ToString();
                            cabecera[numDoc].numdireccion = cabeceraDocs.Rows[i]["PR_NumDir"].ToString();
                            //cabecera[numDoc].SOLU_MEDIDA = cabeceraDocs.Rows[i]["PR_Espesor"].ToString();
                            cabecera[numDoc].SOLU_ALARMA = cabeceraDocs.Rows[i]["PR_AvisoAlarma"].ToString();
                            cabecera[numDoc].SOLU_MENSAJE = cabeceraDocs.Rows[i]["PR_Alarmas"].ToString();
                            cabecera[numDoc].SOLU_DIRECCION = cabeceraDocs.Rows[i]["PR_ObsEntrega"].ToString();
                            cabecera[numDoc].SOLU_ACABADO = cabeceraDocs.Rows[i]["Ped_Espejo"].ToString(); //PR_RefCodPedido
                            cabecera[numDoc].PARAM1= cabeceraDocs.Rows[i]["PR_RefCodPedido"].ToString();
                            cabecera[numDoc].direccionDirEnvio = cabeceraDocs.Rows[i]["PR_IDDirEnt"].ToString().Replace(",0000","");
                            
                            cabecera[numDoc].SOLU_ENTREGA = cabeceraDocs.Rows[i]["PR_PeriodoSalida"].ToString();
                            cabecera[numDoc].SOLU_MEDIDA = cabeceraDocs.Rows[i]["PR_Material"].ToString();
                            cabecera[numDoc].SOLU_RECIBIDO_INTERDELEG = cabeceraDocs.Rows[i]["PR_Recib_Inter"].ToString()=="NO" ? "F" :"T";
                            cabecera[numDoc].SOLU_FC_ENTREGA_INTERDELEG = cabeceraDocs.Rows[i]["PR_Fecha_Entrega_Inter"].ToString();




                            //Poner la fecha del día de hoy
                            cabecera[numDoc].fechaDoc = Convert.ToDateTime(DateTime.Now);

                            numDoc++;

                            //MessageBox.Show("fin asignación valores cabecera");

                        }

                        for (int lin = 0; lin < lineasDocs.Rows.Count; lin++)
                        {
                            //MessageBox.Show("Inicio asignación valores linea" + lin.ToString() );
                            lineas[numLin] = new Objetos.csLineaDocumento();
                            lineas[numLin].numeroDoc = lineasDocs.Rows[lin]["NumPed"].ToString();
                            lineas[numLin].codigoArticulo = lineasDocs.Rows[lin]["Codigo"].ToString().Trim();
                            lineas[numLin].descripcionArticulo = lineasDocs.Rows[lin]["ArticuloName"].ToString();
                            lineas[numLin].numCabecera = lineasDocs.Rows[lin]["NumPed"].ToString();
                            lineas[numLin].numeroLinea = lineasDocs.Rows[lin]["Linea"].ToString();
                            lineas[numLin].SOLU_ALTO = lineasDocs.Rows[lin]["Alto"].ToString();
                            lineas[numLin].SOLU_ANCHO = lineasDocs.Rows[lin]["Ancho"].ToString();
                            lineas[numLin].SOLU_UNIDADES = lineasDocs.Rows[lin]["Cantidad"].ToString();
                            lineas[numLin].almacen = lineasDocs.Rows[lin]["CodAlm"].ToString();

                            lineas[numLin].cantidad = "1";
                            numLin++;
                        }

                        csGlobal.docDestino = "Pedido";

                        //MessageBox.Show("fin asignación lineas");

                        csa3erp a3erp = new klsync.csa3erp();

                        a3erp.abrirEnlace();
                        if (editOrder)
                        {
                            editarDocA3Objeto(cabecera[0].external_id, cabecera, lineas);
                        }
                        else
                        {
                            //MessageBox.Show("Inicio conexion A3");
                            generarDocA3Objeto(cabecera, lineas, false, "no", null);
                        }
                        a3erp.cerrarEnlace();
                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void editOrder(string idPedido)
        {
            csa3erp a3erp = new csa3erp();
            try
            {


                //a3erp.abrirEnlace();
                //a3erp.editarDocA3Objeto(idPedido);
                //a3erp.cerrarEnlace();
            }
            catch (Exception ex)
            {
                //ex.Message.mb();
            }

        }

        public void pedidoTest()
        {

            Objetos.csCabeceraDoc[] cabecera = new Objetos.csCabeceraDoc[1];
            Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[1];

            csa3erp a3erp = new csa3erp();

            try
            {
                int numDoc = 0;
                int numLin = 0;
                cabecera[numDoc] = new Objetos.csCabeceraDoc();
                cabecera[numDoc].codIC = "2348";
                cabecera[numDoc].nombreIC = "CLIENTE PEDIDO PRUEBAS";
                //cabecera[numDoc].referencia = this.dgvProyectos.SelectedRows[i].Cells["PR_ReferenciaCliente"].Value.ToString();
                cabecera[numDoc].serieDoc = "2022";
                cabecera[numDoc].numDocA3 = "220001";
                cabecera[numDoc].extNumdDoc = "220001";
                //cabecera[numDoc].titulo_trabajo = this.dgvProyectos.SelectedRows[i].Cells["PR_NombreProyecto"].Value.ToString();
                string dateString = "05/04/2022";
                cabecera[numDoc].fechaDoc = Convert.ToDateTime(dateString);

                lineas[numLin] = new Objetos.csLineaDocumento();
                lineas[numLin].numeroDoc = "220001";
                lineas[numLin].codigoArticulo = "0";
                lineas[numLin].descripcionArticulo = "ARTICULO PRUEBAS";
                lineas[numLin].numCabecera = "220001";
                lineas[numLin].cantidad = "1";


                csGlobal.docDestino = "Pedido";

                a3erp.abrirEnlace();
                a3erp.generarDocA3Objeto(cabecera, lineas, false, "no", null);
                a3erp.cerrarEnlace();
                
                

            }
            catch (Exception ex)
            {

            }

        }

        /// <summary>
        /// conectividad con A3ERP
        /// </summary>

        static a3ERPActiveX.Enlace enlace = new Enlace();

        public void abrirEnlace()
        {
            try
            {
                if (estadoConexion())
                {
                    return;
                }

                enlace.RaiseOnException = true;

                enlace.LoginUsuario(csGlobal.userA3, csGlobal.passwordA3);
                enlace.Iniciar(csGlobal.nombreEmpresaA3, string.Empty);
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    (ex.Message).mb();
                }

                enlace.Acabar();
            }
        }

        public bool estadoConexion()
        {
            if (enlace.Estado.ToString() == "estACTIVO")
                return true;
            else
                return false;
        }

        public void cerrarEnlace()
        {
            if (enlace.Estado != EstadoEnlace.estACTIVO) enlace.Acabar();
        }

        public decimal generarDocA3Objeto(Objetos.csCabeceraDoc[] cabeceras, Objetos.csLineaDocumento[] lineas, bool docCompras, string usarClienteGenerico, Objetos.csTercero[] objTercero = null, Objetos.csDomBanca[] objDomiciliaciones = null)
        {
            csGlobal.modeDebug = true;
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            csSqlConnects sql = new csSqlConnects();
            double precioArticulo = 0;
            string codIC = "";
            string articulo = "";
            string numeroDocPs = "";
            string numeroDocA3 = "";
            string fechaSync = "";

            //comprobar si existe el número de pedido antes de crearlo (sin tipo contable, sin serie)!!!!

            try
            {

                for (int i = 0; i < cabeceras.Count(); i++)
                {
                    if (cabeceras[i] != null)
                    {
                        // GENERACIÓN DE PEDIDO DE VENTAS
                            DataTable CabeceraDocsA3 = new DataTable();
                            a3ERPActiveX.Maestro clientes = new Maestro();
                            a3ERPActiveX.Pedido naxpedido = new a3ERPActiveX.Pedido();
                            naxpedido.Iniciar();

                            naxpedido.OmitirMensajes = true;
                            naxpedido.ValidarArtBloqueado = false;
                            naxpedido.ActivarAlarmaCab = false;
                            naxpedido.ActivarAlarmaLin = false;

                            codIC = cabeceras[i].codIC;

                            fechaSync = cabeceras[i].fechaDoc.ToShortDateString();
                            numeroDocA3 = cabeceras[i].numDocA3;
                            numeroDocPs = cabeceras[i].extNumdDoc;
                            naxpedido.Nuevo(fechaSync, codIC, false);
                           
                            
                            if (!string.IsNullOrEmpty(cabeceras[i].nif))
                            {
                                naxpedido.set_AsStringCab("nifcli", cabeceras[i].nif);
                            }

                            //if (string.IsNullOrEmpty(cabeceras[i].serieDoc))
                            //{
                            //    naxpedido.set_AsStringCab("serie", csGlobal.serie);
                            //}
                            //else
                            //{
                            //    naxpedido.set_AsStringCab("serie", cabeceras[i].serieDoc);
                            //}

                            if (!string.IsNullOrEmpty(cabeceras[i].nombreIC))
                            {
                                naxpedido.set_AsStringCab("nomcli", cabeceras[i].nombreIC);
                            }

                        naxpedido.set_AsStringCab("NUMDOC", numeroDocA3);

                       

                        if (csGlobal.conexionDB.Contains("PUNTES"))
                            {
                                naxpedido.set_AsStringCab("SOLU_TITULO", cabeceras[i].SOLU_TITULO);
                                naxpedido.set_AsStringCab("SOLU_ACABADO", cabeceras[i].SOLU_ACABADO == "SI" ? "T" : "F");
                                naxpedido.set_AsStringCab("SOLU_FC_ENTREGA", cabeceras[i].SOLU_FC_ENTREGA);
                                
                            
                                naxpedido.set_AsStringCab("SOLU_ALARMA", cabeceras[i].SOLU_ALARMA == "SI" ? "T" : "F");
                                naxpedido.set_AsStringCab("SOLU_MENSAJE", cabeceras[i].SOLU_MENSAJE);
                                naxpedido.set_AsStringCab("OBSERVACIONES", cabeceras[i].comentariosCabecera);
                                naxpedido.set_AsStringCab("REFERENCIA", cabeceras[i].referencia);
                                naxpedido.set_AsStringCab("SOLU_DIR_ENTREGA", cabeceras[i].SOLU_DIRECCION);
                                naxpedido.set_AsStringCab("SOLU_IT_FILEMAKER","T");
                                naxpedido.set_AsStringCab("PARAM1", cabeceras[i].PARAM1);

                                naxpedido.set_AsStringCab("SOLU_ENTREGA", cabeceras[i].SOLU_ENTREGA);
                                naxpedido.set_AsStringCab("SOLU_MEDIDA", cabeceras[i].SOLU_MEDIDA);
                                //naxpedido.set_AsStringCab("SOLU_RECIBIDO_INTERDELEG", cabeceras[i].SOLU_RECIBIDO_INTERDELEG);
                                //naxpedido.set_AsStringCab("SOLU_FC_ENTREGA_INTERDELEG", cabeceras[i].SOLU_FC_ENTREGA_INTERDELEG);







                            if (sql.consultaExisteFilaClave("IDDIRENT", "DIRENT", cabeceras[i].direccionDirEnvio))
                            {
                                try
                                {
                                    string script = "SELECT LTRIM(CODCLI) FROM DIRENT WHERE IDDIRENT=" + cabeceras[i].direccionDirEnvio; 
                                    string codCli = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM DIRENT WHERE IDDIRENT=" + cabeceras[i].direccionDirEnvio);
                                    //MessageBox.Show(script + "\n" + "Cliente FM: " + cabeceras[i].codIC + "\n" + "Cliente Direccion: " + codCli);
                                    if (codCli == cabeceras[i].codIC)
                                    {
                                        //MessageBox.Show(cabeceras[i].direccionDirEnvio.ToString());
                                        naxpedido.set_AsStringCab("IDDIRENT", cabeceras[i].direccionDirEnvio);
                                        naxpedido.set_AsStringCab("NUMDIR", cabeceras[i].numdireccion);
                                    }
                                }
                                catch { }
                            }
                        }


                            //Referencia del pedido, depende de si está instalado el módulo
                            if (csGlobal.moduloReferenciaPedidos)
                            {
                                naxpedido.set_AsStringCab("referencia", CabeceraDocsA3.Rows[i]["Ref_Cliente"].ToString().ToUpper());
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("referencia", (string.IsNullOrEmpty(cabeceras[i].referencia) ? "" : cabeceras[i].referencia.ToUpper()));
                            }

                            //Para informar de la Razón verifico si está informado
                            if (!string.IsNullOrEmpty(cabeceras[i].razonSocial))
                            {
                                naxpedido.set_AsStringCab("razon", cabeceras[i].razonSocial);
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("razon", cabeceras[i].nombreIC + " " + cabeceras[i].apellidoCliente);
                            }


                            //}

                            csMySqlConnect mySql = new csMySqlConnect();

                            DataTable LineasDocsA3 = new DataTable(); // para borrar cuando se pongan objetos


                            //7-7-2017 AÑADO MODIFICACIÓN PARA AGRUPAS ARTÍCULOS DE TALLAS Y COLORES
                            string codArticuloA3 = "";
                            int numLinea = 0;

                            //SI NO TRABAJA CON TALLAS
                            if (!csGlobal.tieneTallas)
                            {
                                for (int ii = 0; ii < lineas.Length; ii++)
                                {

                                    if (lineas[ii].numeroDoc == cabeceras[i].extNumdDoc)
                                    {
                                        articulo = lineas[ii].codigoArticulo;
                                        naxpedido.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad));
                                        //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                                        //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                                        if (!string.IsNullOrEmpty(lineas[ii].tallaCod) || !string.IsNullOrEmpty(lineas[ii].colorCod))
                                        {

                                            if (!string.IsNullOrEmpty(lineas[ii].precio))
                                            {
                                                precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                {
                                                    precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                                    naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                                }
                                                if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                {
                                                    naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                }
                                            }


                                        }
                                        else
                                        {
                                            if (lineas[ii].precio != null)
                                            {

                                                precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                {
                                                    precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ",")); ;
                                                    naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                                }
                                                if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                {
                                                    naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                }
                                            }
                                        }

                                        if (csGlobal.conexionDB.Contains("PUNTES"))
                                        {
                                        //campos personalizados para puntes
                                        naxpedido.set_AsStringLin("DESCLIN", lineas[ii].descripcionArticulo);
                                        naxpedido.set_AsStringLin("SOLU_ALTO", lineas[ii].SOLU_ALTO);
                                        naxpedido.set_AsStringLin("SOLU_ANCHO", lineas[ii].SOLU_ANCHO);
                                        naxpedido.set_AsStringLin("SOLU_UNIDADES", lineas[ii].SOLU_UNIDADES);
                                        naxpedido.set_AsStringLin("REMOTOID", lineas[ii].numeroLinea);
                                        //naxpedido.set_AsStringLin("CODALM", lineas[ii].almacen);
                                    }

                                        naxpedido.AnadirLinea();
                                    }

                            }
                            decimal documento = naxpedido.Anade();
                            naxpedido.Acabar();

                            ///Actualizo en FileMaker el pedido creado
                            if (csGlobal.conexionDB.Contains("PUNTES"))
                            {
                                Puntes.csPuntes puntes = new Puntes.csPuntes();
                                //puntes.updateDocA3Project(cabeceras[i].numDocA3, documento.ToString());
                                puntes.updateDocA3Project(cabeceras[i].extNumdDoc, documento.ToString());
                            }


                        } 
                    } 

                }
            }
            
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
                ex.ToString();
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return 0;
        }

        public void editarDocA3Objeto(string idDocumento, Objetos.csCabeceraDoc[] cabeceras, Objetos.csLineaDocumento[] lineas)
        {

            csGlobal.docDestino = "Pedido";
            a3ERPActiveX.Pedido naxPedido = new a3ERPActiveX.Pedido();
            naxPedido.Iniciar();
            naxPedido.OmitirMensajes = true;
            naxPedido.ActivarAlarmaCab = false;
            naxPedido.ActivarAlarmaLin = false;
            decimal documentoA3 = Convert.ToDecimal(idDocumento);

            csSqlConnects sql = new csSqlConnects();
            DataTable lineasDocs = new DataTable();
            lineasDocs = sql.obtenerDatosSQLScript("select IDLIN, IDPEDV,NUMLINPED,ORDLIN from LINEPEDI WHERE IDPEDV=" + cabeceras[0].external_id);

            naxPedido.Modifica(Convert.ToDecimal(cabeceras[0].external_id), false);

            naxPedido.set_AsStringCab("SOLU_TITULO", cabeceras[0].SOLU_TITULO);
            naxPedido.set_AsStringCab("SOLU_ACABADO", cabeceras[0].SOLU_ACABADO == "SI" ? "T" : "F");
            naxPedido.set_AsStringCab("SOLU_FC_ENTREGA", cabeceras[0].SOLU_FC_ENTREGA);
            naxPedido.set_AsStringCab("SOLU_ALARMA", cabeceras[0].SOLU_ALARMA == "SI" ? "T" : "F");
            naxPedido.set_AsStringCab("SOLU_MENSAJE", cabeceras[0].SOLU_MENSAJE);
            naxPedido.set_AsStringCab("OBSERVACIONES", cabeceras[0].comentariosCabecera);
            naxPedido.set_AsStringCab("REFERENCIA", cabeceras[0].referencia);
            naxPedido.set_AsStringCab("SOLU_DIR_ENTREGA", cabeceras[0].SOLU_DIRECCION);
            naxPedido.set_AsStringCab("SOLU_IT_FILEMAKER", "T");
            naxPedido.set_AsStringCab("PARAM1", cabeceras[0].PARAM1);


            naxPedido.set_AsStringCab("SOLU_ENTREGA", cabeceras[0].SOLU_ENTREGA);
            naxPedido.set_AsStringCab("SOLU_MEDIDA", cabeceras[0].SOLU_MEDIDA);
            //naxPedido.set_AsStringCab("SOLU_RECIBIDO_INTERDELEG", cabeceras[0].SOLU_RECIBIDO_INTERDELEG);
            //naxPedido.set_AsStringCab("SOLU_FC_ENTREGA_INTERDELEG", cabeceras[0].SOLU_FC_ENTREGA_INTERDELEG);

            if (sql.consultaExisteFilaClave("IDDIRENT", "DIRENT", cabeceras[0].direccionDirEnvio))
            {
                try
                {
                    string script = "SELECT LTRIM(CODCLI) FROM DIRENT WHERE IDDIRENT=" + cabeceras[0].direccionDirEnvio;
                    string codCli = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM DIRENT WHERE IDDIRENT=" + cabeceras[0].direccionDirEnvio);
                    //MessageBox.Show(script + "\n" + "Cliente FM: " + cabeceras[i].codIC + "\n" + "Cliente Direccion: " + codCli);
                    if (codCli == cabeceras[0].codIC)
                    {
                        //MessageBox.Show(cabeceras[i].direccionDirEnvio.ToString());
                        naxPedido.set_AsStringCab("IDDIRENT", cabeceras[0].direccionDirEnvio);
                    }
                }
                catch { }
            }




            foreach (DataRow linea in lineasDocs.Rows)
            {
                naxPedido.BorrarLinea(Convert.ToDecimal(linea["NUMLINPED"].ToString()));
            }

            foreach (Objetos.csLineaDocumento linea in lineas)
            {
                naxPedido.NuevaLinea();
                naxPedido.set_AsStringLin("CODART", linea.codigoArticulo);
                naxPedido.set_AsStringLin("DESCLIN", linea.descripcionArticulo);
                naxPedido.set_AsStringLin("UNIDADES", "1");
                naxPedido.set_AsStringLin("REMOTOID", linea.numeroLinea);
                naxPedido.set_AsStringLin("SOLU_ALTO", linea.SOLU_ALTO);
                naxPedido.set_AsStringLin("SOLU_ANCHO", linea.SOLU_ANCHO);
                naxPedido.set_AsStringLin("SOLU_UNIDADES", linea.SOLU_UNIDADES);
                naxPedido.set_AsStringLin("CODALM", linea.almacen);

            }
            naxPedido.Anade();

            naxPedido.Acabar();

            ///Actualizo en FileMaker el pedido creado
            if (csGlobal.conexionDB.Contains("PUNTES"))
            {
                Puntes.csPuntes puntes = new Puntes.csPuntes();
                puntes.updateDocA3Project(cabeceras[0].external_id, idDocumento.ToString());
            }
        }

    }

}