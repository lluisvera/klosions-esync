﻿namespace klsync.Puntes
{
    partial class frMainFormPuntes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frMainFormPuntes));
            this.btnLoadFMData = new System.Windows.Forms.Button();
            this.dgvTareas = new System.Windows.Forms.DataGridView();
            this.dgvProyectos = new System.Windows.Forms.DataGridView();
            this.cMenuProyectos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarSyncronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearIDA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoPedidoEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarPedidoEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCreateOrderA3ERP = new System.Windows.Forms.Button();
            this.rbtAll = new System.Windows.Forms.RadioButton();
            this.rbtPending = new System.Windows.Forms.RadioButton();
            this.rbtSync = new System.Windows.Forms.RadioButton();
            this.btnEditOrder = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblProjects = new System.Windows.Forms.Label();
            this.lblTareas = new System.Windows.Forms.Label();
            this.lblA3ERPServer = new System.Windows.Forms.Label();
            this.lblA3ERPDatabase = new System.Windows.Forms.Label();
            this.lblFileMakerODBC = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblConnectionname = new System.Windows.Forms.Label();
            this.lblConnector = new System.Windows.Forms.Label();
            this.lblA3ERPDBValue = new System.Windows.Forms.Label();
            this.lblODBCValue = new System.Windows.Forms.Label();
            this.lblA3ERPServerValue = new System.Windows.Forms.Label();
            this.lblSerieDocA3Value = new System.Windows.Forms.Label();
            this.lblSerieDoc = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTareas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProyectos)).BeginInit();
            this.cMenuProyectos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLoadFMData
            // 
            this.btnLoadFMData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadFMData.Location = new System.Drawing.Point(300, 48);
            this.btnLoadFMData.Name = "btnLoadFMData";
            this.btnLoadFMData.Size = new System.Drawing.Size(127, 56);
            this.btnLoadFMData.TabIndex = 0;
            this.btnLoadFMData.Text = "&Cargar Datos File Maker";
            this.btnLoadFMData.UseVisualStyleBackColor = true;
            this.btnLoadFMData.Click += new System.EventHandler(this.btnLoadFMData_Click);
            // 
            // dgvTareas
            // 
            this.dgvTareas.AllowUserToAddRows = false;
            this.dgvTareas.AllowUserToDeleteRows = false;
            this.dgvTareas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTareas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTareas.Location = new System.Drawing.Point(298, 384);
            this.dgvTareas.Name = "dgvTareas";
            this.dgvTareas.ReadOnly = true;
            this.dgvTareas.Size = new System.Drawing.Size(991, 207);
            this.dgvTareas.TabIndex = 1;
            // 
            // dgvProyectos
            // 
            this.dgvProyectos.AllowUserToAddRows = false;
            this.dgvProyectos.AllowUserToDeleteRows = false;
            this.dgvProyectos.AllowUserToOrderColumns = true;
            this.dgvProyectos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProyectos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvProyectos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProyectos.ContextMenuStrip = this.cMenuProyectos;
            this.dgvProyectos.Location = new System.Drawing.Point(298, 155);
            this.dgvProyectos.Name = "dgvProyectos";
            this.dgvProyectos.ReadOnly = true;
            this.dgvProyectos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProyectos.Size = new System.Drawing.Size(991, 199);
            this.dgvProyectos.TabIndex = 2;
            this.dgvProyectos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProyectos_CellClick);
            // 
            // cMenuProyectos
            // 
            this.cMenuProyectos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarSyncronizaciónToolStripMenuItem,
            this.resetearIDA3ERPToolStripMenuItem,
            this.nuevoPedidoEnA3ERPToolStripMenuItem,
            this.editarPedidoEnA3ERPToolStripMenuItem});
            this.cMenuProyectos.Name = "cMenuProyectos";
            this.cMenuProyectos.Size = new System.Drawing.Size(203, 92);
            // 
            // borrarSyncronizaciónToolStripMenuItem
            // 
            this.borrarSyncronizaciónToolStripMenuItem.Name = "borrarSyncronizaciónToolStripMenuItem";
            this.borrarSyncronizaciónToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.borrarSyncronizaciónToolStripMenuItem.Text = "&Borrar Syncronización";
            this.borrarSyncronizaciónToolStripMenuItem.Click += new System.EventHandler(this.borrarSyncronizaciónToolStripMenuItem_Click);
            // 
            // resetearIDA3ERPToolStripMenuItem
            // 
            this.resetearIDA3ERPToolStripMenuItem.Name = "resetearIDA3ERPToolStripMenuItem";
            this.resetearIDA3ERPToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.resetearIDA3ERPToolStripMenuItem.Text = "&Resetear ID A3ERP";
            this.resetearIDA3ERPToolStripMenuItem.Click += new System.EventHandler(this.resetearIDA3ERPToolStripMenuItem_Click);
            // 
            // nuevoPedidoEnA3ERPToolStripMenuItem
            // 
            this.nuevoPedidoEnA3ERPToolStripMenuItem.Name = "nuevoPedidoEnA3ERPToolStripMenuItem";
            this.nuevoPedidoEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.nuevoPedidoEnA3ERPToolStripMenuItem.Text = "&Nuevo Pedido en A3ERP";
            this.nuevoPedidoEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.nuevoPedidoEnA3ERPToolStripMenuItem_Click);
            // 
            // editarPedidoEnA3ERPToolStripMenuItem
            // 
            this.editarPedidoEnA3ERPToolStripMenuItem.Name = "editarPedidoEnA3ERPToolStripMenuItem";
            this.editarPedidoEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.editarPedidoEnA3ERPToolStripMenuItem.Text = "&Editar Pedido en A3ERP";
            this.editarPedidoEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.editarPedidoEnA3ERPToolStripMenuItem_Click);
            // 
            // btnCreateOrderA3ERP
            // 
            this.btnCreateOrderA3ERP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateOrderA3ERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateOrderA3ERP.Location = new System.Drawing.Point(996, 46);
            this.btnCreateOrderA3ERP.Name = "btnCreateOrderA3ERP";
            this.btnCreateOrderA3ERP.Size = new System.Drawing.Size(145, 60);
            this.btnCreateOrderA3ERP.TabIndex = 3;
            this.btnCreateOrderA3ERP.Text = "&Nuevo Pedido en A3ERP";
            this.btnCreateOrderA3ERP.UseVisualStyleBackColor = true;
            this.btnCreateOrderA3ERP.Click += new System.EventHandler(this.btnCreateOrderA3ERP_Click);
            // 
            // rbtAll
            // 
            this.rbtAll.AutoSize = true;
            this.rbtAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtAll.Location = new System.Drawing.Point(298, 19);
            this.rbtAll.Name = "rbtAll";
            this.rbtAll.Size = new System.Drawing.Size(71, 24);
            this.rbtAll.TabIndex = 5;
            this.rbtAll.Text = "Todos";
            this.rbtAll.UseVisualStyleBackColor = true;
            this.rbtAll.CheckedChanged += new System.EventHandler(this.rbtAll_CheckedChanged);
            // 
            // rbtPending
            // 
            this.rbtPending.AutoSize = true;
            this.rbtPending.Checked = true;
            this.rbtPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtPending.Location = new System.Drawing.Point(28, 19);
            this.rbtPending.Name = "rbtPending";
            this.rbtPending.Size = new System.Drawing.Size(107, 24);
            this.rbtPending.TabIndex = 6;
            this.rbtPending.TabStop = true;
            this.rbtPending.Text = "Pendientes";
            this.rbtPending.UseVisualStyleBackColor = true;
            this.rbtPending.CheckedChanged += new System.EventHandler(this.rbtPending_CheckedChanged);
            // 
            // rbtSync
            // 
            this.rbtSync.AutoSize = true;
            this.rbtSync.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtSync.Location = new System.Drawing.Point(150, 19);
            this.rbtSync.Name = "rbtSync";
            this.rbtSync.Size = new System.Drawing.Size(127, 24);
            this.rbtSync.TabIndex = 7;
            this.rbtSync.Text = "Sincronizados";
            this.rbtSync.UseVisualStyleBackColor = true;
            this.rbtSync.CheckedChanged += new System.EventHandler(this.rbtSync_CheckedChanged);
            // 
            // btnEditOrder
            // 
            this.btnEditOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditOrder.Location = new System.Drawing.Point(1160, 46);
            this.btnEditOrder.Name = "btnEditOrder";
            this.btnEditOrder.Size = new System.Drawing.Size(129, 60);
            this.btnEditOrder.TabIndex = 14;
            this.btnEditOrder.Text = "&Editar Pedido en A3ERP";
            this.btnEditOrder.UseVisualStyleBackColor = true;
            this.btnEditOrder.Click += new System.EventHandler(this.btnEditOrder_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 105);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 582);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(221, 77);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtAll);
            this.groupBox1.Controls.Add(this.rbtPending);
            this.groupBox1.Controls.Add(this.rbtSync);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(451, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(406, 56);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro Selección";
            // 
            // lblProjects
            // 
            this.lblProjects.AutoSize = true;
            this.lblProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjects.Location = new System.Drawing.Point(294, 128);
            this.lblProjects.Name = "lblProjects";
            this.lblProjects.Size = new System.Drawing.Size(93, 24);
            this.lblProjects.TabIndex = 18;
            this.lblProjects.Text = "Proyectos";
            // 
            // lblTareas
            // 
            this.lblTareas.AutoSize = true;
            this.lblTareas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTareas.Location = new System.Drawing.Point(294, 357);
            this.lblTareas.Name = "lblTareas";
            this.lblTareas.Size = new System.Drawing.Size(68, 24);
            this.lblTareas.TabIndex = 19;
            this.lblTareas.Text = "Tareas";
            // 
            // lblA3ERPServer
            // 
            this.lblA3ERPServer.AutoSize = true;
            this.lblA3ERPServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblA3ERPServer.Location = new System.Drawing.Point(18, 142);
            this.lblA3ERPServer.Name = "lblA3ERPServer";
            this.lblA3ERPServer.Size = new System.Drawing.Size(102, 13);
            this.lblA3ERPServer.TabIndex = 21;
            this.lblA3ERPServer.Text = "Servidor A3ERP:";
            // 
            // lblA3ERPDatabase
            // 
            this.lblA3ERPDatabase.AutoSize = true;
            this.lblA3ERPDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblA3ERPDatabase.Location = new System.Drawing.Point(18, 67);
            this.lblA3ERPDatabase.Name = "lblA3ERPDatabase";
            this.lblA3ERPDatabase.Size = new System.Drawing.Size(138, 13);
            this.lblA3ERPDatabase.TabIndex = 22;
            this.lblA3ERPDatabase.Text = "Base de Datos A3ERP:";
            // 
            // lblFileMakerODBC
            // 
            this.lblFileMakerODBC.AutoSize = true;
            this.lblFileMakerODBC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileMakerODBC.Location = new System.Drawing.Point(18, 186);
            this.lblFileMakerODBC.Name = "lblFileMakerODBC";
            this.lblFileMakerODBC.Size = new System.Drawing.Size(108, 13);
            this.lblFileMakerODBC.TabIndex = 23;
            this.lblFileMakerODBC.Text = "ODBC File Maker:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblSerieDocA3Value);
            this.groupBox2.Controls.Add(this.lblSerieDoc);
            this.groupBox2.Controls.Add(this.lblConnectionname);
            this.groupBox2.Controls.Add(this.lblConnector);
            this.groupBox2.Controls.Add(this.lblA3ERPDBValue);
            this.groupBox2.Controls.Add(this.lblODBCValue);
            this.groupBox2.Controls.Add(this.lblA3ERPServerValue);
            this.groupBox2.Controls.Add(this.lblA3ERPDatabase);
            this.groupBox2.Controls.Add(this.lblFileMakerODBC);
            this.groupBox2.Controls.Add(this.lblA3ERPServer);
            this.groupBox2.Location = new System.Drawing.Point(12, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(239, 226);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos de Configuración";
            // 
            // lblConnectionname
            // 
            this.lblConnectionname.AutoSize = true;
            this.lblConnectionname.Location = new System.Drawing.Point(18, 48);
            this.lblConnectionname.Name = "lblConnectionname";
            this.lblConnectionname.Size = new System.Drawing.Size(10, 13);
            this.lblConnectionname.TabIndex = 28;
            this.lblConnectionname.Text = "-";
            // 
            // lblConnector
            // 
            this.lblConnector.AutoSize = true;
            this.lblConnector.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnector.Location = new System.Drawing.Point(18, 26);
            this.lblConnector.Name = "lblConnector";
            this.lblConnector.Size = new System.Drawing.Size(110, 13);
            this.lblConnector.TabIndex = 27;
            this.lblConnector.Text = "Nombre Conexión:";
            // 
            // lblA3ERPDBValue
            // 
            this.lblA3ERPDBValue.AutoSize = true;
            this.lblA3ERPDBValue.Location = new System.Drawing.Point(18, 89);
            this.lblA3ERPDBValue.Name = "lblA3ERPDBValue";
            this.lblA3ERPDBValue.Size = new System.Drawing.Size(10, 13);
            this.lblA3ERPDBValue.TabIndex = 25;
            this.lblA3ERPDBValue.Text = "-";
            // 
            // lblODBCValue
            // 
            this.lblODBCValue.AutoSize = true;
            this.lblODBCValue.Location = new System.Drawing.Point(18, 208);
            this.lblODBCValue.Name = "lblODBCValue";
            this.lblODBCValue.Size = new System.Drawing.Size(10, 13);
            this.lblODBCValue.TabIndex = 26;
            this.lblODBCValue.Text = "-";
            // 
            // lblA3ERPServerValue
            // 
            this.lblA3ERPServerValue.AutoSize = true;
            this.lblA3ERPServerValue.Location = new System.Drawing.Point(18, 164);
            this.lblA3ERPServerValue.Name = "lblA3ERPServerValue";
            this.lblA3ERPServerValue.Size = new System.Drawing.Size(10, 13);
            this.lblA3ERPServerValue.TabIndex = 24;
            this.lblA3ERPServerValue.Text = "-";
            // 
            // lblSerieDocA3Value
            // 
            this.lblSerieDocA3Value.AutoSize = true;
            this.lblSerieDocA3Value.Location = new System.Drawing.Point(18, 124);
            this.lblSerieDocA3Value.Name = "lblSerieDocA3Value";
            this.lblSerieDocA3Value.Size = new System.Drawing.Size(10, 13);
            this.lblSerieDocA3Value.TabIndex = 30;
            this.lblSerieDocA3Value.Text = "-";
            // 
            // lblSerieDoc
            // 
            this.lblSerieDoc.AutoSize = true;
            this.lblSerieDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerieDoc.Location = new System.Drawing.Point(18, 102);
            this.lblSerieDoc.Name = "lblSerieDoc";
            this.lblSerieDoc.Size = new System.Drawing.Size(115, 13);
            this.lblSerieDoc.TabIndex = 29;
            this.lblSerieDoc.Text = "Serie Doc. A3ERP:";
            // 
            // frMainFormPuntes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1334, 671);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblTareas);
            this.Controls.Add(this.lblProjects);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnEditOrder);
            this.Controls.Add(this.btnCreateOrderA3ERP);
            this.Controls.Add(this.dgvProyectos);
            this.Controls.Add(this.dgvTareas);
            this.Controls.Add(this.btnLoadFMData);
            this.Name = "frMainFormPuntes";
            this.Text = "Conector Diflex FM 2 A3ERP";
            this.Load += new System.EventHandler(this.frMainFormPuntes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTareas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProyectos)).EndInit();
            this.cMenuProyectos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadFMData;
        private System.Windows.Forms.DataGridView dgvTareas;
        private System.Windows.Forms.DataGridView dgvProyectos;
        private System.Windows.Forms.Button btnCreateOrderA3ERP;
        private System.Windows.Forms.RadioButton rbtAll;
        private System.Windows.Forms.RadioButton rbtPending;
        private System.Windows.Forms.RadioButton rbtSync;
        private System.Windows.Forms.ContextMenuStrip cMenuProyectos;
        private System.Windows.Forms.ToolStripMenuItem borrarSyncronizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearIDA3ERPToolStripMenuItem;
        private System.Windows.Forms.Button btnEditOrder;
        private System.Windows.Forms.ToolStripMenuItem nuevoPedidoEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarPedidoEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblProjects;
        private System.Windows.Forms.Label lblTareas;
        private System.Windows.Forms.Label lblA3ERPServer;
        private System.Windows.Forms.Label lblA3ERPDatabase;
        private System.Windows.Forms.Label lblFileMakerODBC;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblConnectionname;
        private System.Windows.Forms.Label lblConnector;
        private System.Windows.Forms.Label lblA3ERPDBValue;
        private System.Windows.Forms.Label lblODBCValue;
        private System.Windows.Forms.Label lblA3ERPServerValue;
        private System.Windows.Forms.Label lblSerieDocA3Value;
        private System.Windows.Forms.Label lblSerieDoc;
    }
}