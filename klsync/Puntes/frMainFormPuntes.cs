﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using ExcelDataReader;



namespace klsync.Puntes
{
    public partial class frMainFormPuntes : Form
    {
        private static frMainFormPuntes m_FormDefInstance;
        public static frMainFormPuntes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frMainFormPuntes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        csPuntes puntes = new csPuntes();
        string tipoSync = "ALL";

        OdbcConnection DbConnection = new OdbcConnection("DSN=PUNTES;UID=ADMIN;PWD=");
        SqlConnection cnn;
        string connectionString = @"Data Source=KLSBRAIN\SQLEXPRESS;Initial Catalog=KLSTIC;User ID=sa;Password=klosions";

        csa3erp a3erp = new csa3erp();

        public frMainFormPuntes()
        {
            InitializeComponent();
        }

        private void btnLoadFMData_Click(object sender, EventArgs e)
        {
            try
            {
                tipoCargaProyectos();
                dgvTareas.DataSource = puntes.loadTasks();
            }
            catch (Exception ex){
                ex.Message.mb();
            }
        }


        /// <summary>
        /// Creación de pedidos en A3ERP
        /// </summary>
        //private void createOrderA3ERP(string idOrder=null, bool modoManual=true)
        //{
        //    DataTable cabeceraDocs = new DataTable();
        //    DataTable lineasDocs = new DataTable();
        //    try {

        //        if (csGlobal.modoManual)
        //        {
        //            if (dgvProyectos.SelectedRows.Count < 1)
        //            {
        //                MessageBox.Show("Debe seleccionar al menos una linea para traspasar");
        //            }
        //        }
        //            //si no recibo un idOrder, salgo de la función
        //            if (string.IsNullOrEmpty(idOrder))
        //            {
        //                return;
        //            }
                   

        //            //Si lepaso un id de Proyecto de FM, cargo los datos de ese documento
        //            if (!string.IsNullOrEmpty(idOrder))
        //            {
        //                cabeceraDocs = puntes.loadProjects("", idOrder);
        //                lineasDocs = puntes.loadTasks(idOrder);
        //            }

        //            Objetos.csCabeceraDoc[] cabecera = new Objetos.csCabeceraDoc[cabeceraDocs.Rows.Count];
        //            Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[lineasDocs.Rows.Count];


        //            try
        //            {
        //                int numDoc = 0;
        //                int numLin = 0;
        //                if (cabeceraDocs.Rows.Count > 0 && lineasDocs.Rows.Count > 0)
        //                {
        //                    for (int i = 0; i < cabeceraDocs.Rows.Count; i++)
        //                    {
        //                        if (!string.IsNullOrEmpty(cabeceraDocs.Rows[i]["PR_NumPedidoERP"].ToString()))
        //                        {
        //                            continue;
        //                        }
        //                        cabecera[numDoc] = new Objetos.csCabeceraDoc();
        //                        cabecera[numDoc].codIC = cabeceraDocs.Rows[i]["PR_IdCliente"].ToString().Trim();
        //                        cabecera[numDoc].nombreIC = cabeceraDocs.Rows[i]["PR_NombreCliente"].ToString();
        //                        cabecera[numDoc].referencia = cabeceraDocs.Rows[i]["Referencia"].ToString();
        //                        cabecera[numDoc].serieDoc = csGlobal.serie;
        //                        cabecera[numDoc].numDocA3 = cabeceraDocs.Rows[i]["PR_IdProyecto"].ToString();
        //                        cabecera[numDoc].extNumdDoc = cabeceraDocs.Rows[i]["PR_IdProyecto"].ToString();
        //                        cabecera[numDoc].titulo_trabajo = cabeceraDocs.Rows[i]["PR_NombreProyecto"].ToString();
        //                        cabecera[numDoc].fechaDoc = Convert.ToDateTime(cabeceraDocs.Rows[i]["PR_FechaSalida"].ToString());

        //                        numDoc++;

        //                    }

        //                    for (int lin = 0; lin < lineasDocs.Rows.Count; lin++)
        //                    {
        //                        lineas[numLin] = new Objetos.csLineaDocumento();
        //                        lineas[numLin].numeroDoc = lineasDocs.Rows[lin]["TA_IdProyecto"].ToString();
        //                        lineas[numLin].codigoArticulo = lineasDocs.Rows[lin]["Codigo"].ToString().Trim();
        //                        lineas[numLin].descripcionArticulo = lineasDocs.Rows[lin]["TA_NombreTarea"].ToString();
        //                        lineas[numLin].numCabecera = lineasDocs.Rows[lin]["TA_IdProyecto"].ToString();
        //                        lineas[numLin].numeroLinea = lineasDocs.Rows[lin]["Linea"].ToString();

        //                        lineas[numLin].cantidad = "1";
        //                        numLin++;
        //                    }

        //                    csGlobal.docDestino = "Pedido";

        //                    a3erp.abrirEnlace();
        //                    this.a3erp.generarDocA3Objeto(cabecera, lineas, false, "no", null);
        //                    a3erp.cerrarEnlace();
        //                    tipoCargaProyectos();
        //                    this.Refresh();

        //                }
        //                else
        //                {

        //                }
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //    }
        //    catch {

        //    }
        //}
        private void btnCreateOrderA3ERP_Click(object sender, EventArgs e)
        {
            try {
                

                foreach (DataGridViewRow fila in dgvProyectos.SelectedRows)
                {
                    puntes.createOrderA3ERP(fila.Cells["NumPed"].Value.ToString());
                    tipoCargaProyectos();
                    dgvTareas.DataSource = puntes.loadTasks();
                    this.Refresh();
                }

            }
            catch (Exception ex) { }
        }

        private void btnUpdateFM_Click(object sender, EventArgs e)
        {
            
            DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"
            string scriptTasks = "UPDATE Tareas SET TA_IdLineaERP=999";
            DataTable dtTraspaso = new DataTable();

            OdbcCommand command = new OdbcCommand(scriptTasks, DbConnection);
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            DbConnection.Close();
        }

        private void rbtAll_CheckedChanged(object sender, EventArgs e)
        {
          if (rbtAll.Checked)  tipoCargaProyectos();
        }

        private void rbtPending_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtPending.Checked) tipoCargaProyectos();
        }

        private void rbtSync_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtSync.Checked)  tipoCargaProyectos();
        }

        private void tipoCargaProyectos()
        {
            tipoSync = rbtAll.Checked ? "ALL": tipoSync;
            tipoSync = rbtPending.Checked ? "PEND" : tipoSync;
            tipoSync = rbtSync.Checked ? "SYNC" : tipoSync;

            //dgvProyectos.DataSource = puntes.loadProjects(tipoSync);

            dgvProyectos.DataSource = puntes.loadOrders(tipoSync);


        }

        private void borrarSyncronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("numDoc");
                dt.Columns.Add("numProjectFM");

                foreach (DataGridViewRow row in dgvProyectos.SelectedRows)
                {
                    DataRow dr = dt.NewRow();
                    dr["numDoc"] = row.Cells["PR_IdPedidoERP"].Value.ToString();
                    dr["numProjectFM"] = row.Cells["PR_IdProyecto"].Value.ToString();
                    dt.Rows.Add(dr);
                }
                puntes.borrarSincronizacionDocumentos(dt);
                tipoCargaProyectos();
                dgvTareas.DataSource = puntes.loadTasks();

            }
            catch
            { }
        }

        private void resetearIDA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgvProyectos.SelectedRows)
                {
                    if (!string.IsNullOrEmpty(row.Cells["PR_IdPedidoERP"].Value.ToString()))
                    {
                        DbConnection.Open();   // catch con windowmessage "Puede que File Maker no este abierto"
                        string scriptTasks = "UPDATE PROYECTOS SET PR_IdPedidoERP=NULL WHERE PR_idPedidoERP=" + row.Cells["PR_IdPedidoERP"].Value.ToString();

                        OdbcCommand command = new OdbcCommand(scriptTasks, DbConnection);
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                        DbConnection.Close();
                        
                    }
                }
                tipoCargaProyectos();
            }
            catch
            {
                DbConnection.Close();
            }
        }

        private void btnLoadDataA3ERP_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            sql.abrirConexion();
            dgvProyectos.DataSource= sql.obtenerDatosSQLScript("SELECT * FROM FORMAPAG");
            sql.cerrarConexion();
        }

        private void btnOpenA3_Click(object sender, EventArgs e)
        {
            gestionarConexion(true);
        }

        private void gestionarConexion(bool abrir = true)
        {
            csa3erp a3erp = new klsync.csa3erp();
            if (abrir) { a3erp.abrirEnlace(); }
            else { a3erp.cerrarEnlace(); }


        }

        private void btnCloseConnection_Click(object sender, EventArgs e)
        {
            gestionarConexion(false);
        }

        private void btnCreateOrderTest_Click(object sender, EventArgs e)
        {
            Puntes.csPuntes puntes = new Puntes.csPuntes();
            puntes.pedidoTest();
        }

        private void btnEditOrder_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow fila in dgvProyectos.SelectedRows)
            {
                puntes.createOrderA3ERP(fila.Cells["PR_IdProyecto"].Value.ToString(), true, true);
                this.Refresh();
            }
        }

        private void nuevoPedidoEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (DataGridViewRow fila in dgvProyectos.SelectedRows)
                {
                    puntes.createOrderA3ERP(fila.Cells["PR_IdProyecto"].Value.ToString());
                    this.Refresh();
                }

                tipoCargaProyectos();
                dgvTareas.DataSource = puntes.loadTasks();
                this.Refresh();
            }
            catch { }
        }

        private void editarPedidoEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow fila in dgvProyectos.SelectedRows)
            {
                puntes.createOrderA3ERP(fila.Cells["PR_IdProyecto"].Value.ToString(), true, true);
                this.Refresh();
            }
        }

        private void frMainFormPuntes_Load(object sender, EventArgs e)
        {
            lblA3ERPDBValue.Text = csGlobal.databaseA3;
            lblA3ERPServerValue.Text = csGlobal.ServerA3;
            lblODBCValue.Text = "PUNTES";
            lblConnectionname.Text = csGlobal.conexionDB;
            lblSerieDocA3Value.Text = csGlobal.serie;

        }

        private void dgvProyectos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //string idProyecto = dgvProyectos.Rows[e.RowIndex].Cells["PR_IdProyecto"].Value.ToString();
                string idProyecto = dgvProyectos.Rows[e.RowIndex].Cells["NumPed"].Value.ToString();
                dgvTareas.DataSource= puntes.loadTasks(idProyecto);
                this.Refresh();
            }
            catch { }
        }
    }
}
