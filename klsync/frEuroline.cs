﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Windows.Documents;
//using RestSharp;

namespace klsync
{
    public partial class frEuroline : Form
    {
        private csSqlConnects sql = new csSqlConnects();
        private int selected_row;
        private bool all_selected = true;

        private static frEuroline m_FormDefInstance;
        public static frEuroline DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frEuroline();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frEuroline()
        {
            InitializeComponent();
            poblarComboAños();
            poblarComboTarifas();
            poblarComboSeries();
            poblarComboProveedores();
 

            comboOperacion.SelectedIndex = 0;
        }

        /// <summary>
        /// 1. Rellenamos combo tarifas
        /// Tabla: tarifas
        /// </summary>
        private void poblarComboTarifas()
        {
            csUtilidades.poblarCombo(toolStripComboBoxTarifa.ComboBox, sql.cargarDatosTablaA3("select desctarifa, tarifa from tarifas"), "DESCTARIFA", true);
        }

        private void poblarComboAños()
        {
            csUtilidades.poblarCombo(tsCBoxYear.ComboBox, sql.cargarDatosTablaA3("select year(fecha) as Año from CABEOFEV group by year(fecha) order by year(fecha) desc"), "AÑO", true);
        }

        /// <summary>
        /// 2. Rellenamos con un distinct de series
        /// Tabla: cabecera de ofertas de venta
        /// </summary>
        private void poblarComboSeries()
        {
            csUtilidades.poblarCombo(toolStripComboBoxSerie.ComboBox, sql.cargarDatosTablaA3("select distinct serie from cabeofev"), "serie", true);
        }

        private void poblarComboProveedores()
        {
            csUtilidades.poblarCombo(cboxProveedores, sql.cargarDatosTablaA3("SELECT (NOMPRO + ' |' + CODPRO ) as PROV FROM PROVEED ORDER BY CODPRO"), "PROV", true);
        }

        private void toolStripComboBoxSerie_SelectedIndexChanged(object sender, EventArgs e)
        {
            poblarComboOfertas();
        }

        /// <summary>
        /// En base al cambio de serie, rellenamos numdocs-combo con combinacion de tarifa y serie
        /// Tabla: cabecera de ofertas de venta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void poblarComboOfertas()
        {
            try
            {
                string query = "select CONVERT(FLOAT, numdoc) AS NUMDOC from cabeofev where serie = '" + toolStripComboBoxSerie.ComboBox.Text + "' AND YEAR(FECHA)=" + tsCBoxYear.Text + " ORDER BY NUMDOC ASC"; 
                string tarifa = sql.obtenerCampoTabla("select tarifa from tarifas where desctarifa = '" + toolStripComboBoxTarifa.ComboBox.Text + "'");
                csUtilidades.poblarCombo(toolStripComboBoxOferta.ComboBox, sql.cargarDatosTablaA3(query), "numdoc", true);

                if (toolStripComboBoxOferta.Items.Count > 0)
                {
                    //btnVerLineas.Enabled = true;
                }
                else
                {
                    //btnVerLineas.Enabled = false;
                    desactivarBotones();
                }
            }
            catch (Exception)
            {
            }
        }

        private void toolStripComboBoxTarifa_SelectedIndexChanged(object sender, EventArgs e)
        {
            poblarComboOfertas();
        }

        private void verLineas()
        {
            desactivarBotones();
            if (toolStripComboBoxOferta.ComboBox.Items.Count > 0)
            {
                
                string numdoc = Int32.Parse(toolStripComboBoxOferta.ComboBox.Text).ToString();
                string idofv = sql.obtenerCampoTabla("SELECT CONVERT(FLOAT,IDOFEV) AS IDOFEV FROM CABEOFEV WHERE NUMDOC = '" + numdoc + "' and serie = '" + toolStripComboBoxSerie.ComboBox.Text + "'");

                activarBotones();
                informarDatosCabecera(idofv);
                string query = "SELECT " +
                    " ltrim(CODART) as CODART, CAP, LINEOFER.PARAM1 AS POS, DESCLIN, CONVERT(FLOAT, IDOFEV) as IDOFEV, NUMLINOFE, ORDLIN, UNIDADES, " +
                    " CONVERT(FLOAT, PRCMONEDA) as PRCMONEDA, CONVERT(FLOAT, PRECIO2) AS PRECIO2, CONVERT(FLOAT, PRECIO3) AS PRECIO3, "  +
                    " CONVERT(FLOAT,BASE) AS BASE, convert(FLOAT,PRCMEDIO) AS COSTE_UNITARIO, convert(FLOAT,PRCMEDIO)*UNIDADES AS COSTE_LINEA, " +
                    " (CONVERT(FLOAT,BASE)-convert(FLOAT,PRCMEDIO)*UNIDADES) AS MARGEN, " +
                    " DTO1_PROV,DTO2_PROV, COSTE_ORIGEN , KLSCODIPROV , NOMPRO," +
                    " OBTPRCCOSTE, SITUACION, IMPLIN " +
                    " FROM dbo.LINEOFER " +
                    " LEFT OUTER JOIN dbo.PROVEED ON dbo.LINEOFER.KLSCODIPROV = dbo.PROVEED.CODPRO " +
                    " where idofev = '" + idofv + "' order by ordlin asc";
                csUtilidades.addDataSource(dgv, sql.cargarDatosTablaA3(query));

                pintarDeRojo(dgv);

                this.dgv.Columns["COSTE_UNITARIO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["COSTE_LINEA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["PRCMONEDA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["PRECIO2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["PRECIO3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["BASE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgv.Columns["IDOFEV"].Visible = false;
                this.dgv.Columns["NUMLINOFE"].Visible = false;
                this.dgv.Columns["ORDLIN"].Visible = false;

                formatearDGVGeneral();
                loadColumns();
                visualizacionColumnasDGV();
            }
        }

        private void pintarDeRojo(DataGridView dgv)
        {
            if (dgv.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.Cells["SITUACION"].Value.ToString() == "S")
                    {
                        row.DefaultCellStyle.BackColor = Color.OrangeRed;
                    }
                    if (Convert.ToDouble(row.Cells["MARGEN"].Value.ToString()) < 0)
                    {
                        row.DefaultCellStyle.ForeColor = Color.OrangeRed;
                    }
                }
            }
        }

        private void activarBotones()
        {
            btnCopiarPrecios.Enabled = true;
            btnCambiarPrecio1.Enabled = true;
            btnCambiarPrecio2.Enabled = true;
            comboBoxPrecios.Enabled = true;
            txtMargenPrecio1.Enabled = true;
            txtMargenPrecio2.Enabled = true;
            btnCambiarPVP.Enabled = true;
        }

        private void desactivarBotones()
        {
            //lblOferta.Text = "Oferta: -";
            //lblCodCli.Text = "Código cliente: ";
            //lblNomCli.Text = "Nombre cliente: -";
            //lblReferencia.Text = "Referencia: -";
            //lblEstado.Text = "Estado: -";
            //lblSituacion.Text = "Situación: -";

            dgv.DataSource = null;
            comboBoxPrecios.Enabled = false;
            btnCopiarPrecios.Enabled = false;
            btnCambiarPrecio1.Enabled = false;
            btnCambiarPrecio2.Enabled = false;
            txtMargenPrecio1.Enabled = false;
            txtMargenPrecio2.Enabled = false;
            btnCambiarPVP.Enabled = false;
        }

        private void informarDatosCabecera(string idofv)
        {
            DataTable cabecera = sql.cargarDatosTablaA3("select estado, situacion, referencia, ltrim(codcli) as codcli, nomcli, CONVERT(FLOAT, numdoc) as numdoc,impprec ImprimirPrecios, convert(int, idofev) as IdOferta from CABEOFEV where IDOFEV = '" + idofv + "'");

            if (csUtilidades.verificarDt(cabecera))
            {
                string estado = "", situacion = "";

                switch (cabecera.Rows[0]["estado"].ToString())
                {
                    case "PA":
                        estado = "Pendiente aceptar";
                        break;
                    case "AC":
                        estado = "Aceptado";
                        break;
                    case "RE":
                        estado = "Rechazado";
                        break;
                }

                switch (cabecera.Rows[0]["situacion"].ToString())
                {
                    case "S":
                        situacion = "Servido";
                        break;
                    case "A":
                        situacion = "Pendiente";
                        break;
                }


                lblOferta.Text = "Oferta: " + cabecera.Rows[0]["NUMDOC"].ToString(); 
                lblCodCli.Text= "Código cliente: " + cabecera.Rows[0]["CODCLI"].ToString(); 
                lblNomCli.Text = "Nombre cliente: " + cabecera.Rows[0]["NOMCLI"].ToString(); 
                lblReferencia.Text = "Referencia: " + cabecera.Rows[0]["REFERENCIA"].ToString(); 
                lblEstado.Text = "Estado: " + estado;
                lblSituacion.Text = "Situacion: " + situacion;
                lblIdOferta.Text = cabecera.Rows[0]["IdOferta"].ToString();
                if (cabecera.Rows[0]["ImprimirPrecios"].ToString() == "T")
                {
                    rbPrintPreciosSI.Checked = true;
                }
                else
                {
                    rbPrintPreciosSI.Checked = false;
                }


            }
        }

        private void btnVerLineas_Click(object sender, EventArgs e)
        {
            verLineas();
        }

        private void btnCambiarPrecio1_Click(object sender, EventArgs e)
        {
            if (btnCambiarPrecio1.Text.Trim() != "")
            {
                btnCambiarPrecio1.Enabled = false;
                string nuevoprecio = txtMargenPrecio1.Text.Trim();
                cambiarPrecio("PRCMONEDA", nuevoprecio);
                btnCambiarPrecio1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Informa el campo del margen a aplicar");
            }

        }

        private void cambiarPrecio(string preciotabla, string nuevoprecio)
        {
            selected_row = dgv.SelectedRows[0].Index;
            float value;

            try
            {
                if (float.TryParse(nuevoprecio, out value))
                {
                    if (float.Parse(nuevoprecio) >= 0 && float.Parse(nuevoprecio) <= 100)
                    {
                        csa3erp a3erp = new csa3erp();
                        a3erp.abrirEnlace();

                        a3ERPActiveX.Oferta ofe = new a3ERPActiveX.Oferta();
                        ofe.Iniciar();

                        if (all_selected)
                        {
                            foreach (DataGridViewRow item in dgv.Rows)
                            {
                                string precio = item.Cells[preciotabla].Value.ToString();
                                string coste = item.Cells["COSTE_UNITARIO"].Value.ToString().Trim();
                                string codart = item.Cells["CODART"].Value.ToString().Trim();

                                if (codart != "CAP" && codart != "KIT")
                                {
                                    if (coste != "0")
                                    {
                                        string ordlin = item.Cells["ORDLIN"].Value.ToString().Trim();
                                        string idofev = item.Cells["IDOFEV"].Value.ToString().Trim();
                                        var dividendo = (100 - float.Parse(nuevoprecio)) / 100;
                                        var total = float.Parse(coste) / dividendo;

                                        decimal lin = decimal.Parse(item.Cells["NUMLINOFE"].Value.ToString());
                                        decimal of = decimal.Parse(item.Cells["IDOFEV"].Value.ToString());

                                        ofe.Modifica(of, false);
                                        ofe.EditarLinea(lin);
                                        ofe.AsFloatLin[preciotabla] = float.Parse(total.ToString());
                                        ofe.AnadirLinea();
                                        ofe.Anade();
                                    }
                                }
                                
                            }
                        }
                        else
                        {
                            foreach (DataGridViewRow item in dgv.SelectedRows)
                            {
                                string precio = item.Cells[preciotabla].Value.ToString();
                                string coste = item.Cells["COSTE"].Value.ToString().Trim();
                                string codart = item.Cells["CODART"].Value.ToString().Trim();

                                if (codart != "CAP" && codart != "KIT")
                                {
                                    string ordlin = item.Cells["ORDLIN"].Value.ToString().Trim();
                                    string idofev = item.Cells["IDOFEV"].Value.ToString().Trim();
                                    var dividendo = (100 - float.Parse(nuevoprecio)) / 100;
                                    var total = float.Parse(coste) / dividendo;

                                    decimal lin = decimal.Parse(item.Cells["NUMLINOFE"].Value.ToString());
                                    decimal of = decimal.Parse(item.Cells["IDOFEV"].Value.ToString());

                                    ofe.Modifica(of, false);
                                    ofe.EditarLinea(lin);
                                    ofe.AsFloatLin[preciotabla] = float.Parse(total.ToString());
                                    ofe.AnadirLinea();
                                    ofe.Anade();
                                }
                            }
                        }

                        ofe.Acabar();
                        a3erp.cerrarEnlace();

                        ////////////////////////////

                        verLineas();
                        dgv.ClearSelection();
                        dgv.Rows[selected_row].Selected = true;

                        MessageBox.Show("El precio se ha cambiado correctamente");
                    }
                    else
                    {
                        MessageBox.Show("El valor debe ser entre 0 y 100");
                    }
                }
                else
                {
                    MessageBox.Show("El valor debe ser numérico y entre 0 y 100");
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception) { }
        }


        private void toolStripComboBoxOferta_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dgv.Rows.Count > 0)
            {
                activarBotones();
            }
            else
            {
                desactivarBotones();
            }
        }

        private void btnCambiarPrecio2_Click(object sender, EventArgs e)
        {
            if (btnCambiarPrecio2.Text.Trim() != "")
            {
                btnCambiarPrecio1.Enabled = false;
                string nuevoprecio = txtMargenPrecio2.Text.Trim();
                cambiarPrecio("PRECIO2", nuevoprecio);
                btnCambiarPrecio1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Informa el campo del margen a aplicar");
            }

        }

        private void btnCopiarPrecios_Click(object sender, EventArgs e)
        {
            try
            {
                var sw = System.Diagnostics.Stopwatch.StartNew();
                Console.WriteLine("Copiar precios empezando: " + sw.Elapsed.Milliseconds.ToString());

                btnCopiarPrecios.Enabled = false;
                selected_row = dgv.SelectedRows[0].Index;

                if (comboBoxPrecios.Text != "")
                {
                    string precio1 = "";
                    string precio2 = "";
                    string precio3 = "";
                    string idofev = dgv.SelectedRows[0].Cells["IDOFEV"].Value.ToString().Trim();
                    
                    //Console.WriteLine("A3ERP abre enlace: " + sw.Elapsed.Milliseconds.ToString());

                    copiarPreciosIntermediate();


                    verLineas();
                    mensaje("Operacion completada");
                    btnCopiarPrecios.Enabled = true;

                    Console.WriteLine("Copiar precios terminado: " + sw.Elapsed.Milliseconds.ToString());
                    sw.Stop();
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                btnCopiarPrecios.Enabled = true;
            }
        }

        private void copiarPreciosIntermediate()
        {
            DataTable dtDocuLines = new DataTable();
            dtDocuLines.Columns.Add("IDLINE");
            dtDocuLines.Columns.Add("FIELD_TO_UPDATE");
            dtDocuLines.Columns.Add("VALOR_DESTINO");
            string idofev="";

            foreach (DataGridViewRow fila in dgv.Rows)
            {
                if (fila.Cells["CODART"].Value.ToString() != "KIT" && fila.Cells["CODART"].Value.ToString() != "CAP")
                {
                    DataRow linDoc = dtDocuLines.NewRow();


                    string combo = comboBoxPrecios.Text;
                    string lin = fila.Cells["NUMLINOFE"].Value.ToString();
                    linDoc["IDLINE"] = fila.Cells["NUMLINOFE"].Value.ToString();
                    string precio1 = fila.Cells["PRCMONEDA"].Value.ToString();
                    string precio2 = fila.Cells["PRECIO2"].Value.ToString();
                    string precio3 = fila.Cells["PRECIO3"].Value.ToString();
                    idofev = dgv.SelectedRows[0].Cells["IDOFEV"].Value.ToString().Trim();

                    switch (combo)
                    {
                        case "1 > 3":
                            linDoc["FIELD_TO_UPDATE"] = "PRECIO3";
                            linDoc["VALOR_DESTINO"] = precio1;
                            break;
                        case "2 > 3":
                            linDoc["FIELD_TO_UPDATE"] = "PRECIO3";
                            linDoc["VALOR_DESTINO"] = precio2;
                            break;
                        case "2 > 1":
                            linDoc["FIELD_TO_UPDATE"] = "PRCMONEDA";
                            linDoc["VALOR_DESTINO"] = precio2;    
                            break;
                        case "3 > 1":
                            linDoc["FIELD_TO_UPDATE"] = "PRCMONEDA";
                            linDoc["VALOR_DESTINO"] = precio3;  
                            break;
                        case "3 > 2":
                            linDoc["FIELD_TO_UPDATE"] = "PRECIO2";
                            linDoc["VALOR_DESTINO"] = precio3;  
                            break;
                        default:
                            break;
                    }
                    dtDocuLines.Rows.Add(linDoc);

                }
            }
            actualizarPreciosA3ERP(idofev, dtDocuLines);

        }



        private void mensaje(string texto)
        {
            MessageBox.Show(texto);
        }


        private void actualizarPreciosA3ERP(string idDocumento, DataTable lineasDoc)
        {
            try
            {
                string idLinea = "";
                string valorDestino = "";
                string campoToUpdate = "";
                int contadorLineas = 1;
                
                csa3erp a3erp = new csa3erp();
                
                a3erp.abrirEnlace();

                a3ERPActiveX.Oferta of = new a3ERPActiveX.Oferta();
                of.Iniciar();
                of.Modifica(decimal.Parse(idDocumento), false);

                foreach (DataRow linea in lineasDoc.Rows)
                {
                    idLinea = linea["IDLINE"].ToString();
                    valorDestino = linea["VALOR_DESTINO"].ToString();
                    campoToUpdate = linea["FIELD_TO_UPDATE"].ToString();


                    of.EditarLinea(decimal.Parse(idLinea));
                    if (campoToUpdate == "KLSCODIPROV")
                    {
                        of.AsStringLin[campoToUpdate] = (valorDestino);
                    }
                    else
                    {
                        of.AsFloatLin[campoToUpdate] = float.Parse(valorDestino);
                    }
                    of.AnadirLinea();

                   // of.Anade();
                   
                    tssInfoProceso.Text = "Procesando Linea " + contadorLineas + " de " + lineasDoc.Rows.Count.ToString();
                    statusStripBottom.Refresh();
                    contadorLineas++;
                }
                of.Anade();
                of.Acabar();
                a3erp.cerrarEnlace();
                verLineas();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                btnCopiarPrecios.Enabled = true;
            }
        
        }

        private void btnCambiarPVP_Click(object sender, EventArgs e)
        {
            btnCambiarPVP.Enabled = false;
            selected_row = dgv.SelectedRows[0].Index;
            csPrecios precios = new csPrecios();

            try
            {
                ///////////////////////////////

                csa3erp a3erp = new csa3erp();
                a3erp.abrirEnlace();

                a3ERPActiveX.Oferta of = new a3ERPActiveX.Oferta();
                of.Iniciar();

                if (all_selected)
                {
                    foreach (DataGridViewRow item in dgv.Rows)
                    {
                        
                       

                        string codart = item.Cells["CODART"].Value.ToString();
                        if (codart != "CAP" && codart != "KIT")
                        {
                            string prcventa = sql.obtenerCampoTabla("select precio from tarifave where ltrim(codart) = '" + codart + "' and tarifa = 1");
                            decimal lin = decimal.Parse(item.Cells["NUMLINOFE"].Value.ToString());
                            decimal idofev = decimal.Parse(item.Cells["IDOFEV"].Value.ToString());

                            of.Modifica(idofev, false);
                            of.EditarLinea(lin);
                            of.AsFloatLin["PRCMONEDA"] = float.Parse(prcventa);
                            of.AnadirLinea();
                            of.Anade();
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewRow item in dgv.SelectedRows)
                    {
                        string codart = item.Cells["CODART"].Value.ToString();
                        if (codart != "CAP" && codart != "KIT")
                        {
                            string prcventa = sql.obtenerCampoTabla("select precio from tarifave where ltrim(codart) = '"+ codart + "' and tarifa = 1");
                            decimal lin = decimal.Parse(item.Cells["NUMLINOFE"].Value.ToString());
                            decimal idofev = decimal.Parse(item.Cells["IDOFEV"].Value.ToString());

                            of.Modifica(idofev, false);
                            of.EditarLinea(lin);
                            of.AsFloatLin["PRCMONEDA"] = float.Parse(prcventa);
                            of.AnadirLinea();
                            of.Anade();
                        }
                    }
                }

                of.Acabar();
                a3erp.cerrarEnlace();

                ////////////////////////////

                verLineas();
                dgv.ClearSelection();
                dgv.Rows[selected_row].Selected = true;

                MessageBox.Show("El precio del articulo se ha cambiado correctamente");
                btnCambiarPVP.Enabled = true;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
                btnCambiarPVP.Enabled = true;
            }
            catch (Exception) { }
        }



        private void comboOperacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboOperacion.SelectedIndex == 1)
                all_selected = false;
            else
                all_selected = true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            verLineas();
            formatearDGVGeneral();
        }

        
        //Función para buscar el siguiente capítulo 
        private string[] obtenerNextCapitulo(int idLineaRef,DataTable dtLineasDoc)
        {
            string capituloIni="";
            int idLineaCapitulo = 0;
            int lineaRef = 0;
            string capitulo = "";
            string articulo = "";
            string[] array = new string[2];
            idLineaCapitulo = dtLineasDoc.Rows.Count - 1 - idLineaRef;

            for (int i = dtLineasDoc.Rows.Count-1; i >= 0; i--)
            {
                if (i <= idLineaCapitulo)
                {
                    articulo = dtLineasDoc.Rows[i]["ARTICULO"].ToString();
                    capitulo = dtLineasDoc.Rows[i]["CAPITULO"].ToString();
                    if (articulo == "CAP")
                    {
                        array[0] = dtLineasDoc.Rows[i]["CAPITULO"].ToString();
                        lineaRef = dtLineasDoc.Rows.Count - 1 - i;
                        array[1] = lineaRef.ToString();
                        break;
                    }
                }
            }
            return array;
        }

        private void cargarInfoDocumento()
        {
            csSqlConnects sqlConnect=new csSqlConnects();
            decimal idofev = decimal.Parse(dgv.Rows[0].Cells["IDOFEV"].Value.ToString());
            DataTable dtLineasDoc = new DataTable();
            DataTable dtInfoDoc = new DataTable();
            int idLineaCapitulo=0;
            string[] arrayCapitulos=new string[2];
            double margen = 0;

            double totalCapitulo = 0;
            double totalPosicion = 0;
            double totalCosteCapitulo = 0;
            double totalCostePosicion = 0;
            string capitulo="";
            string posicion="";
            string articulo = "";
            string capituloIni="";

            dtLineasDoc = sqlConnect.obtenerDatosSQLScript("SELECT CAP AS CAPITULO, PARAM1 AS POSICION,  LTRIM(CODART) AS ARTICULO,DESCLIN,UNIDADES,BASE,ROUND((PRCMEDIO*UNIDADES),2) AS COSTE FROM LINEOFER WHERE IDOFEV=" + idofev + " ORDER BY ORDLIN ASC");
            dtInfoDoc.Columns.Add("CAPITULO");
            dtInfoDoc.Columns.Add("POSICION");
            dtInfoDoc.Columns.Add("NOMBRE");
            dtInfoDoc.Columns.Add("TOTAL_CAPITULO");
            dtInfoDoc.Columns["TOTAL_CAPITULO"].DataType = System.Type.GetType("System.Decimal");
            dtInfoDoc.Columns.Add("TOTAL_COSTE_CAPITULO");
            dtInfoDoc.Columns["TOTAL_COSTE_CAPITULO"].DataType = System.Type.GetType("System.Decimal");

            dtInfoDoc.Columns.Add("TOTAL_POSICION");
            dtInfoDoc.Columns["TOTAL_POSICION"].DataType = System.Type.GetType("System.Decimal");

            dtInfoDoc.Columns.Add("TOTAL_COSTE_POSICION");
            dtInfoDoc.Columns["TOTAL_COSTE_POSICION"].DataType = System.Type.GetType("System.Decimal");

            dtInfoDoc.Columns.Add("MARGEN");
            dtInfoDoc.Columns["MARGEN"].DataType = System.Type.GetType("System.Decimal");
            
            dtInfoDoc.Columns.Add("%MARGEN");

          

            //Busco el primer capítulo por el final para comenzar luego con el capítulo informado
            arrayCapitulos = obtenerNextCapitulo(idLineaCapitulo, dtLineasDoc);
            capituloIni = arrayCapitulos[0];
            idLineaCapitulo = Convert.ToInt16(arrayCapitulos[1]);

            //Comienzo desde el final
            for (int i = dtLineasDoc.Rows.Count-1; i >= 0; i--)
            {
                articulo = dtLineasDoc.Rows[i]["ARTICULO"].ToString();

                DataRow filaInfoDoc = dtInfoDoc.NewRow();

                if (capitulo=="")
                {
                    capitulo=capituloIni;
                }
                if ( dtLineasDoc.Rows[i]["CAPITULO"].ToString()!="")
                {
                    capitulo=dtLineasDoc.Rows[i]["CAPITULO"].ToString();
                }

                if (articulo != "CAP" && articulo != "KIT")
                {
                    totalCapitulo = totalCapitulo + Convert.ToDouble(dtLineasDoc.Rows[i]["BASE"].ToString());
                    totalPosicion = totalPosicion + Convert.ToDouble(dtLineasDoc.Rows[i]["BASE"].ToString());
                    totalCosteCapitulo = totalCosteCapitulo + Convert.ToDouble(dtLineasDoc.Rows[i]["COSTE"].ToString());
                    totalCostePosicion = totalCostePosicion + Convert.ToDouble(dtLineasDoc.Rows[i]["COSTE"].ToString());
                }

                if (articulo == "KIT")
                {
                    filaInfoDoc["CAPITULO"] = capitulo;
                    filaInfoDoc["NOMBRE"] = dtLineasDoc.Rows[i]["DESCLIN"].ToString();
                    filaInfoDoc["POSICION"] = dtLineasDoc.Rows[i]["POSICION"].ToString();
                    filaInfoDoc["TOTAL_CAPITULO"] = "0";
                    filaInfoDoc["TOTAL_POSICION"] = totalPosicion;
                    filaInfoDoc["TOTAL_COSTE_POSICION"] = totalCostePosicion;
                    filaInfoDoc["MARGEN"] = (totalPosicion - totalCostePosicion);
                    filaInfoDoc["%MARGEN"] = "";

                    dtInfoDoc.Rows.Add(filaInfoDoc);
                    totalPosicion = 0;
                    totalCostePosicion = 0;
                }

                if (articulo == "CAP")
                {
                    filaInfoDoc["CAPITULO"] = capitulo;
                    filaInfoDoc["NOMBRE"] = dtLineasDoc.Rows[i]["DESCLIN"].ToString();
                    filaInfoDoc["POSICION"] = "";
                    filaInfoDoc["TOTAL_CAPITULO"] = totalCapitulo*1;
                    filaInfoDoc["TOTAL_POSICION"] = "0";
                    filaInfoDoc["TOTAL_COSTE_CAPITULO"] = totalCosteCapitulo;
                    margen = (totalCapitulo - totalCosteCapitulo);
                    filaInfoDoc["MARGEN"] = Math.Round(margen,2);
                    filaInfoDoc["%MARGEN"] = "";

                    dtInfoDoc.Rows.Add(filaInfoDoc);
                    totalCapitulo = 0;
                    totalPosicion = 0;
                    totalCosteCapitulo = 0;
                    totalCostePosicion = 0;

                    //Busco el siguiente capítulo
                    arrayCapitulos = obtenerNextCapitulo(idLineaCapitulo + 1, dtLineasDoc);
                    capitulo = arrayCapitulos[0];
                    idLineaCapitulo = Convert.ToInt16(arrayCapitulos[1]);
                }
            }
            DataView dv = dtInfoDoc.DefaultView;
            dv.Sort = "CAPITULO asc, POSICION asc";
            DataTable sortedDT = dv.ToTable();
            
            dgvInfoDoc.DataSource = sortedDT;
            formatearDGVInfoDocs();
            
        }




        private void formatearDGVInfoDocs()
        {

            //ALINEACIÓN DEL DATAGRID
            this.dgvInfoDoc.Columns["TOTAL_CAPITULO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInfoDoc.Columns["TOTAL_POSICION"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInfoDoc.Columns["TOTAL_COSTE_CAPITULO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInfoDoc.Columns["TOTAL_COSTE_POSICION"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvInfoDoc.Columns["MARGEN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //FORMATEO NUMÉRICO DEL DATAGRID
            this.dgvInfoDoc.Columns["TOTAL_CAPITULO"].DefaultCellStyle.Format = "N2";
            this.dgvInfoDoc.Columns["TOTAL_POSICION"].DefaultCellStyle.Format = "N2";
            this.dgvInfoDoc.Columns["TOTAL_COSTE_CAPITULO"].DefaultCellStyle.Format = "N2";
            this.dgvInfoDoc.Columns["TOTAL_COSTE_POSICION"].DefaultCellStyle.Format = "N2";
            this.dgvInfoDoc.Columns["MARGEN"].DefaultCellStyle.Format = "N2";
            dgvInfoDoc.Refresh();
        
        }

        private void formatearDGVproveedores()
        {
            //ALINEACIÓN DEL DATAGRID
            this.dgvProveedores.Columns["COSTE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvProveedores.Columns["BASE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvProveedores.Columns["MARGEN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvProveedores.Columns["PESO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvProveedores.Columns["TOTALDOC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //FORMATEO NUMÉRICO DEL DATAGRID
            this.dgvProveedores.Columns["COSTE"].DefaultCellStyle.Format = "N2";
            this.dgvProveedores.Columns["MARGEN"].DefaultCellStyle.Format = "N2";
            this.dgvProveedores.Columns["PESO"].DefaultCellStyle.Format = "P2";
            this.dgvProveedores.Columns["BASE"].DefaultCellStyle.Format = "N2";
            this.dgvProveedores.Columns["TOTALDOC"].DefaultCellStyle.Format = "N2";
            dgvInfoDoc.Refresh();
        }

        private void formatearDGVGeneral()
        {
            //ALINEACIÓN DEL DATAGRID
            this.dgv.Columns["COSTE_UNITARIO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["COSTE_LINEA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["PRCMONEDA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["PRECIO2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["PRECIO3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["BASE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["MARGEN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["DTO1_PROV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["DTO2_PROV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgv.Columns["COSTE_ORIGEN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            //FORMATEO NUMÉRICO DEL DATAGRID
            this.dgv.Columns["COSTE_UNITARIO"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["COSTE_LINEA"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["PRCMONEDA"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["PRECIO2"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["PRECIO3"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["BASE"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["MARGEN"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["DTO1_PROV"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["DTO2_PROV"].DefaultCellStyle.Format = "N2";
            this.dgv.Columns["COSTE_ORIGEN"].DefaultCellStyle.Format = "N2";
            dgvInfoDoc.Refresh();
        
        }

        private void tsCBoxYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            poblarComboOfertas();
        }


        private void btAsignLineas_Click(object sender, EventArgs e)
        {
            cargarInfoDocumento();
            
        }

      
        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
          if (dgv.Columns[e.ColumnIndex].Name == "DTO1_PROV" || dgv.Columns[e.ColumnIndex].Name == "DTO2_PROV" || dgv.Columns[e.ColumnIndex].Name == "PRCMONEDA" || dgv.Columns[e.ColumnIndex].Name == "COSTE_UNITARIO")
            {
                double dto1 = 0;
                double dto2 = 0;

              
                double precio = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["PRCMONEDA"].Value.ToString());
                double precioCoste = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_UNITARIO"].Value.ToString());
                double precoCosteOrigen = string.IsNullOrEmpty(dgv.Rows[e.RowIndex].Cells["COSTE_ORIGEN"].Value.ToString()) ?0: Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_ORIGEN"].Value.ToString());

                string idLin = dgv.Rows[e.RowIndex].Cells["NUMLINOFE"].Value.ToString();
                string idOfev = dgv.Rows[e.RowIndex].Cells["IDOFEV"].Value.ToString();
                double prcMedio = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_UNITARIO"].Value.ToString());
                double nuevoCoste = 0;

                if (dgv.Rows[e.RowIndex].Cells["DTO1_PROV"].Value.ToString() != "" || dgv.Rows[e.RowIndex].Cells["DTO2_PROV"].Value.ToString() != "0")
                {
                    dto1 = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["DTO1_PROV"].Value.ToString());
                    if (precoCosteOrigen != 0)
                    {
                        prcMedio = precoCosteOrigen;
                    }
                }

                if (dgv.Rows[e.RowIndex].Cells["DTO2_PROV"].Value.ToString() != "" && dgv.Rows[e.RowIndex].Cells["DTO2_PROV"].Value.ToString() != "0")
                {
                    dto2 = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["DTO2_PROV"].Value.ToString());
                    if (precoCosteOrigen != 0)
                    {
                        prcMedio = precoCosteOrigen;
                    }
                }

                if (dgv.Columns[e.ColumnIndex].Name == "DTO2_PROV")
                {
                    prcMedio = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_ORIGEN"].Value.ToString());
                    nuevoCoste = (prcMedio * (1 - (dto1 / 100))) * (1 - (dto2 / 100));
                    nuevoCoste = Math.Round(nuevoCoste, 2);
                }
                else if (dgv.Columns[e.ColumnIndex].Name == "DTO1_PROV")
                {
                    if (precoCosteOrigen != 0)
                    {
                        prcMedio = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_ORIGEN"].Value.ToString());
                        nuevoCoste = (prcMedio * (1 - (dto1 / 100))) * (1 - (dto2 / 100));
                        nuevoCoste = Math.Round(nuevoCoste, 2);
                    }
                    else
                    {
                        nuevoCoste = (prcMedio * (1 - (dto1 / 100))) * (1 - (dto2 / 100));
                        nuevoCoste = Math.Round(nuevoCoste, 2);
                    }
                }
                else
                {
                    prcMedio = Convert.ToDouble(dgv.Rows[e.RowIndex].Cells["COSTE_ORIGEN"].Value.ToString());
                    nuevoCoste = (prcMedio * (1 - (dto1 / 100))) * (1 - (dto2 / 100));
                    nuevoCoste = Math.Round(nuevoCoste, 2);
                }

                DataTable dtDocuLines = new DataTable();
                dtDocuLines.Columns.Add("IDLINE");
                dtDocuLines.Columns.Add("FIELD_TO_UPDATE");
                dtDocuLines.Columns.Add("VALOR_DESTINO");

                dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "DTO1_PROV", dto1));

                if (dgv.Columns[e.ColumnIndex].Name == "PRCMONEDA")
                {
                    dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "PRCMONEDA", precio));
                }
                if (dgv.Columns[e.ColumnIndex].Name == "COSTE_UNITARIO")
                {
                    dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "PRCMEDIO", precioCoste));
                }
                else
                {
                    if (precoCosteOrigen != 0)
                    {
                        dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines,idLin,"COSTE_ORIGEN",prcMedio));
                    }
                    dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "PRCMEDIO", nuevoCoste));
                }


                if (nuevoCoste != precoCosteOrigen && precoCosteOrigen == 0)
                {
                    dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "COSTE_ORIGEN", prcMedio));
                }

                dtDocuLines.Rows.Add(lineaToUpdate(dtDocuLines, idLin, "DTO2_PROV", dto2));
              
                actualizarPreciosA3ERP(idOfev, dtDocuLines);
            }
            else
            {
                verLineas();
            }
        }

        private DataRow lineaToUpdate(DataTable dt, string idLine, string fielToUpdate, double  destinyValue)
        {
                DataRow linDoc = dt.NewRow();
                linDoc = dt.NewRow();
                linDoc["IDLINE"] = idLine;
                linDoc["FIELD_TO_UPDATE"] = fielToUpdate;
                linDoc["VALOR_DESTINO"] = destinyValue;
                return linDoc;
        }

        private void btAsignarProveedor_Click(object sender, EventArgs e)
        {
            asignarProveedor();
        }

        private void asignarProveedor()
        {
            string idLin="";
            string idOfev="";
            string codPro = "";
            string[] tokens = cboxProveedores.Text.Split('|');
            codPro = tokens[1];

            DataTable dtDocuLines = new DataTable();
            dtDocuLines.Columns.Add("IDLINE");
            dtDocuLines.Columns.Add("FIELD_TO_UPDATE");
            dtDocuLines.Columns.Add("VALOR_DESTINO");

            foreach (DataGridViewRow fila in dgv.SelectedRows)
            {
                idLin = fila.Cells["NUMLINOFE"].Value.ToString();
                idOfev = fila.Cells["IDOFEV"].Value.ToString();
                DataRow linDoc = dtDocuLines.NewRow();
                linDoc["IDLINE"] = idLin;
                linDoc["FIELD_TO_UPDATE"] = "KLSCODIPROV";
                linDoc["VALOR_DESTINO"] = codPro;
                dtDocuLines.Rows.Add(linDoc);
            }
            actualizarPreciosA3ERP(idOfev, dtDocuLines);
        }

        private void btnResumenProveedores_Click(object sender, EventArgs e)
        {
            cargarResumenProveedores();
            formatearDGVproveedores();
        }

        private void cargarResumenProveedores()
        {
            string idOfev = dgv.Rows[0].Cells["IDOFEV"].Value.ToString();
            string query = "";
            query= "SELECT " +
                " dbo.LINEOFER.KLSCODIPROV AS PROVEEDOR, dbo.PROVEED.NOMPRO, SUM(BASE) AS BASE, SUM(PRCMEDIO * UNIDADES) AS COSTE, SUM(BASE) - SUM(PRCMEDIO * UNIDADES) AS MARGEN, " +
                " SUM(PRCMEDIO * UNIDADES) / (SELECT BASE FROM dbo.CABEOFEV WHERE (IDOFEV = " + idOfev + ")) AS PESO, " +
                " (SELECT BASE FROM dbo.CABEOFEV AS CABEOFEV_1 " +
                " WHERE (IDOFEV = " + idOfev + ")) AS TOTALDOC " + 
                " FROM dbo.LINEOFER " +
                " LEFT OUTER JOIN dbo.PROVEED ON dbo.LINEOFER.KLSCODIPROV = dbo.PROVEED.CODPRO " +
                " WHERE (IDOFEV = " + idOfev + ") " +
                " GROUP BY KLSCODIPROV, dbo.PROVEED.NOMPRO ";
            csUtilidades.addDataSource(dgvProveedores, sql.cargarDatosTablaA3(query));
        
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            ocultarPanelBusqueda();


        }

        private void ocultarPanelBusqueda()
        {
            splitContainer1.SplitterDistance = 70;
            tabControl1.SelectedIndex = 0;
        }

        private void mostrarPanelBusqueda()
        {
            splitContainer1.SplitterDistance = 300;
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            mostrarPanelBusqueda();
            tabControl1.SelectedIndex = 1;
        }

        private void frEuroline_Load(object sender, EventArgs e)
        {
            //Inicializo el tamñao de la barra de buscador
            splitContainer1.SplitterDistance = 70;
            splitContainer1.FixedPanel = FixedPanel.Panel1;
            //al estar incluido en un contenedor, hay que volver a indicar que se adapte
            splitContainer1.Dock = DockStyle.Fill;
        }

        private void btHide3_Click(object sender, EventArgs e)
        {
            ocultarPanelBusqueda();
        }

        private void btHide2_Click(object sender, EventArgs e)
        {
            ocultarPanelBusqueda();
        }

        private void btnServirDoc_Click(object sender, EventArgs e)
        {
            string numOfertaV = dgv.Rows[0].Cells["IDOFEV"].Value.ToString();
            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();
            a3erp.servirOfertaPorLineas("FI", toolStripComboBoxOferta.Text, numOfertaV);
            a3erp.cerrarEnlace();
        }

        private void rbPrintPreciosSI_CheckedChanged(object sender, EventArgs e)
        {
            actualizarImpresionPreciosDoc(lblIdOferta.Text);
        }

        private void actualizarImpresionPreciosDoc(string idDoc)
        {
            
            DialogResult Resultado;
            Resultado = MessageBox.Show("¿Quiere actualizar el valor en el documento?", "Actualizar Imprimir Precios", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (Resultado==DialogResult.Yes)
            {
                string imprimir = "";

                if (rbPrintPreciosSI.Checked)
                {
                    imprimir = "T";
                }
                if (rbPrintPreciosNO.Checked)
                {
                    imprimir = "F";
                }

                csUtilidades.ejecutarConsulta("UPDATE CABEOFEV SET IMPPREC='" + imprimir + "' WHERE IDOFEV=" + idDoc, false);
            }
        
        }

        private void rbPrintPreciosNO_CheckedChanged(object sender, EventArgs e)
        {
            //actualizarImpresionPreciosDoc(lblIdOferta.Text);
        }

        private void btAprobar_Click(object sender, EventArgs e)
        {
            csUtilidades.ejecutarConsulta("UPDATE CABEOFEV SET ESTADO = 'AC' WHERE IDOFEV=" + lblIdOferta.Text, false);
            lblEstado.Text = "Estado: Aceptada";
        }

        private void btnPendiente_Click(object sender, EventArgs e)
        {
            csUtilidades.ejecutarConsulta("UPDATE CABEOFEV SET ESTADO = 'PA' WHERE IDOFEV=" + lblIdOferta.Text, false);
            lblEstado.Text = "Estado: Pendiente Aceptar";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loadColumns();
        }


        private void loadColumns()
        {
            if (clboxColumns.Items.Count == 0)
            {
                foreach (DataGridViewColumn col in dgv.Columns)
                {
                    clboxColumns.Items.Add(col.Name, true);
                }
            }
        }

        private void visualizacionColumnasDGV()
        {
           string columnName = "";
        
           for (int i = 0; i < clboxColumns.Items.Count; i++)
            {
                columnName = clboxColumns.Items[i].ToString();
               if (clboxColumns.GetItemChecked(i))
                {
                    dgv.Columns[columnName].Visible = true;
                }
               else
               {
                   dgv.Columns[columnName].Visible = false;
               }
            }
        }



        private void clboxColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            visualizacionColumnasDGV();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clboxColumns.Items.Count; i++)
            {

                clboxColumns.SetItemChecked(i, true);
            }
            visualizacionColumnasDGV();
        }
    }
}
