﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Threading;
using System.Windows.Forms;

namespace klsync
{
    class csTallasYColoresV2
    {
        csMySqlConnect mySqlConnect = new csMySqlConnect();
        csSqlConnects sqlConnect = new csSqlConnects();
        csIdiomas idiomasPS = new csIdiomas();
        csCheckVersion checkVersion = new csCheckVersion();

        private MySqlConnection connection;

       

        public void inicializarTallasYColores(string idProduct)
        {
            try
            {
                string script="";
                if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                {
                    //Si trabajamos con Monotallas caso Indusnow
                    //marcamos las familias de productos como Monotallas
                    string anexoFamiliasMonoAtributo = "";

                    if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_MONOATRIBUTO"))
                    {
                        if(csGlobal.activarMonotallas=="Y")
                        {
                            marcarFamiliasTallasMonoTalla();
                            anexoFamiliasMonoAtributo = " WHERE FAMILIATALLA.KLS_MONOTALLA IS NULL OR FAMILIATALLA.KLS_MONOTALLA in ('F','')"; 
                        }

                    }
                   



                    DataTable A3_tablaFamiliasAtributos = new DataTable();
                    DataTable PS_tablaFamiliasAtributos = new DataTable();
                    DataTable PS_nuevasFamiliasAtributos = new DataTable();

                    DataTable A3_tablaAtributos = new DataTable();
                    DataTable PS_tablaAtributos = new DataTable();
                    DataTable PS_nuevosAtributos = new DataTable();

                    DataTable TablaAtributos = new DataTable();

                    DataTable TablaAtributosArt1 = new DataTable();
                    DataTable TablaAtributosArt2 = new DataTable();

                    //Cargamos los códigos de familias de PS y A3 para ver que diferencias hay, las diferencias se crearán nuevas en PS
                    A3_tablaFamiliasAtributos = sqlConnect.obtenerDatosSQLScript("SELECT LTRIM(CODFAMTALLA) AS CODFAMTALLA FROM FAMILIATALLA" + anexoFamiliasMonoAtributo);
                    PS_tablaFamiliasAtributos = mySqlConnect.obtenerDatosPS("select kls_codFamiliaA3 from ps_attribute_group");

                    //Busco si hay Familias de Atributos nuevos creados en A3ERP, si encuentra los guarda en el datatable
                    PS_nuevasFamiliasAtributos = csUtilidades.getDiffDataTables(A3_tablaFamiliasAtributos, PS_tablaFamiliasAtributos);

                    //Verificar Grupos Nuevos y crearlos
                    if (PS_nuevasFamiliasAtributos.Rows.Count > 0)
                    {
                        sincronizarGrupoAtributos(PS_nuevasFamiliasAtributos);
                    }

                    //Cargamos los códigos de familias de PS y A3 para ver que diferencias hay, las diferencias se crearán nuevas en PS
                    //Nota: para comparar y que funcione tengo que convertir los números en string

                    if (csGlobal.activarMonotallas=="Y")
                    {
                        script = "SELECT " +
                            " CONVERT(varchar(5), dbo.TALLAS.ID) AS ID " +
                            " FROM " +
                            " dbo.TALLAS INNER JOIN " +
                            " dbo.FAMILIATALLA ON dbo.TALLAS.CODFAMTALLA = dbo.FAMILIATALLA.CODFAMTALLA " +
                            "WHERE (dbo.FAMILIATALLA.KLS_MONOTALLA is NULL OR dbo.FAMILIATALLA.KLS_MONOTALLA in ('F','')) order by dbo.TALLAS.ID";
                    }
                    else
                    {
                        script = "SELECT CONVERT(varchar(5), dbo.TALLAS.ID) AS ID FROM dbo.TALLAS";
                    }

                    A3_tablaAtributos = sqlConnect.obtenerDatosSQLScript(script);
                    PS_tablaAtributos = mySqlConnect.obtenerDatosPS("select cast(id_attribute as char(5)) as ID from ps_attribute");

                    //Busco si hay Familias de Atributos nuevos creados en A3ERP, si encuentra los guarda en el datatable
                    PS_nuevosAtributos = csUtilidades.getDiffDataTables(A3_tablaAtributos, PS_tablaAtributos);

                    //TablaAtributos = cargarAtributos();

                    //Verificar Atributos Nuevos y crearlos
                    if (PS_nuevosAtributos.Rows.Count > 0)
                    {
                        sincronizarAtributos(PS_nuevosAtributos);
                    }

                }

                // Sincronizo los artículos y sus combinaciones
                // borrando la combinacion de ese determinado producto
                sincronizarAtributosArticulos(idProduct);
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        public void resetearTyCSlow(string idProduct)
        {
            try
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();


                string ps_product_attribute_combination = "delete from ps_product_attribute_combination where id_product_attribute in (select id_product_attribute from ps_product_attribute_shop where id_product= " + idProduct + ")";
                //Añado un segundo script a nivel de redundancia, ya que los attribute_id estan en las dos tablas y una podría estar vacía
                string ps_product_attribute_combination2 = "delete from ps_product_attribute_combination where id_product_attribute in (select id_product_attribute from ps_product_attribute where id_product= " + idProduct + ")";
                string ps_product_attribute_shop = "delete from ps_product_attribute_shop where id_product_attribute in (select id_product_attribute from ps_product_attribute where id_product= "+idProduct+")";
                string ps_product_attribute_shop_2 = "delete from ps_product_attribute_shop where id_product= " + idProduct;
                string ps_product_attribute = "delete from ps_product_attribute where id_product=" + idProduct;
                string ps_stock_available = "delete from ps_stock_available where id_product=" + idProduct;

                //csUtilidades.ejecutarConsulta(ps_product_attribute_combination, true);
                //csUtilidades.ejecutarConsulta(ps_product_attribute_shop, true);
                //csUtilidades.ejecutarConsulta(ps_product_attribute_shop_2, true);
                //csUtilidades.ejecutarConsulta(ps_product_attribute, true);
                //csUtilidades.ejecutarConsulta(ps_stock_available, true);

                MySqlCommand cmd = new MySqlCommand(ps_product_attribute_combination, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_product_attribute_combination2, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_product_attribute_shop, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_product_attribute_shop_2, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_product_attribute, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_product_attribute, connection);
                cmd.ExecuteNonQuery();

                connection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        
        
        }


        public void resetearTyC(DataTable Products)
        {
            try
            {
                string idProduct = csUtilidades.concatenarValoresQueryFromDataTable(Products, "idProduct");
                connection = new MySqlConnection(conexionDestino());
                connection.Open();


                // string ps_product_attribute_combination = "delete from ps_product_attribute_combination where id_product_attribute in (select id_product_attribute from ps_product_attribute_shop where id_product in (" + idProduct + "))";
                string ps_product_attribute_combination = "delete from ps_product_attribute_combination where id_product_attribute in (select id_product_attribute from ps_product_attribute where id_product in (" + idProduct + "))";
                string ps_product_attribute_shop = "delete from ps_product_attribute_shop where id_product_attribute in (select id_product_attribute from ps_product_attribute where id_product in (" + idProduct + "))";
                //string ps_product_attribute_shop_2 = "delete from ps_product_attribute_shop where id_product in (" + idProduct + ")";
                string ps_product_attribute = "delete from ps_product_attribute where id_product in (" + idProduct + ")";
                string ps_stock_available = "delete from ps_stock_available where id_product in (" + idProduct + ")";
                
                //string ps_product_attribute_combination = "delete from ps_product_attribute_combination where id_product_attribute in (select id_product_attribute from ps_product_attribute_shop where id_product= " + idProduct + ")";
                //string ps_product_attribute_shop = "delete from ps_product_attribute_shop where id_product_attribute in (select id_product_attribute from ps_product_attribute_shop where id_product= "+idProduct+")";
                //string ps_product_attribute_shop_2 = "delete from ps_product_attribute_shop where id_product= " + idProduct;
                //string ps_product_attribute = "delete from ps_product_attribute where id_product=" + idProduct;
                //string ps_stock_available = "delete from ps_stock_available where id_product=" + idProduct;

                //8/6/2018 modificado para ganar velocidad
            //csUtilidades.ejecutarConsulta(ps_product_attribute_combination, true);
            //csUtilidades.ejecutarConsulta(ps_product_attribute_shop, true);
            //csUtilidades.ejecutarConsulta(ps_product_attribute_shop_2, true);
            //csUtilidades.ejecutarConsulta(ps_product_attribute, true);
            //csUtilidades.ejecutarConsulta(ps_stock_available, true);

                MySqlCommand cmd = new MySqlCommand(ps_product_attribute_combination, connection);
                cmd.ExecuteNonQuery();
                
                cmd = new MySqlCommand(ps_product_attribute_shop, connection);
                cmd.ExecuteNonQuery();
                
                //8-8-2018 comentamos ya que no en todas las versiones de la tabla está el campo idproduct
                //cmd = new MySqlCommand(ps_product_attribute_shop_2, connection);
                //cmd.ExecuteNonQuery();
                
                cmd = new MySqlCommand(ps_product_attribute, connection);
                cmd.ExecuteNonQuery();
                
                cmd = new MySqlCommand(ps_product_attribute, connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand(ps_stock_available, connection);
                cmd.ExecuteNonQuery();
                
                connection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }



        public string conexionDestino()
        {
            //Formamos la cadena de conexión en función de los valores de las Variables Globales
            string connectionString = "";
            string puertoMySQL = "PORT=" + csGlobal.PortPS + ";";

            if (csGlobal.PortPS == "")
            {
                puertoMySQL = "PORT=3306;";
            }

            connectionString = "SERVER=" + csGlobal.ServerPS + ";" + puertoMySQL + "DATABASE=" + csGlobal.databasePS + ";" + "UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";
            //connectionString = "SERVER=" + csGlobal.ServerPS + ";" + "DATABASE=\"copilot-system_com_copishopdb\";UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";

            return connectionString;
        }

        private DataTable cargarAtributos()
        {
            DataTable atributosA3 = new DataTable();
            DataTable atributosPS = new DataTable();
            string queryLoadAtributos = "";
            atributosPS = cargarATributosPS();
            string exludeAtributos = "";
            int i = 0;


            //Cargo la información de PS
            foreach (DataRow fila in atributosPS.Rows)
            {
                if (i == 0)
                {
                    exludeAtributos = exludeAtributos + fila["id_attribute"].ToString();
                }
                else
                {
                    exludeAtributos = exludeAtributos + "," + fila["id_attribute"].ToString();
                }
                i++;
            }


            //Creo una consulta para detectar sólo aquellos atributos que haya que crear nuevos
            if (atributosPS.Rows.Count > 0)
            {
                exludeAtributos = " WHERE ID NOT IN (" + exludeAtributos + ") ";
            }

            queryLoadAtributos = " SELECT " +
                      " dbo.FAMILIATALLA.CODFAMTALLA, dbo.FAMILIATALLA.DESCFAMTALLA, LTRIM(dbo.FAMILIATALLA.KLS_IDGROUP) as IDGROUP, dbo.TALLAS.ID, dbo.TALLAS.CODTALLA, " +
                      " dbo.TALLAS.DESCRIPCION, dbo.TALLAS.ORDENTALLA " +
                      " FROM " +
                      " dbo.TALLAS INNER JOIN " +
                      " dbo.FAMILIATALLA ON dbo.TALLAS.CODFAMTALLA = dbo.FAMILIATALLA.CODFAMTALLA" + exludeAtributos;

            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();

            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(queryLoadAtributos, dataConnection);

            a.Fill(atributosA3);

            return atributosA3;
        }

        private DataTable cargarFamiliasAtributos()
        {

            DataTable FamiliasAtributosA3 = new DataTable();
            DataTable FamiliasAtributosPS = new DataTable();
            string queryLoadAtributosA3 = "";
            FamiliasAtributosPS = cargarFamiliasAtributosPS();
            string excludeFamilias = "";
            int i = 0;

            //Cargo la información de PS
            //Creo un string para ver que familias ya están creadas en Prestashop y que me las cribe al consultar las familias de A3
            foreach (DataRow fila in FamiliasAtributosPS.Rows)
            {
                if (i == 0)
                {
                    excludeFamilias = excludeFamilias + "'" + fila["id_attribute_group"].ToString() + "'";
                }
                else
                {
                    excludeFamilias = excludeFamilias + "," + "'" + fila["id_attribute_group"].ToString() + "'";
                }
                i++;
            }

            if (FamiliasAtributosPS.Rows.Count > 0)
            {

                //20/01/2017 Cambiamos la columna KLS_IDGROUP POR LA COLUMNA CODFAMTALLA
                //excludeFamilias = " WHERE  KLS_IDGROUP NOT IN (" + excludeFamilias + ")";
                excludeFamilias = " WHERE  CODFAMTALLA NOT IN (" + excludeFamilias + ")";
            }
            else
                excludeFamilias = " WHERE LTRIM(KLS_IDGROUP) > 0";

            //Cargo la información de A3
            queryLoadAtributosA3 = "SELECT * FROM dbo.FAMILIATALLA " + excludeFamilias;

            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();

            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(queryLoadAtributosA3, dataConnection);

            a.Fill(FamiliasAtributosA3);

            return FamiliasAtributosA3;

        }

        public DataTable cargarCombinacionesA3(string articulo, string modoTallas)
        {
            string queryLoadCombinacionesA3 = "";
            string anexo = "";
            string anexoMonoAtributo1 = "";
            string anexoMonoAtributo2 = "";
            string anexoMonoAtributo3 = "";

            //Cargo las combinaciones de cada articulo y devuelve aquellas combinaciones a crear
            DataTable FamiliasAtributos = new DataTable();

            //DEFINO DOS MODALIDADES SEGÚN SI EN A3 SE TRABAJA CON TALLAS O NO
            if (modoTallas.ToUpper() == "A3SIPSSI")
            {

                if (articulo != "")
                {
                    anexo = " dbo.ARTICULO.KLS_ID_SHOP = " + articulo;

                }
                
                if (csGlobal.activarMonotallas == "Y" )
                { 
                    anexoMonoAtributo1 = " ,KLS_MONOATRIBUTO ";
                    anexoMonoAtributo2 = " AND KLS_MONOATRIBUTO IS NULL ";
                    anexoMonoAtributo3 = " AND KLS_MONOATRIBUTO IS NULL ";
                }
                
                

                //CONSULTA PARA TODAS LAS COMBINACIONES
                if (articulo == "")
                {
                    queryLoadCombinacionesA3 = " SELECT " +
                                                " LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.KLS_ID_SHOP, COUNT(dbo.ARTICULO.CODART) AS COMBINACIONES " + anexoMonoAtributo1 +
                                                " FROM  " +
                                                " dbo.ARTICULO LEFT OUTER JOIN " +
                                                " dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAV = TALLAS_1.CODFAMTALLA LEFT OUTER JOIN " +
                                                " dbo.TALLAS ON dbo.ARTICULO.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA " +
                                                " WHERE     (dbo.ARTICULO.TALLAS = 'T') " + anexo + anexoMonoAtributo2 +
                                                " GROUP BY dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP " + anexoMonoAtributo1 +
                                                " HAVING      (dbo.ARTICULO.KLS_ID_SHOP > 0)";
                }
                else
                //CONSULTA SÓLO PARA UN ARTÍCULO
                {
                    queryLoadCombinacionesA3 = "SELECT " +
                        " dbo.KLS_COMBINACIONES_VISIBLES.CODART, dbo.ARTICULO.KLS_ID_SHOP, COUNT(dbo.KLS_COMBINACIONES_VISIBLES.CODART) AS COMBINACIONES " + anexoMonoAtributo1 +
                        " FROM " +
                        " dbo.KLS_COMBINACIONES_VISIBLES INNER JOIN " +
                        " dbo.ARTICULO ON LTRIM(dbo.KLS_COMBINACIONES_VISIBLES.CODART) = LTRIM(dbo.ARTICULO.CODART) " +
                        " WHERE " + anexo + anexoMonoAtributo2 +
                        " GROUP BY dbo.KLS_COMBINACIONES_VISIBLES.CODART, dbo.ARTICULO.KLS_ID_SHOP" + anexoMonoAtributo1;
                }
            }
            else if (modoTallas.ToUpper() == "A3NOPSSI")
            {
                if (articulo != "" && articulo != null)
                {
                    anexo = " WHERE AP.KLS_ID_SHOP = " + articulo;

                }

                queryLoadCombinacionesA3 = "SELECT " +
                    " LTRIM(dbo.ARTICULO.KLS_ARTICULO_PADRE) AS CODART, AP.KLS_ARTICULO_TIENDA, AP.KLS_ID_SHOP, COUNT(dbo.ARTICULO.CODART) " +
                    " AS COMBINACIONES " +
                    " FROM " +
                    " dbo.ARTICULO LEFT OUTER JOIN " +
                    " dbo.ARTICULO AS AP ON dbo.ARTICULO.KLS_ARTICULO_PADRE = AP.CODART " +
                    anexo + 
                    " GROUP BY dbo.ARTICULO.KLS_ARTICULO_PADRE, AP.KLS_ARTICULO_TIENDA, AP.KLS_ID_SHOP " +
                    " HAVING      (dbo.ARTICULO.KLS_ARTICULO_PADRE <> '' AND AP.KLS_ID_SHOP>0)";

            }

            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();

            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter adaptA3 = new SqlDataAdapter(queryLoadCombinacionesA3, dataConnection);

            adaptA3.Fill(FamiliasAtributos);

            return FamiliasAtributos;
        }

        private DataTable cargarCombinacionesPs(string articulo)
        {
            string anexo = "";

            if (articulo != "" && articulo != null)
            {
                anexo = " where id_product= " + articulo;
            }
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadCombinacionesPS = "";
            queryLoadCombinacionesPS = " select id_product, COUNT(id_product_attribute) as combinacionesPS from ps_product_attribute " + anexo + " group by id_product ";
            //open connection
            //if (this.OpenConnection() == true)
            //{
            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(queryLoadCombinacionesPS, connection);
            //Execute command
            //cmd.ExecuteNonQuery();
            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadCombinacionesPS, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);

            //close connection
            connection.Close();
            return tabla;
        }


        private DataTable cargarProductosMonoAtributo()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadArticulosMonoatributo = "";
            queryLoadArticulosMonoatributo = "select codart,articulo.KLS_ID_SHOP as product_id, descart, codfamtallah, " +
                                        " (select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAH) group by CODFAMTALLA) as TALLAS,  " +
                                        " codfamtallav,(select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAV) group by CODFAMTALLA) AS COLORES " +
                                        " from ARTICULO where  " +
                                        " (select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAH) group by CODFAMTALLA)=1 and  " +
                                        " (select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAV) group by CODFAMTALLA)=1 ";
            MySqlCommand cmd = new MySqlCommand(queryLoadArticulosMonoatributo, connection);
            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadArticulosMonoatributo, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);

            //close connection
            connection.Close();
            return tabla;
        }



        public void marcarArticulosMonoatributoEnA3ERP(string codArt)
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            string query;
            string anexo = "";

            if (codArt != "")
            {
                anexo = "AND LTRIM(CODART)='" + codArt + "'";
            
            }

            csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_MONOATRIBUTO=NULL WHERE KLS_MONOATRIBUTO = ''", false);

            query = "UPDATE ARTICULO SET articulo.kls_monoatributo='T' WHERE LTRIM(CODART) IN " +
                        " (select LTRIM(CODART) " +
                        " from ARTICULO where " +
                        " ((select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAH) group by CODFAMTALLA)=1 OR  articulo.CODFAMTALLAH IS NULL) " +
                        " and " +
                        " ((select count(codtalla) from tallas where codfamtalla in (articulo.CODFAMTALLAV) group by CODFAMTALLA)=1 OR articulo.CODFAMTALLAV IS NULL)) " + anexo; 
            csUtilidades.ejecutarConsulta(query, false);
        
        }

        private DataTable cargarFamiliasAtributosPS()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadCombinacionesPS = "";
            queryLoadCombinacionesPS = "select id_attribute_group,is_color_group,group_type,position,ltrim(kls_codFamiliaA3) as kls_codfamtalla from ps_attribute_group";

            MySqlCommand cmd = new MySqlCommand(queryLoadCombinacionesPS, connection);
            //Execute command
            //cmd.ExecuteNonQuery();
            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadCombinacionesPS, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);
            //close connection
            connection.Close();
            return tabla;
        }

        private DataTable cargarATributosPS()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadAtributosPS = "";
            queryLoadAtributosPS = "select id_attribute,id_attribute_group,color,position,kls_codAtributoA3 as kls_CodTalla,id_attribute_group as kls_CodFamTalla from ps_attribute";

            MySqlCommand cmd = new MySqlCommand(queryLoadAtributosPS, connection);
            //Execute command
            //cmd.ExecuteNonQuery();
            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadAtributosPS, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);

            //close connection
            connection.Close();
            return tabla;
        }

        private void sincronizarGrupoAtributos(DataTable tablaFamiliaAtributos)
        {
            try
            {
                string output = "";

                for (int i = 0; i < tablaFamiliaAtributos.Rows.Count; i++)
                {
                    if ((tablaFamiliaAtributos.Rows.Count > 1) && (i > 0))
                    {
                        output = output + ",'" + tablaFamiliaAtributos.Rows[i]["CODFAMTALLA"].ToString() + "'";
                        output += (i < tablaFamiliaAtributos.Rows.Count) ? "" : string.Empty;
                    }
                    else
                    {
                        output = output + "'" + tablaFamiliaAtributos.Rows[i]["CODFAMTALLA"].ToString() + "'";
                        output += (i < tablaFamiliaAtributos.Rows.Count) ? "" : string.Empty;
                    }


                }
                csSqlConnects sqlConnects = new csSqlConnects();

                DataTable nuevaTablaFamiliaAtributos = sqlConnect.obtenerDatosSQLScript("SELECT * FROM FAMILIATALLA WHERE LTRIM(CODFAMTALLA) IN (" + output + ")");


                //CARGAMOS LAS FAMILIAS DE TALLAS Y COLORES DE A3
                string tableFields = "";
                string queryAttributeGroups = "";
                string queryAttributeGroups_shop = "";
                string queryAttributeGroupsLang = "";
                string descripcionFamilia = "";
                string codigoFamiliaA3 = "";
                int idCreated = 0;

                //creamos las familias de Atributos en PS
                foreach (DataRow famAtributo in nuevaTablaFamiliaAtributos.Rows)
                {
                    //el índice de la tabla ps_attribute_group es un autoincrement, por lo que hago que se genere automáticamente
                    //y le añado el código de la familia en el campo kls_codFamiliaA3
                    tableFields = " (is_color_group,group_type,position,kls_codFamiliaA3) ";

                    codigoFamiliaA3 = famAtributo["CODFAMTALLA"].ToString().Trim();
                    descripcionFamilia = famAtributo["DESCFAMTALLA"].ToString();
                    queryAttributeGroups = "(0 , 'select' ,0,'" + codigoFamiliaA3 + "')";


                    //21/01/2017 Se cambia la forma de trabajar, para que sea independiente si en A3 el código de familia es numérico o alfanumérico
                    ////inserto la familia en ps_attribute_group
                    //descripcionFamilia = famAtributo["DESCFAMTALLA"].ToString();
                    //codigoFamiliaA3 = famAtributo["KLS_IDGROUP"].ToString().Trim();
                    ////Cambiamos campo origen KLS_IDGROUP POR CODFAMTALLA
                    ////atributeGroup = famAtributo["KLS_IDGROUP"].ToString().Trim();
                    //atributeGroup = famAtributo["CODFAMTALLA"].ToString().Trim();
                    ////queryAttributeGroups = "(" + atributeGroup + ", 0 , 'select' , 0 ," + famAtributo["KLS_IDGROUP"] + ",'" + codigoFamiliaA3 + "')";
                    //queryAttributeGroups = "(" + atributeGroup + ", 0 , 'select' , 0,'" + codigoFamiliaA3 + "')";

                    idCreated = mySqlConnect.InsertValoresEnTabla("ps_attribute_group", tableFields, queryAttributeGroups, true);





                    //mySqlConnect.InsertValoresEnTabla("ps_attribute_group", queryAttributeGroups, true);


                    //inserto la familia en ps_attribute_group_shop

                    //21/01/2017 Cambio el campo por el idCreado
                    queryAttributeGroups_shop = "(" + idCreated + ", 1 )";
                    mySqlConnect.InsertValoresEnTabla("ps_attribute_group_shop", queryAttributeGroups_shop, true);

                    // Para cada idioma grabo la familia de atributos 
                    foreach (string idioma in idiomasPS.idiomasActivosPS())
                    {
                        queryAttributeGroupsLang = "(" + idCreated + ", " + idioma + " , '" + descripcionFamilia + "',' " + descripcionFamilia + "')";
                        mySqlConnect.InsertValoresEnTabla("ps_attribute_group_lang", queryAttributeGroupsLang, true);
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void sincronizarAtributos(DataTable tablaAtributos) // tallas
        {
            try
            {
                csMySqlConnect mySqlConnect = new csMySqlConnect();

                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string codigoGrupo = "";
                string ps_Id_attribute_group = "";
                string a3_codFamTalla = "";
                string descripcion = "";
                string idAtributo = "";
                string position = "";
                string anexo = "";
                int i = 0;
                int ii = 0;


                csSqlConnects sqlConnect = new csSqlConnects();
                //csMySqlConnect mySqlConnect = new csMySqlConnect();
                //DataTable familiaTallasYColores = new DataTable("TallasColores");
                //familiaTallasYColores = sqlConnect.cargarDatosTabla("TALLAS");

                string output = "";

                for (int fila = 0; fila < tablaAtributos.Rows.Count; fila++)
                {
                    if ((tablaAtributos.Rows.Count > 1) && (fila > 0))
                    {
                        output = output + ",'" + tablaAtributos.Rows[fila]["ID"].ToString() + "'";
                        output += (fila < tablaAtributos.Rows.Count) ? "" : string.Empty;
                    }
                    else
                    {
                        output = output + "'" + tablaAtributos.Rows[fila]["ID"].ToString() + "'";
                        output += (fila < tablaAtributos.Rows.Count) ? "" : string.Empty;
                    }


                }

                DataTable nuevaTablaFamiliaAtributos = sqlConnect.obtenerDatosSQLScript("SELECT * FROM TALLAS WHERE ID IN (" + output + ")");

                foreach (DataRow atributo in nuevaTablaFamiliaAtributos.Rows)
                {
                    codigoGrupo = atributo["CODFAMTALLA"].ToString().Trim();
                    a3_codFamTalla = atributo["CODFAMTALLA"].ToString().Trim();
                    idAtributo = atributo["ID"].ToString().Trim();
                    position = atributo["ORDENTALLA"].ToString().Trim();
                    descripcion = atributo["CODTALLA"].ToString().Trim();
                    ps_Id_attribute_group = mySqlConnect.obtenerValorNumericoQuery("select id_attribute_group from ps_attribute_group where kls_codFamiliaA3='" + a3_codFamTalla.Trim() + "'");

                    //Reviso si la familia es monotalla, o si no se ha traspasado
                    //si está subida la familia a PS entonces importo los atributos
                    if (ps_Id_attribute_group != "")
                    {


                        //Hago la composición del insert                    
                        if (i > 0)
                        {
                            Lineas = Lineas + ",";
                            LineasShop = LineasShop + ",";
                        }
                        Lineas = Lineas + "(" + idAtributo + "," + ps_Id_attribute_group + ", " + position + ")";
                        LineasShop = LineasShop + "(" + idAtributo + ",1)";

                        foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                        {
                            if (ii > 0)
                            {
                                LineasLang = LineasLang + ",";
                            }

                            LineasLang = LineasLang + "(" + idAtributo + "," + valuePair.Key.ToString() + ",'" + descripcion + "')";
                            ii++;
                        }

                        i++;
                    }
                    else
                    {
                        string stop = "";
                    }
                }
                if (tablaAtributos != null)
                {
                    if (tablaAtributos.Rows.Count > 0)
                    {
                        mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
                        mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");

                        mySqlConnect.InsertValoresEnTabla("ps_attribute_lang", "(id_attribute, id_lang,name)", LineasLang, "");
                    }
                }
                LineasLang = "";
                //MessageBox.Show("Exportación Finalizada");

                //Para la tabla afectada por idiomas, tenemos que repetirlo para cada idioma

                //MessageBox.Show("Exportación Finalizada");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

        }

        public void sincronizarAtributosArticulos(string articulo)
        {

            try
            {
                //csUtilidades utilidadesEsync = new csUtilidades();
                string numeroAtributos = "";
                int idCombinacion = 1;
                string idAtributo1 = "";
                string idAtributo2 = "";
                string famAtributo1 = "";
                string famAtributo2 = "";
                string singleAtribute = "";         //lo utilizamos cuando sólo hay 1 atributo
                string product_id = "";
                string default_on = "0";
                string defaultAtributoArticulo = "";
                bool defaultAtributte = false;
                string cantidad = "";
                string stock_disponible = "";
                string idproductA3 = "";
                string idproductPS = "";
                bool existeCombinacion = false;
                string referencia = "";
                bool combinacionVisible2 = true;
                int cantidadTotal = 0;


                List<string> valuesComb = new List<string>();
                List<string> valuesProd = new List<string>();
                List<string> valuesShop = new List<string>();
                List<string> valuesStockDetail = new List<string>();
                List<string> valuesStockTotal = new List<string>();

                string LineasComb = ""; //ps_attribute
                string LineasProd = "";
                string LineasShop = "";
                string LineasStock = "";
                string LineasStockTotal = "";

                int i = 0;
                int ii = 0;
                int contadorLineas = 0;
                string precioHijos = "";


                DataTable tablaCombinaciones = new DataTable();
                DataTable tablaCombinacionesArtA3 = new DataTable();
                DataTable tablaNuevasCombinaciones = new DataTable();
                DataTable tablaArticulosConCombinacionesA3NOPSSI = new DataTable();

                //DT para agilizar las consultas evitando hacer muchas consultas a PS, se sustituye por consultas locales
                //Cargo de cada artículo, la combinación de atributos
                DataTable tablaCombinacionesAtributosPS = new DataTable();
                tablaCombinacionesAtributosPS = cargarCombinacionesArticuloPS(articulo);

                //DT para agilizar las consultas evitando hacer muchas consultas a PS, se sustituye por consultas locales
                //Cargo la tabla id_product_atribute 
                DataTable tablaCombinacionesAtributosShopPS = cargarCombinacionesAtributosShop(articulo);

                if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI")
                {
                    tablaArticulosConCombinacionesA3NOPSSI = cargarArticulosconCombinacionesA3NOPSSI(articulo);
                }


                //Listado de Artículos de A3 con combinaciones nuevas para añadir


                // SI DEVUELVE MAS DE UNA LINEA QUIERE DECIR QUE ESTA DUPLICADO EL PRODUCTO EN SQL
                tablaNuevasCombinaciones = actualizarCombinacionesArticulos(articulo);


                //Deshabilitamos 1-12-20 no sabemos qué funcionalidad hace o porqué está (LVG)
                //if (tablaNuevasCombinaciones.Rows.Count> 1) {

                //    /*throw new Exception("El articulo seleccionado tiene más de una coincidencia en SQL para el artículo con KLS_ID_SHOP:"+ tablaNuevasCombinaciones.Rows[0]["ID_PRODUCT"]);*/
                //    throw new Exception("El articulo ID_PS: "+ tablaNuevasCombinaciones.Rows[0]["ID_PRODUCT"] + " esta asignado a más de un articulo en A3. \nNo se actualizarán los atributos.");
                //}

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();

                //Obtengo el último ID y le sumo 1
                //Al crearse un artículo se crea una línea de stock (aunque sea 0)
                int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

                //obtengo la última combinación
                int ultimaCombinacion = mySqlConnect.selectID("ps_product_attribute_combination", "id_product_attribute") + 1;
                idCombinacion = ultimaCombinacion;

                //Verificar que está dada de alta la función   KLS_OBTENERSTOCK
                //Cargamos todas las combinaciones de A3ERP con el stock,
                //Revisar si es necesario cargar sólo las combinaciones que sean visibles para agilizar la carga
                tablaCombinacionesArtA3 = cargarCombinacionesArticulosA3(articulo, false);

                if (tablaNuevasCombinaciones.Rows != null)
                {
                    if (tablaNuevasCombinaciones.Rows.Count > 0)
                    {
                        //Actualizo las tablas temporales locales
                        //Cargo todas las combinaciones en Prestashop indicando el número de combinaciones que tiene cada artículo y el detalle de las mismas
                        tablaCombinacionesAtributosPS = cargarCombinacionesArticuloPS(articulo);
                        //Cargamos la tabla ps_produt_atribute en local para verificar si la combinación existe
                        tablaCombinacionesAtributosShopPS = cargarCombinacionesAtributosShop(articulo);

                        #region A3SIPSSI

                        if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                        {
                            foreach (DataRow filaNuevaCombinacion in tablaNuevasCombinaciones.Rows)
                            {
                                //Iteramos la tabla de los artículos con nuevas combinaciones
                                idproductA3 = filaNuevaCombinacion["ID_PRODUCT"].ToString().Trim();

                                foreach (DataRow filaCombinacionArt in tablaCombinacionesArtA3.Rows)
                                {
                                    idproductPS = filaCombinacionArt["KLS_ID_SHOP"].ToString().Trim();

                                    if (idproductPS == idproductA3)
                                    {
                                        //Utilizamos la región para agrupar temporalmente

                                        //VALIDO SI LA COMBINACIÓN ES VISIBLE EN LA TIENDA O NO

                                        combinacionVisible2 = Convert.ToBoolean(Convert.ToInt32(filaCombinacionArt["COMBINACION_VISIBLE"].ToString()));


                                        //Si todas las combinaciones son visibles
                                        if (csGlobal.ps_visible_combinations)
                                        {
                                            combinacionVisible2 = true;
                                        }

                                        if (combinacionVisible2)
                                        {
                                            singleAtribute = "";
                                            numeroAtributos = filaCombinacionArt["ATRIBUTOS"].ToString().Trim();
                                            product_id = filaCombinacionArt["KLS_ID_SHOP"].ToString().Trim();
                                            idAtributo1 = filaCombinacionArt["ID_ATRIBUTO1"].ToString().Trim();
                                            idAtributo2 = filaCombinacionArt["ID_ATRIBUTO2"].ToString().Trim();

                                            //Si trabajamos con monotallas
                                            if (csGlobal.activarMonotallas == "Y")
                                            {
                                                famAtributo1 = filaCombinacionArt["FAMHMONOTALLA"].ToString().Trim();
                                                famAtributo2 = filaCombinacionArt["FAMVMONOTALLA"].ToString().Trim();

                                                //En el caso de que sea todo monoatributo no subiremos la combinación
                                                if (famAtributo1 == "T" && famAtributo2 == "T")
                                                {
                                                    numeroAtributos = "0";
                                                }
                                                if (famAtributo1 == "T" && string.IsNullOrEmpty(famAtributo2))
                                                {
                                                    numeroAtributos = "1";
                                                    idAtributo1 = "";
                                                }
                                                if (famAtributo2 == "T" && string.IsNullOrEmpty(famAtributo1))
                                                {
                                                    numeroAtributos = "1";
                                                    idAtributo2 = "";
                                                }

                                            }


                                            singleAtribute = idAtributo1 + idAtributo2;

                                            cantidad = filaCombinacionArt["STOCK"].ToString().Trim();

                                            cantidadTotal = cantidadTotal + Convert.ToInt32(cantidadTotal);


                                            //verifico si existe la combinación, si existe no la vuelvo a subir
                                            existeCombinacion = existeCombinacionPSDT(product_id, idAtributo1, idAtributo2, numeroAtributos, tablaCombinacionesAtributosPS);

                                            //existeCombinacion = existeCombinacionPS(product_id, idAtributo1, idAtributo2, numeroAtributos);
                                            if (!existeCombinacion)
                                            {
                                                //Cambiar el nombre de variable stock disponible
                                                stock_disponible = filaCombinacionArt["KLS_PERMITIRCOMPRAS"].ToString().Replace(" ", "");
                                                if (stock_disponible == "SI")
                                                {
                                                    stock_disponible = "1"; //SE PERMITE PEDIDOS SI NO HAY STOCK
                                                }
                                                else if (stock_disponible == "NO")
                                                {
                                                    stock_disponible = "0"; //NO SE PERMITE PEDIDOS SI NO HAY STOCK
                                                }
                                                else
                                                {
                                                    string combinacionVisible = filaCombinacionArt["COMBINACION_VISIBLE"].ToString();
                                                    if (combinacionVisible == "1")
                                                    {
                                                        stock_disponible = "2";
                                                    }
                                                    else
                                                    {
                                                        stock_disponible = "2";
                                                    }
                                                }
                                                //Marcamos como default la primera combinación sólo si no hay otras combinaciones existentes
                                                if (defaultAtributoArticulo != product_id)
                                                {
                                                    if (tablaCombinacionesAtributosShopPS.Select("default_on=1 and id_product=" + product_id).Length > 0)
                                                    {
                                                        default_on = "NULL";
                                                    }
                                                    //Verifico si existe algún registro en la tabla con el valor default_on a 1 ya que sólo puede haber 1 registro
                                                    //if (csUtilidades.existeRegistroMySQL("select * from ps_product_attribute_shop inner join ps_product_attribute on ps_product_attribute.id_product_attribute = ps_product_attribute_shop.id_product_attribute where ps_product_attribute.id_product=" + product_id + " and ps_product_attribute_shop.default_on=1"))
                                                    //    default_on = "NULL";
                                                    else
                                                        default_on = "1";
                                                }
                                                else
                                                {
                                                    default_on = "NULL";
                                                }

                                                if (i > 0)
                                                {
                                                    LineasComb = LineasComb + ",";
                                                    LineasProd = LineasProd + ",";
                                                    LineasShop = LineasShop + ",";
                                                }

                                                if (numeroAtributos == "1")
                                                {
                                                    LineasComb = "(" + singleAtribute + "," + idCombinacion + ")";
                                                    valuesComb.Add(LineasComb);
                                                }
                                                else if (numeroAtributos == "2")
                                                {
                                                    LineasComb = "(" + idAtributo1 + "," + idCombinacion + "),(" + idAtributo2 + "," + idCombinacion + ")";
                                                    valuesComb.Add(LineasComb);
                                                }

                                                //Lo ponemos a cero el precio porque si no falla al ejecutar el insert.
                                                precioHijos = "0";


                                                //El campo vacío es para la "referencia". Los cuatro campos del insert son: Id_product_attribute,id_product,reference,price
                                                LineasProd = "(" + idCombinacion + "," + product_id + ",null," + precioHijos + ")";



                                                valuesProd.Add(LineasProd);


                                                string probandoGit = "";


                                                //LineasProd = "(" + idCombinacion + "," + product_id + ",'')";
                                                //valuesProd.Add(LineasProd);

                                                //Si la versión es superior a la versión 1.6.1 incluimos el product_id en el insert
                                                if (checkVersion.checkVersión("1.6.1"))
                                                {
                                                    LineasShop = "(" + idCombinacion + ",1," + default_on + "," + product_id + "," + precioHijos + ")";
                                                    valuesShop.Add(LineasShop);
                                                }
                                                else
                                                {
                                                    //Si la versión es anterior a la 1.6.1 default_on no puede ser null, y su valor por defecto es 1
                                                    if (default_on == "NULL")
                                                    {
                                                        default_on = "0";
                                                    }

                                                    LineasShop = "(" + idCombinacion + ",1," + default_on + ")";
                                                    valuesShop.Add(LineasShop);
                                                }





                                                LineasStock = "(" + ultimoId.ToString() + "," + product_id + "," + idCombinacion + ",1,0," + cantidad + ",0," + stock_disponible + ")";
                                                valuesStockDetail.Add(LineasStock);
                                                ultimoId = ultimoId + 1;

                                                idCombinacion++;
                                                i++;
                                                contadorLineas++;
                                                defaultAtributoArticulo = product_id;

                                                //Inserto los valores si hay más de 500 registros
                                                if (contadorLineas == 500)
                                                {
                                                    //insertarAtributosArticulo(LineasComb, LineasProd, LineasShop, LineasStock);
                                                    i = 0;
                                                    contadorLineas = 0;
                                                    LineasComb = "";
                                                    LineasProd = "";
                                                    LineasShop = "";
                                                    LineasStock = "";
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        # endregion

                        //Utilizamos la región para agrupar temporalmente
                        #region A3NOPSSI
                        if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI")
                        {
                            int contador = 0;
                            foreach (DataRow filaNuevaCombinacion in tablaNuevasCombinaciones.Rows)
                            {
                                //Iteramos la tabla de los artículos con nuevas combinaciones
                                idproductA3 = filaNuevaCombinacion["ID_PRODUCT"].ToString().Trim();
                                defaultAtributte = true;

                                //Itero cada articulo hijo de A3, cada combinación

                                foreach (DataRow filaCombinacionArt in tablaArticulosConCombinacionesA3NOPSSI.Rows)
                                {

                                    idproductPS = filaCombinacionArt["IDSHOP_PADRE"].ToString().Trim();
                                    referencia = filaCombinacionArt["CODART"].ToString().Trim();
                                    precioHijos = filaCombinacionArt["PRCVENTA"].ToString().Replace(",", ".");

                                    //verifico si existe la combinación, si existe no la vuelvo a subir
                                    if (idproductA3 == idproductPS)
                                    {
                                        existeCombinacion = existeCombinacionPS(idproductA3, referencia);
                                    }

                                    if ((idproductPS == idproductA3) && (existeCombinacion == false))
                                    {

                                        //VALIDO SI LA COMBINACIÓN ES VISIBLE EN LA TIENDA O NO
                                        if (filaCombinacionArt["COMBINACION_VISIBLE"].ToString() == "F")
                                        {

                                            foreach (DataRow filaCombinacionAtributos in tablaCombinacionesArtA3.Rows)
                                            {
                                                if (referencia == filaCombinacionAtributos["CODART"].ToString().Trim())
                                                {

                                                    if (contador == 0)
                                                    {
                                                        //marcamos la primera combinación como atributo por defecto y asignaremos el defaul_on=1
                                                        if (defaultAtributte)
                                                        {
                                                            defaultAtributoArticulo = idCombinacion.ToString();
                                                            default_on = "1";
                                                        }

                                                        LineasComb = "(" + filaCombinacionAtributos["KLS_COD_ATRIBUTO"].ToString().Trim() + "," + idCombinacion + ")";
                                                    }
                                                    else
                                                    {
                                                        LineasComb = LineasComb + "," + "(" + filaCombinacionAtributos["KLS_COD_ATRIBUTO"].ToString().Trim() + "," + idCombinacion + ")";
                                                    }

                                                    contador++;
                                                }
                                            }
                                            if (LineasComb != "")
                                            {

                                                //Combinaciones de productos
                                                valuesComb.Add(LineasComb);
                                                LineasComb = "";
                                                contador = 0;

                                                //REVISAR
                                                cantidad = "0";
                                                stock_disponible = "1";



                                                //Atributos de producto

                                                //Si los atributos tienen precios se lo añadimos, en caso contrario su precio sera 0.
                                                precioHijos = csGlobal.gestionPreciosHijos ? precioHijos : "0.00";


                                                LineasProd = "(" + idCombinacion + "," + idproductPS + ",'" + referencia + "'," + precioHijos + ")";
                                                valuesProd.Add(LineasProd);


                                                //Atributos de producto por Shop

                                                //Marcamos como default la primera combinación sólo si no hay otras combinaciones existentes
                                                //Verifico si existe algún registro en la tabla con el valor default_on a 1 ya que sólo puede haber 1 registro
                                                if (tablaCombinacionesAtributosShopPS.Select("default_on=1 and id_product=" + idproductPS).Length > 0)
                                                {
                                                    default_on = "NULL";
                                                }

                                                //La primera asignación, como ya he marcado una vez el defaul_on, el resto de combinaciones del mismo artículo serán null
                                                if (defaultAtributoArticulo != idCombinacion.ToString())
                                                {
                                                    default_on = "NULL";
                                                }


                                                if (valuesShop.Count == 41)
                                                {
                                                    int ix = 0;
                                                }


                                                if (checkVersion.checkVersión("1.6.1"))
                                                {
                                                    LineasShop = "(" + idCombinacion + ",1," + default_on + "," + idproductPS + "," + precioHijos + ")";
                                                    valuesShop.Add(LineasShop);
                                                }
                                                else
                                                {
                                                    LineasShop = "(" + idCombinacion + ",1," + default_on + ")";
                                                    valuesShop.Add(LineasShop);
                                                }

                                                //Stock por atributo
                                                LineasStock = "(" + ultimoId.ToString() + "," + idproductPS + "," + idCombinacion + ",1,0," + cantidad + ",0," + stock_disponible + ")";
                                                valuesStockDetail.Add(LineasStock);
                                                ultimoId = ultimoId + 1;
                                                idCombinacion++;
                                                defaultAtributte = false;
                                            }

                                        }
                                    }
                                }
                            }

                        }
                        #endregion



                        csUtilidades.WriteListToDatabase("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", valuesComb, csUtilidades.MYSQL, true);
                        //csUtilidades.WriteListToDatabase("ps_product_attribute", "(id_product_attribute, id_product, reference)", valuesProd, csUtilidades.MYSQL, true);

                        //4-1-2019 Gestión de precios por  hijos, se incluye el precio del hijo
                        csUtilidades.WriteListToDatabase("ps_product_attribute", "(id_product_attribute, id_product, reference,price)", valuesProd, csUtilidades.MYSQL, true);

                        //la tabla ps_product_attribute_shop ha sido modificada al cambiar de versión, le han añadido el campo id_product
                        if (checkVersion.checkVersión("1.6.1"))

                            csUtilidades.WriteListToDatabase("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product,price)", valuesShop, csUtilidades.MYSQL, true);
                        else
                            csUtilidades.WriteListToDatabase("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", valuesShop, csUtilidades.MYSQL, true);


                        csUtilidades.WriteListToDatabase("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,id_shop_group,quantity, depends_on_stock, out_of_stock)", valuesStockDetail, csUtilidades.MYSQL, true);


                        valuesComb.Clear();
                        valuesProd.Clear();
                        valuesShop.Clear();
                        valuesStockDetail.Clear();

                        if (csGlobal.gestionPreciosHijos)
                        {
                            csUtilidades.ejecutarConsulta("update ps_product set price=0 where id_product in (select id_product from ps_product_attribute group by id_product)", true);
                            csUtilidades.ejecutarConsulta("update ps_product_shop set price=0 where id_product in (select id_product from ps_product_attribute group by id_product)", true);
                        }
                    }

                }
            }



            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                //Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void insertarAtributosArticulo(string lineasComb, string lineasProd, string lineasShop, string lineasStock)
        {
            mySqlConnect.InsertValoresEnTabla("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", lineasComb, "");
            mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product, reference)", lineasProd, "");

            if (checkVersion.checkVersión("1.6.1"))
            {
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product)", lineasShop, "");
            }

            else
            {
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", lineasShop, "");
            }

            //vigilar versiones porque pide el id_shop_group
            mySqlConnect.InsertValoresEnTabla("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,id_shop_group,quantity, depends_on_stock, out_of_stock)", lineasStock, "");
        }

        //Revisión de combinaciones 

        private DataTable actualizarCombinacionesArticulos(string articulo)
        {
            try
            {
                DataTable combinacionesA3 = new DataTable();
                DataTable combinacionesPS = new DataTable();
                DataTable nuevasTbCombinaciones = new DataTable();
                bool productoNuevo = false;
                bool borrarArticulo = true;

                int numCombinacionesA3 = 0;
                int numCombinacionesPS = 0;

                nuevasTbCombinaciones.Clear();
                nuevasTbCombinaciones.Columns.Add("ID_PRODUCT");


                string combinacionA3 = "";
                string product_id_A3 = "";
                string product_id_PS = "";

                //Cargo las Combinaciones de A3 para sistema (A3SIPSSI)
                combinacionesA3 = cargarCombinacionesA3(articulo, csGlobal.modoTallasYColores);

                //Cargo las Combinaciones de PS
                combinacionesPS = cargarCombinacionesPs(articulo);

                //compruebo que todos los artículos están mapeados en Prestashop
                foreach (DataRow filaPS in combinacionesPS.Rows)
                {
                    borrarArticulo = true;
                    product_id_PS = filaPS["id_product"].ToString();
                    foreach (DataRow filaA3 in combinacionesA3.Rows)
                    {
                        product_id_A3 = filaA3["KLS_ID_SHOP"].ToString();

                        //si existe el artículo en los dos lados no hago nada
                        if (product_id_A3 == product_id_PS)
                        {
                            borrarArticulo = false;
                            break;
                        }
                    }
                    if (borrarArticulo)
                    {
                        borrarCombinaciones(product_id_PS);
                    }


                }

                //compruebo que todos los artículos están mapeados en A3
                foreach (DataRow filaA3 in combinacionesA3.Rows)
                {
                    DataRow nuevaCombinacion = nuevasTbCombinaciones.NewRow();
                    product_id_A3 = filaA3["KLS_ID_SHOP"].ToString();
                    numCombinacionesA3 = Convert.ToInt32(filaA3["COMBINACIONES"].ToString());
                    productoNuevo = true;

                    foreach (DataRow filaPS in combinacionesPS.Rows)
                    {
                        product_id_PS = filaPS["id_product"].ToString();
                        if (product_id_A3 == product_id_PS)
                        {
                            productoNuevo = false;
                            numCombinacionesPS = Convert.ToInt32(filaPS["combinacionesPS"].ToString());
                            if (numCombinacionesA3 != numCombinacionesPS)
                            {
                                //HAY QUE VALIDAR SI HAY QUE AÑADIR O BORRAR
                                nuevaCombinacion["ID_PRODUCT"] = product_id_A3;
                                nuevasTbCombinaciones.Rows.Add(nuevaCombinacion);
                                //borrarCombinaciones(product_id_PS);
                            }
                        }
                    }
                    if (productoNuevo)
                    {
                        nuevaCombinacion["ID_PRODUCT"] = product_id_A3;
                        nuevasTbCombinaciones.Rows.Add(nuevaCombinacion);
                    }
                }
                //si devuelvo algúna fila, quiere decir que ese artículo tiene combinaciones nuevas
                return nuevasTbCombinaciones;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void comprobarStockAvailableConProductAttribute()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();

            DataTable a3 = new DataTable();
            DataTable ps = new DataTable();

            a3 = sql.cargarDatosTablaA3("");

            string consulta = " select ps_product_attribute.id_product, ps_product_attribute.id_product_attribute as a, ps_stock_available.id_product_attribute as b " +
                              " from ps_product_attribute " +
                              " left join ps_stock_available on ps_product_attribute.id_product_attribute = ps_stock_available.id_product_attribute " +
                              " where ps_stock_available.id_product_attribute is null";
            DataTable dt = mysql.cargarTabla(consulta);
            string campos = "id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock";
            string values = "";
            string id_product = "", id_product_attribute = "";
            int contador = 0;

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    id_product = dr["id_product"].ToString();
                    id_product_attribute = dr["a"].ToString();
                    values = id_product + "," + id_product_attribute + ",1,0,0,0,2";
                    csUtilidades.ejecutarConsulta("insert into ps_stock_available (" + campos + ") values (" + values + ")", true);
                }
            }
        }

        public DataTable tablaActualizarStockPS()
        {
            try
            {
                string id_productA3 = "";
                int atributosArticulo = 0;
                string id_productPS = "";
                int numeroAtributos = 0;
                string unidadesStockActualizadoA3 = "";
                string unidadesStockActualizadoPS = "";
                string idStockDisponible = "";
                bool actualizarStockPS = false;


                //Inicializo variables para guardar los id de los atributos tanto de A3 como de PS
                string idAtributo1A3 = "";
                string idAtributo2A3 = "";
                string idAtributo1PS = "";
                string idAtributo2PS = "";
                string idCombinacion = "";

                DataTable stockCombinacionesA3 = new DataTable();
                DataTable stockCombinacionesPS = new DataTable();
                DataTable combinacionesArticulosPS = new DataTable();

                comprobarStockAvailableConProductAttribute();

                stockCombinacionesA3 = cargarStockA3();

                //Inicializo el Stock de Prestashop para Atributos para compararlo con PS
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                mySqlConnect.actualizarValoresEnTablaMySQL("ps_stock_available", "quantity", "0", " id_product_attribute>0 ");

                stockCombinacionesPS = cargarStockPS();
                combinacionesArticulosPS = cargarCombinacionesArticuloPS("");

                DataTable stockActualizadoPS = new DataTable();
                stockActualizadoPS.Columns.Add("id_product");
                stockActualizadoPS.Columns.Add("id_product_attribute");
                stockActualizadoPS.Columns.Add("quantity");
                stockActualizadoPS.Columns.Add("quantity_old");
                stockActualizadoPS.Columns.Add("id_stock_available");
                stockActualizadoPS.Columns.Add("update");

                //Comienzo por A3 porque es el Stock que manda
                foreach (DataRow filaCombinacion in stockCombinacionesA3.Rows)
                {
                    id_productA3 = filaCombinacion["KLS_ID_SHOP"].ToString();
                    atributosArticulo = Convert.ToInt32(filaCombinacion["ATRIBUTOS"].ToString());
                    idAtributo1A3 = filaCombinacion["ID_ATRIBUTO1"].ToString();
                    idAtributo2A3 = filaCombinacion["ID_ATRIBUTO2"].ToString();
                    unidadesStockActualizadoA3 = filaCombinacion["STOCK"].ToString();

                    foreach (DataRow filaCombinacionPS in combinacionesArticulosPS.Rows)
                    {
                        id_productPS = filaCombinacionPS["id_product"].ToString();

                        if (id_productA3 == id_productPS)
                        {
                            numeroAtributos = Convert.ToInt32(filaCombinacionPS["atributos"].ToString());
                            idAtributo1PS = filaCombinacionPS["idAtributo1"].ToString();
                            idAtributo2PS = filaCombinacionPS["idAtributo2"].ToString();
                            idCombinacion = filaCombinacionPS["id_product_attribute"].ToString();

                            if (numeroAtributos == 2)
                            {
                                //Valido que coincide la combinación
                                if ((idAtributo1A3 == idAtributo1PS & idAtributo2A3 == idAtributo2PS) || (idAtributo1A3 == idAtributo2PS & idAtributo2A3 == idAtributo1PS))
                                {
                                    //si coincide activo la opción de comprobar si hay diferencias de stock
                                    actualizarStockPS = true;
                                }
                            }

                            //Valido que coincide la combinación
                            if (numeroAtributos == 1)
                            {
                                if (idAtributo1A3 == idAtributo1PS)
                                {
                                    actualizarStockPS = true;
                                }
                            }

                            if (actualizarStockPS)
                            {
                                foreach (DataRow fila in stockCombinacionesPS.Rows)
                                {
                                    if (idCombinacion == fila["id_product_attribute"].ToString())
                                    {
                                        unidadesStockActualizadoPS = fila["quantity"].ToString();
                                        idStockDisponible = fila["id_stock_available"].ToString();
                                        break;
                                    }
                                }

                                DataRow filaStockActualizado = stockActualizadoPS.NewRow();
                                filaStockActualizado["id_product"] = id_productA3;
                                filaStockActualizado["id_product_attribute"] = idCombinacion;
                                filaStockActualizado["quantity"] = unidadesStockActualizadoA3;
                                filaStockActualizado["quantity_old"] = unidadesStockActualizadoPS;
                                filaStockActualizado["id_stock_available"] = idStockDisponible;

                                if (unidadesStockActualizadoA3 != unidadesStockActualizadoPS)
                                {
                                    filaStockActualizado["update"] = "Y";
                                }
                                else
                                {
                                    filaStockActualizado["update"] = "N";
                                }
                                stockActualizadoPS.Rows.Add(filaStockActualizado);

                            }
                            actualizarStockPS = false;


                        }
                    }

                }

                return stockActualizadoPS;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void actualizarStockPS(DataTable tablaStockActualizadoPS)
        {
            try
            {
                string nuevoStockPS = "";
                string idStockAtributoProducto = "";

                csMySqlConnect mySqlConnect = new csMySqlConnect();

                foreach (DataRow filaStockActualizar in tablaStockActualizadoPS.Rows)
                {
                    if (filaStockActualizar["update"].ToString() == "Y")
                    {
                        nuevoStockPS = filaStockActualizar["quantity"].ToString();
                        idStockAtributoProducto = filaStockActualizar["id_stock_available"].ToString();
                        mySqlConnect.actualizarValoresEnTablaMySQL("ps_stock_available", "quantity", nuevoStockPS, "id_stock_available=" + idStockAtributoProducto);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //temporal
        public void actualizarComportamientoStockPS(DataTable tablaArticulos)
        {

            try
            {
                string nuevoStockPS = "";
                string product_id = "";
                string stock_disponible = "2";
                string idCombinacion = "0";
                string cantidad = "0";
                string idStockAtributoProducto = "";
                string lineasStock = "";
                string scriptBorrar = "";
                string campos = "(id_stock_available, id_product,id_product_attribute,id_shop,id_shop_group,quantity, depends_on_stock, out_of_stock)";
                int i = 0;

                csMySqlConnect mySqlConnect = new csMySqlConnect();

                //Obtengo el último ID y le sumo 1
                //Al crearse un artículo se crea una línea de stock (aunque sea 0)
                int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

                foreach (DataRow filaArticulos in tablaArticulos.Rows)
                {
                    product_id = filaArticulos["PRODUCT_ID"].ToString();
                    if (i > 0)
                    {
                        lineasStock = lineasStock + ",";
                        scriptBorrar = scriptBorrar + ",";
                    }

                    lineasStock = lineasStock + "(" + ultimoId.ToString() + "," + product_id + "," + idCombinacion + ",1,0," + cantidad + ",0," + stock_disponible + ")";
                    scriptBorrar = scriptBorrar + product_id;
                    ultimoId = ultimoId + 1;
                    i++;
                }
                mySqlConnect.consultaBorrar("delete from ps_stock_available where id_product in (" + scriptBorrar + ")");
                mySqlConnect.InsertValoresEnTabla("ps_stock_available", campos, lineasStock, "");
            }
            catch (Exception ex)
            {

            }


        }

        //Cargo en un datatable todas las combinaciones de Atributos de Prestahop, con sus codigos de combinación e indicadores de stock
        private DataTable cargarStockPS()
        {
            try
            {
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
                connection.Open();
                string queryLoadStockPS = "";
                queryLoadStockPS = "select " +
                                " ps_product_attribute.id_product,ps_product_attribute_combination.id_product_attribute,id_attribute ,ps_stock_available.quantity,id_stock_available " +
                                " from " +
                                " ps_product_attribute inner join ps_product_attribute_combination on " +
                                " ps_product_attribute_combination.id_product_attribute=ps_product_attribute.id_product_attribute " +
                                " inner join ps_stock_available on " +
                                " ps_stock_available.id_product=ps_product_attribute.id_product and ps_stock_available.id_product_attribute=ps_product_attribute.id_product_attribute " +
                                " order by ps_product_attribute.id_product,ps_product_attribute.id_product_attribute";

                MySqlCommand cmd = new MySqlCommand(queryLoadStockPS, connection);
                //Execute command
                //cmd.ExecuteNonQuery();
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(queryLoadStockPS, connection);
                DataTable tabla = new DataTable();
                myDA.Fill(tabla);

                //close connection
                connection.Close();
                return tabla;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private DataTable cargarCombinacionesAtributosShop(string product_id)
        {
            try
            {
                DataTable tabla = new DataTable();
                csMySqlConnect mySqlConnect = new csMySqlConnect();

                string queryCombinacionesArticulos = "";
                string anexo = "";
                if (product_id != "")
                {
                    anexo = "where ps_product_attribute.id_product=" + product_id;
                }
                queryCombinacionesArticulos = "select * " +
                                              " from ps_product_attribute_shop inner join ps_product_attribute " +
                                              " on ps_product_attribute.id_product_attribute = ps_product_attribute_shop.id_product_attribute " + anexo;
                if (csGlobal.isODBCConnection)
                {
                    OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    OdbcCommand cmd = new OdbcCommand(queryCombinacionesArticulos, Odbcconnection);
                    //Execute command
                    //cmd.ExecuteNonQuery();
                    OdbcDataAdapter myDA = new OdbcDataAdapter();
                    myDA.SelectCommand = new OdbcCommand(queryCombinacionesArticulos, Odbcconnection);
                    myDA.Fill(tabla);

                    //close connection
                    Odbcconnection.Close();
                }
                else
                {
                    MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(queryCombinacionesArticulos, connection);
                    //Execute command
                    //cmd.ExecuteNonQuery();
                    MySqlDataAdapter myDA = new MySqlDataAdapter();
                    myDA.SelectCommand = new MySqlCommand(queryCombinacionesArticulos, connection);
                    myDA.Fill(tabla);

                    //close connection
                    connection.Close();
                }
                

                return tabla;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        private DataTable cargarArticulosconCombinacionesA3NOPSSI(string id_product)
        {

            try
            {
                csSqlConnects sqlConnect = new csSqlConnects();
                string queryLoadQuery = "";
                string anexo = "";
                string codart = "";
                if (id_product != "")
                {
                    codart = sqlConnect.obtenerCampoTabla("select codart from articulo where KLS_ID_SHOP=" + id_product);
                    anexo = "WHERE (LTRIM(dbo.ARTICULO.KLS_ARTICULO_PADRE) = '" + codart + "')";
                }
                DataTable dt = new DataTable();


                queryLoadQuery = " SELECT " +
                    " LTRIM(dbo.ARTICULO.KLS_ARTICULO_PADRE) AS KLS_ARTICULO_PADRE, dbo.ARTICULO.KLS_ARTICULO_TIENDA, LTRIM(dbo.ARTICULO.CODART) AS CODART, " +
                    " dbo.ARTICULO.DESCART, AP.KLS_ID_SHOP AS IDSHOP_PADRE,dbo.ARTICULO.PRCVENTA,CASE WHEN dbo.ARTICULO.KLS_ARTICULO_BLOQUEAR = 'T' THEN 'T' ELSE 'F' END AS COMBINACION_VISIBLE " +

                    " FROM " +
                    " dbo.ARTICULO INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS_ART AS KLS_ATRIBUTOS_ART_1 ON dbo.ARTICULO.CODART = KLS_ATRIBUTOS_ART_1.CODART INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS ON KLS_ATRIBUTOS_ART_1.KLS_COD_ATRIBUTO = dbo.KLS_ATRIBUTOS.KLS_COD_ATRIBUTO LEFT OUTER JOIN " +
                    " dbo.ARTICULO AS AP ON dbo.ARTICULO.KLS_ARTICULO_PADRE = AP.CODART " +
                    anexo +
                    " GROUP BY LTRIM(dbo.ARTICULO.CODART), dbo.ARTICULO.DESCART, dbo.ARTICULO.KLS_ARTICULO_PADRE, dbo.ARTICULO.KLS_ARTICULO_TIENDA, AP.KLS_ID_SHOP,  " +
                    " dbo.ARTICULO.KLS_ARTICULO_BLOQUEAR,dbo.ARTICULO.PRCVENTA" +
                    " HAVING (dbo.ARTICULO.KLS_ARTICULO_PADRE <> '' AND AP.KLS_ID_SHOP>0) ORDER BY KLS_ARTICULO_PADRE ";

                SqlConnection dataConnection = new SqlConnection();

                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = new SqlDataAdapter(queryLoadQuery, dataConnection);

                a.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        /// <summary>
        /// Combinacion articulo - atributo
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        private DataTable cargarCombinacionesArticuloPS(string productId)
        {
            try
            {
                string anexo = "";
                if (productId != "")
                {
                    anexo = " where id_product=" + productId + " ";


                }
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                //MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
                //connection.Open();
                string queryCombinacionesArticulos = "";

                if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                {
                    //Miro las combinaciones (máximo 2 combinaciones posibles)
                    queryCombinacionesArticulos = "select " +
                        " id_product,ps_product_attribute.id_product_attribute, count(ps_product_attribute.id_product_attribute) as atributos, " +
                        " max(id_attribute) as idAtributo1, min(id_attribute) as idAtributo2  from ps_product_attribute_combination inner join ps_product_attribute on " +
                        " ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute " + anexo +
                        " group by ps_product_attribute.id_product_attribute";
                }
                else if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI")
                {
                    //Miro las combinaciones (reference=codart), tantos artículos de A3 como tenga el articulo padre asociado)
                    queryCombinacionesArticulos = "select " +
                        " id_product,ps_product_attribute.id_product_attribute, count(ps_product_attribute.id_product_attribute) as atributos, " +
                        " ps_product_attribute.reference " +
                        " from ps_product_attribute_combination " +
                        " inner join ps_product_attribute on  ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute " + anexo +
                        " group by ps_product_attribute.id_product_attribute,ps_product_attribute.reference";

                }

                return mySqlConnect.cargarTabla(queryCombinacionesArticulos);

                //MySqlCommand cmd = new MySqlCommand(queryCombinacionesArticulos, connection);
                //MySqlDataAdapter myDA = new MySqlDataAdapter();
                //myDA.SelectCommand = new MySqlCommand(queryCombinacionesArticulos, connection);
                //DataTable tabla = new DataTable();
                //myDA.Fill(tabla);

                ////close connection
                //connection.Close();
                //return tabla;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable cargarCombinacionesArticulosA3(string product_id, bool todos, string nuevaTalla = null, string familiaNuevaTalla = null)
        {
            string queryLoadCombinacionesArticulosA3 = "";
            try
            {

                string anexo = "";
                string articulosShop = "";
                string anexoNuevaTalla = "";
                string anexoMonotallas = "";
                string anexoIndusnow = "";
                string anexoCombinaciones = "";

                //Cargo las combinaciones de cada articulo y devuelve aquellas combinaciones a crear
                DataTable combinacionesArticulosA3 = new DataTable();

                // Nota: El + dentro del select en el case es para indicar el numero de atributos que tiene cada articulo
                if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                {
                    if (csGlobal.activarMonotallas == "Y")
                    {
                        anexoMonotallas = " ,FAMILIATALLA_1.KLS_MONOTALLA AS FAMVMONOTALLA, dbo.FAMILIATALLA.KLS_MONOTALLA AS FAMHMONOTALLA ";


                    }

                    articulosShop = " AND (dbo.ARTICULO.KLS_ID_SHOP > 0) ";
                    if (!string.IsNullOrEmpty(product_id))
                    {
                        anexo = " AND KLS_ID_SHOP= " + product_id + " ";

                    }
                    if (todos)
                    {
                        articulosShop = "";
                    }
                    if (nuevaTalla != null)
                    {
                        anexoNuevaTalla = " AND (LTRIM(dbo.ARTICULO.CODFAMTALLAH)='" + familiaNuevaTalla + "' OR LTRIM(dbo.ARTICULO.CODFAMTALLAV)='" + familiaNuevaTalla + "') " +
                                           " AND (LTRIM(dbo.TALLAS.CODTALLA)='" + nuevaTalla + "' OR LTRIM(TALLAS_1.CODTALLA)='" + nuevaTalla + "')";


                    }


                    //Antiguo

                    /*
                    queryLoadCombinacionesArticulosA3 = " SELECT  " +
                        " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END AS ATRIBUTO1, CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTO2,   " +
                        " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END + CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTOS, LTRIM(dbo.ARTICULO.CODART) AS CODART,   " +
                        " dbo.ARTICULO.DESCART, LTRIM(dbo.ARTICULO.CODFAMTALLAH) AS CODFAMTALLAH, LTRIM(dbo.ARTICULO.CODFAMTALLAV) AS CODFAMTALLAV, dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ID_SHOP,   " +
                        " LTRIM(dbo.TALLAS.CODTALLA) AS CODATRIBUTO1, LTRIM(TALLAS_1.CODTALLA) AS CODATRIBUTO2, dbo.TALLAS.ID AS ID_ATRIBUTO1, TALLAS_1.ID AS ID_ATRIBUTO2,  " +
                        " dbo.KLS_OBTENERSTOCK(CODART,dbo.TALLAS.CODTALLA,TALLAS_1.CODTALLA, '" + csGlobal.almacenA3 + "') AS STOCK,KLS_PERMITIRCOMPRAS, " +
                        " dbo.TallaVisible(LTRIM(dbo.ARTICULO.CODART), LTRIM(dbo.TALLAS.CODTALLA), LTRIM(TALLAS_1.CODTALLA)) AS COMBINACION_VISIBLE " + anexoMonotallas +
                        " FROM  " +
                        " dbo.TALLAS RIGHT OUTER JOIN " +
                        " dbo.FAMILIATALLA RIGHT OUTER JOIN " +
                        " dbo.ARTICULO LEFT OUTER JOIN " +
                        " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAV = FAMILIATALLA_1.CODFAMTALLA ON  " +
                        " dbo.FAMILIATALLA.CODFAMTALLA = dbo.ARTICULO.CODFAMTALLAH LEFT OUTER JOIN " +
                        " dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAV = TALLAS_1.CODFAMTALLA ON dbo.TALLAS.CODFAMTALLA = dbo.ARTICULO.CODFAMTALLAH " +
                        " WHERE (dbo.ARTICULO.TALLAS = 'T') " + articulosShop + anexo + anexoNuevaTalla;

                

                    */
                    //Nuevo

                    if (csGlobal.nodeTienda.Contains("INDUSN"))
                    {
                        anexoIndusnow= " AND dbo.KLS_OBTENERSTOCK(CODART, dbo.TALLAS.CODTALLA, TALLAS_1.CODTALLA, '" + csGlobal.almacenA3 + " >0' ";
                    }
                    
                    if (csGlobal.combinacionesVisibles)
                    {
                        anexoCombinaciones= " AND dbo.TallaVisible(LTRIM(dbo.ARTICULO.CODART), LTRIM(dbo.TALLAS.CODTALLA), LTRIM(TALLAS_1.CODTALLA)) = 1 ";
                    }


                    queryLoadCombinacionesArticulosA3 = " SELECT  " +
                            " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END AS ATRIBUTO1, CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTO2,   " +
                            " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END + CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTOS, LTRIM(dbo.ARTICULO.CODART) AS CODART,   " +
                            " dbo.ARTICULO.DESCART, LTRIM(dbo.ARTICULO.CODFAMTALLAH) AS CODFAMTALLAH, LTRIM(dbo.ARTICULO.CODFAMTALLAV) AS CODFAMTALLAV, dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ID_SHOP,   " +
                            " LTRIM(dbo.TALLAS.CODTALLA) AS CODATRIBUTO1, LTRIM(TALLAS_1.CODTALLA) AS CODATRIBUTO2, dbo.TALLAS.ID AS ID_ATRIBUTO1, TALLAS_1.ID AS ID_ATRIBUTO2,  " +
                            " dbo.KLS_OBTENERSTOCK(CODART,dbo.TALLAS.CODTALLA,TALLAS_1.CODTALLA, '" + csGlobal.almacenA3 + "') AS STOCK,KLS_PERMITIRCOMPRAS, " +
                            " dbo.TallaVisible(LTRIM(dbo.ARTICULO.CODART), LTRIM(dbo.TALLAS.CODTALLA), LTRIM(TALLAS_1.CODTALLA)) AS COMBINACION_VISIBLE " + anexoMonotallas +
                            " FROM  " +
                            " dbo.TALLAS RIGHT OUTER JOIN " +
                            " dbo.FAMILIATALLA RIGHT OUTER JOIN " +
                            " dbo.ARTICULO LEFT OUTER JOIN " +
                            " dbo.FAMILIATALLA AS FAMILIATALLA_1 ON dbo.ARTICULO.CODFAMTALLAV = FAMILIATALLA_1.CODFAMTALLA ON  " +
                            " dbo.FAMILIATALLA.CODFAMTALLA = dbo.ARTICULO.CODFAMTALLAH LEFT OUTER JOIN " +
                            " dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAV = TALLAS_1.CODFAMTALLA ON dbo.TALLAS.CODFAMTALLA = dbo.ARTICULO.CODFAMTALLAH " +

                            " WHERE (dbo.ARTICULO.TALLAS = 'T') " + articulosShop + anexo + anexoNuevaTalla + anexoIndusnow + anexoCombinaciones;

                }




                else if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI")
                {
                    articulosShop = " AND (AP.KLS_ID_SHOP > 0) ";
                    if (product_id != "")
                    {
                        anexo = " AND AP.KLS_ID_SHOP= " + product_id + " ";

                    }
                    if (todos)
                    {
                        articulosShop = "";
                    }

                    queryLoadCombinacionesArticulosA3 = " SELECT " +
                    " LTRIM(dbo.ARTICULO.CODART) AS CODART, dbo.ARTICULO.DESCART, dbo.ARTICULO.KLS_ARTICULO_PADRE,  " +
                    " dbo.ARTICULO.KLS_ARTICULO_TIENDA, AP.KLS_ID_SHOP AS KLS_ID_SHOP, KLS_ATRIBUTOS_ART_1.KLS_COD_ATRIBUTO,  " +
                    " dbo.KLS_ATRIBUTOS.KLS_VALOR_ATRIBUTO, dbo.KLS_ATRIBUTOS.KLS_DESCGROUP_PS, dbo.KLS_ATRIBUTOS.KLS_IDGROUP_PS, " +
                    " (SELECT COUNT(CODART) AS NUM_ATRIBUTOS FROM dbo.KLS_ATRIBUTOS_ART WHERE (CODART = dbo.ARTICULO.CODART)) AS NUM_ATRIBUTOS_ART, " +
                    " dbo.ARTICULO.KLS_ARTICULO_TIENDA AS COMBINACION_VISIBLE " +
                    " FROM " +
                    " dbo.ARTICULO INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS_ART AS KLS_ATRIBUTOS_ART_1 ON dbo.ARTICULO.CODART = KLS_ATRIBUTOS_ART_1.CODART INNER JOIN " +
                    " dbo.KLS_ATRIBUTOS ON KLS_ATRIBUTOS_ART_1.KLS_COD_ATRIBUTO = dbo.KLS_ATRIBUTOS.KLS_COD_ATRIBUTO LEFT OUTER JOIN " +
                    " dbo.ARTICULO AS AP ON dbo.ARTICULO.KLS_ARTICULO_PADRE = AP.CODART " +
                    " WHERE     (dbo.ARTICULO.KLS_ARTICULO_PADRE <> '') " + articulosShop + anexo +
                    " ORDER BY dbo.KLS_ATRIBUTOS.KLS_VALOR_ATRIBUTO, dbo.ARTICULO.DESCART ";


                }

                csSqlConnects sql = new csSqlConnects();
                return sql.cargarDatosTablaA3(queryLoadCombinacionesArticulosA3);

                //SqlConnection dataConnection = new SqlConnection();
                //csSqlConnects sqlConnect = new csSqlConnects();

                //dataConnection.ConnectionString = csGlobal.cadenaConexion;
                //dataConnection.Open();

                //csSqlScripts sqlScript = new csSqlScripts();
                //SqlDataAdapter adaptA3 = new SqlDataAdapter(queryLoadCombinacionesArticulosA3, dataConnection);

                //adaptA3.Fill(combinacionesArticulosA3);

                //return combinacionesArticulosA3;
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("QUERY: " + queryLoadCombinacionesArticulosA3 + "\n" + ex.ToString());
                return null;
            }
        }

        private DataTable cargarStockA3()
        {
            try
            {

                //Cargo las combinaciones de cada articulo y devuelve aquellas combinaciones a crear
                DataTable stockCombinacionesA3 = new DataTable();
                string queryLoadStockCombinacionesA3 = "";

                queryLoadStockCombinacionesA3 = " SELECT " +
                     " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END AS ATRIBUTO1, CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTO2, " +
                     " CASE WHEN CODFAMTALLAH IS NULL THEN 0 ELSE 1 END + CASE WHEN CODFAMTALLAV IS NULL THEN 0 ELSE 1 END AS ATRIBUTOS, dbo.ARTICULO.CODART,    " +
                     " dbo.ARTICULO.DESCART, dbo.ARTICULO.CODFAMTALLAH, dbo.ARTICULO.CODFAMTALLAV, dbo.ARTICULO.TALLAS, dbo.ARTICULO.KLS_ID_SHOP,    " +
                     " dbo.TALLAS.CODTALLA AS CODATRIBUTO1, TALLAS_1.CODTALLA AS CODATRIBUTO2, dbo.TALLAS.ID AS ID_ATRIBUTO1, TALLAS_1.ID AS ID_ATRIBUTO2,   " +
                     " dbo.KLS_OBTENERSTOCK(CODART,dbo.TALLAS.CODTALLA,TALLAS_1.CODTALLA, '" + csGlobal.almacenA3 + "') AS STOCK,KLS_PERMITIRCOMPRAS " +
                     " FROM  " +
                     " dbo.ARTICULO LEFT OUTER JOIN   " +
                     " dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAV = TALLAS_1.CODFAMTALLA LEFT OUTER JOIN   " +
                     " dbo.TALLAS ON dbo.ARTICULO.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA   " +
                     " WHERE (dbo.ARTICULO.TALLAS = 'T') AND (dbo.ARTICULO.KLS_ID_SHOP > 0) AND dbo.KLS_OBTENERSTOCK(CODART,dbo.TALLAS.CODTALLA,TALLAS_1.CODTALLA, '" + csGlobal.almacenA3 + "')>0 " +
                     " order by KLS_ID_SHOP";

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();

                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter adaptA3 = new SqlDataAdapter(queryLoadStockCombinacionesA3, dataConnection);

                adaptA3.Fill(stockCombinacionesA3);

                return stockCombinacionesA3;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        //Función para borrar las combinaciones no operativas o visibles
        private void borrarCombinaciones(string productId)
        {
            try
            {
                if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                {
                    borrarCombinacionesA3SIPSSI(productId);
                }

                else if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI")
                {
                    borrarCombinacionesA3NOPSSI(productId);
                }


            }
            catch (Exception ex)
            {

            }
        }


        //EL COMPORTAMIENTO DE SI HAY TALLAS EN A3 O NO, VARÍA EN CÓMO SE VERIFICA LA EXISTENCIA O NO
        //SI EN A3 HAY TALLAS Y COLORES EL MÁXIMO DE ATRIBUTOS QUE MIRAMOS ES 2
        //SI EN A3 NO HAY TALLAS, LO QUE VERIFICAMOS SON LAS REFERENCIAS DE A3 O CODART CONTRA LA TABLA ps_product_attribute
        private void borrarCombinacionesA3SIPSSI(string productId)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string idProductPS = "";
            string idProductA3 = "";
            int numAtributes = 0;
            string atributo1PS = "";
            string atributo2PS = "";
            string atributo1A3 = "";
            string atributo2A3 = "";
            string ps_product_attribute = "";
            bool combinacion2Delete = true;
            string atributoVisible = "0";

            DataTable combinacionesArticulosPS = new DataTable();
            combinacionesArticulosPS = cargarCombinacionesArticuloPS(productId);

            DataTable combinacionesArticulosA3 = new DataTable();
            combinacionesArticulosA3 = cargarCombinacionesArticulosA3(productId, false);

            DataTable combinacionesBorrarPS = new DataTable();
            combinacionesBorrarPS.Columns.Add("id_product_attribute");

            foreach (DataRow filaCombPS in combinacionesArticulosPS.Rows)
            {
                atributo1PS = filaCombPS["idAtributo1"].ToString();
                atributo2PS = filaCombPS["idAtributo2"].ToString();
                idProductPS = filaCombPS["id_product"].ToString();
                ps_product_attribute = filaCombPS["id_product_attribute"].ToString();

                //verifico el número de atributos
                if (atributo1PS == atributo2PS)
                    numAtributes = 1;
                else
                    numAtributes = 2;

                foreach (DataRow filaCombA3 in combinacionesArticulosA3.Rows)
                {
                    idProductA3 = filaCombA3["kls_id_shop"].ToString();
                    atributo1A3 = filaCombA3["id_Atributo1"].ToString();
                    atributo2A3 = filaCombA3["id_Atributo2"].ToString();
                    atributoVisible = filaCombA3["COMBINACION_VISIBLE"].ToString();

                    if (numAtributes == 1)
                    {
                        if (idProductA3 == idProductPS && (atributo1A3 == atributo1PS || atributo2A3 == atributo2PS))
                        {
                            if (filaCombA3["COMBINACION_VISIBLE"].ToString() == "0")
                            {
                                combinacion2Delete = true;
                                DataRow combinacionParaBorrar = combinacionesBorrarPS.NewRow();
                                combinacionParaBorrar["id_product_attribute"] = ps_product_attribute.ToString();
                                combinacionesBorrarPS.Rows.Add(combinacionParaBorrar);
                                break;
                            }
                            else
                            {
                                combinacion2Delete = false;
                                break;
                            }
                        }
                    }

                    if (numAtributes == 2)
                    {
                        if ((idProductA3 == idProductPS && atributo1A3 == atributo1PS && atributo2A3 == atributo2PS) || (idProductA3 == idProductPS && atributo1A3 == atributo2PS && atributo2A3 == atributo1PS))
                        {
                            if (filaCombA3["COMBINACION_VISIBLE"].ToString() == "0")
                            {
                                combinacion2Delete = true;
                                DataRow combinacionParaBorrar = combinacionesBorrarPS.NewRow();
                                combinacionParaBorrar["id_product_attribute"] = ps_product_attribute.ToString();
                                combinacionesBorrarPS.Rows.Add(combinacionParaBorrar);
                                break;
                            }
                            else
                            {
                                combinacion2Delete = false;
                                break;
                            }
                        }
                    }

                }
                //if (combinacion2Delete)
                //{
                //    DataRow combinacionParaBorrar = combinacionesBorrarPS.NewRow();
                //    combinacionParaBorrar["id_product_attribute"] = ps_product_attribute.ToString();
                //    combinacionesBorrarPS.Rows.Add(combinacionParaBorrar);
                //}
                combinacion2Delete = true;
            }


            if (combinacionesBorrarPS.Rows.Count > 0)
            {
                int i = 0;
                string filtroProducts = "";


                foreach (DataRow filaCombBorrar in combinacionesBorrarPS.Rows)
                {
                    productId = filaCombBorrar["id_product_attribute"].ToString();

                    if (i > 0)
                    {
                        filtroProducts = filtroProducts + ",";
                    }

                    filtroProducts = filtroProducts + productId;

                    i++;


                }
                mySqlConnect.borrar("ps_product_attribute", "id_product_attribute in (" + filtroProducts + ")");
                mySqlConnect.borrar("ps_product_attribute_combination", "id_product_attribute in (" + filtroProducts + ")");
                mySqlConnect.borrar("ps_product_attribute_shop", "id_product_attribute in )" + filtroProducts + ")");
                mySqlConnect.borrar("ps_stock_available", "id_product_attribute in (" + filtroProducts + ")");
            }
        }


        //EL COMPORTAMIENTO DE SI HAY TALLAS EN A3 O NO, VARÍA EN CÓMO SE VERIFICA LA EXISTENCIA O NO
        //SI EN A3 HAY TALLAS Y COLORES EL MÁXIMO DE ATRIBUTOS QUE MIRAMOS ES 2
        //SI EN A3 NO HAY TALLAS, LO QUE VERIFICAMOS SON LAS REFERENCIAS DE A3 O CODART CONTRA LA TABLA ps_product_attribute
        private void borrarCombinacionesA3NOPSSI(string productId)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string idProductPS = "";
            string idProductA3 = "";
            string atributo1PS = "";
            string atributo2PS = "";
            string atributo1A3 = "";
            string atributo2A3 = "";
            string ps_product_attribute = "";
            string atributoPS_reference = "";
            string atributoA3_codart = "";
            bool combinacion2Delete = false;


            DataTable combinacionesArticulosPS = new DataTable();
            combinacionesArticulosPS = cargarCombinacionesArticuloPS(productId);

            DataTable combinacionesArticulosA3 = new DataTable();
            combinacionesArticulosA3 = cargarCombinacionesArticulosA3(productId, false);

            DataTable combinacionesBorrarPS = new DataTable();
            combinacionesBorrarPS.Columns.Add("id_product_attribute");

            foreach (DataRow filaCombPS in combinacionesArticulosPS.Rows)
            {
                atributoPS_reference = filaCombPS["reference"].ToString();
                idProductPS = filaCombPS["id_product"].ToString();
                ps_product_attribute = filaCombPS["id_product_attribute"].ToString();

                foreach (DataRow filaCombA3 in combinacionesArticulosA3.Rows)
                {
                    idProductA3 = filaCombA3["kls_id_shop"].ToString();
                    atributoA3_codart = filaCombA3["codart"].ToString();

                    if (idProductA3 == idProductPS && (atributo1A3 == atributo1PS || atributo2A3 == atributo2PS))
                    {
                        //SOLO MOSTRAREMOS LAS COMBINACIONES MARCADAS COMO TRUE (NO BLOQUEADAS)    
                        if (filaCombA3["COMBINACION_VISIBLE"].ToString() != "T")
                        {
                            combinacion2Delete = true;
                            DataRow combinacionParaBorrar = combinacionesBorrarPS.NewRow();
                            combinacionParaBorrar["id_product_attribute"] = ps_product_attribute.ToString();
                            combinacionesBorrarPS.Rows.Add(combinacionParaBorrar);
                            break;
                        }
                        else
                        {
                            combinacion2Delete = false;
                            break;
                        }
                    }


                }
                //if (combinacion2Delete)
                //{
                //    DataRow combinacionParaBorrar = combinacionesBorrarPS.NewRow();
                //    combinacionParaBorrar["id_product_attribute"] = ps_product_attribute.ToString();
                //    combinacionesBorrarPS.Rows.Add(combinacionParaBorrar);
                //}
                combinacion2Delete = true;
            }


            if (combinacionesBorrarPS.Rows.Count > 0)
            {
                int i = 0;
                string filtroProducts = "";


                foreach (DataRow filaCombBorrar in combinacionesBorrarPS.Rows)
                {
                    productId = filaCombBorrar["id_product_attribute"].ToString();

                    if (i > 0)
                    {
                        filtroProducts = filtroProducts + ",";
                    }

                    filtroProducts = filtroProducts + productId;

                    i++;


                }
                mySqlConnect.borrar("ps_product_attribute", "id_product_attribute in (" + filtroProducts + ")");
                mySqlConnect.borrar("ps_product_attribute_combination", "id_product_attribute in (" + filtroProducts + ")");
                mySqlConnect.borrar("ps_product_attribute_shop", "id_product_attribute in )" + filtroProducts + ")");
                mySqlConnect.borrar("ps_stock_available", "id_product_attribute in (" + filtroProducts + ")");
            }
        }


        //Para conexiones lentas de internet, se utiliza esta esta consulta que tiene los datos en un datatable
        public bool existeCombinacionPSDT(string idProduct, string atributo1, string atributo2, string numAtributos, DataTable dtAtributos)
        {
            try
            {
                DataTable dt = new DataTable();
                //Se ha de validar la posibilidad de que los atributos estén alternados

                bool existe = false;
                string filtro1 = "";
                string filtro2 = "";


                //24/02/2017 añadimos el filtro del id_product porque no lo tenía en cuenta y buscaba que existiera la combinación, pero miraba en cualquier artículo
                if (numAtributos == "2")
                {
                    filtro1 = "id_product=" + idProduct + " AND idAtributo1=" + atributo1 + " AND idAtributo2=" + atributo2;
                    filtro2 = "id_product=" + idProduct + " AND idAtributo1=" + atributo2 + " AND idAtributo2=" + atributo1;
                }
                else
                {
                    if (atributo1 == "")
                    {
                        filtro1 = "id_product=" + idProduct + " AND idAtributo1=" + atributo1;
                        filtro2 = "id_product=" + idProduct + " AND idAtributo1=" + atributo2;
                    }
                    else
                    {
                        filtro1 = "id_product=" + idProduct + " AND idAtributo2=" + atributo2;
                        filtro2 = "id_product=" + idProduct + " AND idAtributo2=" + atributo1;
                    }
                }

                DataRow[] drs1 = dtAtributos.Select(filtro1);
                DataRow[] drs2 = dtAtributos.Select(filtro2);

                if (drs1.Length > 0 || drs2.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool existeCombinacionPSDT(string idProduct, string atributo1, string atributo2, DataTable dtAtributos)
        {
            try
            {
                DataTable dt = new DataTable();
                bool existe = false;
                string filtro1 = "";
                string filtro2 = "";
                int numCombinaciones = 0;

                if (atributo1 != "" && atributo2 != "")
                {
                    numCombinaciones = 2;
                }
                else
                {
                    numCombinaciones = 1;
                }

                if (numCombinaciones == 2)
                {
                    filtro1 = "idAtributo1=" + atributo1 + " AND idAtributo2=" + atributo2;
                    filtro2 = "idAtributo1=" + atributo2 + " AND idAtributo2=" + atributo1;
                }
                else
                {
                    if (atributo1 == "")
                    {
                        filtro1 = "idAtributo1=" + atributo1;
                        filtro2 = "idAtributo1=" + atributo2;
                    }
                    else
                    {
                        filtro1 = "idAtributo2=" + atributo2;
                        filtro2 = "idAtributo2=" + atributo1;
                    }
                }

                DataRow[] drs1 = dtAtributos.Select(filtro1);
                DataRow[] drs2 = dtAtributos.Select(filtro2);

                if (drs1.Length > 0 || drs2.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private bool existeCombinacionPS(string idProduct, string atributo1, string atributo2, string numAtributos)
        {
            try
            {
                bool existe = false;
                string filtro = "";

                if (numAtributos == "2")
                {
                    filtro = atributo1 + "," + atributo2;
                }
                else
                {
                    filtro = atributo1 + atributo2;
                }

                string query = "select  ps_product_attribute.id_product,ps_product_attribute.id_product_attribute, count(ps_product_attribute.id_product_attribute) as atributos, " +
                    " max(id_attribute) as idAtributo1, min(id_attribute) as idAtributo2 " +
                    " from " +
                    " ps_product_attribute_combination inner join ps_product_attribute " +
                    " on  ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute  " +
                    " where id_product= " + idProduct + " and id_attribute in (" + filtro + " ) " +
                    " group by ps_product_attribute.id_product_attribute " +
                    " having count(ps_product_attribute.id_product_attribute)= " + numAtributos;

                MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                DataTable tabla = new DataTable();
                myDA.Fill(tabla);

                //close connection
                connection.Close();

                if (tabla.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        private bool existeCombinacionPS(string idProduct, string codart)
        {
            try
            {
                bool existe = false;

                string query = "select  ps_product_attribute.id_product,ps_product_attribute.id_product_attribute,ps_product_attribute.reference " +
                    " from " +
                    " ps_product_attribute " +
                    " where id_product=" + idProduct + " and reference='" + codart + "'";

                MySqlConnection connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                DataTable tabla = new DataTable();
                myDA.Fill(tabla);

                //close connection
                connection.Close();

                if (tabla.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void generarTodasLasCombinacionesA3(string id_product)
        {
            DataTable combinacionesA3 = new DataTable();
            csTallasYColoresV2 TyC = new csTallasYColoresV2();
            csSqlConnects sqlConnect = new csSqlConnects();
            combinacionesA3 = TyC.cargarCombinacionesArticulosA3(id_product, true);
            if (csGlobal.modoTallasYColores == "A3SIPSSI")
            {
                sqlConnect.insertarTodasCombinacionesAtributosA3(combinacionesA3);
            }
        }

        public void generarTodasLasCombinacionesA3(string nuevaTalla = null, string familiaNuevaTalla = null)
        {
            DataTable combinacionesA3 = new DataTable();
            csTallasYColoresV2 TyC = new csTallasYColoresV2();
            csSqlConnects sqlConnect = new csSqlConnects();
            if (nuevaTalla == null)
            {
                sqlConnect.borrarDatosSqlTabla("KLS_COMBINACIONES_VISIBLES");
            }
            combinacionesA3 = TyC.cargarCombinacionesArticulosA3("", true, nuevaTalla, familiaNuevaTalla);
            sqlConnect.insertarTodasCombinacionesAtributosA3(combinacionesA3);
        }

        private void borrarStockTallasyColores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_stock_available", " id_product_attribute<>0 ");
        }

        public void borrarAsignacionTallasycolores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_product_attribute");
            mySqlConnect.borrar("ps_product_attribute_combination");
            mySqlConnect.borrar("ps_product_attribute_shop");
        }

        private void marcarFamiliasTallasMonoTalla()
        {
            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_MONOATRIBUTO"))
            {
                csSqlConnects sqlConnect = new csSqlConnects();
                string query;
                query = "UPDATE FAMILIATALLA SET  FAMILIATALLA.KLS_MONOTALLA='T' WHERE LTRIM(FAMILIATALLA.CODFAMTALLA) IN  " +
                            " (SELECT LTRIM(dbo.FAMILIATALLA.CODFAMTALLA) " +
                            " FROM " +
                            " dbo.FAMILIATALLA INNER JOIN " +
                            " dbo.TALLAS ON dbo.FAMILIATALLA.CODFAMTALLA = dbo.TALLAS.CODFAMTALLA " +
                            " GROUP BY dbo.FAMILIATALLA.CODFAMTALLA, dbo.FAMILIATALLA.DESCFAMTALLA, dbo.FAMILIATALLA.KLS_MONOTALLA " +
                            " HAVING (COUNT(dbo.TALLAS.CODTALLA) = 1)) ";
                csUtilidades.ejecutarConsulta(query, false);

            }
        }

        public void vaciarTablasTallasyColores()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_attribute_group");
            mySqlConnect.borrar("ps_attribute_group_lang");
            mySqlConnect.borrar("ps_attribute_group_shop");
            mySqlConnect.borrar("ps_attribute");
            mySqlConnect.borrar("ps_attribute_lang");
            mySqlConnect.borrar("ps_product_attribute");
            mySqlConnect.borrar("ps_product_attribute_combination");
            mySqlConnect.borrar("ps_product_attribute_shop");
            mySqlConnect.borrar("ps_product_attribute_image");
        }

        public void actualizarTotalesArticulo(string articulo)
        {
            string scriptUpdateTotal = "insert into ps_stock_available (id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock) " +
                            " (select id_product,0,1,0,sum(quantity),0,out_of_stock " +
                            " from ps_stock_available  where id_product_attribute<>0 and id_product=" + articulo + " group by id_product,out_of_stock)";
            csUtilidades.ejecutarConsulta(scriptUpdateTotal, true);

        }


        public void resetearAtributos(DataGridView dgv, bool formUtilidades=false)
        {
            DataTable articulos = new DataTable();
            articulos.Columns.Add("idProduct");
            int numfila = 0;
            csStocks stock = new csStocks();
            string producto = "";
            string codart = "";

            if (dgv.Rows.Count > 0)
            {
                int productos_seleccionados = dgv.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de resetear {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgv.SelectedRows)
                        {
                            DataRow fila = articulos.NewRow();
                            fila["idProduct"] = formUtilidades? item.Cells["id_product"].Value.ToString(): item.Cells["ID EN TIENDA"].Value.ToString();
                            articulos.Rows.Add(fila);
                            numfila++;
                            if (numfila == 500)
                            {
                                //TyC.resetearTyC(item.Cells["ID EN TIENDA"].Value.ToString());
                                TyC.resetearTyC(articulos);
                                articulos.Rows.Clear();
                                numfila = 0;
                            }
                        }
                        if (numfila < 500 && numfila > 0)
                        {
                            TyC.resetearTyC(articulos);
                        }

                        "Producto reseteado con exito".mb();

                        if ("Quieres sincronizar estos atributos?".what())
                        {
                            foreach (DataGridViewRow item in dgv.SelectedRows)
                            {
                                producto = formUtilidades ? item.Cells["id_product"].Value.ToString() : item.Cells["ID EN TIENDA"].Value.ToString();
                                codart = formUtilidades ? item.Cells["reference"].Value.ToString() : item.Cells["CODIGO"].Value.ToString(); 

                                //visible_combinations es si la tienda muestra todas las combinaciones
                                //por ejemplo Hebo, no muestra todas las combinaciones
                                if (csGlobal.ps_visible_combinations)
                                {
                                    csUtilidades.ejecutarConsulta("DELETE FROM KLS_COMBINACIONES_VISIBLES WHERE LTRIM(CODART)='" + codart + "'", false);
                                    TyC.generarTodasLasCombinacionesA3(producto);
                                }
                                TyC.sincronizarAtributosArticulos(producto);
                                actualizarTotalesArticulo(producto);
                            }
                            if (csGlobal.ocultarArticuloSiStock0)
                            {
                                stock.actualizarVisibilidadArticulosSinStockIndusnow();
                            }

                            "Atributos sincronizados".mb();
                        }
                        else
                        {
                            "Operación cancelada".mb();
                        }
                    }
                    else
                    {
                        "Operación cancelada".mb();
                    }
                }
            }

        }

    }
}
