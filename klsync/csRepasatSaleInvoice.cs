﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csRepasatSaleInvoice
    {
        public class Series
        {
            public int idSerie { get; set; }
            public int idEntidadSerie { get; set; }
            public string nomSerie { get; set; }
            public int contSerie { get; set; }
            public int contFacturasSerie { get; set; }
            public int contAbonosSerie { get; set; }
            public int contFacturasCompraSerie { get; set; }
            public int contAbonosCompraSerie { get; set; }
            public int? contPedidosSerie { get; set; }
            public int? contRecibosSerie { get; set; }
            public int serieAplicacionSerie { get; set; }
            public int? autorSerie { get; set; }
            public int contratoRepasatSerie { get; set; }
            public string fecAltaSerie { get; set; }
            public int activoSerie { get; set; }
            public int contPedidosVentaSerie { get; set; }
            public int contPedidosCompraSerie { get; set; }
            public int contAlbaranesVentaSerie { get; set; }
            public int contAlbaranesCompraSerie { get; set; }
            public int contAlbaranesRegularizacionSerie { get; set; }
        }

        public class Line
        {
            public int idLineaDocumento { get; set; }
            public int idEntidadLineaDocumento { get; set; }
            public int idDocumento { get; set; }
            public int idArticulo { get; set; }
            public object capituloLineaDocumento { get; set; }
            public int posicionLineaDocumento { get; set; }
            public string refArticuloLineaDocumento { get; set; }
            public string nomArticuloLineaDocumento { get; set; }
            public int? idTipoArticulo { get; set; }
            public string descArticuloLineaDocumento { get; set; }
            public string unidadesArticuloLineaDocumento { get; set; }
            public string precioVentaArticuloLineaDocumento { get; set; }
            public string tipoDescuentoLineaDocumento { get; set; }
            public string descuentoLineaDocumento { get; set; }
            public string totalBaseImponibleLineaDocumento { get; set; }
            public int? idTipoImpuesto { get; set; }
            public int? idUsuario { get; set; }
            public string fecAltaLineaDocumento { get; set; }
            public object idDocumentoOrigen { get; set; }
            public object idLineaPresupuesto { get; set; }
            public int? idLineaPedido { get; set; }
            public object idLineaAlbaran { get; set; }
            public object idLineaFactura { get; set; }
            public string totalImpuestosLineaDocumento { get; set; }
            public string totalLineaDocumento { get; set; }
            public object idAlmacen { get; set; }
            public string totalRecargoEquivalenciaLineaDocumento { get; set; }
            public object idActividad { get; set; }
            public object fecIniLineaPedidoRepasat { get; set; }
            public object fecFinLineaPedidoRepasat { get; set; }
        }

        public class Customer
        {
            public int idCli { get; set; }
            public int codCli { get; set; }
            public string nomCli { get; set; }
            public string aliasCli { get; set; }
            public string descCli { get; set; }
            public string tipoCli { get; set; }
            public int? idZonaGeo { get; set; }
            public int? idCam { get; set; }
            public string cifCli { get; set; }
            public string razonSocialCli { get; set; }
            public string tel1 { get; set; }
            public string emailCli { get; set; }
            public string webCli { get; set; }
            public string codExternoCli { get; set; }
            public int clienteAplicacionCli { get; set; }
            public int? idEmpresaClienteAplicacionCli { get; set; }
            public int? idTipoCli { get; set; }
            public int activoCli { get; set; }
            public int? idTrabajador { get; set; }
            public int? idDocuPago { get; set; }
            public int? idFormaPago { get; set; }
            public int? diaPago1Cli { get; set; }
            public object diaPago2Cli { get; set; }
            public object diaPago3Cli { get; set; }
            public object idUsuario { get; set; }
            public string fecAltaCli { get; set; }
            public string idIdioma { get; set; }
            public int porcentajeDescuentoCli { get; set; }
            public object idTarifa { get; set; }
            public object idAgencia { get; set; }
            public string cuentaContableClienteCli { get; set; }
            public object cuentaContableProveedorCli { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public object idEmpresaAccesoExterno { get; set; }
            public object idClasificacion { get; set; }
            public int? idSerie { get; set; }
            public object representadoCli { get; set; }
            public object comisionCli { get; set; }
            public int? descuentoCli { get; set; }
            public object idFormatoImpresionPresupuesto { get; set; }
            public object idFormatoImpresionPedidoVenta { get; set; }
            public object idFormatoImpresionPedidoCompra { get; set; }
            public object idFormatoImpresionAlbaranVenta { get; set; }
            public object idFormatoImpresionAlbaranCompra { get; set; }
            public object idFormatoImpresionFacturaVenta { get; set; }
            public object idFormatoImpresionFacturaCompra { get; set; }
        }

        public class Datum
        {
            public int idDocumento { get; set; }
            public int idEntidadDocumento { get; set; }
            public string tipoDocumento { get; set; }
            public int idCli { get; set; }
            public object idProyecto { get; set; }
            public int? idDireccionEnvio { get; set; }
            public int? idDireccionFacturacion { get; set; }
            public int idSerie { get; set; }
            public int numSerieDocumento { get; set; }
            public string refDocumento { get; set; }
            public int idFormaPago { get; set; }
            public object idCam { get; set; }
            public double totalBaseImponibleDocumento { get; set; }
            public double totalImpuestosDocumento { get; set; }
            public double totalDocumento { get; set; }
            public object totalPagadoDocumento { get; set; }
            public int anticipoDocumento { get; set; }
            public int porcentajeRetencionDocumento { get; set; }
            public double totalRetencionDocumento { get; set; }
            public object domiciliacionBancariaDocumento { get; set; }
            public int? idContratoCli { get; set; }
            public int? mesContratoCliDocumento { get; set; }
            public int? anoContratoCliDocumento { get; set; }
            public string fecDocumento { get; set; }
            public string fecContableDocumento { get; set; }
            public string fecAltaDocumento { get; set; }
            public int? idTrabajador { get; set; }
            public int? idUsuario { get; set; }
            public int idDocuPago { get; set; }
            public int portesDocumento { get; set; }
            public int? idTipoImpuestoPortes { get; set; }
            public double totalImpuestosPortesDocumento { get; set; }
            public double totalPortesDocumento { get; set; }
            public int porcentajeDescuentoDocumento { get; set; }
            public string codExternoDocumento { get; set; }
            public object idTarifa { get; set; }
            public object idAlmacen { get; set; }
            public int? idRegimenImpuesto { get; set; }
            public object idTipoRetencion { get; set; }
            public string totalRecargoEquivalenciaDocumento { get; set; }
            public string totalRetencionesDocumento { get; set; }
            public string totalRecargoEquivalenciaPortesDocumento { get; set; }
            public string fecEnvioMailDocumento { get; set; }
            public object idRepresentado { get; set; }
            public string observacionesCabeceraDocumento { get; set; }
            public string observacionesPieDocumento { get; set; }
            public int idTipoImpuesto { get; set; }
            public Series series { get; set; }
            public List<Line> lines { get; set; }
            public Customer customer { get; set; }
        }

        public class RootObject
        {
            public double total { get; set; }
            public int per_page { get; set; }
            public string current_page { get; set; }
            public int last_page { get; set; }
            public string next_page_url { get; set; }
            public object prev_page_url { get; set; }
            public List<Datum> data { get; set; }
        }
    }
}
