﻿namespace klsync
{
    partial class frPSWebService
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frPSWebService));
            this.cbApiObjetos = new System.Windows.Forms.ComboBox();
            this.btLoadApi = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tbOrigen = new System.Windows.Forms.TextBox();
            this.tbFinal = new System.Windows.Forms.TextBox();
            this.btExtract = new System.Windows.Forms.Button();
            this.dgvDocumentosPSXML = new System.Windows.Forms.DataGridView();
            this.dgvDetalleLineas = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product_attribute_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product_quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.product_price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_price_tax_excl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_price_tax_incl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_Atributo_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor_atributo_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_Atributo_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor_Atributo_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvBorrame = new System.Windows.Forms.DataGridView();
            this.btEnviarA3 = new System.Windows.Forms.Button();
            this.btExportarArticuloPS = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btModificarEstadoDoc = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.btOrderDetail = new System.Windows.Forms.Button();
            this.btXMLOrdere = new System.Windows.Forms.Button();
            this.btAddCustomers = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btLoadXMLClientes = new System.Windows.Forms.Button();
            this.btInsertCustomerAddress = new System.Windows.Forms.Button();
            this.btnWSRepasat = new System.Windows.Forms.Button();
            this.dropTablas = new System.Windows.Forms.ComboBox();
            this.btnContador = new System.Windows.Forms.Button();
            this.btnLoadDrop = new System.Windows.Forms.Button();
            this.contadorForm = new System.Windows.Forms.NumericUpDown();
            this.btnCreateClientA3 = new System.Windows.Forms.Button();
            this.btnServir = new System.Windows.Forms.Button();
            this.btnCheckFTP = new System.Windows.Forms.Button();
            this.btSendMail = new System.Windows.Forms.Button();
            this.btnUpdateFamilyProduct = new System.Windows.Forms.Button();
            this.btnArticulosBloqueados = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnCrearDomBancaA3 = new System.Windows.Forms.Button();
            this.btnFra2Pdf = new System.Windows.Forms.Button();
            this.btnSyncTYCNuevo = new System.Windows.Forms.Button();
            this.buttonAddAddress = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPSXML)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleLineas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBorrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contadorForm)).BeginInit();
            this.SuspendLayout();
            // 
            // cbApiObjetos
            // 
            this.cbApiObjetos.FormattingEnabled = true;
            this.cbApiObjetos.Items.AddRange(new object[] {
            "addresses",
            "carriers",
            "cart_rules",
            "carts",
            "categories",
            "combinations",
            "configurations",
            "contacts",
            "content_management_system",
            "countries",
            "currencies",
            "customer_messages",
            "customer_threads",
            "customers",
            "deliveries",
            "employees",
            "groups",
            "guests",
            "image_types",
            "images",
            "languages",
            "manufacturers",
            "order_carriers",
            "order_details",
            "order_discounts",
            "order_histories",
            "order_invoices",
            "order_payments",
            "order_states",
            "orders",
            "price_ranges",
            "product_feature_values",
            "product_features",
            "product_option_values",
            "product_options",
            "product_suppliers",
            "products",
            "search",
            " shop_groups",
            " shops",
            " specific_price_rules",
            " specific_prices",
            " states",
            " stock_availables",
            " stock_movement_reasons",
            " stock_movements",
            " stocks",
            " stores",
            " suppliers",
            " supply_order_details",
            " supply_order_histories",
            " supply_order_receipt_histories",
            " supply_order_states",
            " supply_orders",
            " tags",
            " tax_rule_groups",
            " tax_rules",
            " taxes",
            " translated_configurations",
            " warehouse_product_locations",
            " warehouses",
            " weight_ranges",
            " zones"});
            this.cbApiObjetos.Location = new System.Drawing.Point(12, 22);
            this.cbApiObjetos.Name = "cbApiObjetos";
            this.cbApiObjetos.Size = new System.Drawing.Size(121, 21);
            this.cbApiObjetos.TabIndex = 0;
            // 
            // btLoadApi
            // 
            this.btLoadApi.Location = new System.Drawing.Point(169, 22);
            this.btLoadApi.Name = "btLoadApi";
            this.btLoadApi.Size = new System.Drawing.Size(107, 23);
            this.btLoadApi.TabIndex = 1;
            this.btLoadApi.Text = "btLoadApi";
            this.btLoadApi.UseVisualStyleBackColor = true;
            this.btLoadApi.Click += new System.EventHandler(this.btLoadApi_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(169, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Actualizar Estado del Pedido";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(301, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(416, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "OBTENER DATOS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbOrigen
            // 
            this.tbOrigen.Location = new System.Drawing.Point(965, 22);
            this.tbOrigen.Name = "tbOrigen";
            this.tbOrigen.Size = new System.Drawing.Size(153, 20);
            this.tbOrigen.TabIndex = 6;
            this.tbOrigen.Text = "<![CDATA[0.00]]>";
            // 
            // tbFinal
            // 
            this.tbFinal.Location = new System.Drawing.Point(965, 51);
            this.tbFinal.Name = "tbFinal";
            this.tbFinal.Size = new System.Drawing.Size(153, 20);
            this.tbFinal.TabIndex = 7;
            // 
            // btExtract
            // 
            this.btExtract.Location = new System.Drawing.Point(1124, 22);
            this.btExtract.Name = "btExtract";
            this.btExtract.Size = new System.Drawing.Size(40, 23);
            this.btExtract.TabIndex = 8;
            this.btExtract.Text = "button3";
            this.btExtract.UseVisualStyleBackColor = true;
            this.btExtract.Click += new System.EventHandler(this.btExtract_Click);
            // 
            // dgvDocumentosPSXML
            // 
            this.dgvDocumentosPSXML.AllowUserToAddRows = false;
            this.dgvDocumentosPSXML.AllowUserToDeleteRows = false;
            this.dgvDocumentosPSXML.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocumentosPSXML.Location = new System.Drawing.Point(12, 153);
            this.dgvDocumentosPSXML.Name = "dgvDocumentosPSXML";
            this.dgvDocumentosPSXML.ReadOnly = true;
            this.dgvDocumentosPSXML.Size = new System.Drawing.Size(511, 194);
            this.dgvDocumentosPSXML.TabIndex = 45;
            // 
            // dgvDetalleLineas
            // 
            this.dgvDetalleLineas.AllowUserToAddRows = false;
            this.dgvDetalleLineas.AllowUserToDeleteRows = false;
            this.dgvDetalleLineas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDetalleLineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalleLineas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.product_id,
            this.product_attribute_id,
            this.product_quantity,
            this.product_name,
            this.product_price,
            this.unit_price_tax_excl,
            this.unit_price_tax_incl,
            this.Nom_Atributo_1,
            this.Valor_atributo_1,
            this.Nom_Atributo_2,
            this.Valor_Atributo_2});
            this.dgvDetalleLineas.Location = new System.Drawing.Point(15, 369);
            this.dgvDetalleLineas.Name = "dgvDetalleLineas";
            this.dgvDetalleLineas.ReadOnly = true;
            this.dgvDetalleLineas.Size = new System.Drawing.Size(1382, 210);
            this.dgvDetalleLineas.TabIndex = 47;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // product_id
            // 
            this.product_id.HeaderText = "product_id";
            this.product_id.Name = "product_id";
            this.product_id.ReadOnly = true;
            // 
            // product_attribute_id
            // 
            this.product_attribute_id.HeaderText = "product_attribute_id";
            this.product_attribute_id.Name = "product_attribute_id";
            this.product_attribute_id.ReadOnly = true;
            // 
            // product_quantity
            // 
            this.product_quantity.HeaderText = "product_quantity";
            this.product_quantity.Name = "product_quantity";
            this.product_quantity.ReadOnly = true;
            // 
            // product_name
            // 
            this.product_name.HeaderText = "product_name";
            this.product_name.Name = "product_name";
            this.product_name.ReadOnly = true;
            // 
            // product_price
            // 
            this.product_price.HeaderText = "product_price";
            this.product_price.Name = "product_price";
            this.product_price.ReadOnly = true;
            // 
            // unit_price_tax_excl
            // 
            this.unit_price_tax_excl.HeaderText = "unit_price_tax_excl";
            this.unit_price_tax_excl.Name = "unit_price_tax_excl";
            this.unit_price_tax_excl.ReadOnly = true;
            // 
            // unit_price_tax_incl
            // 
            this.unit_price_tax_incl.HeaderText = "unit_price_tax_incl";
            this.unit_price_tax_incl.Name = "unit_price_tax_incl";
            this.unit_price_tax_incl.ReadOnly = true;
            // 
            // Nom_Atributo_1
            // 
            this.Nom_Atributo_1.HeaderText = "Nom_Atributo_1";
            this.Nom_Atributo_1.Name = "Nom_Atributo_1";
            this.Nom_Atributo_1.ReadOnly = true;
            // 
            // Valor_atributo_1
            // 
            this.Valor_atributo_1.HeaderText = "Valor_Atributo_1";
            this.Valor_atributo_1.Name = "Valor_atributo_1";
            this.Valor_atributo_1.ReadOnly = true;
            // 
            // Nom_Atributo_2
            // 
            this.Nom_Atributo_2.HeaderText = "Nom_Atributo_2";
            this.Nom_Atributo_2.Name = "Nom_Atributo_2";
            this.Nom_Atributo_2.ReadOnly = true;
            // 
            // Valor_Atributo_2
            // 
            this.Valor_Atributo_2.HeaderText = "Valor_Atributo_2";
            this.Valor_Atributo_2.Name = "Valor_Atributo_2";
            this.Valor_Atributo_2.ReadOnly = true;
            // 
            // dgvBorrame
            // 
            this.dgvBorrame.AllowUserToAddRows = false;
            this.dgvBorrame.AllowUserToDeleteRows = false;
            this.dgvBorrame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBorrame.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBorrame.Location = new System.Drawing.Point(15, 585);
            this.dgvBorrame.Name = "dgvBorrame";
            this.dgvBorrame.ReadOnly = true;
            this.dgvBorrame.Size = new System.Drawing.Size(1382, 150);
            this.dgvBorrame.TabIndex = 48;
            // 
            // btEnviarA3
            // 
            this.btEnviarA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btEnviarA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnviarA3.ForeColor = System.Drawing.Color.Navy;
            this.btEnviarA3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEnviarA3.Location = new System.Drawing.Point(1310, 53);
            this.btEnviarA3.Name = "btEnviarA3";
            this.btEnviarA3.Size = new System.Drawing.Size(87, 35);
            this.btEnviarA3.TabIndex = 49;
            this.btEnviarA3.Text = "Enviar A3";
            this.btEnviarA3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEnviarA3.UseVisualStyleBackColor = true;
            this.btEnviarA3.Click += new System.EventHandler(this.btEnviarA3_Click);
            // 
            // btExportarArticuloPS
            // 
            this.btExportarArticuloPS.Location = new System.Drawing.Point(169, 80);
            this.btExportarArticuloPS.Name = "btExportarArticuloPS";
            this.btExportarArticuloPS.Size = new System.Drawing.Size(107, 23);
            this.btExportarArticuloPS.TabIndex = 50;
            this.btExportarArticuloPS.Text = "Crear Artículo";
            this.btExportarArticuloPS.UseVisualStyleBackColor = true;
            this.btExportarArticuloPS.Click += new System.EventHandler(this.btExportarArticuloPS_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(416, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(107, 37);
            this.button3.TabIndex = 51;
            this.button3.Text = "Crear Diccionario Idiomas";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btModificarEstadoDoc
            // 
            this.btModificarEstadoDoc.Location = new System.Drawing.Point(529, 51);
            this.btModificarEstadoDoc.Name = "btModificarEstadoDoc";
            this.btModificarEstadoDoc.Size = new System.Drawing.Size(92, 37);
            this.btModificarEstadoDoc.TabIndex = 52;
            this.btModificarEstadoDoc.Text = "Modificar Estado Doc";
            this.btModificarEstadoDoc.UseVisualStyleBackColor = true;
            this.btModificarEstadoDoc.Click += new System.EventHandler(this.btModificarEstadoDoc_Click);
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(529, 153);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(589, 194);
            this.textBox2.TabIndex = 53;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Navy;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(1310, 94);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(87, 35);
            this.button4.TabIndex = 54;
            this.button4.Text = "Parte 2";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btOrderDetail
            // 
            this.btOrderDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btOrderDetail.ForeColor = System.Drawing.Color.Navy;
            this.btOrderDetail.Image = ((System.Drawing.Image)(resources.GetObject("btOrderDetail.Image")));
            this.btOrderDetail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btOrderDetail.Location = new System.Drawing.Point(1014, 93);
            this.btOrderDetail.Name = "btOrderDetail";
            this.btOrderDetail.Size = new System.Drawing.Size(58, 35);
            this.btOrderDetail.TabIndex = 46;
            this.btOrderDetail.Text = "Cargar Datos xml";
            this.btOrderDetail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btOrderDetail.UseVisualStyleBackColor = true;
            this.btOrderDetail.Click += new System.EventHandler(this.btOrderDetail_Click);
            // 
            // btXMLOrdere
            // 
            this.btXMLOrdere.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btXMLOrdere.ForeColor = System.Drawing.Color.Navy;
            this.btXMLOrdere.Image = ((System.Drawing.Image)(resources.GetObject("btXMLOrdere.Image")));
            this.btXMLOrdere.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btXMLOrdere.Location = new System.Drawing.Point(851, 93);
            this.btXMLOrdere.Name = "btXMLOrdere";
            this.btXMLOrdere.Size = new System.Drawing.Size(137, 35);
            this.btXMLOrdere.TabIndex = 44;
            this.btXMLOrdere.Text = "Cargar Datos xml";
            this.btXMLOrdere.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btXMLOrdere.UseVisualStyleBackColor = true;
            this.btXMLOrdere.Click += new System.EventHandler(this.btXMLOrdere_Click);
            // 
            // btAddCustomers
            // 
            this.btAddCustomers.Location = new System.Drawing.Point(169, 106);
            this.btAddCustomers.Name = "btAddCustomers";
            this.btAddCustomers.Size = new System.Drawing.Size(107, 23);
            this.btAddCustomers.TabIndex = 55;
            this.btAddCustomers.Text = "Crear Clientes";
            this.btAddCustomers.UseVisualStyleBackColor = true;
            this.btAddCustomers.Click += new System.EventHandler(this.btAddCustomers_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "dgvDocumentosPSXML";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "dgvDetalleLineas";
            // 
            // btLoadXMLClientes
            // 
            this.btLoadXMLClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadXMLClientes.ForeColor = System.Drawing.Color.Navy;
            this.btLoadXMLClientes.Image = ((System.Drawing.Image)(resources.GetObject("btLoadXMLClientes.Image")));
            this.btLoadXMLClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadXMLClientes.Location = new System.Drawing.Point(387, 113);
            this.btLoadXMLClientes.Name = "btLoadXMLClientes";
            this.btLoadXMLClientes.Size = new System.Drawing.Size(190, 35);
            this.btLoadXMLClientes.TabIndex = 58;
            this.btLoadXMLClientes.Text = "Cargar XML Clientes";
            this.btLoadXMLClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLoadXMLClientes.UseVisualStyleBackColor = true;
            this.btLoadXMLClientes.Click += new System.EventHandler(this.btLoadXMLClientes_Click);
            // 
            // btInsertCustomerAddress
            // 
            this.btInsertCustomerAddress.Location = new System.Drawing.Point(583, 113);
            this.btInsertCustomerAddress.Name = "btInsertCustomerAddress";
            this.btInsertCustomerAddress.Size = new System.Drawing.Size(107, 34);
            this.btInsertCustomerAddress.TabIndex = 59;
            this.btInsertCustomerAddress.Text = "Insertar Dirección Cliente";
            this.btInsertCustomerAddress.UseVisualStyleBackColor = true;
            this.btInsertCustomerAddress.Click += new System.EventHandler(this.btInsertCustomerAddress_Click);
            // 
            // btnWSRepasat
            // 
            this.btnWSRepasat.Location = new System.Drawing.Point(696, 113);
            this.btnWSRepasat.Name = "btnWSRepasat";
            this.btnWSRepasat.Size = new System.Drawing.Size(123, 35);
            this.btnWSRepasat.TabIndex = 60;
            this.btnWSRepasat.Text = "Webservice Repasat";
            this.btnWSRepasat.UseVisualStyleBackColor = true;
            // 
            // dropTablas
            // 
            this.dropTablas.FormattingEnabled = true;
            this.dropTablas.Location = new System.Drawing.Point(678, 14);
            this.dropTablas.Name = "dropTablas";
            this.dropTablas.Size = new System.Drawing.Size(141, 21);
            this.dropTablas.TabIndex = 61;
            // 
            // btnContador
            // 
            this.btnContador.Location = new System.Drawing.Point(825, 40);
            this.btnContador.Name = "btnContador";
            this.btnContador.Size = new System.Drawing.Size(94, 34);
            this.btnContador.TabIndex = 63;
            this.btnContador.Text = "Actualizar contador";
            this.btnContador.UseVisualStyleBackColor = true;
            this.btnContador.Click += new System.EventHandler(this.btnContador_Click);
            // 
            // btnLoadDrop
            // 
            this.btnLoadDrop.Location = new System.Drawing.Point(825, 12);
            this.btnLoadDrop.Name = "btnLoadDrop";
            this.btnLoadDrop.Size = new System.Drawing.Size(94, 23);
            this.btnLoadDrop.TabIndex = 64;
            this.btnLoadDrop.Text = "Cargar Lista";
            this.btnLoadDrop.UseVisualStyleBackColor = true;
            this.btnLoadDrop.Click += new System.EventHandler(this.btnLoadDrop_Click);
            // 
            // contadorForm
            // 
            this.contadorForm.Location = new System.Drawing.Point(678, 53);
            this.contadorForm.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.contadorForm.Name = "contadorForm";
            this.contadorForm.Size = new System.Drawing.Size(140, 20);
            this.contadorForm.TabIndex = 65;
            this.contadorForm.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnCreateClientA3
            // 
            this.btnCreateClientA3.Location = new System.Drawing.Point(1197, 94);
            this.btnCreateClientA3.Name = "btnCreateClientA3";
            this.btnCreateClientA3.Size = new System.Drawing.Size(107, 35);
            this.btnCreateClientA3.TabIndex = 66;
            this.btnCreateClientA3.Text = "Crear Cliente A3";
            this.btnCreateClientA3.UseVisualStyleBackColor = true;
            this.btnCreateClientA3.Click += new System.EventHandler(this.btnCreateClientA3_Click);
            // 
            // btnServir
            // 
            this.btnServir.Location = new System.Drawing.Point(1084, 94);
            this.btnServir.Name = "btnServir";
            this.btnServir.Size = new System.Drawing.Size(107, 35);
            this.btnServir.TabIndex = 67;
            this.btnServir.Text = "Servir Documento";
            this.btnServir.UseVisualStyleBackColor = true;
            this.btnServir.Click += new System.EventHandler(this.btnServir_Click);
            // 
            // btnCheckFTP
            // 
            this.btnCheckFTP.Location = new System.Drawing.Point(1310, 12);
            this.btnCheckFTP.Name = "btnCheckFTP";
            this.btnCheckFTP.Size = new System.Drawing.Size(86, 35);
            this.btnCheckFTP.TabIndex = 68;
            this.btnCheckFTP.Text = "Check Dir FTP";
            this.btnCheckFTP.UseVisualStyleBackColor = true;
            this.btnCheckFTP.Click += new System.EventHandler(this.btnCheckFTP_Click);
            // 
            // btSendMail
            // 
            this.btSendMail.Location = new System.Drawing.Point(12, 58);
            this.btSendMail.Name = "btSendMail";
            this.btSendMail.Size = new System.Drawing.Size(107, 23);
            this.btSendMail.TabIndex = 69;
            this.btSendMail.Text = "Email";
            this.btSendMail.UseVisualStyleBackColor = true;
            this.btSendMail.Click += new System.EventHandler(this.btSendMail_Click);
            // 
            // btnUpdateFamilyProduct
            // 
            this.btnUpdateFamilyProduct.Location = new System.Drawing.Point(1197, 12);
            this.btnUpdateFamilyProduct.Name = "btnUpdateFamilyProduct";
            this.btnUpdateFamilyProduct.Size = new System.Drawing.Size(107, 35);
            this.btnUpdateFamilyProduct.TabIndex = 70;
            this.btnUpdateFamilyProduct.Text = "Actualizar ids Familias-Productos";
            this.btnUpdateFamilyProduct.UseVisualStyleBackColor = true;
            this.btnUpdateFamilyProduct.Click += new System.EventHandler(this.btnUpdateFamilyProduct_Click);
            // 
            // btnArticulosBloqueados
            // 
            this.btnArticulosBloqueados.Location = new System.Drawing.Point(1197, 47);
            this.btnArticulosBloqueados.Name = "btnArticulosBloqueados";
            this.btnArticulosBloqueados.Size = new System.Drawing.Size(107, 41);
            this.btnArticulosBloqueados.TabIndex = 71;
            this.btnArticulosBloqueados.Text = "Actualizar Art. Bloq";
            this.btnArticulosBloqueados.UseVisualStyleBackColor = true;
            this.btnArticulosBloqueados.Click += new System.EventHandler(this.btnArticulosBloqueados_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(283, 81);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(118, 23);
            this.button5.TabIndex = 72;
            this.button5.Text = "Nodos XML";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnCrearDomBancaA3
            // 
            this.btnCrearDomBancaA3.Location = new System.Drawing.Point(169, 130);
            this.btnCrearDomBancaA3.Name = "btnCrearDomBancaA3";
            this.btnCrearDomBancaA3.Size = new System.Drawing.Size(107, 23);
            this.btnCrearDomBancaA3.TabIndex = 72;
            this.btnCrearDomBancaA3.Text = "Crear Dom Banca";
            this.btnCrearDomBancaA3.UseVisualStyleBackColor = true;
            this.btnCrearDomBancaA3.Click += new System.EventHandler(this.btnCrearDomBancaA3_Click);
            // 
            // btnFra2Pdf
            // 
            this.btnFra2Pdf.Location = new System.Drawing.Point(1133, 153);
            this.btnFra2Pdf.Name = "btnFra2Pdf";
            this.btnFra2Pdf.Size = new System.Drawing.Size(107, 35);
            this.btnFra2Pdf.TabIndex = 73;
            this.btnFra2Pdf.Text = "Factura PDF";
            this.btnFra2Pdf.UseVisualStyleBackColor = true;
            this.btnFra2Pdf.Click += new System.EventHandler(this.btnFra2Pdf_Click);
            // 
            // btnSyncTYCNuevo
            // 
            this.btnSyncTYCNuevo.Location = new System.Drawing.Point(1133, 194);
            this.btnSyncTYCNuevo.Name = "btnSyncTYCNuevo";
            this.btnSyncTYCNuevo.Size = new System.Drawing.Size(107, 35);
            this.btnSyncTYCNuevo.TabIndex = 74;
            this.btnSyncTYCNuevo.Text = "Sincronizar TyC";
            this.btnSyncTYCNuevo.UseVisualStyleBackColor = true;
            this.btnSyncTYCNuevo.Click += new System.EventHandler(this.btnSyncTYCNuevo_Click);
            // 
            // buttonAddAddress
            // 
            this.buttonAddAddress.Location = new System.Drawing.Point(1133, 235);
            this.buttonAddAddress.Name = "buttonAddAddress";
            this.buttonAddAddress.Size = new System.Drawing.Size(107, 56);
            this.buttonAddAddress.TabIndex = 75;
            this.buttonAddAddress.Text = "Crear Direccion";
            this.buttonAddAddress.UseVisualStyleBackColor = true;
            this.buttonAddAddress.Click += new System.EventHandler(this.buttonAddAddress_Click);
            // 
            // frPSWebService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1427, 747);
            this.Controls.Add(this.buttonAddAddress);
            this.Controls.Add(this.btnSyncTYCNuevo);
            this.Controls.Add(this.btnFra2Pdf);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnCrearDomBancaA3);
            this.Controls.Add(this.btnArticulosBloqueados);
            this.Controls.Add(this.btnUpdateFamilyProduct);
            this.Controls.Add(this.btSendMail);
            this.Controls.Add(this.btnCheckFTP);
            this.Controls.Add(this.btnServir);
            this.Controls.Add(this.btnCreateClientA3);
            this.Controls.Add(this.contadorForm);
            this.Controls.Add(this.btnLoadDrop);
            this.Controls.Add(this.btnContador);
            this.Controls.Add(this.dropTablas);
            this.Controls.Add(this.btnWSRepasat);
            this.Controls.Add(this.btInsertCustomerAddress);
            this.Controls.Add(this.btLoadXMLClientes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btAddCustomers);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btModificarEstadoDoc);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btExportarArticuloPS);
            this.Controls.Add(this.btEnviarA3);
            this.Controls.Add(this.dgvBorrame);
            this.Controls.Add(this.dgvDetalleLineas);
            this.Controls.Add(this.btOrderDetail);
            this.Controls.Add(this.dgvDocumentosPSXML);
            this.Controls.Add(this.btXMLOrdere);
            this.Controls.Add(this.btExtract);
            this.Controls.Add(this.tbFinal);
            this.Controls.Add(this.tbOrigen);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btLoadApi);
            this.Controls.Add(this.cbApiObjetos);
            this.Name = "frPSWebService";
            this.Text = "frPSWebService";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPSXML)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleLineas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBorrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contadorForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbApiObjetos;
        private System.Windows.Forms.Button btLoadApi;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbOrigen;
        private System.Windows.Forms.TextBox tbFinal;
        private System.Windows.Forms.Button btExtract;
        private System.Windows.Forms.Button btXMLOrdere;
        private System.Windows.Forms.DataGridView dgvDocumentosPSXML;
        private System.Windows.Forms.Button btOrderDetail;
        private System.Windows.Forms.DataGridView dgvDetalleLineas;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn product_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn product_attribute_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn product_quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn product_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn product_price;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_price_tax_excl;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_price_tax_incl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_Atributo_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor_atributo_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_Atributo_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor_Atributo_2;
        private System.Windows.Forms.DataGridView dgvBorrame;
        private System.Windows.Forms.Button btEnviarA3;
        private System.Windows.Forms.Button btExportarArticuloPS;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btModificarEstadoDoc;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btAddCustomers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btLoadXMLClientes;
        private System.Windows.Forms.Button btInsertCustomerAddress;
        private System.Windows.Forms.Button btnWSRepasat;
        private System.Windows.Forms.ComboBox dropTablas;
        private System.Windows.Forms.Button btnContador;
        private System.Windows.Forms.Button btnLoadDrop;
        private System.Windows.Forms.NumericUpDown contadorForm;
        private System.Windows.Forms.Button btnCreateClientA3;
        private System.Windows.Forms.Button btnServir;
        private System.Windows.Forms.Button btnCheckFTP;
        private System.Windows.Forms.Button btSendMail;
        private System.Windows.Forms.Button btnUpdateFamilyProduct;
        private System.Windows.Forms.Button btnArticulosBloqueados;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnCrearDomBancaA3;
        private System.Windows.Forms.Button btnFra2Pdf;
        private System.Windows.Forms.Button btnSyncTYCNuevo;
        private System.Windows.Forms.Button buttonAddAddress;
    }
}