﻿namespace klsync
{
    partial class frCaptio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvCaptio = new System.Windows.Forms.DataGridView();
            this.toolStripCaptio = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonImportCSV = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.listBoxCaptio = new System.Windows.Forms.ListBox();
            this.splitContainerCaptio = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaptio)).BeginInit();
            this.toolStripCaptio.SuspendLayout();
            this.splitContainerCaptio.Panel1.SuspendLayout();
            this.splitContainerCaptio.Panel2.SuspendLayout();
            this.splitContainerCaptio.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCaptio
            // 
            this.dgvCaptio.AllowUserToAddRows = false;
            this.dgvCaptio.AllowUserToDeleteRows = false;
            this.dgvCaptio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCaptio.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCaptio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCaptio.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCaptio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCaptio.Location = new System.Drawing.Point(0, 0);
            this.dgvCaptio.Name = "dgvCaptio";
            this.dgvCaptio.ReadOnly = true;
            this.dgvCaptio.Size = new System.Drawing.Size(1120, 513);
            this.dgvCaptio.TabIndex = 5;
            // 
            // toolStripCaptio
            // 
            this.toolStripCaptio.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripCaptio.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonImportCSV,
            this.toolStripButton1});
            this.toolStripCaptio.Location = new System.Drawing.Point(0, 0);
            this.toolStripCaptio.Name = "toolStripCaptio";
            this.toolStripCaptio.Padding = new System.Windows.Forms.Padding(0, 5, 1, 0);
            this.toolStripCaptio.Size = new System.Drawing.Size(1144, 65);
            this.toolStripCaptio.TabIndex = 6;
            this.toolStripCaptio.Text = "toolStrip1";
            // 
            // toolStripButtonImportCSV
            // 
            this.toolStripButtonImportCSV.Image = global::klsync.Properties.Resources.csv_file_format_extension;
            this.toolStripButtonImportCSV.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonImportCSV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonImportCSV.Name = "toolStripButtonImportCSV";
            this.toolStripButtonImportCSV.Size = new System.Drawing.Size(108, 57);
            this.toolStripButtonImportCSV.Text = "Importar CSV";
            this.toolStripButtonImportCSV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonImportCSV.Click += new System.EventHandler(this.toolStripButtonImportCSV_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::klsync.Properties.Resources.a3erp;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(87, 57);
            this.toolStripButton1.Text = "Pasar a A3";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // listBoxCaptio
            // 
            this.listBoxCaptio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCaptio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.listBoxCaptio.FormattingEnabled = true;
            this.listBoxCaptio.ItemHeight = 20;
            this.listBoxCaptio.Location = new System.Drawing.Point(0, 0);
            this.listBoxCaptio.Name = "listBoxCaptio";
            this.listBoxCaptio.Size = new System.Drawing.Size(1120, 53);
            this.listBoxCaptio.TabIndex = 7;
            // 
            // splitContainerCaptio
            // 
            this.splitContainerCaptio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerCaptio.Location = new System.Drawing.Point(12, 68);
            this.splitContainerCaptio.Name = "splitContainerCaptio";
            this.splitContainerCaptio.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerCaptio.Panel1
            // 
            this.splitContainerCaptio.Panel1.Controls.Add(this.dgvCaptio);
            // 
            // splitContainerCaptio.Panel2
            // 
            this.splitContainerCaptio.Panel2.Controls.Add(this.listBoxCaptio);
            this.splitContainerCaptio.Size = new System.Drawing.Size(1120, 570);
            this.splitContainerCaptio.SplitterDistance = 513;
            this.splitContainerCaptio.TabIndex = 8;
            // 
            // frCaptio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 650);
            this.Controls.Add(this.splitContainerCaptio);
            this.Controls.Add(this.toolStripCaptio);
            this.Name = "frCaptio";
            this.Text = "Captio";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaptio)).EndInit();
            this.toolStripCaptio.ResumeLayout(false);
            this.toolStripCaptio.PerformLayout();
            this.splitContainerCaptio.Panel1.ResumeLayout(false);
            this.splitContainerCaptio.Panel2.ResumeLayout(false);
            this.splitContainerCaptio.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCaptio;
        private System.Windows.Forms.ToolStrip toolStripCaptio;
        private System.Windows.Forms.ToolStripButton toolStripButtonImportCSV;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ListBox listBoxCaptio;
        private System.Windows.Forms.SplitContainer splitContainerCaptio;
    }
}