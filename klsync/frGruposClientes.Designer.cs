﻿namespace klsync
{
    partial class frGruposClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripGrupoClientes = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCargarGrupos = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBusquedaGruposClientes = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.splitContainerGruposClientes = new System.Windows.Forms.SplitContainer();
            this.lblstep1 = new System.Windows.Forms.Label();
            this.dgvGruposClientes = new System.Windows.Forms.DataGridView();
            this.splitContainerClientesYGrupos = new System.Windows.Forms.SplitContainer();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.lblGrupoDestino = new System.Windows.Forms.Label();
            this.dgvGruposDestino = new System.Windows.Forms.DataGridView();
            this.btnaddcustomergroup = new System.Windows.Forms.Button();
            this.lblstep2 = new System.Windows.Forms.Label();
            this.statusStripGruposClientes = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSprang = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelContarClientes = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripGrupoClientes.SuspendLayout();
            this.splitContainerGruposClientes.Panel1.SuspendLayout();
            this.splitContainerGruposClientes.Panel2.SuspendLayout();
            this.splitContainerGruposClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposClientes)).BeginInit();
            this.splitContainerClientesYGrupos.Panel1.SuspendLayout();
            this.splitContainerClientesYGrupos.Panel2.SuspendLayout();
            this.splitContainerClientesYGrupos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposDestino)).BeginInit();
            this.statusStripGruposClientes.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripGrupoClientes
            // 
            this.toolStripGrupoClientes.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripGrupoClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCargarGrupos,
            this.toolStripButtonBusquedaGruposClientes,
            this.toolStripSeparator1,
            this.toolStripButton1});
            this.toolStripGrupoClientes.Location = new System.Drawing.Point(0, 0);
            this.toolStripGrupoClientes.Name = "toolStripGrupoClientes";
            this.toolStripGrupoClientes.Size = new System.Drawing.Size(1284, 60);
            this.toolStripGrupoClientes.TabIndex = 0;
            this.toolStripGrupoClientes.Text = "toolStrip1";
            // 
            // toolStripButtonCargarGrupos
            // 
            this.toolStripButtonCargarGrupos.Image = global::klsync.Properties.Resources.people;
            this.toolStripButtonCargarGrupos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCargarGrupos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCargarGrupos.Name = "toolStripButtonCargarGrupos";
            this.toolStripButtonCargarGrupos.Size = new System.Drawing.Size(182, 57);
            this.toolStripButtonCargarGrupos.Text = "Cargar grupos y clientes";
            this.toolStripButtonCargarGrupos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonCargarGrupos.Click += new System.EventHandler(this.toolStripButtonCargarGrupos_Click);
            // 
            // toolStripButtonBusquedaGruposClientes
            // 
            this.toolStripButtonBusquedaGruposClientes.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonBusquedaGruposClientes.Image = global::klsync.Properties.Resources.magnifying_glass;
            this.toolStripButtonBusquedaGruposClientes.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonBusquedaGruposClientes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBusquedaGruposClientes.Name = "toolStripButtonBusquedaGruposClientes";
            this.toolStripButtonBusquedaGruposClientes.Size = new System.Drawing.Size(159, 57);
            this.toolStripButtonBusquedaGruposClientes.Text = "Búsqueda de clientes";
            this.toolStripButtonBusquedaGruposClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonBusquedaGruposClientes.Click += new System.EventHandler(this.toolStripButtonBusquedaGruposClientes_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::klsync.Properties.Resources.personal19;
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(256, 57);
            this.toolStripButton1.Text = "Cargar clientes grupo seleccionado";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.ToolTipText = "Cargar clientes del grupo seleccionado";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // splitContainerGruposClientes
            // 
            this.splitContainerGruposClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerGruposClientes.Location = new System.Drawing.Point(0, 60);
            this.splitContainerGruposClientes.Name = "splitContainerGruposClientes";
            // 
            // splitContainerGruposClientes.Panel1
            // 
            this.splitContainerGruposClientes.Panel1.Controls.Add(this.lblstep1);
            this.splitContainerGruposClientes.Panel1.Controls.Add(this.dgvGruposClientes);
            // 
            // splitContainerGruposClientes.Panel2
            // 
            this.splitContainerGruposClientes.Panel2.Controls.Add(this.splitContainerClientesYGrupos);
            this.splitContainerGruposClientes.Panel2.Controls.Add(this.lblstep2);
            this.splitContainerGruposClientes.Size = new System.Drawing.Size(1284, 653);
            this.splitContainerGruposClientes.SplitterDistance = 577;
            this.splitContainerGruposClientes.SplitterWidth = 5;
            this.splitContainerGruposClientes.TabIndex = 1;
            // 
            // lblstep1
            // 
            this.lblstep1.AutoSize = true;
            this.lblstep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblstep1.Location = new System.Drawing.Point(3, 15);
            this.lblstep1.Name = "lblstep1";
            this.lblstep1.Size = new System.Drawing.Size(251, 20);
            this.lblstep1.TabIndex = 4;
            this.lblstep1.Text = "1. Selecciona un grupo de clientes";
            // 
            // dgvGruposClientes
            // 
            this.dgvGruposClientes.AllowUserToAddRows = false;
            this.dgvGruposClientes.AllowUserToDeleteRows = false;
            this.dgvGruposClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGruposClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGruposClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGruposClientes.Location = new System.Drawing.Point(0, 65);
            this.dgvGruposClientes.MultiSelect = false;
            this.dgvGruposClientes.Name = "dgvGruposClientes";
            this.dgvGruposClientes.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvGruposClientes.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGruposClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGruposClientes.Size = new System.Drawing.Size(672, 644);
            this.dgvGruposClientes.TabIndex = 3;
            this.dgvGruposClientes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvGruposClientes_CellMouseDoubleClick);
            // 
            // splitContainerClientesYGrupos
            // 
            this.splitContainerClientesYGrupos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerClientesYGrupos.Location = new System.Drawing.Point(6, 65);
            this.splitContainerClientesYGrupos.Name = "splitContainerClientesYGrupos";
            this.splitContainerClientesYGrupos.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerClientesYGrupos.Panel1
            // 
            this.splitContainerClientesYGrupos.Panel1.Controls.Add(this.dgvClientes);
            // 
            // splitContainerClientesYGrupos.Panel2
            // 
            this.splitContainerClientesYGrupos.Panel2.Controls.Add(this.lblGrupoDestino);
            this.splitContainerClientesYGrupos.Panel2.Controls.Add(this.btnaddcustomergroup);
            this.splitContainerClientesYGrupos.Panel2.Controls.Add(this.dgvGruposDestino);
            this.splitContainerClientesYGrupos.Size = new System.Drawing.Size(684, 506);
            this.splitContainerClientesYGrupos.SplitterDistance = 251;
            this.splitContainerClientesYGrupos.TabIndex = 7;
            // 
            // dgvClientes
            // 
            this.dgvClientes.AllowUserToAddRows = false;
            this.dgvClientes.AllowUserToDeleteRows = false;
            this.dgvClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Location = new System.Drawing.Point(0, 0);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvClientes.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvClientes.Size = new System.Drawing.Size(681, 206);
            this.dgvClientes.TabIndex = 2;
            this.dgvClientes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientes_CellDoubleClick);
            // 
            // lblGrupoDestino
            // 
            this.lblGrupoDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblGrupoDestino.Location = new System.Drawing.Point(3, 0);
            this.lblGrupoDestino.Name = "lblGrupoDestino";
            this.lblGrupoDestino.Size = new System.Drawing.Size(557, 39);
            this.lblGrupoDestino.TabIndex = 6;
            this.lblGrupoDestino.Text = "3. Selecciona el grupo de destino";
            // 
            // dgvGruposDestino
            // 
            this.dgvGruposDestino.AllowUserToAddRows = false;
            this.dgvGruposDestino.AllowUserToDeleteRows = false;
            this.dgvGruposDestino.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvGruposDestino.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGruposDestino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGruposDestino.Location = new System.Drawing.Point(3, 42);
            this.dgvGruposDestino.MultiSelect = false;
            this.dgvGruposDestino.Name = "dgvGruposDestino";
            this.dgvGruposDestino.ReadOnly = true;
            this.dgvGruposDestino.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvGruposDestino.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGruposDestino.Size = new System.Drawing.Size(678, 156);
            this.dgvGruposDestino.TabIndex = 4;
            // 
            // btnaddcustomergroup
            // 
            this.btnaddcustomergroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnaddcustomergroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnaddcustomergroup.Location = new System.Drawing.Point(3, 204);
            this.btnaddcustomergroup.Name = "btnaddcustomergroup";
            this.btnaddcustomergroup.Size = new System.Drawing.Size(678, 47);
            this.btnaddcustomergroup.TabIndex = 6;
            this.btnaddcustomergroup.Text = "4. Añadir al grupo de clientes";
            this.btnaddcustomergroup.UseVisualStyleBackColor = true;
            this.btnaddcustomergroup.Click += new System.EventHandler(this.btnaddcustomergroup_Click);
            // 
            // lblstep2
            // 
            this.lblstep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblstep2.Location = new System.Drawing.Point(9, 15);
            this.lblstep2.Name = "lblstep2";
            this.lblstep2.Size = new System.Drawing.Size(536, 47);
            this.lblstep2.TabIndex = 5;
            this.lblstep2.Text = "2. Selecciona los clientes que quieres que tengan el grupo seleccionado en el pri" +
    "mer paso";
            // 
            // statusStripGruposClientes
            // 
            this.statusStripGruposClientes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelInfo,
            this.toolStripStatusLabelSprang,
            this.toolStripStatusLabelContarClientes});
            this.statusStripGruposClientes.Location = new System.Drawing.Point(0, 683);
            this.statusStripGruposClientes.Name = "statusStripGruposClientes";
            this.statusStripGruposClientes.Size = new System.Drawing.Size(1284, 30);
            this.statusStripGruposClientes.TabIndex = 2;
            this.statusStripGruposClientes.Text = "statusStrip1";
            // 
            // toolStripStatusLabelInfo
            // 
            this.toolStripStatusLabelInfo.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.toolStripStatusLabelInfo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripStatusLabelInfo.Name = "toolStripStatusLabelInfo";
            this.toolStripStatusLabelInfo.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripStatusLabelInfo.Size = new System.Drawing.Size(553, 25);
            this.toolStripStatusLabelInfo.Text = "Clicando dos veces en un cliente se pueden ver los grupos a los que pertenece";
            // 
            // toolStripStatusLabelSprang
            // 
            this.toolStripStatusLabelSprang.Name = "toolStripStatusLabelSprang";
            this.toolStripStatusLabelSprang.Size = new System.Drawing.Size(660, 25);
            this.toolStripStatusLabelSprang.Spring = true;
            // 
            // toolStripStatusLabelContarClientes
            // 
            this.toolStripStatusLabelContarClientes.Name = "toolStripStatusLabelContarClientes";
            this.toolStripStatusLabelContarClientes.Size = new System.Drawing.Size(56, 25);
            this.toolStripStatusLabelContarClientes.Text = "0 clientes";
            // 
            // frGruposClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 713);
            this.Controls.Add(this.statusStripGruposClientes);
            this.Controls.Add(this.splitContainerGruposClientes);
            this.Controls.Add(this.toolStripGrupoClientes);
            this.KeyPreview = true;
            this.Name = "frGruposClientes";
            this.Text = "Grupos clientes";
            this.toolStripGrupoClientes.ResumeLayout(false);
            this.toolStripGrupoClientes.PerformLayout();
            this.splitContainerGruposClientes.Panel1.ResumeLayout(false);
            this.splitContainerGruposClientes.Panel1.PerformLayout();
            this.splitContainerGruposClientes.Panel2.ResumeLayout(false);
            this.splitContainerGruposClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposClientes)).EndInit();
            this.splitContainerClientesYGrupos.Panel1.ResumeLayout(false);
            this.splitContainerClientesYGrupos.Panel2.ResumeLayout(false);
            this.splitContainerClientesYGrupos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposDestino)).EndInit();
            this.statusStripGruposClientes.ResumeLayout(false);
            this.statusStripGruposClientes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripGrupoClientes;
        private System.Windows.Forms.ToolStripButton toolStripButtonCargarGrupos;
        private System.Windows.Forms.SplitContainer splitContainerGruposClientes;
        private System.Windows.Forms.ToolStripButton toolStripButtonBusquedaGruposClientes;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.Label lblstep1;
        private System.Windows.Forms.DataGridView dgvGruposClientes;
        private System.Windows.Forms.Label lblstep2;
        private System.Windows.Forms.Button btnaddcustomergroup;
        private System.Windows.Forms.StatusStrip statusStripGruposClientes;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSprang;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelContarClientes;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelInfo;
        private System.Windows.Forms.SplitContainer splitContainerClientesYGrupos;
        private System.Windows.Forms.DataGridView dgvGruposDestino;
        private System.Windows.Forms.Label lblGrupoDestino;
    }
}