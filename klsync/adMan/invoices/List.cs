﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace klsync.adMan.invoices
{
    class List
    {
        public class AWS
        {
            public string s3_bucket { get; set; }
            public string s3_key { get; set; }
        }

        public class Tax
        {
            public string name { get; set; }
            public string value { get; set; }
            public string amount { get; set; }
            public string applied { get; set; }
        }

        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string enterprise_id { get; set; }
            public string permalink { get; set; }
            public string external_id_es { get; set; }
            public string external_id_fr { get; set; }
        }

        public class Affiliate
        {
            public string company { get; set; }
            public string permalink { get; set; }
            public FiscalIdentity fiscal_identity { get; set; }
        }

        public class NumberSeries
        {
            public string prefix { get; set; }
            public string description { get; set; }
            public string updated_ts { get; set; }
        }

        public class Detail
        {
            public string name { get; set; }
            public string amount { get; set; }
            public string actions { get; set; }
            public string action_type { get; set; }
        }

        public class Result
        {
            public string accounting_date_tag { get; set; }
            public string amount { get; set; }
            public string date_tag { get; set; }
            public string status { get; set; }
            public string number { get; set; }
            public string accounting_number { get; set; }
            public string total_amount { get; set; }
            public string is_charge { get; set; }
            public string permalink { get; set; }
            public string mailed_at { get; set; }
            public string old_id { get; set; }
            public string external_id { get; set; }
            public string comments { get; set; }
            public string rejected_reason { get; set; }
            public string pdf_url { get; set; }
            public AWS AWS { get; set; }
            public string affiliate_invoice_number { get; set; }
            public string date { get; set; }
            public string from_date { get; set; }
            public string to_date { get; set; }
            public List<Tax> taxes { get; set; }
            public string payment_method { get; set; }
            public string enterprise { get; set; }
            public string currency { get; set; }
            public Affiliate affiliate { get; set; }
            public NumberSeries number_series { get; set; }
            public List<Detail> details { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
