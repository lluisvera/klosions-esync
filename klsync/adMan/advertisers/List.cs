﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.advertisers
{
    class List
    {

        // Código generado desde http://json2csharp.com/
        public class Currency
        {
            public object id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country
        {
            public int id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Result
        {
            public string id { get; set; }
            public string company { get; set; }
            public string permalink { get; set; }
            public bool locked { get; set; }
            public string ho_id { get; set; }
            public string status { get; set; }
            public string user_email { get; set; }
            public string user_first_name { get; set; }
            public string user_last_name { get; set; }
            public string user_phone { get; set; }
            public string user_cell_phone { get; set; }
            public string cif { get; set; }
            public string source { get; set; }
            public string medium { get; set; }
            public string term { get; set; }
            public string content { get; set; }
            public string campaign { get; set; }
            public string segment { get; set; }
            public object activation_permalink { get; set; }
            public bool activated { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string region { get; set; }
            public string zipcode { get; set; }
            public string user_ho_id { get; set; }
            public string invoicing_company { get; set; }
            public string invoicing_address1 { get; set; }
            public string invoicing_address2 { get; set; }
            public string invoicing_zipcode { get; set; }
            public string invoicing_country { get; set; }
            public string invoicing_cif { get; set; }
            public string mailing_company { get; set; }
            public string mailing_address1 { get; set; }
            public object mailing_address2 { get; set; }
            public string mailing_zipcode { get; set; }
            public string mailing_country { get; set; }
            public string expiring_days { get; set; }
            public object currency_id { get; set; }
            public string billing_country_id { get; set; }
            public string country_id { get; set; }
            public object enterprise_id { get; set; }
            public object admin_contact { get; set; }
            public string invoicing_city { get; set; }
            public string mailing_city { get; set; }
            public string deleted_reason { get; set; }
            public List<Currency> currency { get; set; }
            public List<Country> country { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
        }

        public class FiscalIdentity
        {
            public string external_id { get; set; }
            public string permalink { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
