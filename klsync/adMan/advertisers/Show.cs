﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.advertisers
{
    class Show
    {
        public class Currency
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
            public string created_ts { get; set; }
            public string updated_ts { get; set; }
            public string old_id { get; set; }
        }

        public class AdmanCompanyDetail
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail adman_company_detail { get; set; }
        }

        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string permalink { get; set; }
            public string external_id_es { get; set; }
            public string external_id_fr { get; set; }
            public string pipedrive_id { get; set; }
        }

        public class AdmanCompanyDetail2
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class BillingCountry
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail2 adman_company_detail { get; set; }
        }

        public class Data
        {
            public string id { get; set; }
            public string company { get; set; }
            public string permalink { get; set; }
            public string locked { get; set; }
            public string ho_id { get; set; }
            public string status { get; set; }
            public string user_email { get; set; }
            public string user_first_name { get; set; }
            public string user_last_name { get; set; }
            public string user_phone { get; set; }
            public string user_cell_phone { get; set; }
            public string cif { get; set; }
            public string source { get; set; }
            public string medium { get; set; }
            public string term { get; set; }
            public string content { get; set; }
            public string campaign { get; set; }
            public string segment { get; set; }
            public string activation_permalink { get; set; }
            public string activated { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string region { get; set; }
            public string zipcode { get; set; }
            public string user_ho_id { get; set; }
            public string invoicing_company { get; set; }
            public string invoicing_address1 { get; set; }
            public string invoicing_address2 { get; set; }
            public string invoicing_zipcode { get; set; }
            public string invoicing_country { get; set; }
            public string invoicing_cif { get; set; }
            public string mailing_company { get; set; }
            public string mailing_address1 { get; set; }
            public string mailing_address2 { get; set; }
            public string mailing_zipcode { get; set; }
            public string mailing_country { get; set; }
            public string expiring_days { get; set; }
            public string enterprise_id { get; set; }
            public string admin_contact { get; set; }
            public string invoicing_city { get; set; }
            public string mailing_city { get; set; }
            public string deleted_reason { get; set; }
            public string invoicing_country_id { get; set; }
            public Currency currency { get; set; }
            public Country country { get; set; }
            public BillingCountry billing_country { get; set; }
            public FiscalIdentity fiscal_identity { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Data data { get; set; }
        }
    }
}
