﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExcelDataReader;
using System.Collections;
using System.Text.RegularExpressions;

namespace klsync.adMan
{
    public partial class frFacturasVentaAdMan : Form
    {
        private static frFacturasVentaAdMan m_FormDefInstance;


        public static frFacturasVentaAdMan DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frFacturasVentaAdMan();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        private DataSet result;
        private DataTable datos;
        private string rutaArchivo;
        public Objetos.csCabeceraDoc[] cabeceras = null;
        public Objetos.csLineaDocumento[] lineas = null;
        public int contadorFras = 0;
        public frFacturasVentaAdMan()
        {
            InitializeComponent();
            traspasar.Enabled = false;
        }

        private DataSet leerExcel(Stream stream)
        {
            var result = new DataSet();

            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                result = reader.AsDataSet();
                return result;
            }
        }
        private void btnCargar_Click(object sender, EventArgs e)
        {
            if (dgvFacturas.Rows.Count > 0)
            {
                this.dgvFacturas.DataSource = null;
                dgvFacturas.Rows.Clear();
            }

            Stream myStream;
            OpenFileDialog abrirArchivo = new OpenFileDialog();


            //Con el GetFolderPath recuperamos la ruta de la carpeta Documentos (MyDocuments)
            abrirArchivo.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            abrirArchivo.Filter = "CSV files (*.csv)|*.csv|Excel Files|*.xls;*.xlsx";
            abrirArchivo.FilterIndex = 2;
            abrirArchivo.RestoreDirectory = true;
            if (abrirArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                rutaArchivo = abrirArchivo.FileName;
                try
                {
                    if ((myStream = abrirArchivo.OpenFile()) != null)
                    {
                        //Dataset completo
                        result = leerExcel(myStream);
                        datos = this.result.Tables[0];

                        //Validación del formato del excel. Se comprueba el numero de columnas y el nombre de la primera.
                        if (datos.Columns.Count==13 && datos.Rows[0].ItemArray[0].ToString().ToUpper().Equals("FECHA DOCUMENTO"))
                        {
                            transformarFacturas();
                            dateFormat();
                            
                            dgvFacturas.DataSource = this.datos;
                            cellFomat();

                            textBox1.Text = abrirArchivo.SafeFileName;
                            traspasar.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Formato de Excel incorrecto. ");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cierre el documento previamente. ");
                }
            }
        }

        private void transformarFacturas()
        {
            for (int i = 0; i < datos.Columns.Count; i++)
            {
                datos.Columns["Column" + i.ToString()].ColumnName = datos.Rows[0].ItemArray[i].ToString() != "" ? datos.Rows[0].ItemArray[i].ToString().ToUpper() : "Column" + i.ToString();
            }
            datos.Rows[0].Delete();
            datos.AcceptChanges();
        }

        private void dateFormat()
        {
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("es-ES");
            foreach (DataRow row in datos.Rows)
            {
                DateTime dateFeDoc = Convert.ToDateTime(row["FECHA DOCUMENTO"].ToString());
                DateTime datePeDesde = Convert.ToDateTime(row["PERIODO DESDE"].ToString());
                DateTime datePeHasta = Convert.ToDateTime(row["PERIODO HASTA"].ToString());
                row["FECHA DOCUMENTO"] = dateFeDoc.ToString("d", culture);
                row["PERIODO DESDE"] = datePeDesde.ToString("d", culture);
                row["PERIODO HASTA"] = datePeHasta.ToString("d", culture);
            }
            //dgvFacturas.Columns[3].Width = 60;
        }
        private void cellFomat()
        {
            dgvFacturas.Columns["NIF"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            

        }
        
        private void traspasar_Click(object sender, EventArgs e)
        {
            
            cargarFactVentas();
            traspasar.Enabled = false;
            a3erp.abrirEnlace();
            
            csGlobal.docDestino = "Factura";
            this.a3erp.generarDocA3Objeto(cabeceras, lineas, false, "no");
            a3erp.cerrarEnlace();
            MessageBox.Show("Traspaso realizado. "+contadorFras+"/"+ datos.Rows.Count+" lineas");
        }
        csa3erp a3erp = new csa3erp();


        /* public static string quitaAccentos (string text)
         {
             string textNorm = text.Normalize(NormalizationForm.FormD);
             Regex reg = new Regex("[^a-zA-Z0-9 ]");
             string textSinAcentos = reg.Replace(textNorm, "");
             return textSinAcentos;
         }
         */
        
        private void cargarFactVentas()
        {
            

            cabeceras = new Objetos.csCabeceraDoc[datos.Rows.Count];
            lineas = new Objetos.csLineaDocumento[datos.Rows.Count];
            foreach (DataRow fila in datos.Rows)
            {
                try
                {
                    //Se comprueba si el cliente esta añadido en la base de datos, si no, la factura(linea de excel) no se guarda
                    if (obtenCodIC(fila["NIF"].ToString())=="")
                    {
                        throw new Exception("El cliente NIF: "+ fila["NIF"].ToString() + " no existe. La linea no se ha traspasado. ");
                    }
                    if (fila["BASE IMPONIBLE"].ToString() == "0" || fila["BASE IMPONIBLE"].ToString() == "")
                    {
                        throw new Exception("La base imponible de una factura no puede ser 0 ");
                    }
                    cabeceras[contadorFras] = new Objetos.csCabeceraDoc();
                    cabeceras[contadorFras].fechaDoc = Convert.ToDateTime(fila["FECHA DOCUMENTO"].ToString());
                    cabeceras[contadorFras].serieDoc = fila["SERIE DOCUMENTO"].ToString();
                    cabeceras[contadorFras].tipoDoc = fila["TIPO DOCUMENTO"].ToString().ToUpper() == "FACTURA" ? fila["TIPO DOCUMENTO"].ToString().ToUpper() : "CREDIT_NOTE";
                    cabeceras[contadorFras].periodoDesde = Convert.ToDateTime(fila["PERIODO DESDE"].ToString());
                    cabeceras[contadorFras].periodoHasta = Convert.ToDateTime(fila["PERIODO HASTA"].ToString());
                    cabeceras[contadorFras].nombreIC = obtenNombreIC(fila["NIF"].ToString());
                    cabeceras[contadorFras].nif = fila["NIF"].ToString();
                    cabeceras[contadorFras].moneda = fila["DIVISA"].ToString();
                    cabeceras[contadorFras].comentariosCabecera = fila["DESCRIPCION"].ToString();
                    cabeceras[contadorFras].referencia = fila["REFERENCIA"].ToString();
                    cabeceras[contadorFras].bancoEmpresa = fila["BANCO DE COBRO"].ToString();
                    cabeceras[contadorFras].codIC = obtenCodIC(fila["NIF"].ToString());
                    cabeceras[contadorFras].extNumdDoc = contadorFras.ToString();
                   
                    lineas[contadorFras] = new Objetos.csLineaDocumento();
                    lineas[contadorFras].totalLinea = fila["BASE IMPONIBLE"].ToString();
                    lineas[contadorFras].precio = fila["BASE IMPONIBLE"].ToString();
                    lineas[contadorFras].descripcionArticulo = fila["DESCRIPCION"].ToString();
                    lineas[contadorFras].param1 = fila["DESCRIPCION AMPLIADA"].ToString();
                    lineas[contadorFras].cantidad = "1";
                    lineas[contadorFras].codigoArticulo = fila["ARTICULO"].ToString();
                    lineas[contadorFras].numCabecera = contadorFras.ToString();
                    contadorFras += 1;
                   
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            } 
        }
        private string obtenCodIC(string nif)
        {
            //A partir del NIF del cliente se obtiene el Codigo IC
            csSqlConnects sql = new csSqlConnects();
            DataTable dtCliIC = new DataTable();
            string codigoIC = "";

            dtCliIC = sql.cargarDatosTablaA3("select CODCLI from CLIENTES where NIFCLI = '" + nif + "'");
            foreach (DataRow row in dtCliIC.Rows)
            {
                codigoIC = row[0].ToString().Trim();
            }
            return codigoIC;
        }

        private string obtenNombreIC(string nif)
        {
            //A partir del NIF del cliente se obtiene el Codigo IC
            csSqlConnects sql = new csSqlConnects();
            DataTable dtCliIC = new DataTable();
            string nombreIC = "";

            dtCliIC = sql.cargarDatosTablaA3("select NOMCLI from CLIENTES where NIFCLI = '" + nif + "'");
            foreach (DataRow row in dtCliIC.Rows)
            {
                nombreIC = row[0].ToString().Trim();
            }
            return nombreIC;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                //Botón para abrir el excel cargado en la ruta.
                System.Diagnostics.Process.Start(rutaArchivo);
            }
            catch (Exception ex) { }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void frFacturasVentaAdMan_Load(object sender, EventArgs e)
        {

        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

