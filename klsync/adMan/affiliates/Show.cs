﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.affiliates
{
    class Show
    {
        public class Currency
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
            public string created_ts { get; set; }
            public string updated_ts { get; set; }
            public string old_id { get; set; }
        }

        public class AdmanCompanyDetail
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class AdmanCompanyDetail2
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail adman_company_detail { get; set; }
        }

        public class Enterprise
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public Currency currency { get; set; }
            public Country country { get; set; }
        }

        public class Country2
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail2 adman_company_detail { get; set; }
        }

        public class AffiliateUser
        {
            public string id { get; set; }
            public string ho_id { get; set; }
            public string email { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string phone { get; set; }
            public string cell_phone { get; set; }
            public string title { get; set; }
            public string status { get; set; }
            public string locked { get; set; }
            public string permalink { get; set; }
            public string login { get; set; }
            public string promo_code { get; set; }
            public string zip { get; set; }
            public string accept_news { get; set; }
            public string accept_conditions { get; set; }
            public string accept_payment_conditions { get; set; }
            public string snapshot { get; set; }
            public string successfully_logged_from_addon { get; set; }
            public string activation_permalink { get; set; }
            public string activated { get; set; }
            public string core_url { get; set; }
            public string source { get; set; }
            public string medium { get; set; }
            public string term { get; set; }
            public string content { get; set; }
            public string campaign { get; set; }
            public string segment { get; set; }
            public string language { get; set; }
            public string ever_passed_verification { get; set; }
            public string is_hard_blocked { get; set; }
            public string karma { get; set; }
            public string created_ts { get; set; }
            public string user_tracking_active { get; set; }
            public string user_tracking_domain { get; set; }
            public string last_password_reset { get; set; }
            public string last_login_ts { get; set; }
            public string allow_pr { get; set; }
            public string statistics_only { get; set; }
            public Country2 country { get; set; }
        }

        public class Country3
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Currency2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
            public string created_ts { get; set; }
            public string updated_ts { get; set; }
            public string old_id { get; set; }
        }

        public class Country4
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public Currency2 currency { get; set; }
            public Country4 country { get; set; }
        }

        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string permalink { get; set; }
            public string external_id_es { get; set; }
            public string external_id_fr { get; set; }
            public Country3 country { get; set; }
            public Enterprise2 enterprise { get; set; }
        }

        public class Data
        {
            public string permalink { get; set; }
            public string document { get; set; }
            public string document_type { get; set; }
            public string region { get; set; }
            public string city { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string zipcode { get; set; }
            public string ho_id { get; set; }
            public string website { get; set; }
            public string status { get; set; }
            public string locked { get; set; }
            public string company { get; set; }
            public string phone { get; set; }
            public string payment_method { get; set; }
            public string direct_deposit_account_holder { get; set; }
            public string direct_deposit_account_number { get; set; }
            public string direct_deposit_iban { get; set; }
            public string direct_deposit_swift { get; set; }
            public string direct_deposit_bank_name { get; set; }
            public string direct_deposit_bank_address { get; set; }
            public string direct_deposit_bank_city { get; set; }
            public string direct_deposit_routing_number { get; set; }
            public string direct_deposit_other_details { get; set; }
            public string paypal_email { get; set; }
            public string invoicing_status { get; set; }
            public string wants_iva { get; set; }
            public string fiscal_status { get; set; }
            public string wants_irpf { get; set; }
            public string own_bill { get; set; }
            public string kind { get; set; }
            public Enterprise enterprise { get; set; }
            public List<AffiliateUser> affiliate_users { get; set; }
            public FiscalIdentity fiscal_identity { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Data data { get; set; }
        }
    }
}
