﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.affiliates
{
    class List
    {
        // Código generado desde http://json2csharp.com/
        public class AffiliateUser
        {
            public string id { get; set; }
            public string ho_id { get; set; }
            public string email { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string phone { get; set; }
            public string cell_phone { get; set; }
            public string title { get; set; }
            public string status { get; set; }
            public bool locked { get; set; }
            public string permalink { get; set; }
            public string login { get; set; }
            public string password { get; set; }
            public object promo_code { get; set; }
            public string zip { get; set; }
            public string accept_news { get; set; }
            public string accept_conditions { get; set; }
            public string accept_payment_conditions { get; set; }
            public string snapshot { get; set; }
            public string successfully_logged_from_addon { get; set; }
            public string activation_permalink { get; set; }
            public string activated { get; set; }
            public string core_url { get; set; }
            public string source { get; set; }
            public string medium { get; set; }
            public string term { get; set; }
            public string content { get; set; }
            public string campaign { get; set; }
            public string segment { get; set; }
            public string language { get; set; }
            public string ever_passed_verification { get; set; }
            public string is_hard_blocked { get; set; }
            public string karma { get; set; }
            public object user_tracking_active { get; set; }
            public object user_tracking_domain { get; set; }
            public string last_password_reset { get; set; }
            public long last_login_ts { get; set; }
            public bool allow_pr { get; set; }
            public bool statistics_only { get; set; }
        }

        public class Result
        {
            public string permalink { get; set; }
            public string document { get; set; }
            public string document_type { get; set; }
            public string region { get; set; }
            public string city { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string zipcode { get; set; }
            public int ho_id { get; set; }
            public string website { get; set; }
            public string status { get; set; }
            public bool locked { get; set; }
            public string company { get; set; }
            public string phone { get; set; }
            public string payment_method { get; set; }
            public string direct_deposit_account_holder { get; set; }
            public string direct_deposit_account_number { get; set; }
            public string direct_deposit_iban { get; set; }
            public string direct_deposit_swift { get; set; }
            public string direct_deposit_bank_name { get; set; }
            public string direct_deposit_bank_address { get; set; }
            public string direct_deposit_bank_city { get; set; }
            public string direct_deposit_routing_number { get; set; }
            public string direct_deposit_other_details { get; set; }
            public string paypal_email { get; set; }
            public int invoicing_status { get; set; }
            public bool wants_iva { get; set; }
            public int fiscal_status { get; set; }
            public bool wants_irpf { get; set; }
            public bool own_bill { get; set; }
            public int kind { get; set; }
            public List<object> enterprise { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
            public List<AffiliateUser> affiliate_user { get; set; }
        }

        public class FiscalIdentity
        {
            public string external_id { get; set; }
            public string permalink { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
