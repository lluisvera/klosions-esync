﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.third_invoices
{
    class List
    {
        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string enterprise_id { get; set; }
            public string permalink { get; set; }
            public string external_id_es { get; set; }
            public string external_id_fr { get; set; }
            public List<Enterprise2> enterprise { get; set; }
        }

        public class Supplier
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string phone { get; set; }
            public string paypal_email { get; set; }
            public string permalink { get; set; }
            public string document { get; set; }
            public string payment_method { get; set; }
            public string pipedrive_id { get; set; }
            public string zipcode { get; set; }
            public string account_type { get; set; }
            public string identifier_type { get; set; }
            public string account_number { get; set; }
            public string identifier_number { get; set; }
            public string accounting_number { get; set; }
            public string status { get; set; }
            public List<object> affiliate_user { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
        }

        public class Currency
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency> currency { get; set; }
            public List<Country> country { get; set; }
        }

        public class Currency2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country2
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency2> currency { get; set; }
            public List<Country2> country { get; set; }
        }

       

        public class Affiliate
        {
            public string permalink { get; set; }
            public string document { get; set; }
            public string document_type { get; set; }
            public string region { get; set; }
            public string city { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string zipcode { get; set; }
            public string ho_id { get; set; }
            public string website { get; set; }
            public string status { get; set; }
            public string locked { get; set; }
            public string company { get; set; }
            public string phone { get; set; }
            public string payment_method { get; set; }
            public string direct_deposit_account_holder { get; set; }
            public string direct_deposit_account_number { get; set; }
            public string direct_deposit_iban { get; set; }
            public string direct_deposit_swift { get; set; }
            public string direct_deposit_bank_name { get; set; }
            public string direct_deposit_bank_address { get; set; }
            public string direct_deposit_bank_city { get; set; }
            public string direct_deposit_routing_number { get; set; }
            public string direct_deposit_other_details { get; set; }
            public string paypal_email { get; set; }
            public string invoicing_status { get; set; }
            public string wants_iva { get; set; }
            public string fiscal_status { get; set; }
            public string wants_irpf { get; set; }
            public string own_bill { get; set; }
            public string kind { get; set; }
            public string enterprise_id { get; set; }
            public List<Enterprise> enterprise { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
        }

        public class Currency3
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Currency4
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country3
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise3
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency4> currency { get; set; }
            public List<Country3> country { get; set; }
        }

        public class Tax
        {
            public string name { get; set; }
            public string value { get; set; }
            public string kind { get; set; }
            public string display_name { get; set; }
        }

        public class ThirdInvoiceItem
        {
            public string pipedrive_id { get; set; }
            public string amount { get; set; }
            public string status { get; set; }
            public string prepago { get; set; }
            public string imported_ts { get; set; }
            public string date_tag { get; set; }
        }

        public class Result
        {
            public string accounting_date_tag { get; set; }
            public string accounting_number { get; set; }
            public List<Affiliate> affiliate { get; set; }
            public string amount { get; set; }
            public string approved_tag { get; set; }
            public string client_premium_proof { get; set; }
            public string comments_approved { get; set; }
            public string comments_paid { get; set; }
            public string comments_posted { get; set; }
            public string comments_ready_to_pay { get; set; }
            public string comments_rejected { get; set; }
            public string comments_revision { get; set; }
            public string comments_upload { get; set; }
            public List<Currency3> currency { get; set; }
            public string date { get; set; }
            public string date_tag { get; set; }
            public string document_url { get; set; }
            public string due_date_tag { get; set; }
            public string end_date_tag { get; set; }
            public List<Enterprise3> enterprise { get; set; }
            public string expiring_days { get; set; }
            public string external_id { get; set; }
            public string notes { get; set; }
            public string notes_date_tag { get; set; }
            public string notes_status { get; set; }
            public string number { get; set; }
            public string operation { get; set; }
            public string paid_tag { get; set; }
            public string payment_amount { get; set; }
            public string payment_currency_id { get; set; }
            public string payment_period { get; set; }
            public string permalink { get; set; }
            public string post_url { get; set; }
            public string posted_tag { get; set; }
            public string prepago { get; set; }
            public string ready_to_pay_tag { get; set; }
            public string rejected_reason { get; set; }
            public string rejected_tag { get; set; }
            public string revision_tag { get; set; }
            public string start_date_tag { get; set; }
            public string status { get; set; }
            public List<Supplier> supplier { get; set; }
            public string supplier_account_number { get; set; }
            public string supplier_account_type { get; set; }
            public string supplier_address1 { get; set; }
            public string supplier_address2 { get; set; }
            public string supplier_city { get; set; }
            public string supplier_document { get; set; }
            public string supplier_identifier_number { get; set; }
            public string supplier_identifier_type { get; set; }
            public string supplier_name { get; set; }
            public string supplier_payment_method { get; set; }
            public string supplier_paypal_email { get; set; }
            public string supplier_phone { get; set; }
            public string supplier_zipcode { get; set; }
            public List<Tax> tax { get; set; }
            public List<ThirdInvoiceItem> third_invoice_item { get; set; }
            public string total_amount { get; set; }
            public string uploaded_tag { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
