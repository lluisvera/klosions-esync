﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.suppliers
{
    class Show
    {
        public class AdmanCompanyDetail
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail adman_company_detail { get; set; }
        }

        public class Currency
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
            public string created_ts { get; set; }
            public string updated_ts { get; set; }
            public string old_id { get; set; }
        }

        public class AdmanCompanyDetail2
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
        }

        public class Country2
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
            public AdmanCompanyDetail2 adman_company_detail { get; set; }
        }

        public class Enterprise
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public Currency currency { get; set; }
            public Country2 country { get; set; }
        }

        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string permalink { get; set; }
            public string external_id_es { get; set; }
            public string external_id_fr { get; set; }
            public Country country { get; set; }
            public Enterprise enterprise { get; set; }
        }

        public class Data
        {
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string phone { get; set; }
            public string paypal_email { get; set; }
            public string permalink { get; set; }
            public string document { get; set; }
            public string payment_method { get; set; }
            public string pipedrive_id { get; set; }
            public string zipcode { get; set; }
            public string account_type { get; set; }
            public string identifier_type { get; set; }
            public string account_number { get; set; }
            public string identifier_number { get; set; }
            public string accounting_number { get; set; }
            public string status { get; set; }
            public List<object> taxes { get; set; }
            public FiscalIdentity fiscal_identity { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Data data { get; set; }
        }
    }
}
