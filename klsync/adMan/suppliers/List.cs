﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.suppliers
{
    class List
    {
        public class Result
        {
            public string company { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string phone { get; set; }
            public string paypal_email { get; set; }
            public string permalink { get; set; }
            public string document { get; set; }
            public string payment_method { get; set; }
            public string pipedrive_id { get; set; }
            public string zipcode { get; set; }
            public string account_type { get; set; }
            public string identifier_type { get; set; }
            public string account_number { get; set; }
            public string identifier_number { get; set; }
            public string accounting_number { get; set; }
            public string status { get; set; }
            public List<object> affiliate_user { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
            public List<object> tax { get; set; }
        }

        public class FiscalIdentity
        {
            public string permalink { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
