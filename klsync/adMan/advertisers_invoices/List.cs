﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.adMan.advertisers_invoices
{
    class List
    {
        public class Currency
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Currency2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country2
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency2> currency { get; set; }
            public List<Country2> country { get; set; }
        }

        public class FiscalIdentity
        {
            public string business_name { get; set; }
            public string document_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string zipcode { get; set; }
            public string enterprise_id { get; set; }
            public string permalink { get; set; }
            public string external_id { get; set; }
            public List<Enterprise> enterprise { get; set; }
        }

        public class Currency3
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country3
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise2
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency3> currency { get; set; }
            public List<Country3> country { get; set; }
        }

        public class Advertiser
        {
            public string id { get; set; }
            public string company { get; set; }
            public string permalink { get; set; }
            public string locked { get; set; }
            public string ho_id { get; set; }
            public string status { get; set; }
            public string user_email { get; set; }
            public string user_first_name { get; set; }
            public string user_last_name { get; set; }
            public string user_phone { get; set; }
            public string user_cell_phone { get; set; }
            public string cif { get; set; }
            public string source { get; set; }
            public string medium { get; set; }
            public string term { get; set; }
            public string content { get; set; }
            public string campaign { get; set; }
            public string segment { get; set; }
            public string activation_permalink { get; set; }
            public string activated { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string region { get; set; }
            public string zipcode { get; set; }
            public string user_ho_id { get; set; }
            public string invoicing_company { get; set; }
            public string invoicing_address1 { get; set; }
            public string invoicing_address2 { get; set; }
            public string invoicing_zipcode { get; set; }
            public string invoicing_country { get; set; }
            public string invoicing_cif { get; set; }
            public string mailing_company { get; set; }
            public string mailing_address1 { get; set; }
            public string mailing_address2 { get; set; }
            public string mailing_zipcode { get; set; }
            public string mailing_country { get; set; }
            public string expiring_days { get; set; }
            public string currency_id { get; set; }
            public string billing_country_id { get; set; }
            public string country_id { get; set; }
            public string enterprise_id { get; set; }
            public string admin_contact { get; set; }
            public string invoicing_city { get; set; }
            public string mailing_city { get; set; }
            public string deleted_reason { get; set; }
            public string invoicing_country_id { get; set; }
            public List<Currency> currency { get; set; }
            public List<Country> country { get; set; }
            public List<FiscalIdentity> fiscal_identity { get; set; }
            public List<Enterprise2> enterprise { get; set; }
        }

        public class AdvertiserInvoiceItem
        {
            public string amount { get; set; }
            public string clicks { get; set; }
            public string conversions { get; set; }
            public string impressions { get; set; }
            public string offer_id { get; set; }
        }

        public class Currency4
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Currency5
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string symbol { get; set; }
        }

        public class Country4
        {
            public string id { get; set; }
            public string iso_code { get; set; }
            public string name { get; set; }
        }

        public class Enterprise3
        {
            public string id { get; set; }
            public string name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string fiscal_id { get; set; }
            public string registry_data { get; set; }
            public string number_series_prefix { get; set; }
            public string code { get; set; }
            public string min_invoicing_amount { get; set; }
            public string number_series_no_charge_prefix { get; set; }
            public string preinvoice { get; set; }
            public string invoice_document_type { get; set; }
            public string number_series_refund_prefix { get; set; }
            public List<Currency5> currency { get; set; }
            public List<Country4> country { get; set; }
        }

        public class Tax
        {
            public string name { get; set; }
            public string value { get; set; }
            public string kind { get; set; }
            public string display_name { get; set; }
        }

        public class Result
        {
            public string accounting_date_tag { get; set; }
            public string advertiser_amount { get; set; }
            public string advertiser_currency { get; set; }
            public string accounting_number { get; set; }
            public List<Advertiser> advertiser { get; set; }
            public string advertiser_address1 { get; set; }
            public string advertiser_address2 { get; set; }
            public string advertiser_cif { get; set; }
            public string advertiser_company { get; set; }
            public string advertiser_country { get; set; }
            public List<AdvertiserInvoiceItem> advertiser_invoice_item { get; set; }
            public string advertiser_zipcode { get; set; }
            public string amount { get; set; }
            public string clicks { get; set; }
            public string conversions { get; set; }
            public string creation_type { get; set; }
            public List<Currency4> currency { get; set; }
            public string date { get; set; }
            public string date_tag { get; set; }
            public string end_date { get; set; }
            public string end_date_tag { get; set; }
            public List<Enterprise3> enterprise { get; set; }
            public string expiring_days { get; set; }
            public string external_id { get; set; }
            public string impressions { get; set; }
            public string is_charge { get; set; }
            public string iva_amount { get; set; }
            public string mailing_address1 { get; set; }
            public string mailing_address2 { get; set; }
            public string mailing_company { get; set; }
            public string mailing_country { get; set; }
            public string mailing_zipcode { get; set; }
            public string notes { get; set; }
            public string notes_date_tag { get; set; }
            public string notes_status { get; set; }
            public string number { get; set; }
            public string permalink { get; set; }
            public string purchase_order_mother_id { get; set; }
            public string start_date { get; set; }
            public string start_date_tag { get; set; }
            public string status { get; set; }
            public List<Tax> tax { get; set; }
            public string total_amount { get; set; }
        }

        public class Pagination
        {
            public int page { get; set; }
            public int page_count { get; set; }
            public int per_page { get; set; }
        }

        public class Response
        {
            public List<Result> result { get; set; }
            public Pagination pagination { get; set; }
        }

        public class RootObject
        {
            public bool valid { get; set; }
            public Response response { get; set; }
        }
    }
}
