﻿namespace klsync
{
    partial class frImagenes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frImagenes));
            this.dgvImagenes = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.subirImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarImagenesSeleccionadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rbutTodosArticulos = new System.Windows.Forms.RadioButton();
            this.rbutArticulosSinImagen = new System.Windows.Forms.RadioButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssLbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_imagenesLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImagenes)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvImagenes
            // 
            this.dgvImagenes.AllowUserToAddRows = false;
            this.dgvImagenes.AllowUserToDeleteRows = false;
            this.dgvImagenes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvImagenes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvImagenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImagenes.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvImagenes.Location = new System.Drawing.Point(12, 57);
            this.dgvImagenes.Name = "dgvImagenes";
            this.dgvImagenes.ReadOnly = true;
            this.dgvImagenes.Size = new System.Drawing.Size(1089, 573);
            this.dgvImagenes.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subirImagenToolStripMenuItem,
            this.borrarImagenesSeleccionadasToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(295, 56);
            // 
            // subirImagenToolStripMenuItem
            // 
            this.subirImagenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("subirImagenToolStripMenuItem.Image")));
            this.subirImagenToolStripMenuItem.Name = "subirImagenToolStripMenuItem";
            this.subirImagenToolStripMenuItem.Size = new System.Drawing.Size(294, 26);
            this.subirImagenToolStripMenuItem.Text = "Subir Imagen";
            this.subirImagenToolStripMenuItem.Click += new System.EventHandler(this.subirImagenToolStripMenuItem_Click);
            // 
            // borrarImagenesSeleccionadasToolStripMenuItem
            // 
            this.borrarImagenesSeleccionadasToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("borrarImagenesSeleccionadasToolStripMenuItem.Image")));
            this.borrarImagenesSeleccionadasToolStripMenuItem.Name = "borrarImagenesSeleccionadasToolStripMenuItem";
            this.borrarImagenesSeleccionadasToolStripMenuItem.Size = new System.Drawing.Size(294, 26);
            this.borrarImagenesSeleccionadasToolStripMenuItem.Text = "Borrar imagenes seleccionadas";
            this.borrarImagenesSeleccionadasToolStripMenuItem.Click += new System.EventHandler(this.borrarImagenesSeleccionadasToolStripMenuItem_Click);
            // 
            // rbutTodosArticulos
            // 
            this.rbutTodosArticulos.AutoSize = true;
            this.rbutTodosArticulos.Checked = true;
            this.rbutTodosArticulos.Location = new System.Drawing.Point(12, 23);
            this.rbutTodosArticulos.Name = "rbutTodosArticulos";
            this.rbutTodosArticulos.Size = new System.Drawing.Size(116, 17);
            this.rbutTodosArticulos.TabIndex = 2;
            this.rbutTodosArticulos.TabStop = true;
            this.rbutTodosArticulos.Text = "Todos los Artículos";
            this.rbutTodosArticulos.UseVisualStyleBackColor = true;
            this.rbutTodosArticulos.CheckedChanged += new System.EventHandler(this.rbutTodosArticulos_CheckedChanged);
            // 
            // rbutArticulosSinImagen
            // 
            this.rbutArticulosSinImagen.AutoSize = true;
            this.rbutArticulosSinImagen.Location = new System.Drawing.Point(134, 23);
            this.rbutArticulosSinImagen.Name = "rbutArticulosSinImagen";
            this.rbutArticulosSinImagen.Size = new System.Drawing.Size(121, 17);
            this.rbutArticulosSinImagen.TabIndex = 3;
            this.rbutArticulosSinImagen.Text = "Artículos sin Imagen";
            this.rbutArticulosSinImagen.UseVisualStyleBackColor = true;
            this.rbutArticulosSinImagen.CheckedChanged += new System.EventHandler(this.rbutArticulosSinImagen_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssLbl,
            this.tss_imagenesLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 640);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1113, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssLbl
            // 
            this.tssLbl.Name = "tssLbl";
            this.tssLbl.Size = new System.Drawing.Size(0, 17);
            // 
            // tss_imagenesLabel
            // 
            this.tss_imagenesLabel.Name = "tss_imagenesLabel";
            this.tss_imagenesLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(970, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 39);
            this.button1.TabIndex = 7;
            this.button1.Text = "Codificar imágenes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frImagenes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1113, 662);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.rbutArticulosSinImagen);
            this.Controls.Add(this.rbutTodosArticulos);
            this.Controls.Add(this.dgvImagenes);
            this.Name = "frImagenes";
            this.Text = "frImagenes";
            this.Load += new System.EventHandler(this.frImagenes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImagenes)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvImagenes;
        private System.Windows.Forms.RadioButton rbutTodosArticulos;
        private System.Windows.Forms.RadioButton rbutArticulosSinImagen;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssLbl;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem subirImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tss_imagenesLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem borrarImagenesSeleccionadasToolStripMenuItem;
    }
}