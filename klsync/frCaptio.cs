﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Globalization;

namespace klsync
{
    public partial class frCaptio : Form
    {
        private static frCaptio m_FormDefInstance;
        public static frCaptio DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCaptio();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frCaptio()
        {
            InitializeComponent();
        }

        private void toolStripButtonImportCSV_Click(object sender, EventArgs e)
        {
            cargarCSV();
        }

        private void cargarCSV()
        {
            Stream myStream = null;
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Abrir archivo";
            theDialog.Filter = "All files|*.*";
            //theDialog.InitialDirectory = @"C:\Users\Alex\Desktop";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = theDialog.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            dgvCaptio.DataSource = csUtilidades.ConvertCSVtoDataTable(theDialog.FileName);
                        }
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Cierra el documento para importar el csv", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (DuplicateNameException ex)
                {
                }
            }
        }


        // Comprobamos que :
        // 1. Existan los proveedores (a través del correo)
        // 2. Existan los artículos (a través de la descripcion)
        private void pasarExcelAA3()
        {
            #region Variables
            csSqlConnects sql = new csSqlConnects();
            DateTime fecha_cierre_facc = Convert.ToDateTime(sql.obtenerCampoTabla("SELECT FECHA FROM __FECHASCIERRES WHERE TIPODOCUMENTO = 'FACC'"));
            DateTime fecha_cierre_a3 = Convert.ToDateTime(sql.obtenerCampoTabla("SELECT FECHACIERRE FROM DATOSCON"));
            bool check_fecha_cierre = false;
            DataTable codProAndEmails = sql.cargarDatosTablaA3("SELECT E_MAIL as Email, LTRIM(CODPRO) as CODPRO FROM PROVEED WHERE E_MAIL <> '' and CAR1 = 'CAP'");
            DataTable codArtAndDescart = sql.cargarDatosTablaA3("SELECT LTRIM(CODART) as CODART, DESCART FROM ARTICULO");
            csa3erp a3 = new csa3erp();
            DataTable dgvDataTable = (DataTable)dgvCaptio.DataSource;
            Objetos.csCabeceraDoc[] cabeceras = null;
            Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[dgvDataTable.Rows.Count];
            listBoxCaptio.Items.Clear();
            string numDoc = "";
            string codigo_informe = "";
            int contador = 0;
            #endregion

            // Paso 0 - Comprobamos las fechas de cierre
            foreach (DataRow dr in dgvDataTable.Rows)
            {
                DateTime cierre_factura_iteracion = Convert.ToDateTime(dr["fecha"].ToString());
                if (fecha_cierre_a3 >= cierre_factura_iteracion || fecha_cierre_facc >= cierre_factura_iteracion)
                {
                    check_fecha_cierre = true;
                }
            }

            if (!check_fecha_cierre)
            {
                // Paso 1 - Comprobar proveedores
                DataView view = new DataView(dgvDataTable);
                DataTable distinctEmails = view.ToTable(true, "Email");
                DataTable dt = csUtilidades.check2DataTablesSQL(distinctEmails, "SELECT DISTINCT E_MAIL FROM PROVEED WHERE CAR1 = 'CAP'");
                informarListBox(listBoxCaptio, "Los siguientes proveedores no estan creados", dt);
                cabeceras = new Objetos.csCabeceraDoc[distinctEmails.Rows.Count];

                // Paso 2 - Comprobar que existan los articulos
                DataTable distinctProducts = view.ToTable(true, "Categoría");
                DataTable dt2 = csUtilidades.check2DataTablesSQL(distinctProducts, "SELECT DISTINCT DESCART FROM ARTICULO");
                informarListBox(listBoxCaptio, "Los siguientes articulos no están creados:", dt2);

                // Paso 3 - Comprobar que existen las facturas
                DataTable distinctInvoices = view.ToTable(true, "Código informe");
                string where = "";
                int cont = 0;
                for (int i = 0; i < distinctInvoices.Rows.Count; i++)
                {
                    if (cont == 0)
                        where += "'" + distinctInvoices.Rows[i]["Código informe"].ToString() + "'";
                    else
                        where += ",'" + distinctInvoices.Rows[i]["Código informe"].ToString() + "'";

                    cont++;
                }

                DataTable dt3 = sql.cargarDatosTablaA3("SELECT REFERENCIA FROM CABEFACC WHERE REFERENCIA IN (" + where + ")");
                informarListBox(listBoxCaptio, "Las siguientes facturas ya están en el sistema:", dt3);

                // 4 - Generar cabecera y lineas
                // y posteriormente, documentos.
                //
                // Si ambos datatables estan vacios significa
                // que todo está en orden en la base de datos
                if (dt.Rows.Count == 0 && dt2.Rows.Count == 0 && dt3.Rows.Count == 0)
                {
                    numDoc = "";
                    for (int i = 0; i < dgvDataTable.Rows.Count; i++)
                    {
                        codigo_informe = dgvDataTable.Rows[i]["Código informe"].ToString().Trim();

                        if (codigo_informe != numDoc)
                        {
                            numDoc = codigo_informe;
                            cabeceras[contador] = cargarCab(dgvDataTable.Rows[i], codProAndEmails);
                            cabeceras[contador].captio = true;
                            contador++;
                        }

                        lineas[i] = cargarLin(dgvDataTable.Rows[i], codProAndEmails, codArtAndDescart);
                    }

                    a3.abrirEnlace();
                    a3.generarDocA3Objeto(cabeceras, lineas, true, "NO", null, null);
                    a3.cerrarEnlace();
                }
            }
            else
            {
                MessageBox.Show("Una o más facturas se encuentran dentro del periodo contable cerrado");
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (dgvCaptio.Rows.Count > 0)
            {
                if (MessageBox.Show("¿Estás seguro/a de querer pasar los datos a A3?", "Atencion", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    pasarExcelAA3();
                }
                else
                {
                    MessageBox.Show("Operación cancelada");
                }
            }
        }

        private void informarListBox(ListBox lb, string texto, DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                lb.Items.Add(texto);
                foreach (DataRow dr in dt.Rows)
                {
                    lb.Items.Add(">> " + dr.ItemArray[0].ToString());
                }
            }
        }

        private Objetos.csCabeceraDoc cargarCab(DataRow dr, DataTable codProAndEmails)
        {
            Objetos.csCabeceraDoc cabeceras = new Objetos.csCabeceraDoc();
            cabeceras = new Objetos.csCabeceraDoc();
            string email1 = "", email2 = "";
            foreach (DataRow dr2 in codProAndEmails.Rows)
            {
                email1 = dr["Email"].ToString();
                email2 = dr2["Email"].ToString();
                if (email1 == email2)
                {
                    cabeceras.codIC = dr2["CODPRO"].ToString();
                }
            }

            cabeceras.nombreIC = dr["Usuario"].ToString().ToUpper();
            cabeceras.referencia = dr["Código informe"].ToString();
            cabeceras.fecha = csUtilidades.getLastDayOfMonth(dr["Fecha"].ToString()) + " 00:00";
            cabeceras.codFormaPago = "TR";
            cabeceras.empresa = dr["Código de empresa"].ToString();

            return cabeceras;
        }

        private Objetos.csLineaDocumento cargarLin(DataRow dr, DataTable codProAndEmails, DataTable codArtAndDescArt)
        {
            Objetos.csLineaDocumento linea = new Objetos.csLineaDocumento();

            foreach (DataRow dr2 in codProAndEmails.Rows)
            {
                if (dr["Email"].ToString() == dr2["Email"].ToString())
                {
                    linea.descripcionArticulo = dr["Usuario"].ToString().ToUpper();
                }
            }

            foreach (DataRow dr2 in codArtAndDescArt.Rows)
            {
                if (dr["Categoría"].ToString() == dr2["DESCART"].ToString())
                {
                    linea.codigoArticulo = dr2["CODART"].ToString();
                }
            }

            linea.cantidad = "1";
            linea.numCabecera = dr["Código informe"].ToString();
            linea.precio = dr["Total (€)"].ToString();

            return linea;
        }
    }
}
