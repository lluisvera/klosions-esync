﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    class csSqlScriptRepasat
    {
        //CLIENTES
        public string selectClientesRPST()
        {
            string selectDocs = "";
            selectDocs = "Select " +
                              " clientes.idCli as ID, " +
                              " clientes.nomCli as Nombre ," +
                              " clientes.razonSocialCli as Razon, " +
                              " direccionesclientes.principalDireccion, " +
                              " direcciones.nomDireccion, " +
                              " direcciones.direccion1Direccion, " +
                              " direcciones.direccion2Direccion, " +
                              " direcciones.cpDireccion, " +
                              " direcciones.poblacionDireccion, " +
                              " direcciones.idProvincia, " +
                              " direcciones.idPais, " +
                              " clientes.emailCli as email," +
                              " clientes.webCli as web ," +
                              " clientes.tel1" +
                              " From " +
                              " clientes Left Join " +
                              " direccionesclientes On direccionesclientes.idCli = clientes.idCli Left Join " +
                              " direcciones On direccionesclientes.idDireccion = direcciones.idDireccion " +
                              " Where " +
                              " clientes.idEntidadcli=101";
            return selectDocs;
        }

        public string insertClientesRPST(string codcli, string nomcli, string dircli, string cpcli, string pobcli, string provcli, string telf, string contacto)
        {
            string selectDocs = "";
            selectDocs = "Insert into clientes (identidadcli,codCli,nomCli,dirCli,cpCli,pobCli,provCli,tel1,contacto) values (100,'" +
                codcli.Trim() + "','" + nomcli.Replace("'", "''") + "','" + dircli.Replace("'", "''") + "','" + cpcli + "','" + pobcli.Replace("'", "''") + "','" + provcli.Replace("'", "''") + "','" + telf + "','" + contacto.Replace("'", "''") + "')";

            return selectDocs;
        }

        public string borrarClientesRPST(string Id)
        {

            string selectId = "";
            selectId = "delete from clientes where codcli=" + Id.Trim();

            return selectId;

        }

        //ARTICULOS
        public string selectArticulosRPST()
        {
            string selectDocs = "";
            selectDocs = "Select " +
                              " articulos.idArticulo, " +
                              " articulos.nomArticulo, " +
                              " articulos.descArticulo, " +
                              " articulos.precioVentaArticulo " +
                            " From " +
                              " articulos";
            return selectDocs;
        }





        public string insertArticulosRPST(string idArticulo, string nomArticulo, string descArticulo, string precioArticulo)
        {
            string selectDocs = "";
            selectDocs = "Insert into articulos (idEntidadArticulo, idArticulo,nomArticulo,descArticulo,refArticulo,idTipoArticulo,precioVentaArticulo,fecAltaArticulo,activoArticulo) values (101,'" +
                idArticulo.Replace(" ", "") + "','" + nomArticulo.ToUpper() + "','" + descArticulo.ToUpper() + "','" + idArticulo.Replace(" ", "") + "',3," + precioArticulo.Replace(",", ".") + ",'" + DateTime.Now.ToString("yyyy-MM-dd") + "',1)";

            return selectDocs;
        }



        public string borrarArticulosRPST(string Id)
        {

            string selectId = "";
            selectId = "delete from articulos where idArticulo=" + Id.Trim();

            return selectId;

        }

        //PROYECTOS
        public string selectProyectosRPST()
        {
            string selectDocs = "";
            selectDocs = "Select " +
                            " proyectos.idProyecto, " +
                            " proyectos.nomProyecto, " +
                            " proyectos.codCli, " +
                            " proyectos.docRef, " +
                            " proyectos.dirProyecto, " +
                            " proyectos.cpProyecto, " +
                            " proyectos.pobProyecto, " +
                            " proyectos.provProyecto, " +
                            " proyectos.telf1, " +
                            " proyectos.contacto, " +
                            " proyectos.centroCoste, " +
                            " proyectos.responsable, " +
                            " proyectos.activoProyecto, " +
                            " proyectos.fecAltaProyecto, " +
                            " proyectos.fecBajaProyecto " +
                            " From " +
                            " proyectos ";
            return selectDocs;
        }





        public string insertProyectosRPST(string nomProyecto, string codCli, string dirProyecto, string cpProyecto, string pobProyecto, string Proyecto, string telf)
        {
            string fechaMySQL = DateTime.Now.ToString("yyyy-MM-dd");
            string selectDocs = "";
            selectDocs = "Insert into proyectos (identidadproyecto,nomProyecto,codCli,dirProyecto,cpProyecto, pobProyecto, centroCoste,telf1,activoProyecto,fecAltaProyecto) values (100,'" +
                nomProyecto.Replace("'", "''") + "','" + codCli + "','" + dirProyecto.Replace("'", "''") + "','" + cpProyecto + "','" + pobProyecto.Replace("'", "''") + "','" + Proyecto.Replace("'", "''") + "','" + telf + "',1,'" + DateTime.Now.ToString("yyyy-MM-dd") + "')";

            return selectDocs;
        }



        public string borrarProyectosRPST(string Id)
        {

            string selectId = "";
            selectId = "delete from proyectos where idProyecto=" + Id.Trim();

            return selectId;

        }

        //ACTIVIDADES
        public string selectActividadesRPST(string dateIni, string dateFin)
        {
            string selectDocs = "Select " +
                          " actividades.idActividad, " +
                          " actividades.idTarea, " +
                          " actividades.tipoTarea, " +
                          " actividades.fecIniRealAct, " +
                          " tareas.nomTarea, " +
                          " proyectos.nomProyecto, " +
                          " clientes.codExternoCli, " +
                          " clientes.nomCli, " +
                          " actividades.codExternoActividad as docExterno " +
                          " From " +
                          " actividades Inner Join " +
                          " tareas On actividades.idTarea = tareas.idTarea Inner Join " +
                          " proyectos On tareas.idProyecto = proyectos.idProyecto Inner Join " +
                          " clientes On proyectos.idCli = clientes.idCli " +
                          " Where " +
                          " clientes.codExternoCli <> '' And " +
                          " actividades.realizadaActividad = True And " +
                          " actividades.idEntidadActividad = 101 And" +
                          " actividades.fecIniRealAct Between '" + dateIni + " ' And  '" + dateFin + "' ";


            return selectDocs;
        }

        public string selectDetalleActividadesRPST(string dateIni, string dateFin)
        {

            string selectDocs = "Select " +
                             " actividades.idActividad, " +
                             " actividades.idTarea, " +
                             " actividades.tipoTarea, " +
                             " actividades.horasRealizadas, " +
                             " actividades.fecIniRealAct, " +
                             " actividades.fecFinRealAct, " +
                             " tareas.nomTarea, " +
                             " proyectos.nomProyecto, " +
                             " clientes.codExternoCli, " +
                             " clientes.nomCli, " +
                             " actividadesarticulos.refArticulo, " +
                             " actividadesarticulos.descArticulo, " +
                             " actividadesarticulos.cantidadArticulo, " +
                             " actividadesarticulos.precioVentaArticulo " +
                             " From " +
                             " actividades Inner Join " +
                             " tareas On actividades.idTarea = tareas.idTarea Inner Join " +
                             " proyectos On tareas.idProyecto = proyectos.idProyecto Inner Join " +
                             " clientes On proyectos.idCli = clientes.idCli Left Join " +
                             " actividadesarticulos On actividadesarticulos.idActividad = actividades.idActividad " +
                             " Where " +
                             " clientes.codExternoCli <> '' And " +
                             " actividades.realizadaActividad = True And " +
                             " actividades.idEntidadActividad = 101 And" +
                             " actividades.fecIniRealAct Between '" + dateIni + " ' And  '" + dateFin + "' ";

            return selectDocs;




        }

        public string selectTrabajadoresActividadesRPST()
        {
            string selectDocs = "";
            selectDocs = "Select distinct " +
                              " CONCAT(actividades.idTrabajador,'-',trabajadores.nombreTrabajador,' ',trabajadores.apellidosTrabajador) as EMPLEADO " +
                              " From" +
                              " actividades Inner Join" +
                              " trabajadores On actividades.idTrabajador = trabajadores.idTrabajador" +
                              " Inner Join" +
                              " tareas On actividades.idTarea = tareas.idTarea " +
                              " Inner Join proyectos On tareas.idProyecto = proyectos.idProyecto Inner Join" +
                              " clientes On proyectos.idCli = clientes.idCli ";
            //" where actividades.fecIniPrevAct Between '" + dateIni + " ' And  ' " + dateFin + "' ";
            return selectDocs;
        }

    }
}
