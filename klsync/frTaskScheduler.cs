﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frTaskScheduler : Form
    {
        csTasks task = new csTasks();

        private static frTaskScheduler m_FormDefInstance;
        public static frTaskScheduler DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frTaskScheduler();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frTaskScheduler()
        {
            InitializeComponent();
        }

        private void btnAddTask_Click(object sender, EventArgs e)
        {
            double repetir = 30;
            string name = txtNameTask.Text;
            string path2file = txtRutaTask.Text;
            string path = txtIniciarEnTask.Text;
            string args = "";
            DateTime fecha = dtpDateTask.Value;
            DateTime time = dtpTimeTask.Value;
            double hora = time.Hour;
            double minuto = time.Minute;

            // Miramos que los campos esten rellenados
            if ((!string.IsNullOrEmpty(name) &&
                !string.IsNullOrEmpty(path2file) &&
                !string.IsNullOrEmpty(args) &&
                !string.IsNullOrEmpty(path) &&
                comboRepetirTask.SelectedItem != null)

                ||

                // tiene que haber al menos una opcion de argumentos marcada
                cblArgumentos.CheckedItems.Count > 0)
            {

                foreach (Object item in cblArgumentos.CheckedItems)
                {
                    if (item.ToString() == "documentos")
                    {
                        args += " documentos ";
                    }
                    if (item.ToString() == "tags")
                    {
                        args += " tags ";
                    }
                    if (item.ToString() == "stock") // Resto de clientes
                    {
                        args += " stock ";
                    }
                    if (item.ToString() == "replog")
                    {
                        args += " replog ";
                    }
                    if (item.ToString() == "tallas")
                    {
                        args += " tallas ";
                    }
                    if (item.ToString() == "categorias")
                    {
                        args += " categorias ";
                    }
                    if (item.ToString() == "dismay")
                    {
                        args += " dismay ";
                    }
                    // Precios
                    if (item.ToString() == "precios familias clientes")
                    {
                        args += " familiasclientearticulo ";
                    }
                    if (item.ToString() == "precios tarifas clientes") // Ifigen
                    {
                        args += " preciostarifasclientes ";
                    }
                    if (item.ToString() == "descuentos") 
                    {
                        args += " preciosdescuentos ";
                    }
                    if (item.ToString() == "amigos")
                    {
                        args += " amigos_miro ";
                    }
                }

                switch (comboRepetirTask.Text)
                {
                    case "5 minutos":
                        repetir = 5;
                        break;
                    case "15 minutos":
                        repetir = 15;
                        break;
                    case "30 minutos":
                        repetir = 30;
                        break;
                    case "1 hora":
                        repetir = 60;
                        break;
                    case "12 horas":
                        repetir = 720;
                        break;
                    case "24 horas":
                        repetir = 1440;
                        break;
                    case "semana":
                        repetir = 10080;
                        break;
                }

                // añadimos la tarea
                if (task.addTask(name, path2file, args, path, repetir, fecha, hora, minuto))
                {
                    lblError.ForeColor = Color.Green;
                    lblError.Text = "Tarea programada añadida con éxito.";
                }
                else
                {
                    lblError.ForeColor = Color.Red;
                    lblError.Text = "No se ha podido crear la tarea programada.";
                }

                loadTasks();
            }
            else
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Rellena los campos";
            }
        }


        public void loadTasks()
        {
            treeTask.Nodes.Clear();
            TreeView t = task.getTasks();
            foreach (TreeNode node in t.Nodes)
            {
                treeTask.Nodes.Add((TreeNode)node.Clone());
            }
        }

        private void btnRutaFinderTask_Click(object sender, EventArgs e)
        {
            openFile();
        }

        private void openFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.InitialDirectory = "C:\\";
            ofd.Filter = "Applications (*.exe)|*.exe";
            ofd.FilterIndex = 2;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    txtRutaTask.Text = "";
                    txtRutaTask.Text = ofd.FileName.ToString();
                }
                catch { }
            }
        }

        private void frTaskScheduler_Load(object sender, EventArgs e)
        {
            loadTasks();
        }

        private void btnLoadTasks_Click(object sender, EventArgs e)
        {
            loadTasks();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            //Para mas informacion consulte la ayuda de la consola con cmd.exe /? 
            System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + "taskschd.msc");
            // Indicamos que la salida del proceso se redireccione en un Stream
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procStartInfo.CreateNoWindow = false;
            //Inicializa el proceso
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
            //Consigue la salida de la Consola(Stream) y devuelve una cadena de texto
            string result = proc.StandardOutput.ReadToEnd();
            //Muestra en pantalla la salida del Comando
            Console.WriteLine(result);
        }

    }
}
