﻿using System.Data;

namespace klsync
{
    public class csExcelExportData
    {
        public static void exportarDatos(DataTable datos, string nombreArchivo)
        {
            if (datos == null || datos.Rows.Count == 0)
            {
                throw new System.ArgumentException("El DataTable está vacío. No hay datos para exportar.\nRevisa que la marca seleccionada contennga datos.");
            }

            DataTableExt.exportToExcelNew(datos, nombreArchivo);
        }
    }
}
