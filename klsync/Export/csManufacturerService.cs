﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace klsync.Export
{
    public class csManufacturerService
    {
        // Obtener marcas para llenar el ComboBox
        public static DataTable obtenerMarcas()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            try
            {
                string query = "SELECT id_manufacturer, name FROM ps_manufacturer";
                DataTable dt = mysql.cargarTabla(query);

                return dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener marcas: {ex.Message}");
                throw;
            }
        }


        // Obtener datos de productos relacionados con una marca específica
        public static DataTable obtenerDatosDeMarca(string nombreMarca)
        {
            csMySqlConnect mysql = new csMySqlConnect();

            try
            {
                string query = $@"
                SELECT 
                    p.`reference` AS `Referencia`,
                    CONCAT('', p.`ean13`) AS `EAN`, 
                    pl.`name` AS `Nombre`,
                    SUBSTRING(pl.`description_short`, 1, 255) AS `Breve descripción`,
                    SUBSTRING(pl.`description`, 1, 500) AS `Descripción`,
                    m.`name` AS `Nombre de la marca`,
                    GROUP_CONCAT(
                        DISTINCT 
                        CONCAT(
                            fl.`name`, 
                            ':', 
                            fvl.`value`,
                            ':', 
                            fv.`custom`
                        ) SEPARATOR ', '
                    ) AS `Característica (Nombre:Valor:Personalizado)`,
                    CONCAT('https://dismay.es/img/p/',
                            SUBSTRING(img_shop.id_image, 1, 1), '/', 
                            SUBSTRING(img_shop.id_image, 2, 1), '/',
                            SUBSTRING(img_shop.id_image, 3, 1), '/',
                            SUBSTRING(img_shop.id_image, 4, 1), '/',
                            SUBSTRING(img_shop.id_image, 5, 1), '/', 
                            img_shop.id_image, '.jpg') AS `URL de la imagen de portada`
                FROM 
                    ps_product p
                LEFT JOIN 
                    ps_product_lang pl ON pl.`id_product` = p.`id_product` AND pl.`id_lang` = 1
                LEFT JOIN 
                    ps_manufacturer m ON m.`id_manufacturer` = p.`id_manufacturer`
                LEFT JOIN 
                    ps_image_shop img_shop ON img_shop.`id_product` = p.`id_product` AND img_shop.`cover` = 1 AND img_shop.`id_shop` = 1
                LEFT JOIN 
                    ps_feature_product fp ON fp.`id_product` = p.`id_product`
                LEFT JOIN 
                    ps_feature_lang fl ON fl.`id_feature` = fp.`id_feature` AND fl.`id_lang` = 1
                LEFT JOIN 
                    ps_feature_value_lang fvl ON fvl.`id_feature_value` = fp.`id_feature_value` AND fvl.`id_lang` = 1
                LEFT JOIN 
                    ps_feature_value fv ON fv.`id_feature_value` = fp.`id_feature_value`
                WHERE 
                    m.`name` LIKE '%{nombreMarca}%'
                GROUP BY 
                    p.`id_product`
                ORDER BY 
                    p.`id_product` DESC";



                DataTable dt = mysql.cargarTabla(query);

                return dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener datos de la marca: {ex.Message}");
                throw;
            }
        }
    }
}
