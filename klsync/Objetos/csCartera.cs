﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csCartera
    {
        public string cobroPago { get; set; }

        public string idEfectoRPST { get; set; }

        public string codigoClienteA3 { get; set; }

        public string moneda { get; set; }

        public string banco { get; set; }

        public string nombreBanco { get; set; }

        public string codBanco { get; set; }

        public string cuentaContableBanco { get; set; }

        public string cobrado { get; set; }

        public string idSerieRPST { get; set; }

        public string extNumDoc { get; set; }

        public string codIC { get; set; }

        public string nombreIC { get; set; }

        public string serieDoc { get; set; }

        public string numDocA3 { get; set; }

        public DateTime fechaDoc { get; set; }

        public DateTime fechaCobroPago { get; set; }
        public string numVencimiento { get; set; }

        public DateTime fechaVencimiento { get; set; }
        public string fechaImpagado { get; set; }

        public string idCarteraRPST { get; set; }

        public string idRemesa { get; set; }
        public string remesado { get; set; }

        public string codDocumentoPago { get; set; }

        public string externalFacturaId { get; set; }

        public double importeEfecto { get; set; }
       
        public double importeEfectoCobrado { get; set; }

        public double importeGastosImpagado { get; set; }

        public bool esAnticipo { get; set; }

        public bool esEfectoAgrupado { get; set; }
        public bool perteneceEfectoAgrupado { get; set; }
        public int? idAgrupacion { get; set; }
        public string refAgrupacionCarteraCobros { get; set; }
        public bool generarMovimientosContables { get; set; }

        public bool esEfectoCobradoVISA { get; set; }

        public string codExternoAgrupacion { get; set; }






    }
}
