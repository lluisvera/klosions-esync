﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csCuentasContables
    {
        public string idCuenta
        {
            get;
            set;
        }

        public string nomCuenta
        {
            get;
            set;
        }

        public string codExternoCuentaContable
        {
            get;
            set;
        }
    }
}
