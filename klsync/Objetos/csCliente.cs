﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csCliente
    {
        public string company { get; set; }
        public string type { get; set; }
        public string customer_type { get; set; }
        public string payment_method { get; set; }
        public string payment_method_id { get; set; }

        public string nombreComercial
        {
            get;
            set;
        }

        public string razonSocial
        {
            get;
            set;
        }

        public string codCli
        {
            get;
            set;
        }

        public string nomIC
        {
            get;
            set;
        }

        public string codIC
        {
            get;
            set;
        }

        public string id_customer_Ext //Código Cliente Aplicación Externa
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }

        public string nombre
        {
            get;
            set;
        }

        public string apellidos
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

        public string direccion
        {
            get;
            set;
        }

        public string codigoPostal
        {
            get;
            set;
        }

        public string poblacion
        {
            get;
            set;
        }

        public string provincia
        {
            get;
            set;
        }

        public string codprovincia
        {
            get;
            set;
        }

        public string pais
        {
            get;
            set;
        }

        public string codPais
        {
            get;
            set;
        }

        public string telf
        {
            get;
            set;
        }

        public string movil
        {
            get;
            set;
        }

        public string web
        {
            get;
            set;
        }

        public string permalink
        {
            get;
            set;
        }

        public string permalink_fiscal_identity
        {
            get;
            set;
        }

        public string representante
        {
            get;
            set;
        }

        public string nifcif
        {
            get;
            set;
        }

        public string grupoPS
        {
            get;
            set;
        }

        public bool active_Status
        {
            get;
            set;
        }

        public bool shop_Visibility
        {
            get;
            set;
        }

        public string codFormaPago
        {
            get;
            set;
        }

        public string descFormaPago
        {
            get;
            set;
        }

        public string tipoOperacion  //Indicar si es operación interior, intracomunitario, etc.
        {
            get;
            set;
        }

        public string cuentaContable
        {
            get;
            set;
        }

        public string regIva
        {
            get;
            set;

        }

        public string opeccm            //Indica que es un cliente u operación de Canarias, Ceuta o Melilla
        {
            get;
            set;
        }

        public DateTime fechaLastUpdate            //Indica que es un cliente u operación de Canarias, Ceuta o Melilla
        {
            get;
            set;
        }

        public bool cuentaGenerica {
            get;
            set;
        }

        public string tipoProveedor { get; set; }
    }
}
