﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csProveedor
    {
        public string account_number
        {
            get;
            set;
        }

        public string identifier_number
        {
            get;
            set;
        }

        public string nombreComercial
        {
            get;
            set;
        }

        public string razonSocial
        {
            get;
            set;
        }

        public string codProv
        {
            get;
            set;
        }

        public string id_Supplier_Ext //Código Proveedor Aplicación Externa
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }

        public string nombre
        {
            get;
            set;
        }

        public string apellidos
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

        public string direccion
        {
            get;
            set;
        }

        public string codigoPostal
        {
            get;
            set;
        }

        public string poblacion
        {
            get;
            set;
        }

        public string provincia
        {
            get;
            set;
        }

        public string codprovincia
        {
            get;
            set;
        }

        public string pais
        {
            get;
            set;
        }

        public string codPais
        {
            get;
            set;
        }

        public string telf
        {
            get;
            set;
        }

        public string movil
        {
            get;
            set;
        }

        public string web
        {
            get;
            set;
        }

        public string permalink
        {
            get;
            set;
        }
        
        public string permalinkAffiliateUser
        {
            get;
            set;
        }

        public string representante
        {
            get;
            set;
        }

        public string nifcif
        {
            get;
            set;
        }

        public string grupoPS
        {
            get;
            set;
        }

        public string active_Status
        {
            get;
            set;
        }

        public bool shop_Visibility
        {
            get;
            set;
        }

        public string codFormaPago
        {
            get;
            set;
        }

        public string descFormaPago
        {
            get;
            set;
        }

        public string tipoOperacion  //Indicar si es operación interior, intracomunitario, etc.
        {
            get;
            set;
        }

        public string cuentaContable
        {
            get;
            set;
        }

        public string codigoA3
        {
            get;
            set;
        }
    }
}
