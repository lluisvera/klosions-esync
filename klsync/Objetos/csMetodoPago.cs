﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csMetodoPago
    {

        public string idFormaPagoRepasat
        {
            get;
            set;
        }

        public string nomFormaPago
        {
            get;
            set;
        }

        public string codExternoFormaPago
        {
            get;
            set;
        }

        public string pagadoFormaPago  //indica si el documento se dará como pagado
        {
            get;
            set;
        }

        public string numeroVencimientos
        {
            get;
            set;
        }
        public string primerLapso
        {
            get;
            set;
        }
        public string siguientesLapsos
        {
            get;
            set;
        }
    }
}
