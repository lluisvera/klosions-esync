﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csProyecto
    {

        public String codProyecto
        {
            get;
            set;
        }

        public String idProyecto
        {
            get;
            set;
        }
        public string nomProyecto
        {
            get;
            set;
        }

        public string codExternoProyecto  //código Externo del Proyecto
        {
            get;
            set;
        }
        public string codCliente
        {
            get;
            set;
        }

        public string nomCliente
        {
            get;
            set;
        }



        public DateTime fechaInicio
        {
            get;
            set;
        }

        public DateTime fechaFin
        {
            get;
            set;
        }
        public bool active_Status
        {
            get;
            set;
        }

        public bool shop_Visibility
        {
            get;
            set;
        }
    }
}
