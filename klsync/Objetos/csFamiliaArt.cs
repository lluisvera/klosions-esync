﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csFamiliaArt
    {
        public string idFam
        {
            get;
            set;
        }

        public string nomFam
        {
            get;
            set;
        }

        public string codExternoFamilia
        {
            get;
            set;
        }
    }
}
