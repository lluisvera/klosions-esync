﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csRemesas
    {

        public string cobroPago { get; set; }

        public string idRemesa { get; set; }

        public DateTime fechaRemesa { get; set; }

        public DateTime fechaCobro { get; set; }

        public string codBanco { get; set; }

        public string cuentaBanco { get; set; }

        public string cobrado { get; set; }

        public string numeroRemesa { get; set; }

        public string extNumDoc { get; set; }

        public double importeRemesa { get; set; }

        public int numEfectosRemesa { get; set; }

        public DateTime fechaDoc { get; set; }

    }
}
