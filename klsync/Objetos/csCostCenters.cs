﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csCostCenters
    {


        public string idCentroCoste
        {
            get;
            set;
        }

        public string nomCentroCoste
        {
            get;
            set;
        }
        public string descCentroCoste
        {
            get;
            set;
        }
        public string refCentroCoste
        {
            get;
            set;
        }
        public string codExternoCentroCoste
        {
            get;
            set;
        }
        public string level1
        {
            get;
            set;
        }
        public string level2
        {
            get;
            set;
        }
        public string level3
        {
            get;
            set;
        }

        public string bloqueado
        {
            get;
            set;
        }
        public string obsoleto
        {
            get;
            set;
        }
        public string fecModificacion
        {
            get;
            set;
        }
        //APLICARLO ASI SI SE REALIZA LA MODIFICACION EN frRepasat.cs lin 10938
        //public DateTime? fecModificacionCenCos { get; set; }
    }
}
