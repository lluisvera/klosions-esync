﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csTiposRentencion
    {
        public string idTipoRentencion
        {
            get;
            set;
        }

        public string nomTipoRentencion
        {
            get;
            set;
        }

        public string codExterno
        {
            get;
            set;
        }
        public string porcentajeTipoRetencion
        {
            get;
            set;
        }


    }
}
