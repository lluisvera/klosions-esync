﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csTiposIVA
    {
        public string idTipoIVA
        {
            get;
            set;
        }

        public string nombreTipoIVA
        {
            get;
            set;
        }

        public string codExterno
        {
            get;
            set;
        }
    }
}
