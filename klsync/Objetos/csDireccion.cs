﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace klsync.Objetos
{
    public class csDireccion
    {
        public string idDireccionRepasat
        {
            get;
            set;
        }

        public string idClienteDireccionRepasat
        {
            get;
            set;
        }

        public string codCli
        {
            get;
            set;
        }

        public bool direccionfiscal
        {
            get;
            set;
        }

        public string codExternoDireccion
        {
            get;
            set;
        }

        public string idDireccionERP
        {
            get;
            set;
        }

        public string nombreDireccion
        {
            get;
            set;
        }

        public string direccion1
        {
            get;
            set;
        }

        public string direccion2
        {
            get;
            set;
        }

        public string codigoPostal
        {
            get;
            set;
        }

        public string poblacion
        {
            get;
            set;
        }

        public string codProvincia
        {
            get;
            set;
        }
        public string provincia
        {
            get;
            set;
        }

        public string codPais
        {
            get;
            set;

        }
        public string pais
        {
            get;
            set;
        }

        public string telefono
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }


        public void actualizarDireccionesFiscales(string codigoCliente = null)
        {
            try
            {
                string scriptUpdateAddress = "";
                string scriptUpdateCustomer = "";
                string anexoClientePS = "";
                string anexoClientesA3 = "";
                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mySql = new csMySqlConnect();

                string anexoDirFiscal = csGlobal.nombreServidor.Contains("ANDREUTOYS") ? "KLS_FISCAL" : "DEFECTO";

                //Marco todas las direcciones como fiscales para que no se puedan editar

                anexoClientePS = string.IsNullOrEmpty(codigoCliente) ? "" : " where id_customer=" + codigoCliente;
                csUtilidades.ejecutarConsulta("update ps_address set is_fiscal_address=0" + anexoClientePS, true);

                //cargo en un datatable aquellas que no son fiscales y actualizaré PS 
                DataTable dt = new DataTable();
                anexoClientesA3 = string.IsNullOrEmpty(codigoCliente) ? "" : " AND KLS_CODCLIENTE=" + codigoCliente;
                dt = sql.obtenerDatosSQLScript("SELECT " +
                    " LTRIM(DIRENT.CODCLI) AS CODCLI, DEFECTO, DIRENT1, CAST(IDDIRENT as int) as IDDIRENT, ID_PS, KLS_CODCLIENTE " +
                    " FROM DIRENT INNER JOIN __CLIENTES ON __CLIENTES.CODCLI=DIRENT.CODCLI WHERE  ID_PS  >0 AND " + anexoDirFiscal + "='T' " + anexoClientesA3 +
                    " ORDER BY CODCLI,NUMDIR");
                mySql.OpenConnection();
                foreach (DataRow fila in dt.Rows)
                {
                    scriptUpdateAddress = "update ps_address set is_fiscal_address=1 where id_address=" + fila["ID_PS"];
                    mySql.ejecutarConsulta(scriptUpdateAddress, true);

                    scriptUpdateCustomer = "update ps_customer set id_fiscal_address=" + fila["ID_PS"] + " where kls_a3erp_id=" + fila["CODCLI"];
                    mySql.ejecutarConsulta(scriptUpdateCustomer, true);
                }
                mySql.CloseConnection();

            }
            catch
            { }
        }


    }
}
