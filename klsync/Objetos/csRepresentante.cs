﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csRepresentante
    {
        public string idRepresentante
        {
            get;
            set;
        }

        public string nombreRepresentante
        {
            get;
            set;
        }

        public string codExternoTrabajador
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }

        public string telefono
        {
            get;
            set;
        }
    }
}
