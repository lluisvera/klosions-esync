﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csTipoArt
    {
        public int idTipoArticulo
        {
            get;
            set;
        }

        public int codTipoArticulo
        {
            get;
            set;
        }

        public string nomTipoArticulo
        {
            get;
            set;
        }

        public string codExternoTipoArticulo
        {
            get;
            set;
        }
    }
}
