﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    public class csTercero
    {
        public string codIC
        {
            get;
            set;
        }

        public string nomIC
        {
            get;
            set;
        }

        public string tipoCliente
        {
            get;
            set;
        }

        public string alias
        {
            get;
            set;
        }


        public string regimenIva
        {
            get;
            set;
        }

        public string opeccm            //Indica que es un cliente u operación de Canarias, Ceuta o Melilla
        {
            get;
            set;
        }

        public string moneda
        {
            get;
            set;
        }
        
        public string tipoIRPF
        {
            get;
            set;
        }

        public string claveIRPF
        {
            get;
            set;
        }

        public string subclaveIRPF
        {
            get;
            set;
        }

        public string porcentajeIRPF
        {
            get;
            set;
        }

        //hay que definir el enum y el string
        public enum  tipoIC { Cli , Pro };

        public string nombreComercial
        {
            get;
            set;
        }

        public string account_number
        {
            get;
            set;
        }

        public string identifier_number
        {
            get;
            set;
        }

        public string idRepasat
        {
            get;
            set;
        }

        public string razonSocial
        {
            get;
            set;
        }

        public string nombre
        {
            get;
            set;
        }

        public string apellidos
        {
            get;
            set;
        }

        public string representante
        {
            get;
            set;
        }

        public string nifcif
        {
            get;
            set;
        }

        public string codigoA3
        {
            get;
            set;
        }

        public string ruta
        {
            get;
            set;
        }

        public string zona
        {
            get;
            set;
        }


        public string diaPago1
        {
            get;
            set;
        }
        public string diaPago2
        {
            get;
            set;
        }
        public string diaPago3
        {
            get;
            set;
        }

        public string observaciones
        {
            get;
            set;
        }

        public string id_customer_Ext //Código Cliente Aplicación Externa
        {
            get;
            set;
        }

        //INFO  DIRECCIONES

        public string direccion
        {
            get;
            set;
        }

        public string direccion2
        {
            get;
            set;
        }

        public string codigoPostal
        {
            get;
            set;
        }

        public string poblacion
        {
            get;
            set;
        }

        public string provincia
        {
            get;
            set;
        }

        public string codprovincia
        {
            get;
            set;
        }

        public string codPais
        {
            get;
            set;
        }
        
        public string pais
        {
            get;
            set;
        }

        public DateTime fechaLastUpdateDir            //Indica que es un cliente u operación de Canarias, Ceuta o Melilla
        {
            get;
            set;
        }


        //DATOS FINANCIEROS

        public string paypal
        {
            get;
            set;
        }

        public string codFormaPago
        {
            get;
            set;
        }

        public string descFormaPago
        {
            get;
            set;
        }

        public string codDocPago
        {
            get;
            set;
        }

        public string descDocPago
        {
            get;
            set;
        }

        public string tipoOperacion  //Indicar si es operación interior, intracomunitario, etc.
        {
            get;
            set;
        }

        public string cuentaContable
        {
            get;
            set;
        }
        
        //DATOS CONTACTO

        public string email
        {
            get;
            set;
        }

        public string telf
        {
            get;
            set;
        }
        public string telf2
        {
            get;
            set;
        }
        public string fax
        {
            get;
            set;
        }
        public string movil
        {
            get;
            set;
        }
        public string famCliEsp
        {
            get;
            set;
        }

        public string carDos
        {
            get;
            set;
        }

        public string web
        {
            get;
            set;
        }


        public string id_Supplier_Ext //Código Proveedor Aplicación Externa
        {
            get;
            set;
        }

        //DATOS PS

        public string password
        {
            get;
            set;
        }

    
        public string grupoPS
        {
            get;
            set;
        }

        public bool shop_Visibility
        {
            get;
            set;
        }

  
        //DATOS ADMAN

        public string permalink
        {
            get;
            set;
        }

        public string permalinkAffiliateUser
        {
            get;
            set;
        }

        public string permalinkFiscalIdentity
        {
            get;
            set;
        }

        public string active_Status
        {
            get;
            set;
        }

        public string external_id
        {
            get;
            set;
        }

        public string repasatID
        {
            get;
            set;
        }

        public string prestashopID
        {
            get;
            set;
        }
        public string idDireccionPS
        {
            get;
            set;
        }
        public string codRepasat
        {
            get;
            set;
        }
        public string cuentaGenerica
        {
            get;
            set;
        }
        public string tipoProveedor { get; set; }
        public string conexionExterna { get; set; }
        public string unidadNegocio { get; set; }
    }
}
