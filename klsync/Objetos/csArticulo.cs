﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csArticulo
    {

        public string codart
        {
            get;
            set;
        }

        public string codExternoArt
        {
            get;
            set;
        }

        public string descArt
        {
            get;
            set;
        }

        public string alias
        {
            get;
            set;
        }

        public string precioSinIVA
        {
            get;
            set;
        }

        public string precioConIVA
        {
            get;
            set;
        }

        public string precioCompra
        {
            get;
            set;
        }
        
        public string precioCoste
        {
            get;
            set;
        }

        public string esStock
        {
            get;
            set;
        }

        public string esVenta
        {
            get;
            set;
        }

        public string esCompra
        {
            get;
            set;
        }
        public string id_product_PS
        {
            get;
            set;
        }

        public string id_manufacturer
        {
            get;
            set;
        }


        public string refProveedor
        {
            get;
            set;
        }

        public string tipoIVAVenta
        {
            get;
            set;
        }

        public string tipoIVACompra
        {
            get;
            set;
        }

        public string proveedorDefecto
        {
            get;
            set;
        }

        public bool active_Status
        {
            get;
            set;
        }

        public string bloqueado
        {
            get;
            set;
        }

        public bool shop_Visibility
        {
            get;
            set;
        }

        public int idFam
        {
            get;
            set;
        }

        public int idSubFam
        {
            get;
            set;
        }

        public string uuidMarca
        {
            get;
            set;
        }

    }
}
