﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    public class csLineaDocumento
    {
        public string codFamiliaH
        {
            get;
            set;
        }
        
        public string codFamiliaV
        {
            get;
            set;
        }
        
        public string nombreArticulo
        {
            get;
            set;
        }
        
        public string idArticulo
        {
            get;
            set;
        }

        public string numCabecera
        {
            get;
            set;
        }

        public string numeroDoc
        {
            get;
            set;
        }

        public string numeroLinea
        {
            get;
            set;
        }

        public string codigoArticulo
        {
            get;
            set;
        }

        public string descripcionArticulo
        {
            get;
            set;
        }

        public string cantidad
        {
            get;
            set;
        }

        public string precio
        {
            get;
            set;
        }

        public double price
        {
            get;
            set;
        }

        public string descuento
        {
            get;
            set;
        }

        public string descuento2
        {
            get;
            set;
        }

        public string tipoIVA
        {
            get;
            set;
        }

        public bool ivaIncluido
        {
            get;
            set;
        }

        public string coste
        {
            get;
            set;
        }

        public string totalLinea
        {
            get;
            set;
        }

        public string codProyecto
        {
            get;
            set;
        }

        public string centroCoste
        {
            get;
            set;
        }

        public string almacen
        {
            get;
            set;
        }

        public string cuentaContable
        {
            get;
            set;
        }

        public string tallaDesc
        {
            get;
            set;
        }

        public string tallaCod
        {
            get;
            set;
        }

        public string colorDesc
        {
            get;
            set;
        }

        public string colorCod
        {
            get;
            set;
        }

        public string actionType
        {
            get;
            set;
        }

        public string numSerie
        {
            get;
            set;
        }
        public string param1
        {
            get;
            set;
        }
        public string caracterCuota
        {
            get;
            set;
        }

        public string centroCoste1{ get; set; }
        public string centroCoste2 { get; set; }
        public string centroCoste3 { get; set; }


        //Inicio Campos personalizados para Puntes
        public string SOLU_ANCHO{get;set;}
        public string SOLU_ALTO { get; set; }
        public string SOLU_UNIDADES { get; set; }
        //Fin campos personalizados para Puntes


    }
}
