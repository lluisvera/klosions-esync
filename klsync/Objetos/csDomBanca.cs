﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csDomBanca
    {
        public string cuentaext    //Número de domiciliación o cuenta bancaria
        {
            get;
            set;
        }
        
        public string numdom    //Número de domiciliación o cuenta bancaria
        {
            get;
            set;
        }

        public bool proveedor   //Indica si es proveedor o cliente
        {
            get;
            set;
        }


        public string codTercero   //Código de Cliente o Proveedor
        {
            get;
            set;
        }

        public string nomTercero   //Nombre de Cliente o Proveedor
        {
            get;
            set;
        }

        public string iban
        {
            get;
            set;
        }

        public string banco
        {
            get;
            set;
        }
        public string agencia       //Oficina bancaria
        {
            get;
            set;
        }
        public string digitoControl 
        {
            get;
            set;
        }
        public string numcuenta
        {
            get;
            set;
        }

        public string bic
        {
            get;
            set;
        }

        public string nomBanco
        {
            get;
            set;
        }

        public string titular
        {
            get;
            set;
        }

        public string rpstIdBanco
        {
            get;
            set;
        }

        public string full_iban_account
        {
            get;
            set;
        }

        public string bancoPrincipal
        {
            get;
            set;
        }

    }
}
