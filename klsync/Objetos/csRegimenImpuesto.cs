﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csRegimenImpuesto
    {

        public string idRegimenImpuesto
        {
            get;
            set;
        }

        public string nombreRegimenImpuesto
        {
            get;
            set;
        }

        public string codExterno
        {
            get;
            set;
        }
    }
}
