﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    class csDocumentosPago
    {

        public string idDocumentoPago
        {
            get;
            set;
        }

        public string nomDocumentoPago
        {
            get;
            set;
        }

        public string codExternoDocuPago
        {
            get;
            set;
        }

        public string remesable
        {
            get;
            set;
        }
    }
}
