﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync.Objetos
{
    public class csCabeceraDoc
    {
        public string numSerieDocumento
        {
            get;
            set;
        }

        public string objeto
        {
            get;
            set;
        }

        public string moneda
        {
            get;
            set;
        }

        public string permalink
        {
            get;
            set;
        }

        public string external_id
        {
            get;
            set;
        }

        public string serieDoc
        {
            get;
            set;
        }

        public string serieDocTBAI
        {
            get;
            set;
        }

        public bool captio
        {
            get;
            set;
        }

        public string numDocA3
        {
            get;
            set;
        }

        public string extNumdDoc
        {
            get;
            set;
        }

        public string tipoDoc
        {
            get;
            set;
        }

        public string codIC
        {
            get;
            set;
        }

        public string id_Customer_Ext
        {
            get;
            set;
        }

        public string nombreIC
        {
            get;
            set;
        }

        public DateTime fechaDoc
        {
            get;
            set;
        }

        public string fecha
        {
            get;
            set;
        }

        public DateTime fechaContableDoc { get; set; }

        public string centroCoste
        {
            get;
            set;
        }

        public string agenteComercial
        {
            get;
            set;
        }

        public string tipoOperacion
        {
            get;
            set;
        }

        public string porc_ProntoPago
        {
            get;
            set;
        }

        public string transportista
        {
            get;
            set;
        }

        public string codFormaPago
        {
            get;
            set;
        }

        public string codDocumentoPago
        {
            get;
            set;
        }

        public string referencia
        {
            get;
            set;
        }

        public string empresa
        {
            get;
            set;
        }

        public string razonSocial
        {
            get;
            set;
        }

        public string PSidCliente
        {
            get;
            set;
        }

        public string nif
        {
            get;
            set;
        }

        public string apellidoCliente
        {
            get;
            set;
        }

        //Dirección de Facturación

        public string idDireccionFacturacionA3ERP
        {
            get;
            set;
        }

        public string direccionDirFacturacion
        {
            get;
            set;
        }

        public string codigoPostalDirFacturacion
        {
            get;
            set;
        }

        public string poblacionDirFacturacion
        {
            get;
            set;
        }

        public string provinciaDirFacturacion
        {
            get;
            set;
        }

        //Dirección de Envío

        public string idDireccionEnvioA3ERP
        {
            get;
            set;
        }

        public string direccionDirEnvio
        {
            get;
            set;
        }

        public string codigoPostalDirEnvio
        {
            get;
            set;
        }

        public string poblacionDirEnvio
        {
            get;
            set;
        }

        public string ciudadDirEnvio
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }


        public string portes
        {
            get;
            set;
        }

        public string tipoIva
        {
            get;
            set;
        }

        public string regIva
        {
            get;
            set;

        }

        public string opeccm            //Indica que es un cliente u operación de Canarias, Ceuta o Melilla
        {
            get;
            set;
        }


        public string comentariosCabecera
        {
            get;
            set;
        }

        public string comentariosPie
        {
            get;
            set;
        }
        public string comentariosDocumento
        {
            get;
            set;
        }

        public string numeroDocProveedor
        {
            get;
            set;
        }
        public string bancoEmpresa
        {
            get;
            set;
        }
        public DateTime periodoDesde
        {
            get;
            set;
        }
        public DateTime periodoHasta
        {
            get;
            set;
        }
        public bool fraConRetencion { get; set; }
        public string porcentajeRetencion { get; set; }
        public string tipoRentencion { get; set; }
        public string baseRetencion { get; set; }

        public string claveIRPF { get; set; }
        public string subclaveIRPF { get; set; }
        public int? clienteGenerico { get; set; }
        public int? direccionGenerica { get; set; }
        public string numdireccion { get; set; }
        public string pais { get; set; }
        public string idDocumentoAbonado { get; set; }


        //Inicio Campos propios para Proyecto Puntes
        public string SOLU_TITULO { get; set; }
        public string SOLU_FC_ENTREGA { get; set; }
        public string SOLU_ENTREGA { get; set; }
        public string SOLU_DIR_ENTREGA { get; set; }
        public string SOLU_MEDIDA { get; set; }
        public string SOLU_ALARMA { get; set; }
        public string SOLU_MENSAJE { get; set; }
        public string SOLU_ACABADO { get; set; }
        public string PARAM1 { get; set; }  // PEDIDO SAP
        public string SOLU_DIRECCION { get; set; }
        public string SOLU_RECIBIDO_INTERDELEG { get; set; }
        public string SOLU_FC_ENTREGA_INTERDELEG { get; set; }



        //Fin Campos propios para Proyecto 

        public bool ticketBaiActivo { get; set; }
        public bool ticketBaiFraBorrador { get; set; }
        public bool repercusionesContables { get; set; }
        public bool conVencimientos { get; set; }

        public float totalBaseImponibleDocumento { get; set;}
        public float totalImpuestosDocumento { get; set; }
        public float totalDocumento { get; set; }




    }
}
