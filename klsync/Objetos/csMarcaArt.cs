﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csMarcaArt
    {
        public string uuidMarca
        {
            get;
            set;
        }

        public string nomMarca
        {
            get;
            set;
        }

        public string codExternoMarca
        {
            get;
            set;
        }
    }
}
