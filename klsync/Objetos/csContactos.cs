﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    public class csContactos
    {
        public string codCuentaA3ERP
        {
            get;
            set;
        }

        public string tipoCuenta
        {
            get;
            set;
        }

        public string nombre
        {
            get;
            set;
        }
        public string apellidos
        {
            get;
            set;
        }
        public string email
        {
            get;
            set;
        }
        public string telf1
        {
            get;
            set;
        }
        public string telf2
        {
            get;
            set;
        }
        public string idContactoRepasat
        {
            get;
            set;
        }

        public string codClienteERP
        {
            get;
            set;
        }

        public string idClienteRepasat
        {
            get;
            set;
        }
        public string nombreCliente
        {
            get;
            set;
        }

        public string razonSocialCuenta
        {
            get;
            set;
        }
        public string cargo
        {
            get;
            set;
        }

        public string codExternoContacto
        {
            get;
            set;
        }
    }
}
