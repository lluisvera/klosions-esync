﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csWarehouses
    {
        public string idAlmacen
        {
            get;
            set;
        }

        public string nomAlmacen
        {
            get;
            set;
        }

        public string codExternoAlmacen
        {
            get;
            set;
        }
    }
}
