﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csZonaGeografica
    {
        public string idZona
        {
            get;
            set;
        }

        public string nomZona
        {
            get;
            set;
        }

        public string codExternoZona
        {
            get;
            set;
        }

    }
}
