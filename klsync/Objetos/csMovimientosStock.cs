﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Objetos
{
    class csMovimientosStock
    {
        public string codart
        {
            get;
            set;
        }

        public int idMovimientoStock
        {
            get;
            set;
        }
        public DateTime fechaMov
        {
            get;
            set;
        }

        public string almacen
        {
            get;
            set;
        }

        public string tipoMov //tipo movimiento
        {
            get;
            set;
        }

        public double unidades
        {
            get;
            set;
        }

        public string motivoMov
        {
            get;
            set;
        }

        public string idActividad
        {
            get;
            set;
        }

    }
}
