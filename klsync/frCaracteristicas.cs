﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using MySql.Data.MySqlClient;
using RestSharp;




namespace klsync
{
    public partial class frCaracteristicas : Form
    {


        private static frCaracteristicas m_FormDefInstance;
        public static frCaracteristicas DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCaracteristicas();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        
        public frCaracteristicas()
        {
            InitializeComponent();
        }
       
        

        public void cargarArticulos()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            CheckForIllegalCrossThreadCalls = false;

            DataTable tb = new DataTable();
            tb = cargarCaracteristicas(cboxCodigo.Text,cBoxCaracteristicas.Text);

            cargarDGV(dgvProductosA3, tb);
        }
        
        private void cargarDGV(DataGridView dgv, DataTable dt)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke(new MethodInvoker(() => dgv.DataSource = dt));
            else
                dgv.DataSource = dt;

           
        }
        private DataTable cargarCaracteristicas(string codigo,string caracteristica) {
            string codigoNombre = "";
            string caracteristicas = "";
            DataTable datos = new DataTable();
            csMySqlConnect mysql = new csMySqlConnect();

            if (!codigo.Equals("")) {
                codigoNombre = "AND (ps_product_lang.name LIKE '%"+codigo+ "%' OR ps_product.reference LIKE '%"+ codigo +"%')";
            }
            if (!caracteristica.Equals(""))
            {
                caracteristicas = "AND ps_feature_lang.name LIKE '%" + caracteristica+"%'";
            }


            string consulta = "select ps_product.id_product,ps_product.reference, ps_product_lang.name, ps_product.active, ps_feature_product.id_feature," +
                          "ps_feature_lang.name, ps_feature_product.id_feature_value, ps_feature_value_lang.value " +
                          "from ps_feature_product inner join ps_product_lang on ps_feature_product.id_product=ps_product_lang.id_product " +
                          "inner join ps_feature_lang on ps_feature_lang.id_feature=ps_feature_product.id_feature " +
                          "inner join ps_feature_value_lang on ps_feature_product.id_feature_value=ps_feature_value_lang.id_feature_value " +
                          "inner join ps_product on ps_feature_product.id_product=ps_product.id_product " +
                          "where ps_product_lang.id_lang=1 and ps_feature_lang.id_lang=1 and ps_feature_value_lang.id_lang=1 "+
                           caracteristicas + codigoNombre ;

            datos = mysql.cargarTabla(consulta);


            return datos;
        }

        private void btLoadAdvance_Click_1(object sender, EventArgs e)
        {
            cargarArticulos();
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            if (dgvProductosA3.RowCount > 0)
            {
                try
                {

                    DataTable dt = (DataTable)(dgvProductosA3.DataSource);

                    if (dt != null)
                    {
                        int i = 0;

                        if (dt.hasRows())
                        {
                            string fechaHoy = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
                            dt.exportToExcel("Características");

                            if (csGlobal.modoManual)
                            {
                                csUtilidades.openFolder(csGlobal.fileExportPath);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);

                }
            }
            else {
                MessageBox.Show("Debe hacer una búsqueda");
                }

        }

    }
}
