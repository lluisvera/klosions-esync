﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.ComponentModel;


namespace klsync
{
    class csFtpTools
    {
        public bool crearDirectorioFTP(string rutaBase, string usuario, string pass)
        {
            bool directorioCreado = true;
            FtpWebRequest crearDirectorio = (FtpWebRequest)FtpWebRequest.Create(rutaBase);
            try
            {
                crearDirectorio.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);
                crearDirectorio.KeepAlive = false;
                crearDirectorio.Method = WebRequestMethods.Ftp.MakeDirectory;
                crearDirectorio.GetResponse();
            }
            catch (Exception ex)
            {

                int code = System.Runtime.InteropServices.Marshal.GetExceptionCode();
                if (code == -532459699)
                {
                    //string var = "no existe el directorio";
                    directorioCreado = false;
                }
            }
            return directorioCreado;

            //Si da error al crear el directorio, verificar que existan los subdirectorios previos
        }

        public static bool DeleteFileOnFtpServer(Uri serverUri, string ftpUsername, string ftpPassword)
        {
            try
            {
                // The serverUri parameter should use the ftp:// scheme.
                // It contains the name of the server file that is to be deleted.
                // Example: ftp://contoso.com/someFile.txt.
                // 

                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                request.Method = WebRequestMethods.Ftp.DeleteFile;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                //Console.WriteLine("Delete status: {0}", response.StatusDescription);
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ExisteDirectorio(string ruta, string usuario, string pass, string directorio = null)
        {
            bool bExiste = true;
            string[] directorioTarget = new string[0];
            string rutaTarget = csGlobal.rutaFTPImagenes;

            if (directorio != null)
            {
                Array.Resize(ref directorioTarget, directorio.Length);
                directorioTarget = directorio.Select(c => c.ToString()).ToArray();
            }

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ruta);
                request.Credentials = new NetworkCredential(usuario, pass);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                // Parece que no funciona bien - 2017-03-17
                // request.UsePassive = false; // Prueba para cuando especifican el puerto de ftp

                FtpWebResponse respuesta = (FtpWebResponse)request.GetResponse();
                
                respuesta.Close();

                //1/11/2015 añadimos para cerrar la petición
            }
            catch (WebException ex)
            {
                FtpWebResponse respuestaBis;
                if (ex.Response != null)
                {
                    FtpWebResponse respuesta = (FtpWebResponse)ex.Response;
                    if (respuesta.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        bExiste = false;
                        for (int i = 0; i < directorioTarget.Count(); i++)
                        {
                            try
                            {
                                rutaTarget = rutaTarget + directorioTarget[i] + "/";
                                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(rutaTarget);
                                request.Credentials = new NetworkCredential(usuario, pass);
                                request.Method = WebRequestMethods.Ftp.ListDirectory;
                                // request.UsePassive = false;

                                respuestaBis = (FtpWebResponse)request.GetResponse();
                                respuestaBis.Close();
                            }
                            catch
                            {
                                crearDirectorioFTP(rutaTarget, usuario, pass);
                            }
                        }
                        //if (!crearDirectorioFTP(ruta, usuario, pass))
                        //{
                        //    string nuevoDir = ruta.Substring(0, ruta.Length - 2);
                        //    if (crearDirectorioFTP(nuevoDir, usuario, pass))
                        //    {
                        //        crearDirectorioFTP(ruta, usuario, pass);
                        //        bExiste = true;
                        //    }
                        //}
                    }
                }
            }
            return bExiste;
        }

        public bool BorrarDirectorio(string ruta)
        {
            bool directorioBorrado = false;

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ruta);
                request.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);
                request.KeepAlive = false;
                request.Method = WebRequestMethods.Ftp.RemoveDirectory;
                request.GetResponse();

                directorioBorrado = true;
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return directorioBorrado;
        }
    }
}
