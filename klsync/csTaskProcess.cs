﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
//para borrar luego
using System.Windows.Forms;

namespace klsync
{
    class csTaskProcess
    {

        public DataTable consultaDocsPS()
        {
            csSqlScripts sqlScript = new csSqlScripts();
            csMySqlConnect sqlConnect = new csMySqlConnect();
            MySqlConnection dataConnection = new MySqlConnection();
            dataConnection.ConnectionString = sqlConnect.conexionDestino();
            dataConnection.Open();
            //SqlDataAdapter Adapt = new SqlDataAdapter(sqlScript.selectDocsPresta(csGlobal.versionPS,true,"2013-01-01","2013-12-31",true,""), dataConnection);
            MySqlDataAdapter Adapt = new MySqlDataAdapter(sqlScript.selectDocsPresta(csGlobal.versionPS, true, "2013-01-01", "2013-12-31", true, ""), dataConnection);
            DataTable tabla = new DataTable();
            Adapt.Fill(tabla);
            return tabla;
        }
        public void CrearDocsA3()
        {
            DataTable docsPS = consultaDocsPS();
            if (docsPS.Rows.Count < 1)
            {
                ////MessageBox.Show("No hay documentos para traspasar");
            }
            else
            {

                string[,] documentos = new string[docsPS.Rows.Count, 15];
                //Repaso todas las líneas a traspasar
                for (int i = 0; i < docsPS.Rows.Count; i++)
                {
                    documentos[i, 0] = docsPS.Rows[i].ItemArray[15].ToString(); //"invoice_date"
                    documentos[i, 1] = docsPS.Rows[i].ItemArray[3].ToString();//"dni"
                    documentos[i, 2] = docsPS.Rows[i].ItemArray[6].ToString();//"firstname"
                    documentos[i, 3] = docsPS.Rows[i].ItemArray[7].ToString();//"lastname"
                    documentos[i, 4] = docsPS.Rows[i].ItemArray[8].ToString();//"company"
                    documentos[i, 5] = docsPS.Rows[i].ItemArray[5].ToString();//"address1"
                    documentos[i, 6] = docsPS.Rows[i].ItemArray[19].ToString();//"product_reference"
                    documentos[i, 7] = docsPS.Rows[i].ItemArray[21].ToString();//"product_quantity"
                    documentos[i, 8] = docsPS.Rows[i].ItemArray[22].ToString();//"unit_price_tax_incl"
                    documentos[i, 9] = docsPS.Rows[i].ItemArray[16].ToString();//"invoice_number"
                    documentos[i, 10] = docsPS.Rows[i].ItemArray[10].ToString();//"descuento"
                    documentos[i, 11] = docsPS.Rows[i].ItemArray[7].ToString();//"lastname"
                    documentos[i, 12] = docsPS.Rows[i].ItemArray[9].ToString();//"payment"
                    documentos[i, 13] = docsPS.Rows[i].ItemArray[13].ToString();//"postcode"
                    documentos[i, 14] = docsPS.Rows[i].ItemArray[14].ToString();//"city"
                }

                csa3erp A3Func = new csa3erp();
                A3Func.abrirEnlace();
                A3Func.generaDocA3(documentos);
                A3Func.cerrarEnlace();
            }

        }



    }
}
