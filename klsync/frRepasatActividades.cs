﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace klsync
{
    public partial class frRepasatActividades : Form
    {

        private static frRepasatActividades m_FormDefInstance;
        public static frRepasatActividades DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRepasatActividades();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frRepasatActividades()
        {
            InitializeComponent();
        }


        private void btLoadActividades_Click(object sender, EventArgs e)
        {
            cargarActividadesRPST();
            cargarDetalleActividadesRPST();
            


        }

        private void cargarActividadesRPST()
        {

            string fecIni = dtpFecIni.Value.ToString("yyyy/MM/dd");
            string fecFin = dtpFecFin.Value.ToString("yyyy/MM/dd");
            csSqlScriptRepasat csScriptRPST = new csSqlScriptRepasat();
            conectarDB(csScriptRPST.selectActividadesRPST(fecIni,fecFin), false, dgvActividades);
        }
        private void cargarDetalleActividadesRPST()
        {

            string fecIni = dtpFecIni.Value.ToString("yyyy/MM/dd");
            string fecFin = dtpFecFin.Value.ToString("yyyy/MM/dd");
            csSqlScriptRepasat csScriptRPST = new csSqlScriptRepasat();
            conectarDB(csScriptRPST.selectDetalleActividadesRPST(fecIni, fecFin), false, dgvLineasActividades);
        }

        private void conectarDB(string sqlScript, bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {

                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }

                    }
                    numeroDocumentos = table.Rows.Count - contador;

                }


            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }

        private void exportarAExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csOfficeIntegration integradorOffice = new csOfficeIntegration();
            integradorOffice.ExportToExcel(dgvActividades);
        }

        private void frRepasatActividades_Load(object sender, EventArgs e)
        {
            
        }

        private void cboxFiltroPorEmpleado_CheckedChanged(object sender, EventArgs e)
        {
            if (cboxFiltroPorEmpleado.Checked)
            {
                csSqlScriptRepasat ScriptRepasat = new csSqlScriptRepasat();

                csMySqlConnect conectorMySql = new csMySqlConnect();
                cboxTrabajadores.DataSource = conectorMySql.select(ScriptRepasat.selectTrabajadoresActividadesRPST());
            }
        }

        private void btTraspasatA3_Click(object sender, EventArgs e)
        {
            traspasarActividadesA3();
        
        }

        

        private void traspasarActividadesA3()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            DataSet A3Dataset = new DataSet("DocumentosA3");
            DataTable LineasDocs = new DataTable("LineasDocs");
            DataTable CabeceraDocs = new DataTable("CabeceraDocs");

            ////MessageBox.Show("Dataset Cargado");
            CabeceraDocs = A3CabeceraDocs();
            LineasDocs = A3TablaLineas();
            CabeceraDocs.TableName = "CabeceraDocs";
            LineasDocs.TableName = "LineasDocs";
            A3Dataset.Tables.Add(CabeceraDocs);
            A3Dataset.Tables.Add(LineasDocs);

            csa3erp A3Func = new csa3erp();
            A3Func.abrirEnlace();
            A3Func.generaDocA3DatasetFromRPST(A3Dataset);
            A3Func.cerrarEnlace();
            mySqlConnect.actualizarDocumentosTraspasadosRPST(CabeceraDocs);


        }

        

        private DataTable A3CabeceraDocs()
        {
            DataTable CabeceraDocs = new DataTable();
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScriptRepasat sqlScript = new csSqlScriptRepasat();
            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript.selectActividadesRPST(dtpFecIni.Value.ToString("yy-MM-dd"),dtpFecFin.Value.ToString("yy-MM-dd")), conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvActividades.DataSource = bSource;
                CabeceraDocs = table;

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

            return CabeceraDocs;
        }

        private DataTable A3TablaLineas()
        {
            DataTable CabeceraDocs = new DataTable();
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScriptRepasat sqlScript = new csSqlScriptRepasat();
            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript.selectDetalleActividadesRPST(dtpFecIni.Value.ToString("yy-MM-dd"), dtpFecFin.Value.ToString("yy-MM-dd")), conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvLineasActividades.DataSource = bSource;
                CabeceraDocs = table;

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

            return CabeceraDocs;
        }




    }
}
