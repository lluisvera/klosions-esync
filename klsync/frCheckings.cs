﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace klsync
{
    public partial class frCheckings : Form
    {
        private static frCheckings m_FormDefInstance;
        public static frCheckings DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCheckings();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        
        public frCheckings()
        {
            InitializeComponent();
        }

        private void comprobarTodo()
        {
            var mysql = csUtilidades.check_MySQL_DB();
            var sql = csUtilidades.check_SQL_DB();
            var api = isValidURL(csGlobal.APIUrl + "products?ws_key=" + csGlobal.webServiceKey);
            var ftp = isValidFTP();
            var folder = folderExists(csGlobal.rutaImagenes);

            marcarCheckListBox(mysql, sql, api, ftp, folder);
        }

        private void marcarCheckListBox(bool mysql, bool sql, bool api, bool ftp, bool folder)
        {
            if (mysql)
            {
                clb.SetItemChecked(0, true);
            }

            if (sql)
            {
                clb.SetItemChecked(1, true);
            }

            if (api)
            {
                clb.SetItemChecked(2, true);
            }

            if (ftp)
            {
                clb.SetItemChecked(3, true);
            }

            if (folder)
            {
                clb.SetItemChecked(4, true);
            }
        }

        #region CHECKS

        private bool isValidURL(string url)
        {
            WebRequest webRequest = WebRequest.Create(url);
            WebResponse webResponse;
            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch //If exception thrown then couldn't get response from address
            {
                return false;
            }

            return true;
        }

        private bool isValidFTP()
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(csGlobal.nombreServidor);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);
                request.GetResponse();
            }
            catch (WebException ex)
            {
                return false;
            }

            return true;
        }

        private bool folderExists(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }            
        }

        #endregion

        private void tsb_Click(object sender, EventArgs e)
        {
            comprobarTodo();
        }
    }
}
