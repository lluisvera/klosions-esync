﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Data;

namespace klsync
{
    class csRepLog
    {
        public string MOD = "MOD";
        public string ALT = "ALT";
        public string BOR = "BOR";

        private csSqlConnects sql = new csSqlConnects();
        private csPSWebService psWebService = new csPSWebService();

        private void informarCamposTablaKlsRepLog(string tabla)
        {
            if (!csUtilidades.existeRegistroSQL("select tabla from kls_replog where tabla = '" + tabla + "'"))
            {
                csUtilidades.ejecutarConsulta("insert into kls_replog values('" + DateTime.Now + "', '" + tabla + "')", false);
            }
        }

        public void cambiosTabla(string tabla)
        {
            informarCamposTablaKlsRepLog(tabla);

            DataTable dt = new DataTable();

            DateTime fecha = Convert.ToDateTime(sql.obtenerCampoTabla("SELECT FECHA_MOD FROM KLS_REPLOG WHERE TABLA = '" + tabla + "'"));

            dt = sql.cargarDatosTablaA3("select * from replog where fecha > '" + fecha + "' and tabla = '" + tabla + "'");

            // Llamamos al método que se encargará de gestionar altas, modificaciones y borrados
            if (dt.Rows.Count > 0)
            {
                switch (tabla)
                {
                    case "ARTICULO":
                        articulos(dt);
                        csStocks stock = new csStocks();
                        stock.stock();
                        break;
                    case "DIRENT":
                        direcciones(dt);
                        break;
                    case "TALLAS":
                        combinarNuevasTallas(dt);
                        break;
                }
            }

            if (csUtilidades.verificarDt(dt))
            {
                setearTabla(tabla); // Si todo ha ido bien seteamos la fecha a dia
            }
        }

        private void direcciones(DataTable dt)
        {
            // 1. select * from replog where tabla = 'dirent' ------- idreg1 = codcli, idreg2 = numdir
            // 2. select SELECT * FROM DIRENT WHERE LTRIM(CODCLI) = 9 AND LTRIM(NUMDIR) = 2
            // 3. setear tabla 

        }

        private void combinarNuevasTallas(DataTable dt)
        {
            csSqlConnects sqlConnects = new csSqlConnects();
            
            string familiaTalla = "";
            string nuevaTalla = "";
            DataTable articulosAfectados = new DataTable();

            foreach (DataRow talla in dt.Rows)
            {
                //Obtengo la familia de tallas y la talla creada nueva
                familiaTalla = talla["IDREG1"].ToString().Trim();
                nuevaTalla = talla["IDREG2"].ToString().Trim();
                //string query= "SELECT CASE WHEN VERTICAL='F' THEN 'HORIZONTAL' ELSE 'VERTICAL' END AS TIPOATRIBUTO FROM FAMILIATALLA WHERE LTRIM(CODFAMTALLA)='" + familiaTalla + "'";
                ////detecto que tipo de atributo estamos creando
                //verticalHorizontal = sqlConnects.obtenerCampoTabla(query);
                //articulosAfectados = sqlConnects.obtenerDatosSQLScript("SELECT CODART, CODFAMTALLAH, CODFAMTALLAV FROM ARTICULO WHERE LTRIM(CODFAMTALLAH)='" + familiaTalla + "' OR LTRIM(CODFAMTALLAV)='" + familiaTalla + "'");

                csTallasYColoresV2 tycV2 = new csTallasYColoresV2();

                tycV2.generarTodasLasCombinacionesA3(nuevaTalla, familiaTalla);
            
            }
        
        
        }

        private void setearTabla(string tabla)
        {
            string consulta = "UPDATE KLS_REPLOG SET FECHA_MOD = '" + DateTime.Now + "' WHERE TABLA = '" + tabla + "'";
            csUtilidades.ejecutarConsulta(consulta, false);
        }

        private void articulos(DataTable dt)
        {
            // Gestionar el datatable y diferenciar las altas de las modificaciones, etc

            //DataTable alta = csUtilidades.dataRowArray2DataTable(dt.Select("MOVIMIENTO = 'ALT'"));
            DataTable modificacion = csUtilidades.dataRowArray2DataTable(dt.Select("MOVIMIENTO = 'MOD'"));
            //DataTable borrado = csUtilidades.dataRowArray2DataTable(dt.Select("MOVIMIENTO = 'BOR'"));

            modificacionArticulo(modificacion);
        }

        private void modificacionArticulo(DataTable dt)
        {
            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string codart = item["IDREG1"].ToString().Trim();
                    string id_product = sql.obtenerCampoTabla("select KLS_ID_SHOP from articulo where ltrim(codart) = '" + codart.Trim() + "'");

                    if (id_product != "")
                    {
                        //string a = "";
                        psWebService.cdPSWebService("PUT", "products", "", id_product, true, codart);
                    }
                }
            }
        }

        /// <summary>
        /// Cuando se hace alguna operación desde Esync contra A3, marcar el registro en el Replog como cambio realizado
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public void insertarRegistroRegLog(string codigo, string tabla, string tipo)
        {
            string campos = "(FECHA, IDREG1, IDREG2, IDREP, MOVIMIENTO, TABLA, USUARIO)";
            string ultimoRegistro = "(SELECT MAX(IDREP) + 1 FROM REPLOG)";
            string codart = "(SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + codigo + "')";
            //el índice IDREP, no es autoincrementable, por lo que calculo el máximo y le sumo uno en el momento de hacer el Insert
            string valores = "(GETDATE(), " + codart + ",'', " + ultimoRegistro + ",'MOD', '" + tabla + "', 'SA')";
            string consulta = "INSERT INTO REPLOG " + campos + " VALUES " + valores;
            csUtilidades.ejecutarConsulta(consulta, false);
            //Actualizo la tabla Indicadores, que es la que guarda el último valor del indice de la tabla replog
            string consulta2 = "UPDATE IDENTIFICADORES SET VALOR=(SELECT MAX(IDREP) FROM REPLOG) WHERE NOMIDE='IDREP'";
            csUtilidades.ejecutarConsulta(consulta2, false);

            // ACTUALIZAMOS
            //cambiosTabla(tabla);
        }

    }
}
