﻿namespace klsync
{
    partial class frConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frConfig));
            this.button2 = new System.Windows.Forms.Button();
            this.cbConexiones = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbNombreConexion = new System.Windows.Forms.Label();
            this.tbNombreConexion = new System.Windows.Forms.TextBox();
            this.toolTipConfig = new System.Windows.Forms.ToolTip(this.components);
            this.btTablasAuxA3 = new System.Windows.Forms.Button();
            this.cbCaractEnlace = new System.Windows.Forms.ComboBox();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.cbAlmacenA3 = new System.Windows.Forms.ComboBox();
            this.cboxDefaultLang = new System.Windows.Forms.ComboBox();
            this.cboxIntegratedSecurity = new System.Windows.Forms.ComboBox();
            this.cboxDefCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboxUsaProntoPago = new System.Windows.Forms.ComboBox();
            this.cboxModoTyC = new System.Windows.Forms.ComboBox();
            this.btCrearConfiguracion = new System.Windows.Forms.Button();
            this.tbTablasAuxiliares = new System.Windows.Forms.Button();
            this.btnSendNotifyTest = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.tabEmail = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnActualizarProvincias = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboxProducts = new System.Windows.Forms.CheckBox();
            this.cboxIdiomas = new System.Windows.Forms.CheckBox();
            this.cboxTallasyColores = new System.Windows.Forms.CheckBox();
            this.lbl_IS = new System.Windows.Forms.Label();
            this.btTestA3 = new System.Windows.Forms.Button();
            this.tbPasswordA3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbUserA3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbDatabaseA3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbServerA3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboxModeAp = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtBackOffice = new System.Windows.Forms.TextBox();
            this.txtTienda = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblTablasAux = new System.Windows.Forms.Label();
            this.tbWebServicekey = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbPasswordFTP = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbUserFTP = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btTestPS = new System.Windows.Forms.Button();
            this.tbPasswordPS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbUserPS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDatabasePS = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbServerPS = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblModoTallasYColores = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tbEstadosPedidosBloqueados = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btBuscarAlmacenes = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cbIvaIncluido = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbDocDestino = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbVersionPS = new System.Windows.Forms.ComboBox();
            this.btCargarAnalitica = new System.Windows.Forms.Button();
            this.lblNivelesAnalitica = new System.Windows.Forms.Label();
            this.tbNivelesAnalitica = new System.Windows.Forms.TextBox();
            this.cbNivel3 = new System.Windows.Forms.ComboBox();
            this.cbNivel2 = new System.Windows.Forms.ComboBox();
            this.cbNivel1 = new System.Windows.Forms.ComboBox();
            this.cboxAnalitica = new System.Windows.Forms.CheckBox();
            this.cbClienteGenerico = new System.Windows.Forms.ComboBox();
            this.cbSerie = new System.Windows.Forms.ComboBox();
            this.cboxClienteGenerico = new System.Windows.Forms.CheckBox();
            this.btSelectClienteGen = new System.Windows.Forms.Button();
            this.btSelectSerie = new System.Windows.Forms.Button();
            this.tabDirectorios = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.tbRutaRemotaImg = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbNombreServidor = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbAPIUrl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbRutaFicConfig = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbRutaFTPImagenes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbRutaImagenes = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRutaA3 = new System.Windows.Forms.TextBox();
            this.btBuscarDirectorioConfig = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btBuscarRutaImagenes = new System.Windows.Forms.Button();
            this.btBuscarDirectorioA3 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cb_Webservice = new System.Windows.Forms.CheckBox();
            this.cbA3 = new System.Windows.Forms.CheckBox();
            this.cbPrestashop = new System.Windows.Forms.CheckBox();
            this.cbIdiomas = new System.Windows.Forms.CheckBox();
            this.cbAddonPS = new System.Windows.Forms.CheckBox();
            this.cbXML = new System.Windows.Forms.CheckBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label44 = new System.Windows.Forms.Label();
            this.tbEmailCustomerNotify = new System.Windows.Forms.TextBox();
            this.textUsuarioPass = new System.Windows.Forms.TextBox();
            this.textUsuarioEmail = new System.Windows.Forms.TextBox();
            this.textEmailSMTP = new System.Windows.Forms.TextBox();
            this.textEmailDestino = new System.Windows.Forms.TextBox();
            this.comboServirPedido = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textEmailEnvio = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnSaveEmail = new System.Windows.Forms.Button();
            this.btGrabarCambios = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.btNuevoEnlace = new System.Windows.Forms.Button();
            this.btBorrarEnlace = new System.Windows.Forms.Button();
            this.btnUpdateCountry = new System.Windows.Forms.Button();
            this.tabEmail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabDirectorios.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Navy;
            this.button2.Location = new System.Drawing.Point(1103, 97);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 30);
            this.button2.TabIndex = 31;
            this.button2.Text = "Crear XML INVENIO";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbConexiones
            // 
            this.cbConexiones.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConexiones.FormattingEnabled = true;
            this.cbConexiones.Location = new System.Drawing.Point(433, 18);
            this.cbConexiones.Name = "cbConexiones";
            this.cbConexiones.Size = new System.Drawing.Size(176, 32);
            this.cbConexiones.TabIndex = 33;
            this.cbConexiones.SelectedIndexChanged += new System.EventHandler(this.cbConexiones_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(218, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(197, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "Seleccionar Conexión";
            // 
            // lbNombreConexion
            // 
            this.lbNombreConexion.AutoSize = true;
            this.lbNombreConexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreConexion.ForeColor = System.Drawing.Color.Navy;
            this.lbNombreConexion.Location = new System.Drawing.Point(19, 109);
            this.lbNombreConexion.Name = "lbNombreConexion";
            this.lbNombreConexion.Size = new System.Drawing.Size(152, 24);
            this.lbNombreConexion.TabIndex = 36;
            this.lbNombreConexion.Text = "Nueva Conexión";
            // 
            // tbNombreConexion
            // 
            this.tbNombreConexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreConexion.Location = new System.Drawing.Point(222, 104);
            this.tbNombreConexion.Name = "tbNombreConexion";
            this.tbNombreConexion.Size = new System.Drawing.Size(176, 29);
            this.tbNombreConexion.TabIndex = 9;
            // 
            // btTablasAuxA3
            // 
            this.btTablasAuxA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTablasAuxA3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btTablasAuxA3.Location = new System.Drawing.Point(13, 38);
            this.btTablasAuxA3.Name = "btTablasAuxA3";
            this.btTablasAuxA3.Size = new System.Drawing.Size(102, 39);
            this.btTablasAuxA3.TabIndex = 41;
            this.btTablasAuxA3.Text = "Tablas Aux.";
            this.btTablasAuxA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTipConfig.SetToolTip(this.btTablasAuxA3, "Creación de Tablas Auxiliares en Prestashop");
            this.btTablasAuxA3.UseVisualStyleBackColor = true;
            this.btTablasAuxA3.Click += new System.EventHandler(this.btTablasAuxA3_Click);
            // 
            // cbCaractEnlace
            // 
            this.cbCaractEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCaractEnlace.FormattingEnabled = true;
            this.cbCaractEnlace.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbCaractEnlace.Location = new System.Drawing.Point(315, 175);
            this.cbCaractEnlace.Name = "cbCaractEnlace";
            this.cbCaractEnlace.Size = new System.Drawing.Size(54, 24);
            this.cbCaractEnlace.TabIndex = 52;
            this.toolTipConfig.SetToolTip(this.cbCaractEnlace, "Informar aquí del número campo Característica de A3ERP \r\nque indica si un artícul" +
        "o se enlaza con PRESTASHOP.");
            // 
            // cbCategoria
            // 
            this.cbCategoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbCategoria.Location = new System.Drawing.Point(315, 202);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(54, 24);
            this.cbCategoria.TabIndex = 54;
            this.toolTipConfig.SetToolTip(this.cbCategoria, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cbAlmacenA3
            // 
            this.cbAlmacenA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAlmacenA3.FormattingEnabled = true;
            this.cbAlmacenA3.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbAlmacenA3.Location = new System.Drawing.Point(315, 229);
            this.cbAlmacenA3.Name = "cbAlmacenA3";
            this.cbAlmacenA3.Size = new System.Drawing.Size(54, 24);
            this.cbAlmacenA3.TabIndex = 56;
            this.toolTipConfig.SetToolTip(this.cbAlmacenA3, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cboxDefaultLang
            // 
            this.cboxDefaultLang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxDefaultLang.FormattingEnabled = true;
            this.cboxDefaultLang.Items.AddRange(new object[] {
            "CAS",
            "CAT",
            "ING",
            "FRA",
            "ITA"});
            this.cboxDefaultLang.Location = new System.Drawing.Point(315, 256);
            this.cboxDefaultLang.Name = "cboxDefaultLang";
            this.cboxDefaultLang.Size = new System.Drawing.Size(54, 24);
            this.cboxDefaultLang.TabIndex = 60;
            this.toolTipConfig.SetToolTip(this.cboxDefaultLang, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cboxIntegratedSecurity
            // 
            this.cboxIntegratedSecurity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxIntegratedSecurity.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxIntegratedSecurity.FormattingEnabled = true;
            this.cboxIntegratedSecurity.Items.AddRange(new object[] {
            "True",
            "False"});
            this.cboxIntegratedSecurity.Location = new System.Drawing.Point(203, 127);
            this.cboxIntegratedSecurity.Name = "cboxIntegratedSecurity";
            this.cboxIntegratedSecurity.Size = new System.Drawing.Size(127, 24);
            this.cboxIntegratedSecurity.TabIndex = 64;
            this.toolTipConfig.SetToolTip(this.cboxIntegratedSecurity, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cboxDefCustomerGroup
            // 
            this.cboxDefCustomerGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxDefCustomerGroup.FormattingEnabled = true;
            this.cboxDefCustomerGroup.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cboxDefCustomerGroup.Location = new System.Drawing.Point(315, 283);
            this.cboxDefCustomerGroup.Name = "cboxDefCustomerGroup";
            this.cboxDefCustomerGroup.Size = new System.Drawing.Size(54, 24);
            this.cboxDefCustomerGroup.TabIndex = 61;
            this.toolTipConfig.SetToolTip(this.cboxDefCustomerGroup, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cboxUsaProntoPago
            // 
            this.cboxUsaProntoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxUsaProntoPago.FormattingEnabled = true;
            this.cboxUsaProntoPago.Items.AddRange(new object[] {
            "SI",
            "NO"});
            this.cboxUsaProntoPago.Location = new System.Drawing.Point(315, 310);
            this.cboxUsaProntoPago.Name = "cboxUsaProntoPago";
            this.cboxUsaProntoPago.Size = new System.Drawing.Size(54, 24);
            this.cboxUsaProntoPago.TabIndex = 65;
            this.toolTipConfig.SetToolTip(this.cboxUsaProntoPago, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // cboxModoTyC
            // 
            this.cboxModoTyC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxModoTyC.FormattingEnabled = true;
            this.cboxModoTyC.Items.AddRange(new object[] {
            "A3NOPSNO",
            "A3SIPSSI",
            "A3NOPSSI",
            "DISMAY"});
            this.cboxModoTyC.Location = new System.Drawing.Point(315, 362);
            this.cboxModoTyC.Name = "cboxModoTyC";
            this.cboxModoTyC.Size = new System.Drawing.Size(54, 24);
            this.cboxModoTyC.TabIndex = 69;
            this.toolTipConfig.SetToolTip(this.cboxModoTyC, "Informar aquí del número campo Característica de A3ERP \r\nque indica la Categoría " +
        "al que pertenece un artículo en PRESTASHOP.");
            // 
            // btCrearConfiguracion
            // 
            this.btCrearConfiguracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCrearConfiguracion.ForeColor = System.Drawing.Color.Navy;
            this.btCrearConfiguracion.Image = ((System.Drawing.Image)(resources.GetObject("btCrearConfiguracion.Image")));
            this.btCrearConfiguracion.Location = new System.Drawing.Point(404, 104);
            this.btCrearConfiguracion.Name = "btCrearConfiguracion";
            this.btCrearConfiguracion.Size = new System.Drawing.Size(32, 32);
            this.btCrearConfiguracion.TabIndex = 35;
            this.btCrearConfiguracion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTipConfig.SetToolTip(this.btCrearConfiguracion, "Grabar Nueva Conexión");
            this.btCrearConfiguracion.UseVisualStyleBackColor = true;
            this.btCrearConfiguracion.Click += new System.EventHandler(this.btCrearConfiguracion_Click);
            // 
            // tbTablasAuxiliares
            // 
            this.tbTablasAuxiliares.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTablasAuxiliares.Image = ((System.Drawing.Image)(resources.GetObject("tbTablasAuxiliares.Image")));
            this.tbTablasAuxiliares.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tbTablasAuxiliares.Location = new System.Drawing.Point(208, 322);
            this.tbTablasAuxiliares.Name = "tbTablasAuxiliares";
            this.tbTablasAuxiliares.Size = new System.Drawing.Size(128, 37);
            this.tbTablasAuxiliares.TabIndex = 40;
            this.tbTablasAuxiliares.Text = "Tablas Aux.";
            this.tbTablasAuxiliares.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTipConfig.SetToolTip(this.tbTablasAuxiliares, "Creación de Tablas Auxiliares en Prestashop");
            this.tbTablasAuxiliares.UseVisualStyleBackColor = true;
            this.tbTablasAuxiliares.Click += new System.EventHandler(this.tbTablasAuxiliares_Click);
            // 
            // btnSendNotifyTest
            // 
            this.btnSendNotifyTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendNotifyTest.Image = global::klsync.Properties.Resources.mail__1_;
            this.btnSendNotifyTest.Location = new System.Drawing.Point(359, 235);
            this.btnSendNotifyTest.Name = "btnSendNotifyTest";
            this.btnSendNotifyTest.Size = new System.Drawing.Size(69, 30);
            this.btnSendNotifyTest.TabIndex = 13;
            this.btnSendNotifyTest.Text = "Test";
            this.btnSendNotifyTest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTipConfig.SetToolTip(this.btnSendNotifyTest, "Test Email Notificaciones");
            this.btnSendNotifyTest.UseVisualStyleBackColor = true;
            this.btnSendNotifyTest.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(16, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 39);
            this.label14.TabIndex = 52;
            this.label14.Text = "ENLACES";
            // 
            // tabEmail
            // 
            this.tabEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabEmail.Controls.Add(this.tabPage1);
            this.tabEmail.Controls.Add(this.tabPage2);
            this.tabEmail.Controls.Add(this.tabDirectorios);
            this.tabEmail.Controls.Add(this.tabPage3);
            this.tabEmail.Controls.Add(this.tabPage4);
            this.tabEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabEmail.Location = new System.Drawing.Point(15, 171);
            this.tabEmail.Name = "tabEmail";
            this.tabEmail.SelectedIndex = 0;
            this.tabEmail.Size = new System.Drawing.Size(1237, 594);
            this.tabEmail.TabIndex = 62;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1229, 563);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Conexiones";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.cboxIntegratedSecurity);
            this.groupBox2.Controls.Add(this.lbl_IS);
            this.groupBox2.Controls.Add(this.btTestA3);
            this.groupBox2.Controls.Add(this.tbPasswordA3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbUserA3);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbDatabaseA3);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tbServerA3);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(554, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(584, 488);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "A3ERP";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnUpdateCountry);
            this.groupBox4.Controls.Add(this.btnActualizarProvincias);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(35, 272);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(394, 100);
            this.groupBox4.TabIndex = 68;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Paises y Provincias";
            // 
            // btnActualizarProvincias
            // 
            this.btnActualizarProvincias.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizarProvincias.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnActualizarProvincias.Location = new System.Drawing.Point(14, 34);
            this.btnActualizarProvincias.Name = "btnActualizarProvincias";
            this.btnActualizarProvincias.Size = new System.Drawing.Size(101, 43);
            this.btnActualizarProvincias.TabIndex = 52;
            this.btnActualizarProvincias.Text = "Provincias";
            this.btnActualizarProvincias.UseVisualStyleBackColor = true;
            this.btnActualizarProvincias.Click += new System.EventHandler(this.btnActualizarProvincias_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btTablasAuxA3);
            this.groupBox3.Controls.Add(this.cboxProducts);
            this.groupBox3.Controls.Add(this.cboxIdiomas);
            this.groupBox3.Controls.Add(this.cboxTallasyColores);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox3.Location = new System.Drawing.Point(35, 167);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(394, 104);
            this.groupBox3.TabIndex = 67;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tablas Aux. A3ERP";
            // 
            // cboxProducts
            // 
            this.cboxProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxProducts.AutoSize = true;
            this.cboxProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxProducts.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxProducts.Location = new System.Drawing.Point(168, 18);
            this.cboxProducts.Name = "cboxProducts";
            this.cboxProducts.Size = new System.Drawing.Size(148, 20);
            this.cboxProducts.TabIndex = 12;
            this.cboxProducts.Text = "kls_presta_products";
            this.cboxProducts.UseVisualStyleBackColor = true;
            // 
            // cboxIdiomas
            // 
            this.cboxIdiomas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxIdiomas.AutoSize = true;
            this.cboxIdiomas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxIdiomas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxIdiomas.Location = new System.Drawing.Point(168, 46);
            this.cboxIdiomas.Name = "cboxIdiomas";
            this.cboxIdiomas.Size = new System.Drawing.Size(144, 20);
            this.cboxIdiomas.TabIndex = 13;
            this.cboxIdiomas.Text = "kls_eSync_Idiomas";
            this.cboxIdiomas.UseVisualStyleBackColor = true;
            // 
            // cboxTallasyColores
            // 
            this.cboxTallasyColores.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxTallasyColores.AutoSize = true;
            this.cboxTallasyColores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxTallasyColores.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxTallasyColores.Location = new System.Drawing.Point(168, 74);
            this.cboxTallasyColores.Name = "cboxTallasyColores";
            this.cboxTallasyColores.Size = new System.Drawing.Size(194, 20);
            this.cboxTallasyColores.TabIndex = 14;
            this.cboxTallasyColores.Text = "kls_eSync_Tallas y Colores";
            this.cboxTallasyColores.UseVisualStyleBackColor = true;
            // 
            // lbl_IS
            // 
            this.lbl_IS.AutoSize = true;
            this.lbl_IS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_IS.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_IS.Location = new System.Drawing.Point(32, 130);
            this.lbl_IS.Name = "lbl_IS";
            this.lbl_IS.Size = new System.Drawing.Size(119, 16);
            this.lbl_IS.TabIndex = 63;
            this.lbl_IS.Text = "Integrated Security";
            // 
            // btTestA3
            // 
            this.btTestA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTestA3.ForeColor = System.Drawing.Color.Navy;
            this.btTestA3.Image = ((System.Drawing.Image)(resources.GetObject("btTestA3.Image")));
            this.btTestA3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btTestA3.Location = new System.Drawing.Point(466, 20);
            this.btTestA3.Name = "btTestA3";
            this.btTestA3.Size = new System.Drawing.Size(45, 50);
            this.btTestA3.TabIndex = 9;
            this.btTestA3.Text = "Test";
            this.btTestA3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btTestA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTestA3.UseVisualStyleBackColor = true;
            this.btTestA3.Click += new System.EventHandler(this.btTestA3_Click);
            // 
            // tbPasswordA3
            // 
            this.tbPasswordA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPasswordA3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tbPasswordA3.Location = new System.Drawing.Point(203, 102);
            this.tbPasswordA3.Name = "tbPasswordA3";
            this.tbPasswordA3.PasswordChar = '*';
            this.tbPasswordA3.Size = new System.Drawing.Size(127, 22);
            this.tbPasswordA3.TabIndex = 7;
            this.tbPasswordA3.Text = "klosions";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(32, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Password";
            // 
            // tbUserA3
            // 
            this.tbUserA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUserA3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tbUserA3.Location = new System.Drawing.Point(203, 75);
            this.tbUserA3.Name = "tbUserA3";
            this.tbUserA3.Size = new System.Drawing.Size(127, 22);
            this.tbUserA3.TabIndex = 5;
            this.tbUserA3.Text = "SA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(32, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 16);
            this.label6.TabIndex = 4;
            this.label6.Text = "Usuario";
            // 
            // tbDatabaseA3
            // 
            this.tbDatabaseA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDatabaseA3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tbDatabaseA3.Location = new System.Drawing.Point(203, 48);
            this.tbDatabaseA3.Name = "tbDatabaseA3";
            this.tbDatabaseA3.Size = new System.Drawing.Size(127, 22);
            this.tbDatabaseA3.TabIndex = 3;
            this.tbDatabaseA3.Text = "BORRAME";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(32, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "Base de Dastos A3ERP";
            // 
            // tbServerA3
            // 
            this.tbServerA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbServerA3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tbServerA3.Location = new System.Drawing.Point(203, 23);
            this.tbServerA3.Name = "tbServerA3";
            this.tbServerA3.Size = new System.Drawing.Size(260, 22);
            this.tbServerA3.TabIndex = 1;
            this.tbServerA3.Text = "VBA3ERP\\SQLEXPRESS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(32, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Servidor A3ERP";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.cboxModeAp);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.txtBackOffice);
            this.groupBox1.Controls.Add(this.txtTienda);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.lblTablasAux);
            this.groupBox1.Controls.Add(this.tbWebServicekey);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.tbPasswordFTP);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.tbUserFTP);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.tbTablasAuxiliares);
            this.groupBox1.Controls.Add(this.btTestPS);
            this.groupBox1.Controls.Add(this.tbPasswordPS);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbUserPS);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbDatabasePS);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbServerPS);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(16, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(532, 488);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PRESTASHOP";
            // 
            // cboxModeAp
            // 
            this.cboxModeAp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxModeAp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxModeAp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxModeAp.FormattingEnabled = true;
            this.cboxModeAp.Items.AddRange(new object[] {
            "ESYNC",
            "REPASAT"});
            this.cboxModeAp.Location = new System.Drawing.Point(208, 30);
            this.cboxModeAp.Name = "cboxModeAp";
            this.cboxModeAp.Size = new System.Drawing.Size(121, 24);
            this.cboxModeAp.TabIndex = 15;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(13, 38);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(102, 16);
            this.label25.TabIndex = 16;
            this.label25.Text = "Tipo Aplicación";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(13, 333);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(148, 16);
            this.label43.TabIndex = 52;
            this.label43.Text = "Crear Tablas Auxiliares";
            // 
            // txtBackOffice
            // 
            this.txtBackOffice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBackOffice.ForeColor = System.Drawing.Color.Black;
            this.txtBackOffice.Location = new System.Drawing.Point(208, 283);
            this.txtBackOffice.Name = "txtBackOffice";
            this.txtBackOffice.Size = new System.Drawing.Size(248, 22);
            this.txtBackOffice.TabIndex = 51;
            // 
            // txtTienda
            // 
            this.txtTienda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienda.ForeColor = System.Drawing.Color.Black;
            this.txtTienda.Location = new System.Drawing.Point(208, 255);
            this.txtTienda.Name = "txtTienda";
            this.txtTienda.Size = new System.Drawing.Size(248, 22);
            this.txtTienda.TabIndex = 50;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(13, 282);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(106, 16);
            this.label32.TabIndex = 49;
            this.label32.Text = "URL Back Office";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(13, 255);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(81, 16);
            this.label31.TabIndex = 48;
            this.label31.Text = "URL Tienda";
            // 
            // lblTablasAux
            // 
            this.lblTablasAux.AutoSize = true;
            this.lblTablasAux.Location = new System.Drawing.Point(29, 436);
            this.lblTablasAux.Name = "lblTablasAux";
            this.lblTablasAux.Size = new System.Drawing.Size(0, 24);
            this.lblTablasAux.TabIndex = 47;
            // 
            // tbWebServicekey
            // 
            this.tbWebServicekey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWebServicekey.ForeColor = System.Drawing.Color.Black;
            this.tbWebServicekey.Location = new System.Drawing.Point(208, 227);
            this.tbWebServicekey.Name = "tbWebServicekey";
            this.tbWebServicekey.PasswordChar = '*';
            this.tbWebServicekey.Size = new System.Drawing.Size(248, 22);
            this.tbWebServicekey.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(13, 228);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(124, 16);
            this.label26.TabIndex = 45;
            this.label26.Text = "Clave Web Service";
            // 
            // tbPasswordFTP
            // 
            this.tbPasswordFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPasswordFTP.ForeColor = System.Drawing.Color.Black;
            this.tbPasswordFTP.Location = new System.Drawing.Point(208, 199);
            this.tbPasswordFTP.Name = "tbPasswordFTP";
            this.tbPasswordFTP.PasswordChar = '*';
            this.tbPasswordFTP.Size = new System.Drawing.Size(248, 22);
            this.tbPasswordFTP.TabIndex = 44;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(13, 201);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 16);
            this.label23.TabIndex = 43;
            this.label23.Text = "Password FTP";
            // 
            // tbUserFTP
            // 
            this.tbUserFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUserFTP.ForeColor = System.Drawing.Color.Black;
            this.tbUserFTP.Location = new System.Drawing.Point(208, 171);
            this.tbUserFTP.Name = "tbUserFTP";
            this.tbUserFTP.Size = new System.Drawing.Size(248, 22);
            this.tbUserFTP.TabIndex = 42;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(13, 174);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(84, 16);
            this.label24.TabIndex = 41;
            this.label24.Text = "Usuario FTP";
            // 
            // btTestPS
            // 
            this.btTestPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTestPS.ForeColor = System.Drawing.Color.Navy;
            this.btTestPS.Image = ((System.Drawing.Image)(resources.GetObject("btTestPS.Image")));
            this.btTestPS.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btTestPS.Location = new System.Drawing.Point(462, 24);
            this.btTestPS.Name = "btTestPS";
            this.btTestPS.Size = new System.Drawing.Size(46, 50);
            this.btTestPS.TabIndex = 8;
            this.btTestPS.Text = "Test";
            this.btTestPS.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btTestPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btTestPS.UseVisualStyleBackColor = true;
            this.btTestPS.Click += new System.EventHandler(this.btTestPS_Click);
            // 
            // tbPasswordPS
            // 
            this.tbPasswordPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPasswordPS.ForeColor = System.Drawing.Color.Black;
            this.tbPasswordPS.Location = new System.Drawing.Point(208, 143);
            this.tbPasswordPS.Name = "tbPasswordPS";
            this.tbPasswordPS.PasswordChar = '*';
            this.tbPasswordPS.Size = new System.Drawing.Size(248, 22);
            this.tbPasswordPS.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password MySql";
            // 
            // tbUserPS
            // 
            this.tbUserPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUserPS.ForeColor = System.Drawing.Color.Black;
            this.tbUserPS.Location = new System.Drawing.Point(208, 115);
            this.tbUserPS.Name = "tbUserPS";
            this.tbUserPS.Size = new System.Drawing.Size(248, 22);
            this.tbUserPS.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(13, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Usuario MySQL";
            // 
            // tbDatabasePS
            // 
            this.tbDatabasePS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDatabasePS.ForeColor = System.Drawing.Color.Black;
            this.tbDatabasePS.Location = new System.Drawing.Point(208, 87);
            this.tbDatabasePS.Name = "tbDatabasePS";
            this.tbDatabasePS.Size = new System.Drawing.Size(248, 22);
            this.tbDatabasePS.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Base de Dastos Prestashop";
            // 
            // tbServerPS
            // 
            this.tbServerPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbServerPS.ForeColor = System.Drawing.Color.Black;
            this.tbServerPS.Location = new System.Drawing.Point(208, 59);
            this.tbServerPS.Name = "tbServerPS";
            this.tbServerPS.Size = new System.Drawing.Size(248, 22);
            this.tbServerPS.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor Prestashop";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cboxModoTyC);
            this.tabPage2.Controls.Add(this.lblModoTallasYColores);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.tbEstadosPedidosBloqueados);
            this.tabPage2.Controls.Add(this.cboxUsaProntoPago);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.cboxDefCustomerGroup);
            this.tabPage2.Controls.Add(this.cboxDefaultLang);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.btBuscarAlmacenes);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.cbAlmacenA3);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.cbCategoria);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.cbCaractEnlace);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.cbIvaIncluido);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.cbDocDestino);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.cbVersionPS);
            this.tabPage2.Controls.Add(this.btCargarAnalitica);
            this.tabPage2.Controls.Add(this.lblNivelesAnalitica);
            this.tabPage2.Controls.Add(this.tbNivelesAnalitica);
            this.tabPage2.Controls.Add(this.cbNivel3);
            this.tabPage2.Controls.Add(this.cbNivel2);
            this.tabPage2.Controls.Add(this.cbNivel1);
            this.tabPage2.Controls.Add(this.cboxAnalitica);
            this.tabPage2.Controls.Add(this.cbClienteGenerico);
            this.tabPage2.Controls.Add(this.cbSerie);
            this.tabPage2.Controls.Add(this.cboxClienteGenerico);
            this.tabPage2.Controls.Add(this.btSelectClienteGen);
            this.tabPage2.Controls.Add(this.btSelectSerie);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1229, 563);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Preferencias";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblModoTallasYColores
            // 
            this.lblModoTallasYColores.AutoSize = true;
            this.lblModoTallasYColores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModoTallasYColores.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblModoTallasYColores.Location = new System.Drawing.Point(56, 363);
            this.lblModoTallasYColores.Name = "lblModoTallasYColores";
            this.lblModoTallasYColores.Size = new System.Drawing.Size(144, 16);
            this.lblModoTallasYColores.TabIndex = 68;
            this.lblModoTallasYColores.Text = "Modo Tallas y Colores";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Navy;
            this.label42.Location = new System.Drawing.Point(423, 340);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(501, 16);
            this.label42.TabIndex = 67;
            this.label42.Text = "Indicar separados por coma, los estados de pedido que no se traspasan a A3ERP";
            // 
            // tbEstadosPedidosBloqueados
            // 
            this.tbEstadosPedidosBloqueados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEstadosPedidosBloqueados.Location = new System.Drawing.Point(316, 337);
            this.tbEstadosPedidosBloqueados.Name = "tbEstadosPedidosBloqueados";
            this.tbEstadosPedidosBloqueados.Size = new System.Drawing.Size(99, 22);
            this.tbEstadosPedidosBloqueados.TabIndex = 66;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label41.Location = new System.Drawing.Point(56, 337);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(208, 16);
            this.label41.TabIndex = 64;
            this.label41.Text = "Estados de Pedidos Bloqueados";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label40.Location = new System.Drawing.Point(56, 311);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(135, 16);
            this.label40.TabIndex = 63;
            this.label40.Text = "Usa Dto Pronto Pago";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label39.Location = new System.Drawing.Point(56, 285);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(194, 16);
            this.label39.TabIndex = 62;
            this.label39.Text = "Grupo de Clientes (por defecto)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label30.Location = new System.Drawing.Point(56, 259);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 16);
            this.label30.TabIndex = 59;
            this.label30.Text = "Idioma Principal";
            // 
            // btBuscarAlmacenes
            // 
            this.btBuscarAlmacenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBuscarAlmacenes.ForeColor = System.Drawing.Color.Navy;
            this.btBuscarAlmacenes.Location = new System.Drawing.Point(375, 296);
            this.btBuscarAlmacenes.Name = "btBuscarAlmacenes";
            this.btBuscarAlmacenes.Size = new System.Drawing.Size(30, 26);
            this.btBuscarAlmacenes.TabIndex = 58;
            this.btBuscarAlmacenes.Text = "...";
            this.btBuscarAlmacenes.UseVisualStyleBackColor = true;
            this.btBuscarAlmacenes.Click += new System.EventHandler(this.btBuscarAlmacenes_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(56, 233);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 16);
            this.label22.TabIndex = 57;
            this.label22.Text = "Almacén A3ERP";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(56, 207);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(199, 16);
            this.label20.TabIndex = 55;
            this.label20.Text = "Característica A3ERP Categoría";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(56, 181);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(182, 16);
            this.label19.TabIndex = 53;
            this.label19.Text = "Característica A3ERP Enlace";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(56, 155);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(152, 16);
            this.label17.TabIndex = 51;
            this.label17.Text = "Precios con IVA incluido";
            // 
            // cbIvaIncluido
            // 
            this.cbIvaIncluido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIvaIncluido.FormattingEnabled = true;
            this.cbIvaIncluido.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.cbIvaIncluido.Location = new System.Drawing.Point(315, 148);
            this.cbIvaIncluido.Name = "cbIvaIncluido";
            this.cbIvaIncluido.Size = new System.Drawing.Size(54, 24);
            this.cbIvaIncluido.TabIndex = 50;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(56, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(164, 16);
            this.label16.TabIndex = 49;
            this.label16.Text = "Seleccionar Serie Destino";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(56, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(126, 16);
            this.label13.TabIndex = 48;
            this.label13.Text = "Documento Destino";
            // 
            // cbDocDestino
            // 
            this.cbDocDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDocDestino.FormattingEnabled = true;
            this.cbDocDestino.Items.AddRange(new object[] {
            "Pedido",
            "Factura"});
            this.cbDocDestino.Location = new System.Drawing.Point(315, 121);
            this.cbDocDestino.Name = "cbDocDestino";
            this.cbDocDestino.Size = new System.Drawing.Size(100, 24);
            this.cbDocDestino.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(56, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 16);
            this.label12.TabIndex = 46;
            this.label12.Text = "Versión Prestashop";
            // 
            // cbVersionPS
            // 
            this.cbVersionPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVersionPS.FormattingEnabled = true;
            this.cbVersionPS.Items.AddRange(new object[] {
            "1.4",
            "1.5",
            "1.6"});
            this.cbVersionPS.Location = new System.Drawing.Point(315, 94);
            this.cbVersionPS.Name = "cbVersionPS";
            this.cbVersionPS.Size = new System.Drawing.Size(54, 24);
            this.cbVersionPS.TabIndex = 45;
            // 
            // btCargarAnalitica
            // 
            this.btCargarAnalitica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCargarAnalitica.ForeColor = System.Drawing.Color.Navy;
            this.btCargarAnalitica.Location = new System.Drawing.Point(375, 68);
            this.btCargarAnalitica.Name = "btCargarAnalitica";
            this.btCargarAnalitica.Size = new System.Drawing.Size(30, 26);
            this.btCargarAnalitica.TabIndex = 44;
            this.btCargarAnalitica.Text = "...";
            this.btCargarAnalitica.UseVisualStyleBackColor = true;
            this.btCargarAnalitica.Click += new System.EventHandler(this.btCargarAnalitica_Click);
            // 
            // lblNivelesAnalitica
            // 
            this.lblNivelesAnalitica.AutoSize = true;
            this.lblNivelesAnalitica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNivelesAnalitica.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblNivelesAnalitica.Location = new System.Drawing.Point(250, 72);
            this.lblNivelesAnalitica.Name = "lblNivelesAnalitica";
            this.lblNivelesAnalitica.Size = new System.Drawing.Size(54, 16);
            this.lblNivelesAnalitica.TabIndex = 43;
            this.lblNivelesAnalitica.Text = "Niveles";
            // 
            // tbNivelesAnalitica
            // 
            this.tbNivelesAnalitica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNivelesAnalitica.Location = new System.Drawing.Point(315, 69);
            this.tbNivelesAnalitica.Name = "tbNivelesAnalitica";
            this.tbNivelesAnalitica.Size = new System.Drawing.Size(54, 22);
            this.tbNivelesAnalitica.TabIndex = 42;
            // 
            // cbNivel3
            // 
            this.cbNivel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNivel3.FormattingEnabled = true;
            this.cbNivel3.Location = new System.Drawing.Point(581, 68);
            this.cbNivel3.Name = "cbNivel3";
            this.cbNivel3.Size = new System.Drawing.Size(70, 24);
            this.cbNivel3.TabIndex = 41;
            // 
            // cbNivel2
            // 
            this.cbNivel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNivel2.FormattingEnabled = true;
            this.cbNivel2.Location = new System.Drawing.Point(505, 68);
            this.cbNivel2.Name = "cbNivel2";
            this.cbNivel2.Size = new System.Drawing.Size(68, 24);
            this.cbNivel2.TabIndex = 40;
            // 
            // cbNivel1
            // 
            this.cbNivel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNivel1.FormattingEnabled = true;
            this.cbNivel1.Location = new System.Drawing.Point(426, 68);
            this.cbNivel1.Name = "cbNivel1";
            this.cbNivel1.Size = new System.Drawing.Size(70, 24);
            this.cbNivel1.TabIndex = 39;
            // 
            // cboxAnalitica
            // 
            this.cboxAnalitica.AutoSize = true;
            this.cboxAnalitica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxAnalitica.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxAnalitica.Location = new System.Drawing.Point(35, 73);
            this.cboxAnalitica.Name = "cboxAnalitica";
            this.cboxAnalitica.Size = new System.Drawing.Size(110, 20);
            this.cboxAnalitica.TabIndex = 38;
            this.cboxAnalitica.Text = "Usar Analítica";
            this.cboxAnalitica.UseVisualStyleBackColor = true;
            this.cboxAnalitica.CheckedChanged += new System.EventHandler(this.cboxAnalitica_CheckedChanged);
            // 
            // cbClienteGenerico
            // 
            this.cbClienteGenerico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClienteGenerico.FormattingEnabled = true;
            this.cbClienteGenerico.Location = new System.Drawing.Point(315, 15);
            this.cbClienteGenerico.Name = "cbClienteGenerico";
            this.cbClienteGenerico.Size = new System.Drawing.Size(149, 24);
            this.cbClienteGenerico.TabIndex = 35;
            // 
            // cbSerie
            // 
            this.cbSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSerie.FormattingEnabled = true;
            this.cbSerie.Location = new System.Drawing.Point(315, 42);
            this.cbSerie.Name = "cbSerie";
            this.cbSerie.Size = new System.Drawing.Size(147, 24);
            this.cbSerie.TabIndex = 33;
            // 
            // cboxClienteGenerico
            // 
            this.cboxClienteGenerico.AutoSize = true;
            this.cboxClienteGenerico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxClienteGenerico.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cboxClienteGenerico.Location = new System.Drawing.Point(35, 17);
            this.cboxClienteGenerico.Name = "cboxClienteGenerico";
            this.cboxClienteGenerico.Size = new System.Drawing.Size(158, 20);
            this.cboxClienteGenerico.TabIndex = 31;
            this.cboxClienteGenerico.Text = "Usar Cliente Genérico";
            this.cboxClienteGenerico.UseVisualStyleBackColor = true;
            // 
            // btSelectClienteGen
            // 
            this.btSelectClienteGen.BackgroundImage = global::klsync.adMan.Resources.magnifier13;
            this.btSelectClienteGen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSelectClienteGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSelectClienteGen.ForeColor = System.Drawing.Color.Navy;
            this.btSelectClienteGen.Location = new System.Drawing.Point(470, 13);
            this.btSelectClienteGen.Name = "btSelectClienteGen";
            this.btSelectClienteGen.Size = new System.Drawing.Size(25, 25);
            this.btSelectClienteGen.TabIndex = 36;
            this.btSelectClienteGen.UseVisualStyleBackColor = true;
            this.btSelectClienteGen.Click += new System.EventHandler(this.btSelectClienteGen_Click);
            // 
            // btSelectSerie
            // 
            this.btSelectSerie.BackgroundImage = global::klsync.adMan.Resources.magnifier13;
            this.btSelectSerie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btSelectSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSelectSerie.ForeColor = System.Drawing.Color.Navy;
            this.btSelectSerie.Location = new System.Drawing.Point(470, 40);
            this.btSelectSerie.Name = "btSelectSerie";
            this.btSelectSerie.Size = new System.Drawing.Size(25, 25);
            this.btSelectSerie.TabIndex = 34;
            this.btSelectSerie.UseVisualStyleBackColor = true;
            this.btSelectSerie.Click += new System.EventHandler(this.btSelectSerie_Click);
            // 
            // tabDirectorios
            // 
            this.tabDirectorios.Controls.Add(this.label29);
            this.tabDirectorios.Controls.Add(this.tbRutaRemotaImg);
            this.tabDirectorios.Controls.Add(this.label28);
            this.tabDirectorios.Controls.Add(this.tbNombreServidor);
            this.tabDirectorios.Controls.Add(this.label27);
            this.tabDirectorios.Controls.Add(this.tbAPIUrl);
            this.tabDirectorios.Controls.Add(this.label11);
            this.tabDirectorios.Controls.Add(this.label21);
            this.tabDirectorios.Controls.Add(this.tbRutaFicConfig);
            this.tabDirectorios.Controls.Add(this.label18);
            this.tabDirectorios.Controls.Add(this.tbRutaFTPImagenes);
            this.tabDirectorios.Controls.Add(this.label15);
            this.tabDirectorios.Controls.Add(this.tbRutaImagenes);
            this.tabDirectorios.Controls.Add(this.label10);
            this.tabDirectorios.Controls.Add(this.tbRutaA3);
            this.tabDirectorios.Controls.Add(this.btBuscarDirectorioConfig);
            this.tabDirectorios.Controls.Add(this.button1);
            this.tabDirectorios.Controls.Add(this.btBuscarRutaImagenes);
            this.tabDirectorios.Controls.Add(this.btBuscarDirectorioA3);
            this.tabDirectorios.Location = new System.Drawing.Point(4, 27);
            this.tabDirectorios.Name = "tabDirectorios";
            this.tabDirectorios.Padding = new System.Windows.Forms.Padding(3);
            this.tabDirectorios.Size = new System.Drawing.Size(1229, 563);
            this.tabDirectorios.TabIndex = 2;
            this.tabDirectorios.Text = "Directorios";
            this.tabDirectorios.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(19, 252);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(180, 20);
            this.label29.TabIndex = 62;
            this.label29.Text = "Ruta Remota Imagenes";
            // 
            // tbRutaRemotaImg
            // 
            this.tbRutaRemotaImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRutaRemotaImg.Location = new System.Drawing.Point(266, 246);
            this.tbRutaRemotaImg.Name = "tbRutaRemotaImg";
            this.tbRutaRemotaImg.Size = new System.Drawing.Size(365, 26);
            this.tbRutaRemotaImg.TabIndex = 61;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(19, 217);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(152, 20);
            this.label28.TabIndex = 60;
            this.label28.Text = "Nombre del Servidor";
            // 
            // tbNombreServidor
            // 
            this.tbNombreServidor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreServidor.Location = new System.Drawing.Point(266, 214);
            this.tbNombreServidor.Name = "tbNombreServidor";
            this.tbNombreServidor.Size = new System.Drawing.Size(365, 26);
            this.tbNombreServidor.TabIndex = 59;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(19, 182);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(97, 20);
            this.label27.TabIndex = 58;
            this.label27.Text = "API PS URL";
            // 
            // tbAPIUrl
            // 
            this.tbAPIUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAPIUrl.Location = new System.Drawing.Point(266, 182);
            this.tbAPIUrl.Name = "tbAPIUrl";
            this.tbAPIUrl.Size = new System.Drawing.Size(365, 26);
            this.tbAPIUrl.TabIndex = 57;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(686, 117);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(321, 18);
            this.label11.TabIndex = 56;
            this.label11.Text = "Recuerde Crear un Directorio Temporal \"Temp\"";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(19, 40);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(236, 20);
            this.label21.TabIndex = 55;
            this.label21.Text = "Directorio Fichero Configuración";
            // 
            // tbRutaFicConfig
            // 
            this.tbRutaFicConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRutaFicConfig.Location = new System.Drawing.Point(266, 40);
            this.tbRutaFicConfig.Name = "tbRutaFicConfig";
            this.tbRutaFicConfig.Size = new System.Drawing.Size(365, 26);
            this.tbRutaFicConfig.TabIndex = 53;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(19, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 20);
            this.label18.TabIndex = 52;
            this.label18.Text = "Ruta FTP Imágenes";
            // 
            // tbRutaFTPImagenes
            // 
            this.tbRutaFTPImagenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRutaFTPImagenes.Location = new System.Drawing.Point(266, 146);
            this.tbRutaFTPImagenes.Name = "tbRutaFTPImagenes";
            this.tbRutaFTPImagenes.Size = new System.Drawing.Size(365, 26);
            this.tbRutaFTPImagenes.TabIndex = 50;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(19, 111);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(152, 20);
            this.label15.TabIndex = 49;
            this.label15.Text = "Directorio Imágenes";
            // 
            // tbRutaImagenes
            // 
            this.tbRutaImagenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRutaImagenes.Location = new System.Drawing.Point(266, 111);
            this.tbRutaImagenes.Name = "tbRutaImagenes";
            this.tbRutaImagenes.Size = new System.Drawing.Size(365, 26);
            this.tbRutaImagenes.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(19, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 20);
            this.label10.TabIndex = 46;
            this.label10.Text = "Directorio A3ERP";
            // 
            // tbRutaA3
            // 
            this.tbRutaA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRutaA3.Location = new System.Drawing.Point(266, 72);
            this.tbRutaA3.Name = "tbRutaA3";
            this.tbRutaA3.Size = new System.Drawing.Size(365, 26);
            this.tbRutaA3.TabIndex = 44;
            // 
            // btBuscarDirectorioConfig
            // 
            this.btBuscarDirectorioConfig.Image = ((System.Drawing.Image)(resources.GetObject("btBuscarDirectorioConfig.Image")));
            this.btBuscarDirectorioConfig.Location = new System.Drawing.Point(637, 40);
            this.btBuscarDirectorioConfig.Name = "btBuscarDirectorioConfig";
            this.btBuscarDirectorioConfig.Size = new System.Drawing.Size(32, 29);
            this.btBuscarDirectorioConfig.TabIndex = 54;
            this.btBuscarDirectorioConfig.UseVisualStyleBackColor = true;
            this.btBuscarDirectorioConfig.Click += new System.EventHandler(this.btBuscarDirectorioConfig_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(637, 146);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 29);
            this.button1.TabIndex = 51;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btBuscarRutaImagenes
            // 
            this.btBuscarRutaImagenes.Image = ((System.Drawing.Image)(resources.GetObject("btBuscarRutaImagenes.Image")));
            this.btBuscarRutaImagenes.Location = new System.Drawing.Point(637, 111);
            this.btBuscarRutaImagenes.Name = "btBuscarRutaImagenes";
            this.btBuscarRutaImagenes.Size = new System.Drawing.Size(32, 29);
            this.btBuscarRutaImagenes.TabIndex = 48;
            this.btBuscarRutaImagenes.UseVisualStyleBackColor = true;
            this.btBuscarRutaImagenes.Click += new System.EventHandler(this.btBuscarRutaImagenes_Click);
            // 
            // btBuscarDirectorioA3
            // 
            this.btBuscarDirectorioA3.Image = ((System.Drawing.Image)(resources.GetObject("btBuscarDirectorioA3.Image")));
            this.btBuscarDirectorioA3.Location = new System.Drawing.Point(637, 72);
            this.btBuscarDirectorioA3.Name = "btBuscarDirectorioA3";
            this.btBuscarDirectorioA3.Size = new System.Drawing.Size(32, 29);
            this.btBuscarDirectorioA3.TabIndex = 45;
            this.btBuscarDirectorioA3.UseVisualStyleBackColor = true;
            this.btBuscarDirectorioA3.Click += new System.EventHandler(this.btBuscarDirectorioA3_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cb_Webservice);
            this.tabPage3.Controls.Add(this.cbA3);
            this.tabPage3.Controls.Add(this.cbPrestashop);
            this.tabPage3.Controls.Add(this.cbIdiomas);
            this.tabPage3.Controls.Add(this.cbAddonPS);
            this.tabPage3.Controls.Add(this.cbXML);
            this.tabPage3.Controls.Add(this.btnCheck);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1229, 563);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Comprobación";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cb_Webservice
            // 
            this.cb_Webservice.AutoSize = true;
            this.cb_Webservice.Enabled = false;
            this.cb_Webservice.Location = new System.Drawing.Point(36, 258);
            this.cb_Webservice.Name = "cb_Webservice";
            this.cb_Webservice.Size = new System.Drawing.Size(105, 22);
            this.cb_Webservice.TabIndex = 9;
            this.cb_Webservice.Text = "Webservice";
            this.cb_Webservice.UseVisualStyleBackColor = true;
            // 
            // cbA3
            // 
            this.cbA3.AutoSize = true;
            this.cbA3.Enabled = false;
            this.cbA3.Location = new System.Drawing.Point(36, 216);
            this.cbA3.Name = "cbA3";
            this.cbA3.Size = new System.Drawing.Size(75, 22);
            this.cbA3.TabIndex = 8;
            this.cbA3.Text = "A3ERP";
            this.cbA3.UseVisualStyleBackColor = true;
            // 
            // cbPrestashop
            // 
            this.cbPrestashop.AutoSize = true;
            this.cbPrestashop.Enabled = false;
            this.cbPrestashop.Location = new System.Drawing.Point(36, 172);
            this.cbPrestashop.Name = "cbPrestashop";
            this.cbPrestashop.Size = new System.Drawing.Size(103, 22);
            this.cbPrestashop.TabIndex = 7;
            this.cbPrestashop.Text = "Prestashop";
            this.cbPrestashop.UseVisualStyleBackColor = true;
            // 
            // cbIdiomas
            // 
            this.cbIdiomas.AutoSize = true;
            this.cbIdiomas.Enabled = false;
            this.cbIdiomas.Location = new System.Drawing.Point(36, 79);
            this.cbIdiomas.Name = "cbIdiomas";
            this.cbIdiomas.Size = new System.Drawing.Size(79, 22);
            this.cbIdiomas.TabIndex = 6;
            this.cbIdiomas.Text = "Idiomas";
            this.cbIdiomas.UseVisualStyleBackColor = true;
            // 
            // cbAddonPS
            // 
            this.cbAddonPS.AutoSize = true;
            this.cbAddonPS.Enabled = false;
            this.cbAddonPS.Location = new System.Drawing.Point(36, 125);
            this.cbAddonPS.Name = "cbAddonPS";
            this.cbAddonPS.Size = new System.Drawing.Size(149, 22);
            this.cbAddonPS.TabIndex = 5;
            this.cbAddonPS.Text = "Addon Prestashop";
            this.cbAddonPS.UseVisualStyleBackColor = true;
            // 
            // cbXML
            // 
            this.cbXML.AutoSize = true;
            this.cbXML.Enabled = false;
            this.cbXML.Location = new System.Drawing.Point(36, 36);
            this.cbXML.Name = "cbXML";
            this.cbXML.Size = new System.Drawing.Size(58, 22);
            this.cbXML.TabIndex = 4;
            this.cbXML.Text = "XML";
            this.cbXML.UseVisualStyleBackColor = true;
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(36, 339);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(323, 61);
            this.btnCheck.TabIndex = 0;
            this.btnCheck.Text = "Comprobar";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label44);
            this.tabPage4.Controls.Add(this.tbEmailCustomerNotify);
            this.tabPage4.Controls.Add(this.btnSendNotifyTest);
            this.tabPage4.Controls.Add(this.textUsuarioPass);
            this.tabPage4.Controls.Add(this.textUsuarioEmail);
            this.tabPage4.Controls.Add(this.textEmailSMTP);
            this.tabPage4.Controls.Add(this.textEmailDestino);
            this.tabPage4.Controls.Add(this.comboServirPedido);
            this.tabPage4.Controls.Add(this.label38);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Controls.Add(this.label36);
            this.tabPage4.Controls.Add(this.label35);
            this.tabPage4.Controls.Add(this.label34);
            this.tabPage4.Controls.Add(this.textEmailEnvio);
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.btnSaveEmail);
            this.tabPage4.Location = new System.Drawing.Point(4, 27);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1229, 563);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Email";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(27, 241);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(136, 18);
            this.label44.TabIndex = 15;
            this.label44.Text = "Email envío errores";
            // 
            // tbEmailCustomerNotify
            // 
            this.tbEmailCustomerNotify.Location = new System.Drawing.Point(174, 241);
            this.tbEmailCustomerNotify.Name = "tbEmailCustomerNotify";
            this.tbEmailCustomerNotify.Size = new System.Drawing.Size(179, 24);
            this.tbEmailCustomerNotify.TabIndex = 14;
            // 
            // textUsuarioPass
            // 
            this.textUsuarioPass.Location = new System.Drawing.Point(174, 192);
            this.textUsuarioPass.Name = "textUsuarioPass";
            this.textUsuarioPass.PasswordChar = '*';
            this.textUsuarioPass.Size = new System.Drawing.Size(179, 24);
            this.textUsuarioPass.TabIndex = 12;
            this.textUsuarioPass.UseSystemPasswordChar = true;
            // 
            // textUsuarioEmail
            // 
            this.textUsuarioEmail.Location = new System.Drawing.Point(174, 162);
            this.textUsuarioEmail.Name = "textUsuarioEmail";
            this.textUsuarioEmail.Size = new System.Drawing.Size(179, 24);
            this.textUsuarioEmail.TabIndex = 11;
            // 
            // textEmailSMTP
            // 
            this.textEmailSMTP.Location = new System.Drawing.Point(174, 132);
            this.textEmailSMTP.Name = "textEmailSMTP";
            this.textEmailSMTP.Size = new System.Drawing.Size(179, 24);
            this.textEmailSMTP.TabIndex = 10;
            // 
            // textEmailDestino
            // 
            this.textEmailDestino.Location = new System.Drawing.Point(174, 103);
            this.textEmailDestino.Name = "textEmailDestino";
            this.textEmailDestino.Size = new System.Drawing.Size(179, 24);
            this.textEmailDestino.TabIndex = 9;
            // 
            // comboServirPedido
            // 
            this.comboServirPedido.FormattingEnabled = true;
            this.comboServirPedido.Items.AddRange(new object[] {
            "Sí",
            "No"});
            this.comboServirPedido.Location = new System.Drawing.Point(253, 41);
            this.comboServirPedido.Name = "comboServirPedido";
            this.comboServirPedido.Size = new System.Drawing.Size(100, 26);
            this.comboServirPedido.TabIndex = 8;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(27, 192);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 18);
            this.label38.TabIndex = 7;
            this.label38.Text = "Contraseña";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(25, 162);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(135, 18);
            this.label37.TabIndex = 6;
            this.label37.Text = "Nombre de usuario";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(25, 132);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(87, 18);
            this.label36.TabIndex = 5;
            this.label36.Text = "EmailSMTP";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(25, 103);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(97, 18);
            this.label35.TabIndex = 4;
            this.label35.Text = "Email destino";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(25, 73);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(84, 18);
            this.label34.TabIndex = 3;
            this.label34.Text = "Email envío";
            // 
            // textEmailEnvio
            // 
            this.textEmailEnvio.Location = new System.Drawing.Point(174, 73);
            this.textEmailEnvio.Name = "textEmailEnvio";
            this.textEmailEnvio.Size = new System.Drawing.Size(179, 24);
            this.textEmailEnvio.TabIndex = 2;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(22, 41);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 18);
            this.label33.TabIndex = 1;
            this.label33.Text = "Servir Pedido";
            // 
            // btnSaveEmail
            // 
            this.btnSaveEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveEmail.Location = new System.Drawing.Point(1111, 519);
            this.btnSaveEmail.Name = "btnSaveEmail";
            this.btnSaveEmail.Size = new System.Drawing.Size(100, 38);
            this.btnSaveEmail.TabIndex = 0;
            this.btnSaveEmail.Text = "Grabar";
            this.btnSaveEmail.UseVisualStyleBackColor = true;
            this.btnSaveEmail.Click += new System.EventHandler(this.button3_Click);
            // 
            // btGrabarCambios
            // 
            this.btGrabarCambios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btGrabarCambios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGrabarCambios.ForeColor = System.Drawing.Color.Navy;
            this.btGrabarCambios.Image = ((System.Drawing.Image)(resources.GetObject("btGrabarCambios.Image")));
            this.btGrabarCambios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btGrabarCambios.Location = new System.Drawing.Point(949, 133);
            this.btGrabarCambios.Name = "btGrabarCambios";
            this.btGrabarCambios.Size = new System.Drawing.Size(148, 32);
            this.btGrabarCambios.TabIndex = 61;
            this.btGrabarCambios.Text = "Grabar &Cambios";
            this.btGrabarCambios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btGrabarCambios.UseVisualStyleBackColor = true;
            this.btGrabarCambios.Click += new System.EventHandler(this.btGrabarCambios_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.BackgroundImage")));
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbLogo.Location = new System.Drawing.Point(1163, 8);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(89, 76);
            this.pbLogo.TabIndex = 60;
            this.pbLogo.TabStop = false;
            // 
            // btNuevoEnlace
            // 
            this.btNuevoEnlace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btNuevoEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNuevoEnlace.ForeColor = System.Drawing.Color.Navy;
            this.btNuevoEnlace.Image = ((System.Drawing.Image)(resources.GetObject("btNuevoEnlace.Image")));
            this.btNuevoEnlace.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btNuevoEnlace.Location = new System.Drawing.Point(802, 133);
            this.btNuevoEnlace.Name = "btNuevoEnlace";
            this.btNuevoEnlace.Size = new System.Drawing.Size(141, 32);
            this.btNuevoEnlace.TabIndex = 39;
            this.btNuevoEnlace.Text = "&Nuevo Enlace";
            this.btNuevoEnlace.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btNuevoEnlace.UseVisualStyleBackColor = true;
            this.btNuevoEnlace.Click += new System.EventHandler(this.btNuevoEnlace_Click);
            // 
            // btBorrarEnlace
            // 
            this.btBorrarEnlace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBorrarEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBorrarEnlace.ForeColor = System.Drawing.Color.Navy;
            this.btBorrarEnlace.Image = ((System.Drawing.Image)(resources.GetObject("btBorrarEnlace.Image")));
            this.btBorrarEnlace.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btBorrarEnlace.Location = new System.Drawing.Point(1103, 133);
            this.btBorrarEnlace.Name = "btBorrarEnlace";
            this.btBorrarEnlace.Size = new System.Drawing.Size(145, 32);
            this.btBorrarEnlace.TabIndex = 38;
            this.btBorrarEnlace.Text = "&Borrar Enlace";
            this.btBorrarEnlace.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btBorrarEnlace.UseVisualStyleBackColor = true;
            this.btBorrarEnlace.Click += new System.EventHandler(this.btBorrarEnlace_Click);
            // 
            // btnUpdateCountry
            // 
            this.btnUpdateCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCountry.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnUpdateCountry.Location = new System.Drawing.Point(147, 34);
            this.btnUpdateCountry.Name = "btnUpdateCountry";
            this.btnUpdateCountry.Size = new System.Drawing.Size(101, 43);
            this.btnUpdateCountry.TabIndex = 53;
            this.btnUpdateCountry.Text = "Paises";
            this.btnUpdateCountry.UseVisualStyleBackColor = true;
            this.btnUpdateCountry.Click += new System.EventHandler(this.btnUpdateCountry_Click);
            // 
            // frConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1264, 777);
            this.Controls.Add(this.btGrabarCambios);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btNuevoEnlace);
            this.Controls.Add(this.btBorrarEnlace);
            this.Controls.Add(this.tbNombreConexion);
            this.Controls.Add(this.lbNombreConexion);
            this.Controls.Add(this.btCrearConfiguracion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbConexiones);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tabEmail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frConfig";
            this.Text = "Configuración Enlace";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.frConfig_Load);
            this.tabEmail.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabDirectorios.ResumeLayout(false);
            this.tabDirectorios.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cbConexiones;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btCrearConfiguracion;
        private System.Windows.Forms.Label lbNombreConexion;
        private System.Windows.Forms.TextBox tbNombreConexion;
        private System.Windows.Forms.Button btBorrarEnlace;
        private System.Windows.Forms.Button btNuevoEnlace;
        private System.Windows.Forms.ToolTip toolTipConfig;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Button btGrabarCambios;
        private System.Windows.Forms.TabControl tabEmail;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabDirectorios;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btBuscarDirectorioA3;
        private System.Windows.Forms.TextBox tbRutaA3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btTablasAuxA3;
        private System.Windows.Forms.Button btTestA3;
        private System.Windows.Forms.TextBox tbPasswordA3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbUserA3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbDatabaseA3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbServerA3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button tbTablasAuxiliares;
        private System.Windows.Forms.Button btTestPS;
        private System.Windows.Forms.TextBox tbPasswordPS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbUserPS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbDatabasePS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbServerPS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbDocDestino;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbVersionPS;
        private System.Windows.Forms.Button btCargarAnalitica;
        private System.Windows.Forms.Label lblNivelesAnalitica;
        private System.Windows.Forms.TextBox tbNivelesAnalitica;
        private System.Windows.Forms.ComboBox cbNivel3;
        private System.Windows.Forms.ComboBox cbNivel2;
        private System.Windows.Forms.ComboBox cbNivel1;
        private System.Windows.Forms.CheckBox cboxAnalitica;
        private System.Windows.Forms.Button btSelectClienteGen;
        private System.Windows.Forms.ComboBox cbClienteGenerico;
        private System.Windows.Forms.Button btSelectSerie;
        private System.Windows.Forms.ComboBox cbSerie;
        private System.Windows.Forms.CheckBox cboxClienteGenerico;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btBuscarRutaImagenes;
        private System.Windows.Forms.TextBox tbRutaImagenes;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbIvaIncluido;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbCaractEnlace;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbRutaFTPImagenes;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btBuscarDirectorioConfig;
        private System.Windows.Forms.TextBox tbRutaFicConfig;
        private System.Windows.Forms.Button btBuscarAlmacenes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbAlmacenA3;
        private System.Windows.Forms.CheckBox cboxIdiomas;
        private System.Windows.Forms.CheckBox cboxProducts;
        private System.Windows.Forms.CheckBox cboxTallasyColores;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbPasswordFTP;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbUserFTP;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cboxModeAp;
        private System.Windows.Forms.TextBox tbWebServicekey;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbAPIUrl;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbRutaRemotaImg;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbNombreServidor;
        private System.Windows.Forms.ComboBox cboxDefaultLang;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox cbIdiomas;
        private System.Windows.Forms.CheckBox cbAddonPS;
        private System.Windows.Forms.CheckBox cbXML;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.CheckBox cbA3;
        private System.Windows.Forms.CheckBox cbPrestashop;
        private System.Windows.Forms.ComboBox cboxIntegratedSecurity;
        private System.Windows.Forms.Label lbl_IS;
        private System.Windows.Forms.CheckBox cb_Webservice;
        private System.Windows.Forms.Label lblTablasAux;
        private System.Windows.Forms.TextBox txtBackOffice;
        private System.Windows.Forms.TextBox txtTienda;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textUsuarioPass;
        private System.Windows.Forms.TextBox textUsuarioEmail;
        private System.Windows.Forms.TextBox textEmailSMTP;
        private System.Windows.Forms.TextBox textEmailDestino;
        private System.Windows.Forms.ComboBox comboServirPedido;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textEmailEnvio;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnSaveEmail;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cboxDefCustomerGroup;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbEstadosPedidosBloqueados;
        private System.Windows.Forms.ComboBox cboxUsaProntoPago;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnActualizarProvincias;
        private System.Windows.Forms.ComboBox cboxModoTyC;
        private System.Windows.Forms.Label lblModoTallasYColores;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbEmailCustomerNotify;
        private System.Windows.Forms.Button btnSendNotifyTest;
        private System.Windows.Forms.Button btnUpdateCountry;
    }
}