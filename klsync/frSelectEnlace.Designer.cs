﻿namespace klsync
{
    partial class frSelectEnlace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frSelectEnlace));
            this.label14 = new System.Windows.Forms.Label();
            this.lbSelectEnlace = new System.Windows.Forms.Label();
            this.cbEnlace = new System.Windows.Forms.ComboBox();
            this.btCargarEnlace = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(12, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 40);
            this.label14.TabIndex = 55;
            this.label14.Text = "ENLACES";
            this.label14.Visible = false;
            // 
            // lbSelectEnlace
            // 
            this.lbSelectEnlace.AutoSize = true;
            this.lbSelectEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSelectEnlace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbSelectEnlace.Location = new System.Drawing.Point(15, 85);
            this.lbSelectEnlace.Name = "lbSelectEnlace";
            this.lbSelectEnlace.Size = new System.Drawing.Size(174, 24);
            this.lbSelectEnlace.TabIndex = 53;
            this.lbSelectEnlace.Text = "Seleccionar Enlace";
            this.lbSelectEnlace.Visible = false;
            // 
            // cbEnlace
            // 
            this.cbEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEnlace.FormattingEnabled = true;
            this.cbEnlace.Location = new System.Drawing.Point(195, 82);
            this.cbEnlace.Name = "cbEnlace";
            this.cbEnlace.Size = new System.Drawing.Size(180, 32);
            this.cbEnlace.TabIndex = 54;
            this.cbEnlace.Visible = false;
            // 
            // btCargarEnlace
            // 
            this.btCargarEnlace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCargarEnlace.Location = new System.Drawing.Point(497, 82);
            this.btCargarEnlace.Name = "btCargarEnlace";
            this.btCargarEnlace.Size = new System.Drawing.Size(98, 32);
            this.btCargarEnlace.TabIndex = 56;
            this.btCargarEnlace.Text = "&Aceptar";
            this.btCargarEnlace.UseVisualStyleBackColor = true;
            this.btCargarEnlace.Visible = false;
            this.btCargarEnlace.Click += new System.EventHandler(this.btCargarEnlace_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancel.Location = new System.Drawing.Point(381, 82);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(98, 32);
            this.btCancel.TabIndex = 57;
            this.btCancel.Text = "&Cancelar";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Visible = false;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.BackgroundImage")));
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbLogo.Location = new System.Drawing.Point(325, 9);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(50, 50);
            this.pbLogo.TabIndex = 58;
            this.pbLogo.TabStop = false;
            this.pbLogo.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("flowLayoutPanel1.BackgroundImage")));
            this.flowLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(289, 142);
            this.flowLayoutPanel1.TabIndex = 59;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // frSelectEnlace
            // 
            this.AcceptButton = this.btCargarEnlace;
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(289, 142);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btCargarEnlace);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbSelectEnlace);
            this.Controls.Add(this.cbEnlace);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frSelectEnlace";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SELECCIONAR ENLACE";
            this.Load += new System.EventHandler(this.frSelectEnlace_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbSelectEnlace;
        private System.Windows.Forms.ComboBox cbEnlace;
        private System.Windows.Forms.Button btCargarEnlace;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}