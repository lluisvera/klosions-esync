﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Data;
using klsync.esyncro;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace klsync
{
    class csPSWebService
    {
        //POST para crear un nuevo Registro   PUT para actualizar
        public void cdPSWebService(string metodo, string objeto, string objetoXML)
        {

            try
            {
                //string WebService_URL = csGlobal.APIUrl;
                //string WebService_LoginName = csGlobal.webServiceKey;
                //string WebService_Password = "";

                string RequestURL = csGlobal.APIUrl + objeto + "&ws_key=" + csGlobal.webServiceKey;
                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL);
                peticionWeb.Method = metodo; //POST para crear un nuevo Registro   PUT para actualizar
                peticionWeb.ContentType = "text/xml";
                //peticionWeb.ContentType = "application/xml";
                //peticionWeb.Credentials = new NetworkCredential(WebService_LoginName, WebService_Password);
                //convierto en bytes el texto del xml
                //para insertar un producto
                if (metodo == "POST")
                {
                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();
                    //byte[] byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML("CARAMELO",true,""));
                    byte[] byteArray = Encoding.UTF8.GetBytes(objetoWebService.direccionesClientesXML(""));
                    //Para probar countries
                    //byte[] byteArray = Encoding.UTF8.GetBytes(docXML());
                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da
                }

                else if (metodo == "PUT")
                {

                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();
                    byte[] byteArray = Encoding.UTF8.GetBytes(objetoWebService.actualizarDocumentoPS("12"));
                    //byte[] byteArray = Encoding.UTF8.GetBytes(objetoXML);
                    //Para probar countries
                    //byte[] byteArray = Encoding.UTF8.GetBytes(docXML());
                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    // Test(peticionWeb); //Activo si quiero ver el tipo de error que me da
                }
                else if (metodo == "DELETE")
                {
                    var request = WebRequest.Create(csGlobal.APIUrl + objeto + "/" + objetoXML);
                    request.Method = "DELETE";
                    SetBasicAuthHeader(request, csGlobal.webServiceKey);

                    //10/04/2017 prueba para mejorar velocidad 
                   //request.Proxy = null;
                    
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);

                        reader.Close();
                        dataStream.Close();
                        response.Close();
                    }
                }

                if (metodo != "DELETE")
                {
                    //10/04/2017 prueba para mejorar velocidad 
                    //peticionWeb.Proxy = null;


                    HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();

                    if (peticionWeb.HaveResponse) // si hay respuesta
                    {
                        //obtener contenido de la respuesta
                        using (Stream streamContenido = respuesta.GetResponseStream())
                        {
                            ////MessageBox.Show(new StreamReader(streamContenido).ReadToEnd());
                        }
                    }
                    respuesta.Close();
                }
            }

            catch (WebException ex)
            {
                Program.guardarErrorFichero("cdPSWebService: " + ex.ToString());
                ////MessageBox.Show(ps_e.Message);
                //agregarTexto(DateTime.Now.ToString() + " - Webservice - " + objeto + ":" + ps_e.Message.ToString());
            }

        }

        public void SetBasicAuthHeader(WebRequest request, String userName)
        {
            string authInfo = userName + ":";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
        }

        public void agregarTexto(string texto)
        {
            const string fic = @"errorLog.txt";

            System.IO.StreamWriter sw = new System.IO.StreamWriter(fic, true);
            sw.WriteLine(DateTime.Now.ToShortTimeString() + ".......................");
            sw.WriteLine(texto);
            sw.WriteLine(".......................");
            sw.Close();
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /*public void RPSTWebservice()
        {
            try
            {
                string RequestURL = "http://localhost:8080/webservice/index.php/api/customer/";
                string WebService_LoginName = "admin";
                string WebService_Password = "1234";

                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL);
                peticionWeb.Method = "POST"; //PUT para crear un nuevo Registro   POST para actualizar
                peticionWeb.ContentType = "text/xml";
                //peticionWeb.ContentType = "application/xml";
                peticionWeb.Credentials = new NetworkCredential(WebService_LoginName, WebService_Password);
                //convierto en bytes el texto del xml
                //para actualizar un producto


                //inicializo el byteArray
                byte[] byteArray = new byte[0];
                //csRPSTWebserviceObjetos objetoWebService = new csRPSTWebserviceObjetos();

                //byteArray = Encoding.UTF8.GetBytes(objetoWebService.clientesXML());

                //Para probar countries
                //byte[] byteArray = Encoding.UTF8.GetBytes(docXML());
                peticionWeb.ContentLength = byteArray.Length;
                Stream dataStream = peticionWeb.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da



                HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();

                if (respuesta.StatusCode == HttpStatusCode.OK)
                {
                    ////MessageBox.Show("Datos Actualizados");
                }
                else // revisar - alex
                {
                    if (peticionWeb.HaveResponse) // si hay respuesta
                    {
                        //obtener contenido de la respuesta
                        using (Stream streamContenido = respuesta.GetResponseStream())
                        {
                            XmlDocument documentoXML = new XmlDocument();
                            documentoXML.Load(streamContenido);

                            //string datosInsertados = new StreamReader(streamContenido).ReadToEnd();

                            //XmlDocument xdocInsertado = new XmlDocument();
                            //xdocInsertado.Load(datosIns);
                            ////MessageBox.Show(new StreamReader(streamContenido).ReadToEnd());
                            //ObtenerIdInsertado(documentoXML, objeto);


                        }
                    }
                }
                respuesta.Close();

            }

            catch (WebException ps_e)
            {
                ////MessageBox.Show(ps_e.Message);
                //agregarTexto(DateTime.Now.ToString() + ": " + ps_e.ToString());
            }
        }*/
        private string urlstring = "";

        public void cdPSWebService(string metodo, string objeto, string objetoXML, string identidad, bool nuevo, string id_product, string tipoDir = null, DataRow dr = null, int i = -1, bool TyC = false)
        {
            try
            {
                //24/12/2020 Añadido para CadCanarias, se añade porque el certificado no era compatible con esync.
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string RequestURL = csGlobal.APIUrl + objeto + "?ws_key=" + csGlobal.webServiceKey;
                urlstring = RequestURL;
                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL); //Código validado
                peticionWeb.Method = metodo; //PUT para modificar un nuevo Registro - POST para crear
                peticionWeb.ContentType = "text/xml";

                peticionWeb.UseDefaultCredentials = true;

                byte[] byteArray = new byte[0];
               
                if (metodo == "POST")
                {
                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();
                    if (objeto == "products")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML(identidad, true, id_product));
                    }
                    else if (objeto == "customers")
                    {
                        if (csGlobal.conexionDB.Contains("VILARDELL"))
                        {
                            byteArray = Encoding.UTF8.GetBytes(objetoWebService.clientesXML(dr));
                        }
                        else
                        {
                            byteArray = Encoding.UTF8.GetBytes(objetoWebService.clientesXML(identidad));
                        }
                    }
                    else if (objeto == "addresses")
                    {
                        if (csGlobal.conexionDB.Contains("VILARDELL"))
                        {
                            byteArray = Encoding.UTF8.GetBytes(objetoWebService.direccionesClientesXML(dr));
                        }
                        else
                        {
                            byteArray = Encoding.UTF8.GetBytes(objetoWebService.direccionesClientesXML(identidad, tipoDir, dr));
                        }
                    }
                    else if (objeto == "categories")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.categoriaXML(identidad));
                    }
                    
                    //deprecated 18-3-18 no se pueden crear movimientos de stock
                    //else if (objeto == "stock_availables")
                    //{
                    //    byteArray = Encoding.UTF8.GetBytes(objetoWebService.stockProductos(dr["id_stock_available"].ToString(), dr["id_product"].ToString(), dr["id_product_attribute"].ToString(), dr["quantity"].ToString(), dr["out_of_stock"].ToString()));
                    //}
                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da
                }
                else if (metodo == "PUT")  // actualización
                {
                    //descripciones(identidad);

                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();

                    if (objeto == "customers")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.clientesXML(identidad));
                    }
                    else if (objeto == "products")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML(id_product, false, identidad));
                    }
                    else if (objeto == "stock_availables")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.stockProductos(dr["id_stock_available"].ToString(), dr["id_product"].ToString(), dr["id_product_attribute"].ToString(), dr["quantity"].ToString(), dr["out_of_stock"].ToString()));
                    }

                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }

                //10/04/2017 prueba para mejorar velocidad 
                //peticionWeb.Proxy = null;

                HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();
                //HttpWebRequest respuesta = (HttpWebRequest)WebRequest.Create(urlstring);
                //respuesta.Method = "HEAD";
                //using (HttpWebResponse headResponse = (HttpWebResponse)respuesta.GetResponse())
                //{
                //    if (headResponse.StatusCode == HttpStatusCode.OK)
                //    {

                //    }
                //    else
                //    {
                //        if (peticionWeb.HaveResponse) // si hay respuesta
                //        {
                //            //obtener contenido de la respuesta
                //            using (Stream streamContenido = headResponse.GetResponseStream())
                //            {
                //                XmlDocument documentoXML = new XmlDocument();
                //                documentoXML.PreserveWhitespace = false;
                //                documentoXML.Load(streamContenido);
                //                //XmlDocument xmlDoc = new XmlDocument();

                //                // Para direcciones llevo el contador y le voy sumando 1 para cada nueva dirección
                //                i++;
                //                ObtenerIdInsertado(documentoXML, objeto, identidad, i, TyC);

                //                //En AndreuToys se marca la dirección como fiscal para que no se pueda editar
                //                if (csGlobal.nombreServidor.Contains("ANDREUTOYS") && objeto == "addresses")
                //                {
                //                    Objetos.csDireccion direcciones = new Objetos.csDireccion();
                //                    direcciones.actualizarDireccionesFiscales(identidad);
                //                }
                //            }
                //        }
                //    }
                //}
                if (respuesta.StatusCode == HttpStatusCode.OK)
                {

                }
                else
                {
                    if (peticionWeb.HaveResponse) // si hay respuesta
                    {
                        //obtener contenido de la respuesta
                        using (Stream streamContenido = respuesta.GetResponseStream())
                        {
                            XmlDocument documentoXML = new XmlDocument();
                            documentoXML.PreserveWhitespace = false;
                            documentoXML.Load(streamContenido);
                            //XmlDocument xmlDoc = new XmlDocument();

                            // Para direcciones llevo el contador y le voy sumando 1 para cada nueva dirección
                            i++;
                            ObtenerIdInsertado(documentoXML, objeto, identidad, i, TyC);

                            //En AndreuToys se marca la dirección como fiscal para que no se pueda editar
                            if (csGlobal.nombreServidor.Contains("ANDREUTOYS") && objeto == "addresses")
                            {
                                Objetos.csDireccion direcciones = new Objetos.csDireccion();
                                direcciones.actualizarDireccionesFiscales(identidad);
                            }
                        }
                    }
                }

                respuesta.Close();
            }
            catch (WebException ps_e)
            {
                string errorInfo = DateTime.Now.ToString() + " - Webservice - " + objeto + ":" + identidad + " / " + ps_e.Message.ToString() + "\n";
                string errorhelp = "\n" + "\n" + "Verificar lo siguiente: " + "\n" +
                    " - Configuración de Idiomas (están creados y mapeados en SQL)" + "\n" +
                    " - Webservice Key (Validar que el webservice esté correctamente informado en el XML)" + "\n" +
                    " - Webservice Activo (Validar que el webservice esté activo y la tabla correspondiente)" + "\n" +
                    " - Webservice Activar modo CGI para PHP debe estar activado" + "\n" +
                    " - Si es hosting nuevo de Dinahosting revisar fichero httaccess, añadir linea al final --> SecRuleRemoveById 208 210" + "\n" +
                    " - Módulo Klosions con campos adicionales informados";
                MessageBox.Show(ps_e.Message);

                Program.guardarErrorFichero(errorInfo + "\n" + "(" + urlstring + ") - " + csGlobal.globalXML + "\n" + errorhelp);
            }
        }

        private void descripciones(string id_product)
        {
            string descripcion = "<description>";
            string descripcion_short = "<description_short>";
            //Creo dos variables para alojar el contenido de las descripciones pero espadando el html
            string html_description = "";
            string html_description_short = "";

            if (!string.IsNullOrEmpty(id_product))
            {
                using (WebClient client = new WebClient())
                {
                    string url = csGlobal.APIUrl + "products/" + id_product + "?ws_key=" + csGlobal.webServiceKey;
                    //Se añade la codificación UTF8 para qeu reconozca los acentos
                    client.Encoding = System.Text.Encoding.GetEncoding("UTF-8");
                    string html = client.DownloadString(url);

                    XmlDocument xml = new XmlDocument();

                    //Cargo texto 
                    xml.LoadXml(html);

                    XmlNodeList desc = xml.SelectNodes("/prestashop/product/description/language");
                    XmlNodeList desc_short = xml.SelectNodes("/prestashop/product/description_short/language");

                    foreach (XmlNode item in desc)
                    {
                        html_description = WebUtility.HtmlEncode(item.InnerText);
                        descripcion += "<language id=\"" + item.Attributes["id"].Value + "\">" + html_description + "</language>";
                    }

                    foreach (XmlNode item in desc_short)
                    {
                        html_description_short = WebUtility.HtmlEncode(item.InnerText);
                        descripcion_short += "<language id=\"" + item.Attributes["id"].Value + "\">" + html_description_short + "</language>";
                    }
                }
            }

            descripcion += "</description>";
            descripcion_short += "</description_short>";

            //descripcion = WebUtility.HtmlEncode(descripcion);
            //descripcion_short = WebUtility.HtmlEncode(descripcion_short);

            csGlobal.description = descripcion;
            csGlobal.description_short = descripcion_short;
        }

        public void cdPSWebServiceborrame(string metodo, string objeto, string objetoXML, string identidad, bool nuevo, string id_product, string tipoDir = null, DataRow dr = null, int i = -1, bool TyC = false)
        {
            try
            {
                //METODO original Webservicekey por separado
                string WebService_URL = csGlobal.APIUrl;
                string WebService_LoginName = csGlobal.webServiceKey;
                string WebService_Password = "";

                string RequestURL = csGlobal.APIUrl + objeto + "/";

                //METODO WebserviceKey en Url
                //string RequestURL = csGlobal.APIUrl + objeto + "&ws_key=" + csGlobal.webServiceKey;
                urlstring = RequestURL;

                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL); //Código validado

                peticionWeb.Method = metodo; //PUT para modificar un nuevo Registro   POST para crear
                peticionWeb.ContentType = "text/xml";
                //añado esta línea para ver si se soluciona el error 403


                //METODO ORIGINAL
                peticionWeb.ContentType = "application/xml";
                //activo las credenciales
                peticionWeb.Credentials = new NetworkCredential(WebService_LoginName, WebService_Password);
                peticionWeb.UseDefaultCredentials = true;
                //convierto en bytes el texto del xml
                //para actualizar un producto

                byte[] byteArray = new byte[0];

                if (metodo == "POST")
                {
                    //inicializo el byteArray
                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();
                    if (objeto == "products")
                    {
                        //byte[] byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML(identidad, true, id_product));
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML(identidad, true, id_product));
                    }
                    else if (objeto == "customers")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.clientesXML(identidad));
                    }
                    else if (objeto == "addresses")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.direccionesClientesXML(identidad, tipoDir, dr));
                    }
                    else if (objeto == "categories")
                    {
                        //byteArray = Encoding.UTF8.GetBytes(objetoWebService.categoriaXML("", identidad, objetoXML));
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.categoriaXML(identidad));
                    }
                    //Para probar countries
                    //byte[] byteArray = Encoding.UTF8.GetBytes(docXML());
                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da
                }
                else if (metodo == "PUT")  //actualización
                {
                    csPSWebServiceObjetos objetoWebService = new csPSWebServiceObjetos();

                    if (objeto == "products")
                    {
                        byteArray = Encoding.UTF8.GetBytes(objetoWebService.articulosXML(identidad, false, id_product));
                    }

                    peticionWeb.ContentLength = byteArray.Length;
                    Stream dataStream = peticionWeb.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da
                }

                //peticionWeb.Headers.Add("Access-Control-Allow-Origin", "*");
                HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();

                if (respuesta.StatusCode == HttpStatusCode.OK)
                {
                    ////MessageBox.Show("Datos Actualizados");
                }
                else
                {
                    if (peticionWeb.HaveResponse) // si hay respuesta
                    {
                        //obtener contenido de la respuesta
                        using (Stream streamContenido = respuesta.GetResponseStream())
                        {
                            XmlDocument documentoXML = new XmlDocument();
                            documentoXML.Load(streamContenido);

                            //Para direcciones llevo el contador y le voy sumando 1 para cada nueva dirección
                            i++;
                            ObtenerIdInsertado(documentoXML, objeto, identidad, i, TyC);
                        }
                    }
                }
                respuesta.Close();
            }
            catch (WebException ps_e)
            {
                string errorInfo = DateTime.Now.ToString() + " - Webservice - " + objeto + ":" + identidad + " / " + ps_e.Message.ToString() + "\n";
                string errorhelp = "\n" + "\n" + "Verificar lo siguiente: " + "\n" +
                    " - Configuración de Idiomas (están creados y mapeados en SQL)" + "\n" +
                    " - Webservice Key (Validar que el webservice esté correctamente informado en el XML)" + "\n" +
                    " - Webservice Activo (Validar que el webservice esté activo y la tabla correspondiente)" + "\n" +
                    " - Webservice Activar modo CGI para PHP debe estar desactivado" + "\n" +
                    " - Módulo Klosions con campos adicionales informados";
                ////MessageBox.Show(ps_e.Message);
                Program.guardarErrorFichero(errorInfo + "\n" + "(" + urlstring + ") - " + csGlobal.globalXML + "\n" + errorhelp);
                //agregarTexto(DateTime.Now.ToString() + " - Webservice - " + objeto + ":" + identidad + " / " + ps_e.Message.ToString());
            }
        }

        public void ObtenerIdInsertado(XmlDocument xdocInsertado, string objeto, string identidad = null, int i = 0, bool TyC = false)
        {
            string id_product = "";
            string idCli = "";
            string anexoDireccionFiscal = "";
            if (csUtilidades.existeColumnaSQL("DIRENT", "KLS_FISCAL"))
            {
                anexoDireccionFiscal = " , KLS_FISCAL ";
            }
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable dt = new DataTable();
            string consultaAddresses = "";
            csDatatimeSyncro dataTimeSyncro = new csDatatimeSyncro();

            DataTable ItemsToUpdate = new DataTable();
            csPSWebService psWebService = new csPSWebService();

            ItemsToUpdate = dataTimeSyncro.obtenerDatosSyncronizacion(xdocInsertado, objeto);

            if (!csGlobal.conexionDB.Contains("VILARDELL"))
            {
                consultaAddresses = " SELECT dbo.CLIENTES.KLS_CODCLIENTE, dbo.CLIENTES.CODCLI, dbo.CLIENTES.NOMCLI, dbo.CLIENTES.RAZON, dbo.CLIENTES.VIAFISCAL, dbo.CLIENTES.VIA, dbo.CLIENTES.PROVIFISCAL, " +
                      " dbo.CLIENTES.POBFISCAL, dbo.CLIENTES.POBCLI, dbo.CLIENTES.PAISFISCAL, dbo.CLIENTES.DIRFISCAL, dbo.CLIENTES.DIRCLI, dbo.CLIENTES.DIRCLI1, dbo.CLIENTES.CODPROVI, " +
                      " dbo.CLIENTES.CODPAIS, dbo.DIRENT.DIRENT1, dbo.CLIENTES.CODPROVI AS Expr1, dbo.CLIENTES.CODREP, dbo.CLIENTES.CODPAIS AS Expr2, dbo.DIRENT.TELENT1, dbo.DIRENT.POBENT, " +
                      " dbo.CLIENTES.KLS_APELL_USUARIO, dbo.DIRENT.DTOENT, dbo.CLIENTES.KLS_NOM_USUARIO, dbo.DIRENT.IDDIRENT,ID_PS,dbo.DIRENT.DIRENT2, dbo.CLIENTES.NIFCLI,dbo.DIRENT.DEFECTO " + anexoDireccionFiscal +
                        " FROM dbo.CLIENTES INNER JOIN " +
                      // " dbo.DIRENT ON dbo.CLIENTES.CODCLI = dbo.DIRENT.CODCLI WHERE LTRIM(KLS_CODCLIENTE) ='";
                      " dbo.DIRENT ON dbo.CLIENTES.CODCLI = dbo.DIRENT.CODCLI WHERE LTRIM(clientes.CODCLI) ='";
            }
            else
            { 
            consultaAddresses= "SELECT  " +
                        " dbo.v_Cliente.ClienteID, dbo.v_Cliente.CompaniaID, dbo.v_Cliente.CodigoCliente, dbo.ps_Direccion.Nombre AS NombreCliente, " +
                        " dbo.ps_Direccion.DireccionEMail AS EMAIL, dbo.v_Cliente.TipoCliente,   " +
                        " dbo.ps_Direccion.DeudorID, dbo.v_Cliente.DireccionFiscalID, dbo.v_Cliente.RepresentanteID, dbo.v_Cliente.IdiomaID,   " +
                        " dbo.v_Cliente.IdentificacionComoProveedor, dbo.ps_Direccion.DireccionID, dbo.ps_Direccion.DeudorID AS IdDeudor, dbo.ps_Direccion.CodigoDireccion,   " +
                        "  dbo.ps_Direccion.TipoDireccion, dbo.ps_Direccion.NIF, dbo.ps_Direccion.Nombre, dbo.ps_Direccion.NombreViaPublica, dbo.ps_Direccion.NumeroCasa,   " +
                        "  dbo.ps_Direccion.CPostal, dbo.ps_Direccion.MunicipioID, dbo.ps_Direccion.NombreMunicipio, dbo.ps_GeoProvincia.CodigoProvincia, dbo.ps_Direccion.PaisID,   " +
                        "  dbo.ps_Direccion.CCCPais, dbo.ps_Direccion.CCCDigitoIBAN, dbo.ps_Direccion.CCCBancoID, dbo.ps_Direccion.CCCAgencia, dbo.ps_Direccion.CCCDigito,   " +
                        "  dbo.ps_Direccion.CCCCuenta, dbo.ps_Direccion.FormatoLibreCta, dbo.ps_Direccion.Telefono,  dbo.v_Cliente.CodigoCliente AS CODCLI,  " +
                        "  dbo.ps_Direccion.DireccionCompletaSelector, dbo.ps_Direccion.Baja,'VILARDELL123' AS PASSWORD,'NOMBRE' AS NOMBRE,'APELLIDOS' AS APELLIDO, dbo.ps_Direccion.Nombre AS RAZON  " +
                        " FROM            dbo.ps_Direccion INNER JOIN  " +
                        "  dbo.v_Cliente ON dbo.ps_Direccion.DireccionID = dbo.v_Cliente.DireccionFiscalID  " +
                        " INNER JOIN dbo.ps_GeoProvincia " +
                        " ON dbo.ps_Direccion.ProvinciaID = dbo.ps_GeoProvincia.ProvinciaID" + 
                        " WHERE dbo.v_Cliente.CompaniaID = 2 and  dbo.v_Cliente.CodigoCliente=";
            
            }


            if (objeto == "products")
            {
                csTallasYColoresV2 tyc2 = new csTallasYColoresV2();
                id_product = sqlConnect.actualizarIdA3(ItemsToUpdate, objeto);

                if ((csGlobal.nombreServidor.Contains("ANDREUTOYS")))
                {
                    actualizarInfoProduct(id_product);
                }

                if (TyC && !string.IsNullOrEmpty(id_product))
                {

                    //Edito para pruebas en hebo 25/01/2017
                    if (csGlobal.databaseA3 != "HEBO")
                    {
                        tyc2.generarTodasLasCombinacionesA3(id_product);
                    }

                    tyc2.inicializarTallasYColores(id_product);
                }
                //Función para crear los hijos como atributos
                if (csGlobal.modoTallasYColores == "A3NOPSSI")
                {

                    csSqlConnects sql = new csSqlConnects();

                    string resultado = sql.obtenerCampoTabla("SELECT * FROM ARTICULO WHERE KLS_ARTICULO_PADRE IN (SELECT CODART FROM ARTICULO WHERE KLS_ID_SHOP=" + id_product + ")");
                    if (!string.IsNullOrEmpty(resultado))
                    {
                        tyc2.sincronizarAtributosArticulos(id_product);
                    }
                }
            }

            else if (objeto == "customers")
            {
                csSqlConnects sql = new csSqlConnects();
                //string id = FilaToUpdate.ItemArray[0].ToString();
                if (!csGlobal.conexionDB.Contains("VILARDELL"))
                {
                    idCli = sqlConnect.actualizarIdA3(ItemsToUpdate, objeto);

                    //Actualizo Recargo Equivalencia Andreu Toys
                    if (csGlobal.nombreServidor.Contains("ANDREUTOYS"))
                    {
                        asignarRecargoEquivalenciaAndreuToys(idCli);
                    }

                    dt = sql.cargarDatosTablaA3(consultaAddresses + identidad.Trim() + "'");
                    if (csUtilidades.verificarDt(dt))
                    {
                        string dirent = dt.Rows[0]["DIRENT1"].ToString();
                        string idDirentPS = dt.Rows[0]["ID_PS"].ToString();
                        if (dirent != "" && (idDirentPS == "0" || string.IsNullOrEmpty(idDirentPS)))
                        {
                            //DOY DE ALTA LAS DIRECCIONES DE CLIENTE EN PRESTASHOP
                            // POR QUÉ AQUI SE ENVIA IDCLI Y ABAJO IDENTIDAD?????
                            psWebService.cdPSWebService("POST", "addresses", "", idCli, true, "", "F", dt.Rows[i], i);
                            esyncro.csSyncAddresses syncDirecciones = new esyncro.csSyncAddresses();
                            syncDirecciones.crearDireccionesA3ToPS(idCli);
                            //SE DESHABILITA PORQUE ItemsToUpdate COGE LOS VALORES DEL CLIENTE Y NO LOS DE LA DIRECCIÓN
                            //SE TRASLADA AL PUNTO DE DIRECCIONES
                            sqlConnect.actualizarIdA3(ItemsToUpdate, "addresses");
                        }
                    }
                }
                else
                {
                    // Vilardell section
                    ////////////////////

                    idCli = ItemsToUpdate.Rows[0][0].ToString();
                    string codcli = ItemsToUpdate.Rows[0][1].ToString();
                    csUtilidades.ejecutarConsulta("UPDATE V_CLIENTE SET IdentificacionComoProveedor='" + idCli + "' where CodigoCliente='" + codcli + "'", false);

                    //Actualización Recargo de Equivalencia
                    string valorRecargoEquivalencia = sql.obtenerCampoTabla("select TratamientoNRE FROM v_Cliente where CodigoCliente='" + codcli + "'");
                    string queryUpdate = "INSERT INTO ps_surchage_eq_customers (id_customer, active) VALUES(" + idCli + "," + valorRecargoEquivalencia + ") " +
                        " ON DUPLICATE KEY UPDATE " +
                           " active=" + valorRecargoEquivalencia + " where id_customer='" + idCli + "'";
                    csUtilidades.ejecutarConsulta(queryUpdate, true);

                    //fin actualización recargo Equivalencia

                    dt = sql.cargarDatosTablaA3(consultaAddresses + "'" + identidad + "'");
                    psWebService.cdPSWebService("POST", "addresses", "", idCli, true, "", "F", dt.Rows[0], i);
                    File.AppendAllText("customers.txt", idCli + " " + codcli + Environment.NewLine);

                }
            }

            //COMENTAR MEJOR LA OPERATIVA DE DIRECCIONES
            else if (objeto == "addresses")
            {
                if (!csGlobal.conexionDB.Contains("VILARDELL"))
                {

                    csSqlConnects sql = new csSqlConnects();

                    sqlConnect.actualizarIdA3(ItemsToUpdate, "addresses");
                    //string id = FilaToUpdate.ItemArray[0].ToString();
                    idCli = sqlConnect.actualizarIdA3(ItemsToUpdate, objeto);

                    dt = sql.cargarDatosTablaA3(consultaAddresses + identidad + "'");
                    if (csUtilidades.verificarDt(dt))
                    {
                        string dirent = dt.Rows[0]["DIRENT1"].ToString();
                        string idDirentPS = dt.Rows[0]["ID_PS"].ToString();
                        //if (dirent != "")
                        if (dt.Rows.Count > i && idDirentPS == "0")
                        {
                            psWebService.cdPSWebService("POST", "addresses", "", identidad, true, "", "E", dt.Rows[i], i);
                            sqlConnect.actualizarIdA3(ItemsToUpdate, "addresses");
                        }
                        else
                        {
                            //ACTUALIZO LOS ID DE LAS DIRECCIONES EN PRESTASHOP
                            sqlConnect.actualizarIdA3(ItemsToUpdate, "addresses");
                        }
                    }
                }
            }
            else
            {
                sqlConnect.actualizarIdA3(ItemsToUpdate, objeto);
            }
        }

        private void subirDirecciones(string idCli, int i)
        {
            DataTable dt = csUtilidades.GetDataSourceFromFile("customers.txt");
            DataTable direcciones = csUtilidades.dataRowArray2DataTable(dt.Select("Column1 = '" + idCli + "'"));

            if (csUtilidades.verificarDt(direcciones))
            {
                csPSWebService psWebService = new csPSWebService();
                psWebService.cdPSWebService("POST", "addresses", "", direcciones.Rows[0][0].ToString(), true, "", "E", direcciones.Rows[i], i);
            }
        }

        private void subirImagen()
        {
            csImagenesPS img = new csImagenesPS();
            img.subirImagenesSilentMode();
        }

        //private Boolean ValidarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        //{
        //    return true;
        //}

        ////para fmiro problemas certificado
        //private static bool ServerCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        //{
        //    if (sslPolicyErrors == SslPolicyErrors.None)
        //    {
        //        Console.WriteLine("Certificate OK");
        //        return true;
        //    }
        //    else
        //    {
        //        Console.WriteLine("Certificate ERROR");
        //        return false;
        //    }
        //}

        public XmlDocument readPSWebService(string metodo, string objeto)
        {
            //24/12/2020 Añadido para CadCanarias, se añade porque el certificado no era compatible con esync.
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                //string WebService_URL = csGlobal.APIUrl;
                //string WebService_LoginName = csGlobal.webServiceKey;
                //string WebService_Password = "";

                string RequestURL = csGlobal.APIUrl + objeto + "?ws_key=" + csGlobal.webServiceKey;
                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL);
                
                //HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(WebService_URL);
                //peticionWeb.Credentials = new NetworkCredential(WebService_LoginName, WebService_Password);


                peticionWeb.Method = metodo; //POST para crear un nuevo Registro PUT para actualizar
                peticionWeb.ContentType = "text/xml";

                //peticionWeb.ServerCertificateValidationCallback += ServerCertificateValidationCallback;

               
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidarCertificado);
                //System.Net.ServicePointManager.Expect100Continue = false;

                //10/04/2017 prueba para mejorar velocidad 
                //peticionWeb.Proxy = null;

                HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();

                if (peticionWeb.HaveResponse) // si hay respuesta
                {
                    //obtener contenido de la respuesta
                    using (Stream streamContenido = respuesta.GetResponseStream())
                    {
                        xmlDoc.LoadXml(new StreamReader(streamContenido).ReadToEnd());
                    }
                }
                respuesta.Close();

            }
            catch (WebException ps_e)
            {
                ////MessageBox.Show(ps_e.Message);
            }
            return xmlDoc;
        }

        public string Test(HttpWebRequest request)
        {
            string responseText = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    //...
                }
            }
            catch (WebException wex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("ERROR:" + wex.Message + ". STATUS: " + wex.Status.ToString());

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ((HttpWebResponse)wex.Response);
                    sb.AppendLine(string.Format("Status Code : {0}", response.StatusCode));
                    sb.AppendLine(string.Format("Status Description : {0}", response.StatusDescription));

                    try
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        sb.AppendLine(reader.ReadToEnd());
                    }
                    catch (WebException ex) { throw; }
                }

                throw new Exception(sb.ToString(), wex);
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return responseText;
        }

        public void exportarClientesPS(DataGridView dgv)
        {
            csPSWebService psWebService = new csPSWebService();

            try
            {
                List<string> clientesQueYaExisten = new List<string>();
                csMySqlConnect mysql = new csMySqlConnect();
                string clientes = "Los clientes siguientes ya existen en la tienda: \n\n";

                DataTable dt = csUtilidades.dgv2dtSelectedRows(dgv);

                try
                {
                    foreach (DataRow fila in dt.Rows)
                    {
                        string kls_codcliente = fila["KLS_CODCLIENTE"].ToString();
                        // Si el KLS_CODCLIENTE está vacío o si KLS_EMAIL_USUARIO contiene una arroba
                        if (kls_codcliente == "" && csUtilidades.emailIsValid(fila["KLS_EMAIL_USUARIO"].ToString()))// && fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString().Contains("@"))
                        {
                            string existeEmail = "select count(email) from ps_customer where email = '" + fila["KLS_EMAIL_USUARIO"].ToString() + "'";
                            string existeCodigoCliente = "select count(kls_a3erp_id) from ps_customer where kls_a3erp_id = '" + fila["CODCLI"].ToString().Trim() + "'";
                            if (!mysql.existeCampo(existeEmail) || !mysql.existeCampo(existeCodigoCliente))
                            {
                                psWebService.cdPSWebService("POST", "customers", "", fila["CODCLI"].ToString(), true, ""); //en minúsculas el objeto
                            }
                            else
                            {
                                clientesQueYaExisten.Add(fila["KLS_EMAIL_USUARIO"].ToString());
                            }
                        }
                    }

                    if (clientesQueYaExisten.Count > 0)
                    {
                        foreach (var item in clientesQueYaExisten)
                        {
                            clientes += ">> " + item + "\n";
                        }

                        MessageBox.Show(clientes);

                        if (MessageBox.Show("¿Sincronizar clientes?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            syncBetweenPSA3_by_Email();

                            MessageBox.Show("Clientes sincronizados con éxito");
                        }
                    }
                }
                catch (Exception ex) { }
            }
            catch (Exception ex) { }
        }

        private void syncBetweenPSA3_by_Email()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();

            bool check = false;
            string consulta = "SELECT * " +
                                " FROM information_schema.COLUMNS " +
                                " WHERE " +
                                " TABLE_SCHEMA = '" + csGlobal.databasePS + "' " +
                                " AND TABLE_NAME = 'ps_customer' " +
                                " AND COLUMN_NAME = 'kls_a3erp_id'";

            if (mysql.comprobarConsulta(consulta))
            {
                check = true;
            }

            if (check == true)
            {
                // bajamos los de ps a a3
                DataTable ps_final = mysql.cargarTabla("select id_customer as ID, email as EMAIL, ltrim(kls_a3erp_id) as CODCLI from ps_customer");
                DataTable a3_final = sql.cargarDatosTablaA3("SELECT KLS_CODCLIENTE as ID, KLS_EMAIL_USUARIO as EMAIL, LTRIM(CODCLI) as CODCLI FROM __CLIENTES");

                foreach (DataRow ps in ps_final.Rows)
                {
                    foreach (DataRow a3 in a3_final.Rows)
                    {
                        string email_ps = ps["EMAIL"].ToString();
                        string email_a3 = a3["EMAIL"].ToString();
                        string codcli_ps = ps["CODCLI"].ToString().Trim();
                        string id_ps = ps["ID"].ToString();

                        if (email_ps == email_a3)
                        {
                            sql.actualizarCampo("UPDATE __CLIENTES SET KLS_CODCLIENTE = '" + id_ps
                                + "' WHERE KLS_EMAIL_USUARIO = '" + email_ps + "' AND LTRIM(CODCLI) = '" + codcli_ps + "'");
                            break;
                        }
                    }
                }

                MessageBox.Show("Clientes de A3 sincronizados con la tienda");
            }
            else
            {
                MessageBox.Show("No tiene instalado el módulo KLS eSync Clientes");
            }
        }

        private void asignarRecargoEquivalenciaAndreuToys(string idCli)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                string idClienteRecEq = sql.obtenerCampoTabla("SELECT KLS_CODCLIENTE FROM __CLIENTES WHERE REGIVA='VNACR' AND KLS_CODCLIENTE=" + idCli);
                if (!string.IsNullOrEmpty(idClienteRecEq))
                {
                    csUtilidades.ejecutarConsulta("insert into ps_iRE_cliente values (" + idClienteRecEq + ")", true);
                    csUtilidades.ejecutarConsulta("insert into ps_customer_group (id_customer,id_group) values (" + idClienteRecEq + ",5)", true);
                }
            }
            catch { }

        }
        /// <summary>
        /// función para AndreuToys para actualizar los campos personalizados 
        /// </summary>
        /// <param name="idProduct"></param>
        public void actualizarInfoProduct(string idProduct)
        {
            try
            {
                string scriptUpdate = "";
                csSqlConnects sql = new csSqlConnects();
                DataTable dtProducts = new DataTable();
                dtProducts = sql.obtenerDatosSQLScript("SELECT LTRIM(CODART) AS CODART, DESCART, KLS_ID_SHOP, IF_UD_VTA_ALTO,IF_UD_VTA_ANCHO,IF_UD_VTA_LARGO, IF_PACKAG_ALTO, IF_PACKAG_ANCHO, IF_PACKAG_LARGO FROM ARTICULO WHERE KLS_ID_SHOP=" + idProduct);
                string udAlto = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_UD_VTA_ALTO"].ToString())? dtProducts.Rows[0]["IF_UD_VTA_ALTO"].ToString():"0";
                string udAncho = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_UD_VTA_ANCHO"].ToString()) ? dtProducts.Rows[0]["IF_UD_VTA_ANCHO"].ToString() : "0";
                string udLargo = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_UD_VTA_LARGO"].ToString()) ? dtProducts.Rows[0]["IF_UD_VTA_LARGO"].ToString() : "0";
                string packAlto = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_PACKAG_ALTO"].ToString()) ? dtProducts.Rows[0]["IF_PACKAG_ALTO"].ToString() : "0";
                string packAncho = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_PACKAG_ANCHO"].ToString()) ? dtProducts.Rows[0]["IF_PACKAG_ANCHO"].ToString() : "0";
                string packLargo = !string.IsNullOrEmpty(dtProducts.Rows[0]["IF_PACKAG_LARGO"].ToString()) ? dtProducts.Rows[0]["IF_PACKAG_LARGO"].ToString() : "0";

                udAlto = udAlto.Replace(",", ".");
                udAncho = udAncho.Replace(",", ".");
                udLargo = udLargo.Replace(",", ".");
                packAlto = packAlto.Replace(",", ".");
                packAncho = packAncho.Replace(",", ".");
                packLargo = packLargo.Replace(",", ".");

                scriptUpdate = "update ps_product set " +
                    " width= " + udAncho +
                    " , height=" + udAlto +
                    " , depth=" + udLargo +
                    " , kls_width_pack= " + packAncho  +
                    " , kls_height_pack=" + packAlto +
                    " , kls_depth_pack=" + packLargo +
                    " where id_product=" + idProduct;
                csUtilidades.ejecutarConsulta(scriptUpdate, true);
            }

            catch { }

        }
    }

}
