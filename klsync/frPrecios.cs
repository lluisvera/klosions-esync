﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Threading;
using System.Globalization;
using klsync.SQL;

namespace klsync
{
    public partial class frPrecios : Form
    {
        private string comboClienteFiltro = "";
        private static frPrecios m_FormDefInstance;
        public static frPrecios DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPrecios();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frPrecios()
        {
            InitializeComponent();

            PreciosAmigosMiro();
            ImpactoBastide();
        }

        private void ImpactoBastide()
        {
            if (csGlobal.conexionDB.Contains("BASTIDE"))
            {
                btnPreciosBastide.Visible = true;
            }
        }

        private void PreciosAmigosMiro()
        {
            if (csGlobal.conexionDB.Contains("MIRO"))
            {
                btnPreciosMiro.Visible = true;
            }
        }

        private void introducirDescuentosPorPartes(string parte, DataTable dt, bool descuentosfinales = false)
        {
            if (dt.Rows.Count > 0)
            {
                // HEBO 1.6.0.9 (no tiene el campo reduction_tax) - DISMAY 1.6.0.1.1 (tiene el campo reduction_tax)
                int versionInstalada = Int32.Parse(csGlobal.versionPS.Replace(".", ""));
                string campos = "(id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to" + ((versionInstalada == 16011) ? ",reduction_tax" : "") + ")";

                string articulo = "0";
                string id_shop = "0";
                string cliente = "0"; // variable (customer)
                string precio = "-1";
                string descuento = "";
                string desdeCantidad = "1";
                string tipoDescuento = "percentage";
                string famcli = "";
                string famart = "";
                DateTime fechaDesde = new DateTime();
                DateTime fechaHasta = new DateTime();
                string Lineas = "";
                string LineasLang = "";
                string LineasShop = "";
                int contador = 0;
                string lineas_descuentos = "";
                string campos_descuentos = "(id_specific_price_rule, id_product, id_shop, id_customer, price, from_quantity, reduction, " +
                                            " reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli, kls_famart, " +
                                            " id_cart, id_shop_group, id_currency, id_country, id_group, id_product_attribute" + ((versionInstalada == 16011) ? ",reduction_tax" : "") + ")";
                // Añadimos los campos de la última fila porque los pide al hacer el insert

                csMySqlConnect mysql = new csMySqlConnect();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    toolStripStatusLabelProgreso.Text = "Parte " + parte + ": " + i.ToString() + " registros";

                    precio = "-1";

                    if (!descuentosfinales)
                    {
                        articulo = dt.Rows[i]["KLS_ID_SHOP"].ToString().Trim();
                        cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString();
                    }

                    descuento = dt.Rows[i]["DESC1"].ToString();
                    //descuento = descuento.Replace(',', '.');
                    //en función del tipo de descuento indico si es de precio o descuento
                    //if (dgvPreciosA3.Rows[i].Cells[10].Value.ToString() == "DESCUENTO")
                    if (dt.Rows[i]["TIPO"].ToString() == "DESCUENTO")
                    {
                        tipoDescuento = "percentage";
                    }
                    else
                    {
                        tipoDescuento = "amount";
                        precio = dgvPreciosA3.SelectedRows[i].Cells[4].Value.ToString();
                        precio = precio.Replace(",", ".");
                        descuento = "0";
                    }
                    if (cliente == "")
                    {
                        cliente = "0";
                    }

                    fechaDesde = Convert.ToDateTime(dt.Rows[i]["FECMIN"].ToString());
                    fechaHasta = Convert.ToDateTime(dt.Rows[i]["FECMAX"].ToString());

                    if (contador > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasLang = LineasLang + ",";
                        LineasShop = LineasShop + ",";

                        if (descuentosfinales)
                        {
                            lineas_descuentos = lineas_descuentos + ",";
                        }
                    }

                    double dcost = Convert.ToDouble(descuento);
                    dcost = (dcost / 100);
                    descuento = dcost.ToString().Replace(",", ".");
                    Lineas = Lineas + "(" + articulo + "," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                        + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + "','"
                        + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "'" + ((versionInstalada == 16011) ? ",1" : "") + ")";


                    //(id_specific_price_rule, id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli, kls_famart)";
                    if (descuentosfinales)
                    {
                        famart = dt.Rows[i]["FAMART"].ToString().Trim();
                        famcli = dt.Rows[i]["FAMCLI"].ToString().Trim();
                        if (famcli == "")
                        {
                            famcli = "0";
                        }
                        cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString().Trim();

                        lineas_descuentos = lineas_descuentos + "(" + articulo + ",0," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                            + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + "','"
                            + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "', '" + famcli + "', '" + famart + "',0,0,0,0,0,0" + ((versionInstalada == 16011) ? ",1" : "") + ")";
                    }

                    if (contador > 500)
                    {
                        if (descuentosfinales)
                        {
                            mysql.InsertValoresEnTabla("ps_specific_price", campos_descuentos, lineas_descuentos, "");
                        }
                        else
                        {
                            mysql.InsertValoresEnTabla("ps_specific_price", campos, Lineas, "");
                        }

                        contador = -1;
                        lineas_descuentos = "";
                        Lineas = "";
                        LineasLang = "";
                        LineasShop = "";
                    }

                    contador++;
                }


                if (Lineas != "")
                {
                    if (descuentosfinales)
                    {
                        mysql.InsertValoresEnTabla("ps_specific_price", campos_descuentos, lineas_descuentos, "");
                    }
                    else
                    {
                        mysql.InsertValoresEnTabla("ps_specific_price", campos, Lineas, "");
                    }
                }

                toolStripStatusLabelProgreso.Text = "Actualización de descuentos finalizada";
            }
        }

        private void introducirDescuentosPorPartesV2(string parte, DataTable dt, bool descuentosfinales = false)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    List<string> valuesList = new List<string>();

                    // HEBO 1.6.0.9 (no tiene el campo reduction_tax) - DISMAY 1.6.0.1.1 (tiene el campo reduction_tax)
                    int versionInstalada = Int32.Parse(csGlobal.versionPS.Replace(".", ""));

                    string articulo = "0";
                    //string id_shop = "0";
                    string id_shop = csGlobal.conexionDB.ToUpper().Contains("DISMAY") ? "1" : "0";
                    string cliente = "0"; // variable (customer)
                    string precio = "-1";
                    string descuento = "";
                    string desdeCantidad = "1";
                    string tipoDescuento = "percentage";
                    string famcli = "";
                    string famart = "";
                    DateTime fechaDesde = new DateTime();
                    DateTime fechaHasta = new DateTime();
                    string Lineas = "";
                    string LineasLang = "";
                    string LineasShop = "";
                    int contador = 0;
                    string lineas_descuentos = "";

                    //string campos_descuentos = "(id_product, id_shop, id_customer, price, from_quantity, reduction, " +
                    //                            " reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli, kls_famart, " +
                    //                            " id_cart, id_shop_group, id_currency, id_country, id_group, id_product_attribute" + ((versionInstalada >= 1601) ? ",reduction_tax" : "") + ")";
                    string campos = "(id_specific_price_rule, id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to" + ((versionInstalada >= 1601) ? ",reduction_tax" : "") + ")";

                    string campos_descuentos = "(id_specific_price_rule, id_product, id_shop, id_customer, price, from_quantity, reduction, " +
                                               " reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli, kls_famart, " +
                                               " id_cart, id_shop_group, id_currency, id_country, id_group, id_product_attribute" + ((versionInstalada >= 1601) ? ",reduction_tax" : "") + ")";
                    // Añadimos los campos de la última fila porque los pide al hacer el insert

                    csMySqlConnect mysql = new csMySqlConnect();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        toolStripStatusLabelProgreso.Text = "Parte " + parte + ": " + i.ToString() + " registros";
                        precio = "-1";

                        if (!descuentosfinales)
                        {
                            articulo = dt.Rows[i]["KLS_ID_SHOP"].ToString().Trim();
                            cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString();
                        }

                        descuento = dt.Rows[i]["DESC1"].ToString();
                        if (dt.Rows[i]["TIPO"].ToString() == "DESCUENTO")
                        {
                            tipoDescuento = "percentage";
                        }
                        else
                        {
                            tipoDescuento = "amount";
                            precio = dgvPreciosA3.SelectedRows[i].Cells[4].Value.ToString();
                            precio = precio.Replace(",", ".");
                            descuento = "0";
                        }

                        cliente = string.IsNullOrEmpty(cliente) ? "0" : cliente;
                        if (cliente=="766")
                        {
                            bool a = true;
                        }

                        fechaDesde = Convert.ToDateTime(dt.Rows[i]["FECMIN"].ToString());
                        fechaHasta = Convert.ToDateTime(dt.Rows[i]["FECMAX"].ToString());

                        double dcost = Convert.ToDouble(descuento);
                        dcost = (dcost / 100);
                        descuento = dcost.ToString().Replace(",", ".");

                        if (descuentosfinales)
                        {
                            famart = dt.Rows[i]["FAMART"].ToString().Trim();
                            famcli = dt.Rows[i]["FAMCLI"].ToString().Trim();

                            if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
                            {
                                famcli = string.IsNullOrEmpty(famcli) ? "" : famcli;
                                famart = string.IsNullOrEmpty(famart) ? "" : famart;
                            }
                            else
                            {
                                famcli = string.IsNullOrEmpty(famcli) ? "0" : famcli;
                                famart = string.IsNullOrEmpty(famart) ? "0" : famart;
                            }

                            desdeCantidad = Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())) == 0 ? "1" : Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())).ToString();
                            desdeCantidad = desdeCantidad.Replace(",", ".");
                            cliente = dt.Rows[i]["KLS_CODCLIENTE"].ToString().Trim();

                            // añadimos esta condición por si es artículo familia de cliente
                            if (parte == "6" && !string.IsNullOrEmpty(dt.Rows[i]["KLS_ID_SHOP"].ToString()))
                            {
                                articulo = dt.Rows[i]["KLS_ID_SHOP"].ToString();
                            }
                            //lineas_descuentos = "(" + articulo + ",0," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                            //    + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00" + "','"
                            //    + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "', '" + famcli + "', '"
                            //    + famart + "',0,0,0,0,0,0" + ((versionInstalada == 16011) ? ",1" : "") + ")";
                            
                            lineas_descuentos = "(0," + articulo + "," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                                   + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00" + "','"
                                   + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "', '" + famcli + "', '"
                                   + famart + "',0,0,0,0,0,0" + ((versionInstalada >= 1601) ? ",1" : "") + ")";

                            valuesList.Add(lineas_descuentos);

                        }
                        else
                        {
                            Lineas = "(0," + articulo + "," + id_shop + "," + cliente + "," + precio + "," + desdeCantidad
                            + "," + descuento + ",'" + tipoDescuento + "','" + fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00" + "','"
                            + fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00" + "'" + ((versionInstalada >= 1601) ? ",1" : "") + ")";

                            valuesList.Add(Lineas);
                        }
                    }

                    csUtilidades.WriteListToDatabase("ps_specific_price", ((descuentosfinales) ? campos_descuentos : campos), valuesList);

                    toolStripStatusLabelProgreso.Text = "Actualización de descuentos finalizada";
                }
            }
            catch (Exception ex) { }
        }

        public void traspasarDescuentosPS()
        {
            Control.CheckForIllegalCrossThreadCalls = false;

            try
            {
                csSqlScripts sqlScript = new csSqlScripts();
                csSqlConnects sql = new csSqlConnects();

                btnUpdateDiscounts.Enabled = false;

                if (MessageBox.Show("¿Desea actualizar las familias de clientes y artículos?", "-Operación larga-", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    actualiarFamiliasArticulosEnPS();
                    actualizarFamiliasClientesPS();
                }
                //Borramos sólo los precios que no procedan de una regla introducida desde Prestashop
                ConexionSingleton singleton = ConexionSingleton.ObtenerInstancia();
                singleton.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='percentage'", true);
                //csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='percentage'", true);

                // ARTICULO - CLIENTE
                DataTable dt1 = sql.cargarDatosTablaA3(sqlScript.selectDescuento1ArtCli());
                introducirDescuentosPorPartesV2("1", dt1);

                // CLIENTE - FAMILIAS DE ARTÍCULOS - HEBO
                DataTable dt3_bis = sql.cargarDatosTablaA3(sqlScript.selectDescuento3_bis_FamArtCli());
                introducirDescuentosPorPartesV2("3", dt3_bis, true);

                // COMBINACIÓN DE FAMILIAS DE ARTÍCULOS SIN CLIENTES
                DataTable dt31_bis = sql.cargarDatosTablaA3(sqlScript.selectDescuento31_bis_FamArtCli());
                introducirDescuentosPorPartesV2("3", dt31_bis, true);

                // COMBINACIÓN DE FAMILIAS CLIENTES Y FAMILIAS DE ARTÍCULOS - DISMAY
                DataTable dt51 = sql.cargarDatosTablaA3(sqlScript.selectDescuento5_1_FamCliFamArt());
                introducirDescuentosPorPartesV2("5", dt51, true);

                // COMBINACION ARTICULO FAMILIA DE CLIENTES - DISMAY
                DataTable dt6 = sql.cargarDatosTablaA3(sqlScript.selectDescuentosFamCliArticuloCasuisticaDismay());
                introducirDescuentosPorPartesV2("6", dt6, true);

                // COMBINACION ARTICULO PARA TODOS LOS CLIENTES
                DataTable dt7 = sql.cargarDatosTablaA3(sqlScript.selectArticuloParaTodosLosClientes());
                introducirDescuentosPorPartesV2("7", dt7, false);

                toolStripStatusLabelProgreso.Text = "<< Actualización de descuentos finalizada >>";
                btnUpdateDiscounts.Enabled = true;
            }
            catch (Exception ex)
            {
                btnUpdateDiscounts.Enabled = true;
                csUtilidades.log(ex.Message);
            }
        }

        // Carga de Precios
        private void cargarPrecios(string tipoPrecio)
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter((tipoPrecio == "Descuentos") ? sqlScript.selectTarifaDescuentosA3() : sqlScript.selectTarifaPreciosEspecialesA3(comboClienteFiltro), dataConnection);

            DataTable t = new DataTable();
            try
            {
                a.Fill(t);
            }
            catch (Exception ex)
            {

            }
            dgvPreciosA3.DataSource = t;
        }

        private void cargarPreciosFichaArticulo()
        {
            try
            {
                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();

                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectPreciosFichaArticulo(), dataConnection);

                DataTable t = new DataTable();
                a.Fill(t);
                dgvPreciosA3.DataSource = t;
                toolStripStatusLabel1.Text = "Total Filas: " + dgvPreciosA3.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarPreciosEspecialesArticuloCliente()
        {
            csSqlConnects sql = new csSqlConnects();

            string consulta = "SELECT dbo.DESCUENT.CODART, dbo.DESCUENT.FAMCLI, dbo.DESCUENT.DESC1, dbo.DESCUENT.CODCLI FROM         dbo.ARTICULO INNER JOIN dbo.DESCUENT ON dbo.ARTICULO.CODART = dbo.DESCUENT.CODART";
        }

        private void cargarDescuentosClienteFamiliaArticulo()
        {
            string consulta = "SELECT  dbo.DESCUENT.CODCLI, dbo.ARTICULO.CODART, dbo.DESCUENT.DESC1, dbo.ARTICULO.FAMARTDESCVEN, dbo.DESCUENT.FAMART FROM         dbo.ARTICULO INNER JOINdbo.DESCUENT ON dbo.ARTICULO.FAMARTDESCVEN = dbo.DESCUENT.FAMART ORDER BY dbo.DESCUENT.CODCLI";
        }

        // Actualizar precios

        // Precio ficha articulo


        private void traspasarAPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            traspasarDescuentosPS();
        }

        private void cargarDescuentos()
        {
            cargarPrecios("Descuentos");
            tssRows.Text = "Total filas: " + dgvPreciosA3.Rows.Count.ToString();

        }

        private void cargarPreciosEspeciales()
        {
            cargarPrecios("");
            tssRows.Text = "Total filas: " + dgvPreciosA3.Rows.Count.ToString();

        }

        private void traspasarPreciosEspecialesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(false);
        }

        private void traspasarPreciosConReducciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(true);
        }

        private void traspasarPreciosEspeciales(bool conReduccion)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            string idshop, codart, codcli, precio, fromQuantity, reduction, reduction_type = "";
            string qryFields, qryValues = "";

            idshop = "1";
            fromQuantity = "0";

            // borramos la tabla antes
            mysql.borrar("ps_specific_price");

            // insertamos todos los precios
            foreach (DataGridViewRow fila in dgvPreciosA3.Rows)
            {
                codart = fila.Cells["KLS_ID_SHOP"].Value.ToString();
                codcli = fila.Cells["KLS_CODCLIENTE"].Value.ToString();
                if (codcli == "")
                {
                    codcli = "0";
                }
                precio = fila.Cells["PRICE"].Value.ToString();
                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                {
                    precio = Math.Round(Convert.ToDouble(precio) / 1.21, 3).ToString();
                }
                precio = precio.Replace(",", ".");
                //reduction = "0.000000";
                reduction = "0";
                reduction_type = "amount";

                if (conReduccion)
                {
                    idshop = "0";
                    precio = "-1";
                    fromQuantity = "1";
                    reduction = fila.Cells["IMPORTEDTO"].Value.ToString();
                    reduction = Math.Round(Convert.ToDouble(reduction), 3).ToString();
                    reduction = reduction.Replace(".", "");
                    reduction = reduction.Replace(",", ".");
                }

                qryFields = " (id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type) ";
                qryValues = " (" + codart + "," + idshop + "," + codcli + "," + precio + "," + fromQuantity + "," + reduction + ",'" + reduction_type + "')";

                mysql.InsertValoresEnTabla("ps_specific_price", qryFields, qryValues, "", false);
            }

            toolStripStatusLabelProgreso.Text = "Actualización de precios especiales finalizada";
        }




        private void traspasarPreciosEspecialesV2(bool conReduccion)
        {
            int errorfila = 0;
            int error = 0;
            string qryFields, qryValues = "";

            //DialogResult result = MessageBox.Show("¿Quieres guardar los valores anteriores y nuevos?","Guardar Detalles",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //bool guardarDetalles = (result == DialogResult.Yes);
            //bool guardarDetalles = true;

            try
            {
                csUtilidades.guardarProgresoFichero("CONEXION: " + csGlobal.conexionDB);
                csUtilidades.guardarProgresoFichero("Inicio de la actualización de precios especiales");

                csSqlScripts scripts = new csSqlScripts();
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlConnects sql = new csSqlConnects();

                DataTable dt = csUtilidades.fillDataTableFromDataGridView(dgvPreciosA3, false);

                int contador = -1;
                string idshop = csGlobal.conexionDB.ToUpper().Contains("DISMAY") ? "1" : "0";
                string kls_famcli = "0";
                string kls_famart = "0";
                string fromQuantity = "0";
                string codart, codcli, precio, reduction, reduction_type, campo_prc = "";
                string type_prc = "' ),";

                List<string> codarts_borrar_miro = new List<string>();

                if (csGlobal.gestionPreciosHijos)
                    campo_prc = ", type_prc";




                if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
                {
                    qryFields = " (id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, `ps_specific_price`.`from`, `ps_specific_price`.`to`, kls_famcli, kls_famart" + campo_prc + ") ";
                }
                else
                {
                    qryFields = " (id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, `ps_specific_price`.`from`, `ps_specific_price`.`to`, kls_famcli" + campo_prc + ") ";
                }
                csUtilidades.guardarProgresoFichero("Borrando datos antiguos...");
                if (comboClienteFiltro != "")
                {
                    int id_customer = Convert.ToInt32(csGlobal.comboFormValue);
                    csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_customer = " + id_customer, true);
                }
                else if (csGlobal.gestionPreciosHijos)
                {
                    csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='amount' and type_prc='esp' ", true);
                }
                else
                {
                    if (!csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
                    {
                        error = 1;
                        csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='amount'", true);
                    }
                    else
                    {
                        csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type = 'amount'", true);
                    }
                }

                csUtilidades.guardarProgresoFichero("Datos antiguos eliminados");

                csUtilidades.guardarProgresoFichero("Iniciando inserción de nuevos precios...");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    errorfila = i;
                    if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
                    {
                        kls_famcli = " ";
                        kls_famart = " ";
                    }
                    codart = dt.Rows[i]["KLS_ID_SHOP"].ToString();
                    codcli = dt.Rows[i]["KLS_CODCLIENTE"].ToString();

                    DateTime fechaDesde = Convert.ToDateTime(dt.Rows[i]["FECMIN"].ToString());
                    DateTime fechaHasta = Convert.ToDateTime(dt.Rows[i]["FECMAX"].ToString());

                    string fd = fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00";
                    string fh = fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00";

                    if (codcli == "")
                    {
                        codcli = "0";
                    }

                    precio = dt.Rows[i]["PRICE"].ToString();
                    if (csGlobal.ivaIncluido.ToUpper() == "SI")
                    {
                        double poriva = (Convert.ToDouble(dt.Rows[i]["PORIVA"].ToString()) / 100) + 1;
                        double precio_double = Convert.ToDouble(precio);
                        precio = Math.Round(precio_double / poriva, 3).ToString();
                    }

                    precio = precio.Replace(",", ".");
                    reduction = "0";
                    reduction_type = "amount";

                    if (conReduccion)
                    {
                        precio = "-1";
                        idshop = "0";
                        fromQuantity = "1";
                        reduction = dt.Rows[i]["IMPORTEDTO"].ToString();
                        reduction = Math.Round(Convert.ToDouble(reduction), 3).ToString();
                        reduction = reduction.Replace(".", "");
                        reduction = reduction.Replace(",", ".");
                    }

                    if (dt.Columns.Contains("UNIDADES"))
                    {
                        fromQuantity = Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())) == 0 ? "1" : Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())).ToString();
                    }

                    if (dt.Rows[i]["FAMCLI"].ToString().Trim() != "")
                    {
                        kls_famcli = dt.Rows[i]["FAMCLI"].ToString().Trim();
                    }

                    if (csGlobal.gestionPreciosHijos)
                        type_prc = "','esp'),";

                    //if (guardarDetalles)
                    //{
                    //    // Si se ha marcado el checkbox, obtenemos y registramos los valores antiguos
                    //    DataTable valoresAnteriores = mysql.cargarTabla("SELECT * FROM ps_specific_price WHERE id_product = " + codart + " AND id_customer = " + codcli);
                    //    if (valoresAnteriores.Rows.Count > 0)
                    //    {
                    //        string valoresAntiguos = "Valores anteriores para producto " + codart + ", cliente " + codcli + ": " +
                    //                                  "Precio: " + valoresAnteriores.Rows[0]["price"].ToString() + ", " +
                    //                                  "Reducción: " + valoresAnteriores.Rows[0]["reduction"].ToString() + ", " +
                    //                                  "Desde: " + valoresAnteriores.Rows[0]["from"].ToString() + ", " +
                    //                                  "Hasta: " + valoresAnteriores.Rows[0]["to"].ToString();

                    //        csUtilidades.guardarProgresoFichero(valoresAntiguos);
                    //    }

                    //    // Guardar los valores nuevos
                    //    string valoresNuevos = "Valores nuevos para producto " + codart + ", cliente " + codcli + ": " +
                    //                            "Precio: " + precio + ", " +
                    //                            "Reducción: " + reduction + ", " +
                    //                            "Desde: " + fd + ", " +
                    //                            "Hasta: " + fh;

                    //    csUtilidades.guardarProgresoFichero(valoresNuevos);
                    //}

                    // Actualizar query values






                    if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
                    {
                        qryValues += " (" + codart + "," + idshop + "," + codcli + "," + precio + "," + fromQuantity + "," + reduction + ",'" + reduction_type + "', '" + fd + "','" + fh + "','" + kls_famcli + "','" + kls_famart + type_prc;
                    }
                    else
                    {
                        qryValues += " (" + codart + "," + idshop + "," + codcli + "," + precio + "," + fromQuantity + "," + reduction + ",'" + reduction_type + "', '" + fd + "','" + fh + "','" + kls_famcli + type_prc;
                    }
                        
                        contador++;

                    // Cada 500 registros, hacer un insert
                    if (contador > 500)
                    {
                        csUtilidades.guardarProgresoFichero("Ejecutando batch de 500 inserciones...");
                        mysql.InsertValoresEnTabla("ps_specific_price", qryFields, qryValues, "", false);
                        csUtilidades.guardarProgresoFichero("Batch ejecutado");
                        contador = -1;
                        qryValues = "";
                    }
                }

                // Insertar los registros restantes
                if (qryValues != "")
                {
                    csUtilidades.guardarProgresoFichero("Ejecutando inserción final...");
                    mysql.InsertValoresEnTabla("ps_specific_price", qryFields, qryValues, "", false);
                    csUtilidades.guardarProgresoFichero("Inserción final completada");
                }

                csUtilidades.guardarProgresoFichero("Actualización de precios especiales finalizada con éxito");

            }
            catch (Exception ex)
            {
                csUtilidades.guardarProgresoFichero("Error en fila: " + errorfila.ToString() + " - " + ex.Message);
                MessageBox.Show("Error en la actualización: " + ex.Message);
            }
        }




        //SE COMENTA PARA INCLUIRLO CON CONTROLES
        //private void traspasarPreciosEspecialesV2(bool conReduccion)
        //{
        //    int errorfila = 0;
        //    int error = 0;
        //    string qryFields, qryValues = "";

        //    try
        //    {
        //        csSqlScripts scripts = new csSqlScripts();
        //        csMySqlConnect mysql = new csMySqlConnect();
        //        csSqlConnects sql = new csSqlConnects();
        //        //DataTable dt = sql.cargarDatosTablaA3(scripts.selectTarifaPreciosEspecialesA3());

        //        DataTable dt = csUtilidades.fillDataTableFromDataGridView(dgvPreciosA3, false);

        //        int contador = -1;
        //        string idshop, codart, codcli, precio, fromQuantity, reduction, reduction_type = "";

        //        string campo_prc = "";
        //        string type_prc = "' ),";



        //        if (csGlobal.gestionPreciosHijos)
        //            campo_prc = ", type_prc";

        //        //qryFields = " (id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli" + campo_prc + ") ";
        //        qryFields = " (id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, `ps_specific_price`.`from`, `ps_specific_price`.`to`, kls_famcli" + campo_prc + ") ";

        //        string fdesde = "", fhasta = "";
        //        List<string> codarts_borrar_miro = new List<string>();
        //        idshop = "1";
        //        fromQuantity = "0";

        //        // borramos la tabla antes
        //        // si ha filtrado por cliente, borramos solo la de ese cliente
        //        if (comboClienteFiltro != "")
        //        {
        //            int id_customer = Convert.ToInt32(csGlobal.comboFormValue);
        //            csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_customer = " + id_customer, true);
        //        }
        //        else if (csGlobal.gestionPreciosHijos)
        //        {
        //            csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='amount' and type_prc='esp' ", true);
        //        }
        //        else
        //        {
        //            if (!csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
        //            {
        //                // mysql.borrar("ps_specific_price");
        //                error = 1;
        //                csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 and reduction_type='amount'", true);
        //            }
        //            else
        //            {
        //                csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_specific_price_rule = 0 reduction_type = 'amount'", true);
        //            }
        //        }

        //        error = 2;
        //        // insertamos todos los precios
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            errorfila = i;
        //            string kls_famcli = "0";
        //            codart = dt.Rows[i]["KLS_ID_SHOP"].ToString();
        //            codcli = dt.Rows[i]["KLS_CODCLIENTE"].ToString();

        //            DateTime fechaDesde = Convert.ToDateTime(dt.Rows[i]["FECMIN"].ToString());
        //            DateTime fechaHasta = Convert.ToDateTime(dt.Rows[i]["FECMAX"].ToString());

        //            int desde_comparar = fechaDesde.CompareTo(DateTime.Now);
        //            int hasta_comparar = fechaHasta.CompareTo(DateTime.Now);

        //            // Filtro para precios especiales de miro
        //            if (csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
        //            {
        //                if (desde_comparar < 0 && hasta_comparar > 0)
        //                {
        //                    codarts_borrar_miro.Add(codart);
        //                }
        //            }

        //            string fd = fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00";
        //            string fh = fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00";

        //            if (codcli == "")
        //            {
        //                codcli = "0";
        //            }

        //            precio = dt.Rows[i]["PRICE"].ToString();
        //            if (csGlobal.ivaIncluido.ToUpper() == "SI")
        //            {
        //                if (csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
        //                {
        //                    //double poriva = Convert.ToDouble(dt.Rows[i]["PORIVA"].ToString());
        //                    double poriva = (Convert.ToDouble(dt.Rows[i]["PORIVA"].ToString()) / 100) + 1;
        //                    double precio_double = Convert.ToDouble(precio);
        //                    precio = Math.Round(precio_double / poriva, 3).ToString();
        //                }
        //            }

        //            precio = precio.Replace(",", ".");
        //            reduction = "0";
        //            reduction_type = "amount";

        //            if (conReduccion)
        //            {
        //                precio = "-1";
        //                idshop = "0";
        //                fromQuantity = "1";
        //                reduction = dt.Rows[i]["IMPORTEDTO"].ToString();
        //                reduction = Math.Round(Convert.ToDouble(reduction), 3).ToString();
        //                reduction = reduction.Replace(".", "");
        //                reduction = reduction.Replace(",", ".");
        //            }

        //            if (dt.Columns.Contains("unidades"))
        //            {
        //                //fromQuantity = float.Parse(dt.Rows[i]["unidades"].ToString()).ToString();
        //                fromQuantity = Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())) == 0 ? "1" : Math.Truncate(Convert.ToDecimal(dt.Rows[i]["UNIDADES"].ToString())).ToString();
        //            }

        //            if (dt.Rows[i]["FAMCLI"].ToString().Trim() != (""))
        //            {
        //                kls_famcli = (dt.Rows[i]["FAMCLI"].ToString().Trim());
        //            }
        //            if (csGlobal.gestionPreciosHijos)
        //                type_prc = "','esp'),";

        //            //Cargamos todos los precios que no tengan codigopadre(es decir los que no tienen hijos).

        //            if (dt.Rows[i]["CODIGOPADRE"].ToString().Equals(""))
        //            {

        //                qryValues += " (" + codart + "," + idshop + "," + codcli + "," + precio + "," + fromQuantity + "," + reduction + ",'" + reduction_type + "', '" + fd + "','" + fh + "','" + kls_famcli + type_prc;

        //                contador++;
        //            }





        //            if (contador > 500)
        //            {


        //                mysql.InsertValoresEnTabla("ps_specific_price", qryFields, qryValues, "", false);

        //                contador = -1;
        //                qryValues = "";
        //            }


        //        }
        //        error = 3;
        //        if (qryValues != "")
        //        {

        //            mysql.InsertValoresEnTabla("ps_specific_price", qryFields, qryValues, "", false);
        //            error = 4;

        //        }
        //        if (csGlobal.gestionPreciosHijos)
        //        {
        //            atualizarPreciosHijos(dt, "esp");
        //        }

        //        if (csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
        //        {
        //            foreach (string item in codarts_borrar_miro)
        //            {
        //                string consulta = string.Format("delete from ps_specific_price where id_product = {0} and reduction_type = 'percentage'", item);
        //                csUtilidades.ejecutarConsulta(consulta, true);
        //            }
        //        }
        //        error = 5;
        //        toolStripStatusLabelProgreso.Text = "Actualización de precios especiales finalizada";
        //}
        //catch (Exception ex)
        //{
        //        MessageBox.Show(ex.Message);
        //        //MessageBox.Show("numero de fila: " + errorfila.ToString() + "\n" +
        //        //                                           "query: " + qryValues);
        //        if (csGlobal.modeDebug) csUtilidades.log("Error: " + error.ToString());
        //        if (csGlobal.modeDebug) csUtilidades.log("numero de fila: " + errorfila.ToString() + "\n" +
        //                                                   "query: " + qryValues);

        //    }
        //}



        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            comboClienteFiltro = "";
            cargarPreciosEspeciales();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            cargarDescuentos();
        }

        private void frPrecios_Load(object sender, EventArgs e)
        {
            cargarTarifas();
            dgvPreciosA3.DataSource = null;
        }

        private void cargarTarifas()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT TARIFA, DESCTARIFA FROM dbo.TARIFAS ORDER BY TARIFA", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                comboRates.ValueMember = "TARIFA";
                comboRates.DisplayMember = "DESCTARIFA";
                comboRates.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarTarifaPreciosArticulos()
        {
            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();

            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectPreciosArticulos(comboRates.Text), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvPreciosA3.DataSource = t;
            toolStripStatusLabel1.Text = "Total Filas: " + dgvPreciosA3.Rows.Count.ToString();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            tarifas = false;
            cargarPreciosFichaArticulo();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            tarifas = true;
            cargarTarifaPreciosArticulos();
        }

        private bool tarifas = false;



        /// <summary>
        /// Función para actualizar los precios de los artículos de Prestashop, pasando como parámetro si es de tarifa o no
        /// </summary>
        /// <param name="tarifa"></param>
        private void actualizarPrecioFichaArticulo(bool tarifa = false)
        {
            try
            {
                bool preciosFicha = true;
                bool preciosAtributos = false;

                DataTable dt = new DataTable();
                dt.Columns.Add("REFERENCIA");
                dt.Columns.Add("PRECIO");

                if (dgvPreciosA3.Rows.Count > 0)
                {
                    DialogResult result = MessageBox.Show("¡Se van a actualizar " + dgvPreciosA3.SelectedRows.Count.ToString() + " Precio/s! ¿Está seguro?", "Actualización de Precios", MessageBoxButtons.YesNoCancel);

                    if (result == DialogResult.Yes)
                    {
                        string precio = "";
                        string id_product = "";
                        string taxGroup = "";
                        string reference = "";

                        csMySqlConnect mySqlConnect = new csMySqlConnect();

                        foreach (DataGridViewRow fila in dgvPreciosA3.SelectedRows)
                        {
                            DataRow filaPrecio = dt.NewRow();

                            try
                            {
                                reference = fila.Cells["Articulo"].Value.ToString().Trim();
                            }
                            catch (Exception ex) {
                                reference = fila.Cells["CODART"].Value.ToString().Trim();
                            }
                            if (!tarifa)
                            {
                                preciosFicha = fila.Cells["ORIGEN"].Value.ToString() == "PRECIOSFICHA" ? true : false;
                                preciosAtributos = fila.Cells["ORIGEN"].Value.ToString() == "PRECIOSATRIBUTOS" ? true : false;
                            }

                            // En función de si es tarifa o es artículo de ficha tendrá un tratamiento diferente
                            if (preciosFicha || tarifa)
                            {
                                //3-1-18 descomento para Thagson
                                //Trabajan con IVA incluido
                                if (tarifas && csGlobal.ivaIncluido.ToUpper() == "SI")
                                {
                                    precio = fila.Cells["BASE"].Value.ToString();
                                }
                                else if (!tarifas && csGlobal.ivaIncluido.ToUpper() == "SI")
                                {
                                    precio = fila.Cells["BASE"].Value.ToString();
                                }
                                else // entrará cuando ivaIncluido sea NO
                                {
                                    precio = fila.Cells["PRECIO"].Value.ToString();
                                }

                                // MIRO
                                if (csGlobal.conexionDB.ToUpper().Contains("FMIRO"))
                                {
                                    double poriva = (Convert.ToDouble(fila.Cells["PORIVA"].Value.ToString()) / 100) + 1;
                                    //precio = fila.Cells["PRECIO"].Value.ToString();
                                    double final = Convert.ToDouble(fila.Cells["PRECIO"].Value.ToString()) / poriva;
                                    precio = Math.Round(final, 3).ToString();
                                }

                                precio = precio.Replace(",", ".");
                                id_product = fila.Cells["KLS_ID_SHOP"].Value.ToString();
                                taxGroup = fila.Cells["TAXGROUP"].Value.ToString();
                                if (csGlobal.conexionDB.ToUpper().Contains("ROSSO"))
                                {
                                    csSqlConnects sql = new csSqlConnects();
                                    csMySqlConnect mysql = new csMySqlConnect();
                                    csTallasYColoresV2 tyc = new csTallasYColoresV2();
                                    string precios_tallas = "SELECT __TARIFAVETALLA.CODART, kls_id_shop, __TARIFAVETALLA.CODFAMTALLAH, __TARIFAVETALLA.CODFAMTALLAV, __TARIFAVETALLA.CODTALLAH, __TARIFAVETALLA.CODTALLAV, __TARIFAVETALLA.ID, __TARIFAVETALLA.IDTARIFAV, __TARIFAVETALLA.PRECIO,TALLAS_1.ID AS TALLAV,TALLAS.ID AS TALLAH, PORIVA " +
                                        "FROM __TARIFAVETALLA " +
                                        "    left join articulo on articulo.codart = __tarifavetalla.codart " +
                                        "INNER JOIN TALLAS ON __TARIFAVETALLA.CODFAMTALLAH = TALLAS.CODFAMTALLA " +
                                        "AND __TARIFAVETALLA.CODTALLAH = TALLAS.CODTALLA " +
                                        "INNER JOIN TALLAS TALLAS_1 ON __TARIFAVETALLA.CODFAMTALLAV = TALLAS_1.CODFAMTALLA " +
                                        "AND __TARIFAVETALLA.CODTALLAV = TALLAS_1.CODTALLA " +
                                        " left join TIPOIVA ON dbo.ARTICULO.TIPIVA = TIPOIVA.TIPIVA "+
                                        "WHERE LTRIM(__TARIFAVETALLA.CODART)='"+ reference+"'";

                                    // 1. Meter precios_tallas en un dt
                                    DataTable precios_tallas_dt = sql.cargarDatosTablaA3(precios_tallas);
                                    DataTable ps_product_attribute_dt = mysql.cargarTabla("select * from ps_product_attribute where id_product="+ id_product);
                                    if (csUtilidades.verificarDt(ps_product_attribute_dt))
                                    {
                                        foreach (DataRow ps in ps_product_attribute_dt.Rows)
                                        {


                                            string id_product_attribute = ps["id_product_attribute"].ToString();
                                            DataTable ps_product_attribute_combination = mysql.cargarTabla("select * from ps_product_attribute_combination where id_product_attribute="+id_product_attribute);
                                            int num_rows = ps_product_attribute_combination.Rows.Count;
                                            DataTable final_t = csUtilidades.dataRowArray2DataTable(ps_product_attribute_combination.Select("id_product_attribute = " + ps["id_product_attribute"].ToString()));
                                            if (csUtilidades.verificarDt(final_t) && num_rows>=2)
                                            {
                                                string v1 = final_t.Rows[0]["id_attribute"].ToString();
                                                string v2 = final_t.Rows[1]["id_attribute"].ToString();
                                                float precio_fin;
                                                DataTable precio_talla = csUtilidades.dataRowArray2DataTable(precios_tallas_dt.Select("((tallav = '" + v1 + "' AND tallah = '" + v2 + "') OR (tallav = '" + v2 + "' AND tallah = '" + v1 + "')) AND kls_id_shop = '" + id_product + "'"));

                                                if (csUtilidades.verificarDt(precio_talla))
                                                {
                                                    //string reference = precio_talla.Rows[0]["codart"].ToString().Trim();
                                                    float iva = (float.Parse(precio_talla.Rows[0]["poriva"].ToString()) / 100) + 1;
                                                    string precio1 = float.Parse(precio_talla.Rows[0]["precio"].ToString()).ToString();
                                                    precio_fin =  float.Parse(precio_talla.Rows[0]["precio"].ToString())/iva;

                                                    
                                                }
                                                else
                                                {
                                                    //float iva = float.Parse(21 / 100) + 1.0;
                                                    precio_fin = float.Parse(fila.Cells["PRECIO"].Value.ToString()) / float.Parse("1,21");
                                                }
                                                csUtilidades.ejecutarConsulta("update ps_product_attribute set price = " + precio_fin.ToString().Replace(",", ".") + " where id_product_attribute = " + id_product_attribute, true);
                                                csUtilidades.ejecutarConsulta("update ps_product_attribute_shop set price = " + precio_fin.ToString().Replace(",", ".") + " where id_product_attribute = " + id_product_attribute, true);

                                                csUtilidades.ejecutarConsulta("update ps_product set price = 0 where id_product = " + id_product, true);
                                                csUtilidades.ejecutarConsulta("update ps_product_shop set price = 0 where id_product = " + id_product, true);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        mySqlConnect.actualizarPreciosImpuestosPS(id_product, precio, taxGroup);
                                    }
                                }

                                else if (id_product != "")

                                    mySqlConnect.actualizarPreciosImpuestosPS(id_product, precio, taxGroup);

                                else
                                {

                                    filaPrecio["REFERENCIA"] = reference;
                                    filaPrecio["PRECIO"] = precio;
                                    dt.Rows.Add(filaPrecio);
                                }

                            }
                            //hay que añadir un else en el que entre en caso ya que si no has seleccionado Cargar precios Ficha siempre va a dar un error de que no in la columna de "ORIGEN"
                            else if (preciosAtributos)
                            {
                                reference = fila.Cells["CODART"].Value.ToString().Trim();
                                precio = fila.Cells["PRECIO"].Value.ToString();
                                precio = precio.Replace(",", ".");
                                csUtilidades.ejecutarConsulta("update ps_product_attribute set price = " + precio + " where reference='" + reference + "'", true);
                            }

                        }

                        //Actualizamos precios de los atributos
                        if (csGlobal.precioAtributos && dt.Rows.Count > 0)
                        {

                            mySqlConnect.actualizarPreciosImpuestosAtributos(dt);
                        }

                        toolStripStatusLabelProgreso.Text = "<< Actualización de los precios finalizada >>";
                    }
                }
                else
                {
                    MessageBox.Show("CARGAR PRECIOS EN PANTALLA Y SELECCIONAR LINEAS PARA ACTUALIZAR");
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void actualizarPrecioFichaArticuloV2()
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                csSqlScripts scripts = new csSqlScripts();
                string precio = "", id_product = "", taxGroup = "";

                DataTable dt = sql.cargarDatosTablaA3(scripts.selectPreciosFichaArticulo());

                csMySqlConnect mySqlConnect = new csMySqlConnect();
                foreach (DataGridViewRow fila in dgvPreciosA3.SelectedRows)
                {
                    if (csGlobal.ivaIncluido.ToUpper() == "SI")
                    {
                        precio = fila.Cells["BASE"].Value.ToString();
                    }
                    else
                    {
                        precio = fila.Cells["PRECIO"].Value.ToString();
                    }
                    precio = precio.Replace(",", ".");
                    id_product = fila.Cells["KLS_ID_SHOP"].Value.ToString();
                    taxGroup = fila.Cells["TAXGROUP"].Value.ToString();

                    mySqlConnect.actualizarPreciosImpuestosPS(id_product, precio, taxGroup);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
            tssInfoPrecios.Text = "Actualizados " + dgvPreciosA3.SelectedRows.Count.ToString() + " precios";
        }

        private void btTraspasarPreciosReduccion_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(true);
        }

        private void btUpdatePreciosEspeciales_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspeciales(false);
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            actualizarPrecioFichaArticulo(false);
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            actualizarPrecioFichaArticulo(false);
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            preciosDescuentos();
        }

        public void preciosDescuentos()
        {
            Thread t = new Thread(traspasarDescuentosPS);
            t.Start();
        }

        private void toolsCargarPreciosEspecialesArticuloCliente_Click(object sender, EventArgs e)
        {
            cargarPreciosEspecialesArticuloCliente();
        }

        private void toolsCargarDescuentosClienteFamiliaArticulo_Click(object sender, EventArgs e)
        {
            cargarDescuentosClienteFamiliaArticulo();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(false);
        }

        private void filtrarPorClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            DataTable dt = sql.cargarDatosTablaA3("select distinct ltrim(codcli) as CODCLI from __clientes");
            frDialogCombo combo = new frDialogCombo(dt, "CODCLI", "Selecciona el cliente para filtrar", true);
            if (combo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                comboClienteFiltro = " AND LTRIM(__CLIENTES.CODCLI) = '" + csGlobal.comboFormValue + "'";
                cargarPreciosEspeciales();
            }
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            actualiarFamiliasArticulosEnPS();
            actualizarFamiliasClientesPS();
        }

        public void actualiarFamiliasArticulosEnPS()
        {
            try
            {
                string filtroArticulos = "";
                int filasTabla = 0;
                int contador = 0;
                if (csGlobal.modeDebug) csUtilidades.log("Inicio Actualización de Familias de Artículos");

                string productPS = "";
                string familiaA3 = "";
                string scriptUpdate = "";
                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                //ConexionSingleton testSingleton = new ConexionSingleton();

                if (csGlobal.modeDebug) csUtilidades.log("Inicio familias de artículos en A3ERP");
                DataTable dtA3 = sql.cargarDatosTablaA3("SELECT KLS_ID_SHOP as id_product,ltrim(codart) as reference, LTRIM(FAMARTDESCVEN) as fam FROM dbo.ARTICULO WHERE (KLS_ID_SHOP > 0) AND (FAMARTDESCVEN IS NOT NULL)");
                if (csGlobal.modeDebug) csUtilidades.log("Fin familias de artículos en A3ERP, inicio carga familia productos en PS");

                DataTable dtPS = mySqlConnect.cargarTabla("select id_product, reference, kls_a3erp_fam_id as fam from ps_product");
                if (csGlobal.modeDebug) csUtilidades.log("Fin carga de Familias de Artículos");

                DataTable final = csUtilidades.getDiffDataTables(dtA3, dtPS);

                if (csGlobal.modeDebug) csUtilidades.log("Inicio actualización familias de artículos");
                mySqlConnect.OpenConnection();
                Cursor.Current = Cursors.WaitCursor;
                foreach (DataRow fila in final.Rows)
                {
                    contador++;
                    productPS = fila["id_product"].ToString();
                    familiaA3 = fila["fam"].ToString().Trim();
                    //csUtilidades.log("fila :" + filasTabla + " / " + final.Rows.Count + " artículo " + productPS + " de familia: " + familiaA3);
                    if (familiaA3 != "")
                    {
                        try
                        {
                            scriptUpdate = "Update ps_product set kls_a3erp_fam_id='" + familiaA3 + "' where id_product=" + productPS;
                            //mySqlConnect.ejecutarConsulta(scriptUpdate, true);
                            ConexionSingleton.ObtenerInstancia().ejecutarConsulta(scriptUpdate, true);
                        }
                        catch (Exception ex) 
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    filasTabla++;
                    toolStripStatusLabelProgreso.Text =  "Actualizando...   " + contador + " de " + final.Rows.Count + " ";
                }
                Cursor.Current = Cursors.Default;
                mySqlConnect.CloseConnection();

                if (csGlobal.modeDebug) csUtilidades.log("Fin Actualización de Familias de Artículos");
                contador = 0;

                //Actualizo las familias de los artículos que se han desasignado la familia
                DataTable dtA3DtoNull = sql.cargarDatosTablaA3("SELECT KLS_ID_SHOP as id_product,ltrim(codart) as reference, LTRIM(FAMARTDESCVEN) as fam FROM dbo.ARTICULO WHERE (KLS_ID_SHOP > 0) AND (FAMARTDESCVEN IS NULL)");

                if (dtA3DtoNull.Rows.Count > 0)
                {
                    for (int i = 0; i < dtA3DtoNull.Rows.Count; i++)
                    {
                        filtroArticulos = contador == 0 ? dtA3DtoNull.Rows[i]["id_product"].ToString() : filtroArticulos + "," + dtA3DtoNull.Rows[i]["id_product"].ToString();
                        if (contador == 500)
                        {
                            csUtilidades.ejecutarConsulta("Update ps_product set kls_a3erp_fam_id=null where id_product in (" + filtroArticulos + ")", true);
                            contador = 0;
                        }
                        else
                        {
                            contador++;
                        }
                    }
                    if (contador < 500)
                    {
                        csUtilidades.ejecutarConsulta("Update ps_product set kls_a3erp_fam_id=null where id_product in (" + filtroArticulos + ")", true);
                    }

                }
            }
            catch (Exception ex)
            {
                csUtilidades.log(ex.Message);
            }
        }


        public void actualizarFamiliasClientesPS()
        {
            try
            {
                string clientePS = "";
                string clienteA3 = "";
                string familiaA3 = "";
                
                csSqlConnects sql = new csSqlConnects();                    
                csMySqlConnect mySqlConnect = new csMySqlConnect();  

                if (csGlobal.modeDebug) csUtilidades.log("Inicio actualización de Familias de ClientesArtículos");
                DataTable dtA3 = sql.cargarDatosTablaA3("SELECT CODCLI, LTRIM(FAMCLIDESC) as FAMCLIDESC, KLS_CODCLIENTE FROM dbo.__CLIENTES WHERE (KLS_CODCLIENTE > 0) AND (FAMCLIDESC IS NOT NULL)");
                DataTable dtPS = mySqlConnect.cargarTabla("select id_customer, kls_a3erp_fam_id from ps_customer");

                if (csGlobal.isODBCConnection)
                {
                    OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                }
                else
                {
                    MySqlConnection connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection.Open();
                }

                sql.abrirConexion();
                foreach (DataRow fila in dtPS.Rows)
                {
                    clientePS = fila["id_customer"].ToString();
                    foreach (DataRow filaA3 in dtA3.Rows)
                    {
                        clienteA3 = filaA3["KLS_CODCLIENTE"].ToString();
                        familiaA3 = filaA3["FAMCLIDESC"].ToString();
                        {
                            if (clienteA3 == clientePS)
                            {
                                mySqlConnect.actualizarGenerica("Update ps_customer set kls_a3erp_fam_id='" + familiaA3 + "' where id_customer = " + clientePS);
                            }
                        }
                    }
                }
                sql.cerrarConexion();
                mySqlConnect.CloseConnection();

                toolStripStatusLabelProgreso.Text = "<< Actualización de Familias Cliente / Artículo actualizados >>";
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }
        public void tarifasclientes()
        {
            // Borramos la tabla primero
            if (csGlobal.gestionPreciosHijos)
                csUtilidades.ejecutarConsulta("delete from ps_specific_price where reduction_type = 'amount' and type_prc='trf'", true);
            else
                csUtilidades.ejecutarConsulta("delete from ps_specific_price where reduction_type = 'amount'", true);

            // Variables necesarias
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            List<string> values = new List<string>();
            DataTable a3 = new DataTable();
            string campo_prc = "";

            string type_prc = "')";

            if (csGlobal.gestionPreciosHijos)
            {
                campo_prc = ",type_prc";
                type_prc = "','trf')";
            }

            string campos = "(id_product, id_shop, id_customer, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to " + campo_prc + ")";
            string idproduct = "", customer = "", precio = "", from = "", to = "";
            // Consulta de A3 en un datatable
            a3 = sql.cargarDatosTablaA3("SELECT TARIFAS.DESCTARIFA, CLIENTES.KLS_CODCLIENTE, CLIENTES.CODCLI, ARTICULO.KLS_ID_SHOP, TARIFAVE.* FROM TARIFAVE " +
                                          "LEFT JOIN TARIFAS ON TARIFAS.TARIFA = TARIFAVE.TARIFA " +
                                          "INNER JOIN CLIENTES ON CLIENTES.TARIFA = TARIFAS.TARIFA " +
                                          "INNER JOIN ARTICULO ON ARTICULO.CODART = TARIFAVE.CODART " +
                                          "WHERE PRECIO > 0 AND FECMAX >= GETDATE() " +
                                          "AND KLS_ID_SHOP > 0 " +
                                          "AND KLS_CODCLIENTE > 0 ");

            // Iteramos el DataTable
            foreach (DataRow fila in a3.Rows)
            {
                idproduct = fila["KLS_ID_SHOP"].ToString();
                customer = fila["KLS_CODCLIENTE"].ToString();
                precio = fila["PRECIO"].ToString();
                from = Convert.ToDateTime(fila["FECMIN"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                to = Convert.ToDateTime(fila["FECMAX"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");

                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                {
                    precio = Math.Round(Convert.ToDouble(precio) / 1.21, 3).ToString();
                }
                precio = precio.Replace(",", ".");

                values.Add("(" + idproduct + ", 1, " + customer + ", " + precio + ",1, 0, 'amount', '" + from + "', '" + to + type_prc);
            }

            // Insertamos en Prestashop
            csUtilidades.WriteListToDatabase("ps_specific_price", campos, values, true);

            // Informamos del Progreso
            toolStripStatusLabelProgreso.Text = "Tarifas de clientes actualizadas";

            //Actualizamos las tarifas de los artículos hijos
            if (csGlobal.gestionPreciosHijos)
            {
                tarifasArticulosHijos();
            }
        }
        private void tarifasArticulosHijos()
        {

            csUtilidades.ejecutarConsulta("delete from ps_specific_price where reduction_type = 'amount' and id_product_attribute>0 and type_prc='trf'", true);

            //TODO:ACTUALZIAR TARIFAS DE HIJOS
            //Seleccionamos los artículos que estan en 
            csSqlConnects sql = new csSqlConnects();
            DataTable tarifasA3 = new DataTable();
            string selectTarifaA3 = "SELECT tarifas.desctarifa, " +
                                    "clientes.kls_codcliente, " +
                                    "clientes.codcli, " +
                                    "articulo.kls_id_shop, " +
                                    "articulo.KLS_ARTICULO_PADRE, " +
                                    "tarifave.CODART,tarifave.CODMON,tarifave.FECMAX,tarifave.IDTARIFAV, " +
                                    "tarifave.PRECIO as PRICE,tarifave.TARIFA,tarifave.UNIDADES,tarifave.FECACTTLR,tarifave.TARIFATLR,tarifave.FECMIN " +
                                    "FROM tarifave " +
                                    "LEFT JOIN tarifas " +
                                    "ON tarifas.tarifa = tarifave.tarifa " +
                                    "INNER JOIN clientes " +
                                    "ON clientes.tarifa = tarifas.tarifa " +
                                    "INNER JOIN articulo " +
                                    "ON articulo.codart = tarifave.codart " +
                                    "WHERE precio > 0 " +
                                    "AND fecmax >= Getdate() " +
                                    "AND kls_codcliente > 0 " +
                                    "AND KLS_ARTICULO_PADRE IS NOT NULL ";
            //VERIFICAR SI LOS ARTÍCULOS HIJOS TIENEN QUE ESTAR O NO SINCRONIZADOS
                                    //"AND KLS_ID_SHOP IS NULL";

            tarifasA3 = sql.cargarDatosTablaA3(selectTarifaA3);
            atualizarPreciosHijos(tarifasA3);

        }

        /// <summary>
        /// Método para actualizar los hijos a partir de la referencia
        /// </summary>
        /// <param name="A3ERP"></param>
        /// <param name="PRESTASHOP"></param>

        private void atualizarPreciosHijos(DataTable tarifasA3, string type_prc = "trf")
        {
            //En este caso como unicamente es para actualizar precios hijos no deberia tener que controlar si es gestion de hijos o no.



            string idproduct = "", customer = "", precio = "", from = "", to = "", idAttribute = "", famcli = "";
            string values = "";
            string campo_prc = "";
            if (csGlobal.gestionPreciosHijos)
                campo_prc = ", type_prc";

            string campos = "(id_product, id_shop, id_customer, id_product_attribute, price, from_quantity, reduction, reduction_type, ps_specific_price.from, ps_specific_price.to, kls_famcli" + campo_prc + ")";
            DataTable atributosPS = new DataTable();
            csMySqlConnect mysql = new csMySqlConnect();

            string selectAtributos = "select * from ps_product_attribute";

            atributosPS = mysql.cargarTabla(selectAtributos);


            foreach (DataRow fila in atributosPS.Rows)
            {
                foreach (DataRow fila2 in tarifasA3.Rows)
                {
                    if (fila["reference"].ToString() == fila2["CODART"].ToString().Trim())
                    {

                        idproduct = fila["id_product"].ToString();
                        idAttribute = fila["id_product_attribute"].ToString();
                        customer = fila2["KLS_CODCLIENTE"].ToString().Equals("") ? "0" : fila2["KLS_CODCLIENTE"].ToString();
                        precio = fila2["PRICE"].ToString();
                        from = Convert.ToDateTime(fila2["FECMIN"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        to = Convert.ToDateTime(fila2["FECMAX"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");

                        //Añadimos famcli porque este método tambien se usa para actualziar los precios especiales donde si hay famcli y si existe nos interesa que se especifique en PS.

                        if (tarifasA3.Columns.Contains("famcli"))
                            famcli = fila2["famcli"].ToString().Equals("") ? "0" : fila2["famcli"].ToString();


                        if (csGlobal.ivaIncluido.ToUpper() == "SI")
                        {
                            precio = Math.Round(Convert.ToDouble(precio) / 1.21, 3).ToString();
                        }
                        precio = precio.Replace(",", ".");


                        values += ("(" + idproduct + ", 1, " + customer + ", " + idAttribute + ", " + precio + ", 1, 0, 'amount', '" + from + "', '" + to + "' ,'" + famcli + "' ,'" + type_prc + "'),");

                    }

                }

            }
            //Si values esta vacio significa que no hay precios especiales de productos hijos
            if (values!="")
            {
                mysql.InsertValoresEnTabla("ps_specific_price", campos, values, "", true);
            }
            

        }

        private void btnTarifasClientes_Click(object sender, EventArgs e)
        {
            tarifasclientes();
        }

        private void btnCargarPreciosFicha_Click(object sender, EventArgs e)
        {
            toolStripStatusLabelProgreso.Text = "<< Precios de Ficha cargados >>";
            tarifas = false;
            cargarPreciosFichaArticulo();
        }

        private void btnActualizarPreciosFicha_Click(object sender, EventArgs e)
        {
            actualizarPrecioFichaArticulo(false);
        }

        private void btnCargarPreciosEspeciales_Click(object sender, EventArgs e)
        {
            comboClienteFiltro = "";
            cargarPreciosEspeciales();
            toolStripStatusLabelProgreso.Text = "<< Precios especiales cargados >>";
        }

        private void btnActualizarPreciosEspeciales_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(false);
            toolStripStatusLabelProgreso.Text = "<< Precios especiales actualizados >>";
        }

        private void btnPreciosEspecialesConReduccion_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(true);
            toolStripStatusLabelProgreso.Text = "<< Precios especiales con reducción actualizados >>";
        }

        private void btnActualizarDescuentos_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(traspasarDescuentosPS);
            t.Start();
        }

        private void btnActualizarFamiliasCliArt_Click(object sender, EventArgs e)
        {
            actualiarFamiliasArticulosEnPS();
            actualizarFamiliasClientesPS();
        }

        private void brnActualizarTarifas_Click(object sender, EventArgs e)
        {
            actualizarPrecioFichaArticulo(false);
        }

        private void comboTarifas_SelectedIndexChanged(object sender, EventArgs e)
        {
            tarifas = true;
            cargarTarifaPreciosArticulos();
            if (comboTarifas.SelectedIndex != 0)
            {
                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();

                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectPreciosArticulos(comboTarifas.Text), dataConnection);

                DataTable t = new DataTable();
                a.Fill(t);
                dgvPreciosA3.DataSource = t;
                toolStripStatusLabel1.Text = "Total Filas: " + dgvPreciosA3.Rows.Count.ToString();
            }
        }

        private void btnLoadPrices_Click(object sender, EventArgs e)
        {
            toolStripStatusLabelProgreso.Text = "<< Cargando Precios de Ficha >>";
            tarifas = false;
            cargarPreciosFichaArticulo();
            toolStripStatusLabelProgreso.Text = "<< Precios de Ficha cargados >>";
        }



        private void btnUpdatePrices_Click(object sender, EventArgs e)
        {
            tarifas = true;
            actualizarPrecioFichaArticulo(false);
        }

        private void btnUpdateRates_Click(object sender, EventArgs e)
        {
            tarifasclientes();
        }

        private void btnUpdateCustomerRates_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(false);
            toolStripStatusLabelProgreso.Text = "<< Precios especiales actualizados >>";
        }

        private void btnLoadSpecialPrices_Click(object sender, EventArgs e)
        {
            comboClienteFiltro = "";
            cargarPreciosEspeciales();
            toolStripStatusLabelProgreso.Text = "<< Precios especiales cargados >>";
        }

        private void btnUpdateSpecialPrices_Click(object sender, EventArgs e)
        {
            traspasarPreciosEspecialesV2(true);
            toolStripStatusLabelProgreso.Text = "<< Precios especiales con reducción actualizados >>";
        }

        private void btnLoadDiscounts_Click(object sender, EventArgs e)
        {
            comboClienteFiltro = "";
            cargarPreciosEspeciales();
            toolStripStatusLabelProgreso.Text = "<< Descuentos cargados >>";
        }

        private void btnUpdateDiscounts_Click(object sender, EventArgs e)
        {

            Thread t = new Thread(traspasarDescuentosPS);
            t.Start();

        }

        private void btnUpdateFamilyCustomer_Click(object sender, EventArgs e)
        {

        }



        private void comboRates_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarTarifaPreciosArticulos();
        }
        private void button2_Click(object sender, EventArgs e)
        {

            actualizarPrecioFichaArticulo(true);

        }

        private void btnPrioridades_Click(object sender, EventArgs e)
        {
            if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
            {
                csUtilidades.ejecutarConsulta("update ps_specific_price_priority set priority = 'id_shop;id_currency;id_country;id_group'", true);
            }
            else
            {
                csUtilidades.ejecutarConsulta("update ps_specific_price_priority set priority = 'id_product;id_shop;id_currency;id_country;id_group'", true);
            }
            
            MessageBox.Show("Prioridades actualizadas");
        }

        private void btnPreciosMiro_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            string consulta = "SELECT dbo.ARTICULO.CODART, " +
                        "       dbo.ARTICULO.DESCART, " +
                        "       dbo.DESCUENT.FECMIN, " +
                        "       dbo.DESCUENT.FECMAX, " +
                        "       dbo.DESCUENT.DESC1, " +
                        "       dbo.ARTICULO.FAMARTDESCVEN, " +
                        "       dbo.DESCUENT.FAMART, " +
                        "       dbo.DESCUENT.CODCLI, " +
                        "       dbo.ARTICULO.KLS_ID_SHOP " +
                        "FROM dbo.ARTICULO " +
                        "INNER JOIN dbo.DESCUENT ON dbo.ARTICULO.FAMARTDESCVEN = dbo.DESCUENT.FAMART " +
                        "WHERE (LTRIM(dbo.DESCUENT.CODCLI) = '3') " +
                        "  AND (dbo.ARTICULO.KLS_ID_SHOP > 0) ";

            DataTable precios = sql.cargarDatosTablaA3(consulta);

            csUtilidades.ejecutarConsulta("delete from ps_specific_price where id_group <> 0", true);
            List<string> valores = new List<string>();
            if (csUtilidades.verificarDt(precios))
            {
                string campos = "(id_specific_price_rule, id_cart, id_product, id_shop, id_shop_group, id_currency, id_country, id_group, id_customer, id_product_attribute, price, from_quantity, reduction, reduction_tax, reduction_type, ps_specific_price.from, ps_specific_price.to)";
                foreach (DataRow item in precios.Rows)
                {
                    DateTime fechaDesde = Convert.ToDateTime(item["FECMIN"].ToString());
                    DateTime fechaHasta = Convert.ToDateTime(item["FECMAX"].ToString());

                    string fd = fechaDesde.ToString("yyyy-MM-dd") + " 00:00:00";
                    string fh = fechaHasta.ToString("yyyy-MM-dd") + " 23:59:00";

                    string kls_id_shop = item["KLS_ID_SHOP"].ToString();
                    string descuento = (float.Parse(item["DESC1"].ToString().Trim()) / 100).ToString().Replace(",", ".");

                    valores.Add("(0,0," + kls_id_shop + ",0,0,0,0,4,0,0,-1,1," + descuento + ",1,'percentage', '" + fd + "','" + fh + "')");
                }

                csUtilidades.WriteListToDatabase("ps_specific_price", campos, valores, true);
                MessageBox.Show("Precios amigo actualizados");
            }
            else
            {
                MessageBox.Show("No hay descuentos");
            }
        }

        private void btnPreciosBastide_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            string precios_tallas = "SELECT __TARIFAVETALLA.CODART, kls_id_shop, __TARIFAVETALLA.CODFAMTALLAH, __TARIFAVETALLA.CODFAMTALLAV, __TARIFAVETALLA.CODTALLAH, __TARIFAVETALLA.CODTALLAV, __TARIFAVETALLA.ID, __TARIFAVETALLA.IDTARIFAV, __TARIFAVETALLA.PRECIO,TALLAS_1.ID AS TALLAV,TALLAS.ID AS TALLAH, PORIVA " +
                "FROM __TARIFAVETALLA " +
                "    left join articulo on articulo.codart = __tarifavetalla.codart " +
            "INNER JOIN TALLAS ON __TARIFAVETALLA.CODFAMTALLAH = TALLAS.CODFAMTALLA " +
                "AND __TARIFAVETALLA.CODTALLAH = TALLAS.CODTALLA " +
                "INNER JOIN TALLAS TALLAS_1 ON __TARIFAVETALLA.CODFAMTALLAV = TALLAS_1.CODFAMTALLA " +
                "AND __TARIFAVETALLA.CODTALLAV = TALLAS_1.CODTALLA " +
                " left join TIPOIVA ON dbo.ARTICULO.TIPIVA = TIPOIVA.TIPIVA ";

            // 1. Meter precios_tallas en un dt
            DataTable precios_tallas_dt = sql.cargarDatosTablaA3(precios_tallas);
            // 2. Cargar ps_product_attribute en un dt
            DataTable ps_product_attribute_dt = mysql.cargarTabla("select * from ps_product_attribute");
            DataTable ps_product_attribute_combination = mysql.cargarTabla("select * from ps_product_attribute_combination");

            if (csUtilidades.verificarDt(ps_product_attribute_dt))
            {
                foreach (DataRow ps in ps_product_attribute_dt.Rows)
                {
                    string id_product = ps["id_product"].ToString();

                    string id_product_attribute = ps["id_product_attribute"].ToString();
                    DataTable final = csUtilidades.dataRowArray2DataTable(ps_product_attribute_combination.Select("id_product_attribute = " + ps["id_product_attribute"].ToString()));
                    if (csUtilidades.verificarDt(final))
                    {
                        string v1 = final.Rows[0]["id_attribute"].ToString();
                        string v2 = final.Rows[1]["id_attribute"].ToString();

                        DataTable precio_talla = csUtilidades.dataRowArray2DataTable(precios_tallas_dt.Select("((tallav = '" + v1 + "' AND tallah = '" + v2 + "') OR (tallav = '" + v2 + "' AND tallah = '" + v1 + "')) AND kls_id_shop = '" + id_product + "'"));

                        if (csUtilidades.verificarDt(precio_talla))
                        {
                            //string reference = precio_talla.Rows[0]["codart"].ToString().Trim();
                            float iva = (float.Parse(precio_talla.Rows[0]["poriva"].ToString()) / 100) + 1;
                            string precio = float.Parse(precio_talla.Rows[0]["precio"].ToString()).ToString();
                            string precio_final = (float.Parse(precio) / iva).ToString().Replace(",", ".");

                            csUtilidades.ejecutarConsulta("update ps_product_attribute set price = " + precio_final + " where id_product_attribute = " + id_product_attribute, true);
                            csUtilidades.ejecutarConsulta("update ps_product_attribute_shop set price = " + precio_final + " where id_product_attribute = " + id_product_attribute, true);

                            csUtilidades.ejecutarConsulta("update ps_product set price = 0 where id_product = " + id_product, true);
                            csUtilidades.ejecutarConsulta("update ps_product_shop set price = 0 where id_product = " + id_product, true);
                        }
                    }
                }
            }

            MessageBox.Show("OPERACION COMPLETADA");
        }

        private void btnLoadSpecialPricesReduction_Click(object sender, EventArgs e)
        {
            comboClienteFiltro = "";
            cargarPreciosEspeciales();
            //cargarDescuentos();
            toolStripStatusLabelProgreso.Text = "<< Precios especiales cargados >>";


        }

        private void actualizarFamDescuentosAClientesEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarFamiliasClientesPS();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}

