﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace klsync
{
    public class csSingletonSQL : IDisposable
    {
        private SqlConnection conn;
        private SqlCommand dc = new SqlCommand();
        private string server;
        private string db;
        private string user;
        private string pass;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public csSingletonSQL(string server, string db, string user, string pass)
        {
            this.server = server;
            this.db = db;
            this.user = user;
            this.pass = pass;

            if ((conn = _cnn()) == null)
            {
                this.Dispose();
            }
        }

        private SqlConnection _cnn()
        {
            SqlConnection conn = null;


            string cnnString = string.Format("Server={0};Database={1};Trusted_Connection=SSPI;User Id={2};Password={3};Data Source={4};Initial Catalog={5};Integrated Security=true;persist security info=True", server, db, user, pass, server, db);
            try
            {
                conn = new SqlConnection();
                conn.ConnectionString = cnnString;
                conn.Open();
                return conn;
            }
            catch
            {
                //System.Windows.Forms.MessageBox.Show("ERROR AL ACCEDER A LA BASE DE DATOS");

                conn.Dispose();
                return null;
            }
        }

        /// <summary>
        /// Libera los recursos
        /// </summary>
        public void Dispose()
        {
            if (conn != null)
            {
                conn.Dispose();
            }
        }

        /// <summary>
        /// Devuelve un DataTable con la consulta
        /// </summary>
        /// <param name="consulta"></param>
        /// <returns></returns>
        public DataTable cargarConsultaDT(string consulta)
        {
            try
            {
                SqlDataAdapter a = new SqlDataAdapter(consulta, conn);
                DataTable t = new DataTable();
                a.Fill(t);
                return t;
            }
            catch
            {
                return null;
            }
        }

        public string obtenerCampoTabla(string consulta)
        {
            SqlDataAdapter a = new SqlDataAdapter(consulta, conn);
            DataTable dt = new DataTable();
            a.Fill(dt);

            if (dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();
            else
                return "";
        }

        /// <summary>
        /// Sirve para hacer inserts, updates o deletes
        /// </summary>
        /// <param name="consulta"></param>
        /// <returns></returns>
        public bool ejecutarConsulta(string consulta)
        {
            try
            {
                dc.CommandText = consulta;
                dc.Connection = conn;
                dc.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

}
