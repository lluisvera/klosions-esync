﻿using System;

namespace klsync
{
    partial class csDismay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(csDismay));
            this.cboxMaestros = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStripFacturas = new System.Windows.Forms.StatusStrip();
            this.tssLabelTotalRowsRPST = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.progress = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssTotalAmountRows = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblNumRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLabelTotalRows = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextFactura = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.comprobarSERIESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctextMenuSync = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.enlazarFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarSincronizaciónToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.documentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarImportesToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.totalDocumentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDocumentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numLineasDocsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarSincronizaciónDeFacturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarCódigoDeCuentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otrasUtilidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearEnlaceEnRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carteraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarSituaciónEfectosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarRepasatDesdeA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaActualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filasSeleccionadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearEfectoEnRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarImportesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.recibirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.informaciónSobreEfectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarVencimientoEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remesasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarImportesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarEfectosRemesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarIDsSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarRemesaEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.a3ERPRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearCuentaEnRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.sincronizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.síToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ticketBaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarDocsDesdeA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsProductos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabAuxiliares = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.dgvRepasatData = new System.Windows.Forms.DataGridView();
            this.ctexMenuAuxiliars = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.verificarCodigosEnRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarSincronizaciónEnRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.btnCrearTipoArtA3 = new System.Windows.Forms.Button();
            this.btnUploadTipoArt = new System.Windows.Forms.Button();
            this.btnTipoArt = new System.Windows.Forms.Button();
            this.btnCrearMarcasArtA3 = new System.Windows.Forms.Button();
            this.btnUploadMarcasArt = new System.Windows.Forms.Button();
            this.btnMarcasArt = new System.Windows.Forms.Button();
            this.btnCrearSubfamiliaArtA3 = new System.Windows.Forms.Button();
            this.btnUploadSubfamiliaArt = new System.Windows.Forms.Button();
            this.btnSubfamiliaArt = new System.Windows.Forms.Button();
            this.btnCrearFamiliaArtA3 = new System.Windows.Forms.Button();
            this.btnUploadFamiliaArt = new System.Windows.Forms.Button();
            this.btnFamiliaArt = new System.Windows.Forms.Button();
            this.btnAuxiliaresToRPST = new System.Windows.Forms.Button();
            this.btnCrearAlmacenA3 = new System.Windows.Forms.Button();
            this.btnCrearZonasA3 = new System.Windows.Forms.Button();
            this.btnCrearRutasA3 = new System.Windows.Forms.Button();
            this.btnCrearTranspotA3 = new System.Windows.Forms.Button();
            this.btnCrearDocsPagoA3 = new System.Windows.Forms.Button();
            this.btnCrearSeriesA3 = new System.Windows.Forms.Button();
            this.btnUploadCentrosCoste = new System.Windows.Forms.Button();
            this.btnCrearCentrosCosteA3 = new System.Windows.Forms.Button();
            this.btnCostCenters = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnCrearProyectosA3 = new System.Windows.Forms.Button();
            this.btnBanks = new System.Windows.Forms.Button();
            this.btnProyectos = new System.Windows.Forms.Button();
            this.btnUploadCuentasContables = new System.Windows.Forms.Button();
            this.btnPlanContable = new System.Windows.Forms.Button();
            this.btnUploadTipoImpuestos = new System.Windows.Forms.Button();
            this.btnTiposImpuestos = new System.Windows.Forms.Button();
            this.btnUploadAlmacenes = new System.Windows.Forms.Button();
            this.btnUploadSeries = new System.Windows.Forms.Button();
            this.btnAlmacenes = new System.Windows.Forms.Button();
            this.btnSeries = new System.Windows.Forms.Button();
            this.btnUploadRegimenes = new System.Windows.Forms.Button();
            this.btnRegImpuestos = new System.Windows.Forms.Button();
            this.btnUploadZonas = new System.Windows.Forms.Button();
            this.btnDocsPago = new System.Windows.Forms.Button();
            this.btnMetPago = new System.Windows.Forms.Button();
            this.btnRepresen = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnUploadRutas = new System.Windows.Forms.Button();
            this.btnZonas = new System.Windows.Forms.Button();
            this.btnRutas = new System.Windows.Forms.Button();
            this.btnRepresentantes = new System.Windows.Forms.Button();
            this.picboxRepasat = new System.Windows.Forms.PictureBox();
            this.picboxA3ERP = new System.Windows.Forms.PictureBox();
            this.btPaymentDocs = new System.Windows.Forms.Button();
            this.btnCarriers = new System.Windows.Forms.Button();
            this.btnPaymentMethods = new System.Windows.Forms.Button();
            this.dgvA3ERPData = new System.Windows.Forms.DataGridView();
            this.cMenuAuxiliar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarSincronizaciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabMasterFiles = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvAccountsRepasat = new System.Windows.Forms.DataGridView();
            this.ctexMenuMaster = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarSincronizaciónToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarEnA3ERPSelecciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.articulosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearCódigoExternoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.siToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarHaciaA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comercialesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.validarEnlacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.porCódigoExternoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.direccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.porCIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDays = new System.Windows.Forms.Label();
            this.numUpdownMovDays = new System.Windows.Forms.NumericUpDown();
            this.rbtnSelectProductsMovs = new System.Windows.Forms.RadioButton();
            this.rbtnAllProducts = new System.Windows.Forms.RadioButton();
            this.btnSyncStock = new System.Windows.Forms.Button();
            this.grBoxAccounts = new System.Windows.Forms.GroupBox();
            this.btUpdateRepasatToA3ERP = new System.Windows.Forms.Button();
            this.btUpdateA3ERPToRepasat = new System.Windows.Forms.Button();
            this.lblUdadNegocio = new System.Windows.Forms.Label();
            this.grBoxActivos = new System.Windows.Forms.GroupBox();
            this.rbActiveYes = new System.Windows.Forms.RadioButton();
            this.rbActiveAll = new System.Windows.Forms.RadioButton();
            this.rbActiveNo = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbObsoletYes = new System.Windows.Forms.RadioButton();
            this.rbObsoletAll = new System.Windows.Forms.RadioButton();
            this.rbObsoletNo = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbSyncYes = new System.Windows.Forms.RadioButton();
            this.rbSyncAll = new System.Windows.Forms.RadioButton();
            this.rbSyncNo = new System.Windows.Forms.RadioButton();
            this.btnRPSTAddress = new System.Windows.Forms.Button();
            this.tbAccountId = new System.Windows.Forms.TextBox();
            this.btnLoadAccountsRepasat = new System.Windows.Forms.Button();
            this.btnLoadAccountsERP = new System.Windows.Forms.Button();
            this.btSendCustomersRepasatToA3ERP = new System.Windows.Forms.Button();
            this.btSendCustomersA3ERPToRepasat = new System.Windows.Forms.Button();
            this.dgvAccountsA3ERP = new System.Windows.Forms.DataGridView();
            this.contextMenuERPData = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarPorCodigoExternoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.códigoExternoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcarSyncroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcarNoSyncroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEnRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selecciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.articulosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activarControlStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearIdRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarArticulosEnRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarArticulosDeA3EnRPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarHaciaRepasatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comercialesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabDocs = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grBoxTicketBai = new System.Windows.Forms.GroupBox();
            this.bt_tbai = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.rbTraspasarComoBorradorSi = new System.Windows.Forms.RadioButton();
            this.rbTraspasarComoBorradorNo = new System.Windows.Forms.RadioButton();
            this.picboxTicketBai = new System.Windows.Forms.PictureBox();
            this.grBoxDocPago = new System.Windows.Forms.GroupBox();
            this.checkBoxDocPagos = new System.Windows.Forms.CheckBox();
            this.cboxDocPagos = new System.Windows.Forms.ComboBox();
            this.rdbutFromRPST = new System.Windows.Forms.RadioButton();
            this.rdbutFromA3ERP = new System.Windows.Forms.RadioButton();
            this.lblShowData = new System.Windows.Forms.Label();
            this.picBoxRPST = new System.Windows.Forms.PictureBox();
            this.grBoxCuenta = new System.Windows.Forms.GroupBox();
            this.checkBoxCuentas = new System.Windows.Forms.CheckBox();
            this.cboxCuentas = new System.Windows.Forms.ComboBox();
            this.grBoxRemesado = new System.Windows.Forms.GroupBox();
            this.rbRemesadoSi = new System.Windows.Forms.RadioButton();
            this.rbRemesadoAll = new System.Windows.Forms.RadioButton();
            this.rbRemesadoNo = new System.Windows.Forms.RadioButton();
            this.grBoxTipoDocumento = new System.Windows.Forms.GroupBox();
            this.rbVentas = new System.Windows.Forms.RadioButton();
            this.rbCompras = new System.Windows.Forms.RadioButton();
            this.cboxTipoDoc = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.grBoxFechas = new System.Windows.Forms.GroupBox();
            this.rbutFechaFactura = new System.Windows.Forms.RadioButton();
            this.rbutFechaVencimiento = new System.Windows.Forms.RadioButton();
            this.dtpFromInvoicesV = new System.Windows.Forms.DateTimePicker();
            this.dtpToInvoicesV = new System.Windows.Forms.DateTimePicker();
            this.lbDesde = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grBoxSeries = new System.Windows.Forms.GroupBox();
            this.checkBoxSeries = new System.Windows.Forms.CheckBox();
            this.cboxSeries = new System.Windows.Forms.ComboBox();
            this.grBoxEstadoCobro = new System.Windows.Forms.GroupBox();
            this.rbPaidYes = new System.Windows.Forms.RadioButton();
            this.rbPaidAll = new System.Windows.Forms.RadioButton();
            this.rbPaidNo = new System.Windows.Forms.RadioButton();
            this.btnLoadDocsRPST = new System.Windows.Forms.Button();
            this.btnLoadDocsA3ERP = new System.Windows.Forms.Button();
            this.btnSyncToRPST = new System.Windows.Forms.Button();
            this.btnSyncToA3ERP = new System.Windows.Forms.Button();
            this.grBoxSincronizados = new System.Windows.Forms.GroupBox();
            this.rbSyncDocYes = new System.Windows.Forms.RadioButton();
            this.rbSyncDocAll = new System.Windows.Forms.RadioButton();
            this.rbSyncDocNo = new System.Windows.Forms.RadioButton();
            this.picBoxA3 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cboxTicketBai = new System.Windows.Forms.CheckBox();
            this.cboxDescripcionArticulosA3 = new System.Windows.Forms.CheckBox();
            this.cboxCheckCuentas = new System.Windows.Forms.CheckBox();
            this.cboxGenerarMovsContables = new System.Windows.Forms.CheckBox();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.dgvRPST = new System.Windows.Forms.DataGridView();
            this.dgvSyncDocsDetail = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabStock = new System.Windows.Forms.TabPage();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.grboxSelectStock = new System.Windows.Forms.GroupBox();
            this.rbutStock = new System.Windows.Forms.RadioButton();
            this.rbutStockActivity = new System.Windows.Forms.RadioButton();
            this.grBoxAlmacenes = new System.Windows.Forms.GroupBox();
            this.checkBAlmacen = new System.Windows.Forms.CheckBox();
            this.cboxAlmacen = new System.Windows.Forms.ComboBox();
            this.btnQueryStockRepasat = new System.Windows.Forms.Button();
            this.btnQueryStockA3ERP = new System.Windows.Forms.Button();
            this.btnUpdateStockToRepasat = new System.Windows.Forms.Button();
            this.btnUpdateStockToA3ERP = new System.Windows.Forms.Button();
            this.grBoxSelectDatesStock = new System.Windows.Forms.GroupBox();
            this.dtPickerFromStock = new System.Windows.Forms.DateTimePicker();
            this.dtPickerToStock = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgvStock = new System.Windows.Forms.DataGridView();
            this.ctextMenuStock = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.regularizarEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabValidations = new System.Windows.Forms.TabPage();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.btnTestConexionesBD = new System.Windows.Forms.Button();
            this.btnLoadValidationData = new System.Windows.Forms.Button();
            this.cboxvalidatingMaster = new System.Windows.Forms.ComboBox();
            this.dgvValidatingData = new System.Windows.Forms.DataGridView();
            this.tabUpdate = new System.Windows.Forms.TabPage();
            this.testcc = new System.Windows.Forms.Button();
            this.btnTestConnect1 = new System.Windows.Forms.Button();
            this.dtpDateUpdateClientes = new System.Windows.Forms.DateTimePicker();
            this.btnCrearDirecciones = new System.Windows.Forms.Button();
            this.dtpickerResetFrom = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnUpdateProveedorsRPST = new System.Windows.Forms.Button();
            this.btnBanksToRepasat = new System.Windows.Forms.Button();
            this.btnGestionarBancosEnA3ERP = new System.Windows.Forms.Button();
            this.btnUpdateClientes = new System.Windows.Forms.Button();
            this.btnFullUpdate = new System.Windows.Forms.Button();
            this.btnUpdateClientesA3ERP = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnGestionarDireccionesEnA3ERP = new System.Windows.Forms.Button();
            this.btnContactsTorepasat = new System.Windows.Forms.Button();
            this.btnContactsToA3 = new System.Windows.Forms.Button();
            this.btnResetUpdateDateSync = new System.Windows.Forms.Button();
            this.btnUpdateProducts = new System.Windows.Forms.Button();
            this.btnProductUpdateToRepasat = new System.Windows.Forms.Button();
            this.btnUpdateClientesRPST = new System.Windows.Forms.Button();
            this.btnUpdateArticulosFromRPSTToA3 = new System.Windows.Forms.Button();
            this.btnProveedores = new System.Windows.Forms.Button();
            this.tabUtilidades = new System.Windows.Forms.TabPage();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.btklstic = new System.Windows.Forms.Button();
            this.btnApex = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.btnAsientosUtils = new System.Windows.Forms.Button();
            this.btnContactos = new System.Windows.Forms.Button();
            this.btnRegStock = new System.Windows.Forms.Button();
            this.btnCheckDombanca = new System.Windows.Forms.Button();
            this.btnConfirmarMandato = new System.Windows.Forms.Button();
            this.btnAsignBankRecibos = new System.Windows.Forms.Button();
            this.dgvUtilidades = new System.Windows.Forms.DataGridView();
            this.tabReasignacion = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbIdDomBancaDestino = new System.Windows.Forms.TextBox();
            this.tbIdDomBancaOrigen = new System.Windows.Forms.TextBox();
            this.lbIdDomBancaD = new System.Windows.Forms.Label();
            this.lbIdDomBancaO = new System.Windows.Forms.Label();
            this.lbIdDirentD = new System.Windows.Forms.Label();
            this.lbIdDirentO = new System.Windows.Forms.Label();
            this.tbIdDirentD = new System.Windows.Forms.TextBox();
            this.tbIdDirentO = new System.Windows.Forms.TextBox();
            this.IdDirentD = new System.Windows.Forms.Label();
            this.IdDirentO = new System.Windows.Forms.Label();
            this.lbCTAPROVISIONNPGCDestino = new System.Windows.Forms.Label();
            this.lbCTAPROVISIONDestino = new System.Windows.Forms.Label();
            this.lbCTAPORCUENTADestino = new System.Windows.Forms.Label();
            this.lbCTALETRADestino = new System.Windows.Forms.Label();
            this.lbCTADESCDestino = new System.Windows.Forms.Label();
            this.lbcuentaDestino = new System.Windows.Forms.Label();
            this.lbcodCliD = new System.Windows.Forms.Label();
            this.lbCTAPROVISIONNPGCOrigen = new System.Windows.Forms.Label();
            this.lbCTAPROVISIONOrigen = new System.Windows.Forms.Label();
            this.lbCTAPORCUENTAOrigen = new System.Windows.Forms.Label();
            this.lbCTALETRAOrigen = new System.Windows.Forms.Label();
            this.lbCTADESCOrigen = new System.Windows.Forms.Label();
            this.lbCuentaOrigen = new System.Windows.Forms.Label();
            this.lbCodCliO = new System.Windows.Forms.Label();
            this.tbCTAPROVISIONNPGCDestino = new System.Windows.Forms.TextBox();
            this.tbCTAPROVISIONDestino = new System.Windows.Forms.TextBox();
            this.tbCTAPORCUENTADestino = new System.Windows.Forms.TextBox();
            this.tbCTALETRADestino = new System.Windows.Forms.TextBox();
            this.tbCTADESCDestino = new System.Windows.Forms.TextBox();
            this.tbcuentaDestino = new System.Windows.Forms.TextBox();
            this.tbCodCliDestino = new System.Windows.Forms.TextBox();
            this.tbCTAPROVISIONNPGCOrigen = new System.Windows.Forms.TextBox();
            this.tbCTAPROVISIONOrigen = new System.Windows.Forms.TextBox();
            this.tbCTAPORCUENTAOrigen = new System.Windows.Forms.TextBox();
            this.tbCTALETRAOrigen = new System.Windows.Forms.TextBox();
            this.tbCTADESCOrigen = new System.Windows.Forms.TextBox();
            this.tbCuentaOrigen = new System.Windows.Forms.TextBox();
            this.tbCodCliOrigen = new System.Windows.Forms.TextBox();
            this.rbClientes = new System.Windows.Forms.RadioButton();
            this.rbProveedores = new System.Windows.Forms.RadioButton();
            this.btReAsing = new System.Windows.Forms.Button();
            this.CTAPROVISIONNPGCDestino = new System.Windows.Forms.Label();
            this.CTAPROVISIONDestino = new System.Windows.Forms.Label();
            this.CTAPORCUENTADestino = new System.Windows.Forms.Label();
            this.CTALETRADestino = new System.Windows.Forms.Label();
            this.CTAPROVISIONNPGCOrigen = new System.Windows.Forms.Label();
            this.CTAPROVISIONOrigen = new System.Windows.Forms.Label();
            this.CTAPORCUENTAOrigen = new System.Windows.Forms.Label();
            this.CTALETRAOrigen = new System.Windows.Forms.Label();
            this.CTADESCDestino = new System.Windows.Forms.Label();
            this.cuentaDestino = new System.Windows.Forms.Label();
            this.codCliD = new System.Windows.Forms.Label();
            this.CTADESCOrigen = new System.Windows.Forms.Label();
            this.cuentaOrigen = new System.Windows.Forms.Label();
            this.cbClienteDestino = new System.Windows.Forms.ComboBox();
            this.cbClienteOrigen = new System.Windows.Forms.ComboBox();
            this.codCliO = new System.Windows.Forms.Label();
            this.textoComboboxDestino = new System.Windows.Forms.Label();
            this.textoComboboxOrigen = new System.Windows.Forms.Label();
            this.tabCronJobs = new System.Windows.Forms.TabPage();
            this.btnAbririProgramadorTareas = new System.Windows.Forms.Button();
            this.btnSalirModoEdicionTareasProgramadas = new System.Windows.Forms.Button();
            this.lblModoEdicionTareasProgramadas = new System.Windows.Forms.Label();
            this.dgvCronJobs = new System.Windows.Forms.DataGridView();
            this.dtpStartHour = new System.Windows.Forms.DateTimePicker();
            this.cmbFrequency = new System.Windows.Forms.ComboBox();
            this.lblStartHour = new System.Windows.Forms.Label();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.lblNameTask = new System.Windows.Forms.Label();
            this.cmbNameTask = new System.Windows.Forms.ComboBox();
            this.btnGuardarConfTareasProgramadas = new System.Windows.Forms.Button();
            this.lblTituloTareasProgramadas = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.toolTipSync = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbMinutos = new System.Windows.Forms.ComboBox();
            this.lblMinutos = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStripFacturas.SuspendLayout();
            this.contextFactura.SuspendLayout();
            this.ctextMenuSync.SuspendLayout();
            this.cmsProductos.SuspendLayout();
            this.tabAuxiliares.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepasatData)).BeginInit();
            this.ctexMenuAuxiliars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxRepasat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxA3ERP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3ERPData)).BeginInit();
            this.cMenuAuxiliar.SuspendLayout();
            this.tabMasterFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsRepasat)).BeginInit();
            this.ctexMenuMaster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpdownMovDays)).BeginInit();
            this.grBoxAccounts.SuspendLayout();
            this.grBoxActivos.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsA3ERP)).BeginInit();
            this.contextMenuERPData.SuspendLayout();
            this.tabDocs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grBoxTicketBai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTicketBai)).BeginInit();
            this.grBoxDocPago.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRPST)).BeginInit();
            this.grBoxCuenta.SuspendLayout();
            this.grBoxRemesado.SuspendLayout();
            this.grBoxTipoDocumento.SuspendLayout();
            this.grBoxFechas.SuspendLayout();
            this.grBoxSeries.SuspendLayout();
            this.grBoxEstadoCobro.SuspendLayout();
            this.grBoxSincronizados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxA3)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRPST)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSyncDocsDetail)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.grboxSelectStock.SuspendLayout();
            this.grBoxAlmacenes.SuspendLayout();
            this.grBoxSelectDatesStock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).BeginInit();
            this.ctextMenuStock.SuspendLayout();
            this.tabValidations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.Panel1.SuspendLayout();
            this.splitContainer9.Panel2.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValidatingData)).BeginInit();
            this.tabUpdate.SuspendLayout();
            this.tabUtilidades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUtilidades)).BeginInit();
            this.tabReasignacion.SuspendLayout();
            this.tabCronJobs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCronJobs)).BeginInit();
            this.SuspendLayout();
            // 
            // cboxMaestros
            // 
            this.cboxMaestros.FormattingEnabled = true;
            this.cboxMaestros.Items.AddRange(new object[] {
            "Clientes",
            "Proveedores",
            "Productos",
            "Direcciones Clientes",
            "Direcciones Proveedores"});
            this.cboxMaestros.Location = new System.Drawing.Point(16, 21);
            this.cboxMaestros.Name = "cboxMaestros";
            this.cboxMaestros.Size = new System.Drawing.Size(99, 28);
            this.cboxMaestros.TabIndex = 20;
            this.cboxMaestros.Text = "Clientes";
            this.cboxMaestros.SelectedIndexChanged += new System.EventHandler(this.cboxMaestros_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.statusStripFacturas, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.30561F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1394, 793);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // statusStripFacturas
            // 
            this.statusStripFacturas.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.statusStripFacturas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssLabelTotalRowsRPST,
            this.toolStripStatusLabel1,
            this.progress,
            this.toolStripStatusLabel4,
            this.tssTotalAmountRows,
            this.statusLabel,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.lblNumRegistros,
            this.tssLabelTotalRows});
            this.statusStripFacturas.Location = new System.Drawing.Point(0, 767);
            this.statusStripFacturas.Name = "statusStripFacturas";
            this.statusStripFacturas.Size = new System.Drawing.Size(1394, 26);
            this.statusStripFacturas.TabIndex = 0;
            this.statusStripFacturas.Text = "statusStrip2";
            // 
            // tssLabelTotalRowsRPST
            // 
            this.tssLabelTotalRowsRPST.Name = "tssLabelTotalRowsRPST";
            this.tssLabelTotalRowsRPST.Size = new System.Drawing.Size(164, 21);
            this.tssLabelTotalRowsRPST.Text = "REGISTROS REPASAT: ";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(994, 21);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // progress
            // 
            this.progress.MergeIndex = 1;
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(100, 20);
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(14, 21);
            this.toolStripStatusLabel4.Text = "|";
            // 
            // tssTotalAmountRows
            // 
            this.tssTotalAmountRows.Name = "tssTotalAmountRows";
            this.tssTotalAmountRows.Size = new System.Drawing.Size(0, 21);
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(14, 21);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(91, 21);
            this.toolStripStatusLabel3.Text = "REGISTROS";
            // 
            // lblNumRegistros
            // 
            this.lblNumRegistros.Name = "lblNumRegistros";
            this.lblNumRegistros.Size = new System.Drawing.Size(0, 21);
            // 
            // tssLabelTotalRows
            // 
            this.tssLabelTotalRows.Name = "tssLabelTotalRows";
            this.tssLabelTotalRows.Size = new System.Drawing.Size(0, 21);
            // 
            // contextFactura
            // 
            this.contextFactura.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextFactura.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comprobarSERIESToolStripMenuItem});
            this.contextFactura.Name = "contextClientes";
            this.contextFactura.ShowItemToolTips = false;
            this.contextFactura.Size = new System.Drawing.Size(213, 30);
            // 
            // comprobarSERIESToolStripMenuItem
            // 
            this.comprobarSERIESToolStripMenuItem.Name = "comprobarSERIESToolStripMenuItem";
            this.comprobarSERIESToolStripMenuItem.Size = new System.Drawing.Size(212, 26);
            this.comprobarSERIESToolStripMenuItem.Text = "Comprobar SERIES";
            // 
            // ctextMenuSync
            // 
            this.ctextMenuSync.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enlazarFacturaToolStripMenuItem,
            this.borrarSincronizaciónToolStripMenuItem3,
            this.documentosToolStripMenuItem,
            this.otrasUtilidadesToolStripMenuItem,
            this.carteraToolStripMenuItem,
            this.remesasToolStripMenuItem,
            this.a3ERPRPSTToolStripMenuItem,
            this.toolStripSeparator3,
            this.sincronizarToolStripMenuItem,
            this.toolStripSeparator1,
            this.ticketBaiToolStripMenuItem});
            this.ctextMenuSync.Name = "ctextMenuSync";
            this.ctextMenuSync.Size = new System.Drawing.Size(187, 214);
            this.ctextMenuSync.Opening += new System.ComponentModel.CancelEventHandler(this.ctextMenuSync_Opening);
            // 
            // enlazarFacturaToolStripMenuItem
            // 
            this.enlazarFacturaToolStripMenuItem.Name = "enlazarFacturaToolStripMenuItem";
            this.enlazarFacturaToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.enlazarFacturaToolStripMenuItem.Text = "Enlazar Documento";
            this.enlazarFacturaToolStripMenuItem.Click += new System.EventHandler(this.enlazarFacturaToolStripMenuItem_Click);
            // 
            // borrarSincronizaciónToolStripMenuItem3
            // 
            this.borrarSincronizaciónToolStripMenuItem3.Name = "borrarSincronizaciónToolStripMenuItem3";
            this.borrarSincronizaciónToolStripMenuItem3.Size = new System.Drawing.Size(186, 22);
            this.borrarSincronizaciónToolStripMenuItem3.Text = "Borrar Sincronización";
            this.borrarSincronizaciónToolStripMenuItem3.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem3_Click);
            // 
            // documentosToolStripMenuItem
            // 
            this.documentosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarImportesToolStripMenuItem3,
            this.verificarSincronizaciónDeFacturasToolStripMenuItem,
            this.verificarCódigoDeCuentasToolStripMenuItem,
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem});
            this.documentosToolStripMenuItem.Name = "documentosToolStripMenuItem";
            this.documentosToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.documentosToolStripMenuItem.Text = "Documentos";
            // 
            // verificarImportesToolStripMenuItem3
            // 
            this.verificarImportesToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.totalDocumentoToolStripMenuItem,
            this.baseDocumentoToolStripMenuItem,
            this.numLineasDocsToolStripMenuItem});
            this.verificarImportesToolStripMenuItem3.Name = "verificarImportesToolStripMenuItem3";
            this.verificarImportesToolStripMenuItem3.Size = new System.Drawing.Size(302, 22);
            this.verificarImportesToolStripMenuItem3.Text = "Verificar Importes/Líneas";
            // 
            // totalDocumentoToolStripMenuItem
            // 
            this.totalDocumentoToolStripMenuItem.Name = "totalDocumentoToolStripMenuItem";
            this.totalDocumentoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.totalDocumentoToolStripMenuItem.Text = "Total Documento";
            this.totalDocumentoToolStripMenuItem.Click += new System.EventHandler(this.totalDocumentoToolStripMenuItem_Click);
            // 
            // baseDocumentoToolStripMenuItem
            // 
            this.baseDocumentoToolStripMenuItem.Name = "baseDocumentoToolStripMenuItem";
            this.baseDocumentoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.baseDocumentoToolStripMenuItem.Text = "Base Documento";
            this.baseDocumentoToolStripMenuItem.Click += new System.EventHandler(this.baseDocumentoToolStripMenuItem_Click);
            // 
            // numLineasDocsToolStripMenuItem
            // 
            this.numLineasDocsToolStripMenuItem.Name = "numLineasDocsToolStripMenuItem";
            this.numLineasDocsToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.numLineasDocsToolStripMenuItem.Text = "Número Lineas";
            this.numLineasDocsToolStripMenuItem.Click += new System.EventHandler(this.numLineasDocsToolStripMenuItem_Click);
            // 
            // verificarSincronizaciónDeFacturasToolStripMenuItem
            // 
            this.verificarSincronizaciónDeFacturasToolStripMenuItem.Name = "verificarSincronizaciónDeFacturasToolStripMenuItem";
            this.verificarSincronizaciónDeFacturasToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.verificarSincronizaciónDeFacturasToolStripMenuItem.Text = "Verificar Sincronización de Facturas";
            this.verificarSincronizaciónDeFacturasToolStripMenuItem.Click += new System.EventHandler(this.verificarSincronizaciónDeFacturasToolStripMenuItem_Click);
            // 
            // verificarCódigoDeCuentasToolStripMenuItem
            // 
            this.verificarCódigoDeCuentasToolStripMenuItem.Name = "verificarCódigoDeCuentasToolStripMenuItem";
            this.verificarCódigoDeCuentasToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.verificarCódigoDeCuentasToolStripMenuItem.Text = "Verificar Código de Cuentas en Docs A3ERP";
            this.verificarCódigoDeCuentasToolStripMenuItem.Click += new System.EventHandler(this.verificarCódigoDeCuentasToolStripMenuItem_Click);
            // 
            // verificarCódigoDeCuentaEnA3ERPToolStripMenuItem
            // 
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem.Name = "verificarCódigoDeCuentaEnA3ERPToolStripMenuItem";
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem.Text = "Verificar Código de Cuenta en A3ERP";
            this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.verificarCódigoDeCuentaEnA3ERPToolStripMenuItem_Click);
            // 
            // otrasUtilidadesToolStripMenuItem
            // 
            this.otrasUtilidadesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem,
            this.resetearEnlaceEnRepasatToolStripMenuItem,
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem});
            this.otrasUtilidadesToolStripMenuItem.Name = "otrasUtilidadesToolStripMenuItem";
            this.otrasUtilidadesToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.otrasUtilidadesToolStripMenuItem.Text = "Otras Utilidades";
            // 
            // actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem
            // 
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem.Name = "actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem";
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(362, 22);
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem.Text = "Actualizar ID Documento RPST en A3ERP";
            this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem_Click);
            // 
            // resetearEnlaceEnRepasatToolStripMenuItem
            // 
            this.resetearEnlaceEnRepasatToolStripMenuItem.Name = "resetearEnlaceEnRepasatToolStripMenuItem";
            this.resetearEnlaceEnRepasatToolStripMenuItem.Size = new System.Drawing.Size(362, 22);
            this.resetearEnlaceEnRepasatToolStripMenuItem.Text = "Resetear Enlace en Repasat";
            this.resetearEnlaceEnRepasatToolStripMenuItem.Click += new System.EventHandler(this.resetearEnlaceEnRepasatToolStripMenuItem_Click);
            // 
            // forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem
            // 
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem.Name = "forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem";
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem.Size = new System.Drawing.Size(362, 22);
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem.Text = "Forzar traspaso cuenta a A3ERP con Código Informado";
            this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem.Click += new System.EventHandler(this.forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem_Click);
            // 
            // carteraToolStripMenuItem
            // 
            this.carteraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarSituaciónEfectosToolStripMenuItem,
            this.actualizarRepasatDesdeA3ToolStripMenuItem,
            this.actualizarToolStripMenuItem,
            this.verificarImportesToolStripMenuItem2,
            this.recibirToolStripMenuItem1,
            this.informaciónSobreEfectoToolStripMenuItem,
            this.editarVencimientoEnA3ERPToolStripMenuItem});
            this.carteraToolStripMenuItem.Name = "carteraToolStripMenuItem";
            this.carteraToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.carteraToolStripMenuItem.Text = "Cartera";
            // 
            // verificarSituaciónEfectosToolStripMenuItem
            // 
            this.verificarSituaciónEfectosToolStripMenuItem.Name = "verificarSituaciónEfectosToolStripMenuItem";
            this.verificarSituaciónEfectosToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.verificarSituaciónEfectosToolStripMenuItem.Text = "Verificar Situación Efectos";
            this.verificarSituaciónEfectosToolStripMenuItem.Click += new System.EventHandler(this.verificarSituaciónEfectosToolStripMenuItem_Click);
            // 
            // actualizarRepasatDesdeA3ToolStripMenuItem
            // 
            this.actualizarRepasatDesdeA3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaActualToolStripMenuItem,
            this.filasSeleccionadasToolStripMenuItem,
            this.resetearEfectoEnRPSTToolStripMenuItem});
            this.actualizarRepasatDesdeA3ToolStripMenuItem.Name = "actualizarRepasatDesdeA3ToolStripMenuItem";
            this.actualizarRepasatDesdeA3ToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.actualizarRepasatDesdeA3ToolStripMenuItem.Text = "Actualizar Repasat desde A3";
            // 
            // consultaActualToolStripMenuItem
            // 
            this.consultaActualToolStripMenuItem.Name = "consultaActualToolStripMenuItem";
            this.consultaActualToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.consultaActualToolStripMenuItem.Text = "Consulta Actual";
            this.consultaActualToolStripMenuItem.Click += new System.EventHandler(this.consultaActualToolStripMenuItem_Click);
            // 
            // filasSeleccionadasToolStripMenuItem
            // 
            this.filasSeleccionadasToolStripMenuItem.Name = "filasSeleccionadasToolStripMenuItem";
            this.filasSeleccionadasToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.filasSeleccionadasToolStripMenuItem.Text = "Filas Seleccionadas";
            this.filasSeleccionadasToolStripMenuItem.Click += new System.EventHandler(this.filasSeleccionadasToolStripMenuItem_Click);
            // 
            // resetearEfectoEnRPSTToolStripMenuItem
            // 
            this.resetearEfectoEnRPSTToolStripMenuItem.Name = "resetearEfectoEnRPSTToolStripMenuItem";
            this.resetearEfectoEnRPSTToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.resetearEfectoEnRPSTToolStripMenuItem.Text = "Resetear efecto en RPST";
            this.resetearEfectoEnRPSTToolStripMenuItem.Click += new System.EventHandler(this.resetearEfectoEnRPSTToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.actualizarToolStripMenuItem.Text = "Actualizar A3 desde Repasat";
            // 
            // verificarImportesToolStripMenuItem2
            // 
            this.verificarImportesToolStripMenuItem2.Name = "verificarImportesToolStripMenuItem2";
            this.verificarImportesToolStripMenuItem2.Size = new System.Drawing.Size(226, 22);
            this.verificarImportesToolStripMenuItem2.Text = "Verificar Importes";
            this.verificarImportesToolStripMenuItem2.Click += new System.EventHandler(this.verificarImportesToolStripMenuItem2_Click);
            // 
            // recibirToolStripMenuItem1
            // 
            this.recibirToolStripMenuItem1.Name = "recibirToolStripMenuItem1";
            this.recibirToolStripMenuItem1.Size = new System.Drawing.Size(226, 22);
            this.recibirToolStripMenuItem1.Text = "Recibir";
            this.recibirToolStripMenuItem1.Click += new System.EventHandler(this.recibirToolStripMenuItem1_Click);
            // 
            // informaciónSobreEfectoToolStripMenuItem
            // 
            this.informaciónSobreEfectoToolStripMenuItem.Name = "informaciónSobreEfectoToolStripMenuItem";
            this.informaciónSobreEfectoToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.informaciónSobreEfectoToolStripMenuItem.Text = "Información sobre Efecto";
            this.informaciónSobreEfectoToolStripMenuItem.Click += new System.EventHandler(this.informaciónSobreEfectoToolStripMenuItem_Click);
            // 
            // editarVencimientoEnA3ERPToolStripMenuItem
            // 
            this.editarVencimientoEnA3ERPToolStripMenuItem.Name = "editarVencimientoEnA3ERPToolStripMenuItem";
            this.editarVencimientoEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.editarVencimientoEnA3ERPToolStripMenuItem.Text = "Editar Vencimiento en A3ERP";
            this.editarVencimientoEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.editarVencimientoEnA3ERPToolStripMenuItem_Click);
            // 
            // remesasToolStripMenuItem
            // 
            this.remesasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarImportesToolStripMenuItem1,
            this.cargarEfectosRemesaToolStripMenuItem,
            this.verificarIDsSincronizaciónToolStripMenuItem,
            this.borrarRemesaEnA3ERPToolStripMenuItem});
            this.remesasToolStripMenuItem.Name = "remesasToolStripMenuItem";
            this.remesasToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.remesasToolStripMenuItem.Text = "Remesas";
            this.remesasToolStripMenuItem.Visible = false;
            // 
            // verificarImportesToolStripMenuItem1
            // 
            this.verificarImportesToolStripMenuItem1.Name = "verificarImportesToolStripMenuItem1";
            this.verificarImportesToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.verificarImportesToolStripMenuItem1.Text = "Verificar Importes";
            this.verificarImportesToolStripMenuItem1.Click += new System.EventHandler(this.verificarImportesToolStripMenuItem1_Click);
            // 
            // cargarEfectosRemesaToolStripMenuItem
            // 
            this.cargarEfectosRemesaToolStripMenuItem.Name = "cargarEfectosRemesaToolStripMenuItem";
            this.cargarEfectosRemesaToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.cargarEfectosRemesaToolStripMenuItem.Text = "Cargar Efectos Remesa";
            this.cargarEfectosRemesaToolStripMenuItem.Click += new System.EventHandler(this.cargarEfectosRemesaToolStripMenuItem_Click);
            // 
            // verificarIDsSincronizaciónToolStripMenuItem
            // 
            this.verificarIDsSincronizaciónToolStripMenuItem.Name = "verificarIDsSincronizaciónToolStripMenuItem";
            this.verificarIDsSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.verificarIDsSincronizaciónToolStripMenuItem.Text = "Verificar IDs Sincronización";
            this.verificarIDsSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.verificarIDsSincronizaciónToolStripMenuItem_Click);
            // 
            // borrarRemesaEnA3ERPToolStripMenuItem
            // 
            this.borrarRemesaEnA3ERPToolStripMenuItem.Name = "borrarRemesaEnA3ERPToolStripMenuItem";
            this.borrarRemesaEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.borrarRemesaEnA3ERPToolStripMenuItem.Text = "Borrar Remesa en A3ERP";
            this.borrarRemesaEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.borrarRemesaEnA3ERPToolStripMenuItem_Click);
            // 
            // a3ERPRPSTToolStripMenuItem
            // 
            this.a3ERPRPSTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearCuentaEnRepasatToolStripMenuItem,
            this.validarFacturaToolStripMenuItem});
            this.a3ERPRPSTToolStripMenuItem.Name = "a3ERPRPSTToolStripMenuItem";
            this.a3ERPRPSTToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.a3ERPRPSTToolStripMenuItem.Text = "A3ERP >>> RPST";
            this.a3ERPRPSTToolStripMenuItem.Visible = false;
            // 
            // crearCuentaEnRepasatToolStripMenuItem
            // 
            this.crearCuentaEnRepasatToolStripMenuItem.Name = "crearCuentaEnRepasatToolStripMenuItem";
            this.crearCuentaEnRepasatToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.crearCuentaEnRepasatToolStripMenuItem.Text = "Crear Cuenta en Repasat";
            this.crearCuentaEnRepasatToolStripMenuItem.Click += new System.EventHandler(this.crearCuentaEnRepasatToolStripMenuItem_Click);
            // 
            // validarFacturaToolStripMenuItem
            // 
            this.validarFacturaToolStripMenuItem.Name = "validarFacturaToolStripMenuItem";
            this.validarFacturaToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.validarFacturaToolStripMenuItem.Text = "Validar Factura";
            this.validarFacturaToolStripMenuItem.Click += new System.EventHandler(this.validarFacturaToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(183, 6);
            // 
            // sincronizarToolStripMenuItem
            // 
            this.sincronizarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.síToolStripMenuItem,
            this.noToolStripMenuItem});
            this.sincronizarToolStripMenuItem.Name = "sincronizarToolStripMenuItem";
            this.sincronizarToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.sincronizarToolStripMenuItem.Text = "&Sincronizar";
            // 
            // síToolStripMenuItem
            // 
            this.síToolStripMenuItem.Name = "síToolStripMenuItem";
            this.síToolStripMenuItem.Size = new System.Drawing.Size(90, 22);
            this.síToolStripMenuItem.Text = "&Sí";
            this.síToolStripMenuItem.Click += new System.EventHandler(this.síToolStripMenuItem_Click);
            // 
            // noToolStripMenuItem
            // 
            this.noToolStripMenuItem.Name = "noToolStripMenuItem";
            this.noToolStripMenuItem.Size = new System.Drawing.Size(90, 22);
            this.noToolStripMenuItem.Text = "&No";
            this.noToolStripMenuItem.Click += new System.EventHandler(this.noToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // ticketBaiToolStripMenuItem
            // 
            this.ticketBaiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarDocsDesdeA3ToolStripMenuItem});
            this.ticketBaiToolStripMenuItem.Name = "ticketBaiToolStripMenuItem";
            this.ticketBaiToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.ticketBaiToolStripMenuItem.Text = "Ticket Bai";
            this.ticketBaiToolStripMenuItem.Visible = false;
            // 
            // actualizarDocsDesdeA3ToolStripMenuItem
            // 
            this.actualizarDocsDesdeA3ToolStripMenuItem.Name = "actualizarDocsDesdeA3ToolStripMenuItem";
            this.actualizarDocsDesdeA3ToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.actualizarDocsDesdeA3ToolStripMenuItem.Text = "Actualizar Docs Desde A3";
            this.actualizarDocsDesdeA3ToolStripMenuItem.Click += new System.EventHandler(this.actualizarDocsDesdeA3ToolStripMenuItem_Click);
            // 
            // cmsProductos
            // 
            this.cmsProductos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsProductos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.cmsProductos.Name = "contextClientes";
            this.cmsProductos.ShowItemToolTips = false;
            this.cmsProductos.Size = new System.Drawing.Size(192, 30);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(191, 26);
            this.toolStripMenuItem1.Text = "Subir productos";
            // 
            // tabAuxiliares
            // 
            this.tabAuxiliares.Controls.Add(this.splitContainer3);
            this.tabAuxiliares.Location = new System.Drawing.Point(4, 29);
            this.tabAuxiliares.Name = "tabAuxiliares";
            this.tabAuxiliares.Size = new System.Drawing.Size(1374, 719);
            this.tabAuxiliares.TabIndex = 6;
            this.tabAuxiliares.Text = "Auxiliares";
            this.tabAuxiliares.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.dgvRepasatData);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(1374, 719);
            this.splitContainer3.SplitterDistance = 469;
            this.splitContainer3.TabIndex = 0;
            // 
            // dgvRepasatData
            // 
            this.dgvRepasatData.AllowDrop = true;
            this.dgvRepasatData.AllowUserToAddRows = false;
            this.dgvRepasatData.AllowUserToDeleteRows = false;
            this.dgvRepasatData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvRepasatData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRepasatData.ContextMenuStrip = this.ctexMenuAuxiliars;
            this.dgvRepasatData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRepasatData.Location = new System.Drawing.Point(0, 0);
            this.dgvRepasatData.Name = "dgvRepasatData";
            this.dgvRepasatData.ReadOnly = true;
            this.dgvRepasatData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRepasatData.Size = new System.Drawing.Size(469, 719);
            this.dgvRepasatData.TabIndex = 0;
            this.dgvRepasatData.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvRepasatData_DragDrop);
            this.dgvRepasatData.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvRepasatData_DragEnter);
            this.dgvRepasatData.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvRepasatData_DragOver);
            this.dgvRepasatData.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvRepasatData_MouseDown);
            this.dgvRepasatData.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvRepasatData_MouseMove);
            // 
            // ctexMenuAuxiliars
            // 
            this.ctexMenuAuxiliars.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarCodigosEnRepasatToolStripMenuItem,
            this.borrarSincronizaciónEnRepasatToolStripMenuItem});
            this.ctexMenuAuxiliars.Name = "ctexMenuAuxiliars";
            this.ctexMenuAuxiliars.Size = new System.Drawing.Size(247, 48);
            // 
            // verificarCodigosEnRepasatToolStripMenuItem
            // 
            this.verificarCodigosEnRepasatToolStripMenuItem.Name = "verificarCodigosEnRepasatToolStripMenuItem";
            this.verificarCodigosEnRepasatToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.verificarCodigosEnRepasatToolStripMenuItem.Text = "Verificar Codigos en &Repasat";
            this.verificarCodigosEnRepasatToolStripMenuItem.Click += new System.EventHandler(this.verificarCodigosEnRepasatToolStripMenuItem_Click);
            // 
            // borrarSincronizaciónEnRepasatToolStripMenuItem
            // 
            this.borrarSincronizaciónEnRepasatToolStripMenuItem.Name = "borrarSincronizaciónEnRepasatToolStripMenuItem";
            this.borrarSincronizaciónEnRepasatToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.borrarSincronizaciónEnRepasatToolStripMenuItem.Text = "Borrar Sincronización en Repasat";
            this.borrarSincronizaciónEnRepasatToolStripMenuItem.Click += new System.EventHandler(this.borrarSincronizaciónEnRepasatToolStripMenuItem_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.AutoScroll = true;
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearTipoArtA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadTipoArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnTipoArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearMarcasArtA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadMarcasArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnMarcasArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearSubfamiliaArtA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadSubfamiliaArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnSubfamiliaArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearFamiliaArtA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadFamiliaArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnFamiliaArt);
            this.splitContainer4.Panel1.Controls.Add(this.btnAuxiliaresToRPST);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearAlmacenA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearZonasA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearRutasA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearTranspotA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearDocsPagoA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearSeriesA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadCentrosCoste);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearCentrosCosteA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnCostCenters);
            this.splitContainer4.Panel1.Controls.Add(this.button2);
            this.splitContainer4.Panel1.Controls.Add(this.btnCrearProyectosA3);
            this.splitContainer4.Panel1.Controls.Add(this.btnBanks);
            this.splitContainer4.Panel1.Controls.Add(this.btnProyectos);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadCuentasContables);
            this.splitContainer4.Panel1.Controls.Add(this.btnPlanContable);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadTipoImpuestos);
            this.splitContainer4.Panel1.Controls.Add(this.btnTiposImpuestos);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadAlmacenes);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadSeries);
            this.splitContainer4.Panel1.Controls.Add(this.btnAlmacenes);
            this.splitContainer4.Panel1.Controls.Add(this.btnSeries);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadRegimenes);
            this.splitContainer4.Panel1.Controls.Add(this.btnRegImpuestos);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadZonas);
            this.splitContainer4.Panel1.Controls.Add(this.btnDocsPago);
            this.splitContainer4.Panel1.Controls.Add(this.btnMetPago);
            this.splitContainer4.Panel1.Controls.Add(this.btnRepresen);
            this.splitContainer4.Panel1.Controls.Add(this.button1);
            this.splitContainer4.Panel1.Controls.Add(this.btnUploadRutas);
            this.splitContainer4.Panel1.Controls.Add(this.btnZonas);
            this.splitContainer4.Panel1.Controls.Add(this.btnRutas);
            this.splitContainer4.Panel1.Controls.Add(this.btnRepresentantes);
            this.splitContainer4.Panel1.Controls.Add(this.picboxRepasat);
            this.splitContainer4.Panel1.Controls.Add(this.picboxA3ERP);
            this.splitContainer4.Panel1.Controls.Add(this.btPaymentDocs);
            this.splitContainer4.Panel1.Controls.Add(this.btnCarriers);
            this.splitContainer4.Panel1.Controls.Add(this.btnPaymentMethods);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.dgvA3ERPData);
            this.splitContainer4.Size = new System.Drawing.Size(901, 719);
            this.splitContainer4.SplitterDistance = 360;
            this.splitContainer4.TabIndex = 0;
            // 
            // btnCrearTipoArtA3
            // 
            this.btnCrearTipoArtA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearTipoArtA3.Enabled = false;
            this.btnCrearTipoArtA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearTipoArtA3.Location = new System.Drawing.Point(274, 415);
            this.btnCrearTipoArtA3.Name = "btnCrearTipoArtA3";
            this.btnCrearTipoArtA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearTipoArtA3.TabIndex = 49;
            this.btnCrearTipoArtA3.Text = ">>";
            this.btnCrearTipoArtA3.UseVisualStyleBackColor = true;
            this.btnCrearTipoArtA3.Click += new System.EventHandler(this.btnCrearTipoArtA3_Click);
            // 
            // btnUploadTipoArt
            // 
            this.btnUploadTipoArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadTipoArt.Location = new System.Drawing.Point(14, 415);
            this.btnUploadTipoArt.Name = "btnUploadTipoArt";
            this.btnUploadTipoArt.Size = new System.Drawing.Size(39, 27);
            this.btnUploadTipoArt.TabIndex = 48;
            this.btnUploadTipoArt.Text = "<<";
            this.btnUploadTipoArt.UseVisualStyleBackColor = true;
            this.btnUploadTipoArt.Click += new System.EventHandler(this.btnUploadTipoArt_Click);
            // 
            // btnTipoArt
            // 
            this.btnTipoArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTipoArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoArt.Location = new System.Drawing.Point(59, 415);
            this.btnTipoArt.Name = "btnTipoArt";
            this.btnTipoArt.Size = new System.Drawing.Size(209, 27);
            this.btnTipoArt.TabIndex = 47;
            this.btnTipoArt.Text = "Tipo Art";
            this.btnTipoArt.UseVisualStyleBackColor = true;
            this.btnTipoArt.Click += new System.EventHandler(this.btnTipoArt_Click);
            // 
            // btnCrearMarcasArtA3
            // 
            this.btnCrearMarcasArtA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearMarcasArtA3.Enabled = false;
            this.btnCrearMarcasArtA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearMarcasArtA3.Location = new System.Drawing.Point(274, 382);
            this.btnCrearMarcasArtA3.Name = "btnCrearMarcasArtA3";
            this.btnCrearMarcasArtA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearMarcasArtA3.TabIndex = 46;
            this.btnCrearMarcasArtA3.Text = ">>";
            this.btnCrearMarcasArtA3.UseVisualStyleBackColor = true;
            // 
            // btnUploadMarcasArt
            // 
            this.btnUploadMarcasArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadMarcasArt.Location = new System.Drawing.Point(14, 382);
            this.btnUploadMarcasArt.Name = "btnUploadMarcasArt";
            this.btnUploadMarcasArt.Size = new System.Drawing.Size(39, 27);
            this.btnUploadMarcasArt.TabIndex = 45;
            this.btnUploadMarcasArt.Text = "<<";
            this.btnUploadMarcasArt.UseVisualStyleBackColor = true;
            this.btnUploadMarcasArt.Click += new System.EventHandler(this.btnUploadMarcasArt_Click);
            // 
            // btnMarcasArt
            // 
            this.btnMarcasArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMarcasArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcasArt.Location = new System.Drawing.Point(59, 382);
            this.btnMarcasArt.Name = "btnMarcasArt";
            this.btnMarcasArt.Size = new System.Drawing.Size(209, 27);
            this.btnMarcasArt.TabIndex = 44;
            this.btnMarcasArt.Text = "Marcas Art";
            this.btnMarcasArt.UseVisualStyleBackColor = true;
            this.btnMarcasArt.Click += new System.EventHandler(this.btnMarcaArt_Click);
            // 
            // btnCrearSubfamiliaArtA3
            // 
            this.btnCrearSubfamiliaArtA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearSubfamiliaArtA3.Enabled = false;
            this.btnCrearSubfamiliaArtA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearSubfamiliaArtA3.Location = new System.Drawing.Point(274, 349);
            this.btnCrearSubfamiliaArtA3.Name = "btnCrearSubfamiliaArtA3";
            this.btnCrearSubfamiliaArtA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearSubfamiliaArtA3.TabIndex = 43;
            this.btnCrearSubfamiliaArtA3.Text = ">>";
            this.btnCrearSubfamiliaArtA3.UseVisualStyleBackColor = true;
            // 
            // btnUploadSubfamiliaArt
            // 
            this.btnUploadSubfamiliaArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadSubfamiliaArt.Location = new System.Drawing.Point(14, 349);
            this.btnUploadSubfamiliaArt.Name = "btnUploadSubfamiliaArt";
            this.btnUploadSubfamiliaArt.Size = new System.Drawing.Size(39, 27);
            this.btnUploadSubfamiliaArt.TabIndex = 42;
            this.btnUploadSubfamiliaArt.Text = "<<";
            this.btnUploadSubfamiliaArt.UseVisualStyleBackColor = true;
            this.btnUploadSubfamiliaArt.Click += new System.EventHandler(this.btnUploadSubfamiliaArt_Click);
            // 
            // btnSubfamiliaArt
            // 
            this.btnSubfamiliaArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubfamiliaArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubfamiliaArt.Location = new System.Drawing.Point(59, 349);
            this.btnSubfamiliaArt.Name = "btnSubfamiliaArt";
            this.btnSubfamiliaArt.Size = new System.Drawing.Size(209, 27);
            this.btnSubfamiliaArt.TabIndex = 41;
            this.btnSubfamiliaArt.Text = "Subfamilia Art";
            this.btnSubfamiliaArt.UseVisualStyleBackColor = true;
            this.btnSubfamiliaArt.Click += new System.EventHandler(this.btnSubfamiliaArt_Click);
            // 
            // btnCrearFamiliaArtA3
            // 
            this.btnCrearFamiliaArtA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearFamiliaArtA3.Enabled = false;
            this.btnCrearFamiliaArtA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearFamiliaArtA3.Location = new System.Drawing.Point(274, 315);
            this.btnCrearFamiliaArtA3.Name = "btnCrearFamiliaArtA3";
            this.btnCrearFamiliaArtA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearFamiliaArtA3.TabIndex = 40;
            this.btnCrearFamiliaArtA3.Text = ">>";
            this.btnCrearFamiliaArtA3.UseVisualStyleBackColor = true;
            this.btnCrearFamiliaArtA3.Click += new System.EventHandler(this.btnCrearFamiliaArtA3_Click);
            // 
            // btnUploadFamiliaArt
            // 
            this.btnUploadFamiliaArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadFamiliaArt.Location = new System.Drawing.Point(14, 315);
            this.btnUploadFamiliaArt.Name = "btnUploadFamiliaArt";
            this.btnUploadFamiliaArt.Size = new System.Drawing.Size(39, 27);
            this.btnUploadFamiliaArt.TabIndex = 39;
            this.btnUploadFamiliaArt.Text = "<<";
            this.btnUploadFamiliaArt.UseVisualStyleBackColor = true;
            this.btnUploadFamiliaArt.Click += new System.EventHandler(this.btnUploadFamiliaArt_Click);
            // 
            // btnFamiliaArt
            // 
            this.btnFamiliaArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFamiliaArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFamiliaArt.Location = new System.Drawing.Point(59, 315);
            this.btnFamiliaArt.Name = "btnFamiliaArt";
            this.btnFamiliaArt.Size = new System.Drawing.Size(209, 27);
            this.btnFamiliaArt.TabIndex = 38;
            this.btnFamiliaArt.Text = "Familia Art";
            this.btnFamiliaArt.UseVisualStyleBackColor = true;
            this.btnFamiliaArt.Click += new System.EventHandler(this.btnFamiliaArt_Click);
            // 
            // btnAuxiliaresToRPST
            // 
            this.btnAuxiliaresToRPST.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuxiliaresToRPST.Location = new System.Drawing.Point(74, 5);
            this.btnAuxiliaresToRPST.Name = "btnAuxiliaresToRPST";
            this.btnAuxiliaresToRPST.Size = new System.Drawing.Size(44, 31);
            this.btnAuxiliaresToRPST.TabIndex = 37;
            this.btnAuxiliaresToRPST.Text = "<<<";
            this.btnAuxiliaresToRPST.UseVisualStyleBackColor = true;
            this.btnAuxiliaresToRPST.Click += new System.EventHandler(this.btnAuxiliaresToRPST_Click);
            // 
            // btnCrearAlmacenA3
            // 
            this.btnCrearAlmacenA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearAlmacenA3.Enabled = false;
            this.btnCrearAlmacenA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearAlmacenA3.Location = new System.Drawing.Point(275, 280);
            this.btnCrearAlmacenA3.Name = "btnCrearAlmacenA3";
            this.btnCrearAlmacenA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearAlmacenA3.TabIndex = 36;
            this.btnCrearAlmacenA3.Text = ">>";
            this.btnCrearAlmacenA3.UseVisualStyleBackColor = true;
            this.btnCrearAlmacenA3.Click += new System.EventHandler(this.btnCrearAlmacenA3_Click);
            // 
            // btnCrearZonasA3
            // 
            this.btnCrearZonasA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearZonasA3.Enabled = false;
            this.btnCrearZonasA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearZonasA3.Location = new System.Drawing.Point(275, 214);
            this.btnCrearZonasA3.Name = "btnCrearZonasA3";
            this.btnCrearZonasA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearZonasA3.TabIndex = 35;
            this.btnCrearZonasA3.Text = ">>";
            this.btnCrearZonasA3.UseVisualStyleBackColor = true;
            this.btnCrearZonasA3.Click += new System.EventHandler(this.btnCrearZonasA3_Click);
            // 
            // btnCrearRutasA3
            // 
            this.btnCrearRutasA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearRutasA3.Enabled = false;
            this.btnCrearRutasA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearRutasA3.Location = new System.Drawing.Point(275, 181);
            this.btnCrearRutasA3.Name = "btnCrearRutasA3";
            this.btnCrearRutasA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearRutasA3.TabIndex = 34;
            this.btnCrearRutasA3.Text = ">>";
            this.btnCrearRutasA3.UseVisualStyleBackColor = true;
            this.btnCrearRutasA3.Click += new System.EventHandler(this.btnCrearRutasA3_Click);
            // 
            // btnCrearTranspotA3
            // 
            this.btnCrearTranspotA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearTranspotA3.Enabled = false;
            this.btnCrearTranspotA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearTranspotA3.Location = new System.Drawing.Point(275, 115);
            this.btnCrearTranspotA3.Name = "btnCrearTranspotA3";
            this.btnCrearTranspotA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearTranspotA3.TabIndex = 33;
            this.btnCrearTranspotA3.Text = ">>";
            this.btnCrearTranspotA3.UseVisualStyleBackColor = true;
            this.btnCrearTranspotA3.Click += new System.EventHandler(this.btnCrearTranspotA3_Click);
            // 
            // btnCrearDocsPagoA3
            // 
            this.btnCrearDocsPagoA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearDocsPagoA3.Enabled = false;
            this.btnCrearDocsPagoA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearDocsPagoA3.Location = new System.Drawing.Point(275, 51);
            this.btnCrearDocsPagoA3.Name = "btnCrearDocsPagoA3";
            this.btnCrearDocsPagoA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearDocsPagoA3.TabIndex = 32;
            this.btnCrearDocsPagoA3.Text = ">>";
            this.btnCrearDocsPagoA3.UseVisualStyleBackColor = true;
            this.btnCrearDocsPagoA3.Click += new System.EventHandler(this.btnCrearDocsPagoA3_Click);
            // 
            // btnCrearSeriesA3
            // 
            this.btnCrearSeriesA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearSeriesA3.Enabled = false;
            this.btnCrearSeriesA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearSeriesA3.Location = new System.Drawing.Point(275, 247);
            this.btnCrearSeriesA3.Name = "btnCrearSeriesA3";
            this.btnCrearSeriesA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearSeriesA3.TabIndex = 31;
            this.btnCrearSeriesA3.Text = ">>";
            this.btnCrearSeriesA3.UseVisualStyleBackColor = true;
            this.btnCrearSeriesA3.Click += new System.EventHandler(this.btnCrearSeriesA3_Click);
            // 
            // btnUploadCentrosCoste
            // 
            this.btnUploadCentrosCoste.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadCentrosCoste.Location = new System.Drawing.Point(15, 517);
            this.btnUploadCentrosCoste.Name = "btnUploadCentrosCoste";
            this.btnUploadCentrosCoste.Size = new System.Drawing.Size(39, 27);
            this.btnUploadCentrosCoste.TabIndex = 30;
            this.btnUploadCentrosCoste.Text = "<<";
            this.btnUploadCentrosCoste.UseVisualStyleBackColor = true;
            this.btnUploadCentrosCoste.Click += new System.EventHandler(this.btnUploadCentrosCoste_Click);
            // 
            // btnCrearCentrosCosteA3
            // 
            this.btnCrearCentrosCosteA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearCentrosCosteA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearCentrosCosteA3.Location = new System.Drawing.Point(275, 517);
            this.btnCrearCentrosCosteA3.Name = "btnCrearCentrosCosteA3";
            this.btnCrearCentrosCosteA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearCentrosCosteA3.TabIndex = 29;
            this.btnCrearCentrosCosteA3.Text = ">>";
            this.btnCrearCentrosCosteA3.UseVisualStyleBackColor = true;
            this.btnCrearCentrosCosteA3.Click += new System.EventHandler(this.btnCrearCentrosCosteA3_Click);
            // 
            // btnCostCenters
            // 
            this.btnCostCenters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCostCenters.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCostCenters.Location = new System.Drawing.Point(60, 517);
            this.btnCostCenters.Name = "btnCostCenters";
            this.btnCostCenters.Size = new System.Drawing.Size(209, 27);
            this.btnCostCenters.TabIndex = 28;
            this.btnCostCenters.Text = "Centros de Coste";
            this.btnCostCenters.UseVisualStyleBackColor = true;
            this.btnCostCenters.Click += new System.EventHandler(this.btnCostCenters_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(59, 669);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(209, 37);
            this.button2.TabIndex = 27;
            this.button2.Text = "Retenciones";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnCrearProyectosA3
            // 
            this.btnCrearProyectosA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearProyectosA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearProyectosA3.Location = new System.Drawing.Point(275, 484);
            this.btnCrearProyectosA3.Name = "btnCrearProyectosA3";
            this.btnCrearProyectosA3.Size = new System.Drawing.Size(39, 27);
            this.btnCrearProyectosA3.TabIndex = 26;
            this.btnCrearProyectosA3.Text = ">>";
            this.btnCrearProyectosA3.UseVisualStyleBackColor = true;
            this.btnCrearProyectosA3.Click += new System.EventHandler(this.btnCrearProyectosA3_Click);
            // 
            // btnBanks
            // 
            this.btnBanks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBanks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanks.Location = new System.Drawing.Point(60, 550);
            this.btnBanks.Name = "btnBanks";
            this.btnBanks.Size = new System.Drawing.Size(209, 27);
            this.btnBanks.TabIndex = 25;
            this.btnBanks.Text = "Bancos";
            this.btnBanks.UseVisualStyleBackColor = true;
            this.btnBanks.Click += new System.EventHandler(this.btnBanks_Click);
            // 
            // btnProyectos
            // 
            this.btnProyectos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProyectos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProyectos.Location = new System.Drawing.Point(60, 484);
            this.btnProyectos.Name = "btnProyectos";
            this.btnProyectos.Size = new System.Drawing.Size(209, 27);
            this.btnProyectos.TabIndex = 24;
            this.btnProyectos.Text = "Proyectos";
            this.btnProyectos.UseVisualStyleBackColor = true;
            this.btnProyectos.Click += new System.EventHandler(this.btnProyectos_Click);
            // 
            // btnUploadCuentasContables
            // 
            this.btnUploadCuentasContables.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadCuentasContables.Location = new System.Drawing.Point(15, 451);
            this.btnUploadCuentasContables.Name = "btnUploadCuentasContables";
            this.btnUploadCuentasContables.Size = new System.Drawing.Size(39, 27);
            this.btnUploadCuentasContables.TabIndex = 23;
            this.btnUploadCuentasContables.Text = "<<";
            this.btnUploadCuentasContables.UseVisualStyleBackColor = true;
            this.btnUploadCuentasContables.Click += new System.EventHandler(this.btnUploadCuentasContables_Click);
            // 
            // btnPlanContable
            // 
            this.btnPlanContable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlanContable.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlanContable.Location = new System.Drawing.Point(60, 451);
            this.btnPlanContable.Name = "btnPlanContable";
            this.btnPlanContable.Size = new System.Drawing.Size(209, 27);
            this.btnPlanContable.TabIndex = 22;
            this.btnPlanContable.Text = "Cuentas Contables";
            this.btnPlanContable.UseVisualStyleBackColor = true;
            this.btnPlanContable.Click += new System.EventHandler(this.btnPlanContable_Click);
            // 
            // btnUploadTipoImpuestos
            // 
            this.btnUploadTipoImpuestos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUploadTipoImpuestos.Location = new System.Drawing.Point(274, 626);
            this.btnUploadTipoImpuestos.Name = "btnUploadTipoImpuestos";
            this.btnUploadTipoImpuestos.Size = new System.Drawing.Size(39, 37);
            this.btnUploadTipoImpuestos.TabIndex = 21;
            this.btnUploadTipoImpuestos.Text = ">>";
            this.btnUploadTipoImpuestos.UseVisualStyleBackColor = true;
            // 
            // btnTiposImpuestos
            // 
            this.btnTiposImpuestos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTiposImpuestos.Location = new System.Drawing.Point(59, 626);
            this.btnTiposImpuestos.Name = "btnTiposImpuestos";
            this.btnTiposImpuestos.Size = new System.Drawing.Size(209, 37);
            this.btnTiposImpuestos.TabIndex = 20;
            this.btnTiposImpuestos.Text = "Tipos de Impuestos";
            this.btnTiposImpuestos.UseVisualStyleBackColor = true;
            this.btnTiposImpuestos.Click += new System.EventHandler(this.btnTiposImpuestos_Click);
            // 
            // btnUploadAlmacenes
            // 
            this.btnUploadAlmacenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadAlmacenes.Location = new System.Drawing.Point(15, 280);
            this.btnUploadAlmacenes.Name = "btnUploadAlmacenes";
            this.btnUploadAlmacenes.Size = new System.Drawing.Size(39, 27);
            this.btnUploadAlmacenes.TabIndex = 19;
            this.btnUploadAlmacenes.Text = "<<";
            this.btnUploadAlmacenes.UseVisualStyleBackColor = true;
            // 
            // btnUploadSeries
            // 
            this.btnUploadSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadSeries.Location = new System.Drawing.Point(15, 247);
            this.btnUploadSeries.Name = "btnUploadSeries";
            this.btnUploadSeries.Size = new System.Drawing.Size(39, 27);
            this.btnUploadSeries.TabIndex = 18;
            this.btnUploadSeries.Text = "<<";
            this.btnUploadSeries.UseVisualStyleBackColor = true;
            this.btnUploadSeries.Click += new System.EventHandler(this.btnUploadSeries_Click);
            // 
            // btnAlmacenes
            // 
            this.btnAlmacenes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlmacenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlmacenes.Location = new System.Drawing.Point(60, 280);
            this.btnAlmacenes.Name = "btnAlmacenes";
            this.btnAlmacenes.Size = new System.Drawing.Size(209, 27);
            this.btnAlmacenes.TabIndex = 17;
            this.btnAlmacenes.Text = "Almacenes";
            this.btnAlmacenes.UseVisualStyleBackColor = true;
            this.btnAlmacenes.Click += new System.EventHandler(this.btnAlmacenes_Click);
            // 
            // btnSeries
            // 
            this.btnSeries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeries.Location = new System.Drawing.Point(60, 247);
            this.btnSeries.Name = "btnSeries";
            this.btnSeries.Size = new System.Drawing.Size(209, 27);
            this.btnSeries.TabIndex = 16;
            this.btnSeries.Text = "Series";
            this.btnSeries.UseVisualStyleBackColor = true;
            this.btnSeries.Click += new System.EventHandler(this.btnSeries_Click);
            // 
            // btnUploadRegimenes
            // 
            this.btnUploadRegimenes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUploadRegimenes.Location = new System.Drawing.Point(274, 583);
            this.btnUploadRegimenes.Name = "btnUploadRegimenes";
            this.btnUploadRegimenes.Size = new System.Drawing.Size(39, 37);
            this.btnUploadRegimenes.TabIndex = 15;
            this.btnUploadRegimenes.Text = ">>";
            this.btnUploadRegimenes.UseVisualStyleBackColor = true;
            // 
            // btnRegImpuestos
            // 
            this.btnRegImpuestos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegImpuestos.Location = new System.Drawing.Point(59, 583);
            this.btnRegImpuestos.Name = "btnRegImpuestos";
            this.btnRegImpuestos.Size = new System.Drawing.Size(209, 37);
            this.btnRegImpuestos.TabIndex = 14;
            this.btnRegImpuestos.Text = "Regimen Impuestos";
            this.btnRegImpuestos.UseVisualStyleBackColor = true;
            this.btnRegImpuestos.Click += new System.EventHandler(this.btnRegImpuestos_Click);
            // 
            // btnUploadZonas
            // 
            this.btnUploadZonas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadZonas.Location = new System.Drawing.Point(15, 214);
            this.btnUploadZonas.Name = "btnUploadZonas";
            this.btnUploadZonas.Size = new System.Drawing.Size(39, 27);
            this.btnUploadZonas.TabIndex = 13;
            this.btnUploadZonas.Text = "<<";
            this.btnUploadZonas.UseVisualStyleBackColor = true;
            this.btnUploadZonas.Click += new System.EventHandler(this.btnUploadZonas_Click);
            // 
            // btnDocsPago
            // 
            this.btnDocsPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDocsPago.Location = new System.Drawing.Point(15, 51);
            this.btnDocsPago.Name = "btnDocsPago";
            this.btnDocsPago.Size = new System.Drawing.Size(39, 27);
            this.btnDocsPago.TabIndex = 12;
            this.btnDocsPago.Text = "<<";
            this.btnDocsPago.UseVisualStyleBackColor = true;
            this.btnDocsPago.Click += new System.EventHandler(this.btnDocsPago_Click);
            // 
            // btnMetPago
            // 
            this.btnMetPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMetPago.Location = new System.Drawing.Point(15, 82);
            this.btnMetPago.Name = "btnMetPago";
            this.btnMetPago.Size = new System.Drawing.Size(39, 27);
            this.btnMetPago.TabIndex = 11;
            this.btnMetPago.Text = "<<";
            this.btnMetPago.UseVisualStyleBackColor = true;
            this.btnMetPago.Click += new System.EventHandler(this.btnMetPago_Click);
            // 
            // btnRepresen
            // 
            this.btnRepresen.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepresen.Location = new System.Drawing.Point(15, 148);
            this.btnRepresen.Name = "btnRepresen";
            this.btnRepresen.Size = new System.Drawing.Size(39, 27);
            this.btnRepresen.TabIndex = 10;
            this.btnRepresen.Text = "<<";
            this.btnRepresen.UseVisualStyleBackColor = true;
            this.btnRepresen.Click += new System.EventHandler(this.btnRepresen_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(15, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 27);
            this.button1.TabIndex = 9;
            this.button1.Text = "<<";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnUploadRutas
            // 
            this.btnUploadRutas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadRutas.Location = new System.Drawing.Point(15, 181);
            this.btnUploadRutas.Name = "btnUploadRutas";
            this.btnUploadRutas.Size = new System.Drawing.Size(39, 27);
            this.btnUploadRutas.TabIndex = 8;
            this.btnUploadRutas.Text = "<<";
            this.btnUploadRutas.UseVisualStyleBackColor = true;
            this.btnUploadRutas.Click += new System.EventHandler(this.btnUploadRutas_Click);
            // 
            // btnZonas
            // 
            this.btnZonas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZonas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZonas.Location = new System.Drawing.Point(60, 214);
            this.btnZonas.Name = "btnZonas";
            this.btnZonas.Size = new System.Drawing.Size(209, 27);
            this.btnZonas.TabIndex = 7;
            this.btnZonas.Text = "Zonas Geográficas";
            this.btnZonas.UseVisualStyleBackColor = true;
            this.btnZonas.Click += new System.EventHandler(this.btnZonas_Click);
            // 
            // btnRutas
            // 
            this.btnRutas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRutas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRutas.Location = new System.Drawing.Point(60, 181);
            this.btnRutas.Name = "btnRutas";
            this.btnRutas.Size = new System.Drawing.Size(209, 27);
            this.btnRutas.TabIndex = 6;
            this.btnRutas.Text = "Rutas";
            this.btnRutas.UseVisualStyleBackColor = true;
            this.btnRutas.Click += new System.EventHandler(this.btnRutas_Click);
            // 
            // btnRepresentantes
            // 
            this.btnRepresentantes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRepresentantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepresentantes.Location = new System.Drawing.Point(60, 148);
            this.btnRepresentantes.Name = "btnRepresentantes";
            this.btnRepresentantes.Size = new System.Drawing.Size(209, 27);
            this.btnRepresentantes.TabIndex = 5;
            this.btnRepresentantes.Text = "Representantes / Trabajadores";
            this.btnRepresentantes.UseVisualStyleBackColor = true;
            this.btnRepresentantes.Click += new System.EventHandler(this.btnRepresentantes_Click);
            // 
            // picboxRepasat
            // 
            this.picboxRepasat.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picboxRepasat.BackgroundImage")));
            this.picboxRepasat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picboxRepasat.Location = new System.Drawing.Point(3, 5);
            this.picboxRepasat.Name = "picboxRepasat";
            this.picboxRepasat.Size = new System.Drawing.Size(65, 31);
            this.picboxRepasat.TabIndex = 4;
            this.picboxRepasat.TabStop = false;
            // 
            // picboxA3ERP
            // 
            this.picboxA3ERP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picboxA3ERP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picboxA3ERP.BackgroundImage")));
            this.picboxA3ERP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picboxA3ERP.Location = new System.Drawing.Point(261, 5);
            this.picboxA3ERP.Name = "picboxA3ERP";
            this.picboxA3ERP.Size = new System.Drawing.Size(62, 31);
            this.picboxA3ERP.TabIndex = 3;
            this.picboxA3ERP.TabStop = false;
            // 
            // btPaymentDocs
            // 
            this.btPaymentDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btPaymentDocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPaymentDocs.Location = new System.Drawing.Point(60, 51);
            this.btPaymentDocs.Name = "btPaymentDocs";
            this.btPaymentDocs.Size = new System.Drawing.Size(209, 27);
            this.btPaymentDocs.TabIndex = 0;
            this.btPaymentDocs.Text = "Documentos Pago";
            this.btPaymentDocs.UseVisualStyleBackColor = true;
            this.btPaymentDocs.Click += new System.EventHandler(this.btPaymentDocs_Click);
            // 
            // btnCarriers
            // 
            this.btnCarriers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCarriers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarriers.Location = new System.Drawing.Point(60, 115);
            this.btnCarriers.Name = "btnCarriers";
            this.btnCarriers.Size = new System.Drawing.Size(209, 27);
            this.btnCarriers.TabIndex = 2;
            this.btnCarriers.Text = "Transportistas";
            this.btnCarriers.UseVisualStyleBackColor = true;
            this.btnCarriers.Click += new System.EventHandler(this.btnCarriers_Click);
            // 
            // btnPaymentMethods
            // 
            this.btnPaymentMethods.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaymentMethods.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaymentMethods.Location = new System.Drawing.Point(60, 82);
            this.btnPaymentMethods.Name = "btnPaymentMethods";
            this.btnPaymentMethods.Size = new System.Drawing.Size(209, 27);
            this.btnPaymentMethods.TabIndex = 1;
            this.btnPaymentMethods.Text = "Métodos Pago (Plazos)";
            this.btnPaymentMethods.UseVisualStyleBackColor = true;
            this.btnPaymentMethods.Click += new System.EventHandler(this.btnPaymentMethods_Click);
            // 
            // dgvA3ERPData
            // 
            this.dgvA3ERPData.AllowDrop = true;
            this.dgvA3ERPData.AllowUserToAddRows = false;
            this.dgvA3ERPData.AllowUserToDeleteRows = false;
            this.dgvA3ERPData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvA3ERPData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvA3ERPData.ContextMenuStrip = this.cMenuAuxiliar;
            this.dgvA3ERPData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvA3ERPData.Location = new System.Drawing.Point(0, 0);
            this.dgvA3ERPData.Name = "dgvA3ERPData";
            this.dgvA3ERPData.ReadOnly = true;
            this.dgvA3ERPData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvA3ERPData.Size = new System.Drawing.Size(537, 719);
            this.dgvA3ERPData.TabIndex = 0;
            this.dgvA3ERPData.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvA3ERPData_DragDrop);
            this.dgvA3ERPData.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvA3ERPData_DragEnter);
            this.dgvA3ERPData.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvA3ERPData_DragOver);
            this.dgvA3ERPData.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvA3ERPData_MouseDown);
            this.dgvA3ERPData.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvA3ERPData_MouseMove);
            // 
            // cMenuAuxiliar
            // 
            this.cMenuAuxiliar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarSincronizaciónToolStripMenuItem1});
            this.cMenuAuxiliar.Name = "cMenuAuxiliar";
            this.cMenuAuxiliar.Size = new System.Drawing.Size(186, 26);
            // 
            // borrarSincronizaciónToolStripMenuItem1
            // 
            this.borrarSincronizaciónToolStripMenuItem1.Name = "borrarSincronizaciónToolStripMenuItem1";
            this.borrarSincronizaciónToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.borrarSincronizaciónToolStripMenuItem1.Text = "Borrar sincronización";
            this.borrarSincronizaciónToolStripMenuItem1.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem1_Click);
            // 
            // tabMasterFiles
            // 
            this.tabMasterFiles.Controls.Add(this.splitContainer1);
            this.tabMasterFiles.Location = new System.Drawing.Point(4, 29);
            this.tabMasterFiles.Name = "tabMasterFiles";
            this.tabMasterFiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabMasterFiles.Size = new System.Drawing.Size(1374, 719);
            this.tabMasterFiles.TabIndex = 0;
            this.tabMasterFiles.Text = "Ficheros";
            this.tabMasterFiles.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvAccountsRepasat);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer1.Size = new System.Drawing.Size(1368, 713);
            this.splitContainer1.SplitterDistance = 452;
            this.splitContainer1.TabIndex = 3;
            // 
            // dgvAccountsRepasat
            // 
            this.dgvAccountsRepasat.AllowDrop = true;
            this.dgvAccountsRepasat.AllowUserToAddRows = false;
            this.dgvAccountsRepasat.AllowUserToDeleteRows = false;
            this.dgvAccountsRepasat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAccountsRepasat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccountsRepasat.ContextMenuStrip = this.ctexMenuMaster;
            this.dgvAccountsRepasat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAccountsRepasat.Location = new System.Drawing.Point(0, 0);
            this.dgvAccountsRepasat.Name = "dgvAccountsRepasat";
            this.dgvAccountsRepasat.ReadOnly = true;
            this.dgvAccountsRepasat.Size = new System.Drawing.Size(452, 713);
            this.dgvAccountsRepasat.TabIndex = 0;
            this.dgvAccountsRepasat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountsRepasat_CellClick);
            this.dgvAccountsRepasat.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvAccountsRepasat_CellFormatting);
            this.dgvAccountsRepasat.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvAccountsRepasat_DragDrop);
            this.dgvAccountsRepasat.DragOver += new System.Windows.Forms.DragEventHandler(this.dgvAccountsRepasat_DragOver);
            // 
            // ctexMenuMaster
            // 
            this.ctexMenuMaster.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarSincronizaciónToolStripMenuItem2,
            this.actualizarEnA3ERPSelecciónToolStripMenuItem,
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem,
            this.articulosToolStripMenuItem1,
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem,
            this.sincronizarToolStripMenuItem1,
            this.sincronizarHaciaA3ERPToolStripMenuItem,
            this.validarEnlacesToolStripMenuItem});
            this.ctexMenuMaster.Name = "ctexMenuMaster";
            this.ctexMenuMaster.Size = new System.Drawing.Size(301, 180);
            this.ctexMenuMaster.Opening += new System.ComponentModel.CancelEventHandler(this.ctexMenuMaster_Opening);
            // 
            // borrarSincronizaciónToolStripMenuItem2
            // 
            this.borrarSincronizaciónToolStripMenuItem2.Name = "borrarSincronizaciónToolStripMenuItem2";
            this.borrarSincronizaciónToolStripMenuItem2.Size = new System.Drawing.Size(300, 22);
            this.borrarSincronizaciónToolStripMenuItem2.Text = "Borrar Sincronización";
            this.borrarSincronizaciónToolStripMenuItem2.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem2_Click);
            // 
            // actualizarEnA3ERPSelecciónToolStripMenuItem
            // 
            this.actualizarEnA3ERPSelecciónToolStripMenuItem.Name = "actualizarEnA3ERPSelecciónToolStripMenuItem";
            this.actualizarEnA3ERPSelecciónToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.actualizarEnA3ERPSelecciónToolStripMenuItem.Text = "Actualizar en A3ERP Selección";
            this.actualizarEnA3ERPSelecciónToolStripMenuItem.Click += new System.EventHandler(this.actualizarEnA3ERPSelecciónToolStripMenuItem_Click);
            // 
            // forzarTraspasoConCódigoClienteInformadoToolStripMenuItem
            // 
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem.Name = "forzarTraspasoConCódigoClienteInformadoToolStripMenuItem";
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem.Text = "Forzar Traspaso con Código Informado";
            this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem.Click += new System.EventHandler(this.forzarTraspasoConCódigoClienteInformadoToolStripMenuItem_Click);
            // 
            // articulosToolStripMenuItem1
            // 
            this.articulosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetearCódigoExternoToolStripMenuItem,
            this.validarSincronizaciónToolStripMenuItem});
            this.articulosToolStripMenuItem1.Name = "articulosToolStripMenuItem1";
            this.articulosToolStripMenuItem1.Size = new System.Drawing.Size(300, 22);
            this.articulosToolStripMenuItem1.Text = "Articulos";
            // 
            // resetearCódigoExternoToolStripMenuItem
            // 
            this.resetearCódigoExternoToolStripMenuItem.Name = "resetearCódigoExternoToolStripMenuItem";
            this.resetearCódigoExternoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.resetearCódigoExternoToolStripMenuItem.Text = "Resetear Código Externo";
            this.resetearCódigoExternoToolStripMenuItem.Click += new System.EventHandler(this.resetearCódigoExternoToolStripMenuItem_Click);
            // 
            // validarSincronizaciónToolStripMenuItem
            // 
            this.validarSincronizaciónToolStripMenuItem.Name = "validarSincronizaciónToolStripMenuItem";
            this.validarSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.validarSincronizaciónToolStripMenuItem.Text = "Validar Sincronización";
            this.validarSincronizaciónToolStripMenuItem.ToolTipText = resources.GetString("validarSincronizaciónToolStripMenuItem.ToolTipText");
            this.validarSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.validarSincronizaciónToolStripMenuItem_Click);
            // 
            // enlaceDesdeRepasatA3ERPToolStripMenuItem
            // 
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem.Name = "enlaceDesdeRepasatA3ERPToolStripMenuItem";
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem.Text = "Enlazar Códigos desde Repasat >>> A3ERP";
            this.enlaceDesdeRepasatA3ERPToolStripMenuItem.Click += new System.EventHandler(this.enlaceDesdeRepasatA3ERPToolStripMenuItem_Click);
            // 
            // sincronizarToolStripMenuItem1
            // 
            this.sincronizarToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.siToolStripMenuItem,
            this.noToolStripMenuItem1});
            this.sincronizarToolStripMenuItem1.Name = "sincronizarToolStripMenuItem1";
            this.sincronizarToolStripMenuItem1.Size = new System.Drawing.Size(300, 22);
            this.sincronizarToolStripMenuItem1.Text = "&Sincronizar";
            // 
            // siToolStripMenuItem
            // 
            this.siToolStripMenuItem.Name = "siToolStripMenuItem";
            this.siToolStripMenuItem.Size = new System.Drawing.Size(90, 22);
            this.siToolStripMenuItem.Text = "&Si";
            this.siToolStripMenuItem.Click += new System.EventHandler(this.siToolStripMenuItem_Click);
            // 
            // noToolStripMenuItem1
            // 
            this.noToolStripMenuItem1.Name = "noToolStripMenuItem1";
            this.noToolStripMenuItem1.Size = new System.Drawing.Size(90, 22);
            this.noToolStripMenuItem1.Text = "&No";
            this.noToolStripMenuItem1.Click += new System.EventHandler(this.noToolStripMenuItem1_Click);
            // 
            // sincronizarHaciaA3ERPToolStripMenuItem
            // 
            this.sincronizarHaciaA3ERPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comercialesToolStripMenuItem1});
            this.sincronizarHaciaA3ERPToolStripMenuItem.Name = "sincronizarHaciaA3ERPToolStripMenuItem";
            this.sincronizarHaciaA3ERPToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.sincronizarHaciaA3ERPToolStripMenuItem.Text = "Sincronizar hacia A3ERP";
            // 
            // comercialesToolStripMenuItem1
            // 
            this.comercialesToolStripMenuItem1.Name = "comercialesToolStripMenuItem1";
            this.comercialesToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.comercialesToolStripMenuItem1.Text = "&Comerciales";
            this.comercialesToolStripMenuItem1.Click += new System.EventHandler(this.comercialesToolStripMenuItem1_Click);
            // 
            // validarEnlacesToolStripMenuItem
            // 
            this.validarEnlacesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.porCódigoExternoToolStripMenuItem,
            this.direccionesToolStripMenuItem,
            this.porCIFToolStripMenuItem});
            this.validarEnlacesToolStripMenuItem.Name = "validarEnlacesToolStripMenuItem";
            this.validarEnlacesToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.validarEnlacesToolStripMenuItem.Text = "Validar Enlaces";
            // 
            // porCódigoExternoToolStripMenuItem
            // 
            this.porCódigoExternoToolStripMenuItem.Name = "porCódigoExternoToolStripMenuItem";
            this.porCódigoExternoToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.porCódigoExternoToolStripMenuItem.Text = "por Código &Externo";
            this.porCódigoExternoToolStripMenuItem.Click += new System.EventHandler(this.porCódigoExternoToolStripMenuItem_Click);
            // 
            // direccionesToolStripMenuItem
            // 
            this.direccionesToolStripMenuItem.Name = "direccionesToolStripMenuItem";
            this.direccionesToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.direccionesToolStripMenuItem.Text = "&Direcciones";
            this.direccionesToolStripMenuItem.Click += new System.EventHandler(this.direccionesToolStripMenuItem_Click);
            // 
            // porCIFToolStripMenuItem
            // 
            this.porCIFToolStripMenuItem.Name = "porCIFToolStripMenuItem";
            this.porCIFToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.porCIFToolStripMenuItem.Text = "por CIF";
            this.porCIFToolStripMenuItem.Click += new System.EventHandler(this.porCIFToolStripMenuItem_Click);
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer5.Panel1.Controls.Add(this.grBoxAccounts);
            this.splitContainer5.Panel1.SizeChanged += new System.EventHandler(this.splitContainer5_Panel1_SizeChanged);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.dgvAccountsA3ERP);
            this.splitContainer5.Size = new System.Drawing.Size(912, 713);
            this.splitContainer5.SplitterDistance = 291;
            this.splitContainer5.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lblDays);
            this.groupBox2.Controls.Add(this.numUpdownMovDays);
            this.groupBox2.Controls.Add(this.rbtnSelectProductsMovs);
            this.groupBox2.Controls.Add(this.rbtnAllProducts);
            this.groupBox2.Controls.Add(this.btnSyncStock);
            this.groupBox2.Location = new System.Drawing.Point(20, 397);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 213);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Stock";
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(105, 160);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(38, 20);
            this.lblDays.TabIndex = 21;
            this.lblDays.Text = "días";
            // 
            // numUpdownMovDays
            // 
            this.numUpdownMovDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpdownMovDays.Location = new System.Drawing.Point(36, 154);
            this.numUpdownMovDays.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numUpdownMovDays.Name = "numUpdownMovDays";
            this.numUpdownMovDays.Size = new System.Drawing.Size(63, 31);
            this.numUpdownMovDays.TabIndex = 20;
            this.numUpdownMovDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numUpdownMovDays.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // rbtnSelectProductsMovs
            // 
            this.rbtnSelectProductsMovs.AutoSize = true;
            this.rbtnSelectProductsMovs.Location = new System.Drawing.Point(18, 124);
            this.rbtnSelectProductsMovs.Name = "rbtnSelectProductsMovs";
            this.rbtnSelectProductsMovs.Size = new System.Drawing.Size(202, 24);
            this.rbtnSelectProductsMovs.TabIndex = 19;
            this.rbtnSelectProductsMovs.Text = "Con Movimientos últimos";
            this.rbtnSelectProductsMovs.UseVisualStyleBackColor = true;
            // 
            // rbtnAllProducts
            // 
            this.rbtnAllProducts.AutoSize = true;
            this.rbtnAllProducts.Checked = true;
            this.rbtnAllProducts.Location = new System.Drawing.Point(18, 94);
            this.rbtnAllProducts.Name = "rbtnAllProducts";
            this.rbtnAllProducts.Size = new System.Drawing.Size(160, 24);
            this.rbtnAllProducts.TabIndex = 18;
            this.rbtnAllProducts.TabStop = true;
            this.rbtnAllProducts.Text = "Todos los Articulos";
            this.rbtnAllProducts.UseVisualStyleBackColor = true;
            // 
            // btnSyncStock
            // 
            this.btnSyncStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSyncStock.Image = ((System.Drawing.Image)(resources.GetObject("btnSyncStock.Image")));
            this.btnSyncStock.Location = new System.Drawing.Point(27, 38);
            this.btnSyncStock.Name = "btnSyncStock";
            this.btnSyncStock.Size = new System.Drawing.Size(179, 50);
            this.btnSyncStock.TabIndex = 17;
            this.btnSyncStock.Text = "<<< Actualizar Stock";
            this.btnSyncStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSyncStock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSyncStock.UseVisualStyleBackColor = true;
            this.btnSyncStock.Click += new System.EventHandler(this.btnSyncStock_Click);
            // 
            // grBoxAccounts
            // 
            this.grBoxAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grBoxAccounts.Controls.Add(this.btUpdateRepasatToA3ERP);
            this.grBoxAccounts.Controls.Add(this.btUpdateA3ERPToRepasat);
            this.grBoxAccounts.Controls.Add(this.lblUdadNegocio);
            this.grBoxAccounts.Controls.Add(this.grBoxActivos);
            this.grBoxAccounts.Controls.Add(this.groupBox5);
            this.grBoxAccounts.Controls.Add(this.groupBox3);
            this.grBoxAccounts.Controls.Add(this.cboxMaestros);
            this.grBoxAccounts.Controls.Add(this.btnRPSTAddress);
            this.grBoxAccounts.Controls.Add(this.tbAccountId);
            this.grBoxAccounts.Controls.Add(this.btnLoadAccountsRepasat);
            this.grBoxAccounts.Controls.Add(this.btnLoadAccountsERP);
            this.grBoxAccounts.Controls.Add(this.btSendCustomersRepasatToA3ERP);
            this.grBoxAccounts.Controls.Add(this.btSendCustomersA3ERPToRepasat);
            this.grBoxAccounts.Location = new System.Drawing.Point(20, 18);
            this.grBoxAccounts.Name = "grBoxAccounts";
            this.grBoxAccounts.Size = new System.Drawing.Size(242, 366);
            this.grBoxAccounts.TabIndex = 18;
            this.grBoxAccounts.TabStop = false;
            this.grBoxAccounts.Text = "Datos";
            // 
            // btUpdateRepasatToA3ERP
            // 
            this.btUpdateRepasatToA3ERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdateRepasatToA3ERP.Image = global::klsync.Properties.Resources.refresh_A3ERP_32;
            this.btUpdateRepasatToA3ERP.Location = new System.Drawing.Point(105, 140);
            this.btUpdateRepasatToA3ERP.Name = "btUpdateRepasatToA3ERP";
            this.btUpdateRepasatToA3ERP.Size = new System.Drawing.Size(83, 36);
            this.btUpdateRepasatToA3ERP.TabIndex = 23;
            this.btUpdateRepasatToA3ERP.UseVisualStyleBackColor = true;
            this.btUpdateRepasatToA3ERP.Click += new System.EventHandler(this.btUpdateRepasatToA3ERP_Click);
            // 
            // btUpdateA3ERPToRepasat
            // 
            this.btUpdateA3ERPToRepasat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btUpdateA3ERPToRepasat.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUpdateA3ERPToRepasat.Image = global::klsync.Properties.Resources.refresh_RPST_32;
            this.btUpdateA3ERPToRepasat.Location = new System.Drawing.Point(16, 140);
            this.btUpdateA3ERPToRepasat.Name = "btUpdateA3ERPToRepasat";
            this.btUpdateA3ERPToRepasat.Size = new System.Drawing.Size(83, 36);
            this.btUpdateA3ERPToRepasat.TabIndex = 22;
            this.btUpdateA3ERPToRepasat.UseVisualStyleBackColor = true;
            this.btUpdateA3ERPToRepasat.Click += new System.EventHandler(this.btUpdateA3ERPToRepasat_Click);
            // 
            // lblUdadNegocio
            // 
            this.lblUdadNegocio.AutoSize = true;
            this.lblUdadNegocio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUdadNegocio.Location = new System.Drawing.Point(121, 24);
            this.lblUdadNegocio.Name = "lblUdadNegocio";
            this.lblUdadNegocio.Size = new System.Drawing.Size(92, 15);
            this.lblUdadNegocio.TabIndex = 21;
            this.lblUdadNegocio.Text = "Udad. Negocio:";
            this.lblUdadNegocio.Visible = false;
            // 
            // grBoxActivos
            // 
            this.grBoxActivos.Controls.Add(this.rbActiveYes);
            this.grBoxActivos.Controls.Add(this.rbActiveAll);
            this.grBoxActivos.Controls.Add(this.rbActiveNo);
            this.grBoxActivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxActivos.Location = new System.Drawing.Point(18, 273);
            this.grBoxActivos.Name = "grBoxActivos";
            this.grBoxActivos.Size = new System.Drawing.Size(170, 42);
            this.grBoxActivos.TabIndex = 19;
            this.grBoxActivos.TabStop = false;
            this.grBoxActivos.Text = "Activos";
            this.grBoxActivos.Visible = false;
            // 
            // rbActiveYes
            // 
            this.rbActiveYes.AutoSize = true;
            this.rbActiveYes.Location = new System.Drawing.Point(18, 17);
            this.rbActiveYes.Name = "rbActiveYes";
            this.rbActiveYes.Size = new System.Drawing.Size(34, 17);
            this.rbActiveYes.TabIndex = 3;
            this.rbActiveYes.Text = "Si";
            this.rbActiveYes.UseVisualStyleBackColor = true;
            // 
            // rbActiveAll
            // 
            this.rbActiveAll.AutoSize = true;
            this.rbActiveAll.Checked = true;
            this.rbActiveAll.Location = new System.Drawing.Point(103, 17);
            this.rbActiveAll.Name = "rbActiveAll";
            this.rbActiveAll.Size = new System.Drawing.Size(55, 17);
            this.rbActiveAll.TabIndex = 2;
            this.rbActiveAll.TabStop = true;
            this.rbActiveAll.Text = "Todos";
            this.rbActiveAll.UseVisualStyleBackColor = true;
            // 
            // rbActiveNo
            // 
            this.rbActiveNo.AutoSize = true;
            this.rbActiveNo.Location = new System.Drawing.Point(58, 17);
            this.rbActiveNo.Name = "rbActiveNo";
            this.rbActiveNo.Size = new System.Drawing.Size(39, 17);
            this.rbActiveNo.TabIndex = 1;
            this.rbActiveNo.Text = "No";
            this.rbActiveNo.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbObsoletYes);
            this.groupBox5.Controls.Add(this.rbObsoletAll);
            this.groupBox5.Controls.Add(this.rbObsoletNo);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(18, 317);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(170, 42);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Obsoletos A3ERP";
            this.groupBox5.Visible = false;
            // 
            // rbObsoletYes
            // 
            this.rbObsoletYes.AutoSize = true;
            this.rbObsoletYes.Location = new System.Drawing.Point(18, 17);
            this.rbObsoletYes.Name = "rbObsoletYes";
            this.rbObsoletYes.Size = new System.Drawing.Size(34, 17);
            this.rbObsoletYes.TabIndex = 3;
            this.rbObsoletYes.Text = "Si";
            this.rbObsoletYes.UseVisualStyleBackColor = true;
            // 
            // rbObsoletAll
            // 
            this.rbObsoletAll.AutoSize = true;
            this.rbObsoletAll.Checked = true;
            this.rbObsoletAll.Location = new System.Drawing.Point(103, 17);
            this.rbObsoletAll.Name = "rbObsoletAll";
            this.rbObsoletAll.Size = new System.Drawing.Size(55, 17);
            this.rbObsoletAll.TabIndex = 2;
            this.rbObsoletAll.TabStop = true;
            this.rbObsoletAll.Text = "Todos";
            this.rbObsoletAll.UseVisualStyleBackColor = true;
            // 
            // rbObsoletNo
            // 
            this.rbObsoletNo.AutoSize = true;
            this.rbObsoletNo.Location = new System.Drawing.Point(58, 17);
            this.rbObsoletNo.Name = "rbObsoletNo";
            this.rbObsoletNo.Size = new System.Drawing.Size(39, 17);
            this.rbObsoletNo.TabIndex = 1;
            this.rbObsoletNo.Text = "No";
            this.rbObsoletNo.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbSyncYes);
            this.groupBox3.Controls.Add(this.rbSyncAll);
            this.groupBox3.Controls.Add(this.rbSyncNo);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(18, 229);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(170, 42);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sincronizados";
            // 
            // rbSyncYes
            // 
            this.rbSyncYes.AutoSize = true;
            this.rbSyncYes.Location = new System.Drawing.Point(18, 16);
            this.rbSyncYes.Name = "rbSyncYes";
            this.rbSyncYes.Size = new System.Drawing.Size(34, 17);
            this.rbSyncYes.TabIndex = 3;
            this.rbSyncYes.Text = "Si";
            this.rbSyncYes.UseVisualStyleBackColor = true;
            // 
            // rbSyncAll
            // 
            this.rbSyncAll.AutoSize = true;
            this.rbSyncAll.Checked = true;
            this.rbSyncAll.Location = new System.Drawing.Point(103, 16);
            this.rbSyncAll.Name = "rbSyncAll";
            this.rbSyncAll.Size = new System.Drawing.Size(55, 17);
            this.rbSyncAll.TabIndex = 2;
            this.rbSyncAll.TabStop = true;
            this.rbSyncAll.Text = "Todos";
            this.rbSyncAll.UseVisualStyleBackColor = true;
            // 
            // rbSyncNo
            // 
            this.rbSyncNo.AutoSize = true;
            this.rbSyncNo.Location = new System.Drawing.Point(58, 16);
            this.rbSyncNo.Name = "rbSyncNo";
            this.rbSyncNo.Size = new System.Drawing.Size(39, 17);
            this.rbSyncNo.TabIndex = 1;
            this.rbSyncNo.Text = "No";
            this.rbSyncNo.UseVisualStyleBackColor = true;
            // 
            // btnRPSTAddress
            // 
            this.btnRPSTAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRPSTAddress.Location = new System.Drawing.Point(16, 195);
            this.btnRPSTAddress.Name = "btnRPSTAddress";
            this.btnRPSTAddress.Size = new System.Drawing.Size(83, 29);
            this.btnRPSTAddress.TabIndex = 18;
            this.btnRPSTAddress.Text = "Direcciones";
            this.btnRPSTAddress.UseVisualStyleBackColor = true;
            this.btnRPSTAddress.Click += new System.EventHandler(this.btnRPSTAddress_Click);
            // 
            // tbAccountId
            // 
            this.tbAccountId.Location = new System.Drawing.Point(107, 195);
            this.tbAccountId.Name = "tbAccountId";
            this.tbAccountId.Size = new System.Drawing.Size(71, 26);
            this.tbAccountId.TabIndex = 17;
            // 
            // btnLoadAccountsRepasat
            // 
            this.btnLoadAccountsRepasat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadAccountsRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadAccountsRepasat.Image")));
            this.btnLoadAccountsRepasat.Location = new System.Drawing.Point(16, 96);
            this.btnLoadAccountsRepasat.Name = "btnLoadAccountsRepasat";
            this.btnLoadAccountsRepasat.Size = new System.Drawing.Size(83, 38);
            this.btnLoadAccountsRepasat.TabIndex = 16;
            this.btnLoadAccountsRepasat.UseVisualStyleBackColor = true;
            this.btnLoadAccountsRepasat.Click += new System.EventHandler(this.btnLoadAccountsRepasat_Click);
            // 
            // btnLoadAccountsERP
            // 
            this.btnLoadAccountsERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadAccountsERP.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadAccountsERP.Image")));
            this.btnLoadAccountsERP.Location = new System.Drawing.Point(107, 96);
            this.btnLoadAccountsERP.Name = "btnLoadAccountsERP";
            this.btnLoadAccountsERP.Size = new System.Drawing.Size(81, 38);
            this.btnLoadAccountsERP.TabIndex = 15;
            this.btnLoadAccountsERP.UseVisualStyleBackColor = true;
            this.btnLoadAccountsERP.Click += new System.EventHandler(this.btnLoadAccountsERP_Click);
            // 
            // btSendCustomersRepasatToA3ERP
            // 
            this.btSendCustomersRepasatToA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btSendCustomersRepasatToA3ERP.Image")));
            this.btSendCustomersRepasatToA3ERP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSendCustomersRepasatToA3ERP.Location = new System.Drawing.Point(105, 55);
            this.btSendCustomersRepasatToA3ERP.Name = "btSendCustomersRepasatToA3ERP";
            this.btSendCustomersRepasatToA3ERP.Size = new System.Drawing.Size(83, 38);
            this.btSendCustomersRepasatToA3ERP.TabIndex = 8;
            this.btSendCustomersRepasatToA3ERP.Text = ">>>";
            this.btSendCustomersRepasatToA3ERP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSendCustomersRepasatToA3ERP.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btSendCustomersRepasatToA3ERP.UseVisualStyleBackColor = true;
            this.btSendCustomersRepasatToA3ERP.Click += new System.EventHandler(this.btSendCustomersRepasatToA3ERP_Click);
            // 
            // btSendCustomersA3ERPToRepasat
            // 
            this.btSendCustomersA3ERPToRepasat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btSendCustomersA3ERPToRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btSendCustomersA3ERPToRepasat.Image")));
            this.btSendCustomersA3ERPToRepasat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSendCustomersA3ERPToRepasat.Location = new System.Drawing.Point(16, 55);
            this.btSendCustomersA3ERPToRepasat.Name = "btSendCustomersA3ERPToRepasat";
            this.btSendCustomersA3ERPToRepasat.Size = new System.Drawing.Size(83, 38);
            this.btSendCustomersA3ERPToRepasat.TabIndex = 7;
            this.btSendCustomersA3ERPToRepasat.Text = "<<< ";
            this.btSendCustomersA3ERPToRepasat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btSendCustomersA3ERPToRepasat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSendCustomersA3ERPToRepasat.UseVisualStyleBackColor = true;
            this.btSendCustomersA3ERPToRepasat.Click += new System.EventHandler(this.btSendCustomersA3ERPToRepasat_Click);
            // 
            // dgvAccountsA3ERP
            // 
            this.dgvAccountsA3ERP.AllowDrop = true;
            this.dgvAccountsA3ERP.AllowUserToAddRows = false;
            this.dgvAccountsA3ERP.AllowUserToDeleteRows = false;
            this.dgvAccountsA3ERP.AllowUserToOrderColumns = true;
            this.dgvAccountsA3ERP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAccountsA3ERP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccountsA3ERP.ContextMenuStrip = this.contextMenuERPData;
            this.dgvAccountsA3ERP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAccountsA3ERP.Location = new System.Drawing.Point(0, 0);
            this.dgvAccountsA3ERP.Name = "dgvAccountsA3ERP";
            this.dgvAccountsA3ERP.ReadOnly = true;
            this.dgvAccountsA3ERP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAccountsA3ERP.Size = new System.Drawing.Size(617, 713);
            this.dgvAccountsA3ERP.TabIndex = 1;
            this.dgvAccountsA3ERP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountsA3ERP_CellClick);
            this.dgvAccountsA3ERP.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvAccountsA3ERP_CellFormatting);
            this.dgvAccountsA3ERP.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAccountsA3ERP_DataBindingComplete);
            this.dgvAccountsA3ERP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvAccountsA3ERP_MouseDown);
            this.dgvAccountsA3ERP.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgvAccountsA3ERP_MouseMove);
            // 
            // contextMenuERPData
            // 
            this.contextMenuERPData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarSincronizaciónToolStripMenuItem,
            this.validarPorCodigoExternoToolStripMenuItem,
            this.syncronizaciónToolStripMenuItem,
            this.crearEnRepasatToolStripMenuItem,
            this.articulosToolStripMenuItem,
            this.sincronizarHaciaRepasatToolStripMenuItem,
            this.verificarSincronizaciónToolStripMenuItem});
            this.contextMenuERPData.Name = "contextMenuERPData";
            this.contextMenuERPData.Size = new System.Drawing.Size(208, 158);
            // 
            // borrarSincronizaciónToolStripMenuItem
            // 
            this.borrarSincronizaciónToolStripMenuItem.Name = "borrarSincronizaciónToolStripMenuItem";
            this.borrarSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.borrarSincronizaciónToolStripMenuItem.Text = "Borrar Sincronización";
            this.borrarSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem_Click);
            // 
            // validarPorCodigoExternoToolStripMenuItem
            // 
            this.validarPorCodigoExternoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.códigoExternoToolStripMenuItem,
            this.nIFToolStripMenuItem});
            this.validarPorCodigoExternoToolStripMenuItem.Name = "validarPorCodigoExternoToolStripMenuItem";
            this.validarPorCodigoExternoToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.validarPorCodigoExternoToolStripMenuItem.Text = "Validar Por";
            // 
            // códigoExternoToolStripMenuItem
            // 
            this.códigoExternoToolStripMenuItem.Name = "códigoExternoToolStripMenuItem";
            this.códigoExternoToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.códigoExternoToolStripMenuItem.Text = "Código Externo";
            this.códigoExternoToolStripMenuItem.Click += new System.EventHandler(this.códigoExternoToolStripMenuItem_Click);
            // 
            // nIFToolStripMenuItem
            // 
            this.nIFToolStripMenuItem.Name = "nIFToolStripMenuItem";
            this.nIFToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.nIFToolStripMenuItem.Text = "NIF";
            this.nIFToolStripMenuItem.Click += new System.EventHandler(this.nIFToolStripMenuItem_Click);
            // 
            // syncronizaciónToolStripMenuItem
            // 
            this.syncronizaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marcarSyncroToolStripMenuItem,
            this.marcarNoSyncroToolStripMenuItem});
            this.syncronizaciónToolStripMenuItem.Name = "syncronizaciónToolStripMenuItem";
            this.syncronizaciónToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.syncronizaciónToolStripMenuItem.Text = "Syncronización";
            // 
            // marcarSyncroToolStripMenuItem
            // 
            this.marcarSyncroToolStripMenuItem.Name = "marcarSyncroToolStripMenuItem";
            this.marcarSyncroToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.marcarSyncroToolStripMenuItem.Text = "Marcar: Syncro";
            this.marcarSyncroToolStripMenuItem.Click += new System.EventHandler(this.marcarSyncroToolStripMenuItem_Click);
            // 
            // marcarNoSyncroToolStripMenuItem
            // 
            this.marcarNoSyncroToolStripMenuItem.Name = "marcarNoSyncroToolStripMenuItem";
            this.marcarNoSyncroToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.marcarNoSyncroToolStripMenuItem.Text = "Marcar: No Syncro";
            this.marcarNoSyncroToolStripMenuItem.Click += new System.EventHandler(this.marcarNoSyncroToolStripMenuItem_Click);
            // 
            // crearEnRepasatToolStripMenuItem
            // 
            this.crearEnRepasatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todosToolStripMenuItem,
            this.selecciónToolStripMenuItem});
            this.crearEnRepasatToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("crearEnRepasatToolStripMenuItem.Image")));
            this.crearEnRepasatToolStripMenuItem.Name = "crearEnRepasatToolStripMenuItem";
            this.crearEnRepasatToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.crearEnRepasatToolStripMenuItem.Text = "Crear en Repasat";
            // 
            // todosToolStripMenuItem
            // 
            this.todosToolStripMenuItem.Name = "todosToolStripMenuItem";
            this.todosToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.todosToolStripMenuItem.Text = "Todos";
            this.todosToolStripMenuItem.Click += new System.EventHandler(this.todosToolStripMenuItem_Click);
            // 
            // selecciónToolStripMenuItem
            // 
            this.selecciónToolStripMenuItem.Name = "selecciónToolStripMenuItem";
            this.selecciónToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.selecciónToolStripMenuItem.Text = "Selección";
            this.selecciónToolStripMenuItem.Click += new System.EventHandler(this.selecciónToolStripMenuItem_Click);
            // 
            // articulosToolStripMenuItem
            // 
            this.articulosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.activarControlStockToolStripMenuItem,
            this.resetearIdRepasatToolStripMenuItem,
            this.actualizarArticulosEnRPSTToolStripMenuItem,
            this.validarArticulosDeA3EnRPSTToolStripMenuItem});
            this.articulosToolStripMenuItem.Name = "articulosToolStripMenuItem";
            this.articulosToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.articulosToolStripMenuItem.Text = "Articulos";
            this.articulosToolStripMenuItem.Visible = false;
            // 
            // activarControlStockToolStripMenuItem
            // 
            this.activarControlStockToolStripMenuItem.Name = "activarControlStockToolStripMenuItem";
            this.activarControlStockToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.activarControlStockToolStripMenuItem.Text = "Activar Control Stock";
            this.activarControlStockToolStripMenuItem.Click += new System.EventHandler(this.activarControlStockToolStripMenuItem_Click);
            // 
            // resetearIdRepasatToolStripMenuItem
            // 
            this.resetearIdRepasatToolStripMenuItem.Name = "resetearIdRepasatToolStripMenuItem";
            this.resetearIdRepasatToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.resetearIdRepasatToolStripMenuItem.Text = "Resetear Id Repasat";
            this.resetearIdRepasatToolStripMenuItem.Click += new System.EventHandler(this.resetearIdRepasatToolStripMenuItem_Click);
            // 
            // actualizarArticulosEnRPSTToolStripMenuItem
            // 
            this.actualizarArticulosEnRPSTToolStripMenuItem.Name = "actualizarArticulosEnRPSTToolStripMenuItem";
            this.actualizarArticulosEnRPSTToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.actualizarArticulosEnRPSTToolStripMenuItem.Text = "Actualizar Articulos en RPST";
            this.actualizarArticulosEnRPSTToolStripMenuItem.Click += new System.EventHandler(this.actualizarArticulosEnRPSTToolStripMenuItem_Click);
            // 
            // validarArticulosDeA3EnRPSTToolStripMenuItem
            // 
            this.validarArticulosDeA3EnRPSTToolStripMenuItem.Name = "validarArticulosDeA3EnRPSTToolStripMenuItem";
            this.validarArticulosDeA3EnRPSTToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.validarArticulosDeA3EnRPSTToolStripMenuItem.Text = "Validar Articulos de A3 en RPST";
            this.validarArticulosDeA3EnRPSTToolStripMenuItem.Click += new System.EventHandler(this.validarArticulosDeA3EnRPSTToolStripMenuItem_Click);
            // 
            // sincronizarHaciaRepasatToolStripMenuItem
            // 
            this.sincronizarHaciaRepasatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comercialesToolStripMenuItem});
            this.sincronizarHaciaRepasatToolStripMenuItem.Name = "sincronizarHaciaRepasatToolStripMenuItem";
            this.sincronizarHaciaRepasatToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.sincronizarHaciaRepasatToolStripMenuItem.Text = "Sincronizar hacia Repasat";
            // 
            // comercialesToolStripMenuItem
            // 
            this.comercialesToolStripMenuItem.Name = "comercialesToolStripMenuItem";
            this.comercialesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.comercialesToolStripMenuItem.Text = "Comerciales";
            this.comercialesToolStripMenuItem.Click += new System.EventHandler(this.comercialesToolStripMenuItem_Click);
            // 
            // verificarSincronizaciónToolStripMenuItem
            // 
            this.verificarSincronizaciónToolStripMenuItem.Name = "verificarSincronizaciónToolStripMenuItem";
            this.verificarSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.verificarSincronizaciónToolStripMenuItem.Text = "&Verificar Sincronización";
            this.verificarSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.verificarSincronizaciónToolStripMenuItem_Click);
            // 
            // tabDocs
            // 
            this.tabDocs.Controls.Add(this.splitContainer2);
            this.tabDocs.Location = new System.Drawing.Point(4, 29);
            this.tabDocs.Name = "tabDocs";
            this.tabDocs.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocs.Size = new System.Drawing.Size(1374, 719);
            this.tabDocs.TabIndex = 5;
            this.tabDocs.Text = "Documentos";
            this.tabDocs.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tabControl2);
            this.splitContainer2.Panel1MinSize = 20;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer8);
            this.splitContainer2.Size = new System.Drawing.Size(1368, 713);
            this.splitContainer2.SplitterDistance = 356;
            this.splitContainer2.TabIndex = 0;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Location = new System.Drawing.Point(6, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(347, 684);
            this.tabControl2.TabIndex = 4;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(339, 651);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Documentos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grBoxTicketBai);
            this.groupBox1.Controls.Add(this.grBoxDocPago);
            this.groupBox1.Controls.Add(this.rdbutFromRPST);
            this.groupBox1.Controls.Add(this.rdbutFromA3ERP);
            this.groupBox1.Controls.Add(this.lblShowData);
            this.groupBox1.Controls.Add(this.picBoxRPST);
            this.groupBox1.Controls.Add(this.grBoxCuenta);
            this.groupBox1.Controls.Add(this.grBoxRemesado);
            this.groupBox1.Controls.Add(this.grBoxTipoDocumento);
            this.groupBox1.Controls.Add(this.grBoxFechas);
            this.groupBox1.Controls.Add(this.grBoxSeries);
            this.groupBox1.Controls.Add(this.grBoxEstadoCobro);
            this.groupBox1.Controls.Add(this.btnLoadDocsRPST);
            this.groupBox1.Controls.Add(this.btnLoadDocsA3ERP);
            this.groupBox1.Controls.Add(this.btnSyncToRPST);
            this.groupBox1.Controls.Add(this.btnSyncToA3ERP);
            this.groupBox1.Controls.Add(this.grBoxSincronizados);
            this.groupBox1.Controls.Add(this.picBoxA3);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 607);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Documentos";
            // 
            // grBoxTicketBai
            // 
            this.grBoxTicketBai.Controls.Add(this.bt_tbai);
            this.grBoxTicketBai.Controls.Add(this.label16);
            this.grBoxTicketBai.Controls.Add(this.rbTraspasarComoBorradorSi);
            this.grBoxTicketBai.Controls.Add(this.rbTraspasarComoBorradorNo);
            this.grBoxTicketBai.Controls.Add(this.picboxTicketBai);
            this.grBoxTicketBai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxTicketBai.Location = new System.Drawing.Point(14, 486);
            this.grBoxTicketBai.Name = "grBoxTicketBai";
            this.grBoxTicketBai.Size = new System.Drawing.Size(274, 55);
            this.grBoxTicketBai.TabIndex = 26;
            this.grBoxTicketBai.TabStop = false;
            this.grBoxTicketBai.Text = "Ticket Bai";
            this.grBoxTicketBai.Visible = false;
            // 
            // bt_tbai
            // 
            this.bt_tbai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_tbai.Location = new System.Drawing.Point(212, 15);
            this.bt_tbai.Name = "bt_tbai";
            this.bt_tbai.Size = new System.Drawing.Size(46, 33);
            this.bt_tbai.TabIndex = 49;
            this.bt_tbai.Text = "TBai";
            this.bt_tbai.UseVisualStyleBackColor = true;
            this.bt_tbai.Click += new System.EventHandler(this.bt_tbai_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(65, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "Traspasar como Borrador";
            // 
            // rbTraspasarComoBorradorSi
            // 
            this.rbTraspasarComoBorradorSi.AutoSize = true;
            this.rbTraspasarComoBorradorSi.Checked = true;
            this.rbTraspasarComoBorradorSi.Location = new System.Drawing.Point(134, 16);
            this.rbTraspasarComoBorradorSi.Name = "rbTraspasarComoBorradorSi";
            this.rbTraspasarComoBorradorSi.Size = new System.Drawing.Size(38, 20);
            this.rbTraspasarComoBorradorSi.TabIndex = 20;
            this.rbTraspasarComoBorradorSi.TabStop = true;
            this.rbTraspasarComoBorradorSi.Text = "Si";
            this.rbTraspasarComoBorradorSi.UseVisualStyleBackColor = true;
            // 
            // rbTraspasarComoBorradorNo
            // 
            this.rbTraspasarComoBorradorNo.AutoSize = true;
            this.rbTraspasarComoBorradorNo.Location = new System.Drawing.Point(84, 16);
            this.rbTraspasarComoBorradorNo.Name = "rbTraspasarComoBorradorNo";
            this.rbTraspasarComoBorradorNo.Size = new System.Drawing.Size(44, 20);
            this.rbTraspasarComoBorradorNo.TabIndex = 21;
            this.rbTraspasarComoBorradorNo.Text = "No";
            this.rbTraspasarComoBorradorNo.UseVisualStyleBackColor = true;
            // 
            // picboxTicketBai
            // 
            this.picboxTicketBai.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picboxTicketBai.ErrorImage")));
            this.picboxTicketBai.Image = ((System.Drawing.Image)(resources.GetObject("picboxTicketBai.Image")));
            this.picboxTicketBai.Location = new System.Drawing.Point(21, 18);
            this.picboxTicketBai.Name = "picboxTicketBai";
            this.picboxTicketBai.Size = new System.Drawing.Size(35, 31);
            this.picboxTicketBai.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picboxTicketBai.TabIndex = 47;
            this.picboxTicketBai.TabStop = false;
            // 
            // grBoxDocPago
            // 
            this.grBoxDocPago.Controls.Add(this.checkBoxDocPagos);
            this.grBoxDocPago.Controls.Add(this.cboxDocPagos);
            this.grBoxDocPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxDocPago.Location = new System.Drawing.Point(17, 341);
            this.grBoxDocPago.Name = "grBoxDocPago";
            this.grBoxDocPago.Size = new System.Drawing.Size(200, 54);
            this.grBoxDocPago.TabIndex = 29;
            this.grBoxDocPago.TabStop = false;
            this.grBoxDocPago.Text = "Docs Pago";
            this.grBoxDocPago.Visible = false;
            // 
            // checkBoxDocPagos
            // 
            this.checkBoxDocPagos.AutoSize = true;
            this.checkBoxDocPagos.Location = new System.Drawing.Point(12, 27);
            this.checkBoxDocPagos.Name = "checkBoxDocPagos";
            this.checkBoxDocPagos.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDocPagos.TabIndex = 28;
            this.checkBoxDocPagos.UseVisualStyleBackColor = true;
            this.checkBoxDocPagos.CheckedChanged += new System.EventHandler(this.checkBoxDocPagos_CheckedChanged);
            // 
            // cboxDocPagos
            // 
            this.cboxDocPagos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxDocPagos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxDocPagos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxDocPagos.DropDownWidth = 300;
            this.cboxDocPagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxDocPagos.FormattingEnabled = true;
            this.cboxDocPagos.Location = new System.Drawing.Point(44, 19);
            this.cboxDocPagos.Name = "cboxDocPagos";
            this.cboxDocPagos.Size = new System.Drawing.Size(144, 24);
            this.cboxDocPagos.TabIndex = 13;
            // 
            // rdbutFromRPST
            // 
            this.rdbutFromRPST.AutoSize = true;
            this.rdbutFromRPST.Enabled = false;
            this.rdbutFromRPST.Location = new System.Drawing.Point(268, 203);
            this.rdbutFromRPST.Name = "rdbutFromRPST";
            this.rdbutFromRPST.Size = new System.Drawing.Size(14, 13);
            this.rdbutFromRPST.TabIndex = 46;
            this.rdbutFromRPST.TabStop = true;
            this.rdbutFromRPST.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rdbutFromRPST.UseVisualStyleBackColor = true;
            // 
            // rdbutFromA3ERP
            // 
            this.rdbutFromA3ERP.AutoSize = true;
            this.rdbutFromA3ERP.Enabled = false;
            this.rdbutFromA3ERP.Location = new System.Drawing.Point(246, 203);
            this.rdbutFromA3ERP.Name = "rdbutFromA3ERP";
            this.rdbutFromA3ERP.Size = new System.Drawing.Size(14, 13);
            this.rdbutFromA3ERP.TabIndex = 45;
            this.rdbutFromA3ERP.TabStop = true;
            this.rdbutFromA3ERP.UseVisualStyleBackColor = true;
            // 
            // lblShowData
            // 
            this.lblShowData.AutoSize = true;
            this.lblShowData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShowData.Location = new System.Drawing.Point(194, 175);
            this.lblShowData.Name = "lblShowData";
            this.lblShowData.Size = new System.Drawing.Size(128, 16);
            this.lblShowData.TabIndex = 44;
            this.lblShowData.Text = "Mostrando datos de";
            this.lblShowData.Visible = false;
            // 
            // picBoxRPST
            // 
            this.picBoxRPST.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picBoxRPST.ErrorImage")));
            this.picBoxRPST.Image = ((System.Drawing.Image)(resources.GetObject("picBoxRPST.Image")));
            this.picBoxRPST.Location = new System.Drawing.Point(288, 192);
            this.picBoxRPST.Name = "picBoxRPST";
            this.picBoxRPST.Size = new System.Drawing.Size(34, 35);
            this.picBoxRPST.TabIndex = 42;
            this.picBoxRPST.TabStop = false;
            this.picBoxRPST.Visible = false;
            // 
            // grBoxCuenta
            // 
            this.grBoxCuenta.Controls.Add(this.checkBoxCuentas);
            this.grBoxCuenta.Controls.Add(this.cboxCuentas);
            this.grBoxCuenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxCuenta.Location = new System.Drawing.Point(17, 286);
            this.grBoxCuenta.Name = "grBoxCuenta";
            this.grBoxCuenta.Size = new System.Drawing.Size(200, 54);
            this.grBoxCuenta.TabIndex = 26;
            this.grBoxCuenta.TabStop = false;
            this.grBoxCuenta.Text = "Cuenta";
            this.grBoxCuenta.Visible = false;
            // 
            // checkBoxCuentas
            // 
            this.checkBoxCuentas.AutoSize = true;
            this.checkBoxCuentas.Location = new System.Drawing.Point(12, 27);
            this.checkBoxCuentas.Name = "checkBoxCuentas";
            this.checkBoxCuentas.Size = new System.Drawing.Size(15, 14);
            this.checkBoxCuentas.TabIndex = 28;
            this.checkBoxCuentas.UseVisualStyleBackColor = true;
            this.checkBoxCuentas.CheckedChanged += new System.EventHandler(this.cboxCuentas_CheckedChanged);
            // 
            // cboxCuentas
            // 
            this.cboxCuentas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxCuentas.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxCuentas.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxCuentas.DropDownWidth = 300;
            this.cboxCuentas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxCuentas.FormattingEnabled = true;
            this.cboxCuentas.Location = new System.Drawing.Point(44, 19);
            this.cboxCuentas.Name = "cboxCuentas";
            this.cboxCuentas.Size = new System.Drawing.Size(144, 24);
            this.cboxCuentas.TabIndex = 13;
            this.cboxCuentas.Visible = false;
            // 
            // grBoxRemesado
            // 
            this.grBoxRemesado.Controls.Add(this.rbRemesadoSi);
            this.grBoxRemesado.Controls.Add(this.rbRemesadoAll);
            this.grBoxRemesado.Controls.Add(this.rbRemesadoNo);
            this.grBoxRemesado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxRemesado.Location = new System.Drawing.Point(16, 439);
            this.grBoxRemesado.Name = "grBoxRemesado";
            this.grBoxRemesado.Size = new System.Drawing.Size(200, 41);
            this.grBoxRemesado.TabIndex = 25;
            this.grBoxRemesado.TabStop = false;
            this.grBoxRemesado.Text = "Remesado";
            // 
            // rbRemesadoSi
            // 
            this.rbRemesadoSi.AutoSize = true;
            this.rbRemesadoSi.Location = new System.Drawing.Point(134, 18);
            this.rbRemesadoSi.Name = "rbRemesadoSi";
            this.rbRemesadoSi.Size = new System.Drawing.Size(38, 20);
            this.rbRemesadoSi.TabIndex = 20;
            this.rbRemesadoSi.Text = "Si";
            this.rbRemesadoSi.UseVisualStyleBackColor = true;
            // 
            // rbRemesadoAll
            // 
            this.rbRemesadoAll.AutoSize = true;
            this.rbRemesadoAll.Checked = true;
            this.rbRemesadoAll.Location = new System.Drawing.Point(12, 18);
            this.rbRemesadoAll.Name = "rbRemesadoAll";
            this.rbRemesadoAll.Size = new System.Drawing.Size(66, 20);
            this.rbRemesadoAll.TabIndex = 22;
            this.rbRemesadoAll.TabStop = true;
            this.rbRemesadoAll.Text = "Todos";
            this.rbRemesadoAll.UseVisualStyleBackColor = true;
            // 
            // rbRemesadoNo
            // 
            this.rbRemesadoNo.AutoSize = true;
            this.rbRemesadoNo.Location = new System.Drawing.Point(84, 18);
            this.rbRemesadoNo.Name = "rbRemesadoNo";
            this.rbRemesadoNo.Size = new System.Drawing.Size(44, 20);
            this.rbRemesadoNo.TabIndex = 21;
            this.rbRemesadoNo.Text = "No";
            this.rbRemesadoNo.UseVisualStyleBackColor = true;
            // 
            // grBoxTipoDocumento
            // 
            this.grBoxTipoDocumento.Controls.Add(this.rbVentas);
            this.grBoxTipoDocumento.Controls.Add(this.rbCompras);
            this.grBoxTipoDocumento.Controls.Add(this.cboxTipoDoc);
            this.grBoxTipoDocumento.Controls.Add(this.label14);
            this.grBoxTipoDocumento.Location = new System.Drawing.Point(16, 17);
            this.grBoxTipoDocumento.Name = "grBoxTipoDocumento";
            this.grBoxTipoDocumento.Size = new System.Drawing.Size(305, 54);
            this.grBoxTipoDocumento.TabIndex = 41;
            this.grBoxTipoDocumento.TabStop = false;
            // 
            // rbVentas
            // 
            this.rbVentas.AutoSize = true;
            this.rbVentas.Checked = true;
            this.rbVentas.Location = new System.Drawing.Point(214, 8);
            this.rbVentas.Name = "rbVentas";
            this.rbVentas.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbVentas.Size = new System.Drawing.Size(78, 24);
            this.rbVentas.TabIndex = 11;
            this.rbVentas.TabStop = true;
            this.rbVentas.Text = "Ventas";
            this.rbVentas.UseVisualStyleBackColor = true;
            this.rbVentas.CheckedChanged += new System.EventHandler(this.rbVentas_CheckedChanged);
            // 
            // rbCompras
            // 
            this.rbCompras.AutoSize = true;
            this.rbCompras.Location = new System.Drawing.Point(214, 30);
            this.rbCompras.Name = "rbCompras";
            this.rbCompras.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbCompras.Size = new System.Drawing.Size(91, 24);
            this.rbCompras.TabIndex = 12;
            this.rbCompras.Text = "Compras";
            this.rbCompras.UseVisualStyleBackColor = true;
            // 
            // cboxTipoDoc
            // 
            this.cboxTipoDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxTipoDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxTipoDoc.FormattingEnabled = true;
            this.cboxTipoDoc.ItemHeight = 20;
            this.cboxTipoDoc.Items.AddRange(new object[] {
            "Facturas",
            "Albaranes",
            "Pedidos",
            "Cartera",
            "Remesas",
            "Impagados",
            "Agrupaciones",
            "Tarifas"});
            this.cboxTipoDoc.Location = new System.Drawing.Point(53, 17);
            this.cboxTipoDoc.Name = "cboxTipoDoc";
            this.cboxTipoDoc.Size = new System.Drawing.Size(147, 28);
            this.cboxTipoDoc.TabIndex = 31;
            this.cboxTipoDoc.SelectedIndexChanged += new System.EventHandler(this.cboxTipoDoc_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 20);
            this.label14.TabIndex = 32;
            this.label14.Text = "Tipo";
            // 
            // grBoxFechas
            // 
            this.grBoxFechas.Controls.Add(this.rbutFechaFactura);
            this.grBoxFechas.Controls.Add(this.rbutFechaVencimiento);
            this.grBoxFechas.Controls.Add(this.dtpFromInvoicesV);
            this.grBoxFechas.Controls.Add(this.dtpToInvoicesV);
            this.grBoxFechas.Controls.Add(this.lbDesde);
            this.grBoxFechas.Controls.Add(this.label1);
            this.grBoxFechas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxFechas.Location = new System.Drawing.Point(17, 77);
            this.grBoxFechas.Name = "grBoxFechas";
            this.grBoxFechas.Size = new System.Drawing.Size(257, 98);
            this.grBoxFechas.TabIndex = 39;
            this.grBoxFechas.TabStop = false;
            this.grBoxFechas.Text = "Fechas";
            // 
            // rbutFechaFactura
            // 
            this.rbutFechaFactura.AutoSize = true;
            this.rbutFechaFactura.Location = new System.Drawing.Point(99, 73);
            this.rbutFechaFactura.Name = "rbutFechaFactura";
            this.rbutFechaFactura.Size = new System.Drawing.Size(90, 20);
            this.rbutFechaFactura.TabIndex = 9;
            this.rbutFechaFactura.Text = "Fecha Fra.";
            this.rbutFechaFactura.UseVisualStyleBackColor = true;
            // 
            // rbutFechaVencimiento
            // 
            this.rbutFechaVencimiento.AutoSize = true;
            this.rbutFechaVencimiento.Checked = true;
            this.rbutFechaVencimiento.Location = new System.Drawing.Point(13, 73);
            this.rbutFechaVencimiento.Name = "rbutFechaVencimiento";
            this.rbutFechaVencimiento.Size = new System.Drawing.Size(90, 20);
            this.rbutFechaVencimiento.TabIndex = 8;
            this.rbutFechaVencimiento.TabStop = true;
            this.rbutFechaVencimiento.Text = "Fecha Vto.";
            this.rbutFechaVencimiento.UseVisualStyleBackColor = true;
            // 
            // dtpFromInvoicesV
            // 
            this.dtpFromInvoicesV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromInvoicesV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromInvoicesV.Location = new System.Drawing.Point(12, 41);
            this.dtpFromInvoicesV.Name = "dtpFromInvoicesV";
            this.dtpFromInvoicesV.Size = new System.Drawing.Size(103, 26);
            this.dtpFromInvoicesV.TabIndex = 3;
            // 
            // dtpToInvoicesV
            // 
            this.dtpToInvoicesV.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpToInvoicesV.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToInvoicesV.Location = new System.Drawing.Point(141, 41);
            this.dtpToInvoicesV.Name = "dtpToInvoicesV";
            this.dtpToInvoicesV.Size = new System.Drawing.Size(102, 26);
            this.dtpToInvoicesV.TabIndex = 5;
            // 
            // lbDesde
            // 
            this.lbDesde.AutoSize = true;
            this.lbDesde.Location = new System.Drawing.Point(9, 22);
            this.lbDesde.Name = "lbDesde";
            this.lbDesde.Size = new System.Drawing.Size(49, 16);
            this.lbDesde.TabIndex = 6;
            this.lbDesde.Text = "Desde";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Hasta";
            // 
            // grBoxSeries
            // 
            this.grBoxSeries.Controls.Add(this.checkBoxSeries);
            this.grBoxSeries.Controls.Add(this.cboxSeries);
            this.grBoxSeries.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxSeries.Location = new System.Drawing.Point(17, 224);
            this.grBoxSeries.Name = "grBoxSeries";
            this.grBoxSeries.Size = new System.Drawing.Size(171, 56);
            this.grBoxSeries.TabIndex = 38;
            this.grBoxSeries.TabStop = false;
            this.grBoxSeries.Text = "Series";
            // 
            // checkBoxSeries
            // 
            this.checkBoxSeries.AutoSize = true;
            this.checkBoxSeries.Location = new System.Drawing.Point(12, 26);
            this.checkBoxSeries.Name = "checkBoxSeries";
            this.checkBoxSeries.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSeries.TabIndex = 27;
            this.checkBoxSeries.UseVisualStyleBackColor = true;
            this.checkBoxSeries.CheckedChanged += new System.EventHandler(this.checkBoxSeries_CheckedChanged);
            // 
            // cboxSeries
            // 
            this.cboxSeries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxSeries.FormattingEnabled = true;
            this.cboxSeries.Location = new System.Drawing.Point(44, 21);
            this.cboxSeries.Name = "cboxSeries";
            this.cboxSeries.Size = new System.Drawing.Size(121, 24);
            this.cboxSeries.TabIndex = 25;
            // 
            // grBoxEstadoCobro
            // 
            this.grBoxEstadoCobro.Controls.Add(this.rbPaidYes);
            this.grBoxEstadoCobro.Controls.Add(this.rbPaidAll);
            this.grBoxEstadoCobro.Controls.Add(this.rbPaidNo);
            this.grBoxEstadoCobro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxEstadoCobro.Location = new System.Drawing.Point(16, 397);
            this.grBoxEstadoCobro.Name = "grBoxEstadoCobro";
            this.grBoxEstadoCobro.Size = new System.Drawing.Size(200, 41);
            this.grBoxEstadoCobro.TabIndex = 24;
            this.grBoxEstadoCobro.TabStop = false;
            this.grBoxEstadoCobro.Text = "Estado Cobro";
            this.grBoxEstadoCobro.Visible = false;
            // 
            // rbPaidYes
            // 
            this.rbPaidYes.AutoSize = true;
            this.rbPaidYes.Location = new System.Drawing.Point(134, 17);
            this.rbPaidYes.Name = "rbPaidYes";
            this.rbPaidYes.Size = new System.Drawing.Size(38, 20);
            this.rbPaidYes.TabIndex = 20;
            this.rbPaidYes.Text = "Si";
            this.rbPaidYes.UseVisualStyleBackColor = true;
            // 
            // rbPaidAll
            // 
            this.rbPaidAll.AutoSize = true;
            this.rbPaidAll.Checked = true;
            this.rbPaidAll.Location = new System.Drawing.Point(12, 17);
            this.rbPaidAll.Name = "rbPaidAll";
            this.rbPaidAll.Size = new System.Drawing.Size(66, 20);
            this.rbPaidAll.TabIndex = 22;
            this.rbPaidAll.TabStop = true;
            this.rbPaidAll.Text = "Todos";
            this.rbPaidAll.UseVisualStyleBackColor = true;
            // 
            // rbPaidNo
            // 
            this.rbPaidNo.AutoSize = true;
            this.rbPaidNo.Location = new System.Drawing.Point(84, 17);
            this.rbPaidNo.Name = "rbPaidNo";
            this.rbPaidNo.Size = new System.Drawing.Size(44, 20);
            this.rbPaidNo.TabIndex = 21;
            this.rbPaidNo.Text = "No";
            this.rbPaidNo.UseVisualStyleBackColor = true;
            // 
            // btnLoadDocsRPST
            // 
            this.btnLoadDocsRPST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadDocsRPST.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadDocsRPST.Image")));
            this.btnLoadDocsRPST.Location = new System.Drawing.Point(100, 555);
            this.btnLoadDocsRPST.Name = "btnLoadDocsRPST";
            this.btnLoadDocsRPST.Size = new System.Drawing.Size(51, 45);
            this.btnLoadDocsRPST.TabIndex = 37;
            this.btnLoadDocsRPST.UseVisualStyleBackColor = true;
            this.btnLoadDocsRPST.Click += new System.EventHandler(this.btnLoadDocsRPST_Click);
            // 
            // btnLoadDocsA3ERP
            // 
            this.btnLoadDocsA3ERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadDocsA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadDocsA3ERP.Image")));
            this.btnLoadDocsA3ERP.Location = new System.Drawing.Point(152, 555);
            this.btnLoadDocsA3ERP.Name = "btnLoadDocsA3ERP";
            this.btnLoadDocsA3ERP.Size = new System.Drawing.Size(51, 45);
            this.btnLoadDocsA3ERP.TabIndex = 36;
            this.btnLoadDocsA3ERP.UseVisualStyleBackColor = true;
            this.btnLoadDocsA3ERP.Click += new System.EventHandler(this.btnLoadDocsA3ERP_Click);
            // 
            // btnSyncToRPST
            // 
            this.btnSyncToRPST.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSyncToRPST.Image = ((System.Drawing.Image)(resources.GetObject("btnSyncToRPST.Image")));
            this.btnSyncToRPST.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSyncToRPST.Location = new System.Drawing.Point(14, 555);
            this.btnSyncToRPST.Margin = new System.Windows.Forms.Padding(0);
            this.btnSyncToRPST.Name = "btnSyncToRPST";
            this.btnSyncToRPST.Size = new System.Drawing.Size(83, 45);
            this.btnSyncToRPST.TabIndex = 34;
            this.btnSyncToRPST.Text = "<<< ";
            this.btnSyncToRPST.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSyncToRPST.UseVisualStyleBackColor = true;
            this.btnSyncToRPST.Click += new System.EventHandler(this.btnSyncToRPST_Click);
            // 
            // btnSyncToA3ERP
            // 
            this.btnSyncToA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnSyncToA3ERP.Image")));
            this.btnSyncToA3ERP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSyncToA3ERP.Location = new System.Drawing.Point(207, 555);
            this.btnSyncToA3ERP.Name = "btnSyncToA3ERP";
            this.btnSyncToA3ERP.Size = new System.Drawing.Size(83, 45);
            this.btnSyncToA3ERP.TabIndex = 33;
            this.btnSyncToA3ERP.Text = ">>>";
            this.btnSyncToA3ERP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSyncToA3ERP.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSyncToA3ERP.UseVisualStyleBackColor = true;
            this.btnSyncToA3ERP.Click += new System.EventHandler(this.btnSyncToA3ERP_Click);
            // 
            // grBoxSincronizados
            // 
            this.grBoxSincronizados.Controls.Add(this.rbSyncDocYes);
            this.grBoxSincronizados.Controls.Add(this.rbSyncDocAll);
            this.grBoxSincronizados.Controls.Add(this.rbSyncDocNo);
            this.grBoxSincronizados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxSincronizados.Location = new System.Drawing.Point(16, 175);
            this.grBoxSincronizados.Name = "grBoxSincronizados";
            this.grBoxSincronizados.Size = new System.Drawing.Size(172, 49);
            this.grBoxSincronizados.TabIndex = 23;
            this.grBoxSincronizados.TabStop = false;
            this.grBoxSincronizados.Text = "Sincronizados";
            // 
            // rbSyncDocYes
            // 
            this.rbSyncDocYes.AutoSize = true;
            this.rbSyncDocYes.Location = new System.Drawing.Point(134, 17);
            this.rbSyncDocYes.Name = "rbSyncDocYes";
            this.rbSyncDocYes.Size = new System.Drawing.Size(38, 20);
            this.rbSyncDocYes.TabIndex = 20;
            this.rbSyncDocYes.Text = "Si";
            this.rbSyncDocYes.UseVisualStyleBackColor = true;
            // 
            // rbSyncDocAll
            // 
            this.rbSyncDocAll.AutoSize = true;
            this.rbSyncDocAll.Checked = true;
            this.rbSyncDocAll.Location = new System.Drawing.Point(12, 17);
            this.rbSyncDocAll.Name = "rbSyncDocAll";
            this.rbSyncDocAll.Size = new System.Drawing.Size(66, 20);
            this.rbSyncDocAll.TabIndex = 22;
            this.rbSyncDocAll.TabStop = true;
            this.rbSyncDocAll.Text = "Todos";
            this.rbSyncDocAll.UseVisualStyleBackColor = true;
            // 
            // rbSyncDocNo
            // 
            this.rbSyncDocNo.AutoSize = true;
            this.rbSyncDocNo.Location = new System.Drawing.Point(84, 17);
            this.rbSyncDocNo.Name = "rbSyncDocNo";
            this.rbSyncDocNo.Size = new System.Drawing.Size(44, 20);
            this.rbSyncDocNo.TabIndex = 21;
            this.rbSyncDocNo.Text = "No";
            this.rbSyncDocNo.UseVisualStyleBackColor = true;
            // 
            // picBoxA3
            // 
            this.picBoxA3.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picBoxA3.ErrorImage")));
            this.picBoxA3.Image = ((System.Drawing.Image)(resources.GetObject("picBoxA3.Image")));
            this.picBoxA3.Location = new System.Drawing.Point(197, 192);
            this.picBoxA3.Name = "picBoxA3";
            this.picBoxA3.Size = new System.Drawing.Size(34, 35);
            this.picBoxA3.TabIndex = 43;
            this.picBoxA3.TabStop = false;
            this.picBoxA3.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cboxTicketBai);
            this.tabPage3.Controls.Add(this.cboxDescripcionArticulosA3);
            this.tabPage3.Controls.Add(this.cboxCheckCuentas);
            this.tabPage3.Controls.Add(this.cboxGenerarMovsContables);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(339, 651);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Opciones Sync";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cboxTicketBai
            // 
            this.cboxTicketBai.AutoSize = true;
            this.cboxTicketBai.Checked = true;
            this.cboxTicketBai.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxTicketBai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxTicketBai.Location = new System.Drawing.Point(19, 105);
            this.cboxTicketBai.Name = "cboxTicketBai";
            this.cboxTicketBai.Size = new System.Drawing.Size(127, 20);
            this.cboxTicketBai.TabIndex = 54;
            this.cboxTicketBai.Text = "Ticket Bai Activo";
            this.toolTipSync.SetToolTip(this.cboxTicketBai, "Si está marcado, utilizará el nombre del artículo de A3ERP\r\n");
            this.cboxTicketBai.UseVisualStyleBackColor = true;
            this.cboxTicketBai.CheckedChanged += new System.EventHandler(this.cboxTicketBai_CheckedChanged);
            // 
            // cboxDescripcionArticulosA3
            // 
            this.cboxDescripcionArticulosA3.AutoSize = true;
            this.cboxDescripcionArticulosA3.Checked = true;
            this.cboxDescripcionArticulosA3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxDescripcionArticulosA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxDescripcionArticulosA3.Location = new System.Drawing.Point(19, 79);
            this.cboxDescripcionArticulosA3.Name = "cboxDescripcionArticulosA3";
            this.cboxDescripcionArticulosA3.Size = new System.Drawing.Size(219, 20);
            this.cboxDescripcionArticulosA3.TabIndex = 53;
            this.cboxDescripcionArticulosA3.Text = "Descripción Articulos de A3ERP";
            this.toolTipSync.SetToolTip(this.cboxDescripcionArticulosA3, "Si está marcado, utilizará el nombre del artículo de A3ERP\r\n");
            this.cboxDescripcionArticulosA3.UseVisualStyleBackColor = true;
            // 
            // cboxCheckCuentas
            // 
            this.cboxCheckCuentas.AutoSize = true;
            this.cboxCheckCuentas.Checked = true;
            this.cboxCheckCuentas.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxCheckCuentas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxCheckCuentas.Location = new System.Drawing.Point(19, 53);
            this.cboxCheckCuentas.Name = "cboxCheckCuentas";
            this.cboxCheckCuentas.Size = new System.Drawing.Size(207, 20);
            this.cboxCheckCuentas.TabIndex = 52;
            this.cboxCheckCuentas.Text = "Verificar Cuentas Previamente";
            this.toolTipSync.SetToolTip(this.cboxCheckCuentas, "Si está marcado, verifica antes de traspasar los documentos\r\nsi hay cuentas (Clie" +
        "ntes o proveedores) no creados, y los creará \r\nen A3ERP.");
            this.cboxCheckCuentas.UseVisualStyleBackColor = true;
            // 
            // cboxGenerarMovsContables
            // 
            this.cboxGenerarMovsContables.AutoSize = true;
            this.cboxGenerarMovsContables.Checked = true;
            this.cboxGenerarMovsContables.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxGenerarMovsContables.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxGenerarMovsContables.Location = new System.Drawing.Point(19, 27);
            this.cboxGenerarMovsContables.Name = "cboxGenerarMovsContables";
            this.cboxGenerarMovsContables.Size = new System.Drawing.Size(195, 20);
            this.cboxGenerarMovsContables.TabIndex = 51;
            this.cboxGenerarMovsContables.Text = "Generar Asientos Contables";
            this.cboxGenerarMovsContables.UseVisualStyleBackColor = true;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.Location = new System.Drawing.Point(0, 0);
            this.splitContainer8.Name = "splitContainer8";
            this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.dgvRPST);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.dgvSyncDocsDetail);
            this.splitContainer8.Size = new System.Drawing.Size(1008, 713);
            this.splitContainer8.SplitterDistance = 382;
            this.splitContainer8.TabIndex = 1;
            // 
            // dgvRPST
            // 
            this.dgvRPST.AllowUserToAddRows = false;
            this.dgvRPST.AllowUserToDeleteRows = false;
            this.dgvRPST.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvRPST.ContextMenuStrip = this.ctextMenuSync;
            this.dgvRPST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRPST.Location = new System.Drawing.Point(0, 0);
            this.dgvRPST.Name = "dgvRPST";
            this.dgvRPST.ReadOnly = true;
            this.dgvRPST.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRPST.Size = new System.Drawing.Size(1008, 382);
            this.dgvRPST.TabIndex = 0;
            this.dgvRPST.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRPST_CellContentClick);
            this.dgvRPST.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvRPST_CellFormatting);
            // 
            // dgvSyncDocsDetail
            // 
            this.dgvSyncDocsDetail.AllowUserToAddRows = false;
            this.dgvSyncDocsDetail.AllowUserToDeleteRows = false;
            this.dgvSyncDocsDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSyncDocsDetail.ContextMenuStrip = this.ctextMenuSync;
            this.dgvSyncDocsDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSyncDocsDetail.Location = new System.Drawing.Point(0, 0);
            this.dgvSyncDocsDetail.Name = "dgvSyncDocsDetail";
            this.dgvSyncDocsDetail.ReadOnly = true;
            this.dgvSyncDocsDetail.Size = new System.Drawing.Size(1008, 327);
            this.dgvSyncDocsDetail.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabDocs);
            this.tabControl1.Controls.Add(this.tabMasterFiles);
            this.tabControl1.Controls.Add(this.tabStock);
            this.tabControl1.Controls.Add(this.tabAuxiliares);
            this.tabControl1.Controls.Add(this.tabValidations);
            this.tabControl1.Controls.Add(this.tabUpdate);
            this.tabControl1.Controls.Add(this.tabUtilidades);
            this.tabControl1.Controls.Add(this.tabReasignacion);
            this.tabControl1.Controls.Add(this.tabCronJobs);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1382, 752);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabStock
            // 
            this.tabStock.Controls.Add(this.splitContainer7);
            this.tabStock.Location = new System.Drawing.Point(4, 29);
            this.tabStock.Name = "tabStock";
            this.tabStock.Size = new System.Drawing.Size(1374, 719);
            this.tabStock.TabIndex = 10;
            this.tabStock.Text = "Stock";
            this.tabStock.UseVisualStyleBackColor = true;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.grboxSelectStock);
            this.splitContainer7.Panel1.Controls.Add(this.grBoxAlmacenes);
            this.splitContainer7.Panel1.Controls.Add(this.btnQueryStockRepasat);
            this.splitContainer7.Panel1.Controls.Add(this.btnQueryStockA3ERP);
            this.splitContainer7.Panel1.Controls.Add(this.btnUpdateStockToRepasat);
            this.splitContainer7.Panel1.Controls.Add(this.btnUpdateStockToA3ERP);
            this.splitContainer7.Panel1.Controls.Add(this.grBoxSelectDatesStock);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.dgvStock);
            this.splitContainer7.Size = new System.Drawing.Size(1374, 719);
            this.splitContainer7.SplitterDistance = 325;
            this.splitContainer7.TabIndex = 0;
            // 
            // grboxSelectStock
            // 
            this.grboxSelectStock.Controls.Add(this.rbutStock);
            this.grboxSelectStock.Controls.Add(this.rbutStockActivity);
            this.grboxSelectStock.Location = new System.Drawing.Point(16, 12);
            this.grboxSelectStock.Name = "grboxSelectStock";
            this.grboxSelectStock.Size = new System.Drawing.Size(257, 92);
            this.grboxSelectStock.TabIndex = 48;
            this.grboxSelectStock.TabStop = false;
            this.grboxSelectStock.Text = "Gestionar Stock";
            // 
            // rbutStock
            // 
            this.rbutStock.AutoSize = true;
            this.rbutStock.Checked = true;
            this.rbutStock.Location = new System.Drawing.Point(25, 25);
            this.rbutStock.Name = "rbutStock";
            this.rbutStock.Size = new System.Drawing.Size(134, 24);
            this.rbutStock.TabIndex = 46;
            this.rbutStock.TabStop = true;
            this.rbutStock.Text = "Stock Almacén";
            this.rbutStock.UseVisualStyleBackColor = true;
            // 
            // rbutStockActivity
            // 
            this.rbutStockActivity.AutoSize = true;
            this.rbutStockActivity.Location = new System.Drawing.Point(25, 55);
            this.rbutStockActivity.Name = "rbutStockActivity";
            this.rbutStockActivity.Size = new System.Drawing.Size(153, 24);
            this.rbutStockActivity.TabIndex = 47;
            this.rbutStockActivity.Text = "Stock Actividades";
            this.rbutStockActivity.UseVisualStyleBackColor = true;
            this.rbutStockActivity.CheckedChanged += new System.EventHandler(this.rbutStockActivity_CheckedChanged);
            // 
            // grBoxAlmacenes
            // 
            this.grBoxAlmacenes.Controls.Add(this.checkBAlmacen);
            this.grBoxAlmacenes.Controls.Add(this.cboxAlmacen);
            this.grBoxAlmacenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxAlmacenes.Location = new System.Drawing.Point(16, 214);
            this.grBoxAlmacenes.Name = "grBoxAlmacenes";
            this.grBoxAlmacenes.Size = new System.Drawing.Size(200, 56);
            this.grBoxAlmacenes.TabIndex = 45;
            this.grBoxAlmacenes.TabStop = false;
            this.grBoxAlmacenes.Text = "Almacén";
            // 
            // checkBAlmacen
            // 
            this.checkBAlmacen.AutoSize = true;
            this.checkBAlmacen.Location = new System.Drawing.Point(12, 26);
            this.checkBAlmacen.Name = "checkBAlmacen";
            this.checkBAlmacen.Size = new System.Drawing.Size(15, 14);
            this.checkBAlmacen.TabIndex = 27;
            this.checkBAlmacen.UseVisualStyleBackColor = true;
            this.checkBAlmacen.CheckedChanged += new System.EventHandler(this.checkBAlmacen_CheckedChanged);
            // 
            // cboxAlmacen
            // 
            this.cboxAlmacen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxAlmacen.FormattingEnabled = true;
            this.cboxAlmacen.Location = new System.Drawing.Point(44, 21);
            this.cboxAlmacen.Name = "cboxAlmacen";
            this.cboxAlmacen.Size = new System.Drawing.Size(141, 24);
            this.cboxAlmacen.TabIndex = 25;
            this.cboxAlmacen.Visible = false;
            // 
            // btnQueryStockRepasat
            // 
            this.btnQueryStockRepasat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQueryStockRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnQueryStockRepasat.Image")));
            this.btnQueryStockRepasat.Location = new System.Drawing.Point(99, 276);
            this.btnQueryStockRepasat.Name = "btnQueryStockRepasat";
            this.btnQueryStockRepasat.Size = new System.Drawing.Size(48, 45);
            this.btnQueryStockRepasat.TabIndex = 44;
            this.btnQueryStockRepasat.UseVisualStyleBackColor = true;
            this.btnQueryStockRepasat.Click += new System.EventHandler(this.btnQueryStockRepasat_Click);
            // 
            // btnQueryStockA3ERP
            // 
            this.btnQueryStockA3ERP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQueryStockA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnQueryStockA3ERP.Image")));
            this.btnQueryStockA3ERP.Location = new System.Drawing.Point(148, 276);
            this.btnQueryStockA3ERP.Name = "btnQueryStockA3ERP";
            this.btnQueryStockA3ERP.Size = new System.Drawing.Size(48, 45);
            this.btnQueryStockA3ERP.TabIndex = 43;
            this.btnQueryStockA3ERP.UseVisualStyleBackColor = true;
            this.btnQueryStockA3ERP.Visible = false;
            this.btnQueryStockA3ERP.Click += new System.EventHandler(this.btnQueryStockA3ERP_Click);
            // 
            // btnUpdateStockToRepasat
            // 
            this.btnUpdateStockToRepasat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnUpdateStockToRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateStockToRepasat.Image")));
            this.btnUpdateStockToRepasat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateStockToRepasat.Location = new System.Drawing.Point(16, 276);
            this.btnUpdateStockToRepasat.Margin = new System.Windows.Forms.Padding(0);
            this.btnUpdateStockToRepasat.Name = "btnUpdateStockToRepasat";
            this.btnUpdateStockToRepasat.Size = new System.Drawing.Size(80, 45);
            this.btnUpdateStockToRepasat.TabIndex = 42;
            this.btnUpdateStockToRepasat.Text = "<<< ";
            this.btnUpdateStockToRepasat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdateStockToRepasat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateStockToRepasat.UseVisualStyleBackColor = true;
            this.btnUpdateStockToRepasat.Visible = false;
            this.btnUpdateStockToRepasat.Click += new System.EventHandler(this.btnUpdateStockToRepasat_Click);
            // 
            // btnUpdateStockToA3ERP
            // 
            this.btnUpdateStockToA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateStockToA3ERP.Image")));
            this.btnUpdateStockToA3ERP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateStockToA3ERP.Location = new System.Drawing.Point(199, 276);
            this.btnUpdateStockToA3ERP.Name = "btnUpdateStockToA3ERP";
            this.btnUpdateStockToA3ERP.Size = new System.Drawing.Size(83, 45);
            this.btnUpdateStockToA3ERP.TabIndex = 41;
            this.btnUpdateStockToA3ERP.Text = ">>>";
            this.btnUpdateStockToA3ERP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpdateStockToA3ERP.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnUpdateStockToA3ERP.UseVisualStyleBackColor = true;
            this.btnUpdateStockToA3ERP.Visible = false;
            this.btnUpdateStockToA3ERP.Click += new System.EventHandler(this.btnUpdateStockToA3ERP_Click);
            // 
            // grBoxSelectDatesStock
            // 
            this.grBoxSelectDatesStock.Controls.Add(this.dtPickerFromStock);
            this.grBoxSelectDatesStock.Controls.Add(this.dtPickerToStock);
            this.grBoxSelectDatesStock.Controls.Add(this.label2);
            this.grBoxSelectDatesStock.Controls.Add(this.label15);
            this.grBoxSelectDatesStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxSelectDatesStock.Location = new System.Drawing.Point(16, 110);
            this.grBoxSelectDatesStock.Name = "grBoxSelectDatesStock";
            this.grBoxSelectDatesStock.Size = new System.Drawing.Size(257, 98);
            this.grBoxSelectDatesStock.TabIndex = 40;
            this.grBoxSelectDatesStock.TabStop = false;
            this.grBoxSelectDatesStock.Text = "Fechas";
            this.grBoxSelectDatesStock.Visible = false;
            // 
            // dtPickerFromStock
            // 
            this.dtPickerFromStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPickerFromStock.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPickerFromStock.Location = new System.Drawing.Point(12, 41);
            this.dtPickerFromStock.Name = "dtPickerFromStock";
            this.dtPickerFromStock.Size = new System.Drawing.Size(103, 26);
            this.dtPickerFromStock.TabIndex = 3;
            // 
            // dtPickerToStock
            // 
            this.dtPickerToStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtPickerToStock.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPickerToStock.Location = new System.Drawing.Point(141, 41);
            this.dtPickerToStock.Name = "dtPickerToStock";
            this.dtPickerToStock.Size = new System.Drawing.Size(102, 26);
            this.dtPickerToStock.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Desde";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(138, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 16);
            this.label15.TabIndex = 7;
            this.label15.Text = "Hasta";
            // 
            // dgvStock
            // 
            this.dgvStock.AllowUserToAddRows = false;
            this.dgvStock.AllowUserToDeleteRows = false;
            this.dgvStock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvStock.ContextMenuStrip = this.ctextMenuStock;
            this.dgvStock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStock.Location = new System.Drawing.Point(0, 0);
            this.dgvStock.Name = "dgvStock";
            this.dgvStock.ReadOnly = true;
            this.dgvStock.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStock.Size = new System.Drawing.Size(1045, 719);
            this.dgvStock.TabIndex = 0;
            this.dgvStock.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvStock_CellFormatting);
            // 
            // ctextMenuStock
            // 
            this.ctextMenuStock.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regularizarEnA3ERPToolStripMenuItem});
            this.ctextMenuStock.Name = "ctextMenuStock";
            this.ctextMenuStock.Size = new System.Drawing.Size(186, 26);
            // 
            // regularizarEnA3ERPToolStripMenuItem
            // 
            this.regularizarEnA3ERPToolStripMenuItem.Name = "regularizarEnA3ERPToolStripMenuItem";
            this.regularizarEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.regularizarEnA3ERPToolStripMenuItem.Text = "Regularizar en A3ERP";
            this.regularizarEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.regularizarEnA3ERPToolStripMenuItem_Click);
            // 
            // tabValidations
            // 
            this.tabValidations.Controls.Add(this.splitContainer9);
            this.tabValidations.Location = new System.Drawing.Point(4, 29);
            this.tabValidations.Name = "tabValidations";
            this.tabValidations.Size = new System.Drawing.Size(1374, 719);
            this.tabValidations.TabIndex = 11;
            this.tabValidations.Text = "Validaciones";
            this.tabValidations.UseVisualStyleBackColor = true;
            // 
            // splitContainer9
            // 
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.Location = new System.Drawing.Point(0, 0);
            this.splitContainer9.Name = "splitContainer9";
            this.splitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.Controls.Add(this.btnTestConexionesBD);
            this.splitContainer9.Panel1.Controls.Add(this.btnLoadValidationData);
            this.splitContainer9.Panel1.Controls.Add(this.cboxvalidatingMaster);
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.Controls.Add(this.dgvValidatingData);
            this.splitContainer9.Size = new System.Drawing.Size(1374, 719);
            this.splitContainer9.SplitterDistance = 105;
            this.splitContainer9.TabIndex = 0;
            // 
            // btnTestConexionesBD
            // 
            this.btnTestConexionesBD.Location = new System.Drawing.Point(427, 17);
            this.btnTestConexionesBD.Name = "btnTestConexionesBD";
            this.btnTestConexionesBD.Size = new System.Drawing.Size(187, 34);
            this.btnTestConexionesBD.TabIndex = 23;
            this.btnTestConexionesBD.Text = "Test conexiones a BD";
            this.btnTestConexionesBD.UseVisualStyleBackColor = true;
            this.btnTestConexionesBD.Click += new System.EventHandler(this.btnTestConexionesBD_Click);
            // 
            // btnLoadValidationData
            // 
            this.btnLoadValidationData.Location = new System.Drawing.Point(192, 17);
            this.btnLoadValidationData.Name = "btnLoadValidationData";
            this.btnLoadValidationData.Size = new System.Drawing.Size(87, 28);
            this.btnLoadValidationData.TabIndex = 22;
            this.btnLoadValidationData.Text = "&Cargar";
            this.btnLoadValidationData.UseVisualStyleBackColor = true;
            this.btnLoadValidationData.Click += new System.EventHandler(this.btnLoadValidationData_Click);
            // 
            // cboxvalidatingMaster
            // 
            this.cboxvalidatingMaster.FormattingEnabled = true;
            this.cboxvalidatingMaster.Items.AddRange(new object[] {
            "Clientes",
            "Proveedores",
            "Productos",
            "Direcciones Clientes",
            "Direcciones Proveedores"});
            this.cboxvalidatingMaster.Location = new System.Drawing.Point(17, 17);
            this.cboxvalidatingMaster.Name = "cboxvalidatingMaster";
            this.cboxvalidatingMaster.Size = new System.Drawing.Size(147, 28);
            this.cboxvalidatingMaster.TabIndex = 21;
            this.cboxvalidatingMaster.Text = "Clientes";
            // 
            // dgvValidatingData
            // 
            this.dgvValidatingData.AllowUserToAddRows = false;
            this.dgvValidatingData.AllowUserToDeleteRows = false;
            this.dgvValidatingData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValidatingData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvValidatingData.Location = new System.Drawing.Point(0, 0);
            this.dgvValidatingData.Name = "dgvValidatingData";
            this.dgvValidatingData.ReadOnly = true;
            this.dgvValidatingData.Size = new System.Drawing.Size(1374, 610);
            this.dgvValidatingData.TabIndex = 0;
            // 
            // tabUpdate
            // 
            this.tabUpdate.Controls.Add(this.testcc);
            this.tabUpdate.Controls.Add(this.btnTestConnect1);
            this.tabUpdate.Controls.Add(this.dtpDateUpdateClientes);
            this.tabUpdate.Controls.Add(this.btnCrearDirecciones);
            this.tabUpdate.Controls.Add(this.dtpickerResetFrom);
            this.tabUpdate.Controls.Add(this.label13);
            this.tabUpdate.Controls.Add(this.label12);
            this.tabUpdate.Controls.Add(this.label11);
            this.tabUpdate.Controls.Add(this.label10);
            this.tabUpdate.Controls.Add(this.label9);
            this.tabUpdate.Controls.Add(this.label8);
            this.tabUpdate.Controls.Add(this.label7);
            this.tabUpdate.Controls.Add(this.label6);
            this.tabUpdate.Controls.Add(this.label5);
            this.tabUpdate.Controls.Add(this.label4);
            this.tabUpdate.Controls.Add(this.label3);
            this.tabUpdate.Controls.Add(this.button7);
            this.tabUpdate.Controls.Add(this.button5);
            this.tabUpdate.Controls.Add(this.button4);
            this.tabUpdate.Controls.Add(this.button3);
            this.tabUpdate.Controls.Add(this.btnUpdateProveedorsRPST);
            this.tabUpdate.Controls.Add(this.btnBanksToRepasat);
            this.tabUpdate.Controls.Add(this.btnGestionarBancosEnA3ERP);
            this.tabUpdate.Controls.Add(this.btnUpdateClientes);
            this.tabUpdate.Controls.Add(this.btnFullUpdate);
            this.tabUpdate.Controls.Add(this.btnUpdateClientesA3ERP);
            this.tabUpdate.Controls.Add(this.button6);
            this.tabUpdate.Controls.Add(this.btnGestionarDireccionesEnA3ERP);
            this.tabUpdate.Controls.Add(this.btnContactsTorepasat);
            this.tabUpdate.Controls.Add(this.btnContactsToA3);
            this.tabUpdate.Controls.Add(this.btnResetUpdateDateSync);
            this.tabUpdate.Controls.Add(this.btnUpdateProducts);
            this.tabUpdate.Controls.Add(this.btnProductUpdateToRepasat);
            this.tabUpdate.Controls.Add(this.btnUpdateClientesRPST);
            this.tabUpdate.Controls.Add(this.btnUpdateArticulosFromRPSTToA3);
            this.tabUpdate.Controls.Add(this.btnProveedores);
            this.tabUpdate.Location = new System.Drawing.Point(4, 29);
            this.tabUpdate.Name = "tabUpdate";
            this.tabUpdate.Padding = new System.Windows.Forms.Padding(3);
            this.tabUpdate.Size = new System.Drawing.Size(1374, 719);
            this.tabUpdate.TabIndex = 7;
            this.tabUpdate.Text = "Actualizaciones";
            this.tabUpdate.UseVisualStyleBackColor = true;
            // 
            // testcc
            // 
            this.testcc.Location = new System.Drawing.Point(81, 0);
            this.testcc.Name = "testcc";
            this.testcc.Size = new System.Drawing.Size(35, 23);
            this.testcc.TabIndex = 39;
            this.testcc.UseVisualStyleBackColor = true;
            this.testcc.Click += new System.EventHandler(this.testcc_Click);
            // 
            // btnTestConnect1
            // 
            this.btnTestConnect1.Location = new System.Drawing.Point(0, 0);
            this.btnTestConnect1.Name = "btnTestConnect1";
            this.btnTestConnect1.Size = new System.Drawing.Size(75, 23);
            this.btnTestConnect1.TabIndex = 0;
            // 
            // dtpDateUpdateClientes
            // 
            this.dtpDateUpdateClientes.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateUpdateClientes.Location = new System.Drawing.Point(476, 158);
            this.dtpDateUpdateClientes.Name = "dtpDateUpdateClientes";
            this.dtpDateUpdateClientes.Size = new System.Drawing.Size(106, 26);
            this.dtpDateUpdateClientes.TabIndex = 37;
            // 
            // btnCrearDirecciones
            // 
            this.btnCrearDirecciones.Image = ((System.Drawing.Image)(resources.GetObject("btnCrearDirecciones.Image")));
            this.btnCrearDirecciones.Location = new System.Drawing.Point(476, 290);
            this.btnCrearDirecciones.Name = "btnCrearDirecciones";
            this.btnCrearDirecciones.Size = new System.Drawing.Size(40, 40);
            this.btnCrearDirecciones.TabIndex = 36;
            this.btnCrearDirecciones.UseVisualStyleBackColor = true;
            this.btnCrearDirecciones.Click += new System.EventHandler(this.btnCrearDirecciones_Click);
            // 
            // dtpickerResetFrom
            // 
            this.dtpickerResetFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickerResetFrom.Location = new System.Drawing.Point(297, 438);
            this.dtpickerResetFrom.Name = "dtpickerResetFrom";
            this.dtpickerResetFrom.Size = new System.Drawing.Size(120, 26);
            this.dtpickerResetFrom.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(84, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(201, 20);
            this.label13.TabIndex = 29;
            this.label13.Text = "Actualizar Bancos Cuentas";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(623, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 20);
            this.label12.TabIndex = 26;
            this.label12.Text = "eSync";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 20);
            this.label11.TabIndex = 23;
            this.label11.Text = "ActualizarTodo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 438);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(163, 20);
            this.label10.TabIndex = 22;
            this.label10.Text = "Resetear Fecha Sync";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(84, 392);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 20);
            this.label9.TabIndex = 21;
            this.label9.Text = "Actualizar Artículos";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(84, 300);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 20);
            this.label8.TabIndex = 20;
            this.label8.Text = "Actualizar Direcciones";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 254);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 20);
            this.label7.TabIndex = 18;
            this.label7.Text = "Actualizar Contactos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(84, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Actualizar Proveedores";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(413, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "A3ERP>>Repasat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(253, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Repasat >> A3ERP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Actualizar Clientes";
            // 
            // button7
            // 
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(627, 333);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 40);
            this.button7.TabIndex = 34;
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(627, 288);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 40);
            this.button5.TabIndex = 33;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(627, 243);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 40);
            this.button4.TabIndex = 32;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(627, 198);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 40);
            this.button3.TabIndex = 31;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnUpdateProveedorsRPST
            // 
            this.btnUpdateProveedorsRPST.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateProveedorsRPST.Image")));
            this.btnUpdateProveedorsRPST.Location = new System.Drawing.Point(430, 198);
            this.btnUpdateProveedorsRPST.Name = "btnUpdateProveedorsRPST";
            this.btnUpdateProveedorsRPST.Size = new System.Drawing.Size(40, 40);
            this.btnUpdateProveedorsRPST.TabIndex = 30;
            this.btnUpdateProveedorsRPST.UseVisualStyleBackColor = true;
            this.btnUpdateProveedorsRPST.Click += new System.EventHandler(this.btnUpdateProveedorsRPST_Click);
            // 
            // btnBanksToRepasat
            // 
            this.btnBanksToRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnBanksToRepasat.Image")));
            this.btnBanksToRepasat.Location = new System.Drawing.Point(430, 333);
            this.btnBanksToRepasat.Name = "btnBanksToRepasat";
            this.btnBanksToRepasat.Size = new System.Drawing.Size(40, 40);
            this.btnBanksToRepasat.TabIndex = 28;
            this.btnBanksToRepasat.UseVisualStyleBackColor = true;
            this.btnBanksToRepasat.Click += new System.EventHandler(this.btnBanksToRepasat_Click);
            // 
            // btnGestionarBancosEnA3ERP
            // 
            this.btnGestionarBancosEnA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnGestionarBancosEnA3ERP.Image")));
            this.btnGestionarBancosEnA3ERP.Location = new System.Drawing.Point(297, 333);
            this.btnGestionarBancosEnA3ERP.Name = "btnGestionarBancosEnA3ERP";
            this.btnGestionarBancosEnA3ERP.Size = new System.Drawing.Size(40, 40);
            this.btnGestionarBancosEnA3ERP.TabIndex = 27;
            this.btnGestionarBancosEnA3ERP.UseVisualStyleBackColor = true;
            this.btnGestionarBancosEnA3ERP.Click += new System.EventHandler(this.btnGestionarBancosEnA3ERP_Click);
            // 
            // btnUpdateClientes
            // 
            this.btnUpdateClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateClientes.Image")));
            this.btnUpdateClientes.Location = new System.Drawing.Point(627, 153);
            this.btnUpdateClientes.Name = "btnUpdateClientes";
            this.btnUpdateClientes.Size = new System.Drawing.Size(40, 40);
            this.btnUpdateClientes.TabIndex = 25;
            this.btnUpdateClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateClientes.UseVisualStyleBackColor = true;
            this.btnUpdateClientes.Click += new System.EventHandler(this.btnUpdateClientes_Click);
            // 
            // btnFullUpdate
            // 
            this.btnFullUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnFullUpdate.Image")));
            this.btnFullUpdate.Location = new System.Drawing.Point(297, 61);
            this.btnFullUpdate.Name = "btnFullUpdate";
            this.btnFullUpdate.Size = new System.Drawing.Size(40, 40);
            this.btnFullUpdate.TabIndex = 24;
            this.btnFullUpdate.UseVisualStyleBackColor = true;
            // 
            // btnUpdateClientesA3ERP
            // 
            this.btnUpdateClientesA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateClientesA3ERP.Image")));
            this.btnUpdateClientesA3ERP.Location = new System.Drawing.Point(298, 153);
            this.btnUpdateClientesA3ERP.Name = "btnUpdateClientesA3ERP";
            this.btnUpdateClientesA3ERP.Size = new System.Drawing.Size(40, 40);
            this.btnUpdateClientesA3ERP.TabIndex = 19;
            this.btnUpdateClientesA3ERP.UseVisualStyleBackColor = true;
            this.btnUpdateClientesA3ERP.Click += new System.EventHandler(this.btnUpdateClientesA3ERP_Click);
            // 
            // button6
            // 
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(430, 288);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 40);
            this.button6.TabIndex = 13;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnGestionarDireccionesEnA3ERP
            // 
            this.btnGestionarDireccionesEnA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btnGestionarDireccionesEnA3ERP.Image")));
            this.btnGestionarDireccionesEnA3ERP.Location = new System.Drawing.Point(297, 288);
            this.btnGestionarDireccionesEnA3ERP.Name = "btnGestionarDireccionesEnA3ERP";
            this.btnGestionarDireccionesEnA3ERP.Size = new System.Drawing.Size(40, 40);
            this.btnGestionarDireccionesEnA3ERP.TabIndex = 12;
            this.btnGestionarDireccionesEnA3ERP.UseVisualStyleBackColor = true;
            this.btnGestionarDireccionesEnA3ERP.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnContactsTorepasat
            // 
            this.btnContactsTorepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnContactsTorepasat.Image")));
            this.btnContactsTorepasat.Location = new System.Drawing.Point(430, 243);
            this.btnContactsTorepasat.Name = "btnContactsTorepasat";
            this.btnContactsTorepasat.Size = new System.Drawing.Size(40, 40);
            this.btnContactsTorepasat.TabIndex = 11;
            this.btnContactsTorepasat.UseVisualStyleBackColor = true;
            this.btnContactsTorepasat.Click += new System.EventHandler(this.btnContactsTorepasat_Click);
            // 
            // btnContactsToA3
            // 
            this.btnContactsToA3.Image = ((System.Drawing.Image)(resources.GetObject("btnContactsToA3.Image")));
            this.btnContactsToA3.Location = new System.Drawing.Point(298, 243);
            this.btnContactsToA3.Name = "btnContactsToA3";
            this.btnContactsToA3.Size = new System.Drawing.Size(40, 40);
            this.btnContactsToA3.TabIndex = 10;
            this.btnContactsToA3.UseVisualStyleBackColor = true;
            this.btnContactsToA3.Click += new System.EventHandler(this.btnContactsToA3_Click);
            // 
            // btnResetUpdateDateSync
            // 
            this.btnResetUpdateDateSync.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnResetUpdateDateSync.BackgroundImage")));
            this.btnResetUpdateDateSync.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnResetUpdateDateSync.Location = new System.Drawing.Point(430, 438);
            this.btnResetUpdateDateSync.Name = "btnResetUpdateDateSync";
            this.btnResetUpdateDateSync.Size = new System.Drawing.Size(29, 26);
            this.btnResetUpdateDateSync.TabIndex = 7;
            this.btnResetUpdateDateSync.UseVisualStyleBackColor = true;
            this.btnResetUpdateDateSync.Click += new System.EventHandler(this.btnResetUpdateDateSync_Click);
            // 
            // btnUpdateProducts
            // 
            this.btnUpdateProducts.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateProducts.Image")));
            this.btnUpdateProducts.Location = new System.Drawing.Point(627, 378);
            this.btnUpdateProducts.Name = "btnUpdateProducts";
            this.btnUpdateProducts.Size = new System.Drawing.Size(40, 40);
            this.btnUpdateProducts.TabIndex = 6;
            this.btnUpdateProducts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateProducts.UseVisualStyleBackColor = true;
            this.btnUpdateProducts.Click += new System.EventHandler(this.btnUpdateProducts_Click);
            // 
            // btnProductUpdateToRepasat
            // 
            this.btnProductUpdateToRepasat.Image = ((System.Drawing.Image)(resources.GetObject("btnProductUpdateToRepasat.Image")));
            this.btnProductUpdateToRepasat.Location = new System.Drawing.Point(430, 378);
            this.btnProductUpdateToRepasat.Name = "btnProductUpdateToRepasat";
            this.btnProductUpdateToRepasat.Size = new System.Drawing.Size(40, 40);
            this.btnProductUpdateToRepasat.TabIndex = 5;
            this.btnProductUpdateToRepasat.UseVisualStyleBackColor = true;
            this.btnProductUpdateToRepasat.Click += new System.EventHandler(this.btnProductUpdateToRepasat_Click);
            // 
            // btnUpdateClientesRPST
            // 
            this.btnUpdateClientesRPST.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdateClientesRPST.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnUpdateClientesRPST.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateClientesRPST.Image")));
            this.btnUpdateClientesRPST.Location = new System.Drawing.Point(430, 153);
            this.btnUpdateClientesRPST.Name = "btnUpdateClientesRPST";
            this.btnUpdateClientesRPST.Size = new System.Drawing.Size(40, 34);
            this.btnUpdateClientesRPST.TabIndex = 4;
            this.btnUpdateClientesRPST.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateClientesRPST.UseVisualStyleBackColor = true;
            this.btnUpdateClientesRPST.Click += new System.EventHandler(this.btnUpdateClientesRPST_Click);
            // 
            // btnUpdateArticulosFromRPSTToA3
            // 
            this.btnUpdateArticulosFromRPSTToA3.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateArticulosFromRPSTToA3.Image")));
            this.btnUpdateArticulosFromRPSTToA3.Location = new System.Drawing.Point(297, 378);
            this.btnUpdateArticulosFromRPSTToA3.Name = "btnUpdateArticulosFromRPSTToA3";
            this.btnUpdateArticulosFromRPSTToA3.Size = new System.Drawing.Size(40, 40);
            this.btnUpdateArticulosFromRPSTToA3.TabIndex = 2;
            this.btnUpdateArticulosFromRPSTToA3.UseVisualStyleBackColor = true;
            this.btnUpdateArticulosFromRPSTToA3.Click += new System.EventHandler(this.btnArticulos_Click);
            // 
            // btnProveedores
            // 
            this.btnProveedores.ForeColor = System.Drawing.Color.Black;
            this.btnProveedores.Image = ((System.Drawing.Image)(resources.GetObject("btnProveedores.Image")));
            this.btnProveedores.Location = new System.Drawing.Point(298, 198);
            this.btnProveedores.Name = "btnProveedores";
            this.btnProveedores.Size = new System.Drawing.Size(40, 40);
            this.btnProveedores.TabIndex = 1;
            this.btnProveedores.UseVisualStyleBackColor = true;
            this.btnProveedores.Click += new System.EventHandler(this.btnProveedores_Click);
            // 
            // tabUtilidades
            // 
            this.tabUtilidades.Controls.Add(this.splitContainer6);
            this.tabUtilidades.Location = new System.Drawing.Point(4, 29);
            this.tabUtilidades.Name = "tabUtilidades";
            this.tabUtilidades.Size = new System.Drawing.Size(1374, 719);
            this.tabUtilidades.TabIndex = 9;
            this.tabUtilidades.Text = "Utilidades";
            this.tabUtilidades.UseVisualStyleBackColor = true;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.btklstic);
            this.splitContainer6.Panel1.Controls.Add(this.btnApex);
            this.splitContainer6.Panel1.Controls.Add(this.button10);
            this.splitContainer6.Panel1.Controls.Add(this.btnAsientosUtils);
            this.splitContainer6.Panel1.Controls.Add(this.btnContactos);
            this.splitContainer6.Panel1.Controls.Add(this.btnRegStock);
            this.splitContainer6.Panel1.Controls.Add(this.btnCheckDombanca);
            this.splitContainer6.Panel1.Controls.Add(this.btnConfirmarMandato);
            this.splitContainer6.Panel1.Controls.Add(this.btnAsignBankRecibos);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.dgvUtilidades);
            this.splitContainer6.Size = new System.Drawing.Size(1374, 719);
            this.splitContainer6.SplitterDistance = 235;
            this.splitContainer6.TabIndex = 0;
            this.splitContainer6.Visible = false;
            // 
            // btklstic
            // 
            this.btklstic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btklstic.Location = new System.Drawing.Point(16, 298);
            this.btklstic.Name = "btklstic";
            this.btklstic.Size = new System.Drawing.Size(142, 44);
            this.btklstic.TabIndex = 12;
            this.btklstic.Text = "KLSTIC";
            this.btklstic.UseVisualStyleBackColor = true;
            this.btklstic.Click += new System.EventHandler(this.btklstic_Click);
            // 
            // btnApex
            // 
            this.btnApex.Location = new System.Drawing.Point(16, 635);
            this.btnApex.Name = "btnApex";
            this.btnApex.Size = new System.Drawing.Size(142, 40);
            this.btnApex.TabIndex = 11;
            this.btnApex.Text = "Puntes";
            this.btnApex.UseVisualStyleBackColor = true;
            this.btnApex.Click += new System.EventHandler(this.btnApex_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(16, 589);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(142, 40);
            this.button10.TabIndex = 10;
            this.button10.Text = "ASIENTOS2";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnAsientosUtils
            // 
            this.btnAsientosUtils.Location = new System.Drawing.Point(16, 543);
            this.btnAsientosUtils.Name = "btnAsientosUtils";
            this.btnAsientosUtils.Size = new System.Drawing.Size(142, 40);
            this.btnAsientosUtils.TabIndex = 9;
            this.btnAsientosUtils.Text = "ASIENTOS";
            this.btnAsientosUtils.UseVisualStyleBackColor = true;
            this.btnAsientosUtils.Click += new System.EventHandler(this.btnAsientosUtils_Click);
            // 
            // btnContactos
            // 
            this.btnContactos.Location = new System.Drawing.Point(16, 348);
            this.btnContactos.Name = "btnContactos";
            this.btnContactos.Size = new System.Drawing.Size(142, 40);
            this.btnContactos.TabIndex = 7;
            this.btnContactos.Text = "Contactos";
            this.btnContactos.UseVisualStyleBackColor = true;
            this.btnContactos.Visible = false;
            this.btnContactos.Click += new System.EventHandler(this.btnContactos_Click);
            // 
            // btnRegStock
            // 
            this.btnRegStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegStock.Location = new System.Drawing.Point(16, 248);
            this.btnRegStock.Name = "btnRegStock";
            this.btnRegStock.Size = new System.Drawing.Size(142, 44);
            this.btnRegStock.TabIndex = 4;
            this.btnRegStock.Text = "stock";
            this.btnRegStock.UseVisualStyleBackColor = true;
            this.btnRegStock.Click += new System.EventHandler(this.btnRegStock_Click);
            // 
            // btnCheckDombanca
            // 
            this.btnCheckDombanca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckDombanca.Location = new System.Drawing.Point(16, 135);
            this.btnCheckDombanca.Name = "btnCheckDombanca";
            this.btnCheckDombanca.Size = new System.Drawing.Size(142, 52);
            this.btnCheckDombanca.TabIndex = 2;
            this.btnCheckDombanca.Text = "Verificar cuentas bancarias en A3ERP";
            this.btnCheckDombanca.UseVisualStyleBackColor = true;
            this.btnCheckDombanca.Click += new System.EventHandler(this.btnCheckDombanca_Click);
            // 
            // btnConfirmarMandato
            // 
            this.btnConfirmarMandato.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmarMandato.Location = new System.Drawing.Point(16, 85);
            this.btnConfirmarMandato.Name = "btnConfirmarMandato";
            this.btnConfirmarMandato.Size = new System.Drawing.Size(142, 44);
            this.btnConfirmarMandato.TabIndex = 1;
            this.btnConfirmarMandato.Text = "Confirmar Mandatos Bancos";
            this.btnConfirmarMandato.UseVisualStyleBackColor = true;
            this.btnConfirmarMandato.Click += new System.EventHandler(this.btnConfirmarMandato_Click);
            // 
            // btnAsignBankRecibos
            // 
            this.btnAsignBankRecibos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAsignBankRecibos.Location = new System.Drawing.Point(16, 25);
            this.btnAsignBankRecibos.Name = "btnAsignBankRecibos";
            this.btnAsignBankRecibos.Size = new System.Drawing.Size(142, 44);
            this.btnAsignBankRecibos.TabIndex = 0;
            this.btnAsignBankRecibos.Text = "Asignar Banco a Recibos";
            this.btnAsignBankRecibos.UseVisualStyleBackColor = true;
            this.btnAsignBankRecibos.Click += new System.EventHandler(this.btnAsignBankRecibos_Click);
            // 
            // dgvUtilidades
            // 
            this.dgvUtilidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUtilidades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUtilidades.Location = new System.Drawing.Point(0, 0);
            this.dgvUtilidades.Name = "dgvUtilidades";
            this.dgvUtilidades.Size = new System.Drawing.Size(1135, 719);
            this.dgvUtilidades.TabIndex = 0;
            // 
            // tabReasignacion
            // 
            this.tabReasignacion.BackColor = System.Drawing.SystemColors.Menu;
            this.tabReasignacion.Controls.Add(this.label17);
            this.tabReasignacion.Controls.Add(this.label18);
            this.tabReasignacion.Controls.Add(this.tbIdDomBancaDestino);
            this.tabReasignacion.Controls.Add(this.tbIdDomBancaOrigen);
            this.tabReasignacion.Controls.Add(this.lbIdDomBancaD);
            this.tabReasignacion.Controls.Add(this.lbIdDomBancaO);
            this.tabReasignacion.Controls.Add(this.lbIdDirentD);
            this.tabReasignacion.Controls.Add(this.lbIdDirentO);
            this.tabReasignacion.Controls.Add(this.tbIdDirentD);
            this.tabReasignacion.Controls.Add(this.tbIdDirentO);
            this.tabReasignacion.Controls.Add(this.IdDirentD);
            this.tabReasignacion.Controls.Add(this.IdDirentO);
            this.tabReasignacion.Controls.Add(this.lbCTAPROVISIONNPGCDestino);
            this.tabReasignacion.Controls.Add(this.lbCTAPROVISIONDestino);
            this.tabReasignacion.Controls.Add(this.lbCTAPORCUENTADestino);
            this.tabReasignacion.Controls.Add(this.lbCTALETRADestino);
            this.tabReasignacion.Controls.Add(this.lbCTADESCDestino);
            this.tabReasignacion.Controls.Add(this.lbcuentaDestino);
            this.tabReasignacion.Controls.Add(this.lbcodCliD);
            this.tabReasignacion.Controls.Add(this.lbCTAPROVISIONNPGCOrigen);
            this.tabReasignacion.Controls.Add(this.lbCTAPROVISIONOrigen);
            this.tabReasignacion.Controls.Add(this.lbCTAPORCUENTAOrigen);
            this.tabReasignacion.Controls.Add(this.lbCTALETRAOrigen);
            this.tabReasignacion.Controls.Add(this.lbCTADESCOrigen);
            this.tabReasignacion.Controls.Add(this.lbCuentaOrigen);
            this.tabReasignacion.Controls.Add(this.lbCodCliO);
            this.tabReasignacion.Controls.Add(this.tbCTAPROVISIONNPGCDestino);
            this.tabReasignacion.Controls.Add(this.tbCTAPROVISIONDestino);
            this.tabReasignacion.Controls.Add(this.tbCTAPORCUENTADestino);
            this.tabReasignacion.Controls.Add(this.tbCTALETRADestino);
            this.tabReasignacion.Controls.Add(this.tbCTADESCDestino);
            this.tabReasignacion.Controls.Add(this.tbcuentaDestino);
            this.tabReasignacion.Controls.Add(this.tbCodCliDestino);
            this.tabReasignacion.Controls.Add(this.tbCTAPROVISIONNPGCOrigen);
            this.tabReasignacion.Controls.Add(this.tbCTAPROVISIONOrigen);
            this.tabReasignacion.Controls.Add(this.tbCTAPORCUENTAOrigen);
            this.tabReasignacion.Controls.Add(this.tbCTALETRAOrigen);
            this.tabReasignacion.Controls.Add(this.tbCTADESCOrigen);
            this.tabReasignacion.Controls.Add(this.tbCuentaOrigen);
            this.tabReasignacion.Controls.Add(this.tbCodCliOrigen);
            this.tabReasignacion.Controls.Add(this.rbClientes);
            this.tabReasignacion.Controls.Add(this.rbProveedores);
            this.tabReasignacion.Controls.Add(this.btReAsing);
            this.tabReasignacion.Controls.Add(this.CTAPROVISIONNPGCDestino);
            this.tabReasignacion.Controls.Add(this.CTAPROVISIONDestino);
            this.tabReasignacion.Controls.Add(this.CTAPORCUENTADestino);
            this.tabReasignacion.Controls.Add(this.CTALETRADestino);
            this.tabReasignacion.Controls.Add(this.CTAPROVISIONNPGCOrigen);
            this.tabReasignacion.Controls.Add(this.CTAPROVISIONOrigen);
            this.tabReasignacion.Controls.Add(this.CTAPORCUENTAOrigen);
            this.tabReasignacion.Controls.Add(this.CTALETRAOrigen);
            this.tabReasignacion.Controls.Add(this.CTADESCDestino);
            this.tabReasignacion.Controls.Add(this.cuentaDestino);
            this.tabReasignacion.Controls.Add(this.codCliD);
            this.tabReasignacion.Controls.Add(this.CTADESCOrigen);
            this.tabReasignacion.Controls.Add(this.cuentaOrigen);
            this.tabReasignacion.Controls.Add(this.cbClienteDestino);
            this.tabReasignacion.Controls.Add(this.cbClienteOrigen);
            this.tabReasignacion.Controls.Add(this.codCliO);
            this.tabReasignacion.Controls.Add(this.textoComboboxDestino);
            this.tabReasignacion.Controls.Add(this.textoComboboxOrigen);
            this.tabReasignacion.Location = new System.Drawing.Point(4, 29);
            this.tabReasignacion.Name = "tabReasignacion";
            this.tabReasignacion.Padding = new System.Windows.Forms.Padding(3);
            this.tabReasignacion.Size = new System.Drawing.Size(1374, 719);
            this.tabReasignacion.TabIndex = 12;
            this.tabReasignacion.Text = "Reasignacion";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(792, 187);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(0, 20);
            this.label17.TabIndex = 67;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(308, 189);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 20);
            this.label18.TabIndex = 66;
            // 
            // tbIdDomBancaDestino
            // 
            this.tbIdDomBancaDestino.Enabled = false;
            this.tbIdDomBancaDestino.Location = new System.Drawing.Point(919, 183);
            this.tbIdDomBancaDestino.Name = "tbIdDomBancaDestino";
            this.tbIdDomBancaDestino.Size = new System.Drawing.Size(120, 26);
            this.tbIdDomBancaDestino.TabIndex = 65;
            // 
            // tbIdDomBancaOrigen
            // 
            this.tbIdDomBancaOrigen.Enabled = false;
            this.tbIdDomBancaOrigen.Location = new System.Drawing.Point(426, 183);
            this.tbIdDomBancaOrigen.Name = "tbIdDomBancaOrigen";
            this.tbIdDomBancaOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbIdDomBancaOrigen.TabIndex = 64;
            // 
            // lbIdDomBancaD
            // 
            this.lbIdDomBancaD.AutoSize = true;
            this.lbIdDomBancaD.Location = new System.Drawing.Point(575, 187);
            this.lbIdDomBancaD.Name = "lbIdDomBancaD";
            this.lbIdDomBancaD.Size = new System.Drawing.Size(103, 20);
            this.lbIdDomBancaD.TabIndex = 63;
            this.lbIdDomBancaD.Text = "IdDomBanca";
            // 
            // lbIdDomBancaO
            // 
            this.lbIdDomBancaO.AutoSize = true;
            this.lbIdDomBancaO.Location = new System.Drawing.Point(82, 189);
            this.lbIdDomBancaO.Name = "lbIdDomBancaO";
            this.lbIdDomBancaO.Size = new System.Drawing.Size(103, 20);
            this.lbIdDomBancaO.TabIndex = 62;
            this.lbIdDomBancaO.Text = "IdDomBanca";
            // 
            // lbIdDirentD
            // 
            this.lbIdDirentD.AutoSize = true;
            this.lbIdDirentD.Location = new System.Drawing.Point(792, 154);
            this.lbIdDirentD.Name = "lbIdDirentD";
            this.lbIdDirentD.Size = new System.Drawing.Size(0, 20);
            this.lbIdDirentD.TabIndex = 61;
            // 
            // lbIdDirentO
            // 
            this.lbIdDirentO.AutoSize = true;
            this.lbIdDirentO.Location = new System.Drawing.Point(308, 156);
            this.lbIdDirentO.Name = "lbIdDirentO";
            this.lbIdDirentO.Size = new System.Drawing.Size(0, 20);
            this.lbIdDirentO.TabIndex = 60;
            // 
            // tbIdDirentD
            // 
            this.tbIdDirentD.Enabled = false;
            this.tbIdDirentD.Location = new System.Drawing.Point(919, 150);
            this.tbIdDirentD.Name = "tbIdDirentD";
            this.tbIdDirentD.Size = new System.Drawing.Size(120, 26);
            this.tbIdDirentD.TabIndex = 59;
            // 
            // tbIdDirentO
            // 
            this.tbIdDirentO.Enabled = false;
            this.tbIdDirentO.Location = new System.Drawing.Point(426, 150);
            this.tbIdDirentO.Name = "tbIdDirentO";
            this.tbIdDirentO.Size = new System.Drawing.Size(120, 26);
            this.tbIdDirentO.TabIndex = 58;
            // 
            // IdDirentD
            // 
            this.IdDirentD.AutoSize = true;
            this.IdDirentD.Location = new System.Drawing.Point(575, 154);
            this.IdDirentD.Name = "IdDirentD";
            this.IdDirentD.Size = new System.Drawing.Size(66, 20);
            this.IdDirentD.TabIndex = 57;
            this.IdDirentD.Text = "IdDirent";
            // 
            // IdDirentO
            // 
            this.IdDirentO.AutoSize = true;
            this.IdDirentO.Location = new System.Drawing.Point(82, 156);
            this.IdDirentO.Name = "IdDirentO";
            this.IdDirentO.Size = new System.Drawing.Size(66, 20);
            this.IdDirentO.TabIndex = 56;
            this.IdDirentO.Text = "IdDirent";
            // 
            // lbCTAPROVISIONNPGCDestino
            // 
            this.lbCTAPROVISIONNPGCDestino.AutoSize = true;
            this.lbCTAPROVISIONNPGCDestino.Location = new System.Drawing.Point(792, 377);
            this.lbCTAPROVISIONNPGCDestino.Name = "lbCTAPROVISIONNPGCDestino";
            this.lbCTAPROVISIONNPGCDestino.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPROVISIONNPGCDestino.TabIndex = 55;
            // 
            // lbCTAPROVISIONDestino
            // 
            this.lbCTAPROVISIONDestino.AutoSize = true;
            this.lbCTAPROVISIONDestino.Location = new System.Drawing.Point(792, 345);
            this.lbCTAPROVISIONDestino.Name = "lbCTAPROVISIONDestino";
            this.lbCTAPROVISIONDestino.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPROVISIONDestino.TabIndex = 54;
            // 
            // lbCTAPORCUENTADestino
            // 
            this.lbCTAPORCUENTADestino.AutoSize = true;
            this.lbCTAPORCUENTADestino.Location = new System.Drawing.Point(792, 313);
            this.lbCTAPORCUENTADestino.Name = "lbCTAPORCUENTADestino";
            this.lbCTAPORCUENTADestino.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPORCUENTADestino.TabIndex = 53;
            // 
            // lbCTALETRADestino
            // 
            this.lbCTALETRADestino.AutoSize = true;
            this.lbCTALETRADestino.Location = new System.Drawing.Point(792, 281);
            this.lbCTALETRADestino.Name = "lbCTALETRADestino";
            this.lbCTALETRADestino.Size = new System.Drawing.Size(0, 20);
            this.lbCTALETRADestino.TabIndex = 52;
            // 
            // lbCTADESCDestino
            // 
            this.lbCTADESCDestino.AutoSize = true;
            this.lbCTADESCDestino.Location = new System.Drawing.Point(792, 249);
            this.lbCTADESCDestino.Name = "lbCTADESCDestino";
            this.lbCTADESCDestino.Size = new System.Drawing.Size(0, 20);
            this.lbCTADESCDestino.TabIndex = 51;
            // 
            // lbcuentaDestino
            // 
            this.lbcuentaDestino.AutoSize = true;
            this.lbcuentaDestino.Location = new System.Drawing.Point(792, 217);
            this.lbcuentaDestino.Name = "lbcuentaDestino";
            this.lbcuentaDestino.Size = new System.Drawing.Size(0, 20);
            this.lbcuentaDestino.TabIndex = 50;
            // 
            // lbcodCliD
            // 
            this.lbcodCliD.AutoSize = true;
            this.lbcodCliD.Location = new System.Drawing.Point(792, 123);
            this.lbcodCliD.Name = "lbcodCliD";
            this.lbcodCliD.Size = new System.Drawing.Size(0, 20);
            this.lbcodCliD.TabIndex = 49;
            // 
            // lbCTAPROVISIONNPGCOrigen
            // 
            this.lbCTAPROVISIONNPGCOrigen.AutoSize = true;
            this.lbCTAPROVISIONNPGCOrigen.Location = new System.Drawing.Point(308, 379);
            this.lbCTAPROVISIONNPGCOrigen.Name = "lbCTAPROVISIONNPGCOrigen";
            this.lbCTAPROVISIONNPGCOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPROVISIONNPGCOrigen.TabIndex = 48;
            // 
            // lbCTAPROVISIONOrigen
            // 
            this.lbCTAPROVISIONOrigen.AutoSize = true;
            this.lbCTAPROVISIONOrigen.Location = new System.Drawing.Point(308, 347);
            this.lbCTAPROVISIONOrigen.Name = "lbCTAPROVISIONOrigen";
            this.lbCTAPROVISIONOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPROVISIONOrigen.TabIndex = 47;
            // 
            // lbCTAPORCUENTAOrigen
            // 
            this.lbCTAPORCUENTAOrigen.AutoSize = true;
            this.lbCTAPORCUENTAOrigen.Location = new System.Drawing.Point(308, 315);
            this.lbCTAPORCUENTAOrigen.Name = "lbCTAPORCUENTAOrigen";
            this.lbCTAPORCUENTAOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCTAPORCUENTAOrigen.TabIndex = 46;
            // 
            // lbCTALETRAOrigen
            // 
            this.lbCTALETRAOrigen.AutoSize = true;
            this.lbCTALETRAOrigen.Location = new System.Drawing.Point(308, 283);
            this.lbCTALETRAOrigen.Name = "lbCTALETRAOrigen";
            this.lbCTALETRAOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCTALETRAOrigen.TabIndex = 45;
            // 
            // lbCTADESCOrigen
            // 
            this.lbCTADESCOrigen.AutoSize = true;
            this.lbCTADESCOrigen.Location = new System.Drawing.Point(308, 251);
            this.lbCTADESCOrigen.Name = "lbCTADESCOrigen";
            this.lbCTADESCOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCTADESCOrigen.TabIndex = 44;
            // 
            // lbCuentaOrigen
            // 
            this.lbCuentaOrigen.AutoSize = true;
            this.lbCuentaOrigen.Location = new System.Drawing.Point(308, 219);
            this.lbCuentaOrigen.Name = "lbCuentaOrigen";
            this.lbCuentaOrigen.Size = new System.Drawing.Size(0, 20);
            this.lbCuentaOrigen.TabIndex = 43;
            // 
            // lbCodCliO
            // 
            this.lbCodCliO.AutoSize = true;
            this.lbCodCliO.Location = new System.Drawing.Point(308, 125);
            this.lbCodCliO.Name = "lbCodCliO";
            this.lbCodCliO.Size = new System.Drawing.Size(0, 20);
            this.lbCodCliO.TabIndex = 42;
            // 
            // tbCTAPROVISIONNPGCDestino
            // 
            this.tbCTAPROVISIONNPGCDestino.Enabled = false;
            this.tbCTAPROVISIONNPGCDestino.Location = new System.Drawing.Point(919, 377);
            this.tbCTAPROVISIONNPGCDestino.Name = "tbCTAPROVISIONNPGCDestino";
            this.tbCTAPROVISIONNPGCDestino.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPROVISIONNPGCDestino.TabIndex = 41;
            // 
            // tbCTAPROVISIONDestino
            // 
            this.tbCTAPROVISIONDestino.Enabled = false;
            this.tbCTAPROVISIONDestino.Location = new System.Drawing.Point(919, 345);
            this.tbCTAPROVISIONDestino.Name = "tbCTAPROVISIONDestino";
            this.tbCTAPROVISIONDestino.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPROVISIONDestino.TabIndex = 40;
            // 
            // tbCTAPORCUENTADestino
            // 
            this.tbCTAPORCUENTADestino.Enabled = false;
            this.tbCTAPORCUENTADestino.Location = new System.Drawing.Point(919, 312);
            this.tbCTAPORCUENTADestino.Name = "tbCTAPORCUENTADestino";
            this.tbCTAPORCUENTADestino.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPORCUENTADestino.TabIndex = 39;
            // 
            // tbCTALETRADestino
            // 
            this.tbCTALETRADestino.Enabled = false;
            this.tbCTALETRADestino.Location = new System.Drawing.Point(919, 279);
            this.tbCTALETRADestino.Name = "tbCTALETRADestino";
            this.tbCTALETRADestino.Size = new System.Drawing.Size(120, 26);
            this.tbCTALETRADestino.TabIndex = 38;
            // 
            // tbCTADESCDestino
            // 
            this.tbCTADESCDestino.Enabled = false;
            this.tbCTADESCDestino.Location = new System.Drawing.Point(919, 246);
            this.tbCTADESCDestino.Name = "tbCTADESCDestino";
            this.tbCTADESCDestino.Size = new System.Drawing.Size(120, 26);
            this.tbCTADESCDestino.TabIndex = 37;
            // 
            // tbcuentaDestino
            // 
            this.tbcuentaDestino.Enabled = false;
            this.tbcuentaDestino.Location = new System.Drawing.Point(919, 214);
            this.tbcuentaDestino.Name = "tbcuentaDestino";
            this.tbcuentaDestino.Size = new System.Drawing.Size(120, 26);
            this.tbcuentaDestino.TabIndex = 36;
            // 
            // tbCodCliDestino
            // 
            this.tbCodCliDestino.Enabled = false;
            this.tbCodCliDestino.Location = new System.Drawing.Point(919, 119);
            this.tbCodCliDestino.Name = "tbCodCliDestino";
            this.tbCodCliDestino.Size = new System.Drawing.Size(120, 26);
            this.tbCodCliDestino.TabIndex = 35;
            // 
            // tbCTAPROVISIONNPGCOrigen
            // 
            this.tbCTAPROVISIONNPGCOrigen.Enabled = false;
            this.tbCTAPROVISIONNPGCOrigen.Location = new System.Drawing.Point(426, 377);
            this.tbCTAPROVISIONNPGCOrigen.Name = "tbCTAPROVISIONNPGCOrigen";
            this.tbCTAPROVISIONNPGCOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPROVISIONNPGCOrigen.TabIndex = 34;
            // 
            // tbCTAPROVISIONOrigen
            // 
            this.tbCTAPROVISIONOrigen.Enabled = false;
            this.tbCTAPROVISIONOrigen.Location = new System.Drawing.Point(426, 345);
            this.tbCTAPROVISIONOrigen.Name = "tbCTAPROVISIONOrigen";
            this.tbCTAPROVISIONOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPROVISIONOrigen.TabIndex = 33;
            // 
            // tbCTAPORCUENTAOrigen
            // 
            this.tbCTAPORCUENTAOrigen.Enabled = false;
            this.tbCTAPORCUENTAOrigen.Location = new System.Drawing.Point(426, 312);
            this.tbCTAPORCUENTAOrigen.Name = "tbCTAPORCUENTAOrigen";
            this.tbCTAPORCUENTAOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCTAPORCUENTAOrigen.TabIndex = 32;
            // 
            // tbCTALETRAOrigen
            // 
            this.tbCTALETRAOrigen.Enabled = false;
            this.tbCTALETRAOrigen.Location = new System.Drawing.Point(426, 279);
            this.tbCTALETRAOrigen.Name = "tbCTALETRAOrigen";
            this.tbCTALETRAOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCTALETRAOrigen.TabIndex = 31;
            // 
            // tbCTADESCOrigen
            // 
            this.tbCTADESCOrigen.Enabled = false;
            this.tbCTADESCOrigen.Location = new System.Drawing.Point(426, 246);
            this.tbCTADESCOrigen.Name = "tbCTADESCOrigen";
            this.tbCTADESCOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCTADESCOrigen.TabIndex = 30;
            // 
            // tbCuentaOrigen
            // 
            this.tbCuentaOrigen.Enabled = false;
            this.tbCuentaOrigen.Location = new System.Drawing.Point(426, 214);
            this.tbCuentaOrigen.Name = "tbCuentaOrigen";
            this.tbCuentaOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCuentaOrigen.TabIndex = 29;
            // 
            // tbCodCliOrigen
            // 
            this.tbCodCliOrigen.Enabled = false;
            this.tbCodCliOrigen.Location = new System.Drawing.Point(426, 119);
            this.tbCodCliOrigen.Name = "tbCodCliOrigen";
            this.tbCodCliOrigen.Size = new System.Drawing.Size(120, 26);
            this.tbCodCliOrigen.TabIndex = 28;
            // 
            // rbClientes
            // 
            this.rbClientes.AutoSize = true;
            this.rbClientes.Checked = true;
            this.rbClientes.Location = new System.Drawing.Point(1132, 31);
            this.rbClientes.Name = "rbClientes";
            this.rbClientes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbClientes.Size = new System.Drawing.Size(84, 24);
            this.rbClientes.TabIndex = 26;
            this.rbClientes.TabStop = true;
            this.rbClientes.Text = "Clientes";
            this.rbClientes.UseVisualStyleBackColor = true;
            // 
            // rbProveedores
            // 
            this.rbProveedores.AutoSize = true;
            this.rbProveedores.Location = new System.Drawing.Point(1132, 53);
            this.rbProveedores.Name = "rbProveedores";
            this.rbProveedores.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbProveedores.Size = new System.Drawing.Size(116, 24);
            this.rbProveedores.TabIndex = 27;
            this.rbProveedores.Text = "Proveedores";
            this.rbProveedores.UseVisualStyleBackColor = true;
            this.rbProveedores.CheckedChanged += new System.EventHandler(this.rbProveedores_CheckedChanged);
            // 
            // btReAsing
            // 
            this.btReAsing.Location = new System.Drawing.Point(1132, 109);
            this.btReAsing.Name = "btReAsing";
            this.btReAsing.Size = new System.Drawing.Size(174, 42);
            this.btReAsing.TabIndex = 25;
            this.btReAsing.Text = "&Reasignar";
            this.btReAsing.UseVisualStyleBackColor = true;
            this.btReAsing.Click += new System.EventHandler(this.btReAsing_Click);
            // 
            // CTAPROVISIONNPGCDestino
            // 
            this.CTAPROVISIONNPGCDestino.AutoSize = true;
            this.CTAPROVISIONNPGCDestino.Location = new System.Drawing.Point(575, 377);
            this.CTAPROVISIONNPGCDestino.Name = "CTAPROVISIONNPGCDestino";
            this.CTAPROVISIONNPGCDestino.Size = new System.Drawing.Size(174, 20);
            this.CTAPROVISIONNPGCDestino.TabIndex = 24;
            this.CTAPROVISIONNPGCDestino.Text = "CTAPROVISIONNPGC";
            // 
            // CTAPROVISIONDestino
            // 
            this.CTAPROVISIONDestino.AutoSize = true;
            this.CTAPROVISIONDestino.Location = new System.Drawing.Point(575, 345);
            this.CTAPROVISIONDestino.Name = "CTAPROVISIONDestino";
            this.CTAPROVISIONDestino.Size = new System.Drawing.Size(129, 20);
            this.CTAPROVISIONDestino.TabIndex = 23;
            this.CTAPROVISIONDestino.Text = "CTAPROVISION";
            // 
            // CTAPORCUENTADestino
            // 
            this.CTAPORCUENTADestino.AutoSize = true;
            this.CTAPORCUENTADestino.Location = new System.Drawing.Point(575, 313);
            this.CTAPORCUENTADestino.Name = "CTAPORCUENTADestino";
            this.CTAPORCUENTADestino.Size = new System.Drawing.Size(139, 20);
            this.CTAPORCUENTADestino.TabIndex = 22;
            this.CTAPORCUENTADestino.Text = "CTAPORCUENTA";
            // 
            // CTALETRADestino
            // 
            this.CTALETRADestino.AutoSize = true;
            this.CTALETRADestino.Location = new System.Drawing.Point(575, 281);
            this.CTALETRADestino.Name = "CTALETRADestino";
            this.CTALETRADestino.Size = new System.Drawing.Size(92, 20);
            this.CTALETRADestino.TabIndex = 21;
            this.CTALETRADestino.Text = "CTALETRA";
            // 
            // CTAPROVISIONNPGCOrigen
            // 
            this.CTAPROVISIONNPGCOrigen.AutoSize = true;
            this.CTAPROVISIONNPGCOrigen.Location = new System.Drawing.Point(82, 379);
            this.CTAPROVISIONNPGCOrigen.Name = "CTAPROVISIONNPGCOrigen";
            this.CTAPROVISIONNPGCOrigen.Size = new System.Drawing.Size(174, 20);
            this.CTAPROVISIONNPGCOrigen.TabIndex = 20;
            this.CTAPROVISIONNPGCOrigen.Text = "CTAPROVISIONNPGC";
            // 
            // CTAPROVISIONOrigen
            // 
            this.CTAPROVISIONOrigen.AutoSize = true;
            this.CTAPROVISIONOrigen.Location = new System.Drawing.Point(82, 347);
            this.CTAPROVISIONOrigen.Name = "CTAPROVISIONOrigen";
            this.CTAPROVISIONOrigen.Size = new System.Drawing.Size(129, 20);
            this.CTAPROVISIONOrigen.TabIndex = 19;
            this.CTAPROVISIONOrigen.Text = "CTAPROVISION";
            // 
            // CTAPORCUENTAOrigen
            // 
            this.CTAPORCUENTAOrigen.AutoSize = true;
            this.CTAPORCUENTAOrigen.Location = new System.Drawing.Point(82, 315);
            this.CTAPORCUENTAOrigen.Name = "CTAPORCUENTAOrigen";
            this.CTAPORCUENTAOrigen.Size = new System.Drawing.Size(139, 20);
            this.CTAPORCUENTAOrigen.TabIndex = 18;
            this.CTAPORCUENTAOrigen.Text = "CTAPORCUENTA";
            // 
            // CTALETRAOrigen
            // 
            this.CTALETRAOrigen.AutoSize = true;
            this.CTALETRAOrigen.Location = new System.Drawing.Point(82, 283);
            this.CTALETRAOrigen.Name = "CTALETRAOrigen";
            this.CTALETRAOrigen.Size = new System.Drawing.Size(92, 20);
            this.CTALETRAOrigen.TabIndex = 17;
            this.CTALETRAOrigen.Text = "CTALETRA";
            // 
            // CTADESCDestino
            // 
            this.CTADESCDestino.AutoSize = true;
            this.CTADESCDestino.Location = new System.Drawing.Point(575, 249);
            this.CTADESCDestino.Name = "CTADESCDestino";
            this.CTADESCDestino.Size = new System.Drawing.Size(85, 20);
            this.CTADESCDestino.TabIndex = 16;
            this.CTADESCDestino.Text = "CTADESC";
            // 
            // cuentaDestino
            // 
            this.cuentaDestino.AutoSize = true;
            this.cuentaDestino.Location = new System.Drawing.Point(575, 217);
            this.cuentaDestino.Name = "cuentaDestino";
            this.cuentaDestino.Size = new System.Drawing.Size(61, 20);
            this.cuentaDestino.TabIndex = 15;
            this.cuentaDestino.Text = "Cuenta";
            // 
            // codCliD
            // 
            this.codCliD.AutoSize = true;
            this.codCliD.Location = new System.Drawing.Point(575, 123);
            this.codCliD.Name = "codCliD";
            this.codCliD.Size = new System.Drawing.Size(61, 20);
            this.codCliD.TabIndex = 14;
            this.codCliD.Text = "CodCLi";
            // 
            // CTADESCOrigen
            // 
            this.CTADESCOrigen.AutoSize = true;
            this.CTADESCOrigen.Location = new System.Drawing.Point(82, 251);
            this.CTADESCOrigen.Name = "CTADESCOrigen";
            this.CTADESCOrigen.Size = new System.Drawing.Size(85, 20);
            this.CTADESCOrigen.TabIndex = 13;
            this.CTADESCOrigen.Text = "CTADESC";
            // 
            // cuentaOrigen
            // 
            this.cuentaOrigen.AutoSize = true;
            this.cuentaOrigen.Location = new System.Drawing.Point(82, 219);
            this.cuentaOrigen.Name = "cuentaOrigen";
            this.cuentaOrigen.Size = new System.Drawing.Size(61, 20);
            this.cuentaOrigen.TabIndex = 12;
            this.cuentaOrigen.Text = "Cuenta";
            // 
            // cbClienteDestino
            // 
            this.cbClienteDestino.FormattingEnabled = true;
            this.cbClienteDestino.Location = new System.Drawing.Point(579, 49);
            this.cbClienteDestino.Name = "cbClienteDestino";
            this.cbClienteDestino.Size = new System.Drawing.Size(460, 28);
            this.cbClienteDestino.TabIndex = 10;
            this.cbClienteDestino.SelectedIndexChanged += new System.EventHandler(this.cbClienteDestino_SelectedIndexChanged);
            // 
            // cbClienteOrigen
            // 
            this.cbClienteOrigen.FormattingEnabled = true;
            this.cbClienteOrigen.Location = new System.Drawing.Point(86, 49);
            this.cbClienteOrigen.Name = "cbClienteOrigen";
            this.cbClienteOrigen.Size = new System.Drawing.Size(460, 28);
            this.cbClienteOrigen.TabIndex = 9;
            this.cbClienteOrigen.SelectedIndexChanged += new System.EventHandler(this.cbClienteOrigen_SelectedIndexChanged);
            // 
            // codCliO
            // 
            this.codCliO.AutoSize = true;
            this.codCliO.Location = new System.Drawing.Point(82, 125);
            this.codCliO.Name = "codCliO";
            this.codCliO.Size = new System.Drawing.Size(61, 20);
            this.codCliO.TabIndex = 5;
            this.codCliO.Text = "CodCLi";
            // 
            // textoComboboxDestino
            // 
            this.textoComboboxDestino.AutoSize = true;
            this.textoComboboxDestino.Location = new System.Drawing.Point(575, 26);
            this.textoComboboxDestino.Name = "textoComboboxDestino";
            this.textoComboboxDestino.Size = new System.Drawing.Size(117, 20);
            this.textoComboboxDestino.TabIndex = 1;
            this.textoComboboxDestino.Text = "Cliente Destino";
            // 
            // textoComboboxOrigen
            // 
            this.textoComboboxOrigen.AutoSize = true;
            this.textoComboboxOrigen.Location = new System.Drawing.Point(82, 26);
            this.textoComboboxOrigen.Name = "textoComboboxOrigen";
            this.textoComboboxOrigen.Size = new System.Drawing.Size(109, 20);
            this.textoComboboxOrigen.TabIndex = 0;
            this.textoComboboxOrigen.Text = "Cliente Origen";
            // 
            // tabCronJobs
            // 
            this.tabCronJobs.BackColor = System.Drawing.Color.LightGray;
            this.tabCronJobs.Controls.Add(this.lblMinutos);
            this.tabCronJobs.Controls.Add(this.cmbMinutos);
            this.tabCronJobs.Controls.Add(this.btnAbririProgramadorTareas);
            this.tabCronJobs.Controls.Add(this.btnSalirModoEdicionTareasProgramadas);
            this.tabCronJobs.Controls.Add(this.lblModoEdicionTareasProgramadas);
            this.tabCronJobs.Controls.Add(this.dgvCronJobs);
            this.tabCronJobs.Controls.Add(this.dtpStartHour);
            this.tabCronJobs.Controls.Add(this.cmbFrequency);
            this.tabCronJobs.Controls.Add(this.lblStartHour);
            this.tabCronJobs.Controls.Add(this.lblFrequency);
            this.tabCronJobs.Controls.Add(this.lblNameTask);
            this.tabCronJobs.Controls.Add(this.cmbNameTask);
            this.tabCronJobs.Controls.Add(this.btnGuardarConfTareasProgramadas);
            this.tabCronJobs.Controls.Add(this.lblTituloTareasProgramadas);
            this.tabCronJobs.Controls.Add(this.label19);
            this.tabCronJobs.Location = new System.Drawing.Point(4, 29);
            this.tabCronJobs.Name = "tabCronJobs";
            this.tabCronJobs.Padding = new System.Windows.Forms.Padding(3);
            this.tabCronJobs.Size = new System.Drawing.Size(1374, 719);
            this.tabCronJobs.TabIndex = 13;
            this.tabCronJobs.Text = "Tareas programadas";
            // 
            // btnAbririProgramadorTareas
            // 
            this.btnAbririProgramadorTareas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAbririProgramadorTareas.BackgroundImage")));
            this.btnAbririProgramadorTareas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAbririProgramadorTareas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbririProgramadorTareas.Location = new System.Drawing.Point(11, 144);
            this.btnAbririProgramadorTareas.Name = "btnAbririProgramadorTareas";
            this.btnAbririProgramadorTareas.Size = new System.Drawing.Size(54, 34);
            this.btnAbririProgramadorTareas.TabIndex = 15;
            this.btnAbririProgramadorTareas.UseVisualStyleBackColor = true;
            this.btnAbririProgramadorTareas.Click += new System.EventHandler(this.btnAbririProgramadorTareas_Click);
            // 
            // btnSalirModoEdicionTareasProgramadas
            // 
            this.btnSalirModoEdicionTareasProgramadas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSalirModoEdicionTareasProgramadas.Location = new System.Drawing.Point(844, 103);
            this.btnSalirModoEdicionTareasProgramadas.Name = "btnSalirModoEdicionTareasProgramadas";
            this.btnSalirModoEdicionTareasProgramadas.Size = new System.Drawing.Size(137, 28);
            this.btnSalirModoEdicionTareasProgramadas.TabIndex = 14;
            this.btnSalirModoEdicionTareasProgramadas.Text = "Cancelar edición";
            this.btnSalirModoEdicionTareasProgramadas.UseVisualStyleBackColor = false;
            this.btnSalirModoEdicionTareasProgramadas.Visible = false;
            this.btnSalirModoEdicionTareasProgramadas.Click += new System.EventHandler(this.btnSalirModoEdicionTareasProgramadas_Click);
            // 
            // lblModoEdicionTareasProgramadas
            // 
            this.lblModoEdicionTareasProgramadas.AutoSize = true;
            this.lblModoEdicionTareasProgramadas.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.lblModoEdicionTareasProgramadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModoEdicionTareasProgramadas.Location = new System.Drawing.Point(329, 151);
            this.lblModoEdicionTareasProgramadas.Name = "lblModoEdicionTareasProgramadas";
            this.lblModoEdicionTareasProgramadas.Size = new System.Drawing.Size(141, 20);
            this.lblModoEdicionTareasProgramadas.TabIndex = 13;
            this.lblModoEdicionTareasProgramadas.Text = "MODO EDICIÓN";
            this.lblModoEdicionTareasProgramadas.Visible = false;
            // 
            // dgvCronJobs
            // 
            this.dgvCronJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCronJobs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCronJobs.Location = new System.Drawing.Point(11, 191);
            this.dgvCronJobs.Name = "dgvCronJobs";
            this.dgvCronJobs.Size = new System.Drawing.Size(1355, 522);
            this.dgvCronJobs.TabIndex = 12;
            this.dgvCronJobs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCronJobs_CellClick);
            // 
            // dtpStartHour
            // 
            this.dtpStartHour.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpStartHour.Location = new System.Drawing.Point(501, 103);
            this.dtpStartHour.Name = "dtpStartHour";
            this.dtpStartHour.ShowUpDown = true;
            this.dtpStartHour.Size = new System.Drawing.Size(111, 26);
            this.dtpStartHour.TabIndex = 11;
            this.dtpStartHour.Visible = false;
            // 
            // cmbFrequency
            // 
            this.cmbFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFrequency.FormattingEnabled = true;
            this.cmbFrequency.Items.AddRange(new object[] {
            "DIA",
            "HORA",
            "MINUTO"});
            this.cmbFrequency.Location = new System.Drawing.Point(304, 103);
            this.cmbFrequency.Name = "cmbFrequency";
            this.cmbFrequency.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbFrequency.Size = new System.Drawing.Size(180, 28);
            this.cmbFrequency.TabIndex = 10;
            // 
            // lblStartHour
            // 
            this.lblStartHour.AutoSize = true;
            this.lblStartHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartHour.Location = new System.Drawing.Point(497, 75);
            this.lblStartHour.Name = "lblStartHour";
            this.lblStartHour.Size = new System.Drawing.Size(129, 20);
            this.lblStartHour.TabIndex = 9;
            this.lblStartHour.Text = "Hora ejecución";
            this.lblStartHour.Visible = false;
            // 
            // lblFrequency
            // 
            this.lblFrequency.AutoSize = true;
            this.lblFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrequency.Location = new System.Drawing.Point(300, 75);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(98, 20);
            this.lblFrequency.TabIndex = 8;
            this.lblFrequency.Text = "Frecuencia";
            // 
            // lblNameTask
            // 
            this.lblNameTask.AutoSize = true;
            this.lblNameTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameTask.Location = new System.Drawing.Point(7, 75);
            this.lblNameTask.Name = "lblNameTask";
            this.lblNameTask.Size = new System.Drawing.Size(55, 20);
            this.lblNameTask.TabIndex = 7;
            this.lblNameTask.Text = "Tarea";
            // 
            // cmbNameTask
            // 
            this.cmbNameTask.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNameTask.FormattingEnabled = true;
            this.cmbNameTask.Location = new System.Drawing.Point(11, 103);
            this.cmbNameTask.Name = "cmbNameTask";
            this.cmbNameTask.Size = new System.Drawing.Size(272, 28);
            this.cmbNameTask.TabIndex = 6;
            this.cmbNameTask.SelectedIndexChanged += new System.EventHandler(this.cmbNameTask_SelectedIndexChanged);
            // 
            // btnGuardarConfTareasProgramadas
            // 
            this.btnGuardarConfTareasProgramadas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnGuardarConfTareasProgramadas.Location = new System.Drawing.Point(744, 103);
            this.btnGuardarConfTareasProgramadas.Name = "btnGuardarConfTareasProgramadas";
            this.btnGuardarConfTareasProgramadas.Size = new System.Drawing.Size(82, 28);
            this.btnGuardarConfTareasProgramadas.TabIndex = 4;
            this.btnGuardarConfTareasProgramadas.Text = "Guardar";
            this.btnGuardarConfTareasProgramadas.UseVisualStyleBackColor = false;
            this.btnGuardarConfTareasProgramadas.Click += new System.EventHandler(this.btnGuardarConfTareasProgramadas_Click);
            // 
            // lblTituloTareasProgramadas
            // 
            this.lblTituloTareasProgramadas.AutoSize = true;
            this.lblTituloTareasProgramadas.BackColor = System.Drawing.Color.LightGray;
            this.lblTituloTareasProgramadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloTareasProgramadas.Location = new System.Drawing.Point(6, 17);
            this.lblTituloTareasProgramadas.Name = "lblTituloTareasProgramadas";
            this.lblTituloTareasProgramadas.Size = new System.Drawing.Size(531, 29);
            this.lblTituloTareasProgramadas.TabIndex = 1;
            this.lblTituloTareasProgramadas.Text = "CONFIGURADOR TAREAS PROGRAMADAS";
            this.lblTituloTareasProgramadas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(350, 36);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 20);
            this.label19.TabIndex = 0;
            // 
            // toolTipSync
            // 
            this.toolTipSync.BackColor = System.Drawing.SystemColors.ControlLightLight;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // cmbMinutos
            // 
            this.cmbMinutos.FormattingEnabled = true;
            this.cmbMinutos.Items.AddRange(new object[] {
            "5",
            "10",
            "15",
            "30",
            "45"});
            this.cmbMinutos.Location = new System.Drawing.Point(657, 103);
            this.cmbMinutos.Name = "cmbMinutos";
            this.cmbMinutos.Size = new System.Drawing.Size(62, 28);
            this.cmbMinutos.TabIndex = 16;
            this.cmbMinutos.Visible = false;
            // 
            // lblMinutos
            // 
            this.lblMinutos.AutoSize = true;
            this.lblMinutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinutos.Location = new System.Drawing.Point(653, 75);
            this.lblMinutos.Name = "lblMinutos";
            this.lblMinutos.Size = new System.Drawing.Size(72, 20);
            this.lblMinutos.TabIndex = 17;
            this.lblMinutos.Text = "Minutos";
            this.lblMinutos.Visible = false;
            // 
            // csDismay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1394, 793);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "csDismay";
            this.Text = "Repasat > A3 ||| A3 > Repasat";
            this.Load += new System.EventHandler(this.frRepasat_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.statusStripFacturas.ResumeLayout(false);
            this.statusStripFacturas.PerformLayout();
            this.contextFactura.ResumeLayout(false);
            this.ctextMenuSync.ResumeLayout(false);
            this.cmsProductos.ResumeLayout(false);
            this.tabAuxiliares.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepasatData)).EndInit();
            this.ctexMenuAuxiliars.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxRepasat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxA3ERP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3ERPData)).EndInit();
            this.cMenuAuxiliar.ResumeLayout(false);
            this.tabMasterFiles.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsRepasat)).EndInit();
            this.ctexMenuMaster.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpdownMovDays)).EndInit();
            this.grBoxAccounts.ResumeLayout(false);
            this.grBoxAccounts.PerformLayout();
            this.grBoxActivos.ResumeLayout(false);
            this.grBoxActivos.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsA3ERP)).EndInit();
            this.contextMenuERPData.ResumeLayout(false);
            this.tabDocs.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grBoxTicketBai.ResumeLayout(false);
            this.grBoxTicketBai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxTicketBai)).EndInit();
            this.grBoxDocPago.ResumeLayout(false);
            this.grBoxDocPago.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRPST)).EndInit();
            this.grBoxCuenta.ResumeLayout(false);
            this.grBoxCuenta.PerformLayout();
            this.grBoxRemesado.ResumeLayout(false);
            this.grBoxRemesado.PerformLayout();
            this.grBoxTipoDocumento.ResumeLayout(false);
            this.grBoxTipoDocumento.PerformLayout();
            this.grBoxFechas.ResumeLayout(false);
            this.grBoxFechas.PerformLayout();
            this.grBoxSeries.ResumeLayout(false);
            this.grBoxSeries.PerformLayout();
            this.grBoxEstadoCobro.ResumeLayout(false);
            this.grBoxEstadoCobro.PerformLayout();
            this.grBoxSincronizados.ResumeLayout(false);
            this.grBoxSincronizados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxA3)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
            this.splitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRPST)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSyncDocsDetail)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabStock.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.grboxSelectStock.ResumeLayout(false);
            this.grboxSelectStock.PerformLayout();
            this.grBoxAlmacenes.ResumeLayout(false);
            this.grBoxAlmacenes.PerformLayout();
            this.grBoxSelectDatesStock.ResumeLayout(false);
            this.grBoxSelectDatesStock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStock)).EndInit();
            this.ctextMenuStock.ResumeLayout(false);
            this.tabValidations.ResumeLayout(false);
            this.splitContainer9.Panel1.ResumeLayout(false);
            this.splitContainer9.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvValidatingData)).EndInit();
            this.tabUpdate.ResumeLayout(false);
            this.tabUpdate.PerformLayout();
            this.tabUtilidades.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUtilidades)).EndInit();
            this.tabReasignacion.ResumeLayout(false);
            this.tabReasignacion.PerformLayout();
            this.tabCronJobs.ResumeLayout(false);
            this.tabCronJobs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCronJobs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip contextFactura;
        private System.Windows.Forms.StatusStrip statusStripFacturas;
        private System.Windows.Forms.ToolStripMenuItem comprobarSERIESToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsProductos;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripProgressBar progress;
        private System.Windows.Forms.ContextMenuStrip ctextMenuSync;
        private System.Windows.Forms.TabPage tabAuxiliares;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dgvRepasatData;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button btnRepresentantes;
        private System.Windows.Forms.PictureBox picboxRepasat;
        private System.Windows.Forms.PictureBox picboxA3ERP;
        private System.Windows.Forms.Button btPaymentDocs;
        private System.Windows.Forms.Button btnCarriers;
        private System.Windows.Forms.Button btnPaymentMethods;
        private System.Windows.Forms.DataGridView dgvA3ERPData;
        private System.Windows.Forms.TabPage tabMasterFiles;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvAccountsRepasat;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Button btSendCustomersA3ERPToRepasat;
        private System.Windows.Forms.Button btSendCustomersRepasatToA3ERP;
        private System.Windows.Forms.DataGridView dgvAccountsA3ERP;
        private System.Windows.Forms.TabPage tabDocs;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbCompras;
        private System.Windows.Forms.RadioButton rbVentas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbDesde;
        private System.Windows.Forms.DateTimePicker dtpToInvoicesV;
        private System.Windows.Forms.DateTimePicker dtpFromInvoicesV;
        private System.Windows.Forms.DataGridView dgvRPST;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox grBoxAccounts;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSyncStock;
        private System.Windows.Forms.Button btnRutas;
        private System.Windows.Forms.Button btnLoadAccountsERP;
        private System.Windows.Forms.ContextMenuStrip contextMenuERPData;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.Button btnZonas;
        private System.Windows.Forms.Button btnUploadRutas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRepresen;
        private System.Windows.Forms.ContextMenuStrip cMenuAuxiliar;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem1;
        private System.Windows.Forms.Button btnMetPago;
        private System.Windows.Forms.Button btnDocsPago;
        private System.Windows.Forms.Button btnUploadZonas;
        private System.Windows.Forms.Button btnLoadAccountsRepasat;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbSyncAll;
        private System.Windows.Forms.RadioButton rbSyncNo;
        private System.Windows.Forms.ContextMenuStrip ctexMenuMaster;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem2;
        private System.Windows.Forms.TextBox tbAccountId;
        private System.Windows.Forms.Button btnRPSTAddress;
        private System.Windows.Forms.ToolStripMenuItem validarPorCodigoExternoToolStripMenuItem;
        private System.Windows.Forms.Button btnRegImpuestos;
        private System.Windows.Forms.Button btnUploadRegimenes;
        private System.Windows.Forms.TabPage tabUpdate;
        private System.Windows.Forms.Button btnUpdateArticulosFromRPSTToA3;
        private System.Windows.Forms.Button btnProveedores;
        private System.Windows.Forms.Button btnUpdateClientesRPST;
        private System.Windows.Forms.Button btnProductUpdateToRepasat;
        private System.Windows.Forms.Button btnUpdateProducts;
        private System.Windows.Forms.Button btnResetUpdateDateSync;
        private System.Windows.Forms.Button btnContactsTorepasat;
        private System.Windows.Forms.Button btnContactsToA3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnGestionarDireccionesEnA3ERP;
        private System.Windows.Forms.ToolStripMenuItem syncronizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marcarSyncroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marcarNoSyncroToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUpdateClientesA3ERP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnFullUpdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnUpdateClientes;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnBanksToRepasat;
        private System.Windows.Forms.Button btnGestionarBancosEnA3ERP;
        private System.Windows.Forms.ToolStripMenuItem códigoExternoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nIFToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbSyncYes;
        private System.Windows.Forms.ToolStripMenuItem crearEnRepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selecciónToolStripMenuItem;
        private System.Windows.Forms.ComboBox cboxMaestros;
        private System.Windows.Forms.Button btnUpdateProveedorsRPST;
        private System.Windows.Forms.Button btnUploadTipoImpuestos;
        private System.Windows.Forms.Button btnTiposImpuestos;
        private System.Windows.Forms.Button btnUploadAlmacenes;
        private System.Windows.Forms.Button btnUploadSeries;
        private System.Windows.Forms.Button btnAlmacenes;
        private System.Windows.Forms.Button btnSeries;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox grBoxSincronizados;
        private System.Windows.Forms.RadioButton rbSyncDocYes;
        private System.Windows.Forms.RadioButton rbSyncDocAll;
        private System.Windows.Forms.RadioButton rbSyncDocNo;
        private System.Windows.Forms.ToolStripMenuItem enlazarFacturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ComboBox cboxSeries;
        private System.Windows.Forms.CheckBox checkBoxSeries;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem3;
        private System.Windows.Forms.Button btnSyncToRPST;
        private System.Windows.Forms.Button btnSyncToA3ERP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboxTipoDoc;
        private System.Windows.Forms.Button btnLoadDocsRPST;
        private System.Windows.Forms.Button btnLoadDocsA3ERP;
        private System.Windows.Forms.GroupBox grBoxEstadoCobro;
        private System.Windows.Forms.RadioButton rbPaidYes;
        private System.Windows.Forms.RadioButton rbPaidAll;
        private System.Windows.Forms.RadioButton rbPaidNo;
        private System.Windows.Forms.Button btnProyectos;
        private System.Windows.Forms.Button btnUploadCuentasContables;
        private System.Windows.Forms.Button btnPlanContable;
        private System.Windows.Forms.Button btnBanks;
        private System.Windows.Forms.GroupBox grBoxSeries;
        private System.Windows.Forms.GroupBox grBoxTipoDocumento;
        private System.Windows.Forms.GroupBox grBoxFechas;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel lblNumRegistros;
        private System.Windows.Forms.GroupBox grBoxRemesado;
        private System.Windows.Forms.RadioButton rbRemesadoSi;
        private System.Windows.Forms.RadioButton rbRemesadoAll;
        private System.Windows.Forms.RadioButton rbRemesadoNo;
        private System.Windows.Forms.Button btnCrearProyectosA3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem actualizarEnA3ERPSelecciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otrasUtilidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarIDDocumentoRPSTEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearEnlaceEnRepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tssLabelTotalRows;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tssTotalAmountRows;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox grBoxCuenta;
        private System.Windows.Forms.ComboBox cboxCuentas;
        private System.Windows.Forms.CheckBox checkBoxCuentas;
        private System.Windows.Forms.ToolStripMenuItem carteraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarRepasatDesdeA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
        private System.Windows.Forms.Label lblShowData;
        private System.Windows.Forms.PictureBox picBoxA3;
        private System.Windows.Forms.PictureBox picBoxRPST;
        private System.Windows.Forms.ToolStripMenuItem consultaActualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filasSeleccionadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearEfectoEnRPSTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remesasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarImportesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cargarEfectosRemesaToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbutFechaFactura;
        private System.Windows.Forms.RadioButton rbutFechaVencimiento;
        private System.Windows.Forms.ToolStripMenuItem verificarSituaciónEfectosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarIDsSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarImportesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem recibirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verificarImportesToolStripMenuItem3;
        private System.Windows.Forms.RadioButton rdbutFromRPST;
        private System.Windows.Forms.RadioButton rdbutFromA3ERP;
        private System.Windows.Forms.ToolStripMenuItem verificarSincronizaciónDeFacturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalDocumentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseDocumentoToolStripMenuItem;
        private System.Windows.Forms.GroupBox grBoxDocPago;
        private System.Windows.Forms.CheckBox checkBoxDocPagos;
        private System.Windows.Forms.ComboBox cboxDocPagos;
        private System.Windows.Forms.ToolStripMenuItem informaciónSobreEfectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forzarTraspasoConCódigoClienteInformadoToolStripMenuItem;
        private System.Windows.Forms.TabPage tabUtilidades;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Button btnAsignBankRecibos;
        private System.Windows.Forms.DataGridView dgvUtilidades;
        private System.Windows.Forms.Button btnConfirmarMandato;
        private System.Windows.Forms.Button btnCheckDombanca;
        private System.Windows.Forms.ToolStripMenuItem forzarTraspasoCuentaAA3ERPConCódigoInformadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarCódigoDeCuentasToolStripMenuItem;
        private System.Windows.Forms.Button btnRegStock;
        private System.Windows.Forms.TabPage tabStock;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.Button btnQueryStockRepasat;
        private System.Windows.Forms.Button btnQueryStockA3ERP;
        private System.Windows.Forms.Button btnUpdateStockToRepasat;
        private System.Windows.Forms.Button btnUpdateStockToA3ERP;
        private System.Windows.Forms.GroupBox grBoxSelectDatesStock;
        private System.Windows.Forms.DateTimePicker dtPickerFromStock;
        private System.Windows.Forms.DateTimePicker dtPickerToStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgvStock;
        private System.Windows.Forms.ToolStripMenuItem editarVencimientoEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.GroupBox grBoxAlmacenes;
        private System.Windows.Forms.CheckBox checkBAlmacen;
        private System.Windows.Forms.ComboBox cboxAlmacen;
        private System.Windows.Forms.ContextMenuStrip ctextMenuStock;
        private System.Windows.Forms.ToolStripMenuItem regularizarEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dtpickerResetFrom;
        private System.Windows.Forms.Button btnContactos;
        private System.Windows.Forms.Button btnCrearDirecciones;
        private System.Windows.Forms.GroupBox grboxSelectStock;
        private System.Windows.Forms.RadioButton rbutStock;
        private System.Windows.Forms.RadioButton rbutStockActivity;
        private System.Windows.Forms.ToolStripMenuItem articulosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activarControlStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearIdRepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem articulosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resetearCódigoExternoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validarSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enlaceDesdeRepasatA3ERPToolStripMenuItem;
        private System.Windows.Forms.Button btnAsientosUtils;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button btnApex;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem síToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem siToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sincronizarHaciaRepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comercialesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarHaciaA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comercialesToolStripMenuItem1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbObsoletYes;
        private System.Windows.Forms.RadioButton rbObsoletAll;
        private System.Windows.Forms.RadioButton rbObsoletNo;
        private System.Windows.Forms.GroupBox grBoxActivos;
        private System.Windows.Forms.RadioButton rbActiveYes;
        private System.Windows.Forms.RadioButton rbActiveAll;
        private System.Windows.Forms.RadioButton rbActiveNo;
        private System.Windows.Forms.ToolStripMenuItem verificarSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tssLabelTotalRowsRPST;
        private System.Windows.Forms.ToolStripMenuItem borrarRemesaEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.PictureBox picboxTicketBai;
        private System.Windows.Forms.GroupBox grBoxTicketBai;
        private System.Windows.Forms.RadioButton rbTraspasarComoBorradorSi;
        private System.Windows.Forms.RadioButton rbTraspasarComoBorradorNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ToolStripMenuItem ticketBaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem a3ERPRPSTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearCuentaEnRepasatToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer8;
        private System.Windows.Forms.DataGridView dgvSyncDocsDetail;
        private System.Windows.Forms.ToolStripMenuItem validarEnlacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem porCódigoExternoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem direccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarArticulosEnRPSTToolStripMenuItem;
        private System.Windows.Forms.TabPage tabValidations;
        private System.Windows.Forms.SplitContainer splitContainer9;
        private System.Windows.Forms.Button btnLoadValidationData;
        private System.Windows.Forms.ComboBox cboxvalidatingMaster;
        private System.Windows.Forms.DataGridView dgvValidatingData;
        private System.Windows.Forms.ToolTip toolTipSync;
        private System.Windows.Forms.ContextMenuStrip ctexMenuAuxiliars;
        private System.Windows.Forms.ToolStripMenuItem verificarCodigosEnRepasatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarCódigoDeCuentaEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.Button btklstic;
        private System.Windows.Forms.Button bt_tbai;
        private System.Windows.Forms.ToolStripMenuItem actualizarDocsDesdeA3ToolStripMenuItem;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.NumericUpDown numUpdownMovDays;
        private System.Windows.Forms.RadioButton rbtnSelectProductsMovs;
        private System.Windows.Forms.RadioButton rbtnAllProducts;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox cboxDescripcionArticulosA3;
        private System.Windows.Forms.CheckBox cboxCheckCuentas;
        private System.Windows.Forms.CheckBox cboxGenerarMovsContables;
        private System.Windows.Forms.Button btnUploadCentrosCoste;
        private System.Windows.Forms.Button btnCrearCentrosCosteA3;
        private System.Windows.Forms.Button btnCostCenters;
        private System.Windows.Forms.Label lblUdadNegocio;
        private System.Windows.Forms.ToolStripMenuItem porCIFToolStripMenuItem;
        private System.Windows.Forms.TabPage tabReasignacion;
        private System.Windows.Forms.Label textoComboboxOrigen;
        private System.Windows.Forms.Label textoComboboxDestino;
        private System.Windows.Forms.Label codCliO;
        private System.Windows.Forms.ComboBox cbClienteDestino;
        private System.Windows.Forms.ComboBox cbClienteOrigen;
        private System.Windows.Forms.Label CTADESCOrigen;
        private System.Windows.Forms.Label cuentaOrigen;
        private System.Windows.Forms.Label CTADESCDestino;
        private System.Windows.Forms.Label cuentaDestino;
        private System.Windows.Forms.Label codCliD;
        private System.Windows.Forms.Label CTAPROVISIONNPGCDestino;
        private System.Windows.Forms.Label CTAPROVISIONDestino;
        private System.Windows.Forms.Label CTAPORCUENTADestino;
        private System.Windows.Forms.Label CTALETRADestino;
        private System.Windows.Forms.Label CTAPROVISIONNPGCOrigen;
        private System.Windows.Forms.Label CTAPROVISIONOrigen;
        private System.Windows.Forms.Label CTAPORCUENTAOrigen;
        private System.Windows.Forms.Label CTALETRAOrigen;
        private System.Windows.Forms.RadioButton rbClientes;
        private System.Windows.Forms.RadioButton rbProveedores;
        private System.Windows.Forms.Button btReAsing;
        private System.Windows.Forms.TextBox tbCTAPROVISIONNPGCDestino;
        private System.Windows.Forms.TextBox tbCTAPROVISIONDestino;
        private System.Windows.Forms.TextBox tbCTAPORCUENTADestino;
        private System.Windows.Forms.TextBox tbCTALETRADestino;
        private System.Windows.Forms.TextBox tbCTADESCDestino;
        private System.Windows.Forms.TextBox tbcuentaDestino;
        private System.Windows.Forms.TextBox tbCodCliDestino;
        private System.Windows.Forms.TextBox tbCTAPROVISIONNPGCOrigen;
        private System.Windows.Forms.TextBox tbCTAPROVISIONOrigen;
        private System.Windows.Forms.TextBox tbCTAPORCUENTAOrigen;
        private System.Windows.Forms.TextBox tbCTALETRAOrigen;
        private System.Windows.Forms.TextBox tbCTADESCOrigen;
        private System.Windows.Forms.TextBox tbCuentaOrigen;
        private System.Windows.Forms.TextBox tbCodCliOrigen;
        private System.Windows.Forms.Label lbCTAPROVISIONNPGCDestino;
        private System.Windows.Forms.Label lbCTAPROVISIONDestino;
        private System.Windows.Forms.Label lbCTAPORCUENTADestino;
        private System.Windows.Forms.Label lbCTALETRADestino;
        private System.Windows.Forms.Label lbCTADESCDestino;
        private System.Windows.Forms.Label lbcuentaDestino;
        private System.Windows.Forms.Label lbcodCliD;
        private System.Windows.Forms.Label lbCTAPROVISIONNPGCOrigen;
        private System.Windows.Forms.Label lbCTAPROVISIONOrigen;
        private System.Windows.Forms.Label lbCTAPORCUENTAOrigen;
        private System.Windows.Forms.Label lbCTALETRAOrigen;
        private System.Windows.Forms.Label lbCTADESCOrigen;
        private System.Windows.Forms.Label lbCuentaOrigen;
        private System.Windows.Forms.Label lbCodCliO;
        private System.Windows.Forms.Label lbIdDirentD;
        private System.Windows.Forms.Label lbIdDirentO;
        private System.Windows.Forms.TextBox tbIdDirentD;
        private System.Windows.Forms.TextBox tbIdDirentO;
        private System.Windows.Forms.Label IdDirentD;
        private System.Windows.Forms.Label IdDirentO;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbIdDomBancaDestino;
        private System.Windows.Forms.TextBox tbIdDomBancaOrigen;
        private System.Windows.Forms.Label lbIdDomBancaD;
        private System.Windows.Forms.Label lbIdDomBancaO;
        private System.Windows.Forms.ToolStripMenuItem validarFacturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validarArticulosDeA3EnRPSTToolStripMenuItem;
        private System.Windows.Forms.CheckBox cboxTicketBai;
        private System.Windows.Forms.Button btnCrearSeriesA3;
        private System.Windows.Forms.Button btnCrearDocsPagoA3;
        private System.Windows.Forms.Button btnCrearTranspotA3;
        private System.Windows.Forms.Button btnCrearRutasA3;
        private System.Windows.Forms.Button btnCrearZonasA3;
        private System.Windows.Forms.Button btnCrearAlmacenA3;
        private System.Windows.Forms.Button btnAuxiliaresToRPST;
        private System.Windows.Forms.Button btnCrearMarcasArtA3;
        private System.Windows.Forms.Button btnUploadMarcasArt;
        private System.Windows.Forms.Button btnMarcasArt;
        private System.Windows.Forms.Button btnCrearSubfamiliaArtA3;
        private System.Windows.Forms.Button btnUploadSubfamiliaArt;
        private System.Windows.Forms.Button btnSubfamiliaArt;
        private System.Windows.Forms.Button btnCrearFamiliaArtA3;
        private System.Windows.Forms.Button btnUploadFamiliaArt;
        private System.Windows.Forms.Button btnFamiliaArt;
        private System.Windows.Forms.Button btUpdateRepasatToA3ERP;
        private System.Windows.Forms.Button btUpdateA3ERPToRepasat;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.DateTimePicker dtpDateUpdateClientes;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónEnRepasatToolStripMenuItem;
        private System.Windows.Forms.Button btnTestConnect1;
        private System.Windows.Forms.Button btnCrearTipoArtA3;
        private System.Windows.Forms.Button btnUploadTipoArt;
        private System.Windows.Forms.Button btnTipoArt;
        private System.Windows.Forms.ToolStripMenuItem numLineasDocsToolStripMenuItem;
        private System.Windows.Forms.Button testcc;
        private System.Windows.Forms.Button btnTestConexionesBD;
        private System.Windows.Forms.TabPage tabCronJobs;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblTituloTareasProgramadas;
        private System.Windows.Forms.Button btnGuardarConfTareasProgramadas;
        private System.Windows.Forms.ComboBox cmbNameTask;
        private System.Windows.Forms.ComboBox cmbFrequency;
        private System.Windows.Forms.Label lblStartHour;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Label lblNameTask;
        private System.Windows.Forms.DataGridView dgvCronJobs;
        private System.Windows.Forms.DateTimePicker dtpStartHour;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnSalirModoEdicionTareasProgramadas;
        private System.Windows.Forms.Label lblModoEdicionTareasProgramadas;
        private System.Windows.Forms.Button btnAbririProgramadorTareas;
        private System.Windows.Forms.Label lblMinutos;
        private System.Windows.Forms.ComboBox cmbMinutos;
    }
}