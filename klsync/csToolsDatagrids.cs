﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace klsync
{
    class csToolsDatagrids
    {
        public int calcularlineas(DataGridView MiDatagrid,int columna)
        {
            int sum = 0;
            for (int i = 0; i < MiDatagrid.RowCount; i++)
            {
                //sum = sum + int.Parse(MiDatagrid.Rows[i- 1].Cells[columna-1].Value.ToString());
             sum = sum + int.Parse(MiDatagrid.Rows[i].Cells[columna - 1].Value.ToString());
}
            return sum;
        }

         public void ClonarColumnas(DataGridView DGV1, DataGridView DGV2)
         {
            for (int i = 0;i< DGV1.Columns.Count; i++)
            {
                DataGridViewColumn columna = new DataGridViewColumn();
                columna.Name = DGV1.Columns[i].Name;
                columna.CellTemplate = DGV1.Columns[i].CellTemplate;
                columna.ValueType = DGV1.Columns[i].ValueType;
                //columna.CellType = DGV1.Columns[i].CellType;
                DGV2.Columns.Add(columna);
            }
         } 




         public DataGridViewRow CloneWithValues(DataGridViewRow row)
        {
            DataGridViewRow clonedRow = (DataGridViewRow)row.Clone();
            for (Int32 index = 0; index < row.Cells.Count; index++)
            {
                clonedRow.Cells[index].Value = row.Cells[index].Value;
            }
            return clonedRow;
        }

         public void TraspasaLineas(DataGridView DGV1, DataGridView DGV2, int idfila)
         {
             DataGridViewRow fila = new DataGridViewRow();

             fila = CloneWithValues(DGV1.Rows[idfila]);
             DGV2.Rows.Add(fila);
             DGV1.Rows.RemoveAt(idfila);
         }

         public void TraspasaCampoLineas(DataGridView DGV1,string columna,  DataGridView DGV2, int idfila)
         {
             DataGridViewRow fila = new DataGridViewRow();

             fila = CloneWithValues(DGV1.Rows[idfila]);
             DGV2.Rows.Add(fila);
             DGV1.Rows.RemoveAt(idfila);
         }




        


        public void cargarImagenesDGV(DataGridView DGV1, int ColImg)
        {
                DataGridViewImageColumn dgvICol = new DataGridViewImageColumn();
                DGV1.Columns.Insert(3, dgvICol);
            
                for (int c = 0; c < DGV1.Rows.Count - 1; c++)
                {
                    //dataGridView1.Rows[c].Cells[3].Value = Image.FromFile(dataGridView1.Rows[c].Cells[2].Value.ToString());
                    DGV1.Rows[c].Cells[ColImg].Value = ResizeImage(DGV1.Rows[c].Cells[2].Value.ToString(), 100, DGV1.Rows[0].Height, false);
                }
                DGV1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                //dataGridView1.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells);
                //dataGridView1.AutoResizeRow = true;
             
        
        }





        public static Image ResizeImage(string file,
                             int width,
                             int height,
                             bool onlyResizeIfWider)
        {
            using (Image image = Image.FromFile(file))
            {
                // Prevent using images internal thumbnail
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);

                if (onlyResizeIfWider == true)
                {
                    if (image.Width <= width)
                    {
                        width = image.Width;
                    }
                }

                int newHeight = image.Height * width / image.Width;
                if (newHeight > height)
                {
                    // Resize with height instead
                    width = image.Width * height / image.Height;
                    newHeight = height;
                }

                Image NewImage = image.GetThumbnailImage(width,
                                                         newHeight,
                                                         null,
                                                         IntPtr.Zero);

                return NewImage;
            }
        }



    }
}
