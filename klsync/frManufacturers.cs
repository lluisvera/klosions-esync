﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frManufacturers : Form
    {

        private static frManufacturers m_FormDefInstance;
        public static frManufacturers DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frManufacturers();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frManufacturers()
        {
            InitializeComponent();
        }
        private MySqlConnection connection;

        private void btLoadManufacturersA3_Click(object sender, EventArgs e)
        {
            cargarFabricantesA3();
        }

        private void cargarFabricantesPS()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            MySqlConnection conn = new MySqlConnection(mysql.conexionDestino());
            conn.Open();
   
            csSqlScripts mySqlScript = new csSqlScripts();
            MySqlDataAdapter MyDA = new MySqlDataAdapter();

            MyDA.SelectCommand = new MySqlCommand(mySqlScript.selectFabricantesPS(), conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgvMarcasPS.DataSource = bSource;

        }
        private void cargarFabricantesA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectFabricantesA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvMarcasA3.DataSource = t;

        }

        private void cargarArticulosFabricantesA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectArticulosFabricantesPS(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvArticulosFabricantes.DataSource = t;

        }

        private void btLoadManufacturersPS_Click(object sender, EventArgs e)
        {
            cargarFabricantesPS();
        }

        private void btExportToPS_Click(object sender, EventArgs e)
        {
            insertMarcasEnPS(dgvMarcasA3.SelectedRows);
        }


        private void insertMarcasEnPS(DataGridViewSelectedRowCollection dgv)
        {

            string Lineas = "";
            string LineasLang = "";
            string LineasShop = "";
            int defLangPS= 1;
            string marca = "";
            string codMarca = "";
            csMySqlConnect conectorMySql = new csMySqlConnect();
            defLangPS = conectorMySql.defaultLangPS();
            for (int i = 0; i < dgv.Count; i++)
            {
                if (i > 0)
                {
                    Lineas = Lineas + ",";
                    LineasLang = LineasLang + ",";
                    LineasShop = LineasShop + ",";
                }

                codMarca = dgv[i].Cells["CODIGO"].Value.ToString();
                marca = dgv[i].Cells["FABRICANTE"].Value.ToString();
                marca = MySqlHelper.EscapeString(marca);
                

                Lineas = Lineas + "(" + codMarca + ",'" + marca + "','2014-09-01','2014-09-01',1)";
                LineasLang = LineasLang + "(" + codMarca + "," + defLangPS.ToString() + ")";
                LineasShop = LineasShop + "(" + codMarca + ",1)";
            }
            conectorMySql.InsertValoresEnTabla("ps_manufacturer", "(id_manufacturer, name, date_add, date_upd,active)", Lineas, "");
            conectorMySql.InsertValoresEnTabla("ps_manufacturer_lang", "(id_manufacturer, id_lang)", LineasLang, "");
            conectorMySql.InsertValoresEnTabla("ps_manufacturer_shop", "(id_manufacturer, id_shop)", LineasShop, "");
            
            MessageBox.Show("Exportación Finalizada");
        }

        private void cargarMarcasNuevas()
        {
            csMySqlConnect mySqlConnect= new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable marcas = new DataTable();
            DataTable marcasWeb = new DataTable();
            string marcasCreadasWeb="";
            marcasWeb = mySqlConnect.cargarTabla("select id_manufacturer from ps_manufacturer");
            for (int i=0; i<marcasWeb.Rows.Count;i++)
            {
                if (i > 0)
                {
                    marcasCreadasWeb = marcasCreadasWeb + ",";
                }

                marcasCreadasWeb = marcasCreadasWeb + marcasWeb.Rows[i].ItemArray[0].ToString();
            }
            marcas = sqlConnect.obtenerMarcasNuevas(marcasCreadasWeb);
            dgvMarcasNuevas.DataSource = marcas;


        
        }
        
        
        private void insertarMarcasNuevas()
        { 
        
        
        
        }

        private void btLoadFabricantesArticulos_Click(object sender, EventArgs e)
        {
            cargarArticulosFabricantesA3();
        }
      
          private string conexionDestino()
        {
        //Formamos la cadena de conexión en función de los valores de las Variables Globales
        string connectionString;
        connectionString = "SERVER=" + csGlobal.ServerPS + ";" + "DATABASE=" + csGlobal.databasePS + ";" + "UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";
        return connectionString;

        }
        private void btUpdateFabricantesArticulos_Click(object sender, EventArgs e)
   {
            try
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();
                for (int i = 0; i < dgvArticulosFabricantes.SelectedRows.Count; i++)
                {

                    string query = "update ps_product set id_manufacturer =" + dgvArticulosFabricantes.SelectedRows[i].Cells["COD_FABRICANTE"].Value.ToString() + " where id_product=" + dgvArticulosFabricantes.SelectedRows[i].Cells["KLS_ID_SHOP"].Value.ToString();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
            }

            finally
            {
                 
                    try
                    {
                        connection.Close();
                  
                    }
                    catch (MySqlException ex)
                    {
                        //MessageBox.Show(ex.Message);
                  
                    }
   
            }
        }

        private void btExportToA3_Click(object sender, EventArgs e)
        {
            copiarMarcasA3();
        }

        private void copiarMarcasA3()
        {
            string Lineas = "";
            //string LineasLang = "";
            //string LineasShop = "";
            csSqlConnects sqlConnect = new csSqlConnects();
            
            for (int i = 0; i < dgvMarcasPS.Rows.Count; i++)
            {
                if (i > 0)
                {
                    Lineas = Lineas + ",";
                }
                Lineas = Lineas + "('" + dgvMarcasPS.Rows[i].Cells[0].Value.ToString() + "','" + dgvMarcasPS.Rows[i].Cells[1].Value.ToString() + "')";
            }
            sqlConnect.insertarValoresEnTabla("KLS_MARCAS", "(KLS_CODMARCA, KLS_DESCMARCA)", Lineas);
            
        }

        private void btLoadMarcasNuevas_Click(object sender, EventArgs e)
        {
            cargarMarcasNuevas();
        }

        private void btInsertMarcasNuevas_Click(object sender, EventArgs e)
        {
            if (dgvMarcasNuevas.SelectedRows.Count > 0)
            {
                insertMarcasEnPS(dgvMarcasNuevas.SelectedRows);
            }
        }




        


     
    }
}
