﻿namespace klsync
{
    partial class frTaskScheduler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLoadTasks = new System.Windows.Forms.Button();
            this.lblNameTask = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.txtNameTask = new System.Windows.Forms.TextBox();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cblArgumentos = new System.Windows.Forms.CheckedListBox();
            this.txtIniciarEnTask = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRutaFinderTask = new System.Windows.Forms.Button();
            this.txtRutaTask = new System.Windows.Forms.TextBox();
            this.lblRutaTask = new System.Windows.Forms.Label();
            this.lblArgsTask = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpTimeTask = new System.Windows.Forms.DateTimePicker();
            this.dtpDateTask = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.comboRepetirTask = new System.Windows.Forms.ComboBox();
            this.lblPeriodicidadTask = new System.Windows.Forms.Label();
            this.treeTask = new System.Windows.Forms.TreeView();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1039, 794);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1031, 761);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Editor";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1025, 755);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.treeTask, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1025, 755);
            this.tableLayoutPanel1.TabIndex = 20;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.btnLoadTasks);
            this.panel2.Controls.Add(this.lblNameTask);
            this.panel2.Controls.Add(this.lblError);
            this.panel2.Controls.Add(this.txtNameTask);
            this.panel2.Controls.Add(this.btnAddTask);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(506, 749);
            this.panel2.TabIndex = 0;
            // 
            // btnLoadTasks
            // 
            this.btnLoadTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadTasks.CausesValidation = false;
            this.btnLoadTasks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadTasks.Location = new System.Drawing.Point(239, 596);
            this.btnLoadTasks.Name = "btnLoadTasks";
            this.btnLoadTasks.Size = new System.Drawing.Size(124, 45);
            this.btnLoadTasks.TabIndex = 20;
            this.btnLoadTasks.Text = "Cargar Tareas";
            this.btnLoadTasks.UseVisualStyleBackColor = true;
            this.btnLoadTasks.Click += new System.EventHandler(this.btnLoadTasks_Click);
            // 
            // lblNameTask
            // 
            this.lblNameTask.AutoSize = true;
            this.lblNameTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameTask.Location = new System.Drawing.Point(18, 34);
            this.lblNameTask.Name = "lblNameTask";
            this.lblNameTask.Size = new System.Drawing.Size(148, 20);
            this.lblNameTask.TabIndex = 16;
            this.lblNameTask.Text = "Nombre de la Tarea";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(13, 596);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 20);
            this.lblError.TabIndex = 19;
            // 
            // txtNameTask
            // 
            this.txtNameTask.CausesValidation = false;
            this.txtNameTask.Location = new System.Drawing.Point(172, 34);
            this.txtNameTask.Name = "txtNameTask";
            this.txtNameTask.Size = new System.Drawing.Size(187, 26);
            this.txtNameTask.TabIndex = 15;
            // 
            // btnAddTask
            // 
            this.btnAddTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddTask.CausesValidation = false;
            this.btnAddTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTask.Location = new System.Drawing.Point(369, 596);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(124, 45);
            this.btnAddTask.TabIndex = 14;
            this.btnAddTask.Text = "Añadir Tarea";
            this.btnAddTask.UseVisualStyleBackColor = true;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cblArgumentos);
            this.groupBox1.Controls.Add(this.txtIniciarEnTask);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnRutaFinderTask);
            this.groupBox1.Controls.Add(this.txtRutaTask);
            this.groupBox1.Controls.Add(this.lblRutaTask);
            this.groupBox1.Controls.Add(this.lblArgsTask);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(10, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(483, 365);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acciones";
            // 
            // cblArgumentos
            // 
            this.cblArgumentos.FormattingEnabled = true;
            this.cblArgumentos.Items.AddRange(new object[] {
            "categorias",
            "documentos",
            "stock",
            "tags",
            "tallas",
            "replog",
            "precios tarifas clientes",
            "precios descuentos",
            "precios familias clientes",
            "dismay",
            "amigos"});
            this.cblArgumentos.Location = new System.Drawing.Point(162, 74);
            this.cblArgumentos.Name = "cblArgumentos";
            this.cblArgumentos.Size = new System.Drawing.Size(307, 235);
            this.cblArgumentos.TabIndex = 18;
            // 
            // txtIniciarEnTask
            // 
            this.txtIniciarEnTask.CausesValidation = false;
            this.txtIniciarEnTask.Location = new System.Drawing.Point(162, 315);
            this.txtIniciarEnTask.Name = "txtIniciarEnTask";
            this.txtIniciarEnTask.Size = new System.Drawing.Size(187, 26);
            this.txtIniciarEnTask.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Iniciar en";
            // 
            // btnRutaFinderTask
            // 
            this.btnRutaFinderTask.Location = new System.Drawing.Point(362, 25);
            this.btnRutaFinderTask.Name = "btnRutaFinderTask";
            this.btnRutaFinderTask.Size = new System.Drawing.Size(107, 34);
            this.btnRutaFinderTask.TabIndex = 7;
            this.btnRutaFinderTask.Text = "Examinar...";
            this.btnRutaFinderTask.UseVisualStyleBackColor = true;
            this.btnRutaFinderTask.Click += new System.EventHandler(this.btnRutaFinderTask_Click);
            // 
            // txtRutaTask
            // 
            this.txtRutaTask.CausesValidation = false;
            this.txtRutaTask.Enabled = false;
            this.txtRutaTask.Location = new System.Drawing.Point(162, 29);
            this.txtRutaTask.Name = "txtRutaTask";
            this.txtRutaTask.Size = new System.Drawing.Size(187, 26);
            this.txtRutaTask.TabIndex = 3;
            // 
            // lblRutaTask
            // 
            this.lblRutaTask.AutoSize = true;
            this.lblRutaTask.Location = new System.Drawing.Point(8, 29);
            this.lblRutaTask.Name = "lblRutaTask";
            this.lblRutaTask.Size = new System.Drawing.Size(44, 20);
            this.lblRutaTask.TabIndex = 4;
            this.lblRutaTask.Text = "Ruta";
            // 
            // lblArgsTask
            // 
            this.lblArgsTask.AutoSize = true;
            this.lblArgsTask.Location = new System.Drawing.Point(8, 74);
            this.lblArgsTask.Name = "lblArgsTask";
            this.lblArgsTask.Size = new System.Drawing.Size(96, 20);
            this.lblArgsTask.TabIndex = 6;
            this.lblArgsTask.Text = "Argumentos";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dtpTimeTask);
            this.groupBox2.Controls.Add(this.dtpDateTask);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.comboRepetirTask);
            this.groupBox2.Controls.Add(this.lblPeriodicidadTask);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(10, 444);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(483, 146);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Desencadenadores";
            // 
            // dtpTimeTask
            // 
            this.dtpTimeTask.CausesValidation = false;
            this.dtpTimeTask.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeTask.Location = new System.Drawing.Point(246, 80);
            this.dtpTimeTask.Name = "dtpTimeTask";
            this.dtpTimeTask.Size = new System.Drawing.Size(104, 26);
            this.dtpTimeTask.TabIndex = 14;
            // 
            // dtpDateTask
            // 
            this.dtpDateTask.CausesValidation = false;
            this.dtpDateTask.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateTask.Location = new System.Drawing.Point(123, 79);
            this.dtpDateTask.Name = "dtpDateTask";
            this.dtpDateTask.Size = new System.Drawing.Size(106, 26);
            this.dtpDateTask.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Inicio";
            // 
            // comboRepetirTask
            // 
            this.comboRepetirTask.CausesValidation = false;
            this.comboRepetirTask.FormattingEnabled = true;
            this.comboRepetirTask.Items.AddRange(new object[] {
            "5 minutos",
            "10 minutos",
            "15 minutos",
            "30 minutos",
            "1 hora",
            "12 horas",
            "24 horas",
            "semana"});
            this.comboRepetirTask.Location = new System.Drawing.Point(162, 29);
            this.comboRepetirTask.Name = "comboRepetirTask";
            this.comboRepetirTask.Size = new System.Drawing.Size(187, 28);
            this.comboRepetirTask.TabIndex = 9;
            // 
            // lblPeriodicidadTask
            // 
            this.lblPeriodicidadTask.AutoSize = true;
            this.lblPeriodicidadTask.Location = new System.Drawing.Point(8, 29);
            this.lblPeriodicidadTask.Name = "lblPeriodicidadTask";
            this.lblPeriodicidadTask.Size = new System.Drawing.Size(100, 20);
            this.lblPeriodicidadTask.TabIndex = 8;
            this.lblPeriodicidadTask.Text = "Repetir cada";
            // 
            // treeTask
            // 
            this.treeTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeTask.Location = new System.Drawing.Point(515, 3);
            this.treeTask.Name = "treeTask";
            this.treeTask.Size = new System.Drawing.Size(507, 749);
            this.treeTask.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.CausesValidation = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(276, 691);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(217, 45);
            this.button1.TabIndex = 21;
            this.button1.Text = "Abrir Programador Tareas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frTaskScheduler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 794);
            this.Controls.Add(this.tabControl1);
            this.Name = "frTaskScheduler";
            this.Text = "Programador de Tareas";
            this.Load += new System.EventHandler(this.frTaskScheduler_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpTimeTask;
        private System.Windows.Forms.DateTimePicker dtpDateTask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboRepetirTask;
        private System.Windows.Forms.Label lblPeriodicidadTask;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRutaTask;
        private System.Windows.Forms.Label lblRutaTask;
        private System.Windows.Forms.Label lblArgsTask;
        private System.Windows.Forms.TextBox txtNameTask;
        private System.Windows.Forms.Label lblNameTask;
        private System.Windows.Forms.Button btnRutaFinderTask;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TextBox txtIniciarEnTask;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView treeTask;
        private System.Windows.Forms.Button btnLoadTasks;
        private System.Windows.Forms.CheckedListBox cblArgumentos;
        private System.Windows.Forms.Button button1;
    }
}