﻿namespace klsync
{
    partial class frArticulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frArticulos));
            this.contextMenuProd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aCCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.subirArticuloAPrestashopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.activarEnTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desactivarEnTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.artículosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarArticulosConStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarArticulosMalCodificadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarIDEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarArticulosEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertarDescripcionesA3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarAtributosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artículoSeleccionadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.resetearAtributosSelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarDescripcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.eliminarProductoPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.validarPresenciaEnLaTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarImpuestosEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarFechaDeDisponibilidadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarMedidasEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imágenesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarProductosPorImagenesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.subirImagenesAPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replicarImagenesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.actualizarArtículosEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.generarFicheroLogistica = new System.Windows.Forms.ToolStripMenuItem();
            this.contextPSaA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bajarProductosAA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarIDsDePSAA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.aCTUALIZARToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarPreciosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarEstadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.ponerVisibleEnTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ponerInvisibleEnTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincFamiliasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.tabAdvanced = new System.Windows.Forms.TabControl();
            this.tabA3toPS = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.cBoxEnTienda = new System.Windows.Forms.CheckBox();
            this.cboxDisponible = new System.Windows.Forms.CheckBox();
            this.btClearFilter = new System.Windows.Forms.Button();
            this.tbFilterDesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboxColecciones = new System.Windows.Forms.ComboBox();
            this.cboxMarcas = new System.Windows.Forms.ComboBox();
            this.btLoadAdvance = new System.Windows.Forms.Button();
            this.lblColeccion = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.btHide = new System.Windows.Forms.Button();
            this.btShow = new System.Windows.Forms.Button();
            this.dgvProductosA3 = new System.Windows.Forms.DataGridView();
            this.tabPStoA3 = new System.Windows.Forms.TabPage();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btLoadProductosPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btCheckIdA3 = new System.Windows.Forms.ToolStripButton();
            this.btAsignarIdA3ERP = new System.Windows.Forms.ToolStripButton();
            this.ProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.cbox_ActualizarEstado = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonLockUnlock = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolstripButtonA3 = new System.Windows.Forms.ToolStripButton();
            this.dgvProductosPS = new System.Windows.Forms.DataGridView();
            this.tabCat = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.treeCat1 = new System.Windows.Forms.TreeView();
            this.cmsNewCat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cATEGORIASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.irACategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renombrarCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activarCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desactivarCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarACategoriaPadreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.regenerarPosicionesCategoriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.asignarImagenCategoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.expandirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprimirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.comboSusAdd = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBtnReplicarIMG = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabAsign = new System.Windows.Forms.TabPage();
            this.dgvCat = new System.Windows.Forms.DataGridView();
            this.tabCreation = new System.Windows.Forms.TabPage();
            this.btAddCategories = new System.Windows.Forms.Button();
            this.dgvNewCategories = new System.Windows.Forms.DataGridView();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.treeCat = new System.Windows.Forms.TreeView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSendProductToPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btSincronizarStockNoTallas = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btSincronizarTallasColores = new System.Windows.Forms.ToolStripButton();
            this.btUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.tssLb = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelBusquedaCtrlFArticulos = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelProgreso = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslbInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssSeparator = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssInfoTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuProd.SuspendLayout();
            this.contextPSaA3.SuspendLayout();
            this.tabAdvanced.SuspendLayout();
            this.tabA3toPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosA3)).BeginInit();
            this.tabPStoA3.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosPS)).BeginInit();
            this.tabCat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.cmsNewCat.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabAsign.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCat)).BeginInit();
            this.tabCreation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewCategories)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tssLb.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuProd
            // 
            this.contextMenuProd.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuProd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCCIONESToolStripMenuItem,
            this.toolStripMenuItem1,
            this.subirArticuloAPrestashopToolStripMenuItem,
            this.toolStripMenuItem4,
            this.activarEnTiendaToolStripMenuItem,
            this.desactivarEnTiendaToolStripMenuItem,
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem,
            this.toolStripMenuItem2,
            this.artículosToolStripMenuItem,
            this.imágenesToolStripMenuItem,
            this.toolStripSeparator7,
            this.actualizarArtículosEnPSToolStripMenuItem,
            this.toolStripSeparator17,
            this.generarFicheroLogistica});
            this.contextMenuProd.Name = "contextMenuProd";
            this.contextMenuProd.Size = new System.Drawing.Size(401, 268);
            this.contextMenuProd.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuProd_Opening);
            // 
            // aCCIONESToolStripMenuItem
            // 
            this.aCCIONESToolStripMenuItem.Enabled = false;
            this.aCCIONESToolStripMenuItem.Name = "aCCIONESToolStripMenuItem";
            this.aCCIONESToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.aCCIONESToolStripMenuItem.Text = "ACCIONES";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(397, 6);
            // 
            // subirArticuloAPrestashopToolStripMenuItem
            // 
            this.subirArticuloAPrestashopToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("subirArticuloAPrestashopToolStripMenuItem.Image")));
            this.subirArticuloAPrestashopToolStripMenuItem.Name = "subirArticuloAPrestashopToolStripMenuItem";
            this.subirArticuloAPrestashopToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.subirArticuloAPrestashopToolStripMenuItem.Text = "Subir articulo a Prestashop";
            this.subirArticuloAPrestashopToolStripMenuItem.Click += new System.EventHandler(this.subirArticuloAPrestashopToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(397, 6);
            // 
            // activarEnTiendaToolStripMenuItem
            // 
            this.activarEnTiendaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("activarEnTiendaToolStripMenuItem.Image")));
            this.activarEnTiendaToolStripMenuItem.Name = "activarEnTiendaToolStripMenuItem";
            this.activarEnTiendaToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.activarEnTiendaToolStripMenuItem.Text = "&Activar en Tienda";
            this.activarEnTiendaToolStripMenuItem.Click += new System.EventHandler(this.activarEnTiendaToolStripMenuItem_Click);
            // 
            // desactivarEnTiendaToolStripMenuItem
            // 
            this.desactivarEnTiendaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("desactivarEnTiendaToolStripMenuItem.Image")));
            this.desactivarEnTiendaToolStripMenuItem.Name = "desactivarEnTiendaToolStripMenuItem";
            this.desactivarEnTiendaToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.desactivarEnTiendaToolStripMenuItem.Text = "&Desactivar en Tienda";
            this.desactivarEnTiendaToolStripMenuItem.Click += new System.EventHandler(this.desactivarEnTiendaToolStripMenuItem_Click);
            // 
            // ponerANullLosProductosSELECCIONADOSToolStripMenuItem
            // 
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.DoubleClickEnabled = true;
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Image")));
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Name = "ponerANullLosProductosSELECCIONADOSToolStripMenuItem";
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Text = "Poner a NULL los Productos &SELECCIONADOS";
            this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem.Click += new System.EventHandler(this.ponerANullLosProductosSELECCIONADOSToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(397, 6);
            // 
            // artículosToolStripMenuItem
            // 
            this.artículosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarArticulosConStockToolStripMenuItem,
            this.cargarArticulosMalCodificadosToolStripMenuItem,
            this.actualizarIDEnA3ToolStripMenuItem,
            this.verificarArticulosEnA3ERPToolStripMenuItem,
            this.insertarDescripcionesA3ToolStripMenuItem1,
            this.sincronizarAtributosToolStripMenuItem,
            this.sincronizarDescripcionesToolStripMenuItem,
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem,
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem,
            this.toolStripMenuItem8,
            this.eliminarProductoPSToolStripMenuItem,
            this.toolStripSeparator9,
            this.validarPresenciaEnLaTiendaToolStripMenuItem,
            this.actualizarToolStripMenuItem,
            this.toolStripMenuItem14,
            this.actualizarImpuestosEnPSToolStripMenuItem,
            this.actualizarFechaDeDisponibilidadToolStripMenuItem,
            this.pesoToolStripMenuItem,
            this.actualizarMedidasEnPSToolStripMenuItem});
            this.artículosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("artículosToolStripMenuItem.Image")));
            this.artículosToolStripMenuItem.Name = "artículosToolStripMenuItem";
            this.artículosToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.artículosToolStripMenuItem.Text = "Artículos";
            // 
            // cargarArticulosConStockToolStripMenuItem
            // 
            this.cargarArticulosConStockToolStripMenuItem.Name = "cargarArticulosConStockToolStripMenuItem";
            this.cargarArticulosConStockToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.cargarArticulosConStockToolStripMenuItem.Text = "Cargar stock articulos";
            this.cargarArticulosConStockToolStripMenuItem.Click += new System.EventHandler(this.cargarArticulosConStockToolStripMenuItem_Click);
            // 
            // cargarArticulosMalCodificadosToolStripMenuItem
            // 
            this.cargarArticulosMalCodificadosToolStripMenuItem.Name = "cargarArticulosMalCodificadosToolStripMenuItem";
            this.cargarArticulosMalCodificadosToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.cargarArticulosMalCodificadosToolStripMenuItem.Text = "Cargar articulos mal codificados";
            this.cargarArticulosMalCodificadosToolStripMenuItem.Click += new System.EventHandler(this.cargarArticulosMalCodificadosToolStripMenuItem_Click);
            // 
            // actualizarIDEnA3ToolStripMenuItem
            // 
            this.actualizarIDEnA3ToolStripMenuItem.Name = "actualizarIDEnA3ToolStripMenuItem";
            this.actualizarIDEnA3ToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarIDEnA3ToolStripMenuItem.Text = "Actualizar ID seleccionados en A3";
            this.actualizarIDEnA3ToolStripMenuItem.Click += new System.EventHandler(this.actualizarIDEnA3ToolStripMenuItem_Click);
            // 
            // verificarArticulosEnA3ERPToolStripMenuItem
            // 
            this.verificarArticulosEnA3ERPToolStripMenuItem.Name = "verificarArticulosEnA3ERPToolStripMenuItem";
            this.verificarArticulosEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.verificarArticulosEnA3ERPToolStripMenuItem.Text = "Verificar articulos en A3";
            this.verificarArticulosEnA3ERPToolStripMenuItem.Visible = false;
            this.verificarArticulosEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.verificarArticulosEnA3ERPToolStripMenuItem_Click);
            // 
            // insertarDescripcionesA3ToolStripMenuItem1
            // 
            this.insertarDescripcionesA3ToolStripMenuItem1.Name = "insertarDescripcionesA3ToolStripMenuItem1";
            this.insertarDescripcionesA3ToolStripMenuItem1.Size = new System.Drawing.Size(382, 26);
            this.insertarDescripcionesA3ToolStripMenuItem1.Text = "Insertar descripciones A3";
            this.insertarDescripcionesA3ToolStripMenuItem1.Visible = false;
            this.insertarDescripcionesA3ToolStripMenuItem1.Click += new System.EventHandler(this.insertarDescripcionesA3ToolStripMenuItem1_Click);
            // 
            // sincronizarAtributosToolStripMenuItem
            // 
            this.sincronizarAtributosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artículoSeleccionadoToolStripMenuItem,
            this.todosToolStripMenuItem,
            this.toolStripMenuItem10,
            this.resetearAtributosSelToolStripMenuItem});
            this.sincronizarAtributosToolStripMenuItem.Name = "sincronizarAtributosToolStripMenuItem";
            this.sincronizarAtributosToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.sincronizarAtributosToolStripMenuItem.Text = "Sincronizar Atributos";
            // 
            // artículoSeleccionadoToolStripMenuItem
            // 
            this.artículoSeleccionadoToolStripMenuItem.Name = "artículoSeleccionadoToolStripMenuItem";
            this.artículoSeleccionadoToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.artículoSeleccionadoToolStripMenuItem.Text = "Sincronizar seleccionado";
            this.artículoSeleccionadoToolStripMenuItem.Click += new System.EventHandler(this.artículoSeleccionadoToolStripMenuItem_Click);
            // 
            // todosToolStripMenuItem
            // 
            this.todosToolStripMenuItem.Name = "todosToolStripMenuItem";
            this.todosToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.todosToolStripMenuItem.Text = "Todos";
            this.todosToolStripMenuItem.Click += new System.EventHandler(this.todosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(248, 6);
            // 
            // resetearAtributosSelToolStripMenuItem
            // 
            this.resetearAtributosSelToolStripMenuItem.Name = "resetearAtributosSelToolStripMenuItem";
            this.resetearAtributosSelToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.resetearAtributosSelToolStripMenuItem.Text = "Resetear seleccionados";
            this.resetearAtributosSelToolStripMenuItem.Click += new System.EventHandler(this.resetearAtributosSelToolStripMenuItem_Click);
            // 
            // sincronizarDescripcionesToolStripMenuItem
            // 
            this.sincronizarDescripcionesToolStripMenuItem.Name = "sincronizarDescripcionesToolStripMenuItem";
            this.sincronizarDescripcionesToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.sincronizarDescripcionesToolStripMenuItem.Text = "Sincronizar Descripciones";
            this.sincronizarDescripcionesToolStripMenuItem.Click += new System.EventHandler(this.sincronizarDescripcionesToolStripMenuItem_Click);
            // 
            // validarPresenciaEnTiendaConReferenciaToolStripMenuItem
            // 
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem.Name = "validarPresenciaEnTiendaConReferenciaToolStripMenuItem";
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem.Text = "Sincronizar articulos por referencia";
            this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem.Click += new System.EventHandler(this.validarPresenciaEnTiendaConReferenciaToolStripMenuItem_Click);
            // 
            // sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem
            // 
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem.Name = "sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem";
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem.Text = "Sincronizar Activo(PS)-Visible en tienda(A3)";
            this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem.Click += new System.EventHandler(this.sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(379, 6);
            // 
            // eliminarProductoPSToolStripMenuItem
            // 
            this.eliminarProductoPSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("eliminarProductoPSToolStripMenuItem.Image")));
            this.eliminarProductoPSToolStripMenuItem.Name = "eliminarProductoPSToolStripMenuItem";
            this.eliminarProductoPSToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.eliminarProductoPSToolStripMenuItem.Text = "Eliminar producto PS";
            this.eliminarProductoPSToolStripMenuItem.Click += new System.EventHandler(this.eliminarProductoPSToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(379, 6);
            // 
            // validarPresenciaEnLaTiendaToolStripMenuItem
            // 
            this.validarPresenciaEnLaTiendaToolStripMenuItem.Name = "validarPresenciaEnLaTiendaToolStripMenuItem";
            this.validarPresenciaEnLaTiendaToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.validarPresenciaEnLaTiendaToolStripMenuItem.Text = "Validar Presencia en la Tienda";
            this.validarPresenciaEnLaTiendaToolStripMenuItem.Click += new System.EventHandler(this.validarPresenciaEnLaTiendaToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarToolStripMenuItem.Text = "Actualizar Stock (Selección)";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click_1);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(382, 26);
            this.toolStripMenuItem14.Text = "Actualizar referencias en PS";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // actualizarImpuestosEnPSToolStripMenuItem
            // 
            this.actualizarImpuestosEnPSToolStripMenuItem.Name = "actualizarImpuestosEnPSToolStripMenuItem";
            this.actualizarImpuestosEnPSToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarImpuestosEnPSToolStripMenuItem.Text = "Actualizar impuestos en PS";
            this.actualizarImpuestosEnPSToolStripMenuItem.Click += new System.EventHandler(this.actualizarImpuestosEnPSToolStripMenuItem_Click);
            // 
            // actualizarFechaDeDisponibilidadToolStripMenuItem
            // 
            this.actualizarFechaDeDisponibilidadToolStripMenuItem.Name = "actualizarFechaDeDisponibilidadToolStripMenuItem";
            this.actualizarFechaDeDisponibilidadToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarFechaDeDisponibilidadToolStripMenuItem.Text = "Actualizar fecha de disponibilidad";
            this.actualizarFechaDeDisponibilidadToolStripMenuItem.Click += new System.EventHandler(this.actualizarFechaDeDisponibilidadToolStripMenuItem_Click);
            // 
            // pesoToolStripMenuItem
            // 
            this.pesoToolStripMenuItem.Name = "pesoToolStripMenuItem";
            this.pesoToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.pesoToolStripMenuItem.Text = "Actualizar peso en PS";
            this.pesoToolStripMenuItem.Click += new System.EventHandler(this.pesoToolStripMenuItem_Click);
            // 
            // actualizarMedidasEnPSToolStripMenuItem
            // 
            this.actualizarMedidasEnPSToolStripMenuItem.Name = "actualizarMedidasEnPSToolStripMenuItem";
            this.actualizarMedidasEnPSToolStripMenuItem.Size = new System.Drawing.Size(382, 26);
            this.actualizarMedidasEnPSToolStripMenuItem.Text = "Actualizar medidas en PS";
            this.actualizarMedidasEnPSToolStripMenuItem.Click += new System.EventHandler(this.actualizarMedidasEnPSToolStripMenuItem_Click);
            // 
            // imágenesToolStripMenuItem
            // 
            this.imágenesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarProductosPorImagenesToolStripMenuItem1,
            this.subirImagenesAPSToolStripMenuItem,
            this.replicarImagenesToolStripMenuItem1});
            this.imágenesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("imágenesToolStripMenuItem.Image")));
            this.imágenesToolStripMenuItem.Name = "imágenesToolStripMenuItem";
            this.imágenesToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.imágenesToolStripMenuItem.Text = "Imágenes";
            // 
            // cargarProductosPorImagenesToolStripMenuItem1
            // 
            this.cargarProductosPorImagenesToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("cargarProductosPorImagenesToolStripMenuItem1.Image")));
            this.cargarProductosPorImagenesToolStripMenuItem1.Name = "cargarProductosPorImagenesToolStripMenuItem1";
            this.cargarProductosPorImagenesToolStripMenuItem1.Size = new System.Drawing.Size(300, 26);
            this.cargarProductosPorImagenesToolStripMenuItem1.Text = "Cargar productos por imagenes";
            this.cargarProductosPorImagenesToolStripMenuItem1.Click += new System.EventHandler(this.cargarProductosPorImagenesToolStripMenuItem1_Click);
            // 
            // subirImagenesAPSToolStripMenuItem
            // 
            this.subirImagenesAPSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("subirImagenesAPSToolStripMenuItem.Image")));
            this.subirImagenesAPSToolStripMenuItem.Name = "subirImagenesAPSToolStripMenuItem";
            this.subirImagenesAPSToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.subirImagenesAPSToolStripMenuItem.Text = "Subir imagenes a PS";
            this.subirImagenesAPSToolStripMenuItem.Click += new System.EventHandler(this.subirImagenesAPSToolStripMenuItem_Click);
            // 
            // replicarImagenesToolStripMenuItem1
            // 
            this.replicarImagenesToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("replicarImagenesToolStripMenuItem1.Image")));
            this.replicarImagenesToolStripMenuItem1.Name = "replicarImagenesToolStripMenuItem1";
            this.replicarImagenesToolStripMenuItem1.Size = new System.Drawing.Size(300, 26);
            this.replicarImagenesToolStripMenuItem1.Text = "Replicar imagenes";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(397, 6);
            // 
            // actualizarArtículosEnPSToolStripMenuItem
            // 
            this.actualizarArtículosEnPSToolStripMenuItem.Name = "actualizarArtículosEnPSToolStripMenuItem";
            this.actualizarArtículosEnPSToolStripMenuItem.Size = new System.Drawing.Size(400, 26);
            this.actualizarArtículosEnPSToolStripMenuItem.Text = "Actualizar Artículos en PS";
            this.actualizarArtículosEnPSToolStripMenuItem.Visible = false;
            this.actualizarArtículosEnPSToolStripMenuItem.Click += new System.EventHandler(this.actualizarArtículosEnPSToolStripMenuItem_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(397, 6);
            // 
            // generarFicheroLogistica
            // 
            this.generarFicheroLogistica.Name = "generarFicheroLogistica";
            this.generarFicheroLogistica.Size = new System.Drawing.Size(400, 26);
            this.generarFicheroLogistica.Text = "Generar fichero Logística";
            this.generarFicheroLogistica.Click += new System.EventHandler(this.generarFicheroLogistica_Click);
            // 
            // contextPSaA3
            // 
            this.contextPSaA3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextPSaA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bajarProductosAA3ToolStripMenuItem,
            this.eliminarProductosToolStripMenuItem,
            this.asignarIDsDePSAA3ToolStripMenuItem,
            this.toolStripMenuItem3,
            this.aCTUALIZARToolStripMenuItem1,
            this.toolStripMenuItem7,
            this.toolStripMenuItem5,
            this.actualizarPreciosToolStripMenuItem,
            this.actualizarEstadosToolStripMenuItem,
            this.toolStripMenuItem6,
            this.ponerVisibleEnTiendaToolStripMenuItem,
            this.ponerInvisibleEnTiendaToolStripMenuItem,
            this.sincFamiliasToolStripMenuItem,
            this.toolStripSeparator16});
            this.contextPSaA3.Name = "contextPSaA3";
            this.contextPSaA3.Size = new System.Drawing.Size(289, 288);
            // 
            // bajarProductosAA3ToolStripMenuItem
            // 
            this.bajarProductosAA3ToolStripMenuItem.Name = "bajarProductosAA3ToolStripMenuItem";
            this.bajarProductosAA3ToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.bajarProductosAA3ToolStripMenuItem.Text = "Bajar productos a A3";
            this.bajarProductosAA3ToolStripMenuItem.Click += new System.EventHandler(this.bajarProductosAA3ToolStripMenuItem_Click);
            // 
            // eliminarProductosToolStripMenuItem
            // 
            this.eliminarProductosToolStripMenuItem.Name = "eliminarProductosToolStripMenuItem";
            this.eliminarProductosToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.eliminarProductosToolStripMenuItem.Text = "&Eliminar productos";
            this.eliminarProductosToolStripMenuItem.Visible = false;
            // 
            // asignarIDsDePSAA3ToolStripMenuItem
            // 
            this.asignarIDsDePSAA3ToolStripMenuItem.Name = "asignarIDsDePSAA3ToolStripMenuItem";
            this.asignarIDsDePSAA3ToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.asignarIDsDePSAA3ToolStripMenuItem.Text = "Asignar ID\'s de PS a A3";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(285, 6);
            // 
            // aCTUALIZARToolStripMenuItem1
            // 
            this.aCTUALIZARToolStripMenuItem1.Enabled = false;
            this.aCTUALIZARToolStripMenuItem1.Name = "aCTUALIZARToolStripMenuItem1";
            this.aCTUALIZARToolStripMenuItem1.Size = new System.Drawing.Size(288, 26);
            this.aCTUALIZARToolStripMenuItem1.Text = "ACTUALIZAR";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(285, 6);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(288, 26);
            this.toolStripMenuItem5.Text = "Actualizar &Nombres en A3ERP";
            // 
            // actualizarPreciosToolStripMenuItem
            // 
            this.actualizarPreciosToolStripMenuItem.Name = "actualizarPreciosToolStripMenuItem";
            this.actualizarPreciosToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.actualizarPreciosToolStripMenuItem.Text = "Actualizar Precios";
            // 
            // actualizarEstadosToolStripMenuItem
            // 
            this.actualizarEstadosToolStripMenuItem.Name = "actualizarEstadosToolStripMenuItem";
            this.actualizarEstadosToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.actualizarEstadosToolStripMenuItem.Text = "Actualizar Estados";
            this.actualizarEstadosToolStripMenuItem.ToolTipText = "Manda PS";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(285, 6);
            // 
            // ponerVisibleEnTiendaToolStripMenuItem
            // 
            this.ponerVisibleEnTiendaToolStripMenuItem.Name = "ponerVisibleEnTiendaToolStripMenuItem";
            this.ponerVisibleEnTiendaToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.ponerVisibleEnTiendaToolStripMenuItem.Text = "&ACTIVAR en tienda";
            // 
            // ponerInvisibleEnTiendaToolStripMenuItem
            // 
            this.ponerInvisibleEnTiendaToolStripMenuItem.Name = "ponerInvisibleEnTiendaToolStripMenuItem";
            this.ponerInvisibleEnTiendaToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.ponerInvisibleEnTiendaToolStripMenuItem.Text = "&DESACTIVAR en tienda";
            // 
            // sincFamiliasToolStripMenuItem
            // 
            this.sincFamiliasToolStripMenuItem.Name = "sincFamiliasToolStripMenuItem";
            this.sincFamiliasToolStripMenuItem.Size = new System.Drawing.Size(288, 26);
            this.sincFamiliasToolStripMenuItem.Text = "Sinc. Familias";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(285, 6);
            // 
            // tabAdvanced
            // 
            this.tabAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabAdvanced.Controls.Add(this.tabA3toPS);
            this.tabAdvanced.Controls.Add(this.tabPStoA3);
            this.tabAdvanced.Controls.Add(this.tabCat);
            this.tabAdvanced.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabAdvanced.Location = new System.Drawing.Point(0, 62);
            this.tabAdvanced.Multiline = true;
            this.tabAdvanced.Name = "tabAdvanced";
            this.tabAdvanced.SelectedIndex = 0;
            this.tabAdvanced.Size = new System.Drawing.Size(1235, 628);
            this.tabAdvanced.TabIndex = 20;
            this.tabAdvanced.Click += new System.EventHandler(this.tabAdvanced_Click);
            // 
            // tabA3toPS
            // 
            this.tabA3toPS.Controls.Add(this.splitContainer2);
            this.tabA3toPS.Location = new System.Drawing.Point(4, 29);
            this.tabA3toPS.Name = "tabA3toPS";
            this.tabA3toPS.Padding = new System.Windows.Forms.Padding(3);
            this.tabA3toPS.Size = new System.Drawing.Size(1227, 595);
            this.tabA3toPS.TabIndex = 0;
            this.tabA3toPS.Text = "A3 a PS";
            this.tabA3toPS.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.cBoxEnTienda);
            this.splitContainer2.Panel1.Controls.Add(this.cboxDisponible);
            this.splitContainer2.Panel1.Controls.Add(this.btClearFilter);
            this.splitContainer2.Panel1.Controls.Add(this.tbFilterDesc);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.cboxColecciones);
            this.splitContainer2.Panel1.Controls.Add(this.cboxMarcas);
            this.splitContainer2.Panel1.Controls.Add(this.btLoadAdvance);
            this.splitContainer2.Panel1.Controls.Add(this.lblColeccion);
            this.splitContainer2.Panel1.Controls.Add(this.lblMarca);
            this.splitContainer2.Panel1.Controls.Add(this.btHide);
            this.splitContainer2.Panel1.Controls.Add(this.btShow);
            this.splitContainer2.Panel1MinSize = 0;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvProductosA3);
            this.splitContainer2.Panel2MinSize = 0;
            this.splitContainer2.Size = new System.Drawing.Size(1221, 589);
            this.splitContainer2.SplitterDistance = 232;
            this.splitContainer2.SplitterWidth = 2;
            this.splitContainer2.TabIndex = 2;
            // 
            // cBoxEnTienda
            // 
            this.cBoxEnTienda.AutoSize = true;
            this.cBoxEnTienda.Location = new System.Drawing.Point(57, 268);
            this.cBoxEnTienda.Name = "cBoxEnTienda";
            this.cBoxEnTienda.Size = new System.Drawing.Size(100, 24);
            this.cBoxEnTienda.TabIndex = 13;
            this.cBoxEnTienda.Text = "En Tienda";
            this.cBoxEnTienda.UseVisualStyleBackColor = true;
            this.cBoxEnTienda.CheckedChanged += new System.EventHandler(this.cBoxEnTienda_CheckedChanged);
            // 
            // cboxDisponible
            // 
            this.cboxDisponible.AutoSize = true;
            this.cboxDisponible.Location = new System.Drawing.Point(57, 238);
            this.cboxDisponible.Name = "cboxDisponible";
            this.cboxDisponible.Size = new System.Drawing.Size(147, 24);
            this.cboxDisponible.TabIndex = 12;
            this.cboxDisponible.Text = "Stock Disponible";
            this.cboxDisponible.UseVisualStyleBackColor = true;
            this.cboxDisponible.CheckedChanged += new System.EventHandler(this.cboxDisponible_CheckedChanged);
            // 
            // btClearFilter
            // 
            this.btClearFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btClearFilter.BackgroundImage = global::klsync.Properties.Resources.cross_circular_button_outline;
            this.btClearFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btClearFilter.Location = new System.Drawing.Point(83, 348);
            this.btClearFilter.Name = "btClearFilter";
            this.btClearFilter.Size = new System.Drawing.Size(33, 33);
            this.btClearFilter.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btClearFilter, "Borrar Filtros");
            this.btClearFilter.UseVisualStyleBackColor = true;
            this.btClearFilter.Click += new System.EventHandler(this.btClearFilter_Click);
            // 
            // tbFilterDesc
            // 
            this.tbFilterDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilterDesc.Location = new System.Drawing.Point(77, 82);
            this.tbFilterDesc.Name = "tbFilterDesc";
            this.tbFilterDesc.Size = new System.Drawing.Size(120, 26);
            this.tbFilterDesc.TabIndex = 10;
            this.tbFilterDesc.TextChanged += new System.EventHandler(this.tbFilterDesc_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Código/Desc.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cboxColecciones
            // 
            this.cboxColecciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxColecciones.FormattingEnabled = true;
            this.cboxColecciones.Location = new System.Drawing.Point(77, 193);
            this.cboxColecciones.Name = "cboxColecciones";
            this.cboxColecciones.Size = new System.Drawing.Size(121, 28);
            this.cboxColecciones.TabIndex = 8;
            this.cboxColecciones.SelectedIndexChanged += new System.EventHandler(this.cboxColecciones_SelectedIndexChanged);
            // 
            // cboxMarcas
            // 
            this.cboxMarcas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxMarcas.FormattingEnabled = true;
            this.cboxMarcas.Location = new System.Drawing.Point(76, 134);
            this.cboxMarcas.Name = "cboxMarcas";
            this.cboxMarcas.Size = new System.Drawing.Size(121, 28);
            this.cboxMarcas.TabIndex = 7;
            this.cboxMarcas.SelectedIndexChanged += new System.EventHandler(this.cboxMarcas_SelectedIndexChanged);
            // 
            // btLoadAdvance
            // 
            this.btLoadAdvance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btLoadAdvance.Location = new System.Drawing.Point(122, 348);
            this.btLoadAdvance.Name = "btLoadAdvance";
            this.btLoadAdvance.Size = new System.Drawing.Size(75, 33);
            this.btLoadAdvance.TabIndex = 6;
            this.btLoadAdvance.Text = "&Buscar";
            this.btLoadAdvance.UseVisualStyleBackColor = true;
            this.btLoadAdvance.Click += new System.EventHandler(this.btLoadAdvance_Click);
            // 
            // lblColeccion
            // 
            this.lblColeccion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColeccion.AutoSize = true;
            this.lblColeccion.Location = new System.Drawing.Point(119, 170);
            this.lblColeccion.Name = "lblColeccion";
            this.lblColeccion.Size = new System.Drawing.Size(78, 20);
            this.lblColeccion.TabIndex = 5;
            this.lblColeccion.Text = "Colección";
            this.lblColeccion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblColeccion.Click += new System.EventHandler(this.lblColeccion_Click);
            // 
            // lblMarca
            // 
            this.lblMarca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMarca.AutoSize = true;
            this.lblMarca.Location = new System.Drawing.Point(144, 111);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(53, 20);
            this.lblMarca.TabIndex = 4;
            this.lblMarca.Text = "Marca";
            this.lblMarca.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblMarca.Click += new System.EventHandler(this.lblMarca_Click);
            // 
            // btHide
            // 
            this.btHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btHide.BackgroundImage = global::klsync.Properties.Resources.hide;
            this.btHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btHide.Location = new System.Drawing.Point(170, 3);
            this.btHide.Name = "btHide";
            this.btHide.Size = new System.Drawing.Size(27, 28);
            this.btHide.TabIndex = 1;
            this.btHide.UseVisualStyleBackColor = true;
            this.btHide.Click += new System.EventHandler(this.btHide_Click);
            // 
            // btShow
            // 
            this.btShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btShow.BackgroundImage = global::klsync.Properties.Resources.magnifying_glass;
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btShow.Location = new System.Drawing.Point(203, 3);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(26, 28);
            this.btShow.TabIndex = 0;
            this.btShow.UseVisualStyleBackColor = true;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // dgvProductosA3
            // 
            this.dgvProductosA3.AllowUserToAddRows = false;
            this.dgvProductosA3.AllowUserToDeleteRows = false;
            this.dgvProductosA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductosA3.ContextMenuStrip = this.contextMenuProd;
            this.dgvProductosA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProductosA3.Location = new System.Drawing.Point(0, 0);
            this.dgvProductosA3.Name = "dgvProductosA3";
            this.dgvProductosA3.ReadOnly = true;
            this.dgvProductosA3.RowHeadersWidth = 20;
            this.dgvProductosA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductosA3.Size = new System.Drawing.Size(987, 589);
            this.dgvProductosA3.TabIndex = 1;
            this.dgvProductosA3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductosA3_CellContentClick);
            this.dgvProductosA3.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvProductosA3_CellFormatting);
            // 
            // tabPStoA3
            // 
            this.tabPStoA3.Controls.Add(this.toolStrip3);
            this.tabPStoA3.Controls.Add(this.dgvProductosPS);
            this.tabPStoA3.Location = new System.Drawing.Point(4, 29);
            this.tabPStoA3.Name = "tabPStoA3";
            this.tabPStoA3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPStoA3.Size = new System.Drawing.Size(1227, 595);
            this.tabPStoA3.TabIndex = 1;
            this.tabPStoA3.Text = "PS a A3";
            this.tabPStoA3.UseVisualStyleBackColor = true;
            // 
            // toolStrip3
            // 
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btLoadProductosPS,
            this.toolStripSeparator15,
            this.toolStripButton10,
            this.toolStripButton1,
            this.btCheckIdA3,
            this.btAsignarIdA3ERP,
            this.ProgressBar1,
            this.toolStripLabel4,
            this.cbox_ActualizarEstado,
            this.toolStripButtonLockUnlock,
            this.toolStripButton7,
            this.toolstripButtonA3});
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(1221, 59);
            this.toolStrip3.TabIndex = 1;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btLoadProductosPS
            // 
            this.btLoadProductosPS.Image = ((System.Drawing.Image)(resources.GetObject("btLoadProductosPS.Image")));
            this.btLoadProductosPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btLoadProductosPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btLoadProductosPS.Name = "btLoadProductosPS";
            this.btLoadProductosPS.Size = new System.Drawing.Size(74, 56);
            this.btLoadProductosPS.Text = "Artículos PS";
            this.btLoadProductosPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btLoadProductosPS.Click += new System.EventHandler(this.btLoadProductosPS_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(94, 56);
            this.toolStripButton10.Text = "Sin ID Asignado";
            this.toolStripButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.toolStripButton1.Size = new System.Drawing.Size(90, 56);
            this.toolStripButton1.Text = "Duplicados";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Visible = false;
            // 
            // btCheckIdA3
            // 
            this.btCheckIdA3.Image = ((System.Drawing.Image)(resources.GetObject("btCheckIdA3.Image")));
            this.btCheckIdA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btCheckIdA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCheckIdA3.Name = "btCheckIdA3";
            this.btCheckIdA3.Size = new System.Drawing.Size(95, 56);
            this.btCheckIdA3.Text = "Check Ids en A3";
            this.btCheckIdA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCheckIdA3.Visible = false;
            // 
            // btAsignarIdA3ERP
            // 
            this.btAsignarIdA3ERP.Image = ((System.Drawing.Image)(resources.GetObject("btAsignarIdA3ERP.Image")));
            this.btAsignarIdA3ERP.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btAsignarIdA3ERP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAsignarIdA3ERP.Name = "btAsignarIdA3ERP";
            this.btAsignarIdA3ERP.Size = new System.Drawing.Size(97, 56);
            this.btAsignarIdA3ERP.Text = "Asignar Id en A3";
            this.btAsignarIdA3ERP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btAsignarIdA3ERP.Visible = false;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.ProgressBar1.Size = new System.Drawing.Size(120, 56);
            this.ProgressBar1.Visible = false;
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolStripLabel4.Size = new System.Drawing.Size(67, 56);
            this.toolStripLabel4.Text = "Acción:";
            this.toolStripLabel4.Visible = false;
            // 
            // cbox_ActualizarEstado
            // 
            this.cbox_ActualizarEstado.Items.AddRange(new object[] {
            "Borrar",
            "Actualizar Estado"});
            this.cbox_ActualizarEstado.Name = "cbox_ActualizarEstado";
            this.cbox_ActualizarEstado.Size = new System.Drawing.Size(121, 59);
            this.cbox_ActualizarEstado.Visible = false;
            // 
            // toolStripButtonLockUnlock
            // 
            this.toolStripButtonLockUnlock.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLockUnlock.Image")));
            this.toolStripButtonLockUnlock.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonLockUnlock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLockUnlock.Name = "toolStripButtonLockUnlock";
            this.toolStripButtonLockUnlock.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.toolStripButtonLockUnlock.Size = new System.Drawing.Size(126, 56);
            this.toolStripButtonLockUnlock.Text = "Actualizar Estados";
            this.toolStripButtonLockUnlock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonLockUnlock.Visible = false;
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(119, 56);
            this.toolStripButton7.Text = "Reasignar categorías";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.ToolTipText = "Reasignar categorías";
            this.toolStripButton7.Visible = false;
            // 
            // toolstripButtonA3
            // 
            this.toolstripButtonA3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolstripButtonA3.CheckOnClick = true;
            this.toolstripButtonA3.Image = ((System.Drawing.Image)(resources.GetObject("toolstripButtonA3.Image")));
            this.toolstripButtonA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolstripButtonA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolstripButtonA3.Name = "toolstripButtonA3";
            this.toolstripButtonA3.Padding = new System.Windows.Forms.Padding(10);
            this.toolstripButtonA3.Size = new System.Drawing.Size(56, 56);
            this.toolstripButtonA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolstripButtonA3.ToolTipText = "Este filtro muestra todos los productos que no están en A3";
            // 
            // dgvProductosPS
            // 
            this.dgvProductosPS.AllowUserToAddRows = false;
            this.dgvProductosPS.AllowUserToDeleteRows = false;
            this.dgvProductosPS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProductosPS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProductosPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductosPS.ContextMenuStrip = this.contextPSaA3;
            this.dgvProductosPS.Location = new System.Drawing.Point(3, 60);
            this.dgvProductosPS.Name = "dgvProductosPS";
            this.dgvProductosPS.ReadOnly = true;
            this.dgvProductosPS.Size = new System.Drawing.Size(1127, 427);
            this.dgvProductosPS.TabIndex = 0;
            // 
            // tabCat
            // 
            this.tabCat.Controls.Add(this.splitContainer1);
            this.tabCat.Location = new System.Drawing.Point(4, 29);
            this.tabCat.Name = "tabCat";
            this.tabCat.Size = new System.Drawing.Size(1227, 595);
            this.tabCat.TabIndex = 2;
            this.tabCat.Text = "Asignación Categorías";
            this.tabCat.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1227, 595);
            this.splitContainer1.SplitterDistance = 368;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.treeCat1);
            this.panel3.Controls.Add(this.toolStrip2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(368, 595);
            this.panel3.TabIndex = 5;
            // 
            // treeCat1
            // 
            this.treeCat1.AllowDrop = true;
            this.treeCat1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeCat1.CheckBoxes = true;
            this.treeCat1.ContextMenuStrip = this.cmsNewCat;
            this.treeCat1.Location = new System.Drawing.Point(8, 0);
            this.treeCat1.Name = "treeCat1";
            this.treeCat1.Size = new System.Drawing.Size(365, 555);
            this.treeCat1.TabIndex = 0;
            this.treeCat1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeCat1_NodeMouseClick);
            this.treeCat1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeCat1_NodeMouseDoubleClick);
            // 
            // cmsNewCat
            // 
            this.cmsNewCat.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsNewCat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cATEGORIASToolStripMenuItem,
            this.toolStripMenuItem9,
            this.toolStripMenuItem13,
            this.irACategoriaToolStripMenuItem,
            this.addCatToolStripMenuItem,
            this.renombrarCategoriaToolStripMenuItem,
            this.activarCategoriaToolStripMenuItem,
            this.desactivarCategoriaToolStripMenuItem,
            this.asignarACategoriaPadreToolStripMenuItem,
            this.eliminarCategoriaToolStripMenuItem,
            this.toolStripMenuItem12,
            this.regenerarPosicionesCategoriasToolStripMenuItem,
            this.toolStripMenuItem11,
            this.asignarImagenCategoriaToolStripMenuItem,
            this.toolStripSeparator5,
            this.expandirToolStripMenuItem,
            this.comprimirToolStripMenuItem});
            this.cmsNewCat.Name = "cmsNewCat";
            this.cmsNewCat.Size = new System.Drawing.Size(310, 346);
            // 
            // cATEGORIASToolStripMenuItem
            // 
            this.cATEGORIASToolStripMenuItem.Enabled = false;
            this.cATEGORIASToolStripMenuItem.Name = "cATEGORIASToolStripMenuItem";
            this.cATEGORIASToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.cATEGORIASToolStripMenuItem.Text = "CATEGORIAS";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(306, 6);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(306, 6);
            // 
            // irACategoriaToolStripMenuItem
            // 
            this.irACategoriaToolStripMenuItem.Name = "irACategoriaToolStripMenuItem";
            this.irACategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.irACategoriaToolStripMenuItem.Text = "Ir a Categoría";
            // 
            // addCatToolStripMenuItem
            // 
            this.addCatToolStripMenuItem.Name = "addCatToolStripMenuItem";
            this.addCatToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.addCatToolStripMenuItem.Text = "Añadir Categoría";
            // 
            // renombrarCategoriaToolStripMenuItem
            // 
            this.renombrarCategoriaToolStripMenuItem.Name = "renombrarCategoriaToolStripMenuItem";
            this.renombrarCategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.renombrarCategoriaToolStripMenuItem.Text = "Renombrar Categoría";
            this.renombrarCategoriaToolStripMenuItem.Click += new System.EventHandler(this.renombrarCategoriaToolStripMenuItem_Click);
            // 
            // activarCategoriaToolStripMenuItem
            // 
            this.activarCategoriaToolStripMenuItem.Name = "activarCategoriaToolStripMenuItem";
            this.activarCategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.activarCategoriaToolStripMenuItem.Text = "Activar Categoría";
            // 
            // desactivarCategoriaToolStripMenuItem
            // 
            this.desactivarCategoriaToolStripMenuItem.Name = "desactivarCategoriaToolStripMenuItem";
            this.desactivarCategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.desactivarCategoriaToolStripMenuItem.Text = "Desactivar Categoría";
            // 
            // asignarACategoriaPadreToolStripMenuItem
            // 
            this.asignarACategoriaPadreToolStripMenuItem.Name = "asignarACategoriaPadreToolStripMenuItem";
            this.asignarACategoriaPadreToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.asignarACategoriaPadreToolStripMenuItem.Text = "Asignar a categoria padre";
            // 
            // eliminarCategoriaToolStripMenuItem
            // 
            this.eliminarCategoriaToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.eliminarCategoriaToolStripMenuItem.Name = "eliminarCategoriaToolStripMenuItem";
            this.eliminarCategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.eliminarCategoriaToolStripMenuItem.Text = "Eliminar Categoria";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(306, 6);
            // 
            // regenerarPosicionesCategoriasToolStripMenuItem
            // 
            this.regenerarPosicionesCategoriasToolStripMenuItem.Name = "regenerarPosicionesCategoriasToolStripMenuItem";
            this.regenerarPosicionesCategoriasToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.regenerarPosicionesCategoriasToolStripMenuItem.Text = "Regenerar posiciones categorias";
            this.regenerarPosicionesCategoriasToolStripMenuItem.Click += new System.EventHandler(this.regenerarPosicionesCategoriasToolStripMenuItem_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(306, 6);
            // 
            // asignarImagenCategoriaToolStripMenuItem
            // 
            this.asignarImagenCategoriaToolStripMenuItem.Name = "asignarImagenCategoriaToolStripMenuItem";
            this.asignarImagenCategoriaToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.asignarImagenCategoriaToolStripMenuItem.Text = "Asignar imagen categoria default";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(306, 6);
            // 
            // expandirToolStripMenuItem
            // 
            this.expandirToolStripMenuItem.Name = "expandirToolStripMenuItem";
            this.expandirToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.expandirToolStripMenuItem.Text = "Expandir";
            // 
            // comprimirToolStripMenuItem
            // 
            this.comprimirToolStripMenuItem.Name = "comprimirToolStripMenuItem";
            this.comprimirToolStripMenuItem.Size = new System.Drawing.Size(309, 26);
            this.comprimirToolStripMenuItem.Text = "Comprimir";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel3,
            this.toolStripTextBox1,
            this.toolStripSeparator13,
            this.comboSusAdd,
            this.toolStripSeparator14,
            this.toolStripBtnReplicarIMG});
            this.toolStrip2.Location = new System.Drawing.Point(0, 564);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(368, 31);
            this.toolStrip2.TabIndex = 24;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolStripLabel3.Size = new System.Drawing.Size(57, 28);
            this.toolStripLabel3.Text = "Filtrar";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 31);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 31);
            // 
            // comboSusAdd
            // 
            this.comboSusAdd.Items.AddRange(new object[] {
            "Sustituir",
            "Añadir"});
            this.comboSusAdd.Name = "comboSusAdd";
            this.comboSusAdd.Size = new System.Drawing.Size(121, 31);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripBtnReplicarIMG
            // 
            this.toolStripBtnReplicarIMG.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnReplicarIMG.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnReplicarIMG.Image")));
            this.toolStripBtnReplicarIMG.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripBtnReplicarIMG.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnReplicarIMG.Name = "toolStripBtnReplicarIMG";
            this.toolStripBtnReplicarIMG.Size = new System.Drawing.Size(28, 28);
            this.toolStripBtnReplicarIMG.Text = "Replicar Imágenes";
            this.toolStripBtnReplicarIMG.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripBtnReplicarIMG.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabAsign);
            this.tabControl1.Controls.Add(this.tabCreation);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(849, 588);
            this.tabControl1.TabIndex = 1;
            // 
            // tabAsign
            // 
            this.tabAsign.Controls.Add(this.dgvCat);
            this.tabAsign.Location = new System.Drawing.Point(4, 29);
            this.tabAsign.Name = "tabAsign";
            this.tabAsign.Padding = new System.Windows.Forms.Padding(3);
            this.tabAsign.Size = new System.Drawing.Size(841, 555);
            this.tabAsign.TabIndex = 0;
            this.tabAsign.Text = "Asignación";
            this.tabAsign.UseVisualStyleBackColor = true;
            // 
            // dgvCat
            // 
            this.dgvCat.AllowDrop = true;
            this.dgvCat.AllowUserToAddRows = false;
            this.dgvCat.AllowUserToDeleteRows = false;
            this.dgvCat.AllowUserToResizeRows = false;
            this.dgvCat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCat.Location = new System.Drawing.Point(3, 3);
            this.dgvCat.Name = "dgvCat";
            this.dgvCat.ReadOnly = true;
            this.dgvCat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCat.Size = new System.Drawing.Size(835, 549);
            this.dgvCat.TabIndex = 0;
            // 
            // tabCreation
            // 
            this.tabCreation.Controls.Add(this.btAddCategories);
            this.tabCreation.Controls.Add(this.dgvNewCategories);
            this.tabCreation.Location = new System.Drawing.Point(4, 29);
            this.tabCreation.Name = "tabCreation";
            this.tabCreation.Padding = new System.Windows.Forms.Padding(3);
            this.tabCreation.Size = new System.Drawing.Size(841, 555);
            this.tabCreation.TabIndex = 1;
            this.tabCreation.Text = "Utilidades";
            this.tabCreation.UseVisualStyleBackColor = true;
            // 
            // btAddCategories
            // 
            this.btAddCategories.Location = new System.Drawing.Point(6, 348);
            this.btAddCategories.Name = "btAddCategories";
            this.btAddCategories.Size = new System.Drawing.Size(167, 45);
            this.btAddCategories.TabIndex = 1;
            this.btAddCategories.Text = "<<<Crear Categorias";
            this.btAddCategories.UseVisualStyleBackColor = true;
            this.btAddCategories.Click += new System.EventHandler(this.btAddCategories_Click);
            // 
            // dgvNewCategories
            // 
            this.dgvNewCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewCategories.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Valor});
            this.dgvNewCategories.Location = new System.Drawing.Point(6, 6);
            this.dgvNewCategories.Name = "dgvNewCategories";
            this.dgvNewCategories.Size = new System.Drawing.Size(240, 336);
            this.dgvNewCategories.TabIndex = 0;
            // 
            // Valor
            // 
            this.Valor.HeaderText = "Valor";
            this.Valor.Name = "Valor";
            // 
            // treeCat
            // 
            this.treeCat.AllowDrop = true;
            this.treeCat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeCat.LineColor = System.Drawing.Color.Empty;
            this.treeCat.Location = new System.Drawing.Point(3, 3);
            this.treeCat.Name = "treeCat";
            this.treeCat.Size = new System.Drawing.Size(362, 532);
            this.treeCat.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripSeparator8,
            this.toolStripButton9,
            this.toolStripSeparator20,
            this.toolStripButton8,
            this.toolStripSeparator12,
            this.toolStripButton5,
            this.toolStripSeparator10,
            this.toolStripSeparator11,
            this.btnSendProductToPS,
            this.toolStripSeparator2,
            this.toolStripButton2,
            this.toolStripSeparator1,
            this.btSincronizarStockNoTallas,
            this.toolStripSeparator4,
            this.btSincronizarTallasColores,
            this.btUpdate,
            this.toolStripSeparator6,
            this.toolStripSeparator3,
            this.toolStripButton6});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1235, 59);
            this.toolStrip1.TabIndex = 21;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(51, 56);
            this.toolStripButton4.Text = "Nuevos";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.ToolTipText = "Productos Nuevos";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(76, 56);
            this.toolStripButton9.Text = " Bloqueados";
            this.toolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton9.ToolTipText = "Productos Bloqueados";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(79, 56);
            this.toolStripButton8.Text = "Todos A3ERP";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.ToolTipText = "Todos los Productos de A3";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(62, 56);
            this.toolStripButton5.Text = "En Tienda";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.ToolTipText = "Productos subidos a PS";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 59);
            // 
            // btnSendProductToPS
            // 
            this.btnSendProductToPS.Image = ((System.Drawing.Image)(resources.GetObject("btnSendProductToPS.Image")));
            this.btnSendProductToPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSendProductToPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSendProductToPS.Name = "btnSendProductToPS";
            this.btnSendProductToPS.Size = new System.Drawing.Size(78, 56);
            this.btnSendProductToPS.Text = "Artículo a PS";
            this.btnSendProductToPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSendProductToPS.ToolTipText = "Exportar artículos seleccionados a Prestashop";
            this.btnSendProductToPS.Click += new System.EventHandler(this.btnSendProductToPS_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(87, 56);
            this.toolStripButton2.Text = "Imagenes a PS";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.ToolTipText = "Subir imágenes a PS";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 59);
            // 
            // btSincronizarStockNoTallas
            // 
            this.btSincronizarStockNoTallas.Image = ((System.Drawing.Image)(resources.GetObject("btSincronizarStockNoTallas.Image")));
            this.btSincronizarStockNoTallas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btSincronizarStockNoTallas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSincronizarStockNoTallas.Name = "btSincronizarStockNoTallas";
            this.btSincronizarStockNoTallas.Size = new System.Drawing.Size(95, 56);
            this.btSincronizarStockNoTallas.Text = "Actualizar Stock";
            this.btSincronizarStockNoTallas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btSincronizarStockNoTallas.Click += new System.EventHandler(this.btSincronizarStockNoTallas_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 59);
            // 
            // btSincronizarTallasColores
            // 
            this.btSincronizarTallasColores.Image = ((System.Drawing.Image)(resources.GetObject("btSincronizarTallasColores.Image")));
            this.btSincronizarTallasColores.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btSincronizarTallasColores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSincronizarTallasColores.Name = "btSincronizarTallasColores";
            this.btSincronizarTallasColores.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.btSincronizarTallasColores.Size = new System.Drawing.Size(76, 56);
            this.btSincronizarTallasColores.Text = "TyC";
            this.btSincronizarTallasColores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btSincronizarTallasColores.ToolTipText = "Actualizar Tallas y Colores\r\n";
            this.btSincronizarTallasColores.Click += new System.EventHandler(this.btSincronizarTallasColores_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btUpdate.Image")));
            this.btUpdate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(63, 56);
            this.btUpdate.Text = "Actualizar";
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 59);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Padding = new System.Windows.Forms.Padding(0, 0, 20, 20);
            this.toolStripButton6.Size = new System.Drawing.Size(56, 56);
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.ToolTipText = "CTRL + F (Abrir diálogo de búsqueda)";
            this.toolStripButton6.Visible = false;
            // 
            // tssLb
            // 
            this.tssLb.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tssLb.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tssLb.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelBusquedaCtrlFArticulos,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabelProgreso,
            this.tsslbInfo,
            this.tssSeparator,
            this.tssInfoTime,
            this.toolStripProgressBar});
            this.tssLb.Location = new System.Drawing.Point(0, 685);
            this.tssLb.Name = "tssLb";
            this.tssLb.Size = new System.Drawing.Size(1235, 30);
            this.tssLb.TabIndex = 22;
            this.tssLb.Text = "statusStrip1";
            // 
            // toolStripStatusLabelBusquedaCtrlFArticulos
            // 
            this.toolStripStatusLabelBusquedaCtrlFArticulos.BackColor = System.Drawing.Color.Silver;
            this.toolStripStatusLabelBusquedaCtrlFArticulos.Name = "toolStripStatusLabelBusquedaCtrlFArticulos";
            this.toolStripStatusLabelBusquedaCtrlFArticulos.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripStatusLabelBusquedaCtrlFArticulos.Size = new System.Drawing.Size(152, 25);
            this.toolStripStatusLabelBusquedaCtrlFArticulos.Text = "Búsqueda: CTRL + F";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(758, 25);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // toolStripStatusLabelProgreso
            // 
            this.toolStripStatusLabelProgreso.Name = "toolStripStatusLabelProgreso";
            this.toolStripStatusLabelProgreso.Size = new System.Drawing.Size(83, 25);
            this.toolStripStatusLabelProgreso.Text = "Progreso -";
            // 
            // tsslbInfo
            // 
            this.tsslbInfo.Name = "tsslbInfo";
            this.tsslbInfo.Size = new System.Drawing.Size(51, 25);
            this.tsslbInfo.Text = "0 filas";
            // 
            // tssSeparator
            // 
            this.tssSeparator.Name = "tssSeparator";
            this.tssSeparator.Size = new System.Drawing.Size(14, 25);
            this.tssSeparator.Text = "|";
            // 
            // tssInfoTime
            // 
            this.tssInfoTime.Name = "tssInfoTime";
            this.tssInfoTime.Size = new System.Drawing.Size(60, 25);
            this.tssInfoTime.Text = "00:00 s";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 24);
            // 
            // frArticulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1235, 715);
            this.Controls.Add(this.tssLb);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabAdvanced);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frArticulos";
            this.Text = "Productos";
            this.Load += new System.EventHandler(this.frArticulos_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frArticulos_KeyPress);
            this.contextMenuProd.ResumeLayout(false);
            this.contextPSaA3.ResumeLayout(false);
            this.tabAdvanced.ResumeLayout(false);
            this.tabA3toPS.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosA3)).EndInit();
            this.tabPStoA3.ResumeLayout(false);
            this.tabPStoA3.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductosPS)).EndInit();
            this.tabCat.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.cmsNewCat.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabAsign.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCat)).EndInit();
            this.tabCreation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewCategories)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tssLb.ResumeLayout(false);
            this.tssLb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuProd;
        private System.Windows.Forms.ToolStripMenuItem activarEnTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desactivarEnTiendaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextPSaA3;
        private System.Windows.Forms.ToolStripMenuItem bajarProductosAA3ToolStripMenuItem;
        private System.Windows.Forms.TabControl tabAdvanced;
        private System.Windows.Forms.TabPage tabA3toPS;
        private System.Windows.Forms.TabPage tabPStoA3;
        private System.Windows.Forms.TabPage tabCat;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvCat;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnSendProductToPS;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.StatusStrip tssLb;
        private System.Windows.Forms.ToolStripStatusLabel tsslbInfo;
        private System.Windows.Forms.ToolStripButton btSincronizarStockNoTallas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton btUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ContextMenuStrip cmsNewCat;
        private System.Windows.Forms.TreeView treeCat1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripComboBox comboSusAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton toolStripBtnReplicarIMG;
        private System.Windows.Forms.TreeView treeCat;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btCheckIdA3;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar1;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton btAsignarIdA3ERP;
        private System.Windows.Forms.ToolStripButton btLoadProductosPS;
        private System.Windows.Forms.ToolStripButton toolStripButtonLockUnlock;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripComboBox cbox_ActualizarEstado;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem eliminarProductosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarIDsDePSAA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ponerANullLosProductosSELECCIONADOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tssInfoTime;
        private System.Windows.Forms.ToolStripStatusLabel tssSeparator;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem ponerVisibleEnTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ponerInvisibleEnTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelBusquedaCtrlFArticulos;
        private System.Windows.Forms.ToolStripMenuItem aCCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarPreciosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem aCTUALIZARToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.DataGridView dgvProductosPS;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProgreso;
        private System.Windows.Forms.ToolStripMenuItem sincFamiliasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarImagenCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolstripButtonA3;
        private System.Windows.Forms.ToolStripMenuItem actualizarEstadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripMenuItem subirArticuloAPrestashopToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem artículosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarIDEnA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imágenesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subirImagenesAPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem replicarImagenesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verificarArticulosEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertarDescripcionesA3ToolStripMenuItem1;

        private System.Windows.Forms.ToolStripMenuItem sincronizarAtributosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artículoSeleccionadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todosToolStripMenuItem;

        private System.Windows.Forms.ToolStripButton btSincronizarTallasColores;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem sincronizarDescripcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem eliminarProductoPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem resetearAtributosSelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regenerarPosicionesCategoriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem addCatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renombrarCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem cATEGORIASToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem irACategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desactivarCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activarCategoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarArticulosConStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarArticulosMalCodificadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarProductosPorImagenesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asignarACategoriaPadreToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button btClearFilter;
        private System.Windows.Forms.TextBox tbFilterDesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxColecciones;
        private System.Windows.Forms.ComboBox cboxMarcas;
        private System.Windows.Forms.Button btLoadAdvance;
        private System.Windows.Forms.Label lblColeccion;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.Button btHide;
        private System.Windows.Forms.Button btShow;
        private System.Windows.Forms.DataGridView dgvProductosA3;
        private System.Windows.Forms.CheckBox cboxDisponible;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAsign;
        private System.Windows.Forms.TabPage tabCreation;
        private System.Windows.Forms.DataGridView dgvNewCategories;
        private System.Windows.Forms.Button btAddCategories;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem expandirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprimirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem actualizarArtículosEnPSToolStripMenuItem;
        private System.Windows.Forms.CheckBox cBoxEnTienda;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem validarPresenciaEnLaTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem generarFicheroLogistica;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem actualizarImpuestosEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarFechaDeDisponibilidadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validarPresenciaEnTiendaConReferenciaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarMedidasEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem;
    }
}