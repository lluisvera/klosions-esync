﻿namespace klsync
{
    partial class frAtributos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCopiarAtributosToA3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCopiarAtributosToA3
            // 
            this.btnCopiarAtributosToA3.Location = new System.Drawing.Point(31, 28);
            this.btnCopiarAtributosToA3.Name = "btnCopiarAtributosToA3";
            this.btnCopiarAtributosToA3.Size = new System.Drawing.Size(200, 48);
            this.btnCopiarAtributosToA3.TabIndex = 0;
            this.btnCopiarAtributosToA3.Text = "Copiar Atributos PS -> A3";
            this.btnCopiarAtributosToA3.UseVisualStyleBackColor = true;
            this.btnCopiarAtributosToA3.Click += new System.EventHandler(this.btnCopiarAtributosToA3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 103);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 48);
            this.button1.TabIndex = 1;
            this.button1.Text = "Copiar Atributos PS -> A3";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(31, 174);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 48);
            this.button2.TabIndex = 2;
            this.button2.Text = "Copiar Atributos A3 -> PS";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(31, 246);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 48);
            this.button3.TabIndex = 3;
            this.button3.Text = "Copiar Atributos A3 -> PS";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // frAtributos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 596);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCopiarAtributosToA3);
            this.Name = "frAtributos";
            this.Text = "frAtributos";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCopiarAtributosToA3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}