﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a3ERPActiveX;
using System.Data;
using System.Windows.Forms;

namespace klsync
{
    class csa3erpTercero
    {
        // vv vv vv SAGRADO vv vv vv
        public string crearClienteA3(string nombre, string referencia)
        {
            a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
            maestro.Iniciar("clientes");
            maestro.Nuevo();
            maestro.set_AsString("CODCLI", maestro.NuevoCodigoNum());
            maestro.set_AsString("NOMCLI", nombre);
            maestro.set_AsString("KLS_CODCLIENTE", referencia);
            maestro.set_AsString("TIPOMANDATO", "CORE (B2C)");
            maestro.set_AsString("IF_EXPORTAR_WEB", "T");
            string codcli = maestro.get_AsString("CODCLI");
            maestro.Guarda(true);
            maestro.Acabar();

            return codcli.Trim();
        }

        // ^^ ^^ ^^ SAGRADO ^^ ^^ ^^ 
        public string crearClienteObjetoA3(Objetos.csTercero cliente)
        {
            string codcli = "";
            try
            {
                string idDirA3 = "";
                string idDirPS = "";

                csSqlConnects sql = new csSqlConnects();


                Maestro maestroCli = new Maestro();
                maestroCli.Iniciar("clientes");
                maestroCli.Nuevo();

                maestroCli.set_AsString("CODCLI", maestroCli.NuevoCodigoNum());
                maestroCli.set_AsString("NOMCLI", cliente.nomIC);
                maestroCli.set_AsString("KLS_NOM_USUARIO", cliente.nombre);
                maestroCli.set_AsString("KLS_APELL_USUARIO", cliente.apellidos);
                maestroCli.set_AsString("KLS_CODCLIENTE", cliente.id_customer_Ext);
                maestroCli.set_AsString("DIRCLI", cliente.direccion);
                maestroCli.set_AsString("NIFCLI", cliente.nifcif);
                maestroCli.set_AsString("DIRCLI1", cliente.direccion);
                maestroCli.set_AsString("DIRCLI2", cliente.direccion2);
                maestroCli.set_AsString("E_MAIL_DOCS", cliente.email);
                maestroCli.set_AsString("E_MAIL", cliente.email);
                maestroCli.set_AsString("KLS_EMAIL_USUARIO", cliente.email);
                maestroCli.set_AsString("POBCLI", cliente.poblacion);
                maestroCli.set_AsString("DTOCLI", cliente.codigoPostal);
                maestroCli.set_AsString("KLS_PASS_INICIAL", "SECRETO");
                maestroCli.set_AsString("TELCLI", cliente.telf);
                maestroCli.set_AsString("TELCLI2", cliente.movil);
                maestroCli.set_AsString("CODPROVI", cliente.codprovincia);
                maestroCli.set_AsString("REGIVA", cliente.regimenIva);
                maestroCli.set_AsString("CODPAIS", cliente.pais);
                maestroCli.set_AsString("PAISFISCAL", cliente.pais);


                if (cliente.pais.Equals("ES"))
                {
                    if (cliente.provincia.ToString() != null)
                    {
                        string codprovi = provinciaFiscal(cliente.provincia.ToString());

                        maestroCli.set_AsString("PROVIFISCAL", "");
                        maestroCli.set_AsString("CODPROVI", "");
                    }
                }
                else
                {
                    maestroCli.set_AsString("PROVIFISCAL", "");
                    maestroCli.set_AsString("CODPROVI", "");

                }

                if (cliente.descFormaPago == "")
                    maestroCli.set_AsString("DOCPAG", "TR");

                if (cliente.codPais == "" || cliente.codPais == null)
                {
                    if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                        maestroCli.set_AsString("CODPAIS", "ES");
                    else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                        maestroCli.set_AsString("CODPAIS", "FR");

                }
                else
                {
                    maestroCli.set_AsString("CODPAIS", cliente.codPais);
                }
                //!cliente.famCliEsp.ToString().Equals("") &&
                if (csGlobal.conexionDB.Contains("MIMASA") && !cliente.famCliEsp.ToString().Equals("") && cliente.famCliEsp != null)
                {
                    maestroCli.set_AsString("FAMCLIESP", cliente.famCliEsp);
                    maestroCli.set_AsString("CAR2", cliente.carDos);
                }


                if (csGlobal.ivaIncluido.ToUpper() == "SI" && csGlobal.usarClienteGenerico.ToUpper() == "NO")
                {
                    maestroCli.set_AsString("IVAINCLUIDO", "T");
                }

                codcli = maestroCli.get_AsString("CODCLI");
                maestroCli.Guarda(true);
                maestroCli.Acabar();
                codcli = codcli.Trim();

                //Asigno el Id de la dirección en Prestashop y en A3
                if (csGlobal.modeAp.ToUpper() == "ESYNC")
                {
                    actualidarIdDirecciones(codcli, cliente.id_customer_Ext, cliente.idDireccionPS);
                }

                return codcli;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public void actualidarIdDirecciones(string codCliA3, string codCliPS, string idDireccionPS)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mySql = new csMySqlConnect();
            string idDireccionA3 = sql.obtenerCampoTabla("SELECT CAST(IDDIRENT AS INT) AS IDDIR FROM DIRENT WHERE LTRIM(CODCLI)='" + codCliA3 + "'");
            sql.actualizarCampo("UPDATE DIRENT SET ID_PS=" + idDireccionPS + " WHERE LTRIM(CODCLI)='" + codCliA3 + "'", true);

            mySql.ejecutarConsulta("update ps_customer set kls_a3erp_id='" + codCliA3 + "' where id_customer=" + codCliPS);
            mySql.ejecutarConsulta("update ps_address set kls_id_dirent='" + idDireccionA3 + "' where id_address=" + idDireccionPS);
        }


        public void borrarProveedorA3(string codPro)
        {
            codPro = codPro.Replace(" ", "");
            a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
            maestro.Iniciar("PROVEED");
            maestro.Buscar(codPro);
            //maestro.set_AsString("CODPRO", codPro);
            maestro.Borrar();
            maestro.Acabar();
        }

        public void borrarClienteA3(string codCli)
        {
            codCli = codCli.Replace(" ", "");
            a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
            maestro.Iniciar("CLIENTES");
            maestro.Buscar(codCli);
            //maestro.set_AsString("CODPRO", codPro);
            maestro.Borrar();
            maestro.Acabar();
        }

        public string crearClienteA3(Objetos.csTercero cliente, Objetos.csDomBanca domiciliacion = null)
        {
            Maestro a3maestro = new Maestro();
            string codigo = "";
            string idClientePS = "";
            csMySqlConnect mySqlConnect = new csMySqlConnect();

            try
            {
                idClientePS = cliente.codIC;
                a3maestro.Iniciar("clientes");
                a3maestro.Nuevo();

                codigo = a3maestro.NuevoCodigoNum();
                codigo = codigo.Trim();
                a3maestro.AsString["CODCLI"] = codigo.Trim();


                if (string.IsNullOrEmpty(cliente.razonSocial))
                {
                    if (!string.IsNullOrEmpty(cliente.apellidos))
                        a3maestro.AsString["NOMCLI"] = cliente.nombre.ToUpper() + " " + cliente.apellidos.ToUpper();
                    else
                        a3maestro.AsString["NOMCLI"] = cliente.nombre.ToUpper();
                }
                else
                {
                    a3maestro.AsString["NOMCLI"] = cliente.razonSocial.ToUpper();
                }



                a3maestro.set_AsString("DIRCLI", cliente.direccion);
                a3maestro.set_AsString("POBCLI", cliente.poblacion);
                a3maestro.set_AsString("DTOCLI", cliente.codigoPostal);
                a3maestro.set_AsString("TELCLI", cliente.telf);
                a3maestro.set_AsString("FAXCLI", cliente.telf);
                a3maestro.set_AsString("RAZON", cliente.razonSocial);
                a3maestro.set_AsString("E_MAIL", cliente.email);
                a3maestro.set_AsString("KLS_EMAIL_USUARIO", cliente.email);
                a3maestro.set_AsString("KLS_NOM_USUARIO", cliente.nombre);
                a3maestro.set_AsString("KLS_APELL_USUARIO", cliente.apellidos);
                a3maestro.set_AsString("KLS_CLIENTE_TIENDA", "T");
                a3maestro.set_AsString("KLS_CODCLIENTE", cliente.codIC);
                a3maestro.set_AsString("E_MAIL", cliente.email);
                a3maestro.set_AsString("PARAM1", cliente.permalinkFiscalIdentity);
                a3maestro.set_AsString("NIFCLI", cliente.nifcif);
                a3maestro.set_AsString("TIPOMANDATO", "CORE (B2C)");
                a3maestro.set_AsString("CODPAIS", cliente.pais);
                a3maestro.set_AsString("PAISFISCAL", cliente.pais);




                //Si el pais es españa añadimos la provincia según su código

                if (!String.IsNullOrEmpty(cliente.pais))
                {
                    if (cliente.pais.Equals("ES"))
                    {
                        string codprovi = provinciaFiscal(cliente.provincia.ToString());
                        if (codprovi != null)
                        {
                            a3maestro.set_AsString("PROVIFISCAL", cliente.codprovincia);
                            a3maestro.set_AsString("CODPROVI", cliente.codprovincia);
                        }
                    }
                    else
                    {
                        a3maestro.set_AsString("PROVIFISCAL", "");
                        a3maestro.set_AsString("CODPROVI", "");

                    }
                }

                if (cliente.descFormaPago == "")
                    a3maestro.set_AsString("DOCPAG", "TR");

                if (cliente.codPais == "" || cliente.codPais == null)
                {
                    if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                        a3maestro.set_AsString("CODPAIS", "ES");
                    else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                        a3maestro.set_AsString("CODPAIS", "FR");

                }
                else
                {
                    a3maestro.set_AsString("CODPAIS", cliente.codPais);
                }

                if (csGlobal.conexionDB.Contains("MIMASA") && !cliente.famCliEsp.ToString().Equals(""))
                {
                    a3maestro.set_AsString("FAMCLIESP", cliente.famCliEsp);
                    a3maestro.set_AsString("CAR2", cliente.carDos);
                }

                try
                {
                    a3maestro.Guarda(true);
                }
                catch (Exception ex)
                {

                }
                a3maestro.Acabar();

                mySqlConnect.actualizarIdClientePS(idClientePS, codigo);
            }
            finally
            {
                //a3maestro.Acabar();
                a3maestro = null;
            }


            if (domiciliacion != null)
            {
                csSqlConnects sql = new csSqlConnects();
                if (sql.obtenerCampoTabla("SELECT VERSION FROM VERSION") == "9")
                {
                    domiciliacion.codTercero = codigo.Replace(" ", "");
                    crearDomiciliacion(domiciliacion, true);
                }
            }

            return codigo;
        }
        public string provinciaFiscal(string provinciaFiscal)
        {
            int codigoProvincia = 00;
            string codProviString;
            csSqlConnects sql = new csSqlConnects();
            DataTable provinciasA3 = new DataTable();
            provinciasA3 = sql.cargarDatosTablaA3("SELECT * FROM PROVINCI");


            foreach (DataRow provincia in provinciasA3.Rows)
            {
                if (provincia["NOMPROVI"].ToString() == provinciaFiscal.ToUpper())
                    codigoProvincia = int.Parse(provincia["CODPROVI"].ToString());
            }
            if (codigoProvincia < 10)
            {
                codProviString = "0" + codigoProvincia.ToString();
            }
            else
            {
                codProviString = codigoProvincia.ToString();
            }
            return codProviString;

        }

        // Sincronizacion Clientes Repasat > A
        public void crearArrayClientesA3Objetos(Objetos.csTercero[] customers, Objetos.csDireccion[] direcciones = null)
        {
            try
            {
                string provincia = "";
                string idA3ERPDireccionPpal = "";
                string idRepasatDireccionPpal = "";
                csSqlConnects sql = new csSqlConnects();
                csRepasatWebService rpstWS = new csRepasatWebService();

                csa3erpDirecciones direcionesA3ERP = new csa3erpDirecciones();
                //Creo un datatable para guardar todos los clientes que vamos a ir creando
                DataTable dtParams = new DataTable();
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.Columns.Add("KEY");

                for (int i = 0; i < customers.Length; i++)
                {
                    string codigo = "0";
                    if (customers[i] != null)
                    {
                        Maestro a3maestro = new Maestro();
                        a3maestro.Iniciar("clientes");
                        a3maestro.Nuevo();
                        codigo = a3maestro.NuevoCodigoNum();
                        codigo = codigo.Trim();
                        a3maestro.AsString["CODCLI"] = codigo.Trim();
                        if (customers[i].apellidos.Trim() != "")
                            a3maestro.AsString["NOMCLI"] = customers[i].nombre.ToUpper() + " " + customers[i].apellidos.ToUpper();
                        else
                            a3maestro.AsString["NOMCLI"] = customers[i].nombre.ToUpper();

                        a3maestro.set_AsString("DIRCLI", customers[i].direccion);
                        a3maestro.set_AsString("POBCLI", customers[i].poblacion);
                        a3maestro.set_AsString("DTOCLI", customers[i].codigoPostal);
                        a3maestro.set_AsString("CODPROVI", customers[i].provincia);
                        a3maestro.set_AsString("TELCLI", customers[i].telf);
                        a3maestro.set_AsString("RAZON", customers[i].razonSocial);
                        a3maestro.set_AsString("RUTA", customers[i].ruta);

                        a3maestro.set_AsString("NIFCLI", customers[i].nifcif);
                        a3maestro.set_AsString("REGIVA", customers[i].regimenIva);

                        if (csGlobal.databaseA3.Contains("ADMANMEDIA"))
                        {
                            a3maestro.set_AsString("PARAM1", customers[i].codIC);
                            a3maestro.set_AsString("PARAM9", customers[i].id_customer_Ext); // codCli
                        }

                        if (customers[i].moneda == "EUR")
                            a3maestro.set_AsString("CODMON", "EURO");
                        else
                            a3maestro.set_AsString("CODMON", customers[i].moneda);

                        if (customers[i].tipoIRPF != null)
                        {
                            a3maestro.set_AsString("TIPOIRPF", customers[i].tipoIRPF);
                            a3maestro.set_AsString("PORIRPF", customers[i].porcentajeIRPF);
                            a3maestro.set_AsString("ID_CLAVEIRPF", customers[i].claveIRPF);
                            a3maestro.set_AsString("ID_SUBCLAVEIRPF", customers[i].subclaveIRPF);
                        }

                        if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                        {
                            a3maestro.set_AsString("FORPAG", "90");
                        }
                        else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                        {
                            a3maestro.set_AsString("FORPAG", "60");
                        }

                        if (customers[i].codPais == "" || customers[i].codPais == null)
                        {
                            if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                            {
                                a3maestro.set_AsString("CODPAIS", "ES");
                            }
                            else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                            {
                                a3maestro.set_AsString("CODPAIS", "FR");
                            }
                        }
                        else
                        {
                            a3maestro.set_AsString("CODPAIS", customers[i].codPais);
                        }

                        if (customers[i].descFormaPago == "paypal" || customers[i].descFormaPago == "" || customers[i].descFormaPago == "0")
                        {
                            a3maestro.set_AsString("E_MAIL", customers[i].paypal);
                            a3maestro.set_AsString("DOCPAG", "PAY");
                            a3maestro.set_AsString("PARAM2", customers[i].paypal);
                        }
                        else
                        {
                            a3maestro.set_AsString("DOCPAG", "TR");
                            a3maestro.set_AsString("E_MAIL", customers[i].email);
                        }

                        a3maestro.Guarda(true);
                        a3maestro.Acabar();
                        a3maestro = null;

                        DataRow drParams = dtParams.NewRow();
                        drParams["FIELD"] = "codExternoCli";
                        drParams["KEY"] = customers[i].idRepasat;
                        drParams["VALUE"] = codigo;
                        dtParams.Rows.Add(drParams);

                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = customers[i].idRepasat;
                        dtKeys.Rows.Add(drKeys);


                        for (int cont = 0; cont < direcciones.Length; cont++)
                        {
                            if (direcciones[cont].idClienteDireccionRepasat == customers[i].repasatID)
                            {
                                direcciones[cont].codCli = codigo;
                                if (direcciones[cont].direccionfiscal)
                                {
                                    idRepasatDireccionPpal = direcciones[cont].idDireccionRepasat;
                                    break;
                                }
                            }
                        }

                        //obtener id de la dirección principal y actualizar la dirección en Repasat
                        idA3ERPDireccionPpal = sql.obtenerCampoTabla("SELECT IDDIRENT FROM DIRENT WHERE LTRIM(CODCLI)='" + codigo + "' AND DEFECTO ='T'");
                        idA3ERPDireccionPpal = Math.Round(Convert.ToDecimal(idA3ERPDireccionPpal), 0).ToString();
                        rpstWS.actualizarDocumentoRepasat("accountaddresses", "address[codExternoDireccion]", idRepasatDireccionPpal, idA3ERPDireccionPpal);
                    }

                    direcionesA3ERP.crearDireccionRepasat(direcciones, codigo);
                }
                //Sincronización de cuentas
                rpstWS.sincronizarObjetoRepasat("accounts", "PUT", dtKeys, dtParams, "");
            }
            catch (Exception ex)
            {

            }
        }


        //Función para unificar la creación de clientes y proveedores
        //Función para crear cuentas desde Repasat
        public string crearCuentasA3Objects(Objetos.csTercero[] tercero, Objetos.csDireccion[] direcciones = null, Objetos.csDomBanca domiciliacion = null, bool clientes = true, bool forceSync = false, Objetos.csContactos[] contactos = null)
         {
            csa3erp a3erp = new csa3erp();

            if (!a3erp.estadoConexion())
            {
                a3erp.abrirEnlace();
            }


            Maestro a3maestro = new Maestro();

            int indice = 0;


            try
            {
                string codRPSTCuenta = "";
                string scriptUpdateDireccion = "";

                string tipoTercero = clientes ? "Clientes" : "proveedores";
                string provincia = "";
                string idA3ERPDireccionPpal = "";
                string idRepasatDireccionPpal = "";
                csSqlConnects sql = new csSqlConnects();
                csRepasatWebService rpstWS = new csRepasatWebService();

                //Defino todos los valores en función de si es cliente o proveedor
                string codigoTercero = clientes ? "CODCLI" : "CODPRO";
                string nombre = clientes ? "NOMCLI" : "NOMPRO";
                string direccion = clientes ? "DIRCLI" : "DIRPRO";
                string poblacion = clientes ? "POBCLI" : "POBPRO";
                string cPostal = clientes ? "DTOCLI" : "DTOPRO";
                string nif = clientes ? "NIFCLI" : "NIFPRO";
                string telef = clientes ? "TELCLI" : "TELPRO";
                string telef2 = clientes ? "TELCLI2" : "TEL2PRO";
                string fax = clientes ? "FAXCLI" : "FAXPRO";
                string rpst_id_direccion = clientes ? "RPST_ID_DIR" : "RPST_ID_DIRPROV";

                //consultas SQL
                string tablaDirecciones = clientes ? "DIRENT" : "__DIRENTPRO";
                string keyFieldtablaDirecciones = clientes ? "CODCLI" : "CODPRO";

                csa3erpDirecciones direcionesA3ERP = new csa3erpDirecciones();
                A3ERP.updateA3ERP updateA3 = new A3ERP.updateA3ERP();  //Para gestionar contactos
                //Creo un datatable para guardar todos los clientes que vamos a ir creando
                DataTable dtParams = new DataTable();
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.Columns.Add("KEY");

                a3maestro.Iniciar(tipoTercero);
                a3maestro.OmitirMensajes = true;

                for (int i = 0; i < tercero.Length; i++)
                {
                    //Reviso si la conexión está abierta, si no la abro (por si se ha cerrado la conexión)
                    if (!a3erp.estadoConexion())
                    {
                        a3erp.abrirEnlace();
                    }


                    indice = i;
                    string codigo = "";

                    //CONDICIONALES PARA COMPROBAR 
                    //REVISAR DE IMPLEMENTARLO CORRECTAMENTE
                    //COMPROBACIÓN NIF
                    //if (comprobarNif(tercero[i].nifcif, clientes))
                    //{
                    //    if (csGlobal.modoManual)
                    //    {

                    //        MessageBox.Show("Ya existe un cliente con este NIF: " + tercero[i].nifcif + "\n" + "DATOS CLIENTE: " + "\n" + "Nombre:" + tercero[i].nombre + "\n" + "ID REPASAT:" + tercero[i].idRepasat);
                    //    }
                    //    //continue;
                    //}

                    ////COMPROBACIÓN CORREO
                    //if (comprobarCorreo(tercero[i].email, clientes))
                    //{
                    //    if (csGlobal.modoManual)
                    //    {
                    //        MessageBox.Show("Ya existe un cliente con este CORREO: " + tercero[i].email + "\n" + "DATOS CLIENTE: " + "\n" + "Nombre:" + tercero[i].nombre + "\n" + "ID REPASAT:" + tercero[i].idRepasat);
                    //    }
                    //    continue;
                    //}

                    ////COMPROBACIÓN TEL1
                    //if (comprobarTelf1(tercero[i].telf, clientes))
                    //{
                    //    if (csGlobal.modoManual)
                    //    {
                    //        MessageBox.Show("Ya existe un cliente con este TELÉFONO 1: " + tercero[i].telf + "\n" + "DATOS CLIENTE: " + "\n" + "Nombre:" + tercero[i].nombre + "\n" + "ID REPASAT:" + tercero[i].idRepasat);
                    //    }
                    //    continue;
                    //}

                    ////COMPROBACIÓN TEL2
                    //if (comprobarTelf2(tercero[i].telf2, clientes))
                    //{
                    //    if (csGlobal.modoManual)
                    //    {
                    //        MessageBox.Show("Ya existe un cliente con este TELÉFONO 2: " + tercero[i].telf2 + "\n" + "DATOS CLIENTE: " + "\n" + "Nombre:" + tercero[i].nombre + "\n" + "ID REPASAT:" + tercero[i].idRepasat);
                    //    }
                    //    continue;
                    //}

                    if (forceSync) //valido si existe el cliente previamente
                    {

                        string queryClientes = "SELECT CODCLI FROM CLIENTES WHERE LTRIM(CODCLI)='" + tercero[i].codIC + "'";
                        string queryProveedores = "SELECT CODPRO FROM PROVEED WHERE LTRIM(CODPRO)='" + tercero[i].codIC + "'";
                        string queryCheck = clientes ? queryClientes : queryProveedores;


                        if (sql.consultaExiste(queryCheck))
                        {
                            continue;
                        }
                    }
                    if (tercero[i].nombre != null)
                    {

                        codRPSTCuenta = tercero[i].codRepasat;
                        //Incorporamos estas lineas para indicar que si estamos forzando la creación del cliente o proveedor
                        //que utilice el código del cliente informado externamente

                        a3maestro.Nuevo();


                        codigo = forceSync ? tercero[i].codIC : a3maestro.NuevoCodigoNum();
                        codigo = string.IsNullOrEmpty(codigo) ? a3maestro.NuevoCodigoNum() : codigo;
                        codigo = codigo.Trim();
                        a3maestro.AsString[codigoTercero] = codigo.Trim();

                        if (tercero[i].apellidos.Trim() != "")
                            a3maestro.AsString[nombre] = tercero[i].nombre.ToUpper() + " " + tercero[i].apellidos.ToUpper();
                        else
                            a3maestro.AsString[nombre] = tercero[i].nombre.ToUpper();

                        a3maestro.set_AsString("RAZON", string.IsNullOrEmpty(tercero[i].razonSocial) ? "" : tercero[i].razonSocial.ToUpper());
                        a3maestro.set_AsString("NOMFISCAL", string.IsNullOrEmpty(tercero[i].razonSocial) ? "" : tercero[i].razonSocial.ToUpper());
                        a3maestro.set_AsString(direccion, string.IsNullOrEmpty(tercero[i].direccion) ? "" : tercero[i].direccion.ToUpper());
                        a3maestro.set_AsString("VIAPUBLICAFISCAL", string.IsNullOrEmpty(tercero[i].direccion) ? "" : tercero[i].direccion.ToUpper());
                        a3maestro.set_AsString(poblacion, string.IsNullOrEmpty(tercero[i].poblacion) ? "" : tercero[i].poblacion.ToUpper());
                        a3maestro.set_AsString(cPostal, tercero[i].codigoPostal);
                        a3maestro.set_AsString("CODPROVI", string.IsNullOrEmpty(tercero[i].codprovincia) ? "" : tercero[i].codprovincia);
                        a3maestro.set_AsString("PROVIFISCAL", string.IsNullOrEmpty(tercero[i].codprovincia) ? "" : tercero[i].codprovincia);
                        a3maestro.set_AsString(nif, tercero[i].nifcif);
                        a3maestro.set_AsString(telef, tercero[i].telf);
                        a3maestro.set_AsString(telef2, tercero[i].telf2);
                        a3maestro.set_AsString(fax, tercero[i].fax);
                        a3maestro.set_AsString("E_MAIL", tercero[i].email);
                        a3maestro.set_AsString("EMAILFISCAL", tercero[i].email);
                        a3maestro.set_AsString("PAGINAWEB", tercero[i].web);




                        a3maestro.set_AsString("ZONA", tercero[i].zona);
                        a3maestro.set_AsString("RUTA", tercero[i].ruta);
                        a3maestro.set_AsString("CAR1", tercero[i].nomIC);
                        a3maestro.set_AsString("PARAM1", tercero[i].permalinkFiscalIdentity);
                        a3maestro.set_AsString("DIASPAGO1", string.IsNullOrEmpty(tercero[i].diaPago1) ? "" : tercero[i].diaPago1.ToString());
                        a3maestro.set_AsString("DIASPAGO2", string.IsNullOrEmpty(tercero[i].diaPago2) ? "" : tercero[i].diaPago2.ToString());
                        a3maestro.set_AsString("DIASPAGO3", string.IsNullOrEmpty(tercero[i].diaPago3) ? "" : tercero[i].diaPago3.ToString());

                        //Clientes Genéricos
                        a3maestro.set_AsString("generico", tercero[i].cuentaGenerica == "1" ? "T" : "F");

                        if (!clientes && tercero[i].tipoCliente == "PRO")
                        {
                            a3maestro.set_AsString("TIPOREGISTRO", tercero[i].tipoProveedor == "ACR" ? "A" : "P");
                        }

                        a3maestro.set_AsString("REGIVA", tercero[i].regimenIva);

                        if (csGlobal.databaseA3.Contains("ADMANMEDIA"))
                        {
                            a3maestro.set_AsString("PARAM1", tercero[i].codIC);
                            a3maestro.set_AsString("PARAM9", tercero[i].id_customer_Ext); // codCli
                        }

                        a3maestro.set_AsString("CODMON", tercero[i].moneda == "EUR" ? "EURO" : tercero[i].moneda);

                        if (tercero[i].tipoIRPF != null)
                        {
                            a3maestro.set_AsString("TIPOIRPF", tercero[i].tipoIRPF);
                            a3maestro.set_AsString("PORIRPF", tercero[i].porcentajeIRPF);
                            a3maestro.set_AsString("ID_CLAVEIRPF", tercero[i].claveIRPF);
                            a3maestro.set_AsString("ID_SUBCLAVEIRPF", tercero[i].subclaveIRPF);
                        }

                        if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                        {
                            a3maestro.set_AsString("FORPAG", "90");
                        }
                        else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                        {
                            a3maestro.set_AsString("FORPAG", "60");
                        }

                        if (tercero[i].codPais == "" || tercero[i].codPais == null)
                        {
                            if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                            {
                                a3maestro.set_AsString("CODPAIS", "ES");
                            }
                            else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
                            {
                                a3maestro.set_AsString("CODPAIS", "FR");
                            }
                        }
                        else
                        {
                            a3maestro.set_AsString("CODPAIS", tercero[i].codPais);
                        }

                        if (tercero[i].descFormaPago == "paypal" || tercero[i].descFormaPago == "" || tercero[i].descFormaPago == "0")
                        {
                            a3maestro.set_AsString("E_MAIL", tercero[i].paypal);
                            a3maestro.set_AsString("DOCPAG", "PAY");
                            a3maestro.set_AsString("PARAM2", tercero[i].paypal);
                        }
                        else
                        {
                            a3maestro.set_AsString("DOCPAG", (string.IsNullOrEmpty(tercero[i].codDocPago) ? "TR" : tercero[i].codDocPago));
                        }

                        a3maestro.set_AsString("FORPAG", (string.IsNullOrEmpty(tercero[i].codFormaPago) ? "C" : tercero[i].codFormaPago));

                        a3maestro.Guarda(true);

                        ///Preparo la actualización del cliente creado e informo el Id en Repasat

                        DataRow drParams = dtParams.NewRow();
                        drParams["FIELD"] = "codExternoCli";
                        drParams["KEY"] = tercero[i].idRepasat;
                        drParams["VALUE"] = codigo;
                        dtParams.Rows.Add(drParams);

                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = tercero[i].idRepasat;
                        dtKeys.Rows.Add(drKeys);
                        //Sincronización de cuentas

                        rpstWS.sincronizarObjetoRepasat("accounts", "PUT", dtKeys, dtParams, "");

                        //Falta sincronizar la dirección principal e informale el id

                        for (int cont = 0; cont < direcciones.Length; cont++)
                        {
                            if (direcciones[cont].idClienteDireccionRepasat == tercero[i].repasatID)
                            {
                                direcciones[cont].codCli = codigo;
                                if (direcciones[cont].direccionfiscal)
                                {
                                    idRepasatDireccionPpal = direcciones[cont].idDireccionRepasat;
                                    continue;
                                }
                            }
                        }

                        //Actualizamos en cada contacto el Codigo de Cliente creado

                        for (int cont = 0; cont < contactos.Length; cont++)
                        {
                            if (contactos[cont].idClienteRepasat == tercero[i].idRepasat)
                            {
                                contactos[cont].codClienteERP = codigo;
                            }
                        }
                        updateA3.actualizarContactos(contactos, false, tercero[i].idRepasat, clientes);

                        //Fin gestión Alta de Contactos de la cuenta
                        //*******************************************




                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                        {
                            //obtener id de la dirección principal y actualizar la dirección en Repasat
                            idA3ERPDireccionPpal = sql.obtenerCampoTabla("SELECT IDDIRENT FROM " + tablaDirecciones + " WHERE LTRIM(" + keyFieldtablaDirecciones + ")='" + codigo + "' AND DEFECTO ='T'");
                            idA3ERPDireccionPpal = Math.Round(Convert.ToDecimal(idA3ERPDireccionPpal), 0).ToString();
                            rpstWS.actualizarDocumentoRepasat("addresses", "address[codExternoDireccion]", idRepasatDireccionPpal, idA3ERPDireccionPpal);
                            scriptUpdateDireccion = "UPDATE " + tablaDirecciones + " SET " + rpst_id_direccion + "=" + idRepasatDireccionPpal + " WHERE IDDIRENT=" + idA3ERPDireccionPpal;
                            csUtilidades.ejecutarConsulta(scriptUpdateDireccion, false);
                        }

                        if (clientes)
                        {
                            direcionesA3ERP.crearDireccionRepasat(direcciones, tercero[i].idRepasat, clientes);
                        }

                        if (domiciliacion != null)
                        {
                            domiciliacion.codTercero = codigo.Replace(" ", "");
                            //crearDomiciliacion(domiciliacion, true);
                        }
                        Repasat.csUpdateData updateRPST = new Repasat.csUpdateData();

                        //Reviso las cuentas bancarias del cliente, si existen las creo en A3
                        updateRPST.gestionarCuentasBancariasRepasatToA3ERP(clientes, false, null, false, false, false, codRPSTCuenta, true);
                    }

                }

                // return codigo;
                //a3maestro.Acabar();
                //a3maestro = null;
                return null;
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.Message + tercero[indice].nombre);
                a3maestro.Acabar();
                return null;
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
        }


        public string crearProveedorA3Objects(Objetos.csTercero proveedor)
        {
            Maestro a3maestro = null;
            string codigo = "";

            try
            {
                a3maestro = new Maestro();
                a3maestro.Iniciar("proveedores");
                a3maestro.Nuevo();

                codigo = a3maestro.NuevoCodigoNum();
                codigo = codigo.Trim();
                a3maestro.AsString["CODPRO"] = codigo.Trim();

                a3maestro.AsString["NOMPRO"] = proveedor.nombre.ToUpper() + " " + proveedor.apellidos.ToUpper();
                a3maestro.set_AsString("DIRPRO", proveedor.direccion);
                a3maestro.set_AsString("POBPRO", proveedor.poblacion);
                a3maestro.set_AsString("DTOPRO", proveedor.codigoPostal);
                a3maestro.set_AsString("FAXPRO", proveedor.telf);
                a3maestro.set_AsString("RAZON", proveedor.razonSocial);
                a3maestro.set_AsString("E_MAIL", proveedor.email);
                a3maestro.set_AsString("CAR1", proveedor.nomIC);
                a3maestro.set_AsString("PARAM1", proveedor.permalink);
                a3maestro.set_AsString("PARAM2", proveedor.paypal);
                a3maestro.set_AsString("NIFPRO", proveedor.nifcif);

                if (proveedor.codFormaPago == "PAY")
                    a3maestro.set_AsString("DOCPAG", "PAY");
                else
                    a3maestro.set_AsString("DOCPAG", "TR");
                a3maestro.Guarda(true);
                return codigo;


            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }

        }


        public string crearProveedoresA3Objects(Objetos.csTercero[] proveedores, Objetos.csDomBanca[] domiciliaciones = null)
        {
            Maestro a3maestro = null;
            string codigo = "";

            for (int i = 0; i < proveedores.Count(); i++)
            {
                try
                {
                    a3maestro = new Maestro();
                    a3maestro.Iniciar("proveedores");
                    a3maestro.Nuevo();

                    codigo = a3maestro.NuevoCodigoNum();
                    codigo = codigo.Trim();
                    a3maestro.AsString["CODPRO"] = codigo.Trim();

                    a3maestro.AsString["NOMPRO"] = proveedores[i].nombre.ToUpper() + " " + proveedores[i].apellidos.ToUpper();
                    a3maestro.set_AsString("DIRPRO", proveedores[i].direccion);
                    a3maestro.set_AsString("POBPRO", proveedores[i].poblacion);
                    a3maestro.set_AsString("DTOPRO", proveedores[i].codigoPostal);
                    a3maestro.set_AsString("FAXPRO", proveedores[i].telf);
                    a3maestro.set_AsString("RAZON", proveedores[i].razonSocial);
                    a3maestro.set_AsString("E_MAIL", proveedores[i].email);
                    a3maestro.set_AsString("CAR1", proveedores[i].nomIC);
                    a3maestro.set_AsString("PARAM1", proveedores[i].permalink);
                    a3maestro.set_AsString("PARAM2", proveedores[i].paypal);
                    a3maestro.set_AsString("NIFPRO", proveedores[i].nifcif);

                    if (proveedores[i].codFormaPago == "PAY")
                        a3maestro.set_AsString("DOCPAG", "PAY");
                    else
                        a3maestro.set_AsString("DOCPAG", "TR");
                    a3maestro.Guarda(true);



                    /*  if (domiciliaciones[i] != null)
                      {
                          domiciliaciones[i].codTercero = codigo.Replace(" ", "");
                          crearDomiciliacion(domiciliaciones[i], true);
                      }*/
                }
                catch (Exception ex)
                {
                }

                finally
                {
                    a3maestro.Acabar();
                    a3maestro = null;
                }
            }

            /*if (dtProveedores == null && csGlobal.nodeTienda.ToUpper() != "ADMAN")
                mySqlConnect.actualizarIdClientePS(idClientePS, codigo);
                */

            return codigo;
        }

        public string crearProveedoresA3(string nombre, string idClientePS, DataRow dtProveedores = null, string tipoProveedor = "")
        {
            DataRow datosProveedor = null;
            csMySqlConnect mySqlConnect = new csMySqlConnect();

            // 27/07/2015 modificacion para adman
            if (dtProveedores == null)
            {
                datosProveedor = mySqlConnect.selectDatosClientesAltaA3(idClientePS).Rows[0];
            }
            else
            {
                datosProveedor = dtProveedores;
            }

            //select ps_address.company 0,
            //ps_customer.firstname 1, ps_customer.lastname 2, email 3,id_address_delivery 4," +
            //" ps_address.address1 5, ps_address.postcode 6, ps_address.city 7, ps_address.phone 8, 
            //ps_address.phone_mobile 9, ps_address.vat_number 10" +
            string empresa = datosProveedor.ItemArray[0].ToString().ToUpper();
            //string nombreUsuario = datosProveedor.ItemArray[1].ToString().ToUpper();
            //string apellUsuario = datosProveedor.ItemArray[2].ToString().ToUpper();
            string email = datosProveedor.ItemArray[1].ToString();
            string direccion = datosProveedor.ItemArray[2].ToString().ToUpper();
            string codPostal = datosProveedor.ItemArray[3].ToString().ToUpper();
            string poblacion = datosProveedor.ItemArray[4].ToString().ToUpper();
            string telefono = datosProveedor.ItemArray[5].ToString().ToUpper();
            string movil = datosProveedor.ItemArray[6].ToString().ToUpper();


            string permalink = datosProveedor.ItemArray[7].ToString();

            Maestro a3maestro = new Maestro();
            string codigo = "";
            try
            {
                a3maestro.Iniciar("proveedores");
                a3maestro.Nuevo();
                codigo = a3maestro.NuevoCodigoNum();
                a3maestro.AsString["CODPRO"] = codigo;
                a3maestro.AsString["NOMPRO"] = nombre.ToUpper();
                a3maestro.set_AsString("DIRPRO", direccion);
                a3maestro.set_AsString("POBPRO", poblacion);
                a3maestro.set_AsString("DTOPRO", codPostal);
                a3maestro.set_AsString("FAXPRO", movil);
                a3maestro.set_AsString("RAZON", empresa);
                a3maestro.set_AsString("E_MAIL", email);
                a3maestro.set_AsString("CAR1", tipoProveedor);
                a3maestro.set_AsString("PARAM1", permalink);
                if (email != "")
                    a3maestro.set_AsString("DOCPAG", "PAY");
                else
                    a3maestro.set_AsString("DOCPAG", "TR");
                a3maestro.Guarda(true);
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }


            if (dtProveedores == null && csGlobal.nodeTienda.ToUpper() != "ADMAN")
                mySqlConnect.actualizarIdClientePS(idClientePS, codigo);


            ////////////////////////////////////////////////


            #region Datos Bancarios

            csa3erp a3erp = new csa3erp();
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            Objetos.csDomBanca domiciliacion = new Objetos.csDomBanca();

            //string account_type = datosProveedor.ItemArray[8].ToString().ToUpper();
            string account_number = datosProveedor.ItemArray[10].ToString().ToUpper();
            string identifier_number = datosProveedor.ItemArray[11].ToString().ToUpper();
            //string identifier_type = datosProveedor.ItemArray[12].ToString().ToUpper();

            if (account_number != "") // TIENE IBAN
            {
                domiciliacion.iban = account_number.ToUpper().Substring(0, 4); // ES87
                domiciliacion.banco = account_number.ToUpper().Substring(4, 4); //"0081";
                domiciliacion.agencia = account_number.ToUpper().Substring(8, 4); //"0023";
                domiciliacion.digitoControl = account_number.ToUpper().Substring(12, 2); // "11";
                domiciliacion.numcuenta = account_number.ToUpper().Substring(14); ; //"1111222234";
                domiciliacion.titular = nombre.ToUpper(); //"KLOSIONS";
                domiciliacion.bic = identifier_number.ToUpper(); //"BSABESBBXXX";
                domiciliacion.codTercero = codigo; // "4";

                a3erpTercero.crearDomiciliacion(domiciliacion, true);
            }

            #endregion


            return codigo;
        }

        public string crearProveedoresA3V2(Objetos.csProveedor[] proveedores, Objetos.csDomBanca[] domiciliacion, string tipoProveedor)
        {
            Maestro a3maestro = new Maestro();
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            string codigo = "";
            csa3erp a3erp = new csa3erp();

            try
            {
                for (int i = 0; i < proveedores.Length; i++)
                {
                    a3maestro.Iniciar("proveedores");
                    a3maestro.Nuevo();
                    codigo = a3maestro.NuevoCodigoNum();
                    if (domiciliacion[i] != null)
                        domiciliacion[i].codTercero = codigo;
                    proveedores[i].codigoA3 = codigo;
                    a3maestro.AsString["CODPRO"] = codigo;
                    a3maestro.AsString["NOMPRO"] = proveedores[i].nombre;
                    a3maestro.set_AsString("DIRPRO", proveedores[i].direccion);
                    a3maestro.set_AsString("POBPRO", proveedores[i].poblacion);
                    a3maestro.set_AsString("DTOPRO", proveedores[i].codigoPostal);
                    a3maestro.set_AsString("FAXPRO", proveedores[i].telf);
                    a3maestro.set_AsString("RAZON", proveedores[i].razonSocial);
                    a3maestro.set_AsString("E_MAIL", proveedores[i].email);
                    a3maestro.set_AsString("CAR1", tipoProveedor);
                    a3maestro.set_AsString("PARAM1", proveedores[i].permalink);
                    if (proveedores[i].email != "")
                        a3maestro.set_AsString("DOCPAG", "PAY");
                    else
                        a3maestro.set_AsString("DOCPAG", "TR");
                    a3maestro.Guarda(true);

                    if (domiciliacion[i] != null) // TIENE IBAN
                    {
                        a3erpTercero.crearDomiciliacion(domiciliacion[i], true);
                    }

                    a3maestro.Acabar();
                }
            }
            finally
            {
                a3maestro = null;
            }

            return codigo;
        }


        public string crearClientesA3Adman(string nombre, string idClientePS, DataRow dtClientes = null)
        {
            DataRow datosCliente = null;
            csMySqlConnect mySqlConnect = new csMySqlConnect();

            // 27/07/2015 modificacion para adman
            if (dtClientes == null)
            {
                datosCliente = mySqlConnect.selectDatosClientesAltaA3(idClientePS).Rows[0];
            }
            else
            {
                datosCliente = dtClientes;
            }

            string empresa = datosCliente.ItemArray[2].ToString().ToUpper();
            string nombreUsuario = datosCliente.ItemArray[5].ToString().ToUpper();
            string apellUsuario = datosCliente.ItemArray[6].ToString().ToUpper();
            string telefono = datosCliente.ItemArray[7].ToString().ToUpper();
            string movil = datosCliente.ItemArray[7].ToString().ToUpper();
            string direccion = datosCliente.ItemArray[12].ToString().ToUpper();
            string codPostal = datosCliente.ItemArray[16].ToString().ToUpper();
            string poblacion = datosCliente.ItemArray[14].ToString().ToUpper();
            string email = datosCliente.ItemArray[4].ToString();
            string permalink = datosCliente.ItemArray[1].ToString();

            Maestro a3maestro = new Maestro();
            string codigo = "";
            try
            {
                if (nombre.Trim() != "")
                {
                    a3maestro.Iniciar("clientes");
                    a3maestro.Nuevo();
                    codigo = a3maestro.NuevoCodigoNum().Trim();
                    a3maestro.AsString["CODCLI"] = codigo;
                    a3maestro.AsString["NOMCLI"] = nombre.ToUpper();
                    //a3maestro.set_AsString("DIRCLI", idClientePS);
                    a3maestro.set_AsString("POBCLI", poblacion);
                    a3maestro.set_AsString("DTOCLI", codPostal);
                    a3maestro.set_AsString("FAXCLI", movil);
                    a3maestro.set_AsString("RAZON", empresa);
                    a3maestro.set_AsString("DIRCLI", direccion);
                    a3maestro.set_AsString("E_MAIL", email);
                    //a3maestro.set_AsString("KLS_CODCLIENTE", direccion);
                    a3maestro.set_AsString("KLS_EMAIL_USUARIO", email);
                    a3maestro.set_AsString("KLS_NOM_USUARIO", nombreUsuario.ToUpper());
                    a3maestro.set_AsString("KLS_APELL_USUARIO", apellUsuario.ToUpper());
                    a3maestro.set_AsString("PARAM1", permalink);
                    a3maestro.Guarda(true);
                }
            }
            finally
            {
                if (nombre.Trim() != "")
                {
                    a3maestro.Acabar();
                    a3maestro = null;
                }
            }

            if (dtClientes == null)
                mySqlConnect.actualizarIdClientePS(idClientePS, codigo);

            return codigo;
        }

        public string crearClientesA3V2(string nombre, string idClientePS, DataTable dtClientes = null)
        {
            DataTable datosCliente = null;
            csMySqlConnect mySqlConnect = new csMySqlConnect();

            // 27/07/2015 modificacion para adman
            if (dtClientes == null)
            {
                datosCliente = mySqlConnect.selectDatosClientesAltaA3(idClientePS);
            }
            else
            {
                datosCliente = dtClientes;
            }

            //select ps_address.company 0,
            //ps_customer.firstname 1, ps_customer.lastname 2, email 3,id_address_delivery 4," +
            //" ps_address.address1 5, ps_address.postcode 6, ps_address.city 7, ps_address.phone 8, 
            //ps_address.phone_mobile 9, ps_address.vat_number 10" +
            string empresa = datosCliente.Rows[0].ItemArray[0].ToString().ToUpper();
            string nombreUsuario = datosCliente.Rows[0].ItemArray[1].ToString().ToUpper();
            string apellUsuario = datosCliente.Rows[0].ItemArray[2].ToString().ToUpper();
            string telefono = datosCliente.Rows[0].ItemArray[8].ToString().ToUpper();
            string movil = datosCliente.Rows[0].ItemArray[9].ToString().ToUpper();
            string direccion = datosCliente.Rows[0].ItemArray[5].ToString().ToUpper();
            string codPostal = datosCliente.Rows[0].ItemArray[6].ToString().ToUpper();
            string poblacion = datosCliente.Rows[0].ItemArray[7].ToString().ToUpper();
            string email = datosCliente.Rows[0].ItemArray[3].ToString();

            Maestro a3maestro = new Maestro();
            string codigo = "";
            try
            {
                a3maestro.Iniciar("clientes");
                a3maestro.Nuevo();
                codigo = a3maestro.NuevoCodigoNum();
                a3maestro.AsString["CODCLI"] = codigo;
                a3maestro.AsString["NOMCLI"] = nombre;
                a3maestro.set_AsString("DIRCLI", idClientePS);
                a3maestro.set_AsString("POBCLI", poblacion);
                a3maestro.set_AsString("KLS_CODCLIENTE", direccion);
                a3maestro.set_AsString("DTOCLI", codPostal);
                a3maestro.set_AsString("FAXCLI", movil);
                a3maestro.set_AsString("RAZON", empresa);
                a3maestro.set_AsString("DIRCLI", direccion);
                a3maestro.set_AsString("E_MAIL", email);
                a3maestro.set_AsString("KLS_EMAIL_USUARIO", email);
                a3maestro.set_AsString("KLS_NOM_USUARIO", nombreUsuario);
                a3maestro.set_AsString("KLS_APELL_USUARIO", apellUsuario);
                a3maestro.Guarda(true);
                //ACTUALIZO EN PRESTASHOP EL ID DEL CLIENTE CREADO
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;

            }

            if (dtClientes == null)
                mySqlConnect.actualizarIdClientePS(idClientePS, codigo);

            return codigo;
        }



        /// <summary>
        /// Esto se usa para Adman
        /// </summary>
        /// <param name="domiciliacion"></param>
        /// <param name="proveedor"></param>
        /// <returns></returns>
        public string crearDomiciliacion(Objetos.csDomBanca domiciliacion, bool proveedor)
        {
            a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
            char pad = '0';

            if (proveedor)
            {
                maestro.Iniciar("DOMBANCAC");
                maestro.Nuevo();
                maestro.set_AsString("CODPRO", domiciliacion.codTercero);
            }
            else
            {
                maestro.Iniciar("DOMBANCAV");
                maestro.Nuevo();
                maestro.set_AsString("CODCLI", domiciliacion.codTercero);
            }

            maestro.OmitirMensajes = true;
            //SI NO PONEMOS NÚMERO DE DOMICILIACIÓN "NUMDOM", COGE EL NÚMERO CORRELATIVO
            //maestro.set_AsInteger("NUMDOM", 4);
            //SI LOS DATOS BANCARIOS NO SON CORRECTOS DA ERROR (QUITAR AVISOS)

            //VERIFICO TAMAÑOS Y COMPLETO CON 0 A LA IZQUIERDA SI NO LLEGAN
            domiciliacion.banco = domiciliacion.banco.PadLeft(4, pad);
            domiciliacion.agencia = domiciliacion.agencia.PadLeft(4, pad);
            domiciliacion.digitoControl = domiciliacion.digitoControl.PadLeft(2, pad);
            domiciliacion.numcuenta = domiciliacion.numcuenta.PadLeft(10, pad);
            domiciliacion.titular = domiciliacion.titular.Replace(")", "").Replace("(", "").Replace("&", "");
            //maestro.set_AsString("BANCO", domiciliacion.banco);
            //maestro.set_AsString("AGENCIA", domiciliacion.agencia);
            maestro.set_AsString("TITULAR", domiciliacion.titular);
            //maestro.set_AsString("DIGITO", domiciliacion.digitoControl);

            //maestro.set_AsString("DEFECTO", "T");
            maestro.set_AsString("ZONASEPA", "T");
            maestro.set_AsString("CODPAIS", "ES");
            string[] bic = null;
            if (domiciliacion.bic.Contains(":"))
            {
                bic = domiciliacion.bic.Split(':');
                domiciliacion.bic = bic[2];
            }

            if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
            {
                string cuenta = domiciliacion.banco + domiciliacion.agencia + domiciliacion.digitoControl + domiciliacion.numcuenta;
                //maestro.set_AsString("IBAN", domiciliacion.iban);
                maestro.set_AsString("IBANEXT", domiciliacion.iban);
                maestro.set_AsString("CUENTAEXT", cuenta);
                maestro.set_AsString("BICEXT", (domiciliacion.bic == null) ? "XXX" : domiciliacion.bic);

                //Deprecated 27-3-2018 actualizado porque han cambiado a versión 10.0.7.8 de A3ERP
                //maestro.set_AsString("CUENTAEXT", domiciliacion.cuentaext);
                // maestro.set_AsString("CUENTAEXT", cuenta);
                //maestro.set_AsString("NUMCUENTA", domiciliacion.numcuenta);
            }
            else if (csGlobal.databaseA3 == "ADMANMEDIA_FR")
            {
                maestro.set_AsString("IBANEXT", domiciliacion.iban);
                maestro.set_AsString("BICEXT", (domiciliacion.bic == null) ? "XXX" : domiciliacion.bic);
                //maestro.set_AsString("BICEXT", "XXX");
                maestro.set_AsString("CUENTAEXT", domiciliacion.numcuenta);
                //maestro.set_AsString("CUENTAEXT", domiciliacion.numcuenta);
            }

            //string codcli = maestro.get_AsString("NUMDOM");
            maestro.Guarda(true);
            string codcli = maestro.get_AsString("NUMDOM");
            maestro.Acabar();

            return codcli;
        }

        public void verificarDomiciliaciones(Objetos.csDomBanca[] domiciliacion, bool proveedor)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                DataTable dtBancos = new DataTable();
                DataTable dtBancos2Check = new DataTable();
                dtBancos = sql.obtenerDatosSQLScript("SELECT LTRIM(CODCLI) AS CODIC, IBANEXT+CUENTAEXT AS FULL_IBAN_ACCOUNT, RPST_ID_BANCO, IDDOMBANCA FROM DOMBANCA");
                string clienteA3 = "";
                string fullBancA3 = "";
                string clientExtRPST = "";
                string fullIBANRPST = "";
                int bancosActualizados = 0;
                string resumenActualizacion = "";

                foreach (Objetos.csDomBanca cuentabancaria in domiciliacion)
                {
                    clientExtRPST = cuentabancaria.codTercero;
                    fullIBANRPST = cuentabancaria.full_iban_account;
                    foreach (DataRow fila in dtBancos.Rows)
                    {
                        clienteA3 = fila["CODIC"].ToString();
                        fullBancA3 = fila["FULL_IBAN_ACCOUNT"].ToString();
                        if (clienteA3 == clientExtRPST && cuentabancaria.rpstIdBanco == fila["RPST_ID_BANCO"].ToString())
                        {
                            if (fullBancA3 == fullIBANRPST)
                            {
                                clienteA3 = clienteA3;
                                clientExtRPST = clientExtRPST;
                                continue;
                            }
                            else
                            {
                                string message = "Se han encontrado 2 cuentas no coincidentes " + "\n" + "\n" +
                                   "CLIENTE: " + clienteA3 + " " + cuentabancaria.nomTercero + "\n" + "\n" +
                                   "REPASAT: " + fullIBANRPST + "\n" +
                                    "A3ERP:   " + fullBancA3 + "\n" + "\n";

                                DialogResult Resultado = MessageBox.Show(message + "Se va a actualizar el banco del cliente en A3ERP. \n¿Está seguro?", "Actualizar Datos Bancarios", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (Resultado == DialogResult.Yes)
                                {
                                    sql.actualizarCampo("UPDATE DOMBANCA " +
                                     " SET IBAN='" + cuentabancaria.iban + "'," +
                                     " IBANEXT = '" + cuentabancaria.iban + "', " +
                                     " BANCO = '" + cuentabancaria.banco + "'," +
                                     " AGENCIA = '" + cuentabancaria.agencia + "'," +
                                     " DIGITO = '" + cuentabancaria.digitoControl + "'," +
                                     " NUMCUENTA = '" + cuentabancaria.numcuenta + "'," +
                                     " CUENTAEXT = '" + cuentabancaria.banco + cuentabancaria.agencia + cuentabancaria.digitoControl + cuentabancaria.numcuenta + "'" +
                                     " WHERE LTRIM(CODCLI)='" + clienteA3 + "' AND RPST_ID_BANCO=" + fila["RPST_ID_BANCO"].ToString());
                                    bancosActualizados++;
                                    resumenActualizacion = "CLIENTE: " + clienteA3 + "|" + "Banco en REPASAT: " + fullIBANRPST + " >>> " + "Banco sustituido en A3ERP:   " + fullBancA3 + "\n" + "\n" + resumenActualizacion;
                                }



                            }

                        }

                    }

                }
                ("Se han actualizado " + bancosActualizados.ToString() + " cuentas bancarias de clientes en A3ERP" + "\n" + resumenActualizacion).mb();
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }

        }

        public string crearDomiciliacion(Objetos.csDomBanca[] domiciliacion, bool cliente)
        {
            //GESTIÓN ERRORES
            string errorCodIC = "";
            string errorCuentaBancaria = "";
            string errorIdRPST = "";
            string errorMsg = "";
            string defaultBank = "";
            string updateQueryMandato = "";
            string codBancoA3ERP = "";
            csRepasatWebService rpstWebService = new csRepasatWebService();

            try
            {
                bool proveedor = cliente ? false : true;
                a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
                string idNumDom = "";

                csSqlConnects sql = new csSqlConnects();

                if (proveedor)
                {
                    maestro.Iniciar("DOMBANCAC");
                }
                else
                {
                    maestro.Iniciar("DOMBANCAV");
                }

                for (int i = 0; i < domiciliacion.Count(); i++)
                {
                    string cuenta = domiciliacion[i].banco + domiciliacion[i].agencia + domiciliacion[i].digitoControl + domiciliacion[i].numcuenta;

                    errorCodIC = domiciliacion[i].codTercero;
                    errorCuentaBancaria = cuenta;
                    errorIdRPST = domiciliacion[i].rpstIdBanco;
                    errorMsg = "Cuenta ERP " + errorCodIC + "\n" +
                               "Banco " + errorCuentaBancaria + "\n" +
                               "Código Banco RPST " + errorIdRPST + "\n\n";


                    // valido si el banco existe
                    // si existe, salto al siguiente registro
                    //12-9-20 cambio de verificación de existencia de datos bancarios
                    //el campo RPST_ID_BANCO da problemas y pasamos a verificar el contenido de la cuenta bancaria completa

                    //if (sql.consultaExiste("SELECT IDDOMBANCA FROM DOMBANCA WHERE RPST_ID_BANCO=" + domiciliacion[i].rpstIdBanco))
                    if (sql.consultaExiste("SELECT CONCAT(IBANEXT,CUENTAEXT) AS DOMICILIACION FROM DOMBANCA WHERE CONCAT(IBANEXT,CUENTAEXT)='" + domiciliacion[i].full_iban_account + "'"))
                    {
                        continue;
                    }
                    else  //verifico que no sea un problema de sincronización (que exista la cuenta pero no esté dada de alta)
                    {
                        if (sql.consultaExiste("SELECT IDDOMBANCA FROM DOMBANCA WHERE CUENTAEXT='" + cuenta + "' AND LTRIM(CODCLI)='" + domiciliacion[i].codTercero + "'"))
                        {
                            string idDombBanca = sql.obtenerCampoTabla("SELECT IDDOMBANCA FROM DOMBANCA WHERE CUENTAEXT='" + cuenta + "' AND LTRIM(CODCLI)='" + domiciliacion[i].codTercero + "'");
                            //quiere decir que existe la cuenta pero no está sincronizada, actualizo id Repasat

                            rpstWebService.actualizarDocumentoRepasat("Bank", "bank[codExternoDatosBancarios]", domiciliacion[i].rpstIdBanco, idDombBanca, false);
                            //sql.actualizarCampo("UPDATE DOMBANCA SET RPST_ID_BANCO=" + domiciliacion[i].rpstIdBanco + " WHERE CUENTAEXT='" + cuenta + "' AND LTRIM(CODCLI)='" + domiciliacion[i].codTercero + "'");
                            continue;
                        }
                    }

                    if (proveedor)
                    {
                        maestro.Nuevo();
                        maestro.set_AsString("CODPRO", domiciliacion[i].codTercero);
                    }
                    else
                    {
                        maestro.Nuevo();
                        maestro.set_AsString("CODCLI", domiciliacion[i].codTercero);
                    }
                    maestro.OmitirMensajes = true;
                    char pad = '0';

                    //SI NO PONEMOS NÚMERO DE DOMICILIACIÓN "NUMDOM", COGE EL NÚMERO CORRELATIVO
                    //maestro.set_AsInteger("NUMDOM", 4);
                    //SI LOS DATOS BANCARIOS NO SON CORRECTOS DA ERROR (QUITAR AVISOS)

                    //VERIFICO TAMAÑOS Y COMPLETO CON 0 A LA IZQUIERDA SI NO LLEGAN
                    domiciliacion[i].banco = domiciliacion[i].banco.PadLeft(4, pad);
                    domiciliacion[i].agencia = domiciliacion[i].agencia.PadLeft(4, pad);
                    domiciliacion[i].digitoControl = domiciliacion[i].digitoControl.PadLeft(2, pad);
                    domiciliacion[i].numcuenta = domiciliacion[i].numcuenta.PadLeft(10, pad);
                    domiciliacion[i].titular = string.IsNullOrEmpty(domiciliacion[i].titular) ? "PRINCIPAL" : domiciliacion[i].titular.Replace(")", "").Replace("(", "").Replace("&", "");
                    //maestro.set_AsString("BANCO", domiciliacion[i].banco);
                    //maestro.set_AsString("AGENCIA", domiciliacion[i].agencia);
                    maestro.set_AsString("TITULAR", domiciliacion[i].titular);
                    //maestro.set_AsString("DIGITO", domiciliacion[i].digitoControl);


                    defaultBank = domiciliacion[i].bancoPrincipal == "1" ? "T" : "F";
                    maestro.set_AsString("DEFECTO", defaultBank);
                    //maestro.set_AsString("ZONASEPA", "T");
                    //maestro.set_AsString("CODPAIS", "ES");



                    // maestro.set_AsString("IBAN", domiciliacion[i].iban);
                    maestro.set_AsString("IBANEXT", domiciliacion[i].iban);
                    maestro.set_AsString("CUENTAEXT", cuenta);
                    maestro.set_AsString("BICEXT", (domiciliacion[i].bic == null) ? "" : domiciliacion[i].bic);
                    maestro.set_AsString("NOMBAN", domiciliacion[i].nomBanco);
                    //maestro.set_AsString("RPST_ID_BANCO", domiciliacion[i].rpstIdBanco);

                    maestro.Guarda(true);
                    codBancoA3ERP = maestro.get_AsString("IDDOMBANCA");

                    //Actualizo Repasat
                    rpstWebService.actualizarDocumentoRepasat("banks", "bank[codExternoDatosBancarios]", domiciliacion[i].rpstIdBanco, codBancoA3ERP, false);

                    //MANDATOS
                    sql.actualizarCampo("UPDATE DOMBANCA SET MANDATOCONFIRMADO='T', MANDATOFECHACONFIRMADO='01-01-2020', MANDATOFECHAFIRMA='01-01-2020' " +
                        " WHERE LTRIM(CODCLI) ='" + domiciliacion[i].codTercero + "'");

                    //idNumDom = maestro.get_AsString("NUMDOM");

                }
                maestro.Acabar();
                return idNumDom;
            }
            catch (Exception ex)
            {
                (errorMsg + ex.Message).mb();
                return null;
            }

        }

        public void crearConctactoA3(string nombre, string referencia, bool modifica = false)
        {


            Maestro a3maestro = new Maestro();
            try
            {   //Obtener Número de Organización según cliente o proveedor
                csSqlConnects sql = new csSqlConnects();
                string codCuentaA3 = "";
                string idOrgCuenta = "";
                string idContactoRPST = nombre;
                int numContactoCuenta = 0;
                string codigo = "";

                idOrgCuenta = sql.obtenerCampoTabla("SELECT IDORG FROM __CLIENTES WHERE LTRIM(CODCLI)='" + codCuentaA3 + "'");
                idOrgCuenta = "1";
                if (modifica)
                {
                    numContactoCuenta = Convert.ToInt32(sql.obtenerCampoTabla("SELECT NUMCON AS NUMCONTACTO  FROM CONTACTOS WHERE RPST_ID_CONTACT='" + idContactoRPST + "'"));
                }
                else
                {
                    numContactoCuenta = Convert.ToInt32(sql.obtenerCampoTabla("SELECT MAX(NUMCON)+1 AS NUMCONTACTO  FROM CONTACTOS WHERE TABLA='ORG' AND LTRIM(CODIGO)='" + idOrgCuenta + "'"));
                }

                a3maestro.Iniciar("CONTACTOS");

                if (modifica)
                {
                    if (a3maestro.Buscar(new object[] { "ORG", idOrgCuenta, numContactoCuenta }))
                    {
                        a3maestro.Edita();
                        a3maestro.AsString["NOMBRE"] = "ELVIS (modificado)";
                        a3maestro.AsString["EMAIL"] = "hola@luz.com";
                        a3maestro.AsString["TELEFONO1"] = "9122222";
                        a3maestro.AsString["TELEFONO2"] = "666222555";
                    }
                }
                else
                {
                    a3maestro.Nuevo();
                    a3maestro.AsString["TABLA"] = "ORG";
                    a3maestro.AsString["CODIGO"] = idOrgCuenta;
                    a3maestro.AsInteger["NUMCON"] = numContactoCuenta;
                    a3maestro.AsString["NOMBRE"] = "Contacto 1 del cliente 22";
                    a3maestro.AsString["EMAIL"] = "Contacto 1 del cliente 22";
                    a3maestro.AsString["TELEFONO1"] = "9122222";
                    a3maestro.AsString["TELEFONO2"] = "666222555";
                    a3maestro.AsString["RPST_ID_CONTACT"] = "11";
                }
                a3maestro.Guarda(true);
                //Recupero el id de contacto creado para pasarlo a Repasat
                codigo = a3maestro.get_AsString("IDCONTACTO");

            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }

        }


        public void crearContactosA3ERPV12()
        {
            csa3erp a3erp = new csa3erp();
            try
            {

                a3erp.abrirEnlace();
                Maestro a3maestro = new Maestro();

                a3maestro.Iniciar("__CONTACTOS");

                a3maestro.Nuevo();
                string nuevoCodigo = a3maestro.NuevoCodigoNum();
                //a3maestro.AsString["TABLA"] = "ORG";
                a3maestro.AsString["CODIGO"] = nuevoCodigo;
                //a3maestro.AsInteger["NUMCON"] = 111;
                a3maestro.AsString["NOMBRE"] = "JULIETA BENEGAS";
                a3maestro.AsString["EMAIL"] = "julietab@beta.com";
                a3maestro.AsString["TELEFONO1"] = "931234567";
                a3maestro.AsString["TELEFONO2"] = "666555888";
                a3maestro.AsString["RPST_ID_CONTACT"] = "222";

                a3maestro.Guarda(true);
                string codigo = a3maestro.get_AsString("CODIGO");  // PODEMOS UTILIZAR EL ID ES EL ID DEL CONTACTO
                //int idContacto = Convert.ToInt32(sql.obtenerCampoTabla("SELECT ID FROM __CONTACTOS WHERE LTRIM(CODIGO)='" + codigo.Trim() + "'"));

                a3maestro.Acabar();
                a3maestro = null;

                IContactoRelacion contactRel = new ContactoRelacion();
                contactRel.Iniciar();
                contactRel.Nuevo(Convert.ToInt32(codigo.Trim()), ContactoRelacionEntidad.tcreCliente, "5");
                contactRel.Guardar();

                a3erp.cerrarEnlace();
            }
            catch (Exception ex)
            {
                string mess = ex.Message.ToString();
                a3erp.cerrarEnlace();
            }
        }

        public bool comprobarNif(string nif, bool clientes = true)
        {
            csSqlConnects sql = new csSqlConnects();

            if (nif == "" || nif == null)
            {
                return false;
            }

            string queryNifClientes = "SELECT NIFCLI FROM CLIENTES WHERE NIFCLI='" + nif + "'";
            string queryNifProveedores = "SELECT NIFPRO FROM PROVEED WHERE NIFPRO='" + nif + "'";
            string queryNifCheck = clientes ? queryNifClientes : queryNifProveedores;


            if (sql.consultaExiste(queryNifCheck))
            {
                return true;
            }
            return false;
        }

        public bool comprobarCorreo(string correo, bool clientes = true)
        {
            csSqlConnects sql = new csSqlConnects();

            if (correo == "" || correo == null)
            {
                return false;
            }

            string queryCorreoClientes = "SELECT EMAILFISCAL FROM CLIENTES WHERE EMAILFISCAL='" + correo + "'";
            string queryCorreoProveedores = "SELECT EMAILFISCAL FROM PROVEED WHERE EMAILFISCAL='" + correo + "'";
            string queryCorreoCheck = clientes ? queryCorreoClientes : queryCorreoProveedores;


            if (sql.consultaExiste(queryCorreoCheck))
            {
                return true;
            }
            return false;
        }

        public bool comprobarTelf1(string tel1, bool clientes = true)
        {
            csSqlConnects sql = new csSqlConnects();

            if (tel1 == "" || tel1 == null)
            {
                return false;
            }

            string queryTel1Clientes = "SELECT TELCLI FROM CLIENTES WHERE TELCLI='" + tel1 + "'";
            string queryTel1Proveedores = "SELECT TELPRO FROM PROVEED WHERE TELPRO='" + tel1 + "'";
            string queryTel1Check = clientes ? queryTel1Clientes : queryTel1Proveedores;


            if (sql.consultaExiste(queryTel1Check))
            {
                return true;
            }
            return false;
        }

        public bool comprobarTelf2(string tel2, bool clientes = true)
        {
            csSqlConnects sql = new csSqlConnects();

            if (tel2 == "" || tel2 == null)
            {
                return false;
            }

            string queryTel2Clientes = "SELECT TELCLI2 FROM CLIENTES WHERE TELCLI2='" + tel2 + "'";
            string queryTel2Proveedores = "SELECT TELPRO FROM PROVEED WHERE TELPRO='" + tel2 + "'";
            string queryTel2Check = clientes ? queryTel2Clientes : queryTel2Proveedores;


            if (sql.consultaExiste(queryTel2Check))
            {
                return true;
            }
            return false;
        }
    }
}
