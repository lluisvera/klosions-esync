﻿namespace klsync
{
    partial class frReminderPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbDaysBefore = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDaysAfter = new System.Windows.Forms.TextBox();
            this.btSave = new System.Windows.Forms.Button();
            this.rbutYes = new System.Windows.Forms.RadioButton();
            this.rbutNo = new System.Windows.Forms.RadioButton();
            this.sendToCustomer = new System.Windows.Forms.Label();
            this.btnSendReminder = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEmailsEmpresa = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbDaysBefore
            // 
            this.tbDaysBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDaysBefore.Location = new System.Drawing.Point(291, 29);
            this.tbDaysBefore.Name = "tbDaysBefore";
            this.tbDaysBefore.Size = new System.Drawing.Size(51, 26);
            this.tbDaysBefore.TabIndex = 0;
            this.tbDaysBefore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Días previos a la notificación";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(237, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Días posteriores a la notificación";
            // 
            // tbDaysAfter
            // 
            this.tbDaysAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDaysAfter.Location = new System.Drawing.Point(291, 66);
            this.tbDaysAfter.Name = "tbDaysAfter";
            this.tbDaysAfter.Size = new System.Drawing.Size(51, 26);
            this.tbDaysAfter.TabIndex = 2;
            this.tbDaysAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btSave
            // 
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Location = new System.Drawing.Point(289, 192);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(89, 41);
            this.btSave.TabIndex = 4;
            this.btSave.Text = "Guardar";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // rbutYes
            // 
            this.rbutYes.AutoSize = true;
            this.rbutYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbutYes.Location = new System.Drawing.Point(289, 101);
            this.rbutYes.Name = "rbutYes";
            this.rbutYes.Size = new System.Drawing.Size(41, 24);
            this.rbutYes.TabIndex = 5;
            this.rbutYes.Text = "Sí";
            this.rbutYes.UseVisualStyleBackColor = true;
            // 
            // rbutNo
            // 
            this.rbutNo.AutoSize = true;
            this.rbutNo.Checked = true;
            this.rbutNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbutNo.Location = new System.Drawing.Point(331, 101);
            this.rbutNo.Name = "rbutNo";
            this.rbutNo.Size = new System.Drawing.Size(47, 24);
            this.rbutNo.TabIndex = 6;
            this.rbutNo.TabStop = true;
            this.rbutNo.Text = "No";
            this.rbutNo.UseVisualStyleBackColor = true;
            // 
            // sendToCustomer
            // 
            this.sendToCustomer.AutoSize = true;
            this.sendToCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendToCustomer.Location = new System.Drawing.Point(20, 105);
            this.sendToCustomer.Name = "sendToCustomer";
            this.sendToCustomer.Size = new System.Drawing.Size(119, 20);
            this.sendToCustomer.TabIndex = 7;
            this.sendToCustomer.Text = "Enviar a Cliente";
            // 
            // btnSendReminder
            // 
            this.btnSendReminder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendReminder.Location = new System.Drawing.Point(511, 282);
            this.btnSendReminder.Name = "btnSendReminder";
            this.btnSendReminder.Size = new System.Drawing.Size(169, 34);
            this.btnSendReminder.TabIndex = 8;
            this.btnSendReminder.Text = "Enviar Cartas";
            this.btnSendReminder.UseVisualStyleBackColor = true;
            this.btnSendReminder.Click += new System.EventHandler(this.btnSendReminder_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbEmailsEmpresa);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnSendReminder);
            this.groupBox1.Controls.Add(this.tbDaysBefore);
            this.groupBox1.Controls.Add(this.sendToCustomer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbutNo);
            this.groupBox1.Controls.Add(this.tbDaysAfter);
            this.groupBox1.Controls.Add(this.rbutYes);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Location = new System.Drawing.Point(30, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(686, 322);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recordatorio Cartera";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(294, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "correos separados por \";\"";
            // 
            // tbEmailsEmpresa
            // 
            this.tbEmailsEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEmailsEmpresa.Location = new System.Drawing.Point(289, 133);
            this.tbEmailsEmpresa.Name = "tbEmailsEmpresa";
            this.tbEmailsEmpresa.Size = new System.Drawing.Size(288, 26);
            this.tbEmailsEmpresa.TabIndex = 9;
            this.tbEmailsEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Emails notificación empresa";
            // 
            // frReminderPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 516);
            this.Controls.Add(this.groupBox1);
            this.Name = "frReminderPayment";
            this.Text = "frReminderPayment";
            this.Load += new System.EventHandler(this.frReminderPayment_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbDaysBefore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDaysAfter;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.RadioButton rbutYes;
        private System.Windows.Forms.RadioButton rbutNo;
        private System.Windows.Forms.Label sendToCustomer;
        private System.Windows.Forms.Button btnSendReminder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEmailsEmpresa;
        private System.Windows.Forms.Label label3;
    }
}