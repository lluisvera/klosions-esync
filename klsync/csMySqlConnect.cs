﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Windows.Forms;
using System.Data;
using System.Net;
using a3ERPActiveX;
using klsync.SQL;
using System.Data.SqlClient;

namespace klsync
{
    class csMySqlConnect
    {

        private MySqlConnection connection;
        private OdbcConnection Odbcconnection;

        // Constructor original, sin parámetros
        public csMySqlConnect()
        {

        }

        // Nuevo constructor que acepta una conexión existente (proveniente del Singleton)
        public csMySqlConnect(MySqlConnection conexion)
        {
            this.connection = conexion;
        }


        //Constructor
        public void DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {


        }


        public string getHostAddress(string Host)
        {
            string add = "";
            foreach (IPAddress address in Dns.GetHostAddresses(csGlobal.ServerPS))
            {
                try
                {
                    add = address.ToString();
                }
                catch (Exception)
                {
                }
            }

            return add;
        }

        

        public string conexionDestino()
        {
            //Formamos la cadena de conexión en función de los valores de las Variables Globales
            string connectionString = "";
            string puertoMySQL = "PORT=" + csGlobal.PortPS + ";";

            if (csGlobal.PortPS == "")
            {
                puertoMySQL = "PORT=3306;";
            }

            connectionString = "SERVER=" + csGlobal.ServerPS + ";" + puertoMySQL + "DATABASE=" + csGlobal.databasePS + ";" + "UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";
            //connectionString = "SERVER=" + csGlobal.ServerPS + ";" + "DATABASE=\"copilot-system_com_copishopdb\";UID=" + csGlobal.userPS + ";" + "PASSWORD=" + csGlobal.passwordPS + ";Allow Zero Datetime=True";

            return connectionString;
        }

        public string contarTabla(string tabla, string where = "")
        {            
            string query = "select count(*) from " + tabla + where;
            DataTable tabla2 = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(tabla2);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(tabla2);
            }


            BindingSource bsource = new BindingSource();
            bsource.DataSource = tabla2;
            this.CloseConnection();
            return tabla2.Rows[0].ItemArray[0].ToString();
        }


        //public object obtenerValorScalar(string consulta)
        //{
        //    object result = null;
        //    try
        //    {
        //        if (csGlobal.isODBCConnection)
        //        {
        //            using (OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop"))
        //            {
        //                Odbcconnection.Open();
        //                OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
        //                result = cmd.ExecuteScalar(); // Ejecuta la consulta y obtiene el valor escalar.
        //            }
        //        }
        //        else
        //        {
        //            using (MySqlConnection connection = new MySqlConnection(csGlobal.cadenaConexionPS))
        //            {
        //                connection.Open();
        //                MySqlCommand cmd = new MySqlCommand(consulta, connection);
        //                result = cmd.ExecuteScalar(); // Ejecuta la consulta y obtiene el valor escalar.
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Aquí puedes manejar el error, registrar logs, o simplemente devolver null.
        //        MessageBox.Show($"Error en obtenerValorScalar: {ex.Message}");
        //    }

        //    return result;
        //}


        public string obtenerDatoFromQuery(string query, bool conexionOpen = false)
        {
            try
            {
                DataTable tabla = new DataTable();
                if (csGlobal.isODBCConnection)
                {
                    if (!conexionOpen)
                    {
                        //string connStr = "Driver={MariaDB ODBC 3.1 Driver};Server=vl26220.dinaserver.com;Database=ps_shop;User=u_dismay;Password=GrhgO3373w*;Option=3;";
                        string connStr = "DSN=ps_shop";
                        Odbcconnection = new OdbcConnection(connStr);
                        Odbcconnection.Open();
                    }
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    OdbcDataAdapter myDA = new OdbcDataAdapter();
                    myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                    myDA.Fill(tabla); BindingSource bsource = new BindingSource();
                    bsource.DataSource = tabla;
                    if (!conexionOpen)
                    {
                        Odbcconnection.Close();
                    }
                }
                else
                {
                    if (!conexionOpen)
                    {
                        //csGlobal.cadenaConexionPS = "SERVER=vl26220.dinaserver.com;DATABASE=ps_shop;PORT=3306;userid=u_dismay;PASSWORD=GrhgO3373w*;Convert Zero Datetime=True;";
                        connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                        connection.Open();
                    }
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataAdapter myDA = new MySqlDataAdapter();
                    myDA.SelectCommand = new MySqlCommand(query, connection);
                    myDA.Fill(tabla);
                    BindingSource bsource = new BindingSource();
                    bsource.DataSource = tabla;
                    if (!conexionOpen)
                    {
                        connection.Close();
                    }
                }

                if (tabla.Rows.Count > 0)
                    return tabla.Rows[0].ItemArray[0].ToString();
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //return string.Empty;
                MessageBox.Show($"Error: {ex.Message}\nTipo: {ex.GetType()}\nStack Trace: {ex.StackTrace}");
                return string.Empty;
            }
        }


        public string obtenerDatoFromQueryA3ERP(string query)
        {
            try
            {
                // Crear una instancia del objeto Maestro proporcionado por A3ERP
                Maestro a3maestro = new Maestro();

                // Iniciar la conexión con la base de datos para el objeto "series", por ejemplo
                a3maestro.Iniciar("series");

                // Aplicar el filtro o ejecutar la consulta
                a3maestro.Filtro = query;
                a3maestro.Filtrado = true; // Activar el filtrado

                // Comprobar si se encontró algún resultado
                if (!a3maestro.EOF)
                {
                    // Obtener el valor que te interesa, por ejemplo "SERIE" o "NOMSERIE"
                    string valorResultado = a3maestro.get_AsString("SERIE");

                    // Limpiar el filtro
                    a3maestro.Filtrado = false;
                    a3maestro.Filtro = "";

                    // Finalizar el objeto Maestro
                    a3maestro.Acabar();

                    return valorResultado;
                }
                else
                {
                    // No se encontró ningún resultado
                    a3maestro.Filtrado = false;
                    a3maestro.Filtro = "";
                    a3maestro.Acabar();
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al ejecutar la consulta en A3ERP: {ex.Message}");
                return string.Empty;
            }
        }

        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                }
                else { 
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                }
              //  Insert();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show(ex.Message);
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection.Close();
                }
                else
                {
                    connection.Close();
                }
                return true;
            }
            catch (MySqlException)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void checkConnection(string Destino)
        {
            if (OpenConnection())
            {
                //MessageBox.Show("Conexión Válida");

            }
        }

        //Crear Tablas Auxiliares
        public void crearTablasAuxiliares(string sqlScript)
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    OdbcCommand cmd = new OdbcCommand(sqlScript, Odbcconnection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(sqlScript, connection);
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1050)
                {
                    //MessageBox.Show("Las Tablas Auxiliares ya están creadas");
                }
                else
                {
                    //MessageBox.Show(ex.Message);   
                }
            }
        }

        //Insert Facturas Generadas
        public void InsertFrasGeneradas(string[] frasGeneradas)
        {
            string[] fras = new string[3];
            try
            {
                if (csGlobal.isODBCConnection) {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    for (int i = 0; i < frasGeneradas.GetLength(0); i++)
                    {
                        fras = frasGeneradas[i].Split(';');
                        string query = "insert into " + csGlobal.databasePS + ".kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('" + fras[0] + "','" + fras[1] + "','" + fras[2] + "')";
                        OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    for (int i = 0; i < frasGeneradas.GetLength(0); i++)
                    {
                        fras = frasGeneradas[i].Split(';');
                        string query = "insert into " + csGlobal.databasePS + ".kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('" + fras[0] + "','" + fras[1] + "','" + fras[2] + "')";
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        cmd.ExecuteNonQuery();
                    }
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void sincronizarDocumentoManualmente(string numDoc)
        {
            try
            {
                string query = "";
                if (numDoc.Length>3)
                {
                    query = "insert into " + csGlobal.databasePS + ".kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('" + numDoc + "','999" + numDoc + "','" + DateTime.Now.ToString("yy-MM-dd") + "')";
                }
                else
                {
                    query = "insert into " + csGlobal.databasePS + ".kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('" + numDoc + "','99000" + numDoc + "','" + DateTime.Now.ToString("yy-MM-dd") + "')";
                }
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        //función que inserta el valor del documento creado en A3ERP, para llevar el control de los documentos traspasados
        public void insertarDocumentosA3Generados(string documento_PS, string documento_A3, string fecha_Sync)
        {
            string[] fras = new string[3];                    
            string query = "insert into " + csGlobal.databasePS + ".kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('" + documento_PS + "','" + documento_A3 + "','" + fecha_Sync + "')";

            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else { 
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void InsertDescuentosArticulos(string tabla, string campos, string valores)
        {
            string[] fras = new string[3];                
            string query = "insert into " + csGlobal.databasePS + tabla + campos + " values " + valores;

            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        public void borrar(string tabla, string campoRef, string filtro)
        {
            string query = "delete from " + tabla + " where " + campoRef + " in (" + filtro + ")";
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void borrar(string tabla, string filtro)
        {

            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    string query = "delete from " + tabla + " where " + filtro;
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                    Odbcconnection.Close();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    string query = "delete from " + tabla + " where " + filtro;
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void borrar(string tabla)
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    string query = "delete from " + tabla;
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                    Odbcconnection.Close();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    string query = "delete from " + tabla;
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void consultaBorrar(string consulta)
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(consulta, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void ejecutarConsulta(string consulta, bool connectionOpen=false)
        {
            try
            {
                if (csGlobal.isODBCConnection){
                    if (!connectionOpen)
                    {
                        Odbcconnection = new OdbcConnection("DSN=ps_shop");
                        Odbcconnection.Open();
                    }
                    OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
                    cmd.ExecuteNonQuery();

                    if (!connectionOpen)
                    {
                        Odbcconnection.Close();
                    }
                }
                else
                {
                    if (!connectionOpen)
                    {
                        connection = new MySqlConnection(conexionDestino());
                        connection.Open();
                    }
                    MySqlCommand cmd = new MySqlCommand(consulta, connection);
                    cmd.ExecuteNonQuery();

                    if (!connectionOpen)
                    {
                        connection.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        //Tablas de la Base de Datos
        public DataTable dbTablas()
        {
            string query = "SELECT TABLE_NAME FROM Information_Schema.Tables where Table_Type = 'BASE TABLE'";
            DataTable tabla = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(tabla);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();
                //open connection
                //if (this.OpenConnection() == true)
                //{
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(tabla);
            }
            BindingSource bsource = new BindingSource();
            bsource.DataSource = tabla;
            //close connection
            this.CloseConnection();
            return tabla;

        }

        public DataTable importarProductos()
        {
            string query = "SELECT id_product,reference FROM " + csGlobal.databasePS + ".ps_product";
            DataTable tabla = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(tabla);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(tabla);
            }
            this.CloseConnection();
            return tabla;

        }

        public DataTable cargarTabla(string consulta)
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    string connStr = "DSN=ps_shop";
                    Odbcconnection = new OdbcConnection(connStr);
                    Odbcconnection.Open();

                    OdbcDataAdapter myDA = new OdbcDataAdapter();

                    myDA.SelectCommand = new OdbcCommand(consulta, Odbcconnection);
                    DataTable tablaDatos = new DataTable();
                    myDA.Fill(tablaDatos);

                    Odbcconnection.Close();
                    return tablaDatos;
                }
                else
                {
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection.Open();

                    MySqlDataAdapter myDA = new MySqlDataAdapter();

                    myDA.SelectCommand = new MySqlCommand(consulta, connection);
                    DataTable tablaDatos = new DataTable();
                    myDA.Fill(tablaDatos);

                    connection.Close();
                    return tablaDatos;
                }
            }
            catch (Exception ex)
            {
                csUtilidades.log(ex.Message);
                return null;
            }
        }



        public DataTable dbCampos(string tabla)
        {
            string query = "SHOW COLUMNS FROM " + tabla;
            DataTable campos = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(campos);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(campos);
            }
            BindingSource bsource = new BindingSource();
            bsource.DataSource = campos;
            //close connection
            this.CloseConnection();
            return campos;
        }

        public DataTable dbDatos(string tabla)
        {
            string query = "Select * from  " + tabla;
            DataTable campos = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(campos);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();
                
                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(campos);
            }
            BindingSource bsource = new BindingSource();
            bsource.DataSource = campos;
            //close connection
            this.CloseConnection();
            return campos;
        }

        public DataTable dbDatos(string tabla, string camposSelect)
        {
            string query = "Select " + camposSelect + " from  " + tabla;
            DataTable campos = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(campos);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(campos);
            }
            BindingSource bsource = new BindingSource();
            bsource.DataSource = campos;
            //close connection
            this.CloseConnection();
            return campos;
        }

        public void insertItems(string script)
        {
            try
            {                
                string query = script;
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
                this.CloseConnection();
            }
            catch (Exception ex)
            {

                Program.guardarErrorFichero("QUERY: " + script + "\n" + ex.ToString() +", "+ ex.StackTrace);
            }
        }

        public int ObtenerIdInsertado(string script)
        {            
            string query = script;
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                int id = Convert.ToInt32(cmd.ExecuteScalar());
                this.CloseConnection();
                return id;
            }
            else
            {
                //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);
                int id = Convert.ToInt32(cmd.ExecuteScalar());            
                this.CloseConnection();
                return id;
            }

        }

        public int defaultLangPS()
        {
            int lastId = 0;
            try
            {
                csSqlScripts sqlScript = new csSqlScripts();

                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(sqlScript.selectDefaultLangPS(), Odbcconnection);
                    lastId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                }
                else
                {
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    //connection = new MySqlConnection(conexionDestino());
                    //connection = new MySqlConnection(conexionDestino("hola"));
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(sqlScript.selectDefaultLangPS(), connection);
                    lastId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                }
            }
            finally
            {
                this.CloseConnection();
            }
            return lastId;
        }

        //Función para obtener el último id de la tabla de imágenes de productos
        public int selectID(string tabla, string campoId)
        {
            int lastId = 0;                    
            string query = "select max(" + campoId + ") from " + tabla;
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    //Verifico si hay algún artículo creado o no
                    if (cmd.ExecuteScalar() is DBNull)
                    {
                        lastId = 0;
                    }
                    else
                    {
                        lastId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    }
                }
                else
                {
                    //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Verifico si hay algún artículo creado o no
                    if (cmd.ExecuteScalar() is DBNull)
                    {
                        lastId = 0;
                    }
                    else
                    {
                        lastId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    }
                }
            }
            finally
            {
                this.CloseConnection();

            }
            return lastId;
        }

        public int obtenerIdImagen(string reference, bool cover = false)
        {
            int IdImage = 0;
            string query = "Select ps_image.id_image From ps_product Inner Join ps_image On ps_product.id_product = ps_image.id_product where reference='" + reference + "'";
            if (cover)
            {
                query = "Select ps_image.id_image From ps_product Inner Join ps_image On ps_product.id_product = ps_image.id_product where reference='" + reference + "' and cover=1";
            }
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    //Verifico si hay algún artículo creado o no

                    if (cmd.ExecuteScalar() is DBNull)
                    {
                        IdImage = 0;
                    }
                    else
                    {
                        try
                        {
                            IdImage = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        }
                        catch (Exception e)
                        { }
                    }
                }
                else
                {
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    //connection = new MySqlConnection(conexionDestino("hola"));
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Verifico si hay algún artículo creado o no

                    if (cmd.ExecuteScalar() is DBNull)
                    {
                        IdImage = 0;
                    }
                    else
                    {
                        try
                        {
                            IdImage = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        }
                        catch (Exception e)
                        { }
                    }
                }
                //lastId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            }
            finally
            {
                this.CloseConnection();

            }
            return IdImage;

        }

        public int codArticulo(string tabla, string campofiltro)
        {
            int idproduct = 0;
            try
            {                
                string query = "select id_product from " + tabla + " where reference='" + MySqlHelper.EscapeString(campofiltro) + "'";
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        string valor = cmd.ExecuteScalar().ToString();
                        idproduct = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    }
                }
                else
                {
                    //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        string valor = cmd.ExecuteScalar().ToString();
                        idproduct = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    }
                }
            }
            finally
            {
                this.CloseConnection();
            }

            return idproduct;

        }

        public bool existeArticulo(string id_articulo)
        {
            bool existe = false;
            //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
            if(csGlobal.isODBCConnection)
            {
                string connStr = "DSN=ps_shop";
                Odbcconnection = new OdbcConnection(connStr);
                Odbcconnection.Open();

                try
                {
                    if (id_articulo != "" && id_articulo != "0")
                    {
                        string condicionWhere = " where id_product= " + id_articulo;

                        string query = "Select ps_product.id_product, ps_product.reference From  ps_product " + condicionWhere;

                        OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                        if (cmd.ExecuteScalar() != null)
                        {
                            existe = true;
                        }
                    }
                }
                finally
                {
                    Odbcconnection.Close();
                }
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                try
                {
                    if (id_articulo != "" && id_articulo != "0")
                    {
                        string condicionWhere = " where id_product= " + id_articulo;

                        string query = "Select ps_product.id_product, ps_product.reference From  ps_product " + condicionWhere;

                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        if (cmd.ExecuteScalar() != null)
                        {
                            existe = true;
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
           
            return existe;

        }

        public bool existeRegistro(string tabla, string filtro)
        {
            bool existe = false;
            //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
            string query = "Select Count(ps_category_product.id_product) From " + tabla + " where " + filtro;

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();
                try
                {
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt16(cmd.ExecuteScalar().ToString());
                        if (valor > 1)
                        {
                            existe = true;
                        }
                    }
                }
                finally
                {
                    Odbcconnection.Close();
                }
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();
                try
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt16(cmd.ExecuteScalar().ToString());
                        if (valor > 1)
                        {
                            existe = true;
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            return existe;
        }

        public bool existeCombinacionTallayColor(string id_articulo, string atributoH, string atributoV)
        {
            bool existe = false;
            try
            {
                string query = "Select " +
                                  " count(   ps_product_attribute.id_product_attribute) " +
                                  " From " +
                                  " ps_product_attribute Inner Join " +
                                  " ps_product_attribute_combination On ps_product_attribute.id_product_attribute = ps_product_attribute_combination.id_product_attribute " +
                                  " Where " +
                                  " ps_product_attribute.id_product =" + id_articulo + " And " +
                                  " (ps_product_attribute_combination.id_attribute = " + atributoH + " Or " +
                                  " ps_product_attribute_combination.id_attribute = " + atributoV + ") " +
                                  " Group By " +
                                  " ps_product_attribute.id_product_attribute, ps_product_attribute.id_product " +
                                  " having count(   ps_product_attribute.id_product_attribute)=2 ";

                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        existe = true;
                    }
                }
                else
                {
                    //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        existe = true;
                    }
                }
            }
            finally
            {
                this.CloseConnection();
            }

            return existe;

        }

        public bool existeCampo(string consulta)
        {
            bool existe = false;

           try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar());
                        if (valor > 0)
                        {
                            existe = true;
                        }
                    }
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(consulta, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar());
                        if (valor > 0)
                        {
                            existe = true;
                        }
                    }
                }
            }
            finally
            {
                this.CloseConnection();
            }

            return existe;
        }

        public string obtenerValorNumericoQuery(string consulta)
        {
            string  valorQuery = "";
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar());
                        if (valor > 0)
                        {
                            valorQuery = valor.ToString();
                        }
                    }
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(consulta, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar());
                        if (valor > 0)
                        {
                            valorQuery = valor.ToString();
                        }
                    }
                }
            }
            finally
            {
                this.CloseConnection();
            }

            return valorQuery;
        
        
        }

        public bool existeTabla(string tabla)
        {
            bool existe = false;
            if (csGlobal.isODBCConnection)
            {
                string connStr = "DSN=ps_shop";
                Odbcconnection = new OdbcConnection(connStr);
                Odbcconnection.Open();

                try
                {

                    string query = "select count(*) from " + tabla;

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        if (valor > 0)
                        {
                            existe = true;
                        }
                    }
                }
                finally
                {
                    Odbcconnection.Close();
                }
            }
            else
            {
                //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                connection.Open();

                try
                {

                    string query = "select count(*) from " + tabla;

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        if (valor > 0)
                        {
                            existe = true;
                        }
                    }

                }
                finally
                {
                    connection.Close();
                }
            }
            return existe;
        }


        //public void actualizaStock(string script, bool conexionAbierta = false)
        //{
        //    try
        //    {
        //        if (csGlobal.isODBCConnection)
        //        {
        //            Odbcconnection = new OdbcConnection("DSN=ps_shop");
        //            if (!conexionAbierta) Odbcconnection.Open();
        //            if (Odbcconnection.State == ConnectionState.Closed)
        //            {
        //                Odbcconnection.Open();
        //            }

        //            string query = script;
        //            OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);

        //            cmd.ExecuteNonQuery();
        //            if (!conexionAbierta) Odbcconnection.Close();
        //        }
        //        else
        //        {
        //            connection = new MySqlConnection(csGlobal.cadenaConexionPS);
        //            if (!conexionAbierta) connection.Open();

        //            if (connection.State == ConnectionState.Closed)
        //            {
        //                connection.Open();
        //            }

        //            string query = script;
        //            MySqlCommand cmd = new MySqlCommand(query, connection);

        //            cmd.ExecuteNonQuery();
        //            if (!conexionAbierta) connection.Close();
        //        }
        //    }
        //    catch (MySqlException ex)
        //    {
        //        if (csGlobal.modoManual)
        //        {
        //            MessageBox.Show(ex.Message);
        //        }
        //    }
        //    finally
        //    {
        //        if (csGlobal.isODBCConnection)
        //        {
        //            Odbcconnection.Close();
        //        }
        //        else
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public void actualizaStock(string script, bool conexionAbierta = false)
        {
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    using (OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop"))
                    {
                        if (!conexionAbierta) Odbcconnection.Open();

                        string query = script;
                        using (OdbcCommand cmd = new OdbcCommand(query, Odbcconnection))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    using (MySqlConnection connection = new MySqlConnection(csGlobal.cadenaConexionPS))
                    {
                        if (!conexionAbierta) connection.Open();

                        string query = script;
                        using (MySqlCommand cmd = new MySqlCommand(query, connection))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void actualizarGenerica(string script, bool conexionAbierta=false, MySqlConnection connection=null)
        {
            try
            {
                string query = script;

                if (csGlobal.isODBCConnection)
                {
                    if (!conexionAbierta)
                    {
                        Odbcconnection = new OdbcConnection("DSN=ps_shop");
                        Odbcconnection.Open();
                    }
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (!conexionAbierta)
                    {
                        connection = new MySqlConnection(conexionDestino());
                        connection.Open();
                    }
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }                    
                if (!conexionAbierta)
                {
                    this.CloseConnection();
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void Insert()
        {
            string query = "insert into invenio2.kls_invoice_erp (invoice_number,invoice_number_erp,date_sync) values ('33','44','2013-1-3')";

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                cmd.ExecuteNonQuery();
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Execute command
                cmd.ExecuteNonQuery();
            }
            //close connection
            this.CloseConnection();
            //}
        }

        public void actualizarIdClientePS(string idClientePS, string idClienteA3)
        {
            string query = "update " + csGlobal.databasePS + ".ps_customer set kls_a3erp_id=" + idClienteA3 + " where ps_customer.id_customer=" + idClientePS;

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                cmd.ExecuteNonQuery();
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Execute command
                cmd.ExecuteNonQuery();
            }
            //close connection
            this.CloseConnection();
            //}
        }

        public void actualizarPreciosImpuestosPS(string articulo, string price, string taxGroup)
        {
            string query = "";
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set price=" + price + ", id_tax_rules_group=" + csGlobal.id_tax_group + " where id_product=" + articulo;
                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                cmd.ExecuteNonQuery();

                query = "update " + csGlobal.databasePS + ".ps_product_shop set price=" + price + ", id_tax_rules_group=" + csGlobal.id_tax_group + " where id_product=" + articulo;
                OdbcCommand cmd2 = new OdbcCommand(query, Odbcconnection);
                cmd2.ExecuteNonQuery();
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set price=" + price + ", id_tax_rules_group=" + csGlobal.id_tax_group + " where id_product=" + articulo;
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();

                query = "update " + csGlobal.databasePS + ".ps_product_shop set price=" + price + ", id_tax_rules_group=" + csGlobal.id_tax_group + " where id_product=" + articulo;
                MySqlCommand cmd2 = new MySqlCommand(query, connection);
                cmd2.ExecuteNonQuery();
            }

            //close connection
            this.CloseConnection();
            //}
        }
        public void actualizarPreciosImpuestosAtributos(DataTable dt)
        {
            //Acabar
            csMySqlConnect mysql = new csMySqlConnect();

            string idAtributo = "";
            string update = "";
            string select = "";
            string referencia = "";
            string precio = "";

            try
            {
                //TODO:MIRAR CONEXION FALLA Y NO ACTUALIZA TARIFA
                mysql.OpenConnection();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    referencia = dt.Rows[i]["REFERENCIA"].ToString();
                    precio = dt.Rows[i]["PRECIO"].ToString();

                    update = "update ps_product_attribute set price= " + precio + " where reference= " + referencia ;
                    mysql.ejecutarConsulta(update,true);

                    select = "select id_product_attribute from ps_product_attribute where reference= " + dt.Rows[i]["REFERENCIA"].ToString();

                    idAtributo = mysql.obtenerDatoFromQuery(select,true);
                    update = "update ps_product_attribute_shop set price= " + precio + " where id_product_attribute=" + idAtributo;

                    mysql.ejecutarConsulta(update,true);

                }

                mysql.CloseConnection();

                
            }
            catch (Exception e) {


            }
            
        }

        public void actualizarEstadoProductosPS(bool Activar, string Articulo)
        {

            string estadoProducto = "0";
            if (Activar)
            {
                estadoProducto = "1";
            }
            else
            {
                estadoProducto = "0";
            }

            string query = "";

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set active=" + estadoProducto + " where id_product=" + Articulo;
                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                cmd.ExecuteNonQuery();

                query = "update " + csGlobal.databasePS + ".ps_product_shop set active=" + estadoProducto + " where id_product=" + Articulo;
                OdbcCommand cmd2 = new OdbcCommand(query, Odbcconnection);
                cmd2.ExecuteNonQuery();
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set active=" + estadoProducto + " where id_product=" + Articulo;
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();

                query = "update " + csGlobal.databasePS + ".ps_product_shop set active=" + estadoProducto + " where id_product=" + Articulo;
                MySqlCommand cmd2 = new MySqlCommand(query, connection);
                cmd2.ExecuteNonQuery();
            }

            //close connection
            this.CloseConnection();
            //}
        }

        public void asignarMarcaProducto(string marca, string articulo)
        {

            string query = "";
            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set id_manufacturer=" + marca + " where id_product=" + articulo;
                OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
            else { 
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                query = "update " + csGlobal.databasePS + ".ps_product set id_manufacturer=" + marca + " where id_product=" + articulo;
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
        }

        public string obtenerVersionPS()
        {
            string versionPS = "";
            try
            {
                if (csGlobal.isODBCConnection) {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    //string query = "SELECT value from " + csGlobal.databasePS + ".ps_configuration where name='PS_VERSION_DB'";
                    string query = "SELECT value from " + csGlobal.databasePS + ".ps_configuration where name='PS_VERSION_DB'";
                    OdbcDataAdapter myDA = new OdbcDataAdapter();
                    myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                    DataTable dt = new DataTable();
                    myDA.Fill(dt);
                    versionPS = dt.Rows[0].ItemArray[0].ToString();
                } else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    //string query = "SELECT value from " + csGlobal.databasePS + ".ps_configuration where name='PS_VERSION_DB'";
                    string query = "SELECT value from " + csGlobal.databasePS + ".ps_configuration where name='PS_VERSION_DB'";
                    MySqlDataAdapter myDA = new MySqlDataAdapter();
                    myDA.SelectCommand = new MySqlCommand(query, connection);
                    DataTable dt = new DataTable();
                    myDA.Fill(dt);
                    versionPS = dt.Rows[0].ItemArray[0].ToString();
                }

            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return versionPS;
        }



        public DataTable obtenerDatosPS(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcDataAdapter myDA = new OdbcDataAdapter();
                    myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                    myDA.Fill(dt);
                }
                else
                {
                    connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                    connection.Open();

                    MySqlDataAdapter myDA = new MySqlDataAdapter();
                    myDA.SelectCommand = new MySqlCommand(query, connection);
                    myDA.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return dt;

        }

        //select y devuelve un array
        public string[] select()
        {
            int i = 0;
            string query = "SELECT payment from " + csGlobal.databasePS + ".ps_orders group by payment";
            DataTable dt = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(dt);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(dt);
            }

            string[] formasPago = new string[dt.Rows.Count];
            foreach (DataRow fila in dt.Rows)
            {
                formasPago[i] = fila.ItemArray[0].ToString();
                i++;
            }

            return formasPago;
        }

        public string[] select(string scriptSelect)
        {
            int i = 0;
            DataTable dt = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(scriptSelect, Odbcconnection);
                myDA.Fill(dt);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(scriptSelect, connection);
                myDA.Fill(dt);
            }

            string[] formasPago = new string[dt.Rows.Count];
            foreach (DataRow fila in dt.Rows)
            {
                formasPago[i] = fila.ItemArray[0].ToString();
                i++;
            }

            return formasPago;
        }

        //Utilizar los paréntesis para dentro de los parámetros "campos y values"
        //public void InsertValoresEnTabla(string tabla, string campos, string values, string condicionWhere, bool escape = true, bool showErrors = true)
        //{
        //    char[] charsToTrim = { ',', '.', ' ' };
        //    string query = "";
        //    try
        //    {

        //        values = values.TrimEnd(charsToTrim);
        //        query = "insert into " + tabla + " " + campos + " values " + values + ";";

        //        //query = query + "; select last_insert_id();";
        //        if (csGlobal.isODBCConnection)
        //        {
        //            Odbcconnection = new OdbcConnection("DSN=ps_shop");
        //            Odbcconnection.Open();

        //            OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
        //            //21/01/2017 Cambio cmd.executenonquery por executescalar para que me devuelva el id insertado
        //            //Execute command
        //            cmd.ExecuteNonQuery();
        //            //cmd.ExecuteScalar();
        //           // idInserted = Convert.ToInt32(cmd.ExecuteScalar());
        //            //int)cmd.LastInsertedId;

        //        }
        //        else
        //        {
        //            connection = new MySqlConnection(conexionDestino());
        //            connection.Open();

        //            //query = MySqlHelper.EscapeString(query);
        //            MySqlCommand cmd = new MySqlCommand(query, connection);
        //            //21/01/2017 Cambio cmd.executenonquery por executescalar para que me devuelva el id insertado
        //            //Execute command
        //            cmd.ExecuteNonQuery();
        //            //cmd.ExecuteScalar();
        //            //idInserted = (int)cmd.LastInsertedId;
        //        }
        //        //if(csGlobal.isODBCConnection){
        //        //    Odbcconnection = new OdbcConnection(conexionDestino());
        //        //    Odbcconnection.Open();
        //        //    values = values.TrimEnd(charsToTrim);

        //        //    query = "insert into " + tabla + " " + campos + " values " + values;
        //        //    //query = MySqlHelper.EscapeString(query);
        //        //    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
        //        //    //Execute command
        //        //    cmd.ExecuteNonQuery();
        //        //}
        //        //else
        //        //{
        //        //    connection = new MySqlConnection(conexionDestino());
        //        //    connection.Open();
        //        //    values = values.TrimEnd(charsToTrim);

        //        //    query = "insert into " + tabla + " " + campos + " values " + values;
        //        //    //query = MySqlHelper.EscapeString(query);
        //        //    MySqlCommand cmd = new MySqlCommand(query, connection);
        //        //    //Execute command
        //        //    cmd.ExecuteNonQuery();
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        if (showErrors)
        //            Program.guardarErrorFichero("Query: " + query + "\n\n\n\n\n\n" + ex.ToString());
        //        if (csGlobal.modoManual)
        //            MessageBox.Show("ERROR EN EL INSERT DE LA QUERY:\n" + "Query: " + query + "\n\n\n\n\n\n" + ex.ToString());
                        
        //    }
        //    finally
        //    {
        //        //close connection
        //        this.CloseConnection();

        //        //}
        //    }
        //}


        public void InsertValoresEnTabla(string tabla, string campos, string values, string condicionWhere, bool escape = true, bool showErrors = true)
        {
            char[] charsToTrim = { ',', '.', ' ' };
            string query = "";
            try
            {
                // Limpiar valores y preparar la query
                values = values.TrimEnd(charsToTrim);
                query = "insert into " + tabla + " " + campos + " values " + values + ";";

                csUtilidades.guardarProgresoFichero("Ejecutando inserción: " + query);

                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }

                csUtilidades.guardarProgresoFichero("Inserción realizada con éxito.");
            }
            catch (Exception ex)
            {
                if (showErrors)
                    Program.guardarErrorFichero("Query: " + query + "\n\n" + ex.ToString());
                csUtilidades.guardarProgresoFichero("Error en la inserción: " + ex.Message);
                if (csGlobal.modoManual)
                    MessageBox.Show("ERROR EN EL INSERT DE LA QUERY:\n" + "Query: " + query + "\n\n\n\n\n\n" + ex.ToString());

            }
            finally
            {
                this.CloseConnection();
            }
        }




        public void InsertValoresEnTabla(string tabla, string values, bool showErrors = true)
        {

            string query = "";
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection(conexionDestino());
                    Odbcconnection.Open();

                    query = "insert into " + tabla + " values " + values;
                    //query = MySqlHelper.EscapeString(query);
                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    query = "insert into " + tabla + " values " + values;
                    //query = MySqlHelper.EscapeString(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                if (showErrors)
                    Program.guardarErrorFichero("Query: " + query + "\n\n\n\n\n\n" + ex.ToString());
            }
            finally
            {
                //close connection
                this.CloseConnection();

                //}
            }
        }


        //21/01/2017 cambio de void a string para que me devuelva el código generado
        public int InsertValoresEnTabla(string tabla, string fields, string values, bool showErrors = true)
        {
            string query = "";
            string keyInserted = "";
            int idInserted = 0;
            try
            {
                query = "insert into " + tabla + " " + fields +" values " + values;
                query = query + "; select last_insert_id();";

                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    //21/01/2017 Cambio cmd.executenonquery por executescalar para que me devuelva el id insertado
                    //Execute command
                    cmd.ExecuteNonQuery();
                    //cmd.ExecuteScalar();
                    idInserted = Convert.ToInt32(cmd.ExecuteScalar());
                    //int)cmd.LastInsertedId;
                    
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    //query = MySqlHelper.EscapeString(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //21/01/2017 Cambio cmd.executenonquery por executescalar para que me devuelva el id insertado
                    //Execute command
                    cmd.ExecuteNonQuery();
                    //cmd.ExecuteScalar();
                    idInserted = (int)cmd.LastInsertedId;
                }
                return idInserted;
            }
            catch (Exception ex)
            {
                if (showErrors)
                    Program.guardarErrorFichero("Query: " + query + "\n\n\n\n\n\n" + ex.ToString());
                return 0;

            }
            finally
            {
                //close connection
                this.CloseConnection();
                //}
            }
        }

        public int ImportBulk(string path, string tabla)
        {
            try
            {
                string e = "LOAD DATA INFILE '" + path + "' INTO TABLE " + tabla + " FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'";

                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(e, Odbcconnection);
                    cmd.ExecuteNonQuery();
                    return Convert.ToInt32(cmd);
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(e, connection);
                    cmd.ExecuteNonQuery();
                    return Convert.ToInt32(cmd);
                }
            }
            catch
            {
                return -1;
            }
        }

        public void actualizarValoresEnTabla(string tabla, string campos, string values, string campoWhere, string idWhere)
        {                    
            string query = "update " + tabla + " set " + campos + " = '" + values + "' where " + campoWhere + " = " + idWhere;

            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                this.CloseConnection();
            }

        }

        public void actualizarValoresEnTabla(string tabla, string camposValues, string campoWhere, string idWhere)
        {                    
            string query = "update " + tabla + " set " + camposValues + " where " + campoWhere + " = " + idWhere;
            if (csGlobal.isODBCConnection)
            {
                try
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    //close connection
                    this.CloseConnection();
                    //}
                }
            }
            else
            {
                try
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    //close connection
                    this.CloseConnection();
                    //}
                }
            }

        }

        public void actualizarValoresEnTabla(string tabla, string campo, string values)
        {
            string query = "update " + tabla + " set " + campo + " = " + values;
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Execute command
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                //close connection
                this.CloseConnection();
                //}
            }
        }

        public void actualizarValoresEnTablaMySQL(string tabla, string campos, string values, string condicion)
        {                        
            string query = "update " + tabla + " set " + campos + " = " + values + " where " + condicion;

            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                this.CloseConnection();
            }
        }

        public void actualizarDocumentosTraspasadosRPST(DataTable tablaDocs)
        {
            string documento = "";
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();
                }


                for (int i = 0; i < tablaDocs.Rows.Count; i++)
                {
                    documento = tablaDocs.Rows[i].ItemArray[0].ToString();
                    string query = "update actividades set codExternoActividad='" + documento + "' where idActividad=" + documento;
                    if (csGlobal.isODBCConnection)
                    {
                        OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                        //Execute command
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        //Execute command
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {
                //close connection
                this.CloseConnection();
                //}
            }

        }

        public DataTable selectClientesEnlazadosRPST(string entidad)
        {
            string query = "Select clientes.idEntidadCli, clientes.idCli, clientes.nomCli,clientes.codExternoCli " +
                            " From clientes " +
                            " Where " +
                            " clientes.idEntidadCli = 101 And clientes.codExternoCli <> \"\"";
            DataTable dt = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(dt);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(dt);
            }
            return dt;
        }

        public DataTable obtenerIdArticulosPS()
        {
            int i = 0;            
            string query = "Select reference, id_product from ps_product";            
            DataTable dt = new DataTable();
            if (csGlobal.isODBCConnection)
            {
                OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(dt);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(dt);
            }

            return dt;
        }

        public DataTable selectDatosClientesAltaA3(string id_cliente)
        {
            int i = 0;
            string query = "select ps_address.company, ps_customer.firstname, ps_customer.lastname, email,id_address_delivery," +
                        " ps_address.address1, ps_address.postcode, ps_address.city , ps_address.phone , ps_address.phone_mobile, ps_address.vat_number " +
                        " from ps_orders inner join ps_customer on ps_orders.id_customer=ps_customer.id_customer " +
                        " left join ps_address on ps_orders.id_address_delivery=ps_address.id_address where ps_orders.id_customer=" + id_cliente;
            DataTable dt = new DataTable();

            if (csGlobal.isODBCConnection)
            {
                OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                Odbcconnection.Open();

                OdbcDataAdapter myDA = new OdbcDataAdapter();
                myDA.SelectCommand = new OdbcCommand(query, Odbcconnection);
                myDA.Fill(dt);
            }
            else
            {
                connection = new MySqlConnection(conexionDestino());
                connection.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter();
                myDA.SelectCommand = new MySqlCommand(query, connection);
                myDA.Fill(dt);
            }

            return dt;
        }

        public bool comprobarSiElArticuloTienePortada(int articulo)
        {
            bool portada = false;
            //connection = new MySqlConnection(csGlobal.cadenaConexionPS);
            string query = "select count(*) " +
                           "from ps_image " +
                           "where id_product = " + articulo.ToString() + " and cover = 1";
            try
            {

                if (csGlobal.isODBCConnection)
                {
                    OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcCommand cmd = new OdbcCommand(query, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt16(cmd.ExecuteScalar().ToString());
                        if (valor > 0)
                        {
                            portada = true;
                        }
                    }
                }
                else
                {
                    connection = new MySqlConnection(conexionDestino());
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        int valor = Convert.ToInt16(cmd.ExecuteScalar().ToString());
                        if (valor > 0)
                        {
                            portada = true;
                        }
                    }
                }
            }
            finally
            {
                this.CloseConnection();
            }

            return portada;
        }

        public bool comprobarConsulta(string consulta)
        {            
            bool check = false;
            if (csGlobal.isODBCConnection)
            {
                try
                {
                    Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();
                    OdbcCommand cmd = new OdbcCommand(consulta, Odbcconnection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        check = true;
                    }
                }
                finally
                {
                    Odbcconnection.Close();                   
                }
            }
            else
            {
                //connection = new MySqlConnection(csGlobal.cadenaConexionPS+ ";charset=utf8mb4;");
                connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                connection.Open();
                try
                {
                    MySqlCommand cmd = new MySqlCommand(consulta, connection);
                    if (cmd.ExecuteScalar() != null)
                    {
                        check = true;
                    }
                }
                finally
                {
                    this.CloseConnection();
                }


                //try
                //{
                //    string consulta2 = "SELECT COUNT(*) FROM ps_product WHERE kls_a3erp_fam_id IS NOT NULL";
                //    MySqlCommand cmd = new MySqlCommand(consulta2, connection);
                //    var result = cmd.ExecuteScalar(); // Ejecuta la consulta

                //    if (result != null)
                //    {
                //        check = true;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine("Error en ExecuteScalar: " + ex.Message);
                //    Console.WriteLine("Detalles: " + ex.StackTrace);
                //}
                //finally
                //{
                //    this.CloseConnection();
                //}
            }
            return check;
        }


        public bool probarConexionRepasatOPresta()
        {
            try
            {
                using (var conexion = new MySqlConnection(csGlobal.cadenaConexionPS))
                {
                    conexion.Open();

                    string query = (csGlobal.modeAp.ToUpper() == "REPASAT" ? "SELECT idCli FROM clientes ORDER BY idCli DESC LIMIT 1" : "SELECT id_customer FROM ps_customer ORDER BY id_customer DESC LIMIT 1");

                    using (var comando = new MySqlCommand(query, conexion))
                    {
                        using (var reader = comando.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                //string idCli = reader[0].ToString();
                                //MessageBox.Show($"Conexión exitosa. Datos obtenidos:\nIdCli: {idCli}");
                                return true;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }



        //Update statement
        //public void Update()
        //{
        //    string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

        //    //Open connection
        //    if (this.OpenConnection() == true)
        //    {
        //        //create mysql command
        //        MySqlCommand cmd = new MySqlCommand();
        //        //Assign the query using CommandText
        //        cmd.CommandText = query;
        //        //Assign the connection using Connection
        //        cmd.Connection = connection;

        //        //Execute query
        //        cmd.ExecuteNonQuery();

        //        //close connection
        //        this.CloseConnection();
        //    }
        //}

        //Delete statement
        //public void Delete()
        //{
        //    string query = "DELETE FROM tableinfo WHERE name='John Smith'";

        //    if (this.OpenConnection() == true)
        //    {
        //        MySqlCommand cmd = new MySqlCommand(query, connection);
        //        cmd.ExecuteNonQuery();
        //        this.CloseConnection();
        //    }
        //}
        //Select statement
        //public List<string>[] Select()
        //{
        //    string query = "SELECT TABLE_NAME FROM Information_Schema.Tables where Table_Type = 'BASE TABLE'";

        //    //Create a list to store the result
        //    List<string>[] list = new List<string>[3];
        //    list[0] = new List<string>();
        //    list[1] = new List<string>();
        //    list[2] = new List<string>();

        //    //Open connection
        //    if (this.OpenConnection() == true)
        //    {
        //        //Create Command
        //        MySqlCommand cmd = new MySqlCommand(query, connection);
        //        //Create a data reader and Execute the command
        //        MySqlDataReader dataReader = cmd.ExecuteReader();

        //        //Read the data and store them in the list
        //        while (dataReader.Read())
        //        {
        //            list[0].Add(dataReader["id"] + "");
        //            list[1].Add(dataReader["name"] + "");
        //            list[2].Add(dataReader["age"] + "");
        //        }

        //        //close Data Reader
        //        dataReader.Close();

        //        //close Connection
        //        this.CloseConnection();

        //        //return list to be displayed
        //        return list;
        //    }
        //    else
        //    {
        //        return list;
        //    }
        //}

        //Count statement
        //public int Count()
        //{
        //    string query = "SELECT Count(*) FROM tableinfo";
        //    int Count = -1;

        //    //Open Connection
        //    if (this.OpenConnection() == true)
        //    {
        //        //Create Mysql Command
        //        MySqlCommand cmd = new MySqlCommand(query, connection);

        //        //ExecuteScalar will return one value
        //        Count = int.Parse(cmd.ExecuteScalar() + "");

        //        //close Connection
        //        this.CloseConnection();

        //        return Count;
        //    }
        //    else
        //    {
        //        return Count;
        //    }
        //}
    }
}