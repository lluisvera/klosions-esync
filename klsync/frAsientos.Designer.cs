﻿namespace klsync
{
    partial class frAsientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripAsientos = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCargarAsientos = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCambiarCuenta = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelpAsientos = new System.Windows.Forms.ToolStripButton();
            this.dgvAsientos = new System.Windows.Forms.DataGridView();
            this.toolStripAsientos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsientos)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripAsientos
            // 
            this.toolStripAsientos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripAsientos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCargarAsientos,
            this.toolStripButtonCambiarCuenta,
            this.toolStripButtonHelpAsientos});
            this.toolStripAsientos.Location = new System.Drawing.Point(0, 0);
            this.toolStripAsientos.Name = "toolStripAsientos";
            this.toolStripAsientos.Padding = new System.Windows.Forms.Padding(0, 5, 1, 0);
            this.toolStripAsientos.Size = new System.Drawing.Size(1011, 65);
            this.toolStripAsientos.TabIndex = 1;
            this.toolStripAsientos.Text = "toolStrip1";
            // 
            // toolStripButtonCargarAsientos
            // 
            this.toolStripButtonCargarAsientos.Image = global::klsync.Properties.Resources.armchair;
            this.toolStripButtonCargarAsientos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCargarAsientos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCargarAsientos.Name = "toolStripButtonCargarAsientos";
            this.toolStripButtonCargarAsientos.Size = new System.Drawing.Size(131, 57);
            this.toolStripButtonCargarAsientos.Text = "1. Cargar asiento";
            this.toolStripButtonCargarAsientos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonCargarAsientos.Click += new System.EventHandler(this.toolStripButtonCargarAsientos_Click);
            // 
            // toolStripButtonCambiarCuenta
            // 
            this.toolStripButtonCambiarCuenta.Image = global::klsync.Properties.Resources.change;
            this.toolStripButtonCambiarCuenta.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCambiarCuenta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCambiarCuenta.Name = "toolStripButtonCambiarCuenta";
            this.toolStripButtonCambiarCuenta.Size = new System.Drawing.Size(139, 57);
            this.toolStripButtonCambiarCuenta.Text = "2. Cambiar cuenta";
            this.toolStripButtonCambiarCuenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonCambiarCuenta.Click += new System.EventHandler(this.toolStripButtonCambiarCuenta_Click);
            // 
            // toolStripButtonHelpAsientos
            // 
            this.toolStripButtonHelpAsientos.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonHelpAsientos.Image = global::klsync.Properties.Resources.question_mark;
            this.toolStripButtonHelpAsientos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonHelpAsientos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelpAsientos.Name = "toolStripButtonHelpAsientos";
            this.toolStripButtonHelpAsientos.Size = new System.Drawing.Size(58, 57);
            this.toolStripButtonHelpAsientos.Text = "Ayuda";
            this.toolStripButtonHelpAsientos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dgvAsientos
            // 
            this.dgvAsientos.AllowUserToAddRows = false;
            this.dgvAsientos.AllowUserToDeleteRows = false;
            this.dgvAsientos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAsientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAsientos.Location = new System.Drawing.Point(12, 74);
            this.dgvAsientos.Name = "dgvAsientos";
            this.dgvAsientos.ReadOnly = true;
            this.dgvAsientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAsientos.Size = new System.Drawing.Size(987, 503);
            this.dgvAsientos.TabIndex = 2;
            // 
            // frAsientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 603);
            this.Controls.Add(this.dgvAsientos);
            this.Controls.Add(this.toolStripAsientos);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frAsientos";
            this.Text = "Asientos contables";
            this.toolStripAsientos.ResumeLayout(false);
            this.toolStripAsientos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAsientos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripAsientos;
        private System.Windows.Forms.ToolStripButton toolStripButtonCargarAsientos;
        private System.Windows.Forms.DataGridView dgvAsientos;
        private System.Windows.Forms.ToolStripButton toolStripButtonCambiarCuenta;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelpAsientos;
    }
}