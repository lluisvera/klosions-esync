﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Drawing;
using System.Threading;
using RestSharp;
using System.Xml.Linq;
using System.Diagnostics;
using a3ERPActiveX;
using klsync.CronJobs;
using System.IO;
using klsync.CronJobs.Unificacion;
using System.Text;

namespace klsync
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            csGlobal.perfilAdminEsync = false;
            csGlobal.modoManual = true;

            if (args.Count() > 0)
            {
                csGlobal.modoManual = false;
                if (args[1].ToString() == "min")
                {
                    if (cargarConfig(args))
                    {
                        NotifyIcon trayIcon = new NotifyIcon();
                        trayIcon.Text = "Klosions Esync";
                        trayIcon.Icon = new Icon("esync.ico", 40, 40);

                        ContextMenu trayMenu = new ContextMenu();

                        trayMenu.MenuItems.Add("Sincronizar documentos", pedidos_Click);
                        trayMenu.MenuItems.Add("Stock", stock_Click);
                        trayMenu.MenuItems.Add("-");
                        trayMenu.MenuItems.Add("Salir", salir_Click);

                        trayIcon.ContextMenu = trayMenu;
                        trayIcon.Visible = true;

                        Application.Run();
                    }
                }
            }
            if (args.Count() > 0)
            {
                if (cargarConfig(args))
                {
                    guardarErrorFichero("LLAMA A TAREAS PROGRAMADAS");
                    tareasProgramadas(args);
                    guardarErrorFichero("SALE DE TAREAS PROGRAMADAS");
                }
            }
            else
            {
                Application.Run(new frMainForm());
            }

            if (args.Count() > 0)
            {
                matarProceso();
            }
        }

        private static void matarProceso()
        {
            try
            {
                Process.GetCurrentProcess().Kill();
            } 
            catch(Exception ex)
            {
                guardarErrorFichero("ERROR EN MATAR PROCESO " + ex.Message);
            }
        }


        public static bool cargarConfig(string[] args)
        {
            csSqlConnects conectorSQL = new csSqlConnects();
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csGlobal.conexionDB = args[0].ToString();
            csXMLConfig xml = new csXMLConfig();

            try
            {
                if (xml.cargarConexionesXML().Contains(args[0].ToString()))
                {
                    xml.cargarConfiguracionConexionesXML();

                    conectorSQL.cargarIdiomas();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private static void salir_Click(object sender, EventArgs e)
        {
            csUtilidades.salirDelPrograma();
        }

        private static void pedidos_Click(object sender, EventArgs e)
        {
            CronJobs.csPrestaCronJobs PrestaCronJobs = new CronJobs.csPrestaCronJobs();
            Thread t = new Thread(PrestaCronJobs.documentos);
            t.Start();
        }

        private static void stock_Click(object sender, EventArgs e)
        {
            CronJobs.csPrestaCronJobs PrestaCronJobs = new CronJobs.csPrestaCronJobs();
            Thread t = new Thread(PrestaCronJobs.stock);
            t.Start();
        }

        private static void tareasProgramadas(string[] args)
        {

            CronJobs.csRPSTCronJobs RPSTcronJobs = new CronJobs.csRPSTCronJobs();
            CronJobs.csPrestaCronJobs PrestaCronJobs = new CronJobs.csPrestaCronJobs();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].ToString() == "documentos")
                {
                    PrestaCronJobs.documentos();
                }
                if (args[i].ToString() == "stock")
                {
                    PrestaCronJobs.stock();
                }
                if (args[i].ToString() == "tallas")
                {
                    PrestaCronJobs.replog_tallas();
                }
                if (args[i].ToString() == "articulos")
                {
                    PrestaCronJobs.replog_articulos();
                }
                if (args[i].ToString() == "categorias")
                {
                    PrestaCronJobs.act_categorias();
                }
                if (args[i].ToString() == "dismay")
                {
                    PrestaCronJobs.TyC_dismay();
                }
                // Precios
                if (args[i].ToString() == "preciosdescuentos")
                {
                    PrestaCronJobs.precios_descuentos();
                }
                if (args[i].ToString() == "familiasclientearticulo")
                {
                    PrestaCronJobs.familias_cliente_articulo();
                }
                if (args[i].ToString() == "preciostarifasclientes")
                {
                    PrestaCronJobs.preciostarifasclientes();
                }
                if (args[i].ToString() == "amigosMiro")
                {
                    csUtilidades.log("PARAMETRO LEIDO AMIGOS MIRO");
                    amigos_miro();
                }
                if (args[i].ToString() == "pedidosPuntes")
                {
                    string idDocumento = "";
                    bool tipoAccion = false;

                    if (!string.IsNullOrEmpty(args[2].ToString()))
                    {
                            idDocumento = args[2].ToString();
                            Puntes.csPuntes puntes = new Puntes.csPuntes();
                            puntes.createOrderA3ERP(idDocumento, false);
                    }
                }

                if (args[i].ToString() == "deletePedidosPuntes")
                {
                    string idDocumento = "";

                    if (!string.IsNullOrEmpty(args[2].ToString()))
                    {
                        idDocumento = args[2].ToString();
                        Puntes.csPuntes puntes = new Puntes.csPuntes();
                        puntes.deleteOrderERP(idDocumento, false);
                    }
                }

                if (args[i].ToString() == "subir_cliente")
                {
                    RPSTcronJobs.subirCliente();
                }
                if (args[i].ToString() == "documentosRPST")
                {
                    RPSTcronJobs.documentosRPST();
                }
                if (args[i].ToString() == "facturasA3ERPToRepasat")
                {
                    guardarErrorFichero("Ejecutando facturasA3ERPToRepasat");
                    string serie = args.Length > 2 && !string.IsNullOrEmpty(args[2].ToString()) ? args[2].ToString() : null;
                    RPSTcronJobs.facturasA3ERPToRepasat(serie);
                    guardarErrorFichero("Saliendo facturasA3ERPToRepasat");
                }
                if (args[i].ToString() == "facturasRepasatToA3ERP")
                {
                    string serie = args.Length>2 && !string.IsNullOrEmpty(args[2].ToString()) ? args[2].ToString() : null;
                    RPSTcronJobs.facturasVRepasatToA3ERP(serie);
                }
                if (args[i].ToString() == "facturasCompraRepasatToA3ERP")
                {
                    string serie = args.Length > 2 && !string.IsNullOrEmpty(args[2].ToString()) ? args[2].ToString() : null;
                    RPSTcronJobs.facturasCRepasatToA3ERP(serie);
                }
                if (args[i].ToString() == "pedidosRepasatToA3ERP")
                {
                    string serie = args.Length > 2 && !string.IsNullOrEmpty(args[2].ToString()) ? args[2].ToString() : null;
                    RPSTcronJobs.pedidosVRepasatToA3ERP(serie);
                }
                if (args[i].ToString() == "actualizarStockA3ERPToRepasat")
                {
                    guardarErrorFichero("Ejecutando actualizarStockA3ERPToRepasat");
                    RPSTcronJobs.actualizarStockA3ERPToRepasat();
                    guardarErrorFichero("Saliendo actualizarStockA3ERPToRepasat");
                }
                if (args[i].ToString() == "actualizarClientesA3ERPToRepasat")
                {
                    RPSTcronJobs.actualizarClientesA3ERPToRepasat();
                }
                if (args[i].ToString() == "cuentasRepasatToA3ERP")
                {
                    RPSTcronJobs.cuentasRepasatToA3ERP(true);
                }
                if (args[i].ToString() == "auxiliaresA3ERPToRepasat")
                {
                    RPSTcronJobs.actualizarAuxiliaresA3ERPToRepasat();
                }
                if (args[i].ToString() == "actualizarArticulosA3toRPST")
                {
                    RPSTcronJobs.actualizarArticulosA3toRPST();
                }
                if (args[i].ToString() == "crearArticulosA3toRPST")
                {
                    RPSTcronJobs.crearArticulosA3toRPST();
                }
               if (args[i].ToString() == "actualizarArticulosRPSTtoA3")
                {
                    RPSTcronJobs.actualizarArticulosRPSTtoA3();
                }
                if (args[i].ToString() == "crearArticulosRPSTtoA3")
                {
                    //RPSTcronJobs.crearArticulosRPSTtoA3();
                    //Al traspasar facturas hay una funcion que detecta si hay articulos nuevos y los traspasa
                }
                if (args[i].ToString().Equals("sincroCentrosCosteA3ToRPST", StringComparison.OrdinalIgnoreCase))
                {
                    RPSTcronJobs.sincroCentrosCosteA3ToRPST();
                    return;
                }
            }
            

            Process[] works = Process.GetProcessesByName("Klosions");
            foreach (Process work in works)
            {
               // work.WaitForExit();
            }
        }

        /// <summary>
        /// Comprobar en Prestashop los amigos que han pagado.
        /// Si se registra uno nuevo en la tienda y coincide con el de A3, ponerle en el grupo amigo automat.
        /// Los que esten en los dos sitios van al grupo 4 (amigos) y los que no, al 3 (customer)
        /// Tarea programada cada 24 horas
        /// </summary>
        public static void amigos_miro()
        {
            try
            {
                int contador = 0;
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlConnects sql = new csSqlConnects();
                if (csGlobal.modeDebug)
                    csUtilidades.log("ACCESO AMIGOS MIRO");

                DataTable mysql_friends = mysql.cargarTabla("select id_customer, email from ps_customer");

                if (csGlobal.modeDebug)
                    csUtilidades.log("CARGA DE " + mysql_friends.Rows.Count.ToString() + " filas DE MYSQL");

                DataTable sql_friends = sql.cargarDatosTablaA3("select correu from [euromus-amics-actius] where correu is not null");

                if (csGlobal.modeDebug)
                    csUtilidades.log("CARGA DE " + sql_friends.Rows.Count.ToString() + " filas de amigos de SQL");


                foreach (DataRow item in mysql_friends.Rows)
                {
                    string id_mysql = item["id_customer"].ToString();
                    string email_mysql = item["email"].ToString();

                    if (csGlobal.modeDebug)
                    {
                        csUtilidades.log("Check Registro: " + contador + ": " + email_mysql + " / " + id_mysql);
                        contador++;
                    }

                    DataTable result = sql_friends.buscar(string.Format("correu = '{0}'", email_mysql));

                    // El email esta en los dos sitios. Actualizamos su grupo al 4
                    if (csUtilidades.verificarDt(result))
                    {
                        cambiarGrupoCliente(id_mysql, "4");
                    }
                    else // El email no está en A3, lo pasamos al grupo 3
                    {
                        cambiarGrupoCliente(id_mysql, "3");
                    }
                }

                if (csGlobal.modeDebug)
                    csUtilidades.log("Fin iteracción con amigos");
            }
            catch (Exception ex)
            {
                if (csGlobal.modeDebug)
                {
                    ("Error en proceso amigos: " + ex.Message).mb();
                    csUtilidades.log("Error en proceso amigos: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// http://restsharp.org/
        /// Mediante esta libreria actualizamos por webservice 
        /// el default_group de cada uno de los clientes pasado por parámetro
        /// </summary>
        /// <param name="id_customer"></param>
        /// <param name="id_default_group"></param>
        public static void cambiarGrupoCliente(string id_customer, string id_default_group)
        {
            try
            {
                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest("customers/" + id_customer + "?ws_key=" + csGlobal.webServiceKey, Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();
                orderEl.Element("id_default_group").Value = id_default_group;
                orderEl.Element("associations").Element("groups").Descendants().Last().Value = id_default_group;

                request = new RestRequest("customers?ws_key=" + csGlobal.webServiceKey, Method.PUT);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception ex) 
            {
                csUtilidades.log("error al cambiar de grupo: " + ex.Message);
            }            
        }


        public static void guardarErrorFichero(string texto)
        {
            const string fic = @"errorLog.txt";
            System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true);
            swq.WriteLine("_____________________________________________________________ " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString() + " __________________________________________________________________________");
            swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString());
            swq.WriteLine();
            swq.WriteLine();
            swq.WriteLine();
            swq.Close();


            List<string> ccs = new List<string>();
            string asunto = "";
            string bodyMail = "";
            string adjunto ="";
            string from ="";
            string passwordMail="";
            string servidorSMTP="";
            
            
            
            ccs.Add("lluisvera@klosions.com");
            ccs.Add("ofi@repasat.com");
            if (csGlobal.emailNotificacionCliente!="")
            {
                ccs.Add(csGlobal.emailNotificacionCliente);
            }

            //Composición de Email
            asunto = "(ESYNC) ERROR";
            bodyMail = "NOMBRE DE CONEXIÓN:" + csGlobal.conexionDB + " \n" + "NOMBRE DE EQUIPO: (" + Environment.MachineName + ")" + " \n" + "Te envía la siguiente notificación: "  + " \n" + texto;
            from ="notificaciones@repasat.com";
            passwordMail="KL0S10N$";
            servidorSMTP = "mail.repasat.com";


           // csUtilidades.enviarMail(asunto,bodyMail, ccs,adjunto,"dev@klosions.com",from, passwordMail,servidorSMTP,false, false);

        }

        public static void guardarProcesoTareasProgramadas(string texto)
        {
            const string fic = @"procesoTareasProgramadasLog.txt";
            System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true);
            swq.WriteLine("_____________________________________________________________ " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString() + " __________________________________________________________________________");
            swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString());
            swq.WriteLine();
            swq.WriteLine();
            swq.WriteLine();
            swq.Close();

        }

        public static void escribirErrorEnFichero(string texto)
        {
            const string fic = @"errorLog.txt";
            System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true);
            swq.WriteLine("_____________________________________________________________ " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString() + " __________________________________________________________________________");
            swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString().ToString() + " - " + DateTime.Now.ToShortTimeString().ToString());
            swq.WriteLine();
            swq.WriteLine();
            swq.WriteLine();
            swq.Close();
        }
    }
}

