﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frTablasAuxiliares : Form
    {
        #region Start
        private csMySqlConnect mysql = new csMySqlConnect();
        private csSqlConnects sql = new csSqlConnects();
        private string current_table_ps = "";
        private static frTablasAuxiliares m_FormDefInstance;
        public static frTablasAuxiliares DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frTablasAuxiliares();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frTablasAuxiliares()
        {
            InitializeComponent();

          
        }

        private void frTablasAuxiliares_Load(object sender, EventArgs e)
        {
            if (csGlobal.modoTallasYColores == "A3NOPSSI")
            {
                toolStripButtonAtributos.Enabled = true;
                this.Refresh();
            }
            else
            {
                toolStripButtonAtributos.Enabled = false;
            }
        }



        #endregion

        private void toolStripButtonSincronizar_Click(object sender, EventArgs e)
        {
            sincronizar();
        }

        private void toolStripButtonFabricantes_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = true;
            habilitarCMS(false, false); 
            setCurrentTable("ps_manufacturer");
            fabricantes();
        }

        private void toolStripButtonCategorias_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            toolStripButtonA3ERPtoPS.Enabled = false;
            habilitarCMS(false, false); 

            setCurrentTable("ps_category");
            categorias();
        }

        private void toolStripButtonCaracteristicas_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            habilitarCMS(false, false); 

            setCurrentTable("ps_feature");
            caracteristicas();
        }

        private void toolStripButtonAtributos_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            toolStripButtonA3ERPtoPS.Enabled = false;
            if (csGlobal.conexionDB.ToUpper().Contains("SLIDESPORTS"))
            {
                toolStripButtonPStoA3ERP.Enabled = true;
            }

            habilitarCMS(false, false); 

            setCurrentTable("ps_attribute");
            atributos();
        }


        /// <summary>
        /// Enviar contenido de A3ERP a PS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonA3ERPtoPS_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (dgvA3.Rows.Count > 0)
            {
                switch (current_table_ps)
                {
                    case "ps_category":
                        csUtilidades.ejecutarConsulta("delete from ps_category where id_category not in (1,2)", true);
                        csUtilidades.ejecutarConsulta("delete from ps_category_shop where id_category not in (1,2)", true);
                        csUtilidades.ejecutarConsulta("delete from ps_category_lang  where id_category not in (1,2)", true);
                        csUtilidades.ejecutarConsulta("delete from ps_category_group", true);
                        fillCategoryGroup();
                        break;

                    case "ps_feature":
                        mysql.borrar("ps_feature");
                        mysql.borrar("ps_feature_lang");
                        mysql.borrar("ps_feature_shop");
                        mysql.borrar("ps_feature_value");
                        mysql.borrar("ps_feature_value_lang");
                        break;
                }

                foreach (DataGridViewRow item in dgvA3.SelectedRows)
                {
                    string id = "", name = "";
                    switch (current_table_ps)
                    {
                        case "ps_manufacturer":
                            id = item.Cells["KLS_CODMARCA"].Value.ToString();
                            name = item.Cells["KLS_DESCMARCA"].Value.ToString();
                            csUtilidades.ejecutarConsulta("insert into ps_manufacturer (id_manufacturer, name, active) values(" + Convert.ToInt32(id) + ", '" + name + "',1)", true);
                            fabricantes();
                            break;
                        case "ps_category":
                            subirCategorias(item.Cells["KLS_COD_PS"].Value.ToString(),
                                item.Cells["KLS_DESCCATEGORIA"].Value.ToString());
                            categorias();
                            break;
                        case "ps_feature":
                            subirCaracteristicas(item.Cells["KLS_COD_PS"].Value.ToString(),
                                item.Cells["KLS_CODCARACTERISTICA"].Value.ToString(),
                                item.Cells["KLS_DESCCARACTERISTICA"].Value.ToString(),
                                item.Cells["KLS_DESCVALORCARACTERISTICA"].Value.ToString());
                            caracteristicas();
                            break;
                        default:
                            break;
                    }
                }

                MessageBox.Show("COMPLETADO");

            }
        }

        private void fillCategoryGroup()
        {
            DataTable dt_groups = mysql.cargarTabla("select id_group from ps_group");
            DataTable dt_categories = sql.cargarDatosTablaA3("select distinct kls_cod_ps from kls_categorias");

            foreach (DataRow a3 in dt_categories.Rows)
            {
                string id_category = a3["KLS_COD_PS"].ToString();
                foreach (DataRow ps in dt_groups.Rows)
                {
                    string id_group = ps["id_group"].ToString();
                    string consulta = "insert into ps_category_group values(" + id_category + ", " + id_group + ")";

                    csUtilidades.ejecutarConsulta(consulta, true);
                }
            }

            MessageBox.Show("PS_CATEGORY_GROUP COMPLETADO");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">5</param>
        /// <param name="cod">1</param>
        /// <param name="name">COMPOSICION</param>
        /// <param name="valor">FIBRA CARBOTECH</param>
        private void subirCaracteristicas(string id, string cod, string name, string valor)
        {
            string query1 = "insert into ps_feature values (" + id + ",1)";
            csUtilidades.ejecutarConsulta(query1, true);

            foreach (KeyValuePair<int, string> entry in csGlobal.IdiomasEsync)
            {
                string query2 = "insert into ps_feature_lang values(" + id + "," + entry.Key + ",'" + name + "')";

                csUtilidades.ejecutarConsulta(query2, true);
            }

            string query3 = "insert into ps_feature_shop values (" + id + ",1)";
            csUtilidades.ejecutarConsulta(query3, true);

            string query4 = "insert into ps_feature_value (id_feature, custom) values (" + cod + ",'" + valor + "')";
            csUtilidades.ejecutarConsulta(query4, true);

            foreach (KeyValuePair<int, string> entry in csGlobal.IdiomasEsync)
            {
                string query5 = "insert into ps_feature_value_lang (id_feature_value, id_lang, value) values(" + id + "," + entry.Key + ",'" + valor + "')";

                csUtilidades.ejecutarConsulta(query5, true);
            }
        }

        private void subirCategorias(string id, string name)
        {
            if (id != "1" && id != "2")
            {
                string query1 = "insert into ps_category (id_category, id_parent, id_shop_default, level_depth, " +
                    " nleft, nright, active, date_add, date_upd, position, is_root_category) " +
                    " values (" + id + ",2,1,1,1,1,1,'" + DateTime.Now + "','" + DateTime.Now + "',1,0)";
                csUtilidades.ejecutarConsulta(query1, true);


                string query2 = "insert into ps_category_shop values (" + id + ", 1,1)";
                csUtilidades.ejecutarConsulta(query2, true);

                foreach (KeyValuePair<int, string> entry in csGlobal.IdiomasEsync)
                {
                    string query3 = "insert into ps_category_lang (id_category, id_shop, id_lang, name, link_rewrite) values " +
                        " (" + id + ",1," + entry.Key + ", '" + name + "','" + name + "')";

                    csUtilidades.ejecutarConsulta(query3, true);
                }
            }
        }

        /// <summary>
        /// Enviar contenido de PS a A3ERP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        private void toolStripButtonLanguages_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            habilitarCMS(false, false); 

            current_table_ps = "ps_lang";

            idiomas();
        }

        /**************************** MÉTODOS ************************************/

        private void idiomas()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("select * from kls_esync_idiomas"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("select * from ps_lang where active = 1"));
        }


        private void activarBotonSincronizar()
        {
            toolStripButtonSincronizar.Enabled = true;
        }

        private void setCurrentTable(string ps)
        {
            current_table_ps = ps;
        }

        private void fabricantes()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("select KLS_CODMARCA, KLS_DESCMARCA from kls_marcas"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("select id_manufacturer as KLS_CODMARCA, name AS KLS_DESCMARCA from ps_manufacturer where active = 1"));
        }

        private string idIdiomaDefault()
        {
            return "(Select value from ps_configuration where name='PS_LANG_DEFAULT')";
        }

        private void categorias()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("SELECT KLS_COD_PS,KLS_DESCCATEGORIA FROM KLS_CATEGORIAS"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("select ps_category.id_category as KLS_COD_PS, ps_category_lang.name AS KLS_DESCCATEGORIA, ps_category.id_parent from ps_category left join ps_category_lang on ps_category.id_category = ps_category_lang.id_category WHERE ps_category.active = 1 and ps_category_lang.id_lang = " + idIdiomaDefault()));
        }

        private void caracteristicas()
        {
            string consulta_ps = "Select ps_feature.id_feature as KLS_COD_PS, ps_feature_value_lang.id_feature_value KLS_CODCARACTERISTICA, ps_feature_lang.name AS KLS_DESCCARACTERISTICA, ps_feature_value_lang.value AS KLS_DESCVALORCARACTERISTICA From ps_feature Inner Join ps_feature_lang On ps_feature_lang.id_feature = ps_feature.id_feature Inner Join ps_feature_value On ps_feature.id_feature = ps_feature_value.id_feature Inner Join ps_feature_value_lang On ps_feature_value_lang.id_feature_value = ps_feature_value.id_feature_value Where ps_feature_lang.id_lang = (Select value from ps_configuration where name='PS_LANG_DEFAULT') and ps_feature_value_lang.id_lang= (Select value from ps_configuration where name='PS_LANG_DEFAULT')";

            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("SELECT KLS_COD_PS, KLS_CODCARACTERISTICA, KLS_DESCCARACTERISTICA, KLS_DESCVALORCARACTERISTICA from kls_caracteristicas"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla(consulta_ps));
        }

        private void atributos()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("select * from kls_atributos order by kls_cod_atributo asc"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("SELECT distinct ps_attribute.id_attribute AS KLS_COD_ATRIBUTO, ps_attribute_group_lang.name AS KLS_DESCGROUP_PS, ps_attribute.id_attribute_group AS KLS_IDGROUP_PS, ps_attribute_lang.name AS KLS_VALOR_ATRIBUTO FROM ps_attribute_lang LEFT JOIN ps_attribute ON ps_attribute_lang.id_attribute = ps_attribute.id_attribute LEFT JOIN ps_attribute_group_lang ON ps_attribute_group_lang.id_attribute_group = ps_attribute.id_attribute_group where ps_attribute_group_lang.id_lang = " + idIdiomaDefault() + " and ps_attribute_lang.id_lang = " + idIdiomaDefault() + " order by kls_cod_atributo asc"));
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            switch ((current_table_ps == "") ? this.current_table_ps : current_table_ps)
            {
                case "ps_orders":
                    sincronizarFormaPago();
                    formaspago();
                    break;
                case "ps_carrier":
                    sincronizarTransportista();
                    transportistas();
                    break;
                case "ps_manufacturer":
                    sincronizarFabricantes();
                    fabricantes();
                    break;
                case "ps_category":
                    sincronizarCategorias();
                    categorias();
                    break;
                case "ps_feature":
                    sincronizarCaracteristicas();
                    caracteristicas();
                    break;
                case "ps_attribute":
                    sincronizarAtributos();
                    atributos();
                    break;
                case "ps_lang":
                    csIdiomas idioma = new csIdiomas();
                    idioma.fixLanguages();
                    idiomas();
                    break;
                default:
                    break;
            }
        }

        private void sincronizarFormaPago()
        {
            if (dgvA3.SelectedRows.Count == 1 && dgvPS.SelectedRows.Count == 1)
            {
                string formapago = dgvPS.SelectedRows[0].Cells["payment"].Value.ToString();
                string docpag = dgvA3.SelectedRows[0].Cells["docpag"].Value.ToString();
                string consulta = string.Format("update docupago set kls_id_ps = '{0}' where docpag = '{1}'", formapago, docpag);

                csUtilidades.ejecutarConsulta(consulta, false);
            }
        }

        private void sincronizarTransportista()
        {
            if (dgvA3.SelectedRows.Count == 1 && dgvPS.SelectedRows.Count == 1)
            {
                string id_reference = dgvPS.SelectedRows[0].Cells["id_reference"].Value.ToString();
                string codtra = dgvA3.SelectedRows[0].Cells["codtra"].Value.ToString();
                string consulta = string.Format("update transpor set kls_id_ps = '{0}' where codtra = '{1}'", id_reference, codtra);

                csUtilidades.ejecutarConsulta(consulta, false);
            }
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripProgressBar.MarqueeAnimationSpeed = 0;
            toolStripProgressBar.Style = ProgressBarStyle.Blocks;
            toolStripProgressBar.Value = toolStripProgressBar.Maximum;
        }

        public void sincronizar(string current_table_ps = "")
        {
            toolStripProgressBar.Style = ProgressBarStyle.Marquee;
            toolStripProgressBar.MarqueeAnimationSpeed = 50;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private DataTable diferenciasDGV()
        {
            DataTable ps = csUtilidades.fillDataTableFromDataGridView(dgvPS, false);
            DataTable a3 = csUtilidades.fillDataTableFromDataGridView(dgvA3, false);

            return csUtilidades.getDiffDataTables(ps, a3);
        }

        private void sincronizarAtributos()
        {
            //frAtributos attribute = new frAtributos();
            //attribute.copiarAtributosToA3();
            traspasarAtributos();
            //traspasarAtributosProductos();
            MessageBox.Show("ATRIBUTOS TRASPASADOS");
        }

        private void traspasarAtributosProductos()
        {
            DataTable dt = mysql.cargarTabla("select id_product_attribute AS KLS_COD_ATRIBUTO, reference AS CODART from ps_product_attribute where reference is not null");

            if (csUtilidades.verificarDt(dt))
            {
                List<string> values = new List<string>();
                string campos = "(CODART, KLS_COD_ATRIBUTO)";

                foreach (DataRow item in dt.Rows)
                {
                    string codart = item["CODART"].ToString();
                    string kls_cod_atributo = item["KLS_COD_ATRIBUTO"].ToString();

                    string consulta = "insert into KLS_ATRIBUTOS_ART " + campos + " SELECT CODART, '" + kls_cod_atributo + "'  FROM ARTICULO WHERE LTRIM(CODART) = '" + codart + "'";
                    csUtilidades.ejecutarConsulta(consulta, false);
                }
            }
        }

        private void traspasarAtributos()
        {
            DataTable dt = diferenciasDGV();
            string scriptUpdate = "";

            //// TODO - Actualizar valores
            //foreach(DataRow item in dt.Rows)
            //{
            //    string id_attribute = item["KLS_COD_ATRIBUTO"].ToString();
            //    string query = string.Format("SELECT ps_attribute_group_lang.name " +
            //                    "FROM ps_attribute_lang " +
            //                    "LEFT JOIN ps_attribute ON ps_attribute_lang.id_attribute = ps_attribute.id_attribute " +
            //                    "LEFT JOIN ps_attribute_group_lang ON ps_attribute_group_lang.id_attribute_group = ps_attribute.id_attribute_group  " +
            //                    "where ps_attribute_group_lang.id_lang = 1 and ps_attribute_lang.id_lang =  1  where ps_attribute.id_attribute = {0} " +
            //                    "order by kls_cod_atributo asc ", id_attribute);

            //    DataTable dtquery = mysql.cargarTabla(query);
            //}

            if (csUtilidades.verificarDt(dt))
            {
                List<string> values = new List<string>();
                string valores = "";
                string campos = "(KLS_COD_ATRIBUTO, KLS_DESCGROUP_PS, KLS_IDGROUP_PS, KLS_VALOR_ATRIBUTO)";

                foreach (DataRow item in dt.Rows)
                {
                    string kls_cod_atributo = item["KLS_COD_ATRIBUTO"].ToString();
                    string kls_descgroup_ps = item["KLS_DESCGROUP_PS"].ToString().ToString().Replace("'", "").Replace("(", "").Replace(")", "");
                    string kls_idgroup_ps = item["KLS_IDGROUP_PS"].ToString();
                    string kls_valor_atributo = item["KLS_VALOR_ATRIBUTO"].ToString().Replace("'","").Replace("(","").Replace(")","");
                    valores = "(" + kls_cod_atributo + ", '" + kls_descgroup_ps + "', " + kls_idgroup_ps + ", '" + kls_valor_atributo + "')";
                    scriptUpdate = "UPDATE KLS_ATRIBUTOS SET " +
                        " KLS_DESCGROUP_PS = '" + kls_descgroup_ps + "', " +
                        " KLS_IDGROUP_PS = '" + kls_idgroup_ps + "', " +
                        " KLS_VALOR_ATRIBUTO = '" + kls_valor_atributo + "' " +
                        " WHERE KLS_COD_ATRIBUTO = " + kls_cod_atributo +
                        " IF @@ROWCOUNT = 0 " +
                        " INSERT INTO KLS_ATRIBUTOS " + campos + " VALUES " + valores;
                    csUtilidades.ejecutarConsulta(scriptUpdate, false);
                }

                //csUtilidades.WriteListToDatabase("KLS_ATRIBUTOS", campos, values, csUtilidades.SQL, true);
            }
        }

        

        private void sincronizarCaracteristicas()
        {
            DataTable dt = diferenciasDGV();

            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string kls_cod_ps = item["KLS_COD_PS"].ToString();
                    string kls_codcaracteristica = item["KLS_CODCARACTERISTICA"].ToString();
                    string kls_valorcaracteristica = kls_codcaracteristica;
                    string kls_desccaracteristica = item["KLS_DESCCARACTERISTICA"].ToString();
                    string kls_descvalorcaracteristica = item["KLS_DESCVALORCARACTERISTICA"].ToString();

                    csUtilidades.ejecutarConsulta("insert into kls_caracteristicas (KLS_COD_PS, KLS_CODCARACTERISTICA, KLS_DESCCARACTERISTICA, KLS_DESCVALORCARACTERISTICA, KLS_VALORCARACTERISTICA) " +
                                " VALUES (" + Convert.ToInt32(kls_cod_ps) + ",'" + kls_codcaracteristica + "','" + kls_desccaracteristica.Replace("'", "") + "','" + kls_descvalorcaracteristica + "','" + Convert.ToInt32(kls_valorcaracteristica) + "')", false);
                }
            }
        }

        private void sincronizarCategorias()
        {
            DataTable dt = diferenciasDGV();

            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string id_category = item["KLS_COD_PS"].ToString();
                    string name = item["KLS_DESCCATEGORIA"].ToString();
                    string id_parent = item["id_parent"].ToString();
                    csUtilidades.crearCategoria("insert into kls_categorias (KLS_COD_PS, KLS_CODCATEGORIA, KLS_DESCCATEGORIA, KLS_ID_CATPARENT) " +
                                " VALUES ('" + id_category + "','" + id_category + "','" + name + "', '" + id_parent + "')", false);
                }
            }
        }

        private void sincronizarFabricantes()
        {
            DataTable dt = diferenciasDGV();

            if (csUtilidades.verificarDt(dt))
            {
                foreach (DataRow item in dt.Rows)
                {
                    string codmarca = item["KLS_CODMARCA"].ToString();
                    string descmarca = item["KLS_DESCMARCA"].ToString();
                    csUtilidades.ejecutarConsulta("insert into kls_marcas (KLS_CODMARCA, " +
                                " KLS_DESCMARCA) VALUES ('" + codmarca + "', '" + descmarca + "')", false);
                }
            }
        }

        private void toolTransportistas_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            habilitarCMS(true, false); 

            setCurrentTable("ps_carrier");
            transportistas();
        }

        private void habilitarCMS(bool transportistas, bool formaspago)
        {
            limpiarTransportistasToolStripMenuItem.Enabled = transportistas;
            limpiarFormasPagoToolStripMenuItem.Enabled = formaspago;
        }

        private void transportistas()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("SELECT codtra, KLS_ID_PS, nomtra from transpor"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("select ps_carrier.id_reference, is_free, name from ps_carrier where deleted = 0 and active = 1"));
        }

        private void toolFormasPago_Click(object sender, EventArgs e)
        {
            toolStripButtonPStoA3ERP.Enabled = false;
            habilitarCMS(false, true);
            setCurrentTable("ps_orders");
            formaspago();
        }

        private void formaspago()
        {
            activarBotonSincronizar();

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3("select docpag, descdoc, kls_id_ps from DOCUPAGO"));
            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla("select distinct payment from ps_orders"));
        }

        private void limpiarTransportistasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Borrar los ids seleccionados
            if ("Vas a borrar los ids seleccionados, ¿deseas proceder?".what())
            {
                if (dgvA3.Rows.Count > 0)
                {
                    if (dgvA3.SelectedRows.Count > 0)
                    {
                        foreach(DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string codtra = item.Cells["codtra"].Value.ToString();
                            string query = string.Format("update transpor set kls_id_ps = NULL where ltrim(codtra) = '{0}'", codtra.Trim());

                            csUtilidades.ejecutarConsulta(query, false);
                        }

                        csMsg.operacionExito();
                    }
                    else { csMsg.noRowSelected(); } 
                }
                else { csMsg.noResults(); } 
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void limpiarFormasPagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Borrar los ids seleccionados
            if ("Vas a borrar los ids seleccionados, ¿deseas proceder?".what())
            {
                if (dgvA3.Rows.Count > 0)
                {
                    if (dgvA3.SelectedRows.Count > 0)
                    {
                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string codtra = item.Cells["docpag"].Value.ToString();
                            string query = string.Format("update docupago set kls_id_ps = NULL where ltrim(docpag) = '{0}'", codtra.Trim());

                            csUtilidades.ejecutarConsulta(query, false);
                        }

                        csMsg.operacionExito();
                    }
                    else { csMsg.noRowSelected(); }
                }
                else { csMsg.noResults(); }
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        //Actualizo las marcas de A3ERP en base a las marcas de Prestashop
        //Nueva función de actualizar o insertar
        private void toolStripButtonPStoA3ERP_Click(object sender, EventArgs e)
        {
            if (csGlobal.conexionDB.ToUpper().Contains("SLIDESPORTS"))
            {
                sincronizarAtributos();
            }
            else { 

                string codMarca = "";
                string descMarca = "";
                string query = "";
                csSqlConnects sqlConnect = new csSqlConnects();

                foreach (DataGridViewRow fila in dgvPS.Rows)
                {
                    codMarca = fila.Cells["KLS_CODMARCA"].Value.ToString();
                    descMarca = fila.Cells["KLS_DESCMARCA"].Value.ToString();
                        query="UPDATE KLS_MARCAS SET KLS_DESCMARCA='" + descMarca + "' WHERE KLS_CODMARCA=" + codMarca +
                              " IF @@ROWCOUNT=0 " +
                              " INSERT INTO KLS_MARCAS (KLS_CODMARCA, KLS_DESCMARCA) VALUES (" + codMarca  + ",'" + descMarca + "')";
                        csUtilidades.ejecutarConsulta(query, false);
            
                }
                fabricantes();
            }
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            tyc.inicializarTallasYColores(null);

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            sincronizarRepresentantes();
            sincronizarRepClientesPS();
            MessageBox.Show("Representantes sincronizados.");
        }

        public void sincronizarRepClientesPS() {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string representante = "";

            DataTable dta3 = sql.cargarDatosTablaA3("select clientes.NOMCLI,clientes.CODCLI,clientes.KLS_CODCLIENTE as id_customer," +
                                                    " clientes.CODREP,represen.NOMREP,represen.KLS_CODCLIENTE as COD_REP_PS "+
                                                    " from clientes "+
                                                    " inner join represen on represen.CODREP=clientes.CODREP "+
                                                    " where CLIENTES.KLS_CODCLIENTE>0 and CLIENTES.CODREP>0 ");
            mysql.OpenConnection();
            

            foreach (DataRow row in dta3.Rows) {
                representante = row["COD_REP_PS"].ToString().Equals("")? "NULL" : row["COD_REP_PS"].ToString();

                    mysql.ejecutarConsulta("update ps_customer set kls_id_manager="+ representante + " where id_customer="+ row["id_customer"],true);
            }


            mysql.CloseConnection();



        }
        public void sincronizarRepresentantes() {

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            DataTable dta3 = sql.cargarDatosTablaA3("select CODREP,E_MAIL,KLS_CODCLIENTE from represen where OBSOLETO='F'");

            DataTable dtps = mysql.cargarTabla("select id_customer, email from ps_customer");

            for (int i=0; i < dta3.Rows.Count; i++)
            {
                for (int x = 0; x < dtps.Rows.Count; x++) {
                    if (dta3.Rows[i]["E_MAIL"].ToString() == dtps.Rows[x]["email"].ToString())
                    {
                        dta3.Rows[i]["KLS_CODCLIENTE"] = dtps.Rows[x]["id_customer"].ToString();
                    }
                }
            }

            sql.abrirConexion();
            //Eliminamos los codigos de tienda de aquellos representantes que estan obsoletos.
            sql.actualizarCampo("update represen set KLS_CODCLIENTE=NULL where OBSOLETO='F'", false);
            foreach (DataRow row in dta3.Rows)
            {
                if (row["KLS_CODCLIENTE"].ToString() != "")
                {
                    sql.actualizarCampo("update represen set kls_codcliente=" + row["KLS_CODCLIENTE"] + " where LTRIM(CODREP)=" + row["CODREP"], false);

                }
            }
            sql.cerrarConexion();


        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            tyc.vaciarTablasTallasyColores();
        }

        private void sincronizarArticulosCategoriasEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tabla = current_table_ps;

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable categoriasA3;
            DataTable categoriasPs;
            string codart, email, cat;

            int i = 0;

            categoriasA3 = sql.cargarTabla("KLS_CATEGORIAS", "*");
            categoriasPs = mysql.cargarTabla("SELECT id_category, ps_product.id_product, ps_product.reference, CASE WHEN id_category = ps_product.id_category_default THEN 'T' ELSE '' END AS KLS_PRINCIPAL " +
                                        "FROM ps_category_product "+
                                        "LEFT JOIN ps_product ON ps_category_product.id_product = ps_product.id_product");

            if ("Vas a resetar la sincronización categorias-articulos, ¿deseas proceder?".what())
            {
                sql.borrarDatosSqlTabla("KLS_CATEGORIAS_ART");
                if (categoriasA3.Rows.Count > 0)
                {
                    foreach (DataRow product in categoriasPs.Rows)
                    {
                        codart = sql.obtenerCampoTabla("SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + product["reference"] + "'", true);
                        if (codart != "")
                        {
                            cat = sql.obtenerCampoTabla("SELECT KLS_COD_PS FROM KLS_CATEGORIAS WHERE LTRIM(KLS_COD_PS)='" + product["id_category"] + "'", true);
                            if (cat != "")
                            {
                                sql.actualizarCampo("INSERT INTO KLS_CATEGORIAS_ART VALUES ('" + codart + "','" + cat + "','" + product["KLS_PRINCIPAL"] + "')", true);
                                i++;
                            }
                        }

                    }
                }
                MessageBox.Show("Se han sincronizado " + i + " categorias-articulos");
            }
            else
            {
                MessageBox.Show("Operación cancelada.");
            }


        }

        private void sincronizarArticulosMarcasEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tabla = current_table_ps;

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable productoPs;
            string codart;

            int i = 0;

            productoPs = mysql.cargarTabla("SELECT reference, id_manufacturer from ps_product WHERE id_manufacturer>0");


            foreach (DataRow product in productoPs.Rows)
            {
                if (product["id_manufacturer"].ToString() != "0")
                {
                    codart = sql.obtenerCampoTabla("SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + product["reference"] + "'", true);
                    if (codart != "")
                    { 
                        sql.actualizarCampo("UPDATE ARTICULO SET KLS_CODMARCA='"+ product["id_manufacturer"].ToString()+ "' WHERE LTRIM(CODART)='" + product["reference"] + "'", true);
                        i++;
                    } 
                }
            }
           
            MessageBox.Show("Se han sincronizado " + i + " articulos-marcas");
        }
    }
}
