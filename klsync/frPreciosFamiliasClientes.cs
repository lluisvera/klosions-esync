﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frPreciosFamiliasClientes : Form
    {
        bool customers = false;
        csMySqlConnect mysql = new csMySqlConnect();
        private static frPreciosFamiliasClientes m_FormDefInstance;
        public static frPreciosFamiliasClientes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPreciosFamiliasClientes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frPreciosFamiliasClientes()
        {
            InitializeComponent();
        }
        private string id_product = "";
        private string id_customer = "";
        
        private void btnCargarClientes_Click(object sender, EventArgs e)
        {
            customers = true;

            string consulta = "select id_customer, upper(firstname) as name, upper(lastname) as lastname, email as email, kls_a3erp_id as codigo, kls_a3erp_fam_id as familia from ps_customer where active = 1 and deleted = 0";
            csUtilidades.cargarDGV(dgvIZQ, consulta, csUtilidades.MYSQL);
            
            tssInfo.Text = "Clientes cargados";
        }

        private void btnCargarArticulos_Click(object sender, EventArgs e)
        {
            customers = false;

            string consulta = "select distinct ps_specific_price.id_product, ps_specific_price.id_customer, reference as name, ps_specific_price.price as precio, ps_specific_price.`from`, ps_specific_price.`to`, ps_specific_price.kls_famcli as cli, kls_famart as familia from ps_specific_price left join ps_product on ps_specific_price.id_product = ps_product.id_product order by ps_specific_price.`from` desc";
            csUtilidades.cargarDGV(dgvIZQ, consulta, csUtilidades.MYSQL);
            
            tssInfo.Text = "Articulos cargados";
        }

        private void moveRow()
        {
            
            id_customer = dgvIZQ.SelectedRows[0].Cells[0].Value.ToString();
            if (id_customer != "0")
            {
                cargarDatos(id_customer);
            }
            else
            {
                cargarDatos(id_customer, dgvIZQ.SelectedRows[0].Cells[6].Value.ToString());
            }
        }

        private void dgvIZQ_KeyUp(object sender, KeyEventArgs e)
        {
            moveRow();
        }

        private void cargarDatos(string id, string fam = "")
        {
            string consulta = "";

            if (customers)
            {
                consulta = "select price as precio, reduction as descuento,ps_specific_price.from, ps_specific_price.to, kls_famart as familia from ps_specific_price where id_customer = " + id + " order by ps_specific_price.from desc";
                csUtilidades.cargarDGV(dgvDER, consulta, csUtilidades.MYSQL);
            }
            else 
            {
                if (fam != "")
                {
                    consulta = string.Format("select price as precio, reduction as descuento, kls_famcli as familia from ps_specific_price where id_product = {0} and kls_famart = '{1}'", id, fam);
                }
                else
                {
                    consulta = "select price as precio, reduction as descuento, kls_famcli as familia from ps_specific_price where id_product = " + id; 
                }
                
                csUtilidades.cargarDGV(dgvDER, consulta, csUtilidades.MYSQL);
            }
        }

        private void dgvIZQ_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            moveRow();
        }
    }
}
