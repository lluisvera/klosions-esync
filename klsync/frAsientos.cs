﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frAsientos : Form
    {
        private string cuenta_origen = "";
        
        csSqlConnects sql = new csSqlConnects();
        private static frAsientos m_FormDefInstance;
        public static frAsientos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frAsientos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frAsientos()
        {
            InitializeComponent();
            this.toolStripButtonHelpAsientos.ToolTipText = "El formulario permite cambiar el ID de la cuenta entre dos asientos";
        }
       
        private void toolStripButtonCargarAsientos_Click(object sender, EventArgs e)
        {
            cargarAsientos();
        }

        private void cargarAsientos()
        {
            DataTable dt = sql.cargarDatosTablaA3("SELECT DISTINCT CUENTAS.CUENTA FROM __ASIENTOS LEFT JOIN CUENTAS ON __ASIENTOS.IDCUENTA = CUENTAS.IDCUENTA WHERE NIVEL = 5 AND PLACON = 'NPGC' ORDER BY CUENTA ASC");
            frDialogCombo combo = new frDialogCombo(dt, "CUENTA", "Selecciona la cuenta de origen");

            if (combo.ShowDialog() == DialogResult.OK)
            {
                dgvAsientos.DataSource = sql.cargarDatosTablaA3("SELECT CUENTAS.CUENTA, __ASIENTOS.IDCUENTA, __ASIENTOS.* FROM __ASIENTOS LEFT JOIN CUENTAS ON __ASIENTOS.IDCUENTA = CUENTAS.IDCUENTA WHERE CUENTAS.CUENTA = '" +csGlobal.comboFormValue + "'");
                cuenta_origen = csGlobal.comboFormValue;
            }

        }

        private void toolStripButtonCambiarCuenta_Click(object sender, EventArgs e)
        {
            if (dgvAsientos.SelectedRows.Count > 0)
            {
                //DataTable dt = sql.cargarDatosTablaA3("select distinct cuenta from cuentas where nivel = 5 and placon = 'NPGC' order by cuenta asc");
                DataTable dt = sql.cargarDatosTablaA3("SELECT DISTINCT CUENTAS.CUENTA FROM __ASIENTOS LEFT JOIN CUENTAS ON __ASIENTOS.IDCUENTA = CUENTAS.IDCUENTA WHERE NIVEL = 5 AND PLACON = 'NPGC' ORDER BY CUENTA ASC");
                frDialogCombo combo = new frDialogCombo(dt, "cuenta", "Selecciona la cuenta de destino");
                
                if (combo.ShowDialog() == DialogResult.OK)
                {
                    string cuenta_destino = csGlobal.comboFormValue;
                    if (MessageBox.Show("¿Estás seguro/a de querer realizar este cambio?\n\nCuenta origen: " + cuenta_origen+"\nCuenta destino: " + cuenta_destino, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach(DataGridViewRow dr in dgvAsientos.SelectedRows)
                        {
                            string idapunte = Convert.ToDouble(dr.Cells["IDAPUNTE"].Value.ToString()).ToString();
                            //string id_cuenta_origen = Convert.ToDouble(sql.obtenerCampoTabla("select idcuenta from cuentas where cuenta = '" + cuenta_origen + "' and placon = 'NPGC'")).ToString();
                            string id_cuenta_destino = Convert.ToDouble(sql.obtenerCampoTabla("select idcuenta from cuentas where cuenta = '" + cuenta_destino + "' and placon = 'NPGC'")).ToString();

                            csUtilidades.ejecutarConsulta("UPDATE __ASIENTOS SET IDCUENTA = '" + id_cuenta_destino + "' WHERE IDAPUNTE = '" + idapunte + "'", false);
                        }
                       

                        MessageBox.Show("El id de la cuenta del asiento ha sido cambiado");

                        dgvAsientos.DataSource = sql.cargarDatosTablaA3("SELECT CUENTAS.CUENTA, __ASIENTOS.IDCUENTA, __ASIENTOS.* FROM __ASIENTOS LEFT JOIN CUENTAS ON __ASIENTOS.IDCUENTA = CUENTAS.IDCUENTA WHERE CUENTAS.CUENTA = '" + cuenta_destino + "'");
                    }
                }                
            }
        }
    }
}
