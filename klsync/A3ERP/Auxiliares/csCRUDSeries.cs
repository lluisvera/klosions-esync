﻿using a3ERPActiveX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.A3ERP.Auxiliares
{
    class csCRUDSeries
    {
        private bool SerieExiste(string nomSerie)
        {
            try
            {
                string query = $"SELECT COUNT(*) FROM SERIES WHERE LTRIM(NOMSERIE) = '{nomSerie.Trim()}'";
                object result = csUtilidades.ejecutarConsultaScalar(query, false); 

                return result != null && Convert.ToInt32(result) > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al comprobar si la serie '{nomSerie}' existe: {ex.Message}");
                return false;
            }
        }


        //SIN SQL: Esta funcion hace uso de '.Filtro' para asignar que condicion debe comprobar, el '.Filtrado' aplica el filtro ya establecido sobre el objeto Maestro,
        //posteriormente se hace uso de '.EOF' para ver si se ha llegado al final del objeto Maestro (si no ha llegado quiere decir que ha encontrado algun valor con el condicional del filtro),
        //por lo que devuelve false y al aplicarse el '!' dara como resultado de la variable existe como true.

        //private bool SerieExiste(string nomSerie, Maestro a3maestro)
        //{
        //    try
        //    {
        //        a3maestro.Filtro = $"NOMSERIE = '{nomSerie}'";
        //        a3maestro.Filtrado = true;

        //        bool existe = !a3maestro.EOF;

        //        a3maestro.Filtrado = false;
        //        a3maestro.Filtro = "";

        //        return existe;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Error al comprobar la existencia del centro de coste '{nomSerie}': {ex.Message}");
        //        return false;
        //    }
        //}
        public void crearSerieA3(Objetos.csSeries[] series, string tipoObjeto, string campoExternoRepasat)
        {
            string errorSerie = "";
            string codigoSerie = "";

            csRepasatWebService rpstWS = new csRepasatWebService();
            Maestro a3maestro = new Maestro();
            a3maestro.Iniciar("series");
            csDismay frRep = new csDismay();
            try
            {
                for (int i = 0; i < series.Length; i++)
                {
                    if (SerieExiste(series[i].nomSerie))
                    {
                        continue;
                    }

                    a3maestro.OmitirMensajes = true;
                    a3maestro.Nuevo();

                    codigoSerie = string.IsNullOrEmpty(series[i].codExternoSerie) ? a3maestro.NuevoCodigoNum() : series[i].codExternoSerie;

                    a3maestro.set_AsString("SERIE", codigoSerie);
                    a3maestro.set_AsString("NOMSERIE", series[i].nomSerie);
                    a3maestro.set_AsString("NUMEROINICIAL", "1");
                    a3maestro.set_AsString("FACTURABORRADOR", "FALSE");
                    a3maestro.set_AsString("RPST_ID_SERIE", series[i].idSerie);
                    a3maestro.Guarda(true);
                    rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, series[i].idSerie, codigoSerie);
                }
            }
            catch (Exception ex)
            {
                errorSerie += $"{ex.Message}\n";
                MessageBox.Show(errorSerie);
            }
            finally 
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
        }
    }
}
