﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using a3ERPActiveX;

namespace klsync.A3ERP.Auxiliares
{
    class csCRUDCentroCoste
    {

        private bool CentroCosteExiste(string nomCentroCoste)
        {
            try
            {
                string query = $"SELECT COUNT(*) FROM CENTROSC WHERE LTRIM(CENTROCOSTE) = '{nomCentroCoste.Trim()}'";
                object result = csUtilidades.ejecutarConsultaScalar(query, false);

                return result != null && Convert.ToInt32(result) > 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show($"Error al comprobar si el centro de coste '{nomCentroCoste}' existe: {ex.Message}");
                return false;
            }
        }


        //SIN SQL: Esta funcion hace uso de '.Filtro' para asignar que condicion debe comprobar, el '.Filtrado' aplica el filtro ya establecido sobre el objeto Maestro,
        //posteriormente se hace uso de '.EOF' para ver si se ha llegado al final del objeto Maestro (si no ha llegado quiere decir que ha encontrado algun valor con el condicional del filtro),
        //por lo que devuelve false y al aplicarse el '!' dara como resultado de la variable existe como true.

        //private bool CentroCosteExiste(string nomCentroCoste, Maestro a3maestro)
        //{
        //    try
        //    {
        //        a3maestro.Filtro = $"CENTROCOSTE = '{nomCentroCoste}'";
        //        a3maestro.Filtrado = true;

        //        bool existe = !a3maestro.EOF;

        //        a3maestro.Filtrado = false;
        //        a3maestro.Filtro = ""; 

        //        return existe;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Error al comprobar la existencia del centro de coste '{nomCentroCoste}': {ex.Message}");
        //        return false;
        //    }
        //}



        public void crearCentroCosteA3(Objetos.csCostCenters[] centroCoste, string tipoObjeto, string campoExternoRepasat)
        {
            string errorCenCos = "";
            string codigoCenCos = "";

            csRepasatWebService rpstWS = new csRepasatWebService();
            Maestro a3maestro = new Maestro();
            a3maestro.Iniciar("centrosc");

            try
            {
                for (int i = 0; i < centroCoste.Length; i++)
                {
                    //SI EXISTE MIRA SI SE DEBE ACTUALIZAR
                    if (CentroCosteExiste(centroCoste[i].codExternoCentroCoste))
                    {
                        DateTime fechaModificacionRepasat;
                        bool conversionExitosa = DateTime.TryParse(centroCoste[i].fecModificacion, out fechaModificacionRepasat);

                        if (conversionExitosa)
                        {
                            DateTime? fechaModificacionA3 = ObtenerFechaModificacionA3(centroCoste[i].codExternoCentroCoste);

                            if (fechaModificacionRepasat != null && fechaModificacionA3 != null && fechaModificacionRepasat > fechaModificacionA3)
                            {
                                a3maestro.OmitirMensajes = true;
                                bool encontrado = a3maestro.Buscar(centroCoste[i].codExternoCentroCoste);

                                a3maestro.Edita();
                                a3maestro.set_AsString("DESCCENTRO", centroCoste[i].descCentroCoste);
                                a3maestro.set_AsString("APLINIVEL1", centroCoste[i].level1 == "1" ? "T" : "F");
                                a3maestro.set_AsString("APLINIVEL2", centroCoste[i].level2 == "1" ? "T" : "F");
                                a3maestro.set_AsString("APLINIVEL3", centroCoste[i].level3 == "1" ? "T" : "F");
                                a3maestro.set_AsString("BLOQUEADO", centroCoste[i].bloqueado);
                                a3maestro.set_AsString("OBSOLETO", centroCoste[i].obsoleto);
                                a3maestro.Guarda(true); 
                            }
                            else
                            {
                                Console.WriteLine($"La fecha de modificación en Repasat es mas vieja que la de a3erp en el centro de coste: {centroCoste[i].codExternoCentroCoste}");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"La fecha de modificación en Repasat no es válida para el centro de coste: {centroCoste[i].codExternoCentroCoste}");
                        }

                        continue;
                    }
                    else
                    {
                        // CREA CENTRO COSTE SI NO EXISTE
                        a3maestro.OmitirMensajes = true;

                        a3maestro.Nuevo();
                        codigoCenCos = string.IsNullOrEmpty(centroCoste[i].refCentroCoste) ? a3maestro.NuevoCodigoNum() : centroCoste[i].refCentroCoste;
                        a3maestro.set_AsString("CENTROCOSTE", codigoCenCos);
                        a3maestro.set_AsString("DESCCENTRO", centroCoste[i].descCentroCoste);
                        a3maestro.set_AsString("APLINIVEL1", centroCoste[i].level1 == "1" ? "T" : "F");
                        a3maestro.set_AsString("APLINIVEL2", centroCoste[i].level2 == "1" ? "T" : "F");
                        a3maestro.set_AsString("APLINIVEL3", centroCoste[i].level3 == "1" ? "T" : "F");
                        a3maestro.set_AsString("BLOQUEADO", centroCoste[i].bloqueado);
                        a3maestro.set_AsString("OBSOLETO", centroCoste[i].obsoleto);
                        a3maestro.set_AsString("RPST_ID_CENTROC", centroCoste[i].idCentroCoste);
                        a3maestro.Guarda(true);

                        rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, centroCoste[i].idCentroCoste, codigoCenCos);
                    }
                }
            }
            catch (Exception ex)
            {
                errorCenCos += $"{ex.Message}\n";
                MessageBox.Show(errorCenCos);
            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
        }

        private DateTime? ObtenerFechaModificacionA3(string codExternoCentroCoste)
        {
            DateTime? fecha = null;
            var sql = new csSqlConnects();
            string consulta = $"SELECT TOP 1 FECHA FROM REPLOG WHERE TABLA = 'CENTROSC' AND IDREG1 = '{codExternoCentroCoste}' ORDER BY FECHA DESC";
            
            DataTable resultado = sql.cargarDatosTablaA3(consulta);

            if (resultado != null && resultado.Rows.Count > 0)
            {
                object valorFecha = resultado.Rows[0]["FECHA"];
                if (valorFecha != DBNull.Value)
                {
                    fecha = Convert.ToDateTime(valorFecha);
                }
            }
            return fecha;
        }





        //public void crearCentroCosteA3(Objetos.csCostCenters[] centroCoste, string tipoObjeto, string campoExternoRepasat)
        //{
        //    string errorCenCos = "";
        //    string codigoCenCos = "";

        //    csRepasatWebService rpstWS = new csRepasatWebService();
        //    Maestro a3maestro = new Maestro();
        //    a3maestro.Iniciar("centrosc");
        //    //csDismay frRep = new csDismay();
        //    try
        //    {
        //        for (int i = 0; i < centroCoste.Length; i++)
        //        {
        //            if (CentroCosteExiste(centroCoste[i].codExternoCentroCoste))
        //            {
        //                //IMPLEMENTAR AQUI LA FUNCIONALIDAD DE ACTUALIZAR
        //                continue;
        //            }

        //            a3maestro.OmitirMensajes = true;
        //            a3maestro.Nuevo();

        //            codigoCenCos = string.IsNullOrEmpty(centroCoste[i].refCentroCoste) ? a3maestro.NuevoCodigoNum() : centroCoste[i].refCentroCoste;

        //            a3maestro.set_AsString("CENTROCOSTE", codigoCenCos);
        //            a3maestro.set_AsString("DESCCENTRO", centroCoste[i].descCentroCoste);
        //            a3maestro.set_AsString("APLINIVEL1", centroCoste[i].level1 == "1" ? "T" : "F");
        //            a3maestro.set_AsString("APLINIVEL2", centroCoste[i].level2 == "1" ? "T" : "F");
        //            a3maestro.set_AsString("APLINIVEL3", centroCoste[i].level3 == "1" ? "T" : "F");
        //            a3maestro.set_AsString("BLOQUEADO", centroCoste[i].bloqueado);
        //            a3maestro.set_AsString("OBSOLETO", centroCoste[i].obsoleto);
        //            a3maestro.set_AsString("RPST_ID_CENTROC", centroCoste[i].idCentroCoste);
        //            a3maestro.Guarda(true);
        //            rpstWS.actualizarDocumentoRepasat(tipoObjeto, campoExternoRepasat, centroCoste[i].idCentroCoste, codigoCenCos);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        errorCenCos += $"{ex.Message}\n";
        //        MessageBox.Show(errorCenCos);
        //    }
        //    finally
        //    {
        //        a3maestro.Acabar();
        //        a3maestro = null;
        //    }
        //}
    }
}
