﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a3ERPActiveX;
using System.Data;
using System.Windows.Forms;


namespace klsync.A3ERP
{
    class updateA3ERP
    {

        csa3erp a3ERP = new csa3erp();

        public void actualizarCuentas(Objetos.csTercero[] cuentas)
        {
            //a3ERP.abrirEnlace();
            string bloqueado = "";
            string errorNomCliente = "";
            string errorCodCliente = "";

            Maestro a3maestro = new Maestro();
            try
            {
                a3maestro.Iniciar("clientes");
                for (int i = 0; i < cuentas.Count(); i++)
                {
                    try
                    {
                        if (a3maestro.Buscar(cuentas[i].codIC.ToUpper()))
                        {

                            //gestión de errores
                            errorCodCliente = cuentas[i].codIC;
                            errorNomCliente = cuentas[i].nombre.ToUpper();

                            bloqueado = (cuentas[i].active_Status == "1") ? "F" : "T";
                            a3maestro.Edita();
                            a3maestro.AsString["NOMCLI"] = cuentas[i].nombre.ToUpper();
                            a3maestro.AsString["CLIALIAS"] = cuentas[i].alias.ToUpper();
                            a3maestro.AsString["RAZON"] = cuentas[i].razonSocial.ToUpper();
                            a3maestro.AsString["NOMFISCAL"] = cuentas[i].razonSocial.ToUpper();
                            a3maestro.AsString["NIFCLI"] = string.IsNullOrEmpty(cuentas[i].nifcif) ? "" : cuentas[i].nifcif.ToString();
                            a3maestro.AsString["BLOQUEADO"] = bloqueado;
                            a3maestro.AsString["BLOQUEADO_ORG"] = bloqueado;
                            a3maestro.AsString["ZONA"] = cuentas[i].zona.ToUpper();
                            a3maestro.AsString["RUTA"] = cuentas[i].ruta.ToUpper();
                            a3maestro.AsString["DIASPAGO1"] = cuentas[i].diaPago1.ToUpper();
                            a3maestro.AsString["DIASPAGO2"] = cuentas[i].diaPago2.ToUpper();
                            a3maestro.AsString["DIASPAGO3"] = cuentas[i].diaPago3.ToUpper();
                            a3maestro.AsString["DOCPAG"] = cuentas[i].codDocPago.ToUpper();
                            a3maestro.AsString["FORPAG"] = cuentas[i].codFormaPago.ToUpper();
                            a3maestro.AsString["OBSCLI"] = string.IsNullOrEmpty(cuentas[i].observaciones) ? "" : cuentas[i].observaciones.ToString();
                            a3maestro.AsString["REGIVA"] = string.IsNullOrEmpty(cuentas[i].regimenIva) ? "VNAC" : cuentas[i].regimenIva.ToUpper();
                            a3maestro.AsString["TELCLI"] = string.IsNullOrEmpty(cuentas[i].telf) ? "" : cuentas[i].telf.ToString();
                            a3maestro.AsString["TELEFONOFISCAL"] = string.IsNullOrEmpty(cuentas[i].telf) ? "" : cuentas[i].telf.ToString();
                            a3maestro.AsString["TELCLI2"] = string.IsNullOrEmpty(cuentas[i].telf2) ? "" : cuentas[i].telf2.ToString();
                            a3maestro.AsString["TELEFONO2FISCAL"] = string.IsNullOrEmpty(cuentas[i].telf2) ? "" : cuentas[i].telf2.ToString();
                            a3maestro.AsString["FAXCLI"] = string.IsNullOrEmpty(cuentas[i].fax) ? "" : cuentas[i].fax.ToString();
                            a3maestro.AsString["E_MAIL"] = string.IsNullOrEmpty(cuentas[i].email) ? "" : cuentas[i].email.ToString();
                            a3maestro.AsString["EMAILFISCAL"] = string.IsNullOrEmpty(cuentas[i].email) ? "" : cuentas[i].email.ToString();
                            a3maestro.AsString["PAGINAWEB"] = string.IsNullOrEmpty(cuentas[i].web) ? "" : cuentas[i].web.ToString();
                            a3maestro.AsString["CODREP"] = string.IsNullOrEmpty(cuentas[i].representante) ? "" : cuentas[i].representante.ToString();    //REPRESENTANTE O COMERCIAL
                            //DIRECCION
                            a3maestro.AsString["DIRCLI"] = string.IsNullOrEmpty(cuentas[i].direccion) ? "" : cuentas[i].direccion.ToString();
                            a3maestro.AsString["DIRCLI1"] = string.IsNullOrEmpty(cuentas[i].direccion) ? "" : cuentas[i].direccion.ToString();
                            a3maestro.AsString["POBCLI"] = string.IsNullOrEmpty(cuentas[i].poblacion) ? "" : cuentas[i].poblacion.ToString();
                            a3maestro.AsString["DTOCLI"] = string.IsNullOrEmpty(cuentas[i].codigoPostal) ? "" : cuentas[i].codigoPostal.ToString();
                            a3maestro.AsString["CODPROVI"] = string.IsNullOrEmpty(cuentas[i].codprovincia) ? "" : cuentas[i].codprovincia.ToString();
                            a3maestro.AsString["CODPAIS"] = string.IsNullOrEmpty(cuentas[i].codPais) ? "" : cuentas[i].codPais.ToString();
                            //DIRECCION FISCAL
                            a3maestro.AsString["DIRFISCAL"] = string.IsNullOrEmpty(cuentas[i].direccion) ? "" : cuentas[i].direccion.ToString();
                            a3maestro.AsString["VIAPUBLICAFISCAL"] = string.IsNullOrEmpty(cuentas[i].direccion) ? "" : cuentas[i].direccion.ToString();
                            a3maestro.AsString["POBFISCAL"] = string.IsNullOrEmpty(cuentas[i].poblacion) ? "" : cuentas[i].poblacion.ToString();
                            a3maestro.AsString["DTOFISCAL"] = string.IsNullOrEmpty(cuentas[i].codigoPostal) ? "" : cuentas[i].codigoPostal.ToString();
                            a3maestro.AsString["PROVIFISCAL"] = string.IsNullOrEmpty(cuentas[i].codprovincia) ? "" : cuentas[i].codprovincia.ToString();
                            a3maestro.AsString["PAISFISCAL"] = string.IsNullOrEmpty(cuentas[i].codPais) ? "" : cuentas[i].codPais.ToString();
                        }
                        a3maestro.Guarda(true);
                    }
                    catch (Exception ex)
                    {
                        (errorCodCliente + " " + errorNomCliente + "\n" + ex.Message).mb();
                    }
                    //CheckAcciones();
                    //Log.EscribirLog("Contacto del cliente 22 creado/modificado correctamente");
                }
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();

                //Solo actualizo si se hacen modificaciones masivas
                if (cuentas.Length > 1)
                {
                    updateData.actualizarFechaSync("accounts", cuentas.Length, true);
                }

            }
            catch (Exception ex)
            {
                (errorCodCliente + " " + errorNomCliente + "\n" +
                ex.Message).mb();
            }

            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
           // a3ERP.cerrarEnlace();


        }

        public void actualizarArticulos(Objetos.csArticulo[] articulos)
        {
            string codart = "";

            Maestro a3maestro = new Maestro();
            try
            {
                a3maestro.Iniciar("articulos");
                for (int i = 0; i < articulos.Count(); i++)
                {
                    codart = articulos[i].codart.ToUpper();
                    if (a3maestro.Buscar(codart))
                    {
                        a3maestro.Edita();
                        a3maestro.AsString["DESCART"] = articulos[i].descArt.ToUpper();
                        a3maestro.AsString["BLOQUEADO"] = articulos[i].bloqueado.ToUpper();
                        a3maestro.AsString["ARTALIAS"] = articulos[i].alias.ToUpper();
                        a3maestro.AsString["ESVENTA"] = articulos[i].esVenta.ToUpper();
                        a3maestro.AsString["ESCOMPRA"] = articulos[i].esCompra.ToUpper();
                        a3maestro.AsString["AFESTOCK"] = articulos[i].esStock.ToUpper();
                        a3maestro.AsString["PRCVENTA"] = articulos[i].precioSinIVA.ToString();
                        a3maestro.AsString["PRCCOMPRA"] = articulos[i].precioCompra.ToUpper();
                        a3maestro.AsString["PRCCOSTE"] = articulos[i].precioCoste.ToUpper();
                        a3maestro.AsString["CODPRO"] = string.IsNullOrEmpty(articulos[i].proveedorDefecto)?"": articulos[i].proveedorDefecto.ToString();
                        a3maestro.AsString["ARTPRO"] = articulos[i].refProveedor.ToUpper();
                        //a3maestro.AsString["TIPIVA"] = articulos[i].tipoIVAVenta.ToUpper();
                        //a3maestro.AsString["TIPIVACOMPRAS"] = articulos[i].tipoIVACompra.ToUpper();

                    }
                    else
                    {
                    }
                    a3maestro.Guarda(true);

                    //CheckAcciones();
                    //Log.EscribirLog("Contacto del cliente 22 creado/modificado correctamente");
                }
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                updateData.actualizarFechaSync("products", articulos.Length,true);

            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }
            // a3ERP.cerrarEnlace();

        }

        public void actualizarDirecciones(Objetos.csDireccion[] direcciones)
        {
            
            csSqlConnects sql = new csSqlConnects();
          
            string resultado = string.Empty;

            //Defino un objeto para guardar el cliente y la direccion porque para buscar la direccion y modificarla le tengo que pasar el código de cliente y el número de dirección

            string[] objetoDireccion = new string[2];
            string codCli = "";
            string numDireccion = "";
            
            try
            {
                

                for (int i = 0; i < direcciones.Count(); i++)
                {
                    Maestro a3maestro = new Maestro();
                    // a3maestro.Nuevo();
                    objetoDireccion = sql.obtenerObjetoDireccionA3ERP(direcciones[i].idDireccionRepasat.ToString());
                    codCli = objetoDireccion[0];
                    numDireccion = objetoDireccion[1];
                    // if (a3maestro.Buscar(direcciones[i].idDireccionERP.ToUpper()))
                    //if (a3maestro.Buscar(new object[] { "       1", "1" }))
                    //Si la dirección es la fiscal, para actualizar en A3ERP hay que hacerlo modificando el objeto cliente
                    //si la dirección no es la fiscal se actualiza el objeto dirent
                    if (direcciones[i].direccionfiscal)
                    {
                        a3maestro.Iniciar("clientes");
                        if (a3maestro.Buscar(codCli))
                        {
                            a3maestro.Edita();
                            a3maestro.AsString["DIRCLI1"] = direcciones[i].direccion1.ToUpper();
                            a3maestro.AsString["DIRCLI2"] = direcciones[i].direccion2.ToUpper();
                            a3maestro.AsString["TELCLI"] = string.IsNullOrEmpty(direcciones[i].telefono) ?"":direcciones[i].telefono.ToUpper();
                            a3maestro.AsString["DTOCLI"] = string.IsNullOrEmpty(direcciones[i].codigoPostal) ? "" : direcciones[i].codigoPostal.ToUpper(); 
                            a3maestro.AsString["POBCLI"] = string.IsNullOrEmpty(direcciones[i].poblacion) ? "" : direcciones[i].poblacion.ToUpper();
                            a3maestro.AsString["CODPROVI"] = string.IsNullOrEmpty(direcciones[i].codProvincia) ? "" : direcciones[i].codProvincia.ToUpper(); 
                            a3maestro.AsString["CODPAIS"] = string.IsNullOrEmpty(direcciones[i].codPais) ? "" : direcciones[i].codPais.ToUpper(); 
                        }
                        a3maestro.Guarda(true);
                        a3maestro.Acabar();
                        a3maestro = null;
                       
                    }
                    else
                    {
                        a3maestro.Iniciar("DIRENT");
                        if (a3maestro.Buscar(new object[] { codCli, numDireccion }))
                        {
                            //bloqueado = (cuentas[i].active_Status == "1") ? "F" : "T";
                            a3maestro.Edita();
                            a3maestro.AsString["NOMENT"] = "DIRECCION EDITADA";
                            a3maestro.AsString["DIRENT1"] = direcciones[i].direccion1.ToUpper();
                            a3maestro.AsString["DIRENT2"] = direcciones[i].direccion2.ToUpper();
                            a3maestro.AsString["TELENT1"] = string.IsNullOrEmpty(direcciones[i].telefono)? "": direcciones[i].telefono.ToUpper();
                            a3maestro.AsString["DTOENT"] = string.IsNullOrEmpty(direcciones[i].codigoPostal) ? "" : direcciones[i].codigoPostal.ToUpper();
                            a3maestro.AsString["POBENT"] = direcciones[i].poblacion.ToUpper();
                            a3maestro.AsString["CODPROVI"] = string.IsNullOrEmpty(direcciones[i].codProvincia) ? "" : direcciones[i].codProvincia.ToUpper();
                            a3maestro.AsString["CODPAIS"] = string.IsNullOrEmpty(direcciones[i].pais) ? "" : direcciones[i].pais.ToUpper();
                            a3maestro.AsString["DEFECTO"] = "F";
                        }
                        a3maestro.Guarda(true);
                        a3maestro.Acabar();
                        a3maestro = null;
                    }

                    //CheckAcciones();
                    //Log.EscribirLog("Contacto del cliente 22 creado/modificado correctamente");
                }
                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                updateData.actualizarFechaSync("addresses", direcciones.Length, true);

            }
            catch (Exception ex)
            {
            }
        }

        public void actualizarContactos(Objetos.csContactos[] contactos, bool esUpdate=false, string codCuentaA3Ref=null, bool ventas=true)
        {
            csRepasatWebService rpstWS = new csRepasatWebService();
            csSqlConnects sql = new csSqlConnects();

            string resultado = string.Empty;

            string[] objetoDireccion = new string[2];

            //Si A3ERP está en versión 12 y posteriores, la actualización de contactos es diferente
            string tipoMaestro = csGlobal.versionA3ERP < 12 ? "CONTACTOS" : "__CONTACTOS";

            try
            {   //Obtener Número de Organización según cliente o proveedor
                string codCuentaA3 = "";
                string idOrgCuenta = "";
                string idContactoRPST = "";
                string idOrganizacionA3ERP = "";
                int numContactoCuenta = 0;
                string codigo = "";
                string idContactoA3ERP = "";       

                DataTable dtContactos = new DataTable();
                dtContactos.Columns.Add("CODIGO");
                dtContactos.Columns.Add("CUENTA");
                dtContactos.Columns.Add("NOMBRE");
                dtContactos.Columns.Add("APELLIDOS");
                dtContactos.Columns.Add("MOTIVO");



                for (int i = 0; i < contactos.Count(); i++)
                {

                    if (codCuentaA3Ref != null)
                    {
                        if (codCuentaA3Ref != contactos[i].idClienteRepasat)
                        {
                            continue;
                        }
                    }

                    Maestro a3maestro = new Maestro();
                    codCuentaA3 = contactos[i].codClienteERP;
                    string scriptQuery = ventas ? "SELECT IDORG FROM __CLIENTES WHERE LTRIM(CODCLI)='" + codCuentaA3 + "'" : "SELECT IDORG FROM __PROVEED WHERE LTRIM(CODPRO)='" + codCuentaA3 + "'";


                    idOrgCuenta = sql.obtenerCampoTabla(scriptQuery);
                    //idOrgCuenta = "1";

                    //Si el contacto pertenece a una cuenta/cliente que no está dado de alta en A3ERP pasamos al siguiente contacto
                    if (string.IsNullOrEmpty(idOrgCuenta))
                    {
                        DataRow drContacto = dtContactos.NewRow();
                        drContacto["CODIGO"]= contactos[i].codClienteERP;
                        drContacto["CUENTA"] = contactos[i].razonSocialCuenta;
                        drContacto["NOMBRE"] = contactos[i].nombre;
                        drContacto["APELLIDOS"] = contactos[i].apellidos;
                        drContacto["MOTIVO"] = "Cuenta no dada de alta en A3ERP";
                        dtContactos.Rows.Add(drContacto);

                        continue;
                    }

                    if (esUpdate)
                    {
                        numContactoCuenta = Convert.ToInt32(sql.obtenerCampoTabla("SELECT NUMCON AS NUMCONTACTO  FROM CONTACTOS WHERE RPST_ID_CONTACT='" + idContactoRPST + "'"));
                    }
                    else
                    {
                        numContactoCuenta = sql.obtenerCampoTabla("SELECT MAX(NUMCON)+1 AS NUMCONTACTO  FROM CONTACTOS WHERE TABLA='ORG' AND LTRIM(CODIGO)='" + idOrgCuenta + "'")==""? 1 : Convert.ToInt32(sql.obtenerCampoTabla("SELECT MAX(NUMCON)+1 AS NUMCONTACTO  FROM CONTACTOS WHERE TABLA='ORG' AND LTRIM(CODIGO)='" + idOrgCuenta + "'"));
                    }


                    a3maestro.Iniciar(tipoMaestro);

                    if (esUpdate)
                    {
                        if (a3maestro.Buscar(new object[] { "ORG", idOrgCuenta, numContactoCuenta }))
                        {
                            a3maestro.Edita();
                            a3maestro.AsString["NOMBRE"] = contactos[i].nombre;
                            a3maestro.AsString["EMAIL"] = contactos[i].email;
                            a3maestro.AsString["TELEFONO1"] = contactos[i].telf1;
                            a3maestro.AsString["TELEFONO2"] = contactos[i].telf2;
                        }
                    }
                    else
                    {
                        a3maestro.Nuevo();
                        if (csGlobal.versionA3ERP<12)
                        {
                            a3maestro.AsString["TABLA"] = "ORG";
                            a3maestro.AsInteger["NUMCON"] = numContactoCuenta;
                        }

                        string nuevoCodigo = a3maestro.NuevoCodigoNum();
                        //a3maestro.AsString["TABLA"] = "ORG";
                        a3maestro.AsString["CODIGO"] = nuevoCodigo.Trim(); ;
                        a3maestro.AsString["NOMBRE"] = contactos[i].nombre;
                        a3maestro.AsString["EMAIL"] = contactos[i].email.ToLower();
                        a3maestro.AsString["TELEFONO1"] = contactos[i].telf1;
                        a3maestro.AsString["TELEFONO2"] = contactos[i].telf2;
                        a3maestro.AsString["RPST_ID_CONTACT"] = contactos[i].idContactoRepasat;
                    }
                    a3maestro.Guarda(true);
                    codigo = a3maestro.get_AsString("CODIGO").Trim();
                    a3maestro.Acabar();
                    a3maestro = null;

                    if (csGlobal.versionA3ERP > 11)
                    {
                        idContactoA3ERP= sql.obtenerCampoTabla("SELECT ID FROM __CONTACTOS WHERE LTRIM(CODIGO)='" + codigo.Trim() + "'");
                        IContactoRelacion contactRel = new ContactoRelacion();
                        contactRel.Iniciar();
                        contactRel.Nuevo(Convert.ToInt32(idContactoA3ERP), ContactoRelacionEntidad.tcreCliente, idOrgCuenta);
                        contactRel.Guardar();
                    }
                    //Recupero el id de contacto creado para pasarlo a Repasat
                    rpstWS.actualizarDocumentoRepasat("contacts", "contact[codExternoContacto]", contactos[i].idContactoRepasat.ToString(), codigo);
                }

                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                updateData.actualizarFechaSync("contacts", contactos.Length, true);

                //Notifico que hay contactos que no cumplen
                string contactosIncidencias = "";

                foreach (DataRow fila in dtContactos.Rows)
                {
                    contactosIncidencias += "CLIENTE:" + "\n" + fila["CODIGO"] + " | " + fila["CUENTA"] + "\n" +
                                    fila["NOMBRE"] + " | " + fila["APELLIDOS"] +
                                    "MOTIVO: " + fila["MOTIVO"] +
                                    "\n" + "------------------" + "\n";
                }

                if (!string.IsNullOrEmpty(contactosIncidencias))
                {
                    contactosIncidencias = "No se han podido crear los siguientes contacto/s:" + "\n\n" + contactosIncidencias;
                    MessageBox.Show(contactosIncidencias);
                }


            }

            catch (Exception ex)
            {
            }
        }

    }
}
