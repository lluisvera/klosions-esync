﻿namespace klsync.A3ERP.Accounting
{
    partial class frBudget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnImportBudgetToA3ERP = new System.Windows.Forms.Button();
            this.btLoadBudget = new System.Windows.Forms.Button();
            this.dgvBudget = new System.Windows.Forms.DataGridView();
            this.btnLoadBudget = new System.Windows.Forms.Button();
            this.lblNivelesCCoste = new System.Windows.Forms.Label();
            this.tbNivelesAnalitica = new System.Windows.Forms.TextBox();
            this.tbCCoste1Def = new System.Windows.Forms.TextBox();
            this.lblNomCCoste1 = new System.Windows.Forms.Label();
            this.tbCCoste2Def = new System.Windows.Forms.TextBox();
            this.lblNomCCoste2 = new System.Windows.Forms.Label();
            this.tbCCoste3Def = new System.Windows.Forms.TextBox();
            this.lblNomCCoste3 = new System.Windows.Forms.Label();
            this.cboxEjercicios = new System.Windows.Forms.ComboBox();
            this.lblEjercicio = new System.Windows.Forms.Label();
            this.btnDeleteBudget = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBudget)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnDeleteBudget);
            this.splitContainer1.Panel1.Controls.Add(this.lblEjercicio);
            this.splitContainer1.Panel1.Controls.Add(this.cboxEjercicios);
            this.splitContainer1.Panel1.Controls.Add(this.tbCCoste3Def);
            this.splitContainer1.Panel1.Controls.Add(this.lblNomCCoste3);
            this.splitContainer1.Panel1.Controls.Add(this.tbCCoste2Def);
            this.splitContainer1.Panel1.Controls.Add(this.lblNomCCoste2);
            this.splitContainer1.Panel1.Controls.Add(this.tbCCoste1Def);
            this.splitContainer1.Panel1.Controls.Add(this.lblNomCCoste1);
            this.splitContainer1.Panel1.Controls.Add(this.tbNivelesAnalitica);
            this.splitContainer1.Panel1.Controls.Add(this.lblNivelesCCoste);
            this.splitContainer1.Panel1.Controls.Add(this.btnLoadBudget);
            this.splitContainer1.Panel1.Controls.Add(this.btnImportBudgetToA3ERP);
            this.splitContainer1.Panel1.Controls.Add(this.btLoadBudget);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvBudget);
            this.splitContainer1.Size = new System.Drawing.Size(972, 469);
            this.splitContainer1.SplitterDistance = 236;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnImportBudgetToA3ERP
            // 
            this.btnImportBudgetToA3ERP.Image = global::klsync.adMan.Resources.a3erp;
            this.btnImportBudgetToA3ERP.Location = new System.Drawing.Point(15, 247);
            this.btnImportBudgetToA3ERP.Name = "btnImportBudgetToA3ERP";
            this.btnImportBudgetToA3ERP.Size = new System.Drawing.Size(130, 41);
            this.btnImportBudgetToA3ERP.TabIndex = 1;
            this.btnImportBudgetToA3ERP.Text = "Importar >>>";
            this.btnImportBudgetToA3ERP.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnImportBudgetToA3ERP.UseVisualStyleBackColor = true;
            this.btnImportBudgetToA3ERP.Click += new System.EventHandler(this.btnImportBudgetToA3ERP_Click);
            // 
            // btLoadBudget
            // 
            this.btLoadBudget.Image = global::klsync.adMan.Resources.microsoft_excel;
            this.btLoadBudget.Location = new System.Drawing.Point(15, 200);
            this.btLoadBudget.Name = "btLoadBudget";
            this.btLoadBudget.Size = new System.Drawing.Size(130, 41);
            this.btLoadBudget.TabIndex = 0;
            this.btLoadBudget.Text = "Cargar Presupuesto";
            this.btLoadBudget.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btLoadBudget.UseVisualStyleBackColor = true;
            this.btLoadBudget.Click += new System.EventHandler(this.btLoadBudget_Click);
            // 
            // dgvBudget
            // 
            this.dgvBudget.AllowUserToAddRows = false;
            this.dgvBudget.AllowUserToDeleteRows = false;
            this.dgvBudget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBudget.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBudget.Location = new System.Drawing.Point(0, 0);
            this.dgvBudget.Name = "dgvBudget";
            this.dgvBudget.ReadOnly = true;
            this.dgvBudget.Size = new System.Drawing.Size(732, 469);
            this.dgvBudget.TabIndex = 0;
            this.dgvBudget.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvBudget_MouseUp);
            // 
            // btnLoadBudget
            // 
            this.btnLoadBudget.Location = new System.Drawing.Point(15, 324);
            this.btnLoadBudget.Name = "btnLoadBudget";
            this.btnLoadBudget.Size = new System.Drawing.Size(130, 41);
            this.btnLoadBudget.TabIndex = 2;
            this.btnLoadBudget.Text = "Cargar Presupuesto de A3ERP";
            this.btnLoadBudget.UseVisualStyleBackColor = true;
            this.btnLoadBudget.Click += new System.EventHandler(this.btnLoadBudget_Click);
            // 
            // lblNivelesCCoste
            // 
            this.lblNivelesCCoste.AutoSize = true;
            this.lblNivelesCCoste.Location = new System.Drawing.Point(12, 28);
            this.lblNivelesCCoste.Name = "lblNivelesCCoste";
            this.lblNivelesCCoste.Size = new System.Drawing.Size(87, 13);
            this.lblNivelesCCoste.TabIndex = 3;
            this.lblNivelesCCoste.Text = "Niveles Analítica";
            // 
            // tbNivelesAnalitica
            // 
            this.tbNivelesAnalitica.Enabled = false;
            this.tbNivelesAnalitica.Location = new System.Drawing.Point(112, 25);
            this.tbNivelesAnalitica.Name = "tbNivelesAnalitica";
            this.tbNivelesAnalitica.Size = new System.Drawing.Size(33, 20);
            this.tbNivelesAnalitica.TabIndex = 4;
            this.tbNivelesAnalitica.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCCoste1Def
            // 
            this.tbCCoste1Def.Enabled = false;
            this.tbCCoste1Def.Location = new System.Drawing.Point(112, 53);
            this.tbCCoste1Def.Name = "tbCCoste1Def";
            this.tbCCoste1Def.Size = new System.Drawing.Size(70, 20);
            this.tbCCoste1Def.TabIndex = 6;
            // 
            // lblNomCCoste1
            // 
            this.lblNomCCoste1.AutoSize = true;
            this.lblNomCCoste1.Location = new System.Drawing.Point(12, 56);
            this.lblNomCCoste1.Name = "lblNomCCoste1";
            this.lblNomCCoste1.Size = new System.Drawing.Size(37, 13);
            this.lblNomCCoste1.TabIndex = 5;
            this.lblNomCCoste1.Text = "Nivel1";
            // 
            // tbCCoste2Def
            // 
            this.tbCCoste2Def.Enabled = false;
            this.tbCCoste2Def.Location = new System.Drawing.Point(112, 79);
            this.tbCCoste2Def.Name = "tbCCoste2Def";
            this.tbCCoste2Def.Size = new System.Drawing.Size(70, 20);
            this.tbCCoste2Def.TabIndex = 8;
            // 
            // lblNomCCoste2
            // 
            this.lblNomCCoste2.AutoSize = true;
            this.lblNomCCoste2.Location = new System.Drawing.Point(12, 82);
            this.lblNomCCoste2.Name = "lblNomCCoste2";
            this.lblNomCCoste2.Size = new System.Drawing.Size(37, 13);
            this.lblNomCCoste2.TabIndex = 7;
            this.lblNomCCoste2.Text = "Nivel2";
            // 
            // tbCCoste3Def
            // 
            this.tbCCoste3Def.Enabled = false;
            this.tbCCoste3Def.Location = new System.Drawing.Point(112, 105);
            this.tbCCoste3Def.Name = "tbCCoste3Def";
            this.tbCCoste3Def.Size = new System.Drawing.Size(70, 20);
            this.tbCCoste3Def.TabIndex = 10;
            // 
            // lblNomCCoste3
            // 
            this.lblNomCCoste3.AutoSize = true;
            this.lblNomCCoste3.Location = new System.Drawing.Point(12, 108);
            this.lblNomCCoste3.Name = "lblNomCCoste3";
            this.lblNomCCoste3.Size = new System.Drawing.Size(37, 13);
            this.lblNomCCoste3.TabIndex = 9;
            this.lblNomCCoste3.Text = "Nivel3";
            // 
            // cboxEjercicios
            // 
            this.cboxEjercicios.FormattingEnabled = true;
            this.cboxEjercicios.Location = new System.Drawing.Point(112, 142);
            this.cboxEjercicios.Name = "cboxEjercicios";
            this.cboxEjercicios.Size = new System.Drawing.Size(70, 21);
            this.cboxEjercicios.TabIndex = 11;
            // 
            // lblEjercicio
            // 
            this.lblEjercicio.AutoSize = true;
            this.lblEjercicio.Location = new System.Drawing.Point(12, 145);
            this.lblEjercicio.Name = "lblEjercicio";
            this.lblEjercicio.Size = new System.Drawing.Size(47, 13);
            this.lblEjercicio.TabIndex = 12;
            this.lblEjercicio.Text = "Ejercicio";
            // 
            // btnDeleteBudget
            // 
            this.btnDeleteBudget.Enabled = false;
            this.btnDeleteBudget.Location = new System.Drawing.Point(15, 371);
            this.btnDeleteBudget.Name = "btnDeleteBudget";
            this.btnDeleteBudget.Size = new System.Drawing.Size(130, 41);
            this.btnDeleteBudget.TabIndex = 13;
            this.btnDeleteBudget.Text = "Borrar Presupuesto de A3ERP";
            this.btnDeleteBudget.UseVisualStyleBackColor = true;
            this.btnDeleteBudget.Click += new System.EventHandler(this.btnDeleteBudget_Click);
            // 
            // frBudget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 469);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frBudget";
            this.Text = "Presupuesto";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBudget)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnImportBudgetToA3ERP;
        private System.Windows.Forms.Button btLoadBudget;
        private System.Windows.Forms.DataGridView dgvBudget;
        private System.Windows.Forms.Button btnLoadBudget;
        private System.Windows.Forms.TextBox tbCCoste3Def;
        private System.Windows.Forms.Label lblNomCCoste3;
        private System.Windows.Forms.TextBox tbCCoste2Def;
        private System.Windows.Forms.Label lblNomCCoste2;
        private System.Windows.Forms.TextBox tbCCoste1Def;
        private System.Windows.Forms.Label lblNomCCoste1;
        private System.Windows.Forms.TextBox tbNivelesAnalitica;
        private System.Windows.Forms.Label lblNivelesCCoste;
        private System.Windows.Forms.Label lblEjercicio;
        private System.Windows.Forms.ComboBox cboxEjercicios;
        private System.Windows.Forms.Button btnDeleteBudget;
    }
}