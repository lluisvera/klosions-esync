﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExcelDataReader;

namespace klsync.A3ERP.Accounting
{
    public partial class frBudget : Form
    {
        private static frBudget m_FormDefInstance;
        public static frBudget DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frBudget();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frBudget()
        {
            InitializeComponent();
            loadCentroCostes();
            string ejercicio = DateTime.Now.Year.ToString();
            for (int i = -5; i < 10; i++) {
                cboxEjercicios.Items.Add(DateTime.Now.AddYears(i).Year.ToString());
            }

            cboxEjercicios.Text = DateTime.Now.Year.ToString();
            btnImportBudgetToA3ERP.Enabled = false;


        }

        private void btLoadBudget_Click(object sender, EventArgs e)
        {
            loadBudget();
            if (dgvBudget.Rows.Count > 0) btnImportBudgetToA3ERP.Enabled = true;

        }

        private DataSet result;
        private DataTable datos;
        private string rutaArchivo;
        public Objetos.csCabeceraDoc[] cabeceras = null;
        public Objetos.csLineaDocumento[] lineas = null;
        public int contadorFras = 0;
        csSqlConnects sql = new csSqlConnects();
        int nivelesAnalitica = 0;
        string nomCentroCoste1 = "";
        string nomCentroCoste2 = "";
        string nomCentroCoste3 = "";
        string centroCoste1Def = "";
        string centroCoste2Def = "";
        string centroCoste3Def = "";


        private void loadCentroCostes() {
            try
            {
                DataTable dtCostCentersData = sql.obtenerDatosSQLScript("SELECT NIVELES,NOMCENTRO1, CENTROCOSTE, NOMCENTRO2,CENTROCOSTE2, NOMCENTRO3, CENTROCOSTE3 FROM DATOSCON");
                nivelesAnalitica = Convert.ToInt16( dtCostCentersData.Rows[0]["NIVELES"].ToString());
                nomCentroCoste1 = dtCostCentersData.Rows[0]["NOMCENTRO1"].ToString();
                nomCentroCoste2 = dtCostCentersData.Rows[0]["NOMCENTRO2"].ToString();
                nomCentroCoste3 = dtCostCentersData.Rows[0]["NOMCENTRO3"].ToString();
                centroCoste1Def = dtCostCentersData.Rows[0]["CENTROCOSTE"].ToString();
                centroCoste2Def = dtCostCentersData.Rows[0]["CENTROCOSTE2"].ToString();
                centroCoste3Def = dtCostCentersData.Rows[0]["CENTROCOSTE3"].ToString();

                tbNivelesAnalitica.Text = nivelesAnalitica.ToString();
                lblNomCCoste1.Text = nomCentroCoste1;
                lblNomCCoste2.Text = nomCentroCoste2;
                lblNomCCoste3.Text = nomCentroCoste3;
                tbCCoste1Def.Text = centroCoste1Def;
                tbCCoste2Def.Text = centroCoste2Def;
                tbCCoste3Def.Text = centroCoste3Def;

            }
            catch (Exception ex) {

            }
        }


        private void loadBudget()
        {

            if (dgvBudget.Rows.Count > 0)
            {
                this.dgvBudget.DataSource = null;
                dgvBudget.Rows.Clear();
            }

            Stream myStream;
            OpenFileDialog abrirArchivo = new OpenFileDialog();


            //Con el GetFolderPath recuperamos la ruta de la carpeta Documentos (MyDocuments)
            abrirArchivo.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            abrirArchivo.Filter = "CSV files (*.csv)|*.csv|Excel Files|*.xls;*.xlsx";
            abrirArchivo.FilterIndex = 2;
            abrirArchivo.RestoreDirectory = true;
            if (abrirArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                rutaArchivo = abrirArchivo.FileName;
                try
                {


                    if ((myStream = abrirArchivo.OpenFile()) != null)
                    {
                        //Dataset completo
                        result = leerExcel(myStream);
                        datos = this.result.Tables[0];

                        //Validación del formato del excel. Se comprueba el numero de columnas y el nombre de la primera.
                        if (datos.Columns.Count >= 18)
                        {
                            transformarFichero();
                            //dateFormat();

                            dgvBudget.DataSource = this.datos;
                            cellFomat();
                            dgvBudget.AutoResizeColumns();

                            //textBox1.Text = abrirArchivo.SafeFileName;
                            //traspasar.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Formato de Excel incorrecto. ");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cierre el documento previamente. ");
                }
            }
        }

        private DataSet leerExcel(Stream stream)
        {
            var result = new DataSet();

            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                result = reader.AsDataSet();
                return result;
            }
        }

        private void transformarFichero()
        {
            for (int i = 0; i < datos.Columns.Count; i++)
            {
                datos.Columns["Column" + i.ToString()].ColumnName = datos.Rows[0].ItemArray[i].ToString() != "" ? datos.Rows[0].ItemArray[i].ToString().ToUpper() : "Column" + i.ToString();
            }
            datos.Rows[0].Delete();
            datos.AcceptChanges();
        }

        private void cellFomat()
        {
            foreach (DataGridViewColumn columna in dgvBudget.Columns)
            {
                if (columna.Name.Contains("MES"))
                {
                    dgvBudget.Columns[columna.Name].DefaultCellStyle.Format = "N2";
                    dgvBudget.Columns[columna.Name].ValueType = typeof(Decimal);
                    dgvBudget.Columns[columna.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }
        }

        private void btnImportBudgetToA3ERP_Click(object sender, EventArgs e)
        {
            if (csGlobal.conexionDB.Contains("BUDGETPURES"))
            {
                importarObjetivos();
            }
            else
            {
                importarPresupuesto();
            }
            //btnImportBudgetToA3ERP.Enabled = false;
        }


        private void importarPresupuesto() {
            try
            {



                DialogResult Resultado = MessageBox.Show("¿QUIERES BORRAR LOS DATOS DEL PRESUPUESTO EXISTENTES?", "BORRAR PRESUPUESTO EXISTENTE", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Resultado == DialogResult.Yes)
                {
                    string scriptDeleteBudgetHeader = "DELETE FROM PRESUP WHERE EJERCICIO=2023 AND CENTROCOSTE='       1' AND CENTROCOSTE2='       1' AND CENTROCOSTE3 ='       1'";
                    sql.consultaInsertar(scriptDeleteBudgetHeader);
                    string scriptDeleteBudgetDetail = "DELETE FROM LINEPRES WHERE EJERCICIO=2023 AND CENTROCOSTE='       1' AND CENTROCOSTE2='       1' AND CENTROCOSTE3 ='       1'";
                    sql.consultaInsertar(scriptDeleteBudgetDetail);

                }

                string cuenta = "";
                string ejercicio = "";
                double[] importeMens = new double[12];
                string scriptBudgetMes = "INSERT INTO LINEPRES (CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, CODMON, CUENTA, EJERCICIO, IMPORTE, MES, PESO, TIPOCONT) VALUES ";
                string[] scriptBudgetMesValues = new string[12];
                double totalFila = 0;
                int mes = 0;
                string columName = "";
                string query = "";
                string centroCoste1 = "";
                string centroCoste2 = "";
                string centroCoste3 = "";


                foreach (DataGridViewRow fila in dgvBudget.Rows)
                {
                    cuenta = fila.Cells["CUENTA"].Value.ToString();
                    ejercicio = fila.Cells["EJERCICIO"].Value.ToString();
                    //columName = dgvBudget.Columns[fila.Index].Name;
                    centroCoste1 = fila.Cells["CC1"].Value.ToString();
                    centroCoste1 = csUtilidades.comprobarEsNumero(centroCoste1) ? centroCoste1.PadLeft(8) : centroCoste1;
                    centroCoste2 = fila.Cells["CC2"].Value.ToString();
                    centroCoste2 = csUtilidades.comprobarEsNumero(centroCoste2) ? centroCoste2.PadLeft(8) : centroCoste2;
                    centroCoste3 = fila.Cells["CC3"].Value.ToString();
                    centroCoste3 = csUtilidades.comprobarEsNumero(centroCoste3) ? centroCoste3.PadLeft(8) : centroCoste3;





                    foreach (DataGridViewColumn columna in dgvBudget.Columns)
                    {
                        if (columna.Name.Contains("MES"))
                        {
                            importeMens[mes] = Convert.ToDouble(             
                                string.IsNullOrEmpty( fila.Cells[columna.Name].Value.ToString())?"0": fila.Cells[columna.Name].Value.ToString()

                                );
                            totalFila += importeMens[mes];
                            scriptBudgetMesValues[mes] = "('"+ centroCoste1 + "','" + centroCoste2 + "','" + centroCoste3 + "','EURO'," + cuenta + "," + ejercicio + "," + importeMens[mes].ToString().Replace(',','.') + "," + (mes + 1) + ",1,1)";
                            sql.ejecutarQuery(scriptBudgetMes + scriptBudgetMesValues[mes]);
                            mes++;



                        }
                        if (mes == 12)
                        {
                            query = "INSERT INTO PRESUP (CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, CODMON, CUENTA, EJERCICIO, IMPORTE, TIPOCONT) VALUES " +
                            "('" + centroCoste1 + "','" + centroCoste2 + "','" + centroCoste3 + "','EURO'," + cuenta + ", 2023," + totalFila.ToString().Replace(',', '.') + ",1)";
                            sql.ejecutarQuery(query);


                            mes = 0; totalFila = 0;
                            Array.Clear(importeMens, 0, 12);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void importarObjetivos()
        {
            string cliente = "";
            string ejercicio = "";
            double[] importeMens = new double[12];
            string scriptBudgetMes = "INSERT INTO KLSOBJETIVOS (FECHA,CODCLI,CODZONA,CODMARCA,IMPORTE) VALUES ";
            string[] scriptBudgetMesValues = new string[12];
            double totalFila = 0;
            int mes = 0;
            string columName = "";
            string query = "";
            string codZona = "";
            string codMarca = "";
            string centroCoste3 = "";

           try
            {



                DialogResult Resultado = MessageBox.Show("¿QUIERES BORRAR LOS DATOS DEL PRESUPUESTO EXISTENTES?", "BORRAR PRESUPUESTO EXISTENTE", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (Resultado == DialogResult.Yes)
                {
                    string scriptDeleteObjetivos = "DELETE FROM KLSOBJETIVOS";
                    sql.consultaInsertar(scriptDeleteObjetivos);

                }



                foreach (DataGridViewRow fila in dgvBudget.Rows)
                {
                    cliente = fila.Cells["CODCLI"].Value.ToString();
                    //ejercicio = fila.Cells["EJERCICIO"].Value.ToString();
                    //columName = dgvBudget.Columns[fila.Index].Name;
                    codZona = fila.Cells["CODZONA"].Value.ToString();
                    codZona = csUtilidades.comprobarEsNumero(codZona) ? codZona.PadLeft(8) : codZona;
                    codMarca = fila.Cells["CODMARCA"].Value.ToString();
                    codMarca = csUtilidades.comprobarEsNumero(codMarca) ? codMarca.PadLeft(8) : codMarca;
                    //centroCoste3 = fila.Cells["CC3"].Value.ToString();
                    //centroCoste3 = csUtilidades.comprobarEsNumero(centroCoste3) ? centroCoste3.PadLeft(8) : centroCoste3;



                    string[] meses = { "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE","DICIEMBRE" };

                    foreach (DataGridViewColumn columna in dgvBudget.Columns)
                    {
                        switch (columna.Name)
                        { 
                            case "ENERO":
                                mes = 0;
                                break;
                            case "FEBRERO":
                                mes = 1;
                                break;
                            case "MARZO":
                                mes = 2;
                                break;
                            case "ABRIL":
                                mes = 3;
                                break;
                            case "MAYO":
                                mes = 4;
                                break;
                            case "JUNIO":
                                mes = 5;
                                break;
                            case "JULIO":
                                mes = 6;
                                break;
                            case "AGOSTO":
                                mes = 7;
                                break;
                            case "SEPTIEMBRE":
                                mes = 8;
                                break;
                            case "OCTUBRE":
                                mes = 9;
                                break;
                            case "NOVIEMBRE":
                                mes = 10;
                                break;
                            case "DICIEMBRE":
                                mes = 11;
                                break;
                        }
                        if (meses.Contains(columna.Name))
                        {
                            DateTime fecha = new DateTime(year: 2024, month: mes+1, day: 1);

                            try
                            {
                                importeMens[mes] = Convert.ToDouble(
                                    string.IsNullOrEmpty(fila.Cells[columna.Name].Value.ToString()) ? "0" : fila.Cells[columna.Name].Value.ToString()

                                    );
                                importeMens[mes] = Math.Round(importeMens[mes], 2);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            if (importeMens[mes] == 0) continue;

                            totalFila += importeMens[mes];
                            scriptBudgetMesValues[mes] = "('" + fecha + "','" + cliente + "','" + codZona + "','" + codMarca + "'," + importeMens[mes].ToString().Replace(',', '.') + ")";
                            sql.ejecutarQuery(scriptBudgetMes + scriptBudgetMesValues[mes]);
                        }
                        if (columna.Name == "DICIEMBRE")
                        {
                            totalFila = 0;
                            Array.Clear(importeMens, 0, 12);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                sql.cerrarConexion();
            }


        }

        private void btnLoadBudget_Click(object sender, EventArgs e)
        {
            loadBudgetFromA3();
        }




        private void loadBudgetFromA3() {
            try
            {

                string scriptLoadBudget = "SELECT EJERCICIO, CUENTA, DESCCUE, CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, " +
                                            " [1] AS MES1, [2] AS MES2,[3] AS MES3, [4] AS MES4, [5] AS MES5, [6] AS MES6, " +
                                            " [7] AS MES7, [8] AS MES8, [9] AS MES9, [10] AS MES10, [11] AS MES11, [12] AS MES12 FROM " +
                                            " ( " +
                                            " SELECT " +
                                            " LINEPRES.CUENTA, DESCCUE, EJERCICIO, CENTROCOSTE, CENTROCOSTE2, CENTROCOSTE3, IMPORTE, MES " +
                                            " FROM LINEPRES INNER JOIN CUENTAS " +
                                            " ON LINEPRES.CUENTA = CUENTAS.CUENTA " +
                                            " WHERE PLACON = 'NPGC' " +
                                            " ) AS SOURCE " +
                                            " PIVOT " +
                                            "( " +
                                                "SUM(IMPORTE) " +
                                                "FOR MES IN " +
                                                "( " +
                                                "[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]) " +
                                            ") AS pvt " +
                                            "WHERE EJERCICIO = " + cboxEjercicios.Text;
                dgvBudget.DataSource = sql.cargarDatosScriptEnTabla(scriptLoadBudget);
                cellFomat();
                dgvBudget.AutoResizeColumns();
                btnDeleteBudget.Enabled = false;
            }
            catch (Exception ex)
            {

            }
        }

        private void dgvBudget_MouseUp(object sender, MouseEventArgs e)
        {
            if (dgvBudget.SelectedRows.Count > 0)
            {
                btnDeleteBudget.Enabled = true;
            }
            else {
                btnDeleteBudget.Enabled = false;
            }
        }

        private void btnDeleteBudget_Click(object sender, EventArgs e)
        {
            try
            {
                string centroCoste1 = "";
                string centroCoste2 = "";
                string centroCoste3 = "";
                string ejercicio = "";
                string cuenta = "";
                string scriptDelete = "";

                foreach (DataGridViewRow fila in dgvBudget.SelectedRows) {

                    centroCoste1 = fila.Cells["CENTROCOSTE"].Value.ToString();
                    centroCoste2 = fila.Cells["CENTROCOSTE2"].Value.ToString();
                    centroCoste3 = fila.Cells["CENTROCOSTE3"].Value.ToString();
                    ejercicio = fila.Cells["EJERCICIO"].Value.ToString();
                    cuenta = fila.Cells["CUENTA"].Value.ToString();

                    scriptDelete = "DELETE FROM PRESUP WHERE " +
                        " CENTROCOSTE='" + centroCoste1 + "' AND " +
                        " CENTROCOSTE2='" + centroCoste2 + "' AND " +
                        " CENTROCOSTE3='" + centroCoste3 + "' AND " +
                        " EJERCICIO=" + ejercicio + " AND " +
                        " CUENTA='" + cuenta + "' AND " +
                        " TIPOCONT=1";

                    sql.ejecutarQuery(scriptDelete);

                    scriptDelete = "DELETE FROM LINEPRES WHERE " +
                       " CENTROCOSTE='" + centroCoste1 + "' AND " +
                       " CENTROCOSTE2='" + centroCoste2 + "' AND " +
                       " CENTROCOSTE3='" + centroCoste3 + "' AND " +
                       " EJERCICIO=" + ejercicio + " AND " +
                       " CUENTA='" + cuenta + "' AND " +
                       " TIPOCONT=1";

                    sql.ejecutarQuery(scriptDelete);


                }

                loadBudgetFromA3();

            }
            catch (Exception ex) {


            }
        }
    }
}
