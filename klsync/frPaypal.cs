﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace klsync
{
    public partial class frPaypal : Form
    {
        private ToolStripControlHost tsdtIni;
        private ToolStripControlHost tsdtFin;
        private ToolStripControlHost tslblIni;
        private ToolStripControlHost tslblFin;
        private ToolStripControlHost tscombodiv;
        private ToolStripControlHost tslblcombodiv;
        private ToolStripControlHost tslblFiltroRemesa;
        private ToolStripControlHost tslblradioRemesa;
        private ToolStripControlHost tslblradioFecha;
        private ToolStripControlHost filtroRemesa;
        private ToolStripControlHost lblRemesaFiltro;

        RadioButton rbRemesa = new RadioButton();
        RadioButton rbFecha = new RadioButton();

        ComboBox toolStripComboBoxRemesa = new ComboBox();
        ComboBox comboDivisa = new ComboBox();
        DateTimePicker dtpDesde = new DateTimePicker();
        DateTimePicker dtpHasta = new DateTimePicker();
        ToolStripButton btnCargarDocumentos = new ToolStripButton();
        ToolStripButton btnEnviarDocumento = new ToolStripButton();
        
        private static frPaypal m_FormDefInstance;
        public static frPaypal DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPaypal();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frPaypal()
        {
            InitializeComponent();

            Label lblFiltrarRemesa = new Label();
            lblFiltrarRemesa.Text = "Remesa:";
            lblFiltrarRemesa.TextAlign = ContentAlignment.MiddleCenter;
            lblFiltrarRemesa.BackColor = Color.Transparent;
            lblFiltrarRemesa.Padding = new Padding(10);

            Label lblFiltrarPor = new Label();
            lblFiltrarPor.Text = "Filtrar por: ";
            lblFiltrarPor.TextAlign = ContentAlignment.MiddleCenter;
            lblFiltrarPor.BackColor = Color.Transparent;
            lblFiltrarPor.Padding = new Padding(10);

            rbRemesa.Text = "Remesa";
            rbFecha.Text = "Fecha";

            Label lblDesde = new Label();
            lblDesde.Text = "Desde:";
            lblDesde.TextAlign = ContentAlignment.MiddleCenter;
            lblDesde.BackColor = Color.Transparent;
            lblDesde.Padding = new Padding(10);

            dtpDesde.Format = DateTimePickerFormat.Short;
            dtpHasta.Format = DateTimePickerFormat.Short;

            Label lblHasta = new Label();
            lblHasta.Text = "Hasta:";
            lblHasta.TextAlign = ContentAlignment.MiddleCenter;
            lblHasta.BackColor = Color.Transparent;
            lblHasta.Padding = new Padding(20);

            Label lblCombo = new Label();
            lblCombo.Text = "Divisa: ";
            lblCombo.TextAlign = ContentAlignment.MiddleCenter;
            lblCombo.BackColor = Color.Transparent;
            lblCombo.Padding = new Padding(5);

            csSqlConnects sqlDiv = new csSqlConnects();
            string consultaDiv = "SELECT CODMON FROM MONEDAS";
            csUtilidades.poblarCombo(comboDivisa, sqlDiv.cargarDatosTablaA3(consultaDiv), "CODMON", true);

            //toolStripComboBoxRemesa.DataSource = new object[] { "" };
            //comboDivisa.DataSource = new object[] { "EURO", "USD", "GBP", "CAD", "JPY", "AUD", "NZD", "CHF", "HKD", "SGD", 
              //  "SEK", "DKK", "PLN", "NOK", "HUF", "CZK", "ILS", "MXN", "BRL", "MYR", "PHP", "THB", "TRY", "TWD", "RUB" };
            comboDivisa.Margin = new Padding(20);
            comboDivisa.DropDownStyle = ComboBoxStyle.DropDownList;
            toolStripComboBoxRemesa.DropDownStyle = ComboBoxStyle.DropDownList;
            
            //comboMierdas.SelectedIndex = 1;
            //string valorDivisa = comboMierdas.SelectedValue.ToString();

            btnCargarDocumentos.Image = klsync.Properties.Resources.magnifier13;
            btnCargarDocumentos.ImageScaling = ToolStripItemImageScaling.None;
            btnCargarDocumentos.ToolTipText = "Cargar cartera";
            btnCargarDocumentos.TextImageRelation = TextImageRelation.ImageAboveText;
            btnCargarDocumentos.Padding = new Padding(20);
            btnCargarDocumentos.Click += new EventHandler(cargarDocumentos);

            btnEnviarDocumento.Image = klsync.Properties.Resources.save31;
            btnEnviarDocumento.ImageScaling = ToolStripItemImageScaling.None;
            btnEnviarDocumento.ToolTipText = "Generar documento";
            btnEnviarDocumento.TextImageRelation = TextImageRelation.ImageAboveText;
            btnEnviarDocumento.Padding = new Padding(20);
            btnEnviarDocumento.Click += new EventHandler(exportarTXT);


            lblRemesaFiltro = new ToolStripControlHost(lblFiltrarRemesa);
            tslblIni = new ToolStripControlHost(lblDesde);
            tsdtIni = new ToolStripControlHost(dtpDesde);
            tslblFiltroRemesa = new ToolStripControlHost(toolStripComboBoxRemesa);
            tslblFin = new ToolStripControlHost(lblHasta);
            tsdtFin = new ToolStripControlHost(dtpHasta);
            tslblcombodiv = new ToolStripControlHost(lblCombo);
            tscombodiv = new ToolStripControlHost(comboDivisa);
            tslblradioRemesa = new ToolStripControlHost(rbRemesa);
            tslblradioFecha = new ToolStripControlHost(rbFecha);
            filtroRemesa = new ToolStripControlHost(lblFiltrarPor);

            toolStripPaypal.Items.Add(filtroRemesa);
            toolStripPaypal.Items.Add(tslblradioRemesa);
            toolStripPaypal.Items.Add(tslblradioFecha);
            toolStripPaypal.Items.Add(new ToolStripSeparator());
            toolStripPaypal.Items.Add(lblRemesaFiltro);
            toolStripPaypal.Items.Add(tslblFiltroRemesa);
            toolStripPaypal.Items.Add(tslblIni);
            toolStripPaypal.Items.Add(tsdtIni);
            toolStripPaypal.Items.Add(tslblFin);
            toolStripPaypal.Items.Add(tsdtFin);
            toolStripPaypal.Items.Add(new ToolStripSeparator());
            toolStripPaypal.Items.Add(tslblcombodiv);
            toolStripPaypal.Items.Add(tscombodiv); 
            toolStripPaypal.Items.Add(btnCargarDocumentos);
            toolStripPaypal.Items.Add(btnEnviarDocumento);

            toolStripPaypal.Items[3].Padding = new System.Windows.Forms.Padding(20);
            toolStripPaypal.Items[11].Padding = new System.Windows.Forms.Padding(20);
            rbRemesa.Checked = true;

            csSqlConnects sql = new csSqlConnects();
            string consulta = "SELECT ROUND(PADREREME,0) AS REMESA FROM CARTERA ORDER BY REMESA DESC";
            csUtilidades.poblarCombo(toolStripComboBoxRemesa, sql.cargarDatosTablaA3(consulta), "REMESA");

            rbRemesa.CheckedChanged += new EventHandler(rbRemesa_CheckedChanged);

            tslblFiltroRemesa.Enabled = true;
            lblRemesaFiltro.Enabled = true;
            tslblIni.Enabled = false;
            tslblFin.Enabled = false;
            tsdtIni.Enabled = false;
            tsdtFin.Enabled = false;

            toolStripPaypal.CanOverflow = false;
            toolStripPaypal.CanOverflow = true;
        }

        private void rbRemesa_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRemesa.Checked)
            {
                tslblFiltroRemesa.Enabled = true;
                lblRemesaFiltro.Enabled = true;
                tslblIni.Enabled = false;
                tslblFin.Enabled = false;
                tsdtIni.Enabled = false;
                tsdtFin.Enabled = false;
            }
            else if (rbFecha.Checked)
            {
                tslblFiltroRemesa.Enabled = false;
                lblRemesaFiltro.Enabled = false;
                tslblIni.Enabled = true;
                tslblFin.Enabled = true;
                tsdtIni.Enabled = true;
                tsdtFin.Enabled = true;
            }
        }
        
        public void cargarDocumentos(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            DateTime FechaDesde = dtpDesde.Value;
            DateTime FechaHasta = dtpHasta.Value;
            string strFechaDesde = FechaDesde.ToString("yyyy-MM-dd 00:00:00");// Al cambiar de Servidor puede dar problemas con el formato de fecha  dd/MM/yyyy o yyyy/MM/dd 
            string strFechaHasta = FechaHasta.ToString("yyyy-MM-dd 23:55:00");
            string consulta = "";
            string where = "";
            string divisa_docs = comboDivisa.Text;

            #region switch_divisas
            switch (divisa_docs)
            {
                case "EURO":
                    where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;

                case "BRL":
                where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;

                case "COP":
                where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;

                case "GBP":
                where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;

                case "MXN":
                where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;

                case "USD":
                where = " AND CARTERA.CODMON = '" + divisa_docs + "'";
                break;
            }
            #endregion

            if ((toolStripComboBoxRemesa.SelectedItem != null && toolStripComboBoxRemesa.Text.Trim() != "") && rbRemesa.Checked)
            {
                consulta = "SELECT PROVEED.E_MAIL,NOMBRE, CARTERA.CODPRO,FECHAFACTURA,FECHACALCULOVTO AS FECHAVTO,IMPORTE,IMPORTECOB, " +
                       " CARTERA.CODMON, ROUND(PADREREME,0) AS REMESA, CARTERA.REFERENCIA " +
                       " FROM CARTERA LEFT JOIN PROVEED ON PROVEED.CODPRO = CARTERA.CODPRO WHERE PADREREME = '" + 
                       Convert.ToDouble(toolStripComboBoxRemesa.Text).ToString() + "'" + where + " ORDER BY CARTERA.CODMON ASC, NOMBRE ASC";
            }
            else if (rbFecha.Checked)
            {
                consulta = "SELECT PROVEED.E_MAIL,NOMBRE,CARTERA.CODPRO,FECHAFACTURA,FECHACALCULOVTO AS FECHAVTO,IMPORTE,IMPORTECOB, " +
                       " CARTERA.CODMON, ROUND(PADREREME,0) AS REMESA, CARTERA.REFERENCIA " +
                       " FROM CARTERA LEFT JOIN PROVEED ON PROVEED.CODPRO = CARTERA.CODPRO WHERE FECHAFACTURA BETWEEN '"
                       + FechaDesde + "' AND '" + FechaHasta + "'" + where + " ORDER BY CARTERA.CODMON ASC, FECHAFACTURA ASC";
            }

            if (consulta != "")
            {
                dgvPaypal.DataSource = sql.cargarDatosTablaA3(consulta);

                dgvPaypal.Columns["IMPORTECOB"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvPaypal.Columns["IMPORTECOB"].DefaultCellStyle.Format = "N2";
                dgvPaypal.Columns["IMPORTE"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvPaypal.Columns["IMPORTE"].DefaultCellStyle.Format = "N2";
                dgvPaypal.Columns["REMESA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvPaypal.Columns["REMESA"].DefaultCellStyle.Format = "N0";
            }
        }

        public string delimitador = "\t";
        public char espacio = ' ';
        public string mailDestinatario = "";
        public string importePago = "";
        public string divisa = "";
        public int divisa_len = 3;
        //public string ultimaDivisa = "";
        public string identificador = "";
        public int identificador_len = 30;
        //public string notas = "NO HAY NOTAS SOBRE EL PAGO";
        public string notas = "";

        private void exportarTXT(object sender, EventArgs e)
        {
            int rowcount = dgvPaypal.SelectedRows.Count;
            string line = "";
            bool check = false;

            if (rowcount > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Text File | *.txt";
                sfd.FileName = "PagoEnSerie" + DateTime.Today.ToString("ddMMyyyy") + "PAYPAL";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(sfd.FileName);

                    for (int i = 0; i < rowcount; i++)
                    {
                        if (Convert.ToInt32(dgvPaypal.SelectedRows[i].Cells["IMPORTE"].Value) < 8000)
                        {
                            mailDestinatario = dgvPaypal.SelectedRows[i].Cells["E_MAIL"].Value.ToString();
                            importePago = dgvPaypal.SelectedRows[i].Cells["IMPORTE"].Value.ToString();
                            //divisa = dgvPaypal.Rows[i].Cells["IMPORTE"].Value.ToString();
                            identificador = dgvPaypal.SelectedRows[i].Cells["referencia"].Value.ToString();
                            divisa = dgvPaypal.SelectedRows[i].Cells["CODMON"].Value.ToString();

                            if (identificador.Length > 30)
                            {
                                identificador = identificador.Substring(0, identificador_len);
                            }

                            line = mailDestinatario + delimitador + importePago.Substring(0, importePago.Length - 2) + delimitador + divisa.Substring(0, divisa_len) + delimitador +
                                identificador.Replace(" ", "_").Replace(".", "").Replace(",", "") + delimitador + notas;

                            writer.WriteLine(line);
                            check = true;
                            //ultimaDivisa = dgvPaypal.SelectedRows[i].Cells["CODMON"].Value.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No se ha podido registrar el pago a " + identificador + ". Los pagos individuales a destinatarios no pueden superar los 8.000€ ( 10.000 $ USD).");
                        }
                    }

                    writer.Close();
                    if (check)
                        MessageBox.Show("Los datos se han exportado con éxito.");
                }

            }
            else if (dgvPaypal.SelectedRows.Count == 0)
            {
                MessageBox.Show("Debes seleccionar como mínimo un registro de la tabla para generar el archivo.");
            }
        }
    }
}
