﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using RestSharp;

namespace klsync
{
    public partial class frArticulos : Form
    {

        private bool tipoBusqueda;
        string tipoCargaArticulos = "";
        private static frArticulos m_FormDefInstance;
        public static frArticulos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frArticulos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frArticulos()
        {
            InitializeComponent();

            //if (!csGlobal.tieneTallas) { btSincronizarTallasColores.Enabled = false; }

            dismayTyC();
            thagson();
        }

        private void thagson()
        {
            if (csGlobal.conexionDB.ToUpper().Contains("THAGSON"))
            {
                eliminarCategoriaToolStripMenuItem.Visible = false;
            }
        }

        private void dismayTyC()
        {
            if (!csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
            {
                btSincronizarTallasColores.Visible = false;
            }
            else
            {
                tabAdvanced.TabPages.Remove(tabPStoA3);
            }
        }

        DataTable ArticulosPS = new DataTable();

        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = csGlobal.ServerA3;
            csb.InitialCatalog = csGlobal.databaseA3;
            csb.IntegratedSecurity = csGlobal.integratedSecurity;
            csb.UserID = csGlobal.userA3;
            csb.Password = csGlobal.passwordA3;
            return csb.ConnectionString;
        }

        private void crearProductosPS()
        {

            string codart = "";
            string producto = "";
            string precio = "";
            string categoria = "";

            foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
            {

                codart = fila.Cells["CODIGO"].Value.ToString();
                producto = fila.Cells["DESCART"].Value.ToString();
                precio = fila.Cells["PRECIO"].Value.ToString();
                precio = precio.Replace(",", ".");
                // string[] categ = fila.Cells["CARACTERISTICA"].Value.ToString().Split('-');
                //categoria = categ[0];

                csPrestaTools prestatool = new csPrestaTools();
                prestatool.crearArticulo(producto, precio, codart, categoria);
            }

            DialogResult Resultado;
            //Resultado = //MessageBox.Show("Se han importado " + dgvProductosA3.SelectedRows.Count + " artículos correctamente, " +
            //  "es recomendable reindexar los artículos para que su búsqueda sea eficiente. \n" +
            //"¿Desea reindexar ahora?\n(si elige Sí, se abrirá momentaneamente un navegador)", "Reindexar Artículos", //MessageBoxButtons.YesNo, //MessageBoxIcon.Question);
            /*if (Resultado == DialogResult.Yes)
            {
                reindexar();
            }*/
        }

        /*private void button3_Click(object sender, EventArgs e)
        {
            reindexar();
            //MessageBox.Show("Reindexación completada");
        }

        private void reindexar()
        {
            ProcessStartInfo psi = new ProcessStartInfo("http://prestashop.klosions.com/admin123/searchcron.php?full=1&token=fzdt3zzq");
            psi.RedirectStandardOutput = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = true;

            Process.Start(psi);
        }*/


        //Grabo en A3ERP los productos creados en Prestashop para verificar cuales están creados

        private void btLoadArticulos_Click(object sender, EventArgs e)
        {
            cargarArticulos();
            actualizarArtículosEnPSToolStripMenuItem.Visible = csGlobal.nombreServidor.Contains("ANDREUTOYS") ? true : false;

        }

        private void cargarArticulos()
        {
            this.tipoBusqueda = false;
            var watch = Stopwatch.StartNew();
            cargarArticulos(tipoCargaArticulos, busqueda);

            contarFilasGrid(dgvProductosA3);


            dgvProductosA3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells; // DESCART

            watch.Stop();
            if (watch.Elapsed.Seconds >= 1)
                tssInfoTime.Text = watch.Elapsed.Seconds + " s";
            else
                tssInfoTime.Text = watch.Elapsed.Milliseconds + " ms";
        }

        private void contarFilasGrid(DataGridView dgv)
        {
            tsslbInfo.Text = dgv.Rows.Count.ToString() + " filas";
        }

        public void cargarArticulos(string tipoCarga, string filtro)
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = CadenaConexion();
            dataConnection.Open();

            CheckForIllegalCrossThreadCalls = false;

            string Marca = "";
            string Coleccion = "";
            if (cboxMarcas.Text == "")
            {
                Marca = "0";
            }
            else
            {
                if(cboxMarcas.SelectedValue == null)
                {
                    Marca = "0";
                }
                else
                {
                    Marca = cboxMarcas.SelectedValue.ToString();
                }
               
            }

            if (cboxColecciones.SelectedIndex == -1)
            {
                Coleccion = "0";
            }
            else
            {
                Coleccion = cboxColecciones.SelectedValue.ToString();

            }

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectArticulosSync(tipoCarga, filtro, this.where_in, Marca, Coleccion, cboxDisponible.Checked, cBoxEnTienda.Checked), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);

            //dgvProductosA3.SetPagedDataSource(t, bindingNavigator1);
            cargarDGV(dgvProductosA3, t);
        }

        public void cargarProductos()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();

            if (tipoCargaArticulos != "Duplicados")
            {
                DataTable ArticulosA3 = new DataTable();
                DataTable ArticulosToUpdate = new DataTable();
                DataTable KlsPrestaProducts = new DataTable();

                ArticulosToUpdate.Columns.Add("KLS_ID_SHOP");
                ArticulosToUpdate.Columns.Add("CODART");

                KlsPrestaProducts.Columns.Add("ID_PRODUCT");
                KlsPrestaProducts.Columns.Add("REFERENCE");
                KlsPrestaProducts.Columns.Add("CODART");
                KlsPrestaProducts.Columns.Add("KLS_ID_SHOP");

                if (tabAdvanced.SelectedTab == tabAdvanced.TabPages[1])
                {
                    string idioma;
                    string consultaPS = "Select " +
                                          " ps_product_lang.id_product, " +
                                          " ps_product.reference, " +
                                          " ps_product_lang.description_short, " +
                                          " ps_product_lang.description, " +
                                          " ps_product_lang.id_lang, " +
                                          " ps_product.active, " +
                                          " ps_product.price, " +
                                          " ps_product.id_manufacturer, " +
                                          " 'idioma' as idioma " +
                                          " From " +
                                          " ps_product Inner Join " +
                                          " ps_product_lang On ps_product.id_product = ps_product_lang.id_product";


                    ArticulosPS = mysql.cargarTabla(consultaPS);
                    csIdiomas idiomas = new csIdiomas();
                    for (int i = 0; i < ArticulosPS.Rows.Count; i++)
                    {
                        ArticulosPS.Rows[i][8] = idiomas.getIdioma(ArticulosPS.Rows[i].ItemArray[4].ToString());
                    }

                    cargarDGV(dgvProductosPS, ArticulosPS);

                }
                else
                {
                    // se cogen los datos de prestashop

                    string consultaPS = "Select ps_product.id_product as ID_PRODUCT, ps_product.reference as REFERENCE from ps_product";
                    ArticulosPS = mysql.cargarTabla(consultaPS);

                    string refA3 = "";
                    string refPS = "";
                    string consultaA3 = "SELECT LTRIM(CODART), KLS_ID_SHOP FROM ARTICULO WHERE KLS_ID_SHOP IS NOT NULL";
                    ArticulosA3 = sqlConnect.cargarDatosTablaA3(consultaA3);
                    ArticulosPS.Columns.Add("IdA3ERP");
                    for (int i = 0; i < ArticulosPS.Rows.Count; i++)
                    {
                        refA3 = ArticulosPS.Rows[i].ItemArray[1].ToString();
                        for (int ii = 0; ii < ArticulosA3.Rows.Count; ii++)
                        {
                            refPS = ArticulosA3.Rows[ii].ItemArray[0].ToString();
                            if (refPS == refA3)
                            {
                                ArticulosPS.Rows[i]["IdA3ERP"] = ArticulosA3.Rows[ii].ItemArray[1].ToString();
                                break;
                            }
                        }
                    }

                    cargarDGV(dgvProductosPS, ArticulosPS);
                }
            }
            else
            {
                string consulta = "SELECT min(ps_product.id_product) as id_product, count(ps_product.id_product) FROM ps_product GROUP BY ps_product.reference HAVING count(ps_product.id_product) > 1";
                DataTable dt = mysql.cargarTabla(consulta);
                cargarDGV(dgvProductosPS, dt);
            }
        }

        private void cargarDGV(DataGridView dgv, DataTable dt)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke(new MethodInvoker(() => dgv.DataSource = dt));
            else
                dgv.DataSource = dt;

            if (dgv.Name == "dgvProductosA3")
            {
                establecerColumnaImagen();
            }
        }

        //Esta función comprueba que artículos no deberían estar en la tienda
        //Verifica aquellos artículos de la tienda que están desactivados en A3
        public void verificarArticulosTienda()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable Articulos = new DataTable();

            string consultaPS = "SELECT dbo.ARTICULO.CODART, dbo.ARTICULO.DESCART, dbo.KLS_PRESTA_PRODUCTS.id_producto, dbo.KLS_PRESTA_PRODUCTS.reference, " +
                     " dbo.ARTICULO.KLS_ID_SHOP, dbo.ARTICULO.KLS_ARTICULO_TIENDA " +
                     " FROM dbo.ARTICULO RIGHT OUTER JOIN " +
                     " dbo.KLS_PRESTA_PRODUCTS ON LTRIM(dbo.ARTICULO.KLS_ID_SHOP) = dbo.KLS_PRESTA_PRODUCTS.id_producto " +
                     " WHERE (dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'F')";


            Articulos = sqlConnect.cargarDatosScriptEnTabla(consultaPS, "");

            dgvProductosA3.DataSource = Articulos;
            establecerColumnaImagen();
            //Jugando

        }



        private void exportarEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csOfficeIntegration officeInt = new csOfficeIntegration();
            officeInt.ExportToExcel(dgvProductosA3);
        }



        private void enviarProductoAPrestashopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // crearProductosPS(); // via alex
            if (dgvProductosA3.Rows.Count > 0)
            {
                Control.CheckForIllegalCrossThreadCalls = false;
                Thread art = new Thread(exportarArticulosPS);
                art.Start();
            }
        }

        private void exportarArticulosPS()
        {
            csPSWebService psWebService = new csPSWebService();
            csMySqlConnect mySqlconector = new csMySqlConnect();
            bool TyC = false;

            try
            {
                DataTable productos_prestashop = mysql.cargarTabla("select ltrim(reference) as reference, id_product from ps_product");
                Dictionary<string, string> referencias_duplicadas = new Dictionary<string, string>();

                //TRASPASO LAS FILAS SELECCIONADAS DEL DATAGRID DE LOS PRODUCTOS EN UN DATATABLE
                DataTable dt = csUtilidades.fillDataTableFromDataGridView(dgvProductosA3, true);

                foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                {
                    string codigo = fila.Cells["CODIGO"].Value.ToString().Trim();
                    string articuloPS = fila.Cells["ID EN TIENDA"].Value.ToString();
                    string visibleTienda = fila.Cells["VISIBLE TIENDA"].Value.ToString();

                    DataTable check = productos_prestashop.buscar(string.Format("reference = '{0}'", codigo));
                    if (!csUtilidades.verificarDt(check))
                    {
                        //verificar si el artículo ya existe en la base de datos de Prestahop
                        if (visibleTienda == "T")
                        {
                            if (!mySqlconector.existeArticulo(articuloPS) /*|| articuloPS == ""*/)
                            {
                                try
                                {
                                    if (fila.Cells["TyC"].Value.ToString() == "T")
                                        TyC = true;
                                    else
                                        TyC = false;
                                    /////
                                    string p1 = csGlobal.versionPS;
                                    string p2 = csGlobal.webServiceKey;
                                    /////
                                    psWebService.cdPSWebService("POST", "products", "", codigo, true, "", null, null, -1, TyC); //en minúsculas el objeto

                                    csImagenesPS imag = new csImagenesPS();
                                    imag.subirImagenesSilentMode(codigo);

                                    //subirArticuloWebservice(codigo);
                                }
                                 catch (Exception ex)
                                {
                                    Program.guardarErrorFichero(ex.ToString());
                                }
                            }
                            else
                                MessageBox.Show("El producto con código " + codigo + " ya existe en la tienda");
                        }
                        else
                            MessageBox.Show("El producto " + codigo + " debe estar activo en tienda para poder subirlo");
                    }
                    else
                    {
                        string id_product = csUtilidades.dataRowArray2DataTable(productos_prestashop.Select(string.Format("reference = '{0}'", codigo))).Rows[0]["id_product"].ToString();
                        referencias_duplicadas.Add(codigo, id_product);
                    }
                }

                if (referencias_duplicadas.Count > 0)
                {
                    string strReferencias = "Las referencias siguientes estan ya en la web y no se han subido: ";
                    foreach (KeyValuePair<string, string> entry in referencias_duplicadas)
                    {
                        strReferencias += entry.Key + ",";
                    }
                    strReferencias = strReferencias.TrimEnd(',');
                    strReferencias += "\n\nQuieres sincronizar las id's restantes?";

                    if (strReferencias.what())
                    {
                        csSqlConnects sql = new csSqlConnects();

                        DataTable result = dt.AsEnumerable()
                        .Where(row => referencias_duplicadas.Keys.Contains(row.Field<string>("CODIGO")))
                        .CopyToDataTable();

                        if (csUtilidades.verificarDt(result))
                        {
                            sincronizarIdPrestashop(referencias_duplicadas);
                            MessageBox.Show("Id sincronizadas");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }

                // Subimos las imagenes cuando acaba de subir los productos
                //csImagenesPS img = new csImagenesPS();
                //img.subirImagenesSilentMode();
                //csImagenesPS imagen = new csImagenesPS();
                //imagen.procedimientoCarpetasImagenes();
                cargarArticulosNuevos();
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void subirArticuloWebservice(string codigo)
        {
            string url = csGlobal.APIUrl + "products?ws_key=" + csGlobal.webServiceKey;

            csMySqlConnect mysql = new csMySqlConnect();

            RestRequest request;
            var client = new RestClient(csGlobal.APIUrl);
            request = new RestRequest("products?schema=synopsis&ws_key=" + csGlobal.webServiceKey, Method.GET);
            IRestResponse response = client.Execute(request);

            csPSWebServiceObjetos ws = new csPSWebServiceObjetos();
            string xml = ws.articulosXML(codigo, true, "");

            request = new RestRequest("products?ws_key=" + csGlobal.webServiceKey, Method.POST);
            request.AddParameter("xml", xml, ParameterType.RequestBody);
            IRestResponse response2 = client.Execute(request);
        }

        private void sincronizarIdPrestashop(Dictionary<string, string> referencias_duplicadas)
        {
            csSqlConnects sql = new csSqlConnects();
            foreach (KeyValuePair<string, string> entry in referencias_duplicadas)
            {
                string consulta = string.Format("UPDATE ARTICULO SET KLS_ID_SHOP = '{0}' WHERE LTRIM(CODART) = '{1}'", entry.Value, entry.Key);
                csUtilidades.ejecutarConsulta(consulta, false);
            }
        }

        //private void comprobarMarca(string codigo)
        //{
        //    csSqlConnects sql = new csSqlConnects();
        //    csMySqlConnect mysql = new csMySqlConnect();

        //    string marca = sql.obtenerCampoTabla("SELECT KLS_DESCMARCA FROM ARTICULO LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA WHERE LTRIM(CODART) = '" + codigo + "'");
        //    if (marca != "")
        //    {
        //        string manufacturer = mysql.obtenerDatoFromQuery("select name from ps_manufacturer where name = '" + marca + "'");

        //        if (manufacturer == "")
        //        {
        //            mysql.insertItems("insert into ps_manufacturers (name, active) values ('" + manufacturer + "',1)");
        //        }
        //    }
        //}

        private void btCrearArticulosWS_Click(object sender, EventArgs e)
        {
            if (dgvProductosA3.Rows.Count > 0)
            {
                exportarArticulosPS();
            }
        }

        private void btCheckArticulos_Click(object sender, EventArgs e)
        {
            verificarIdsArticulos();
        }

        private void verificarIdsArticulos()
        {
            //Select de todos los artículos de Prestashop
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable ArticulosPS = new DataTable();
            DataTable ArticulosA3 = new DataTable();
            DataTable ArticulosToUpdate = new DataTable();
            ArticulosToUpdate.Columns.Add("CODART");
            ArticulosToUpdate.Columns.Add("ID_PRODUCT");
            //ArticulosPS = mySqlConnect.dbDatos(csGlobal.databasePS + ".ps_product", "id_product,reference");
            ArticulosPS = mySqlConnect.dbDatos("ps_product", "id_product,reference");
            ArticulosA3 = sqlConnect.cargarDatosTabla("Articulo", "LTRIM(CODART) AS CODART");


            foreach (DataRow fila in ArticulosPS.Rows)
            {
                if (fila["reference"].ToString() != "")
                {
                    foreach (DataRow filaA3 in ArticulosA3.Rows)
                    {
                        if (fila["reference"].ToString() == filaA3["CODART"].ToString())
                        {
                            DataRow FilaToUpdate = ArticulosToUpdate.NewRow();
                            FilaToUpdate["CODART"] = filaA3["CODART"].ToString();
                            FilaToUpdate["ID_PRODUCT"] = fila["ID_PRODUCT"].ToString();
                            ArticulosToUpdate.Rows.Add(FilaToUpdate);
                            //MessageBox.Show(filaA3["CODART"].ToString() + " en las dos aplicaciones");
                        }
                    }
                }
            }


            //REVISAR ESTA FUNCIÓN ALEX Y LUIS
            //sqlConnect.actualizarIdArticulosA3(ArticulosToUpdate); 




        }

        /* private void btUploadTallasyColores_Click(object sender, EventArgs e)
        {
            subirTallasYColores(dgvProductosA3.SelectedRows[0].Cells[0].Value.ToString(), dgvProductosA3.SelectedRows[0].Cells[5].Value.ToString());
        }



        private void subirTallasYColores(string articuloA3,string articulosPS)
        {
            string A3_Grupo_H="";
            string A3_Grupo_V="";
            string A3_Valor_H="";
            string A3_Valor_V="";
            int maxId = 0;

            csSqlConnects sqlConector = new csSqlConnects();
            csSqlScripts sqlScript = new csSqlScripts();
            csMySqlConnect mySqlConector = new csMySqlConnect();
            DataTable tablaTallasyColores=new DataTable();
            tablaTallasyColores= sqlConector.cargarDatosScriptEnTabla(sqlScript.seletArticulosConTallasYColores()," where ltrim(CODART)='" + articuloA3 + "'");
            foreach (DataRow fila in tablaTallasyColores.Rows)
            {
                //OBTENER POR CADA DUPLA DE COMBINACIONES LOS VALORES DE A3ERP
                A3_Grupo_H=fila["CODFAMTALLAH"].ToString();
                A3_Grupo_V=fila["CODFAMTALLAV"].ToString();
                A3_Valor_H=fila["CODTALLA_H"].ToString();
                A3_Valor_V=fila["CODTALLA_V"].ToString();

                ////MessageBox.Show(A3_Grupo_H + " - " + A3_Valor_H + "\n" + 
                    //A3_Grupo_V + " - " + A3_Valor_V);


                //OBTENER LOS EQUIVALENTES DE CADA DUPLA EN PRESTASHOP
                string Atributo1PS = sqlConector.obtenerCodigoTallaParaPS(A3_Grupo_H, A3_Valor_H);
                string Atributo2PS = sqlConector.obtenerCodigoTallaParaPS(A3_Grupo_V, A3_Valor_V);

                //VERIFICO SI EXISTE YA LA COMBINACIÓN DE ATRIBUTOS PARA EL ARTÍCULO

                if (!mySqlConector.existeCombinacionTallayColor(articulosPS,Atributo1PS,Atributo2PS))
                {

                    //OBTENGO EL ULTIMO ID DE LA TABLA PRODUCT_ATTRIBUTE_COMBINATION
                    maxId = mySqlConector.selectID("ps_product_attribute_combination", "id_product_attribute") + 1;
                    ////MessageBox.Show(maxId.ToString());
                    string duplaDeValores = "(" + maxId + "," + Atributo1PS + "),(" + maxId + "," + Atributo2PS + ") ";
                    //INSERTO LA NUEVA DUPLA EN LAS TABLAS NECESARIAS
                    //TABLA PS_PRODUCT_ATTRIBUTE_COMBINATION

                    mySqlConector.InsertValoresEnTabla("ps_product_attribute_combination", " (id_product_attribute,id_attribute) ", duplaDeValores, "");

                    //TABLA PS_PRODUCT_ATTRIBUTE
                    string valores = "(" + maxId + "," + articulosPS + ",1)";

                    mySqlConector.InsertValoresEnTabla("ps_product_attribute", " (id_product_attribute,id_product,quantity) ", valores, "");

                    //TABKA PS_PRODUCT_ATTRIBUTE_SHOP

                    string valoresShop = "(" + maxId + ",1,0,1)";
                    mySqlConector.InsertValoresEnTabla("ps_product_attribute_shop", " (id_product_attribute,id_shop,default_on,minimal_quantity) ", valoresShop, "");
                }

            }
        
        }*/

        private void activarEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarProductosEnTienda(true);
            cargarArticulos();
        }

        private void desactivarEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarProductosEnTienda(false);
            cargarArticulos();
        }

        private void activarProductosEnTienda(bool activar)
        {
            string articulo = "";
            try
            {

                bool activarArticulos = false;
                if (activar)
                {
                    activarArticulos = true;
                }
                csSqlConnects sqlConector = new csSqlConnects();
                DataTable Tabla = new DataTable();
                Tabla.Columns.Add("Articulo");

                for (int i = 0; i < dgvProductosA3.SelectedRows.Count; i++)
                {
                    articulo = dgvProductosA3.SelectedRows[i].Cells["CODIGO"].Value.ToString();
                    articulo = articulo.Replace("'", "''");
                    Tabla.Rows.Add(articulo);
                }
                sqlConector.actualizarArticuloA3(Tabla, activarArticulos);


                // Activar / Desactivar producto en PS
                int activo = (activarArticulos) ? 1 : 0;
                foreach (DataGridViewRow dr in dgvProductosA3.SelectedRows)
                {
                    string kls_id_shop = dr.Cells["ID EN TIENDA"].Value.ToString();
                    if (kls_id_shop != "")
                    {
                        mysql.actualizaStock("UPDATE ps_product SET active = " + activo + " WHERE id_product = '" + kls_id_shop + "'");
                        mysql.actualizaStock("UPDATE ps_product_shop SET active = " + activo + " WHERE id_product = '" + kls_id_shop + "'");
                    }
                }

                MessageBox.Show("La actualizacion de los productos se ha llevado a cabo con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cambiar el estado del producto" + articulo + "\n" +
                    ex.ToString());
            }
        }

        private void rbActualizarArticulos_CheckedChanged(object sender, EventArgs e)
        {
            desactivarUpdate(true);
            cargarArticulos();
        }

        private void rbNuevosArticulos_CheckedChanged(object sender, EventArgs e)
        {
            desactivarUpdate(false);
            cargarArticulos();
        }

        private void rbVisibleTienda_CheckedChanged(object sender, EventArgs e)
        {
            desactivarUpdate(false);
            cargarArticulos();
        }

        private void rbTodosArticulos_CheckedChanged(object sender, EventArgs e)
        {
            desactivarUpdate(false);
            cargarArticulos();
        }

        private void frArticulos_Load(object sender, EventArgs e)
        {

            if (csGlobal.conexionDB.ToUpper().Contains("ANDREU"))
            {
                actualizarArtículosEnPSToolStripMenuItem.Visible = true;
            }

            if (csGlobal.medidasA3)
            {
                actualizarMedidasEnPSToolStripMenuItem.Visible = true;
            }

            dgvProductosA3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;




            //comboA3PS.SelectedIndex = comboA3PS.FindStringExact("A3");

            desactivarUpdate(false);
            cargarArticulos();

            //Inicializo el tamñao de la barra de buscador
            splitContainer2.SplitterDistance = 30;
            splitContainer2.FixedPanel = FixedPanel.Panel1;
            //al estar incluido en un contenedor, hay que volver a indicar que se adapte
            splitContainer2.Dock = DockStyle.Fill;

            cargarMarcas();
            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_CODCOLECCION"))
            {
                cargarColecciones();
            }

            //comboA3PS.SelectedIndex = 0;
            toolStrip2.Visible = false;
        }

        private void desactivarUpdate(bool activo)
        {
            btUpdate.Enabled = activo;
        }

        private void ponerANULLTodosIDDeLasTiendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (csUtilidades.ventanaPassword())
            {
                setProductsNull();
            }
        }

        private void ponerANullLosProductosSELECCIONADOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setProductsNull();
        }

        private void setProductsNull()
        {
            csSqlConnects sqlConector = new csSqlConnects();

            if (MessageBox.Show("Estás a punto de poner a NULL varios ID de la tienda. ¿Estás seguro?", "- Operación arriesgada -", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                foreach (DataGridViewRow dr in dgvProductosA3.SelectedRows)
                {
                    /*csUtilidades.ejecutarConsulta("update articulo set kls_id_shop = NULL, " +
                        " KLS_ARTICULO_TIENDA = 'F' WHERE LTRIM(CODART) = '" + dr.Cells["CODIGO"].Value.ToString().Trim() + "'",
                        false);
                    */
                    sqlConector.actualizarANull("ARTICULO", "KLS_ID_SHOP", " WHERE LTRIM(CODART) = '" + dr.Cells["CODIGO"].Value.ToString().Trim() + "'");
                }

            }
            else
            {
                MessageBox.Show("Operación cancelada");
            }

            cargarArticulos();
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarInfoArticulo();
        }

        private void actualizarInfoArticulo()
        {
            csPSWebService psWebService = new csPSWebService();
            csMySqlConnect mySqlconector = new csMySqlConnect();
            csSqlConnects connect = new csSqlConnects();

            try
            {
                //psWebService.cdPSWebService("PUT", "products", "", fila.Cells["CODIGO"].Value.ToString(), true, fila.Cells["ID EN TIENDA"].Value.ToString()); //en minúsculas el objeto
                foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                {
                    string articuloPS = fila.Cells["ID EN TIENDA"].Value.ToString();
                    string articuloA3ERP = fila.Cells["CODIGO"].Value.ToString();
                    //verificar si el artículo ya existe en la base de datos de Prestahop
                    if (mySqlconector.existeArticulo(articuloPS))
                    {
                        try
                        {
                            psWebService.cdPSWebService("PUT", "products", "", fila.Cells["CODIGO"].Value.ToString(), true, fila.Cells["ID EN TIENDA"].Value.ToString()); //en minúsculas el objeto
                        }
                        catch (Exception ex)
                        {
                            //MessageBox.Show(ex.ToString());
                        }
                    }

                }

                // borrar tabla kls_articulos_actualizar
                connect.borrarDatosSqlTabla("KLS_ARTICULOS_ACTUALIZAR");
                cargarArticulos();

            }
            catch (Exception ex)
            {

            }


        }

        public void borrarTablaPrestaProducts()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            sqlConnect.borrarDatosSqlTabla("KLS_PRESTA_PRODUCTS");
        }

        private void btnPrestashop_Click(object sender, EventArgs e)
        {
            borrarTablaPrestaProducts();
        }

        private void asignarIDSAKLSSHOPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            asignarIdsdePSaA3();
            MessageBox.Show("Productos sincronizados");
        }

        public void asignarIdsdePSaA3()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable dt = csUtilidades.fillDataTableFromDataGridView(dgvProductosPS, true);
            //DataTable dt = dv.Table.Copy();
            sqlConnect.actualizarIdA3(dt, "products");
        }

        private void comboA3PS_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (comboA3PS.Text == "Prestashop")
            //{
            //btnPrestashop.Visible = true;
            cargarArticulos();
            //}
            //else
            // {
            //    btnPrestashop.Visible = false;
            //   cargarArticulos();
            //}
        }

        private void btn_descripciones_Click(object sender, EventArgs e)
        {

        }

        private void radioDescripciones_CheckedChanged(object sender, EventArgs e)
        {
            desactivarUpdate(false);
            cargarArticulos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        private void txtBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar_Click(sender, e);
            }
        }

        private void insertarDescripcionesA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                {
                    sql.updateDescripa(fila);
                }
            }
            catch (Exception ex)
            { }
        }

        private void ReplicarImagenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csImagenesPS img = new csImagenesPS();

            img.copiarImagenes(dgvProductosA3.SelectedRows);
        }

        private void verificarArtículosEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarArticulosTienda();
        }

        private void desactivarEnTiendaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            actualizarEstadoArticulosPS(false);
        }

        private void actualizarEstadoArticulosPS(bool Activar)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
            {
                mySqlConnect.actualizarEstadoProductosPS(Activar, fila.Cells["id_producto"].Value.ToString());
            }
        }

        private void actualizarEstadoArticulosPS(bool Activar, string id_product)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.actualizarEstadoProductosPS(Activar, id_product);

        }

        private void cargarProductosPS()
        {
            DataTable dt;
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable productos_a3 = sql.cargarDatosTablaA3("select ltrim(codart) as codart from articulo");
            string anexo = " WHERE ps_product_lang.id_lang=" + mysql.defaultLangPS().ToString() + " and reference <> ''";

            if (toolstripButtonA3.Checked)
            {
                dt = mysql.cargarTabla("Select  ps_product.reference as codart From  ps_product Inner Join  ps_product_lang On ps_product.id_product = ps_product_lang.id_product WHERE ps_product_lang.id_lang=1 ");
                DataTable dif = csUtilidades.getDiffDataTables(dt, productos_a3);
                string where_in = csUtilidades.delimitarPorComasDataTableNotDouble(dif, "codart");

                anexo += " and ps_product.reference in ('" + where_in + "')";
            }

            dt = mysql.cargarTabla(string.Format("  SELECT ps_product.id_product, " +
                                        "                ps_product.reference, " +
                                        "                round(ps_product.price,2) AS siniva, " +
                                        "                round((ps_product.price*(rate/100)) + ps_product.price,2) AS coniva, " +
                                        "                ps_product_lang.name, " +
                                        "                ps_product.active, " +
                                        "                SUBSTRING_INDEX(SUBSTRING_INDEX(ps_product_lang.name, ')', 2), ' ', -1) AS ALIAS " +
                                        "                 " +
                                        "FROM ps_product " +
                                        "INNER JOIN ps_product_lang ON ps_product.id_product = ps_product_lang.id_product AND ps_product_lang.id_lang = {0} " +
                                        "INNER JOIN ps_tax_rule ON ps_tax_rule.id_tax_rules_group = ps_product.id_tax_rules_group AND ps_tax_rule.id_country = 6 " +
                                        "INNER JOIN ps_tax ON ps_tax_rule.id_tax = ps_tax.id_tax {1} GROUP BY id_product , ps_product.reference, round(ps_product.price, 2), " +
                                        " round((ps_product.price * (rate / 100)) + ps_product.price, 2), ps_product_lang.name, ps_product.active, " +
                                        " SUBSTRING_INDEX(SUBSTRING_INDEX(ps_product_lang.name, ')', 2), ' ', -1) ", mysql.defaultLangPS().ToString(), anexo));


            DataTable copyDataTable;
            copyDataTable = dt.Copy();
            copyDataTable.Columns.Remove("id_product");
            copyDataTable.Columns.Remove("coniva");
            copyDataTable.Columns.Remove("siniva");
            copyDataTable.Columns.Remove("name");
            copyDataTable.Columns.Remove("active");
            copyDataTable.Columns.Remove("alias");

            csUtilidades.addDataSource(dgvProductosPS, dt);

            contarFilasGrid(dgvProductosPS);
        }

        private void bajarProductosAA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearProductosA3FromPS();
        }



        private void crearProductosA3FromPS()
        {
            csa3erp a3ERP = new csa3erp();
            csa3erpItm a3ERPItm = new csa3erpItm();
            csMySqlConnect mysql = new csMySqlConnect();
            string codart = "";
            string name, price, alias, idProduct, reference = "";
            bool codartReference = false;

            try
            {
                if (dgvProductosPS.Rows.Count > 0)
                {
                    if (MessageBox.Show("¿Quieres utilizar la Referencia del artículo como Código de Artículo en A3ERP?\n" +
                                        "(NO) el código del artículo será autonumérico.", "Eliminar Categoria", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {

                        codartReference = true;
                    }


                    a3ERP.abrirEnlace();

                    foreach (DataGridViewRow fila in dgvProductosPS.SelectedRows)
                    {
                        idProduct = fila.Cells["id_product"].Value.ToString();
                        name = fila.Cells["name"].Value.ToString();
                        alias = fila.Cells["alias"].Value.ToString().Replace("(", "").Replace(")", "");
                        price = fila.Cells["coniva"].Value.ToString();
                        reference = fila.Cells["reference"].Value.ToString();

                        if (csGlobal.conexionDB.ToUpper().Contains("PRATS"))
                        {
                            reference = reference.Replace("-", "");

                        }

                        codart = a3ERPItm.crearArticuloA3V2(name, alias, price, reference, idProduct, codartReference);

                        if (codart != null)
                        {
                            csUtilidades.actualizarProductoPS(codart, idProduct);

                            //mysql.actualizarValoresEnTabla("ps_product", "reference", codart, "id_product", idProduct);

                            //string id_product = mysql.obtenerDatoFromQuery("select id_product from ps_product where reference = '" + codart.Trim() + "'");
                            //csUtilidades.ejecutarConsulta("update ps_order_detail set product_reference = '" + codart + "' where product_id = " + id_product + " and product_attribute_id = 0", 
                            //    true);
                        }
                    }

                    a3ERP.cerrarEnlace();

                    cargarProductosPS();
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        private void asignarCodA3EnPrestashopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string idProduct, codA3ERP = "";
                csMySqlConnect mysql = new csMySqlConnect();

                if (dgvProductosA3.Rows.Count > 0)
                {
                    foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                    {
                        idProduct = fila.Cells["ID EN TIENDA"].Value.ToString();
                        codA3ERP = fila.Cells["CODIGO"].Value.ToString();

                        if (codA3ERP != null)
                            mysql.actualizarValoresEnTabla("ps_product", "reference", codA3ERP, "id_product", idProduct);
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        //Esta función verifica que el ID de un artículo de A3 coincide con el de Prestashop
        private void verificarIdPsA3ERP()
        {
            DataTable ArticulosA3 = new DataTable();
            DataTable ArticulosPS = new DataTable();
            DataTable ArticulosRectificar = new DataTable();
            csSqlConnects sqlConnect = new csSqlConnects();
            ArticulosA3 = sqlConnect.obtenerArticulosA3();
            string A3Codart = "";
            string A3IdPS = "";
            string PSReference = "";
            string PSIdPS = "";
            string idParaRectificar = "";
            int i = 0;

            ArticulosRectificar.Columns.Add("A3CODART");
            ArticulosRectificar.Columns.Add("A3IDPS");
            ArticulosRectificar.Columns.Add("PSIDPD");


            csMySqlConnect mySqlConnect = new csMySqlConnect();
            ArticulosPS = mySqlConnect.obtenerIdArticulosPS();

            foreach (DataRow filaPS in ArticulosPS.Rows)
            {
                PSReference = filaPS.ItemArray[0].ToString();
                PSIdPS = filaPS.ItemArray[1].ToString();
                foreach (DataRow filaA3 in ArticulosA3.Rows)
                {
                    A3Codart = filaA3.ItemArray[0].ToString();
                    A3IdPS = filaA3.ItemArray[1].ToString();
                    if (PSReference == A3Codart)
                    {
                        if (PSIdPS == A3IdPS)
                        {

                        }
                        else
                        {
                            //MessageBox.Show("Articulo erroneo: " + filaA3.ItemArray[0]);

                            if (i < 1)
                            {
                                idParaRectificar = idParaRectificar + A3Codart;
                            }
                            else
                            {
                                idParaRectificar = idParaRectificar + "," + A3Codart;
                            }
                            i++;
                            ArticulosRectificar.Rows.Add(A3Codart, A3IdPS, PSIdPS);
                        }
                    }

                }

            }

            csUtilidades.addDataSource(dgvProductosA3, ArticulosRectificar);
            //dgvProductosA3.DataSource = ArticulosRectificar;
        }

        private void verificarIdEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            verificarIdPsA3ERP();
            contarFilasGrid(dgvProductosA3);
        }

      

        private void actualizarIdPSenA3ERP()
        {
            try
            {
                string idProduct, codA3ERP = "";
                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mysqlconnect = new csMySqlConnect();

                if (dgvProductosA3.Rows.Count > 0)
                {
                    foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                    {
                        codA3ERP = fila.Cells["CODIGO"].Value.ToString();
                        idProduct = mysqlconnect.obtenerDatoFromQuery("select id_product from ps_product where reference='" + codA3ERP + "'");


                        if (codA3ERP != null)
                            sqlConnect.actualizarIdPSenA3ERP(codA3ERP, idProduct);
                    }
                }
                contarFilasGrid(dgvProductosA3);
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }

        }

        private void btSincronizarStockNoTallas_Click(object sender, EventArgs e)
        {
            //Revisar este código
            //no tiene mucho sentido

            if (csGlobal.activarMonotallas == "Y")
            {

                csTallasYColoresV2 tyc = new csTallasYColoresV2();
                tyc.marcarArticulosMonoatributoEnA3ERP("");
            }


            csStocks stock = new csStocks();
            stock.stock();
            "Stock actualizado".mb();
            //actualizar_stock();
            //Thread t = new Thread(actualizarStockSinTallas);
            //t.Start();
        }

        //SINCRONIZAR TALLAS Y COLORES DEPRECATED
        //private void btSincronizarTallasColores_Click(object sender, EventArgs e)
        //{
        //    csTallasYColores tallasyColores = new csTallasYColores();
        //    Thread hilo = new Thread(actualizarTallasYColores);
        //    hilo.Start();
        //}

        public void actualizarStockSinTallas()
        {
            try
            {
                Stopwatch tiempo = new Stopwatch();


                csStocks stock = new csStocks();
                tiempo.Start();
                bloquearBotonesStock(true);
                tssLb.Text = "";
                tsslbInfo.Text = ">>>>>>>S I N C R O N I Z A N D O>>>>>>.....";
                // statusStrip1.Update();

                //stock.sincronizarStockNoTallas();

                TimeSpan tiempoTranscurrido = tiempo.Elapsed;
                string minutos = tiempoTranscurrido.Minutes.ToString() + " m. ";
                string segundos = tiempoTranscurrido.Seconds.ToString() + " s. ";
                tsslbInfo.Text = "S I N C R O N I Z A C I Ó N - F I N A L I Z A D A!!!!! ----->" + minutos + segundos;
                bloquearBotonesStock(false);
            }
            catch (Exception ex) { Program.guardarErrorFichero(ex.ToString()); }
        }

        private void bloquearBotonesStock(bool sincronizando)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            if (sincronizando)
            {
                //btSincronizarStockNoTallas.Enabled = false;
                //btSincronizarTallasColores.Enabled = false;
            }
            else
            {
                //btSincronizarStockNoTallas.Enabled = true;
                //btSincronizarTallasColores.Enabled = true;
            }

            //if (!csGlobal.tieneTallas) { btSincronizarTallasColores.Enabled = false; }
        }

        private void ListDirectory(System.Web.UI.WebControls.TreeView treeView, string path)
        {
            if (treeView is null)
            {
                throw new ArgumentNullException(nameof(treeView));
            }

            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException($"'{nameof(path)}' no puede ser nulo ni estar vacío.", nameof(path));
            }

            treeView.Nodes.Clear();
            var rootDirectoryInfo = new DirectoryInfo(path);
            treeView.Nodes.Add(CreateDirectoryNode(rootDirectoryInfo));
        }

        private static System.Web.UI.WebControls.TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new System.Web.UI.WebControls.TreeNode(directoryInfo.Name);
            foreach (var directory in directoryInfo.GetDirectories())
                directoryNode.ChildNodes.Add(CreateDirectoryNode(directory));
            foreach (var file in directoryInfo.GetFiles())
                directoryNode.ChildNodes.Add(child: new System.Web.UI.WebControls.TreeNode(file.Name));
            return directoryNode;
        }

        #region Config treeCat
        private string CategoriaInicio = "";
        private static int noteID;
        static csMySqlConnect mysql = new csMySqlConnect();
        static csSqlScripts script = new csSqlScripts();
        static DataTable dt = mysql.cargarTabla(script.selectAssignCategories());
        List<int> doneNotes = new List<int>();

        private void CreateNodes()
        {
            dt = mysql.cargarTabla(script.selectAssignCategories());
            DataRow[] rows = new DataRow[dt.Rows.Count];
            dt.Rows.CopyTo(rows, 0);

            // Get the TreeView ready for node creation.
            // This isn't really needed since we're using AddRange (but it's good practice).
            treeCat1.BeginUpdate();
            treeCat1.Nodes.Clear();

            System.Windows.Forms.TreeNode[] nodes = RecurseRows(rows);
            treeCat1.Nodes.AddRange(nodes);

            // Notify the TreeView to resume painting.
            treeCat1.EndUpdate();
        }

        int leveldepth = 0;

        private System.Windows.Forms.TreeNode[] RecurseRows(DataRow[] rows)
        {
            List<System.Windows.Forms.TreeNode> nodeList = new List<System.Windows.Forms.TreeNode>();
            System.Windows.Forms.TreeNode node = null;

            leveldepth++;
            foreach (DataRow dr in rows)
            {
                if (dr[0].ToString() == "20")
                {
                    string hola = "";
                }
                if (dr[0].ToString() == "29")
                {
                    string hola2 = "";
                }
                node = new System.Windows.Forms.TreeNode(dr["name"].ToString());
                node.ForeColor = ((dr["active"].ToString() == "1") ? Color.Black : Color.Red);
                noteID = Convert.ToInt32(dr["id_category"]);

                node.Name = noteID.ToString();
                node.ToolTipText = noteID.ToString();
                //node.ToolTipText = "ID de categoria: " + dr["id_category"].ToString();

                // This method searches the "dirty node list" for already completed nodes.
                if (doneNotes.Contains(noteID)) { continue; }

                // This alternate method using the Find method uses a Predicate generic delegate.
                if (nodeList.Find(FindNode) == null)
                {
                    //test

                    doneNotes.Add(noteID);
                    //fin test

                    int id_cat = int.Parse(dr["id_category"].ToString());
                    DataRow[] childRows = dt.Select("id_parent = " + id_cat);
                    if (childRows.Length > 0)
                    {
                        // Recursively call this function for all childRowsl
                        System.Windows.Forms.TreeNode[] childNodes = RecurseRows(childRows);

                        // Add all childnodes to this node.
                        node.Nodes.AddRange(childNodes);
                        leveldepth = 0;
                    }

                    // Mark this noteID as dirty (already added).
                    doneNotes.Add(noteID);
                    nodeList.Add(node);
                }
            }

            // Convert this List<TreeNode> to an array so it can be added to the parent node/TreeView.
            System.Windows.Forms.TreeNode[] nodeArr = nodeList.ToArray();

            return nodeArr;
        }

        private static bool FindNode(System.Windows.Forms.TreeNode n)
        {
            if (n.Nodes.Count == 0)
                return n.Name == noteID.ToString();
            else
            {
                while (n.Nodes.Count > 0)
                {
                    foreach (System.Windows.Forms.TreeNode tn in n.Nodes)
                    {
                        if (tn.Name == noteID.ToString())
                            return true;
                        else
                            n = tn;
                    }
                }
                return false;
            }
        }


        private void treeCat1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlScripts script = new csSqlScripts();
            dgvCat.DataSource = null;
            dgvCat.DataSource = mysql.cargarTabla(script.getProductsByCat(Convert.ToInt32(e.Node.Name)));
            tabCat.Text = "Asignación categorías - " + e.Node.Text;

            contarFilasGrid(dgvCat);
            if (dgvCat.Rows.Count > 0)
                CategoriaInicio = dgvCat.Rows[0].Cells["category_name"].Value.ToString();
        }

        #endregion

        #region Drag and Drop
        List<string> sc = new List<string>();
        private string id_category_previous = "";
        private string node_selected_name = "";
        private string node_selected_id = "";
        private void treeCat_DragDrop(object sender, DragEventArgs e)
        {
            Point pt;
            System.Windows.Forms.TreeNode destinationNode;
            pt = treeCat1.PointToClient(new Point(e.X, e.Y));
            destinationNode = treeCat1.GetNodeAt(pt);
            //System.Windows.Forms.TreeNode dragedNode = new System.Windows.Forms.TreeNode();
            string filtro = "";
            try
            {
                if (MessageBox.Show("Estás seguro de querer pasar los productos de la categoría: \n\n\t" + CategoriaInicio + " \n >>\n\t " + destinationNode.Text, "Asignar categoría", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    if (comboSusAdd.SelectedItem != null)
                    {
                        if (comboSusAdd.SelectedItem.ToString() == "Sustituir")
                        {
                            foreach (String str in sc)
                            {
                                filtro = " id_product=" + str + " and id_category=" + id_category_previous;
                                deleteCategories(filtro);
                            }

                            foreach (String str in sc)
                            {
                                addCategories(destinationNode.Name, str);
                            }
                        }
                        else
                        {
                            foreach (String str in sc)
                            {
                                addCategories(destinationNode.Name, str);
                            }
                        }

                        MessageBox.Show("Categorías asignadas con éxito.");
                        contarFilasGrid(dgvCat);
                        sc.Clear();
                    }
                    else
                        MessageBox.Show("Elige una opcion: sustituir o añadir.");
                }
                else
                    MessageBox.Show("Operación cancelada.");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.ToString());
            }
        }

        private void addCategories(string id_category, string id_product)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            mysql.insertItems("insert into ps_category_product values(" + id_category + "," + id_product + ",0)");
            mysql.actualizaStock("update ps_product set id_category_default = " + id_category + " where id_product = " + id_product);
            mysql.actualizaStock("update ps_product_shop set id_category_default = " + id_category + " where id_product = " + id_product);

            string codart = csUtilidades.getCodartByKlsIdShop(id_product);
            if (codart != "")
            {
                csUtilidades.ejecutarConsulta("insert into kls_categorias_art values ('" + codart + "', " + id_category + ", NULL)", false);
            }
        }

        private void deleteCategories(string filtro)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            mysql.borrar("ps_category_product", filtro);
        }

        private void treeCat_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void dgvCat_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    foreach (DataGridViewRow dr in dgvCat.SelectedRows)
                    {
                        sc.Add(dr.Cells["id_product"].Value.ToString());
                        id_category_previous = dr.Cells["id_category"].Value.ToString();
                    }

                    //dgvCat.DoDragDrop(dgvCat.SelectedRows.Count, DragDropEffects.Move);
                    dgvCat.DoDragDrop(sc, DragDropEffects.Copy);
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion

        private void btnReplicateImages_Click(object sender, EventArgs e)
        {
            csImagenesPS img = new csImagenesPS();

            if (dgvCat.SelectedRows.Count > 0)
                img.copiarImagenes(dgvCat.SelectedRows);
        }


        private void tabAdvanced_Click(object sender, EventArgs e)
        {
            if (tabAdvanced.SelectedIndex == 2)
            {
                comboSusAdd.SelectedIndex = 0;
                cargarNodosCategorias();
                toolStrip2.Visible = true;
            }
            else if (tabAdvanced.SelectedIndex == 1)
            {
                cbox_ActualizarEstado.SelectedIndex = 0;
            }
            else
                toolStrip2.Visible = false;
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            if (tabAdvanced.SelectedIndex == 2)
            {
                comboSusAdd.SelectedIndex = 0;
                cargarNodosCategorias();
                toolStrip2.Visible = true;
            }
            else if (tabAdvanced.SelectedIndex == 1)
            {
                cbox_ActualizarEstado.SelectedIndex = 0;
            }
            else
                toolStrip2.Visible = false;
        }

        private void cargarNodosCategorias()
        {
            CreateNodes();
            // treeCat1.ExpandAll();
            treeCat1.SelectedNode = tempTreeNode;
            doneNotes.Clear();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csImagenesPS imagenesPS = new csImagenesPS();
            imagenesPS.subirImagenes();
        }


        //private void toolStripButton5_Click(object sender, EventArgs e)
        //{
        //    csTallasYColores tallasyC = new csTallasYColores();
        //    tallasyC.actualizarTablaGralTallasyColores();
        //    tallasyC.procedimientoActualizarStockTallasyColores2();
        //}

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SendKeys.SendWait("^f");
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            cargarArticulosNuevos();
        }

        private void cargarArticulosNuevos()
        {
            busqueda = "";
            this.where_in = "";
            tipoCargaArticulos = "Nuevos";
            desactivarUpdate(false);
            cargarArticulos();
        }


        void bw_subirArticulo(object sender, DoWorkEventArgs e)
        {
            exportarArticulosPS();
        }

        void bw_subirArticuloCompletado(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripProgressBar.Style = ProgressBarStyle.Blocks;
            toolStripProgressBar.Value = toolStripProgressBar.Minimum;

            btnSendProductToPS.Enabled = true;

            if ("Quieres actualizar el stock?".what())
            {
                csStocks oStock = new csStocks();
                oStock.stock();
                "Stock actualizado".mb();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }

            cargarArticulos();
        }

        private void btnSendProductToPS_Click(object sender, EventArgs e)
        {
            subirProductoPS();
        }

        private void subirProductoPS()
        {
            bool check = false;
            bool marcarArticuloMonoAtributo = false;
            csTallasYColoresV2 tyc = new csTallasYColoresV2();


            //Revisar este código
            //no tiene mucho sentido


            if (csUtilidades.existeColumnaSQL("ARTICULO", "KLS_MONOATRIBUTO"))
            {
                marcarArticuloMonoAtributo = true;

            }

            //Marco los artículos como Monoatributos
            foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
            {
                if (string.IsNullOrEmpty(item.Cells["VISIBLE TIENDA"].Value.ToString()))
                {
                    check = true;
                }

                if (marcarArticuloMonoAtributo)
                {
                    tyc.marcarArticulosMonoatributoEnA3ERP(item.Cells["CODIGO"].Value.ToString());
                }
            }

            if (!check)
            {
                if (dgvProductosA3.SelectedRows.Count > 0)
                {
                    toolStripProgressBar.Style = ProgressBarStyle.Marquee;
                    toolStripProgressBar.MarqueeAnimationSpeed = 50;

                    btnSendProductToPS.Enabled = false;

                    BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += bw_subirArticulo;
                    bw.RunWorkerCompleted += bw_subirArticuloCompletado;
                    bw.RunWorkerAsync();
                    actualizarPrecios0();
                }
            }
            else
            {
                MessageBox.Show("Activa el producto en tienda antes de subirlo");
            }
        }


        private void actualizarPrecios0()
        {
            //if (csGlobal.conexionDB.Contains("INDUSNOW")) Se modifica para Cadcanarias
            if (csGlobal.activarMonotallas == "Y")
            {
                csUtilidades.ejecutarConsulta("update ps_product set cache_default_attribute=0", true);
                csUtilidades.ejecutarConsulta("update ps_product_shop set cache_default_attribute=0", true);
            }


        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            csRepLog rep = new csRepLog();
            rep.cambiosTabla("ARTICULO");
            MessageBox.Show("ARTICULOS ACTUALIZADOS");
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.where_in = "";
            tipoCargaArticulos = "enTienda";
            cargarArticulos();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            busqueda = "";
            this.where_in = "";
            tipoCargaArticulos = "Todos";
            cargarArticulos();
        }

        private System.Windows.Forms.TreeNode node_selected = new System.Windows.Forms.TreeNode();

        private void treeCat_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                node_selected_name = e.Node.Text;
                node_selected_id = e.Node.ToolTipText;
                treeCat1.SelectedNode = treeCat1.GetNodeAt(e.X, e.Y);

                if (treeCat1.SelectedNode != null)
                {
                    cmsNewCat.Show(treeCat1, e.Location);
                }

                node_selected = e.Node;
            }
        }

        private void nuevaCategoríaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearCategoria();
        }

        // Creación de nueva categoría dentro del Árbol de categorías
        private void crearCategoria()
        {
            int lang = csGlobal.IdiomasEsync.FirstOrDefault(x => x.Value == "CAS").Key;
            string newCat = Microsoft.VisualBasic.Interaction.InputBox("Introducir nueva categoría dentro de " + node_selected_name + ":", "Nueva categoría", "");

            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                if (string.IsNullOrEmpty(newCat))
                { }
                else
                {
                    csPSWebService psWebService = new csPSWebService();
                    psWebService.cdPSWebService("POST", "categories", newCat, node_selected_id, true, ""); //en minúsculas el objeto

                    string id_category = mysql.cargarTabla("SELECT MAX(id_category) FROM ps_category").Rows[0].ItemArray[0].ToString();
                    //cargarNodosCategorias();
                    node_selected.Nodes.Add(id_category, newCat);
                    treeCat1.Nodes.Add(node_selected);
                    treeCat1.ExpandAll();

                    MessageBox.Show("Categoría " + newCat + " creada con éxito");
                }
            }
            catch (Exception)
            {
            }
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            cargarNodosCategorias();
        }

        public void SetBasicAuthHeader(WebRequest request, String userName)
        {
            string authInfo = userName + ":";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
        }

        // Elimina la categoria del nodo asignado y luego borra el nodo en cuestión
        private string eliminarCategoria()
        {
            String test = String.Empty;

            try
            {
                if (MessageBox.Show("¿Estás seguro que deseas eliminar la categoria " + node_selected.Text + "?", "Eliminar Categoria", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    var request = WebRequest.Create(csGlobal.APIUrl + "categories/" + node_selected.Name);
                    SetBasicAuthHeader(request, csGlobal.webServiceKey);

                    request.Method = "DELETE";
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        test = reader.ReadToEnd();
                        reader.Close();
                        dataStream.Close();

                        node_selected.Nodes.Remove(node_selected);
                        treeCat1.Nodes.Remove(node_selected);
                        MessageBox.Show("Categoría " + node_selected.Text + " eliminada con éxito.");

                    }
                }
                else
                {
                }

                return test;
            }
            catch (WebException ex)
            {
                MessageBox.Show("La categoria " + node_selected.Text + " no se ha podido borrar.");
                return ex.ToString();
            }
        }

        private void eliminarCategoríaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            eliminarCategoria();
        }

        private void toolStripBtnReplicarIMG_Click(object sender, EventArgs e)
        {
            csImagenesPS img = new csImagenesPS();

            img.copiarImagenes(dgvCat.SelectedRows);
        }

        /// <summary>
        /// TINTES DISMAY
        /// *************
        /// Los tintes son productos con KLS_ARTICULO_PADRE informado.
        /// Deben ser numéricos. De haber un sólo artículo con padre informado que sea alfanumérico,
        /// no funcionará correctamente.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSincronizarTallasColores_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(actualizarTallasYColores);
            t.Start();
        }

        public void actualizarTallasYColores()
        {
            Stopwatch tiempo = new Stopwatch();
            tiempo.Start();
            bloquearBotonesStock(true);
            csTallasYColores tallasyColores = new csTallasYColores();
            tssLb.Text = "";
            tsslbInfo.Text = ">>>>>>>S I N C R O N I Z A N D O>>>>>>.....";
            //1º Actualizo la tabla temporal
            tallasyColores.actualizarTablaGralTallasyColores();
            //2º Inicio la sincronización de Tallays y Colores
            tallasyColores.procedimientoActualizarStockTallasyColores2();
            TimeSpan tiempoTranscurrido = tiempo.Elapsed;
            string minutos = tiempoTranscurrido.Minutes.ToString() + " m. ";
            string segundos = tiempoTranscurrido.Seconds.ToString() + " s. ";
            tsslbInfo.Text = "S I N C R O N I Z A C I Ó N - F I N A L I Z A D A!!!!! -----> " + minutos + segundos;
            bloquearBotonesStock(false);
        }

        private void dgvProductosA3_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            tsslbInfo.Text = dgvProductosA3.Rows.Count.ToString() + " Filas";
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            dgvProductosPS.DataSource = mysql.cargarTabla("Select " +
                                  " ps_product.id_product, " +
                                  " ps_product.reference, " +
                                  " ps_product.price, " +
                                  " ps_product_lang.name, " +
                                  " ps_product.active, " +
                                  " SUBSTRING_INDEX(SUBSTRING_INDEX(ps_product_lang.name, ')', 2), ' ', -1) as alias " +
                             " From " +
                                  " ps_product Inner Join " +
                                  " ps_product_lang On ps_product.id_product = ps_product_lang.id_product " +
                                  " where reference = '' and ps_product_lang.id_lang = " + mysql.defaultLangPS().ToString());
        }

        private void btCheckIdA3_Click(object sender, EventArgs e)
        {
            cargarProductos();
        }

        private void btLoadProductosPS_Click(object sender, EventArgs e)
        {
            cargarProductosPS();
        }

        private void btAsignarIdA3ERP_Click(object sender, EventArgs e)
        {
            asignarIdsdePSaA3();
        }

        private void toolStripBtnReplicarIMG_Click_1(object sender, EventArgs e)
        {
            csImagenesPS img = new csImagenesPS();

            img.copiarImagenes(dgvCat.SelectedRows);

            csUtilidades.openFolder(csGlobal.rutaImagenes);
        }

        private void toolStripButtonLockUnlock_Click(object sender, EventArgs e)
        {
            actualizarEstadoArticulosPSA3();
        }

        private void actualizarEstadoArticulosPSA3()
        {
            try
            {
                // se cogen los datos de prestashop
                //borrarTablaPrestaProducts();
                DataTable ArticulosA3 = new DataTable();
                csSqlConnects sql = new csSqlConnects();
                string consultaPS = "Select ps_product.id_product as ID_PRODUCT, ps_product.reference as REFERENCE, ps_product.active as ACTIVE from ps_product";
                ArticulosPS = mysql.cargarTabla(consultaPS);

                ProgressBar1.Maximum = ArticulosPS.Rows.Count;
                ProgressBar1.Minimum = 0;
                ProgressBar1.Value = 0;
                ProgressBar1.Step = 1;

                bool check = false;
                bool refActiveA3 = false;
                bool refActivePS = false;
                string id_product = "";
                string refA3 = "";
                string refPS = "";
                string consultaA3 = "SELECT LTRIM(CODART), KLS_ID_SHOP, KLS_ARTICULO_BLOQUEAR, case WHEN KLS_ARTICULO_BLOQUEAR =  'T' THEN 1 ELSE 0 end AS KLS_ARTICULO_BLOQUEAR FROM ARTICULO WHERE KLS_ID_SHOP IS NOT NULL";
                ArticulosA3 = sql.cargarDatosTablaA3(consultaA3);
                csPSWebService webservice = new csPSWebService();

                if (MessageBox.Show("Vas a hacer cambios importantes en la base de datos. ¿Estás seguro?", "Aviso importante", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {

                    for (int i = 0; i < ArticulosPS.Rows.Count; i++)
                    {
                        // REFERENCE
                        id_product = ArticulosPS.Rows[i].ItemArray[0].ToString().Trim();
                        refPS = ArticulosPS.Rows[i].ItemArray[1].ToString().Trim();
                        for (int ii = 0; ii < ArticulosA3.Rows.Count; ii++)
                        {
                            // KLS_ID_SHOP

                            refA3 = ArticulosA3.Rows[ii].ItemArray[1].ToString().Trim();
                            if (refPS == refA3)
                            {
                                refActiveA3 = ArticulosA3.Rows[ii].ItemArray[3].ToString().Trim().ToBool(); // ACTIVE
                                refActivePS = ArticulosPS.Rows[i].ItemArray[2].ToString().Trim().ToBool(); // BLOQUEADO

                                if (refActivePS != refActiveA3) // si son diferentes, se actualiza el de PS con el valor de A3
                                {
                                    if (!refActiveA3) // si en a3 esta inactivo, se borra de PS
                                    {
                                        if (cbox_ActualizarEstado.SelectedIndex == 0) // Borrar
                                        {
                                            check = true;
                                            webservice.cdPSWebService("DELETE", "products", id_product);
                                        }
                                    }
                                    else // si en A3 está activo y en PS inactivo, se activa en PS
                                    {
                                        if (cbox_ActualizarEstado.SelectedIndex == 1) // Actualizar
                                        {
                                            check = true;
                                            actualizarEstadoArticulosPS(refActiveA3, id_product);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!refActiveA3) // si los dos estan bloqueados, se borra de PS
                                    {
                                        if (cbox_ActualizarEstado.SelectedIndex == 1)
                                        {
                                            check = true;
                                            actualizarEstadoArticulosPS(refActiveA3, id_product);
                                        }
                                        else
                                        {
                                            check = true;
                                            webservice.cdPSWebService("DELETE", "products", id_product);
                                        }

                                    }
                                }

                                break;
                            }
                        }
                    }


                    if (check)
                        MessageBox.Show("El estado de los productos ha sido actualizado");
                    else
                        MessageBox.Show("El estado de los productos no se ha actualizado");

                    ProgressBar1.Value = 0;
                    cargarDGV(dgvProductosPS, ArticulosPS);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

        }

        private void toolStripButton9_Click_1(object sender, EventArgs e)
        {
            busqueda = "";
            this.where_in = "";
            tipoCargaArticulos = "Bloqueados";
            desactivarUpdate(false);
            cargarArticulos();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            dgvProductosPS.DataSource = mysql.cargarTabla("select id_product, reference, active , date_add from ps_product where reference in (select reference from ps_product group by reference having count(reference) > 1) order by reference;");
        }

        private void eliminarProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvProductosPS.Rows.Count > 0)
            {
                csPSWebService webservice = new csPSWebService();

                foreach (DataGridViewRow dr in dgvProductosPS.SelectedRows)
                {
                    webservice.cdPSWebService("DELETE", "products", dr.Cells["id_product"].Value.ToString());
                }

                MessageBox.Show("Terminado");
            }
        }

        public void asignarIDsDePSAA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            asignarIdsdePSaA3();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlScripts script = new csSqlScripts();

                DataTable dt = mysql.cargarTabla(script.repeatedCategories());

                foreach (DataRow dr in dt.Rows)
                {
                    //if (dr["id_category"].ToString() != dr["id_product"].ToString())
                    //{ 
                    //}
                    mysql.actualizaStock("UPDATE ps_product SET id_category_default = " + dr["id_category"].ToString() + " where id_product = " + dr["id_product"].ToString());
                    mysql.actualizaStock("UPDATE ps_product_shop SET id_category_default = " + dr["id_category"].ToString() + " where id_product = " + dr["id_product"].ToString());

                    //mysql.actualizaStock("UPDATE ps_product SET id_category_default = 1229 where id_product = 8");
                    //mysql.actualizaStock("UPDATE ps_product_shop SET id_category_default = 1229 where id_product = 8");
                    //mysql.actualizaStock("UPDATE ps_product SET id_category_default = 1229 where id_product = 9");
                    //mysql.actualizaStock("UPDATE ps_product_shop SET id_category_default = 1229 where id_product = 9");
                }

                MessageBox.Show("Las categorias se han actualizado con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error inesperado.\n\n Error: " + ex.ToString());
            }
        }

        private void toolStripTextBox1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    (dgvCat.DataSource as DataTable).DefaultView.RowFilter = string.Format("product_name like '%{0}%' or category_name like '%{0}%'", toolStripTextBox1.Text);
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnRegenerarImagenes_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csImagenesPS imagenesPS = new csImagenesPS();
            imagenesPS.regenerarImagenes();
        }

        private void verificarArtículosEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataTable ArticulosPS = new DataTable();
            string consultaPS = "Select ps_product.id_product as ID_PRODUCT, ps_product.reference as REFERENCE from ps_product";
            ArticulosPS = mysql.cargarTabla(consultaPS);
            string referencePS = "";
            string codartA3 = "";

            for (int i = 0; i < dgvProductosA3.Rows.Count; i++)
            {
                codartA3 = dgvProductosA3.Rows[i].Cells[0].Value.ToString().Replace(" ", "");
                foreach (DataRow filaPS in ArticulosPS.Rows)
                {
                    referencePS = filaPS[1].ToString();
                    if (codartA3 == referencePS)
                    {
                        //dgvProductosA3.Rows.RemoveAt(i);
                        dgvProductosA3.Rows[i].Selected = true;
                        break;
                    }
                }

            }

            foreach (DataGridViewRow r in dgvProductosA3.SelectedRows)
            {
                dgvProductosA3.Rows.RemoveAt(r.Index);
            }



            //foreach (DataGridViewRow filaA3 in dgvProductosA3.Rows)
            //{
            //    codartA3=filaA3.Cells[0].Value.ToString().Replace(" ","");
            //    foreach (DataRow filaPS in ArticulosPS.Rows)
            //    {
            //        referencePS=filaPS[1].ToString();
            //        if (codartA3==referencePS)
            //        {
            //            dgvProductosA3.Rows.RemoveAt(filaA3.Index);
            //            break;
            //        }
            //    }

            //}
        }

        private void ponerVisibleEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enableOrNot(1);
        }

        private void ponerInvisibleEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            enableOrNot(0);
        }

        private void enableOrNot(int enable)
        {
            try
            {
                string id_product = "";

                if (dgvProductosPS.SelectedRows.Count > 0)
                {
                    csMySqlConnect mysql = new csMySqlConnect();
                    foreach (DataGridViewRow dr in dgvProductosPS.SelectedRows)
                    {
                        id_product = dr.Cells[0].Value.ToString();

                        mysql.actualizaStock("UPDATE ps_product SET active = " + enable + " WHERE id_product = '" + id_product + "'");
                        mysql.actualizaStock("UPDATE ps_product_shop SET active = " + enable + " WHERE id_product = '" + id_product + "'");
                    }

                    cargarProductosPS();
                }
            }
            catch (Exception)
            {
            }
        }

        private void btnTagsGenerator_CbtnUpdateShortDescription_Click(object sender, EventArgs e)
        {
            //Thread t = new Thread(actualizarPS_TAG);
            //t.Start();
        }

        private void btnPSProductTag_Click(object sender, EventArgs e)
        {
            //Thread t = new Thread(actualizarPS_PRODUCT_TAG);
            //t.Start();
        }

        public void actualizarPS_TAG(ProgressBar progressBarTags)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable dt = new DataTable();
            string name = "";
            int id_lang = 0;
            int kls_id_product = 0;
            int contador = -1;
            dt = mysql.cargarTabla("select id_product, id_lang, name from ps_product_lang");
            mysql.borrar("ps_tag");

            foreach (DataRow dr in dt.Rows)
            {
                contador++;
                progressBarTags.Maximum = dt.Rows.Count;
                progressBarTags.Value = contador;
                id_lang = Convert.ToInt32(dr["id_lang"].ToString());
                name = Regex.Replace(dr["name"].ToString(), @"\D", "");
                kls_id_product = Convert.ToInt32(dr["id_product"].ToString());

                mysql.insertItems("INSERT INTO ps_tag (id_lang, name, kls_id_product) VALUES (" + id_lang + ",'" + name + "'," + kls_id_product + ");");
            }
        }

        public void actualizarPS_PRODUCT_TAG(ProgressBar progressBarTags)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csMySqlConnect mysql = new csMySqlConnect();
            mysql.borrar("ps_product_tag");

            DataTable dt = mysql.cargarTabla("select * from ps_tag where id_lang = 1");
            int id_product = 0;
            int id_tag = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                progressBarTags.Maximum = dt.Rows.Count;
                progressBarTags.Value = i;

                id_product = Convert.ToInt32(dt.Rows[i]["kls_id_product"].ToString());
                id_tag = Convert.ToInt32(dt.Rows[i]["id_tag"].ToString());
                mysql.insertItems("INSERT INTO ps_product_tag (id_product, id_tag) VALUES (" + id_product + ", " + id_tag + ");");
            }
        }





        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            actualizarNombresProductosEnA3ERP();
        }

        private void actualizarNombresProductosEnA3ERP()
        {
            csSqlConnects sqlConector = new csSqlConnects();
            sqlConector.actualizarNombresA3ERP(dgvProductosPS.SelectedRows);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                csSqlConnects sql = new csSqlConnects();
                string search = Microsoft.VisualBasic.Interaction.InputBox("", "Búsqueda", "");
                if (search != "")
                {
                    buscarLupa(search, cboxMarcas.SelectedValue.ToString(), cboxColecciones.SelectedValue.ToString());
                }
                else
                {
                    buscarLupa("", "", "");
                }

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private string busqueda = "";

        private void buscarLupa(string search, string marca, string coleccion)
        {
            if (!tipoBusqueda)
            {
                if (tabAdvanced.SelectedTab == tabAdvanced.TabPages[0]) // A3
                {
                    tipoCargaArticulos = "Todos";
                    busqueda = search;
                    if (busqueda == "")
                    { }
                    cargarArticulos();
                }
                if (tabAdvanced.SelectedTab == tabAdvanced.TabPages[1]) // PS
                {
                    string consultaPS = string.Format("  SELECT ps_product.id_product, " +
                                        "                ps_product.reference, " +
                                        "                round(ps_product.price,2) AS siniva, " +
                                        "                round((ps_product.price*(rate/100)) + ps_product.price,2) AS coniva, " +
                                        "                ps_product_lang.name, " +
                                        "                ps_product.active, " +
                                        "                SUBSTRING_INDEX(SUBSTRING_INDEX(ps_product_lang.name, ')', 2), ' ', -1) AS ALIAS " +
                                        "                 " +
                                        "FROM ps_product " +
                                        "INNER JOIN ps_product_lang ON ps_product.id_product = ps_product_lang.id_product AND ps_product_lang.id_lang = {0} " +
                                        "INNER JOIN ps_tax_rule ON ps_tax_rule.id_tax_rules_group = ps_product.id_tax_rules_group AND ps_tax_rule.id_country = 6 " +
                                        "INNER JOIN ps_tax ON ps_tax_rule.id_tax = ps_tax.id_tax " +
                                        "WHERE (ps_product.reference LIKE '%{1}%' " +
                                        "OR ps_product_lang.name LIKE '%{1}%' " +
                                        "OR ps_product.price LIKE '%{1}%')", mysql.defaultLangPS().ToString(), search);

                    csUtilidades.addDataSource(dgvProductosPS, mysql.cargarTabla(consultaPS));
                }
            }
            else
            {
                string where = string.Format(" and (ltrim(articulo.codart) like '%{0}%' or ltrim(articulo.descart) like '%{0}%') ", search);
                this.cargarArticulosStock(where);
            }

        }

        private void actualizarPreciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarPreciosToolStripMenuItem.Enabled = false;
            actualizarPrecios();
            actualizarPreciosToolStripMenuItem.Enabled = true;
        }

        ////////////////////////////////
        // Actualizar precios de PS a A3
        ////////////////////////////////
        // Tabla: PRCESP
        // Campo: Precio
        // Where: Referencia = CODART
        ////////////////////////////////
        private void actualizarPrecios()
        {
            try
            {
                if (dgvProductosPS.Rows.Count > 0)
                {
                    DataTable dt = csUtilidades.fillDataTableFromDataGridView(dgvProductosPS, true);

                    if (dt.Rows.Count > 0)
                    {
                        string codart = "", precio = "";
                        csSqlConnects sql = new csSqlConnects();

                        foreach (DataRow dr in dt.Rows)
                        {
                            codart = dr["reference"].ToString();
                            precio = Convert.ToDouble(dr["price"].ToString()).ToString().Replace(",", ".");
                            if (codart != "")
                            {
                                csUtilidades.ejecutarConsulta("update articulo " +
                                                          " set prcventa = " + precio +
                                                          " where ltrim(codart) = '" + codart + "'", false);
                            }
                        }

                        MessageBox.Show("Los precios han sido actualizados");
                    }
                    else
                    {
                        MessageBox.Show("Selecciona al menos una fila para actualizar los precios");
                    }
                }
            }
            catch (Exception ex) { Program.guardarErrorFichero(ex.Message); }
        }

        public void btnNuevoStock_Click(object sender, EventArgs e)
        {
            actualizar_stock();
        }

        public void actualizar_stock()
        {
            string fields = "id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock";
            string id_product_ps = "", id_product_a3 = "", query = "", unidades = "";
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            DataTable ps = mysql.cargarTabla("select * from ps_stock_available where id_product_attribute = 0");

            string almacen = "'" + csGlobal.almacenA3.Replace(",", "','") + "'";

            DataTable a3 = sql.cargarDatosTablaA3("SELECT LTRIM(dbo.STOCKALM.CODART) AS REFERENCE," +
                                                       " SUM(dbo.STOCKALM.UNIDADES) AS UNIDADES, " +
                                                       " dbo.ARTICULO.TALLAS, " +
                                                       " dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                                                       " KLS_ID_SHOP, " +
                                                       " CASE " +
                                                           " WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                                                           " WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                                                           " ELSE 2 " +
                                                       " END AS KLS_PERMITIRCOMPRAS " +
                                                " FROM dbo.STOCKALM " +
                                                " INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                                                " WHERE dbo.STOCKALM.CODFAMTALLAH IS NULL " +
                                                " AND dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T' " +
                                                " AND dbo.STOCKALM.CODFAMTALLAV IS NULL " +
                                                " AND dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T' " +
                                                //" AND dbo.STOCKALM.UNIDADES > 0 " +
                                                " AND LTRIM(dbo.STOCKALM.CODALM) IN (" + almacen + ") " +
                                                " AND KLS_ID_SHOP IS NOT NULL and KLS_ID_SHOP <> 0 " +
                                                " GROUP BY LTRIM(dbo.STOCKALM.CODART), " +
                                                         " dbo.ARTICULO.TALLAS, " +
                                                         " dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                                                         " KLS_ID_SHOP, " +
                                                         " KLS_PERMITIRCOMPRAS");

            if (a3.Rows.Count > 0)
            {
                int contador = 0;
                foreach (DataRow filaA3 in a3.Rows)
                {
                    id_product_a3 = filaA3["KLS_ID_SHOP"].ToString();
                    unidades = filaA3["UNIDADES"].ToString();
                    foreach (DataRow filaPS in ps.Rows)
                    {
                        id_product_ps = filaPS["id_product"].ToString();

                        if (id_product_a3 == id_product_ps)
                        {
                            query = "update ps_stock_available set quantity = " + unidades + " where id_product = " + id_product_ps + " and id_product_attribute = 0";
                            csUtilidades.ejecutarConsulta(query, true);
                            break;
                        }
                    }

                }
            }

            tsslbInfo.Text = "Stock actualizado";

        }

        private void btnUPC_Click(object sender, EventArgs e)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlConnects sql = new csSqlConnects();

                DataTable dtPS = mysql.cargarTabla("select distinct reference from ps_product");
                DataTable dtA3 = sql.cargarDatosTablaA3("select ltrim(codart) as reference, param1 as upc from articulo");

                foreach (DataRow a3 in dtA3.Rows)
                {
                    foreach (DataRow ps in dtPS.Rows)
                    {
                        string a3_upc = a3["upc"].ToString();
                        string a3_reference = a3["reference"].ToString();
                        string ps_reference = ps["reference"].ToString();

                        if (ps_reference == a3_reference)
                        {
                            string consulta = "update ps_product set upc = '" + a3_upc + "' where reference = '" + ps_reference + "'";
                            csUtilidades.ejecutarConsulta(consulta, true);
                            break;
                        }
                    }
                }

                MessageBox.Show("Campos UPC actualizados con éxito");
            }
            catch (Exception) { MessageBox.Show("Error al actualizar los campos UPC"); }
        }

        private void btnSincronizarFamilias_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(SyncFamilias);
            t.Start();
        }

        public void SyncFamilias()
        {
            Control.CheckForIllegalCrossThreadCalls = false;

            DataTable dtA3_Articulos = new DataTable();
            DataTable dtA3_Clientes = new DataTable();
            DataTable dtPS = new DataTable();

            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            dtA3_Articulos = sql.cargarDatosTablaA3("SELECT KLS_ID_SHOP, FAMARTDESCVEN FROM ARTICULO WHERE KLS_ID_SHOP is not null and FAMARTDESCVEN is not null AND KLS_ID_SHOP > 0");
            dtA3_Clientes = sql.cargarDatosTablaA3("SELECT KLS_CODCLIENTE, FAMCLIDESC FROM __CLIENTES WHERE KLS_CODCLIENTE is not null and FAMCLIDESC is not null AND KLS_CODCLIENTE > 0");

            // articulos
            foreach (DataRow dr in dtA3_Articulos.Rows)
            {
                string kls_id_shop = dr["KLS_ID_SHOP"].ToString().Trim();
                string famartdescven = dr["FAMARTDESCVEN"].ToString().Trim();

                csUtilidades.ejecutarConsulta("update ps_product set kls_a3erp_fam_id = '" + famartdescven + "' where id_product = " + kls_id_shop, true);
            }

            // clientes
            foreach (DataRow dr in dtA3_Clientes.Rows)
            {
                string kls_codcliente = dr["KLS_CODCLIENTE"].ToString().Trim();
                string famclidesc = dr["FAMCLIDESC"].ToString().Trim();

                csUtilidades.ejecutarConsulta("update ps_customer set kls_a3erp_fam_id = '" + famclidesc + "' where id_customer = " + kls_codcliente, true);
            }

            toolStripStatusLabelProgreso.Text = "Actualización de familias realizado con éxito";
        }

        private void actualizlarComportamientoStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tycV2 = new csTallasYColoresV2();
            tycV2.actualizarComportamientoStockPS(convertirDatagridViewRowsToDatatable(dgvProductosA3));
        }

        private DataTable convertirDatagridViewRowsToDatatable(DataGridView dgv)
        {
            DataTable dt = new DataTable();
            string product_id = "";

            dt.Columns.Add("PRODUCT_ID");

            for (int i = 0; i < dgv.SelectedRows.Count; i++)
            {
                if (dgv.SelectedRows[i].Cells["TyC"].Value.ToString() == "T")
                {
                    product_id = dgv.SelectedRows[i].Cells["ID EN TIENDA"].Value.ToString();
                    DataRow dr = dt.NewRow();
                    dr["PRODUCT_ID"] = product_id;
                    dt.Rows.Add(dr);

                }
            }

            return dt;
        }

        private void sincFamiliasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(SyncFamilias);
            t.Start();
        }

        private void modoReplicarImágenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csGlobal.replicarImagenes = !csGlobal.replicarImagenes;

            //modoReplicarImágenesToolStripMenuItem.Checked = !csGlobal.replicarImagenes;
        }



        private void asignarImagenCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Abrir OpenFileDialog, escoger imagen
                using (OpenFileDialog ofd = new OpenFileDialog())
                {
                    ofd.Title = "Abrir la imagen de la categoria";
                    ofd.Filter = "JPG files (*.jpg)|*.jpg";
                    ofd.Multiselect = false;

                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        csPrestashop ps = new csPrestashop();
                        ps.subirImagenCategoria(ofd.FileName, node_selected_id);
                        MessageBox.Show("Imagen de la categoria subida con exito");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al subir la imagen de categoria.");
            }
        }

        private void cargarProductosPorImagenesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.where_in = csUtilidades.selectFilesFromFolder();
            tipoCargaArticulos = "Todos";
            cargarArticulos();
        }

        private string where_in = "";

        private void actualizarEstadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            DataTable status_products = mysql.cargarTabla("select id_product, active from ps_product");

            if (status_products.hasRows())
            {
                foreach (DataRow item in status_products.Rows)
                {
                    string bloqueado = (item["active"].ToString() == "1") ? "'F'" : "'T'";
                    string id_product = "'" + item["id_product"].ToString() + "'";

                    csUtilidades.ejecutarConsulta("update articulo set kls_articulo_tienda = 'T', kls_articulo_bloquear = " + bloqueado + " where kls_id_shop = " + id_product,
                        false);
                }

                MessageBox.Show("COMPLETADO");
            }
            else
            {
                MessageBox.Show("NO HAY PRODUCTOS");
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            actualizarTallasYColores();
        }

        private void toolstripButtonA3_Click(object sender, EventArgs e)
        {

        }

        private void actualizarIDEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarIdPSenA3ERP();
            toolStripStatusLabel1.ForeColor = Color.Green;
            toolStripStatusLabel1.Text = "Proceso Realizado";
        }




        private void replicarImagenesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            csImagenesPS img = new csImagenesPS();

            img.copiarImagenes(dgvProductosA3.SelectedRows);
        }

        private void verificarArticulosEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataTable ArticulosPS = new DataTable();
            string consultaPS = "Select ps_product.id_product as ID_PRODUCT, ps_product.reference as REFERENCE from ps_product";
            ArticulosPS = mysql.cargarTabla(consultaPS);
            string referencePS = "";
            string codartA3 = "";

            for (int i = 0; i < dgvProductosA3.Rows.Count; i++)
            {
                codartA3 = dgvProductosA3.Rows[i].Cells[0].Value.ToString().Replace(" ", "");
                foreach (DataRow filaPS in ArticulosPS.Rows)
                {
                    referencePS = filaPS[1].ToString();
                    if (codartA3 == referencePS)
                    {
                        //dgvProductosA3.Rows.RemoveAt(i);
                        dgvProductosA3.Rows[i].Selected = true;
                        break;
                    }
                }

            }

            foreach (DataGridViewRow r in dgvProductosA3.SelectedRows)
            {
                dgvProductosA3.Rows.RemoveAt(r.Index);
            }

        }

       
        private void insertarDescripcionesA3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                {
                    sql.updateDescripa(fila);
                }
            }
            catch (Exception ex)
            { }
        }

        private void subirArticuloAPrestashopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            subirProductoPS();
        }

        private void artículoSeleccionadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvProductosA3.Rows.Count > 0)
            {
                int productos_seleccionados = dgvProductosA3.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de sincronizar {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                        {
                            TyC.sincronizarAtributosArticulos(item.Cells["ID EN TIENDA"].Value.ToString());
                        }

                        string.Format("{0} productos sincronizados con éxito", productos_seleccionados).mb();
                    }
                }
            }
            else
            {
                "Operación cancelada".mb();
            }
        }

        private void todosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new csTallasYColoresV2();
            tyc.sincronizarAtributosArticulos("");
            MessageBox.Show("ARTÍCULOS SINCRONIZADO");
        }

        private void sincronizarDescripcionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ("Vas a actualizar las descripciones de Prestashop de los productos seleccionados. ¿Procedemos?".what())
            {
                csSqlConnects sql = new csSqlConnects();

                DataTable dtCombo = sql.cargarDatosTablaA3("select * from kls_esync_idiomas");
                dtCombo.Rows.Add("Todos", "0");

                DataTable dtListBox = new DataTable();
                dtListBox.Columns.Add("name");
                dtListBox.Columns.Add("code");
                dtListBox.Rows.Add("Nombre del producto", "1");
                dtListBox.Rows.Add("Descripcion larga", "2");
                dtListBox.Rows.Add("Descripcion corta", "3");

                frDialogDescripciones multi = new frDialogDescripciones(dtListBox, dtCombo, "IDIOMAA3", "Campo a actualizar: ", "Selecciona el idioma: ", true);

                if (multi.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    List<string> rowCollection = new List<string>();
                    bool check = false;
                    foreach (DataGridViewRow cell in dgvProductosA3.SelectedRows)
                    {
                        string id_tienda = cell.Cells["ID EN TIENDA"].Value.ToString();
                        if (id_tienda != "")
                        {
                            rowCollection.Add(cell.Cells["ID EN TIENDA"].Value.ToString());
                            check = true;
                        }
                    }
                    string id_tienda_string = "";

                    if (rowCollection.Count > 0)
                    {
                        foreach (var row in rowCollection)
                        {
                            id_tienda_string += "'" + row + "',";
                        }

                        id_tienda_string = id_tienda_string.TrimEnd(',');
                    }

                    string idioma = sql.obtenerCampoTabla("select idiomaps from kls_esync_idiomas where idiomaa3 = '" + csGlobal.comboFormValue + "'");

                    string where = "";
                    if (idioma != "")
                    {
                        where = " and idiomaps = '" + idioma + "'";
                    }

                    if (id_tienda_string != "")
                    {
                        where += string.Format(" and kls_id_shop in ({0})", id_tienda_string);
                    }

                    DataTable descripciones = sql.cargarDatosTablaA3("SELECT dbo.ARTICULO.CODART, " +
                                                                    "       dbo.ARTICULO.DESCART, " +
                                                                    "       dbo.ARTICULO.KLS_ID_SHOP, " +
                                                                    "       dbo.DESCRIPA.CODIDIOMA, " +
                                                                    "       dbo.KLS_ESYNC_IDIOMAS.IDIOMAPS, " +
                                                                    "       dbo.DESCRIPA.DESCART AS NAME, " +
                                                                    "       dbo.DESCRIPA.DESCCORTA, " +
                                                                    "       dbo.DESCRIPA.TEXTO, " +
                                                                    "       dbo.DESCRIPA.DESCLARGA, " +
                                                                    "       dbo.DESCRIPA.KLS_DESC_LARGA " +
                                                                    "FROM dbo.DESCRIPA " +
                                                                    "INNER JOIN dbo.ARTICULO ON dbo.DESCRIPA.CODART = dbo.ARTICULO.CODART " +
                                                                    "INNER JOIN dbo.KLS_ESYNC_IDIOMAS ON dbo.DESCRIPA.CODIDIOMA = dbo.KLS_ESYNC_IDIOMAS.IDIOMAA3 " +
                                                                    "WHERE (dbo.ARTICULO.KLS_ID_SHOP > '0')" + where);

                    if (check)
                    {
                        if (csUtilidades.verificarDt(descripciones))
                        {
                            foreach (DataRow item in descripciones.Rows)
                            {
                                string consulta = "";
                                switch (csGlobal.multiOptionFormValue)
                                {
                                    // name, long, short
                                    // Todos
                                    case 0:
                                        consulta = string.Format("update ps_product_lang set name = '{0}', description_short = '{1}', description = '{2}' where id_product = {3} and id_lang = {4}",
                                                                MySqlHelper.EscapeString(item["NAME"].ToString()),
                                                               //  MySqlHelper.EscapeString(csUtilidades.descriptionShort(item["TEXTO"].ToString(), 395)),
                                                                item["TEXTO"].ToString(),
                                                                MySqlHelper.EscapeString(item["KLS_DESC_LARGA"].ToString()),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                    // name
                                    case 1:
                                        consulta = string.Format("update ps_product_lang set name = '{0}' where id_product = {1} and id_lang = {2}",
                                                                MySqlHelper.EscapeString(item["NAME"].ToString()),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                    // long
                                    case 2:
                                        consulta = string.Format("update ps_product_lang set description = '{0}' where id_product = {1} and id_lang = {2}",
                                                                MySqlHelper.EscapeString(item["KLS_DESC_LARGA"].ToString()),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                    // short
                                    case 3:
                                        /*consulta = string.Format("update ps_product_lang set description_short = '{0}' where id_product = {1} and id_lang = {2}",
                                                                MySqlHelper.EscapeString(csUtilidades.descriptionShort(item["TEXTO"].ToString(), 395)),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());*/
                                        consulta = string.Format("update ps_product_lang set description_short = '{0}' where id_product = {1} and id_lang = {2}",item["TEXTO"].ToString(),item["KLS_ID_SHOP"].ToString(),item["IDIOMAPS"].ToString());


                                        break;
                                    // name y long
                                    case 4:
                                        consulta = string.Format("update ps_product_lang set name = '{0}', description = '{1}' where id_product = {2} and id_lang = {3}",
                                                                MySqlHelper.EscapeString(item["NAME"].ToString()),
                                                                MySqlHelper.EscapeString(item["KLS_DESC_LARGA"].ToString()),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                    // name y short
                                    case 5:
                                        consulta = string.Format("update ps_product_lang set name = '{0}', description_short = '{1}' where id_product = {2} and id_lang = {3}",
                                                                MySqlHelper.EscapeString(item["NAME"].ToString()),
                                                                item["TEXTO"].ToString(),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                    // long y short
                                    case 6:
                                        consulta = string.Format("update ps_product_lang set description_short = '{0}', description = '{1}' where id_product = {2} and id_lang = {3}",
                                                                //MySqlHelper.EscapeString(csUtilidades.descriptionShort(item["TEXTO"].ToString(), 395)),
                                                                item["TEXTO"].ToString(),
                                                                MySqlHelper.EscapeString(item["KLS_DESC_LARGA"].ToString()),
                                                                item["KLS_ID_SHOP"].ToString(),
                                                                item["IDIOMAPS"].ToString());
                                        break;
                                }


                                csUtilidades.ejecutarConsulta(consulta, true);
                            }

                            MessageBox.Show("Descripciones actualizadas");
                        }
                        else
                        {
                            MessageBox.Show("No hay descripciones para actualizar");
                        }
                    }
                    else
                    {
                        "Los articulos seleccionados no tienen id en tienda".mb();
                    }
                }
            }
        }

        private void eliminarProductoPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvProductosA3.SelectedRows.Count > 0)
            {
                try
                {
                    string mensaje = "Estas a punto de borrar " + dgvProductosA3.SelectedRows.Count.ToString() + ", estás seguro/a??";
                    if (MessageBox.Show(mensaje, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                        {
                            string kls_id_shop = item.Cells["ID EN TIENDA"].Value.ToString();
                            string codart = item.Cells["CODIGO"].Value.ToString().Trim();
                            if (kls_id_shop != "")
                            {
                                //Añadido para CADCANARIAS
                                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                string sURL = csGlobal.APIUrl + "products/" + kls_id_shop + "?ws_key=" + csGlobal.webServiceKey;

                                WebRequest request = WebRequest.Create(sURL);
                                request.Method = "DELETE";

                                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                string consulta = "update articulo set kls_id_shop = null where ltrim(codart) = '" + codart + "'";
                                csUtilidades.ejecutarConsulta(consulta, false);
                            }
                        }

                        MessageBox.Show(dgvProductosA3.SelectedRows.Count.ToString() + " artículo/s borrado/s");
                        cargarArticulos();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }



        private void resetearAtributosSelToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DataTable articulos = new DataTable();
            articulos.Columns.Add("idProduct");
            int numfila = 0;
            csStocks stock = new csStocks();
            string producto = "";
            string codart = "";

            if (dgvProductosA3.Rows.Count > 0)
            {
                int productos_seleccionados = dgvProductosA3.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de resetear {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                        {
                            DataRow fila = articulos.NewRow();
                            fila["idProduct"] = item.Cells["ID EN TIENDA"].Value.ToString();
                            articulos.Rows.Add(fila);
                            numfila++;
                            if (numfila == 500)
                            {
                                //TyC.resetearTyC(item.Cells["ID EN TIENDA"].Value.ToString());
                                TyC.resetearTyC(articulos);
                                articulos.Rows.Clear();
                                numfila = 0;
                            }
                        }
                        if (numfila < 500 && numfila > 0)
                        {
                            TyC.resetearTyC(articulos);
                        }

                        "Producto reseteado con exito".mb();

                        if ("Quieres sincronizar estos atributos?".what())
                        {
                            foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                            {
                                producto = item.Cells["ID EN TIENDA"].Value.ToString();
                                codart = item.Cells["CODIGO"].Value.ToString();

                                //visible_combinations es si la tienda muestra todas las combinaciones
                                //por ejemplo Hebo, no muestra todas las combinaciones
                                if (csGlobal.ps_visible_combinations)
                                {
                                    csUtilidades.ejecutarConsulta("DELETE FROM KLS_COMBINACIONES_VISIBLES WHERE LTRIM(CODART)='" + codart + "'", false);
                                    TyC.generarTodasLasCombinacionesA3(producto);
                                }
                                TyC.sincronizarAtributosArticulos(producto);
                                actualizarTotalesArticulo(producto);
                            }
                            if (csGlobal.ocultarArticuloSiStock0)
                            {
                                stock.actualizarVisibilidadArticulosSinStockIndusnow();
                            }

                            "Atributos sincronizados".mb();
                        }
                        else
                        {
                            "Operación cancelada".mb();
                        }
                    }
                    else
                    {
                        "Operación cancelada".mb();
                    }
                }
            }
        }


        private void actualizarTotalesArticulo(string articulo)
        {
            string scriptUpdateTotal = "insert into ps_stock_available (id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock) " +
                            " (select id_product,0,1,0,sum(quantity),0,out_of_stock " +
                            " from ps_stock_available  where id_product_attribute<>0 and id_product=" + articulo + " group by id_product,out_of_stock)";
            csUtilidades.ejecutarConsulta(scriptUpdateTotal, true);

        }

        private void regenerarPosicionesCategoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCat = new DataTable();
                string aux = treeCat1.SelectedNode.Text;
                dtCat = mysql.obtenerDatosPS("select id_category from ps_category_lang where name = '" + aux + "' GROUP BY id_category;");

                string id_category = "";
                string consulta_cat = "";
                string product_cat = "";
                string consulta = "";
                //DataTable dt = mysql.cargarTabla(consulta);
                foreach (DataRow row in dtCat.Rows)
                {
                    //id_category = mysql.obtenerDatoFromQuery(string.Format("select id_category from ps_category_lang where name = '{0}'", row[0].ToString()));
                    id_category = row[0].ToString();
                    consulta = string.Format("select * from ps_category_product where id_category = {0} order by id_category", id_category);
                    DataTable dt = mysql.cargarTabla(consulta);
                    if (dt.hasRows())
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            product_cat = dt.Rows[i]["id_product"].ToString();
                            consulta_cat = string.Format("update ps_category_product set position = {0} where id_product = {1} and id_category = {2}", i, product_cat, row[0].ToString());
                            csUtilidades.ejecutarConsulta(consulta_cat, true);
                        }


                    }
                }
                string.Format("Asignación de las posiciones finalizada para la categoria de:\n\n\n\t{0}", treeCat1.SelectedNode.Text).mb();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void addCatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool check = false;
            Dictionary<int, string> nombres = new Dictionary<int, string>();

            foreach (var lang in csGlobal.IdiomasEsync)
            {
                frDialogInput input = new frDialogInput(string.Format("({0}) Nombre de categoria", lang.Value), "", node_selected_name);
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    nombres.Add(lang.Key, csGlobal.comboFormValue);
                }
                else
                {
                    check = true;
                }
            }

            if (!check)
            {
                csPrestashop ps = new csPrestashop();
                ps.addCategory(nombres, node_selected_id);

                "Categoria creada con exito".mb();

                cargarNodosCategorias();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void treeCat1_MouseUp(object sender, MouseEventArgs e)
        {

        }

        private void treeCat1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            node_selected_name = e.Node.Text;
            node_selected_id = e.Node.ToolTipText;
            cATEGORIASToolStripMenuItem.Text = "CATEGORIA - " + node_selected_name + " (" + node_selected_id + ")";

        }

        private void eliminarCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (csUtilidades.ventanaPassword())
            {
                categoriasToCreate.Columns.Add("Nombre Categoria");
                categoriasToCreate.Columns.Add("Id Categoria");
                List<string> lista = CheckedNames(treeCat1.Nodes);
                csPrestashop ps = new csPrestashop();
                string category_id = "";


                if (string.Format("Vas a borrar las categorías marcadas, ¿Estas seguro?").what())
                {
                    foreach (DataRow dr in categoriasToCreate.Rows)
                    {
                        category_id = dr["Id Categoria"].ToString();
                        ps.deleteCategory(category_id);
                    }

                    "Categorias eliminadas".mb();

                    cargarNodosCategorias();
                    categoriasToCreate.Columns.Clear();
                    categoriasToCreate.Rows.Clear();
                }
                else
                {
                    csUtilidades.operacionCancelada();
                }
            }
        }

        private void renombrarCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renombrarCategorias();
        }
        private void renombrarCategorias()
        {
            bool check = false;
            Dictionary<int, string> nombres = new Dictionary<int, string>();

            foreach (var lang in csGlobal.IdiomasEsync)
            {
                frDialogInput input = new frDialogInput(string.Format("({0}) Nombre de categoria", lang.Value), "", node_selected_name);
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    nombres.Add(lang.Key, csGlobal.comboFormValue);
                }
                else
                {
                    check = true;
                }
            }

            if (!check)
            {
                csPrestashop ps = new csPrestashop();
                ps.renameCategory(nombres, node_selected_id);

                "Categoria renombrada con exito".mb();

                cargarNodosCategorias();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }

        }

        private void modoReplicarImágenesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void irACategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csUtilidades.goToWeb(csGlobal.nodeTienda + "/index.php?controller=category&id_category=" + node_selected_id);
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)13)
                {
                    string filtro = string.Format("(ps_product_lang.name LIKE '%{0}%' OR ps_category_lang.name LIKE '%{0}%' OR ps_product.reference LIKE '%{0}%')", toolStripTextBox1.Text);
                    csMySqlConnect mysql = new csMySqlConnect();
                    csSqlScripts script = new csSqlScripts();
                    dgvCat.DataSource = null;
                    dgvCat.DataSource = mysql.cargarTabla(script.getProductsByName(filtro));
                    tabCat.Text = "Asignación categorías - Busqueda";

                    contarFilasGrid(dgvCat);
                    if (dgvCat.Rows.Count > 0)
                        CategoriaInicio = dgvCat.Rows[0].Cells["category_name"].Value.ToString();
                }
            }
            catch (Exception)
            {
            }
        }

        private void desactivarCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.Format("Quieres desactivar la categoria: {0}?", node_selected_name).what())
            {
                csPrestashop ps = new csPrestashop();
                ps.deactivateCategory(node_selected_id, "0");

                "Categoria desactivada con exito".mb();

                cargarNodosCategorias();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void activarCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.Format("Quieres activar la categoria: {0}?", node_selected_name).what())
            {
                csPrestashop ps = new csPrestashop();
                ps.deactivateCategory(node_selected_id, "1");

                "Categoria activada con exito".mb();

                cargarNodosCategorias();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void filtrarPorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (dgvProductosA3.SelectedCells.Count == 1)
            //{
            //    string cellValue = dgvProductosA3.SelectedCells[0].Value.ToString();

            //    frDialogInput input = new frDialogInput("Filtro: ", cellValue);
            //    if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //    {
            //        string columnHeader = dgvProductosA3.SelectedCells[0].OwningColumn.HeaderText;

            //        string where = string.Format("where {0} = '{1}'", columnHeader, csGlobal.comboFormValue);

            //        csSqlConnects sql = new csSqlConnects();
            //        string selectScript = "SELECT LTRIM(ARTICULO.CODART) AS CODART, DESCART, PRCVENTA, KLS_ARTICULO_TIENDA, " +
            //                          " KLS_ID_SHOP, KLS_ARTICULO_BLOQUEAR, TALLAS, KLS_MARCAS.kls_descmarca " +
            //                          ", caracteristicas.DESCCAR " +
            //                          " FROM DBO.ARTICULO  WITH (NOLOCK) " +
            //                          " left join caracteristicas on caracteristicas.CODCAR = articulo.car1 " +
            //                          " LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA " + where;

            //        csUtilidades.addDataSource(dgvProductosA3, sql.cargarDatosTablaA3(selectScript));
            //    }
            //    else
            //    {
            //        csUtilidades.operacionCancelada();
            //    }
            //}


        }

        private void cargarArticulosStock(string where = "")
        {
            this.tipoBusqueda = true;
            try
            {
                string sb = "";
                csSqlConnects sql = new csSqlConnects();

                if (csGlobal.tieneTallas)
                {
                    if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                    {
                        sb = "SELECT LTRIM(dbo.STOCKALM.CODART) AS CODIGO, " +
                        "       ARTICULO.DESCART, " +
                        "       dbo.ARTICULO.PRCVENTA AS PRECIO, " +
                        "       dbo.ARTICULO.KLS_ARTICULO_TIENDA AS 'VISIBLE TIENDA', " +
                        "       ARTICULO.KLS_ID_SHOP AS 'ID EN TIENDA', " +
                        "       ARTICULO.KLS_ARTICULO_BLOQUEAR AS BLOQUEAR, " +
                        "       ARTICULO.TALLAS AS TYC, " +
                        "       KLS_MARCAS.kls_descmarca AS MARCA, " +
                        "       SUM(dbo.STOCKALM.UNIDADES) AS STOCK_A3 " +
                        "FROM dbo.STOCKALM " +
                        "INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                        "LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA " +
                        " where ltrim(articulo.codart) not like '%''%' " +
                        where +
                        "GROUP BY dbo.STOCKALM.CODART, " +
                        "         ARTICULO.DESCART, " +
                        "         dbo.ARTICULO.PRCVENTA, " +
                        "         ARTICULO.KLS_ARTICULO_TIENDA, " +
                        "         ARTICULO.KLS_ID_SHOP, " +
                        "         ARTICULO.KLS_ARTICULO_BLOQUEAR, " +
                        "         ARTICULO.TALLAS, " +
                        "         KLS_MARCAS.kls_descmarca HAVING SUM(dbo.STOCKALM.UNIDADES) > 0 ";
                    }
                    else if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI") // MIRO
                    {
                        // TODO
                    }
                }
                else
                {
                    sb = "SELECT LTRIM(dbo.STOCKALM.CODART) AS CODIGO, " +
                    "       ARTICULO.DESCART, " +
                    "       dbo.ARTICULO.PRCVENTA AS PRECIO, " +
                    "       dbo.ARTICULO.KLS_ARTICULO_TIENDA AS 'VISIBLE TIENDA', " +
                    "       ARTICULO.KLS_ID_SHOP AS 'ID EN TIENDA', " +
                    "       ARTICULO.KLS_ARTICULO_BLOQUEAR AS BLOQUEAR, " +
                    "       ARTICULO.TALLAS AS TYC, " +
                    "       KLS_MARCAS.kls_descmarca AS MARCA, " +
                    "       SUM(CASE WHEN (dbo.STOCKALM.UNIDADES<0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                    "FROM dbo.STOCKALM WITH (NOLOCK) " +
                    "INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                    "LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA " +
                    "WHERE dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T' " +
                    "  AND dbo.ARTICULO.ESKIT = 'F' " +
                    "  AND LTRIM(dbo.STOCKALM.CODALM) IN ('1') " +
                    "  AND KLS_ID_SHOP > 0 " +
                    "GROUP BY LTRIM(dbo.STOCKALM.CODART), " +
                    "         dbo.ARTICULO.PRCVENTA, " +
                    "         dbo.ARTICULO.TALLAS, " +
                    "         ARTICULO.KLS_ARTICULO_BLOQUEAR, " +
                    "         ARTICULO.DESCART, " +
                    "         dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                    "         KLS_ID_SHOP, " +
                    "         KLS_MARCAS.kls_descmarca HAVING SUM(CASE WHEN (dbo.STOCKALM.UNIDADES<0) THEN 0 ELSE dbo.STOCKALM.UNIDADES END) > 0 " +
                    "UNION " +
                    "SELECT LTRIM(dbo.STOCKALM.CODART) AS CODIGO, " +
                    "       ARTICULO.DESCART, " +
                    "       dbo.ARTICULO.PRCVENTA AS PRECIO, " +
                    "       dbo.ARTICULO.KLS_ARTICULO_TIENDA AS 'VISIBLE TIENDA', " +
                    "       ARTICULO.KLS_ID_SHOP AS 'ID EN TIENDA', " +
                    "       ARTICULO.KLS_ARTICULO_BLOQUEAR AS BLOQUEAR, " +
                    "       ARTICULO.TALLAS AS TYC, " +
                    "       KLS_MARCAS.kls_descmarca AS MARCA, " +
                    "       MIN(CASE WHEN dbo.STOCKALM.UNIDADES IS NULL THEN 0 ELSE dbo.STOCKALM.UNIDADES END) AS STOCK_A3 " +
                    "FROM dbo.ESCANDALLO " +
                    "LEFT OUTER JOIN dbo.STOCKALM ON dbo.ESCANDALLO.CODARTC = dbo.STOCKALM.CODART " +
                    "LEFT OUTER JOIN dbo.ARTICULO ON dbo.ESCANDALLO.CODARTP = dbo.ARTICULO.CODART " +
                    "LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA " +
                    "GROUP BY dbo.stockalm.CODART, " +
                    "         dbo.ESCANDALLO.CODALM, " +
                    "         dbo.ARTICULO.PRCVENTA, " +
                    "         dbo.ARTICULO.ESKIT, " +
                    "         dbo.ARTICULO.KLS_ID_SHOP, " +
                    "         ARTICULO.KLS_ARTICULO_BLOQUEAR, " +
                    "         dbo.ARTICULO.DESCART, " +
                    "         dbo.ARTICULO.TALLAS, " +
                    "         KLS_MARCAS.kls_descmarca, " +
                    "         dbo.ARTICULO.KLS_ARTICULO_TIENDA HAVING (dbo.ARTICULO.ESKIT = 'T') " +
                    "AND (dbo.ARTICULO.KLS_ID_SHOP > 0) " +
                    "AND (dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T') " +
                    "AND MIN(CASE WHEN dbo.STOCKALM.UNIDADES IS NULL THEN 0 ELSE dbo.STOCKALM.UNIDADES END) > 0 ";
                }

                var watch = Stopwatch.StartNew();
                csUtilidades.cargarDGV(dgvProductosA3, sb, false);

                contarFilasGrid(dgvProductosA3);

                watch.Stop();
                if (watch.Elapsed.Seconds >= 1)
                    tssInfoTime.Text = watch.Elapsed.Seconds + " s";
                else
                    tssInfoTime.Text = watch.Elapsed.Milliseconds + " ms";
            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }
        }

        private void cargarArticulosConStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cargarArticulosStock();
        }

        private void cargarArticulosMalCodificadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string sb = "";
                csSqlConnects sql = new csSqlConnects();


                sb = " SELECT LTRIM(dbo.STOCKALM.CODART) AS CODIGO, " +
                " ARTICULO.DESCART, " +
                " dbo.ARTICULO.PRCVENTA AS PRECIO, " +
                " dbo.ARTICULO.KLS_ARTICULO_TIENDA AS 'VISIBLE TIENDA', " +
                " ARTICULO.KLS_ID_SHOP AS 'ID EN TIENDA', " +
                " ARTICULO.KLS_ARTICULO_BLOQUEAR AS BLOQUEAR, " +
                " ARTICULO.TALLAS AS TYC, " +
                " KLS_MARCAS.kls_descmarca AS MARCA, " +
                " SUM(dbo.STOCKALM.UNIDADES) AS STOCK_A3 " +
                " FROM dbo.STOCKALM " +
                "  INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                " LEFT JOIN KLS_MARCAS ON KLS_MARCAS.KLS_CODMARCA = ARTICULO.KLS_CODMARCA  " +
                " where ltrim(articulo.codart) LIKE '%''%' " +
                "   GROUP BY dbo.STOCKALM.CODART, " +
                "  ARTICULO.DESCART, " +
                "  dbo.ARTICULO.PRCVENTA, " +
                "  ARTICULO.KLS_ARTICULO_TIENDA, " +
                "  ARTICULO.KLS_ID_SHOP, " +
                "  ARTICULO.KLS_ARTICULO_BLOQUEAR, " +
                "  ARTICULO.TALLAS, " +
                "  KLS_MARCAS.kls_descmarca";

                var watch = Stopwatch.StartNew();
                csUtilidades.cargarDGV(dgvProductosA3, sb, false);

                contarFilasGrid(dgvProductosA3);

                watch.Stop();
                if (watch.Elapsed.Seconds >= 1)
                    tssInfoTime.Text = watch.Elapsed.Seconds + " s";
                else
                    tssInfoTime.Text = watch.Elapsed.Milliseconds + " ms";
            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }
        }

        private void asignarACategoriaPadreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.Format("Quieres asignar la categoria: {0} a una nueva categoria?", node_selected_name).what())
            {
                DataTable combo_dt = mysql.cargarTabla("select name from ps_category_lang where id_lang = (Select value from ps_configuration where name='PS_LANG_DEFAULT') order by id_category");

                frDialogCombo combo = new frDialogCombo(combo_dt, "name", "Selecciona la cat. padre: ", true);

                if (combo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string selected_value_combo = csGlobal.comboFormValue;
                    string id_parent_new = mysql.obtenerDatoFromQuery(string.Format("select id_category from ps_category_lang where name = '{0}' and id_lang = {1}", selected_value_combo, csUtilidades.idIdiomaDefaultPS()));
                    string id_parent_old = node_selected_id;

                    string consulta = string.Format("update ps_category set id_parent = {0} where id_category = {1}", id_parent_new, id_parent_old);
                    csUtilidades.ejecutarConsulta(consulta, true);

                    "Categoria alternada con éxito".mb();

                    cargarNodosCategorias();
                }
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }



        private void btHide_Click(object sender, EventArgs e)
        {
            ocultarPanelBusqueda();

        }

        private void ocultarPanelBusqueda()
        {
            splitContainer2.SplitterDistance = 30;
        }

        private void mostrarPanelBusqueda()
        {
            splitContainer2.SplitterDistance = 250;
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            mostrarPanelBusqueda();
        }

        private void cargarMarcas()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS KLS_CODMARCA,'' AS KLS_DESCMARCA " +
                                                        " UNION " +
                                                        " SELECT KLS_CODMARCA, KLS_DESCMARCA FROM KLS_MARCAS " +
                                                        " ORDER BY KLS_CODMARCA ", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxMarcas.ValueMember = "KLS_CODMARCA";
                cboxMarcas.DisplayMember = "KLS_DESCMARCA";
                cboxMarcas.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarColecciones()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS KLS_CODCOLECCION,'' AS KLS_DESCCOLECCION " +
                                                        " UNION " +
                                                        " SELECT KLS_CODCOLECCION, KLS_DESCCOLECCION " +
                                                        " FROM KLS_COLECCIONES ORDER BY KLS_CODCOLECCION", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxColecciones.ValueMember = "KLS_CODCOLECCION";
                cboxColecciones.DisplayMember = "KLS_DESCCOLECCION";
                cboxColecciones.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void btLoadAdvance_Click(object sender, EventArgs e)
        {
            cargarBusquedaArticulos();
        }

        private void cargarBusquedaArticulos()
        {
            string Marca = "";
            string Coleccion = "";
            if (cboxMarcas.SelectedIndex == -1)
            {
                Marca = "0";
            }
            else
            {
                Marca = cboxMarcas.SelectedValue.ToString();
            }

            if (cboxColecciones.SelectedIndex == -1)
            {
                Coleccion = "0";
            }
            else
            {
                Coleccion = cboxColecciones.SelectedValue.ToString();

            }
            buscarLupa(tbFilterDesc.Text, Marca, Coleccion);


        }

        private void btClearFilter_Click(object sender, EventArgs e)
        {
            borrarFiltros();
        }

        private void borrarFiltros()
        {
            tbFilterDesc.Text = "";
            cboxMarcas.SelectedValue = "0";
            cboxColecciones.SelectedValue = "0";
        }





        private void btAddCategories_Click(object sender, EventArgs e)
        {
            accionCrearCategoriasMultiple();
        }

        DataTable categoriasToCreate = new DataTable();

        private void accionCrearCategoriasMultiple()
        {



            categoriasToCreate.Columns.Add("Nombre Categoria");
            categoriasToCreate.Columns.Add("Id Categoria");

            List<string> lista = CheckedNames(treeCat1.Nodes);
            crearCategoriaPS(categoriasToCreate);

            categoriasToCreate.Columns.Clear();
            categoriasToCreate.Rows.Clear();

            treeCat1.Focus();
            treeCat1.SelectedNode = tempTreeNode;

        }
        private TreeNode tempTreeNode;

        List<String> CheckedNames(System.Windows.Forms.TreeNodeCollection theNodes)
        {
            List<String> aResult = new List<String>();

            if (theNodes != null)
            {
                foreach (System.Windows.Forms.TreeNode aNode in theNodes)
                {

                    if (aNode.Checked)
                    {
                        DataRow dr = categoriasToCreate.NewRow();
                        aResult.Add(aNode.Text);
                        dr["Nombre Categoria"] = aNode.Text;
                        dr["Id Categoria"] = aNode.Name;
                        categoriasToCreate.Rows.Add(dr);


                        tempTreeNode = aNode;
                    }

                    aResult.AddRange(CheckedNames(aNode.Nodes));
                }
            }

            return aResult;
        }


        private void crearCategoriaPS(DataTable dt)
        {

            csPrestashop ps = new csPrestashop();
            Dictionary<int, string> nombres = new Dictionary<int, string>();

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataGridViewRow fila in dgvNewCategories.Rows)
                {
                    if (fila.Cells["Valor"].Value != null)
                    {
                        foreach (var lang in csGlobal.IdiomasEsync)
                        {
                            nombres.Add(lang.Key, fila.Cells["Valor"].Value.ToString());
                        }
                        ps.addCategory(nombres, dr["Id Categoria"].ToString());
                        nombres.Clear();
                    }
                }
            }

            cargarNodosCategorias();

        }

        private void comprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeCat1.CollapseAll();
        }

        private void expandirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeCat1.ExpandAll();
        }



        private void dgvProductosA3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (this.dgvProductosA3.Columns[e.ColumnIndex].Name == "PS")
                {

                    string urlProduct = "";
                    string idProduct = "";

                    if (dgvProductosA3["ID EN TIENDA", e.RowIndex].Value.ToString() != "" && dgvProductosA3["ID EN TIENDA", e.RowIndex].Value.ToString() != "0" && dgvProductosA3["ID EN TIENDA", e.RowIndex].Value.ToString() != string.Empty)
                    {
                        try
                        {
                            idProduct = dgvProductosA3["ID EN TIENDA", e.RowIndex].Value.ToString();
                            urlProduct = csGlobal.nodeTienda + "/index.php?id_product=" + idProduct + "&controller=product&id_lang=" + csGlobal.defaultIdLangPS;
                            Process.Start("Chrome.exe", urlProduct);
                        }
                        catch
                        {
                            Process.Start("IExplore.exe", urlProduct);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El artículo no está en la tienda");
                    }
                }
            }
        }

        private void dgvProductosA3_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgvProductosA3.Columns[e.ColumnIndex].Name == "PRECIO")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            if (dgvProductosA3.Rows.Count > 0)
            {
                if (this.dgvProductosA3.Columns[e.ColumnIndex].Name == "PS")
                {
                    if (!string.IsNullOrEmpty(this.dgvProductosA3["ID EN TIENDA", e.RowIndex].Value.ToString()))
                    {
                        if ((int)this.dgvProductosA3["ID EN TIENDA", e.RowIndex].Value > 0)
                        {
                            //e.Value = Image.FromFile("C:\\Users\\lluisvera\\Downloads\\bag.png");
                            e.Value = Properties.Resources.esync32;
                        }
                        else
                        {
                            e.Value = Properties.Resources.noImage;
                        }
                    }
                    else
                    {
                        e.Value = Properties.Resources.noImage;
                    }
                }

                if (this.dgvProductosA3.Columns[e.ColumnIndex].Name == "ATR")
                {

                    if (this.dgvProductosA3["TyC", e.RowIndex].Value.ToString() == "T")
                    {
                        e.Value = Properties.Resources.tycAttributes;
                    }
                    else
                    {
                        e.Value = Properties.Resources.tycAttributesVoid;
                    }

                }
            }

        }

        private void establecerColumnaImagen()
        {
            if (!dgvProductosA3.Columns.Contains("PS"))
            {
                DataGridViewImageColumn imagen = new DataGridViewImageColumn();
                imagen.Name = "PS";
                dgvProductosA3.Columns.Add(imagen);
                dgvProductosA3.Columns["PS"].DisplayIndex = 0;
            }
            if (!dgvProductosA3.Columns.Contains("ATR"))
            {
                DataGridViewImageColumn imagen = new DataGridViewImageColumn();
                imagen.Name = "ATR";
                dgvProductosA3.Columns.Add(imagen);
                dgvProductosA3.Columns["ATR"].DisplayIndex = 1;
            }
            dgvProductosA3.Columns["PS"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dgvProductosA3.Columns["ATR"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

        }

        private void actualizarArtículosEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string paramTienda = "";

            if (csGlobal.conexionDB.Contains("ANDREU"))
                paramTienda = "andreutoys";

            actualizarArticulosEnPS(paramTienda);
        }

        private void actualizarArticulosEnPS(string paramtienda)
        {

            //variables funcionales del metodo
            List<string> dbOperations = new List<string>();


            string consultaA3 = "";
            string updateQuery = "";


            DataTable dt = new DataTable();
            csSqlConnects sqlConnect = new csSqlConnects();


            // variables General
            string product_widht = "";
            string product_height = "";
            string product_depth = "";
            string product_weight = "";
            string product_minimal_quantity = "";
            string ean13 = "";
            string idProduct = "";

            //variables Especifico para packaging
            string packAlto = "";
            string packAncho = "";
            string packLargo = "";


            //seleccionamos la consulta segun la tienda
            switch (paramtienda)
            {
                case "andreutoys":
                    consultaA3 = "SELECT " +
                                " LTRIM(dbo.ARTICULO.CODART) AS Reference,KLS_ID_SHOP, IF_PACKAG_ANCHO, IF_PACKAG_ALTO,  " +
                                " IF_PACKAG_LARGO, CONVERT(int, IF_MINIMVENDA)as IF_MINIMVENDA, " +
                                " IF_UD_VTA_ALTO, IF_UD_VTA_ANCHO , IF_UD_VTA_LARGO , IF_PACKAG_ALTO , IF_PACKAG_ANCHO, IF_PACKAG_LARGO, dbo.ALTERNA.CODALT AS EAN " +
                                " FROM  dbo.ARTICULO " +
                                " LEFT OUTER JOIN dbo.ALTERNA on dbo.ALTERNA.CODART=dbo.ARTICULO.CODART" +
                                " WHERE (KLS_ID_SHOP >0) ORDER BY KLS_ID_SHOP";

                    break;

                case "":

                    consultaA3 = "SELECT " +
                                " LTRIM(dbo.ARTICULO.CODART) AS Reference, dbo.ARTICULO.KLS_ID_SHOP as KLS_ID_SHOP, dbo.ARTICULO.IF_PACKAG_ANCHO, dbo.ARTICULO.IF_PACKAG_ALTO,  " +
                                " dbo.ARTICULO.IF_PACKAG_LARGO, dbo.ARTICULO.IF_PACKAG_PESO_BRUTO, CONVERT(int, dbo.ARTICULO.IF_MINIMVENDA) IF_MINIMVENDA, dbo.ALTERNA.CODALT AS EAN " +
                                " FROM            dbo.ARTICULO LEFT OUTER JOIN " +
                                " dbo.ALTERNA ON dbo.ARTICULO.CODART = dbo.ALTERNA.CODART " +
                                " WHERE        (dbo.ARTICULO.KLS_ID_SHOP >0) ORDER BY KLS_ID_SHOP";
                    break;
            }






            dt = sqlConnect.cargarDatosTablaA3(consultaA3);


            foreach (DataRow dr in dt.Rows)
            {

                idProduct = dr["KLS_ID_SHOP"].ToString();

                switch (paramtienda)
                {
                    case "andreutoys":

                        
                        ean13= !string.IsNullOrEmpty(dr["EAN"].ToString()) ? dr["EAN"].ToString() : "' '";
                        product_minimal_quantity = !string.IsNullOrEmpty(dr["IF_MINIMVENDA"].ToString()) ? dr["IF_MINIMVENDA"].ToString() : "1";
                        product_height = !string.IsNullOrEmpty(dr["IF_UD_VTA_ALTO"].ToString()) ? dr["IF_UD_VTA_ALTO"].ToString() : "0";
                        product_widht = !string.IsNullOrEmpty(dr["IF_UD_VTA_ANCHO"].ToString()) ? dr["IF_UD_VTA_ANCHO"].ToString() : "0";
                        product_depth = !string.IsNullOrEmpty(dr["IF_UD_VTA_LARGO"].ToString()) ? dr["IF_UD_VTA_LARGO"].ToString() : "0";

                        packAlto = !string.IsNullOrEmpty(dr["IF_PACKAG_ALTO"].ToString()) ? dr["IF_PACKAG_ALTO"].ToString() : "0";
                        packAncho = !string.IsNullOrEmpty(dr["IF_PACKAG_ANCHO"].ToString()) ? dr["IF_PACKAG_ANCHO"].ToString() : "0";
                        packLargo = !string.IsNullOrEmpty(dr["IF_PACKAG_LARGO"].ToString()) ? dr["IF_PACKAG_LARGO"].ToString() : "0";

                        product_height = product_height.Replace(",", ".");
                        product_widht = product_widht.Replace(",", ".");
                        product_depth = product_depth.Replace(",", ".");
                        packAlto = packAlto.Replace(",", ".");
                        packAncho = packAncho.Replace(",", ".");
                        packLargo = packLargo.Replace(",", ".");

                        updateQuery = "update ps_product set " +
                      "ean13=" + ean13+
                      ",width=" + product_widht +
                      " ,height=" + product_height +
                      " ,depth=" + product_depth +
                      " , kls_width_pack=" + packAncho +
                      " , kls_height_pack=" + packAlto +
                      " , kls_depth_pack=" + packLargo +
                      " , minimal_quantity=" + product_minimal_quantity +
                      " where id_product=" + idProduct;

                        dbOperations.Add(updateQuery);

                        updateQuery = "update ps_product_shop set " +
                       " minimal_quantity=" + product_minimal_quantity +
                       " where id_product=" + idProduct;

                        dbOperations.Add(updateQuery);


                        break;
                    case "":
                        ean13 = !string.IsNullOrEmpty(dr["EAN"].ToString()) ? dr["EAN"].ToString() :"' '";
                        product_widht = dr["IF_PACKAG_ANCHO"].ToString();
                        product_height = dr["IF_PACKAG_ALTO"].ToString();
                        product_depth = dr["IF_PACKAG_LARGO"].ToString();
                        product_weight = dr["IF_PACKAG_PESO_BRUTO"].ToString();
                        product_minimal_quantity = dr["IF_minimvenda"].ToString();
                        ean13 = dr["CODALT"].ToString().Trim();
                        idProduct = dr["KLS_ID_SHOP"].ToString();

                        product_widht = product_widht.Replace(",", ".");
                        product_height = product_height.Replace(",", ".");
                        product_depth = product_depth.Replace(",", ".");
                        product_weight = product_weight.Replace(",", ".");
                        product_minimal_quantity = product_minimal_quantity.Replace(",0000", "");


                        updateQuery = "update ps_product set " +
                        "ean13=" + ean13 + "'," +
                        " width='" + product_widht + "'," +
                        " height='" + product_height + "'," +
                        " depth='" + product_depth + "'," +
                        " minimal_quantity ='" + product_minimal_quantity + "'," +
                        " ean13='" + ean13 + "'" +
                        " where id_product=" + idProduct;

                        dbOperations.Add(updateQuery);

                        updateQuery = "update ps_product_shop set " +
                       " minimal_quantity=" + product_minimal_quantity + "," +
                       " where id_product=" + idProduct;

                        dbOperations.Add(updateQuery);
                        break;
                }


            }
            //recorremos la lista y ejecutamos las consultas con una única apertura de la DB.
            try
            {
                string cadena = csGlobal.cadenaConexionPS;
                using (MySqlConnection mcon = new MySqlConnection(cadena))
                {

                    mcon.Open();
                    foreach (string query in dbOperations)
                    {
                        try
                        {
                            MySqlCommand cmd = new MySqlCommand(query, mcon);
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Program.guardarErrorFichero("Fallo actualizar producto en PS. Consulta: " + query + "\n " +
                                " Error:" + e.StackTrace);
                        }
                    }
                    mcon.Close();

                }
            }
            catch (Exception e)
            {
            }
        }

        private void validarPresenciaEnLaTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable articulos = new DataTable();
            articulos.Columns.Add("idProduct");
            int numfila = 0;
            string klsIdShop = "";
            string codart = "";
            string idproduct = "";




            foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
            {
                klsIdShop = item.Cells["ID EN TIENDA"].Value.ToString();
                codart = item.Cells["CODIGO"].Value.ToString();
                idproduct = mysql.obtenerDatoFromQuery("select reference from ps_product where id_product=" + klsIdShop);

                if (idproduct == "")
                {

                    csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_ID_SHOP=NULL WHERE LTRIM(CODART)='" + codart + "'", false);
                }
            }


        }

      /*  private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string codart = "";
            string idproduct = "";
            csStocks stock = new klsync.csStocks();

            if (csGlobal.activarMonotallas == "Y")
            {
                csTallasYColoresV2 tyc = new csTallasYColoresV2();
                tyc.marcarArticulosMonoatributoEnA3ERP("");
            }

            foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
            {
                try
                {
                    codart = fila.Cells["CODIGO"].Value.ToString();
                    idproduct = fila.Cells["ID EN TIENDA"].Value.ToString();
                    stock.stock(idproduct, codart);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("NO SE HA PODIDO ACTUALIZAR EL STOCK");
                    Program.guardarErrorFichero("Error actualizando stock:" + Environment.NewLine + " Información: " + ex.StackTrace);
                }

            }
            MessageBox.Show("Stock actualizado");
        }*/
        
        private void frArticulos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                cargarBusquedaArticulos();
            }

        }

        private void contextMenuProd_Opening(object sender, CancelEventArgs e)
        {
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE"))
            {
                contextMenuProd.Items["generarFicheroLogistica"].Visible = true;
            }
            else
            {
                contextMenuProd.Items["generarFicheroLogistica"].Visible = false;
            }
        }

        private void generarFicheroLogistica_Click(object sender, EventArgs e)
        {
            csBastide bastide = new csBastide();
            bastide.generarFicheroTXTAlmacenLogistica();
        }

        private void actualizarInformaciónPackagingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                csPSWebService psWS = new klsync.csPSWebService();
                string idProduct = "";

                foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
                {
                    idProduct = fila.Cells["ID EN TIENDA"].Value.ToString();
                    if (!string.IsNullOrEmpty(idProduct))
                    {
                        psWS.actualizarInfoProduct(idProduct);
                    }
                }
                "Proceso finalizado".mb();
            }
            catch
            { }


        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            subirReferenciasAPS();
        }
        private void subirReferenciasAPS()
        {
            try
            {


                MessageBox.Show("Esta operación sincroniza las referencias e IDs");
                string id_productA3= "";
                string referenceA3 = "";

                string id_productPS = "";
                string referencePS = "";


                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mysql = new csMySqlConnect();

                //TODO: TENGO QUE COGER LOS IDS PARA COMPARAR DE PRESTASHIOP PUESTO QUE SI LOS COJO DE EL DGV SIEMPRE VAN A SER LOS DE A3
                //filtar tanto por id como por referencia

                List<string> consultasPS = new List<string>();
                List<string> consultasA3 = new List<string>();

                DataTable productosPS = mysql.cargarTabla("select id_product,reference from ps_product");


                sql.abrirConexion();
                mysql.OpenConnection();




               
                    if (dgvProductosA3.SelectedRows.Count > 0)
                    {

                        foreach (DataGridViewRow dr in dgvProductosA3.SelectedRows)
                        {
                            id_productA3 = dr.Cells["ID EN TIENDA"].Value.ToString();
                            referenceA3 = dr.Cells["CODIGO"].Value.ToString();

                            foreach (DataRow row in productosPS.Rows)
                            {
                                id_productPS = row["id_product"].ToString();
                                referencePS = row["reference"].ToString();

                                //Si coinciden los IDs y las referencias son distintas actualizamos la referencia de A3 en prestashop
                                if ((id_productA3 == id_productPS ) && referenceA3 != row["reference"].ToString())
                                {

                                    consultasPS.Add("update ps_product set reference = '" + referenceA3 + "' where id_product = " + id_productA3);

                                    //mysql.ejecutarConsulta("update ps_product set reference = '" + referenceA3 + "' where id_product = " + id_productA3, true);
                                    break;

                                }
                                //En caso de q los IDs no sean iguales comprobamos que las referencias si lo sean y si es asi actualziamos en A3 el kls_id_shop
                                else if ((referenceA3 ==referencePS )&&(id_productA3 != id_productPS))
                                {
                                    consultasA3.Add("update dbo.ARTICULO set kls_id_shop=" + row["id_product"].ToString() + " where ltrim(codart) ='" + referenceA3 + "'");

                                   //sql.actualizarCampo("update dbo.ARTICULO set kls_id_shop=" + row["id_product"].ToString() + " where ltrim(codart) ='" + referenceA3 + "'", false); ;
                                    break;
                                }

                            }
                        }

                    if (consultasPS.Count > 0)
                    {
                        if (MessageBox.Show("Se van a actualizar: " + consultasPS.Count + " referencia/s ¿Quieres continuar?", "Operación arriesgada", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            foreach (string consulta in consultasPS)
                            {
                                mysql.ejecutarConsulta(consulta, true);
                            }
                            MessageBox.Show("Actualización finalizada");
                        }
                        else
                        {
                            MessageBox.Show("Operación cancelada");
                        }
                    }
                    if (consultasA3.Count > 0)
                    {
                        if (MessageBox.Show("Se va a actualizar: " + consultasA3.Count +" ID/s ¿Quieres continuar?", "Operación arriesgada", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            foreach (String consulta in consultasA3)
                            {
                                sql.actualizarCampo(consulta, false);
                            }
                            MessageBox.Show("Actualización finalizada");
                        }
                        else
                        {
                            MessageBox.Show("Operación cancelada");
                        }
                    }


                        sql.cerrarConexion();
                        mysql.CloseConnection();

                        

                    }
                    else
                    {

                        MessageBox.Show("Debe seleccionar minimo una fila");

                    }
            
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString());
                Program.guardarErrorFichero("SubirReferenciasAPS error: " + ex.ToString() + ex.StackTrace);
            }
        }

        private void subirImagenesAPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csImagenesPS imagenesPS = new csImagenesPS();
            imagenesPS.subirImagenes();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cboxDisponible_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tbFilterDesc_TextChanged(object sender, EventArgs e)
        {

        }

        private void cBoxEnTienda_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cboxColecciones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboxMarcas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblColeccion_Click(object sender, EventArgs e)
        {

        }

        private void lblMarca_Click(object sender, EventArgs e)
        {

        }
        private void actualizarImpuestosEnPS()
        {
            string consulta, consultaDos = "";
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            DataTable iva = mysql.cargarTabla("select * from kls_tax_combination");

            DataTable articulos = sql.cargarDatosTablaA3(" select LTRIM(articulo.CODART) CODART,TIPOIVA.TIPIVA AS TIPIVA, KLS_ID_SHOP from articulo       " +
                                                       " inner join TIPOIVA on TIPOIVA.TIPIVA = articulo.TIPIVA     " +
                                                       " where ARTICULO.KLS_ID_SHOP > 0 and KLS_ARTICULO_PADRE IS NULL ");

            articulos.Columns.Add("PS_TAX");
            foreach (DataRow rowArt in articulos.Rows)
            {
                foreach (DataRow rowIva in iva.Rows)
                {
                    if (rowArt["TIPIVA"].Equals(rowIva["id_tax_a3erp"]))
                    {
                        rowArt["PS_TAX"] = rowIva["id_tax_ps_group"];
                    }
                }
            }
            mysql.OpenConnection();
            try
            {
                foreach (DataRow row in articulos.Rows)
                {
                    consulta = "update ps_product set id_tax_rules_group=" + row["PS_TAX"] + " where id_product=" + row["KLS_ID_SHOP"];
                    consultaDos = "update ps_product_shop set id_tax_rules_group=" + row["PS_TAX"] + " where id_product=" + row["KLS_ID_SHOP"];
                    mysql.ejecutarConsulta(consulta, true);
                    mysql.ejecutarConsulta(consultaDos, true);

                }
                MessageBox.Show("Impuestos actualizados");
            }
            catch (Exception ex)
            {
                //ex.StackTrace;
            }
            mysql.CloseConnection();

        }
        private void actualizarImpuestosEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarImpuestosEnPS();

        }

        private void actualizarFechaDeDisponibilidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csStocks stock = new csStocks();
            stock.updateFechaProximaRecepcion();
        }

        private void actualizarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

            string codart = "";
            string idproduct = "";
            csStocks stock = new klsync.csStocks();

            if (csGlobal.activarMonotallas == "Y")
            {
                csTallasYColoresV2 tyc = new csTallasYColoresV2();
                tyc.marcarArticulosMonoatributoEnA3ERP("");
            }

            foreach (DataGridViewRow fila in dgvProductosA3.SelectedRows)
            {
                try
                {
                    codart = fila.Cells["CODIGO"].Value.ToString();
                    idproduct = fila.Cells["ID EN TIENDA"].Value.ToString();
                    stock.stock(idproduct, codart);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("NO SE HA PODIDO ACTUALIZAR EL STOCK");
                    Program.guardarErrorFichero("Error actualizando stock:" + Environment.NewLine + " Información: " + ex.StackTrace);
                }

            }
            MessageBox.Show("Stock actualizado");
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void validarPresenciaEnTiendaConReferenciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable articulos = new DataTable();
            articulos.Columns.Add("idProduct");
            int numfila = 0;
            int total = dgvProductosA3.SelectedRows.Count;
            string klsIdShop = "";
            string codart = "";
            string idproduct = "";

            foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
            {
                klsIdShop = item.Cells["ID EN TIENDA"].Value.ToString();
                codart = item.Cells["CODIGO"].Value.ToString();
                //DataTable productos_a3 = sql.cargarDatosTablaA3("select ltrim(codart) as codart from articulo WHERE LTRIM(CODART)='" + codart + "'");
                idproduct = mysql.obtenerDatoFromQuery("select id_product from ps_product where reference='" + codart + "'");
                if (idproduct != "")
                {
                    csUtilidades.ejecutarConsulta("UPDATE ARTICULO SET KLS_ID_SHOP="+ idproduct + "WHERE LTRIM(CODART)='" + codart + "'", false);
                    numfila++;
                }
            }
            MessageBox.Show("Se han actualizado " + numfila + " referencias de " + total + " seleccionadas.");
        }

        private void pesoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string codigo = "";
            string peso = "";
            try
            {
                if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI" || csGlobal.modoTallasYColores.ToUpper() == "A3NOPSNO")
                {
                    if (dgvProductosA3.Rows.Count > 0)
                    {
                        if (dgvProductosA3.SelectedRows.Count > 0)
                        {
                            string numSelectedRows = dgvProductosA3.SelectedRows.Count.ToString(); 
                            if (MessageBox.Show("Vas a actualizar el peso de " + numSelectedRows + " articulo/s, estás seguro/a?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            {
                          
                                foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                                {
                                    codigo = item.Cells["CODIGO"].Value.ToString();
                                    peso = sql.obtenerCampoTabla("SELECT PESO FROM ARTICULO WHERE LTRIM(CODART) = '" + codigo + "'");
                                    peso = peso.Replace(",", ".");
                                    csUtilidades.ejecutarConsulta("update ps_product set weight="+peso+" where reference='"+ codigo + "'", true);
                                }

                                "Pesos actualizados".mb();
                            }
                            else
                            {
                                "Operacion cancelada".mb();
                            }
                       
                        }
                    }
                }
                else
                {
                    "Tallas y colores activado. ".mb();
                }
            }
            catch (Exception ex)
            {
                "Ha ocurrido un error al actualizar los pesos".mb();
            }
        }

        private void cargarProductosPorImagenesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.where_in = csUtilidades.selectFilesFromFolder();
            tipoCargaArticulos = "Todos";
            cargarArticulos();
        }

        private void actualizarMedidasEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            string codigo = "";
            string ancho = "";
            string fondo = "";
            string alto = "";
            try
            {
                
                if (dgvProductosA3.Rows.Count > 0)
                {
                    if (dgvProductosA3.SelectedRows.Count > 0)
                    {
                        string numSelectedRows = dgvProductosA3.SelectedRows.Count.ToString();
                        if (MessageBox.Show("Vas a actualizar las medidas de " + numSelectedRows + " articulo/s, estás seguro/a?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {

                            foreach (DataGridViewRow item in dgvProductosA3.SelectedRows)
                            {
                                codigo = item.Cells["CODIGO"].Value.ToString();
                                ancho = sql.obtenerCampoTabla("SELECT PARAM1 FROM ARTICULO WHERE LTRIM(CODART) = '" + codigo + "'");
                                fondo = sql.obtenerCampoTabla("SELECT PARAM2 FROM ARTICULO WHERE LTRIM(CODART) = '" + codigo + "'");
                                alto = sql.obtenerCampoTabla("SELECT PARAM3 FROM ARTICULO WHERE LTRIM(CODART) = '" + codigo + "'");
                                ancho = ancho.Replace(",", ".");
                                fondo = fondo.Replace(",", ".");
                                alto = alto.Replace(",", ".");
                                ancho = !string.IsNullOrEmpty(ancho) ? ancho : "0";
                                fondo = !string.IsNullOrEmpty(fondo) ? fondo : "0";
                                alto =  !string.IsNullOrEmpty(alto) ? alto : "0";

                                csUtilidades.ejecutarConsulta("update ps_product set width='" + ancho + "', depth='" + fondo + "', height='" + alto + "' where reference='" + codigo + "'", true);
                            }

                            "Medidas actualizadas".mb();
                        }
                        else
                        {
                            "Operacion cancelada".mb();
                        }

                    }
                    
                }
            }
            catch (Exception ex)
            {
                "Ha ocurrido un error al actualizar las medidas".mb();
            }
        }

        private void sincronizarActivoPSVisibleEnTiendaA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mysql = new csMySqlConnect();
                DataTable productoPs;
                string codart, visibleTienda;

                int i = 0;

                productoPs = mysql.cargarTabla("SELECT reference, active from ps_product");


                foreach (DataRow product in productoPs.Rows)
                {
                    codart = sql.obtenerCampoTabla("SELECT CODART FROM ARTICULO WHERE LTRIM(CODART)='" + product["reference"] + "'", true);
                    if (codart != "")
                    {
                        visibleTienda = product["active"].ToString() == "1" ? "T" : "F";
                        sql.actualizarCampo("UPDATE ARTICULO SET KLS_ARTICULO_TIENDA='" + visibleTienda + "' WHERE LTRIM(CODART)='" + product["reference"] + "'", true);
                        i++;
                    }
                    
                }

                MessageBox.Show("Se han sincronizado " + i + " articulos.");
            }
        }
    }
}

