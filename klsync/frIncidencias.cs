﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync
{
    public partial class frIncidencias : Form
    {
        private csMySqlConnect mysql = new csMySqlConnect();
        private csSqlConnects sql = new csSqlConnects();

        private static frIncidencias m_FormDefInstance;
        public static frIncidencias DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frIncidencias();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frIncidencias()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Codigo A3, Visible en tienda S/N, KLS_ID_SHOP, TIPO INCIDENCIA, SOLUCIONES
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateA3_Click(object sender, EventArgs e)
        {
            kls_id_shop = 1;
            cargarIncidenciasA3();
        }

        private int kls_id_shop = 0;

        private void cargarIncidenciasA3()
        {
            dgvDetalleA3.Visible = false;
            contextMenuStripA3.Enabled = true;
            string consultaA3 = "SELECT LTRIM(CODART) AS CODART_A3, '' AS CODART_PS, DESCART AS DESCART_A3, '' AS DESCART_PS, KLS_ARTICULO_TIENDA AS KLS_ARTICULO_TIENDA_A3, '' AS KLS_ARTICULO_TIENDA_PS, KLS_ID_SHOP AS KLS_ID_SHOP_A3, '' AS KLS_ID_SHOP_PS, '' AS TIPOINCIDENCIA, '' as SOLUCIONES FROM ARTICULO";
            string consultaPS = "select reference as CODART_PS, name as DESCART_PS, IF(active = 1, 'T', 'F') as KLS_ARTICULO_TIENDA_PS, ps_product.id_product as KLS_ID_SHOP_PS from ps_product left join ps_product_lang on ps_product_lang.id_product = ps_product.id_product";

            DataTable dt_a3 = sql.cargarDatosTablaA3(consultaA3);
            DataTable dt_ps = mysql.cargarTabla(consultaPS);

            foreach (DataRow a3 in dt_a3.Rows)
            {
                string codart_a3 = a3["CODART_A3"].ToString();
                string visible_a3 = a3["KLS_ARTICULO_TIENDA_A3"].ToString();
                string idshop_a3 = a3["KLS_ID_SHOP_A3"].ToString();
                string descart_a3 = a3["DESCART_A3"].ToString();

                foreach (DataRow ps in dt_ps.Rows)
                {
                    string codart_ps = ps["CODART_PS"].ToString();
                    string visible_ps = ps["KLS_ARTICULO_TIENDA_PS"].ToString();
                    string idshop_ps = ps["KLS_ID_SHOP_PS"].ToString();
                    string descart_ps = ps["DESCART_PS"].ToString();

                    if (codart_a3 == codart_ps)
                    {
                        a3["KLS_ID_SHOP_PS"] = ps["KLS_ID_SHOP_PS"].ToString();
                        a3["KLS_ARTICULO_TIENDA_PS"] = ps["KLS_ARTICULO_TIENDA_PS"].ToString();
                        a3["CODART_PS"] = ps["CODART_PS"].ToString();
                        a3["DESCART_PS"] = ps["DESCART_PS"].ToString();

                        if (visible_a3 != visible_ps)
                        {
                            a3["TIPOINCIDENCIA"] += "V";
                            a3["SOLUCIONES"] += "Corregir estado\n";
                        }

                        if (descart_a3 != descart_ps)
                        {
                            a3["TIPOINCIDENCIA"] += "D";
                            a3["SOLUCIONES"] += "Actualizar descripcion\n";
                        }

                        if (idshop_a3 != idshop_ps)
                        {
                            a3["TIPOINCIDENCIA"] += "I";
                            a3["SOLUCIONES"] += "Actualizar id\n";
                        }

                        break;
                    }
                }
            }

            DataTable final = csUtilidades.dataRowArray2DataTable(dt_a3.Select("CODART_A3 = CODART_PS AND TIPOINCIDENCIA <> ''"));

            csUtilidades.addDataSource(dgvA3, final);
        }

        private void btnUpdatePS_Click(object sender, EventArgs e)
        {
            string consulta = "SELECT " +
                            "    reference, COUNT(*) as repeticiones " +
                            "FROM " +
                            "    ps_product " +
                            "GROUP BY " +
                            "    reference " +
                            "HAVING  " +
                            "    COUNT(*) > 1 " +
                            "ORDER BY repeticiones DESC";

            csUtilidades.addDataSource(dgvPS, mysql.cargarTabla(consulta));

            if (dgvPS.Rows.Count > 0)
            {
                dgvPS.Rows[0].Selected = false;
            }
            else
            {
                dgvPSReferenciasDetalle.DataSource = null;
            }
        }

        /* Acciones */

        /// <summary>
        /// Copiar el id_product a los productos de A3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void copiarElDePSAA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvA3.Rows.Count > 0)
            {
                if (dgvA3.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Estas a punto de cambiar el codigo de Prestashop en A3 de  "
                        + dgvA3.SelectedRows.Count + " elemento/s. \n\nEstas seguro/a?", "",
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string idshop_ps = item.Cells["KLS_ID_SHOP_PS"].Value.ToString();
                            string codart_a3 = item.Cells["CODART_A3"].Value.ToString();
                            string consulta = "update articulo set kls_id_shop = '" + idshop_ps + "' where ltrim(codart) = '" + codart_a3 + "'";
                            csUtilidades.ejecutarConsulta(consulta, false);
                        }

                        MessageBox.Show("Operacion completada");
                        if (autoactualizar) { cargarIncidenciasA3(); }
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }
            }

        }

        private void activarEnTiendaEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarEnTiendaA3(true);
        }

        private void desactivarEnTiendaEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarEnTiendaA3(false);
        }

        private void activarEnTiendaA3(bool activar)
        {
            if (dgvA3.Rows.Count > 0)
            {
                if (dgvA3.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Estas a punto de activar en tienda (A3) "
                        + dgvA3.SelectedRows.Count + " elemento/s. \n\nEstas seguro/a?", "",
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        char valor;

                        if (activar)
                        {
                            valor = 'T';
                        }
                        else
                        {
                            valor = 'F';
                        }

                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string codart_a3 = item.Cells["CODART_A3"].Value.ToString();
                            string consulta = "update articulo set KLS_ARTICULO_TIENDA = '" + valor + "' where ltrim(codart) = '" + codart_a3 + "'";
                            csUtilidades.ejecutarConsulta(consulta, false);
                        }

                        MessageBox.Show("Operacion completada");
                        if (autoactualizar) { cargarIncidenciasA3(); }
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }
            }
        }

        private void activarEnTiendaEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarEnTiendaPS(true);
            if (autoactualizar) { cargarIncidenciasA3(); }
        }

        private void desactivarEnTiendaEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarEnTiendaPS(false);
            if (autoactualizar) { cargarIncidenciasA3(); }
        }

        private void activarEnTiendaPS(bool activar)
        {
            if (dgvA3.Rows.Count > 0)
            {
                if (dgvA3.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Estas a punto de activar en tienda (PS) "
                        + dgvA3.SelectedRows.Count + " elemento/s. \n\nEstas seguro/a?", "",
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        int valor;

                        if (activar)
                        {
                            valor = 1;
                        }
                        else
                        {
                            valor = 0;
                        }

                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string reference = item.Cells["CODART_A3"].Value.ToString();
                            string consulta = "update ps_product set active = " + valor + " where ltrim(reference) = '" + reference + "'";
                            string consulta_shop = "update ps_product set active = " + valor + " where ltrim(reference) = '" + reference + "'";

                            csUtilidades.ejecutarConsulta(consulta, true);
                            csUtilidades.ejecutarConsulta(consulta_shop, true);
                        }

                        MessageBox.Show("Operacion completada");
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }
            }
        }

        private void copiarDePSAA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvA3.Rows.Count > 0)
            {
                if (dgvA3.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Estas a punto de cambiar la descripcion (DESCART) en A3 de  "
                        + dgvA3.SelectedRows.Count + " elemento/s. \n\nEstas seguro/a?", "",
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string descart_ps = item.Cells["DESCART_PS"].Value.ToString();
                            string codart_a3 = item.Cells["CODART_A3"].Value.ToString();
                            string consulta = "update articulo set descart = '" + descart_ps + "' where ltrim(codart) = '" + codart_a3 + "'";
                            csUtilidades.ejecutarConsulta(consulta, false);
                        }

                        MessageBox.Show("Operacion completada");
                        if (autoactualizar) { cargarIncidenciasA3(); }
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }
            }
        }

        private void copiarDeA3APSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvA3.Rows.Count > 0)
            {
                if (dgvA3.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Estas a punto de cambiar el nombre del producto para el español en PS de  "
                        + dgvA3.SelectedRows.Count + " elemento/s. \n\nEstas seguro/a?", "",
                        MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (DataGridViewRow item in dgvA3.SelectedRows)
                        {
                            string descart_a3 = item.Cells["DESCART_A3"].Value.ToString();
                            string idshop_ps = item.Cells["KLS_ID_SHOP_PS"].Value.ToString();
                            string idioma = sql.obtenerCampoTabla("SELECT IDIOMAPS FROM KLS_ESYNC_IDIOMAS WHERE IDIOMAA3 = 'CAS'");
                            string consulta = "update ps_product_lang set name = '" + descart_a3 + "' where id_product = " + idshop_ps + " and id_lang = " + idioma;
                            csUtilidades.ejecutarConsulta(consulta, true);
                        }

                        MessageBox.Show("Operacion completada");
                        if (autoactualizar) { cargarIncidenciasA3(); }
                    }
                    else
                    {
                        MessageBox.Show("Operacion cancelada");
                    }
                }
            }
        }

        private void dgvPS_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string selected_reference = dgvPS.SelectedRows[0].Cells["reference"].Value.ToString();

            string consulta = "select id_product, ps_manufacturer.name as marca, reference, cast(price AS DECIMAL(10,2)) as price, ps_product.active from ps_product left join ps_manufacturer on ps_manufacturer.id_manufacturer = ps_product.id_manufacturer where reference = '" + selected_reference + "'";

            csUtilidades.addDataSource(dgvPSReferenciasDetalle, mysql.cargarTabla(consulta));
        }

        private void eliminarProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvPSReferenciasDetalle.SelectedRows.Count > 0)
            {
                try
                {
                    string mensaje = "Estas a punto de borrar " + dgvPSReferenciasDetalle.SelectedRows.Count.ToString() + ", estás seguro/a??";
                    if (MessageBox.Show(mensaje, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {

                        foreach (DataGridViewRow item in dgvPSReferenciasDetalle.SelectedRows)
                        {
                            string sURL = csGlobal.APIUrl + "products/" + item.Cells["id_product"].Value.ToString() + "?ws_key=" + csGlobal.webServiceKey;

                            WebRequest request = WebRequest.Create(sURL);
                            request.Method = "DELETE";

                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        }

                        MessageBox.Show(dgvPSReferenciasDetalle.SelectedRows.Count.ToString() + " artículo/s borrado/s");

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        bool autoactualizar = true;
        private void autoactualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            autoactualizarToolStripMenuItem.Checked = !autoactualizarToolStripMenuItem.Checked;
            autoactualizar = autoactualizarToolStripMenuItem.Checked;
        }

        private void tsbtnIDRepetidosA3_Click(object sender, EventArgs e)
        {
            kls_id_shop = 2;
            dgvDetalleA3.DataSource = null;
            dgvDetalleA3.Visible = true;
            contextMenuStripA3.Enabled = false;
            string consulta = "SELECT " +
                    "    kls_id_shop, COUNT(*) as repeticiones " +
                    "FROM " +
                    "    articulo " +
                    "	WHERE KLS_ID_SHOP IS NOT NULL " +
                    "GROUP BY " +
                    "    kls_id_shop " +
                    "HAVING  " +
                    "    COUNT(*) > 1 " +
                    "order by repeticiones desc ";

            csUtilidades.addDataSource(dgvA3, sql.cargarDatosTablaA3(consulta));

        }

        private void dgvA3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (kls_id_shop == 2)
            {
                string selected_reference = dgvA3.SelectedRows[0].Cells["kls_id_shop"].Value.ToString();
                //string selected_reference = dgvA3.SelectedRows[0].Cells["kls_id_shop_ps"].Value.ToString();

                string consulta = "select ltrim(codart) as codart, descart, prcventa, kls_id_shop from articulo where kls_id_shop = '" + selected_reference + "'";

                csUtilidades.addDataSource(dgvDetalleA3, sql.cargarDatosTablaA3(consulta));
            }
            else
            {

            }
        }

        private void ponerANULLLosProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvDetalleA3.SelectedRows.Count > 0)
            {
                try
                {
                    string mensaje = "Estas a punto de poner a null " + dgvDetalleA3.SelectedRows.Count.ToString() + ", estás seguro/a??";
                    if (MessageBox.Show(mensaje, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {


                        foreach (DataGridViewRow item in dgvDetalleA3.SelectedRows)
                        {
                            string consulta = "update articulo set kls_id_shop = null where ltrim(codart) = '" + item.Cells["codart"].Value.ToString() + "'";
                            csUtilidades.ejecutarConsulta(consulta, false);
                        }

                        MessageBox.Show(dgvDetalleA3.SelectedRows.Count.ToString() + " artículo/s puesto/s a null");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }
    }
}
