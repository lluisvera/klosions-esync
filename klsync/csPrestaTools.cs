﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace klsync
{
    class csPrestaTools
    {

        public void crearArticulo(string producto, string precio, string codart, string categoria)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            int id = mySql.selectID("ps_product", "id_product") + 1;
            string link = producto.Replace(" ", "-");
            string script = "INSERT INTO " + csGlobal.databasePS + ".ps_product (id_product, id_category_default, id_shop_default, id_tax_rules_group, price, wholesale_price, reference, out_of_stock, active, redirect_type, show_price, indexed) VALUES ('" + id + "', '" + categoria + "', '1', '1', '" + precio + "', '" + precio + "', '" + codart + "', '7', '1', '404', '1', '1');";
            mySql.insertItems(script);
            //script =
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_category_product (id_category, id_product, position) VALUES ('" + categoria + "', '" + id + "', '');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_shop (id_product, id_shop, id_category_default, id_tax_rules_group, on_sale, online_only, ecotax, minimal_quantity, price, wholesale_price, unit_price_ratio, additional_shipping_cost, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order) VALUES ('" + id + "', '1', '" + categoria + "', '1', '0', '0', '0', '1', '" + precio + "', '" + precio + "', '0', '0', '0', '0', '0', '1', '404', '0', '1');";
            mySql.insertItems(script);


            //añado los tags
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_tag (id_lang, name) VALUES ('1', '" + producto + "');select last_insert_id()";
            int idCreado = mySql.ObtenerIdInsertado(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_tag (id_product, id_tag) VALUES ('" + id + "', '" + idCreado + "');";
            mySql.insertItems(script);

            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '1', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '2', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '3', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '4', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '5', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite,meta_description,meta_keywords,meta_title,available_now,available_later, name) VALUES ('" + id + "', '1', '6', '" + producto + "', '" + producto + "', '" + link + "','','','','','','" + producto + "');";
            mySql.insertItems(script);

            //Creo la línea para añadir el stock Disponible
            int idStock = mySql.selectID("ps_stock_available", "id_stock_available") + 1;
            script = "INSERT INTO " + csGlobal.databasePS + ".ps_stock_available (id_stock_available, id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock) VALUES ('" + idStock + "', '" + id + "', '0', '1', '0', '0', '0', '2');";
            mySql.insertItems(script);

        }


        public void actualizarPrecios(string idProduct, string precio)
        {
            //wholesale_price es el precio de mayorista o coste
            //Se pasan los precios sin IVA (el programa calcula el iva)
            csMySqlConnect mySql = new csMySqlConnect();
            string script = "UPDATE " + csGlobal.databasePS + ".ps_product SET price=" + precio + ", wholesale_price=" + precio + " where id_product=" + idProduct + ";";
            mySql.insertItems(script);
            //script =
            script = "UPDATE " + csGlobal.databasePS + ".ps_product_shop SET price=" + precio + ", wholesale_price=" + precio + " where id_product=" + idProduct + ";";
            mySql.insertItems(script);

        }







        public Image CambiarTamanoImagen(Image pImagen, int pAncho, int pAlto)
        {
            // si original == rectangular
            // y quiero cuadrada
            //if ((pImagen.Height != pImagen.Width) && (pAncho == pAlto))
            //{
            //    //pImagen = PadImage(pImagen);
            //    pImagen = FixedSize(pImagen, pAncho, pAlto);
            //}
            //if ((pImagen.Height != pImagen.Width) && (pAncho != pAlto))
            //{
            //    pImagen = FixedSize(pImagen, pAncho, pAlto);
            //}
            //if ((pImagen.Height == pImagen.Width) && (pAncho != pAlto))
            //{
            //    pImagen = FixedSize(pImagen, pAncho, pAlto);
            //}

            pImagen = FixedSize(pImagen, pAncho, pAlto);

            //creamos un bitmap con el nuevo tamaño
            Bitmap vBitmap = new Bitmap(pAncho, pAlto);
            //creamos un graphics tomando como base el nuevo Bitmap
            using (Graphics vGraphics = Graphics.FromImage((Image)vBitmap))
            {
                //especificamos el tipo de transformación, se escoge esta para no perder calidad.
                vGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //Se dibuja la nueva imagen
                vGraphics.DrawImage(pImagen, 0, 0, pAncho, pAlto);
            }
            //retornamos la nueva imagen
            return (Image)vBitmap;
        }


        //Función para ajustar las imágenes y hacerlas cuadradas con bordes en blanco
        public static Image PadImage(Image originalImage)
        {

            int largestDimension = Math.Max(originalImage.Height, originalImage.Width);
            Size squareSize = new Size(largestDimension, largestDimension);
            Bitmap squareImage = new Bitmap(squareSize.Width, squareSize.Height);
            using (Graphics graphics = Graphics.FromImage(squareImage))
            {
                graphics.FillRectangle(Brushes.White, 0, 0, squareSize.Width, squareSize.Height);
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                graphics.DrawImage(originalImage, (squareSize.Width / 2) - (originalImage.Width / 2), (squareSize.Height / 2) - (originalImage.Height / 2), originalImage.Width, originalImage.Height);
               
            }
            return squareImage;
        }

        public static Image padImageRectangle(Image originalImage, int pAncho, int pAlto)
        {
            int x = 0;
            double xx = 0;
            int y = 0;
            double nuevaAnchura = (double)(originalImage.Width)*((double)((double)pAncho / (double)originalImage.Width));
            double nuevaAltura = (double)(originalImage.Height) * ((double)((double)pAlto / (double)originalImage.Height));
            int AnchuraDefinitiva = Convert.ToInt32(Math.Round(nuevaAnchura, 0));
            int AlturaDefinitiva = Convert.ToInt32(Math.Round(nuevaAltura, 0));
            if (nuevaAnchura > nuevaAltura)
            {
                AnchuraDefinitiva = Convert.ToInt32(Math.Round(nuevaAltura, 0));
                xx = (double)((pAncho - pAlto) / (double)2);
                x = Math.Abs( Convert.ToInt32(xx));
            }
            
            
            int largestDimension = Math.Max(originalImage.Height, originalImage.Width);
            Size squareSize = new Size(largestDimension, largestDimension);
            Bitmap squareImage = new Bitmap(pAncho, pAlto);
            using (Graphics graphics = Graphics.FromImage(squareImage))
            {
                y = Math.Abs(pAncho - pAlto);
                graphics.FillRectangle(Brushes.White, 0, 0, pAncho, pAlto);
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                //graphics.DrawImage(originalImage, (squareSize.Width / 2) - (originalImage.Width / 2), (squareSize.Height / 2) - (originalImage.Height / 2), originalImage.Width * (pAncho / originalImage.Width), originalImage.Height * (pAlto / originalImage.Height));
                //graphics.DrawImage(originalImage, (squareSize.Width / 2) - (originalImage.Width / 2), (squareSize.Height / 2) - (originalImage.Height / 2),nuevaDimension,nuevaDimension);
                graphics.DrawImage(originalImage, x, y, AnchuraDefinitiva, AlturaDefinitiva);
            }
            return squareImage;
        
        
        }

        // Funciona! -- se puede cambiar el color del pad
        // url: http://stackoverflow.com/a/2001462/5074597
        static Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height,
                              PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White); // aqui se cambia el color
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }
}
