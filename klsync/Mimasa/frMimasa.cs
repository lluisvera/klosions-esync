﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace klsync.Mimasa
{
    public partial class frMimasa : Form
    {

        private static frMimasa m_FormDefInstance;
        public static frMimasa DefInstance
        {

            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frMimasa();
                return m_FormDefInstance;
            }
            set { 
                            m_FormDefInstance = value;
            }
        }

        Mimasa.csMimasa mimasa = new Mimasa.csMimasa();
        Mimasa.csScriptsMimasa scriptMimasa = new csScriptsMimasa();
        public frMimasa()
        {
            InitializeComponent();

        }



        private void cargarMovimientos(string codart, string lote)
        {
          
            csUtilidades.cargarDGV(dgvMovStock, scriptMimasa.selectMovStockArtLote(codart, lote, filtroTipoMovs()), false);
        }

        private string filtroTipoMovs()
        {
            //Añado un valor XX para así añadir la coma a todos los valores que añadamos
            string filtroTipoMovs = " AND TIPDOC IN ('XX'";

            filtroTipoMovs = cboxAC.Checked ? filtroTipoMovs + ",'AC'" : filtroTipoMovs;
            filtroTipoMovs = cboxFC.Checked ? filtroTipoMovs + ",'FC'" : filtroTipoMovs;
            filtroTipoMovs = cboxAV.Checked ? filtroTipoMovs + ",'AV'" : filtroTipoMovs;
            filtroTipoMovs = cboxFV.Checked ? filtroTipoMovs + ",'FV'" : filtroTipoMovs;
            filtroTipoMovs = cboxRE.Checked ? filtroTipoMovs + ",'RE'" : filtroTipoMovs;
            filtroTipoMovs = cboxTR.Checked ? filtroTipoMovs + ",'TR'" : filtroTipoMovs;
            filtroTipoMovs = cboxPR.Checked ? filtroTipoMovs + ",'PR'" : filtroTipoMovs;
            filtroTipoMovs = cboxIN.Checked ? filtroTipoMovs + ",'IN'" : filtroTipoMovs;
            filtroTipoMovs = filtroTipoMovs + ") ";

            return filtroTipoMovs;
        }

        private void dgvOrdFab_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string codart = dgvOrdFab.CurrentRow.Cells["CODART"].Value.ToString().Trim();
            string lote = dgvOrdFab.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
            cargarMovimientos(codart, lote);
        }

        private void dgvMimasa_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMimasa.CurrentRow.Index >= 0)
            {
                string codart = dgvMimasa.CurrentRow.Cells["CODART"].Value.ToString().Trim();
                string lote = dgvMimasa.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
                cargarMovimientos(codart, lote);
            }
        }

        private void dgvMovStock_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMovStock.CurrentRow.Index >= 0)
            {
                string codart = dgvMovStock.CurrentRow.Cells["CODART"].Value.ToString().Trim();
                string lote = dgvMovStock.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
                string tipoDoc = dgvMovStock.CurrentRow.Cells["TIPDOC"].Value.ToString();
                string idDoc = dgvMovStock.CurrentRow.Cells["IDENTIFICADOR"].Value.ToString();
                string idLogProduccion = dgvMovStock.CurrentRow.Cells["LOGP"].Value.ToString();

                if (tipoDoc == "AC")
                {
                    if (dgvOrdFab.Rows.Count == 0)
                    {
                        dgvOrdFab.Height = 25;
                    }

                    csUtilidades.cargarDGV(dgvAlbCompra, scriptMimasa.selectAlbCompras(idDoc, codart, lote), false);
                    csUtilidades.cargarDGV(dgvFactCompra, scriptMimasa.selectFactCompras(codart, lote), false);

                    dgvFactCompra.Refresh();
                    if (dgvFactCompra.Rows.Count > 0)
                    {
                        string idFraCompra = dgvFactCompra.Rows[0].Cells["IDFACC"].Value.ToString();
                        csUtilidades.cargarDGV(dgvFactCompraAsoc, scriptMimasa.selectFacturasAsociadas(idFraCompra), false);
                    }

                }
                if (tipoDoc == "PR")
                {
                    csUtilidades.cargarDGV(dgvOrdFab, scriptMimasa.selectOrdFab(idDoc, codart, idLogProduccion), false);

                }
            }
        }

        private void dgvMimasa_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvMimasa.Rows)
            {
                height += dr.Height;
            }

            dgvMimasa.Height = height;

            lblMovStock.Location = new Point(3, dgvMimasa.Location.Y + dgvMimasa.Height + 30);
            int posicionDgvMovimientos = lblMovStock.Location.Y;
            dgvMovStock.Location = new Point(3, posicionDgvMovimientos + 20);

            lblOrdFab.Location = new Point(3, dgvMovStock.Location.Y + dgvMovStock.Height + 30);
            dgvOrdFab.Location = new Point(3, lblOrdFab.Location.Y + 20);
            ajustarPosicionesDGV();

        }

        private void dgvMovStock_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvMovStock.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvMovStock.Height = height;

            lblOrdFab.Location = new Point(3, dgvMovStock.Location.Y + dgvMovStock.Height + 30);
            dgvOrdFab.Location = new Point(3, lblOrdFab.Location.Y + 20);

            ajustarPosicionesDGV();
        }

        private void dgvOrdFab_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvOrdFab.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvOrdFab.Height = height;

            ajustarPosicionesDGV();
        }

        private void ajustarPosicionDgvAlbCompra()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvAlbCompra.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvAlbCompra.Height = height;

            lblAlbCompra.Location = new Point(3, dgvOrdFab.Location.Y + dgvOrdFab.Height + 30);
            dgvAlbCompra.Location = new Point(3, lblAlbCompra.Location.Y + 20);
        }

        private void ajustarPosicionDgvFactCompra()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvFactCompra.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvFactCompra.Height = height;

            lblFactCompra.Location = new Point(3, dgvAlbCompra.Location.Y + dgvAlbCompra.Height + 30);
            dgvFactCompra.Location = new Point(3, lblFactCompra.Location.Y + 20);
        }

        private void ajustarPosicionDgvFactCompraAsoc()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvFactCompraAsoc.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvFactCompraAsoc.Height = height;

            lblFactCompraAsoc.Location = new Point(3, dgvFactCompra.Location.Y + dgvFactCompra.Height + 30);
            dgvFactCompraAsoc.Location = new Point(3, lblFactCompraAsoc.Location.Y + 20);
        }

        private void ajustarPosicionesDGV()
        {
            ajustarPosicionDgvAlbCompra();
            ajustarPosicionDgvFactCompra();
            ajustarPosicionDgvFactCompraAsoc();
        }

        private void dgvAlbCompra_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvAlbCompra();
            ajustarPosicionesDGV();
        }

        private void dgvFactCompra_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvFactCompra();
            ajustarPosicionesDGV();
        }

        private void dgvFactCompraAsoc_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvFactCompraAsoc();
            ajustarPosicionesDGV();
        }

        private void btnLoadProducts_Click(object sender, EventArgs e)
        {
            dgvAlbCompra.DataSource = null;
            dgvFactCompra.DataSource = null;
            dgvFactCompraAsoc.DataSource = null;

            csUtilidades.cargarDGV(dgvMimasa, scriptMimasa.selectArticulosLote(rbCargaBasica.Checked, cboxArticulos.SelectedValue.ToString(), rbStockDif0.Checked, false), false);

        }

        private void cargarArticulos()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS CODART,'' AS DESCART " +
                                                        " UNION " +
                                                        " SELECT LTRIM(CODART), LTRIM(CODART) + '-' + DESCART FROM ARTICULO " +
                                                        " ORDER BY CODART ", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxArticulos.ValueMember = "CODART";
                cboxArticulos.DisplayMember = "DESCART";
                cboxArticulos.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarProveedores()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS CODPRO,'' AS NOMPRO " +
                                                        " UNION " +
                                                        " SELECT LTRIM(CODPRO), LTRIM(CODPRO) + '-' + NOMPRO FROM PROVEED ", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxProveedores.ValueMember = "CODPRO";
                cboxProveedores.DisplayMember = "NOMPRO";
                cboxProveedores.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void cargarFacturas(string idProv)
        {
            try
            {
                string script = "SELECT " +
                    " CAST(IDFACC as int) as IDFACC, CAST(CAST(NUMDOC AS int) AS varchar) + ' - ' + NOMPRO + ' - ' + convert(VARCHAR(10),FECHA,103) + ' - ' + REFERENCIA AS FACTURA " +
                    " FROM CABEFACC " +
                    " WHERE LTRIM(CODPRO)='" + idProv + "' ORDER BY FECHA DESC";
                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter(script, dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxDocs.ValueMember = "IDFACC";
                cboxDocs.DisplayMember = "FACTURA";
                cboxDocs.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void frMimasa_Load(object sender, EventArgs e)
        {
            cargarArticulos();
            cargarProveedores();
        }

        private void cboxArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvMimasa.DataSource = null;
            dgvMovStock.DataSource = null;
            dgvOrdFab.DataSource = null;
            dgvAlbCompra.DataSource = null;
            dgvFactCompra.DataSource = null;
            dgvFactCompraAsoc.DataSource = null;
            //csUtilidades.cargarDGV(dgvMimasa, scriptMimasa.selectArticulosLote(rbCargaBasica.Checked, cboxArticulos.SelectedValue.ToString(), rbStockDif0.Checked, false), false);
        }

        private void btnLoadDocs_Click(object sender, EventArgs e)
        {
            btnSaveCosteAdicional.Enabled= false;
            string doc = cboxDocs.SelectedValue.ToString();
            csUtilidades.cargarDGV(dgvDocsCompraMP, scriptMimasa.selectFactComprasMP(doc), false);
            csUtilidades.cargarDGV(dgvDocsCompraAsoc, scriptMimasa.selectFactComprasMP(null, doc), false);
            csUtilidades.cargarDGV(dgvImputacion, scriptMimasa.selectImportesImputar(doc), false);
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            btnSaveCosteAdicional.Enabled = true;
            double totalDocumento = 0;
            double importeUnitario = 0;
            double importeUnitarioPonderado = 0;
            double unidadesCalculo = 0;
            double importeDocsAImputar = 0;
            double importeMismaFraImputar = 0;
            double importeArticuloImputar = 0;
            double importeArticuloImputarPonderado = 0;
            double pesoReferenciaArticulo = 0;
            string textBox = "";
            double costeAdicional = 0;
            string codartMP = "";
            string codartIMP = "";      //Articulo tabla imputación
            string loteReferencia = "";
            string descuentoLinea = "";
            string prcMedioActualizado = "";
            int numLineasPromos = 0;

            double cantidadLineasPromos = 0;
            double cantidadLineasSinPromos = 0;
            double precioRefLineasPromos = 0;
            double prcmedioActualizadoPromo = 0;
            double unidadesLinea = 0;
            int descuentoLineaPromo = 0;

            //Calculo las unidades a imputar
            for (int i = 0; i < dgvDocsCompraMP.Rows.Count; i++)
            {
                if (!string.IsNullOrEmpty(dgvDocsCompraMP.Rows[i].Cells["LOTE"].Value.ToString()) && dgvDocsCompraMP.Rows[i].Cells["ETIQ"].Value.ToString() != "SI" && dgvDocsCompraMP.Rows[i].Cells["CODART"].Value.ToString().Trim() != "NI")
                {
                    totalDocumento = totalDocumento + Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["COEF_REPARTO"].Value.ToString());
                }
            }
            totalDocumento = Math.Round(totalDocumento, 4);

            //Calculo Importes Facturas Imputadas
            for (int i = 0; i < dgvImputacion.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(dgvImputacion.Rows[i].Cells["CODART"].Value.ToString()))
                {
                    importeDocsAImputar = importeDocsAImputar + Convert.ToDouble(dgvImputacion.Rows[i].Cells["IMPORTE"].Value.ToString());
                }
            }

            //Calculo Importes Misma Factura
            for (int ii = 0; ii < dgvDocsCompraMP.Rows.Count; ii++)
            {
                //Cojo todos los importes que no tienen lotes (transportes, aranceles, etc.)
                if (string.IsNullOrEmpty(dgvDocsCompraMP.Rows[ii].Cells["LOTE"].Value.ToString()))
                {
                    importeMismaFraImputar = importeMismaFraImputar + Convert.ToDouble(dgvDocsCompraMP.Rows[ii].Cells["BASE"].Value.ToString());
                }
            }


            importeUnitario = importeDocsAImputar / totalDocumento;
            importeUnitario = Math.Round(importeUnitario, 2);

            textBox = "REPARTO ENTRE " + totalDocumento.ToString() + "\r\n" +
                "IMPORTE UNITARIO POR Udad: " + importeUnitario.ToString() + "\n";

            tbResumenAcciones.Text = textBox;

            //Reparto entre las lineas

            for (int i = 0; i < dgvDocsCompraMP.Rows.Count; i++)
            {

                codartMP = dgvDocsCompraMP.Rows[i].Cells["CODART"].Value.ToString();
                if (codartMP == "NI") continue;
                unidadesCalculo = Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["UNIDADES"].Value.ToString());
                descuentoLinea = dgvDocsCompraMP.Rows[i].Cells["DTO"].Value.ToString().Replace(",0000", "");
                loteReferencia = dgvDocsCompraMP.Rows[i].Cells["LOTE"].Value.ToString();

                //MIRO SI HAY LINEAS CON DESCUENTO
                for (int fila = 0; fila < dgvDocsCompraMP.Rows.Count; fila++)
                {
                    //Entraran todas las lineas que tengan el mismo código y lote independientemente de si es descuento o no
                    //Eliminamos la condicion de actualziar por lote dejando unicamente por artículo.
                    if (dgvDocsCompraMP.Rows[fila].Cells["CODART"].Value.ToString() == codartMP /*&& dgvDocsCompraMP.Rows[fila].Cells["LOTE"].Value.ToString() == loteReferencia*/ && dgvDocsCompraMP.Rows[i].Cells["CODART"].Value.ToString().Trim() != "NI")
                    {
                        descuentoLineaPromo = Convert.ToInt32(dgvDocsCompraMP.Rows[fila].Cells["DTO"].Value.ToString().Replace(",0000", ""));
                        unidadesLinea = Convert.ToDouble(dgvDocsCompraMP.Rows[fila].Cells["UNIDADES"].Value.ToString());
                        numLineasPromos++;
                        cantidadLineasPromos = cantidadLineasPromos + unidadesLinea;
                        cantidadLineasSinPromos = descuentoLineaPromo != 100 ? cantidadLineasSinPromos + unidadesLinea : cantidadLineasSinPromos;
                        precioRefLineasPromos = Convert.ToDouble(dgvDocsCompraMP.Rows[fila].Cells["PRECIO"].Value.ToString()) > precioRefLineasPromos ? Convert.ToDouble(dgvDocsCompraMP.Rows[fila].Cells["PRECIO"].Value.ToString()) : precioRefLineasPromos;
                    }
                }

                //Si hay lineas con promos, más de 1 linea, actualizo el precio medio
                if (numLineasPromos > 1)
                {
                    prcmedioActualizadoPromo = (cantidadLineasSinPromos * precioRefLineasPromos) / cantidadLineasPromos;
                    prcmedioActualizadoPromo = Math.Round(prcmedioActualizadoPromo, 4);
                    for (int iii = 0; iii < dgvDocsCompraMP.Rows.Count; iii++)
                    {
                        if (dgvDocsCompraMP.Rows[iii].Cells["CODART"].Value.ToString() == codartMP && dgvDocsCompraMP.Rows[iii].Cells["LOTE"].Value.ToString() == loteReferencia)
                        {
                            dgvDocsCompraMP.Rows[iii].Cells["PRCMEDIO"].Value = prcmedioActualizadoPromo;
                            dgvDocsCompraMP.Rows[iii].Cells["CHECKLINE"].Value = 'F';
                        }
                    }
                }



                for (int ii = 0; ii < dgvImputacion.Rows.Count; ii++)
                {
                    codartIMP = string.IsNullOrEmpty(dgvImputacion.Rows[ii].Cells["CODART"].Value.ToString()) ? "" : dgvImputacion.Rows[ii].Cells["CODART"].Value.ToString();
                    if (codartIMP == codartMP && dgvImputacion.Rows[ii].Cells["CODART"].Value.ToString() != "MISMAFRA" && dgvImputacion.Rows[ii].Cells["ORIGEN"].Value.ToString() != "PROMOS")
                    {
                        importeArticuloImputar = importeArticuloImputar + Convert.ToDouble(dgvImputacion.Rows[ii].Cells["IMPORTE"].Value.ToString());
                    }
                }

                pesoReferenciaArticulo = Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["PESO"].Value.ToString());

                importeArticuloImputarPonderado = (importeArticuloImputar / unidadesCalculo);
                importeUnitarioPonderado = importeUnitario * pesoReferenciaArticulo;
                costeAdicional = Math.Round((importeUnitarioPonderado) + (importeArticuloImputarPonderado), 5);


                prcMedioActualizado = dgvDocsCompraMP.Rows[i].Cells["PRCMEDIO"].Value.ToString();

                dgvDocsCompraMP.Rows[i].Cells["COSTES_COMUNES"].Value = importeUnitarioPonderado;
                dgvDocsCompraMP.Rows[i].Cells["COSTES_ESPECIFICOS"].Value = importeArticuloImputarPonderado;

                if (!string.IsNullOrEmpty(dgvDocsCompraMP.Rows[i].Cells["LOTE"].Value.ToString()) && dgvDocsCompraMP.Rows[i].Cells["ETIQ"].Value.ToString() != "SI")
                {
                    dgvDocsCompraMP.Rows[i].Cells["COSTEADIC"].Value = costeAdicional;
                    dgvDocsCompraMP.Rows[i].Cells["CTU"].Value = costeAdicional + Convert.ToDouble(prcMedioActualizado);
                }
                else
                {
                    dgvDocsCompraMP.Rows[i].Cells["CTU"].Value = Convert.ToDouble(prcMedioActualizado);
                }

                numLineasPromos = 0;
                descuentoLineaPromo = 0;
                unidadesLinea = 0;
                cantidadLineasPromos = 0;
                cantidadLineasSinPromos = 0;
                precioRefLineasPromos = 0; ;
                importeArticuloImputar = 0;
            }

        }

        private void dgvDocsCompraMP_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string descuento = "";

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "COSTEADIC")
            {
                e.CellStyle.ForeColor = Color.Red;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "COSTES_COMUNES")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.ForeColor = Color.Blue;
                e.CellStyle.Format = "N5";
            }

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "COSTES_ESPECIFICOS")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                e.CellStyle.ForeColor = Color.Blue;
                e.CellStyle.Format = "N5";
            }
            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "CTU" || this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "COEF_REPARTO")
            {
                e.CellStyle.Format = "N5";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "PRCMONEDA")
            {
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "PRECIO" || this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "PRCMEDIO" || this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "PESO")
            {
                e.CellStyle.Format = "N5";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "PRCMEDIO")
            {
                descuento = this.dgvDocsCompraMP.Rows[e.RowIndex].Cells["DTO"].Value.ToString().Replace(",0000", "");
                if (descuento == "100")
                {
                    e.CellStyle.ForeColor = Color.Red;
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "BASE")
            {
                e.CellStyle.Format = "N2";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "LOTE")
            {
                if (!string.IsNullOrEmpty(this.dgvDocsCompraMP.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) && this.dgvDocsCompraMP.Rows[e.RowIndex].Cells["ETIQ"].Value.ToString() != "SI")
                {
                    this.dgvDocsCompraMP.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Aquamarine;
                }

                //e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }



            //if (this.dgvStock.Columns[e.ColumnIndex].Name == "StockPS")
            //{
            //    try
            //    {
            //        e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //        cantidadPS = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockPS"].Value);
            //        cantidadA3 = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockA3"].Value);
            //        if (cantidadA3 != cantidadPS)
            //        {

            //            e.CellStyle.ForeColor = Color.Red;
            //        }

            //    }
            //    catch (Exception ex)
            //    { }
            //}
        }

        private void cboxProveedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            string codProveedor = cboxProveedores.SelectedValue.ToString();
            cargarFacturas(codProveedor);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveCosteAdicional_Click(object sender, EventArgs e)
        {
            btnSaveCosteAdicional.Enabled = false;
           
            DialogResult dialogo = MessageBox.Show("¿Se van a modificar datos en los costes de artículo, ¿Está seguro?", "Actualizar Costes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dialogo == DialogResult.Yes)
            {
                string codart = "";
                string lote = "";
                string fecCaduc = "";
                double prcMedio = 0;
                double costeAdic = 0;
                string idFacC = "";
                double precio = 0;
                double descuento= 0;
                double prcMedioNeto = 0;

                foreach (DataGridViewRow fila in dgvDocsCompraMP.Rows)
                {
                    codart = fila.Cells["CODART"].Value.ToString().Trim();
                    lote = fila.Cells["LOTE"].Value.ToString().Trim();
                    costeAdic = Convert.ToDouble(fila.Cells["COSTEADIC"].Value.ToString());
                    //prcMedio = Convert.ToDouble(fila.Cells["PRECIO"].Value.ToString()) + costeAdic;
                    precio = Convert.ToDouble(fila.Cells["PRECIO"].Value.ToString());
                    idFacC = fila.Cells["IDFACC"].Value.ToString().Replace(",0000", "");

                    descuento= Double.Parse(fila.Cells["DTO"].Value.ToString());
                    prcMedioNeto = precio * ((100 - descuento) / 100);
                    if (descuento == 100)
                        prcMedioNeto = Double.Parse(fila.Cells["PRCMEDIO"].Value.ToString()); ;


                    csUtilidades.ejecutarConsulta("UPDATE LINEALBA SET PRCMEDIO=" + prcMedioNeto.ToString().Replace(",", ".") + ",COSTEADIC=" + costeAdic.ToString().Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                    csUtilidades.ejecutarConsulta("UPDATE LINEFACT SET PRCMEDIO=" + prcMedioNeto.ToString().Replace(",", ".") + ",COSTEADIC=" + costeAdic.ToString().Replace(",", ".") + " WHERE IDFACC=" + idFacC + " AND LTRIM(CODART) = '" + codart + "' AND LTRIM(LOTE) = '" + lote + "'", false);
                    csUtilidades.ejecutarConsulta("UPDATE STOCKALM SET PRCMEDIO=" + prcMedioNeto.ToString().Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND LTRIM(LOTE)='" + lote + "'", false);
                    csUtilidades.ejecutarConsulta("UPDATE HISTPROD SET PRCMEDIO=" + prcMedioNeto.ToString().Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND LTRIM(LOTE)='" + lote + "'", false);
                }

            }

            "Proceso finalizado".mb();


        }

        private void dgvImputacion_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //if (this.dgvImputacion.Columns[e.ColumnIndex].Name == "COSTEADIC")
            //{
            //    e.CellStyle.ForeColor = Color.Red;
            //    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //}

            if (this.dgvImputacion.Columns[e.ColumnIndex].Name == "IMPORTE")
            {
                e.CellStyle.Format = "N2";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }

        private void recalcularOrdenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            recalcularOrdenFabricacion();
        }

        private void recalcularOrdenFabricacion()
        {
            double costeUnitarioProdTerminado = 0;
            int unidadesFabricadas = 0;
            double unidadesComponente = 0;
            double precioMedioComponente = 0;
            double costeComponente = 0;
            double costeTotalOF = 0;

            foreach (DataGridViewRow fila in dgvOrdFab.Rows)
            {
                //FABRICADO
                if (fila.Cells["SIGNO"].Value.ToString() == "F")
                {
                    unidadesFabricadas = Convert.ToInt16(fila.Cells["UNIDADES"].Value.ToString());
                }
                //COMPONENTE
                else
                {
                    unidadesComponente = Convert.ToDouble(fila.Cells["UNIDADES"].Value.ToString());
                    precioMedioComponente = Convert.ToDouble(fila.Cells["PRCMEDIO"].Value.ToString());
                    costeComponente = Math.Round(unidadesComponente * precioMedioComponente, 4);
                    costeTotalOF = costeTotalOF + costeComponente;
                    fila.Cells["COSTE_RECALC"].Value = Math.Round(costeTotalOF, 4).ToString();
                }
            }

            //recalculo precio final
            costeUnitarioProdTerminado = costeTotalOF / unidadesFabricadas;
            dgvOrdFab.Rows[0].Cells["COSTE_RECALC"].Value = Math.Round(costeUnitarioProdTerminado, 5).ToString();



        }

        private void dgvOrdFab_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (this.dgvOrdFab.Columns[e.ColumnIndex].Name == "COSTE_RECALC")
            {
                e.CellStyle.ForeColor = Color.Red;
                e.CellStyle.Format = "N5";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }

        private void actualizarPreciosEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string codart = "";
            string lote = "";
            string prcMedio = "";
            foreach (DataGridViewRow fila in dgvOrdFab.Rows)
            {
                //FABRICADO
                if (fila.Cells["SIGNO"].Value.ToString() == "F")
                {
                    codart = fila.Cells["CODART"].Value.ToString();
                    lote = fila.Cells["LOTE"].Value.ToString();
                    prcMedio = fila.Cells["COSTE_RECALC"].Value.ToString();
                    if (string.IsNullOrEmpty(prcMedio) || Convert.ToDouble(prcMedio) == 0)
                    {
                        return;
                    }
                }

            }

            string dialogMessage = "¿Se van a modificar datos de los precios de coste de artículo de A3ERP, ¿Está seguro?\n\n" +
                "ARTICULO: " + codart + " - LOTE: " + lote + " - PRECIO: " + prcMedio + "\n\n" +
                "Tablas que se van a modificar: \n\n" +
                " - Ordenes de fabricación \n" +
                " - Regularizaciones \n" +
                " - Traspasos \n" +
                " - Albaranes de Venta \n" +
                " - Facturas de Venta \n";
            DialogResult dialogo = MessageBox.Show(dialogMessage, "Actualizar Costes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dialogo == DialogResult.Yes)
            {
                csUtilidades.ejecutarConsulta("UPDATE LINEALBA SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEFACT SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEREGU SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEMOVS SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINETRAS SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE HISTPROD SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEINVE SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE STOCKALM SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + lote + "'", false);
                //falta stockalm


                "Proceso finalizado".mb();

            }
        }

        private void dgvMovStock_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dgvMovStock.Columns[e.ColumnIndex].Name == "COSTE_RECALC")
            {
                e.CellStyle.ForeColor = Color.Red;
                e.CellStyle.Format = "N5";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvMovStock.Columns[e.ColumnIndex].Name == "MARGEN" || this.dgvMovStock.Columns[e.ColumnIndex].Name == "NETO")
            {
                e.CellStyle.Format = "N2";
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (this.dgvMovStock.Columns[e.ColumnIndex].Name == "MARGEN_P")
            {
                if (!string.IsNullOrEmpty(dgvMovStock.Rows[e.RowIndex].Cells["MARGEN_P"].Value.ToString()))
                {
                    e.CellStyle.Format = "##.##%";
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }

        }

        private void actualizarCosteEnDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string codart = "";
            string lote = "";
            string loteInventario = "";
            string prcMedio = "";
            string identificador = " ";
            string fecha = "";

            string dialogMessage = "Va modificar el precio de coste de todos los albaranes de venta posterioes al inventario que ha selecionado¿Está seguro?\n\n";

            DialogResult dialogo = MessageBox.Show(dialogMessage, "Actualizar Costes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (dgvMovStock.SelectedRows.Count == 1 && dgvMovStock.CurrentRow.Cells["TIPDOC"].Value.ToString() == "IN" && dialogo == DialogResult.Yes)
            {
                prcMedio = dgvMovStock.CurrentRow.Cells["PRCMEDIO"].Value.ToString();
                loteInventario = dgvMovStock.CurrentRow.Cells["LOTE"].Value.ToString();
                codart = dgvMovStock.CurrentRow.Cells["CODART"].Value.ToString();
                fecha = dgvMovStock.CurrentRow.Cells["FECDOC"].Value.ToString();

                csUtilidades.ejecutarConsulta("UPDATE LINEALBA SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND IDALBV IS NOT NULL AND FECHA>='" + fecha+"'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEFACT SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND IDFACV IS NOT NULL AND FECHA>='" + fecha + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINEREGU SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);

                csUtilidades.ejecutarConsulta("UPDATE LINEMOVS SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE LINETRAS SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE HISTPROD SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);

                csUtilidades.ejecutarConsulta("UPDATE LINEINVE SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);
                csUtilidades.ejecutarConsulta("UPDATE STOCKALM SET PRCMEDIO=" + prcMedio.Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND  LTRIM(LOTE)='" + loteInventario + "'AND FECHA>='" + fecha + "'", false);


                "Proceso finalizado".mb();
                        
                    
                
            }
            else
            {
                MessageBox.Show("Debe selecionar una única linea y que sea de tipo:'IN' ");
            }
        }

        private void cMenuMovimientos_Opening(object sender, CancelEventArgs e)
        {

        }

        private void dgvMimasa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvDocsCompraAsoc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
