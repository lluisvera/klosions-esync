﻿namespace klsync.Mimasa
{
    partial class frMimasa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnLoadProducts = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.grBoxTipoMovs = new System.Windows.Forms.GroupBox();
            this.cboxIN = new System.Windows.Forms.CheckBox();
            this.cboxPR = new System.Windows.Forms.CheckBox();
            this.cboxTR = new System.Windows.Forms.CheckBox();
            this.cboxRE = new System.Windows.Forms.CheckBox();
            this.cboxFC = new System.Windows.Forms.CheckBox();
            this.cboxAC = new System.Windows.Forms.CheckBox();
            this.cboxFV = new System.Windows.Forms.CheckBox();
            this.cboxAV = new System.Windows.Forms.CheckBox();
            this.grBoxStock = new System.Windows.Forms.GroupBox();
            this.rbStockFull = new System.Windows.Forms.RadioButton();
            this.rbStockDif0 = new System.Windows.Forms.RadioButton();
            this.cboxArticulos = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grBoxTipoCarga = new System.Windows.Forms.GroupBox();
            this.rbCargaCompleta = new System.Windows.Forms.RadioButton();
            this.rbCargaBasica = new System.Windows.Forms.RadioButton();
            this.lblFactCompraAsoc = new System.Windows.Forms.Label();
            this.dgvFactCompraAsoc = new System.Windows.Forms.DataGridView();
            this.lblFactCompra = new System.Windows.Forms.Label();
            this.dgvFactCompra = new System.Windows.Forms.DataGridView();
            this.lblAlbCompra = new System.Windows.Forms.Label();
            this.lblOrdFab = new System.Windows.Forms.Label();
            this.lblMovStock = new System.Windows.Forms.Label();
            this.lblStockAlmacen = new System.Windows.Forms.Label();
            this.dgvAlbCompra = new System.Windows.Forms.DataGridView();
            this.dgvMovStock = new System.Windows.Forms.DataGridView();
            this.cMenuMovimientos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.actualizarCosteEnDocumentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvOrdFab = new System.Windows.Forms.DataGridView();
            this.cMenuProduccion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.recalcularOrdenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarPreciosEnA3ERPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvMimasa = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabProducts = new System.Windows.Forms.TabPage();
            this.tabDocs = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.btnSaveCosteAdicional = new System.Windows.Forms.Button();
            this.cboxDocs = new System.Windows.Forms.ComboBox();
            this.tbResumenAcciones = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvImputacion = new System.Windows.Forms.DataGridView();
            this.cboxProveedores = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoadDocs = new System.Windows.Forms.Button();
            this.dgvDocsCompraAsoc = new System.Windows.Forms.DataGridView();
            this.dgvDocsCompraMP = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.grBoxTipoMovs.SuspendLayout();
            this.grBoxStock.SuspendLayout();
            this.grBoxTipoCarga.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactCompraAsoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovStock)).BeginInit();
            this.cMenuMovimientos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrdFab)).BeginInit();
            this.cMenuProduccion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMimasa)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabProducts.SuspendLayout();
            this.tabDocs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImputacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsCompraAsoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsCompraMP)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadProducts
            // 
            this.btnLoadProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadProducts.Location = new System.Drawing.Point(64, 52);
            this.btnLoadProducts.Name = "btnLoadProducts";
            this.btnLoadProducts.Size = new System.Drawing.Size(74, 29);
            this.btnLoadProducts.TabIndex = 0;
            this.btnLoadProducts.Text = "Cargar";
            this.btnLoadProducts.UseVisualStyleBackColor = true;
            this.btnLoadProducts.Click += new System.EventHandler(this.btnLoadProducts_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(6, 6);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.grBoxTipoMovs);
            this.splitContainer1.Panel1.Controls.Add(this.grBoxStock);
            this.splitContainer1.Panel1.Controls.Add(this.cboxArticulos);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.btnLoadProducts);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grBoxTipoCarga);
            this.splitContainer1.Panel2.Controls.Add(this.lblFactCompraAsoc);
            this.splitContainer1.Panel2.Controls.Add(this.dgvFactCompraAsoc);
            this.splitContainer1.Panel2.Controls.Add(this.lblFactCompra);
            this.splitContainer1.Panel2.Controls.Add(this.dgvFactCompra);
            this.splitContainer1.Panel2.Controls.Add(this.lblAlbCompra);
            this.splitContainer1.Panel2.Controls.Add(this.lblOrdFab);
            this.splitContainer1.Panel2.Controls.Add(this.lblMovStock);
            this.splitContainer1.Panel2.Controls.Add(this.lblStockAlmacen);
            this.splitContainer1.Panel2.Controls.Add(this.dgvAlbCompra);
            this.splitContainer1.Panel2.Controls.Add(this.dgvMovStock);
            this.splitContainer1.Panel2.Controls.Add(this.dgvOrdFab);
            this.splitContainer1.Panel2.Controls.Add(this.dgvMimasa);
            this.splitContainer1.Size = new System.Drawing.Size(1195, 1226);
            this.splitContainer1.SplitterDistance = 158;
            this.splitContainer1.TabIndex = 1;
            // 
            // grBoxTipoMovs
            // 
            this.grBoxTipoMovs.Controls.Add(this.cboxIN);
            this.grBoxTipoMovs.Controls.Add(this.cboxPR);
            this.grBoxTipoMovs.Controls.Add(this.cboxTR);
            this.grBoxTipoMovs.Controls.Add(this.cboxRE);
            this.grBoxTipoMovs.Controls.Add(this.cboxFC);
            this.grBoxTipoMovs.Controls.Add(this.cboxAC);
            this.grBoxTipoMovs.Controls.Add(this.cboxFV);
            this.grBoxTipoMovs.Controls.Add(this.cboxAV);
            this.grBoxTipoMovs.Location = new System.Drawing.Point(12, 141);
            this.grBoxTipoMovs.Name = "grBoxTipoMovs";
            this.grBoxTipoMovs.Size = new System.Drawing.Size(131, 210);
            this.grBoxTipoMovs.TabIndex = 10;
            this.grBoxTipoMovs.TabStop = false;
            this.grBoxTipoMovs.Text = "Tipos de movimiento";
            // 
            // cboxIN
            // 
            this.cboxIN.AutoSize = true;
            this.cboxIN.Checked = true;
            this.cboxIN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxIN.Location = new System.Drawing.Point(6, 179);
            this.cboxIN.Name = "cboxIN";
            this.cboxIN.Size = new System.Drawing.Size(93, 17);
            this.cboxIN.TabIndex = 7;
            this.cboxIN.Text = "IN (Inventario)";
            this.cboxIN.UseVisualStyleBackColor = true;
            // 
            // cboxPR
            // 
            this.cboxPR.AutoSize = true;
            this.cboxPR.Checked = true;
            this.cboxPR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxPR.Location = new System.Drawing.Point(6, 156);
            this.cboxPR.Name = "cboxPR";
            this.cboxPR.Size = new System.Drawing.Size(104, 17);
            this.cboxPR.TabIndex = 6;
            this.cboxPR.Text = "PR (Producción)";
            this.cboxPR.UseVisualStyleBackColor = true;
            // 
            // cboxTR
            // 
            this.cboxTR.AutoSize = true;
            this.cboxTR.Checked = true;
            this.cboxTR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxTR.Location = new System.Drawing.Point(6, 134);
            this.cboxTR.Name = "cboxTR";
            this.cboxTR.Size = new System.Drawing.Size(115, 17);
            this.cboxTR.TabIndex = 5;
            this.cboxTR.Text = "TR (Alb. Traspaso)";
            this.cboxTR.UseVisualStyleBackColor = true;
            // 
            // cboxRE
            // 
            this.cboxRE.AutoSize = true;
            this.cboxRE.Checked = true;
            this.cboxRE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxRE.Location = new System.Drawing.Point(6, 111);
            this.cboxRE.Name = "cboxRE";
            this.cboxRE.Size = new System.Drawing.Size(91, 17);
            this.cboxRE.TabIndex = 4;
            this.cboxRE.Text = "RE (Alb. Reg)";
            this.cboxRE.UseVisualStyleBackColor = true;
            // 
            // cboxFC
            // 
            this.cboxFC.AutoSize = true;
            this.cboxFC.Checked = true;
            this.cboxFC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxFC.Location = new System.Drawing.Point(6, 88);
            this.cboxFC.Name = "cboxFC";
            this.cboxFC.Size = new System.Drawing.Size(111, 17);
            this.cboxFC.TabIndex = 3;
            this.cboxFC.Text = "FC (Fact. Compra)";
            this.cboxFC.UseVisualStyleBackColor = true;
            // 
            // cboxAC
            // 
            this.cboxAC.AutoSize = true;
            this.cboxAC.Checked = true;
            this.cboxAC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxAC.Location = new System.Drawing.Point(6, 65);
            this.cboxAC.Name = "cboxAC";
            this.cboxAC.Size = new System.Drawing.Size(106, 17);
            this.cboxAC.TabIndex = 2;
            this.cboxAC.Text = "AC (Alb. Compra)";
            this.cboxAC.UseVisualStyleBackColor = true;
            // 
            // cboxFV
            // 
            this.cboxFV.AutoSize = true;
            this.cboxFV.Checked = true;
            this.cboxFV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxFV.Location = new System.Drawing.Point(6, 42);
            this.cboxFV.Name = "cboxFV";
            this.cboxFV.Size = new System.Drawing.Size(103, 17);
            this.cboxFV.TabIndex = 1;
            this.cboxFV.Text = "FV (Fact. Venta)";
            this.cboxFV.UseVisualStyleBackColor = true;
            // 
            // cboxAV
            // 
            this.cboxAV.AutoSize = true;
            this.cboxAV.Checked = true;
            this.cboxAV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cboxAV.Location = new System.Drawing.Point(6, 19);
            this.cboxAV.Name = "cboxAV";
            this.cboxAV.Size = new System.Drawing.Size(98, 17);
            this.cboxAV.TabIndex = 0;
            this.cboxAV.Text = "AV (Alb. Venta)";
            this.cboxAV.UseVisualStyleBackColor = true;
            // 
            // grBoxStock
            // 
            this.grBoxStock.Controls.Add(this.rbStockFull);
            this.grBoxStock.Controls.Add(this.rbStockDif0);
            this.grBoxStock.Location = new System.Drawing.Point(12, 87);
            this.grBoxStock.Name = "grBoxStock";
            this.grBoxStock.Size = new System.Drawing.Size(131, 48);
            this.grBoxStock.TabIndex = 9;
            this.grBoxStock.TabStop = false;
            this.grBoxStock.Text = "Stock diferente de 0";
            // 
            // rbStockFull
            // 
            this.rbStockFull.AutoSize = true;
            this.rbStockFull.Location = new System.Drawing.Point(58, 19);
            this.rbStockFull.Name = "rbStockFull";
            this.rbStockFull.Size = new System.Drawing.Size(39, 17);
            this.rbStockFull.TabIndex = 1;
            this.rbStockFull.Text = "No";
            this.rbStockFull.UseVisualStyleBackColor = true;
            // 
            // rbStockDif0
            // 
            this.rbStockDif0.AutoSize = true;
            this.rbStockDif0.Checked = true;
            this.rbStockDif0.Location = new System.Drawing.Point(18, 19);
            this.rbStockDif0.Name = "rbStockDif0";
            this.rbStockDif0.Size = new System.Drawing.Size(34, 17);
            this.rbStockDif0.TabIndex = 0;
            this.rbStockDif0.TabStop = true;
            this.rbStockDif0.Text = "Si";
            this.rbStockDif0.UseVisualStyleBackColor = true;
            // 
            // cboxArticulos
            // 
            this.cboxArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxArticulos.DropDownWidth = 300;
            this.cboxArticulos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxArticulos.FormattingEnabled = true;
            this.cboxArticulos.Location = new System.Drawing.Point(12, 25);
            this.cboxArticulos.Name = "cboxArticulos";
            this.cboxArticulos.Size = new System.Drawing.Size(123, 24);
            this.cboxArticulos.TabIndex = 8;
            this.cboxArticulos.SelectedIndexChanged += new System.EventHandler(this.cboxArticulos_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Código Articulo";
            // 
            // grBoxTipoCarga
            // 
            this.grBoxTipoCarga.Controls.Add(this.rbCargaCompleta);
            this.grBoxTipoCarga.Controls.Add(this.rbCargaBasica);
            this.grBoxTipoCarga.Location = new System.Drawing.Point(154, 9);
            this.grBoxTipoCarga.Name = "grBoxTipoCarga";
            this.grBoxTipoCarga.Size = new System.Drawing.Size(148, 36);
            this.grBoxTipoCarga.TabIndex = 12;
            this.grBoxTipoCarga.TabStop = false;
            this.grBoxTipoCarga.Text = "Tipo Carga Datos";
            // 
            // rbCargaCompleta
            // 
            this.rbCargaCompleta.AutoSize = true;
            this.rbCargaCompleta.Location = new System.Drawing.Point(78, 15);
            this.rbCargaCompleta.Name = "rbCargaCompleta";
            this.rbCargaCompleta.Size = new System.Drawing.Size(69, 17);
            this.rbCargaCompleta.TabIndex = 1;
            this.rbCargaCompleta.Text = "Completa";
            this.rbCargaCompleta.UseVisualStyleBackColor = true;
            // 
            // rbCargaBasica
            // 
            this.rbCargaBasica.AutoSize = true;
            this.rbCargaBasica.Checked = true;
            this.rbCargaBasica.Location = new System.Drawing.Point(15, 15);
            this.rbCargaBasica.Name = "rbCargaBasica";
            this.rbCargaBasica.Size = new System.Drawing.Size(57, 17);
            this.rbCargaBasica.TabIndex = 0;
            this.rbCargaBasica.TabStop = true;
            this.rbCargaBasica.Text = "Básica";
            this.rbCargaBasica.UseVisualStyleBackColor = true;
            // 
            // lblFactCompraAsoc
            // 
            this.lblFactCompraAsoc.AutoSize = true;
            this.lblFactCompraAsoc.Location = new System.Drawing.Point(3, 1395);
            this.lblFactCompraAsoc.Name = "lblFactCompraAsoc";
            this.lblFactCompraAsoc.Size = new System.Drawing.Size(195, 13);
            this.lblFactCompraAsoc.TabIndex = 11;
            this.lblFactCompraAsoc.Text = "FACTURAS DE COMPRA ASOCIADAS";
            // 
            // dgvFactCompraAsoc
            // 
            this.dgvFactCompraAsoc.AllowUserToAddRows = false;
            this.dgvFactCompraAsoc.AllowUserToDeleteRows = false;
            this.dgvFactCompraAsoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFactCompraAsoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactCompraAsoc.Location = new System.Drawing.Point(0, 1412);
            this.dgvFactCompraAsoc.Name = "dgvFactCompraAsoc";
            this.dgvFactCompraAsoc.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvFactCompraAsoc.Size = new System.Drawing.Size(1027, 113);
            this.dgvFactCompraAsoc.TabIndex = 10;
            this.dgvFactCompraAsoc.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFactCompraAsoc_DataBindingComplete);
            // 
            // lblFactCompra
            // 
            this.lblFactCompra.AutoSize = true;
            this.lblFactCompra.Location = new System.Drawing.Point(3, 1240);
            this.lblFactCompra.Name = "lblFactCompra";
            this.lblFactCompra.Size = new System.Drawing.Size(131, 13);
            this.lblFactCompra.TabIndex = 9;
            this.lblFactCompra.Text = "FACTURAS DE COMPRA";
            // 
            // dgvFactCompra
            // 
            this.dgvFactCompra.AllowUserToAddRows = false;
            this.dgvFactCompra.AllowUserToDeleteRows = false;
            this.dgvFactCompra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFactCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactCompra.Location = new System.Drawing.Point(0, 1257);
            this.dgvFactCompra.Name = "dgvFactCompra";
            this.dgvFactCompra.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvFactCompra.Size = new System.Drawing.Size(1027, 113);
            this.dgvFactCompra.TabIndex = 8;
            this.dgvFactCompra.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFactCompra_DataBindingComplete);
            // 
            // lblAlbCompra
            // 
            this.lblAlbCompra.AutoSize = true;
            this.lblAlbCompra.Location = new System.Drawing.Point(3, 1082);
            this.lblAlbCompra.Name = "lblAlbCompra";
            this.lblAlbCompra.Size = new System.Drawing.Size(138, 13);
            this.lblAlbCompra.TabIndex = 7;
            this.lblAlbCompra.Text = "ALBARANES DE COMPRA";
            // 
            // lblOrdFab
            // 
            this.lblOrdFab.AutoSize = true;
            this.lblOrdFab.Location = new System.Drawing.Point(3, 796);
            this.lblOrdFab.Name = "lblOrdFab";
            this.lblOrdFab.Size = new System.Drawing.Size(152, 13);
            this.lblOrdFab.TabIndex = 6;
            this.lblOrdFab.Text = "ORDENES DE FABRICACIÓN";
            // 
            // lblMovStock
            // 
            this.lblMovStock.AutoSize = true;
            this.lblMovStock.Location = new System.Drawing.Point(3, 399);
            this.lblMovStock.Name = "lblMovStock";
            this.lblMovStock.Size = new System.Drawing.Size(140, 13);
            this.lblMovStock.TabIndex = 5;
            this.lblMovStock.Text = "MOVIMIENTOS DE STOCK";
            // 
            // lblStockAlmacen
            // 
            this.lblStockAlmacen.AutoSize = true;
            this.lblStockAlmacen.Location = new System.Drawing.Point(3, 25);
            this.lblStockAlmacen.Name = "lblStockAlmacen";
            this.lblStockAlmacen.Size = new System.Drawing.Size(129, 13);
            this.lblStockAlmacen.TabIndex = 4;
            this.lblStockAlmacen.Text = "STOCK EN ALMACENES";
            // 
            // dgvAlbCompra
            // 
            this.dgvAlbCompra.AllowUserToAddRows = false;
            this.dgvAlbCompra.AllowUserToDeleteRows = false;
            this.dgvAlbCompra.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAlbCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlbCompra.Location = new System.Drawing.Point(0, 1099);
            this.dgvAlbCompra.Name = "dgvAlbCompra";
            this.dgvAlbCompra.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvAlbCompra.Size = new System.Drawing.Size(1027, 113);
            this.dgvAlbCompra.TabIndex = 3;
            this.dgvAlbCompra.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAlbCompra_DataBindingComplete);
            // 
            // dgvMovStock
            // 
            this.dgvMovStock.AllowUserToAddRows = false;
            this.dgvMovStock.AllowUserToDeleteRows = false;
            this.dgvMovStock.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMovStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMovStock.ContextMenuStrip = this.cMenuMovimientos;
            this.dgvMovStock.Location = new System.Drawing.Point(3, 422);
            this.dgvMovStock.Name = "dgvMovStock";
            this.dgvMovStock.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvMovStock.Size = new System.Drawing.Size(1027, 236);
            this.dgvMovStock.TabIndex = 2;
            this.dgvMovStock.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMovStock_CellDoubleClick);
            this.dgvMovStock.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvMovStock_CellFormatting);
            this.dgvMovStock.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMovStock_DataBindingComplete);
            // 
            // cMenuMovimientos
            // 
            this.cMenuMovimientos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarCosteEnDocumentosToolStripMenuItem});
            this.cMenuMovimientos.Name = "cMenuMovimientos";
            this.cMenuMovimientos.Size = new System.Drawing.Size(247, 26);
            this.cMenuMovimientos.Opening += new System.ComponentModel.CancelEventHandler(this.cMenuMovimientos_Opening);
            // 
            // actualizarCosteEnDocumentosToolStripMenuItem
            // 
            this.actualizarCosteEnDocumentosToolStripMenuItem.Name = "actualizarCosteEnDocumentosToolStripMenuItem";
            this.actualizarCosteEnDocumentosToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.actualizarCosteEnDocumentosToolStripMenuItem.Text = "Actualizar Coste en Documentos";
            this.actualizarCosteEnDocumentosToolStripMenuItem.Click += new System.EventHandler(this.actualizarCosteEnDocumentosToolStripMenuItem_Click);
            // 
            // dgvOrdFab
            // 
            this.dgvOrdFab.AllowUserToAddRows = false;
            this.dgvOrdFab.AllowUserToDeleteRows = false;
            this.dgvOrdFab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOrdFab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrdFab.ContextMenuStrip = this.cMenuProduccion;
            this.dgvOrdFab.Location = new System.Drawing.Point(0, 812);
            this.dgvOrdFab.Name = "dgvOrdFab";
            this.dgvOrdFab.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvOrdFab.Size = new System.Drawing.Size(1027, 251);
            this.dgvOrdFab.TabIndex = 1;
            this.dgvOrdFab.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrdFab_CellDoubleClick);
            this.dgvOrdFab.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvOrdFab_CellFormatting);
            this.dgvOrdFab.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvOrdFab_DataBindingComplete);
            // 
            // cMenuProduccion
            // 
            this.cMenuProduccion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recalcularOrdenToolStripMenuItem,
            this.actualizarPreciosEnA3ERPToolStripMenuItem});
            this.cMenuProduccion.Name = "cMenuProduccion";
            this.cMenuProduccion.Size = new System.Drawing.Size(221, 48);
            // 
            // recalcularOrdenToolStripMenuItem
            // 
            this.recalcularOrdenToolStripMenuItem.Name = "recalcularOrdenToolStripMenuItem";
            this.recalcularOrdenToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.recalcularOrdenToolStripMenuItem.Text = "Recalcular Orden";
            this.recalcularOrdenToolStripMenuItem.Click += new System.EventHandler(this.recalcularOrdenToolStripMenuItem_Click);
            // 
            // actualizarPreciosEnA3ERPToolStripMenuItem
            // 
            this.actualizarPreciosEnA3ERPToolStripMenuItem.Name = "actualizarPreciosEnA3ERPToolStripMenuItem";
            this.actualizarPreciosEnA3ERPToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.actualizarPreciosEnA3ERPToolStripMenuItem.Text = "Actualizar Precios en A3ERP";
            this.actualizarPreciosEnA3ERPToolStripMenuItem.Click += new System.EventHandler(this.actualizarPreciosEnA3ERPToolStripMenuItem_Click);
            // 
            // dgvMimasa
            // 
            this.dgvMimasa.AllowUserToAddRows = false;
            this.dgvMimasa.AllowUserToDeleteRows = false;
            this.dgvMimasa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMimasa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMimasa.Location = new System.Drawing.Point(3, 52);
            this.dgvMimasa.Name = "dgvMimasa";
            this.dgvMimasa.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dgvMimasa.Size = new System.Drawing.Size(1030, 312);
            this.dgvMimasa.TabIndex = 0;
            this.dgvMimasa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMimasa_CellContentClick);
            this.dgvMimasa.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMimasa_CellDoubleClick);
            this.dgvMimasa.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvMimasa_DataBindingComplete);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabProducts);
            this.tabControl1.Controls.Add(this.tabDocs);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1300, 738);
            this.tabControl1.TabIndex = 2;
            // 
            // tabProducts
            // 
            this.tabProducts.AutoScroll = true;
            this.tabProducts.Controls.Add(this.splitContainer1);
            this.tabProducts.Location = new System.Drawing.Point(4, 22);
            this.tabProducts.Name = "tabProducts";
            this.tabProducts.Padding = new System.Windows.Forms.Padding(3);
            this.tabProducts.Size = new System.Drawing.Size(1292, 712);
            this.tabProducts.TabIndex = 0;
            this.tabProducts.Text = "Artículos";
            this.tabProducts.UseVisualStyleBackColor = true;
            // 
            // tabDocs
            // 
            this.tabDocs.Controls.Add(this.splitContainer2);
            this.tabDocs.Location = new System.Drawing.Point(4, 22);
            this.tabDocs.Name = "tabDocs";
            this.tabDocs.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocs.Size = new System.Drawing.Size(1292, 712);
            this.tabDocs.TabIndex = 1;
            this.tabDocs.Text = "Documentos";
            this.tabDocs.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btnSaveCosteAdicional);
            this.splitContainer2.Panel1.Controls.Add(this.cboxDocs);
            this.splitContainer2.Panel1.Controls.Add(this.tbResumenAcciones);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.btnPreview);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.dgvImputacion);
            this.splitContainer2.Panel1.Controls.Add(this.cboxProveedores);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.btnLoadDocs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvDocsCompraAsoc);
            this.splitContainer2.Panel2.Controls.Add(this.dgvDocsCompraMP);
            this.splitContainer2.Size = new System.Drawing.Size(1286, 706);
            this.splitContainer2.SplitterDistance = 195;
            this.splitContainer2.TabIndex = 0;
            // 
            // btnSaveCosteAdicional
            // 
            this.btnSaveCosteAdicional.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveCosteAdicional.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveCosteAdicional.Location = new System.Drawing.Point(85, 576);
            this.btnSaveCosteAdicional.Name = "btnSaveCosteAdicional";
            this.btnSaveCosteAdicional.Size = new System.Drawing.Size(107, 48);
            this.btnSaveCosteAdicional.TabIndex = 19;
            this.btnSaveCosteAdicional.Text = "Grabar Coste Adicional";
            this.btnSaveCosteAdicional.UseVisualStyleBackColor = true;
            this.btnSaveCosteAdicional.Click += new System.EventHandler(this.btnSaveCosteAdicional_Click);
            // 
            // cboxDocs
            // 
            this.cboxDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxDocs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxDocs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxDocs.DropDownWidth = 400;
            this.cboxDocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxDocs.FormattingEnabled = true;
            this.cboxDocs.Location = new System.Drawing.Point(14, 73);
            this.cboxDocs.Name = "cboxDocs";
            this.cboxDocs.Size = new System.Drawing.Size(135, 28);
            this.cboxDocs.TabIndex = 18;
            // 
            // tbResumenAcciones
            // 
            this.tbResumenAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbResumenAcciones.Enabled = false;
            this.tbResumenAcciones.Location = new System.Drawing.Point(12, 397);
            this.tbResumenAcciones.Multiline = true;
            this.tbResumenAcciones.Name = "tbResumenAcciones";
            this.tbResumenAcciones.Size = new System.Drawing.Size(180, 173);
            this.tbResumenAcciones.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Número Documento";
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview.Location = new System.Drawing.Point(88, 362);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(104, 29);
            this.btnPreview.TabIndex = 14;
            this.btnPreview.Text = "Previsualizar";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Importes a imputar";
            // 
            // dgvImputacion
            // 
            this.dgvImputacion.AllowUserToAddRows = false;
            this.dgvImputacion.AllowUserToDeleteRows = false;
            this.dgvImputacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvImputacion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvImputacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvImputacion.Location = new System.Drawing.Point(3, 167);
            this.dgvImputacion.Name = "dgvImputacion";
            this.dgvImputacion.ReadOnly = true;
            this.dgvImputacion.Size = new System.Drawing.Size(189, 172);
            this.dgvImputacion.TabIndex = 12;
            this.dgvImputacion.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvImputacion_CellFormatting);
            // 
            // cboxProveedores
            // 
            this.cboxProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxProveedores.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxProveedores.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxProveedores.DropDownWidth = 300;
            this.cboxProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxProveedores.FormattingEnabled = true;
            this.cboxProveedores.Location = new System.Drawing.Point(14, 27);
            this.cboxProveedores.Name = "cboxProveedores";
            this.cboxProveedores.Size = new System.Drawing.Size(135, 28);
            this.cboxProveedores.TabIndex = 11;
            this.cboxProveedores.SelectedIndexChanged += new System.EventHandler(this.cboxProveedores_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Código Provedor";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnLoadDocs
            // 
            this.btnLoadDocs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadDocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadDocs.Location = new System.Drawing.Point(63, 105);
            this.btnLoadDocs.Name = "btnLoadDocs";
            this.btnLoadDocs.Size = new System.Drawing.Size(86, 29);
            this.btnLoadDocs.TabIndex = 9;
            this.btnLoadDocs.Text = "Cargar";
            this.btnLoadDocs.UseVisualStyleBackColor = true;
            this.btnLoadDocs.Click += new System.EventHandler(this.btnLoadDocs_Click);
            // 
            // dgvDocsCompraAsoc
            // 
            this.dgvDocsCompraAsoc.AllowUserToAddRows = false;
            this.dgvDocsCompraAsoc.AllowUserToDeleteRows = false;
            this.dgvDocsCompraAsoc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDocsCompraAsoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocsCompraAsoc.Location = new System.Drawing.Point(3, 362);
            this.dgvDocsCompraAsoc.Name = "dgvDocsCompraAsoc";
            this.dgvDocsCompraAsoc.ReadOnly = true;
            this.dgvDocsCompraAsoc.Size = new System.Drawing.Size(1079, 280);
            this.dgvDocsCompraAsoc.TabIndex = 1;
            this.dgvDocsCompraAsoc.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDocsCompraAsoc_CellContentClick);
            // 
            // dgvDocsCompraMP
            // 
            this.dgvDocsCompraMP.AllowUserToAddRows = false;
            this.dgvDocsCompraMP.AllowUserToDeleteRows = false;
            this.dgvDocsCompraMP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDocsCompraMP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocsCompraMP.Location = new System.Drawing.Point(1, 3);
            this.dgvDocsCompraMP.Name = "dgvDocsCompraMP";
            this.dgvDocsCompraMP.ReadOnly = true;
            this.dgvDocsCompraMP.Size = new System.Drawing.Size(1081, 336);
            this.dgvDocsCompraMP.TabIndex = 0;
            this.dgvDocsCompraMP.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvDocsCompraMP_CellFormatting);
            // 
            // frMimasa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1300, 738);
            this.Controls.Add(this.tabControl1);
            this.Name = "frMimasa";
            this.Text = "dgvImputacion";
            this.Load += new System.EventHandler(this.frMimasa_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.grBoxTipoMovs.ResumeLayout(false);
            this.grBoxTipoMovs.PerformLayout();
            this.grBoxStock.ResumeLayout(false);
            this.grBoxStock.PerformLayout();
            this.grBoxTipoCarga.ResumeLayout(false);
            this.grBoxTipoCarga.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactCompraAsoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovStock)).EndInit();
            this.cMenuMovimientos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrdFab)).EndInit();
            this.cMenuProduccion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMimasa)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabProducts.ResumeLayout(false);
            this.tabDocs.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImputacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsCompraAsoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsCompraMP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoadProducts;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvMimasa;
        private System.Windows.Forms.DataGridView dgvOrdFab;
        private System.Windows.Forms.DataGridView dgvMovStock;
        private System.Windows.Forms.DataGridView dgvAlbCompra;
        private System.Windows.Forms.Label lblOrdFab;
        private System.Windows.Forms.Label lblMovStock;
        private System.Windows.Forms.Label lblStockAlmacen;
        private System.Windows.Forms.Label lblAlbCompra;
        private System.Windows.Forms.Label lblFactCompraAsoc;
        private System.Windows.Forms.DataGridView dgvFactCompraAsoc;
        private System.Windows.Forms.Label lblFactCompra;
        private System.Windows.Forms.DataGridView dgvFactCompra;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxArticulos;
        private System.Windows.Forms.GroupBox grBoxStock;
        private System.Windows.Forms.RadioButton rbStockFull;
        private System.Windows.Forms.RadioButton rbStockDif0;
        private System.Windows.Forms.GroupBox grBoxTipoCarga;
        private System.Windows.Forms.RadioButton rbCargaCompleta;
        private System.Windows.Forms.RadioButton rbCargaBasica;
        private System.Windows.Forms.GroupBox grBoxTipoMovs;
        private System.Windows.Forms.CheckBox cboxIN;
        private System.Windows.Forms.CheckBox cboxPR;
        private System.Windows.Forms.CheckBox cboxTR;
        private System.Windows.Forms.CheckBox cboxRE;
        private System.Windows.Forms.CheckBox cboxFC;
        private System.Windows.Forms.CheckBox cboxAC;
        private System.Windows.Forms.CheckBox cboxFV;
        private System.Windows.Forms.CheckBox cboxAV;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProducts;
        private System.Windows.Forms.TabPage tabDocs;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ComboBox cboxProveedores;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLoadDocs;
        private System.Windows.Forms.DataGridView dgvDocsCompraAsoc;
        private System.Windows.Forms.DataGridView dgvDocsCompraMP;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvImputacion;
        private System.Windows.Forms.TextBox tbResumenAcciones;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboxDocs;
        private System.Windows.Forms.Button btnSaveCosteAdicional;
        private System.Windows.Forms.ContextMenuStrip cMenuProduccion;
        private System.Windows.Forms.ToolStripMenuItem recalcularOrdenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarPreciosEnA3ERPToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cMenuMovimientos;
        private System.Windows.Forms.ToolStripMenuItem actualizarCosteEnDocumentosToolStripMenuItem;
    }
}