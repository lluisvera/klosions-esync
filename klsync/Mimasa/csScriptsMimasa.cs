﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.Mimasa
{
    class csScriptsMimasa
    {
        csSqlConnects sql = new csSqlConnects();
        /// <summary>
        /// Script para obtener todos los artículos de ventas con sus diferentes lotes y fechas de caducidad
        /// </summary>
        /// <returns></returns>
        public string selectArticulosLoteFechaFraV()
        {
            string scriptSelect = "";
            scriptSelect =  "SELECT dbo.LINEFACT.CODART, dbo.LINEFACT.DESCLIN, SUM(dbo.LINEFACT.UNIDADES) AS UNITS, dbo.LINEFACT.LOTE, dbo.LINEFACT.FECCADUC, dbo.ARTICULO.ESACABADO " +
                            " FROM dbo.LINEFACT INNER JOIN " +
                            " dbo.CABEFACV ON dbo.CABEFACV.IDFACV = dbo.LINEFACT.IDFACV INNER JOIN "  +
                            " dbo.ARTICULO ON dbo.LINEFACT.CODART = dbo.ARTICULO.CODART " +
                            " WHERE(LTRIM(dbo.LINEFACT.CODART) NOT IN('0')) AND CABEFACV.FECHA>='2015-01-01' AND ARTICULO.AFESTOCK='T'" + 
                            " GROUP BY dbo.LINEFACT.CODART, dbo.LINEFACT.DESCLIN, dbo.LINEFACT.LOTE, dbo.LINEFACT.FECCADUC, dbo.ARTICULO.ESACABADO";
            return scriptSelect;
        }

        public string selectArticulosLote(bool cargaBasica, string codart=null, bool conStock=true, bool bloqueados=false)
        {
            string filtroConStock = conStock ? " AND UNIDADES > 0 " : "";
            string filtroBloqueo = bloqueados ? " AND BLOQUEADO = 'F' " : "";
            string filtroCodart = string.IsNullOrEmpty(codart) ? "" : " AND LTRIM (ARTICULO.CODART)='" + codart + "' ";
            string anexoCamposCargaCompleta = " ,ESACABADO, ESCOMPONENTE, ESCOMPRA, ESVENTA, HAYFECCADUC, HAYLOTES," +
                 "ARTICULO.BLOQUEADO,ARTICULO.MOTIVOBLOQUEO,ESKIT, UNIPAQC AS UNIDADES_PAQUETE ";
            anexoCamposCargaCompleta = cargaBasica ? "" : anexoCamposCargaCompleta;

            string scriptSelect = "";

            scriptSelect = "SELECT " +
                " STOCKALM.CODART, ARTICULO.DESCART, STOCKALM.CODALM, STOCKALM.UNIDADES, ltrim(LOTE) as LOTE, FECCADUC, ARTICULO.AFESTOCK, ARTICULO.CODPRO,  " +
                " STOCKALM.PRCMEDIO,  STOCKALM.UNIDADESSTOCK " + anexoCamposCargaCompleta +
                " FROM STOCKALM INNER JOIN ARTICULO ON ARTICULO.CODART = STOCKALM.CODART " +
                " WHERE STOCKALM.CODART IS NOT NULL " + " AND ESACABADO = 'T' " + filtroConStock + filtroConStock + filtroCodart +
                " ORDER BY CODART, LOTE, FECCADUC";
            return scriptSelect;
        }

        public string selectMovStockArtLote(string codart, string lote, string tipoMovimientos)
        {
            string scriptSelect = "";

            scriptSelect = "SELECT " +
                " LTRIM(CODART) AS CODART, DESCART, CODALM,  UNIDADES, LTRIM(LOTE) AS LOTE, FECCADUC, ENTRAN, SALEN, TIPDOC, " +
                " cast(IDENTIFICADOR as int) as IDENTIFICADOR , FECDOC, NUMDOC,  " +
                " PRCMEDIO,  COSTEADIC, OBTPRCCOSTE,  REFERENCIA,  " +
                " CODMON, CAMBIO, CODIGO, NOMBRE, PRCMONEDA, DESC1, PRCMONEDA*(1-(DESC1/100)) AS NETO, (PRCMEDIO + COSTEADIC) AS COSTE_U, " +
                " CASE WHEN PRCMONEDA>0  AND DESC1<100 THEN (PRCMONEDA*(1-(DESC1/100)) - PRCMEDIO)/(PRCMONEDA*(1-(DESC1/100))) ELSE 0 END AS MARGEN_P, " +
                " CASE WHEN TIPDOC='AV' THEN PRCMONEDA*(1-(DESC1/100))-PRCMEDIO ELSE 0 END AS MARGEN, " +
                " ORDEN, NUMLIN, ORDENDOC, CODALMSAL,  CAST(IDLOGP AS INT) AS LOGP  " +
                " FROM MovStocks  " +
                " WHERE  LTRIM(CODART)='" + codart + "' AND LTRIM(LOTE)='" + lote + "' " + tipoMovimientos;
            return scriptSelect;
        }

        public string selectOrdFab(string idOrdFab = null, string codart=null, string idLogp=null)
        {
            string scriptSelect = "";
            //string grupo = obtenerGrupoFabricacion(idOrdFab, codart);
            string grupo = idLogp;


            scriptSelect = "SELECT " +
                " HISTPROD.CODALM, LTRIM(HISTPROD.CODART) AS CODART, DESCART,  CONVERT(VARCHAR,FECHA,103) AS FECHA, UNIDADES, UNIDADESPREV, " +
                " LTRIM(LOTE) AS LOTE, CONVERT(VARCHAR,FECCADUC,103) AS FECCADUC, HISTPROD.PRCMEDIO, '' AS COSTE_RECALC, " +
                " GRUPO,cast(IDFASE as int) as IDFASE, LTRIM(FASE) AS FASE," +
                " CAST(IDLIN AS INT) AS IDLIN, CAST(IDLINP AS INT) AS IDLINP, cast(IDLOGP as int) as IDLOGP,  " +
                " CAST(IDPROD AS INT) AS IDPROD,  CAST(NUMLIN AS INT) AS NUMLIN, HISTPROD.OBTPRCCOSTE,  SIGNO  " +
                " FROM HISTPROD  " +
                " INNER JOIN ARTICULO " +
                " ON ARTICULO.CODART = HISTPROD.CODART" +
                " WHERE IDPROD=" + idOrdFab + " AND NUMLIN NOT IN (0)  " +
                " AND IDLOGP = " + grupo + 
                " ORDER BY  IDLOGP, IDFASE, CODART ";
            return scriptSelect;


        }


        /// <summary>
        /// Dentro de una OF pueden fabricarse varios productos
        /// Con esta consulta obtenemos el artículo
        /// </summary>
        /// <param name="idOrdFab"></param>
        /// <param name="codart"></param>
        /// <returns></returns>
        public string obtenerGrupoFabricacion(string idOrdFab, string codart)
        {
            string scriptSelect = "";
            string grupo = "";

            scriptSelect = "SELECT cast(IDLOGP as int) as IDLOGP FROM HISTPROD WHERE IDPROD=" + idOrdFab + " AND NUMLIN NOT IN (0) AND LTRIM(CODART)='" + codart +"' AND FASE IS NULL";

            grupo = sql.obtenerCampoTabla(scriptSelect, false);

            return grupo;

        }




        public string selectFacturasAsociadas(string idFraAsociada)
        {
            string scriptSelect = "SELECT " +
                " dbo.CABEFACC.FECHA, dbo.CABEFACC.NUMDOC, dbo.CABEFACC.CODPRO, dbo.CABEFACC.NOMPRO, dbo.LINEFACT.KLS_REL_FRA_C AS FRA_RELACIONADA, "+
                " dbo.LINEFACT.KLS_CODART, SUM(dbo.LINEFACT.BASE) AS IMPORTE_IMPUTAR " +
                " FROM dbo.LINEFACT INNER JOIN " +
                " dbo.CABEFACC ON dbo.CABEFACC.IDFACC = dbo.LINEFACT.IDFACC " +
                " GROUP BY dbo.LINEFACT.KLS_REL_FRA_C, dbo.LINEFACT.KLS_CODART, dbo.CABEFACC.NUMDOC, " +
                " dbo.CABEFACC.CODPRO, dbo.CABEFACC.NOMPRO, dbo.CABEFACC.FECHA " +
                " HAVING(dbo.LINEFACT.KLS_REL_FRA_C = " + idFraAsociada + ") AND(dbo.LINEFACT.KLS_CODART IS NULL)";

            return scriptSelect;


        }

        public string selectAlbCompras(string idDoc, string codart, string lote)
        {
            string scriptSelect = "";
            //idDoc = "2141";

            scriptSelect = "SELECT " +
                " CAST(NUMDOC AS int) as NUMDOC, CAST(LINEALBA.IDALBC AS int) AS IDALBC,  LTRIM(CODART) AS CODART, DESCLIN, " +
                " CABEALBC.CODALM, UNIDADES, UNISERVIDA, LTRIM(LOTE) AS LOTE," +
                " CODPRO,NOMPRO, CODMON, CABEALBC.FECHA, LINEALBA.BASE,  " +
                " COSTEADIC, DESC1, IDLIN, PRECIO, PRCMEDIO, PRCMONEDA, LINEALBA.SITUACION, CABEALBC.BASE,  CABEALBC.BASEMONEDA," +
                " LINEALBA.TIPIVA, SERIE " +
                " FROM CABEALBC INNER JOIN LINEALBA ON CABEALBC.IDALBC = LINEALBA.IDALBC " +
                " WHERE CABEALBC.IDALBC = " + idDoc + " AND LTRIM(CODART) = '" + codart + "' AND LTRIM(LOTE) = '" + lote + "'"; 
            return scriptSelect;

        }

        public string selectFactCompras(string codart, string lote)
        {
            string scriptSelect = "";
            //idDoc = "2141";

            scriptSelect = "SELECT " +
                " CAST(NUMDOC AS int) AS NUMDOC, CAST(LINEFACT.IDFACC AS int) as IDFACC,  CABEFACC.BASE,  CABEFACC.BASEMONEDA, CABEFACC.CODALM,  CODPRO,NOMPRO, " +
                " CODMON, CABEFACC.FECHA, LINEFACT.BASE, CODART, DESCLIN,  COSTEADIC, DESC1, IDLIN, LOTE, " +
                " PRECIO, PRCMEDIO, PRCMONEDA, '' AS SITUACION,  LINEFACT.TIPIVA,UNIDADES, '' AS UNISERVIDA , KLS_CODART, CABEFACC.KLS_REL_FRA_C, SERIE  " +
                " FROM CABEFACC INNER JOIN LINEFACT ON CABEFACC.IDFACC = LINEFACT.IDFACC " +
                " WHERE LTRIM(CODART) = '" + codart + "' AND LTRIM(LOTE) = '" + lote + "'";
            return scriptSelect;

        }


        public string selectFactComprasMP(string idDoc, string asociadas=null)
        {
            string scriptSelect = "";
            //idDoc = "2141";

            string anexoAsociadas = string.IsNullOrEmpty(asociadas) ? " LINEFACT.IDFACC = " + idDoc : " LINEFACT.KLS_REL_FRA_C IN ('" + asociadas + "') AND LINEFACT.IDFACC<>" + asociadas; 

            scriptSelect = "SELECT " +
                " CABEFACC.CODPRO, NOMPRO, CABEFACC.FECHA, CAST(NUMDOC AS int) AS NUMDOC, REFERENCIA, CABEFACC.CODMON,CAMBIO,  LINEFACT.IDFACC, LTRIM(ARTICULO.CODART) AS CODART, DESCLIN,  " +
                " UNIDADES, LTRIM(LOTE) AS LOTE, PRCMONEDA, LINEFACT.PRCMEDIO, PRECIO, COSTEADIC , 0.0000 as COSTES_COMUNES,0.0000 as COSTES_ESPECIFICOS , (LINEFACT.PRCMEDIO+COSTEADIC) AS CTU, ARTICULO.PESO*UNIDADES AS COEF_REPARTO, LINEFACT.BASE,ARTICULO.PESO ,  " +
                " KLS_CODART, LINEFACT.KLS_REL_FRA_C, CAST(IDALBC AS int) AS IDALBC, LINEFACT.DESC1 AS DTO, ARTICULO.CAR4 AS ETIQ, 'T' AS CHECKLINE  " +
                " FROM LINEFACT " +
                " INNER JOIN CABEFACC ON CABEFACC.IDFACC = LINEFACT.IDFACC " +
                " INNER JOIN ARTICULO ON LINEFACT.CODART = ARTICULO.CODART " +
                " WHERE  " + anexoAsociadas;
            return scriptSelect;

        }

        public string selectImportesImputar(string idDoc)
        {
            string scriptSelect = "";
            //idDoc = "2141";

            scriptSelect = "SELECT " +
                " LTRIM(KLS_CODART) AS CODART, SUM(BASEMONEDA) AS IMPORTE, CAST(KLS_REL_FRA_C AS INT) AS DOC, 'FRAS' AS ORIGEN, '' AS DOCORIGEN " +
                " FROM dbo.LINEFACT " +
                " WHERE(KLS_REL_FRA_C IN('" + idDoc + "')) AND LINEFACT.IDFACC<>"  + idDoc +
                " GROUP BY KLS_REL_FRA_C, LTRIM(KLS_CODART) " +
                " UNION " +
                " SELECT LTRIM(KLS_CODART) AS CODART, LINEALBA.BASE*-1 AS IMPORTE, CAST(KLS_REL_FAC_C AS INT), 'ALB' AS ORIGEN, CAST(CABEALBC.NUMDOC AS INT) AS DOCORIGEN " +
                " FROM CABEALBC " +
                " INNER JOIN LINEALBA " +
                " ON CABEALBC.IDALBC = LINEALBA.IDALBC " +
                " WHERE FACTURABLE = 'F' AND KLS_REL_FAC_C IS NOT NULL AND (KLS_REL_FAC_C IN(" + idDoc + ")) " +
                " UNION " +
                " SELECT LTRIM(KLS_CODART) AS CODART, SUM(BASE) AS IMPORTE, '" + idDoc.Replace(",0000","") + "' AS DOC, 'MISMAFRA' AS ORIGEN,'' AS DOCORIGEN " + 
                " FROM dbo.LINEFACT " +
                " WHERE  LOTE='' AND LINEFACT.IDFACC = " + idDoc  + " AND LTRIM(CODART) <> 'NI' GROUP BY  LTRIM(KLS_CODART) " +
                " UNION " +
                " SELECT LTRIM(CODART) AS CODART, SUM(UNIDADES*PRECIO*-1) AS IMPORTE, '" + idDoc.Replace(",0000", "") + "' AS DOC, 'PROMOS' AS ORIGEN,'' AS DOCORIGEN " +
                " FROM dbo.LINEFACT " +
                " WHERE  LOTE<>'' AND LINEFACT.IDFACC = " + idDoc + " AND LINEFACT.DESC1=100 GROUP BY  LTRIM(CODART) ";

            return scriptSelect;

        }




    }
}

