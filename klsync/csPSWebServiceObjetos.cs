﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Xml;
using MySql.Data.MySqlClient;
using System.Xml.Serialization;



namespace klsync
{
    class csPSWebServiceObjetos
    {
        public string paisesXML()
        {
            string context = "<prestashop>" +
                                    "<country>" +
                                        "<id></id>" + //si dejamos la ID en blanco podemos crear un nuevo registro
                                        "<id_zone xlink:href=\"http://.../api/zones/1\">1</id_zone>" +
                                        "<id_currency/>" +
                                        "<iso_code>DE</iso_code>" +
                                        "<call_prefix>49</call_prefix>" +
                                        "<active>0</active>" +
                                        "<contains_states>0</contains_states>" +
                                        "<need_identification_number>0</need_identification_number>" +
                                        "<need_zip_code>1</need_zip_code>" +
                                        "<zip_code_format>NNNNN</zip_code_format>" +
                                        "<display_tax_label>1</display_tax_label>" +
                                        "<name>" +
                                            "<language id=\"3\">PIRULETA</language>" +
                                        // "<language id=\"3\" xlink:href=\"http://.../api/languages/6\">PIRULETA</language>" +
                                        "</name>" +
                                    "</country>" +
                                    "</prestashop>";

            return context;

        }

        /// <summary>
        /// Vilardell clientes
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public string clientesXML(DataRow dr)
        {
            csMySqlConnect mysql = new csMySqlConnect();

            string nodosVilardell = "";
            string defaulCustomerGroup = csGlobal.defCustomerGroup;
            string defaultGroups = "";
            string representanteCliente = "";
            string codigoRepresentante = "";
            string paymentMethod = "";
            string paymentMethodId = "";
            string recargoEquivalencia = "";
            string tipoPersonaJuridica = "";
            string deudorID = "";
            string grupoClienteVilardell = "";

            if (csGlobal.conexionDB.Contains("VILARDELL"))
            {
                csVilardell vilardell = new klsync.csVilardell();
                grupoClienteVilardell = vilardell.defaultGroupCustomer(dr["CodigoProvincia"].ToString(), dr["PaisID"].ToString(), dr["DeudorID"].ToString(), dr["ActividadPrincipalId"].ToString());
                nodosVilardell = "<customer_type>DIRECT</customer_type><type>F</type>";
                representanteCliente = "<repre_cli>" + dr["CodigoRepresentante"].ToString() + "</repre_cli>";
                codigoRepresentante = "<cod_repre></cod_repre>";
                defaultGroups = "<group><id>" + grupoClienteVilardell + "</id></group>";
                defaulCustomerGroup = grupoClienteVilardell;
                paymentMethod = "<kls_payment_method>TRANSFERENCIA</kls_payment_method>";
                paymentMethodId = "<kls_payment_method_id>TRANSF</kls_payment_method_id>";
                recargoEquivalencia = "<equivalence_surcharge>" + dr["TratamientoNRE"].ToString() + "</equivalence_surcharge>";
                tipoPersonaJuridica = "<tipo_persona>" + dr["PersonaJuridica"].ToString() + "</tipo_persona>";
            }

            string context = "<prestashop>" +
                                    "<customer>" +
                                        "<id></id>" +
                                        "<id_default_group>" + defaulCustomerGroup + "</id_default_group>" +
                                        "<id_lang>" + mysql.defaultLangPS().ToString() + "</id_lang>" +
                                        "<newsletter_date_add/>" +
                                        "<ip_registration_newsletter/>" +
                                        "<last_passwd_gen/>" +
                                        "<secure_key/>" +
                                        "<deleted/>" +
                                        "<passwd>" + dr["PASSWORD"].ToString() + "</passwd>" +
                                        "<lastname>" + dr["APELLIDO"].ToString() + "</lastname>" +
                                        "<firstname>" + dr["NOMBRE"].ToString().Replace(",", "") + "</firstname>" +
                                        "<email>" + dr["EMAIL"].ToString() + "</email>" +
                                        "<id_gender/>" +
                                        "<birthday/>" +
                                        "<newsletter/>" +
                                        "<optin>0</optin>" +
                                        "<company>" + dr["RAZON"].ToString() + "</company>" +
                                        "<siret></siret>" +
                                        "<ape/>" +
                                        "<outstanding_allow_amount>0</outstanding_allow_amount>" +
                                        "<show_public_prices>0</show_public_prices>" +
                                        "<id_risk>0</id_risk>" +
                                        "<max_payment_days>0</max_payment_days>" +
                                        "<active>1</active>" +
                                        "<note/>" +
                                        "<is_guest>0</is_guest>" +
                                        "<id_shop>1</id_shop>" +
                                        "<id_shop_group>1</id_shop_group>" +
                                        "<date_add/>" +
                                        "<date_upd/>" +
                                        "<kls_a3erp_id>" + dr["CODCLI"].ToString() + "</kls_a3erp_id>" +
                                        recargoEquivalencia +
                                        paymentMethod +
                                        paymentMethodId +
                                        representanteCliente +
                                        codigoRepresentante +
                                        nodosVilardell +
                                        tipoPersonaJuridica +
                                        "<associations>" +
                                            "<groups>" +
                                                defaultGroups +
                                                "<group>" +
                                                    "<id>" + defaulCustomerGroup + "</id>" +
                                                "</group>" +
                                            "</groups>" +
                                        "</associations>" +
                                    "</customer>" +
                                 "</prestashop>";

            csGlobal.globalXML = context;

            return context;
        }

        public string direccionesClientesXML(DataRow dr)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            string context = "";

            if (!csGlobal.conexionDB.Contains("VILARDELL"))
            {
                string codcli = dr["CODPAIS"].ToString();
                csUtilidades.dataRowArray2DataTable(csUtilidades.GetDataSourceFromFile("customers.txt").Select("Column1 = '" + dr["CODCLI"].ToString() + "'")).Rows[0][0].ToString();
                string codPais = dr["CODPAIS"].ToString();
                string codProvi = dr["CODPROVI"].ToString();
                string company = dr["RAZON"].ToString();
                string lastname = dr["LASTNAME"].ToString();
                string firstname = dr["FIRSTNAME"].ToString();
                string address1 = dr["ADDRESS1"].ToString().ToUpper();
                string postcode = dr["POSTCODE"].ToString();
                string city = dr["CITY"].ToString().ToUpper();
                string phone = dr["PHONE"].ToString();

                context = "<prestashop>" +
                                       "<address>" +
                                       "<id></id>" +
                                           "<id_customer>" + codcli + "</id_customer>" + //se asigna sólo si es cliente
                                           "<id_manufacturer/>" + //se asigna sólo si es fabricante
                                           "<id_supplier/>" +
                                           "<id_warehouse/>" +
                                           "<id_country>" + codPais + "</id_country>" +
                                           "<id_state>" + codProvi + "</id_state>" +
                                           "<alias>Mi direccion</alias>" +
                                           //"<alias>" + dr.ItemArray[2].ToString() + "</alias>" +
                                           "<company>" + new System.Xml.Linq.XText(company).ToString() + "</company>" +
                                           "<lastname>" + new System.Xml.Linq.XText(lastname).ToString() + "</lastname>" +
                                           "<firstname>" + new System.Xml.Linq.XText(firstname).ToString() + "</firstname>" +
                                           "<vat_number/>" +
                                           "<address1>" + new System.Xml.Linq.XText(address1).ToString() + "</address1>" +
                                           "<postcode>" + postcode + "</postcode>" +
                                           "<city>" + new System.Xml.Linq.XText(city).ToString() + "</city>" +
                                           "<other/>" +
                                           "<phone>" + phone + "</phone>" +
                                           "<phone_mobile></phone_mobile>" +
                                           "<dni></dni>" +
                                           "<deleted></deleted>" +
                                           "<date_add></date_add>" +
                                           "<date_upd></date_upd>" +
                                       "</address>" +
                                "</prestashop>";

                csGlobal.globalXML = context;
            }
            else
            {
                //string codcli = csUtilidades.dataRowArray2DataTable(csUtilidades.GetDataSourceFromFile("customers.txt").Select("Column1 = '" + dr["CODCLI"].ToString() + "'")).Rows[0][0].ToString();
                string codcli = dr["IdentificacionComoProveedor"].ToString();
                string codPais = "6";
                string codProvi = mysql.obtenerDatoFromQuery("select id_state from ps_state where kls_external_id=" + dr["CodigoProvincia"].ToString());         // dr["CODPROVI"].ToString();
                string company = dr["NOMBRECLIENTE"].ToString();
                string lastname = dr["APELLIDO"].ToString();
                string firstname = dr["NOMBRE"].ToString().Replace(",", "");
                string address1 = dr["NOMBREVIAPUBLICA"].ToString().ToUpper();
                string postcode = dr["CPOSTAL"].ToString();
                string city = dr["NOMBREMUNICIPIO"].ToString().ToUpper();
                string phone = dr["TELEFONO"].ToString();
                string NIF = dr["NIF"].ToString();
                string codigoDireccion = dr["CodigoDireccion"].ToString();

                context = "<prestashop>" +
                                       "<address>" +
                                       "<id></id>" +
                                           "<id_customer>" + codcli + "</id_customer>" + //se asigna sólo si es cliente
                                           "<id_manufacturer/>" + //se asigna sólo si es fabricante
                                           "<id_supplier/>" +
                                           "<id_warehouse/>" +
                                           "<id_country>" + codPais + "</id_country>" +
                                           "<id_state>" + codProvi + "</id_state>" +
                                           "<alias>Mi direccion</alias>" +
                                           //"<alias>" + dr.ItemArray[2].ToString() + "</alias>" +
                                           "<company>" + new System.Xml.Linq.XText(company).ToString() + "</company>" +
                                           "<lastname>" + new System.Xml.Linq.XText(lastname).ToString() + "</lastname>" +
                                           "<firstname>" + new System.Xml.Linq.XText(firstname).ToString() + "</firstname>" +
                                           "<vat_number>" + NIF + "</vat_number>" +
                                           "<address1>" + new System.Xml.Linq.XText(address1).ToString() + "</address1>" +
                                           "<postcode>" + postcode + "</postcode>" +
                                           "<city>" + new System.Xml.Linq.XText(city).ToString() + "</city>" +
                                           "<other/>" +
                                           "<phone>" + phone + "</phone>" +
                                           "<phone_mobile></phone_mobile>" +
                                           "<dni>NIF</dni>" +
                                           "<deleted></deleted>" +
                                           "<date_add></date_add>" +
                                           "<date_upd></date_upd>" +
                                           "<kls_id_dirent>" + codigoDireccion + "</kls_id_dirent>" +
                                       "</address>" +
                                "</prestashop>";

                csGlobal.globalXML = context;

            }
            return context;
        }

        public string clientesXML(string codcli)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csSqlConnects sqlConector = new csSqlConnects();

            string camposConsulta = " CODCLI, RAZON, NIFCLI, KLS_EMAIL_USUARIO, CONTACTO, KLS_NOM_USUARIO, KLS_APELL_USUARIO, KLS_PASS_INICIAL, REGIVA, IMPMINVENTA ";

            //TODO:Revisar que esto funciona correctamente.
            //Si la empresa es Andreutoys cambiamos IMPMINVENTA por K
            if (csGlobal.conexionDB.Contains("ANDREUTOYS"))
            {
                camposConsulta = " CODCLI, RAZON, NIFCLI, KLS_EMAIL_USUARIO, CONTACTO, KLS_NOM_USUARIO, KLS_APELL_USUARIO, KLS_PASS_INICIAL, REGIVA, KLS_PEDIDO_MINIMO";
            }

                DataTable tablaClientes = sqlConector.cargarDatosTabla("CLIENTES",camposConsulta , " CODCLI='" + codcli + "'");

            string empresa = tablaClientes.Rows[0]["RAZON"].ToString().Replace("&", "y").Replace(",", "").Replace(".", "").Replace("(", "").Replace(")", "").Replace("&", ""); // el ampersand no lo admite
            string nombre = tablaClientes.Rows[0]["KLS_NOM_USUARIO"].ToString().Replace(" ", "").Replace(",", "").Replace(".", "").Replace("!", ""); //Solo caracteres de la A-Z
            string apellidos = tablaClientes.Rows[0]["KLS_APELL_USUARIO"].ToString().Replace(",", "").Replace(".", "").Replace("&", "").Replace("!", "").Trim(); //Solo caracteres de la A-Z
            nombre = csUtilidades.removeDigitsFromString(nombre);
            apellidos = csUtilidades.removeDigitsFromString(apellidos);
            empresa = csUtilidades.removeDigitsFromString(empresa);
            string email = tablaClientes.Rows[0]["KLS_EMAIL_USUARIO"].ToString().Replace(" ", "");
            string password = tablaClientes.Rows[0]["KLS_PASS_INICIAL"].ToString(); //No Obligatorio
            string codCli = tablaClientes.Rows[0]["CODCLI"].ToString().Trim();
            string regIVA = tablaClientes.Rows[0]["REGIVA"].ToString().Trim();
            string defaultIdioma = mySqlConnect.defaultLangPS().ToString();
            string formaPagoA3ERP = csGlobal.fpa3.ToUpper() == "SI" ? sqlConector.obtenerFormaPagoA3ERP(codcli) : string.Empty;
            string kls_a3erp_fam_id = "";
            string grupoCliente = csGlobal.defCustomerGroup;
            string importeMinimoVenta = "";


            if (csUtilidades.existeColumnaMySQL(csGlobal.databasePS, "ps_customer", "kls_a3erp_fam_id"))
            {
                kls_a3erp_fam_id = customProductField("kls_a3erp_fam_id", "select famclidesc from __clientes where ltrim(codcli) = '" + codcli.Trim() + "'");
            }

            if (csGlobal.conexionDB.Contains("THAGSON"))
            {
                if (email == "")
                {
                    email = "cliente" + codCli + "@thagson.com";
                    csUtilidades.ejecutarConsulta("update __clientes set E_MAIL_DOCS = '" + email + "', KLS_EMAIL_USUARIO = '" + email + "' WHERE ltrim(codcli) = '" + codCli + "'", false);
                }
            }

            if (csGlobal.conexionDB.Contains("ANDREUTOYS"))
            {
                if (regIVA == "VNAC" || regIVA == "VNOSUJ")
                {
                    grupoCliente = "4";
                }
                else if (regIVA == "VNACR")
                {
                    grupoCliente = "5";
                }
                else
                {
                    grupoCliente = "6";
                }
                importeMinimoVenta = "<purchase_minimum>" + tablaClientes.Rows[0]["KLS_PEDIDO_MINIMO"].ToString().Replace(",", ".") + "</purchase_minimum>";
            }


            string context = "<prestashop>" +
                                    "<customer>" +
                                        "<id></id>" +
                                        "<id_default_group>" + grupoCliente + "</id_default_group>" +
                                        "<id_lang>" + defaultIdioma + "</id_lang>" +
                                        "<newsletter_date_add/>" +
                                        "<ip_registration_newsletter/>" +
                                        "<last_passwd_gen/>" +
                                        "<secure_key/>" +
                                        "<deleted/>" +
                                        "<passwd>" + new System.Xml.Linq.XText(password).ToString() + "</passwd>" +
                                        "<lastname>" + new System.Xml.Linq.XText(apellidos).ToString() + "</lastname>" +
                                        "<firstname>" + new System.Xml.Linq.XText(nombre).ToString() + "</firstname>" +
                                        "<email>" + new System.Xml.Linq.XText(email).ToString() + "</email>" +
                                        "<id_gender/>" +
                                        "<birthday/>" +
                                        "<newsletter/>" +
                                        "<optin>0</optin>" +
                                        "<company>" + new System.Xml.Linq.XText(empresa).ToString() + "</company>" +
                                        "<siret></siret>" + //SI NO ESTÁ ACTIVADO EN LA APLICACIÓN DA ERROR
                                        "<ape/>" +
                                        "<outstanding_allow_amount>0</outstanding_allow_amount>" +
                                        "<show_public_prices>0</show_public_prices>" +
                                        "<id_risk>0</id_risk>" +
                                        "<max_payment_days>0</max_payment_days>" +
                                        "<active>1</active>" +
                                        "<note/>" +
                                        "<is_guest>0</is_guest>" +
                                        "<id_shop>1</id_shop>" +
                                        "<id_shop_group>1</id_shop_group>" +
                                        "<date_add/>" +
                                        "<date_upd/>" +
                                        "<kls_a3erp_id>" + codCli + "</kls_a3erp_id>" +
                                        //"<kls_a3erp_fam_id>3</kls_a3erp_fam_id>" +
                                        //"<kls_payment_method>" + formaPagoA3ERP + "</kls_payment_method>" +
                                        importeMinimoVenta +
                                        "<associations>" +
                                            "<groups>" +
                                                "<group>" +
                                                    "<id>" + grupoCliente + "</id>" +
                                                "</group>" +
                                            "</groups>" +
                                        "</associations>" +
                                        ((kls_a3erp_fam_id != null && !string.IsNullOrEmpty(kls_a3erp_fam_id.ToString())) ? kls_a3erp_fam_id : "") +
                                    "</customer>" +
                                 "</prestashop>";

            csGlobal.globalXML = context;
            return context;
        }

        public string direccionesClientesXML(string codcli, string tipoDir = null, DataRow dr = null)
        {
            string isFiscalAddress = "";    //Para andreutoys
            string idDirentA3ERP = dr["IDDIRENT"].ToString().Replace(",0000", "");
            csSqlConnects sqlConector = new csSqlConnects();

            string codPais = sqlConector.obtenerCampoTabla("SELECT dbo.PAISES.KLS_CODPAIS_PS " +
                                            " FROM PAISES " +
                                            " LEFT JOIN DIRENT ON PAISES.CODPAIS = DIRENT.CODPAIS " +
                                            " RIGHT JOIN __CLIENTES ON __CLIENTES.CODCLI = DIRENT.CODCLI " +
                                            " WHERE LTRIM(__CLIENTES.kls_codcliente) = '" + codcli.Trim() + "'");

            string codProvi = sqlConector.obtenerCampoTabla("SELECT dbo.PROVINCI.KLS_IDPS " +
                                                            " FROM PROVINCI " +
                                                            " LEFT JOIN DIRENT ON PROVINCI.CODPROVI = DIRENT.CODPROVI " +
                                                            " RIGHT JOIN __CLIENTES ON __CLIENTES.CODCLI = DIRENT.CODCLI " +
                                                            " WHERE LTRIM(__CLIENTES.kls_codcliente) = '" + codcli + "'");

            string company = ((dr["NOMCLI"].ToString() != null) ? dr.ItemArray[2].ToString() : "-");
            string lastname = ((dr["KLS_APELL_USUARIO"].ToString() != null) ? dr["KLS_APELL_USUARIO"].ToString() : "-").Replace(".", "").Replace(",", "");
            string firstname = ((dr["KLS_NOM_USUARIO"].ToString() != null) ? dr["KLS_NOM_USUARIO"].ToString() : "-").Replace(".", "").Replace(",", "");
            lastname = csUtilidades.removeDigitsFromString(lastname);
            firstname = csUtilidades.removeDigitsFromString(firstname);
            company = csUtilidades.removeDigitsFromString(company);
            string address1 = ((dr["DIRENT1"].ToString() != null) ? dr["DIRENT1"].ToString().ToUpper() : "-");
            string address2 = ((dr["DIRENT2"].ToString() != null) ? dr["DIRENT2"].ToString().ToUpper() : "-");
            string postcode = dr["DTOENT"].ToString();
            string city = dr["POBENT"].ToString().ToUpper();
            string phone = csUtilidades.cleanPhone(dr["TELENT1"].ToString());
            string nifcli = dr["NIFCLI"].ToString();

            //PARA ANDREUTOYS)

            if (csGlobal.nombreServidor.Contains("ANDREUTOYS"))
            {
                //CONSULTAR CON LLUIS Y FRANCO 14/05/2020
                isFiscalAddress = (dr["KLS_FISCAL"].ToString() == "T") ? "<is_fiscal_address >1</is_fiscal_address>" : "<is_fiscal_address>0</is_fiscal_address>";
                isFiscalAddress = csGlobal.nombreServidor.Contains("ANDREUTOYS") ? isFiscalAddress : "";
                
            }

            string context = "<prestashop>" +
                                   "<address>" +
                                   "<id></id>" +
                                       "<id_customer>" + codcli + "</id_customer>" + //se asigna sólo si es cliente
                                       "<id_manufacturer/>" + //se asigna sólo si es fabricante
                                       "<id_supplier/>" +
                                       "<id_warehouse/>" +
                                       "<id_country>" + codPais + "</id_country>" +
                                       "<id_state>" + codProvi + "</id_state>" +
                                       "<alias>Mi direccion</alias>" +
                                       //"<alias>" + dr.ItemArray[2].ToString() + "</alias>" +
                                       "<company>" + new System.Xml.Linq.XText(company).ToString() + "</company>" +
                                       "<lastname>" + new System.Xml.Linq.XText(lastname).ToString() + "</lastname>" +
                                       "<firstname>" + new System.Xml.Linq.XText(firstname).ToString() + "</firstname>" +
                                       "<vat_number/>" +
                                       "<address1>" + new System.Xml.Linq.XText(address1).ToString() + "</address1>" +
                                       ((tipoDir == "F") ? "<address2>" + new System.Xml.Linq.XText(address2).ToString() + "</address2>" : "<address2/>") +
                                       "<postcode>" + postcode + "</postcode>" +
                                       "<city>" + new System.Xml.Linq.XText(city).ToString() + "</city>" +
                                       "<other/>" +
                                       "<phone>" + phone + "</phone>" +
                                       "<phone_mobile></phone_mobile>" +
                                       "<dni>" + nifcli + "</dni>" +
                                       "<deleted></deleted>" +
                                       "<date_add></date_add>" +
                                       "<date_upd></date_upd>" +
                                       "<kls_id_dirent>" + idDirentA3ERP + "</kls_id_dirent>" +
                                       isFiscalAddress + //Solo para Andreutoys
                                   "</address>" +
                            "</prestashop>";

            csGlobal.globalXML = context;
            return context;


        }

        public string customProductField(string campo, string consulta, bool medida = false)
        {


            string cadena = "<" + campo + "/>";
            csSqlConnects sql = new csSqlConnects();

            string valor = sql.obtenerCampoTabla(consulta).Trim();

            if (campo == "ean13")
            {
                valor = csUtilidades.resolverPadRight(valor, 13).Replace(" ", "");
            }

            if (valor != "" && !medida)
            {
                cadena = "<" + campo + ">" + valor.Replace(".", "") + "</" + campo + ">";
            }
            else
            {
                cadena = "<" + campo + ">" + valor + "</" + campo + ">";
            }

            return cadena;
        }



        public string stockProductos(string id_stock_available, string id_Product, string id_product_attribute, string quantity, string out_of_stock)
        {
            int test = 0;
            if (id_Product == "671")
                test = 1;
            
            string cadenaXML = "";

            cadenaXML = "<prestashop>" +
                        "<stock_available>" +
                        "<id>" + id_stock_available + "</id>" +
                        "<id_product>" + id_Product + "</id_product>" +
                        "<id_product_attribute>" + id_product_attribute + "</id_product_attribute>" +
                        "<id_shop>1</id_shop>" +
                        "<id_shop_group>0</id_shop_group>" +
                        "<quantity>" + quantity + "</quantity>" +
                        "<depends_on_stock>0</depends_on_stock>" +
                        "<out_of_stock>" + out_of_stock + "</out_of_stock>" +
                        "</stock_available>" +
                        "</prestashop>";

            csGlobal.globalXML = cadenaXML;
            return cadenaXML;
        }


        public string articulosXML(string codArt, bool nuevo, string id_Art)
        {
            DataTable articulosToUpdate = new DataTable();
            string id_product = codArt;
            string product_reference = "";
            string id_tax_rules_group = csGlobal.id_tax_group;
            

            csPSWebServiceUpdate psWebServiceUpdate = new csPSWebServiceUpdate();
            csSqlConnects sql = new csSqlConnects();

            csCheckVersion version = new csCheckVersion();
            string state = "";
            string lowStockAlert = "";



            state = version.checkVersión("1.7") ? "<state>1</state>" : state;
            lowStockAlert = version.checkVersión("1.7.4") ? "<low_stock_alert/>" : "";
            product_reference = nuevo ? codArt : product_reference;
            id_product = nuevo ? "<id/>" : "<id>" + id_Art + "</id>";

            //Productos con peso






            //// Codigo de barras ////
            string tieneCodigoBarras = "";
            tieneCodigoBarras = sql.obtenerCampoTabla("SELECT EAN13 FROM ARTICULO WHERE LTRIM(CODART) = '" + codArt + "'").Trim();
            string codigoBarras = (tieneCodigoBarras == "T") ? sql.obtenerCampoTabla("SELECT CODALT FROM ALTERNA WHERE LTRIM(CODART) = '" + codArt + "'") : "";
            string ean13 = "";
            string kls_a3erp_fam_id = "";

            if (csUtilidades.existeColumnaMySQL(csGlobal.databasePS, "ps_product", "kls_a3erp_fam_id"))
            {
                kls_a3erp_fam_id = customProductField("kls_a3erp_fam_id", "select famartdescven from articulo where ltrim(codart) = '" + codArt + "'");
            }

            if (csGlobal.conexionDB.ToUpper().Contains("DISMAY"))
                ean13 = customProductField("ean13", "select param1 from articulo where ltrim(codart) = '" + codArt + "'");
            else if (csGlobal.conexionDB.ToUpper().Contains("ANDREU"))
            {
                ean13 = customProductField("ean13", "select LTRIM(CODALT) from ALTERNA where ltrim(codart) = '" + codArt + "'");
            }
            else
                ean13 = ((codigoBarras != "") ? "<ean13>" + codigoBarras + "</ean13>" : "<ean13/>");
            csIdiomas idiomasPS = new csIdiomas();
            csPrecios preciosPS = new csPrecios();


            string product_widht = "<width/>";
            string product_height = "<height/>";
            string product_depth = "<depth/>";
            string product_weight = "<weight/>";
            string product_minimal_quantity = "<minimal_quantity>1</minimal_quantity>";
            //PROGRAMACIÓN PARA ANDREUTOYES
            if (csGlobal.conexionDB.ToUpper().Contains("ANDREU"))
            {
                product_widht = customProductField("width", "select IF_UD_VTA_ANCHO from articulo where ltrim(codart) = '" + codArt + "'");
                product_height = customProductField("height", "select IF_UD_VTA_ALTO from articulo where ltrim(codart) = '" + codArt + "'");
                product_depth = customProductField("depth", "select IF_UD_VTA_LARGO from articulo where ltrim(codart) = '" + codArt + "'");
                product_weight = customProductField("weight", "select IF_PACKAG_PESO_BRUTO from articulo where ltrim(codart) = '" + codArt + "'");
                product_minimal_quantity = customProductField("minimal_quantity", "select IF_minimvenda from articulo where ltrim(codart) = '" + codArt + "'");

                product_widht = product_widht.Replace(",", ".");
                product_height = product_height.Replace(",", ".");
                product_depth = product_depth.Replace(",", ".");
                product_weight = product_weight.Replace(",", ".");
                product_minimal_quantity = product_minimal_quantity.Replace(",0000", "");
            }

            //290319- Añadido que segun el campo de la configuración de Esync si tiene pesos incluidos que los añada al XML


            if (csGlobal.productosConPeso)
            {
                product_weight = customProductField("weight", "select peso from articulo where ltrim(codart) = '" + codArt + "'");
                product_weight = product_weight.Replace(",", ".");
            }
            if (csGlobal.medidasA3)
            {
                product_widht = customProductField("width", "select PARAM1 from articulo where ltrim(codart) = '" + codArt + "'",true);
                product_depth = customProductField("depth", "select PARAM2 from articulo where ltrim(codart) = '" + codArt + "'", true);
                product_height = customProductField("height", "select PARAM3 from articulo where ltrim(codart) = '" + codArt + "'", true);
                product_widht = product_widht.Replace(",", ".");
                product_height = product_height.Replace(",", ".");
                product_depth = product_depth.Replace(",", ".");
            }


            //Precio a subir a Prestashop
            string precioToPS = preciosPS.productPrice(codArt);

            string context = "<prestashop>" +
                                "<product>" +
                                id_product +
                                //Selección del fabricante o marca
                                //"<id_manufacturer/>" +
                                sql.obtenerMarcaArticulo(codArt) +
                                          "<id_supplier/>" +
                                          "<id_category_default>" + sql.codCategoriaArticulo(codArt).Trim() + "</id_category_default>" +  //Tenemos que asignar una Categoría como mínimo
                                                                                                                                          //"<id_category_default>2</id_category_default>" +  //Tenemos que asignar una Categoría como mínimo
                                          "<new/>" +
                                          //"<cache_default_attribute>0</cache_default_attribute>" +
                                          "<id_default_image/>" +
                                          "<id_default_combination/>" +
                                          "<id_tax_rules_group>"+csGlobal.id_tax_group+"</id_tax_rules_group>" +  //edit 3 jun
                                          "<type/>" +
                                          "<id_shop_default>"+ id_tax_rules_group + "</id_shop_default>" +  //Tienda por defecto
                                          "<reference>" + product_reference + "</reference>" +
                                          "<supplier_reference/>" +
                                          "<location/>" +
                                          product_widht +
                                          product_height +
                                          product_depth +
                                         //probando
                                         product_weight +

                                          "<weight/>" +
                                          "<quantity_discount/>" +
                                          ean13 +
                                          ((csGlobal.conexionDB.Contains("ifigen")) ? customProductField("upc", "select param1 from articulo where ltrim(codart) = '" + codArt + "'") : "<upc/>") +
                                          "<cache_is_pack/>" +
                                          "<cache_has_attachments/>" +
                                          "<is_virtual/>" +
                                          "<on_sale/>" +
                                          "<online_only/>" +
                                          "<ecotax/>" +
                                          product_minimal_quantity +              //Modificado por Andreu Toys Se calcula por función  "<minimal_quantity>1</minimal_quantity>" +
                                          lowStockAlert +
                                          "<price>" + precioToPS + "</price>" +   //Obligatorio asignarle un Precio, OJO CON DECIMALES NO ACEPTA COMAS
                                          "<wholesale_price/>" +
                                          "<unity/>" +
                                          "<unit_price_ratio/>" +
                                          "<additional_shipping_cost/>" +
                                          "<customizable/>" +
                                          "<text_fields/>" +
                                          "<uploadable_files/>" +
                                          "<active>" + sql.obtenerEstadoArticulo(id_Art, codArt) + "</active>" +  // si no indicamos nada por defecto el artículo está inactivo
                                          "<redirect_type/>" +
                                          "<id_product_redirected/>" +
                                          "<available_for_order>1</available_for_order>" +   //Indicamos que lo podamos buscar y ordenar
                                          "<available_date/>" +
                                          "<condition/>" +
                                          "<show_price>1</show_price>" +    //Cuando Marcamos disponible para ordenar también se indica automáticamente que muestre el precio
                                          "<indexed/>" +
                                          "<visibility/>" +
                                          "<advanced_stock_management/>" +
                                          "<date_add/>" +
                                          "<date_upd/>" +
                                          state +
                                          "<meta_description>" +
                                           idiomasPS.idiomasDefinidos() +
                                          "</meta_description>" +
                                          "<meta_keywords>" +
                                          idiomasPS.idiomasDefinidos() +
                                          "</meta_keywords>" +
                                          "<meta_title>" +
                                          idiomasPS.idiomasDefinidos() +
                                          "</meta_title>" +
                                          idiomasPS.linkRewrite(codArt) +
                                          idiomasPS.productName(codArt) +

                                          //ACTUALIZO DESCRIPCIONES SI SE MARCA (description y short_description)
                                          actualizarDescripciones(nuevo, codArt, articulosToUpdate) +

                                          "<available_now>" +
                                          available_later_Description() +
                                          "</available_now>" +

                                          "<available_later>" +
                                           available_now_Description() +
                                          "</available_later>" +
                                          //el nodo de familia da problemas, lo comentamos de momento
                                          ((kls_a3erp_fam_id != "") ? kls_a3erp_fam_id : "") +
                                              "<associations>" +
                                                  //asignación de una o varias categorías
                                                  idiomasPS.articuloCategoria(codArt, articulosToUpdate) +

                                                  "<images>" +
                                                      "<image>" +
                                                        "<id/>" +
                                                      "</image>" +
                                                  "</images>" +
                //"<combinations>" +
                //    "<combinations>" +
                //       "<id/>" +
                //    "</combinations>" +
                //"</combinations>" +
                //"<product_option_values>" +
                //    "<product_options_values>" +
                //      "<id/>" +
                //    "</product_options_values>" +
                //"</product_option_values>" +


                //INCLUSION DE FEATURES EN EL XML
                sql.caracteristicasXMLProductos(codArt, articulosToUpdate) +
                                              //RAUL 24/09/2020 las etiquetas tags estan provocando error 500 en andreutoys
                                              "<tags />" +
                                              //"<tags >" +
                                              //    "<tag>" +
                                              //       "<id/>" +
                                              //    //"<id>" + Regex.Replace(idiomasPS.productName(codArt), @"\D", "") + "</id>" +
                                              //    "</tag>" +
                                              //"</tags>" +

                                              //"<stock_availables>" +
                                              //    "<stock_available>" +
                                              //        "<id/>" +
                                              //        "<id_product_attribute/>" +
                                              //    "</stock_available>" +
                                              //"</stock_availables>" +
                                              //"<accessories>" +
                                              //    "<product>" +
                                              //      "<id/>" +
                                              //    "</product>" +
                                              //"</accessories>" +
                                              "<product_bundle>" +
                                                  "<products>" +
                                                      "<id/>" +
                                                      "<quantity/>" +
                                                  "</products>" +
                                              "</product_bundle>" +
                                              "</associations>" +
                                          "</product>" +
                                   "</prestashop>";


            csGlobal.globalXML = context;
            return context;

        }

        private string actualizarDescripciones(bool nuevo, string codart, DataTable clienToUpdate = null)
        {
            csIdiomas idiomasPS = new csIdiomas();
            string descripciones = "";

            //Si es un artículo nuevo, cojo las descripciones de la ficha de artículo de A3ERP si las hubiera
            if (nuevo)
            {
                descripciones = idiomasPS.description(codart) +
                                idiomasPS.shortDescription(codart);

            }
            //Si no (actualización) recupero las descripciones previas
            else
            {
                descripciones = csGlobal.description + csGlobal.description_short;
            }

            //    descripciones = idiomasPS.description(codart,clienToUpdate) +
            //                        idiomasPS.shortDescription(codart, clienToUpdate);
            //}
            return descripciones;

        }

        public void insertarCategoriaInicio(string descfinal)
        {
            csIdiomas idiomasPS = new csIdiomas();
            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                                         "<prestashop>" +
                                            "<category>" +
                                                "<id/>" +
                                                "<id_parent>0</id_parent>" + // obligatorio
                                                "<level_depth>0</level_depth>" +
                                                "<active>1</active>" + // obligatorio
                                                "<id_shop_default></id_shop_default>" +
                                                "<is_root_category>1</is_root_category>" + // obligatorio
                                                    "<position/>" +
                                                    "<date_add/>" +
                                                    "<date_upd/>" +
                                                     "<name>" +
                                                        idiomasPS.idiomasDefinidos("Inicio") +
                                                    "</name>" +
                                                    "<link_rewrite>" +
                                                        idiomasPS.idiomasDefinidos("Inicio") +
                                                    "</link_rewrite>" +
                                                    "<description>" +
                                                        idiomasPS.idiomasDefinidos("Inicio") +
                                                    "</description>" +
                                                    "<meta_title>" +
                                                        idiomasPS.idiomasDefinidos("Inicio") +
                                                    "</meta_title>" +
                                                    "<meta_keywords>" +
                                                        idiomasPS.idiomasDefinidos("Inicio") +
                                                    "</meta_keywords>" +
                                                    "<kls_a3erp_id>1</kls_a3erp_id>" +
                                                    "<associations>" +
                                                            "<categories>" +
                                                                "<category>" +
                                                                "<id/>" +
                                                                "</category>" +
                                                            "</categories>" +
                                                            "<products>" +
                                                                "<product>" +
                                                                "<id/>" +
                                                                "</product>" +
                                                            "</products>" +
                                                    "</associations>" +
                                            "</category>" +
                                            "</prestashop>";

            try
            {

                string WebService_URL = csGlobal.APIUrl;
                string WebService_LoginName = csGlobal.webServiceKey;
                string WebService_Password = "";

                string RequestURL = csGlobal.APIUrl + "categories" + "/";
                HttpWebRequest peticionWeb = (HttpWebRequest)WebRequest.Create(RequestURL);
                peticionWeb.Method = "POST";
                peticionWeb.ContentType = "text/xml";
                //peticionWeb.ContentType = "application/xml";
                peticionWeb.Credentials = new NetworkCredential(WebService_LoginName, WebService_Password);
                //convierto en bytes el texto del xml
                //para actualizar un producto
                byte[] byteArray = new byte[0];
                byteArray = Encoding.UTF8.GetBytes(xml);
                //Para probar countries
                //byte[] byteArray = Encoding.UTF8.GetBytes(docXML());
                peticionWeb.ContentLength = byteArray.Length;
                Stream dataStream = peticionWeb.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                //Test(peticionWeb); //Activo si quiero ver el tipo de error que me da

                HttpWebResponse respuesta = (HttpWebResponse)peticionWeb.GetResponse();

                if (respuesta.StatusCode == HttpStatusCode.OK)
                {
                    //MessageBox.Show("Datos Actualizados");
                }
                else // revisar - alex
                {
                    if (peticionWeb.HaveResponse) // si hay respuesta
                    {
                        //obtener contenido de la respuesta
                        using (Stream streamContenido = respuesta.GetResponseStream())
                        {
                            XmlDocument documentoXML = new XmlDocument();
                            documentoXML.Load(streamContenido);

                            //string datosInsertados = new StreamReader(streamContenido).ReadToEnd();

                            //XmlDocument xdocInsertado = new XmlDocument();
                            //xdocInsertado.Load(datosIns);
                            ////MessageBox.Show(new StreamReader(streamContenido).ReadToEnd());
                            csPSWebService p = new csPSWebService();
                            p.ObtenerIdInsertado(documentoXML, "categories");
                        }
                    }
                }
                respuesta.Close();

            }

            catch (WebException ps_e)
            {
                //MessageBox.Show(ps_e.Message);
            }
        }

        public bool existeCategoriaInicio()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadCategoriaInicial = " select id_category from ps_category where id_category in (1,2)";

            // MySqlCommand cmd = new MySqlCommand(queryLoadCategoriaInicial, connection);
            //Execute command
            //cmd.ExecuteNonQuery();
            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadCategoriaInicial, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);
            if (tabla != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string categoriaXML(string codfam)
        {
            csSqlConnects sqlConector = new csSqlConnects();
            csIdiomas idiomasPS = new csIdiomas();
            csMySqlConnect mysql = new csMySqlConnect();
            string nombreCategoria = "";
            string id_parent = "2";

            DataTable tablaFamilias = sqlConector.cargarDatosTabla("KLS_CATEGORIAS", " KLS_CODCATEGORIA AS CODFAM,KLS_DESCCATEGORIA AS DESCFAM, KLS_COD_PS AS ID  ", " LTRIM(KLS_CODCATEGORIA)='" + codfam + "'");
            nombreCategoria = tablaFamilias.Rows[0]["DESCFAM"].ToString().ToUpper();
            //Verifico si hay Categorías creadas

            if (mysql.existeTabla("ps_category") == false) // si la tabla esta vacia, se crea el nodo padre
            {
                insertarCategoriaInicio("Inicio");
            }

            //DataTable tablaFamilias = sqlConector.cargarDatosTabla("FAMILIAS", " CODFAM, DESCFAM, ID  ", " LTRIM(CODFAM)='" + codfam + "'");
            //DataTable tablaFamilias = sqlConector.cargarDatosTabla("KLS_CATEGORIAS", " KLS_CODCATEGORIA AS CODFAM,KLS_DESCCATEGORIA AS DESCFAM, KLS_COD_PS AS ID  ", " LTRIM(KLS_CODCATEGORIA)='" + codfam + "'");

            //if (id_parent == "2")   
            //    descfam = tablaFamilias.Rows[0].ItemArray[1].ToString();
            //string id = tablaFamilias.Rows[0].ItemArray[2].ToString();
            //string url = descfam.Replace(" ", "-");

            //name = name.Replace(" ", "-").Replace("&", "&amp;").Replace("<", "-").Replace(">", "-").Replace("\"", "&quot;").Replace("'", "&apos;");
            string descfinal = nombreCategoria.Replace("&", "&amp;").Replace("<", "-").Replace(">", "-").Replace("\"", "&quot;").Replace("'", "&apos;");
            if (codfam == "")
                codfam = mysql.cargarTabla("SELECT MAX(id_category) FROM ps_category").Rows[0].ItemArray[0].ToString();



            // id_parent 0 y is_root_category 1 -> llega pero no a portada

            string context = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                              "<prestashop>" +
                                 "<category>" +
                                             "<id></id>" +
                                             "<id_parent>" + id_parent + "</id_parent>" +//obligatorio
                                             "<active>1</active>" +//obligatorio
                                                                   //"<level_depth>3</level_depth>" +
                                             "<id_shop_default>1</id_shop_default>" +
                                             "<is_root_category>0</is_root_category>" +//obligatorio
                                              "<position/>" +
                                              "<date_add/>" +
                                              "<date_upd/>" +
                                             "<name>" +
                                                  //idiomasPS.idiomasDefinidos(descfinal) +
                                                  idiomasPS.idiomasDefinidos(nombreCategoria) +
                                                "</name>" +
                                                "<link_rewrite>" +
                                                      //idiomasPS.idiomasDefinidos(Regex.Replace(url, @"[^0-9a-zA-Z\s-]+", "").ToLower()) +
                                                      idiomasPS.idiomasDefinidos(Regex.Replace(nombreCategoria, @"[^0-9a-zA-Z\s-]+", "").ToLower()) +
                                                "</link_rewrite>" +
                                                "<description>" +
                                                      idiomasPS.idiomasDefinidos(nombreCategoria) +
                                                "</description>" +
                                                "<meta_title>" +
                                                      idiomasPS.idiomasDefinidos(nombreCategoria) +
                                                "</meta_title>" +
                                                 "<meta_keywords>" +
                                                      idiomasPS.idiomasDefinidos(nombreCategoria) +
                                                "</meta_keywords>" +
                                                //"<kls_a3erp_id>" + codfam.Trim() + "</kls_a3erp_id>" +
                                                "<kls_a3erp_id>" + codfam.Trim() + "</kls_a3erp_id>" +
                                                "<associations>" +
                                                        "<categories>" +
                                                            "<category>" +
                                                            "<id/>" +
                                                            "</category>" +
                                                        "</categories>" +
                                                        "<products>" +
                                                            "<product>" +
                                                            "<id/>" +
                                                            "</product>" +
                                                        "</products>" +
                                                "</associations>" +
                                 "</category>" +
                                 "</prestashop>";

            csGlobal.globalXML = context;
            return context;
        }



        string grupoClientesXML() //funciona
        {
            string contexto = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                                "<prestashop>" +
                                   "<group>" +
                                               "<id></id>" +
                                               "<reduction></reduction>" +
                                               "<price_display_method>1</price_display_method>" +
                                               "<show_prices>1</show_prices>" +
                                               "<name>" +
                                                    "<language id=\"1\">NEW GROUP</language>" +
                                                    "<language id=\"3\">NEW GROUP</language>" +
                                               "</name>" +

                                "</group>" +
                                "</prestashop>";
            return contexto;



        }

        public string actualizarDocumentoPS(string estadoDocumento) //funciona
        {
            string context = "<prestashop>" +
                                     "<order>" +
                                        "<id>12</id>" + //si dejamos la ID en blanco podemos crear un nuevo registro
                                        "<id_address_delivery>5</id_address_delivery>" +
                                        "<id_address_invoice>5</id_address_invoice>" +
                                        "<id_cart>17</id_cart>" +
                                        "<id_currency>1</id_currency>" +
                                        "<id_lang>3</id_lang>" +
                                        "<id_customer>2</id_customer>" +
                                        "<id_carrier>2</id_carrier>" +
                                        "<current_state>6</current_state>" +
                                        "<module>bankwire</module>" +
                                        "<payment>Trasferencia bancaria</payment>" +
                                        "<total_paid>199.73</total_paid>" +
                                        "<total_paid_real>0.00</total_paid_real>" +
                                        "<total_products>158.07</total_products>" +
                                        "<total_products_wt>191.26</total_products_wt>" +
                                        "<conversion_rate>1.000000</conversion_rate>" +
                                        "<associations>" +
                                        "<order_rows>" +
                                            "<order_row>" +
                                            "<id>17</id>" +
                                            "<product_id>1</product_id>" +
                                            "<product_attribute_id>18</product_attribute_id>" +
                                            "<product_quantity>1</product_quantity>" +
                                            "<product_name>iPod Nano - Capacidad : 16GB, Color : Negro</product_name>" +
                                            "<product_price>166.387960</product_price>" +
                                            "<unit_price_tax_incl>191.260000</unit_price_tax_incl>" +
                                            "<unit_price_tax_excl>158.070000</unit_price_tax_excl>" +
                                            "</order_row>" +
                                        "</order_rows>" +
                                        "</associations>" +
                                     "</order>" +
                                     "</prestashop>";

            return context;



        }

        //Función Test para verificar el error que nos da al ejecutar el WebService        

        public DataTable artulosToUpdate { get; set; }

        public DataTable articulosToUpdate { get; set; }


        private string available_now_Description()
        {
            DataTable valores = new DataTable();
            csSqlConnects valorArticulo = new csSqlConnects();
            valores = valorArticulo.obtenerTextoPorDefecto("available_now");
            string traduccion = "";
            if (valores.Rows.Count == 0)
            {
                foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                {
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\"></language>";
                }
            }
            else
            {
                foreach (DataRow fila in valores.Rows)
                {
                    traduccion = traduccion + "<language id=\"" + fila[0].ToString() + "\">" + fila[1].ToString() + "</language>";
                }

            }
            return traduccion;
        }


        private string available_later_Description()
        {
            DataTable valores = new DataTable();
            csSqlConnects valorArticulo = new csSqlConnects();
            valores = valorArticulo.obtenerTextoPorDefecto("available_later");
            string traduccion = "";
            if (valores.Rows.Count == 0)
            {
                foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
                {
                    traduccion = traduccion + "<language id=\"" + valuePair.Key.ToString() + "\"></language>";
                }
            }
            else
            {
                foreach (DataRow fila in valores.Rows)
                {
                    traduccion = traduccion + "<language id=\"" + fila[0].ToString() + "\">" + fila[1].ToString() + "</language>";
                }

            }
            return traduccion;
        }

    }
}
