﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frSeleccionTallaColor : Form
    {
        csMySqlConnect mysql = new csMySqlConnect();
        string id = "", selected_attribute_id = "";
        public frSeleccionTallaColor(string id_product = "", string id_order_detail = "")
        {
            InitializeComponent();

            id = id_order_detail;

            if (id_product != "" && id_order_detail != "")
            {
                cargarDGV(id_product);
            }
        }

        private void cargarDGV(string id_product)
        {
            string idProductAtribute = "", nombreGrupo = "", valorAtributo;
            int idFila = 0;

            csMySqlConnect mysql = new csMySqlConnect();
            string sb = string.Format("select distinct ps_product_attribute.id_product_attribute, ps_attribute_lang.name as atributo, ps_attribute_group_lang.name as grupo " +
             "from ps_product_attribute  " +
             "left join ps_product_attribute_combination " +
             "on ps_product_attribute_combination.id_product_attribute = ps_product_attribute.id_product_attribute " +
             "left join ps_attribute_lang  " +
             "on ps_attribute_lang.id_attribute=ps_product_attribute_combination.id_attribute " +
             "left join ps_attribute " +
             "on ps_attribute.id_attribute=ps_product_attribute_combination.id_attribute " +
             "left join ps_attribute_group_lang " +
             "on ps_attribute_group_lang.id_attribute_group=ps_attribute.id_attribute_group where id_product = {0}", id_product);

            DataTable graella = mysql.cargarTabla(sb);

            DataView view = new DataView(graella);
            DataTable columns = view.ToTable(true, "grupo");

            DataView view2 = new DataView(graella);
            DataTable id_product_attributes = view2.ToTable(true, "id_product_attribute");

            DataTable combinacionesAtributosPS = new DataTable();
            combinacionesAtributosPS.Columns.Add("id_product_attribute");

            // Añado las columnas con los grupos
            foreach (DataRow item in columns.Rows)
            {
                combinacionesAtributosPS.Columns.Add(item["grupo"].ToString());
            }

            // Pateo el distinct de los id_product_attributes
            foreach (DataRow item in id_product_attributes.Rows)
            {
                DataRow fila = combinacionesAtributosPS.NewRow();
                fila[combinacionesAtributosPS.Columns[0].ColumnName] = item["id_product_attribute"].ToString();
                combinacionesAtributosPS.Rows.Add(fila);

                foreach (DataRow item2 in graella.Rows)
                {
                    if (item["id_product_attribute"].ToString() == item2["id_product_attribute"].ToString())
                    {
                        if (!string.IsNullOrEmpty(item2["atributo"].ToString()))
                        {
                            idProductAtribute = combinacionesAtributosPS.Columns[0].ColumnName;
                            nombreGrupo = item2["grupo"].ToString();
                            valorAtributo = item2["atributo"].ToString();

                            combinacionesAtributosPS.Rows[idFila][nombreGrupo] = valorAtributo;
                        }
                        //combinacionesAtributosPS.Rows[idFila][] = nombreGrupo;
                    }
                }

                idFila++;
            }

            csUtilidades.addDataSource(dgv, combinacionesAtributosPS);
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Deseas cambiar el atributo al seleccionado?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    selected_attribute_id = dgv.SelectedRows[0].Cells["id_product_attribute"].Value.ToString();

                    csPrestashop presta = new csPrestashop();
                    presta.cambiarProductAttributeIdOrderDetail(id, selected_attribute_id);

                    "Atributo cambiado con éxito".mb();

                    this.Close();
                }            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }            
        }
    }
}
