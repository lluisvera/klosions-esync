﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klsync.MigracionExt
{
    class csMigracionExt
    {
        public string cargarClientes(string aplicacion)
        {
            string selectQuery = "";
            if (aplicacion == "EUROWIN")
            {
                selectQuery = "SELECT CODIGO, " +
                        " CODIGO - 43000000 AS CODCLI, CIF," +
                        " UPPER(NOMBRE)AS NOMBRE," +
                        " NOMBRE2 AS RAZON," +
                        " UPPER(DIRECCION) AS DIRECCION," +
                        " CODPOST,POBLACION,PROVINCIA," +
                        " SUBSTRING(CODPOST,1,2)ASCODPROVINCIA," +
                        " PAIS,RUTA,VENDEDOR,TARIFA,VALOR_ALB,TIPOFAC,COPIA_FRA,IDIOMA," +
                        " CREDITO,DESCU1,DESCU2,PRONTO,DIAPAG,DIAPAG2,FPAG as FORMA_PAGO,TIPO_IVA," +
                        " RECARGO,COMUNITARI,RETENCION,MODO_RET,TIPO_RET,OBSERVACIO,EMAIL," +
                        " HTTP,REFUNDIR,ENV_CLI,ALBAFRA,CSB,CIA_CRED,OPERACIO,IDIOMA_IMP," +
                        " AGENCIA,BLOQ_VEN,LIM_MON,PORTES,PORTCOMP,VISTA,F_ALTA,OFERTA," +
                        " BLOQ_CLI,FECHA_BAJ,CONTRAPAR,ZONA,POSICION,MENSAJE,PREGVAC,VALPORTES," +
                        " FRAPED,VAL_PUNT,CONTADO,C_ENT,ES_GRUPO,CLIFINAL,PVERDE,TIPCREDIT," +
                        " RECARFIN,RETNOFISC,TPCRETNOFI,LIBRE1,MODRETNOFI,AUTOTIPDOC,EMAIL_F,FRAESI," +
                        " TIPO_CLI,BLOQALBVTA,BLOQPEDVTA,BLOQPREVTA,BLOQDEPVTA,NOCOMUSMS,NOCOMUEMA," +
                        " NOCOMUCAR,FBLOQNOSMS,FBLOQNOEMA,FBLOQNOCAR,NOCOMUOBS,REGCAJA,RECC," +
                        " CLIENTEERP,CTAERP,NOMBRE3ERP,POBLACERP,PROVINERP,TERRITERP,DIRECC2ERP," +
                        " DELEGERP, GUID_EXP" +
                        " FROM clientes ";
            }
            return selectQuery;
        }

        public string cargarBancosClientes(string aplicacion)
        {
            string selectQuery = "";
            if (aplicacion == "EUROWIN")
            {
                selectQuery = "SELECT " +
                    " CODIGO, " +
                    " CLIENTE, " +
                    " (CLIENTE - 43000000) AS CODCLIENTE, " +
                    " BANCO, " +
                    " CTABANCO, " +
                    " CTASUCUR, " +
                    " DIGCON, " +
                    " CTACUENTA, " +
                    " ORDEN, " +
                    " IBAN, " +
                    " CUENTAIBAN, " +
                    " CONCAT(IBAN, CUENTAIBAN) AS CUENTAGLOBAL, " +
                    " SWIFT, " +
                    " (SELECT COUNT(IBAN) FROM banc_cli AS BANC_1 WHERE BANC_1.CLIENTE = banc_cli.CLIENTE) AS NUMCUENTAS, " +
                    " CASE " +
                    " WHEN(SELECT COUNT(IBAN) FROM banc_cli AS BANC_1 WHERE BANC_1.CLIENTE = banc_cli.CLIENTE) > 1 AND ORDEN = 0 THEN 'T' " +
                    " WHEN(SELECT COUNT(IBAN) FROM banc_cli AS BANC_1 WHERE BANC_1.CLIENTE = banc_cli.CLIENTE) = 1 THEN 'T' ELSE 'F'  " +
                    " END AS BANC_DEFAULT " +
                    " FROM " +
                    " NETTAISEUROWIN.dbo.banc_cli " +
                    " WHERE CUENTAIBAN<>''";
            }
            return selectQuery;
        }

        public string cargarContactosCliente(string aplicacion)
        {
            string selectQuery = "";
            if (aplicacion == "EUROWIN")
            {
                selectQuery = "SELECT " +
                   " CLIENTE      ,CLIENTE - 43000000 AS CODCLI, UPPER(PERSONA)AS CONTACTO,CARGO,EMAIL,LINEA,ORDEN,TELEFONO " +
                   " FROM cont_cli ";
            }
            return selectQuery;
        }

        public string cargarDireccionesCliente(string aplicacion)
        {
            string selectQuery = "";
            if (aplicacion == "EUROWIN")
            {
                selectQuery = "SELECT " +
                    " LINEA, CLIENTE, " +
                    " CLIENTE - 43000000 AS CODCLI, " +
                    " NOMBRE, " +
                    " UPPER(DIRECCION)AS DIRECCION, " +
                    " CODPOS,  POBLACION, PROVINCIA, " +
                    " SUBSTRING(CODPOS, 1, 2) AS CODPROVINCIA, " +
                    " HORARIO,  TELEFONO, VISTA, FAX,PAIS " +
                    " FROM env_cli ";
            }
            return selectQuery;
        }

        public string cargarDireccionesProveedores(string aplicacion)
        {
            string selectQuery = "";
            if (aplicacion == "EUROWIN")
            {
                selectQuery = "SELECT " +
                    " LINEA, PROVEEDOR, " +
                    " CLIENTE - 40000000 AS CODPRO, " +
                    " NOMBRE, " +
                    " UPPER(DIRECCION)AS DIRECCION, " +
                    " CODPOS,  POBLACION, PROVINCIA, " +
                    " SUBSTRING(CODPOS, 1, 2) AS CODPROVINCIA, " +
                    " VISTA " +
                    " FROM env_pro ";
            }
            return selectQuery;
        }




    }
}
