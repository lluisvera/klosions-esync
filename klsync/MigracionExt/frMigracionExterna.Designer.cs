﻿namespace klsync.MigracionExt
{
    partial class frMigracionExterna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.lblAplicacion = new System.Windows.Forms.ToolStripLabel();
            this.cboxSelectAplicacion = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cboxFichero = new System.Windows.Forms.ToolStripComboBox();
            this.btnLoadData = new System.Windows.Forms.ToolStripButton();
            this.dgvDataLoad = new System.Windows.Forms.DataGridView();
            this.cMenuPrincipal = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataLoad)).BeginInit();
            this.cMenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblAplicacion,
            this.cboxSelectAplicacion,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.cboxFichero,
            this.btnLoadData});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1240, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // lblAplicacion
            // 
            this.lblAplicacion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAplicacion.Name = "lblAplicacion";
            this.lblAplicacion.Size = new System.Drawing.Size(81, 36);
            this.lblAplicacion.Text = "Aplicación";
            // 
            // cboxSelectAplicacion
            // 
            this.cboxSelectAplicacion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxSelectAplicacion.Items.AddRange(new object[] {
            "EUROWIN"});
            this.cboxSelectAplicacion.Name = "cboxSelectAplicacion";
            this.cboxSelectAplicacion.Size = new System.Drawing.Size(121, 39);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(61, 36);
            this.toolStripLabel1.Text = "Fichero";
            // 
            // cboxFichero
            // 
            this.cboxFichero.DropDownWidth = 200;
            this.cboxFichero.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxFichero.Items.AddRange(new object[] {
            "CLIENTES",
            "CLIENTES_DIRECCIONES",
            "CLIENTES_BANCOS",
            "CLIENTES_CONTACTOS",
            "PROVEEDORES",
            "PROVEEDORES_DIRECCIONES",
            "PROVEEDORES_BANCOS",
            "PROVEEDORES_CONTACTOS",
            "FORMAS_PAGO",
            "ARTICULOS",
            "INVENTARIO",
            "CONTACTOS",
            "FORMAS DE PAGO",
            "TRANSPORTISTAS",
            "AGENTES",
            "CUENTAS CONTABLES",
            "INVENTARIO",
            "ALMACENES",
            "DIRECCIONES_CLI",
            "DIRECCIONES_PRO"});
            this.cboxFichero.Name = "cboxFichero";
            this.cboxFichero.Size = new System.Drawing.Size(121, 39);
            // 
            // btnLoadData
            // 
            this.btnLoadData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadData.Image = global::klsync.Properties.Resources.magnifyying_glass;
            this.btnLoadData.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnLoadData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(137, 36);
            this.btnLoadData.Text = "Cargar Datos";
            this.btnLoadData.Click += new System.EventHandler(this.btnLoadData_Click);
            // 
            // dgvDataLoad
            // 
            this.dgvDataLoad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDataLoad.ContextMenuStrip = this.cMenuPrincipal;
            this.dgvDataLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDataLoad.Location = new System.Drawing.Point(0, 39);
            this.dgvDataLoad.Name = "dgvDataLoad";
            this.dgvDataLoad.Size = new System.Drawing.Size(1240, 530);
            this.dgvDataLoad.TabIndex = 1;
            // 
            // cMenuPrincipal
            // 
            this.cMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarToolStripMenuItem});
            this.cMenuPrincipal.Name = "cMenuPrincipal";
            this.cMenuPrincipal.Size = new System.Drawing.Size(110, 26);
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.copiarToolStripMenuItem.Text = "&Copiar";
            this.copiarToolStripMenuItem.Click += new System.EventHandler(this.copiarToolStripMenuItem_Click);
            // 
            // frMigracionExterna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 569);
            this.Controls.Add(this.dgvDataLoad);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frMigracionExterna";
            this.Text = "frMigracionExterna";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataLoad)).EndInit();
            this.cMenuPrincipal.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblAplicacion;
        private System.Windows.Forms.ToolStripComboBox cboxSelectAplicacion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cboxFichero;
        private System.Windows.Forms.ToolStripButton btnLoadData;
        private System.Windows.Forms.DataGridView dgvDataLoad;
        private System.Windows.Forms.ContextMenuStrip cMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
    }
}