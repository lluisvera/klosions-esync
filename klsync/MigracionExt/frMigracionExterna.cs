﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;

namespace klsync.MigracionExt
{
    public partial class frMigracionExterna : Form
    {
        string aplicacionToMigrate = "";
        csMigracionExt migration = new csMigracionExt();

        private static frMigracionExterna m_FormDefInstance;
        public static frMigracionExterna DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frMigracionExterna();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frMigracionExterna()
        {
            InitializeComponent();
        }

        public void CopyToClipboardWithHeaders(DataGridView _dgv)
        {
            //Copy to clipboard
            _dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            DataObject dataObj = _dgv.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            aplicacionToMigrate = cboxSelectAplicacion.SelectedItem.ToString();
            switch (cboxFichero.Text)
            { 
            case "CLIENTES":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarClientes(aplicacionToMigrate), false);
                break;
            case "CLIENTES_DIRECCIONES":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarDireccionesCliente(aplicacionToMigrate), false);
                break;
            case "CLIENTES_CONTACTOS":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarContactosCliente(aplicacionToMigrate), false);
                break;
            case "CLIENTES_BANCOS":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarBancosClientes(aplicacionToMigrate), false);
                break;
            case "PROVEEDORES":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarClientes(aplicacionToMigrate), false);
                break;
            case "PROVEEDORES_DIRECCIONES":
                csUtilidades.cargarDGV(dgvDataLoad, migration.cargarDireccionesProveedores(aplicacionToMigrate), false);
                break;
            }
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyToClipboardWithHeaders(dgvDataLoad);
        }

        //CLIENTES_CONTACTOS
        //PROVEEDORES
        //PROVEEDORES_DIRECCIONES
        //PROVEEDORES_BANCOS
        //PROVEEDORES_CONTACTOS
        //FORMAS_PAGO
        //ARTICULOS
        //INVENTARIO
        //CONTACTOS
        //FORMAS DE PAGO
        //TRANSPORTISTAS
        //AGENTES
        //CUENTAS CONTABLES
        //INVENTARIO
        //ALMACENES
        //DIRECCIONES_PRO




    }
}
