﻿namespace klsync
{
    partial class frInformePedidosPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripInformePedidos = new System.Windows.Forms.ToolStrip();
            this.dgvInformePedidos = new System.Windows.Forms.DataGridView();
            this.statusStripInformeDocumentos = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelEspacio = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelNumeroFilas = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInformePedidos)).BeginInit();
            this.statusStripInformeDocumentos.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripInformePedidos
            // 
            this.toolStripInformePedidos.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripInformePedidos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripInformePedidos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripInformePedidos.Location = new System.Drawing.Point(0, 0);
            this.toolStripInformePedidos.Name = "toolStripInformePedidos";
            this.toolStripInformePedidos.Size = new System.Drawing.Size(766, 25);
            this.toolStripInformePedidos.Stretch = true;
            this.toolStripInformePedidos.TabIndex = 0;
            this.toolStripInformePedidos.Text = "toolStrip1";
            // 
            // dgvInformePedidos
            // 
            this.dgvInformePedidos.AllowUserToAddRows = false;
            this.dgvInformePedidos.AllowUserToDeleteRows = false;
            this.dgvInformePedidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInformePedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInformePedidos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvInformePedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInformePedidos.Location = new System.Drawing.Point(12, 76);
            this.dgvInformePedidos.Name = "dgvInformePedidos";
            this.dgvInformePedidos.ReadOnly = true;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInformePedidos.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvInformePedidos.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvInformePedidos.Size = new System.Drawing.Size(742, 389);
            this.dgvInformePedidos.TabIndex = 1;
            // 
            // statusStripInformeDocumentos
            // 
            this.statusStripInformeDocumentos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelEspacio,
            this.toolStripStatusLabelNumeroFilas});
            this.statusStripInformeDocumentos.Location = new System.Drawing.Point(0, 455);
            this.statusStripInformeDocumentos.Name = "statusStripInformeDocumentos";
            this.statusStripInformeDocumentos.Size = new System.Drawing.Size(766, 22);
            this.statusStripInformeDocumentos.TabIndex = 2;
            this.statusStripInformeDocumentos.Text = "statusStrip1";
            // 
            // toolStripStatusLabelEspacio
            // 
            this.toolStripStatusLabelEspacio.Name = "toolStripStatusLabelEspacio";
            this.toolStripStatusLabelEspacio.Size = new System.Drawing.Size(751, 17);
            this.toolStripStatusLabelEspacio.Spring = true;
            // 
            // toolStripStatusLabelNumeroFilas
            // 
            this.toolStripStatusLabelNumeroFilas.Name = "toolStripStatusLabelNumeroFilas";
            this.toolStripStatusLabelNumeroFilas.Size = new System.Drawing.Size(0, 17);
            // 
            // frInformePedidosPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(766, 477);
            this.Controls.Add(this.statusStripInformeDocumentos);
            this.Controls.Add(this.dgvInformePedidos);
            this.Controls.Add(this.toolStripInformePedidos);
            this.Name = "frInformePedidosPS";
            this.Text = "Informe de Pedidos de Prestashop";
            ((System.ComponentModel.ISupportInitialize)(this.dgvInformePedidos)).EndInit();
            this.statusStripInformeDocumentos.ResumeLayout(false);
            this.statusStripInformeDocumentos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripInformePedidos;
        private System.Windows.Forms.DataGridView dgvInformePedidos;
        private System.Windows.Forms.StatusStrip statusStripInformeDocumentos;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelEspacio;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelNumeroFilas;
    }
}