﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace klsync
{
    public partial class frGraellaTyC : Form
    {
        private bool check = false;
        private static frGraellaTyC m_FormDefInstance;
        public static frGraellaTyC DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frGraellaTyC();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        private csSqlConnects sql = new csSqlConnects();
        csTallasYColoresV2 tyc2 = new csTallasYColoresV2();

        /// <summary>
        /// Inicializador de componentes
        /// </summary>
        public frGraellaTyC()
        {
            InitializeComponent();

            dgvGraella.ReadOnly = false;
            dgvGraella.Enabled = true;
            dgvArticulosSimilaresTyC.ReadOnly = false;
            dgvArticulosSimilaresTyC.Enabled = true;
        }

        /// <summary>
        /// Botón de cargar artículos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonBusquedaTyC_Click(object sender, EventArgs e)
        {
            cargarDatosArticulos();
        }

        /// <summary>
        /// Cargamos todos los artículos que tengan talla y que no sean null
        /// </summary>
        private void cargarDatosArticulos()
        {
            string consulta = "SELECT DISTINCT LTRIM(CODART) AS CODIGO, KLS_ID_SHOP AS IDSHOP, DESCART AS DESCRIPCION, CODFAMTALLAV AS V,  CODFAMTALLAH AS H " +
                               "FROM ARTICULO " +
                               "WHERE (CODFAMTALLAH IS NOT NULL OR CODFAMTALLAV IS NOT NULL) AND TALLAS = 'T' and kls_id_shop is not null ";


            csUtilidades.addDataSource(dgvArticulosTyC, sql.cargarDatosTablaA3(consulta));
        }

        /// <summary>
        /// Cargamos el grid de articulos con los mismos códigos de talla
        /// </summary>
        /// <param name="atributoH"></param>
        /// <param name="atributoV"></param>
        private void cargarDatosArticulosSimilares(string atributoH, string atributoV)
        {
            string consulta = "SELECT LTRIM(CODART) AS CODIGO, KLS_ID_SHOP AS IDSHOP, DESCART AS DESCRIPCION, CODFAMTALLAV AS V,  CODFAMTALLAH AS H " +
                               "FROM ARTICULO " +
                               "WHERE CODFAMTALLAH = '" + atributoH + "' and CODFAMTALLAV = '" + atributoV + "'";

            dgvArticulosSimilaresTyC.DataSource = null;
            dgvArticulosSimilaresTyC.Rows.Clear();
            dgvArticulosSimilaresTyC.Columns.Clear();

            if (dgvArticulosSimilaresTyC.Rows.Count > 0)
                dgvArticulosSimilaresTyC.Columns.RemoveAt(0);

            dgvArticulosSimilaresTyC.Columns.Add(checkBoxDataGridView(""));

            dgvArticulosSimilaresTyC.Columns[dgvArticulosSimilaresTyC.Columns.GetLastColumn(DataGridViewElementStates.Displayed, DataGridViewElementStates.None).Index].DisplayIndex = 0;
            csUtilidades.addDataSource(dgvArticulosSimilaresTyC, sql.cargarDatosTablaA3(consulta));
            dgvArticulosSimilaresTyC.Columns[0].Width = 50;
        }

        /// <summary>
        /// Checkeamos la columna entera clicando al header
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvArticulosTyC_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow row in dgvArticulosTyC.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                chk.Value = check;
            }

            check = !check;
        }



        /// <summary>
        /// Cargamos la graella con los checks
        /// </summary>
        private void cargarGridTyC()
        {
            try
            {
                if (dgvArticulosTyC.SelectedRows.Count == 1)
                {
                    dgvGraella.Columns.Clear();
                    string H = dgvArticulosTyC.SelectedRows[0].Cells["H"].Value.ToString();
                    string V = dgvArticulosTyC.SelectedRows[0].Cells["V"].Value.ToString();

                    DataTable filasH = filasTallas(H);
                    DataTable filasV = filasTallas(V);



                    if (filasH.Rows.Count > 0)
                    {
                        if (dgvGraella.Columns.Count > 0)
                            dgvGraella.Columns.RemoveAt(0);

                        for (int i = 0; i < filasH.Rows.Count; i++)
                        {

                            dgvGraella.ReadOnly = false;
                            dgvGraella.Enabled = true;

                            dgvGraella.Columns.Add(checkBoxDataGridView(filasH.Rows[i].ItemArray[0].ToString()));
                        }
                    }
                    else
                    {
                        dgvGraella.Columns.Add(checkBoxDataGridView(""));
                    }

                    if (filasV.Rows.Count > 0)
                    {
                        foreach (DataRow row in filasV.Rows)
                        {
                            DataGridViewRow dgvrow = new DataGridViewRow();

                            dgvrow.HeaderCell.Value = row.ItemArray[0].ToString();
                            dgvGraella.Rows.Add(dgvrow);
                        }
                    }
                    else
                    {
                        dgvGraella.Rows.Add();
                    }

                    dgvGraella.RowHeadersWidth = 75;
                }
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Método que añade un checkboxcolumn al datagrid y lo devuelve
        /// </summary>
        /// <param name="nombreColumna"></param>
        /// <returns></returns>
        private DataGridViewCheckBoxColumn checkBoxDataGridView(string nombreColumna)
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();

            checkColumn.Name = nombreColumna;
            checkColumn.HeaderText = nombreColumna;
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10;

            return checkColumn;
        }

        /// <summary>
        /// Obtenemos los codigos de la talla segun la familia (horizontal o vertical)
        /// </summary>
        /// <param name="familia">Horizontal o Vertical</param>
        /// <returns>DataTable</returns>
        private DataTable filasTallas(string familia)
        {
            //Cargo las combinaciones de cada articulo y devuelve aquellas combinaciones a crear
            DataTable filas = new DataTable();
            string queryFilas = "select codtalla from tallas where ltrim(codfamtalla) = '" + familia.Trim() + "'";

            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();

            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter adaptA3 = new SqlDataAdapter(queryFilas, dataConnection);

            adaptA3.Fill(filas);

            return filas;
        }

        /// <summary>
        /// Evento al clicar encima de un artículo (carga la graella y los similares)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvArticulosTyC_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            actualizarGraella();
        }

        /// <summary>
        /// Iteramos el datagridview y la base de datos para saber cuales estan marcados
        /// </summary>
        private void comprobarCombinacionesBD()
        {

            string codart = dgvArticulosTyC.SelectedRows[0].Cells[0].Value.ToString().Trim();
            string atributoH = "", atributoV = "";
            string columnHeader = "", rowHeader = "";

            DataTable checks = sql.cargarDatosTablaA3("SELECT ATRIBUTOH, ATRIBUTOV FROM KLS_COMBINACIONES_VISIBLES WHERE LTRIM(CODART) = '" + codart + "'");

            if (checks != null && checks.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvGraella.Rows)
                {
                    if (row.HeaderCell.Value == null)
                        rowHeader = "";
                    else
                        rowHeader = row.HeaderCell.Value.ToString().Trim();

                    for (int i = 0; i < dgvGraella.Columns.Count; i++)
                    {
                        columnHeader = dgvGraella.Columns[i].Name.Trim();

                        for (int ii = 0; ii < checks.Rows.Count; ii++)
                        {
                            atributoH = checks.Rows[ii]["ATRIBUTOH"].ToString();
                            atributoV = checks.Rows[ii]["ATRIBUTOV"].ToString();
                            if (atributoH == columnHeader && atributoV == rowHeader)
                            {
                                row.Cells[i].Value = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Búsqueda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonBuscaArt_Click(object sender, EventArgs e)
        {
            lupa();
        }

        private void lupa(string texto = "")
        {
            frDialogInput busqueda = new frDialogInput("Nombre, descripción o talla del artículo: ", texto);
            if (busqueda.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string resultadoBusqueda = csGlobal.comboFormValue;
                string consulta = " SELECT DISTINCT LTRIM(CODART) AS CODIGO, KLS_ID_SHOP AS IDSHOP, DESCART AS DESCRIPCION, CODFAMTALLAV AS V,  CODFAMTALLAH AS H " +
                                  " FROM ARTICULO " +
                                  " WHERE " +
                                  " (descart like '%" + resultadoBusqueda + "%' or ltrim(codart) like '%" + resultadoBusqueda + "%' or ltrim(CODFAMTALLAV) = '" + resultadoBusqueda + "' or ltrim(CODFAMTALLAH) = '" + resultadoBusqueda + "' ) " +
                                  " AND TALLAS = 'T'";

                csUtilidades.addDataSource(dgvArticulosTyC, sql.cargarDatosTablaA3(consulta));
            }
        }

        /// <summary>
        /// Comprobamos los checks y luego insertamos. Previamente hacemos delete con un where del codart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void insertarCombinaciones(string codart = "")
        {
            string atributoV = "";
            string atributoH = "";
            string cabeceraColumna = "";
            string cabeceraLinea = "";
            DataTable dt = new DataTable();

            //Borro todas las combinaciones existentes
            string consulta_delete = "DELETE FROM KLS_COMBINACIONES_VISIBLES WHERE LTRIM(CODART) = '" + codart + "'";
            csUtilidades.ejecutarConsulta(consulta_delete,false);

            //Repaso todo el grid e inserto las combinaciones
            for (int i = 0; i < dgvGraella.Rows.Count; i++)
            {
                for (int ii = 0; ii < dgvGraella.Columns.Count; ii++)
                {
                    cabeceraColumna = dgvGraella.Columns[dgvGraella.Rows[i].Cells[ii].ColumnIndex].HeaderText.ToString();
                    if (dgvGraella.Rows[i].HeaderCell.Value != null)
                    {
                        cabeceraLinea = dgvGraella.Rows[i].HeaderCell.Value.ToString().Trim();
                    }
                    else
                    {
                        cabeceraLinea = "";
                    }
                    bool isCellChecked = false;
                    if (dgvGraella.Rows[i].Cells[ii].Value != null)
                        isCellChecked = (bool)dgvGraella.Rows[i].Cells[ii].Value;

                    if (isCellChecked == true)
                    {
                        //Verifico si sólo hay un atributo vertical u horizontal
                        if (dgvGraella.Columns[dgvGraella.Rows[i].Cells[ii].ColumnIndex].HeaderText != null && dgvGraella.Columns[dgvGraella.Rows[i].Cells[ii].ColumnIndex].HeaderText.ToString() != "")
                            atributoH = "'" + dgvGraella.Columns[dgvGraella.Rows[i].Cells[ii].ColumnIndex].HeaderText.ToString().Trim() + "'";
                        else
                            atributoH = "NULL";


                        if (atributoH == "")
                            atributoH = "NULL";

                        //Verifico si sólo hay un atributo vertical u horizontal
                        if (dgvGraella.Rows[i].HeaderCell.Value != null)
                            atributoV = "'" + dgvGraella.Rows[i].HeaderCell.Value.ToString().Trim() + "'";
                        else
                            atributoV = "NULL";

                        string consulta_insert = "INSERT INTO KLS_COMBINACIONES_VISIBLES (CODART, ATRIBUTOH, ATRIBUTOV) VALUES ('" + codart + "'," + atributoH + "," + atributoV + ")";


                        csUtilidades.ejecutarConsulta(consulta_insert, false);
                    }
                    else
                    {
                        //MessageBox.Show("Is Not Chiked");
                    }
                }
            }


        }

        /// <summary>
        /// Botón de sincronizar tallas y colores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void tsSincronizarTyC_Click(object sender, EventArgs e)
        {
            sincronizarTyCArticulo();
        }

        public void sincronizarTyCArticulo()
        {
            DataGridView dgv;

            if (tabControlGraella.SelectedIndex == 0)
            {
                dgv = dgvArticulosTyC;
            }
            else
            {
                dgv = dgvCruzados;
            }

            if (dgv.SelectedRows.Count > 0)
            {
                dgv.EndEdit();
                dgv.Refresh();

                foreach (DataGridViewRow fila in dgv.SelectedRows)
                {
                    string idProduct = fila.Cells["IDSHOP"].Value.ToString().Trim();
                    string codart = fila.Cells["CODIGO"].Value.ToString().Trim();

                    if (dgvArticulosTyC.SelectedRows.Count == 1)
                    {
                        insertarCombinaciones(codart);
                    }

                    if (csUtilidades.tieneCodigoExternoInformado(idProduct))
                    {
                        tyc2.inicializarTallasYColores(idProduct);
                    }
                }
                MessageBox.Show("Proceso Finalizado");
            }
        }

        /// <summary>
        /// Boton de actualizar stock //  deprecated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsActualizarStock_Click(object sender, EventArgs e)
        {
            tyc2.actualizarStockPS(tyc2.tablaActualizarStockPS());
        }

        //Cargo toda la información referente al artículo seleccionado
        private void dgvArticulosTyC_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string H = dgvArticulosTyC.SelectedRows[0].Cells["H"].Value.ToString();
            string V = dgvArticulosTyC.SelectedRows[0].Cells["V"].Value.ToString();

            cargarGridTyC();

            dgvGraella.Refresh();

            comprobarCombinacionesBD();

            cargarDatosArticulosSimilares(H, V);
        }

        

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            // INSERT TIME CONSUMING OPERATIONS HERE
            // THAT DON'T REPORT PROGRESS
            tyc2.inicializarTallasYColores("");
            csStocks stock = new csStocks();
            stock.stock();
        }


        /// <summary>
        /// Si tiene el idshop informado sincronizamos tallas y colores
        /// </summary>
        /// 
        private void sincronizarTallasProductosSeleccionados()
        {
            bool idEnTienda = false;
            string idshop = "";
            string idProduct = dgvArticulosTyC.SelectedRows[0].Cells["IDSHOP"].Value.ToString().Trim();
            bool isCellChecked = false;

            // si el articulo original tiene idshop informado, actualizamos
            if (csUtilidades.tieneCodigoExternoInformado(idProduct))
            {
                tyc2.inicializarTallasYColores(idProduct);
            }

            // pateamos los similares y si alguno tiene marcado, lo mismo
            for (int ii = 0; ii < dgvArticulosSimilaresTyC.Rows.Count; ii++)
            {
                isCellChecked = false;
                if (dgvArticulosSimilaresTyC.Rows[ii].Cells[0].Value != null)
                    isCellChecked = (bool)dgvArticulosSimilaresTyC.Rows[ii].Cells[0].Value;

                if (isCellChecked == true)
                {
                    idshop = dgvArticulosSimilaresTyC.Rows[ii].Cells[2].Value.ToString();
                    // comprobar que tiene el idshop, si es que si, idEnTienda = true
                    if (csUtilidades.tieneCodigoExternoInformado(idshop))
                    {
                        tyc2.inicializarTallasYColores(idshop);
                    }
                }


            }


        }

        /// <summary>
        /// Insertar los seleccionados en los artículos similares que hayan marcado
        /// </summary>
        private void insertarCombinacionesEnSimilares()
        {
            dgvArticulosSimilaresTyC.EndEdit();
            dgvArticulosSimilaresTyC.Refresh();

            for (int ii = 0; ii < dgvArticulosSimilaresTyC.Rows.Count; ii++)
            {
                bool isCellChecked = false;
                if (dgvArticulosSimilaresTyC.Rows[ii].Cells[0].Value != null)
                    isCellChecked = (bool)dgvArticulosSimilaresTyC.Rows[ii].Cells[0].Value;

                if (isCellChecked == true)
                {
                    string codart = dgvArticulosSimilaresTyC.Rows[ii].Cells[1].Value.ToString();
                    insertarCombinaciones(codart);
                }
            }
        }

        /// <summary>
        /// Para ir por los artículos con las flechas 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvArticulosTyC_KeyUp(object sender, KeyEventArgs e)
        {
            actualizarGraella();
        }

        /// <summary>
        /// Funcion que carga la graella, comprueba los checks y carga los datos de los similares
        /// </summary>
        private void actualizarGraella()
        {
            string H = "", V = "";
            if (dgvArticulosTyC.Rows.Count > 0)
            {
                H = dgvArticulosTyC.SelectedRows[0].Cells["H"].Value.ToString();
                V = dgvArticulosTyC.SelectedRows[0].Cells["V"].Value.ToString();
            }

            cargarGridTyC();

            dgvGraella.Refresh();

            if (dgvArticulosTyC.Rows.Count > 0)
            {
                comprobarCombinacionesBD();
                cargarDatosArticulosSimilares(H, V);
            }
        }

        /// <summary>
        /// Seleccionar la columna entera de checkboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        private void dgvGraella_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            foreach (DataGridViewRow row in dgvGraella.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[e.ColumnIndex];
                chk.Value = check;
            }

            check = !check;
        }




        private void dgvGraella_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dgvGraella.EndEdit();
            foreach (DataGridViewRow row in dgvGraella.Rows)
            {
                if (row.Index == e.RowIndex)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)cell;

                        chk.Value = check;
                    }
                }
            }

            check = !check;
        }

        /// <summary>
        /// Opcion para seleccionar todos los elementos de la graella
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void seleccionarTodosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvGraella.Columns.Count; i++)
            {
                foreach (DataGridViewRow dgv in dgvGraella.Rows)
                {
                    dgv.Cells[i].Value = check;
                }
            }

            seleccionarTodosToolStripMenuItem.Checked = check;
            check = !check;
        }

        private void tsbGenerarTodasCombinaciones_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tycV2 = new csTallasYColoresV2();
            string preguntaSeguridad = "Se van a marcar todas las combinaciones de Tallas y Colores posibles, y se sobreescribirá los valores existentes" + "\n" +
                "¿Está seguro?";
            DialogResult result = MessageBox.Show(preguntaSeguridad, "Actualizar Datos", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                tycV2.generarTodasLasCombinacionesA3();
                MessageBox.Show("Proceso terminado");
            }
        }

        private void toolStripButtonReseteoTyC_Click(object sender, EventArgs e)
        {
            // Reseteo la combinacion en ps antes de cambiarla

            if (dgvArticulosTyC.SelectedRows.Count == 1)
            {
                if (MessageBox.Show("Vas a resetear las Tallas y colores del producto: " + dgvArticulosTyC.SelectedRows[0].Cells["DESCRIPCION"].Value.ToString(), "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    csTallasYColoresV2 TyC = new csTallasYColoresV2();
                    TyC.resetearTyCSlow(dgvArticulosTyC.SelectedRows[0].Cells["IDSHOP"].Value.ToString());

                    MessageBox.Show("Producto reseteado con exito");
                }
                else
                {
                    MessageBox.Show("Operacion cancelada");
                }
            }
            else
            {
                MessageBox.Show("Selecciona solo una fila para resetear el producto");
            }

        }

        private void toolStripButtonCruzados_Click(object sender, EventArgs e)
        {
            cargar_cruzados();
        }

        private void cargar_cruzados()
        {
            csMySqlConnect mysql = new csMySqlConnect();
            string consulta_a3 = "SELECT LTRIM(dbo.ARTICULO.CODART) as CODIGO, dbo.ARTICULO.KLS_ID_SHOP as IDSHOP, COUNT(dbo.ARTICULO.CODART) AS COMBINACIONES, (select count(codart) from KLS_COMBINACIONES_VISIBLES where LTRIM(dbo.ARTICULO.CODART) = KLS_COMBINACIONES_VISIBLES.CODART) as A3 FROM dbo.ARTICULO LEFT OUTER JOIN dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAV = TALLAS_1.CODFAMTALLA LEFT OUTER JOIN dbo.TALLAS ON dbo.ARTICULO.CODFAMTALLAH = dbo.TALLAS.CODFAMTALLA GROUP BY dbo.ARTICULO.CODART, dbo.ARTICULO.KLS_ID_SHOP HAVING (dbo.ARTICULO.KLS_ID_SHOP > 0)";
            DataTable a3 = sql.cargarDatosTablaA3(consulta_a3);
            //string consulta_ps = "select id_product, COUNT(id_product_attribute) as combinacionesPS from ps_product_attribute group by id_product";
            string consulta_ps = "select ps_product_attribute.id_product, COUNT(id_product_attribute) as combinacionesPS from ps_product_attribute left join ps_product on ps_product.id_product = ps_product_attribute.id_product where active = 1 group by ps_product_attribute.id_product";
            DataTable ps = mysql.cargarTabla(consulta_ps);

            a3.Columns.Add("PS");

            foreach (DataRow dra3 in a3.Rows)
            {
                string kls_id_shop = dra3["IDSHOP"].ToString();
                foreach (DataRow drps in ps.Rows)
                {
                    string id_product = drps["id_product"].ToString();
                    string combinaciones_ps = drps["combinacionesPS"].ToString();

                    if (kls_id_shop == id_product)
                    {
                        dra3["PS"] = combinaciones_ps;
                        break;
                    }
                }
            }

            DataTable final = csUtilidades.dataRowArray2DataTable(a3.Select("PS <> A3"));

            csUtilidades.addDataSource(dgvCruzados, final);
            //pintarDeRojo(dgvCruzados);
        }

        private void pintarDeRojo(DataGridView dgvCruzados)
        {
            if (dgvCruzados.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvCruzados.Rows)
                {
                    if (row.Cells["A3"].Value.ToString() != row.Cells["PS"].Value.ToString())
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                    }
                }
            }
        }

        private void dgvCruzados_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string codart = dgvCruzados.SelectedRows[0].Cells["CODIGO"].Value.ToString();
            tabControlGraella.SelectedIndex = 0;
            lupa(codart);
        }

        private void dgvCruzados_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string codart = dgvCruzados.SelectedRows[0].Cells["CODIGO"].Value.ToString();
            tabControlGraella.SelectedIndex = 0;
            lupa(codart);
        }

        private void dgvArticulosTyC_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            dgvGraella.ClearSelection();
            dgvGraella.Rows[0].Selected = true;
        }

        private void sincronizarSeleccionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sincronizarTyCArticulo();
            cargar_cruzados();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                string idAtributo = "";
                string codart = "";
                string[] TyC = new string[4];
                csMySqlConnect mySqlConnect = new klsync.csMySqlConnect();
                string query = "select ps_product_attribute.id_product,ps_product.reference,ps_product_attribute.id_product_attribute,ps_product_lang.name,ps_product_lang.id_lang " +
                                    " from ps_product_attribute " +
                                    " inner join ps_product_lang on ps_product_lang.id_product = ps_product_attribute.id_product " +
                                    " inner join ps_product on ps_product.id_product = ps_product_attribute.id_product " +
                                    " where ps_product_lang.id_lang = 1";

                DataTable ps = mySqlConnect.cargarTabla(query);

                //Añado las columnas para tallas y colores
                ps.Columns.Add("CodFamTallaH");
                ps.Columns.Add("CodFamTallaV");
                ps.Columns.Add("CodTallaH");
                ps.Columns.Add("CodTallaV");

                foreach (DataRow fila in ps.Rows)
                {
                    idAtributo = fila["id_product_attribute"].ToString();
                    codart = fila["reference"].ToString();
                    if (idAtributo != "0")
                    {
                        TyC = tallayColor(idAtributo, codart);
                        fila["CodFamTallaH"] = TyC[0];
                        fila["CodFamTallaV"] = TyC[1];
                        fila["CodTallaH"] = TyC[2];
                        fila["CodTallaV"] = TyC[3];

                    }

                }


                csUtilidades.addDataSource(dgvCruzados, ps);

            }
            catch (Exception ex)
            {

            }

        }

        private string[] tallayColor(string atributoArticulo, string codart = null)
        {
            string atributosPS = "";
            int cont = 0;
            string[] arrayAtributos = new string[4];
            csSqlConnects sqlconector = new csSqlConnects();
            DataRow[] filasAtributos;

            if (csGlobal.modoTallasYColores.ToUpper() == "DISMAY")
            {
                arrayAtributos[0] = ""; //CODFAMTALLAH
                arrayAtributos[1] = ""; //CODFAMTALLAV
                arrayAtributos[2] = ""; //CODIGO COLOR NO ID COLOR
                arrayAtributos[3] = ""; //CODIGO TALLA NO ID TALLA
            }
            else if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
            {
                atributosPS = obtenerTallasYColoresDeLineaPedido(atributoArticulo);

                if (atributosPS != "")
                {
                    DataTable tablaTyC = sqlconector.cargarCombinacionTallasYColores(atributosPS);

                    foreach (DataRow fila in tablaTyC.Rows)
                    {
                        if (fila["VERTICAL"].ToString() == "T")
                        {
                            arrayAtributos[1] = fila["CODFAMTALLA"].ToString(); //CODFAMTALLAV
                            arrayAtributos[3] = fila["CODTALLA"].ToString(); //CODIGO TALLA NO ID TALLA
                        }
                        else
                        {
                            arrayAtributos[0] = fila["CODFAMTALLA"].ToString(); //CODFAMTALLAH
                            arrayAtributos[2] = fila["CODTALLA"].ToString(); //CODIGO COLOR NO ID COLOR
                        }
                    }
                }
            }
            else if (csGlobal.conexionDB != "dismayNuevo")
            {
                //DataTable tablaTyC = sqlconector.cargarDatosTabla("KLS_ESYNC_TALLASYCOLORES");
                DataTable tablaTyC = sqlconector.cargarCombinacionTallasColores();
                string filtroTabla = "ID='" + atributoArticulo + "'";

                filasAtributos = tablaTyC.Select(filtroTabla);
                if (filasAtributos.Length > 0)
                {
                    arrayAtributos[0] = filasAtributos[0][2].ToString(); //CODFAMTALLAH
                    arrayAtributos[1] = filasAtributos[0][3].ToString(); //CODFAMTALLAV
                    arrayAtributos[2] = filasAtributos[0][4].ToString(); //CODIGO COLOR NO ID COLOR
                    arrayAtributos[3] = filasAtributos[0][5].ToString(); //CODIGO TALLA NO ID TALLA
                }
            }
            else //PARA DISMAY A DEPRECAR
            {
                arrayAtributos[0] = ""; //CODFAMTALLAH
                arrayAtributos[1] = ""; //CODFAMTALLAV
                arrayAtributos[2] = ""; //CODIGO COLOR NO ID COLOR
                arrayAtributos[3] = ""; //CODIGO TALLA NO ID TALLA
            }




            //Función creada para indusnow para detectar aquellos artículos que tienen una de sus dos familias de tallas "monoatributo"
            //En prestashop no se han creado estas tallas, pero se debe volver a poner la talla por defecto al bajarla a A3
            if (csGlobal.activarMonotallas == "Y")
            {
                string query = "select ltrim(codart) as codart,kls_id_shop, " +
                               " CODFAMTALLAH,(SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=CODFAMTALLAH) AS TALLASH, " +
                               " CASE WHEN (SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAH)=1 THEN (SELECT LTRIM(CODTALLA) FROM TALLAS WHERE CODFAMTALLA=CODFAMTALLAH) ELSE NULL END AS TALLAH_MONO, " +
                               " CODFAMTALLAV,(SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAV) AS TALLASV, " +
                               " CASE WHEN (SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAV)=1 THEN (SELECT LTRIM(CODTALLA) FROM TALLAS WHERE CODFAMTALLA=CODFAMTALLAV) ELSE NULL END AS TALLAV_MONO " +
                               " from articulo where kls_id_shop=" + codart;
                DataTable tablaArticulosTallasMonotalla = sqlconector.cargarDatosScriptEnTabla(query, "");

                if (tablaArticulosTallasMonotalla.Rows[0]["TALLASH"].ToString() == "1")
                {
                    arrayAtributos[0] = tablaArticulosTallasMonotalla.Rows[0]["CODFAMTALLAH"].ToString().Trim();
                    arrayAtributos[2] = tablaArticulosTallasMonotalla.Rows[0]["TALLAH_MONO"].ToString().Trim();
                }

                if (tablaArticulosTallasMonotalla.Rows[0]["TALLASV"].ToString() == "1")
                {
                    arrayAtributos[1] = tablaArticulosTallasMonotalla.Rows[0]["CODFAMTALLAV"].ToString().Trim();
                    arrayAtributos[3] = tablaArticulosTallasMonotalla.Rows[0]["TALLAV_MONO"].ToString().Trim();
                }

            }

            return arrayAtributos;
        }

        private string obtenerTallasYColoresDeLineaPedido(string id_product_attribute)
        {
            string[] arrayAtributos = new string[2];
            string filtroAtributos = "";
            int filas = 0;
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadCombinacionesPS = "";
            queryLoadCombinacionesPS = " select id_attribute,id_product_attribute from ps_product_attribute_combination where id_product_attribute=" + id_product_attribute;

            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadCombinacionesPS, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);
            //close connection
            connection.Close();
            filas = tabla.Rows.Count;
            if (filas > 0)
            {
                //Redimensiono el array al númeor de filas/atributos que tenga el artículo
                Array.Resize(ref arrayAtributos, filas);
                for (int i = 0; i < filas; i++)
                {
                    arrayAtributos[i] = tabla.Rows[i]["id_attribute"].ToString();

                    if (i > 0)
                    {

                        filtroAtributos = filtroAtributos + "," + arrayAtributos[i];
                    }
                    else
                    {
                        filtroAtributos = filtroAtributos + arrayAtributos[i];

                    }
                }
                filtroAtributos = "(" + filtroAtributos + ")";
            }

            return filtroAtributos;
        }


        //public void generarTodasLasCombinacionesA3(string nuevaTalla=null , string familiaNuevaTalla=null)
        //{
        //    DataTable combinacionesA3 = new DataTable();
        //    csTallasYColoresV2 TyC = new csTallasYColoresV2();
        //    csSqlConnects sqlConnect = new csSqlConnects();
        //    if (nuevaTalla != null)
        //    {
        //        sqlConnect.borrarDatosSqlTabla("KLS_COMBINACIONES_VISIBLES");
        //    }
        //    combinacionesA3 = TyC.cargarCombinacionesArticulosA3("", true);
        //    sqlConnect.insertarTodasCombinacionesAtributosA3(combinacionesA3);
        //    MessageBox.Show("Proceso terminado");
        //}
    }
}
