﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace klsync
{
    class csVilardell
    {

        public string defaultGroupCustomer(string provincia, string pais, string deudorID, string tipoCliente)
        {
            string grupo = "";
            string codProvincia = provincia.Substring(0, 2);

            bool esClienteDirecto = clienteDirecto(deudorID); 

            if (pais == "1")
            {
                //Cataluña
                if (codProvincia == "08" || codProvincia == "08" || codProvincia == "08" || codProvincia == "08")
                {
                    if (esClienteDirecto)
                    grupo = esClienteDirecto? "14":"15";   //  Directo Supos
                }
                //comunitat Valenciana, Balears
                else if (codProvincia == "46" || codProvincia == "03" || codProvincia == "12" || codProvincia == "07")
                {
                    grupo = esClienteDirecto ? "16" : "17";   //  Directo Supos
                }
                // Madrid i Aragón.
                else if (codProvincia == "28" || codProvincia == "50" || codProvincia == "44" || codProvincia == "22")
                {
                    grupo = esClienteDirecto ? "18" : "19";  //  Directo Supos
                }
                //e.TARIFA 5: Andalucía, Extremadura, Castilla la Mancha, Castilla León, Murcia, Galicia, Asturias, Navarra, País Vasco, Cantabria i La Rioja.
                else
                {
                    grupo = esClienteDirecto ? "21" : "22";   // 
                }
            }
            else //Andorra
            {
                // Andorra(a Andorra només servim per transfer)
                grupo = "20";   //  Directo Supos
            }
            //Mayoristas
            if (tipoCliente == "3")
            {
                grupo = "11";
            }
            //Parafarmacias
            if (tipoCliente == "31")
            {
                grupo = esClienteDirecto ? "12" : "13";
            }



            //1)      TIPUS DE CLIENT: Cal identificar quin tipus de client es(farmàcia, parafarmàcia, majorista...)
            //2)      DIRECTE O TRANSFER: Cal identificar-lo com a directe o transfer en base a si tenim o no les dades bancaries donades d’alta.
            //3)      TARIFA: Depenent de la CCAA on estigui el client se li assignarà una tarifa o un altre. La relació és la següent.
            //a.TARIFA 1: Catalunya
            //b.TARIFA 2: Comunitat Valenciana, Balears.
            //c.TARIFA 3: Madrid i Aragón.
            //d.TARIFA 4 : Andorra(a Andorra només servim per transfer)
            //e.TARIFA 5: Andalucía, Extremadura, Castilla la Mancha, Castilla León, Murcia, Galicia, Asturias, Navarra, País Vasco, Cantabria i La Rioja.

            return grupo;

                
        }

        private bool clienteDirecto(string deudorId)
        {
            csSqlConnects sqlConnect = new klsync.csSqlConnects();
            string script = "SELECT FormatoLibreCta FROM ps_Direccion  where DeudorID = " + deudorId + " and TipoDireccion = 1";

            if (sqlConnect.consultaExiste(script))
            {
                return true;
            }
            else
            {
                return false;
            }


        }



        /// <summary>
        /// Función que me devuelve el select de clientes
        /// </summary>
        /// <param name="dtClientes"></param>
        /// <returns></returns>
        public string selectClientes(DataTable dtClientes = null, bool sincronizados=false)
        {
            string scriptWhereCustomer = "";
            string scriptWhereSync = " AND IDENTIFICACIONCOMOPROVEEDOR <> '' ";

            if (sincronizados)
            {
                scriptWhereSync = "";
            }

            if (dtClientes.hasRows())
            {
                scriptWhereCustomer = " AND DBO.V_CLIENTE.DEUDORID IN (" + csUtilidades.concatenarValoresQueryFromDataTable(dtClientes, "idDeudor") + ")";

            }


            string query = "SELECT " +
                               " CLIENTEID, V_CLIENTE.COMPANIAID, CODIGOCLIENTE, V_CLIENTE.NOMBRE,V_CLIENTE.REPRESENTANTEID, V_CLIENTE.FECHAULTACT, V_CLIENTE.DEUDORID, " +
                               " IDENTIFICACIONCOMOPROVEEDOR, TRATAMIENTONRE, CODIGODIRECCION, ACTIVIDADPRINCIPALID " +
                               " FROM " +
                               " DBO.V_CLIENTE INNER JOIN " +
                               " DBO.PS_DIRECCION ON DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND  " +
                               " DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID AND DBO.V_CLIENTE.DEUDORID = DBO.PS_DIRECCION.DEUDORID " +
                               " WHERE " +
                               " (V_CLIENTE.COMPANIAID = 2  " + scriptWhereCustomer + "  AND  TipoDireccion=0) ";

            return query;


        }
    }
}
