﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace klsync
{
    class csStocks
    {
        csSqlConnects sql = new csSqlConnects();
        csMySqlConnect mysql = new csMySqlConnect();

        public DataTable consultaStockA3()
        {
            csSqlScripts sqlScript = new csSqlScripts();
            csSqlConnects sqlConnect = new csSqlConnects();
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter Adapt = new SqlDataAdapter(sqlScript.selectStocks(), dataConnection);
            DataTable tabla = new DataTable();
            Adapt.Fill(tabla);
            return tabla;
        }

        public void insertarArticulos()
        {
            csMySqlConnect mySql = new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();
            try
            {
                sqlConnect.borrarDatosSqlTabla("KLS_PRESTA_PRODUCTS");
            }
            catch (Exception ex)
            {
                sqlConnect.cerrarConexion();
            }

            sqlConnect.insertarProductosEnA3(mySql.importarProductos());
        }

        public void sincronizarStockNoTallas()
        {
            try
            {
                //string PS_id_product = "";
                string PS_reference = "";
                string A3_codart = "";
                string A3_kls_id_shop = "";
                string out_of_stock = "";
                string unidades_a3 = "";
                bool update = false;

                DataTable ArticulosPS = new DataTable();
                DataTable ArticulosA3 = new DataTable();
                DataTable StockInsertar = new DataTable();
                DataTable StockActualizar = new DataTable();
                StockInsertar.Columns.Add("id_product");
                StockInsertar.Columns.Add("quantity");
                StockInsertar.Columns.Add("out_of_stock");
                StockActualizar.Columns.Add("id_product");
                StockActualizar.Columns.Add("quantity");
                StockActualizar.Columns.Add("out_of_stock");
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                ArticulosPS = mySqlConnect.cargarTabla("Select ps_product.id_product as ID_PRODUCT,ps_product.reference as REFERENCE,ps_stock_available.quantity from ps_product Inner Join " +
                        " ps_stock_available On ps_product.id_product = ps_stock_available.id_product where ps_stock_available.id_product_attribute=0");
                csSqlConnects sqlConnect = new csSqlConnects();
                csSqlScripts sqlScript = new csSqlScripts();
                ArticulosA3 = sqlConnect.cargarDatosTablaA3(sqlScript.cargarStockArticulosA3());

                for (int i = 0; i < ArticulosA3.Rows.Count; i++)
                {
                    update = false;
                    A3_codart = ArticulosA3.Rows[i]["REFERENCE"].ToString();
                    A3_kls_id_shop = ArticulosA3.Rows[i]["KLS_ID_SHOP"].ToString();
                    out_of_stock = ArticulosA3.Rows[i]["KLS_PERMITIRCOMPRAS"].ToString();
                    unidades_a3 = ArticulosA3.Rows[i]["UNIDADES"].ToString();

                    for (int ii = 0; ii < ArticulosPS.Rows.Count; ii++)
                    {
                        PS_reference = ArticulosPS.Rows[ii]["REFERENCE"].ToString();
                        if (PS_reference == A3_codart)
                        {
                            //update    
                            update = true;
                            DataRow filaUpd = StockActualizar.NewRow();
                            filaUpd["id_product"] = ArticulosPS.Rows[ii]["ID_PRODUCT"].ToString();
                            //filaUpd["quantity"] = PS_reference;
                            filaUpd["quantity"] = unidades_a3;
                            filaUpd["out_of_stock"] = out_of_stock;
                            StockActualizar.Rows.Add(filaUpd);
                            break;
                        }
                    }
                    //insert
                    if (!update)
                    {
                        DataRow filaIns = StockInsertar.NewRow();
                        filaIns["id_product"] = A3_kls_id_shop;
                        filaIns["quantity"] = ArticulosA3.Rows[i]["UNIDADES"].ToString();
                        filaIns["out_of_stock"] = out_of_stock;
                        StockInsertar.Rows.Add(filaIns);
                    }
                }
                //Actualizar Stock

                //mySqlConnect.borrar("ps_stock_available");
                for (int i = 0; i < StockActualizar.Rows.Count; i++)
                {
                    mySqlConnect.actualizaStock("update ps_stock_available set quantity=" +
                        StockActualizar.Rows[i]["quantity"].ToString() + ", out_of_stock = " + out_of_stock + " where id_product=" + StockActualizar.Rows[i]["id_product"].ToString());
                }

                //foreach (DataRow filaToUpdate in StockActualizar.Rows)
                //{
                //    mySqlConnect.actualizaStock("update ps_stock_available set quantity=" + filaToUpdate.ItemArray[1].ToString() + " where id_product=" + filaToUpdate.ItemArray[0].ToString());
                //}

                //Insertar Nuevo Stock
                string fields = "id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock";
                string values = "";

                foreach (DataRow filaToInsert in StockInsertar.Rows)
                {
                    // el ultimo valor, que es el de out_of_stock tiene que estar a 2
                    // si está a 0 deniega los pedidos cuando no hay existencias
                    values = filaToInsert.ItemArray[0].ToString() + ",0,1,0," + filaToInsert.ItemArray[1].ToString() + ",0," + out_of_stock;
                    mySqlConnect.actualizaStock("insert into ps_stock_available (" + fields + ")  values (" + values + ")");
                }
            }
            catch (Exception ex) { Program.guardarErrorFichero(ex.Message); }
        }

        // DEPRECATED 17/01/17
        public void actualizar_stock()
        {
            string fields = "id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock";
            string id_product_ps = "", id_product_a3 = "", query = "", unidades = "";
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();

            DataTable ps = mysql.cargarTabla("select * from ps_stock_available where id_product_attribute = 0");

            string almacen = "'" + csGlobal.almacenA3.Replace(",", "','") + "'";

            DataTable a3 = sql.cargarDatosTablaA3("SELECT LTRIM(dbo.STOCKALM.CODART) AS REFERENCE," +
                                                       " SUM(dbo.STOCKALM.UNIDADES) AS UNIDADES, " +
                                                       " dbo.ARTICULO.TALLAS, " +
                                                       " dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                                                       " KLS_ID_SHOP, " +
                                                       " CASE " +
                                                           " WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'SI' THEN 1 " +
                                                           " WHEN ARTICULO.KLS_PERMITIRCOMPRAS = 'NO' THEN 0 " +
                                                           " ELSE 2 " +
                                                       " END AS KLS_PERMITIRCOMPRAS " +
                                                " FROM dbo.STOCKALM " +
                                                " INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART " +
                                                " WHERE dbo.STOCKALM.CODFAMTALLAH IS NULL " +
                                                " AND dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T' " +
                                                " AND dbo.STOCKALM.CODFAMTALLAV IS NULL " +
                                                " AND dbo.ARTICULO.KLS_ARTICULO_TIENDA = 'T' " +
                                                //" AND dbo.STOCKALM.UNIDADES > 0 " +
                                                " AND LTRIM(dbo.STOCKALM.CODALM) IN (" + almacen + ") " +
                                                " AND KLS_ID_SHOP IS NOT NULL and KLS_ID_SHOP <> 0 " +
                                                " GROUP BY LTRIM(dbo.STOCKALM.CODART), " +
                                                         " dbo.ARTICULO.TALLAS, " +
                                                         " dbo.ARTICULO.KLS_ARTICULO_TIENDA, " +
                                                         " KLS_ID_SHOP, " +
                                                         " KLS_PERMITIRCOMPRAS");

            if (a3.Rows.Count > 0)
            {
                int contador = 0;
                foreach (DataRow filaA3 in a3.Rows)
                {
                    id_product_a3 = filaA3["KLS_ID_SHOP"].ToString();
                    unidades = filaA3["UNIDADES"].ToString();
                    foreach (DataRow filaPS in ps.Rows)
                    {
                        id_product_ps = filaPS["id_product"].ToString();

                        if (id_product_a3 == id_product_ps)
                        {
                            query = "update ps_stock_available set quantity = " + unidades + " where id_product = " + id_product_ps + " and id_product_attribute = 0";
                            csUtilidades.ejecutarConsulta(query, true);
                            break;
                        }
                    }

                }
            }

            //tsslbInfo.Text = "Stock actualizado";

        }

        // FUNCIONA
        private DataTable stockSinTallas()
        {
            // Cargo en un dt la referencia y el stock de ps
            csSqlScripts scripts = new csSqlScripts();

            DataTable ps = mysql.cargarTabla("select ps_stock_available.id_stock_available, ps_product.reference,  ps_product.id_product, ps_stock_available.id_product_attribute, ps_stock_available.quantity as STOCK_PS from ps_product left join ps_stock_available on ps_product.id_product = ps_stock_available.id_product " + ((csGlobal.tieneTallas) ? "" : " where ps_stock_available.id_product_attribute = 0 and reference <> ''"));
            DataTable a3 = sql.cargarDatosTablaA3(scripts.consultaStockA3SIPSSI(csGlobal.almacenA3, "", null));
            ps.Columns.Add("STOCK_A3");
            ps.Columns.Add("CHECK");

            foreach (DataRow dr_a3 in a3.Rows)
            {
                string reference_a3 = dr_a3["REFERENCE"].ToString();
                string stock_a3 = dr_a3["STOCK_A3"].ToString();

                foreach (DataRow dr_ps in ps.Rows)
                {
                    string reference_ps = dr_ps["reference"].ToString();
                    string stock_ps = dr_ps["STOCK_PS"].ToString();

                    if (reference_a3 == reference_ps)
                    {
                        if (stock_a3 != stock_ps)
                        {
                            dr_ps["CHECK"] = "T";
                        }

                        dr_ps["STOCK_A3"] = stock_a3;
                        break;
                    }
                }
            }

            return csUtilidades.dataRowArray2DataTable(ps.Select("CHECK = 'T'"));
        }

        // FUNCIONA
        private DataTable stockConTallasTotalA3SIPSSI(string idProduct = null)
        {
            bool articuloEnAlmacen = false;
            bool stockSincronizado = false;
            csSqlScripts scripts = new csSqlScripts();


            string queryPS = "";
            string queryA3 = "";

            if (idProduct != null)
            {

                queryPS = "select ps_product.id_product, ps_stock_available.id_stock_available, ps_product.reference, SUM(ps_stock_available.quantity) as STOCK_PS, ps_product.out_of_stock as PERMITIR_COMPRAS_A3" +
                " from ps_product inner join ps_stock_available on ps_product.id_product = ps_stock_available.id_product " +
                " where ps_stock_available.id_product_attribute = 0 and ps_product.id_product=" + idProduct +
                " group by ps_product.reference, ps_product.id_product order by ps_product.id_product";
                queryA3 = scripts.consultaStockA3SIPSSI(csGlobal.almacenA3, "", idProduct);
            }
            else
            {
                queryPS = "select ps_product.id_product, ps_stock_available.id_stock_available, ps_product.reference, SUM(ps_stock_available.quantity) as STOCK_PS, ps_product.out_of_stock as PERMITIR_COMPRAS_A3" +
                    " from ps_product inner join ps_stock_available on ps_product.id_product = ps_stock_available.id_product where ps_stock_available.id_product_attribute = 0 group by ps_product.reference, ps_product.id_product order by ps_product.id_product";
                queryA3 = scripts.consultaStockA3SIPSSI(csGlobal.almacenA3, "", null);

            }

            DataTable ps = mysql.cargarTabla(queryPS);
            DataTable a3 = sql.cargarDatosTablaA3(queryA3);
            ps.Columns.Add("STOCK_A3");
            //ps.Columns.Add("PERMITIR_COMPRAS_A3");
            ps.Columns.Add("CHECK");

            if (ps.Rows.Count > 0 && a3.Rows.Count > 0)
            {
                foreach (DataRow dr_ps in ps.Rows)
                {
                    string reference_ps = dr_ps["reference"].ToString();
                    string stock_ps = dr_ps["STOCK_PS"].ToString();
                    string permitir_compras_p = dr_ps["PERMITIR_COMPRAS_A3"].ToString();

                    foreach (DataRow dr_a3 in a3.Rows)
                    {
                        string reference_a3 = dr_a3["REFERENCE"].ToString();
                        string stock_a3 = dr_a3["STOCK_A3"].ToString();
                        string permitir_compras_a3 = dr_a3["KLS_PERMITIRCOMPRAS"].ToString();

                        if (reference_a3 == reference_ps)
                        {

                            if (stock_ps != stock_a3)
                            {
                                dr_ps["CHECK"] = "T";
                                dr_ps["PERMITIR_COMPRAS_A3"] = permitir_compras_a3;
                                dr_ps["STOCK_A3"] = stock_a3;
                                articuloEnAlmacen = true;
                                break;
                            }
                            else
                            {
                                stockSincronizado = true;
                            }
                        }
                    }
                    if (!articuloEnAlmacen && !stockSincronizado)
                    {

                        if (stock_ps != "0")
                        {
                            //dr_ps["CHECK"] = "T";
                            //dr_ps["STOCK_A3"] = "0";
                            //dr_ps["PERMITIR_COMPRAS_A3"] = "2";

                        }
                    }
                }
            }

            return ps;
        }

        private DataTable stockConTallasTotalA3NOPSSI(string idProduct = null, string codart = null)
        {
            csSqlScripts scripts = new csSqlScripts();
            string anexoProductPS = "";
            string anexoCodart = "";
            string anexoCodartPadre = "";
            if (!string.IsNullOrEmpty(idProduct))
            {
                anexoProductPS = " and ps_product.id_product=" + idProduct;
            }
            if (!string.IsNullOrEmpty(codart))
            {
                anexoCodart = " AND (dbo.ARTICULO.KLS_ID_SHOP =" + idProduct + ") ";
                anexoCodartPadre = " AND (ARTICULO_1.KLS_ID_SHOP =" + idProduct + ") ";
            }



            //Compruebo el stock del atributo 0 para verificar que tiene stock informado
            string queryStockPS = "select " +
                                    " ps_product.id_product, ps_stock_available.id_stock_available, ps_product.reference, ps_stock_available.quantity as STOCK_PS " +
                                    " from ps_product inner join ps_stock_available on ps_product.id_product = ps_stock_available.id_product  " +
                                    " where id_product_attribute=0 " + anexoProductPS +
                                    " order by ps_product.id_product";

            DataTable ps = mysql.cargarTabla(queryStockPS);
            DataTable a3 = sql.cargarDatosTablaA3(scripts.consultaStockA3NOPSSI(csGlobal.almacenA3, anexoCodart, anexoCodartPadre, false));
            ps.Columns.Add("STOCK_A3");
            ps.Columns.Add("PERMITIR_COMPRAS_A3");
            ps.Columns.Add("CHECK");

            if (ps.Rows.Count > 0 && a3.Rows.Count > 0)
            {
                foreach (DataRow dr_ps in ps.Rows)
                {
                    string reference_ps = dr_ps["reference"].ToString();
                    string stock_ps = dr_ps["STOCK_PS"].ToString();
                    string permitir_compras_p = dr_ps["STOCK_PS"].ToString();

                    foreach (DataRow dr_a3 in a3.Rows)
                    {
                        string reference_a3 = dr_a3["CODART"].ToString().Trim();
                        string stock_a3 = dr_a3["STOCK_A3"].ToString();
                        string permitir_compras_a3 = dr_a3["KLS_PERMITIRCOMPRAS"].ToString();

                        if (reference_a3 == reference_ps)
                        {
                            if (stock_ps != stock_a3)
                            {
                                dr_ps["CHECK"] = "T";
                                dr_ps["PERMITIR_COMPRAS_A3"] = permitir_compras_a3;
                                dr_ps["STOCK_A3"] = stock_a3;
                                break;
                            }
                        }
                    }
                }
            }

            return csUtilidades.dataRowArray2DataTable(ps.Select("CHECK = 'T'"));
            //return ps;
        }

        private bool concuerdanAtributos(string A1A3, string A2A3, string A1PS, string A2PS)
        {
            if (A1PS == A1A3 && A2PS == A2A3)
                return true;
            if (A1PS == A2A3 && A2PS == A1A3)
                return true;
            //añadido 18-10-2017 casuistica Hebo, cuando en A3 sólo hay un atributo
            if (A1A3 == A1PS && A2A3 == "" && A1A3 == A2PS)
                return true;
            if (A2A3 == A1PS && A1A3 == "" && A2A3 == A2PS)
                return true;




            //fin modificación

            return false;
        }

        // FUNCIONA
        // CARGO TODAS LAS COMBINACIONES TANTO DE A3 COMO DE PRESTASHOP
        // SE COMPARAN Y SI HAY DIFERENCIAS, SE MARCAN
        private DataTable stockConTallasA3SIPSSI(string idProduct = null)
        {
            bool existeCombinacion = false;
            csSqlScripts scripts = new csSqlScripts();
            // Datatable de los productos con tallas
            DataTable ps = mysql.cargarTabla(scripts.stockPSTallasA3SIPSI(idProduct));
            DataTable a3 = sql.cargarDatosTablaA3(scripts.stockA3TallasA3SIPSSI(idProduct));
            ps.Columns.Add("STOCK_A3");
            ps.Columns.Add("PERMITIR_COMPRAS_A3");
            ps.Columns.Add("CHECK");

            csUtilidades.log("STOCK TALLAS");
            foreach (DataRow dr_ps in ps.Rows)
            {
                existeCombinacion = false;
                string A1PS = dr_ps["idAtributo1"].ToString();
                string A2PS = dr_ps["idAtributo2"].ToString();
                string reference_ps = dr_ps["reference"].ToString();

                string stock_ps = dr_ps["STOCK_PS"].ToString();
                string permitir_compras_ps = dr_ps["permitir_compras_PS"].ToString();

                foreach (DataRow dr_a3 in a3.Rows)
                {
                    string reference_a3 = dr_a3["CODART"].ToString();
                    string stock_a3 = dr_a3["STOCK_A3"].ToString();
                    string permitir_compras_a3 = dr_a3["PERMITIR_COMPRAS"].ToString();

                    if (Convert.ToInt32(stock_a3) < 0)
                    {
                        stock_a3 = "0";
                    }
                    string A1A3 = dr_a3["ID"].ToString();
                    string A2A3 = dr_a3["ID2"].ToString();

                    if (reference_a3 == reference_ps)
                    {
                        csUtilidades.log("STOCK IGUAL");

                        //16-04-2019 Hemos añadido que si la familia es monotalla.
                        //22-04-2021 RAUL - Se modifica el IF para que entre en el caso de que este activadas las monotallas en lugar de en funcion del nombre de la conexión
                        //if (csGlobal.conexionDB.Contains("INDUSNOW") || csGlobal.conexionDB.Contains("CADCANARIAS"))
                        if (csGlobal.activarMonotallas == "Y")
                        {
                            try
                            {
                                if (dr_a3["FAMILIAMONOTALLAV"].ToString().Equals("T"))
                                {
                                    A1A3 = A2A3;
                                }
                                else if (dr_a3["FAMILIAMONOTALLAH"].ToString().Equals("T"))
                                {

                                    A2A3 = A1A3;

                                }
                            }
                            catch (Exception ex) { Program.guardarErrorFichero("Error csStocks->stockConTallasA3SIPSSI()" + 
                                "Error "+ ex.StackTrace);}
                            
                        }

                        // 16-04-2019 FIN

                        if (concuerdanAtributos(A1A3, A2A3, A1PS, A2PS))
                        {
                            csUtilidades.log("EXISTE COMBINACION");
                            existeCombinacion = true;
                            dr_ps["STOCK_A3"] = stock_a3;
                            dr_ps["PERMITIR_COMPRAS_A3"] = permitir_compras_a3;

                            if (stock_a3 != stock_ps || permitir_compras_a3 != permitir_compras_ps)
                            {
                                dr_ps["CHECK"] = "T";
                            }
                            break;

                        }
                    }
                }

                //Añadimos esta linea porque en stockalm no tienen todas las combinaciones, por lo que si no tiene stock la seteamos en prestashop
                if (!existeCombinacion)
                {

                    if (stock_ps != "0")
                    {
                        dr_ps["CHECK"] = "T";
                        dr_ps["STOCK_A3"] = "0";
                        dr_ps["PERMITIR_COMPRAS_A3"] = "0";
                    }
                }
            }

            DataTable final = csUtilidades.dataRowArray2DataTable(ps.Select("CHECK = 'T'"));
            // si devuelve null es que no hay que crear tallas nuevas en PS
            return final;
        }

        public DataTable stockConTallasA3NOPSSI(string idProductPS = null, string codart = null)
        {
            csSqlScripts scripts = new csSqlScripts();
            // Datatable de los productos con tallas
            DataTable ps = mysql.cargarTabla(scripts.stockPSTallasA3SIPSI(idProductPS));
            DataTable a3 = sql.cargarDatosTablaA3(scripts.stockA3TallasA3NOPSSI(codart));
            ps.Columns.Add("STOCK_A3");
            ps.Columns.Add("PERMITIR_COMPRAS_A3");
            ps.Columns.Add("CHECK");
            string hola = "";
            foreach (DataRow dr_ps in ps.Rows)
            {
                string reference_ps = dr_ps["referenceAttr"].ToString();
                string stock_ps = dr_ps["STOCK_PS"].ToString();
                string permitir_compras_ps = dr_ps["permitir_compras_PS"].ToString();

                foreach (DataRow dr_a3 in a3.Rows)
                {
                    string reference_a3 = dr_a3["CODART"].ToString();
                    string stock_a3 = dr_a3["STOCK_A3"].ToString();
                    string permitir_compras_a3 = dr_a3["PERMITIR_COMPRAS"].ToString();

                    if (reference_a3 == "PA02001999051") {
                        hola = "hola"; }

                        if (reference_a3 == reference_ps)
                    {
                        if (Convert.ToInt32(stock_a3) < 0)
                        {
                            stock_a3 = "0";
                        }
 


                        dr_ps["STOCK_A3"] = stock_a3;
                        dr_ps["PERMITIR_COMPRAS_A3"] = permitir_compras_a3;

                        if (stock_a3 != stock_ps || permitir_compras_a3 != permitir_compras_ps)
                        {
                            dr_ps["CHECK"] = "T";
                        }
                    }
                }
            }

            return csUtilidades.dataRowArray2DataTable(ps.Select("CHECK = 'T'"));
            //return ps;
        }

        /// <summary>
        /// FUNCIONA - GLOBAL
        /// </summary>
        public void stock(string idProduct = null, string codart = null)
        {
            // Si ps_stock_available está vacio
            // signficará que hay que coger todo el stock de A3
            // y subirlo de cero

            // Si no tiene registros la tabla, hacemos inserts
            //Inicialización de tallas y colores
            if (csGlobal.modeDebug)
            {
                csUtilidades.log("COMIENZO");
            }
            if (!mysql.existeTabla("ps_stock_available"))
            {
                stockInsert();
            }

            // Si no está vacia, actualizo el stock con updates
            else
            {
                csUtilidades.log("CARGANDO STOCK");
                //Añadimos un control de stock
                //comprobamos el número de artículos sin atributos de Prestashop, y lo cruzamos con el número de artículos con atributo=0 en stock available
                //si no cuadra, añadimos los que faltan

                verificarStockGlobalArticulos(idProduct, codart);

                DataTable dt = null; //Detalle stock
                DataTable dt2 = null; //Total stock articulo

                if (csGlobal.tieneTallas)
                {
                    csUtilidades.log("STOCK TIENE TALLAS");

                    if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                    {
                        //Cargo en un datatable por un lado el stock de los artículos con atributos
                        //y en otro el stock total
                        //Calculo del detall de los atributos
                        csUtilidades.log("A3SIPSSI");
                        dt = stockConTallasA3SIPSSI(idProduct);
                        //cálculo de los totales
                        dt2 = stockConTallasTotalA3SIPSSI(idProduct);
                        csUtilidades.log("A3SIPSSI2");
                    }
                    else if (csGlobal.modoTallasYColores.ToUpper() == "A3NOPSSI") // MIRO
                    {
                        dt = stockConTallasA3NOPSSI(idProduct, codart);
                        dt2 = stockConTallasTotalA3NOPSSI(idProduct, codart);
                    }
                }
                else
                {
                    dt = stockSinTallas();
                }

                if (csUtilidades.verificarDt(dt))
                {
                    csUtilidades.log("UPDATE STOCK");
                    updateStock(dt);
                }

                if (csUtilidades.verificarDt(dt2))
                {
                    //Cojo unicamente las filas que me interesa actualizar
                    DataView dv = new DataView(dt2);
                    dv.RowFilter = "CHECK='T'";
                    DataTable dt2Clean = dv.ToTable();

                    if (dt2Clean.hasRows())
                    {
                        updateStock(dt2Clean, true);
                    }
                }

                actualizarStockEsportissim();
                //actualizarStockPendienteThagson();

                //Desactivo los artículos con Stock a 0 de Indusnow para que no se muestren
                if (csGlobal.conexionDB.Contains("INDUSNOW"))
                {
                    actualizarVisibilidadArticulosSinStockIndusnow();

                }
                csUtilidades.log("FIN STOCK");

            }

        }

     

        public void actualizarVisibilidadArticulosSinStockIndusnow()
        {
            DataTable articulos = new DataTable();
            articulos.Columns.Add("idProduct");
            int numfila = 0;
            string where_in = "";

            string query = "select " +
                " ps_stock_available.id_product,ps_stock_available.quantity , ps_product.active " +
                " from ps_stock_available inner join ps_product " +
                " on ps_stock_available.id_product=ps_product.id_product " +
                " where ps_stock_available.quantity=0 and ps_stock_available.id_product_attribute=0  " +
                " order by ps_product.id_product";

            string filterArticulos = "";
            string queryUpdate = "";

            DataTable dt = mysql.obtenerDatosPS(query);

            foreach (DataRow item in dt.Rows)
            {
                DataRow fila = articulos.NewRow();
                fila["idProduct"] = item["id_product"].ToString();
                articulos.Rows.Add(fila);
                numfila++;

                if (numfila == 500)
                {
                    where_in = csUtilidades.delimitarPorComasDataTableNotDouble(dt, "id_product");
                    queryUpdate = "update ps_product set  ps_product.active=0 where ps_product.id_product in ('" + filterArticulos + "')";
                    csUtilidades.ejecutarConsulta(queryUpdate, true);
                    queryUpdate = "update ps_product_shop set  ps_product.active=0 where ps_product.id_product in ('" + filterArticulos + "')";
                    csUtilidades.ejecutarConsulta(queryUpdate, true);
                    articulos.Rows.Clear();
                    numfila = 0;
                }
            }
            if (numfila < 500 && numfila > 0)
            {
                where_in = csUtilidades.delimitarPorComasDataTableNotDouble(dt, "id_product");
                queryUpdate = "update ps_product set  ps_product.active=0 where ps_product.id_product in ('" + where_in + "')";
                csUtilidades.ejecutarConsulta(queryUpdate, true);
                queryUpdate = "update ps_product_shop set  ps_product_shop.active=0 where ps_product_shop.id_product in ('" + where_in + "')";
                csUtilidades.ejecutarConsulta(queryUpdate, true);
            }
        }

        private static void actualizarStockEsportissim()
        {
            if (csGlobal.databaseA3.Contains("TRIATLO"))
            {
                csUtilidades.ejecutarConsulta("UPDATE ps_stock_available SET out_of_stock = 1 WHERE id_product IN " +
                    " (SELECT id_product FROM ps_category_product WHERE id_category IN (467, 468, 469, 470,472,473))",
                    true);
            }
        }

        private void actualizarStockPendienteThagson()
        {
            if (csGlobal.conexionDB.ToUpper().Contains("THAGSON"))
            {
                string query = "SELECT DISTINCT dbo.LINEPEDI.CODART, " +
                                 "                dbo.LINEPEDI.SITUACION, " +
                                 "				 SUM (CASE WHEN IDPEDC IS NULL THEN dbo.LINEPEDI.UNIDADES ELSE 0 END) + SUM (CASE WHEN IDPEDC IS NULL THEN dbo.LINEPEDI.UNISERVIDA ELSE 0 END) -  " +
                                 "				 SUM (CASE WHEN IDPEDC IS NULL THEN dbo.LINEPEDI.UNIANULADA ELSE 0 END)			  " +
                                 "				  " +
                                 "				 AS PENDIENTE_VENTA, " +
                                 "               	 SUM (CASE WHEN IDPEDV IS NULL THEN dbo.LINEPEDI.UNIDADES ELSE 0 END) + SUM (CASE WHEN IDPEDV IS NULL THEN dbo.LINEPEDI.UNISERVIDA ELSE 0 END) -  " +
                                 "				 SUM (CASE WHEN IDPEDV IS NULL THEN dbo.LINEPEDI.UNIANULADA ELSE 0 END)			  " +
                                 "				  " +
                                 "				 AS PENDIENTE_COMPRA, " +
                                 "             " +
                                 "                dbo.ARTICULO.KLS_ID_SHOP " +
                                 "FROM dbo.LINEPEDI " +
                                 "INNER JOIN dbo.ARTICULO ON dbo.LINEPEDI.CODART = dbo.ARTICULO.CODART " +
                                 "WHERE (dbo.ARTICULO.KLS_ID_SHOP > 0) AND (dbo.LINEPEDI.SITUACION = 'A') " +
                                 "GROUP BY dbo.LINEPEDI.CODART, " +
                                 "         dbo.LINEPEDI.SITUACION, " +
                                 "         dbo.ARTICULO.KLS_ID_SHOP ";

                DataTable pendingStockPS = sql.cargarDatosTablaA3(query);

                string kls_id_shops = csUtilidades.delimitarPorComasDataTable(pendingStockPS, "kls_id_shop");
                DataTable ps = mysql.cargarTabla(string.Format("select id_product, quantity from ps_stock_available where id_product in ({0})", kls_id_shops));
                if (pendingStockPS.hasRows())
                {
                    foreach (DataRow pending in pendingStockPS.Rows)
                    {
                        string pendienteCompra = pending["PENDIENTE_COMPRA"].ToString();
                        string pendienteVenta = pending["PENDIENTE_VENTA"].ToString();
                        string kls_id_shop = pending["KLS_ID_SHOP"].ToString();

                        foreach (DataRow dr_ps in ps.Rows)
                        {
                            string id_product = dr_ps["id_product"].ToString();
                            string quantity = dr_ps["quantity"].ToString();

                            if (id_product == "20357")
                            {

                            }

                            if (id_product == kls_id_shop)
                            {
                                Double quantity_ps = (Convert.ToDouble(quantity));
                                Double quantity_purchase_a3 = (Convert.ToDouble(pendienteCompra));
                                Double quantity_sale_a3 = (Convert.ToDouble(pendienteVenta));
                                int total = Convert.ToInt32(quantity_ps - quantity_sale_a3 + quantity_purchase_a3);

                                string queryUpdateStock = string.Format("update ps_stock_available set quantity = {0} where id_product = {1}", total, kls_id_shop);
                                csUtilidades.ejecutarConsulta(queryUpdateStock, true);

                                break;
                            }
                        }
                    }
                }
            }
        }





        /// <summary>
        /// Función donde se verifica que todos los artículos estén informados en la tabla de stock available (si coincide todo es correcto) si no reviso que artículos no están 
        /// dados de alta en dicha tabla y los asigno
        /// </summary>
        private void verificarStockGlobalArticulos(string idPS = null, string idERP = null)
        {
            DataTable dtPsProducts = new DataTable();
            DataTable dtPsProductsStock = new DataTable();
            DataTable dtPsProducts2Create = new DataTable();
            DataTable dtPsProductsStock2Delete = new DataTable();

            string anexoIdProduct = "";
            if (!string.IsNullOrEmpty(idPS))
            {
                anexoIdProduct = "and ps_product.id_product=" + idPS;
            }
            //no le encuentro sentido al where de esta consulta.

            string scriptArticulosNoTablaStock = "select " +
               " ps_product.id_product " +
               " from ps_product left outer  join ps_stock_available on ps_product.id_product=ps_stock_available.id_product " +
               " where ps_stock_available.id_product is null " + anexoIdProduct +
               " order by ps_product.id_product";

            dtPsProducts2Create = mysql.cargarTabla(scriptArticulosNoTablaStock);

            if (dtPsProducts2Create.hasRows())
            {
                stockAvailableCorrect(dtPsProducts2Create);
            }

        }






        // FUNCIONA
        //Inicialización de Stock
        //Primero se introduce las combinaciones de tallas y colores, y luego los totales
        private void stockInsert()
        {
            if (csGlobal.tieneTallas)
            {
                DataTable dt = null;

                // Obtener de ps_product_attribute el id_product_attribute, el id_product y setear a 0 todo el ps_stock_available
                dt = mysql.cargarTabla("select id_product_attribute, id_product from ps_product_attribute");

                DataView view = new DataView(dt);
                DataTable distinct = view.ToTable(true, "id_product");

                // TyC
                foreach (DataRow item in dt.Rows)
                {
                    string id_product = item["id_product"].ToString();
                    string id_product_attribute = item["id_product_attribute"].ToString();

                    csUtilidades.ejecutarConsulta("insert into ps_stock_available (id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock) " +
                        " values (" + id_product + ", " + id_product_attribute + ",1,0,0,0,2)", true);
                }

                // total
                foreach (DataRow item in distinct.Rows)
                {
                    string id_product = item["id_product"].ToString();
                    string id_product_attribute = "0";

                    csUtilidades.ejecutarConsulta("insert into ps_stock_available (id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock) " +
                        " values (" + id_product + ", " + id_product_attribute + ",1,0,0,0,2)", true);
                }
            }
            else
            {
                sincronizarStockNoTallas();
            }
        }

        //Funcion para corregir y añadir en la tabla ps_stock_available
        //Crea las entradas de aquellos artículos que no están dados de alta en esta tabla
        private void stockAvailableCorrect(DataTable products2Create)
        {
            foreach (DataRow item in products2Create.Rows)
            {
                string id_product = item["id_product"].ToString();
                string id_product_attribute = "0";

                csUtilidades.ejecutarConsulta("insert into ps_stock_available (id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock) " +
                    " values (" + id_product + ", " + id_product_attribute + ",1,0,0,0,2)", true);
            }

        }

        // FUNCIONA
        // Actualización de Stock de artículos
        private void updateStock(DataTable dt, bool total = false)
        {
            string id_product = "";
            string id_product_attribute = "";
            string id_shop = "";
            string id_shop_group = "";
            string quantity = "";
            string depends_on_stock = "";
            string out_of_stock = "";
            string scriptInsertStock = "";


            csPSWebService psWebService = new csPSWebService();
            DataTable dtStock = new DataTable();
            dtStock.Columns.Add("id_stock_available");
            dtStock.Columns.Add("id_product");
            dtStock.Columns.Add("id_product_attribute");
            dtStock.Columns.Add("quantity");
            dtStock.Columns.Add("out_of_stock");
            string permitir_compras = "2";
            DataRow drStock = dtStock.NewRow();

            string estoc = "";

            try
            {
                foreach (DataRow dr in dt.Rows)
                {

                    estoc = dr["STOCK_A3"].ToString();
                    if (estoc == "")
                    {
                        estoc = "0";
                    }
                    else
                    {
                        estoc = dr["STOCK_A3"].ToString();
                        double num = Convert.ToInt32(Math.Floor(Convert.ToDouble(estoc)));

                        if (Convert.ToInt32(Math.Floor(Convert.ToDouble(estoc))) < 0)
                        {
                            estoc = "0";
                        }
                    }
                    string stock = (dr["STOCK_A3"].ToString() == "") ? "0" : dr["STOCK_A3"].ToString();
                    string refe = dr["reference"].ToString();

                    if (dt.Columns.Contains("PERMITIR_COMPRAS_A3"))
                    {
                        permitir_compras = (dr["PERMITIR_COMPRAS_A3"].ToString() == "") ? "2" : dr["PERMITIR_COMPRAS_A3"].ToString();
                    }

                    if (!total)
                    {
                        drStock["id_stock_available"] = dr["id_stock_available"].ToString();
                        drStock["id_product"] = dr["id_product"].ToString();
                        drStock["id_product_attribute"] = dr["id_product_attribute"].ToString();
                        drStock["quantity"] = estoc;
                        drStock["out_of_stock"] = permitir_compras;

                        //Modificar para actualizar el stock via webservice


                        if (string.IsNullOrEmpty(drStock["id_stock_available"].ToString()))
                        {
                            //Creo la entrada de stock available

                            id_product = drStock["id_product"].ToString();
                            id_product_attribute = drStock["id_product_attribute"].ToString();
                            id_shop = "1";
                            id_shop_group = "0";
                            quantity = drStock["quantity"].ToString(); ;
                            depends_on_stock = "0";
                            out_of_stock = drStock["out_of_stock"].ToString();
                            scriptInsertStock = "insert into ps_stock_available (id_product,id_product_attribute,id_shop,id_shop_group,quantity,depends_on_stock,out_of_stock) values (" +
                                    id_product + "," +
                                    id_product_attribute + "," +
                                    id_shop + "," +
                                    id_shop_group + "," +
                                    quantity + "," +
                                    depends_on_stock + "," +
                                    out_of_stock + ")";

                            csUtilidades.ejecutarConsulta(scriptInsertStock, true);

                        }
                        else
                        {
                            try
                            {
                                psWebService.cdPSWebService("PUT", "stock_availables", "", "", true, "", null, drStock); //en minúsculas el objeto
                            }
                            catch (Exception ex)
                            {

                                //MessageBox.Show("Error con el Web Service al actualizar el stock");
                                Program.escribirErrorEnFichero(ex.Message);
                            }
                            
                        }
                    }


                    if (total)
                    {
                        if (dr["id_product"].ToString() == "671")
                        {
                            int i = 0;
                        }

                        drStock["id_stock_available"] = dr["id_stock_available"].ToString();
                        drStock["id_product"] = dr["id_product"].ToString();
                        drStock["id_product_attribute"] = "0";
                        drStock["quantity"] = estoc;
                        drStock["out_of_stock"] = permitir_compras;
                        //Modificar para actualizar el stock via webservice
                        psWebService.cdPSWebService("PUT", "stock_availables", "", "", true, "", null, drStock); //en minúsculas el objeto
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al actualizar stock: \n" + ex.ToString());
            }
        }

        public void updateFechaProximaRecepcion()
        {
            try
            {
                string updateScript = "";
                //Primero inicializamos las fechas de todos los artículos
                updateScript = "update ps_product set available_date='0000-00-00'";
                csUtilidades.ejecutarConsulta(updateScript, true);
                updateScript = "update ps_product_shop set available_date='0000-00-00'";
                csUtilidades.ejecutarConsulta(updateScript, true);


                string codart = "";
                string idProduct = "";
                DateTime fecha = default(DateTime);
                //Query para obtener para cada artículo la fecha de primera recepción 
                string script = "SELECT " +
                    " dbo.CABEPEDC.SERIE, dbo.CABEPEDC.NUMDOC, t.CODART, t.FECENTREGA, t.UNIDADES, t.SITUACION, t.IDPEDC, dbo.ARTICULO.KLS_ID_SHOP " +
                    " FROM " +
                    " dbo.LINEPEDI AS t INNER JOIN " +
                    " (SELECT CODART, MIN(FECENTREGA) AS FECHA " +
                    " FROM dbo.LINEPEDI " +
                    " WHERE (SITUACION = 'A') AND (IDPEDC IS NOT NULL) " +
                    " GROUP BY CODART) AS xx ON t.CODART = xx.CODART AND t.FECENTREGA = xx.FECHA INNER JOIN " +
                    " dbo.ARTICULO ON t.CODART = dbo.ARTICULO.CODART INNER JOIN " +
                    " dbo.CABEPEDC ON t.IDPEDC = dbo.CABEPEDC.IDPEDC " +
                    " WHERE (dbo.ARTICULO.KLS_ID_SHOP > 0)";

                DataTable dt = new DataTable();
                dt = sql.cargarDatosScriptEnTabla(script, "");

                foreach (DataRow dr in dt.Rows)
                {
                    codart = dr["CODART"].ToString();
                    idProduct = dr["KLS_ID_SHOP"].ToString();
                    fecha = Convert.ToDateTime(dr["FECENTREGA"].ToString());
                    updateScript = "update ps_product set available_date='" + fecha.ToString("yyyy-MM-dd") + "' where id_product=" + idProduct;
                    csUtilidades.ejecutarConsulta(updateScript, true);
                    updateScript = "update ps_product_shop set available_date='" + fecha.ToString("yyyy-MM-dd") + "' where id_product=" + idProduct;
                    csUtilidades.ejecutarConsulta(updateScript, true);
                }
            }
            catch (Exception ex)
            {
                Program.escribirErrorEnFichero(ex.Message);
            }
        }
    }
}
