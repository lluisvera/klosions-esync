﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace klsync
{
    public partial class frFamilias : Form
    {
        private static frFamilias m_FormDefInstance;
        public static frFamilias DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)

                    m_FormDefInstance = new frFamilias();

                return m_FormDefInstance;
            }

            set
            {
                m_FormDefInstance = value;
            }
        }

        public frFamilias()
        {
            InitializeComponent();
        }
        private void btnCargarFam_Click(object sender, EventArgs e)
        {
            cargarFam();            
        }

        private void cargarFam()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a;

            a = new SqlDataAdapter(sqlScript.selectFamilias(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvFamCat.DataSource = t;

       //     Objetos.csTercero tercero = new Objetos.csTercero();
        }



            



        private void exportarFilaToolStripMenuItem_Click(object sender, EventArgs e)

        {

            csPSWebService psWebService = new csPSWebService();

            try
            {
                string categoria = "";
                foreach (DataGridViewRow fila in dgvFamCat.SelectedRows)
                {
                    try
                    {
                        categoria = fila.Cells["COD_PS"].Value.ToString();
                        psWebService.cdPSWebService("POST", "categories", "", categoria, true, "");
                    }

                    catch (Exception)
                    {
                        //MessageBox.Show(ex.ToString());
                    }

                }

            }

            catch (Exception ex) { }

            cargarFam(); 

        }



        private void frFamCat_Load(object sender, EventArgs e)

        {



        }





     

    }

}