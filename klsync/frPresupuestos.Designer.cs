﻿namespace klsync
{
    partial class frPresupuestos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripPresupuestos = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonPasarPresupuestoA3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonBorrarCabeceras = new System.Windows.Forms.ToolStripButton();
            this.splitContainerPresu = new System.Windows.Forms.SplitContainer();
            this.dgvPresupuestos = new System.Windows.Forms.DataGridView();
            this.contextMenuRightBtnPresu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.abrirDesdeCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBoxPresupuestos = new System.Windows.Forms.ListBox();
            this.statusStripPresupuestos = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelSprangPresu = new System.Windows.Forms.ToolStripStatusLabel();
            this.numFilas = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripPresupuestos.SuspendLayout();
            this.splitContainerPresu.Panel1.SuspendLayout();
            this.splitContainerPresu.Panel2.SuspendLayout();
            this.splitContainerPresu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPresupuestos)).BeginInit();
            this.contextMenuRightBtnPresu.SuspendLayout();
            this.statusStripPresupuestos.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripPresupuestos
            // 
            this.toolStripPresupuestos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripPresupuestos.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStripPresupuestos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripPresupuestos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPasarPresupuestoA3,
            this.toolStripButtonHelp,
            this.toolStripSeparator1,
            this.toolStripButtonBorrarCabeceras});
            this.toolStripPresupuestos.Location = new System.Drawing.Point(0, 0);
            this.toolStripPresupuestos.Name = "toolStripPresupuestos";
            this.toolStripPresupuestos.Padding = new System.Windows.Forms.Padding(0, 5, 1, 0);
            this.toolStripPresupuestos.Size = new System.Drawing.Size(997, 65);
            this.toolStripPresupuestos.TabIndex = 0;
            this.toolStripPresupuestos.Text = "toolStrip1";
            // 
            // toolStripButtonPasarPresupuestoA3
            // 
            this.toolStripButtonPasarPresupuestoA3.Image = global::klsync.Properties.Resources.a3erp;
            this.toolStripButtonPasarPresupuestoA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPasarPresupuestoA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPasarPresupuestoA3.Margin = new System.Windows.Forms.Padding(10, 1, 0, 0);
            this.toolStripButtonPasarPresupuestoA3.Name = "toolStripButtonPasarPresupuestoA3";
            this.toolStripButtonPasarPresupuestoA3.Size = new System.Drawing.Size(87, 59);
            this.toolStripButtonPasarPresupuestoA3.Text = "Pasar a A3";
            this.toolStripButtonPasarPresupuestoA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonPasarPresupuestoA3.Click += new System.EventHandler(this.toolStripButtonPasarPresupuestoA3_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonHelp.Image = global::klsync.Properties.Resources.question_mark;
            this.toolStripButtonHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(58, 57);
            this.toolStripButtonHelp.Text = "Ayuda";
            this.toolStripButtonHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButtonBorrarCabeceras
            // 
            this.toolStripButtonBorrarCabeceras.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonBorrarCabeceras.Image = global::klsync.Properties.Resources.delete96;
            this.toolStripButtonBorrarCabeceras.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonBorrarCabeceras.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBorrarCabeceras.Name = "toolStripButtonBorrarCabeceras";
            this.toolStripButtonBorrarCabeceras.Size = new System.Drawing.Size(58, 57);
            this.toolStripButtonBorrarCabeceras.Text = "Borrar";
            this.toolStripButtonBorrarCabeceras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonBorrarCabeceras.Click += new System.EventHandler(this.toolStripButtonBorrarCabeceras_Click);
            // 
            // splitContainerPresu
            // 
            this.splitContainerPresu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerPresu.Location = new System.Drawing.Point(12, 73);
            this.splitContainerPresu.Name = "splitContainerPresu";
            this.splitContainerPresu.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerPresu.Panel1
            // 
            this.splitContainerPresu.Panel1.Controls.Add(this.dgvPresupuestos);
            // 
            // splitContainerPresu.Panel2
            // 
            this.splitContainerPresu.Panel2.Controls.Add(this.listBoxPresupuestos);
            this.splitContainerPresu.Size = new System.Drawing.Size(973, 509);
            this.splitContainerPresu.SplitterDistance = 457;
            this.splitContainerPresu.TabIndex = 9;
            // 
            // dgvPresupuestos
            // 
            this.dgvPresupuestos.AllowUserToAddRows = false;
            this.dgvPresupuestos.AllowUserToDeleteRows = false;
            this.dgvPresupuestos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPresupuestos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPresupuestos.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPresupuestos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPresupuestos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPresupuestos.ContextMenuStrip = this.contextMenuRightBtnPresu;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPresupuestos.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPresupuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPresupuestos.Location = new System.Drawing.Point(0, 0);
            this.dgvPresupuestos.Name = "dgvPresupuestos";
            this.dgvPresupuestos.ReadOnly = true;
            this.dgvPresupuestos.Size = new System.Drawing.Size(973, 457);
            this.dgvPresupuestos.TabIndex = 5;
            // 
            // contextMenuRightBtnPresu
            // 
            this.contextMenuRightBtnPresu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuRightBtnPresu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pegarToolStripMenuItem,
            this.limpiarToolStripMenuItem,
            this.toolStripMenuItem1,
            this.abrirDesdeCSVToolStripMenuItem});
            this.contextMenuRightBtnPresu.Name = "contextMenuRightBtn";
            this.contextMenuRightBtnPresu.Size = new System.Drawing.Size(210, 124);
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Image = global::klsync.Properties.Resources.clipboard_paste_button;
            this.pegarToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(209, 38);
            this.pegarToolStripMenuItem.Text = "&Pegar";
            this.pegarToolStripMenuItem.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // limpiarToolStripMenuItem
            // 
            this.limpiarToolStripMenuItem.Image = global::klsync.Properties.Resources.cross_remove_sign;
            this.limpiarToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.limpiarToolStripMenuItem.Name = "limpiarToolStripMenuItem";
            this.limpiarToolStripMenuItem.Size = new System.Drawing.Size(209, 38);
            this.limpiarToolStripMenuItem.Text = "&Limpiar";
            this.limpiarToolStripMenuItem.Click += new System.EventHandler(this.limpiarToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(206, 6);
            // 
            // abrirDesdeCSVToolStripMenuItem
            // 
            this.abrirDesdeCSVToolStripMenuItem.Image = global::klsync.Properties.Resources.csv_file_format_extension;
            this.abrirDesdeCSVToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.abrirDesdeCSVToolStripMenuItem.Name = "abrirDesdeCSVToolStripMenuItem";
            this.abrirDesdeCSVToolStripMenuItem.Size = new System.Drawing.Size(209, 38);
            this.abrirDesdeCSVToolStripMenuItem.Text = "Abrir desde CSV";
            this.abrirDesdeCSVToolStripMenuItem.Click += new System.EventHandler(this.abrirDesdeCSVToolStripMenuItem_Click);
            // 
            // listBoxPresupuestos
            // 
            this.listBoxPresupuestos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxPresupuestos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.listBoxPresupuestos.FormattingEnabled = true;
            this.listBoxPresupuestos.ItemHeight = 20;
            this.listBoxPresupuestos.Location = new System.Drawing.Point(0, 0);
            this.listBoxPresupuestos.Name = "listBoxPresupuestos";
            this.listBoxPresupuestos.Size = new System.Drawing.Size(973, 48);
            this.listBoxPresupuestos.TabIndex = 7;
            // 
            // statusStripPresupuestos
            // 
            this.statusStripPresupuestos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelSprangPresu,
            this.numFilas});
            this.statusStripPresupuestos.Location = new System.Drawing.Point(0, 591);
            this.statusStripPresupuestos.Name = "statusStripPresupuestos";
            this.statusStripPresupuestos.Size = new System.Drawing.Size(997, 26);
            this.statusStripPresupuestos.TabIndex = 10;
            this.statusStripPresupuestos.Text = "statusStrip1";
            // 
            // toolStripStatusLabelSprangPresu
            // 
            this.toolStripStatusLabelSprangPresu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripStatusLabelSprangPresu.Name = "toolStripStatusLabelSprangPresu";
            this.toolStripStatusLabelSprangPresu.Size = new System.Drawing.Size(931, 21);
            this.toolStripStatusLabelSprangPresu.Spring = true;
            // 
            // numFilas
            // 
            this.numFilas.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.numFilas.Name = "numFilas";
            this.numFilas.Size = new System.Drawing.Size(51, 21);
            this.numFilas.Text = "0 filas";
            // 
            // frPresupuestos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 617);
            this.Controls.Add(this.statusStripPresupuestos);
            this.Controls.Add(this.splitContainerPresu);
            this.Controls.Add(this.toolStripPresupuestos);
            this.KeyPreview = true;
            this.Name = "frPresupuestos";
            this.Text = "Presupuestos";
            this.toolStripPresupuestos.ResumeLayout(false);
            this.toolStripPresupuestos.PerformLayout();
            this.splitContainerPresu.Panel1.ResumeLayout(false);
            this.splitContainerPresu.Panel2.ResumeLayout(false);
            this.splitContainerPresu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPresupuestos)).EndInit();
            this.contextMenuRightBtnPresu.ResumeLayout(false);
            this.statusStripPresupuestos.ResumeLayout(false);
            this.statusStripPresupuestos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripPresupuestos;
        private System.Windows.Forms.ToolStripButton toolStripButtonPasarPresupuestoA3;
        private System.Windows.Forms.SplitContainer splitContainerPresu;
        private System.Windows.Forms.DataGridView dgvPresupuestos;
        private System.Windows.Forms.ListBox listBoxPresupuestos;
        private System.Windows.Forms.StatusStrip statusStripPresupuestos;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSprangPresu;
        private System.Windows.Forms.ToolStripStatusLabel numFilas;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightBtnPresu;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripButton toolStripButtonBorrarCabeceras;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirDesdeCSVToolStripMenuItem;
    }
}