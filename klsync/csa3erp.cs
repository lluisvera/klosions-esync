﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a3ERPActiveX;
using System.Windows.Forms;
using System.Data;
using System.Web;
using System.Xml;
using System.Data.Odbc;
using klsync.Configuracion;


namespace klsync
{
    class csa3erp
    {
        static a3ERPActiveX.Enlace enlace = new Enlace();
        string errorNumDocCabecera = "";
        string errorArtLinDocumento = "";
        //public a3ERPActiveX.Enlace enlace;
         
        public void abrirEnlace()
        {
            try
            {
                if (estadoConexion())
                {
                    return;
                }

                enlace.RaiseOnException = true;
                //if (csGlobal.conexionDB == "NET100X100") net100x100(true, false);

                enlace.LoginUsuario(csGlobal.userA3, csGlobal.passwordA3);
                enlace.Iniciar(csGlobal.nombreEmpresaA3, string.Empty);

                "Validando usuario...".log();
                if (csGlobal.modeDebug)
                {
                    csGlobal.nombreEmpresaA3.log();
                    csGlobal.rutaA3.log();
                    "Enlace abierto".log();
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    (ex.Message).mb();
                }

                enlace.Acabar();
                Program.guardarErrorFichero(ex.Message);
            }
        }

        public bool estadoConexion()
        {
            if (enlace.Estado.ToString() == "estACTIVO")
                return true;
            else
                return false;
        }

        public void cerrarEnlace()
        {
            //modificación para net100x100 tema dlls
           // if (csGlobal.conexionDB == "NET100X100") net100x100(false, true);
            if (enlace.Estado != EstadoEnlace.estACTIVO)  enlace.Acabar();

            "Enlace cerrado".log();
        }


        private void net100x100(bool inicio, bool fin)
        {
            if (inicio)
            {
                csUtilidades.ejecutarConsulta("DELETE FROM DLLS_KLS",false);
                csUtilidades.ejecutarConsulta("INSERT INTO DLLS_KLS SELECT * FROM DLLS", false);
                csUtilidades.ejecutarConsulta("DELETE FROM DLLS", false);
            }

            if (fin)
            {
                csUtilidades.ejecutarConsulta("INSERT INTO DLLS SELECT * FROM DLLS_KLS", false);
                csUtilidades.ejecutarConsulta("DELETE FROM DLLS_KLS", false);

                //Contenido de las 2 filas de la tabla DLLS
                //dllBitemca.dllBitemca 	        1	DISI	1	            1
                //MVEA_namespace.MVEA_class_A3ERP   2   DISI    MULTICORREO     2
            }

        }

        public void generaDoc(string fecha, string nifcli, string nomcli, string dircli, string dtocli, string pobcli, string codart, double unidades, double precio)
        {
            string cliente = "";
            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
            {
                cliente = csGlobal.clienteGenerico;
            }
            else
            {
                cliente = "";
            }

            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            
            naxfactura.Iniciar();
            naxfactura.Nuevo(fecha, "3", false, false, true, true);
            naxfactura.set_AsStringCab("nifcli", nifcli);
            naxfactura.set_AsStringCab("nomcli", nomcli);
            naxfactura.set_AsStringCab("dircli", dircli);
            naxfactura.set_AsStringCab("dtocli", dtocli);
            naxfactura.set_AsStringCab("pobcli", pobcli);
            naxfactura.NuevaLineaArt(codart, unidades);
            naxfactura.set_AsFloatLin("Desc1", 0);
            //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
            //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
            naxfactura.set_AsFloatLin("Prcmonedamasiva", precio);
            naxfactura.set_AsStringLin("CentroCoste", "C1");
            naxfactura.set_AsStringLin("CentroCoste2", "C2");
            naxfactura.set_AsStringLin("CentroCoste3", "C3");

            naxfactura.AnadirLinea();
            decimal documento = naxfactura.Anade();
            naxfactura.Acabar();
        }

        private int numeroFras(string[,] documentos)
        {
            int contadorFras = 1;
            string factura = documentos[0, 9];
            for (int i = 0; i < documentos.GetLength(0); ++i)
            {
                if (documentos[i, 9] != factura)
                {
                    contadorFras++;
                    factura = documentos[i, 9];
                }
            }
            return contadorFras;
        }

        public void generaDocA3(string[,] documentos) //para invenio 
        {
            string[] frasGeneradas = new string[numeroFras(documentos)];
            int contadorFrasGeneradas = 0;
            int contador = 0;
            string factura = "";
            bool multiLin = false;
            string cliente = "";

            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            a3ERPActiveX.Pedido naxpedido = new a3ERPActiveX.Pedido();

            if (csGlobal.docDestino == "Factura")
            {
                //int largo = documentos.GetLength(0);
                for (int i = 0; i < documentos.GetLength(0); ++i)
                {
                    frasGeneradas[contadorFrasGeneradas] = documentos[i, 9] + ";" + documentos[i, 9] + ";" + DateTime.Today.ToString("yy-MM-dd");
                    contadorFrasGeneradas++;
                    //en el valor i,9 guarda el número de la factura
                    factura = documentos[i, 9];

                    for (int j = 0; j < documentos.GetLength(0); j++)
                    {
                        if (documentos[j, 9] == factura)
                        {
                            contador = contador + 1;
                        }
                        else
                        {
                            multiLin = false;
                        }
                    }
                    if (contador > 1)
                    {
                        ////MessageBox.Show("La Factura " + factura + " tiene más de una linea");
                        multiLin = true;
                    }

                    naxfactura.Iniciar();


                    if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                    {

                        cliente = csGlobal.clienteGenerico;
                    }
                    else
                    {
                        cliente = documentos[i, 15];
                    }

                    // naxpedido.Nuevo(documentos[i, 0], csGlobal.clienteGenerico, false);
                    naxfactura.Nuevo(documentos[i, 0], cliente, false, false, true, true);


                    naxfactura.set_AsStringCab("nifcli", documentos[i, 1].ToUpper());
                    naxfactura.set_AsStringCab("serie", csGlobal.serie);
                    naxfactura.set_AsStringCab("nomcli", documentos[i, 2].ToUpper() + " " + documentos[i, 3].ToUpper());
                    //Para informar de la Razón verifico si está informado
                    if (documentos[i, 4] != "")
                    {
                        naxfactura.set_AsStringCab("razon", documentos[i, 4].ToUpper());
                    }
                    else
                    {
                        naxfactura.set_AsStringCab("razon", documentos[i, 2].ToUpper() + " " + documentos[i, 3].ToUpper());
                    }
                    naxfactura.set_AsStringCab("dircli", documentos[i, 5].ToUpper());
                    naxfactura.set_AsStringCab("dtocli", documentos[i, 13]);
                    naxfactura.set_AsStringCab("pobcli", documentos[i, 14].ToUpper());
                    naxfactura.set_AsStringCab("numdoc", documentos[i, 9]);

                    if (multiLin)
                    {
                        int ii = 0;
                        for (ii = 0; ii < contador; ii++)
                        {
                            naxfactura.NuevaLineaArt(documentos[i + ii, 6], Convert.ToDouble(documentos[i + ii, 7]));
                            //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                            //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                            if (csGlobal.ivaIncluido.ToUpper() == "SI")
                            {
                                naxfactura.set_AsFloatLin("Prcmonedamasiva", Convert.ToDouble(documentos[i + ii, 8]));
                            }
                            else
                            {
                                naxfactura.set_AsFloatLin("Prcmoneda", Convert.ToDouble(documentos[i + ii, 8]));
                            }
                            naxfactura.set_AsFloatLin("Desc1", Convert.ToDouble(documentos[i, 10]));
                            naxfactura.set_AsStringLin("CentroCoste" ,  csGlobal.nivel1Analitica);
                            naxfactura.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                            naxfactura.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                            naxfactura.AnadirLinea();

                        }
                        multiLin = false;
                        i = i + (ii - 1);
                    }
                    else
                    {
                        naxfactura.NuevaLineaArt(documentos[i, 6], Convert.ToDouble(documentos[i, 7]));
                        naxfactura.set_AsFloatLin("Desc1", 0);
                        //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                        //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                        if (csGlobal.ivaIncluido.ToUpper() == "SI")
                        {
                            naxfactura.set_AsFloatLin("Prcmonedamasiva", Convert.ToDouble(documentos[i, 8]) * 1.21);
                            //naxfactura.set_AsFloatLin("Prcmoneda", Convert.ToDouble(documentos[i, 8]));
                        }
                        else
                        {
                            naxfactura.set_AsFloatLin("Prcmoneda", Convert.ToDouble(documentos[i, 8]));
                        }


                        naxfactura.set_AsFloatLin("Desc1", Convert.ToDouble(documentos[i, 10]));//Descuento
                        naxfactura.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                        naxfactura.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                        naxfactura.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                        naxfactura.AnadirLinea();
                    }
                    decimal documento = naxfactura.Anade();
                    naxfactura.Acabar();
                    contador = 0;
                }

                csMySqlConnect mySql = new csMySqlConnect();
                mySql.InsertFrasGeneradas(frasGeneradas);
            }
            else if (csGlobal.docDestino == "Pedido")
            {
                //int largo = documentos.GetLength(0);
                for (int i = 0; i < documentos.GetLength(0); ++i)
                {
                    frasGeneradas[contadorFrasGeneradas] = documentos[i, 9] + ";" + documentos[i, 9] + ";" + DateTime.Today.ToString("yy-MM-dd");
                    contadorFrasGeneradas++;
                    //en el valor i,9 guarda el número de la factura
                    factura = documentos[i, 9];

                    for (int j = 0; j < documentos.GetLength(0); j++)
                    {
                        if (documentos[j, 9] == factura)
                        {
                            contador = contador + 1;
                        }
                        else
                        {
                            multiLin = false;
                        }
                    }
                    if (contador > 1)
                    {
                        ////MessageBox.Show("La Factura " + factura + " tiene más de una linea");
                        multiLin = true;
                    }

                    naxpedido.Iniciar();
                    naxpedido.Nuevo(documentos[i, 0], csGlobal.clienteGenerico, false);

                    naxpedido.set_AsStringCab("nifcli", documentos[i, 1].ToUpper());
                    naxpedido.set_AsStringCab("serie", csGlobal.serie);
                    naxpedido.set_AsStringCab("nomcli", documentos[i, 2].ToUpper() + " " + documentos[i, 3].ToUpper());
                    //Para informar de la Razón verifico si está informado
                    if (documentos[i, 4] != "")
                    {
                        naxpedido.set_AsStringCab("razon", documentos[i, 4].ToUpper());
                    }
                    else
                    {
                        naxpedido.set_AsStringCab("razon", documentos[i, 2].ToUpper() + " " + documentos[i, 3].ToUpper());
                    }
                    naxpedido.set_AsStringCab("dircli", documentos[i, 5].ToUpper());
                    naxpedido.set_AsStringCab("dtocli", documentos[i, 13]);
                    naxpedido.set_AsStringCab("pobcli", documentos[i, 14].ToUpper());
                    naxpedido.set_AsStringCab("numdoc", documentos[i, 9]);
                    naxpedido.set_AsStringCab("referencia", "From Prestashop");

                    if (multiLin)
                    {
                        int ii = 0;
                        for (ii = 0; ii < contador; ii++)
                        {
                            naxpedido.NuevaLineaArt(documentos[i + ii, 6], Convert.ToDouble(documentos[i + ii, 7]));
                            //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                            //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                            naxpedido.set_AsFloatLin("Prcmoneda", Convert.ToDouble(documentos[i + ii, 8]));
                            naxpedido.set_AsFloatLin("Desc1", Convert.ToDouble(documentos[i, 10]));
                            naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                            naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                            naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                            naxpedido.AnadirLinea();

                        }
                        multiLin = false;
                        i = i + (ii - 1);
                    }
                    else
                    {
                        naxpedido.NuevaLineaArt(documentos[i, 6], Convert.ToDouble(documentos[i, 7]));
                        naxpedido.set_AsFloatLin("Desc1", 0);
                        //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                        //En caso contrario utilizar el campo prcmodena (depende del campo cliente facturas más iva)
                        naxpedido.set_AsFloatLin("Prcmoneda", Convert.ToDouble(documentos[i, 8]));
                        naxpedido.set_AsFloatLin("Desc1", Convert.ToDouble(documentos[i, 10]));//Descuento
                        naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                        naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                        naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                        naxpedido.AnadirLinea();
                    }
                    decimal documento = naxpedido.Anade();
                    naxpedido.Acabar();
                    contador = 0;
                }

                csMySqlConnect mySql = new csMySqlConnect();
                mySql.InsertFrasGeneradas(frasGeneradas);
            }



        }

        public void borrarDocumento(string codDoc, string tipoDocumento, bool esDeCompra = false)
        {
            csSqlConnects sql = new csSqlConnects();
            //string numDoc = sql.obtenerInformacionDocumentoVenta(csGlobal.serie, codDoc)[0];

            if (tipoDocumento == "Pedidos")
            {
                a3ERPActiveX.Pedido ped = new a3ERPActiveX.Pedido();

                try
                {
                    ped.Iniciar();
                    ped.Borra(Convert.ToDecimal(codDoc), esDeCompra);
                }
                catch (Exception)
                { }
                finally
                {
                    ped.Acabar();
                    ped = null;
                }

            }

            else if (tipoDocumento == "Albaranes")
            {
                a3ERPActiveX.Albaran alb = new a3ERPActiveX.Albaran();

                try
                {
                    alb.Iniciar();
                    alb.Borra(Convert.ToDecimal(codDoc), esDeCompra);
                }
                catch (Exception)
                { }
                finally
                {
                    alb.Acabar();
                    alb = null;
                }

            }
            else
            {
                a3ERPActiveX.Factura fac = new a3ERPActiveX.Factura();

                try
                {
                    fac.Iniciar();
                    fac.Borra(Convert.ToDecimal(codDoc), esDeCompra);
                }
                catch (Exception)
                { }
                finally
                {
                    fac.Acabar();
                    fac = null;
                }
            }
        }
        
        public int generarDocA3ObjetoRepasat(Objetos.csCabeceraDoc[] cabeceras, Objetos.csLineaDocumento[] lineas, bool docCompras, string usarClienteGenerico, Objetos.csTercero[] objTercero = null, Objetos.csDomBanca[] objDomiciliaciones = null)
        {

            int documentosGenerados = 0;
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            csSqlConnects sql = new csSqlConnects();
            string codIC = "", documentoLinea = "", articulo = "", numeroDocPs = "", numeroDocA3 = "", fechaContable = "", fechaSync = "", consulta = "";
            double precioArticulo = 0, baseRetencion = 0;
            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            a3ERPActiveX.Albaran naxAlbaran = new a3ERPActiveX.Albaran();
            a3ERPActiveX.Pedido naxPedido = new a3ERPActiveX.Pedido();

            //Gestión de errores
            string errorNumDocumento = "";
            string errorNumSerie = "";
            string errorTipoDoc = "";
            string errorCodCuenta = "";
            string errorNomCuenta = "";
            string errorNumLinea = "";
            string errorCodart = "";
            string errorArticulo = "";

            try
            {
                for (int i = 0; i < cabeceras.Count(); i++)
                {
                    if (cabeceras[i] != null)
                    {
                        try
                        {


                            if (csGlobal.docDestino.ToUpper() == "FACTURA")
                            {
                                errorTipoDoc = csGlobal.docDestino.ToUpper();

                                naxfactura.Iniciar();
                                naxfactura.OmitirMensajes = true;
                                naxfactura.ActivarAlarmaCab = false;

                                if (objTercero != null && objTercero[i] != null)
                                {
                                    if (usarClienteGenerico.ToUpper() == "SI")
                                    {
                                        codIC = csGlobal.clienteGenerico;
                                    }
                                    else
                                    {
                                        if (docCompras) // Proveedores
                                        {
                                            //consulta = "SELECT COUNT(IDFACC) FROM CABEFACC WHERE IDFACC = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                        else // Clientes
                                        {
                                            consulta = "SELECT COUNT(IDFACV) FROM CABEFACV WHERE IDFACV = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                    }
                                }
                                //if (cabeceras[i].numDocA3 == null || sql.obtenerCampoTabla(consulta) == "0")  Deprecated 4-3-2018
                                if (!string.IsNullOrEmpty(cabeceras[i].numDocA3))
                                {
                                    fechaSync = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    fechaContable = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    numeroDocA3 = cabeceras[i].numDocA3; // number
                                    numeroDocPs = cabeceras[i].extNumdDoc;
                                    naxfactura.Nuevo(fechaSync, cabeceras[i].codIC.Trim(), docCompras, false, true, true);

                                    //Gestión de Errores
                                    errorNumDocumento = cabeceras[i].numDocA3;
                                    errorNumSerie = cabeceras[i].serieDoc;
                                    errorCodCuenta = cabeceras[i].codIC.Trim();
                                    errorNomCuenta = cabeceras[i].nombreIC.ToUpper();
                                    //Fin Gestión de Errores

                                    naxfactura.set_AsStringCab("FECHACONTABLE", Convert.ToDateTime(cabeceras[i].fechaContableDoc).ToString("dd/MM/yyyy"));

                                    if (docCompras)
                                    {
                                        naxfactura.set_AsStringCab("NIFPRO", cabeceras[i].nif);
                                        naxfactura.set_AsStringCab("NOMPRO", cabeceras[i].nombreIC.ToUpper());
                                        naxfactura.set_AsStringCab("NUMDOCAPU", cabeceras[i].numeroDocProveedor);

                                        if (csGlobal.ticketBaiActivo && docCompras)
                                        {
                                            naxfactura.set_AsStringCab("TBAI_NUMFACPROV", cabeceras[i].numeroDocProveedor);
                                            naxfactura.set_AsStringCab("TBAI_SERIEFACPROV", string.Empty);
                                        }

                                        if (cabeceras[i].clienteGenerico == 1)
                                        {
                                            naxfactura.set_AsStringCab("RAZON", string.IsNullOrEmpty(cabeceras[i].razonSocial) ? cabeceras[i].nombreIC.ToUpper() : cabeceras[i].razonSocial.ToUpper());
                                            naxfactura.set_AsStringCab("NIFPRO", cabeceras[i].nif);
                                            naxfactura.set_AsStringCab("DIRPRO", string.IsNullOrEmpty(cabeceras[i].direccionDirFacturacion) ? "" : cabeceras[i].direccionDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("DTOPRO", string.IsNullOrEmpty(cabeceras[i].codigoPostalDirFacturacion) ? "" : cabeceras[i].codigoPostalDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("POBPRO", string.IsNullOrEmpty(cabeceras[i].poblacionDirFacturacion) ? "" : cabeceras[i].poblacionDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("CODPROVI", string.IsNullOrEmpty(cabeceras[i].provinciaDirFacturacion) ? "" : cabeceras[i].provinciaDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("CODPAIS", cabeceras[i].pais.ToUpper());
                                        }
                                    }
                                    else
                                    {
                                        naxfactura.set_AsStringCab("nifcli", cabeceras[i].nif);
                                        naxfactura.set_AsStringCab("nomcli", cabeceras[i].nombreIC.ToUpper());
                                        //Clientes genéricos
                                        if (cabeceras[i].clienteGenerico == 1)
                                        {
                                            naxfactura.set_AsStringCab("RAZON", string.IsNullOrEmpty(cabeceras[i].razonSocial) ? cabeceras[i].nombreIC.ToUpper() : cabeceras[i].razonSocial.ToUpper());
                                            naxfactura.set_AsStringCab("NIFCLI", cabeceras[i].nif);
                                            naxfactura.set_AsStringCab("DIRCLI", string.IsNullOrEmpty(cabeceras[i].direccionDirFacturacion) ? "" : cabeceras[i].direccionDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("DTOCLI", string.IsNullOrEmpty(cabeceras[i].codigoPostalDirFacturacion) ? "" : cabeceras[i].codigoPostalDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("POBCLI", string.IsNullOrEmpty(cabeceras[i].poblacionDirFacturacion) ? "" : cabeceras[i].poblacionDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("CODPROVI", string.IsNullOrEmpty(cabeceras[i].provinciaDirFacturacion) ? "" : cabeceras[i].provinciaDirFacturacion.ToUpper());
                                            naxfactura.set_AsStringCab("CODPAIS", cabeceras[i].pais.ToUpper());
                                        }

                                    }

                                    if (cabeceras[i].tipoDoc.ToString() == "CREDIT_NOTE")
                                    {
                                        naxfactura.set_AsStringCab("RECTIFICATIVA", "T");
                                        naxfactura.set_AsStringCab("FECHAINIRECTIFICATIVA", Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy"));
                                        naxfactura.set_AsStringCab("FECHAFINRECTIFICATIVA", Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy"));

                                        //INCORPORADO 20-4-2020 PARA QUE SE REALICE CORRECTAMENTE LA LIQUIDACIÓN DE IVA
                                        naxfactura.set_AsStringCab("TIPORECTIFICACION", "A");

                                        if (!string.IsNullOrEmpty(cabeceras[i].idDocumentoAbonado))
                                        {
                                            string query = docCompras ? "SELECT IDFACC FROM CABEFACC WHERE RPST_ID_FACC =" + cabeceras[i].idDocumentoAbonado : "SELECT IDFACV FROM CABEFACV WHERE RPST_ID_FACV =" + cabeceras[i].idDocumentoAbonado;

                                            string idRect = sql.obtenerCampoTabla(query);

                                            naxfactura.set_AsStringCab("TIPORECTIFICATIVA", "FACTURA");
                                            naxfactura.set_AsStringCab("IDMOTIVORECTIFICACION", "R4");

                                            if (!string.IsNullOrEmpty(idRect))
                                            {
                                                naxfactura.AnadirRectificada(Convert.ToDouble(idRect));
                                            }
                                        }
                                        else
                                        {
                                            naxfactura.set_AsStringCab("TIPORECTIFICATIVA", "PERIODO");
                                        }


                                    }

                                    if (csGlobal.FormaPagoA3)
                                    {
                                        if (!string.IsNullOrEmpty(cabeceras[i].codFormaPago))
                                        {
                                            naxfactura.set_AsStringCab("FORPAG", cabeceras[i].codFormaPago);
                                        }
                                    }

                                    if (csGlobal.DocPagoA3)
                                    {
                                        if (!string.IsNullOrEmpty(cabeceras[i].codDocumentoPago))
                                        {
                                            naxfactura.set_AsStringCab("DOCPAG", cabeceras[i].codDocumentoPago);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].extNumdDoc))
                                    {
                                        //try
                                        //{
                                        //    // Intentar acceder directamente a la propiedad
                                        //    string valorCampo = naxfactura.AsStringCab["FORPAG"];
                                        //    MessageBox.Show("Campo RPST_ID_FACC existe y su valor es: " + valorCampo);
                                        //}
                                        //catch (Exception ex)
                                        //{
                                        //    // Captura el error en caso de que el campo no exista
                                        //    MessageBox.Show("Campo RPST_ID_FACC no existe o hubo un error: " + ex.Message);
                                        //}

                                        naxfactura.set_AsStringCab(docCompras ? "RPST_ID_FACC" : "RPST_ID_FACV", cabeceras[i].extNumdDoc);
                                    }

                                    if (docCompras && cabeceras[i].fraConRetencion)
                                    {
                                        naxfactura.set_AsStringCab("TIPOIRPF", "110");
                                        baseRetencion = Convert.ToDouble(cabeceras[i].baseRetencion.Replace(".", ","));
                                        naxfactura.set_AsFloatCab("BASEIRPFMONEDA", baseRetencion);
                                        naxfactura.set_AsStringCab("ID_CLAVEIRPF", "2");
                                        naxfactura.set_AsStringCab("ID_SUBCLAVEIRPF", "3");
                                        naxfactura.set_AsFloatCab("PORIRPF", Convert.ToDouble(cabeceras[i].porcentajeRetencion));
                                    }
                                    else if (cabeceras[i].fraConRetencion)
                                    {
                                        naxfactura.set_AsFloatCab("PORIRPF", Convert.ToDouble(cabeceras[i].porcentajeRetencion));
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].portes))
                                    {
                                        naxfactura.set_AsFloatCab("BASEPORMONEDA", Convert.ToDouble(cabeceras[i].portes));
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].idDireccionEnvioA3ERP) && !docCompras)
                                    {
                                        naxfactura.set_AsStringCab("IDDIRENT", cabeceras[i].idDireccionEnvioA3ERP);
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].transportista))
                                    {
                                        naxfactura.set_AsStringCab("CODTRA", cabeceras[i].transportista);
                                    }

                                    if (!cabeceras[i].repercusionesContables)
                                    {
                                        naxfactura.set_AsStringCab("CONREPER", "F");
                                        naxfactura.set_AsStringCab("CONVENCIM", "F");
                                    }


                                    if (csGlobal.ticketBaiActivo && cabeceras[i].ticketBaiFraBorrador && !docCompras)
                                    {
                                        naxfactura.set_AsStringCab("CONREPERORIGINALFACTURABORRADOR", "T");
                                        naxfactura.set_AsStringCab("CONVENCIMORIGINALFACTURABORRADOR", "T");
                                        naxfactura.set_AsStringCab("SERIE", cabeceras[i].serieDoc);
                                        naxfactura.set_AsStringCab("SERIEORIGINALFACTURABORRADOR", cabeceras[i].serieDoc);
                                        naxfactura.set_AsStringCab("CONREPER", "T");
                                        naxfactura.set_AsStringCab("CONVENCIM", "T");
                                        naxfactura.set_AsStringCab("RPST_ID_FACV", cabeceras[i].extNumdDoc);
                                    }
                                    else
                                    {
                                        naxfactura.set_AsStringCab("SERIE", cabeceras[i].serieDoc);
                                        naxfactura.set_AsStringCab("NUMDOC", numeroDocA3);
                                    }

                                    if (!docCompras)
                                    {
                                        naxfactura.set_AsStringCab("CODREP", cabeceras[i].agenteComercial);
                                    }
                                    string observaciones = cabeceras[i].comentariosCabecera + "\n" + cabeceras[i].comentariosPie;
                                    naxfactura.set_AsStringCab("OBSERVACIONES", observaciones);

                                    for (int ii = 0; ii < lineas.Count(); ii++)
                                    {
                                        if (lineas[ii] != null)
                                        {
                                            documentoLinea = lineas[ii].numCabecera;

                                            if (documentoLinea == cabeceras[i].extNumdDoc)
                                            {
                                                string numLinDocumento = ii.ToString();
                                                articulo = lineas[ii].codigoArticulo;
                                                naxfactura.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad.Replace(".", ",")));

                                                //Gestión de Errores
                                                errorNumLinea = (ii + 1).ToString();
                                                errorCodart = lineas[ii].codigoArticulo;
                                                errorArticulo = lineas[ii].descripcionArticulo;
                                                //Fin Gestión de Errores


                                                if (articulo == "0" || !string.IsNullOrEmpty(lineas[ii].descripcionArticulo))
                                                {
                                                    naxfactura.set_AsStringLin("DESCLIN", lineas[ii].descripcionArticulo);
                                                }

                                                if (!string.IsNullOrEmpty(lineas[ii].tipoIVA))
                                                {
                                                    naxfactura.set_AsStringLin("TIPIVA", lineas[ii].tipoIVA);
                                                }


                                                precioArticulo = lineas[ii].price;
                                                naxfactura.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                naxfactura.set_AsStringCab("referencia", cabeceras[i].referencia);

                                                naxfactura.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));
                                                naxfactura.set_AsFloatLin("Desc2", Convert.ToDouble(lineas[ii].descuento2));

                                                if (csGlobal.analitica == "SI")
                                                {
                                                    naxfactura.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                                    naxfactura.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                                    naxfactura.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);

                                                }

                                                if (!string.IsNullOrEmpty(lineas[ii].centroCoste1))
                                                {
                                                    naxfactura.set_AsStringLin("CentroCoste", lineas[ii].centroCoste1);
                                                }
                                                if (!string.IsNullOrEmpty(lineas[ii].centroCoste2))
                                                {
                                                    naxfactura.set_AsStringLin("CentroCoste2", lineas[ii].centroCoste2);
                                                }
                                                if (!string.IsNullOrEmpty(lineas[ii].centroCoste3))
                                                {
                                                    naxfactura.set_AsStringLin("CentroCoste3", lineas[ii].centroCoste3);
                                                }


                                                if (!string.IsNullOrEmpty(lineas[ii].cuentaContable))
                                                {
                                                    naxfactura.set_AsStringLin("CTACONL", lineas[ii].cuentaContable);
                                                }

                                                //CARACTER CUOTA DEDUCIBLE. NO DEDUCIBLE O PRORRATA
                                                if (csGlobal.caracterCuota && docCompras)
                                                {
                                                    naxfactura.set_AsStringLin("CARACTER", lineas[ii].caracterCuota);
                                                }

                                                naxfactura.AnadirLinea();
                                            }
                                        }
                                    }

                                    decimal documento = new decimal();

                                    if (csGlobal.ticketBaiActivo && cabeceras[i].ticketBaiFraBorrador && !docCompras)
                                    {
                                        documento = naxfactura.AnadeComoBorrador();
                                    }
                                    else
                                    {
                                        documento = naxfactura.Anade();
                                    }

                                    ///funcion verificar importes
                                    ///
                                    if (documento != 0 && csGlobal.modeAp.ToUpper() == "REPASAT")
                                    {
                                        //checkFactura(cabeceras[i], documento.ToString(), docCompras);
                                    }

                                    naxfactura.Acabar();

                                    // Actualizar con el documento en Repasat
                                    if (documento != 0 && csGlobal.modeAp.ToUpper() == "REPASAT")
                                    {
                                        string tipoObjeto = docCompras ? "purchaseinvoices" : "saleinvoices";
                                        csRepasatWebService rpstWS = new csRepasatWebService();
                                        rpstWS.actualizarDocumentoRepasat(tipoObjeto, "codExternoDocumento", cabeceras[i].extNumdDoc, documento.ToString());
                                        documentosGenerados++;
                                    }

                                }
                            }

                            if (csGlobal.docDestino.ToUpper() == "PEDIDO")
                            {
                                errorTipoDoc = csGlobal.docDestino.ToUpper();

                                naxPedido.Iniciar();
                                naxPedido.OmitirMensajes = true;
                                naxPedido.ActivarAlarmaCab = false;

                                if (objTercero[i] != null)
                                {
                                    if (usarClienteGenerico.ToUpper() == "SI")
                                    {
                                        codIC = csGlobal.clienteGenerico;
                                    }
                                    else
                                    {
                                        if (docCompras) // Proveedores
                                        {
                                            //consulta = "SELECT COUNT(IDFACC) FROM CABEFACC WHERE IDFACC = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                        else // Clientes
                                        {
                                            consulta = "SELECT COUNT(IDFACV) FROM CABEFACV WHERE IDFACV = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(cabeceras[i].numDocA3))
                                {
                                    fechaSync = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    numeroDocA3 = cabeceras[i].numDocA3; // number
                                    numeroDocPs = cabeceras[i].extNumdDoc;
                                    naxPedido.Nuevo(fechaSync, cabeceras[i].codIC.Trim(), docCompras);

                                    //Gestión de Errores
                                    errorNumDocumento = cabeceras[i].numDocA3;
                                    errorNumSerie = cabeceras[i].serieDoc;
                                    errorCodCuenta = cabeceras[i].codIC.Trim();
                                    errorNomCuenta = cabeceras[i].nombreIC.ToUpper();
                                    //Fin Gestión de Errores

                                    if (docCompras)
                                    {
                                        naxPedido.set_AsStringCab("nifpro", cabeceras[i].nif);
                                        naxPedido.set_AsStringCab("nompro", cabeceras[i].nombreIC.ToUpper());
                                    }
                                    else
                                    {
                                        naxPedido.set_AsStringCab("nifcli", cabeceras[i].nif);
                                        naxPedido.set_AsStringCab("nomcli", cabeceras[i].nombreIC.ToUpper());
                                    }
                                    if (csGlobal.FormaPagoA3)
                                    {

                                        if (!string.IsNullOrEmpty(cabeceras[i].codFormaPago))
                                        {
                                            naxPedido.set_AsStringCab("FORPAG", cabeceras[i].codFormaPago);
                                        }
                                    }
                                    if (csGlobal.DocPagoA3)
                                    {
                                        if (!string.IsNullOrEmpty(cabeceras[i].codDocumentoPago))
                                        {
                                            naxPedido.set_AsStringCab("DOCPAG", cabeceras[i].codDocumentoPago);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].portes))
                                    {
                                        naxPedido.set_AsFloatCab("BASEPORMONEDA", Convert.ToDouble(cabeceras[i].portes));
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].idDireccionEnvioA3ERP))
                                    {
                                        naxPedido.set_AsStringCab("IDDIRENT", cabeceras[i].idDireccionEnvioA3ERP);
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].transportista))
                                    {
                                        naxPedido.set_AsStringCab("CODTRA", cabeceras[i].transportista);
                                    }

                                    naxPedido.set_AsStringCab("SERIE", cabeceras[i].serieDoc);
                                    naxPedido.set_AsStringCab("NUMDOC", numeroDocA3);
                                    if (!docCompras)
                                    {
                                        naxPedido.set_AsStringCab("CODREP", cabeceras[i].agenteComercial);
                                    }
                                    string observaciones = cabeceras[i].comentariosCabecera + "\n" + cabeceras[i].comentariosPie;
                                    naxPedido.set_AsStringCab("OBSERVACIONES", observaciones);



                                    for (int ii = 0; ii < lineas.Count(); ii++)
                                    {
                                        if (lineas[ii] != null)
                                        {
                                            documentoLinea = lineas[ii].numCabecera;

                                            if (documentoLinea == cabeceras[i].extNumdDoc)
                                            {
                                                string numLinDocumento = ii.ToString();
                                                if (string.IsNullOrEmpty(lineas[ii].codigoArticulo))
                                                { articulo = "0"; }
                                                else
                                                { articulo = lineas[ii].codigoArticulo; }


                                                naxPedido.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad.Replace(".", ",")));

                                                //Gestión de Errores
                                                errorNumLinea = (ii + 1).ToString();
                                                errorCodart = lineas[ii].codigoArticulo;
                                                errorArticulo = lineas[ii].descripcionArticulo;
                                                //Fin Gestión de Errores

                                                //Valor para comprovar si recogemos los precios desde A3
                                                if (csGlobal.preciosA3)
                                                {
                                                    precioArticulo = lineas[ii].price;
                                                    naxPedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                    naxPedido.set_AsStringCab("referencia", cabeceras[i].referencia);
                                                    naxPedido.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));
                                                    naxPedido.set_AsFloatLin("Desc2", Convert.ToDouble(lineas[ii].descuento2));
                                                }

                                                if (csGlobal.analitica == "SI")
                                                {
                                                    naxPedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                                    naxPedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                                    naxPedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                                                }
                                                naxPedido.AnadirLinea();
                                            }
                                        }
                                    }

                                    decimal documento = naxPedido.Anade();
                                    naxPedido.Acabar();

                                    // Actualizar con el documento en Repasat
                                    if (documento != 0)
                                    {
                                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                                        {
                                            csRepasatWebService rpstWS = new csRepasatWebService();
                                            if (!docCompras)
                                            {
                                                rpstWS.actualizarDocumentoRepasat("saleorders", "codExternoDocumento", cabeceras[i].extNumdDoc, documento.ToString());
                                            }
                                            else
                                            {
                                                rpstWS.actualizarDocumentoRepasat("purchaseorders", "codExternoDocumento", cabeceras[i].extNumdDoc, documento.ToString());
                                            }
                                            documentosGenerados++;
                                        }
                                    }
                                }
                            }

                            if (csGlobal.docDestino.ToUpper() == "ALBARAN")
                            {
                                errorTipoDoc = csGlobal.docDestino.ToUpper();

                                naxAlbaran.Iniciar();
                                naxAlbaran.OmitirMensajes = true;
                                naxAlbaran.ActivarAlarmaCab = false;

                                if (objTercero[i] != null)
                                {
                                    if (usarClienteGenerico.ToUpper() == "SI")
                                    {
                                        codIC = csGlobal.clienteGenerico;
                                    }
                                    else
                                    {
                                        if (docCompras) // Proveedores
                                        {
                                            //consulta = "SELECT COUNT(IDFACC) FROM CABEFACC WHERE IDFACC = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                        else // Clientes
                                        {
                                            consulta = "SELECT COUNT(IDFACV) FROM CABEFACV WHERE IDFACV = '" + cabeceras[i].numDocA3 + "'";
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(cabeceras[i].numDocA3))
                                {
                                    fechaSync = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    numeroDocA3 = cabeceras[i].numDocA3; // number
                                    numeroDocPs = cabeceras[i].extNumdDoc;
                                    naxAlbaran.Nuevo(fechaSync, cabeceras[i].codIC.Trim(), docCompras);

                                    //Gestión de Errores
                                    errorNumDocumento = cabeceras[i].numDocA3;
                                    errorNumSerie = cabeceras[i].serieDoc;
                                    errorCodCuenta = cabeceras[i].codIC.Trim();
                                    errorNomCuenta = cabeceras[i].nombreIC.ToUpper();
                                    //Fin Gestión de Errores

                                    if (docCompras)
                                    {
                                        naxAlbaran.set_AsStringCab("nifpro", cabeceras[i].nif);
                                        naxAlbaran.set_AsStringCab("nompro", cabeceras[i].nombreIC.ToUpper());
                                    }
                                    else
                                    {
                                        naxAlbaran.set_AsStringCab("nifcli", cabeceras[i].nif);
                                        naxAlbaran.set_AsStringCab("nomcli", cabeceras[i].nombreIC.ToUpper());
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].codFormaPago))
                                    {
                                        naxAlbaran.set_AsStringCab("FORPAG", cabeceras[i].codFormaPago);
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].codDocumentoPago))
                                    {
                                        naxAlbaran.set_AsStringCab("DOCPAG", cabeceras[i].codDocumentoPago);
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].portes))
                                    {
                                        naxAlbaran.set_AsFloatCab("BASEPORMONEDA", Convert.ToDouble(cabeceras[i].portes));
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].idDireccionEnvioA3ERP))
                                    {
                                        naxAlbaran.set_AsStringCab("IDDIRENT", cabeceras[i].idDireccionEnvioA3ERP);
                                    }

                                    if (!string.IsNullOrEmpty(cabeceras[i].transportista))
                                    {
                                        naxAlbaran.set_AsStringCab("CODTRA", cabeceras[i].transportista);
                                    }

                                    naxAlbaran.set_AsStringCab("SERIE", cabeceras[i].serieDoc);
                                    naxAlbaran.set_AsStringCab("NUMDOC", numeroDocA3);
                                    if (!docCompras)
                                    {
                                        naxAlbaran.set_AsStringCab("CODREP", cabeceras[i].agenteComercial);
                                    }
                                    string observaciones = cabeceras[i].comentariosCabecera + "\n" + cabeceras[i].comentariosPie;
                                    naxAlbaran.set_AsStringCab("OBSERVACIONES", observaciones);



                                    for (int ii = 0; ii < lineas.Count(); ii++)
                                    {
                                        if (lineas[ii] != null)
                                        {
                                            documentoLinea = lineas[ii].numCabecera;

                                            if (documentoLinea == cabeceras[i].extNumdDoc)
                                            {
                                                string numLinDocumento = ii.ToString();
                                                if (string.IsNullOrEmpty(lineas[ii].codigoArticulo))
                                                { articulo = "0"; }
                                                else
                                                { articulo = lineas[ii].codigoArticulo; }


                                                naxAlbaran.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad.Replace(".", ",")));

                                                //Gestión de Errores
                                                errorNumLinea = (ii + 1).ToString();
                                                errorCodart = lineas[ii].codigoArticulo;
                                                errorArticulo = lineas[ii].descripcionArticulo;
                                                //Fin Gestión de Errores

                                                //Valor para comprovar si recogemos los precios desde A3
                                                if (csGlobal.preciosA3)
                                                {
                                                    precioArticulo = lineas[ii].price;
                                                    naxAlbaran.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                    naxAlbaran.set_AsStringCab("referencia", cabeceras[i].referencia);
                                                    naxAlbaran.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));
                                                    naxAlbaran.set_AsFloatLin("Desc2", Convert.ToDouble(lineas[ii].descuento2));
                                                }

                                                if (csGlobal.analitica == "SI")
                                                {
                                                    naxAlbaran.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                                    naxAlbaran.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                                    naxAlbaran.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                                                }
                                                naxAlbaran.AnadirLinea();
                                            }
                                        }
                                    }

                                    decimal documento = naxAlbaran.Anade();
                                    naxAlbaran.Acabar();

                                    // Actualizar con el documento en Repasat
                                    if (documento != 0)
                                    {
                                        if (csGlobal.modeAp.ToUpper() == "REPASAT")
                                        {
                                            csRepasatWebService rpstWS = new csRepasatWebService();
                                            if (!docCompras)
                                            {
                                                rpstWS.actualizarDocumentoRepasat("saledeliverynotes", "codExternoDocumento", cabeceras[i].extNumdDoc, documento.ToString());
                                            }
                                            else
                                            {
                                                rpstWS.actualizarDocumentoRepasat("purchasedeliverynotes", "codExternoDocumento", cabeceras[i].extNumdDoc, documento.ToString());
                                            }
                                            documentosGenerados++;
                                        }
                                    }
                                }
                            }
                        }
                        
                        catch (Exception ex)
                        {
                            //NACHO 03/07/24: Variable para almacenar el tipo de error que devuelve la función ObtenerIdError
                            //int idError = new GestionErrores.csGestionErrores().ObtenerIdError(ex);
                            string detalleError = "Documento:" + errorNumSerie + "//" + errorNumDocumento + "\n" +
                                                  "Cuenta: (" + errorCodCuenta + ") " + errorNomCuenta + "\n" +
                                                  "Linea: " + errorNumLinea + "\n" +
                                                  "Artículo: " + errorCodart + " - " + errorArticulo + "\n\n";
                            //NACHO 03/07/24: Modo manual
                            if (csGlobal.modoManual)
                            {
                                //if (idError == 1)
                                //{
                                //    MessageBox.Show("Problema relacionado con la DIRECCIÓN DE ENTREGA, revisa que tenga la misma dirección/id de dirección en REPASAT y A3ERP. \n \n \nDETALLES: \n" + detalleError, "Error Sincronización", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //}
                                //else if (idError == 2)
                                //{

                                //}
                             
                                detalleError = detalleError + ex.Message;
                                MessageBox.Show(detalleError, "Error Sincronización", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            //NACHO 03/07/24: Modo automático
                            //else
                            //{
                            //    string asunto = "Error en sincronización automática";
                            //    string cuerpo = "Se ha producido un error durante la sincronización automática.\n\nDETALLES:\n" + detalleError;

                            //    if (idError == 1)
                            //    {
                            //        cuerpo = "Problema relacionado con la DIRECCIÓN DE ENTREGA, revisa que tenga la misma dirección/id de dirección en REPASAT y A3ERP. \n \n \nDETALLES: \n" + detalleError;
                            //    }
                            //    else if (idError == 2)
                            //    {

                            //    }
                            //    //NACHO 03/07/24: Llama a al función de enviar correo con los valores de asunto y cuerpo según el tipo de error
                            //    new GestionErrores.csGestionErrores().EnviarCorreoError(asunto, cuerpo);
                            //}

                            Program.guardarErrorFichero(ex.ToString());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string detalleError = "Documento:" + errorNumSerie + "//" + errorNumDocumento + "\n" +
                                      "Cuenta: (" + errorCodCuenta + ") " + errorNomCuenta + "\n" +
                                      "Linea: " + errorNumLinea + "\n" +
                                      "Artículo: " + errorCodart + " - " + errorArticulo + "\n\n";
                if (csGlobal.modoManual)
                {
                    detalleError = detalleError + ex.Message;
                    MessageBox.Show(detalleError, "Error Sincronización", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Program.guardarErrorFichero(ex.ToString());
            }

            return documentosGenerados;
        }


        //25-9-2020 comento porque no se que pinta esto aquí (LLUIS)
        //class Actual
        //{
        //    public static Objetos.csCabeceraDoc cab
        //    {
        //        get;
        //        set;
        //    }

        //    public static Objetos.csDomBanca domi
        //    {
        //        get;
        //        set;
        //    }
        //}

            /// <summary>
            /// Edición de pedidos de Venta en A3ERP
            /// </summary>
            /// <param name="idDocumento"></param>
            /// <param name="cabeceras"></param>
            /// <param name="lineas"></param>
        public void editarDocA3Objeto(string idDocumento, Objetos.csCabeceraDoc[] cabeceras, Objetos.csLineaDocumento[] lineas) {

            csGlobal.docDestino = "Pedido";
            a3ERPActiveX.Pedido naxPedido = new a3ERPActiveX.Pedido();
            naxPedido.Iniciar();
            naxPedido.OmitirMensajes = true;
            decimal documentoA3 = Convert.ToDecimal(idDocumento);

            csSqlConnects sql = new csSqlConnects();
            DataTable lineasDocs = new DataTable();
            lineasDocs = sql.obtenerDatosSQLScript("select IDLIN, IDPEDV,NUMLINPED,ORDLIN from LINEPEDI WHERE IDPEDV=" + cabeceras[0].external_id);

            naxPedido.Modifica(Convert.ToDecimal(cabeceras[0].external_id), false);

            foreach (DataRow linea in lineasDocs.Rows)
            {
                naxPedido.BorrarLinea(Convert.ToDecimal(linea["NUMLINPED"].ToString()));
            }

            foreach (Objetos.csLineaDocumento linea in lineas)
            {
                naxPedido.NuevaLinea();
                naxPedido.set_AsStringLin("CODART", linea.codigoArticulo);
                naxPedido.set_AsStringLin("DESCLIN", linea.descripcionArticulo);
                naxPedido.set_AsStringLin("UNIDADES", "1");
                naxPedido.set_AsStringLin("REMOTOID", linea.numeroLinea);
            }
            naxPedido.Anade();
           
            naxPedido.Acabar();

            ///Actualizo en FileMaker el pedido creado
            if (csGlobal.conexionDB.Contains("PUNTES"))
            {
                Puntes.csPuntes puntes = new Puntes.csPuntes();
                puntes.updateDocA3Project(cabeceras[0].external_id, idDocumento.ToString());
            }
        }



        /// <summary>
        ///   ADMAN
        ///   Número del documento
        ///   campo: IDPEDV
        ///   tabla: CABEPEDV 
        /// </summary>
        /// <param name="cabeceras"></param>
        /// <param name="lineas"></param>
        /// <param name="docCompras"></param>
        /// <param name="usarClienteGenerico"></param>
        /// <param name="objTercero"></param>
        /// <param name="objDomiciliaciones"></param>
        /// <returns></returns>
        public decimal generarDocA3Objeto(Objetos.csCabeceraDoc[] cabeceras, Objetos.csLineaDocumento[] lineas, bool docCompras, string usarClienteGenerico, Objetos.csTercero[] objTercero = null, Objetos.csDomBanca[] objDomiciliaciones = null)
        {
            csa3erpTercero a3erpTercero = new csa3erpTercero();
            csSqlConnects sql = new csSqlConnects();
            string codfamtallasH = "";
            string codfamtallasV = "";
            string codtallaH = "";
            string codtallaV = "";
            double precioArticulo = 0;
            string codIC = "";
            string documentoLinea = "";
            string articulo = "";
            string numeroDocPs = "";
            string numeroDocA3 = "";
            string fechaSync = "";
            string fechaContable = "";
            string portesGratis = "";
            string dtoPpago = "";
            string porDtoProntoPago = "0";
            bool descuentoCabecera = false;
            double porDescuentoCab = 0;
            string numPedidoV = "";
            string numDir = "";
            string consulta = "";

       
            frRita web = new frRita();

            try
            {

                #region Facturas
                MessageBox.Show("Inicio objeto factura");
                a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();

                for (int i = 0; i < cabeceras.Count(); i++)
                {
                    
                    if (cabeceras[i] != null)
                    {
                        if (csGlobal.docDestino == "Factura")
                        {
                            naxfactura.Iniciar();
                            naxfactura.OmitirMensajes = true;

                            if (objTercero != null)
                            {
                                if (usarClienteGenerico.ToUpper() == "SI")
                                {
                                    codIC = csGlobal.clienteGenerico;
                                }
                                else
                                {
                                    if (docCompras) // Proveedores
                                    {
                                        // Obtener de la BD
                                        if (csGlobal.conexionDB.Contains("ADMAN"))
                                        {
                                            if (sql.consultaExiste("param1", "__proveed", objTercero[i].permalinkFiscalIdentity))
                                            {
                                                codIC = sql.obtenerCampoTabla("select codpro from __proveed where param1 = '" + objTercero[i].permalinkFiscalIdentity + "'").Trim();
                                                cabeceras[i].codIC = codIC.Trim();
                                            }
                                            else // Crear
                                            {
                                                //codIC = a3erpTercero.crearProveedoresA3Objects(objTercero[i],null, objDomiciliaciones[i])[0];
                                                cabeceras[i].codIC = (codIC.Trim() == "") ? objTercero[i].codIC : codIC.Trim();
                                                if (string.IsNullOrEmpty(cabeceras[i].codIC))
                                                {
                                                    codIC = a3erpTercero.crearProveedorA3Objects(objTercero[i]);
                                                }
                                            }
                                        }
                                        else if (cabeceras[i].codIC == "")
                                        {
                                            codIC = a3erpTercero.crearProveedoresA3Objects(objTercero);
                                            cabeceras[i].codIC = codIC.Trim();
                                        }



                                        consulta = "SELECT COUNT(IDFACC) FROM CABEFACC WHERE IDFACC = '" + cabeceras[i].numDocA3 + "'";
                                    }
                                    else // Clientes
                                    {
                                        // Obtener de la BD
                                        if (sql.consultaExiste("param1", "__clientes", objTercero[i].permalinkFiscalIdentity))
                                        {
                                            codIC = sql.obtenerCampoTabla("select codcli from __clientes where param1 = '" + objTercero[i].permalinkFiscalIdentity + "'").Trim();
                                            cabeceras[i].codIC = codIC.Trim();
                                        }
                                        else // Crear
                                        {
                                            codIC = a3erpTercero.crearClienteA3(objTercero[i], objDomiciliaciones[i]);
                                            cabeceras[i].codIC = codIC.Trim();
                                        }

                                        consulta = "SELECT COUNT(IDFACV) FROM CABEFACV WHERE IDFACV = '" + cabeceras[i].numDocA3 + "'";
                                    }

                                    if (csGlobal.nodeTienda.ToUpper().Contains("ADMAN"))
                                    {
                                        // Actualizamos el id del proveedor / cliente
                                        if (!csGlobal.modeDebug)
                                        {
                                            if (objTercero[i].permalinkFiscalIdentity != null && codIC != null && objTercero[i].permalinkFiscalIdentity != null)
                                            {
                                                web.updateIdAdman("fiscal_identities", objTercero[i].permalinkFiscalIdentity, codIC);
                                            }
                                        }
                                    }
                                }
                            }

                            // Si la factura no existe, seguimos. Y si es diferente de "" igualç
                            // De lo contrario, pasará de largo a la siguiente factura

                            if (objTercero == null || sql.obtenerCampoTabla(consulta).Equals("0"))
                            {

                                //25-9-2020 Comento estas lineas de Actual, porque no sé que pintan aquí
                                //Actual.cab = cabeceras[i];
                                //if (objDomiciliaciones != null)
                                //    Actual.domi = objDomiciliaciones[i];

                                // naxpedido.Nuevo(documentos[i, 0], csGlobal.clienteGenerico, false);
                                if (cabeceras[i].fecha == null || cabeceras[i].fecha == "")
                                    if (cabeceras[i].fechaDoc.ToString() == "")
                                    {
                                        fechaSync = "01/01/2016";
                                    }
                                    else
                                    {
                                        fechaSync = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    }
                                else
                                    if (cabeceras[i].fecha.ToString() == "")
                                    {
                                        fechaSync = Convert.ToDateTime(cabeceras[i].fechaDoc).ToString("dd/MM/yyyy");
                                    }
                                    else
                                    {
                                        fechaSync = Convert.ToDateTime(cabeceras[i].fecha).ToString("dd/MM/yyyy");
                                    }
                                //numeroDocA3 = CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper();
                                //numeroDocA3 = "999002";
                                numeroDocA3 = cabeceras[i].numDocA3; // number
                                numeroDocPs = cabeceras[i].extNumdDoc;
                                if (fechaSync == "01/01/0001")
                                    fechaSync = "01/01/2015";

                                //Creamos la factura
                                if (csGlobal.conexionDB == "TRS")
                                {
                                    naxfactura.Nuevo(fechaSync, cabeceras[i].codIC.Trim(), docCompras, false, false, false);
                                }
                                else
                                {
                                    naxfactura.Nuevo(fechaSync, cabeceras[i].codIC.Trim(), docCompras, false, true, true);
                                }



                                if (docCompras)
                                {
                                    naxfactura.set_AsStringCab("nifpro", cabeceras[i].nif);
                                    naxfactura.set_AsStringCab("nompro", cabeceras[i].nombreIC.ToUpper());
                                }
                                else
                                {
                                    naxfactura.set_AsStringCab("nifcli", cabeceras[i].nif);
                                    naxfactura.set_AsStringCab("nomcli", cabeceras[i].nombreIC.ToUpper());
                                }

                                if (!cabeceras[i].captio)
                                {
                                    if (cabeceras[i].serieDoc != null)
                                    {
                                        naxfactura.set_AsStringCab("serie", cabeceras[i].serieDoc);
                                    }
                                    else
                                    {
                                        naxfactura.set_AsStringCab("serie", csGlobal.serie);
                                    }
                                }
                                else

                                    naxfactura.set_AsStringCab("serie", "CAPTIO");


                                //añado por el módulo de generación de documentos de A3
                                if (cabeceras[i].numDocA3 != "")
                                {
                                    naxfactura.set_AsStringCab("numdoc", numeroDocA3);
                                }
                                if (csGlobal.nodeTienda.ToUpper().Contains("ADMAN"))
                                {
                                    if (cabeceras[i].tipoDoc.ToUpper() == "CREDIT_NOTE")
                                    {
                                        naxfactura.set_AsStringCab("RECTIFICATIVA", "T");
                                        naxfactura.set_AsStringCab("FECHAINIRECTIFICATIVA", cabeceras[i].periodoDesde.ToShortDateString());
                                        naxfactura.set_AsStringCab("FECHAFINRECTIFICATIVA", cabeceras[i].periodoHasta.ToShortDateString());
                                        naxfactura.set_AsStringCab("serie", cabeceras[i].serieDoc);
                                    }
                                }

                                if (cabeceras[i].regIva != null )
                                {
                                    naxfactura.set_AsStringCab("REGIVA", cabeceras[i].regIva);

                                }

                                //Incorporado para la fundació puntcat 24/3/2020
                                if (!string.IsNullOrEmpty(cabeceras[i].codDocumentoPago) && csGlobal.modeAp.ToUpper()== "SINGLE")
                                {
                                    naxfactura.set_AsStringCab("DOCPAG", cabeceras[i].codDocumentoPago);
                                }

                                for (int ii = 0; ii < lineas.Count(); ii++)
                                {
                                    if (lineas[ii] != null)
                                    {
                                        documentoLinea = lineas[ii].numCabecera;

                                        if (documentoLinea == cabeceras[i].referencia || documentoLinea == cabeceras[i].permalink || documentoLinea == cabeceras[i].extNumdDoc)
                                        {
                                            string numLinDocumento = ii.ToString();
                                            articulo = lineas[ii].codigoArticulo;
                                            naxfactura.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad));


                                                if (!string.IsNullOrEmpty(lineas[ii].precio))
                                                {
                                                    if (objTercero != null)
                                                        precioArticulo = lineas[ii].price;
                                                    else
                                                        precioArticulo = string.IsNullOrEmpty(lineas[ii].precio) ? 0 : Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                }
                                                if (!string.IsNullOrEmpty(lineas[ii].precio))
                                                {
                                                    naxfactura.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                }
                                            

                                            naxfactura.set_AsStringCab("referencia", cabeceras[i].referencia);
                                            //Añadimos para gestionar si es para Adman o no
                                            if (csGlobal.conexionDB.ToUpper().Contains("ADMAN")){
                                                if (objTercero != null)
                                                {
                                                    naxfactura.set_AsStringLin("Desclin", documentoLinea);
                                                }
                                                else
                                                { // Captio
                                                    naxfactura.set_AsStringLin("Desclin", lineas[ii].descripcionArticulo);
                                                    naxfactura.set_AsStringLin("TEXTO", lineas[ii].param1);
                                                }
                                            }

                                                naxfactura.set_AsStringLin("Desclin", lineas[ii].descripcionArticulo);

                                            if (csGlobal.databaseA3 == "ADMANMEDIA_FR" && cabeceras[i].tipoIva == "Exento")
                                                naxfactura.set_AsStringLin("TIPIVA", "EXE");
                                          
                                            naxfactura.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));

                                            if(lineas[ii].tipoIVA!=null)
                                                naxfactura.set_AsStringLin("TIPIVA", lineas[ii].tipoIVA);
                                            if (csGlobal.analitica == "SI")
                                            {
                                                naxfactura.set_AsStringLin("CentroCoste", lineas[ii].codProyecto != null ? lineas[ii].codProyecto : csGlobal.nivel1Analitica);
                                                //naxfactura.set_AsStringLin("CentroCoste2", lineas[ii].codProyecto != null ? lineas[ii].codProyecto : csGlobal.nivel2Analitica);
                                                // naxfactura.set_AsStringLin("CentroCoste3", lineas[ii].codProyecto != null ? lineas[ii].codProyecto : csGlobal.nivel3Analitica);
                                            }

                                            if (!string.IsNullOrEmpty(lineas[ii].cuentaContable) && csGlobal.conexionDB.Contains("FUNDACIOCAT"))
                                            {
                                                naxfactura.set_AsStringLin("CTACONL", lineas[ii].cuentaContable);
                                            }

                                            //Si hay números de Serie
                                            if (lineas[ii].numSerie != "" && lineas[ii].numSerie != null)
                                            {
                                                naxfactura.AnadirDetalle(Convert.ToDouble(lineas[ii].cantidad), lineas[ii].numSerie, "", "", "");
                                            }

                                            //Program.guardarErrorFichero(lineas[ii].codigoArticulo + " añadido");
                                            naxfactura.AnadirLinea();
                                        }
                                    }
                                }

                                decimal documento = naxfactura.Anade();
                                naxfactura.Acabar();

                                // ADMAN - Actualizamos el id de la factura
                               /* if (!csGlobal.modeDebug)
                                {
                                    if (documento != 0 && objTercero != null)
                                    {
                                        web.updateIdAdman(cabeceras[i].objeto, cabeceras[i].permalink, documento.ToString());
                                    }
                                }*/

                                // Prestashop
                                if (csGlobal.databasePS != "" && objTercero != null)
                                {
                                    csMySqlConnect mySql = new csMySqlConnect();
                                    mySql.insertarDocumentosA3Generados(numeroDocPs, Convert.ToString(documento), fechaSync);
                                }

                                //return documento; WTF!!!!! ECHA FUERA DEL FOR
                            }
                        }

                    #endregion

                        #region Pedidos
                        // GENERACIÓN DE PEDIDO DE VENTAS
                        else if (csGlobal.docDestino == "Pedido")
                        {
                            MessageBox.Show("Inicio pedido");
                            DataTable CabeceraDocsA3 = new DataTable();
                            a3ERPActiveX.Maestro clientes = new Maestro();
                            a3ERPActiveX.Pedido naxpedido = new a3ERPActiveX.Pedido();
                            naxpedido.Iniciar();

                            naxpedido.OmitirMensajes = true;
                            naxpedido.ValidarArtBloqueado = false;
                            naxpedido.ActivarAlarmaLin = false;

                            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                            {
                                codIC = csGlobal.clienteGenerico;
                            }
                            else
                            {
                                //7-7-2017 Comento porque CabeceraDocsA3 no tiene nada
                                //if (sql.consultaExiste("codcli", "__clientes", CabeceraDocsA3.Rows[i]["kls_a3erp_id"].ToString()))
                                if (sql.consultaExiste("codcli", "__clientes", cabeceras[i].codIC))
                                {
                                    codIC = cabeceras[i].codIC;
                                }
                                else
                                {
                                    //cliente = crearClienteA3(CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper(), CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper(), CabeceraDocsA3.Rows[i]["kls_a3erp_id"].ToString());
                                    codIC = a3erpTercero.crearClientesA3V2(CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper(), CabeceraDocsA3.Rows[i]["id_customer"].ToString().ToUpper());

                                }
                            }

                            fechaSync = cabeceras[i].fechaDoc.ToShortDateString();
                            numeroDocA3 = cabeceras[i].numDocA3;
                            numeroDocPs = cabeceras[i].extNumdDoc;
                            numPedidoV = ""; ;
                            naxpedido.Nuevo(fechaSync, codIC, false);

                            if (!string.IsNullOrEmpty(cabeceras[i].nif))
                            {
                                naxpedido.set_AsStringCab("nifcli", cabeceras[i].nif);
                            }

                            if (string.IsNullOrEmpty(cabeceras[i].serieDoc))
                            {
                                naxpedido.set_AsStringCab("serie", csGlobal.serie);
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("serie", cabeceras[i].serieDoc);
                            }

                            if (!string.IsNullOrEmpty(cabeceras[i].nombreIC))
                            {
                                naxpedido.set_AsStringCab("nomcli", cabeceras[i].nombreIC);
                            }

                            //Referencia del pedido, depende de si está instalado el módulo
                            if (csGlobal.moduloReferenciaPedidos)
                            {
                                naxpedido.set_AsStringCab("referencia", CabeceraDocsA3.Rows[i]["Ref_Cliente"].ToString().ToUpper());
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("referencia", (string.IsNullOrEmpty(cabeceras[i].referencia) ? "": cabeceras[i].referencia.ToUpper()));
                            }

                            //Para informar de la Razón verifico si está informado
                            if (!string.IsNullOrEmpty(cabeceras[i].razonSocial))
                            {
                                naxpedido.set_AsStringCab("razon", cabeceras[i].razonSocial);
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("razon", cabeceras[i].nombreIC + " " + cabeceras[i].apellidoCliente);
                            }


                            //}

                            csMySqlConnect mySql = new csMySqlConnect();

                            DataTable LineasDocsA3 = new DataTable(); // para borrar cuando se pongan objetos


                            //7-7-2017 AÑADO MODIFICACIÓN PARA AGRUPAS ARTÍCULOS DE TALLAS Y COLORES
                            string codArticuloA3 = "";
                            int numLinea = 0;
                            if (csGlobal.tieneTallas)
                            {
                                //meto en un datatable todos los artículos de un documento para luego agruparlos "distinct"
                                DataTable articulosEnDoc = new DataTable();
                                articulosEnDoc.Columns.Add("CODART");
                                //DataRow articuloDoc = articulosEnDoc.NewRow();

                                for (int cont = 0; cont < lineas.Length; cont++)
                                {
                                    if (lineas[cont].numeroDoc == cabeceras[i].extNumdDoc)
                                    {
                                        DataRow articuloDoc = articulosEnDoc.NewRow();
                                        articuloDoc["CODART"] = lineas[cont].codigoArticulo;
                                        articulosEnDoc.Rows.Add(articuloDoc);
                                    }
                                }
                                //filtro y me quedo sólo con los artículos, quitando repeticiones
                                DataView vistaArticulosDoc = new DataView(articulosEnDoc);
                                vistaArticulosDoc.Sort = "CODART ASC";
                                DataTable articulosSinRepEnDocs = vistaArticulosDoc.ToTable(true, "CODART");
                                articulosSinRepEnDocs.Columns.Add("CANTIDAD");
                                int unidades = 0;

                                string articuloParaAgrupar = "";
                                string articuloSinAgrupar = "";
                                foreach (DataRow fila in articulosSinRepEnDocs.Rows)
                                {
                                    articuloParaAgrupar = fila["CODART"].ToString();
                                    for (int x = 0; x < lineas.Length; x++)
                                    {
                                        articuloSinAgrupar = lineas[x].codigoArticulo;
                                        if (articuloParaAgrupar == articuloSinAgrupar)
                                        {
                                            unidades = unidades + Convert.ToInt16(lineas[x].cantidad);
                                        }
                                    }
                                    fila["CANTIDAD"] = unidades;
                                    unidades = 0;
                                }

                                //ITERO CADA ARTÍCULO DEL DOCUMENTO
                                foreach (DataRow articuloToCheck in articulosSinRepEnDocs.Rows)
                                {

                                    string articuloEnLinea = articuloToCheck["CODART"].ToString();
                                    if (articuloEnLinea.ToUpper().Trim() == "OBSOLETO")
                                    {
                                        break;
                                    }

                                    string cantidadEnLinea = articuloToCheck["CANTIDAD"].ToString();
                                    naxpedido.NuevaLineaArt(articuloEnLinea, Convert.ToDouble(cantidadEnLinea));

                                    for (int ii = 0; ii < lineas.Length; ii++)
                                    {
                                        if (ii == 56)
                                        {
                                            string dos = "";
                                        }
                                        if (lineas[ii].numeroDoc == cabeceras[i].extNumdDoc)
                                        {


                                            articulo = lineas[ii].codigoArticulo;
                                            //Program.escribirErrorEnFichero(articulo.ToString() + "\n");
                                            if (articulo == "3033-105")
                                            {
                                                string txt = "hola";
                                            }



                                            if (articuloEnLinea == articulo)
                                            {
                                                if (lineas[ii].codigoArticulo.ToUpper() == "OBSOLETO")
                                                {
                                                    break;
                                                }

                                                //campos personalizados para puntes
                                                if (csGlobal.conexionDB.Contains("PUNTES"))
                                                {
                                                    //naxpedido.set_AsStringLin("SOLU_ALTO", lineas[ii].SOLU_ALTO);
                                                    //naxpedido.set_AsStringLin("SOLU_ANCHO", lineas[ii].SOLU_ANCHO);

                                                }

                                                //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                                                //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                                                if (!string.IsNullOrEmpty(lineas[ii].tallaCod) || !string.IsNullOrEmpty(lineas[ii].colorCod))
                                                {
                                                    codfamtallasH = lineas[ii].codFamiliaH;
                                                    codfamtallasV = lineas[ii].codFamiliaV;
                                                    codtallaH = lineas[ii].tallaCod;
                                                    codtallaV = lineas[ii].colorCod;
                                                    //precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));


                                                    precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                    if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                    {

                                                        precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                                        naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);

                                                    }
                                                    if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                    {
                                                        naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                    }
                                                    
                                                    naxpedido.AnadirTalla(codfamtallasH, codfamtallasV, codtallaH.Trim(), codtallaV, Convert.ToDouble(lineas[ii].cantidad), precioArticulo);

                                                }
                                                else
                                                {
                                                    //Valor para comprovar si recogemos los precios desde A3
                                                    if (csGlobal.preciosA3)
                                                    {
                                                        precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                        if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                        {
                                                            precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ",")); ;
                                                            naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);

                                                        }
                                                        if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                        {
                                                            naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                        }
                                                    }
                                                }

                                                //  DESCUENTOS LINEA

                                            }

                                        }




                                    }
                                    if (descuentoCabecera)
                                    {
                                        naxpedido.set_AsFloatLin("Desc1", porDescuentoCab);
                                    }
                                    else
                                    {
                                        //naxpedido.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));

                                    }
                                    if (csGlobal.analitica == "SI")
                                    {
                                        naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                        naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                        naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                                    }
                                   
                                    naxpedido.AnadirLinea();
                                }
                            }

                            //SI NO TRABAJA CON TALLAS
                            if (!csGlobal.tieneTallas)
                            {
                                for (int ii = 0; ii < lineas.Length; ii++)
                                {
                                    //if (csGlobal.conexionDB.ToUpper().Contains("MATT"))
                                    //{
                                    //    codfamtallasH = cabeceras[].FA LineasDocsA3.Rows[ii]["codfamtallah"].ToString().Replace(" ", "");
                                    //    codfamtallasV = LineasDocsA3.Rows[ii]["codfamtallav"].ToString().Replace(" ", "");
                                    //}

                                    if (lineas[ii].numeroDoc == cabeceras[i].extNumdDoc)
                                    {
                                        articulo = lineas[ii].codigoArticulo;
                                        naxpedido.NuevaLineaArt(articulo, Convert.ToDouble(lineas[ii].cantidad));
                                        //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                                        //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                                        if (!string.IsNullOrEmpty(lineas[ii].tallaCod) || !string.IsNullOrEmpty(lineas[ii].colorCod))
                                        {
                                            codfamtallasH = lineas[ii].codFamiliaH;
                                            codfamtallasV = lineas[ii].codFamiliaV;
                                            codtallaH = lineas[ii].tallaCod;
                                            codtallaV = lineas[ii].colorCod;
                                            //precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));

                                            if (!string.IsNullOrEmpty(lineas[ii].precio))
                                            {
                                                precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                {
                                                    precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                                    naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                                }
                                                if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                {
                                                    naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                }
                                            }
                                            
                                            naxpedido.AnadirTalla(codfamtallasH, codfamtallasV, codtallaH.Trim(), codtallaV, Convert.ToDouble(lineas[ii].cantidad), precioArticulo);

                                        }
                                        else
                                        {
                                            if (lineas[ii].precio!=null)
                                            {

                                                precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ","));
                                                if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                                {
                                                    precioArticulo = Convert.ToDouble(lineas[ii].precio.Replace(".", ",")); ;
                                                    naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                                }
                                                if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                                {
                                                    naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                                }
                                            }
                                        }

                                        //  DESCUENTOS LINEA
                                        if (descuentoCabecera)
                                        {
                                            naxpedido.set_AsFloatLin("Desc1", porDescuentoCab);
                                        }
                                        else
                                        {
                                            naxpedido.set_AsFloatLin("Desc1", Convert.ToDouble(lineas[ii].descuento));

                                        }

                                        naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                        naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                        naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);


                                        if (csGlobal.conexionDB.Contains("PUNTES"))
                                        {
                                            naxpedido.set_AsStringLin("REMOTOID", lineas[ii].numeroLinea);
                                        }

                                        naxpedido.AnadirLinea();
                                    }

                                }
                            }
                            decimal documento = naxpedido.Anade();
                            naxpedido.Acabar();

                            ///Actualizo en FileMaker el pedido creado
                            if (csGlobal.conexionDB.Contains("PUNTES"))
                            {
                                Puntes.csPuntes puntes = new Puntes.csPuntes();
                                puntes.updateDocA3Project(cabeceras[i].numDocA3, documento.ToString());
                            }

                            if (csGlobal.ServirPedido.ToUpper() == "SI")
                            {
                                //VERIFICAR QUE AL MENOS UNA LINEA CON STOCK
                                if (pedidoEntregable(numPedidoV))
                                {
                                    servirPedidoPorLineas(csGlobal.serie, numPedidoV, documento.ToString());
                                }
                            }
                            mySql.insertarDocumentosA3Generados(numeroDocPs, Convert.ToString(documento), fechaSync);

                            //return documento; WTF!!!!! ECHA FUERA DEL FOR
                        }
                        #endregion
                    }
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                MessageBox.Show(ex.Message);
                ex.ToString();
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

            return 0;
        }
        

        /// <summary>
        /// Va a buscar el cliente y si no lo encuentra lo crea
        /// </summary>
        /// <param name="CabeceraDocsA3"></param>
        /// <param name="i"></param>
        /// <returns>string codcli del cliente</returns>
        /// lvgstark

        private string gestionSincronizacionCliente(DataTable CabeceraDocsA3, int i)
        {
            try
            {
                string codCli = "";

                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mysql = new csMySqlConnect();
                csPrestashop ps = new csPrestashop();
                bool clienteNoSincronizado = false;     //Creo esta variable para ver si tengo que crear el cliente o tengo que verificar su sincronización

                string kls_a3erp_id = CabeceraDocsA3.Rows[i]["kls_a3erp_id"].ToString();
                string idCliPS = CabeceraDocsA3.Rows[i]["id_customer"].ToString();
                string nifCli = CabeceraDocsA3.Rows[i]["dni"].ToString();

                clienteNoSincronizado = string.IsNullOrEmpty(kls_a3erp_id) ? true : false;

                string id_address_delivery = ((csGlobal.docDestino.ToUpper() == "PEDIDO") ?
                CabeceraDocsA3.Rows[i]["id_address_delivery"].ToString() :
                CabeceraDocsA3.Rows[i]["id_address_invoice"].ToString());

                int numeroDireccionesClienteA3 = 0;
                int numeroDireccionesClientePS = 0;


                if (csGlobal.usarClienteGenerico.ToUpper() == "MX")
                {
                    // Si no lo encuentra, genérico
                    string nif_ps = ps.getNIFByIdAddress(id_address_delivery);

                }

                // Entrará aqui cuando haya modo mixto y encuentre el cliente 
                // Y cuando no use cliente genérico
                if (csGlobal.usarClienteGenerico.ToUpper() == "NO")
                {

                    //1-Valido si el cliente está ya dado de alta en el sistema por el NIF
                    if (clienteNoSincronizado)
                    {
                        if (string.IsNullOrEmpty(nifCli))
                        {
                            codCli = crearClienteDesdeDocumento(CabeceraDocsA3, i); //Si no tenemos nif, creamos directamente el cliente en A3 puesto que no es una empresa.
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(csUtilidades.modoMixtoBuscarClienteNIF(nifCli))) //Si el nif no está dado de alta, entonces creo el cliente
                            {
                                codCli = crearClienteDesdeDocumento(CabeceraDocsA3, i);
                            }
                            else
                            {
                                MessageBox.Show("El NIF de este cliente ya está registrado en A3.");
                            }   
                        }
                    }
                    else    //Quiere decir que el cliente existe y tengo que validar las direcciones
                    {
                        //Valido si existe la dirección en la tabla dirent de A3ERP
                        //si existe recupero el id del Cliente
                        if (sql.consultaExiste("ID_PS", "DIRENT", id_address_delivery))
                        {
                            try
                            {
                                codCli = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM DIRENT WHERE ID_PS=" + id_address_delivery /*+ " AND LTRIM(CODCLI)=" + kls_a3erp_id*/);
                                if (codCli == kls_a3erp_id)
                                {
                                    //PENDIENTE DE TESTEO 27/03/2020
                                    //esyncro.csSyncAddresses esyncro = new esyncro.csSyncAddresses();
                                    //esyncro.actualizarInfoDirecciones(CabeceraDocsA3.Rows[i]["id_customer"].ToString());
                                    return codCli;
                                }
                                else
                                {
                                    //Se obtienen el numero de direcciones asignadas con el mismo ID. 
                                    int numDirecciones = Int32.Parse(sql.obtenerCampoTabla("SELECT COUNT(*) FROM DIRENT GROUP BY ID_PS HAVING ID_PS = " + id_address_delivery));
                                    if (numDirecciones > 1)
                                    {
                                        //Si ese numero es mayor a 1 se comprueba cuales son incorrectos
                                        DataTable dtCodCliEquivocados = sql.cargarDatosTabla("DIRENT", "LTRIM(CODCLI)", "ID_PS = " + id_address_delivery + " AND LTRIM(CODCLI) != " + kls_a3erp_id);
                                        foreach (DataRow row in dtCodCliEquivocados.Rows)
                                        {
                                            //Se actualiza el ID_PS de los numeros incorrectos a null
                                            sql.actualizarANull("DIRENT", "ID_PS", "WHERE LTRIM(CODCLI)='" + row[0] + "'");
                                        }
                                    }else if (numDirecciones==1)
                                    {
                                        //DataTable direccionA3 = sql.cargarDatosTabla("SELECT [CODCLI],[DEFECTO],[DIRENT1],[DIRENT2],[DTOENT],[IDDIRENT],[NOMENT],[NUMDIR],[POBENT],[ID_PS] FROM DIRENT WHERE  LTRIM(CODCLI)='"+ kls_a3erp_id+"'");
                                        DataTable direccionA3 = sql.cargarTabla("DIRENT", "CODCLI,DEFECTO,DIRENT1,DIRENT2,DTOENT,IDDIRENT,NOMENT,NUMDIR,POBENT,ID_PS", " WHERE LTRIM(CODCLI)='" + kls_a3erp_id + "'");
                                        string nomdirA3 = direccionA3.Rows[0]["DIRENT1"].ToString();

                                        string iddirA3split = direccionA3.Rows[0]["IDDIRENT"].ToString();
                                        string[] iddirA3 = iddirA3split.Split(',');

                                        string nomdirPs = mysql.obtenerDatoFromQuery("select address1 from ps_address where id_address = " + id_address_delivery);
                                        if (nomdirA3==nomdirPs)
                                        {
                                            mysql.ejecutarConsulta("UPDATE ps_address set kls_id_dirent='"+ iddirA3[0] + "' where id_address = " + id_address_delivery, false);
                                            sql.actualizarCampo("UPDATE DIRENT set ID_PS=NULL WHERE ID_PS='"+ id_address_delivery + "'");
                                            sql.actualizarCampo("UPDATE DIRENT set ID_PS='"+ id_address_delivery + "' WHERE IDDIRENT='"+ iddirA3[0] + "'");
                                        }
                                    }

                                    //Una vez modificados las direcciones mal sincronizadas, volvemos a ejecutar la consulta
                                    codCli = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM DIRENT WHERE ID_PS=" + id_address_delivery + " AND LTRIM(CODCLI)=" + kls_a3erp_id);

                                    return codCli;

                                }
                            }
                            catch (Exception ex)
                            {
                                numeroDireccionesClienteA3 = Convert.ToInt16(sql.obtenerCampoTabla("SELECT COUNT(*) FROM DIRENT WHERE LTRIM(CODCLI)='" + kls_a3erp_id + "'"));
                                numeroDireccionesClientePS = Convert.ToInt16(mysql.obtenerDatoFromQuery("select count(*) from ps_address where id_customer=" + idCliPS, false));
                                //Si tanto en A3 como en PS sólo hay 1 dirección por cliente, arreglo la sincronización
                                if (numeroDireccionesClienteA3 == numeroDireccionesClientePS && numeroDireccionesClientePS == 1)
                                {
                                    csUtilidades.ejecutarConsulta("UPDATE DIRENT SET ID_PS=" + id_address_delivery + " WHERE LTRIM(CODCLI)='" + kls_a3erp_id + "'", false);
                                }
                                else
                                {
                                    if (csGlobal.modoManual)
                                    {
                                        MessageBox.Show("La dirección del cliente no está correctamente sincronizada. \n Revisar por favor");
                                    }
                                }
                                return codCli;

                            }
                        }
                        else //Si no existe la dirección la creo
                        {
                            codCli = kls_a3erp_id;
                            if (csUtilidades.isAnAddressFromSpain(id_address_delivery))
                            {
                                // Sincronizamos direcciones, pero solo si existe la columna KLS_ID_PS en PROVINCI
                                if (csUtilidades.existeColumnaSQL("PROVINCI", "KLS_IDPS"))
                                {
                                    esyncro.csSyncAddresses esyncro = new esyncro.csSyncAddresses();
                                    if (esyncro.crearDireccionA3(id_address_delivery, informarDatosCliente(CabeceraDocsA3, i), kls_a3erp_id))
                                    {
                                        //esyncro.actualizarInfoDirecciones(CabeceraDocsA3.Rows[i]["id_customer"].ToString());
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{



                    //    //OBTENGO LA FECHA DE ÚLTIMA MODIFICACIÓN DE LA DIRECCION DE PRESTASHOP
                    //    string dateLastUpdate = mysql.obtenerDatoFromQuery("select date_upd from ps_address where id_address=" + id_address_delivery);


                    //    //// Actualizamos el id_ps de la tabla dirent en A3
                    //    //csUtilidades.ejecutarConsulta("update dirent set id_ps = '" + id_address_delivery + "',KLS_LASTUPDATE='" + dateLastUpdate + "'  where ltrim(codcli) = '" + cliente + "'",
                    //    //    csUtilidades.SQL,
                    //    //    csUtilidades.UPDATE);


                    //    //// Actualizamos el kls_a3erp_id de ps_customer en prestashop
                    //    //csUtilidades.ejecutarConsulta("update ps_customer set kls_a3erp_id = " + cliente + " where id_customer = " + CabeceraDocsA3.Rows[i]["id_customer"].ToString(),
                    //    //    csUtilidades.MYSQL,
                    //    //    csUtilidades.UPDATE);
                    //}

                    
                }
                return codCli;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private Objetos.csTercero informarDatosCliente(DataTable CabeceraDocsA3, int i)
        {
            try
            {
                Objetos.csTercero nuevo_cliente = new Objetos.csTercero();

                nuevo_cliente.direccion = csUtilidades.quitarAcentos(CabeceraDocsA3.Rows[i]["address1"].ToString().ToUpper()).Replace("º", "").Replace("ª", "").Replace("°", "o").Replace("?", "");
                nuevo_cliente.direccion2 = csUtilidades.quitarAcentos(CabeceraDocsA3.Rows[i]["address2"].ToString().ToUpper()).Replace("º", "").Replace("ª", "").Replace("°", "o").Replace("?", "");
                nuevo_cliente.idDireccionPS = CabeceraDocsA3.Rows[i]["id_address_delivery"].ToString().ToUpper();
                nuevo_cliente.nombre = CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper();
                nuevo_cliente.apellidos = CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper();
                nuevo_cliente.razonSocial = CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper();
                nuevo_cliente.poblacion = CabeceraDocsA3.Rows[i]["city"].ToString().ToUpper();
                nuevo_cliente.provincia = CabeceraDocsA3.Rows[i]["city"].ToString().ToUpper();
                nuevo_cliente.email = CabeceraDocsA3.Rows[i]["email"].ToString().Trim();
                nuevo_cliente.codigoPostal = CabeceraDocsA3.Rows[i]["postcode"].ToString();
                nuevo_cliente.nomIC = CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper(); ;
                nuevo_cliente.telf = CabeceraDocsA3.Rows[i]["phone"].ToString();
                nuevo_cliente.movil = CabeceraDocsA3.Rows[i]["phone_mobile"].ToString();
                nuevo_cliente.pais = CabeceraDocsA3.Rows[i]["pais"].ToString().ToUpper();
                nuevo_cliente.id_customer_Ext = CabeceraDocsA3.Rows[i]["id_customer"].ToString();
                nuevo_cliente.opeccm = CabeceraDocsA3.Rows[i]["opeccm"].ToString().ToUpper();
                nuevo_cliente.regimenIva = obtenerRegimenIvaCliente(nuevo_cliente.pais, nuevo_cliente.opeccm);
                nuevo_cliente.nifcif = CabeceraDocsA3.Rows[i]["dni"].ToString().ToUpper();
                nuevo_cliente.codprovincia = csUtilidades.obtenerProvinciaEnBaseAlCodigoPostal(nuevo_cliente.codigoPostal);
                nuevo_cliente.fechaLastUpdateDir = DateTime.Now;

                nuevo_cliente.famCliEsp = "";

                if (csGlobal.conexionDB.ToUpper().Contains("MIMASA") && !CabeceraDocsA3.Rows[i]["famcli"].ToString().Equals(""))
                {
                    nuevo_cliente.famCliEsp = CabeceraDocsA3.Rows[i]["famcli"].ToString().Trim();
                    nuevo_cliente.carDos = CabeceraDocsA3.Rows[i]["pais"].ToString().ToUpper();
                }
                if (csGlobal.conexionDB.ToUpper().Contains("MIMASA") && CabeceraDocsA3.Rows[i]["pais"].ToString().ToUpper().Equals("FR"))
                {
                    nuevo_cliente.regimenIva = "VNAC"; //Operac.interiores sujetas
                }
                if (csGlobal.conexionDB.ToUpper().Contains("MIMASA") && CabeceraDocsA3.Rows[i]["pais"].ToString().ToUpper().Equals("CH"))
                {
                    nuevo_cliente.regimenIva = "VCEE"; //Entregas intracomunitarias exentas
                }

                return nuevo_cliente;
            }
            catch(Exception ex) { return null; }
        }

        private string crearClienteDesdeDocumento(DataTable CabeceraDocsA3, int i)
        {
            try
            {
                string cliente = "";
                csa3erpTercero a3erpTercero = new csa3erpTercero();
                Objetos.csTercero nuevo_cliente = new Objetos.csTercero();
                nuevo_cliente = informarDatosCliente(CabeceraDocsA3, i);

                cliente = a3erpTercero.crearClienteObjetoA3(nuevo_cliente);
                return cliente;
            }

            catch { return csGlobal.clienteGenerico; }

        }

        private string obtenerRegimenIvaCliente(string pais, string opeccm)
        {
            string regIVA_A3 = "";
            if (opeccm == "1")
            {
                regIVA_A3 = "OPECCM";
            }
            else
            {
                string script = "SELECT CASE " +
                                " WHEN (CEE='T' AND CODISO3166A2='ES') THEN 'VNAC' " +
                                " WHEN (CEE='T' AND CODISO3166A2<>'ES') THEN 'VCEE' " +
                                " WHEN CEE='F' THEN 'EXP' END AS REGIVA from paises where CODISO3166A2='" + pais + "'";
                csSqlConnects sqlConnect = new csSqlConnects();
                regIVA_A3 = sqlConnect.obtenerCampoTabla(script);
            }
            return regIVA_A3;
        }

        private bool obtenerIvaCliente(string codcli)
        {
            csSqlConnects sql = new csSqlConnects();

            if (sql.obtenerCampoTabla("select ivaincluido from __clientes where ltrim(codcli) = '" + codcli.Trim() + "'") == "T")
                return true;
            else
                return false;
        }

        private string namesOrCompany()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("SGI", "N");
            dict.Add("HEBO", "C");

            if (dict.ContainsKey(csGlobal.conexionDB.ToUpper()))
            {
                foreach (KeyValuePair<string, string> entry in dict)
                {
                    if (entry.Key == csGlobal.conexionDB.ToUpper())
                    {
                        return entry.Value;
                    }
                }
            }

            return "N";
        }



        //SAGRADO  V V V V V 
        public decimal generaDocA3Dataset(DataSet Documentos)
        {
            int errorNumCabecera = 0;
            string errorSerieDoc = "";
            int errorNumLinea = 0;
            string errorCodArticulo = "";
            string errorArticulo = "";

            csMySqlConnect mySql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            csa3erpTercero a3erpTercero = new csa3erpTercero();

            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            decimal documento = 0;
            decimal docsGenerados = 0;
            string codfamtallasH = "";
            string codfamtallasV = "";
            string codtallaH = "";
            string codtallaV = "";
            double precioArticulo = 0;
            string cliente = "";
            string referenciaDocumento = "";
            double dtoLinea = 0;  //Descuento Linea
            string articulo = "";
            string numeroDocPs = "";
            string numeroDocA3 = "";
            string fechaSync = "";
            string portesGratis = "";
            string transportista = "";
            string transportistaRef = ""; // para obtener el id original del transportista
            string formaPago = "";
            string modFormaPago = "";
            string dtoPpago = "";
            string porDtoProntoPago = "0";
            bool descuentoCabecera = false;
            double porDescuentoCab = 0;
            string numPedidoV = "";
            string numDir = "";
            bool ivaIncluidoCliente = false;
            string regIVA = "";
            string group_reduction = "";

            try
            {
                DataTable CabeceraDocsA3 = Documentos.Tables["CabeceraDocs"];
                DataTable LineasDocsA3 = Documentos.Tables["LineasDocs"];
                DataTable cuponesDocs = Documentos.Tables["CuponesDocs"];

                for (int i = 0; i < CabeceraDocsA3.Rows.Count; i++)
                {
                    try
                    {
                        #region Factura

                        //GENERACIÓN DE FACTURA DE VENTAS
                        if (csGlobal.docDestino.ToUpper() == "FACTURA")
                        {
                            naxfactura.Iniciar();

                            // Gestion clientes
                            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                            {
                                cliente = csGlobal.clienteGenerico;

                                if (csGlobal.conexionDB.ToUpper().Contains("MIRO"))
                                {
                                    string id_customer = CabeceraDocsA3.Rows[i]["id_customer"].ToString().Trim();
                                    string id_default_group = mySql.obtenerDatoFromQuery("select id_default_group from ps_customer where id_customer = " + id_customer);
                                    cliente = (id_default_group == "4") ? "3" : "9";
                                }
                            }
                            else
                            {
                                cliente = gestionSincronizacionCliente(CabeceraDocsA3, i);
                            }

                            // naxpedido.Nuevo(documentos[i, 0], csGlobal.clienteGenerico, false);
                            fechaSync = CabeceraDocsA3.Rows[i]["invoice_date"].ToString();
                            if (fechaSync == "00/00/0000")
                            {
                                fechaSync = DateTime.Now.ToShortDateString();
                            }
                            //numeroDocA3 = CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper();
                            numeroDocA3 = CabeceraDocsA3.Rows[i]["id_order"].ToString().ToUpper();
                            numeroDocPs = CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper();
                            naxfactura.Nuevo(CabeceraDocsA3.Rows[i]["invoice_date"].ToString(), cliente, false, false, true, true);


                            //Informo el número de documento para poder luego utilizarlo en el catch
                            errorNumDocCabecera = numeroDocA3;

                            // obtenemos el iva del cliente
                            ivaIncluidoCliente = obtenerIvaCliente(cliente);

                            naxfactura.set_AsStringCab("nifcli", CabeceraDocsA3.Rows[i]["dni"].ToString().ToUpper());
                            naxfactura.set_AsStringCab("serie", csGlobal.serie);

                            //sólo para Ventas con cliente genérico
                            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                            {
                                regIVA = obtenerRegimenIvaCliente(CabeceraDocsA3.Rows[i]["pais"].ToString().ToUpper(), CabeceraDocsA3.Rows[i]["opeccm"].ToString().ToUpper());
                                naxfactura.set_AsStringCab("REGIVA", regIVA);
                            }

                            // Solo para los que tengan el módulo
                            if (CabeceraDocsA3.Columns.Contains("kls_reference"))
                            {
                                naxfactura.set_AsStringCab("referencia", CabeceraDocsA3.Rows[i]["kls_reference"].ToString());
                            }
                            else
                            {
                                referenciaDocumento = "Pedido Web: " + CabeceraDocsA3.Rows[i]["reference"].ToString();
                                naxfactura.set_AsStringCab("referencia", referenciaDocumento);
                            }

                            if (CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper() != "")
                                naxfactura.set_AsStringCab("nomcli", CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper());
                            else
                                naxfactura.set_AsStringCab("nomcli", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());

                            //TRANSPORTISTA

                            transportista = CabeceraDocsA3.Rows[i]["id_carrier"].ToString();
                            transportistaRef = CabeceraDocsA3.Rows[i]["carrier_Ref"].ToString();
                            if (csGlobal.transportistas.Count != 0)
                            {
                                foreach (KeyValuePair<string, string> entry in csGlobal.transportistas)
                                {
                                    if (transportistaRef == entry.Value)
                                    {
                                        naxfactura.set_AsStringCab("codtra", entry.Key);
                                        break;
                                    }
                                }
                            }

                            //DOCUMENTO DE PAGO
                            //Sólo lo informo si la forma de pago es diferente del módulo de A3
                            modFormaPago = CabeceraDocsA3.Rows[i]["module"].ToString();
                            if (modFormaPago != "klosionscustompayment")
                            {
                                formaPago = CabeceraDocsA3.Rows[i]["payment"].ToString();
                                if (csGlobal.formas_pago.Count != 0)
                                {
                                    foreach (KeyValuePair<string, string> entry in csGlobal.formas_pago)
                                    {
                                        if (formaPago == entry.Key)
                                        {
                                            naxfactura.set_AsStringCab("docpag", entry.Value);
                                            break;
                                        }
                                    }
                                }
                            }

                            //Para informar de la Razón verifico si está informado
                            if (CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper() != "")
                            {
                                naxfactura.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper());
                            }
                            else
                            {
                                naxfactura.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());
                            }

                            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                            {
                                naxfactura.set_AsStringCab("dircli", CabeceraDocsA3.Rows[i]["address1"].ToString().ToUpper());
                                naxfactura.set_AsStringCab("dtocli", CabeceraDocsA3.Rows[i]["postcode"].ToString().ToUpper());
                                naxfactura.set_AsStringCab("pobcli", CabeceraDocsA3.Rows[i]["city"].ToString().ToUpper());

                                string telcli = mySql.obtenerDatoFromQuery("select phone from ps_address where id_customer = " + CabeceraDocsA3.Rows[i]["id_customer"].ToString().ToUpper());
                                if (telcli != "")
                                    naxfactura.set_AsStringCab("telcli", telcli);

                                string provincia_ps = mySql.obtenerDatoFromQuery("select id_state from ps_address where id_customer = " + CabeceraDocsA3.Rows[i]["id_customer"].ToString().ToUpper());
                                if (provincia_ps != "")
                                    naxfactura.set_AsStringCab("codprovi", sql.obtenerCampoTabla("SELECT CODPROVI FROM PROVINCI WHERE KLS_IDPS = '" + provincia_ps + "'"));
                            }

                            naxfactura.set_AsStringCab("email", CabeceraDocsA3.Rows[i]["email"].ToString().ToLower());
                            naxfactura.set_AsStringCab("numdoc", CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper());
                            //naxfactura.set_AsStringCab("numdoc", numeroDocA3);
                            //naxfactura.set_AsFloatCab("BasePorMoneda", Convert.ToDouble(CabeceraDocsA3.Rows[i]["portes"].ToString()));
                            naxfactura.set_AsFloatCab("BasePorMasIvaMon", Convert.ToDouble(CabeceraDocsA3.Rows[i]["Portes"].ToString()));

                            //  mySql.InsertFrasGeneradas(frasGeneradas);

                            for (int ii = 0; ii < LineasDocsA3.Rows.Count; ii++)
                            {
                                string numDocumento = CabeceraDocsA3.Rows[i][0].ToString();
                                string numLinDocumento = LineasDocsA3.Rows[ii]["id_order"].ToString();
                                if (numLinDocumento == numDocumento)
                                {

                                    articulo = LineasDocsA3.Rows[ii]["product_reference"].ToString();
                                    naxfactura.NuevaLineaArt(articulo, Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()));
                                    //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                                    //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)


                                    //Informo el número de documento para poder luego utilizarlo en el catch
                                    errorArtLinDocumento = articulo;

                                    if (LineasDocsA3.Rows[ii]["product_attribute_id"].ToString() != "0")
                                    {
                                        if (csGlobal.tieneTallas == true)
                                        {
                                            codfamtallasH = LineasDocsA3.Rows[ii]["CodFamTallaH"].ToString();
                                            codfamtallasV = LineasDocsA3.Rows[ii]["CodFamTallaV"].ToString();
                                            codtallaH = LineasDocsA3.Rows[ii]["CodTallaH"].ToString();
                                            codtallaV = LineasDocsA3.Rows[ii]["CodTallaV"].ToString();

                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));
                                            //if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                            if (ivaIncluidoCliente || csGlobal.ivaIncluido.ToUpper() == "SI")
                                            {
                                                precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                                naxfactura.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                            }
                                            else
                                            {
                                                naxfactura.set_AsFloatLin("Prcmoneda", precioArticulo);
                                            }
                                            naxfactura.AnadirTalla(codfamtallasH, codfamtallasV, codtallaH.Trim(), codtallaV, Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()), precioArticulo);
                                        }
                                    }
                                    else
                                    {
                                        precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));
                                        //if (csGlobal.ivaIncluido.ToUpper() == "SI")
                                        if (ivaIncluidoCliente || csGlobal.ivaIncluido.ToUpper() == "SI")
                                        {
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                            naxfactura.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                        }
                                        else
                                        {
                                            naxfactura.set_AsFloatLin("Prcmoneda", precioArticulo);
                                        }
                                        //naxfactura.set_AsFloatLin("Prcmoneda", precioArticulo);
                                    }

                                    naxfactura.set_AsFloatLin("Desc1", Convert.ToDouble(LineasDocsA3.Rows[ii]["discount_quantity_applied"].ToString()));
                                    naxfactura.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                    naxfactura.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                    naxfactura.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                                    naxfactura.AnadirLinea();
                                }

                            }
                            documento = naxfactura.Anade();
                            naxfactura.Acabar();
                            mySql.insertarDocumentosA3Generados(numeroDocPs, Convert.ToString(documento), fechaSync);

                            //return documento; WTF!!!!! ECHA FUERA DEL FOR
                        }
                        #endregion

                        #region Pedido

                        // GENERACIÓN DE PEDIDO DE VENTAS
                        else if (csGlobal.docDestino.ToUpper() == "PEDIDO")
                        {
                            //"Empieza el pedido".log();

                            // a3ERPActiveX.Maestro clientes = new Maestro();

                            a3ERPActiveX.Pedido naxpedido = new a3ERPActiveX.Pedido();
                            naxpedido.Iniciar();

                            naxpedido.OmitirMensajes = true;
                            naxpedido.ValidarArtBloqueado = false;
                            naxpedido.ActivarAlarmaLin = false;

                            if (csGlobal.usarClienteGenerico.ToUpper() == "SI")
                            {
                                cliente = csGlobal.clienteGenerico;

                                "Cliente genérico asignado".log();

                                if (csGlobal.conexionDB.ToUpper().Contains("MIRO"))
                                {
                                    string id_customer = CabeceraDocsA3.Rows[i]["id_customer"].ToString().Trim();
                                    string id_default_group = mySql.obtenerDatoFromQuery("select id_default_group from ps_customer where id_customer = " + id_customer);
                                    cliente = (id_default_group == "4") ? "3" : "9";
                                }
                            }
                            else
                            {
                                cliente = gestionSincronizacionCliente(CabeceraDocsA3, i);
                                if (string.IsNullOrEmpty(cliente)) continue;
                            }

                            "Cliente creado con éxito".log();

                            fechaSync = CabeceraDocsA3.Rows[i]["invoice_date"].ToString();
                            if (fechaSync == "00/00/0000" || fechaSync == "")
                            {
                                fechaSync = DateTime.Now.ToShortDateString();
                            }
                            numeroDocA3 = CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper();
                            numeroDocPs = CabeceraDocsA3.Rows[i]["invoice_number"].ToString().ToUpper();
                            numPedidoV = CabeceraDocsA3.Rows[i]["id_order"].ToString().ToUpper();


                            //Informo el número de documento para poder luego utilizarlo en el catch
                            errorNumDocCabecera = numPedidoV;

                            naxpedido.Nuevo(fechaSync, cliente, false);

                            // obtenemos el iva del cliente
                            ivaIncluidoCliente = obtenerIvaCliente(cliente);

                            // Solo para los que tengan el módulo
                            if (CabeceraDocsA3.Columns.Contains("kls_reference"))
                            {
                                referenciaDocumento = CabeceraDocsA3.Rows[i]["kls_reference"].ToString();
                                naxpedido.set_AsStringCab("referencia", referenciaDocumento);
                            }
                            else
                            {
                                referenciaDocumento = "Pedido Web: " + CabeceraDocsA3.Rows[i]["reference"].ToString();
                                naxpedido.set_AsStringCab("referencia", referenciaDocumento);
                            }

                            naxpedido.set_AsStringCab("nifcli", CabeceraDocsA3.Rows[i]["dni"].ToString().ToUpper());
                            naxpedido.set_AsStringCab("serie", csGlobal.serie);
                            naxpedido.set_AsStringCab("telcli", CabeceraDocsA3.Rows[i]["phone"].ToString().ToUpper());
                            //30-10-2018 comento esta linea porque en Slide da error, 
                            //naxpedido.set_AsStringCab("telcli2", CabeceraDocsA3.Rows[i]["phone_mobile"].ToString().ToUpper());

                            naxpedido.set_AsStringCab("dircli", CabeceraDocsA3.Rows[i]["address1"].ToString().ToUpper());
                            naxpedido.set_AsStringCab("pobcli", CabeceraDocsA3.Rows[i]["city"].ToString().ToUpper());
                            naxpedido.set_AsStringCab("dtocli", CabeceraDocsA3.Rows[i]["postcode"].ToString().ToUpper());
                            naxpedido.set_AsStringCab("codprovi", csUtilidades.getProvincia(CabeceraDocsA3.Rows[i]["id_state"].ToString()));
                            naxpedido.set_AsStringCab("codpais", csUtilidades.getPais(CabeceraDocsA3.Rows[i]["id_country"].ToString()));

                            "NIF, serie, telefono, fax, direccion, poblacion, codigo postal, provincia y país asignados".log();

                            // Nombre de la compañia o nombre y apellido en nombre de cliente
                            if (namesOrCompany() == "C")
                            {
                                naxpedido.set_AsStringCab("nomcli", CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper());
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("nomcli", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());
                            }

                            //Referencia del pedido, depende de si está instalado el módulo
                            if (csGlobal.moduloReferenciaPedidos)
                            {
                                naxpedido.set_AsStringCab("referencia", CabeceraDocsA3.Rows[i]["Ref_Cliente"].ToString().ToUpper());
                            }

                            //Para informar de la Razón verifico si está informado
                            if (CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper() != "")
                            {
                                naxpedido.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper());
                            }
                            else
                            {
                                naxpedido.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());
                            }

                            //Dirección de entrega del cliente, solo se informa si Id_ps está informado
                            if (csGlobal.usarClienteGenerico == "NO")
                            {
                                string id_address_delivery = CabeceraDocsA3.Rows[i]["id_address_delivery"].ToString().ToUpper();

                                if (sql.obtenerNumDireccion(id_address_delivery, "NUMDIR") != "")
                                {
                                    naxpedido.set_AsStringCab("NUMDIR", sql.obtenerNumDireccion(id_address_delivery, "NUMDIR"));
                                }
                            }

                            naxpedido.set_AsStringCab("email", CabeceraDocsA3.Rows[i]["email"].ToString().ToLower());
                            naxpedido.set_AsStringCab("numdoc", CabeceraDocsA3.Rows[i]["id_order"].ToString().ToUpper());

                            //PORTES
                            portesGratis = CabeceraDocsA3.Rows[i]["PortesGratis"].ToString();
                            if (portesGratis != "True")
                            {
                                naxpedido.set_AsFloatCab("BasePorMasIvaMon", Convert.ToDouble(CabeceraDocsA3.Rows[i]["Portes"].ToString()));
                            }

                            //PRONTO PAGO
                            if (csGlobal.usaProntoPago.ToUpper() == "SI")
                            {
                                if (CabeceraDocsA3.Rows[i]["DtoPPago"].ToString().ToUpper() == "TRUE")
                                {
                                    porDtoProntoPago = CabeceraDocsA3.Rows[i]["Dto"].ToString();
                                    if (porDtoProntoPago == "")
                                    {
                                        porDtoProntoPago = "0";
                                    }
                                    naxpedido.set_AsFloatCab("PORPRONTO", Convert.ToDouble(porDtoProntoPago));
                                }
                            }

                            //TRANSPORTISTA
                            transportista = CabeceraDocsA3.Rows[i]["id_carrier"].ToString();
                            string referenceCarrier = mySql.obtenerDatoFromQuery(string.Format("select id_reference from ps_carrier where id_carrier = {0}", transportista));
                            if (csGlobal.transportistas != null)
                            {
                                foreach (KeyValuePair<string, string> entry in csGlobal.transportistas)
                                {
                                    if (referenceCarrier == entry.Key)
                                    {
                                        naxpedido.set_AsStringCab("codtra", entry.Value);
                                        "Transportista asignado".log();
                                        break;
                                    }
                                }
                            }

                            //DOCUMENTO DE PAGO
                            //Sólo lo informo si la forma de pago es diferente del módulo de A3
                            modFormaPago = CabeceraDocsA3.Rows[i]["module"].ToString();
                            if (modFormaPago != "klosionscustompayment")
                            {
                                formaPago = CabeceraDocsA3.Rows[i]["payment"].ToString();
                                if (csGlobal.formas_pago != null)
                                {
                                    foreach (KeyValuePair<string, string> entry in csGlobal.formas_pago)
                                    {
                                        if (formaPago == entry.Key)
                                        {
                                            naxpedido.set_AsStringCab("docpag", entry.Value);
                                            "Forma de pago asignada".log();
                                            break;
                                        }
                                    }
                                }
                            }

                            //DESCUENTO DE CABECERA
                            descuentoCabecera = false;
                            dtoPpago = "";
                            porDescuentoCab = 0;
                            if (CabeceraDocsA3.Rows[i]["Dto"].ToString() != "")
                            {
                                porDescuentoCab = Convert.ToDouble(CabeceraDocsA3.Rows[i]["Dto"].ToString());
                                dtoPpago = CabeceraDocsA3.Rows[i]["DtoPPago"].ToString();
                                if (porDescuentoCab > 0 && (dtoPpago == "False" || dtoPpago == "0"))
                                {
                                    descuentoCabecera = true;
                                    porDescuentoCab = Convert.ToDouble(CabeceraDocsA3.Rows[i]["Dto"].ToString());
                                }
                            }

                            if (csGlobal.conexionDB.ToUpper() == "BASTIDE")
                            {
                                string descu = Convert.ToDouble(porDescuentoCab.ToString()).ToString();
                                naxpedido.set_AsStringCab("PORPRONTO", descu);
                            }

                            group_reduction = mySql.obtenerDatoFromQuery(string.Format("select group_reduction from ps_order_detail where id_order = {0}", CabeceraDocsA3.Rows[i]["id_order"].ToString()));
                            if (group_reduction != "" )
                            {
                                group_reduction = Convert.ToDouble(group_reduction).ToString();
                                if (Double.Parse(group_reduction) > 0)
                                {
                                    naxpedido.set_AsStringCab("PORPRONTO", group_reduction);
                                }
                            }

                            // Thagson 
                            if (csGlobal.databaseA3.Contains("THAGSON"))
                            {
                                string param9cab = CabeceraDocsA3.Rows[i]["origin"].ToString();
                                if (param9cab != "")
                                    naxpedido.set_AsStringCab("PARAM9", param9cab);
                            }


                            //GENERAMOS LAS LINEAS DEL PEDIDO

                            for (int ii = 0; ii < LineasDocsA3.Rows.Count; ii++)
                            {
                                string id_order_lin = LineasDocsA3.Rows[ii]["id_order"].ToString();
                                string id_order_cab = CabeceraDocsA3.Rows[i]["id_order"].ToString();
                                if (id_order_lin == id_order_cab)
                                {
                                    articulo = LineasDocsA3.Rows[ii]["product_reference"].ToString();
                                    //Se añade para CadCanarias, para que el pedido traspase y se ignoren las lineas en rojo cuando el articulo no exista en A3
                                    string checkarticulos = sql.obtenerCampoTabla("SELECT LTRIM(CODART) as CODART FROM ARTICULO WHERE LTRIM(CODART) IN ('" + articulo + "')", false);
                                    if (csGlobal.gestionArticulosPedidos && (checkarticulos == "" || checkarticulos == null))
                                    {
                                        continue;
                                    }

                                    //Informo el número de documento para poder luego utilizarlo en el catch
                                    errorArtLinDocumento = articulo + " linea: " + (ii + 1).ToString();
                                    errorArticulo = articulo;


                                    naxpedido.NuevaLineaArt(articulo, Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()));



                                    //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                                    //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                                    string product_attribute_id = LineasDocsA3.Rows[ii]["product_attribute_id"].ToString();

                                    //Añado control de Monoatributos

                                    if (csGlobal.activarMonotallas == "Y" && product_attribute_id == "0")
                                    {
                                        DataTable datosArticulo = new DataTable();
                                        string queryTallas = " SELECT " +
                                            " dbo.ARTICULO.CODART, dbo.ARTICULO.DESCART, dbo.ARTICULO.CODFAMTALLAH, dbo.ARTICULO.CODFAMTALLAV, dbo.ARTICULO.KLS_MONOATRIBUTO, " +
                                            " TALLAS_1.CODTALLA AS TALLAH, dbo.TALLAS.CODTALLA AS TALLAV,dbo.ARTICULO.TALLAS " +
                                            " FROM dbo.ARTICULO LEFT OUTER JOIN " +
                                            " dbo.TALLAS ON dbo.ARTICULO.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA LEFT OUTER JOIN " +
                                            " dbo.TALLAS AS TALLAS_1 ON dbo.ARTICULO.CODFAMTALLAH = TALLAS_1.CODFAMTALLA " +
                                            " WHERE LTRIM(CODART)='" + articulo + "'";
                                        datosArticulo = sql.obtenerDatosSQLScript(queryTallas);
                                        if (datosArticulo.Rows[0]["KLS_MONOATRIBUTO"].ToString() == "T")
                                        {
                                            product_attribute_id = "XX";
                                            codfamtallasH = datosArticulo.Rows[0]["CODFAMTALLAH"].ToString().Trim();
                                            codfamtallasH = datosArticulo.Rows[0]["CODFAMTALLAV"].ToString().Trim();
                                            codtallaH = datosArticulo.Rows[0]["TALLAH"].ToString().Trim();
                                            codtallaV = datosArticulo.Rows[0]["TALLAV"].ToString().Trim();
                                            product_attribute_id = (datosArticulo.Rows[0]["TALLAS"].ToString().Trim() == "F" && datosArticulo.Rows[0]["KLS_MONOATRIBUTO"].ToString() == "T") ? "0" : product_attribute_id;
                                        }
                                    }

                                    if (product_attribute_id != "0") // Si tiene tallas
                                    {
                                        //añado una condición para que si trabajamos con monotallas mande el artículo monoatributo calculado arriba
                                        if (product_attribute_id != "XX")
                                        {
                                            codfamtallasH = LineasDocsA3.Rows[ii]["CodFamTallaH"].ToString().Replace(" ", "");
                                            codfamtallasV = LineasDocsA3.Rows[ii]["CodFamTallaV"].ToString().Replace(" ", "");
                                            codtallaH = LineasDocsA3.Rows[ii]["CodTallaH"].ToString();
                                            codtallaV = LineasDocsA3.Rows[ii]["CodTallaV"].ToString();
                                        }

                                        if (csGlobal.ivaIncluido == "SI")
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                        else
                                        {
                                            //9-8-2020 Incluimos este código porque en Indusnow se traspasan los precios con descuentos aplicados
                                            //y además en a3 vuelve a aplicar los descuentos de nuevo
                                            if (csGlobal.a3_gestion_precio_dto)
                                            {
                                                precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));
                                                double descuentoLinea = Convert.ToDouble(LineasDocsA3.Rows[ii]["reduction_percent"].ToString().Replace(".", ","));
                                                naxpedido.set_AsFloatLin("Desc1", descuentoLinea);
                                            }
                                            else
                                            {
                                                precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_excl"].ToString().Replace(".", ","));
                                            }
                                        }

                                        if (ivaIncluidoCliente)
                                        {
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                            //naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                            naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                        }
                                        else
                                        {
                                            naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                        }

                                        naxpedido.AnadirTalla(codfamtallasH, codfamtallasV, codtallaH.Trim(), codtallaV, Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()), precioArticulo);
                                        "Talla añadida".log();
                                    }
                                    else 
                                    {
                                      
                                        precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));
                                        
                                        if (LineasDocsA3.Rows[ii]["discount_quantity_applied"].ToString().Equals("1") && csGlobal.ivaIncluido.Equals("SI"))
                                        {
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                        }
                                        else if (LineasDocsA3.Rows[ii]["discount_quantity_applied"].ToString().Equals("1") && csGlobal.ivaIncluido.Equals("NO"))
                                        {
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_excl"].ToString().Replace(".", ","));
                                        }

                                        if (ivaIncluidoCliente)
                                        {
                                            //17/06/17 revisión para Slide descuentos en línea con IVA incluido
                                            //En el caso de que haya descuentos en la línea hay que calcular el importe base original
                                            dtoLinea = string.IsNullOrEmpty(LineasDocsA3.Rows[ii]["reduction_percent"].ToString()) ? 0.00 : Convert.ToDouble(LineasDocsA3.Rows[ii]["reduction_percent"].ToString().Replace(".", ",")) / 100;
                                            precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["unit_price_tax_incl"].ToString().Replace(".", ","));
                                            precioArticulo = precioArticulo / (1 - dtoLinea);
                                            naxpedido.set_AsFloatLin("prcmonedamasiva", precioArticulo);
                                        }
                                        if (csGlobal.ivaIncluido.ToUpper() != "SI")
                                        {
                                            naxpedido.set_AsFloatLin("Prcmoneda", precioArticulo);
                                        }
                                    }

                                    //  DESCUENTOS LINEA
                                    if (descuentoCabecera)
                                    {
                                        naxpedido.set_AsFloatLin("Desc1", porDescuentoCab);


                                    }
                                    else
                                    {
                                        double descuentoLinea = Convert.ToDouble(LineasDocsA3.Rows[ii]["reduction_percent"].ToString().Replace(".", ","));
                                        naxpedido.set_AsFloatLin("Desc1", descuentoLinea);
                                    }

                                    "Descuentos aplicados".log();

                                    if (csGlobal.databaseA3.Contains("THAGSON"))
                                    {
                                        string param9lin = LineasDocsA3.Rows[ii]["catalogue_origin"].ToString();
                                        if (param9lin != "")
                                            naxpedido.set_AsStringLin("PARAM9", param9lin);
                                    }

                                    naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                                    naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                                    naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                                    naxpedido.AnadirLinea();
                                    "Linea añadida".log();
                                }
                            }

                            if (csGlobal.cupon_as_line)
                            {
                                //Reviso Cupones
                                for (int icup = 0; icup < cuponesDocs.Rows.Count; icup++)
                                {
                                    string id_cupon_lin = cuponesDocs.Rows[icup]["id_order"].ToString();
                                    string id_order_cab = CabeceraDocsA3.Rows[i][0].ToString();
                                    if (id_cupon_lin == id_order_cab)
                                    {
                                        articulo = string.IsNullOrEmpty(csGlobal.productCupon) ? "0" : csGlobal.productCupon;
                                        naxpedido.NuevaLineaArt(articulo, -1);
                                        naxpedido.set_AsFloatLin("Prcmoneda", Convert.ToDouble(cuponesDocs.Rows[icup]["VALUE_TAX_EXCL"].ToString().Replace(".", ",")));
                                        naxpedido.set_AsStringLin("DESCLIN", cuponesDocs.Rows[icup]["NAME"].ToString());
                                        naxpedido.AnadirLinea();

                                    }
                                }
                            }

                            documento = naxpedido.Anade();
                            naxpedido.Acabar();
                            "Pedido creado".log();
                            //Contabilizo los documentos generados correctamente
                            docsGenerados++;

                            if (!csGlobal.modeDebug)
                            {
                                mySql.insertarDocumentosA3Generados(numeroDocPs, documento.ToString(), Convert.ToDateTime(fechaSync).ToString("yyyy-MM-dd"));
                                //return documento; WTF!!!!! ECHA FUERA DEL FOR
                                "--------------------------------------------".log();
                            }

                            if (csGlobal.ServirPedido.ToUpper() == "SI")
                            {
                                //VERIFICAR QUE AL MENOS UNA LINEA CON STOCK
                                if (pedidoEntregable(documento.ToString()))
                                {
                                    servirPedidoPorLineas(csGlobal.serie, numPedidoV, documento.ToString());

                                    "Pedido servido".log();
                                }
                            }

                           
                        }
                        #endregion
                    }
                    catch(Exception exDoc)
                    {
                        string mensajeError = "Error en documento: " + errorNumDocCabecera + "\n" +
                        "Error en el articulo: " + errorArtLinDocumento + "\n" +
                        exDoc.Message;

                        if (csGlobal.modoManual)
                        {
                            MessageBox.Show(mensajeError, "Error en Documento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        Program.guardarErrorFichero(mensajeError);
                        continue;

                    }
                }
            }

            catch (Exception ex)
            {
                if (ex.Message.Contains("No coinciden las unidades de la línea con las del detalle"))
                {
                    csTallasYColoresV2 tyc = new csTallasYColoresV2();
                    tyc.marcarArticulosMonoatributoEnA3ERP("");
                }
                string mensajeError = "Error en documento: " + errorNumDocCabecera + "\n" +
               "Error en el articulo: " + errorArtLinDocumento + "\n" +
               ex.Message;

                mensajeError.log();
                cerrarEnlace();

                Program.guardarErrorFichero(mensajeError);

                return 0;
            }

            string estadoEnlaceA3=enlace.Estado.ToString();
            return docsGenerados;
        }
        //SAGRADO  ^ ^ ^ ^ ^ 


        public void generarAlbaranRegularizacion()
        {
            //    naxfactura.AnadirTalla(codfamtallasH, codfamtallasV, codtallaH.Trim(), codtallaV, Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()), precioArticulo);

            a3ERPActiveX.Regularizacion naxAlbRegularizacion = new a3ERPActiveX.Regularizacion();
            naxAlbRegularizacion.Iniciar();
            naxAlbRegularizacion.Nuevo(DateTime.Now.ToShortDateString(), "1");
            naxAlbRegularizacion.set_AsStringCab("serie", csGlobal.serie);
            naxAlbRegularizacion.set_AsStringCab("motivo", "Reajuste de stock");
            naxAlbRegularizacion.NuevaLineaArt("110029", 4);
            naxAlbRegularizacion.AnadirTalla("0", "13", "99", "10C", 6);
            naxAlbRegularizacion.AnadirTalla("0", "13", "99", "100U", -2);

            naxAlbRegularizacion.Anade();
            naxAlbRegularizacion.Acabar();

        }

        public void generaDocA3DatasetFromRPST(DataSet Documentos)
        {
            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            a3ERPActiveX.Pedido naxpedido = new a3ERPActiveX.Pedido();
            string articulo = "";
            DateTime fecha = DateTime.Today;


            DataTable CabeceraDocsA3 = Documentos.Tables["CabeceraDocs"];
            DataTable LineasDocsA3 = Documentos.Tables["LineasDocs"];
            for (int i = 0; i < CabeceraDocsA3.Rows.Count; i++)
            {
                if (csGlobal.docDestino == "Pedido")
                {
                    naxpedido.Iniciar();
                    fecha = Convert.ToDateTime(CabeceraDocsA3.Rows[i]["fecIniRealAct"].ToString());
                    // naxpedido.Nuevo(documentos[i, 0], csGlobal.clienteGenerico, false);
                    naxpedido.Nuevo(fecha.ToShortDateString(), CabeceraDocsA3.Rows[i]["codExternoCli"].ToString(), false);


                    // naxpedido.set_AsStringCab("nifcli", CabeceraDocsA3.Rows[i]["dni"].ToString().ToUpper());  
                    naxpedido.set_AsStringCab("serie", csGlobal.serie);
                    //naxpedido.set_AsStringCab("nomcli", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());
                    //Para informar de la Razón verifico si está informado
                    //if (CabeceraDocsA3.Rows[i]["company"].ToString().ToUpper() != "")
                    //{
                    //    naxfactura.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper());
                    //}
                    //else
                    //{
                    //    naxfactura.set_AsStringCab("razon", CabeceraDocsA3.Rows[i]["firstname"].ToString().ToUpper() + " " + CabeceraDocsA3.Rows[i]["lastname"].ToString().ToUpper());
                    //}
                    //naxpedido.set_AsStringCab("dircli", CabeceraDocsA3.Rows[i]["address1"].ToString().ToUpper());
                    //naxpedido.set_AsStringCab("dtocli", CabeceraDocsA3.Rows[i]["postcode"].ToString().ToUpper());
                    //naxpedido.set_AsStringCab("pobcli", CabeceraDocsA3.Rows[i]["city"].ToString().ToUpper());
                    naxpedido.set_AsStringCab("numdoc", CabeceraDocsA3.Rows[i]["idActividad"].ToString().ToUpper());
                    //naxpedido.set_AsFloatCab("BasePorMoneda", Convert.ToDouble(CabeceraDocsA3.Rows[i]["portes"].ToString()));



                    csMySqlConnect mySql = new csMySqlConnect();
                    //  mySql.InsertFrasGeneradas(frasGeneradas);

                    for (int ii = 0; ii < LineasDocsA3.Rows.Count; ii++)
                    {
                        if (LineasDocsA3.Rows[ii]["idActividad"].ToString() == CabeceraDocsA3.Rows[i]["idActividad"].ToString())
                        {
                            articulo = LineasDocsA3.Rows[ii]["refArticulo"].ToString();
                            if (articulo == "")
                            {
                                articulo = "0";
                            }
                            //naxpedido.NuevaLineaArt(LineasDocsA3.Rows[ii]["product_reference"].ToString(), Convert.ToDouble(LineasDocsA3.Rows[ii]["product_quantity"].ToString()));
                            naxpedido.NuevaLineaArt(articulo, Convert.ToDouble(LineasDocsA3.Rows[ii]["horasRealizadas"].ToString()));
                            //Si el precio de la web es con iva incluido utilizamos el campo prcmonedamasiva
                            //En caso contrario utilizar el campo precio (depende del campo cliente facturas más iva)
                            //precioArticulo = Convert.ToDouble(LineasDocsA3.Rows[ii]["product_price"].ToString().Replace(".", ","));
                            //naxpedido.set_AsFloatLin("precio", 5);
                            if (articulo == "0")
                            {
                                naxpedido.set_AsStringLin("desclin", LineasDocsA3.Rows[ii]["nomTarea"].ToString());
                            }

                            naxpedido.set_AsFloatLin("prcmoneda", 5);
                            naxpedido.set_AsStringLin("CentroCoste", csGlobal.nivel1Analitica);
                            naxpedido.set_AsStringLin("CentroCoste2", csGlobal.nivel2Analitica);
                            naxpedido.set_AsStringLin("CentroCoste3", csGlobal.nivel3Analitica);
                            naxpedido.AnadirLinea();
                        }

                    }
                    decimal documento = naxpedido.Anade();
                    naxpedido.Acabar();
                }
            }
            //MessageBox.Show("Traspaso Finalizado");






        }

        public void crearFacturaModelo()
        {

            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            naxfactura.Iniciar();
            naxfactura.Nuevo("10/03/2013", "1", false, false, true, true);
            naxfactura.set_AsStringCab("nifcli", "A123456");
            naxfactura.set_AsStringCab("nomcli", "CLIENTE PRESTASHOP");
            naxfactura.set_AsStringCab("dircli", "DIRECCIÓN CLIENTE PRESTASHOP");
            naxfactura.set_AsStringCab("dtocli", "08999");
            naxfactura.set_AsStringCab("pobcli", "Población Presta");
            naxfactura.NuevaLineaArt("1034", 5);
            naxfactura.set_AsFloatLin("Desc1", 8);
            naxfactura.set_AsStringLin("CentroCoste", "C1");
            naxfactura.set_AsStringLin("CentroCoste2", "C2");
            naxfactura.set_AsStringLin("CentroCoste3", "C3");


            naxfactura.AnadirLinea();
            decimal documento = naxfactura.Anade();
            naxfactura.Acabar();

        }

        //a3ERPActiveX.Maestro clientes = new Maestro();
        //clientes.Iniciar("Clientes");
        //clientes.Nuevo();
        //string numero = clientes.NuevoCodigoNum();
        //clientes.set_AsString("CODCLI", "30");
        ////clientes.set_AsInteger("IDORG", 30);

        ////clientes.set_AsString["Codcli"] = "888";
        //clientes.Guarda(false);
        //clientes.Acabar();
        //a3ERPActiveX.Maestro maestro = new a3ERPActiveX.Maestro();
        //maestro.Iniciar("clientes");
        //maestro.Nuevo();
        //maestro.set_AsString("CODCLI", maestro.NuevoCodigoNum());
        //maestro.set_AsString("NOMCLI","Cliente creado desde NAX");
        //maestro.Guarda(false);
        //maestro.Acabar();


        //Maestro naxArticulo = new Maestro();
        //naxArticulo.Iniciar("Articulo");
        //naxArticulo.Nuevo();
        //naxArticulo.set_AsString("CodArt", "101");
        //naxArticulo.set_AsString("DescArt", "101");
        //naxArticulo.Guarda(false);

        ////Maestro naxOrganizacion = new Maestro();
        ////naxOrganizacion.Iniciar("Organizacion");
        ////naxOrganizacion.Nuevo();

        ////naxOrganizacion.set_AsString("idorg", "30");
        ////naxOrganizacion.set_AsString("Nombre", "xxx");
        ////naxOrganizacion.Guarda(false);

  

        /*
      * sIdDocu
      *  ‘O’  Oferta
       ‘P’  Pedido
       ‘A’  Albarán.
       ‘D’  Depósito
      * 
      * En función del valor de sIdDocu (O,P,A,D) representa el identificador de la oferta 
      * (IdOfeV o IdOfeC), del pedido (IdPEdV o IdPedC), del albarán (IdAlbV o IdAlbC) o del 
      * depósito (IdDepV o IdDepC) respectivamente.
      */

        public bool pedidoEntregable(string numdoc)
        {
            bool lineasConStock = false;

            csSqlConnects sqlconnect = new csSqlConnects();
            DataTable lineasPedido = new DataTable();

            lineasPedido = sqlconnect.obtenerLineasPedido(numdoc.ToString());

            //Recorrer lineas del datatable
            foreach (DataRow fila in lineasPedido.Rows)
            {
                string codart = fila["codart"].ToString().Replace(" ", "");
                int unidades = Convert.ToInt32(fila["unidades"].ToString());
                string codtallav = fila["codtallav"].ToString();

                //obtener codart de la línea y consultar stock
                int stock = sqlconnect.obtenerStockArticulo(codart, codtallav);

                if (stock > 0)
                {
                    lineasConStock = true;
                    break;
                }
            }

            return lineasConStock;
        }

        public void servirPedidoTotal(string sIdDocu, string nIdDocu)
        {
            a3ERPActiveX.Albaran naxAlbaran = new a3ERPActiveX.Albaran();
            csSqlConnects sqlconnect = new csSqlConnects();

            try
            {
                //decimal numdoc = 924;
                decimal idAlbV;
                string fechaAlbaran = DateTime.Today.ToString("dd/MM/YYYY");
                string cliente = "136";

                //obtener Id del Pedido de Ventas

                //Crear el Albarán destino
                naxAlbaran.Iniciar();
                naxAlbaran.Nuevo(fechaAlbaran, cliente, false);


                //obtener dataTable de las lineas del pedido

                naxAlbaran.IniciarServir("P", 924, false);
                //naxAlbaran.ServirLinea(0, 1, 0, 0, 1, 0, "", "", "", "");
                naxAlbaran.ServirDocumento();
                naxAlbaran.FinServir();
                idAlbV = naxAlbaran.Anade();
            }
            finally
            {
                //naxAlbaran.Acabar(); 
            }
        }

        public void servirAlbaranPorLineas(string serie, string numeroDoc, string idAlbv)
        {
            csSqlConnects sqlconnect = new csSqlConnects();
            csSqlScripts scripts = new csSqlScripts();
            csMail mailing = new csMail();

            frPedidos pedidos = new frPedidos();

            DataSet Documentos = new DataSet();
            DataTable lineasPedido = new DataTable();

            bool enviarEmailLogistica = false;
            bool enviarEmailCliente = false;

            a3ERPActiveX.Albaran naxAlbaran = new a3ERPActiveX.Albaran();

            string emailCliente = "";

            // 0 IDDIRENT, 1 DIRENT1, 2 POBENT, 3 NOMENT, 4 DTOENT, 5 NOMPROVI,

            DataTable mail = sqlconnect.obtenerCabeceraParaMailAlbaran(idAlbv, serie); // idalbv

            // Definimos las columnas de la cabecera y las lineas del XML
            DataTable cabeceraXML = Documentos.Tables.Add("cabeceraXML");

            #region BASTIDE COLUMNAS CABECERA
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                cabeceraXML.Columns.Add("TipoRegistro");
                cabeceraXML.Columns.Add("CodigoPedido");
                cabeceraXML.Columns.Add("FechaPedido");
                cabeceraXML.Columns.Add("NombreDestinatario");
                cabeceraXML.Columns.Add("DireccionDestinatario");
                cabeceraXML.Columns.Add("CPostalDestinatario");
                cabeceraXML.Columns.Add("PoblacionDestinatario");
                cabeceraXML.Columns.Add("PaisDestinatario");
                cabeceraXML.Columns.Add("CIFDestinatario");
                cabeceraXML.Columns.Add("TelefonoDestinatario");
                cabeceraXML.Columns.Add("Portes");
                cabeceraXML.Columns.Add("ObsDestinatario");
                cabeceraXML.Columns.Add("Reembolso");
                cabeceraXML.Columns.Add("Transportista");
                cabeceraXML.Columns.Add("ServicioTransportista");
                cabeceraXML.Columns.Add("NumAlbaran");
                cabeceraXML.Columns.Add("AlbaranValorado");
                cabeceraXML.Columns.Add("TipoDocumento");
                cabeceraXML.Columns.Add("Provincia");
            }
            #endregion
            #region SGI COLUMNAS CABECERA
            else // SGI
            {
                cabeceraXML.Columns.Add("codCli");
                cabeceraXML.Columns.Add("nifCli");
                cabeceraXML.Columns.Add("telCli");
                cabeceraXML.Columns.Add("refCli");
                cabeceraXML.Columns.Add("fecha");
                cabeceraXML.Columns.Add("direccionEnt");
                cabeceraXML.Columns.Add("nomCliente");
                cabeceraXML.Columns.Add("cPostal");
                cabeceraXML.Columns.Add("poblacion");
                cabeceraXML.Columns.Add("provincia");
                cabeceraXML.Columns.Add("idDoc");
                cabeceraXML.Columns.Add("numDoc");
            }
            #endregion

            DataTable lineasXML = Documentos.Tables.Add("lineasXML");
            #region BASTIDE COLUMNAS LINEAS
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                lineasXML.Columns.Add("TipoRegistro");
                lineasXML.Columns.Add("CodigoPedido");
                lineasXML.Columns.Add("Linea");
                lineasXML.Columns.Add("CodigoArticulo");
                lineasXML.Columns.Add("DescArticulo");
                lineasXML.Columns.Add("Cantidad");
                lineasXML.Columns.Add("CantidadServida");
                lineasXML.Columns.Add("Precio");
                lineasXML.Columns.Add("SinCargo");
                lineasXML.Columns.Add("Observaciones");
                lineasXML.Columns.Add("TipoDocumento");
                lineasXML.Columns.Add("PVP");
                lineasXML.Columns.Add("Marca");
            }
            #endregion
            #region SGI COLUMNAS LINEAS
            else // SGI
            {
                lineasXML.Columns.Add("ORDLIN");
                lineasXML.Columns.Add("CODART");
                lineasXML.Columns.Add("ARTALIAS");
                lineasXML.Columns.Add("DESCLIN");
                lineasXML.Columns.Add("UNIDADES");
                lineasXML.Columns.Add("IDPEDV");
            }
            #endregion

            // Les asignamos los valores
            DataRow dr = cabeceraXML.NewRow();

            #region BASTIDE DATOS CABECERA
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                dr["TipoRegistro"] = "C";
                dr["CodigoPedido"] = numeroDoc;
                dr["FechaPedido"] = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                dr["NombreDestinatario"] = mail.Rows[0].ItemArray[4].ToString();
                dr["DireccionDestinatario"] = mail.Rows[0].ItemArray[7].ToString();
                dr["CPostalDestinatario"] = mail.Rows[0].ItemArray[11].ToString();
                dr["PoblacionDestinatario"] = mail.Rows[0].ItemArray[9].ToString();
                dr["PaisDestinatario"] = mail.Rows[0].ItemArray[16].ToString();
                dr["CIFDestinatario"] = mail.Rows[0].ItemArray[15].ToString();
                dr["TelefonoDestinatario"] = mail.Rows[0].ItemArray[17].ToString();
                dr["Portes"] = "PAGADOS";
                dr["ObsDestinatario"] = mail.Rows[0].ItemArray[8].ToString();
                dr["Reembolso"] = "";
                dr["Transportista"] = "";
                dr["ServicioTransportista"] = "";
                dr["NumAlbaran"] = numeroDoc;
                dr["AlbaranValorado"] = "N";
                dr["TipoDocumento"] = "P";
                dr["Provincia"] = mail.Rows[0].ItemArray[12].ToString();
            }
            #endregion
            #region SGI DATOS CABECERA
            else // SGI
            {
                dr["codCli"] = mail.Rows[0].ItemArray[0].ToString().Replace(" ", "");
                dr["nifCli"] = mail.Rows[0].ItemArray[13].ToString();
                dr["telCli"] = mail.Rows[0].ItemArray[8].ToString();
                dr["refCli"] = mail.Rows[0].ItemArray[6].ToString();
                dr["fecha"] = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                dr["direccionEnt"] = mail.Rows[0].ItemArray[7].ToString();
                dr["nomCliente"] = mail.Rows[0].ItemArray[4].ToString();
                dr["cPostal"] = mail.Rows[0].ItemArray[11].ToString();
                dr["poblacion"] = mail.Rows[0].ItemArray[9].ToString();
                dr["provincia"] = mail.Rows[0].ItemArray[12].ToString();
                dr["idDoc"] = idAlbv;
                dr["numDoc"] = numeroDoc;
            }
            #endregion

            Documentos.Tables["cabeceraXML"].Rows.Add(dr);
            emailCliente = mail.Rows[0].ItemArray[14].ToString();
            string[] cabecera = new string[2];
            string materialPendiente = "";
            string materialEntregarLogistica = "";

            try
            {
                // a3.abrirEnlace();
                decimal idAlbV;
                string fechaAlbaran = DateTime.Today.ToString("dd/MM/yyyy");
                cabecera = sqlconnect.obtenerInformacionDocumentoVentaAlbaran(serie, numeroDoc);
                string cliente = cabecera[1];
                decimal numdoc = Convert.ToDecimal(cabecera[0]);
                numdoc = Math.Round(numdoc, 0);

                //obtener Id del Pedido de Ventas según Serie y Numdoc
                //Crear el Albarán destino


                #region SGI LINEAS PEDIDO
                //obtener dataTable de las lineas del pedido
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    lineasPedido = sqlconnect.obtenerLineasPedido(numdoc.ToString());

                    //Recorrer lineas del datatable
                    foreach (DataRow fila in lineasPedido.Rows)
                    {
                        string desclin = fila["Desclin"].ToString();
                        string codart = fila["codart"].ToString().Replace(" ", "");
                        string artalias = fila["artalias"].ToString();
                        int unidades = Convert.ToInt32(fila["unidades"].ToString());
                        //obtener codart de la línea y consultar stock
                        int stock = sqlconnect.obtenerStockArticulo(codart);
                        decimal numlinea = Convert.ToDecimal(fila["ordlin"].ToString());
                        string idpedv = fila["IDPEDV"].ToString();

                        //si Cantidad de la línea >= Stock del artículo, servir línea
                        if (unidades <= stock)
                        {
                            DataRow drLineas = lineasXML.NewRow();
                            drLineas["ORDLIN"] = numlinea;
                            drLineas["CODART"] = codart;
                            drLineas["ARTALIAS"] = artalias;
                            drLineas["DESCLIN"] = desclin;
                            drLineas["UNIDADES"] = unidades;
                            drLineas["IDPEDV"] = idpedv;
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);
                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, unidades, 0, "", "", "", "");
                            materialEntregarLogistica = materialEntregarLogistica + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades" + "\r\n";
                            enviarEmailLogistica = true;
                        }

                        //entrega parcial
                        else if (unidades > stock && stock > 0)
                        {
                            DataRow drLineas = lineasXML.NewRow();
                            drLineas["ORDLIN"] = numlinea;
                            drLineas["CODART"] = codart;
                            drLineas["ARTALIAS"] = artalias;
                            drLineas["DESCLIN"] = desclin;
                            drLineas["UNIDADES"] = stock;
                            drLineas["IDPEDV"] = idpedv;
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);
                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, stock, 0, "", "", "", "");
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades - stock).ToString() + " Unidades" + "\r\n";
                            materialEntregarLogistica = materialEntregarLogistica + fila["Desclin"].ToString() + " ----> " + (unidades - stock).ToString() + " Unidades" + "\r\n";
                            enviarEmailLogistica = true;

                            enviarEmailCliente = true;
                        }
                        // Material Sin Stock
                        else if (stock <= 0)
                        {
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades" + "\r\n";
                            enviarEmailCliente = true;
                        }
                    }
                }
                #endregion
                #region BASTIDE LINEAS PEDIDO
                else
                {
                    lineasPedido = sqlconnect.obtenerLineasAlbaran(numdoc.ToString());
                    csMySqlConnect mysql = new csMySqlConnect();
                    //Recorrer lineas del datatable
                    foreach (DataRow fila in lineasPedido.Rows)
                    {
                        string desclin = fila["descripcion"].ToString();
                        string codart = fila["codart"].ToString().Replace(" ", "");

                        string available_now = mysql.obtenerDatoFromQuery(string.Format("select ps_product_lang.available_now from ps_product left join ps_product_lang on ps_product_lang.id_product = ps_product.id_product where reference = '{0}' and ps_product_lang.id_lang = {1}", codart.Trim(), csUtilidades.idIdiomaDefaultPS()));
                        //string artalias = fila["artalias"].ToString();
                        int unidades = Convert.ToInt32(fila["UNIDADES"].ToString());
                        //obtener codart de la línea y consultar stock
                        string codtallav = fila["CODTALLAV"].ToString();
                        int stock = sqlconnect.obtenerStockArticulo(codart, codtallav);
                        decimal numlinea = Convert.ToDecimal(fila["numlinalb"].ToString());
                        string idpedv = fila["idalbv"].ToString();
                        //string pvp = fila["PRCMONEDA"].ToString();

                        //si Cantidad de la línea >= Stock del artículo, servir línea
                        if (unidades <= stock)
                        {
                            DataRow drLineas = lineasXML.NewRow();

                            drLineas["TipoRegistro"] = "D";
                            drLineas["CodigoPedido"] = numeroDoc;
                            drLineas["Linea"] = numlinea; // contador
                            drLineas["CodigoArticulo"] = codart;
                            drLineas["DescArticulo"] = csUtilidades.resolverPadRight(desclin, 60);
                            drLineas["Cantidad"] = unidades;
                            drLineas["CantidadServida"] = "";
                            drLineas["Precio"] = "0";
                            drLineas["SinCargo"] = "NO";
                            drLineas["Observaciones"] = codtallav;
                            drLineas["TipoDocumento"] = "P";
                            drLineas["PVP"] = "";
                            drLineas["Marca"] = "";
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);

                            materialEntregarLogistica = materialEntregarLogistica + desclin + " ----> " + unidades.ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailLogistica = true;
                        }

                        //entrega parcial
                        else if (unidades > stock && stock > 0)
                        {
                            DataRow drLineas = lineasXML.NewRow();

                            drLineas["TipoRegistro"] = "D";
                            drLineas["CodigoPedido"] = numeroDoc;
                            drLineas["Linea"] = numlinea; // contador
                            drLineas["CodigoArticulo"] = codart;
                            drLineas["DescArticulo"] = csUtilidades.resolverPadRight(desclin, 60);
                            drLineas["Cantidad"] = unidades;
                            drLineas["CantidadServida"] = "";
                            drLineas["Precio"] = "0";
                            drLineas["SinCargo"] = "NO";
                            drLineas["Observaciones"] = codtallav;
                            drLineas["TipoDocumento"] = "P";
                            drLineas["PVP"] = "";
                            drLineas["Marca"] = "";
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);

                            //naxAlbaran.ServirLinea(0, numlinea, 0, 0, stock, 0, "", "", "", "");
                            materialPendiente = materialPendiente + desclin + " ----> " + (unidades - stock).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            materialEntregarLogistica = materialEntregarLogistica + desclin + " ----> " + (unidades - stock).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailLogistica = true;

                            enviarEmailCliente = true;
                        }
                        // Material Sin Stock
                        else if (stock <= 0)
                        {
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailCliente = true;
                        }
                    }
                }
                #endregion

                //solo para documentos completos
                //naxAlbaran.ServirDocumento();
                //Finalizar las líneas

                string numAlbaran = "";
                numAlbaran = sqlconnect.obtenerNumeroAlbaran(Math.Truncate(Convert.ToDecimal(idAlbv)).ToString());

                // enviamos datos de la cabecera para generar el XML
                string xml = pedidos.generarXMLAlmacen(Documentos, numAlbaran);
                xml = xml.Replace("utf-16", "utf-8");
                string codCli = mail.Rows[0].ItemArray[0].ToString().Replace(" ", "");

                DataTable dt = generarAlbaran(idAlbv.ToString(), emailCliente.Trim(), codCli.Trim(), numAlbaran); // aqui peta


                //Preparación de campos para enviar por email
                string nifCli = mail.Rows[0].ItemArray[13].ToString(); // 
                string telCli = mail.Rows[0].ItemArray[8].ToString();
                string telCli2 = mail.Rows[0].ItemArray[17].ToString();
                string refCli = mail.Rows[0].ItemArray[6].ToString();
                string fecha = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                string direccionEnt = mail.Rows[0].ItemArray[7].ToString();
                string nomCliente = mail.Rows[0].ItemArray[4].ToString();
                string cPostal = mail.Rows[0].ItemArray[11].ToString();
                string poblacion = mail.Rows[0].ItemArray[9].ToString();
                string provincia = mail.Rows[0].ItemArray[12].ToString();
                string pais = mail.Rows[0].ItemArray[16].ToString();

                if (nifCli == "" && (codCli != "" && emailCliente != ""))
                {
                    nifCli = obtenerNif(codCli, emailCliente);
                }

                // correo
                // direccion 2

                #region ENVIO MAIL SGI
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    string cabeceraMaterialPendiente = "Le comunicamos que los siguientes artículos de su pedido " + numeroDoc + " no se encuentran en Stock" + "\r\n" +
                   "en el plazo de 2 días lo recibirán." + "\r\n" +
                   "\r\n" +
                   "para cualquier aclaración, pueden contactar por mail a pedidos@sgi2010.es" + "\r\n" +
                   " ************************** " + "\r\n";
                    string cabeceraMaterialEntregarLogistica = "Solicitamos preparen el pedido número " + numeroDoc + " de nuestro cliente:" + "\r\n" +
                                "Número de Albarán: " + numAlbaran + "\r\n" +
                                "Nombre del Cliente :" + nomCliente + "(" + codCli + ")" + "\r\n" +
                                "Dirección Entrega: " + direccionEnt + "\r\n" +
                                "CP: " + cPostal + " " + poblacion + "\r\n" +
                                "Población: " + poblacion + "\r\n" +
                                "Provincia: " + provincia + "\r\n" +
                                "Teléfono: " + telCli + "\r\n" +
                                "Nif: " + nifCli + "\r\n" + " ************************** " + "\r\n" + "\r\n" + "\r\n";


                    string bodyMaterialPendiente = cabeceraMaterialPendiente + materialPendiente;
                    string bodyMaterialEntregarLogistica = cabeceraMaterialEntregarLogistica + materialEntregarLogistica;

                    if (!csGlobal.perfilAdminEsync)
                    {
                        if (enviarEmailCliente)
                        {
                            mailing.sendMail("SGI-Pedido: " + numeroDoc + " - Material Pendiente", bodyMaterialPendiente, "", emailCliente + ",josecarlos@sgi2010.es", "shop@sgi2010.es");
                        }
                        if (enviarEmailLogistica)
                        {
                            mailing.sendMail("SGI - Preparación del Pedido " + numeroDoc + " del Cliente " + nomCliente + " (" + codCli + ")", bodyMaterialEntregarLogistica, xml);
                        }
                    }
                }
                #endregion
                #region BASTIDE ENVIO MAIL
                else
                {
                    string cabeceraMaterialPendiente = string.Format("Le informamos que los artículos de su pedido nº {0} lo recibirán en los siguientes plazos de entrega (días laborables)\nPueden contactar con nosotros a través del correo info@bastidemedicalsalud.com para cualquier consulta al respecto. Gracias.\n\n", numeroDoc);
                    string cabeceraMaterialEntregarLogistica = "Solicitamos preparen el pedido número " + numeroDoc + " de nuestro cliente:" + "\r\n" +
                                "Número de Albarán: " + numAlbaran + "\r\n" +
                                "Nombre del Cliente :" + nomCliente + "(" + codCli + ")" + "\r\n" +
                                "Dirección Entrega: " + direccionEnt + "\r\n" +
                                "CP: " + cPostal + " " + poblacion + "\r\n" +
                                "Población: " + poblacion + "\r\n" +
                                "Provincia: " + provincia + "\r\n" +
                                "Teléfono: " + telCli + "\r\n" +
                                "Teléfono móvil: " + telCli2 + "\r\n" +
                                "Nif: " + nifCli + "\r\n" +
                                "Pais: " + pais + "\r\n" +
                                "Correo: " + emailCliente + "\r\n" +
                                "************************** " + "\r\n" + "\r\n" + "\r\n";


                    string bodyMaterialPendiente = cabeceraMaterialPendiente + materialPendiente;
                    string bodyMaterialEntregarLogistica = cabeceraMaterialEntregarLogistica + materialEntregarLogistica;

                    if (!csGlobal.perfilAdminEsync)
                    {
                        if (enviarEmailLogistica)
                        {
                            mailing.sendMail("BASTIDE - Preparación del Pedido " + numeroDoc + " del Cliente " + nomCliente + " (" + codCli + ")", bodyMaterialEntregarLogistica, xml, "", "", dt, false, false, true);
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.Message);
            }
            finally { }
        }


        public void servirOfertaPorLineas(string serie, string numeroDoc, string idDoc)
        {
            DataTable dtOferta = new DataTable();
            decimal numlinea = 0;
            double unidades = 0;

            csSqlConnects sql = new csSqlConnects();


            string fechaPedido = DateTime.Today.ToString("dd/MM/yyyy");
            string cliente = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM CABEOFEV WHERE IDOFEV=" + idDoc);
            a3ERPActiveX.Pedido naxPedido = new a3ERPActiveX.Pedido();
            naxPedido.Iniciar();
            naxPedido.Nuevo(fechaPedido, cliente, false);


            string query = "SELECT " +
                   " ltrim(CODART) as CODART, CAP, DESCLIN, CONVERT(FLOAT, IDOFEV) as IDOFEV, NUMLINOFE, ORDLIN, UNIDADES, " +
                   " convert(FLOAT,PRCMEDIO) AS COSTE, CONVERT(FLOAT, PRCMONEDA) as PRCMONEDA, CONVERT(FLOAT, PRECIO2) AS PRECIO2, CONVERT(FLOAT, PRECIO3) AS PRECIO3, " +
                   " CONVERT(FLOAT,BASE) AS BASE, DTO1_PROV,DTO2_PROV, COSTE_ORIGEN ,OBTPRCCOSTE, SITUACION, IMPLIN " +
                   " FROM dbo.LINEOFER where idofev = '" + idDoc + "' order by ordlin asc";
            csSqlConnects sqlConnect = new csSqlConnects();
            dtOferta = sqlConnect.obtenerDatosSQLScript(query);


            naxPedido.OmitirMensajes = true;
            if (csGlobal.perfilAdminEsync)
            {
                naxPedido.OmitirMensajes = false;
            }

            naxPedido.IniciarServir("O", Convert.ToDecimal(idDoc), true);

            foreach (DataRow fila in dtOferta.Rows)
            {
                numlinea = Convert.ToDecimal(fila["NUMLINOFE"].ToString());
                unidades = Convert.ToDouble(fila["UNIDADES"].ToString());

                if (fila["CODART"].ToString() == "KIT" || fila["CODART"].ToString() == "CAP")
                {
                    naxPedido.AnularLinea(numlinea, 0, 0, unidades);
                }
                else
                {
                    naxPedido.ServirLinea(0, numlinea, 0, 0, unidades, 0, "", "", "", "");
                }


            }

            naxPedido.FinServir();
            //Guardar el documento Servido
            decimal idPedV = naxPedido.Anade();

            //string numAlbaran = sqlconnect.obtenerNumeroAlbaran(Math.Truncate(idAlbV).ToString());
            //numAlbaran = Math.Truncate(Convert.ToDouble(numAlbaran)).ToString();



        }


        public void servirPedidoPorLineas(string serie, string numeroDoc, string idDoc)
        {
            csSqlConnects sqlconnect = new csSqlConnects();
            csSqlScripts scripts = new csSqlScripts();
            csMail mailing = new csMail();

            frPedidos pedidos = new frPedidos();

            DataSet Documentos = new DataSet();
            DataTable lineasPedido = new DataTable();

            bool enviarEmailLogistica = false;
            bool enviarEmailCliente = false;

            a3ERPActiveX.Albaran naxAlbaran = new a3ERPActiveX.Albaran();

            string emailCliente = "";

            // 0 IDDIRENT, 1 DIRENT1, 2 POBENT, 3 NOMENT, 4 DTOENT, 5 NOMPROVI,

            DataTable mail = sqlconnect.obtenerCabeceraParaMail(idDoc, serie);

            // Definimos las columnas de la cabecera y las lineas del XML
            DataTable cabeceraXML = Documentos.Tables.Add("cabeceraXML");

            #region BASTIDE COLUMNAS CABECERA
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                cabeceraXML.Columns.Add("TipoRegistro");
                cabeceraXML.Columns.Add("CodigoPedido");
                cabeceraXML.Columns.Add("FechaPedido");
                cabeceraXML.Columns.Add("NombreDestinatario");
                cabeceraXML.Columns.Add("DireccionDestinatario");
                cabeceraXML.Columns.Add("CPostalDestinatario");
                cabeceraXML.Columns.Add("PoblacionDestinatario");
                cabeceraXML.Columns.Add("PaisDestinatario");
                cabeceraXML.Columns.Add("CIFDestinatario");
                cabeceraXML.Columns.Add("TelefonoDestinatario");
                cabeceraXML.Columns.Add("Portes");
                cabeceraXML.Columns.Add("ObsDestinatario");
                cabeceraXML.Columns.Add("Reembolso");
                cabeceraXML.Columns.Add("Transportista");
                cabeceraXML.Columns.Add("ServicioTransportista");
                cabeceraXML.Columns.Add("NumAlbaran");
                cabeceraXML.Columns.Add("AlbaranValorado");
                cabeceraXML.Columns.Add("TipoDocumento");
                cabeceraXML.Columns.Add("Provincia");
                cabeceraXML.Columns.Add("CodCli");
                cabeceraXML.Columns.Add("TelCli");
                cabeceraXML.Columns.Add("TelCli2");
                cabeceraXML.Columns.Add("email");
            }
            #endregion
            #region SGI COLUMNAS CABECERA
            else // SGI
            {
                cabeceraXML.Columns.Add("codCli");
                cabeceraXML.Columns.Add("nifCli");
                cabeceraXML.Columns.Add("telCli");
                cabeceraXML.Columns.Add("refCli");
                cabeceraXML.Columns.Add("fecha");
                cabeceraXML.Columns.Add("direccionEnt");
                cabeceraXML.Columns.Add("nomCliente");
                cabeceraXML.Columns.Add("cPostal");
                cabeceraXML.Columns.Add("poblacion");
                cabeceraXML.Columns.Add("provincia");
                cabeceraXML.Columns.Add("idDoc");
                cabeceraXML.Columns.Add("numDoc");
            }
            #endregion

            DataTable lineasXML = Documentos.Tables.Add("lineasXML");
            #region BASTIDE COLUMNAS LINEAS
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                lineasXML.Columns.Add("TipoRegistro");
                lineasXML.Columns.Add("CodigoPedido");
                lineasXML.Columns.Add("Linea");
                lineasXML.Columns.Add("CodigoArticulo");
                lineasXML.Columns.Add("DescArticulo");
                lineasXML.Columns.Add("Cantidad");
                lineasXML.Columns.Add("CantidadServida");
                lineasXML.Columns.Add("Precio");
                lineasXML.Columns.Add("SinCargo");
                lineasXML.Columns.Add("Observaciones");
                lineasXML.Columns.Add("TipoDocumento");
                lineasXML.Columns.Add("PVP");
                lineasXML.Columns.Add("Marca");
            }
            #endregion
            #region SGI COLUMNAS LINEAS
            else // SGI
            {
                lineasXML.Columns.Add("ORDLIN");
                lineasXML.Columns.Add("CODART");
                lineasXML.Columns.Add("ARTALIAS");
                lineasXML.Columns.Add("DESCLIN");
                lineasXML.Columns.Add("UNIDADES");
                lineasXML.Columns.Add("IDPEDV");
            }
            #endregion

            // Les asignamos los valores
            DataRow dr = cabeceraXML.NewRow();

            #region BASTIDE DATOS CABECERA
            if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE") || csGlobal.conexionDB.ToUpper().Contains("BASIC"))
            {
                dr["TipoRegistro"] = "C";
                dr["CodigoPedido"] = numeroDoc;
                dr["FechaPedido"] = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                dr["NombreDestinatario"] = mail.Rows[0].ItemArray[4].ToString();
                dr["DireccionDestinatario"] = mail.Rows[0].ItemArray[7].ToString();
                dr["CPostalDestinatario"] = mail.Rows[0].ItemArray[11].ToString();
                dr["PoblacionDestinatario"] = mail.Rows[0].ItemArray[9].ToString();
                dr["PaisDestinatario"] = mail.Rows[0].ItemArray[16].ToString();
                dr["CIFDestinatario"] = mail.Rows[0].ItemArray[15].ToString();
                dr["TelefonoDestinatario"] = mail.Rows[0].ItemArray[17].ToString();
                dr["Portes"] = "PAGADOS";
                dr["ObsDestinatario"] = mail.Rows[0].ItemArray[8].ToString();
                dr["Reembolso"] = "";
                dr["Transportista"] = "";
                dr["ServicioTransportista"] = "";
                dr["NumAlbaran"] = numeroDoc;
                dr["AlbaranValorado"] = "N";
                dr["TipoDocumento"] = "P";
                dr["Provincia"] = mail.Rows[0]["NOMPROVI"].ToString();
                dr["CodCli"] = mail.Rows[0]["CODCLI"].ToString();
                dr["TelCli"] = mail.Rows[0]["TELCLI"].ToString();
                dr["TelCli2"] = mail.Rows[0]["TELCLI2"].ToString();
                dr["email"] = mail.Rows[0]["KLS_EMAIL_USUARIO"].ToString();

            }
            #endregion
            #region SGI DATOS CABECERA
            else // SGI
            {
                dr["codCli"] = mail.Rows[0].ItemArray[0].ToString().Replace(" ", "");
                dr["nifCli"] = mail.Rows[0].ItemArray[13].ToString();
                dr["telCli"] = mail.Rows[0].ItemArray[8].ToString();
                dr["refCli"] = mail.Rows[0].ItemArray[6].ToString();
                dr["fecha"] = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                dr["direccionEnt"] = mail.Rows[0].ItemArray[7].ToString();
                dr["nomCliente"] = mail.Rows[0].ItemArray[4].ToString();
                dr["cPostal"] = mail.Rows[0].ItemArray[11].ToString();
                dr["poblacion"] = mail.Rows[0].ItemArray[9].ToString();
                dr["provincia"] = mail.Rows[0].ItemArray[12].ToString();
                dr["idDoc"] = idDoc;
                dr["numDoc"] = numeroDoc;
            }
            #endregion

            Documentos.Tables["cabeceraXML"].Rows.Add(dr);
            emailCliente = mail.Rows[0].ItemArray[14].ToString();
            string[] cabecera = new string[2];
            string materialPendiente = "";
            string materialEntregarLogistica = "";

            if (csGlobal.modeDebug)
            {
                emailCliente = "lluisvera@klosions.com";
            }

            try
            {
                // a3.abrirEnlace();
                decimal idAlbV;
                string fechaAlbaran = DateTime.Today.ToString("dd/MM/yyyy");
                cabecera = sqlconnect.obtenerInformacionDocumentoVenta(serie, numeroDoc);
                string cliente = cabecera[1];
                decimal numdoc = Convert.ToDecimal(cabecera[0]);
                numdoc = Math.Round(numdoc, 0);

                //obtener Id del Pedido de Ventas según Serie y Numdoc
                //Crear el Albarán destino
                naxAlbaran.Iniciar();
                naxAlbaran.Nuevo(fechaAlbaran, cliente, false);

                naxAlbaran.OmitirMensajes = true;
                if (csGlobal.perfilAdminEsync)
                {
                    naxAlbaran.OmitirMensajes = false;
                }

                naxAlbaran.IniciarServir("P", numdoc, true);

                #region SGI LINEAS PEDIDO
                //obtener dataTable de las lineas del pedido
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    lineasPedido = sqlconnect.obtenerLineasPedido(numdoc.ToString());

                    //Recorrer lineas del datatable
                    foreach (DataRow fila in lineasPedido.Rows)
                    {
                        string desclin = fila["Desclin"].ToString();
                        string codart = fila["codart"].ToString().Replace(" ", "");
                        string artalias = fila["artalias"].ToString();
                        int unidades = Convert.ToInt32(fila["unidades"].ToString());
                        //obtener codart de la línea y consultar stock
                        int stock = sqlconnect.obtenerStockArticulo(codart);
                        decimal numlinea = Convert.ToDecimal(fila["ordlin"].ToString());
                        string idpedv = fila["IDPEDV"].ToString();

                        //si Cantidad de la línea >= Stock del artículo, servir línea
                        if (unidades <= stock)
                        {
                            DataRow drLineas = lineasXML.NewRow();
                            drLineas["ORDLIN"] = numlinea;
                            drLineas["CODART"] = codart;
                            drLineas["ARTALIAS"] = artalias;
                            drLineas["DESCLIN"] = desclin;
                            drLineas["UNIDADES"] = unidades;
                            drLineas["IDPEDV"] = idpedv;
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);
                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, unidades, 0, "", "", "", "");
                            materialEntregarLogistica = materialEntregarLogistica + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades" + "\r\n";
                            enviarEmailLogistica = true;
                        }

                        //entrega parcial
                        else if (unidades > stock && stock > 0)
                        {
                            DataRow drLineas = lineasXML.NewRow();
                            drLineas["ORDLIN"] = numlinea;
                            drLineas["CODART"] = codart;
                            drLineas["ARTALIAS"] = artalias;
                            drLineas["DESCLIN"] = desclin;
                            drLineas["UNIDADES"] = stock;
                            drLineas["IDPEDV"] = idpedv;
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);
                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, stock, 0, "", "", "", "");
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades - stock).ToString() + " Unidades" + "\r\n";
                            materialEntregarLogistica = materialEntregarLogistica + fila["Desclin"].ToString() + " ----> " + (unidades - stock).ToString() + " Unidades" + "\r\n";
                            enviarEmailLogistica = true;

                            enviarEmailCliente = true;
                        }
                        // Material Sin Stock
                        else if (stock <= 0)
                        {
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades" + "\r\n";
                            enviarEmailCliente = true;
                        }

                        "Stock controlado".log();
                    }
                }
                #endregion
                #region BASTIDE LINEAS PEDIDO
                else
                {
                    lineasPedido = sqlconnect.obtenerLineasPedido(numdoc.ToString());
                    csMySqlConnect mysql = new csMySqlConnect();
                    //Recorrer lineas del datatable
                    foreach (DataRow fila in lineasPedido.Rows)
                    {
                        string desclin = fila["descripcion"].ToString();
                        string codart = fila["codart"].ToString().Replace(" ", "");

                        string available_now = mysql.obtenerDatoFromQuery(string.Format("select ps_product_lang.available_now from ps_product left join ps_product_lang on ps_product_lang.id_product = ps_product.id_product where reference = '{0}' and ps_product_lang.id_lang = {1}", codart.Trim(), csUtilidades.idIdiomaDefaultPS()));
                        //string artalias = fila["artalias"].ToString();
                        int unidades = Convert.ToInt32(fila["UNIDADES"].ToString());
                        //obtener codart de la línea y consultar stock
                        string codtallav = fila["CODTALLAV"].ToString();
                        int stock = sqlconnect.obtenerStockArticulo(codart, codtallav);
                        decimal numlinea = Convert.ToDecimal(fila["numlinped"].ToString());
                        string idpedv = fila["IDPEDV"].ToString();
                        //string pvp = fila["PRCMONEDA"].ToString();

                        //si Cantidad de la línea >= Stock del artículo, servir línea
                        if (unidades <= stock)
                        {
                            DataRow drLineas = lineasXML.NewRow();

                            drLineas["TipoRegistro"] = "D";
                            drLineas["CodigoPedido"] = numeroDoc;
                            drLineas["Linea"] = numlinea; // contador
                            drLineas["CodigoArticulo"] = codart;
                            drLineas["DescArticulo"] = csUtilidades.resolverPadRight(desclin, 60);
                            drLineas["Cantidad"] = unidades;
                            drLineas["CantidadServida"] = "";
                            drLineas["Precio"] = "0";
                            drLineas["SinCargo"] = "NO";
                            drLineas["Observaciones"] = codtallav;
                            drLineas["TipoDocumento"] = "P";
                            drLineas["PVP"] = "";
                            drLineas["Marca"] = "";
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);

                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, unidades, 0, "", "", "", "");
                            //naxAlbaran.ServirLinea(0, numlinea, 0, 0, unidades, 0, "", "", "", "");
                            materialEntregarLogistica = materialEntregarLogistica + desclin + " ----> " + unidades.ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailLogistica = true;
                        }

                        //entrega parcial
                        else if (unidades > stock && stock > 0)
                        {
                            DataRow drLineas = lineasXML.NewRow();

                            drLineas["TipoRegistro"] = "D";
                            drLineas["CodigoPedido"] = numeroDoc;
                            drLineas["Linea"] = numlinea; // contador
                            drLineas["CodigoArticulo"] = codart;
                            drLineas["DescArticulo"] = csUtilidades.resolverPadRight(desclin, 60);
                            drLineas["Cantidad"] = unidades;
                            drLineas["CantidadServida"] = "";
                            drLineas["Precio"] = "0";
                            drLineas["SinCargo"] = "NO";
                            drLineas["Observaciones"] = codtallav;
                            drLineas["TipoDocumento"] = "P";
                            drLineas["PVP"] = "";
                            drLineas["Marca"] = "";
                            Documentos.Tables["lineasXML"].Rows.Add(drLineas);

                            naxAlbaran.ServirLinea(0, numlinea, 0, 0, stock, 0, "", "", "", "");
                            materialPendiente = materialPendiente + desclin + " ----> " + (unidades - stock).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            materialEntregarLogistica = materialEntregarLogistica + desclin + " ----> " + (unidades - stock).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailLogistica = true;

                            enviarEmailCliente = true;
                        }
                        // Material Sin Stock
                        else if (stock <= 0)
                        {
                            materialPendiente = materialPendiente + fila["Desclin"].ToString() + " ----> " + (unidades).ToString() + " Unidades -" + " Plazo de entrega: " + ((available_now != "") ? available_now + " dias laborables" : "--") + "\r\n";
                            enviarEmailCliente = true;
                        }
                    }
                }
                #endregion

                //solo para documentos completos
                //naxAlbaran.ServirDocumento();
                //Finalizar las líneas

                naxAlbaran.FinServir();
                //Guardar el documento Servido
                idAlbV = naxAlbaran.Anade();

                string numAlbaran = sqlconnect.obtenerNumeroAlbaran(Math.Truncate(idAlbV).ToString());
                numAlbaran = Math.Truncate(Convert.ToDouble(numAlbaran)).ToString();

                string xml = "";
                // enviamos datos de la cabecera para generar el XML
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    csPedidos ped = new csPedidos();
                    xml = ped.exportarFicheroXMLSGIStreaming(numAlbaran.ToString());
                }
                else
                {
                    if (csGlobal.conexionDB.ToUpper().Contains("BASTIDE_KLS"))
                    {
                        xml = pedidos.generarTxtFileAlmacen(Documentos, numAlbaran);
                    }
                    else
                    {
                        xml = pedidos.generarXMLAlmacen(Documentos, numAlbaran);
                        xml = xml.Replace("utf-16", "utf-8");
                    }
                }
                string codCli = mail.Rows[0].ItemArray[0].ToString().Replace(" ", "");

                DataTable dt = generarAlbaran(idAlbV.ToString(), emailCliente.Trim(), codCli.Trim(), numAlbaran); // aqui peta

                //Preparación de campos para enviar por email
                string nifCli = mail.Rows[0].ItemArray[13].ToString(); // 
                string telCli = mail.Rows[0].ItemArray[8].ToString();
                string telCli2 = mail.Rows[0].ItemArray[17].ToString();
                string refCli = mail.Rows[0].ItemArray[6].ToString();
                string fecha = Convert.ToDateTime(mail.Rows[0].ItemArray[5].ToString()).ToShortDateString();
                string direccionEnt = mail.Rows[0].ItemArray[7].ToString();
                string nomCliente = mail.Rows[0].ItemArray[4].ToString();
                string cPostal = mail.Rows[0].ItemArray[11].ToString();
                string poblacion = mail.Rows[0].ItemArray[9].ToString();
                string provincia = mail.Rows[0].ItemArray[12].ToString();
                string pais = mail.Rows[0].ItemArray[16].ToString();

                if (nifCli == "" && (codCli != "" && emailCliente != ""))
                {
                    nifCli = obtenerNif(codCli, emailCliente);
                }

                // correo
                // direccion 2

                #region ENVIO MAIL SGI
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    string cabeceraMaterialPendiente = "Le comunicamos que los siguientes artículos de su pedido " + numeroDoc + " no se encuentran en Stock" + "\r\n" +
                   "en el plazo de 2 días lo recibirán." + "\r\n" +
                   "\r\n" +
                   "para cualquier aclaración, pueden contactar por mail a pedidos@sgi2010.es" + "\r\n" +
                   " ************************** " + "\r\n";
                    string cabeceraMaterialEntregarLogistica = "Solicitamos preparen el pedido número " + numeroDoc + " de nuestro cliente:" + "\r\n" +
                                "Número de Albarán: " + numAlbaran + "\r\n" +
                                "Nombre del Cliente :" + nomCliente + "(" + codCli + ")" + "\r\n" +
                                "Dirección Entrega: " + direccionEnt + "\r\n" +
                                "CP: " + cPostal + " " + poblacion + "\r\n" +
                                "Población: " + poblacion + "\r\n" +
                                "Provincia: " + provincia + "\r\n" +
                                "Teléfono: " + telCli + "\r\n" +
                                "Nif: " + nifCli + "\r\n" + " ************************** " + "\r\n" + "\r\n" + "\r\n";


                    string bodyMaterialPendiente = cabeceraMaterialPendiente + materialPendiente;
                    string bodyMaterialEntregarLogistica = cabeceraMaterialEntregarLogistica + materialEntregarLogistica;

                    if (!csGlobal.perfilAdminEsync)
                    {
                        if (enviarEmailCliente)
                        {
                            if (csGlobal.modeDebug)
                            {
                                emailCliente = "lluisvera@klosions.com";
                            }

                            mailing.sendMail("SGI-Pedido: " + numeroDoc + " - Material Pendiente", bodyMaterialPendiente, "", emailCliente + "," + csGlobal.EmailDestino);
                        }
                        if (enviarEmailLogistica)
                        {
                            mailing.sendMail("SGI - Preparación del Pedido " + numeroDoc + " del Cliente " + nomCliente + " (" + codCli + ")", bodyMaterialEntregarLogistica, xml, csGlobal.EmailDestino);
                        }
                    }
                }
                #endregion
                #region BASTIDE ENVIO MAIL
                else
                {
                    string cabeceraMaterialPendiente = string.Format("Le informamos que los artículos de su pedido nº {0} lo recibirán en los siguientes plazos de entrega (días laborables)\nPueden contactar con nosotros a través del correo info@bastidemedicalsalud.com para cualquier consulta al respecto. Gracias.\n\n", numeroDoc);
                    string cabeceraMaterialEntregarLogistica = "Solicitamos preparen el pedido número " + numeroDoc + " de nuestro cliente:" + "\r\n" +
                                "Número de Albarán: " + numAlbaran + "\r\n" +
                                "Nombre del Cliente :" + nomCliente + "(" + codCli + ")" + "\r\n" +
                                "Dirección Entrega: " + direccionEnt + "\r\n" +
                                "CP: " + cPostal + " " + poblacion + "\r\n" +
                                "Población: " + poblacion + "\r\n" +
                                "Provincia: " + provincia + "\r\n" +
                                "Teléfono: " + telCli + "\r\n" +
                                "Teléfono móvil: " + telCli2 + "\r\n" +
                                "Nif: " + nifCli + "\r\n" +
                                "Pais: " + pais + "\r\n" +
                                "Correo: " + emailCliente + "\r\n" +
                                "************************** " + "\r\n" + "\r\n" + "\r\n";


                    string bodyMaterialPendiente = cabeceraMaterialPendiente + materialPendiente;
                    string bodyMaterialEntregarLogistica = cabeceraMaterialEntregarLogistica + materialEntregarLogistica;

                    if (!csGlobal.perfilAdminEsync)
                    {
                        //if (enviarEmailCliente)
                        //{
                        //    //mailing.sendMail("BASTIDE-Pedido: " + numeroDoc + " - Material Pendiente", bodyMaterialPendiente, "", emailCliente + ",info@bastidemedicalsalud.com", "comercial@bastidemedicalsalud.com");
                        //    mailing.sendMail("BASTIDE-Pedido: " + numeroDoc + " - Material Pendiente", bodyMaterialPendiente, "", emailCliente, "", null, false, false, true);
                        //}
                        if (enviarEmailLogistica)
                        {
                            mailing.sendMail("BASTIDE - Preparación del Pedido " + numeroDoc + " del Cliente " + nomCliente + " (" + codCli + ")", bodyMaterialEntregarLogistica, xml, "", "", dt, false, false, true);
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.Message);
            }
            finally { }
        }


        //Generación del pdf del documento
        private DataTable generarAlbaran(string idAlbv, string email, string codcli, string numdoc)
        {
            csSqlConnects sql = new csSqlConnects();
            //string idfacv = Convert.ToInt32(sql.obtenerCampoTabla(string.Format("select cast(idpedv AS integer) from CABEPEDV where numdoc = '{0}'", numeroDoc))).ToString();
            string directorio = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Albaranes";
            csa3erpDocs doc = new csa3erpDocs();

            string ruta = doc.imprimirFacturaSinAbrirEnlace("AlbaránSHOP" + Math.Truncate(Convert.ToDouble(numdoc)).ToString(), idAlbv, directorio, "LSTIMPRALBVT.001");

            DataTable dt = new DataTable();
            dt.Columns.Add("codcli", typeof(string));
            dt.Columns.Add("ruta", typeof(string));
            dt.Columns.Add("email", typeof(string));
            DataRow dtRow = dt.NewRow();
            dtRow[0] = codcli;
            dtRow[1] = ruta;
            dtRow[2] = csGlobal.EmailDestino;
            dt.Rows.Add(dtRow);

            return dt;
        }

        private string obtenerNif(string codCli, string emailCliente)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            string consulta = string.Format("select distinct ps_address.dni from ps_customer  " +
                            "left join ps_address on ps_address.id_customer = ps_customer.id_customer " +
                            "where ps_customer.kls_a3erp_id = {0} and email = '{1}'", codCli, emailCliente);

            return mysql.obtenerDatoFromQuery(consulta);
        }

        private void checkFactura(Objetos.csCabeceraDoc cabecera, string idA3, bool docCompras)
        {
            csGlobal.docDestino = "Factura";
            a3ERPActiveX.Factura naxfactura = new a3ERPActiveX.Factura();
            //naxfactura.Iniciar();
            //naxfactura.OmitirMensajes = true;
            //naxfactura.ActivarAlarmaCab = false;
            //naxfactura.ActivarAlarmaLin = false;
            string scriptCuenta = "";
            string cuenta = "";
            string unidades = "";
            decimal documentoA3 = Convert.ToDecimal(idA3);


            string baseRPST = cabecera.totalBaseImponibleDocumento.ToString();
            string impuestoRPST = cabecera.totalImpuestosDocumento.ToString();
            string totalRPST = cabecera.totalDocumento.ToString();
            double diferencia = 0;
            string scriptBaseA3 = "";
            scriptBaseA3 = !docCompras ? "SELECT ROUND(BASE,2) FROM CABEFACV WHERE IDFACV= " : scriptBaseA3;
            scriptBaseA3 = docCompras ? "SELECT ROUND(BASE,2) FROM CABEFACC WHERE IDFACC= " : scriptBaseA3;

            string scriptImpuestoA3 = "";
            scriptImpuestoA3 = !docCompras ? "SELECT ROUND(TOTIVA,2) FROM CABEFACV WHERE IDFACV= " : scriptImpuestoA3;
            scriptImpuestoA3 = docCompras ? "SELECT ROUND(TOTIVA,2) FROM CABEFACC WHERE IDFACC= " : scriptImpuestoA3;

            var idDocA3 = idA3;
            var idDocRPST = cabecera.extNumdDoc;

            csSqlConnects sql = new klsync.csSqlConnects();
            sql.abrirConexion();

            var baseA3 = sql.obtenerCampoTabla(scriptBaseA3 + idDocA3);
            baseA3 = string.IsNullOrEmpty(baseA3) ? "0" : Math.Round(Convert.ToDouble(baseA3), 2).ToString();
            var impuestoA3 = sql.obtenerCampoTabla(scriptImpuestoA3 + idDocA3);
            impuestoA3 = string.IsNullOrEmpty(baseA3) ? "0" : Math.Round(Convert.ToDouble(baseA3), 2).ToString();

            //SELECT[CTADIFNREDO],[CTADIFPREDO] FROM[DATOSCON] - select BASE, TOTIVA, TOTDOC, IDFACV from CABEFACV - select BASE,TOTIVA,TOTDOC,IDFACc from CABEFACc

            if (baseRPST != baseA3)
            {
                diferencia = Convert.ToDouble(baseA3) - Convert.ToDouble(baseRPST);
                diferencia = Math.Round(diferencia, 2);
                if (diferencia != 0)
                {
                    if (diferencia < 0)
                    {
                        scriptCuenta = "SELECT [CTADIFNREDO] FROM [DATOSCON]";
                        cuenta = sql.obtenerCampoTabla(scriptCuenta);
                        unidades = "1";
                    }
                    else if (diferencia > 0)
                    {
                        scriptCuenta = "SELECT [CTADIFPREDO] FROM [DATOSCON]";
                        cuenta = sql.obtenerCampoTabla(scriptCuenta);
                        unidades = "-1";
                    }
                    naxfactura.NuevaLinea();
                    naxfactura.set_AsStringLin("CODART", "0");
                    naxfactura.set_AsStringLin("DESCLIN", "igualar base");
                    naxfactura.set_AsStringLin("UNIDADES", unidades);
                    //CTACONL o CUENTA revisar
                    naxfactura.set_AsStringLin("CUENTA", cuenta);
                    naxfactura.set_AsFloatLin("Prcmoneda", diferencia);
                }
            }
            else if (impuestoRPST != impuestoA3)
            {
                diferencia = Convert.ToDouble(impuestoA3) - Convert.ToDouble(impuestoRPST);
                diferencia = Math.Round(diferencia, 2);
                if (diferencia != 0)
                {
                    if (diferencia < 0)
                    {
                        scriptCuenta = "SELECT [CTADIFNREDO] FROM [DATOSCON]";
                        cuenta = sql.obtenerCampoTabla(scriptCuenta);
                        unidades = "1";
                    }
                    else if (diferencia > 0)
                    {
                        scriptCuenta = "SELECT [CTADIFPREDO] FROM [DATOSCON]";
                        cuenta = sql.obtenerCampoTabla(scriptCuenta);
                        unidades = "-1";
                    }
                    naxfactura.NuevaLinea();
                    naxfactura.set_AsStringLin("CODART", "0");
                    naxfactura.set_AsStringLin("DESCLIN", "igualar base");
                    naxfactura.set_AsStringLin("UNIDADES", unidades);
                    //CTACONL o CUENTA revisar
                    naxfactura.set_AsStringLin("CUENTA", cuenta);
                    naxfactura.set_AsFloatLin("prcmonedamasiva", diferencia);


                }
            }
        }
    }
}
