﻿namespace klsync
{
    partial class frNewCategorias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabCatCar = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainerCategoriasA3 = new System.Windows.Forms.SplitContainer();
            this.dgvCategorias = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCategoriasA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cargarCategoríasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarCategoríasSeleccionadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvCategoriasA3 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvFamCat = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCategoriasA32 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cargarFamiliasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarSeleccionadasAPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgvCaracteristicasPS = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCaracteristicasPS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cargarCaracterísticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvCaracteristicasA3 = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCaracteristicasA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarCaracterísticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvFeaturesProducts = new System.Windows.Forms.DataGridView();
            this.contextMenuStripCargarCaracteristicasFeatures = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cargarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabCatCar.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.splitContainerCategoriasA3.Panel1.SuspendLayout();
            this.splitContainerCategoriasA3.Panel2.SuspendLayout();
            this.splitContainerCategoriasA3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategorias)).BeginInit();
            this.contextMenuStripCategoriasA3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoriasA3)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCat)).BeginInit();
            this.contextMenuStripCategoriasA32.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasPS)).BeginInit();
            this.contextMenuStripCaracteristicasPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasA3)).BeginInit();
            this.contextMenuStripCaracteristicasA3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeaturesProducts)).BeginInit();
            this.contextMenuStripCargarCaracteristicasFeatures.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCatCar
            // 
            this.tabCatCar.Controls.Add(this.tabPage1);
            this.tabCatCar.Controls.Add(this.tabPage2);
            this.tabCatCar.Controls.Add(this.tabPage3);
            this.tabCatCar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCatCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tabCatCar.Location = new System.Drawing.Point(0, 0);
            this.tabCatCar.Name = "tabCatCar";
            this.tabCatCar.SelectedIndex = 0;
            this.tabCatCar.Size = new System.Drawing.Size(1293, 610);
            this.tabCatCar.TabIndex = 4;
            this.tabCatCar.Click += new System.EventHandler(this.tabCatCar_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainerCategoriasA3);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1285, 577);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cat. PS > A3";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainerCategoriasA3
            // 
            this.splitContainerCategoriasA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerCategoriasA3.Location = new System.Drawing.Point(3, 3);
            this.splitContainerCategoriasA3.Name = "splitContainerCategoriasA3";
            // 
            // splitContainerCategoriasA3.Panel1
            // 
            this.splitContainerCategoriasA3.Panel1.Controls.Add(this.dgvCategorias);
            // 
            // splitContainerCategoriasA3.Panel2
            // 
            this.splitContainerCategoriasA3.Panel2.Controls.Add(this.dgvCategoriasA3);
            this.splitContainerCategoriasA3.Size = new System.Drawing.Size(1279, 571);
            this.splitContainerCategoriasA3.SplitterDistance = 614;
            this.splitContainerCategoriasA3.SplitterWidth = 10;
            this.splitContainerCategoriasA3.TabIndex = 0;
            // 
            // dgvCategorias
            // 
            this.dgvCategorias.AllowUserToAddRows = false;
            this.dgvCategorias.AllowUserToDeleteRows = false;
            this.dgvCategorias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCategorias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategorias.ContextMenuStrip = this.contextMenuStripCategoriasA3;
            this.dgvCategorias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCategorias.Location = new System.Drawing.Point(0, 0);
            this.dgvCategorias.Name = "dgvCategorias";
            this.dgvCategorias.ReadOnly = true;
            this.dgvCategorias.Size = new System.Drawing.Size(614, 571);
            this.dgvCategorias.TabIndex = 4;
            // 
            // contextMenuStripCategoriasA3
            // 
            this.contextMenuStripCategoriasA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarCategoríasToolStripMenuItem,
            this.copiarCategoríasSeleccionadasToolStripMenuItem});
            this.contextMenuStripCategoriasA3.Name = "contextMenuStripCategoriasA3";
            this.contextMenuStripCategoriasA3.Size = new System.Drawing.Size(243, 48);
            // 
            // cargarCategoríasToolStripMenuItem
            // 
            this.cargarCategoríasToolStripMenuItem.Name = "cargarCategoríasToolStripMenuItem";
            this.cargarCategoríasToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.cargarCategoríasToolStripMenuItem.Text = "Cargar categorías";
            this.cargarCategoríasToolStripMenuItem.Click += new System.EventHandler(this.cargarCategoríasToolStripMenuItem_Click);
            // 
            // copiarCategoríasSeleccionadasToolStripMenuItem
            // 
            this.copiarCategoríasSeleccionadasToolStripMenuItem.Name = "copiarCategoríasSeleccionadasToolStripMenuItem";
            this.copiarCategoríasSeleccionadasToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.copiarCategoríasSeleccionadasToolStripMenuItem.Text = "Copiar categorías seleccionadas";
            this.copiarCategoríasSeleccionadasToolStripMenuItem.Click += new System.EventHandler(this.copiarCategoríasSeleccionadasToolStripMenuItem_Click);
            // 
            // dgvCategoriasA3
            // 
            this.dgvCategoriasA3.AllowUserToAddRows = false;
            this.dgvCategoriasA3.AllowUserToDeleteRows = false;
            this.dgvCategoriasA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategoriasA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCategoriasA3.Location = new System.Drawing.Point(0, 0);
            this.dgvCategoriasA3.Name = "dgvCategoriasA3";
            this.dgvCategoriasA3.ReadOnly = true;
            this.dgvCategoriasA3.Size = new System.Drawing.Size(655, 571);
            this.dgvCategoriasA3.TabIndex = 3;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvFamCat);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1285, 577);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cat. A3 > PS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvFamCat
            // 
            this.dgvFamCat.AllowUserToAddRows = false;
            this.dgvFamCat.AllowUserToDeleteRows = false;
            this.dgvFamCat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFamCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFamCat.ContextMenuStrip = this.contextMenuStripCategoriasA32;
            this.dgvFamCat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFamCat.Location = new System.Drawing.Point(3, 3);
            this.dgvFamCat.Name = "dgvFamCat";
            this.dgvFamCat.ReadOnly = true;
            this.dgvFamCat.Size = new System.Drawing.Size(1279, 571);
            this.dgvFamCat.TabIndex = 3;
            // 
            // contextMenuStripCategoriasA32
            // 
            this.contextMenuStripCategoriasA32.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarFamiliasToolStripMenuItem,
            this.exportarSeleccionadasAPSToolStripMenuItem});
            this.contextMenuStripCategoriasA32.Name = "contextMenuStripCategoriasA32";
            this.contextMenuStripCategoriasA32.Size = new System.Drawing.Size(219, 48);
            // 
            // cargarFamiliasToolStripMenuItem
            // 
            this.cargarFamiliasToolStripMenuItem.Name = "cargarFamiliasToolStripMenuItem";
            this.cargarFamiliasToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.cargarFamiliasToolStripMenuItem.Text = "Cargar familias";
            this.cargarFamiliasToolStripMenuItem.Click += new System.EventHandler(this.cargarFamiliasToolStripMenuItem_Click);
            // 
            // exportarSeleccionadasAPSToolStripMenuItem
            // 
            this.exportarSeleccionadasAPSToolStripMenuItem.Name = "exportarSeleccionadasAPSToolStripMenuItem";
            this.exportarSeleccionadasAPSToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.exportarSeleccionadasAPSToolStripMenuItem.Text = "Exportar seleccionadas a PS";
            this.exportarSeleccionadasAPSToolStripMenuItem.Click += new System.EventHandler(this.exportarSeleccionadasAPSToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1285, 577);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Características";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvFeaturesProducts);
            this.splitContainer1.Size = new System.Drawing.Size(1285, 577);
            this.splitContainer1.SplitterDistance = 580;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvCaracteristicasPS);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvCaracteristicasA3);
            this.splitContainer2.Size = new System.Drawing.Size(580, 577);
            this.splitContainer2.SplitterDistance = 287;
            this.splitContainer2.TabIndex = 0;
            // 
            // dgvCaracteristicasPS
            // 
            this.dgvCaracteristicasPS.AllowUserToAddRows = false;
            this.dgvCaracteristicasPS.AllowUserToDeleteRows = false;
            this.dgvCaracteristicasPS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCaracteristicasPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaracteristicasPS.ContextMenuStrip = this.contextMenuStripCaracteristicasPS;
            this.dgvCaracteristicasPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCaracteristicasPS.Location = new System.Drawing.Point(0, 0);
            this.dgvCaracteristicasPS.Name = "dgvCaracteristicasPS";
            this.dgvCaracteristicasPS.ReadOnly = true;
            this.dgvCaracteristicasPS.Size = new System.Drawing.Size(580, 287);
            this.dgvCaracteristicasPS.TabIndex = 1;
            // 
            // contextMenuStripCaracteristicasPS
            // 
            this.contextMenuStripCaracteristicasPS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarCaracterísticasToolStripMenuItem});
            this.contextMenuStripCaracteristicasPS.Name = "contextMenuStripCaracteristicasPS";
            this.contextMenuStripCaracteristicasPS.Size = new System.Drawing.Size(187, 26);
            // 
            // cargarCaracterísticasToolStripMenuItem
            // 
            this.cargarCaracterísticasToolStripMenuItem.Name = "cargarCaracterísticasToolStripMenuItem";
            this.cargarCaracterísticasToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.cargarCaracterísticasToolStripMenuItem.Text = "Cargar características";
            this.cargarCaracterísticasToolStripMenuItem.Click += new System.EventHandler(this.cargarCaracterísticasToolStripMenuItem_Click);
            // 
            // dgvCaracteristicasA3
            // 
            this.dgvCaracteristicasA3.AllowUserToAddRows = false;
            this.dgvCaracteristicasA3.AllowUserToDeleteRows = false;
            this.dgvCaracteristicasA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCaracteristicasA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaracteristicasA3.ContextMenuStrip = this.contextMenuStripCaracteristicasA3;
            this.dgvCaracteristicasA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCaracteristicasA3.Location = new System.Drawing.Point(0, 0);
            this.dgvCaracteristicasA3.Name = "dgvCaracteristicasA3";
            this.dgvCaracteristicasA3.ReadOnly = true;
            this.dgvCaracteristicasA3.Size = new System.Drawing.Size(580, 286);
            this.dgvCaracteristicasA3.TabIndex = 2;
            // 
            // contextMenuStripCaracteristicasA3
            // 
            this.contextMenuStripCaracteristicasA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.copiarCaracterísticasToolStripMenuItem});
            this.contextMenuStripCaracteristicasA3.Name = "contextMenuStripCaracteristicasA3";
            this.contextMenuStripCaracteristicasA3.Size = new System.Drawing.Size(204, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem1.Text = "Cargar características A3";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // copiarCaracterísticasToolStripMenuItem
            // 
            this.copiarCaracterísticasToolStripMenuItem.Name = "copiarCaracterísticasToolStripMenuItem";
            this.copiarCaracterísticasToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.copiarCaracterísticasToolStripMenuItem.Text = "Copiar características";
            this.copiarCaracterísticasToolStripMenuItem.Click += new System.EventHandler(this.copiarCaracterísticasToolStripMenuItem_Click);
            // 
            // dgvFeaturesProducts
            // 
            this.dgvFeaturesProducts.AllowUserToAddRows = false;
            this.dgvFeaturesProducts.AllowUserToDeleteRows = false;
            this.dgvFeaturesProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFeaturesProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFeaturesProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFeaturesProducts.Location = new System.Drawing.Point(0, 0);
            this.dgvFeaturesProducts.Name = "dgvFeaturesProducts";
            this.dgvFeaturesProducts.ReadOnly = true;
            this.dgvFeaturesProducts.Size = new System.Drawing.Size(701, 577);
            this.dgvFeaturesProducts.TabIndex = 6;
            // 
            // contextMenuStripCargarCaracteristicasFeatures
            // 
            this.contextMenuStripCargarCaracteristicasFeatures.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cargarToolStripMenuItem});
            this.contextMenuStripCargarCaracteristicasFeatures.Name = "contextMenuStripCargarCaracteristicasFeatures";
            this.contextMenuStripCargarCaracteristicasFeatures.Size = new System.Drawing.Size(110, 26);
            // 
            // cargarToolStripMenuItem
            // 
            this.cargarToolStripMenuItem.Name = "cargarToolStripMenuItem";
            this.cargarToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.cargarToolStripMenuItem.Text = "Cargar";
            this.cargarToolStripMenuItem.Click += new System.EventHandler(this.cargarToolStripMenuItem_Click);
            // 
            // frNewCategorias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1293, 610);
            this.Controls.Add(this.tabCatCar);
            this.Name = "frNewCategorias";
            this.Text = "Categorías";
            this.Load += new System.EventHandler(this.frCategorias_Load);
            this.tabCatCar.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainerCategoriasA3.Panel1.ResumeLayout(false);
            this.splitContainerCategoriasA3.Panel2.ResumeLayout(false);
            this.splitContainerCategoriasA3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategorias)).EndInit();
            this.contextMenuStripCategoriasA3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoriasA3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFamCat)).EndInit();
            this.contextMenuStripCategoriasA32.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasPS)).EndInit();
            this.contextMenuStripCaracteristicasPS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracteristicasA3)).EndInit();
            this.contextMenuStripCaracteristicasA3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeaturesProducts)).EndInit();
            this.contextMenuStripCargarCaracteristicasFeatures.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCatCar;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCategoriasA3;
        private System.Windows.Forms.ToolStripMenuItem cargarCategoríasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarCategoríasSeleccionadasToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvFamCat;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCategoriasA32;
        private System.Windows.Forms.ToolStripMenuItem cargarFamiliasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarSeleccionadasAPSToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dgvCaracteristicasPS;
        private System.Windows.Forms.DataGridView dgvCaracteristicasA3;
        private System.Windows.Forms.DataGridView dgvFeaturesProducts;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCaracteristicasPS;
        private System.Windows.Forms.ToolStripMenuItem cargarCaracterísticasToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCaracteristicasA3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copiarCaracterísticasToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCargarCaracteristicasFeatures;
        private System.Windows.Forms.ToolStripMenuItem cargarToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainerCategoriasA3;
        private System.Windows.Forms.DataGridView dgvCategoriasA3;
        private System.Windows.Forms.DataGridView dgvCategorias;
    }
}