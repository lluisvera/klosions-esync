﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frDialogDateTime : Form
    {
        public frDialogDateTime()
        {
            InitializeComponent();
            dtpFecha.CustomFormat = "dd-MM-yyyy";
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            csGlobal.fechaFormValue = dtpFecha.Value.ToString("yyyy-MM-dd");

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
