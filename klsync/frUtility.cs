﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Net;
using System.IO;


namespace klsync
{
    public partial class frUtility : Form
    {

        private static frUtility m_FormDefInstance;
        public static frUtility DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frUtility();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frUtility()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ComprobarServidoresSQL();
        }

        public void ComprobarServidoresSQL()
        {
            SqlDataSourceEnumerator servers = SqlDataSourceEnumerator.Instance;
            DataTable serversTable = servers.GetDataSources();
            //Mediante la función GetDataSources, obtengo una tabla con la información de los servidores

            foreach (DataRow row in serversTable.Rows)
            {
                string serverName = row["ServerName"].ToString();
                string InstanceName = row["InstanceName"].ToString();
                // Add this to your list
                //MessageBox.Show(serverName + InstanceName);
                comboBox1.Items.Add(serverName + InstanceName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection SqlCon = new SqlConnection("Data Source="+textBox1.Text+";Initial Catalog="+textBox2.Text+";Integrated Security=true;User Id=sa;Password=klosions;");

            //             ("server=VBA3ERP\\SQLEXPRESS;uid=sa;password=klosions");
            try
            {

                SqlCon.Open();
                System.Data.SqlClient.SqlCommand SqlCom = new System.Data.SqlClient.SqlCommand();
                SqlCom.Connection = SqlCon;
                SqlCom.CommandType = CommandType.StoredProcedure;
                SqlCom.CommandText = "sp_databases";
                System.Data.SqlClient.SqlDataReader SqlDR;
                SqlDR = SqlCom.ExecuteReader();

                while (SqlDR.Read())
                {
                    //MessageBox.Show(SqlDR.GetString(0));
                }
                SqlDR.Close();

               SqlDataAdapter MyDA = new SqlDataAdapter();
                MyDA.SelectCommand = new SqlCommand("Select * from clientes", SqlCon);
                
                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dataGridView1.DataSource = bSource;



                SqlCon.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            

        }

        private void btLoadFrasSincPresta_Click(object sender, EventArgs e)
        {
            cargarFrasSincronizadas();
        }

        private void cargarFrasSincronizadas()
        { 
        csGlobal.conexionDB = "maslejos";
            csMySqlConnect conector = new csMySqlConnect();
            MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
            conn.Open();
            // - DEBUG 
            MySqlDataAdapter MyDA = new MySqlDataAdapter();
            MyDA.SelectCommand = new MySqlCommand("select * from " + csGlobal.databasePS+ ".kls_invoice_erp", conn);
            //MyDA.SelectCommand = new MySqlCommand("SELECT * FROM `ps_category_product`", conn);
            //MyDA.SelectCommand = new MySqlCommand("Show tables;", conn);
            //MyDA.SelectCommand = new MySqlCommand("SHOW COLUMNS FROM ps_orders;", conn);
            DataTable table = new DataTable();
            MyDA.Fill(table);

            BindingSource bSource = new BindingSource();
            bSource.DataSource = table;

            dataGridView1.DataSource = bSource;

        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            foreach (DataGridViewRow fila in dataGridView1.SelectedRows)
            {
            mySql.borrar("kls_invoice_erp","invoice_number",fila.Cells[0].Value.ToString());
            }
            cargarFrasSincronizadas();
        }

        private void btLoadDBInfo_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySql=new csMySqlConnect();
            listbTablas.DataSource = mySql.dbTablas();
            listbTablas.DisplayMember = "table_name";
            listbTablas.ValueMember = "table_name";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            listbCampos.DataSource = mySql.dbCampos(listbTablas.Text);
            listbCampos.DisplayMember = "field";
            listbCampos.ValueMember = "field";

        }

        private void button5_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySql = new csMySqlConnect();
            dgvTabla.DataSource = mySql.dbDatos(listbTablas.Text).DefaultView;
            
  
        }

        }


    }

