﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;

namespace klsync
{
    class csPSWebServiceUpdate
    {

        public string obtenerEstadoArticuloA3(string idProduct)
        {
            csSqlConnects sqlConnects = new csSqlConnects();
            return sqlConnects.obtenerValorArticuloA3(idProduct);
        
        }

        public DataTable A3ObtenerDatosArticuloXML(string idProduct)
        {

            csIdiomas idiomasPS = new csIdiomas();

            string[] idiomasActivos = idiomasPS.idiomasActivosPS();
            string categoriasPSArticulo = "";
            string caracteristicasPSArticulo = "";
            string caracteristicasValuePSArticulo = "";

            DataTable articulosToUpdate = new DataTable();
            //int numLinea = 0;
            // dgvDetalleLineas.Rows.Add();
            int numLineaPedido = 0;
            Dictionary<string, string> diccioOrderLines = new Dictionary<string, string>();
            DataRow filaX;

            string[] TyC = new string[4];
            string urlRequest = "";
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            //Cargo el XML de cada Cabecera de Documento

            //urlRequest = "orders/" + fila.Cells[0].Value.ToString();
            urlRequest = "products/" + idProduct;
            xdoc = psWebService.readPSWebService("GET", urlRequest);
            //textBox2.Text = xdoc.ToString();
            XmlNodeList listaNodos = xdoc.GetElementsByTagName("product");

            DataTable articulos = new DataTable();
            articulos.Columns.Add("idioma");
            articulos.Columns.Add("categorias");
            articulos.Columns.Add("caracteristicas");

            //La primera fila es para los valores principales sin idiomas
            articulos.Rows.Add();
            //Añado una fila para cada idioma
            for (int i = 1; i <= idiomasActivos.Count(); i++)
            {
                articulos.Rows.Add();
                articulos.Rows[i][0] = i.ToString();
            }

            foreach (XmlNode elemento in listaNodos)
            {

                foreach (XmlNode columnas in elemento.ChildNodes)
                {
                    articulos.Columns.Add(columnas.Name);
                }

                foreach (XmlNode subNodo in elemento.ChildNodes)
                {
                    foreach (XmlNode traducciones in subNodo.ChildNodes)
                    {
                        //Recorro el XML de la cabecera, y en associations tengo las lineas
                        if (traducciones.Name == "language")
                        {
                            foreach (XmlNode idiomas in subNodo.ChildNodes)
                            {
                                articulos.Rows[Convert.ToInt16(idiomas.Attributes[0].Value)][subNodo.Name] = idiomas.InnerText;

                            }
                        }
                        else
                        {
                            articulos.Rows[0][subNodo.Name] = subNodo.InnerText;

                        }
                    }
                    if (subNodo.Name == "associations")
                    {
                        foreach (XmlNode asociaciones in subNodo.ChildNodes)
                        {

                            if (asociaciones.Name == "categories")
                            {

                                int cont = 0;
                                foreach (XmlNode categoria in asociaciones.ChildNodes)
                                {
                                    if (cont == 0)
                                    {
                                        categoriasPSArticulo = categoria.InnerText;
                                    }
                                    else
                                    {
                                        categoriasPSArticulo = categoriasPSArticulo + "," + categoria.InnerText;
                                    }
                                    cont++;

                                }
                                articulos.Rows[0]["categorias"] = categoriasPSArticulo;
                            }
                            if (asociaciones.Name == "product_features")
                            {

                                int cont = 0;
                                int contValue = 0;
                                foreach (XmlNode caracteristicas in asociaciones.ChildNodes)
                                {
                                    foreach (XmlNode caracteristica in caracteristicas.ChildNodes)
                                    {
                                        if (caracteristica.Name == "id")
                                        {
                                            if (cont == 0)
                                            {
                                                caracteristicasPSArticulo = caracteristica.InnerText;
                                            }
                                            else
                                            {
                                                caracteristicasPSArticulo = caracteristicasPSArticulo + "," + caracteristica.InnerText;
                                            }
                                            cont++;
                                        }
                                        if (caracteristica.Name == "id_feature_value")
                                        {
                                            if (contValue == 0)
                                            {
                                                caracteristicasValuePSArticulo = caracteristica.InnerText;
                                            }
                                            else
                                            {
                                                caracteristicasValuePSArticulo = caracteristicasValuePSArticulo + "," + caracteristica.InnerText;
                                            }
                                            contValue++;
                                        }

                                    }
                                }
                                articulos.Rows[0]["caracteristicas"] = caracteristicasPSArticulo + "|" + caracteristicasValuePSArticulo;
                            }

                        }

                    }
                }

            }
            return articulos;

        }
    }
}
