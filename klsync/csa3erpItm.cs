﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a3ERPActiveX;
using System.Data;


namespace klsync
{
    class csa3erpItm
    {
        /// <summary>
        /// Se usa para crear artículos de Repasat
        /// </summary>
        /// <param name="descripcion"></param>
        /// <param name="alias"></param>
        /// <param name="precio"></param>
        /// <param name="reference"></param>
        /// <param name="external_id"></param>
        /// <returns></returns>
        
        public string crearArticuloA3Repasat(string descripcion = "Articulo creado desde NAX", string alias = "", string precio = "", string reference = "", string external_id = "")
        {
            string codigo = "";
            Maestro naxArticulo = new Maestro();
            naxArticulo.Iniciar("Articulos");
            naxArticulo.Nuevo();

            if (string.IsNullOrEmpty(reference))
            {
                codigo = naxArticulo.NuevoCodigoNum();
            }
            else
            {
                codigo = reference;
            }
            //codigo = codigo.Replace(" ", "");
            codigo = codigo.TrimStart();
            naxArticulo.set_AsString("CODART", codigo);
            naxArticulo.set_AsString("DESCART", descripcion);
            naxArticulo.set_AsString("ARTALIAS", alias);
            naxArticulo.set_AsString("PRCVENTA", precio);
            //naxArticulo.set_AsString("KLS_ARTICULO_TIENDA", "T");
            //naxArticulo.set_AsString("KLS_FECHA_ALTA", DateTime.Now.ToShortDateString());
            //naxArticulo.set_AsString("KLS_FECHA_ULT_MOD", DateTime.Now.ToShortDateString());
            naxArticulo.set_AsString("PARAM8", "REPASAT");
            naxArticulo.set_AsString("PARAM9", reference);

            string codart = naxArticulo.get_AsString("CODART");
            naxArticulo.Guarda(true);
            naxArticulo.Acabar();

            return codart.Trim();
        }


        public void crearArticuloEnA3ERP(Objetos.csArticulo[] articulo, bool repasat=false)
        {
            try
            {
                DataTable dtParams = new DataTable();
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.Columns.Add("KEY");

                csRepasatWebService rpstWS = new csRepasatWebService();

                string codigo = "";
                Maestro naxArticulo = new Maestro();
                naxArticulo.Iniciar("Articulos");
                

                for (int i = 0; i < articulo.Length; i++)
                {
                    if (articulo[i] == null)
                    {
                        continue;
                    }
                    naxArticulo.Nuevo();
                    if (string.IsNullOrEmpty(articulo[i].codart))
                    {
                        codigo = naxArticulo.NuevoCodigoNum();
                    }
                    else
                    {
                        codigo = articulo[i].codart;
                    }
                    codigo = codigo.Replace(" ", "");
                    naxArticulo.set_AsString("CODART", codigo);
                    naxArticulo.set_AsString("DESCART", articulo[i].descArt);
                    naxArticulo.set_AsString("ARTALIAS", articulo[i].alias);
                    naxArticulo.set_AsString("PRCVENTA", articulo[i].precioSinIVA);
                    naxArticulo.set_AsString("PRCCOMPRA", articulo[i].precioCompra);
                    naxArticulo.set_AsString("PRCCOSTE", articulo[i].precioCoste.Replace(".", ","));
                    naxArticulo.set_AsString("ESCOMPRA", articulo[i].esCompra);
                    naxArticulo.set_AsString("ESVENTA", articulo[i].esVenta);
                    naxArticulo.set_AsString("AFESTOCK", articulo[i].esStock);
                    naxArticulo.set_AsString("ARTPRO", articulo[i].refProveedor);
                    naxArticulo.set_AsString("RPST_ID_PROD", (repasat ? articulo[i].id_product_PS : ""));

                    string codart = naxArticulo.get_AsString("CODART");
                    naxArticulo.Guarda(true);

                    if (repasat)
                    {
                        DataRow drParams = dtParams.NewRow();
                        drParams["FIELD"] = "codExternoArticulo";
                        drParams["KEY"] = articulo[i].id_product_PS;
                        drParams["VALUE"] = codart.TrimStart();
                        dtParams.Rows.Add(drParams);

                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = articulo[i].id_product_PS;
                        dtKeys.Rows.Add(drKeys);
                        //Sincronización de cuentas

                        rpstWS.sincronizarObjetoRepasat("products", "PUT", dtKeys, dtParams, "");
                    }
                }

                naxArticulo.Acabar();
            }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public void actualizarArticulos(Objetos.csArticulo[] articulo)
        {
            //a3ERP.abrirEnlace();
            string codart = "";
            string bloqueado = "";

            Maestro a3maestro = new Maestro();
            try
            {
                a3maestro.Iniciar("articulos");
                for (int i = 0; i < articulo.Count(); i++)
                {
                    if (a3maestro.Buscar(articulo[i].codart.ToUpper()))
                    {
                        bloqueado = (articulo[i].bloqueado == "1") ? "F" : "T";
                        a3maestro.Edita();

                        a3maestro.AsString["DESCART"]= articulo[i].descArt;
                        a3maestro.AsString["ARTALIAS"]= articulo[i].alias;
                        a3maestro.AsString["PRCVENTA"]= articulo[i].precioSinIVA;
                        a3maestro.AsString["PRCCOMPRA"]= articulo[i].precioCompra;
                        a3maestro.AsString["PRCCOSTE"]= articulo[i].precioCoste;
                        a3maestro.AsString["ESCOMPRA"]= articulo[i].esCompra;
                        a3maestro.AsString["ESVENTA"]= articulo[i].esVenta;
                        a3maestro.AsString["AFESTOCK"]= articulo[i].esStock;
                        a3maestro.AsString["ARTPRO"] = articulo[i].refProveedor;
                    }
                    else
                    {
                    }
                    a3maestro.Guarda(true);

                  

                }

            }
            finally
            {
                a3maestro.Acabar();
                a3maestro = null;
            }

        }



        public string crearArticuloA3V2(string descripcion = "Articulo creado desde NAX", string alias = "", string precio = "", string reference = "", string id_product = "", bool referenceAsCodart=false)
        {
            string codigo = "";
            Maestro naxArticulo = new Maestro();
            naxArticulo.Iniciar("Articulos");
            naxArticulo.Nuevo();
            if (referenceAsCodart)
            {
                codigo = reference;
            }
            else
            {
                codigo = naxArticulo.NuevoCodigoNum();
                codigo = codigo.Replace(" ", "");
            }

            
            naxArticulo.set_AsString("CODART", codigo);
            naxArticulo.set_AsString("DESCART", descripcion);
            naxArticulo.set_AsString("ARTALIAS", alias);
            naxArticulo.set_AsString("PRCVENTA", precio);
            naxArticulo.set_AsString("TIPOUNIDAD", "UNIDADES");
            naxArticulo.set_AsString("KLS_ARTICULO_TIENDA", "T");
            naxArticulo.set_AsString("KLS_ID_SHOP", id_product);

            string codart = naxArticulo.get_AsString("CODART");
            naxArticulo.Guarda(true);
            naxArticulo.Acabar();

            return codart.Trim();
        }

        public void verificarArticulo(string articuloA3)
        {
            a3ERPActiveX.Maestro articulo = new Maestro();
            articulo.Iniciar("articulo");
            if (articulo.Buscar(articuloA3))
            {
                //MessageBox.Show("el articulo " + articuloA3 + " existe");
            }
            else
            {
                //MessageBox.Show("el articulo "+articuloA3+" no existe");

            }
        }
    }
}
