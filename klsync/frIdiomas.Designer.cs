﻿namespace klsync
{
    partial class frIdiomas
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdiomas = new System.Windows.Forms.Label();
            this.dgvIdiomasPS = new System.Windows.Forms.DataGridView();
            this.dgvIdiomasA3 = new System.Windows.Forms.DataGridView();
            this.btLoadIdiomas = new System.Windows.Forms.Button();
            this.lblCargarIdiomas = new System.Windows.Forms.Label();
            this.lblIdiomasPs = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btEnlazarIdiomas = new System.Windows.Forms.Button();
            this.dgvIdiomasEnlazados = new System.Windows.Forms.DataGridView();
            this.btGrabarEnlaces = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFixLang = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasEnlazados)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIdiomas
            // 
            this.lblIdiomas.AutoSize = true;
            this.lblIdiomas.BackColor = System.Drawing.Color.White;
            this.lblIdiomas.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdiomas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblIdiomas.Location = new System.Drawing.Point(12, 9);
            this.lblIdiomas.Name = "lblIdiomas";
            this.lblIdiomas.Size = new System.Drawing.Size(162, 40);
            this.lblIdiomas.TabIndex = 1;
            this.lblIdiomas.Text = "IDIOMAS";
            // 
            // dgvIdiomasPS
            // 
            this.dgvIdiomasPS.AllowUserToAddRows = false;
            this.dgvIdiomasPS.AllowUserToDeleteRows = false;
            this.dgvIdiomasPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIdiomasPS.Location = new System.Drawing.Point(12, 173);
            this.dgvIdiomasPS.Name = "dgvIdiomasPS";
            this.dgvIdiomasPS.ReadOnly = true;
            this.dgvIdiomasPS.Size = new System.Drawing.Size(350, 177);
            this.dgvIdiomasPS.TabIndex = 2;
            this.dgvIdiomasPS.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIdiomasPS_CellDoubleClick);
            // 
            // dgvIdiomasA3
            // 
            this.dgvIdiomasA3.AllowUserToAddRows = false;
            this.dgvIdiomasA3.AllowUserToDeleteRows = false;
            this.dgvIdiomasA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIdiomasA3.Location = new System.Drawing.Point(388, 173);
            this.dgvIdiomasA3.Name = "dgvIdiomasA3";
            this.dgvIdiomasA3.ReadOnly = true;
            this.dgvIdiomasA3.Size = new System.Drawing.Size(367, 177);
            this.dgvIdiomasA3.TabIndex = 3;
            this.dgvIdiomasA3.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIdiomasA3_CellDoubleClick);
            // 
            // btLoadIdiomas
            // 
            this.btLoadIdiomas.BackgroundImage = global::klsync.Properties.Resources.eSync_Idiomas;
            this.btLoadIdiomas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btLoadIdiomas.Location = new System.Drawing.Point(12, 82);
            this.btLoadIdiomas.Name = "btLoadIdiomas";
            this.btLoadIdiomas.Size = new System.Drawing.Size(80, 55);
            this.btLoadIdiomas.TabIndex = 4;
            this.btLoadIdiomas.UseVisualStyleBackColor = true;
            this.btLoadIdiomas.Click += new System.EventHandler(this.btLoadIdiomas_Click);
            // 
            // lblCargarIdiomas
            // 
            this.lblCargarIdiomas.AutoSize = true;
            this.lblCargarIdiomas.ForeColor = System.Drawing.Color.White;
            this.lblCargarIdiomas.Location = new System.Drawing.Point(9, 66);
            this.lblCargarIdiomas.Name = "lblCargarIdiomas";
            this.lblCargarIdiomas.Size = new System.Drawing.Size(100, 13);
            this.lblCargarIdiomas.TabIndex = 5;
            this.lblCargarIdiomas.Text = "CARGAR IDIOMAS";
            // 
            // lblIdiomasPs
            // 
            this.lblIdiomasPs.AutoSize = true;
            this.lblIdiomasPs.ForeColor = System.Drawing.Color.White;
            this.lblIdiomasPs.Location = new System.Drawing.Point(12, 157);
            this.lblIdiomasPs.Name = "lblIdiomasPs";
            this.lblIdiomasPs.Size = new System.Drawing.Size(80, 13);
            this.lblIdiomasPs.TabIndex = 6;
            this.lblIdiomasPs.Text = "PRESTASHOP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(385, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "A3ERP";
            // 
            // btEnlazarIdiomas
            // 
            this.btEnlazarIdiomas.Location = new System.Drawing.Point(287, 393);
            this.btEnlazarIdiomas.Name = "btEnlazarIdiomas";
            this.btEnlazarIdiomas.Size = new System.Drawing.Size(75, 42);
            this.btEnlazarIdiomas.TabIndex = 8;
            this.btEnlazarIdiomas.Text = "Enlazar Idiomas";
            this.btEnlazarIdiomas.UseVisualStyleBackColor = true;
            this.btEnlazarIdiomas.Click += new System.EventHandler(this.btEnlazarIdiomas_Click);
            // 
            // dgvIdiomasEnlazados
            // 
            this.dgvIdiomasEnlazados.AllowUserToAddRows = false;
            this.dgvIdiomasEnlazados.AllowUserToDeleteRows = false;
            this.dgvIdiomasEnlazados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIdiomasEnlazados.Location = new System.Drawing.Point(388, 393);
            this.dgvIdiomasEnlazados.Name = "dgvIdiomasEnlazados";
            this.dgvIdiomasEnlazados.ReadOnly = true;
            this.dgvIdiomasEnlazados.Size = new System.Drawing.Size(367, 122);
            this.dgvIdiomasEnlazados.TabIndex = 9;
            // 
            // btGrabarEnlaces
            // 
            this.btGrabarEnlaces.Location = new System.Drawing.Point(287, 473);
            this.btGrabarEnlaces.Name = "btGrabarEnlaces";
            this.btGrabarEnlaces.Size = new System.Drawing.Size(75, 42);
            this.btGrabarEnlaces.TabIndex = 10;
            this.btGrabarEnlaces.Text = "Grabar Enlaces";
            this.btGrabarEnlaces.UseVisualStyleBackColor = true;
            this.btGrabarEnlaces.Click += new System.EventHandler(this.btGrabarEnlaces_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(385, 375);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "IDIOMAS ENLAZADOS";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(773, 473);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 42);
            this.button1.TabIndex = 12;
            this.button1.Text = "Grabar Enlaces";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFixLang
            // 
            this.btnFixLang.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFixLang.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnFixLang.Location = new System.Drawing.Point(921, 9);
            this.btnFixLang.Name = "btnFixLang";
            this.btnFixLang.Size = new System.Drawing.Size(46, 36);
            this.btnFixLang.TabIndex = 13;
            this.btnFixLang.Text = "Fix";
            this.btnFixLang.UseVisualStyleBackColor = true;
            this.btnFixLang.Click += new System.EventHandler(this.btnFixLang_Click);
            // 
            // frIdiomas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(979, 602);
            this.Controls.Add(this.btnFixLang);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btGrabarEnlaces);
            this.Controls.Add(this.dgvIdiomasEnlazados);
            this.Controls.Add(this.btEnlazarIdiomas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblIdiomasPs);
            this.Controls.Add(this.lblCargarIdiomas);
            this.Controls.Add(this.btLoadIdiomas);
            this.Controls.Add(this.dgvIdiomasA3);
            this.Controls.Add(this.dgvIdiomasPS);
            this.Controls.Add(this.lblIdiomas);
            this.Name = "frIdiomas";
            this.Text = "frIdiomas";
            this.Load += new System.EventHandler(this.frIdiomas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIdiomasEnlazados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdiomas;
        private System.Windows.Forms.DataGridView dgvIdiomasPS;
        private System.Windows.Forms.DataGridView dgvIdiomasA3;
        private System.Windows.Forms.Button btLoadIdiomas;
        private System.Windows.Forms.Label lblCargarIdiomas;
        private System.Windows.Forms.Label lblIdiomasPs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btEnlazarIdiomas;
        private System.Windows.Forms.DataGridView dgvIdiomasEnlazados;
        private System.Windows.Forms.Button btGrabarEnlaces;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnFixLang;
    }
}