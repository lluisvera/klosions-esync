﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace klsync
{
    public static class csMsg
    {
        public static void noResultsForThisQuery() 
        {
            "No hay resultados para esta busqueda".mb();
        }

        public static void noResults()
        {
            "No hay resultados".mb();
        }

        public static void operacionExito()
        {
            "La operación ha finalizado con éxito".mb();
        }

        public static void noRowSelected()
        {
            "No has seleccionado ninguna fila".mb();
        }
    }
}
