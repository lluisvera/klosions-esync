﻿namespace klsync
{
    partial class frSincronizarDocs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frSincronizarDocs));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboxFormasPago = new System.Windows.Forms.CheckBox();
            this.cbFormaPago = new System.Windows.Forms.ComboBox();
            this.rbDocsPtes = new System.Windows.Forms.RadioButton();
            this.rbQueryDocs = new System.Windows.Forms.RadioButton();
            this.rbQueryFras = new System.Windows.Forms.RadioButton();
            this.lbHasta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btLoadSelection = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvDocumentosPS = new System.Windows.Forms.DataGridView();
            this.tabDocumentos = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvDocsPs = new System.Windows.Forms.DataGridView();
            this.contextMenuSync = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.borrarSincronizaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarDocumentoManualmenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.cambiarEstadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgvDetalleLineas = new System.Windows.Forms.DataGridView();
            this.contextLineas = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarAtributoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetearAtributoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revisarTablasAtributosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.crearArticuloA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarReferenciaDelProductoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editarReferenciaDelProductoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarArtículoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslbInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSyncDocs = new System.Windows.Forms.ToolStrip();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPS)).BeginInit();
            this.tabDocumentos.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsPs)).BeginInit();
            this.contextMenuSync.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleLineas)).BeginInit();
            this.contextLineas.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 40);
            this.label1.TabIndex = 60;
            this.label1.Text = "DOCUMENTOS";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboxFormasPago);
            this.groupBox1.Controls.Add(this.cbFormaPago);
            this.groupBox1.Controls.Add(this.rbDocsPtes);
            this.groupBox1.Controls.Add(this.rbQueryDocs);
            this.groupBox1.Controls.Add(this.rbQueryFras);
            this.groupBox1.Controls.Add(this.lbHasta);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btLoadSelection);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(28, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(537, 178);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selección";
            // 
            // cboxFormasPago
            // 
            this.cboxFormasPago.AutoSize = true;
            this.cboxFormasPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxFormasPago.Location = new System.Drawing.Point(40, 134);
            this.cboxFormasPago.Name = "cboxFormasPago";
            this.cboxFormasPago.Size = new System.Drawing.Size(145, 24);
            this.cboxFormasPago.TabIndex = 42;
            this.cboxFormasPago.Text = "Formas de Pago";
            this.cboxFormasPago.UseVisualStyleBackColor = true;
            // 
            // cbFormaPago
            // 
            this.cbFormaPago.DropDownWidth = 200;
            this.cbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFormaPago.ForeColor = System.Drawing.Color.Navy;
            this.cbFormaPago.FormattingEnabled = true;
            this.cbFormaPago.Items.AddRange(new object[] {
            "LaCaixa",
            "Transferencia bancaria"});
            this.cbFormaPago.Location = new System.Drawing.Point(191, 130);
            this.cbFormaPago.Name = "cbFormaPago";
            this.cbFormaPago.Size = new System.Drawing.Size(137, 28);
            this.cbFormaPago.TabIndex = 41;
            // 
            // rbDocsPtes
            // 
            this.rbDocsPtes.AutoSize = true;
            this.rbDocsPtes.Checked = true;
            this.rbDocsPtes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDocsPtes.ForeColor = System.Drawing.Color.Navy;
            this.rbDocsPtes.Location = new System.Drawing.Point(40, 32);
            this.rbDocsPtes.Name = "rbDocsPtes";
            this.rbDocsPtes.Size = new System.Drawing.Size(148, 24);
            this.rbDocsPtes.TabIndex = 38;
            this.rbDocsPtes.TabStop = true;
            this.rbDocsPtes.Text = "Docs Pendientes";
            this.rbDocsPtes.UseVisualStyleBackColor = true;
            // 
            // rbQueryDocs
            // 
            this.rbQueryDocs.AutoSize = true;
            this.rbQueryDocs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbQueryDocs.ForeColor = System.Drawing.Color.Navy;
            this.rbQueryDocs.Location = new System.Drawing.Point(40, 92);
            this.rbQueryDocs.Name = "rbQueryDocs";
            this.rbQueryDocs.Size = new System.Drawing.Size(136, 24);
            this.rbQueryDocs.TabIndex = 37;
            this.rbQueryDocs.Text = "Todos los Docs";
            this.rbQueryDocs.UseVisualStyleBackColor = true;
            // 
            // rbQueryFras
            // 
            this.rbQueryFras.AutoSize = true;
            this.rbQueryFras.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbQueryFras.ForeColor = System.Drawing.Color.Navy;
            this.rbQueryFras.Location = new System.Drawing.Point(40, 62);
            this.rbQueryFras.Name = "rbQueryFras";
            this.rbQueryFras.Size = new System.Drawing.Size(95, 24);
            this.rbQueryFras.TabIndex = 36;
            this.rbQueryFras.Text = "Sólo Fras";
            this.rbQueryFras.UseVisualStyleBackColor = true;
            // 
            // lbHasta
            // 
            this.lbHasta.AutoSize = true;
            this.lbHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHasta.ForeColor = System.Drawing.Color.Navy;
            this.lbHasta.Location = new System.Drawing.Point(317, 67);
            this.lbHasta.Name = "lbHasta";
            this.lbHasta.Size = new System.Drawing.Size(58, 20);
            this.lbHasta.TabIndex = 29;
            this.lbHasta.Text = "Hasta*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(317, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Desde";
            // 
            // btLoadSelection
            // 
            this.btLoadSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLoadSelection.ForeColor = System.Drawing.Color.Navy;
            this.btLoadSelection.Image = ((System.Drawing.Image)(resources.GetObject("btLoadSelection.Image")));
            this.btLoadSelection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btLoadSelection.Location = new System.Drawing.Point(394, 130);
            this.btLoadSelection.Name = "btLoadSelection";
            this.btLoadSelection.Size = new System.Drawing.Size(137, 35);
            this.btLoadSelection.TabIndex = 3;
            this.btLoadSelection.Text = "Cargar Datos";
            this.btLoadSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btLoadSelection.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(28, 236);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1201, 321);
            this.tabControl1.TabIndex = 63;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvDocumentosPS);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1193, 295);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DOCUMENTOS PRESTASHOP";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvDocumentosPS
            // 
            this.dgvDocumentosPS.AllowUserToAddRows = false;
            this.dgvDocumentosPS.AllowUserToDeleteRows = false;
            this.dgvDocumentosPS.AllowUserToOrderColumns = true;
            this.dgvDocumentosPS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvDocumentosPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocumentosPS.Location = new System.Drawing.Point(10, 3);
            this.dgvDocumentosPS.Name = "dgvDocumentosPS";
            this.dgvDocumentosPS.ReadOnly = true;
            this.dgvDocumentosPS.Size = new System.Drawing.Size(300, 289);
            this.dgvDocumentosPS.TabIndex = 0;
            // 
            // tabDocumentos
            // 
            this.tabDocumentos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabDocumentos.Controls.Add(this.tabPage2);
            this.tabDocumentos.Controls.Add(this.tabPage3);
            this.tabDocumentos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.tabDocumentos.Location = new System.Drawing.Point(0, 78);
            this.tabDocumentos.Name = "tabDocumentos";
            this.tabDocumentos.SelectedIndex = 0;
            this.tabDocumentos.Size = new System.Drawing.Size(1096, 584);
            this.tabDocumentos.TabIndex = 60;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvDocsPs);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1088, 556);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "DOCUMENTOS PRESTASHOP";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvDocsPs
            // 
            this.dgvDocsPs.AllowUserToAddRows = false;
            this.dgvDocsPs.AllowUserToDeleteRows = false;
            this.dgvDocsPs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocsPs.ContextMenuStrip = this.contextMenuSync;
            this.dgvDocsPs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocsPs.Location = new System.Drawing.Point(3, 3);
            this.dgvDocsPs.Name = "dgvDocsPs";
            this.dgvDocsPs.ReadOnly = true;
            this.dgvDocsPs.Size = new System.Drawing.Size(1082, 550);
            this.dgvDocsPs.TabIndex = 0;
            this.dgvDocsPs.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvDocsPs_DataBindingComplete);
            // 
            // contextMenuSync
            // 
            this.contextMenuSync.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuSync.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borrarSincronizaciónToolStripMenuItem,
            this.sincronizarDocumentoManualmenteToolStripMenuItem,
            this.toolStripMenuItem2,
            this.cambiarEstadoToolStripMenuItem});
            this.contextMenuSync.Name = "contextMenuSync";
            this.contextMenuSync.Size = new System.Drawing.Size(259, 88);
            // 
            // borrarSincronizaciónToolStripMenuItem
            // 
            this.borrarSincronizaciónToolStripMenuItem.Name = "borrarSincronizaciónToolStripMenuItem";
            this.borrarSincronizaciónToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.borrarSincronizaciónToolStripMenuItem.Text = "&Borrar Sincronización";
            this.borrarSincronizaciónToolStripMenuItem.Click += new System.EventHandler(this.borrarSincronizaciónToolStripMenuItem_Click);
            // 
            // sincronizarDocumentoManualmenteToolStripMenuItem
            // 
            this.sincronizarDocumentoManualmenteToolStripMenuItem.Name = "sincronizarDocumentoManualmenteToolStripMenuItem";
            this.sincronizarDocumentoManualmenteToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.sincronizarDocumentoManualmenteToolStripMenuItem.Text = "Sincronizar Manualmente";
            this.sincronizarDocumentoManualmenteToolStripMenuItem.Click += new System.EventHandler(this.sincronizarDocumentoManualmenteToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(255, 6);
            // 
            // cambiarEstadoToolStripMenuItem
            // 
            this.cambiarEstadoToolStripMenuItem.Name = "cambiarEstadoToolStripMenuItem";
            this.cambiarEstadoToolStripMenuItem.Size = new System.Drawing.Size(258, 26);
            this.cambiarEstadoToolStripMenuItem.Text = "Cambiar estado";
            this.cambiarEstadoToolStripMenuItem.Click += new System.EventHandler(this.cambiarEstadoToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvDetalleLineas);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1088, 556);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "LÍNEAS DE DOCUMENTOS";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgvDetalleLineas
            // 
            this.dgvDetalleLineas.AllowUserToAddRows = false;
            this.dgvDetalleLineas.AllowUserToDeleteRows = false;
            this.dgvDetalleLineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalleLineas.ContextMenuStrip = this.contextLineas;
            this.dgvDetalleLineas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetalleLineas.Location = new System.Drawing.Point(3, 3);
            this.dgvDetalleLineas.Name = "dgvDetalleLineas";
            this.dgvDetalleLineas.ReadOnly = true;
            this.dgvDetalleLineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalleLineas.Size = new System.Drawing.Size(1082, 550);
            this.dgvDetalleLineas.TabIndex = 0;
            this.dgvDetalleLineas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalleLineas_CellContentClick);
            this.dgvDetalleLineas.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvDetalleLineas_CellFormatting);
            // 
            // contextLineas
            // 
            this.contextLineas.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextLineas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarAtributoToolStripMenuItem,
            this.resetearAtributoToolStripMenuItem,
            this.revisarTablasAtributosToolStripMenuItem,
            this.toolStripMenuItem1,
            this.crearArticuloA3ToolStripMenuItem,
            this.copiarReferenciaDelProductoToolStripMenuItem1,
            this.editarReferenciaDelProductoToolStripMenuItem1,
            this.verificarArtículoToolStripMenuItem});
            this.contextLineas.Name = "contextLineas";
            this.contextLineas.Size = new System.Drawing.Size(292, 192);
            // 
            // cambiarAtributoToolStripMenuItem
            // 
            this.cambiarAtributoToolStripMenuItem.Image = global::klsync.Properties.Resources.refresh_button;
            this.cambiarAtributoToolStripMenuItem.Name = "cambiarAtributoToolStripMenuItem";
            this.cambiarAtributoToolStripMenuItem.Size = new System.Drawing.Size(291, 26);
            this.cambiarAtributoToolStripMenuItem.Text = "Cambiar atributo";
            this.cambiarAtributoToolStripMenuItem.Click += new System.EventHandler(this.cambiarAtributoToolStripMenuItem_Click);
            // 
            // resetearAtributoToolStripMenuItem
            // 
            this.resetearAtributoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("resetearAtributoToolStripMenuItem.Image")));
            this.resetearAtributoToolStripMenuItem.Name = "resetearAtributoToolStripMenuItem";
            this.resetearAtributoToolStripMenuItem.Size = new System.Drawing.Size(291, 26);
            this.resetearAtributoToolStripMenuItem.Text = "Resetear atributo";
            this.resetearAtributoToolStripMenuItem.Click += new System.EventHandler(this.resetearAtributoToolStripMenuItem_Click);
            // 
            // revisarTablasAtributosToolStripMenuItem
            // 
            this.revisarTablasAtributosToolStripMenuItem.Name = "revisarTablasAtributosToolStripMenuItem";
            this.revisarTablasAtributosToolStripMenuItem.Size = new System.Drawing.Size(291, 26);
            this.revisarTablasAtributosToolStripMenuItem.Text = "Revisar Tablas Atributos";
            this.revisarTablasAtributosToolStripMenuItem.Click += new System.EventHandler(this.revisarTablasAtributosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(288, 6);
            // 
            // crearArticuloA3ToolStripMenuItem
            // 
            this.crearArticuloA3ToolStripMenuItem.Name = "crearArticuloA3ToolStripMenuItem";
            this.crearArticuloA3ToolStripMenuItem.Size = new System.Drawing.Size(291, 26);
            this.crearArticuloA3ToolStripMenuItem.Text = "Crear articulo A3";
            this.crearArticuloA3ToolStripMenuItem.Click += new System.EventHandler(this.crearArticuloA3ToolStripMenuItem_Click);
            // 
            // copiarReferenciaDelProductoToolStripMenuItem1
            // 
            this.copiarReferenciaDelProductoToolStripMenuItem1.Name = "copiarReferenciaDelProductoToolStripMenuItem1";
            this.copiarReferenciaDelProductoToolStripMenuItem1.Size = new System.Drawing.Size(291, 26);
            this.copiarReferenciaDelProductoToolStripMenuItem1.Text = "Copiar referencia del producto";
            this.copiarReferenciaDelProductoToolStripMenuItem1.Click += new System.EventHandler(this.copiarReferenciaDelProductoToolStripMenuItem1_Click);
            // 
            // editarReferenciaDelProductoToolStripMenuItem1
            // 
            this.editarReferenciaDelProductoToolStripMenuItem1.Name = "editarReferenciaDelProductoToolStripMenuItem1";
            this.editarReferenciaDelProductoToolStripMenuItem1.Size = new System.Drawing.Size(291, 26);
            this.editarReferenciaDelProductoToolStripMenuItem1.Text = "Editar referencia del producto";
            this.editarReferenciaDelProductoToolStripMenuItem1.Click += new System.EventHandler(this.editarReferenciaDelProductoToolStripMenuItem1_Click);
            // 
            // verificarArtículoToolStripMenuItem
            // 
            this.verificarArtículoToolStripMenuItem.Name = "verificarArtículoToolStripMenuItem";
            this.verificarArtículoToolStripMenuItem.Size = new System.Drawing.Size(291, 26);
            this.verificarArtículoToolStripMenuItem.Text = "&Verificar artículo";
            this.verificarArtículoToolStripMenuItem.Click += new System.EventHandler(this.verificarArtículoToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblInfo,
            this.tsslbInfo,
            this.toolStripStatusLabel1,
            this.progressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 662);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1108, 26);
            this.statusStrip1.TabIndex = 66;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslblInfo
            // 
            this.tsslblInfo.Name = "tsslblInfo";
            this.tsslblInfo.Size = new System.Drawing.Size(0, 21);
            // 
            // tsslbInfo
            // 
            this.tsslbInfo.Name = "tsslbInfo";
            this.tsslbInfo.Size = new System.Drawing.Size(0, 21);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(991, 21);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 20);
            // 
            // toolStripSyncDocs
            // 
            this.toolStripSyncDocs.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripSyncDocs.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripSyncDocs.Location = new System.Drawing.Point(0, 0);
            this.toolStripSyncDocs.Name = "toolStripSyncDocs";
            this.toolStripSyncDocs.Size = new System.Drawing.Size(1108, 25);
            this.toolStripSyncDocs.TabIndex = 67;
            // 
            // pbLogo
            // 
            this.pbLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.BackgroundImage")));
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbLogo.Location = new System.Drawing.Point(1159, 9);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(89, 76);
            this.pbLogo.TabIndex = 61;
            this.pbLogo.TabStop = false;
            // 
            // frSincronizarDocs
            // 
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1108, 688);
            this.Controls.Add(this.toolStripSyncDocs);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabDocumentos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frSincronizarDocs";
            this.Text = "DOCUMENTOS COMERCIALES";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frSincronizarDocs_FormClosing);
            this.Load += new System.EventHandler(this.frSincronizarDocs_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentosPS)).EndInit();
            this.tabDocumentos.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocsPs)).EndInit();
            this.contextMenuSync.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleLineas)).EndInit();
            this.contextLineas.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cboxFormasPago;
        private System.Windows.Forms.ComboBox cbFormaPago;
        private System.Windows.Forms.RadioButton rbDocsPtes;
        private System.Windows.Forms.RadioButton rbQueryDocs;
        private System.Windows.Forms.RadioButton rbQueryFras;
        private System.Windows.Forms.Label lbHasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btLoadSelection;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvDocumentosPS;
        private System.Windows.Forms.TabControl tabDocumentos;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvDocsPs;
        
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvDetalleLineas;
        private System.Windows.Forms.ContextMenuStrip contextMenuSync;
        private System.Windows.Forms.ToolStripMenuItem borrarSincronizaciónToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslblInfo;
        private System.Windows.Forms.ToolStripMenuItem sincronizarDocumentoManualmenteToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStripSyncDocs;
        private System.Windows.Forms.ToolStripStatusLabel tsslbInfo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ContextMenuStrip contextLineas;
        private System.Windows.Forms.ToolStripMenuItem cambiarAtributoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetearAtributoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearArticuloA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarReferenciaDelProductoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editarReferenciaDelProductoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cambiarEstadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarArtículoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revisarTablasAtributosToolStripMenuItem;
    }
}