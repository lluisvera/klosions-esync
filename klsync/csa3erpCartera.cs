﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using a3ERPActiveX;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace klsync
{
    class csa3erpCartera
    {
        csSqlConnects sql = new csSqlConnects();

        public int crearRemesa(DataSet remesa)
        {
            int remesasProcesadas = 0;
            string errorNumRemesa = "";
            string errorNumFra = "";
            string errrorNumVto = "";
            string errorMessage = "";
            string efectosRemesas = "";
            try
            {
                csRepasatWebService rpstWS = new csRepasatWebService();

                DataTable cabeceraRemesa = remesa.Tables["CABECERA"];
                DataTable lineasRemesa = remesa.Tables["EFECTOS"];
                string numRemesaCab = "";
                string numRemesaLin = "";
                string idFactura = "";
                decimal idCartera = 0;
                decimal numCartera = 0;
                int numeroVencimiento = 0;
                decimal idRemesaCreada = 0;
                string idRemesaRPST = "";
                string codBancoRemesa = "";
                string cuentaBanco = "";
                string fechaRemesa = "";
                string fechaCobroRemesa = "";
              
                //Crear Remesa
                csa3erp a3 = new csa3erp();
                a3.abrirEnlace();


                a3ERPActiveX.Remesa remesaV = new a3ERPActiveX.Remesa();
                remesaV.Iniciar();

                for (int i = 0; i < cabeceraRemesa.Rows.Count; i++)
                {
                    try
                    {
                        numRemesaCab = cabeceraRemesa.Rows[i]["NUMREMESA"].ToString();
                        errorNumRemesa = numRemesaCab;
                        idRemesaRPST = cabeceraRemesa.Rows[i]["REPASAT_ID_DOC"].ToString();
                        codBancoRemesa = cabeceraRemesa.Rows[i]["BANCO"].ToString();
                        cuentaBanco = cabeceraRemesa.Rows[i]["CUENTA"].ToString();

                        fechaCobroRemesa = cabeceraRemesa.Rows[i]["FECHA_COBRO"].ToString();

                        fechaRemesa = cabeceraRemesa.Rows[i]["FECHA"].ToString();
                        //La variable fecha remesa no se esta utilizando, debería ser la que se envía en vez de fechaCobro

                        //fechaRemesa = cabeceraRemesa.Rows[i]["CUENTA"].ToString();

                        if (string.IsNullOrEmpty(numRemesaCab) || cabeceraRemesa.Rows[i]["ACCIÓN"].ToString() == "NOSYNC")
                        {
                            continue;
                        }

                        //remesaV.Nueva(true, "02/03/2019", "       1", "EURO", "1", false);
                        // remesaV.Nueva(true, fechaCobroRemesa ,codBancoRemesa, "EURO", "2", false);

                        //Utilizamos NuevaV2 porque nos pide el mandato SEPA
                        remesaV.NuevaV2(true, fechaRemesa, codBancoRemesa, "EURO", csGlobal.tipoContable.ToString(), "CORE (B2C)", false);

                        //remesaV.set_AsString("REFERENCIA", "REMESA Nº: " + numRemesaCab);
                        //Pendiente de 

                        for (int ii = 0; ii < lineasRemesa.Rows.Count; ii++)
                        {
                            numRemesaLin = lineasRemesa.Rows[ii]["NUMREMESA"].ToString();
                            if (numRemesaCab == numRemesaLin)
                            {
                                string importe = lineasRemesa.Rows[ii]["IMPORTE_EFECTO"].ToString();

                                //VERIFICO QUE NO SEA UN ANTICIPO

                                if (lineasRemesa.Rows[ii]["ANTICIPO"].ToString() == "SI")
                                {
                                    //if (!existeEfecto(lineasRemesa.Rows[ii]["SERIE"].ToString(), lineasRemesa.Rows[ii]["REFANTICIPO"].ToString(), false, null))
                                    //{
                                    //    crearEfectoManual(lineasRemesa.Rows[ii]["SERIE"].ToString(), lineasRemesa.Rows[ii]["REFANTICIPO"].ToString(), lineasRemesa.Rows[ii]["CODIGO"].ToString(), lineasRemesa.Rows[ii]["FECHA VTO"].ToString(), lineasRemesa.Rows[ii]["IMPORTE_EFECTO"].ToString(), true);

                                    //}
                                }

                                try
                                {
                                    errorNumFra = lineasRemesa.Rows[ii]["NUM_FRA"].ToString();
                                    errrorNumVto = numeroVencimiento.ToString();

                                    numeroVencimiento = Convert.ToInt16(lineasRemesa.Rows[ii]["NUM VTO"].ToString());
                                    idFactura = lineasRemesa.Rows[ii]["IDA3 FRA"].ToString();
                                    numCartera = Convert.ToDecimal(obtenerNumCartera(true, idFactura));
                                    efectosRemesas = efectosRemesas + ',' + numCartera;

                                    remesaV.AnadirEfecto(numCartera, numeroVencimiento);
                                }
                                catch (Exception ex) {
                                    if (csGlobal.modoManual)
                                    {
                                        string efectosAfectador = efectosRemesas;
                                        ex.Message.mb();

                                    }
                                }

                            }
                        }
                        idRemesaCreada = remesaV.Anade();
                        cobrarRemesaTotal(idRemesaCreada, cabeceraRemesa.Rows[i], true);

                        csUtilidades.ejecutarConsulta("UPDATE __REMESAS SET REFERENCIA='REMESA REPASAT Nº: " + numRemesaCab + "', RPST_ID_REMESA=" + idRemesaRPST + " WHERE IDREMESA=" + idRemesaCreada, false);

                        rpstWS.actualizarDocumentoRepasat("remittances", "codExternoRemesa", idRemesaRPST, idRemesaCreada.ToString());
                        remesasProcesadas++;

                    }
                    catch (Exception ex)
                    {
                        errorMessage = "Remesa: " + errorNumRemesa + "\n" +
                            "Fra: " + errorNumFra + "\n" +
                            "Efecto: " + errrorNumVto + "\n";
                        (errorMessage + ex.Message).mb();
                        continue;
                    }
                    
                   
                }
                remesaV.Acabar();
                a3.cerrarEnlace();
                return remesasProcesadas;
            }
            catch (Exception ex)
            {
                errorMessage = "Remesa: " + errorNumRemesa + "\n" +
                            "Fra: " + errorNumFra + "\n" +
                            "Efecto: " + errrorNumVto + "\n";
                (errorMessage + ex.Message).mb();
                return remesasProcesadas;
            }
        }

        public int crearAgrupacion(Objetos.csCartera[] cartera, bool ventas) {
            int numEfectosTratados = 0;
            try
            {
                double importeEfectoAgrupado = 0;
                double importeEfectos = 0;
                int? numAgrupacion = 0;
                //Realizo la Agrupación
              
                var efectosAgrupados = cartera.Where(x => x.esEfectoAgrupado == true);
                var carteraAgrupada = cartera.Where(x => x.perteneceEfectoAgrupado == true);

                csRepasatWebService rpstWS = new csRepasatWebService();
                DataTable dtAgrupacion = new DataTable();
                dtAgrupacion.Columns.Add("idRepasat");
                dtAgrupacion.Columns.Add("idA3ERP");
                

                csa3erp a3 = new csa3erp();
                a3.abrirEnlace();

                a3ERPActiveX.Agrupacion newAgrupacion = new a3ERPActiveX.Agrupacion();
                newAgrupacion.Iniciar();

                foreach (Objetos.csCartera agrupacion in efectosAgrupados)
                {
                    importeEfectoAgrupado = 0;
                    importeEfectos = 0;
                    importeEfectoAgrupado = agrupacion.importeEfecto;
                    numAgrupacion = agrupacion.idAgrupacion;
                    foreach (Objetos.csCartera efectosAgr in carteraAgrupada) {
                        if (numAgrupacion == efectosAgr.idAgrupacion) {

                            importeEfectos += efectosAgr.importeEfecto;
                            if (Math.Round(importeEfectos, 2) == Math.Round(importeEfectoAgrupado, 2))
                            {
                                string amigo = "hola";
                              
                                newAgrupacion.Nueva(true, agrupacion.fechaVencimiento.ToString("dd/MM/yyyy"), agrupacion.fechaVencimiento.ToString("dd/MM/yyyy"), agrupacion.codIC,agrupacion.codDocumentoPago, "       1", "EURO", "1");
                                foreach (Objetos.csCartera efectosAgr2 in carteraAgrupada)
                                {
                                    if (numAgrupacion == efectosAgr2.idAgrupacion)
                                    {
                                        string idCartera = obtenerNumCartera(ventas, efectosAgr2.externalFacturaId, Convert.ToInt32(efectosAgr2.numVencimiento)).ToString();
                                        newAgrupacion.AnadirEfecto(Convert.ToInt32(idCartera), Convert.ToInt32(efectosAgr2.numVencimiento));
                                        DataRow drDetalleAgr = dtAgrupacion.NewRow();
                                        drDetalleAgr["idRepasat"] = efectosAgr2.extNumDoc;
                                        drDetalleAgr["idA3ERP"] = idCartera;
                                        dtAgrupacion.Rows.Add(drDetalleAgr);

                                       
                                        numEfectosTratados++;
                                    }
                                }
                                decimal numCarteraAgrupacion = newAgrupacion.Anade();
                                rpstWS.actualizarDocumentoRepasat("incomingpayments", "refAgrupacionCarteraCobros", agrupacion.extNumDoc, numCarteraAgrupacion.ToString());
                                rpstWS.actualizarDocumentoRepasat("incomingpayments", "codExternoAgrupacion", agrupacion.extNumDoc, numCarteraAgrupacion.ToString());

                                //actualizo el contenido de la agrupación
                                foreach (DataRow fila in dtAgrupacion.Rows) {
                                    rpstWS.actualizarDocumentoRepasat("incomingpayments", "codExternoCarteraCobros", fila["idRepasat"].ToString(), fila["idA3ERP"].ToString());
                                    rpstWS.actualizarDocumentoRepasat("incomingpayments", "codExternoAgrupacion", fila["idRepasat"].ToString(), numCarteraAgrupacion.ToString());
                                }
                                dtAgrupacion.Clear();
                            }
                        }

                    }
                }
               
                newAgrupacion.Acabar();
                a3.cerrarEnlace();

            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }



            int hola = 0;
            return numEfectosTratados;
        }


        private bool existeEfecto(string serie, string numDoc, bool procedeDeFactura, string idFactura)
        {
            try
            {
                string anexoWhere = string.IsNullOrEmpty(idFactura) ? "" : " AND PROCEDEID=" + procedeDeFactura;
                string anexoEfecto = procedeDeFactura ? "" : " AND PROCEDE='' ";
                DataTable dt= sql.obtenerDatosSQLScript("SELECT NUMCARTERA, IDCARTERA, PROCEDE, PROCEDEID FROM CARTERA WHERE LTRIM(SERIE)='" + serie + "' AND NUMDOC=" + numDoc + anexoEfecto);
                string numCartera = dt.Rows[0]["NUMCARTERA"].ToString();
                string idCartera = dt.Rows[0]["IDCARTERA"].ToString();
                return (string.IsNullOrEmpty(idCartera) ? false : true);
            }

            catch
            {
                return false;
            }
            
        }

        //Cobrar Remesa
        private void cobrarRemesaTotal(decimal numRemesa, DataRow banco=null,  bool conectaxionActiva=false)
        {
            try
            {
                string numRemesaCab = banco["NUMREMESA"].ToString();
                string idRemesaRPST = banco["REPASAT_ID_DOC"].ToString();
                string codBancoRemesa = banco["BANCO"].ToString();
                string cuentaBanco = banco["CUENTA"].ToString();

                string fechaCobroRemesa = banco["FECHA_COBRO"].ToString();
                string fechaRemesa = banco["FECHA"].ToString();
                //string fechaCobroRemesa = banco["FECHA"].ToString();
                //string fechaRemesa = banco["CUENTA"].ToString();

                csa3erp a3 = new csa3erp();
                if (!conectaxionActiva) a3.abrirEnlace();

                a3ERPActiveX.OperacionesCartera cart = new a3ERPActiveX.OperacionesCartera();
                //cart.CobrarRemesa(numRemesa, true, false, "57200001", "57200001", "26/02/2019", "26/02/2019", "       1", 1, "", 0, "", "", "", "");
                cart.CobrarRemesa(numRemesa, true, false, cuentaBanco, cuentaBanco ,fechaCobroRemesa, fechaRemesa, codBancoRemesa, 1, "", 0, "", "", "", "");

                if (!conectaxionActiva)  a3.cerrarEnlace();
            }
            catch
            {
                ("Problema al pagar la Remesa " + numRemesa).mb();
            }
        }

        //Cobrar Efectos
        public int cobrarPagarEfecto(Objetos.csCartera[] cartera, bool ventas, bool impagados=false, bool movsContables=true)
        {
            int numEfectosTratados = 0;
            string errorLogMessage = "";

            GestionErrores.csGestionErrores errorLog = new GestionErrores.csGestionErrores();
            try
            {
                string cuentaContableBanco = "";
                string cuentaContableRiesgo = "";
                string codigoBanco = "";
                double importeEfecto = 0;
                decimal importe = 0;

                double importeGastosImpagados = 0;
                decimal importeGastosDevolucion = 0;

                int numEfecto = 1;
                string idFacturaExterna = "";
                string idCartera = "";
                string fechaEfecto = "";
                string fechaCobro = "";
                string fechaImpagado = "";

                //En las devoluciones si hay imputación de gastos, debemos saber el numero de vencimiento generado
                int nuevoVencimientoDevolucion = 0;

                csRepasatWebService rpstWS = new csRepasatWebService();


                csa3erp a3 = new csa3erp();
                a3.abrirEnlace();

                a3ERPActiveX.OperacionesCartera opera = new a3ERPActiveX.OperacionesCartera();

                for (int i = 0; i < cartera.Count(); i++)
                {

                    if (cartera[i] != null)
                    {
                        //Solo actualizo si está cobrado el efecto y no está remesado
                        if (cartera[i].cobrado == "0" || (!string.IsNullOrEmpty(cartera[i].idRemesa) && cartera[i].idRemesa != "0"))
                        {
                            if (!impagados)
                            {
                                continue;
                            }
                        }
                        
                        //Gestión de Errores
                        errorLog.errorCodBanco= string.IsNullOrEmpty(cartera[i].cuentaContableBanco) ? "":cartera[i].cuentaContableBanco.ToString();
                        errorLog.errorSerieDocumento = string.IsNullOrEmpty(cartera[i].serieDoc) ? "" : cartera[i].serieDoc.ToString();
                        errorLog.errorNumeroDocumento = string.IsNullOrEmpty(cartera[i].numDocA3) ? "" : cartera[i].numDocA3.ToString();
                        errorLog.errorNumVencimiento = string.IsNullOrEmpty(cartera[i].numVencimiento) ? "" : cartera[i].numVencimiento.ToString();
                        errorLog.errorCodClienteA3ERP = string.IsNullOrEmpty(cartera[i].codIC) ? "" : cartera[i].codIC.ToString();
                        errorLog.errorNomClienteA3ERP = string.IsNullOrEmpty(cartera[i].nombreIC) ? "" : cartera[i].nombreIC.ToString();
                        errorLog.errorImporteCobrado = string.IsNullOrEmpty(cartera[i].importeEfectoCobrado.ToString()) ? "" : cartera[i].importeEfectoCobrado.ToString();
                        //Fin gestión de errores

                        cuentaContableBanco = string.IsNullOrEmpty(cartera[i].cuentaContableBanco) ? "":cartera[i].cuentaContableBanco.ToString();
                        cuentaContableRiesgo = string.IsNullOrEmpty(cartera[i].cuentaContableBanco) ? "" : cartera[i].cuentaContableBanco.ToString();
                        codigoBanco = string.IsNullOrEmpty(cartera[i].codBanco) ? "": cartera[i].codBanco.ToString();
                        numEfecto = Convert.ToInt16(errorLog.errorNumVencimiento);

                        //a3ERPActiveX.Cartera cart = new a3ERPActiveX.Cartera();
                        importeEfecto = cartera[i].importeEfecto;
                        importe = Convert.ToDecimal(importeEfecto);
                        importeGastosImpagados = cartera[i].importeGastosImpagado;
                        importeGastosDevolucion = Convert.ToDecimal(importeGastosImpagados);


                        numEfecto = Convert.ToInt16(cartera[i].numVencimiento);

                        fechaCobro = cartera[i].fechaCobroPago.ToString("dd/MM/yyyy");
                        fechaEfecto = cartera[i].fechaVencimiento.ToString("dd/MM/yyyy");
                        fechaImpagado  = Convert.ToDateTime(cartera[i].fechaImpagado).ToString("dd/MM/yyyy");

                        //Cobro el efecto i actualizo el Id del efecto en Repasat
                        //Si no se el id del Efecto, lo busco
                        if (cartera[i].esEfectoAgrupado)
                        {
                            idCartera = obtenerNumAgrupacion(ventas, cartera[i].refAgrupacionCarteraCobros, 0).ToString();
                        }
                        else if (!string.IsNullOrEmpty(cartera[i].codExternoAgrupacion)) {
                            idCartera = sql.obtenerCampoTabla("SELECT CAST(NUMCARTERA AS INT) AS IDCARTERA FROM CARTERA WHERE PROCEDE='AG' AND PROCEDEID=" + cartera[i].codExternoAgrupacion);
                        }
                        else {
                            idCartera = obtenerNumCartera(ventas, cartera[i].externalFacturaId, numEfecto).ToString();
                        }

                        
                        if (idCartera == "0")
                            continue;


                        //Para que no se quede bloqueado, miro si me da error el pago
                        //si da error, sigo al siguiente
                        try
                        {
                            if (ventas)
                            {
                                if (!impagados)
                                {
                                    if (!cartera[i].esEfectoCobradoVISA)  //Si está pagado por VISA ya está cobrado en A3ERP por lo que sólo tengo que sincronizar
                                    {
                                        opera.CobrarNax(Convert.ToDecimal(idCartera), numEfecto, true, false, importe, cuentaContableBanco, cuentaContableRiesgo, fechaCobro, fechaCobro, codigoBanco, 1, "", 0, "", "", "", "");
                                    }
                                }
                                else
                                {
                                    //fechaCobro = "01/09/2019";
                                    opera.Devolver(Convert.ToDecimal(idCartera), numEfecto, cartera[i].generarMovimientosContables, fechaImpagado, fechaImpagado, importe, importeGastosDevolucion, "", "", "", "", "");

                                    if (importeGastosDevolucion > 0)
                                    {
                                        string cuentaContImputacion = sql.obtenerCampoTabla("SELECT CTAINGRESOS FROM DATOSCON");
                                        nuevoVencimientoDevolucion = obtenerNuevoVencimiento(idCartera.ToString(), numEfecto);
                                        //La imputación de gastos debe realizarse sobre el recibo devuelto (obtener número de vencimiento o efecto)
                                        opera.ImputarGastos(Convert.ToDecimal(idCartera), nuevoVencimientoDevolucion, cartera[i].generarMovimientosContables, fechaCobro, fechaCobro, importeGastosDevolucion, cuentaContImputacion, "", "", "", "");
                                    }

                                }
                                rpstWS.actualizarDocumentoRepasat("incomingpayments", "codExternoCarteraCobros", cartera[i].extNumDoc, idCartera);
                                numEfectosTratados++;
                            }
                            if (!ventas)
                            {
                                opera.PagarNax(Convert.ToDecimal(idCartera), numEfecto, true, importe, cuentaContableBanco, fechaCobro, fechaCobro, codigoBanco, 1, "", 0, "", "", "", "");
                                rpstWS.actualizarDocumentoRepasat("outgoingpayments", "codExternoCarteraCobros", cartera[i].extNumDoc, idCartera);
                                numEfectosTratados++;
                            }
                        }
                        catch (Exception exPaymen)
                        {
                            errorLogMessage = "Numero Documento:" + errorLog.errorNumeroDocumento.ToString() + "/" + errorLog.errorNumeroDocumento.ToString() + "\n" +
                                    "Cuenta:" + errorLog.errorNomClienteA3ERP.ToString() + "(" + errorLog.errorCodClienteA3ERP + ")" + "\n" +
                                    "Numero Vencimiento:" + errorLog.errorNumVencimiento.ToString() + "\n" +
                                    "Importe: " + importe.ToString();


                            if (csGlobal.modoManual)
                            {
                                (errorLogMessage + exPaymen.Message + "\n¿Continuar?").mb();
                            }
                            continue;
                        }
                    }
                }
                opera = null;
                a3.cerrarEnlace();
                return numEfectosTratados;
            }
            catch (Exception ex)
            {
                

                errorLogMessage = "Numero Documento:" + errorLog.errorNumeroDocumento.ToString() + "/" + errorLog.errorNumeroDocumento.ToString() + "\n" +
                                    "Cuenta:" + errorLog.errorNomClienteA3ERP.ToString() + "(" + errorLog.errorCodClienteA3ERP + ")" + "\n" +
                                    "Numero Vencimiento:" + errorLog.errorNumVencimiento.ToString() + "\n" +
                                    "Importe: " + errorLog.errorImporteCobrado.ToString();

                if (ex.Message == "No se encontró el efecto")
                {
                    errorLogMessage = errorLogMessage + "Revisar si se ha modificado el número de efecto";
                }

                if (csGlobal.modoManual)
                {
                    (errorLogMessage + ex.Message).mb();
                }
                csUtilidades.log(ex.Message);
                return numEfectosTratados;
             }


            }

        public int obtenerNuevoVencimiento(string numcartera, int numVencimiento)
        {
            csSqlConnects sql = new csSqlConnects();
            string nuevoVencimiento = sql.obtenerCampoTabla("select NUMVEN from CARTERA where PADREDEV=" + numcartera + " and PADREDEV2=" + numVencimiento.ToString());
            return Convert.ToInt16(nuevoVencimiento);
        }

        /// <summary>
        /// Funcion para anular el cobro de un efecto de cobro o de pago
        /// Se pasa como parámetros si es cobro o pago, y el número de cartera/efecto y el número de vencimiento
        /// </summary>
        /// <param name="ventas"></param>
        /// <param name="numCartera"></param>
        /// <param name="numVencimiento"></param>
        public void anularCobroPago(bool ventas, string numCartera, int numVencimiento)
        {
            try
            {
                a3ERPActiveX.OperacionesCartera opera = new a3ERPActiveX.OperacionesCartera();
                if (ventas)
                {
                    opera.AnularCobro(Convert.ToDecimal(numCartera), Convert.ToInt16(numVencimiento));
                }
                else
                {
                    opera.AnularPago(Convert.ToDecimal(numCartera), Convert.ToInt16(numVencimiento));
                }
                opera = null;
            }
            catch { }

        }

        public void anularDevolucion(bool ventas, string numCartera, int numVencimiento)
        {
            try
            {
                a3ERPActiveX.OperacionesCartera opera = new a3ERPActiveX.OperacionesCartera();
                try //Si hubiera gastos imputados, primero anulamos la imputación de gastos y luego anulamos la devolución
                {
                    opera.AnularImputacion(Convert.ToDecimal(numCartera), Convert.ToInt16(numVencimiento));
                }
                catch { }
                opera.AnularDevolucion(Convert.ToDecimal(numCartera), Convert.ToInt16(numVencimiento));
                opera = null;
            }
            catch { }

        }

        public void anularCobroRemesa(int numRemesa)
        {
            try
            {
                a3ERPActiveX.OperacionesCartera opera = new a3ERPActiveX.OperacionesCartera();
                opera.AnularCobroRemesa(Convert.ToDecimal(numRemesa));
                borrarRemesa(numRemesa);
            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }

        }

        public void borrarRemesa(int numRemesa, bool idRepasat=false)
        {
            try
            {
                DataTable efectosRemesa = new DataTable();
                decimal numeroCartera = 0;
                int numeroEfecto = 0;

                if (idRepasat)
                {
                    string numeroRemesa = sql.obtenerCampoTabla("SELECT  CONVERT(int, IDREMESA) FROM __REMESAS WHERE RPST_ID_REMESA = " + numRemesa);
                    numRemesa = Convert.ToInt32(numeroRemesa);
                }
                efectosRemesa= obtenerEfectosRemesa(numRemesa);
               
                


                a3ERPActiveX.Remesa carteraA3 = new a3ERPActiveX.Remesa();
                carteraA3.Iniciar();
                carteraA3.Modifica(numRemesa);

                foreach (DataRow dr in efectosRemesa.Rows)
                {
                    numeroCartera = Convert.ToDecimal(dr["NUMCARTERA"].ToString());
                    numeroEfecto = Convert.ToInt16(dr["NUMVEN"].ToString());
                    carteraA3.QuitarEfecto(numeroCartera, numeroEfecto);
                    carteraA3.Anade();
                }

                 //Con Anade finalizamos la edición de la Remesa y se guardan los cambios
                carteraA3.Acabar();
            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }
        }

        public DataTable obtenerEfectosRemesa(int numRemesa, bool idRepasat=false)
        {
            csSqlConnects sql = new csSqlConnects();
            DataTable dt = new DataTable();
            if (idRepasat)
            {
                dt = sql.obtenerDatosSQLScript("SELECT IDREMESA FROM __REMESAS WHERE RPST_ID_REMESA=" + numRemesa);
            }
            else
            {
                dt = sql.obtenerDatosSQLScript("SELECT CAST(NUMCARTERA AS INT) AS NUMCARTERA, NUMVEN FROM CARTERA WHERE COBPAG='C' and PADREREME=" + numRemesa);
            }

            return dt;


        }

        private string obtenerNumCartera(bool venta, string idFactura, int numVencimiento=0)
        {
            a3ERPActiveX.Cartera carteraV = new a3ERPActiveX.Cartera();
            csSqlConnects sql = new csSqlConnects();

            try
            {
                string numCartera = "";
                string numCarteraCheckDB = "";
                
                carteraV.Iniciar();
                numCartera = carteraV.ObtenerNumCartera(venta, Convert.ToDecimal(idFactura)).ToString();
                carteraV.Acabar();

                //Ejecuto esta comprobación únicamente cuando el número de vencimiento no es 1
                //Si por ejemplo es 2, posiblemente no se ha creado el segundo recibo, y hay que hacerlo en una segunda vez
                if (numVencimiento > 1)
                {
                    numCarteraCheckDB = sql.obtenerCampoTabla("SELECT CAST(NUMCARTERA as int) as NUMCARTERA FROM CARTERA WHERE PROCEDEID=" + idFactura + " and NUMVEN=" + numVencimiento.ToString());
                    numCartera = (numCartera == numCarteraCheckDB) ? numCartera : "0";
                }
                carteraV.Acabar();

                return numCartera;
            }
            catch
            {
                carteraV.Acabar();
                return "0";
            }
        }

        /// <summary>
        /// Esta función nos devuelve el NUMCARTERA de la agrupación en base al id de la agrupación que facilitemos
        /// </summary>
        /// <param name="venta"></param>
        /// <param name="idAgrupacion"></param>
        /// <param name="numVencimiento"></param>
        /// <returns></returns>
        private string obtenerNumAgrupacion(bool venta, string idAgrupacion, int numVencimiento = 0) {
            try
            {
                string numCartera = "";
                numCartera = sql.obtenerCampoTabla("select CAST(NUMCARTERA as int) as NUMAGRU from cartera where procede='AG' and PROCEDEID=" + idAgrupacion);
                return numCartera;
            }
            catch
            {
                return "0";
            }
        }

        public bool recibirEfecto(bool ventas, string idFactura, int numEfecto, string fecha)
        {
            try
            {
                string idCartera = "";
                idCartera = obtenerNumCartera(ventas, idFactura, numEfecto).ToString();
                if (idCartera == "0" || idCartera == "0,0000") return false;

                a3ERPActiveX.OperacionesCartera opera = new a3ERPActiveX.OperacionesCartera();
                opera.Recibir(Convert.ToDecimal(idCartera), numEfecto, false,fecha);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }



        }


        //Crear un efecto manualmente
        //cart.Iniciar();

        //cart.Nuevo("19/02/20", "6", "EUR", "1", "2019", "444", false);
        //cart.set.SetAsFloat("IMPORTE", "1000");

        public void crearEfectoManual(string serie, string numeroDoc, string cliente, string fecha,  string importeEfecto, bool ventas=true)
        {
            try
            {
                a3ERPActiveX.Cartera cart = new a3ERPActiveX.Cartera();

                decimal importe = Convert.ToDecimal(importeEfecto.Replace('.',','));
                cart.Iniciar();

                cart.Nuevo(fecha, cliente, "EURO", csGlobal.tipoContable.ToString(), serie.PadLeft(8, ' '), numeroDoc, false);
                cart.NuevoVen(fecha, importe);
                cart.AnadirVen();
                cart.Anade();
                cart.Acabar();
            }
            catch (Exception ex) {
                ex.Message.mb();
            }

        }



        //a3ERPActiveX.Cartera cart = new a3ERPActiveX.Cartera();
        //double importeEfecto = 772.05;
        //decimal importe = Convert.ToDecimal(importeEfecto);


        //opera.CobrarNax(38929, 2, true, false, importe, "57200001", "57200001", "19/02/2019", "19/02/2019", "       1", 1, "", 0, "", "", "", "");
        //cart.Iniciar();

        //cart.Nuevo("19/02/20", "6", "EUR", "1", "2019", "444", false);
        //cart.set.SetAsFloat("IMPORTE", "1000");
    }
}
