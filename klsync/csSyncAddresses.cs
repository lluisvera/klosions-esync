﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace klsync.esyncro
{
    class csSyncAddresses
    {
        csPSWebService psWebService = new csPSWebService();

        private DataTable cargarDireccionesA3ERP(string codCliA3 = "", string query = "")
        {
            DataTable direccionesA3ERP = new DataTable();
            string anexoQuery = "";
            string anexoKlsFiscal = "";
            if (codCliA3 != "")
            {
                anexoQuery = "WHERE LTRIM(dbo.CLIENTES.KLS_CODCLIENTE)='" + codCliA3.Trim() + "'";
            }
            if (codCliA3 != "")
            {
                //query = "";
            }
            //190425-Añadido el campo de KLS_FIscal a la consulta que recoge las direcciones de Andreutoys.

            if (csGlobal.nombreServidor.Contains("ANDREUTOYS")) {
                anexoKlsFiscal = ", dbo.DIRENT.KLS_FISCAL ";
            }
            
            string queryA3 = " SELECT " +
                                " dbo.PROVINCI.KLS_IDPS,dbo.PAISES.KLS_CODPAIS_PS,dbo.DIRENT.CODCLI, dbo.DIRENT.IDDIRENT, dbo.DIRENT.ID_PS, dbo.DIRENT.CODPAIS, dbo.DIRENT.CODPROVI, dbo.DIRENT.DIRENT1, dbo.DIRENT.DIRENT2,  " +
                                " dbo.DIRENT.DTOENT, dbo.DIRENT.NOMENT, dbo.DIRENT.POBENT, dbo.DIRENT.KLS_CREATEDATE, dbo.DIRENT.KLS_LASTUPDATE, dbo.DIRENT.TELENT1,dbo.CLIENTES.NIFCLI, " +
                                " dbo.DIRENT.KLS_ESYNCDATE, dbo.CLIENTES.KLS_CODCLIENTE,  dbo.CLIENTES.NOMCLI,  dbo.CLIENTES.KLS_APELL_USUARIO, dbo.CLIENTES.KLS_NOM_USUARIO " + anexoKlsFiscal +
                                " FROM " +
                                " dbo.DIRENT " +
                                " LEFT JOIN dbo.PROVINCI ON PROVINCI.CODPROVI = DIRENT.CODPROVI " +
                                " LEFT JOIN dbo.PAISES ON PAISES.CODPAIS = DIRENT.CODPAIS " +
                                " INNER JOIN dbo.CLIENTES ON dbo.DIRENT.CODCLI = dbo.CLIENTES.CODCLI " + anexoQuery + query;

            SqlConnection dataConnection = new SqlConnection();
            csSqlConnects sqlConnect = new csSqlConnects();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;
            dataConnection.Open();
            SqlDataAdapter sqlAdaptA3 = new SqlDataAdapter(queryA3, dataConnection);
            sqlAdaptA3.Fill(direccionesA3ERP);
            dataConnection.Close();

            return direccionesA3ERP;
        }

        private DataTable cargarDireccionesPS(string idCliente = "")
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            DataTable direccionPS = new DataTable();
            string anexoQuery = "";
            if (idCliente != "")
            {
                anexoQuery = "where id_customer = " + idCliente;
            }
            string queryPS = "select id_customer, id_address , ps_address.id_country,ps_address.id_state, company, lastname, firstname, address1, address2, postcode, city, date_add,date_upd, " +
                            " ps_country.iso_code as country_isocode ,ps_state.iso_code as state_isocode, alias " +
                            " from ps_address  " +
                            " left join ps_country on ps_country.id_country=ps_address.id_country   " +
                            " left join ps_state on ps_state.id_state=ps_address.id_state " + anexoQuery;

            direccionPS = mySqlConnect.obtenerDatosPS(queryPS);

            return direccionPS;
        }

        //ESTA FUNCIÓN VERIFICA SI UN CLIENTE QUE ESTÁ EN LA TIENDA, TIENE DIRECCIONES NO SUBIDAS EN PRESTASHOP
        // SI DETECTA QUE TIENE DIRECCIONES NO SUBIDAS EN LA TIENDA, LAS CREA EN PRESTASHOSP
        public void crearDireccionesA3ToPS(string codCli = "", string idDireccion=null)
        {
            try
            {
                DataTable DireccionesToPS = new DataTable();
                string filtroDirecciones = " AND (dbo.CLIENTES.KLS_CODCLIENTE > 0)";
                filtroDirecciones = string.IsNullOrEmpty(idDireccion) ? filtroDirecciones : filtroDirecciones + " AND IDDIRENT=" + idDireccion;
                DireccionesToPS = cargarDireccionesA3ERP(codCli,filtroDirecciones);
                

                foreach (DataRow fila in DireccionesToPS.Rows)
                {
                    if (fila["ID_PS"].ToString() == string.Empty || fila["ID_PS"].ToString() == "0")
                    {
                        //CREO LA DIRECCIÓN EN PS
                        psWebService.cdPSWebService("POST", "addresses", "", fila["KLS_CODCLIENTE"].ToString().Trim(), true, "", "F", fila, 0);
                    }
                    else {
                        //TODO:Mirar porque sale siempre este mensaje.

                       // MessageBox.Show("Alguna dirección se encuentra en PS, debe ponerlas a 'Null' \n Botón derecho 'Poner a null Id dirección PS' ");

                    }
                }


            }
            catch (Exception e)
            { Program.guardarErrorFichero(e.StackTrace); }
        }

        public bool actualizarDirección(string idCliente, string idDireccion)
        {
            bool actualizar = false;
            DataTable direccionA3ERP = new DataTable();
            DataTable direccionPS = new DataTable();
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            try
            {
                crearDireccionesA3ToPS();

                int i = 0;

                //PS -->>> A3ERP
                //VERIFICO SI EXISTE EL CLIENTE EN A3ERP
                //VERIFICO SI EXISTE LA DIRECCIÓN EN A3ERP
                //NO EXISTE LA DIRECCIÓN EN A3ERP
                //CREO LA DIRECCIÓN EN A3ERP

                //SI EXISTE LA DIRECCIÓN EN PS (ID_PS>0)
                //VERIFICO date_upd con FECHA ESYNCDATE
                //SI date_upd > FECHA ESYNCDATE -->> ACTUALIZO LA DIRECCIÓN
                //SI date_upd = FECHA ESYNCDATE -->> NO HAGO NADA
                //SI date_upd < FECHA ESYNCDATE -->> NO SE CONTEMPLA ESTA OPCIÓN 


                //A3ERP -->> PS
                //VERIFICO SI EXISTE EL CLIENTE EN PS O ESTÁ MARCADO COMO VISIBLE EN TIENDA
                //SI EXISTE EL CLIENTE VERIFICO SI LA DIRECCIÓN ESTÁ SUBIDA
                //DIRECCIÓN NO SUBIDA -->> SUBO LA DIRECCIÓN
                //ACTUALIZO EN A3 FECHA LAST ESYNC CON FECHA date_upd e informo ID_PS
                //DIRECCIÓN SI SUBIDA -->> 
                //COMPARO date_upd con FECHA_LAST_ESYNC
                //SI date_upd > FECHA ESYNCDATE -->> ACTUALIZO LA DIRECCIÓN
                //SI date_upd = FECHA ESYNCDATE -->> NO HAGO NADA
                //SI LAST_UPDATE > ESYNCDATE  -->> ACTUALIZO DIRECCIÓN EN PS 




                //SI ID_PS ="" CREO LA DIRECCION EN PRESTASHOP


                //SI ID_PS>0

                //COMPARO FECHAS






            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }








            return actualizar;
        }

        public void actualizarInfoDirecciones(string idCliente)
        {
            DataTable direccionesA3 = new DataTable();
            direccionesA3 = cargarDireccionesA3ERP(idCliente);
            DataTable direccionesPS = new DataTable();
            direccionesPS = cargarDireccionesPS(idCliente);
            csSqlConnects sql = new csSqlConnects();

            DateTime fechaSyncronizacion = new DateTime(1900, 1, 1);
            DateTime fechaActualizacionPS = new DateTime(1900, 1, 1);
            DateTime fechaActualizacionA3 = new DateTime(1900, 1, 1);
            string idDireccionA3 = "";
            string idDireccionPS = "";

            if (direccionesA3.Rows.Count > 0 && direccionesPS.Rows.Count > 0)
            {
                foreach (DataRow filaA3 in direccionesA3.Rows)
                {
                    if (filaA3["KLS_ESYNCDATE"].ToString() == "")
                    {
                        //fechaSyncronizacion = DateTime.Now;
                    }
                    else
                    {
                        fechaSyncronizacion = Convert.ToDateTime(filaA3["KLS_ESYNCDATE"].ToString());
                    }
                    if (filaA3["KLS_LASTUPDATE"].ToString() == "")
                    {
                        //fechaActualizacionA3 = DateTime.Now;
                    }
                    else
                    {
                        fechaActualizacionA3 = Convert.ToDateTime(filaA3["KLS_LASTUPDATE"].ToString());
                    }


                    idDireccionA3 = filaA3["ID_PS"].ToString();

                    csMySqlConnect mySqlConnect = new csMySqlConnect();

                    foreach (DataRow filaPS in direccionesPS.Rows)
                    {
                        fechaActualizacionPS = Convert.ToDateTime(filaPS["date_upd"].ToString());
                        idDireccionPS = filaPS["id_address"].ToString();

                        if (idDireccionPS == idDireccionA3)
                        {
                            if (fechaSyncronizacion > fechaActualizacionPS)
                            {
                                //NO DEBERÍA ENTRAR AQUÍ NUNCA, SIEMPRE TIENEN QUE SER IGUALES;
                            }

                            //Si las fechas son iguales, quiere decir que entre Prestashop no se ha modificado
                            //Procedemos a ver si se ha realizado algún cambio en A3
                            if (fechaSyncronizacion == fechaActualizacionPS)
                            {
                                //añadimos un control de tiempo, porque entra la operación pueden pasar varios segundos
                                int lapso = Convert.ToInt32(fechaActualizacionA3.Subtract(fechaSyncronizacion).TotalMinutes);

                                // Si se actualiza en A3
                                if ((fechaSyncronizacion < fechaActualizacionA3) && (lapso > 0))
                                {
                                    //actualizo en PS
                                    string codPais = filaA3["KLS_CODPAIS_PS"].ToString();
                                    string codProvi = filaA3["KLS_IDPS"].ToString();
                                    string dirent1 = filaA3["DIRENT1"].ToString();
                                    string dirent2 = filaA3["DIRENT2"].ToString();
                                    string dtoent = filaA3["DTOENT"].ToString();
                                    string noment = filaA3["NOMENT"].ToString();
                                    string pobent = filaA3["POBENT"].ToString();
                                    string telefono = filaA3["TELENT1"].ToString();
                                    string nombre = filaA3["KLS_NOM_USUARIO"].ToString();
                                    string apellido = filaA3["KLS_APELL_USUARIO"].ToString();

                                    if (codProvi == "")
                                    {
                                        codProvi = csUtilidades.obtenerProvinciaEnBaseAlCodigoPostal(dtoent).Trim();
                                    }

                                    //string esyncDate = fechaActualizacionA3.ToString("dd'-'MM'-'yyyy HH:mm:ss");

                                    //Actualizamos datos en PS
                                    string queryUpdate = "update ps_address set " +
                                        " id_country='" + codPais + "', " +
                                        " id_state='" + codProvi + "'," +
                                        " company='" + noment + "'," +
                                        " firstname='" + nombre + "'," +
                                        " lastname='" + apellido + "'," +
                                        " address1='" + dirent1 + "', " +
                                        " address2='" + dirent2 + "', " +
                                        " postcode='" + dtoent + "', " +
                                        " alias='" + noment + "', " +
                                        " date_upd='" + fechaActualizacionA3.ToString("yyyy'-'MM'-'dd HH:mm:ss") + "', " +
                                        " city='" + pobent + "', " +
                                        " phone='" + telefono + "' " +
                                        " where id_address=" + idDireccionPS;

                                    mySqlConnect.actualizarGenerica(queryUpdate);
                                    string fecha = mySqlConnect.obtenerDatoFromQuery("select date_upd from ps_address where id_address=" + idDireccionPS);
                                    fechaSyncronizacion = Convert.ToDateTime(fecha);

                                    sql.actualizarCampo("UPDATE DIRENT SET KLS_ESYNCDATE='" + fechaSyncronizacion + "' WHERE ID_PS=" + idDireccionPS);

                                    //y sincronizo fechaesync en A3
                                }
                            }

                            // Si se actualiza en PS
                            if (fechaSyncronizacion < fechaActualizacionPS)
                            {
                                if (fechaActualizacionA3 > fechaActualizacionPS)
                                {
                                    //actualizo en PS
                                    string codPais = filaA3["KLS_CODPAIS_PS"].ToString();
                                    string codProvi = filaA3["KLS_IDPS"].ToString();
                                    string dirent1 = filaA3["DIRENT1"].ToString();
                                    string dirent2 = filaA3["DIRENT2"].ToString();
                                    string dtoent = filaA3["DTOENT"].ToString();
                                    string noment = filaA3["NOMENT"].ToString();
                                    string pobent = filaA3["POBENT"].ToString();
                                    string telefono = filaA3["TELENT1"].ToString();
                                    string nombre = filaA3["KLS_NOM_USUARIO"].ToString();
                                    string apellido = filaA3["KLS_APELL_USUARIO"].ToString();

                                    if (codProvi == "")
                                    {
                                        codProvi = csUtilidades.obtenerProvinciaEnBaseAlCodigoPostal(dtoent).Trim();
                                    }

                                    //string esyncDate = fechaActualizacionA3.ToString("dd'-'MM'-'yyyy HH:mm:ss");

                                    //Actualizamos datos en PS
                                    string queryUpdate = "update ps_address set " +
                                        " id_country='" + codPais + "', " +
                                        " id_state='" + codProvi + "'," +
                                        " company='" + noment + "'," +
                                        " firstname='" + nombre.ToUpper() + "'," +
                                        " lastname='" + apellido.ToUpper() + "'," +
                                        " address1='" + dirent1 + "', " +
                                        " address2='" + dirent2 + "', " +
                                        " postcode='" + dtoent + "', " +
                                        " alias='" + noment + "', " +
                                        " date_upd='" + fechaActualizacionA3.ToString("yyyy'-'MM'-'dd HH:mm:ss") + "', " +
                                        " city='" + pobent + "', " +
                                        " phone='" + telefono + "' " +
                                        " where id_address=" + idDireccionPS;

                                    mySqlConnect.actualizarGenerica(queryUpdate);
                                    string fecha = mySqlConnect.obtenerDatoFromQuery("select date_upd from ps_address where id_address=" + idDireccionPS);
                                    fechaSyncronizacion = Convert.ToDateTime(fecha);

                                    sql.actualizarCampo("UPDATE DIRENT SET KLS_ESYNCDATE='" + fechaSyncronizacion + "' WHERE ID_PS=" + idDireccionPS);
                                    //y sincronizo fechaesync en A3

                                }
                                else
                                {
                                    //actualizo en A3
                                    string codPais = filaPS["country_isocode"].ToString();
                                    string codProvi = filaPS["state_isocode"].ToString();
                                    //codProvi = "  08";
                                    codProvi = sql.obtenerCampoTabla("SELECT CODPROVI FROM PROVINCI WHERE KLS_IDPS = '" + filaPS["id_state"].ToString() + "'");
                                    if (codProvi != "")
                                    {
                                        string dirent1 = filaPS["address1"].ToString();
                                        string dirent2 = filaPS["address2"].ToString();
                                        string dtoent = filaPS["postcode"].ToString();
                                        //string noment = filaPS["alias"].ToString();
                                        string noment = filaPS["firstname"].ToString().ToUpper() + " " + filaPS["lastname"].ToString().ToUpper();
                                        string pobent = filaPS["city"].ToString();
                                        string esyncDate = fechaActualizacionPS.ToString("dd'-'MM'-'yyyy HH:mm:ss");
                                        string telf = mySqlConnect.obtenerDatoFromQuery("select phone from ps_address where id_address = " + idDireccionPS);

                                        string queryUpdate = "UPDATE DIRENT SET " +
                                            " CODPAIS='" + codPais + "', " +
                                            " CODPROVI='" + codProvi + "'," +
                                            " DIRENT1='" + dirent1 + "', " +
                                            " DIRENT2='" + dirent2 + "', " +
                                            " DTOENT='" + dtoent + "', " +
                                            " NOMENT='" + noment + "', " +
                                            " POBENT='" + pobent + "', " +
                                            " TELENT1 ='" + telf + "', " +
                                            " KLS_ESYNCDATE='" + esyncDate + "' " +
                                            " WHERE ID_PS=" + idDireccionPS;

                                        sql.actualizarCampo(queryUpdate);
                                    }
                                }

                            }



                        }


                    }


                }
                int i = 0;

            }
        }

        public bool crearDireccionA3(string id_ps, Objetos.csTercero cliente, string codCli)
        {
            bool existe = true;
            try
            {
                
                csSqlConnects sqlconnect = new csSqlConnects();
                if (!sqlconnect.consultaExiste("SELECT ID_PS FROM DIRENT WHERE ID_PS= " + id_ps))
                {
                    cliente.external_id = id_ps;
                    cliente.codIC = string.IsNullOrEmpty(codCli) ? cliente.codIC : codCli;
                    csa3erpDirecciones a3Dir = new csa3erpDirecciones();
                    a3Dir.crearDireccion(cliente);
                    existe = false;
                }
                return existe;
            }
            catch
            {
                return existe;
            }
        }

    }
}
