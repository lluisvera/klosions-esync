﻿namespace klsync
{
    partial class frImagenesTyC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tools = new System.Windows.Forms.SplitContainer();
            this.splitContainerIzq = new System.Windows.Forms.SplitContainer();
            this.splitContainerDerecha = new System.Windows.Forms.SplitContainer();
            this.toolStripGeneral = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCargarProductos = new System.Windows.Forms.ToolStripButton();
            this.listViewProducts = new System.Windows.Forms.ListView();
            this.imageListProduct = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
            this.dataGridViewAttribute = new System.Windows.Forms.DataGridView();
            this.listViewAttribute = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.tools)).BeginInit();
            this.tools.Panel1.SuspendLayout();
            this.tools.Panel2.SuspendLayout();
            this.tools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerIzq)).BeginInit();
            this.splitContainerIzq.Panel1.SuspendLayout();
            this.splitContainerIzq.Panel2.SuspendLayout();
            this.splitContainerIzq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDerecha)).BeginInit();
            this.splitContainerDerecha.Panel1.SuspendLayout();
            this.splitContainerDerecha.Panel2.SuspendLayout();
            this.splitContainerDerecha.SuspendLayout();
            this.toolStripGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAttribute)).BeginInit();
            this.SuspendLayout();
            // 
            // tools
            // 
            this.tools.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tools.Location = new System.Drawing.Point(0, 42);
            this.tools.Name = "tools";
            // 
            // tools.Panel1
            // 
            this.tools.Panel1.Controls.Add(this.splitContainerIzq);
            // 
            // tools.Panel2
            // 
            this.tools.Panel2.Controls.Add(this.splitContainerDerecha);
            this.tools.Size = new System.Drawing.Size(886, 506);
            this.tools.SplitterDistance = 440;
            this.tools.TabIndex = 0;
            // 
            // splitContainerIzq
            // 
            this.splitContainerIzq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerIzq.Location = new System.Drawing.Point(0, 0);
            this.splitContainerIzq.Name = "splitContainerIzq";
            this.splitContainerIzq.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerIzq.Panel1
            // 
            this.splitContainerIzq.Panel1.Controls.Add(this.dataGridViewProducts);
            // 
            // splitContainerIzq.Panel2
            // 
            this.splitContainerIzq.Panel2.Controls.Add(this.dataGridViewAttribute);
            this.splitContainerIzq.Size = new System.Drawing.Size(440, 506);
            this.splitContainerIzq.SplitterDistance = 250;
            this.splitContainerIzq.TabIndex = 0;
            // 
            // splitContainerDerecha
            // 
            this.splitContainerDerecha.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerDerecha.Location = new System.Drawing.Point(0, 0);
            this.splitContainerDerecha.Name = "splitContainerDerecha";
            this.splitContainerDerecha.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerDerecha.Panel1
            // 
            this.splitContainerDerecha.Panel1.Controls.Add(this.listViewProducts);
            // 
            // splitContainerDerecha.Panel2
            // 
            this.splitContainerDerecha.Panel2.Controls.Add(this.listViewAttribute);
            this.splitContainerDerecha.Size = new System.Drawing.Size(442, 506);
            this.splitContainerDerecha.SplitterDistance = 250;
            this.splitContainerDerecha.TabIndex = 1;
            // 
            // toolStripGeneral
            // 
            this.toolStripGeneral.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCargarProductos});
            this.toolStripGeneral.Location = new System.Drawing.Point(0, 0);
            this.toolStripGeneral.Name = "toolStripGeneral";
            this.toolStripGeneral.Size = new System.Drawing.Size(898, 39);
            this.toolStripGeneral.TabIndex = 1;
            this.toolStripGeneral.Text = "toolStrip1";
            // 
            // toolStripButtonCargarProductos
            // 
            this.toolStripButtonCargarProductos.Image = global::klsync.Properties.Resources.refresh_button;
            this.toolStripButtonCargarProductos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonCargarProductos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCargarProductos.Name = "toolStripButtonCargarProductos";
            this.toolStripButtonCargarProductos.Size = new System.Drawing.Size(135, 36);
            this.toolStripButtonCargarProductos.Text = "Cargar productos";
            this.toolStripButtonCargarProductos.Click += new System.EventHandler(this.toolStripButtonCargarProductos_Click);
            // 
            // listViewProducts
            // 
            this.listViewProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewProducts.Location = new System.Drawing.Point(0, 0);
            this.listViewProducts.Name = "listViewProducts";
            this.listViewProducts.Size = new System.Drawing.Size(442, 250);
            this.listViewProducts.TabIndex = 0;
            this.listViewProducts.UseCompatibleStateImageBehavior = false;
            // 
            // imageListProduct
            // 
            this.imageListProduct.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListProduct.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListProduct.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // dataGridViewProducts
            // 
            this.dataGridViewProducts.AllowUserToAddRows = false;
            this.dataGridViewProducts.AllowUserToDeleteRows = false;
            this.dataGridViewProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProducts.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewProducts.Name = "dataGridViewProducts";
            this.dataGridViewProducts.ReadOnly = true;
            this.dataGridViewProducts.Size = new System.Drawing.Size(434, 244);
            this.dataGridViewProducts.TabIndex = 0;
            // 
            // dataGridViewAttribute
            // 
            this.dataGridViewAttribute.AllowUserToAddRows = false;
            this.dataGridViewAttribute.AllowUserToDeleteRows = false;
            this.dataGridViewAttribute.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewAttribute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAttribute.Location = new System.Drawing.Point(3, 4);
            this.dataGridViewAttribute.Name = "dataGridViewAttribute";
            this.dataGridViewAttribute.ReadOnly = true;
            this.dataGridViewAttribute.Size = new System.Drawing.Size(434, 244);
            this.dataGridViewAttribute.TabIndex = 1;
            // 
            // listViewAttribute
            // 
            this.listViewAttribute.Location = new System.Drawing.Point(3, 4);
            this.listViewAttribute.Name = "listViewAttribute";
            this.listViewAttribute.Size = new System.Drawing.Size(436, 244);
            this.listViewAttribute.TabIndex = 0;
            this.listViewAttribute.UseCompatibleStateImageBehavior = false;
            // 
            // frImagenesTyC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 560);
            this.Controls.Add(this.toolStripGeneral);
            this.Controls.Add(this.tools);
            this.Name = "frImagenesTyC";
            this.Text = "Gestor de imágenes";
            this.tools.Panel1.ResumeLayout(false);
            this.tools.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tools)).EndInit();
            this.tools.ResumeLayout(false);
            this.splitContainerIzq.Panel1.ResumeLayout(false);
            this.splitContainerIzq.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerIzq)).EndInit();
            this.splitContainerIzq.ResumeLayout(false);
            this.splitContainerDerecha.Panel1.ResumeLayout(false);
            this.splitContainerDerecha.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDerecha)).EndInit();
            this.splitContainerDerecha.ResumeLayout(false);
            this.toolStripGeneral.ResumeLayout(false);
            this.toolStripGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAttribute)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer tools;
        private System.Windows.Forms.SplitContainer splitContainerIzq;
        private System.Windows.Forms.SplitContainer splitContainerDerecha;
        private System.Windows.Forms.ToolStrip toolStripGeneral;
        private System.Windows.Forms.ToolStripButton toolStripButtonCargarProductos;
        private System.Windows.Forms.ListView listViewProducts;
        private System.Windows.Forms.ImageList imageListProduct;
        private System.Windows.Forms.DataGridView dataGridViewProducts;
        private System.Windows.Forms.DataGridView dataGridViewAttribute;
        private System.Windows.Forms.ListView listViewAttribute;

    }
}