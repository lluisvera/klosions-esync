﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;

namespace klsync.esyncro
{
    class csDatatimeSyncro
    {
        public DataTable obtenerDatosSyncronizacion(XmlDocument xdocInsertado, string objeto)
        {
            //DataTable prueba = csUtilidades.XmlDocument2DataTable(xdocInsertado);
            DataTable dt = new DataTable();
            string idCli = "";
            DataTable ItemsToUpdate = new DataTable();
            DataRow FilaToUpdate = ItemsToUpdate.NewRow();

            ItemsToUpdate.Columns.Add("COD_A3");
            ItemsToUpdate.Columns.Add("COD_PS");
            ItemsToUpdate.Columns.Add("FECHA_SYNC");

            csSqlConnects sqlConnect = new csSqlConnects();
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            xdoc = xdocInsertado;
            //textBox2.Text = xdoc.ToString();

            XmlNodeList listaNodos = null;

            if (objeto == "products")
            {
                listaNodos = xdoc.GetElementsByTagName("product");
            }
            else if (objeto == "customers")
            {
                listaNodos = xdoc.GetElementsByTagName("customer");
            }
            else if (objeto == "categories")
            {
                listaNodos = xdoc.GetElementsByTagName("category");
            }

            else if (objeto == "addresses")
            {
                DateTime fechaSyncro = new DateTime(1900, 1, 1);
                listaNodos = xdoc.GetElementsByTagName("address");

                foreach (XmlNode elemento in listaNodos)
                {
                    foreach (XmlNode subNodo in elemento.ChildNodes)
                    {
                        if (subNodo.Name == "kls_id_dirent")
                        {
                            FilaToUpdate["COD_PS"] = subNodo.InnerText.ToString();
                        }
                        if (subNodo.Name == "id")
                        {
                            FilaToUpdate["COD_A3"] = subNodo.InnerText.ToString();
                        }
                        if (subNodo.Name == "date_upd")
                        {
                            fechaSyncro = Convert.ToDateTime(subNodo.InnerText.ToString());
                            FilaToUpdate["FECHA_SYNC"] = fechaSyncro.ToString("dd'-'MM'-'yyyy HH:mm:ss");
                        }
                    }
                }
            }
            if (objeto != "addresses")
            {
                foreach (XmlNode elemento in listaNodos)
                {
                    foreach (XmlNode subNodo in elemento.ChildNodes)
                    {
                        if (subNodo.Name == "id")
                        {
                            ////MessageBox.Show("Id insertado " + subNodo.Name.ToString() + " - " + subNodo.InnerText.ToString());
                            FilaToUpdate["COD_A3"] = subNodo.InnerText.ToString(); ;
                        }
                        if (subNodo.Name == "kls_a3erp_id")
                        {
                            ////MessageBox.Show("Id insertado " + subNodo.Name.ToString() + " - " + subNodo.InnerText.ToString());
                            FilaToUpdate["COD_PS"] = subNodo.InnerText.ToString();
                        }
                        if (subNodo.Name == "reference")
                        {
                            ////MessageBox.Show("Id insertado " + subNodo.Name.ToString() + " - " + subNodo.InnerText.ToString());
                            FilaToUpdate["COD_PS"] = subNodo.InnerText.ToString();
                        }
                    }
                }
            }

            ItemsToUpdate.Rows.Add(FilaToUpdate);
            
            return ItemsToUpdate;
        }

    }
}
