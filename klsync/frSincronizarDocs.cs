﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using System.Net;

namespace klsync
{
    public partial class frSincronizarDocs : Form
    {
        private static frSincronizarDocs m_FormDefInstance;
        public static frSincronizarDocs DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSincronizarDocs();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        //Defino los elementos que añadiré en el ToolStrip
        private ToolStripControlHost dtTSlblOrigenData;
        private ToolStripControlHost dtTScBoxOrigenData;
        private ToolStripControlHost dtTSdtpDesde;
        private ToolStripControlHost dtTSdtpHasta;
        private ToolStripControlHost dtTSlblDesde;
        private ToolStripControlHost dtTSlblHasta;
        private ToolStripControlHost dtTSchboxDetalles;

        CheckBox cboxDetail = new CheckBox();
        DateTimePicker dtpDesde = new DateTimePicker();
        DateTimePicker dtpHasta = new DateTimePicker();
        ComboBox cboxOrigenData = new ComboBox();
        ToolStripButton btSyncA3ERP = new ToolStripButton();

        public frSincronizarDocs()
        {
            InitializeComponent();

            // Inicializo los componentes del ToolStrip
            Label lblOrigenData = new Label();
            lblOrigenData.Text = "Datos Origen:";
            lblOrigenData.TextAlign = ContentAlignment.MiddleCenter;
            lblOrigenData.BackColor = Color.Transparent;

            cboxOrigenData.Items.Add("Pendientes");
            cboxOrigenData.Items.Add("Facturas");
            cboxOrigenData.Items.Add("Todos");
            //cboxOrigenData.Items.Add("StandBy");
            cboxOrigenData.SelectedIndex = 0;
            cboxOrigenData.SelectedIndexChanged += new EventHandler(cboxOrigenData_SelectedIndexChanged);

            Label lblDesde = new Label();
            lblDesde.Text = "Desde:";
            lblDesde.TextAlign = ContentAlignment.MiddleCenter;
            lblDesde.BackColor = Color.Transparent;
            lblDesde.Padding = new Padding(10);

            dtpDesde.Format = DateTimePickerFormat.Short;
            dtpDesde.Size = new System.Drawing.Size(120, 20);

            Label lblHasta = new Label();
            lblHasta.Text = "Hasta:";
            lblHasta.TextAlign = ContentAlignment.MiddleCenter;
            lblHasta.BackColor = Color.Transparent;
            lblHasta.Padding = new Padding(20);

            dtpHasta.Format = DateTimePickerFormat.Short;
            dtpHasta.Size = new System.Drawing.Size(120, 20);

            //CheckBox cboxDetail = new CheckBox();
            cboxDetail.Text = "Detalle";
            cboxDetail.Checked = true;
            cboxDetail.Padding = new System.Windows.Forms.Padding(20);
            cboxDetail.BackColor = Color.Transparent;

            ToolStripButton btLoad = new ToolStripButton();
            btLoad.Image = klsync.Properties.Resources.loadData;
            btLoad.Text = "Cargar Datos";
            btLoad.TextImageRelation = TextImageRelation.ImageAboveText;
            btLoad.BackColor = Color.Transparent;
            btLoad.ImageScaling = ToolStripItemImageScaling.None;
            btLoad.Click += new EventHandler(btLoadClickHandler); // !

            ToolStripButton btUnSync = new ToolStripButton();
            btUnSync.Image = klsync.Properties.Resources.undoSyncA3ERP;
            btUnSync.Text = "Borrar Traspaso";
            btUnSync.TextImageRelation = TextImageRelation.ImageAboveText;
            btUnSync.BackColor = Color.Transparent;
            btUnSync.ImageScaling = ToolStripItemImageScaling.None;
            btUnSync.Click += new EventHandler(btUnSyncClickHandler);


            btSyncA3ERP.Image = klsync.Properties.Resources.syncA3ERP;
            btSyncA3ERP.Text = "Traspasar a A3ERP";
            btSyncA3ERP.TextImageRelation = TextImageRelation.ImageAboveText;
            btSyncA3ERP.BackColor = Color.Transparent;
            btSyncA3ERP.ImageScaling = ToolStripItemImageScaling.None;
            btSyncA3ERP.Click += new EventHandler(btSyncA3ERPClickHandler); // !

            ToolStripSeparator Separator1 = new ToolStripSeparator();
            ToolStripSeparator Separator2 = new ToolStripSeparator();
            ToolStripSeparator Separator3 = new ToolStripSeparator();
            ToolStripSeparator Separator4 = new ToolStripSeparator();
            ToolStripSeparator Separator5 = new ToolStripSeparator();
            ToolStripSeparator Separator6 = new ToolStripSeparator();
            ToolStripSeparator Separator7 = new ToolStripSeparator();

            dtTSlblOrigenData = new ToolStripControlHost(lblOrigenData);
            dtTScBoxOrigenData = new ToolStripControlHost(cboxOrigenData);

            dtTSlblDesde = new ToolStripControlHost(lblDesde);
            dtTSdtpDesde = new ToolStripControlHost(dtpDesde);

            dtTSlblHasta = new ToolStripControlHost(lblHasta);
            dtTSdtpHasta = new ToolStripControlHost(dtpHasta);

            dtTSchboxDetalles = new ToolStripControlHost(cboxDetail);

            toolStripSyncDocs.Items.Add(dtTSlblOrigenData);
            toolStripSyncDocs.Items.Add(dtTScBoxOrigenData);
            toolStripSyncDocs.Items.Add(Separator1);
            toolStripSyncDocs.Items.Add(dtTSlblDesde);
            toolStripSyncDocs.Items.Add(dtTSdtpDesde);
            toolStripSyncDocs.Items.Add(Separator2);
            toolStripSyncDocs.Items.Add(dtTSlblHasta);
            toolStripSyncDocs.Items.Add(dtTSdtpHasta);
            toolStripSyncDocs.Items.Add(Separator3);
            toolStripSyncDocs.Items.Add(dtTSchboxDetalles);
            toolStripSyncDocs.Items.Add(Separator4);
            toolStripSyncDocs.Items.Add(btLoad);
            toolStripSyncDocs.Items.Add(Separator5);
            toolStripSyncDocs.Items.Add(btUnSync);
            toolStripSyncDocs.Items.Add(Separator6);
            toolStripSyncDocs.Items.Add(btSyncA3ERP);
            toolStripSyncDocs.Items.Add(Separator7);

        }

        private void btLoadClickHandler(object sender, EventArgs e)
        {
            cargarDocumentos();
        }

        private void btUnSyncClickHandler(object sender, EventArgs e)
        {
            borrarSincronización();
        }

        private void btSyncA3ERPClickHandler(object sender, EventArgs e)
        {
            if (dgvDocsPs.Rows.Count > 0)
            {
                progressBar.Style = ProgressBarStyle.Marquee;
                progressBar.MarqueeAnimationSpeed = 50;

                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += bw_TraspasarDocs;
                bw.RunWorkerCompleted += bw_TraspasarDocsCompleted;
                bw.RunWorkerAsync();
            }            
        }

        void bw_TraspasarDocsCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar.MarqueeAnimationSpeed = 0;
            progressBar.Style = ProgressBarStyle.Blocks;
            progressBar.Value = progressBar.Minimum;
        }

        void bw_TraspasarDocs(object sender, DoWorkEventArgs e)
        {
            TraspasarDocsA3ERP(); // !
        }

        private void cboxOrigenData_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboxOrigenData.SelectedIndex.ToString())
            {
                case "0": //PENDIENTES
                    cboxDetail.Checked = true;
                    break;
                case "1": //FACTURAS
                    cboxDetail.Checked = true;
                    break;
                case "2": //TODOS
                    if (!csGlobal.modeDebug)
                    {
                        cboxDetail.Checked = false;
                    }
                    break;
            }
        }

        public DataTable A3CabeceraDocs(bool dgv = true)
        {
            DataTable CabeceraDocs = new DataTable();
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScripts sqlScripts = new csSqlScripts();
            csSqlConnects sql = new csSqlConnects();
            bool pendientes = false;
            bool porFechaPedido = false;
            bool soloFras = false;
            string formaDePago = "";
            string strFechaDesde = "";
            string strFechaHasta = "";
            try
            {                
                // si tiene datagridview, se tienen en cuenta los controles
                if (dgv)
                {
                    DateTime FechaDesde = dtpDesde.Value;
                    DateTime FechaHasta = dtpHasta.Value;

                    strFechaDesde = FechaDesde.ToString("yyyy/MM/dd");// Al cambiar de Servidor puede dar problemas con el formato de fecha  dd/MM/yyyy o yyyy/MM/dd 
                    strFechaHasta = FechaHasta.ToString("yyyy/MM/dd 23:59:59");// Al cambiar de Servidor puede dar problemas con el formato de fecha  dd/MM/yyyy o yyyy/MM/dd
                }
                else
                {
                    strFechaDesde = DateTime.Now.Year.ToString() + "/" + DateTime.Now.ToString("MM") + "/01";
                    strFechaHasta = DateTime.Now.ToString("yyyy/MM/dd 23:59:59");

                }

                //DOCUMENTOS PENDIENTES
                if (cboxOrigenData.SelectedIndex.ToString() == "0")
                {
                    pendientes = true;
                }

                //SOLO FACTURAS
                if (cboxOrigenData.SelectedIndex.ToString() == "1")
                {
                    soloFras = true;
                }


                if (cboxFormasPago.Checked)
                {
                    formaDePago = cboxFormasPago.Text;
                }

                if (csGlobal.modeDebug)
                {
                    pendientes = true;
                }

                DataTable table = new DataTable();

                if (csGlobal.isODBCConnection)
                {
                    OdbcConnection Odbcconnection = new OdbcConnection("DSN=ps_shop");
                    Odbcconnection.Open();

                    OdbcDataAdapter MyDA = new OdbcDataAdapter();

                    //Script para cargar los datos de las cabeceras de documentos
                    MyDA.SelectCommand = new OdbcCommand(sqlScripts.selectDocumentosPS(csGlobal.versionPS, soloFras, strFechaDesde, strFechaHasta, pendientes, formaDePago, "", true, porFechaPedido), Odbcconnection);

                    MyDA.Fill(table);
                }
                else
                {
                    MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                    conn.Open();

                    MySqlDataAdapter MyDA = new MySqlDataAdapter();

                    //Script para cargar los datos de las cabeceras de documentos
                    MyDA.SelectCommand = new MySqlCommand(sqlScripts.selectDocumentosPS(csGlobal.versionPS, soloFras, strFechaDesde, strFechaHasta, pendientes, formaDePago, "", true, porFechaPedido), conn);

                    MyDA.Fill(table);
                }

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                // Nuevo método de añadir el datasource al datagridview
                csUtilidades.addDataSource(dgvDocsPs, bSource);
                ocultarColumnasCabecera();
                string consulta_cliente_bloqueado = "";
                foreach (DataGridViewRow row in dgvDocsPs.Rows)
                {
                    consulta_cliente_bloqueado = "select BLOQUEADO FROM CLIENTES WHERE LTRIM(CODCLI) = '" + row.Cells["kls_a3erp_id"].Value.ToString() + "'";
                    if (sql.obtenerCampoTabla(consulta_cliente_bloqueado) == "T")
                    {
                        row.DefaultCellStyle.BackColor = Color.Red;
                        btSyncA3ERP.Enabled = false;
                    }
                }
                //dgvDocsPs.DataSource = bSource;

                CabeceraDocs = table;
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            return CabeceraDocs;
        }

        public DataTable cuponesPedidos(DataTable cabeceras)
        {
            DataTable cuponesDocs = new DataTable();
            csMySqlConnect conector = new csMySqlConnect();
            csSqlScripts sqlScripts = new csSqlScripts();
            csSqlConnects sql = new csSqlConnects();
            string filtroDocs = "";
            int numFila = 0;

            try
            {
                foreach (DataRow filaDoc in cabeceras.Rows)
                {
                    filtroDocs = (numFila == 0) ? filaDoc["id_order"].ToString() : filtroDocs + "," + filaDoc["id_order"].ToString();
                    numFila++;
                }

                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();
               
                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                //Script para cargar los datos de las cabeceras de documentos
                MyDA.SelectCommand = new MySqlCommand("select id_order, name, value_tax_excl from ps_order_cart_rule where id_order in (" + filtroDocs + ")" , conn);

                MyDA.Fill(cuponesDocs);
                conn.Close();
                return cuponesDocs;

            }
            catch (Exception ex)
            {
                csUtilidades.log(ex.Message);
                return null;
            }



        }

        private void frSincronizarDocs_Load(object sender, EventArgs e)
        {
            cbFormaPago.Visible = false;
            cboxDetail.Checked = true;
            var now = DateTime.Now;
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            dtpDesde.Value = startOfMonth;
            dtpHasta.Value = DateTime.Today;
            //columnasDocDetalle();
            dgvDocsPs.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void facturasActivadas() 
        {
            csMySqlConnect mysql = new csMySqlConnect();

            string activo = mysql.obtenerDatoFromQuery("select value from ps_configuration where name = 'PS_INVOICE'");

            if (activo == "" || !Convert.ToBoolean(Convert.ToInt32(activo)) ) // Facturas activ
            {
                "Atención: Facturas desactivadas! \n\nActiva las facturas en el backoffice > pedidos > facturas > 'Activa las facturas'".mb();
            }
        }

        private void establecerColumnaImagen()
        {
            if (!dgvDetalleLineas.Columns.Contains("PS"))
            {
                DataGridViewImageColumn imagen = new DataGridViewImageColumn();
                imagen.Name = "PS";
                dgvDetalleLineas.Columns.Add(imagen);
                dgvDetalleLineas.Columns["PS"].DisplayIndex = 0;
            }
            dgvDetalleLineas.Columns["PS"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;

        }






        private void cargarDocumentos()
        {
            btSyncA3ERP.Enabled = true;
            DataTable lineasDocumentos = new DataTable();
            if (csGlobal.ServirPedido.ToUpper() == "SI")
            {
                tsslblInfo.Text = "Servir Pedidos Automáticamente Activado";
            }
            else
            {
                tsslblInfo.Text = "";
            }
            
            // Comprobar que estan activadas las facturas
            this.facturasActivadas();

            A3CabeceraDocs();
            if (cboxDetail.Checked)
            {
                establecerColumnaImagen();
                lineasDocumentos = A3TablaLineas();
            }

            if (dgvDetalleLineas.Rows.Count > 0)
            {
                verificarDatosDocumentos(lineasDocumentos);
                dgvDetalleLineas.Refresh();
                verificarDatosDocumentos(lineasDocumentos);
                ocultarColumnasDetalle();
            }

            dgvDocsPs.ClearSelection();
        }

        private void ocultarColumnasDetalle()
        {
            this.dgvDetalleLineas.Columns["group_reduction"].Visible = false;
            this.dgvDetalleLineas.Columns["product_quantity_reinjected"].Visible = false;
            this.dgvDetalleLineas.Columns["discount_quantity_applied"].Visible = false;
            this.dgvDetalleLineas.Columns["download_deadline"].Visible = false;
            this.dgvDetalleLineas.Columns["id_warehouse"].Visible = false;
            this.dgvDetalleLineas.Columns["id_shop"].Visible = false;
            this.dgvDetalleLineas.Columns["reduction_percent"].Visible = false;
            this.dgvDetalleLineas.Columns["reduction_amount"].Visible = false;
            this.dgvDetalleLineas.Columns["product_ean13"].Visible = false;
            this.dgvDetalleLineas.Columns["download_hash"].Visible = false;
            this.dgvDetalleLineas.Columns["product_upc"].Visible = false;
            this.dgvDetalleLineas.Columns["product_quantity_in_stock"].Visible = false;
            this.dgvDetalleLineas.Columns["product_quantity_return"].Visible = false;
            this.dgvDetalleLineas.Columns["product_quantity_refunded"].Visible = false;
            this.dgvDetalleLineas.Columns["reduction_amount_tax_incl"].Visible = false;
            this.dgvDetalleLineas.Columns["product_supplier_reference"].Visible = false;
            this.dgvDetalleLineas.Columns["product_weight"].Visible = false;
            this.dgvDetalleLineas.Columns["ecotax"].Visible = false;
            this.dgvDetalleLineas.Columns["ecotax_tax_rate"].Visible = false;
            this.dgvDetalleLineas.Columns["download_nb"].Visible = false;
            this.dgvDetalleLineas.Columns["tax_computation_method"].Visible = false;
            //this.dgvDetalleLineas.Columns["id_tax_rules_group"].Visible = false;
            this.dgvDetalleLineas.Columns["id_order_invoice"].Visible = false;
            this.dgvDetalleLineas.Columns["product_quantity_discount"].Visible = false;
            this.dgvDetalleLineas.Columns["purchase_supplier_price"].Visible = false;
            this.dgvDetalleLineas.Columns["original_product_price"].Visible = false;
            this.dgvDetalleLineas.Columns["associations"].Visible = false;

        }

        private void ocultarColumnasCabecera()
        {
            this.dgvDocsPs.Columns["module"].Visible = false;
            this.dgvDocsPs.Columns["id_address_invoice"].Visible = false;
            this.dgvDocsPs.Columns["dni"].Visible = false;
            this.dgvDocsPs.Columns["phone"].Visible = false;
            this.dgvDocsPs.Columns["pais"].Visible = false;
            this.dgvDocsPs.Columns["carrier_Ref"].Visible = false;
            this.dgvDocsPs.Columns["OPECCM"].Visible = false;
            this.dgvDocsPs.Columns["total_paid"].DefaultCellStyle.Format = "c";
            this.dgvDocsPs.Columns["total_paid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvDocsPs.Columns["total_discounts"].DefaultCellStyle.Format = "c";
            this.dgvDocsPs.Columns["total_discounts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvDocsPs.Columns["total_products"].DefaultCellStyle.Format = "c";
            this.dgvDocsPs.Columns["total_products"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvDocsPs.Columns["portes"].DefaultCellStyle.Format = "c";
            this.dgvDocsPs.Columns["portes"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        
        }

        public void verificarDatosDocumentos(DataTable datosLineas)
        {
            string articulosToCheck = "";
            csSqlConnects sqlconector = new csSqlConnects();
            DataTable articulosNoA3ERP = new DataTable();
            bool existeArticulo = false;
            string referencePS = "";
            string codartA3 = "";

            int i = 0;
            foreach (DataRow fila in datosLineas.Rows)
            {
                if (i == 0)
                {
                    articulosToCheck = "'" + fila["product_reference"].ToString() + "'";
                }
                else
                {
                    articulosToCheck = articulosToCheck + ",'" + fila["product_reference"].ToString() + "'";
                }
                i++;
            }
            //MessageBox.Show(articulosToCheck);
            articulosNoA3ERP = sqlconector.cargarDatosTablaA3("SELECT LTRIM(CODART) as CODART FROM ARTICULO WHERE LTRIM(CODART) IN (" + articulosToCheck + ")");

            //btSyncA3ERP.Enabled = true;
            if (articulosNoA3ERP != null && articulosNoA3ERP.Rows.Count > 0)
            {
                foreach (DataGridViewRow filaDgv in dgvDetalleLineas.Rows)
                {
                    existeArticulo = false;
                    referencePS = filaDgv.Cells["product_reference"].Value.ToString();
                    foreach (DataRow filaItem in articulosNoA3ERP.Rows)
                    {
                        codartA3 = filaItem["CODART"].ToString();
                        if (referencePS == codartA3)
                        {
                            existeArticulo = true;
                            break;
                        }
                    }
                    if (!existeArticulo)
                    {
                        //CADCANARIAS
                        if (!csGlobal.gestionArticulosPedidos)
                        {
                            btSyncA3ERP.Enabled = false;
                        }
                        
                        filaDgv.DefaultCellStyle.BackColor = Color.Red;
                        tabDocumentos.SelectedIndex = 1;
                    }
                }
            }

        }

        public DataTable A3TablaLineas(bool dgv = true)
        {
            if (dgv)
            {
                do
                {
                    foreach (DataGridViewRow row in dgvDetalleLineas.Rows)
                    {
                        try
                        {
                            dgvDetalleLineas.Rows.Remove(row);
                        }
                        catch (Exception ex) { }
                    }
                } while (dgvDetalleLineas.Rows.Count > 0);
            }

            DataTable lineasDePedidoParaA3 = new DataTable();
            //int numLinea = 0;
            // dgvDetalleLineas.Rows.Add();
            int numLineaPedido = 0;
            Dictionary<string, string> diccioOrderLines = new Dictionary<string, string>();
            DataRow filaX;

            string[] TyC = new string[4];
            string urlRequest = "";
            string urlRequestLines = "";
            XmlDocument xdoc = new XmlDocument();
            csPSWebService psWebService = new csPSWebService();
            //Cargo el XML de cada Cabecera de Documento
            //24/12/2020 Añadido para CadCanarias, se añade porque el certificado no era compatible con esync.
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            foreach (DataGridViewRow fila in dgvDocsPs.Rows)
            {
                urlRequest = "orders/" + fila.Cells["id_order"].Value.ToString();
                xdoc = psWebService.readPSWebService("GET", urlRequest);
                //textBox2.Text = xdoc.ToString();
                XmlNodeList listaNodos = xdoc.GetElementsByTagName("order");

                foreach (XmlNode elemento in listaNodos)
                {
                    foreach (XmlNode subNodo in elemento.ChildNodes)
                    {
                        //Recorro el XML de la cabecera, y en associations tengo las lineas
                        if (subNodo.Name == "associations")
                        {
                            foreach (XmlNode asociaciones in subNodo.ChildNodes)
                            {
                                if (asociaciones.Name == "order_rows")
                                {
                                    foreach (XmlNode lineasPedido in asociaciones.ChildNodes)
                                    {
                                        // dgvDetalleLineas.Rows.Add();
                                        // DataGridViewRow filaAsignada = dgvDetalleLineas.Rows[numLinea];
                                        // numLinea += 1;

                                        // en funcion del numero de lineas del xml, el nodetype es order_rows u order_row
                                        if (lineasPedido.Name == "order_row" || lineasPedido.Name == "order_rows")
                                        {
                                            //COMIENZO A LEER LAS LINEAS DE PEDIDO 

                                            //UTILIZAR EL WEB SERVICE PARA OBTENER EL DETALLE DE LAS LÍNEAS

                                            foreach (XmlNode lineaDePedido in lineasPedido.ChildNodes)
                                            {
                                                if (lineaDePedido.Name == "id")
                                                {
                                                    //Obtengo el XML de cada línea
                                                    numLineaPedido = Convert.ToInt32(lineaDePedido.InnerText);
                                                    urlRequestLines = "order_details/" + numLineaPedido.ToString();
                                                    xdoc = psWebService.readPSWebService("GET", urlRequestLines);

                                                    //CREO LA TABLA QUE CONTENDRÁ LOS VALORES DE LAS LÍNEAS
                                                    XmlNodeList listaNodosLineas = xdoc.GetElementsByTagName("order_detail");
                                                    //Añado las columnas a la tabla que utilizaré
                                                    if (lineasDePedidoParaA3.Columns.Count == 0)
                                                    {
                                                        foreach (XmlNode elementoLinea in listaNodosLineas)
                                                        {
                                                            foreach (XmlNode campoLinea in elementoLinea.ChildNodes)
                                                            {
                                                                lineasDePedidoParaA3.Columns.Add(campoLinea.Name);
                                                            }
                                                            //Añado las columnas para tallas y colores
                                                            lineasDePedidoParaA3.Columns.Add("CodFamTallaH");
                                                            lineasDePedidoParaA3.Columns.Add("CodFamTallaV");
                                                            lineasDePedidoParaA3.Columns.Add("CodTallaH");
                                                            lineasDePedidoParaA3.Columns.Add("CodTallaV");
                                                            
                                                            if (csGlobal.databaseA3.ToUpper().Contains("THAGSON")) lineasDePedidoParaA3.Columns.Add("catalogue_origin");
                                                        }
                                                    }


                                                    //Una vez tengo todas las columnas creadas, paso a grabar las filas
                                                    //Añado una fila nueva para guardar los datos de la línea que estoy leyendo
                                                    filaX = lineasDePedidoParaA3.NewRow();



                                                    foreach (XmlNode elementoLinea in listaNodosLineas)
                                                    {
                                                        string codart = "";
                                                        foreach (XmlNode campoLinea in elementoLinea.ChildNodes)
                                                        {
                                                            if (csGlobal.databaseA3.ToUpper().Contains("THAGSON"))
                                                            {

                                                                XmlNode catalogue_origin = lineasPedido.SelectSingleNode("catalogue_origin");
                                                                if (catalogue_origin.InnerText != "")
                                                                {
                                                                    filaX["catalogue_origin"] = catalogue_origin.InnerText;
                                                                }
                                                            }

                                                            //NACHO 02/10/24 PARA GESTIONAR QUE NO SE APLIQUE EL DESCUENTO EN PEDIDOS 2 VECES A DISMAY
                                                            filaX[campoLinea.Name] = (campoLinea.Name == "product_price" && csGlobal.conexionDB.Contains("DISMAY")) ? elementoLinea.SelectSingleNode("original_product_price").InnerText : campoLinea.InnerText;

                                                            //filaX[campoLinea.Name] = campoLinea.InnerText;

                                                            if (campoLinea.Name == "product_id")
                                                            {
                                                                codart = campoLinea.InnerText;
                                                            }

                                                            if (campoLinea.Name == "product_attribute_id")
                                                            {
                                                                if (csGlobal.tieneTallas == true)
                                                                {
                                                                    if (campoLinea.InnerText != "0")
                                                                    {
                                                                        try
                                                                        {
                                                                            if (csGlobal.conexionDB.Contains("ROSSO") && campoLinea.Name == "product_reference")
                                                                            {
                                                                                filaX["product_reference"] = "0";
                                                                            }


                                                                            TyC = tallayColor(campoLinea.InnerText, codart);
                                                                            filaX["CodFamTallaH"] = TyC[0];
                                                                            filaX["CodFamTallaV"] = TyC[1];
                                                                            filaX["CodTallaH"] = TyC[2];
                                                                            filaX["CodTallaV"] = TyC[3];

                                                                            // Para cuando A3 no tiene atributos y PS sí
                                                                            if (csGlobal.modoTallasYColores == "A3NOPSSI")
                                                                            {
                                                                                filaX[3] = 0;
                                                                            }
                                                                        }catch (Exception ex)
                                                                        {

                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }



                                                    //PROGRAMACIÓN A MEDIDA PARA DISMAY (2-10-15 LVG)
                                                    //EL ÚNICO CAMBIO QUE SE REALIZA ES PONER EL ID DE ATRIBUTO A 0 PARA QUE AL HACER EL DOCUMENTO EN A3
                                                    //CONSIDERE LA LÍNEA COMO UN PRODUCTO INDEPENDIENTE.
                                                    if (csGlobal.conexionDB == "dismayNuevo")
                                                    {
                                                        filaX["product_attribute_id"] = "0";
                                                    }


                                                    //Añado los datos a la nueva fila creada
                                                    lineasDePedidoParaA3.Rows.Add(filaX);

                                                }


                                                //filaAsignada.Cells[lineaDePedido.Name.ToString()].Value = lineaDePedido.InnerText.ToString();
                                                //c += 1;
                                                ////MessageBox.Show("Pedido: " + fila.Cells[0].Value.ToString() + " / Linea  " + numLinea + " / " +  lineaDePedido.Name.ToString() + " - " + lineaDePedido.InnerText.ToString());
                                            }


                                        }
                                    }
                                }
                            }
                        }

                    }

                }

                //Añadir linea de regalo
                //hay que definir un código de artículo
                if (dgvDocsPs.Columns.Contains("gift")) { 
                    if (fila.Cells["gift"].Value.ToString() == "1")
                    {

                    filaX = lineasDePedidoParaA3.NewRow();
                    filaX["id_order"] = fila.Cells["id_order"].Value.ToString(); ;
                    filaX["product_reference"] = string.IsNullOrEmpty(csGlobal.articuloRegalo)? "0" : csGlobal.articuloRegalo;
                    filaX["product_quantity"] = "1" ;
                    filaX["product_price"] = fila.Cells["total_wrapping_tax_excl"].Value.ToString();
                    filaX["reduction_percent"] = "0,00";
                    filaX["unit_price_tax_incl"] = fila.Cells["total_wrapping_tax_incl"].Value.ToString(); ;

                        lineasDePedidoParaA3.Rows.Add(filaX);

                    }
                }

            if (dgv)
                    csUtilidades.addDataSource(dgvDetalleLineas, lineasDePedidoParaA3);
                //dgvDetalleLineas.DataSource = lineasDePedidoParaA3;

            }

            //VALIDO QUE TODOS LOS ARTÍCULOS ESTÉN DADOS DE ALTA EN A3ERP

            return lineasDePedidoParaA3;
        }

        //Esta función carga los datos de prestashop y crea los documentos en A3ERP
        public int sincronicarDocsA3(bool dgv = true, DataTable cabecera = null, DataTable detalles = null)
        {
            decimal docGenerado;
            csa3erp A3Func = new csa3erp();
            try
            {
                //Instancio los Datasets de Cabecera y Lineas
                DataSet A3Dataset = new DataSet("DocumentosA3");
                DataTable LineasDocs = new DataTable("LineasDocs");
                DataTable CabeceraDocs = new DataTable("CabeceraDocs");
                DataTable Cupones = new DataTable("CuponesDocs");
                if (dgv)
                {
                    //Cargo los datos de Cabecera
                     CabeceraDocs = A3CabeceraDocs();

                    //Cargo los datos de las Líneas
                    LineasDocs = A3TablaLineas();

                }
                else // lo recojo de fuera
                {
                    CabeceraDocs = cabecera;
                    LineasDocs = detalles;
                }

               

                CabeceraDocs.TableName = "CabeceraDocs";
                LineasDocs.TableName = "LineasDocs";
                A3Dataset.Tables.Add(CabeceraDocs);
                A3Dataset.Tables.Add(LineasDocs);

                //Función para añadir los cupones como lineas de documento
                if (CabeceraDocs.Rows.Count > 0 && csGlobal.cupon_as_line)
                {
                    Cupones = cuponesPedidos(CabeceraDocs);
                    Cupones.TableName = "CuponesDocs";
                    A3Dataset.Tables.Add(Cupones);
                }

                "--------------------------------------------".log();

                "Cargadas cabecera y lineas pero aun no se ha abierto el enlace".log();

                A3Func.abrirEnlace();
                MessageBox.Show("ENLACE ABIERTO");
                docGenerado = A3Func.generaDocA3Dataset(A3Dataset); // ! 
                A3Func.cerrarEnlace();
            }
            catch (Exception ex)
            {
                ex.Message.log();
                MessageBox.Show(ex.Message);
                error_a3 = ex.Message;
                //A3Func.cerrarEnlace();
                //Program.guardarErrorFichero(ex.ToString());
                return 0;
            }
            finally
            {
                //A3Func.cerrarEnlace();
            }

            return Convert.ToInt16(docGenerado);
        }
        string error_a3 = "";
        //Esta función carga los datos de prestashop y crea los documentos en A3ERP
        private void sincronizarPedidosA3()
        {
            //Instancio los Datasets de Cabecera y Lineas
            DataSet A3Dataset = new DataSet("PedidosA3");
            DataTable LineasDocs = new DataTable("LineasDocs");
            DataTable CabeceraDocs = new DataTable("CabeceraDocs");

            //Cargo los datos de Cabecera
            CabeceraDocs = A3CabeceraDocs();

            //Cargo los datos de las Líneas
            LineasDocs = A3TablaLineas();

            CabeceraDocs.TableName = "CabeceraDocs";
            LineasDocs.TableName = "LineasDocs";
            A3Dataset.Tables.Add(CabeceraDocs);
            A3Dataset.Tables.Add(LineasDocs);
            csa3erp A3Func = new csa3erp();
            A3Func.abrirEnlace();
            A3Func.generaDocA3Dataset(A3Dataset);
            A3Func.cerrarEnlace();
        }

        //Función 
        private string[] tallayColor(string atributoArticulo, string codart = null)
        {
            try
            {


                string atributosPS = "";
                int cont = 0;
                string[] arrayAtributos = new string[4];
                csSqlConnects sqlconector = new csSqlConnects();
                DataRow[] filasAtributos;
                
                if (csGlobal.modoTallasYColores.ToUpper() == "DISMAY")
                {
                    arrayAtributos[0] = ""; //CODFAMTALLAH
                    arrayAtributos[1] = ""; //CODFAMTALLAV
                    arrayAtributos[2] = ""; //CODIGO COLOR NO ID COLOR
                    arrayAtributos[3] = ""; //CODIGO TALLA NO ID TALLA
                }
                else if (csGlobal.modoTallasYColores.ToUpper() == "A3SIPSSI")
                {
                    atributosPS = obtenerTallasYColoresDeLineaPedido(atributoArticulo);

                    if (atributosPS != "")
                    {
                        DataTable tablaTyC = sqlconector.cargarCombinacionTallasYColores(atributosPS);

                        foreach (DataRow fila in tablaTyC.Rows)
                        {
                            if (fila["VERTICAL"].ToString() == "T")
                            {
                                arrayAtributos[1] = fila["CODFAMTALLA"].ToString(); //CODFAMTALLAV
                                arrayAtributos[3] = fila["CODTALLA"].ToString(); //CODIGO TALLA NO ID TALLA
                            }
                            else
                            {
                                arrayAtributos[0] = fila["CODFAMTALLA"].ToString(); //CODFAMTALLAH
                                arrayAtributos[2] = fila["CODTALLA"].ToString(); //CODIGO COLOR NO ID COLOR
                            }
                        }
                    }
                }
                else if (csGlobal.conexionDB != "dismayNuevo")
                {
                    //DataTable tablaTyC = sqlconector.cargarDatosTabla("KLS_ESYNC_TALLASYCOLORES");
                    DataTable tablaTyC = sqlconector.cargarCombinacionTallasColores();
                    string filtroTabla = "ID='" + atributoArticulo + "'";

                    filasAtributos = tablaTyC.Select(filtroTabla);
                    if (filasAtributos.Length > 0)
                    {
                        arrayAtributos[0] = filasAtributos[0][2].ToString(); //CODFAMTALLAH
                        arrayAtributos[1] = filasAtributos[0][3].ToString(); //CODFAMTALLAV
                        arrayAtributos[2] = filasAtributos[0][4].ToString(); //CODIGO COLOR NO ID COLOR
                        arrayAtributos[3] = filasAtributos[0][5].ToString(); //CODIGO TALLA NO ID TALLA
                    }
                }
                else //PARA DISMAY A DEPRECAR
                {
                    arrayAtributos[0] = ""; //CODFAMTALLAH
                    arrayAtributos[1] = ""; //CODFAMTALLAV
                    arrayAtributos[2] = ""; //CODIGO COLOR NO ID COLOR
                    arrayAtributos[3] = ""; //CODIGO TALLA NO ID TALLA
                }




                //Función creada para indusnow para detectar aquellos artículos que tienen una de sus dos familias de tallas "monoatributo"
                //En prestashop no se han creado estas tallas, pero se debe volver a poner la talla por defecto al bajarla a A3
                if (csGlobal.activarMonotallas == "Y")
                {
                    string query = "select ltrim(codart) as codart,kls_id_shop, " +
                                   " CODFAMTALLAH,(SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=CODFAMTALLAH) AS TALLASH, " +
                                   " CASE WHEN (SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAH)=1 THEN (SELECT LTRIM(CODTALLA) FROM TALLAS WHERE CODFAMTALLA=CODFAMTALLAH) ELSE NULL END AS TALLAH_MONO, " +
                                   " CODFAMTALLAV,(SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAV) AS TALLASV, " +
                                   " CASE WHEN (SELECT COUNT(CODTALLA) FROM TALLAS  WHERE CODFAMTALLA=ARTICULO.CODFAMTALLAV)=1 THEN (SELECT LTRIM(CODTALLA) FROM TALLAS WHERE CODFAMTALLA=CODFAMTALLAV) ELSE NULL END AS TALLAV_MONO " +
                                   " from articulo where kls_id_shop=" + codart;
                     DataTable tablaArticulosTallasMonotalla = sqlconector.cargarDatosScriptEnTabla(query, "");

                    if (tablaArticulosTallasMonotalla.Rows[0]["TALLASH"].ToString() == "1")
                    {
                        arrayAtributos[0] = tablaArticulosTallasMonotalla.Rows[0]["CODFAMTALLAH"].ToString().Trim();
                        arrayAtributos[2] = tablaArticulosTallasMonotalla.Rows[0]["TALLAH_MONO"].ToString().Trim();
                    }

                    if (tablaArticulosTallasMonotalla.Rows[0]["TALLASV"].ToString() == "1")
                    {
                        arrayAtributos[1] = tablaArticulosTallasMonotalla.Rows[0]["CODFAMTALLAV"].ToString().Trim();
                        arrayAtributos[3] = tablaArticulosTallasMonotalla.Rows[0]["TALLAV_MONO"].ToString().Trim();
                    }

                }

                return arrayAtributos;

            } catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    if (!csGlobal.gestionArticulosPedidos) {
                        (ex.Message).mb();
                    }
                   
                }
                return null;
            }
        }

        private string obtenerTallasYColoresDeLineaPedido(string id_product_attribute)
        {
            string[] arrayAtributos = new string[2];
            string filtroAtributos = "";
            int filas = 0;
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            MySqlConnection connection = new MySqlConnection(mySqlConnect.conexionDestino());
            connection.Open();
            string queryLoadCombinacionesPS = "";
            queryLoadCombinacionesPS = " select id_attribute,id_product_attribute from ps_product_attribute_combination where id_product_attribute=" + id_product_attribute;

            MySqlDataAdapter myDA = new MySqlDataAdapter();
            myDA.SelectCommand = new MySqlCommand(queryLoadCombinacionesPS, connection);
            DataTable tabla = new DataTable();
            myDA.Fill(tabla);
            //close connection
            connection.Close();
            filas = tabla.Rows.Count;
            if (filas>0)
            {
            //Redimensiono el array al númeor de filas/atributos que tenga el artículo
            Array.Resize(ref arrayAtributos, filas);
            for (int i = 0; i < filas; i++)
            {
                arrayAtributos[i] = tabla.Rows[i]["id_attribute"].ToString();

                if (i > 0)
                {

                    filtroAtributos = filtroAtributos + "," + arrayAtributos[i];
                }
                else
                {
                    filtroAtributos = filtroAtributos + arrayAtributos[i];

                }
            }
            filtroAtributos = "(" + filtroAtributos + ")";
        }

            return filtroAtributos;
        }



        private void sincronizarDirecciones()
        {
            esyncro.csSyncAddresses syncAddress = new esyncro.csSyncAddresses();

            DataView view = new DataView((DataTable)dgvDocsPs.DataSource);
            DataTable distinctCustomers = view.ToTable(true, "id_customer");

            if (distinctCustomers.Rows.Count > 0)
            {
                foreach (DataRow dr in distinctCustomers.Rows)
                {
                    syncAddress.actualizarInfoDirecciones(dr["id_customer"].ToString());
                }
            }
        }

        private void TraspasarDocsA3ERP()
        {
          
            if (dgvDetalleLineas.Rows.Count > 0 && dgvDocsPs.Rows.Count > 0)
            {
                Cursor.Current = Cursors.WaitCursor;
                int documentosGenerados = 0;
                int documentosParaSincronizar = 0;

                //Verifico si los documentos ya están bajados en A3ERP
                if (csGlobal.docDestino.ToUpper() == "PEDIDO")
                {
                    if (verificarDocumentosSincronizar())
                    {
                        return;
                    }
                }

                Control.CheckForIllegalCrossThreadCalls = false;
                btSyncA3ERP.Enabled = false;
                Stopwatch tiempo = new Stopwatch();
                tiempo.Start();

                csStocks stock = new csStocks();
                tsslbInfo.ForeColor = System.Drawing.Color.Black;
                tsslbInfo.Text = "";
                tsslbInfo.Text = ">>>>>>>T R A S P A S A N D O   D O C U M E N T O S   A   A 3 E R P >>>>>>";

                documentosParaSincronizar = dgvDocsPs.Rows.Count;
                documentosGenerados = sincronicarDocsA3(); // !
                cargarDocumentos();

                TimeSpan tiempoTranscurrido = tiempo.Elapsed;
                string minutos = tiempoTranscurrido.Minutes.ToString() + " m. ";
                string segundos = tiempoTranscurrido.Seconds.ToString() + " s. ";

                Cursor.Current = Cursors.Default;

                if (documentosGenerados == 0)
                {
                    tsslbInfo.ForeColor = System.Drawing.Color.Red;
                 
                    if (error_a3 != "")
                    {
                        tsslbInfo.Text = "ERROR AL TRASPASAR: " + error_a3.ToUpper().Replace("\n", " - ");
                    }
                    else
                    {
                        tsslbInfo.Text = "INCIDENCIAS EN LA SINCRONIZACIÓN, SE HAN SINCRONIZADO " + documentosGenerados + " DE " + documentosParaSincronizar + " DOCUMENTOS PARA SINCRONIZAR!!!!! ----->" + minutos + segundos;
                    }
                }
                else
                {
                    tsslbInfo.Text = "T R A S P A S O   R E A L I Z A D O!!!!! SE HAN SINCRONIZADO " + documentosGenerados + " DE " + documentosParaSincronizar + " DOCUMENTOS PARA SINCRONIZAR!!!!! ----->" + minutos + segundos;
                }
                btSyncA3ERP.Enabled = true;

            }
        }

        //Función para verificar si los documentos se pueden descargar
        private bool verificarDocumentosSincronizar()
        { 
            bool descargar=false;
            string numdoc="";
            csSqlConnects sqlConnect = new csSqlConnects();
            foreach (DataGridViewRow fila in dgvDocsPs.Rows)
            {
                numdoc = fila.Cells["id_order"].Value.ToString();
                descargar= sqlConnect.obtenerExisteDocumentoEnA3("select * from cabepedv where numdoc=" + numdoc + " and serie='" + csGlobal.serie + "'");
                if (descargar)
                {
                    tsslbInfo.ForeColor = System.Drawing.Color.Red;
                    tsslbInfo.Text = ">>>>>>>EL DOCUMENTO " + numdoc +  " YA SE HA DESCAGARDO EN A3ERP>>>>>";
                    break;
                }
            }
            
            
            return descargar;
        }

        private void borrarSincronización()
        {

            csMySqlConnect mySql = new csMySqlConnect();
            csa3erp a3 = new csa3erp();
            a3.abrirEnlace();
            bool pedido = false;

            if (csGlobal.docDestino == "Pedido")
            {
                pedido = true;
            }

            foreach (DataGridViewRow fila in dgvDocsPs.SelectedRows)
            {
                mySql.borrar("kls_invoice_erp", "invoice_number_erp", fila.Cells["docsA3"].Value.ToString());
                a3.borrarDocumento(fila.Cells["docsA3"].Value.ToString(), "Pedidos", false);
            }
            tsslbInfo.Text = ">>>>>>> BORRADA LA SINCRONIZACIÓN DE  " + dgvDocsPs.SelectedRows.Count + " DOCUMENTOS>>>>>";
            cargarDocumentos();
            
        }

        private void borrarSincronizaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            borrarSincronización();
        }

        private void sincronizarDocumentoManualmenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvDocsPs.SelectedRows.Count != 1)
            {
                tsslblInfo.Text = "Debe seleccionar sólo la fila del documento que desea sincronizar";
            }
            else if (dgvDocsPs.SelectedRows.Count == 1)
            {
                sincronizarDocumento(dgvDocsPs.SelectedRows[0].Cells["invoice_number"].Value.ToString());
                tsslblInfo.Text = "Documento Sincronizado";
            }
            cargarDocumentos();
        }

        private void sincronizarDocumento(string numDoc)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.sincronizarDocumentoManualmente(numDoc);
        }

        private void dgvDocsPs_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            tsslbInfo.Text = dgvDocsPs.Rows.Count.ToString() + " Documentos";
        }

        private void crearProductosA3FromPS(string articulo)
        {
            csa3erp a3ERP = new csa3erp();
            csa3erpItm a3ERPItm = new csa3erpItm();
            csMySqlConnect mysql = new csMySqlConnect();
            string codart = "";
            string name, price, alias, idProduct, reference = "";

            try
            {
                a3ERP.abrirEnlace();
                foreach (DataGridViewRow fila in dgvDetalleLineas.SelectedRows)
                {
                    idProduct = fila.Cells["product_id"].Value.ToString();
                    name = fila.Cells["product_name"].Value.ToString().ToUpper();
                    alias = fila.Cells["product_name"].Value.ToString().Replace("(", "").Replace(")", "").ToUpper();
                    price = fila.Cells["product_price"].Value.ToString().Replace(".", ",");
                    reference = fila.Cells["product_reference"].Value.ToString();

                    codart = a3ERPItm.crearArticuloA3V2(name, alias, price, reference, idProduct);
                    codart = codart.Replace(" ", "");
                    if (codart != null)
                        csUtilidades.actualizarProductoPS(codart, idProduct);
                    //mysql.actualizarValoresEnTabla("ps_product", "reference", codart, "id_product", idProduct);
                }
                a3ERP.cerrarEnlace();
            }
            catch (Exception ex) { Program.guardarErrorFichero(ex.Message); }
        }

        private void crearArtículoEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearProductosA3FromPS(dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
        }

        private void frSincronizarDocs_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //Environment.Exit(Environment.ExitCode);
            }
            catch (Exception)
            {
            }
        }

        private void copiarReferenciaDelProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                if (dgvDetalleLineas.Rows.Count > 0)
                {
                    if (dgvDetalleLineas.SelectedRows.Count == 1)
                    {
                        Clipboard.SetText(dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
                        MessageBox.Show("Texto copiado en el portapapeles");
                    }
                }
            }
        }

        private void editarReferenciaDelProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvDetalleLineas.SelectedRows.Count == 1)
            {
                string referencia = Microsoft.VisualBasic.Interaction.InputBox("Por favor, Introducir el código de artículo de A3ERP que se traspasará", "Actualizar Referencia", dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
                string idLinea = dgvDetalleLineas.SelectedRows[0].Cells["id"].Value.ToString();
                if (referencia != "")
                {
                    csUtilidades.ejecutarConsulta("update ps_order_detail set product_reference='" + referencia + "' where id_order_detail=" + idLinea, true);
                    csUtilidades.ejecutarConsulta("update ps_product_attribute set reference='" + referencia + "' where id_product_attribute=(select product_attribute_id from ps_order_detail where id_order_detail=" + idLinea + ")", true);
                    cargarDocumentos();
                }
            }
            else
            {
                MessageBox.Show("Sólo se puede seleccionar una fila");
            }
        }


        private void cambiarAtributoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (dgvDetalleLineas.Rows.Count > 0)
            {
                if (dgvDetalleLineas.SelectedRows.Count == 1)
                {
                    string product_id = dgvDetalleLineas.SelectedRows[0].Cells["product_id"].Value.ToString();
                    string order_detail_id = dgvDetalleLineas.SelectedRows[0].Cells["id"].Value.ToString();

                    frSeleccionTallaColor TyC = new frSeleccionTallaColor(product_id, order_detail_id);
                    TyC.Show();
                    //cargarDocumentos();
                }
            }
        }

        private void resetearAtributoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvDetalleLineas.Rows.Count > 0)
            {
                int productos_seleccionados = dgvDetalleLineas.SelectedRows.Count;

                if (productos_seleccionados > 0)
                {
                    if (string.Format("Estas a punto de resetear {0} atributos. Quieres proseguir?", productos_seleccionados).what())
                    {
                        csTallasYColoresV2 TyC = new csTallasYColoresV2();

                        foreach (DataGridViewRow item in dgvDetalleLineas.SelectedRows)
                        {
                            TyC.resetearTyCSlow(item.Cells["product_id"].Value.ToString());
                        }

                        "Producto reseteado con exito".mb();

                        if ("Quieres sincronizar estos atributos?".what())
                        {
                            foreach (DataGridViewRow item in dgvDetalleLineas.SelectedRows)
                            {
                                TyC.sincronizarAtributosArticulos(item.Cells["product_id"].Value.ToString());
                            }

                            "Atributos sincronizados".mb();
                        }
                        else
                        {
                            "Operación cancelada".mb();
                        }
                    }
                    else
                    {
                        "Operación cancelada".mb();
                    }
                }
            }
        }

        private void crearArticuloA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearProductosA3FromPS(dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
        }

        private void copiarReferenciaDelProductoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                if (dgvDetalleLineas.Rows.Count > 0)
                {
                    if (dgvDetalleLineas.SelectedRows.Count == 1)
                    {
                        Clipboard.SetText(dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
                        MessageBox.Show("Texto copiado en el portapapeles");
                    }
                }
            }
        }

        private void editarReferenciaDelProductoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (dgvDetalleLineas.SelectedRows.Count == 1)
            {
                csMySqlConnect mysSqlconnect = new csMySqlConnect();

                string referencia = Microsoft.VisualBasic.Interaction.InputBox("Por favor, Introducir el código de artículo de A3ERP que se traspasará", "Actualizar Referencia", dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString());
                string idLinea = dgvDetalleLineas.SelectedRows[0].Cells["id"].Value.ToString();
                string product_attribute_id = mysSqlconnect.obtenerDatoFromQuery("Select product_attribute_id from ps_order_detail where id_order_detail=" + idLinea);
                string id_product = dgvDetalleLineas.SelectedRows[0].Cells["product_id"].Value.ToString();
                if (referencia != "")
                {
                    csUtilidades.ejecutarConsulta("update ps_order_detail set product_reference='" + referencia + "' where id_order_detail=" + idLinea, true);
                    if (product_attribute_id == "0")
                    {
                        csUtilidades.ejecutarConsulta("update ps_product set reference='" + referencia + "' where id_product=" + id_product, true);
                    }
                    csUtilidades.ejecutarConsulta("update ps_product_attribute set reference='" + referencia + "' where id_product_attribute=(select product_attribute_id from ps_order_detail where id_order_detail=" + idLinea + ")", true);
                    cargarDocumentos();
                }
            }
            else
            {
                MessageBox.Show("Sólo se puede seleccionar una fila");
            }
        }

        private void cambiarEstadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable dt = mysql.cargarTabla("select id_order_state, name from ps_order_state_lang where id_lang = " + csUtilidades.idIdiomaDefaultPS());

            frDialogCombo combo = new frDialogCombo(dt, "name", "Estado", true);

            if (combo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string name = csGlobal.comboFormValue;
                string id = dt.buscar(string.Format("name = '{0}'", name)).Rows[0]["id_order_state"].ToString();

                if (string.Format("Estas seguro de querer cambiar el estado a {0}??", name).what())
                {
                    csPrestashop presta = new csPrestashop();

                    foreach (DataGridViewRow item in dgvDocsPs.SelectedRows)
                    {
                        string id_order = item.Cells["id_order"].Value.ToString();
                        presta.cambiarEstadoPedido(id_order, id);
                    }

                    "Estados cambiados con exito".mb();
                }
            }
        }

        private void dgvDetalleLineas_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgvDetalleLineas.Rows.Count > 0)
            {
                if (this.dgvDetalleLineas.Columns[e.ColumnIndex].Name == "PS")
                {
                    if (!string.IsNullOrEmpty(this.dgvDetalleLineas["product_id", e.RowIndex].Value.ToString()))
                    {

                        e.Value = Properties.Resources.esync32;
                        //if ((int)this.dgvDetalleLineas["product_id", e.RowIndex].Value > 0)
                        //{
                        //    e.Value = Properties.Resources.esync32;
                        //}
                        //else
                        //{
                        //    e.Value = Properties.Resources.noImage;
                        //}
                    }
                    else
                    {
                        e.Value = Properties.Resources.noImage;
                    }
                }
            }
        }

        private void dgvDetalleLineas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (this.dgvDetalleLineas.Columns[e.ColumnIndex].Name == "PS")
                {

                    string urlProduct = "";
                    string idProduct = "";

                    if (dgvDetalleLineas["product_id", e.RowIndex].Value.ToString() != "" && dgvDetalleLineas["product_id", e.RowIndex].Value.ToString() != "0" && dgvDetalleLineas["product_id", e.RowIndex].Value.ToString() != string.Empty)
                    {
                        try
                        {
                            idProduct = dgvDetalleLineas["product_id", e.RowIndex].Value.ToString();
                            urlProduct = csGlobal.nodeTienda + "/index.php?id_product=" + idProduct + "&controller=product&id_lang=" + csGlobal.defaultIdLangPS;
                            Process.Start("Chrome.exe", urlProduct);
                        }
                        catch
                        {
                            Process.Start("IExplore.exe", urlProduct);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El artículo no está en la tienda");
                    }
                }
            }
        }

        private void verificarArtículoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string fichaArticulo = "";
            DataTable articulo = new DataTable();
            string codart = "";
            string kls_idshop = "";
            string idproduct = dgvDetalleLineas.SelectedRows[0].Cells["product_id"].Value.ToString();
            string reference = dgvDetalleLineas.SelectedRows[0].Cells["product_reference"].Value.ToString();
            string tallas = "";
            string familiaTallaV = "";
            string numTallasV = "";
            string familiaTallaH = "";
            string numTallasH = "";
            string articulosHijos = "";
            string errorSyncro = "";

            csSqlConnects sql = new csSqlConnects();

            if (dgvDetalleLineas.SelectedRows.Count == 1)
            {
                kls_idshop = sql.obtenerCampoTabla("SELECT KLS_ID_SHOP FROM ARTICULO WHERE LTRIM(CODART)='" + reference + "'");

                if (kls_idshop != idproduct)
                {
                    errorSyncro = "Este artículo no está correctamente sincronizado, revisar los IDs";
                }

                articulo = sql.obtenerDatosSQLScript("SELECT TALLAS,CODFAMTALLAH,CODFAMTALLAV FROM ARTICULO WHERE LTRIM(CODART)='" + reference + "'");
                tallas = articulo.Rows[0]["TALLAS"].ToString();

                if (tallas == "T")
                {
                    familiaTallaV = articulo.Rows[0]["CODFAMTALLAV"].ToString().Trim();
                    familiaTallaH = articulo.Rows[0]["CODFAMTALLAH"].ToString().Trim();

                    numTallasV= sql.obtenerCampoTabla("SELECT COUNT(*) FROM TALLAS WHERE LTRIM(CODFAMTALLA)='" + familiaTallaV + "'");
                    numTallasH = sql.obtenerCampoTabla("SELECT COUNT(*) FROM TALLAS WHERE LTRIM(CODFAMTALLA)='" + familiaTallaH + "'");
                }
            }

            fichaArticulo = "FICHA ARTICULO: " + reference + "\n" +  "\n" +
                "Id en Prestashop: " + idproduct + "\n" +
                "Id en A3ERP: " + kls_idshop + "\n" +
                " -------------------- " + "\n" +
                "ARTICULO CON TALLAS: " + tallas + "\n" + "\n" +
                "FAMILIA TALLAS V: " + familiaTallaV + " (" + numTallasV + ")" + "\n" +
                "FAMILIA TALLAS H: " + familiaTallaH + " (" + numTallasH + ")" + "\n" + "\n" +
                errorSyncro;

            fichaArticulo.mb();


        }

        private void revisarTablasAtributosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csTallasYColoresV2 tyc = new klsync.csTallasYColoresV2();
           

            if ("Quieres revisar las tablas de atributos?\nSe verificará si hay nuevos grupos y atributos".what())
            {
                foreach (DataGridViewRow item in dgvDetalleLineas.SelectedRows)
                {
                    tyc.inicializarTallasYColores(item.Cells["product_id"].Value.ToString());
                }

                "Atributos verificados".mb();
            }
            else
            {
                "Operación cancelada".mb();
            }
        }

    }
}