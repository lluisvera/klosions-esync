﻿using klsync.Export;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync
{
    public partial class frExportDataToExcel : Form
    {
        public frExportDataToExcel()
        {
            InitializeComponent();
        }

        private void frExportDataToExcel_Load(object sender, EventArgs e)
        {
            cargarMarcas();
        }

        //FUNCION PARA CARGAR LAS MARCAS DISPONIBLES Y ACTIVAS
        private void cargarMarcas()
        {
            try
            {
                DataTable marcas = csManufacturerService.obtenerMarcas();
                cmbExportarDatosMarca.Items.Clear();

                foreach (DataRow row in marcas.Rows)
                {
                    cmbExportarDatosMarca.Items.Add(new csComboBoxItemExcelExport
                    {
                        Text = row["name"].ToString(),
                        Value = row["id_manufacturer"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al cargar las marcas: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //BOTÓN PARA EXPORTAR LOS DATOS DE LA MARCA SELECCIONADA A EXCEL
        private void btnExportDataToExcel_Click(object sender, EventArgs e)
        {
            if (cmbExportarDatosMarca.SelectedItem is csComboBoxItemExcelExport selectedItem)
            {
                try
                {
                    string nombreMarca = selectedItem.Text;

                    DataTable datosMarca = csManufacturerService.obtenerDatosDeMarca(nombreMarca);

                    string nombreArchivo = $"{selectedItem.Text}_Datos_{DateTime.Now:yyyyMMdd}.xlsx";
                    csExcelExportData.exportarDatos(datosMarca, nombreArchivo);

                    MessageBox.Show("Excel generado correctamente en 'Escritorio\\EXPORT_ESYNC'.", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error al generar el Excel: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Seleccione una marca válida.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
