﻿namespace klsync
{
    partial class frManufacturers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btInsertMarcasNuevas = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btLoadMarcasNuevas = new System.Windows.Forms.Button();
            this.dgvMarcasNuevas = new System.Windows.Forms.DataGridView();
            this.btExportToA3 = new System.Windows.Forms.Button();
            this.btLoadManufacturersPS = new System.Windows.Forms.Button();
            this.btLoadManufacturersA3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btExportToPS = new System.Windows.Forms.Button();
            this.dgvMarcasPS = new System.Windows.Forms.DataGridView();
            this.dgvMarcasA3 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btUpdateFabricantesArticulos = new System.Windows.Forms.Button();
            this.btLoadFabricantesArticulos = new System.Windows.Forms.Button();
            this.dgvArticulosFabricantes = new System.Windows.Forms.DataGridView();
            this.labelSeleccionarMarcas = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasNuevas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasA3)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFabricantes)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(22, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(445, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "MARCAS Y FABRICANTES";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 96);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1383, 651);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.labelSeleccionarMarcas);
            this.tabPage1.Controls.Add(this.btInsertMarcasNuevas);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btLoadMarcasNuevas);
            this.tabPage1.Controls.Add(this.dgvMarcasNuevas);
            this.tabPage1.Controls.Add(this.btExportToA3);
            this.tabPage1.Controls.Add(this.btLoadManufacturersPS);
            this.tabPage1.Controls.Add(this.btLoadManufacturersA3);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btExportToPS);
            this.tabPage1.Controls.Add(this.dgvMarcasPS);
            this.tabPage1.Controls.Add(this.dgvMarcasA3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1375, 625);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Fabricantes y Marcas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btInsertMarcasNuevas
            // 
            this.btInsertMarcasNuevas.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btInsertMarcasNuevas.Location = new System.Drawing.Point(550, 422);
            this.btInsertMarcasNuevas.Name = "btInsertMarcasNuevas";
            this.btInsertMarcasNuevas.Size = new System.Drawing.Size(120, 37);
            this.btInsertMarcasNuevas.TabIndex = 20;
            this.btInsertMarcasNuevas.Text = "> > > > > ";
            this.btInsertMarcasNuevas.UseVisualStyleBackColor = true;
            this.btInsertMarcasNuevas.Click += new System.EventHandler(this.btInsertMarcasNuevas_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 399);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "A3ERP NUEVAS MARCAS";
            // 
            // btLoadMarcasNuevas
            // 
            this.btLoadMarcasNuevas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btLoadMarcasNuevas.Location = new System.Drawing.Point(445, 392);
            this.btLoadMarcasNuevas.Name = "btLoadMarcasNuevas";
            this.btLoadMarcasNuevas.Size = new System.Drawing.Size(88, 23);
            this.btLoadMarcasNuevas.TabIndex = 18;
            this.btLoadMarcasNuevas.Text = "Cargar Datos";
            this.btLoadMarcasNuevas.UseVisualStyleBackColor = true;
            this.btLoadMarcasNuevas.Click += new System.EventHandler(this.btLoadMarcasNuevas_Click);
            // 
            // dgvMarcasNuevas
            // 
            this.dgvMarcasNuevas.AllowUserToAddRows = false;
            this.dgvMarcasNuevas.AllowUserToDeleteRows = false;
            this.dgvMarcasNuevas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvMarcasNuevas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMarcasNuevas.Location = new System.Drawing.Point(13, 422);
            this.dgvMarcasNuevas.Name = "dgvMarcasNuevas";
            this.dgvMarcasNuevas.ReadOnly = true;
            this.dgvMarcasNuevas.Size = new System.Drawing.Size(520, 175);
            this.dgvMarcasNuevas.TabIndex = 17;
            // 
            // btExportToA3
            // 
            this.btExportToA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btExportToA3.Location = new System.Drawing.Point(550, 196);
            this.btExportToA3.Name = "btExportToA3";
            this.btExportToA3.Size = new System.Drawing.Size(120, 36);
            this.btExportToA3.TabIndex = 16;
            this.btExportToA3.Text = "< < < < <";
            this.btExportToA3.UseVisualStyleBackColor = true;
            this.btExportToA3.Click += new System.EventHandler(this.btExportToA3_Click);
            // 
            // btLoadManufacturersPS
            // 
            this.btLoadManufacturersPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btLoadManufacturersPS.Location = new System.Drawing.Point(1254, 14);
            this.btLoadManufacturersPS.Name = "btLoadManufacturersPS";
            this.btLoadManufacturersPS.Size = new System.Drawing.Size(115, 23);
            this.btLoadManufacturersPS.TabIndex = 15;
            this.btLoadManufacturersPS.Text = "Cargar Datos";
            this.btLoadManufacturersPS.UseVisualStyleBackColor = true;
            this.btLoadManufacturersPS.Click += new System.EventHandler(this.btLoadManufacturersPS_Click);
            // 
            // btLoadManufacturersA3
            // 
            this.btLoadManufacturersA3.Location = new System.Drawing.Point(445, 14);
            this.btLoadManufacturersA3.Name = "btLoadManufacturersA3";
            this.btLoadManufacturersA3.Size = new System.Drawing.Size(88, 23);
            this.btLoadManufacturersA3.TabIndex = 14;
            this.btLoadManufacturersA3.Text = "Cargar Datos";
            this.btLoadManufacturersA3.UseVisualStyleBackColor = true;
            this.btLoadManufacturersA3.Click += new System.EventHandler(this.btLoadManufacturersA3_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(704, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "PRESTASHOP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "A3ERP";
            // 
            // btExportToPS
            // 
            this.btExportToPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btExportToPS.Location = new System.Drawing.Point(550, 84);
            this.btExportToPS.Name = "btExportToPS";
            this.btExportToPS.Size = new System.Drawing.Size(120, 37);
            this.btExportToPS.TabIndex = 11;
            this.btExportToPS.Text = "> > > > > ";
            this.btExportToPS.UseVisualStyleBackColor = true;
            this.btExportToPS.Click += new System.EventHandler(this.btExportToPS_Click);
            // 
            // dgvMarcasPS
            // 
            this.dgvMarcasPS.AllowUserToAddRows = false;
            this.dgvMarcasPS.AllowUserToDeleteRows = false;
            this.dgvMarcasPS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMarcasPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMarcasPS.Location = new System.Drawing.Point(685, 43);
            this.dgvMarcasPS.Name = "dgvMarcasPS";
            this.dgvMarcasPS.ReadOnly = true;
            this.dgvMarcasPS.Size = new System.Drawing.Size(684, 576);
            this.dgvMarcasPS.TabIndex = 10;
            // 
            // dgvMarcasA3
            // 
            this.dgvMarcasA3.AllowUserToAddRows = false;
            this.dgvMarcasA3.AllowUserToDeleteRows = false;
            this.dgvMarcasA3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvMarcasA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMarcasA3.Location = new System.Drawing.Point(13, 43);
            this.dgvMarcasA3.Name = "dgvMarcasA3";
            this.dgvMarcasA3.ReadOnly = true;
            this.dgvMarcasA3.Size = new System.Drawing.Size(520, 332);
            this.dgvMarcasA3.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btUpdateFabricantesArticulos);
            this.tabPage2.Controls.Add(this.btLoadFabricantesArticulos);
            this.tabPage2.Controls.Add(this.dgvArticulosFabricantes);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1375, 625);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Artículos por Fabricantes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btUpdateFabricantesArticulos
            // 
            this.btUpdateFabricantesArticulos.Location = new System.Drawing.Point(660, 307);
            this.btUpdateFabricantesArticulos.Name = "btUpdateFabricantesArticulos";
            this.btUpdateFabricantesArticulos.Size = new System.Drawing.Size(88, 23);
            this.btUpdateFabricantesArticulos.TabIndex = 9;
            this.btUpdateFabricantesArticulos.Text = "Actualizar Articulos";
            this.btUpdateFabricantesArticulos.UseVisualStyleBackColor = true;
            this.btUpdateFabricantesArticulos.Click += new System.EventHandler(this.btUpdateFabricantesArticulos_Click);
            // 
            // btLoadFabricantesArticulos
            // 
            this.btLoadFabricantesArticulos.Location = new System.Drawing.Point(6, 38);
            this.btLoadFabricantesArticulos.Name = "btLoadFabricantesArticulos";
            this.btLoadFabricantesArticulos.Size = new System.Drawing.Size(88, 23);
            this.btLoadFabricantesArticulos.TabIndex = 8;
            this.btLoadFabricantesArticulos.Text = "Cargar Datos";
            this.btLoadFabricantesArticulos.UseVisualStyleBackColor = true;
            this.btLoadFabricantesArticulos.Click += new System.EventHandler(this.btLoadFabricantesArticulos_Click);
            // 
            // dgvArticulosFabricantes
            // 
            this.dgvArticulosFabricantes.AllowUserToAddRows = false;
            this.dgvArticulosFabricantes.AllowUserToDeleteRows = false;
            this.dgvArticulosFabricantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosFabricantes.Location = new System.Drawing.Point(100, 38);
            this.dgvArticulosFabricantes.Name = "dgvArticulosFabricantes";
            this.dgvArticulosFabricantes.ReadOnly = true;
            this.dgvArticulosFabricantes.Size = new System.Drawing.Size(539, 292);
            this.dgvArticulosFabricantes.TabIndex = 0;
            // 
            // labelSeleccionarMarcas
            // 
            this.labelSeleccionarMarcas.AutoSize = true;
            this.labelSeleccionarMarcas.Location = new System.Drawing.Point(13, 605);
            this.labelSeleccionarMarcas.Name = "labelSeleccionarMarcas";
            this.labelSeleccionarMarcas.Size = new System.Drawing.Size(203, 13);
            this.labelSeleccionarMarcas.TabIndex = 21;
            this.labelSeleccionarMarcas.Text = "Selecciona las marcas que quieres enviar";
            // 
            // frManufacturers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1413, 759);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Name = "frManufacturers";
            this.Text = "Marcas y Fabricantes";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasNuevas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarcasA3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFabricantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btLoadFabricantesArticulos;
        private System.Windows.Forms.DataGridView dgvArticulosFabricantes;
        private System.Windows.Forms.Button btLoadManufacturersPS;
        private System.Windows.Forms.Button btLoadManufacturersA3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btExportToPS;
        private System.Windows.Forms.DataGridView dgvMarcasPS;
        private System.Windows.Forms.DataGridView dgvMarcasA3;
        private System.Windows.Forms.Button btUpdateFabricantesArticulos;
        private System.Windows.Forms.Button btExportToA3;
        private System.Windows.Forms.Button btLoadMarcasNuevas;
        private System.Windows.Forms.DataGridView dgvMarcasNuevas;
        private System.Windows.Forms.Button btInsertMarcasNuevas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelSeleccionarMarcas;
    }
}