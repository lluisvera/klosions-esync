﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace klsync
{
    public partial class frRepasatProyectos : Form
    {

        private static frRepasatProyectos m_FormDefInstance;
        public static frRepasatProyectos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRepasatProyectos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frRepasatProyectos()
        {
            InitializeComponent();
        }



        private void cargarProyectosRPST()
        {
            csSqlScriptRepasat csScriptRPST = new csSqlScriptRepasat();
            conectarDB(csScriptRPST.selectProyectosRPST(), false, dgvRepasatProyectos);
        }

        private void conectarDB(string sqlScript, bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();

                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);

                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {

                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }

                    }
                    numeroDocumentos = table.Rows.Count - contador;

                }


            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }



        //public string CadenaConexion()
        //{
        //    SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
        //    csb.DataSource = csGlobal.ServerA3;
        //    csb.InitialCatalog = csGlobal.databaseA3;
        //    csb.IntegratedSecurity = true;
        //    csb.UserID = csGlobal.userA3;
        //    csb.Password = csGlobal.passwordA3;
        //    return csb.ConnectionString;
        //}

        private void cargarProyectosA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectProyectosA3(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvRepasatProyectos.DataSource = t;


        }

        private void InsertarProyectos(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();

            foreach (DataGridViewRow fila in Filas)
            {
                mysqlconect.insertItems(scriptRepasat.insertProyectosRPST(fila.Cells["NOMPROYECTO"].Value.ToString(),fila.Cells["CODCLI"].Value.ToString(), fila.Cells["DIRCLI"].Value.ToString(),
                    fila.Cells["DTOCLI"].Value.ToString(), fila.Cells["POBCLI"].Value.ToString(),
                    fila.Cells["PROYECTO"].Value.ToString(), fila.Cells["TELCLI"].Value.ToString()));

            }
        }


        private void BorrarProyectos(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();

            foreach (DataGridViewRow fila in Filas)
            {
                mysqlconect.insertItems(scriptRepasat.borrarProyectosRPST(fila.Cells["idProyecto"].Value.ToString()));

            }

        }


        private void seleccionarTodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
                dgvRepasatProyectos.SelectAll();
        }

        private void exportarARPSTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertarProyectos(dgvRepasatProyectos.SelectedRows);
        }

        private void borrarProyectosToolStripMenuItem_Click(object sender, EventArgs e)
        {
                  BorrarProyectos(dgvRepasatProyectos.SelectedRows);
                            cargarProyectosRPST();
        }

        private void btCargarProyectosA3_Click(object sender, EventArgs e)
        {
            cargarProyectosA3();
        }

        private void btCargarProyectosRPST_Click(object sender, EventArgs e)
        {
            cargarProyectosRPST();
        }



    }
}
