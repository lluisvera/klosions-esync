﻿namespace klsync
{
    partial class frPaypal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripPaypal = new System.Windows.Forms.ToolStrip();
            this.dgvPaypal = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaypal)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripPaypal
            // 
            this.toolStripPaypal.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripPaypal.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripPaypal.Location = new System.Drawing.Point(0, 0);
            this.toolStripPaypal.Name = "toolStripPaypal";
            this.toolStripPaypal.Size = new System.Drawing.Size(1042, 25);
            this.toolStripPaypal.TabIndex = 0;
            this.toolStripPaypal.Text = "toolStrip1";
            // 
            // dgvPaypal
            // 
            this.dgvPaypal.AllowUserToAddRows = false;
            this.dgvPaypal.AllowUserToDeleteRows = false;
            this.dgvPaypal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPaypal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPaypal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPaypal.Location = new System.Drawing.Point(12, 66);
            this.dgvPaypal.Name = "dgvPaypal";
            this.dgvPaypal.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvPaypal.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPaypal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPaypal.Size = new System.Drawing.Size(1018, 547);
            this.dgvPaypal.TabIndex = 1;
            // 
            // frPaypal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 625);
            this.Controls.Add(this.dgvPaypal);
            this.Controls.Add(this.toolStripPaypal);
            this.Name = "frPaypal";
            this.Text = "Paypal";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaypal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripPaypal;
        private System.Windows.Forms.DataGridView dgvPaypal;
    }
}