﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.TBai
{
    class csTBai
    {

        csSqlConnects sql = new csSqlConnects();
        public void actualizarDocsRPSTDesdeA3(string tipoObjeto, DataGridView dgvRPST)
        {
            try
            {

                string RPST_serieDoc = "";
                string A3_serie = "";
                string numeroDocumento = "";
                string urlTicketBai = "";
                string idTicketBai = "";
                string idDocRPST = "";
                string RPST_external_doc = "";
                string messageUpdate = "";
                int contPreguntas = 0;
                DialogResult resultConfirmacion = new DialogResult();
                DialogResult result = new DialogResult();
                string messageConfirm = "¿Quieres omitir las confirmaciones?";
                string nomCampoExternoRPST = "";
                csRepasatWebService rpstWS = new csRepasatWebService();


                DataTable dtKeys = new DataTable();
                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");

                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");




                foreach (DataGridViewRow dgvRow in dgvRPST.SelectedRows)
                {

                    if (!dgvRPST.Columns.Contains("ticketbai[url]")) return;

                    RPST_serieDoc = dgvRow.Cells["RPST_ID_SERIE"].Value.ToString().Trim();
                    A3_serie = dgvRow.Cells["SERIE"].Value.ToString().Trim();
                    numeroDocumento = dgvRow.Cells["NUMDOC"].Value.ToString().Trim();
                    urlTicketBai = dgvRow.Cells["ticketbai[url]"].Value.ToString().Trim();
                    idTicketBai = dgvRow.Cells["ticketbai[idTbai]"].Value.ToString().Trim();
                    idDocRPST = dgvRow.Cells["REPASAT_ID_DOC"].Value.ToString().Trim();
                    RPST_external_doc = dgvRow.Cells["codExternoDocumento"].Value.ToString().Trim();

                    messageUpdate = "¿Quieres enlazar el documento: " + A3_serie + " / " + numeroDocumento + "       (Id Repasat:" + idDocRPST + ") ?";

                    contPreguntas++;
                    if (contPreguntas == 2)
                    {
                        resultConfirmacion = MessageBox.Show(messageConfirm, "Confirmación", MessageBoxButtons.YesNoCancel);
                        if (resultConfirmacion == DialogResult.Yes)
                        {
                            resultConfirmacion = DialogResult.Yes;
                        }
                    }

                    if (resultConfirmacion != DialogResult.Yes)
                    {
                        result = MessageBox.Show(messageUpdate, "Actualización", MessageBoxButtons.YesNoCancel);
                    }

                    if (result == DialogResult.Yes || resultConfirmacion == DialogResult.Yes)
                    {
                        dtKeys.Rows.Clear();
                        dtParams.Rows.Clear();

                        DataRow drKeys = dtKeys.NewRow();
                        drKeys["KEY"] = idDocRPST;
                        dtKeys.Rows.Add(drKeys);

                        DataRow drParams_url = dtParams.NewRow();
                        drParams_url["FIELD"] = "ticketbai[url]";
                        drParams_url["KEY"] = idDocRPST;
                        drParams_url["VALUE"] = urlTicketBai;
                        dtParams.Rows.Add(drParams_url);

                        DataRow drParams_idTbai = dtParams.NewRow();
                        drParams_idTbai["FIELD"] = "ticketbai[idTbai]";
                        drParams_idTbai["KEY"] = idDocRPST;
                        drParams_idTbai["VALUE"] = idTicketBai;
                        dtParams.Rows.Add(drParams_idTbai);

                        DataRow drParams_estadoTbai = dtParams.NewRow();
                        drParams_estadoTbai["FIELD"] = "ticketbai[estado]";
                        drParams_estadoTbai["KEY"] = idDocRPST;
                        drParams_estadoTbai["VALUE"] = "OK";
                        dtParams.Rows.Add(drParams_estadoTbai);

                        DataRow drParams_numDoc = dtParams.NewRow();
                        drParams_numDoc["FIELD"] = "numSerieDocumento";
                        drParams_numDoc["KEY"] = idDocRPST;
                        drParams_numDoc["VALUE"] = numeroDocumento;
                        dtParams.Rows.Add(drParams_numDoc);

                        DataRow drParams_idSerie = dtParams.NewRow();
                        drParams_idSerie["FIELD"] = "idSerie";
                        drParams_idSerie["KEY"] = idDocRPST;
                        drParams_idSerie["VALUE"] = RPST_serieDoc;
                        dtParams.Rows.Add(drParams_idSerie);

                        DataRow drParams_codExternoDoc = dtParams.NewRow();
                        drParams_codExternoDoc["FIELD"] = "codExternoDocumento";
                        drParams_codExternoDoc["KEY"] = idDocRPST;
                        drParams_codExternoDoc["VALUE"] = RPST_external_doc;
                        dtParams.Rows.Add(drParams_codExternoDoc);
                       
                        rpstWS.sincronizarObjetoRepasat(tipoObjeto, "PUT", dtKeys, dtParams, idDocRPST,false,false, "updatetbai");
                    }
                }

            }
            catch (Exception ex)
            {
                ex.Message.mb();
            }
        }

        public DataTable facturasTbaiToRpst(string filtroFechaIni,string filtroFechaFin, DataTable dtFacturasFromRPST)
        {
            try
            {
                string queryFrasA3 = "SELECT 'NO ACTUALIZADA' AS SITUACION, cast(IDFACV as int) as codExternoDocumento,  CABEFACV.SERIE, " +
                   " cast(NUMDOC as int) as NUMDOC, CABEFACV.FECHA, SERIES.RPST_ID_SERIE, " +
                   " RPST_ID_FACV AS REPASAT_ID_DOC , BORRADOR, SERIEORIGINALFACTURABORRADOR, RAZON, TBAI_FACTURA.ID, DATOSQR AS 'ticketbai[url]'," +
                   " SUBSTRING(DATOSQR, CHARINDEX('&s=',DATOSQR)+3, CHARINDEX('&nf=',DATOSQR)-CHARINDEX('&s=',DATOSQR)-3 ) AS SERIE_QR," +
                   " SUBSTRING(DATOSQR, CHARINDEX('&nf',DATOSQR)+4, CHARINDEX('&i=',DATOSQR)-CHARINDEX('&nf',DATOSQR)-4 ) AS NUMFAC_QR," +
                   " IDRESPUESTA, REGISTRADO, CABEFACV.FECHAHORAENVIO, IDTBAI  AS 'ticketbai[idTbai]' " +
                   " FROM CABEFACV  " +
                   " INNER JOIN TBAI_FACTURA " +
                   " ON  (CABEFACV.SERIE = TBAI_FACTURA.FACTURASERIE OR CABEFACV.SERIE IS NULL AND TBAI_FACTURA.FACTURASERIE IS NULL) " +
                   " AND CABEFACV.NUMDOC = TBAI_FACTURA.FACTURANUMDOC  " +
                   " INNER JOIN TBAI_FACTURAV ON TBAI_FACTURAV.ID = TBAI_FACTURA.ID LEFT JOIN SERIES ON CABEFACV.SERIE=SERIES.SERIE " +
                   " WHERE FECHA between '" + filtroFechaIni + "' and '" + filtroFechaFin + "'";

                DataTable dtFacturasTbaiToRpst = sql.obtenerDatosSQLScript(queryFrasA3);

                string A3_Serie = "";
                string A3_NumDoc = "";
                string A3_idFacV = "";
                string A3_RPST_IdFac = "";
                string RPST_Serie = "";
                string RPST_Numdoc = "";
                string RPST_DocumentID = "";
                string RPST_ExternalNumDoc = "";
                string RPST_EstadoDocTBai = "";
                bool A3_Fra_Registrada = false;

                if (dtFacturasTbaiToRpst.Rows.Count > 0)
                {

                    foreach (DataRow fila in dtFacturasTbaiToRpst.Rows)
                    {

                        RPST_Serie = string.Empty;
                        RPST_Numdoc = string.Empty;
                        RPST_DocumentID = string.Empty;
                        RPST_ExternalNumDoc = string.Empty;
                        RPST_EstadoDocTBai = string.Empty;


                        A3_Serie = fila["SERIE"].ToString().Trim();
                        A3_NumDoc = fila["NUMDOC"].ToString();
                        A3_idFacV = fila["codExternoDocumento"].ToString();
                        A3_RPST_IdFac = fila["REPASAT_ID_DOC"].ToString();
                        A3_Fra_Registrada = fila["REGISTRADO"].ToString().ToUpper()== "TRUE" ? true : false;


                        foreach (DataRow filaRPSTDoc in dtFacturasFromRPST.Rows)
                        {

                            RPST_Serie = filaRPSTDoc["SERIE"].ToString().Trim();
                            RPST_Numdoc = filaRPSTDoc["NUM_FRA"].ToString();
                            RPST_DocumentID = filaRPSTDoc["REPASAT_ID_DOC"].ToString();
                            RPST_ExternalNumDoc = filaRPSTDoc["EXTERNALID"].ToString();
                            RPST_EstadoDocTBai = filaRPSTDoc["TICKETBAI"].ToString();

                            if (A3_Serie == RPST_Serie && A3_NumDoc == RPST_Numdoc && A3_idFacV == RPST_ExternalNumDoc && A3_RPST_IdFac == RPST_DocumentID)
                            {

                                fila["SITUACION"] = (!A3_Fra_Registrada? "**REVISAR**":"") + RPST_EstadoDocTBai.ToString() + " (RPST)";
                                continue;
                            }
                        }

                        //dtFacturasTbaiToRpst.Rows[fila.ItemArray]["SERIE"]
                    }
                }
                return dtFacturasTbaiToRpst;

            }
            catch (Exception ex) {
                return null;
            }
        }





    }
}
