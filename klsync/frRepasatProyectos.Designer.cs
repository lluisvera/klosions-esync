﻿namespace klsync
{
    partial class frRepasatProyectos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvRepasatProyectos = new System.Windows.Forms.DataGridView();
            this.btCargarProyectosRPST = new System.Windows.Forms.Button();
            this.MenuContextual1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btCargarProyectosA3 = new System.Windows.Forms.Button();
            this.seleccionarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarARPSTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarProyectosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepasatProyectos)).BeginInit();
            this.MenuContextual1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvRepasatProyectos
            // 
            this.dgvRepasatProyectos.AllowUserToAddRows = false;
            this.dgvRepasatProyectos.AllowUserToDeleteRows = false;
            this.dgvRepasatProyectos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRepasatProyectos.ContextMenuStrip = this.MenuContextual1;
            this.dgvRepasatProyectos.Location = new System.Drawing.Point(12, 149);
            this.dgvRepasatProyectos.Name = "dgvRepasatProyectos";
            this.dgvRepasatProyectos.ReadOnly = true;
            this.dgvRepasatProyectos.Size = new System.Drawing.Size(1062, 367);
            this.dgvRepasatProyectos.TabIndex = 0;
            // 
            // btCargarProyectosRPST
            // 
            this.btCargarProyectosRPST.Location = new System.Drawing.Point(12, 109);
            this.btCargarProyectosRPST.Name = "btCargarProyectosRPST";
            this.btCargarProyectosRPST.Size = new System.Drawing.Size(120, 34);
            this.btCargarProyectosRPST.TabIndex = 1;
            this.btCargarProyectosRPST.Text = "Cargar Proyectos REPASAT";
            this.btCargarProyectosRPST.UseVisualStyleBackColor = true;
            this.btCargarProyectosRPST.Click += new System.EventHandler(this.btCargarProyectosRPST_Click);
            // 
            // MenuContextual1
            // 
            this.MenuContextual1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seleccionarTodoToolStripMenuItem,
            this.exportarARPSTToolStripMenuItem,
            this.borrarProyectosToolStripMenuItem});
            this.MenuContextual1.Name = "MenuContextual1";
            this.MenuContextual1.Size = new System.Drawing.Size(166, 70);
            // 
            // btCargarProyectosA3
            // 
            this.btCargarProyectosA3.Location = new System.Drawing.Point(138, 109);
            this.btCargarProyectosA3.Name = "btCargarProyectosA3";
            this.btCargarProyectosA3.Size = new System.Drawing.Size(120, 34);
            this.btCargarProyectosA3.TabIndex = 3;
            this.btCargarProyectosA3.Text = "Cargar Proyectos A3";
            this.btCargarProyectosA3.UseVisualStyleBackColor = true;
            this.btCargarProyectosA3.Click += new System.EventHandler(this.btCargarProyectosA3_Click);
            // 
            // seleccionarTodoToolStripMenuItem
            // 
            this.seleccionarTodoToolStripMenuItem.Name = "seleccionarTodoToolStripMenuItem";
            this.seleccionarTodoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.seleccionarTodoToolStripMenuItem.Text = "Seleccionar &Todo";
            this.seleccionarTodoToolStripMenuItem.Click += new System.EventHandler(this.seleccionarTodoToolStripMenuItem_Click);
            // 
            // exportarARPSTToolStripMenuItem
            // 
            this.exportarARPSTToolStripMenuItem.Name = "exportarARPSTToolStripMenuItem";
            this.exportarARPSTToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exportarARPSTToolStripMenuItem.Text = "&Exportar a RPST";
            this.exportarARPSTToolStripMenuItem.Click += new System.EventHandler(this.exportarARPSTToolStripMenuItem_Click);
            // 
            // borrarProyectosToolStripMenuItem
            // 
            this.borrarProyectosToolStripMenuItem.Name = "borrarProyectosToolStripMenuItem";
            this.borrarProyectosToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.borrarProyectosToolStripMenuItem.Text = "&Borrar Proyectos";
            this.borrarProyectosToolStripMenuItem.Click += new System.EventHandler(this.borrarProyectosToolStripMenuItem_Click);
            // 
            // frRepasatProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1086, 528);
            this.Controls.Add(this.btCargarProyectosA3);
            this.Controls.Add(this.btCargarProyectosRPST);
            this.Controls.Add(this.dgvRepasatProyectos);
            this.Name = "frRepasatProyectos";
            this.Text = "frRepasatProyectos";
            ((System.ComponentModel.ISupportInitialize)(this.dgvRepasatProyectos)).EndInit();
            this.MenuContextual1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRepasatProyectos;
        private System.Windows.Forms.Button btCargarProyectosRPST;
        private System.Windows.Forms.ContextMenuStrip MenuContextual1;
        private System.Windows.Forms.Button btCargarProyectosA3;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarARPSTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarProyectosToolStripMenuItem;
    }
}