﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frPesoBastide : Form
    {
        public frPesoBastide()
        {
            InitializeComponent();
        }

        private void tsb_Click(object sender, EventArgs e)
        {
            cargarPesos();
        }

        private void cargarPesos(string busqueda = "")
        {
            string sb = "select distinct ps_product_attribute.id_product_attribute, ps_product_attribute.id_product, ps_product.reference, ps_attribute_lang.name, ps_product_attribute.weight from ps_product_attribute " +
                "left join ps_product_attribute_combination on ps_product_attribute_combination.id_product_attribute = ps_product_attribute.id_product_attribute " +
                "left join ps_attribute_lang on ps_attribute_lang.id_attribute = ps_product_attribute_combination.id_attribute " +
                "left join ps_product on ps_product.id_product = ps_product_attribute.id_product " + busqueda;

            csMySqlConnect mysql = new csMySqlConnect();

            csUtilidades.addDataSource(dgv, mysql.cargarTabla(sb));
        }

        private void actualizarPesoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    if (dgv.SelectedRows.Count > 0)
                    {
                        string numSelectedRows = dgv.SelectedRows.Count.ToString(); ;
                        if (MessageBox.Show("Vas a actualizar el peso de " + numSelectedRows + " atributo/s, estás seguro/a?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            frDialogInput input = new frDialogInput("Introduce el peso", "");
                            if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                foreach (DataGridViewRow item in dgv.SelectedRows)
                                {
                                    string id_product_attribute = item.Cells["id_product_attribute"].Value.ToString();
                                    string consulta = string.Format("update ps_product_attribute set weight = {0} where id_product_attribute = {1}", csGlobal.comboFormValue, id_product_attribute);
                                    csUtilidades.ejecutarConsulta(consulta, true);
                                }

                                "Pesos actualizados".mb();
                                cargarPesos();
                            }
                            else
                            {
                                "Operacion cancelada".mb();
                            }                            
                        }
                        else
                        {
                            "Operacion cancelada".mb();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                "Ha ocurrido un error al actualizar los pesos".mb();
            }
        }

        private void tsbBuscar_Click(object sender, EventArgs e)
        {
            frDialogInput input = new frDialogInput("Referencia ", "");
            
            if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cargarPesos(string.Format(" where ps_product.reference = '{0}'", csGlobal.comboFormValue));
            }
            else
            {
                "Operacion cancelada".mb();
            }
        }
    }
}
