﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Threading;
using System.IO.Compression;
using RestSharp;
using System.ComponentModel;
using System.Web;
using RestSharp.Extensions.MonoHttp;
using HttpUtility = System.Web.HttpUtility;
using klsync.SQL;
using System.Net.Sockets;
using System.Reflection;
using System.Collections;
using RestSharp.Extensions;

namespace klsync
{
    public static class csUtilidades
    {
        static Utilidades.csConexiones dbConnectEsync = new Utilidades.csConexiones();



        //Función para ver si estamos dentro de Klosions
        public static bool isKlosionsNetwork()
        {


            //string externalip = new WebClient().DownloadString("http://icanhazip.com").Replace("\n", "");

            //string externalip = csIpFinder.IpFinder.GetExternalIp().ToString();

            //string externalip = csIpFinder.IpFinder.

            //if (externalip == csGlobal.ipKlosions)
            //{
            //    return true;
            //}

            return false;
        }



        //Gestión de Log de Errores
        public static void log(this string txt, bool forzar = false)
        {
            try
            {
                if (csGlobal.log == "1" || forzar || csGlobal.modeDebug)
                {
                    txt = txt.Replace("'", "''");
                    string query = string.Format("insert into log (user, message) values ('{0}', '{1}')", csGlobal.nombreServidor, txt.ToUpper());
                    csUtilidades.ConsultaMySQL(dbConnectEsync.cadenaConexionEsyncDB(), query);
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    ex.Message.mb();
                }
            }
        }

        public static void registrarLogError(string errorMessage, string nombreFuncion)
        {
            string metodo = "";
            string txt = "";

            if (csGlobal.modeDebug)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(1);

                metodo = sf.GetMethod().Name;
                txt = metodo + "|" + nombreFuncion + "|" + errorMessage;
                log(txt);
            }
        }


        //Funciona para guardar en base de datos si se ha realizado un proceso
        public static void registroProceso(string mensaje, string nombreFuncion)
        {
            string metodo = "";
            string txt = "";

            if (csGlobal.modeDebug)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(1);

                metodo = sf.GetMethod().Name;
                txt = metodo + "|" + nombreFuncion + "| Validado: "  + mensaje;
                log(txt);
            }

        }


        public static string getDesktopPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        public static string getProvincia(string p)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            string codprovi = "";
            if (p != "")
            {
                string iso_code_ps = mysql.obtenerDatoFromQuery(string.Format("select iso_code from ps_state where id_state = {0}", p));
                if (iso_code_ps != "")
                {
                    codprovi = sql.obtenerCampoTabla(string.Format("select codprovi from provinci where kls_isocode = '{0}'", iso_code_ps));
                }
            }

            return (codprovi == "") ? string.Empty : codprovi;
        }

        public static string getPais(string p)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            string codpais = "";

            if (p != "")
            {
                string iso_code_ps = mysql.obtenerDatoFromQuery(string.Format("select iso_code from ps_country where id_country = {0}", p));
                if (iso_code_ps != "")
                {
                    codpais = sql.obtenerCampoTabla(string.Format("select codpais from paises where codigonif = '{0}'", iso_code_ps));
                }
            }

            return (codpais == "") ? string.Empty : codpais;
        }


            //función para codificar los valores que se enviarán vía POST 
            //corrige las ñ, acentos, etc.
            public static string encodeUrl2TPST(string texto)
            {           
                texto= HttpUtility.UrlEncode(texto, Encoding.UTF8);
                return texto;
            }


            /*public static void zipping(string outputName, List<string> files)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        foreach (string file in files)
                        {
                            zip.AddFile(file);
                        }

                        zip.Save(outputName);
                    }
                }
                catch (System.Exception ex)
                {
                    System.Console.Error.WriteLine("Error: " + ex);
                }
            }*/

        public static string separarPorGuiones(string text)
        {

            return text.Replace(" ", "-").Replace("'", "-").Replace("/", "-").Replace("(", "-").Replace(")", "-").ToLower();
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string selectFilesFromFolder()
        {
            string final = "";

            var x = Directory.GetFiles(csGlobal.rutaImagenes, "*.jpg", SearchOption.TopDirectoryOnly).OrderBy(f => f);

            foreach (var f in x)
            {
                if (f.Contains('_'))
                {
                    final += "'" + Path.GetFileName(Convert.ToString(f)).Replace(".jpg", "").Replace(".jpeg", "").Split('_')[0] + "',";
                }
                else if (f.Contains('-'))
                {
                    final += "'" + Path.GetFileName(Convert.ToString(f)).Replace(".jpg", "").Replace(".jpeg", "").Split('-')[0] + "',";
                }
                else
                {
                    final += "'" + Path.GetFileName(Convert.ToString(f)).Replace(".jpg", "").Replace(".jpeg", "") + "',";
                }
            }

            final = final.TrimEnd(',');

            return final;
        }

        public static void goToWeb(string url)
        {
            System.Diagnostics.Process.Start(url);
        }

        /// <summary>
        /// Comparar dos fechas dado un decalaje de tiempo  
        /// </summary>
        /// <param name="dt">DateTime</param>
        /// <param name="minutos">Decalaje</param>
        /// <returns>true si la diferencia está dentro del decalaje</returns>
        public static bool differenceTwoDateTimes(DateTime dt, int minutos = 5)
        {
            bool result = ((DateTime.UtcNow - dt).TotalMinutes < minutos);

            return result;
        }

        public static string idIdiomaDefaultPS()
        {
            return "(Select value from ps_configuration where name='PS_LANG_DEFAULT')";
        }

        public static bool verificarDt(DataTable dt)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static string getAlmacenWhereIn()
        {
            return "'" + csGlobal.almacenA3.Replace(",", "','") + "'";
        }

        public static void operacionCancelada()
        {
            "Operacion cancelada".mb();
        }

        public static string linkRewrite(this string text)
        {
            return text.Replace("&", "&amp;").Replace("<", "").Replace(">", "").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("/", "-").Replace("#", "").Replace("Ø", "").Replace("=", "").Replace(" ", "-");
        }

        public enum ImageFormat
        {
            bmp,
            jpeg,
            gif,
            tiff,
            png,
            unknown
        }

        public const int DELETE = 1;
        public const int INSERT = 2;
        public const int UPDATE = 3;
        public const bool MYSQL = true;
        public const bool SQL = false;

        public static void resizeAndUploadImage(string rutaFichero, string nuevoNombre, int anchoNuevo, int altoNuevo)
        {
            // Redimensionar imagen


            // Subir fichero FTP
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(csGlobal.userFTP, csGlobal.passwordFTP);
                client.UploadFile(nuevoNombre, "STOR", rutaFichero);
            }
        }

        /// <summary>
        /// Recorre un List para actualizar los valores
        /// </summary>
        /// <param name="table">Nombre de la tabla</param>
        /// <param name="values">El valor del set que va todo en el List</param>
        /// <param name="MySQL">True si es mysql, false si es SQL server</param>
        public static void UpdateDataBaseWithList(string table, List<string> values, bool MySQL = true)
        {
            string consulta = "";
            foreach (string value in values)
            {
                consulta = "update " + table + " set " + value;
                ejecutarConsulta(consulta, MySQL);
            }
        }

        /// <summary>
        /// Recorre un List de strings con los valores del insert   
        /// </summary>
        /// <param name="table">Nombre de la tabla</param>
        /// <param name="fields">Campos del insert con paréntesis</param>
        /// <param name="values">El List con los valores: ejemplo (0,1,1,'PRUEBA',1)</param>
        /// <param name="MySQL">True si es mysql, false si es SQL server</param>
        /// <param name="quitarRepes">De momento desactivado</param>
        //public static void WriteListToDatabase(string table, string fields, List<string> values, bool MySQL = true, bool quitarRepes = false, bool boolValues = true)
        //{
        //    try
        //    {
        //        string consulta = "";
        //        string valueFinal = "";
        //        int contador = 0;

        //        if (values.Count > 0)
        //        {
        //            int last = values.Count - 1;
        //            if (quitarRepes)
        //            {
        //                values = values.Distinct().ToList();
        //            }

        //            foreach (string value in values)
        //            {
        //                //RAUL 29/09/2020  Modifico la ubicacion del valueFinal ya que la estar al final del codigo y dentro de un else, el ultimo valor no se esta añadiendo en la consulta.
        //                valueFinal += value + ((contador == 500 || contador == last) ? "" : ",");
        //                if (contador >= 500)
        //                {
        //                    consulta = "INSERT into " + table + " " + fields + ((boolValues) ? " values " : " ") + valueFinal;
        //                    consulta = consulta.TrimEnd(',');
        //                    ejecutarConsulta(consulta, MySQL);

        //                    valueFinal = "";
        //                    contador = 0;
        //                }
        //                //else
        //                //{
        //                //    valueFinal += value + ((contador == 500||contador==last) ? "" : ",");
        //                //}

        //                contador++;
        //            }


        //            if (valueFinal != "")
        //            {
        //                consulta = "INSERT into " + table + " " + fields + ((boolValues) ? " values " : " ") + valueFinal;
        //                consulta = consulta.TrimEnd(',');
        //                ejecutarConsulta(consulta, MySQL);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if(csGlobal.modoManual)
        //            MessageBox.Show(ex.Message);
        //    }
        //}

        public static void WriteListToDatabase(string table, string fields, List<string> values, bool MySQL = true, bool quitarRepes = false, bool boolValues = true)
        {
            try
            {
                if (values.Count > 0)
                {
                    if (quitarRepes)
                    {
                        values = values.Distinct().ToList();
                    }

                    string valueFinal = "";
                    int contador = 0;
                    int last = values.Count - 1;

                    foreach (string value in values)
                    {
                        valueFinal += value + ((contador == 500 || contador == last) ? "" : ",");

                        if (contador >= 500)
                        {
                            string consulta = "INSERT INTO " + table + " " + fields + ((boolValues) ? " VALUES " : " ") + valueFinal;
                            consulta = consulta.TrimEnd(',');
                            ConexionSingleton.ObtenerInstancia().ejecutarConsulta(consulta, MySQL, true);

                            valueFinal = "";
                            contador = 0;
                        }

                        contador++;
                    }

                    if (!string.IsNullOrEmpty(valueFinal))
                    {
                        string consulta = "INSERT INTO " + table + " " + fields + ((boolValues) ? " VALUES " : " ") + valueFinal;
                        consulta = consulta.TrimEnd(',');

                        ConexionSingleton.ObtenerInstancia().ejecutarConsulta(consulta, MySQL, true);
                    }
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }




        public static void WriteListToDatabaseV2(string table, string fields, List<string> values, bool MySQL = true)
        {
            try
            {
                string consulta = "";
                string valueFinal = "";

                if (values.Count > 0)
                {
                    string last = values[values.Count - 1];

                    foreach (string value in values)
                    {
                        if (value != last)
                        {
                            valueFinal += value + ",";
                        }
                    }

                    consulta = "INSERT into " + table + " " + fields + " values " + valueFinal;
                    ejecutarConsulta(consulta, MySQL);
                }
            }
            catch (Exception ex)
            { }

        }

        public static DataTable cargarCSV()
        {
            DataTable dt = null;
            Stream myStream = null;
            System.Windows.Forms.OpenFileDialog theDialog = new System.Windows.Forms.OpenFileDialog();
            theDialog.Title = "Abrir archivo";
            theDialog.Filter = "CSV Files|*.csv*";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = theDialog.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            dt = ConvertCSVtoDataTable(theDialog.FileName);
                        }
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("Cierra el documento para importar el csv", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception)
                {
                }
            }

            return dt;
        }

        public static void codificarImagenesPrestashop()
        {
            try
            {
                var sql = new csSqlConnects();
                var mysql = new csMySqlConnect();
                // Obtenemos todos los productos que estan en la tienda en un datatable
                var consulta_productos =
                    "select ps_image.id_image AS IDIMAGE, reference as CODART, ps_product.id_product as IDPRODUCT from ps_product left join ps_image on ps_image.id_product = ps_product.id_product WHERE reference is not null";
                // deprecated
                //string consulta_productos = "select ltrim(codart) as CODART, KLS_ID_SHOP as IDPRODUCT from ARTICULO WHERE KLS_ID_SHOP IS NOT NULL";
                var dt = mysql.cargarTabla(consulta_productos);

                if (dt.Rows.Count > 0)
                {
                    int value;
                    var codart = "";
                    DataRow[] dr = null;
                    var fbd = new FolderBrowserDialog();
                    var fbd2 = new FolderBrowserDialog();

                    MessageBox.Show("Selecciona el origen de las imagenes");
                    var result = fbd.ShowDialog();
                    MessageBox.Show("Selecciona donde quieres que se guarden las imagenes codificadas");
                    var result2 = fbd2.ShowDialog();

                    // Seleccionamos donde estan las imagenes y donde queremos guardar las nuevas codificadas
                    if (!string.IsNullOrEmpty(fbd.SelectedPath) && !string.IsNullOrEmpty(fbd2.SelectedPath))
                    {
                        string[] files = Directory.GetFiles(fbd.SelectedPath, "*.jpg", SearchOption.AllDirectories);

                        foreach (string file in files)
                        {
                            try
                            {
                                var neatFile = Path.GetFileName(file).Replace(".jpg", "").Replace(".jpeg", "");

                                // Si no se consigue parsear es que no es un idproduct (ejemplo 289-bis)
                                if (int.TryParse(neatFile, out value))
                                {
                                    dr = dt.Select("IDIMAGE = " + Convert.ToInt32(neatFile));
                                    if (dr.Length > 0)
                                    {
                                        codart = dr[0]["CODART"].ToString();
                                        //codart = codart.Replace("-", "_").Replace("/", "_");
                                        string archivo = fbd2.SelectedPath + "\\" + codart + ".jpg";

                                        // Si no existe se crea
                                        if (!File.Exists(archivo))
                                        {
                                            File.Copy(file, archivo, true);
                                            continue;
                                        }
                                        else
                                        {
                                            string[] archivos = Directory.GetFiles(fbd2.SelectedPath, codart + "*", SearchOption.AllDirectories);
                                            string final = "";
                                            for (int i = 0; i < archivos.Length; i++)
                                            {
                                                string ult = archivos.First().ToString();

                                                if (archivos[i].ToString().Contains("_"))
                                                {
                                                    int nuevo = Convert.ToInt32(archivos[i].Split('_')[1].Replace(".jpg", "").Replace(".jpeg", ""));
                                                    final = fbd2.SelectedPath + "\\" + codart + "_" + (nuevo + 1).ToString() + ".jpg";
                                                }
                                                else
                                                {
                                                    final = archivos[i].Replace(".jpg", "_" + (i + 1) + ".jpg").Replace(".jpeg", "-" + (i + 1) + "jpeg");
                                                }

                                                if (!File.Exists(final))
                                                {
                                                    File.Copy(file, final, false);
                                                }

                                                continue;
                                            }
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                //Con esta funcion, se guardará la referencia de las imagenes que no se codifiquen correctamente.
                                //Pero el programa continuará.
                                string path = fbd2.SelectedPath+"\\"+"Error.txt";
                                if (!File.Exists(path))
                                {
                                    using (StreamWriter error = File.CreateText(path))
                                    {
                                        error.WriteLine(codart + "  ->  " + ex.Message);
                                    }
                                }
                                else
                                {
                                    using (StreamWriter error = new StreamWriter(path, true))
                                    {
                                        error.WriteLine(codart + "  ->  " + ex.Message);
                                    }
                                }
                                continue; 
                            }
                        }

                        MessageBox.Show("La operación ha terminado satisfactoriamente");
                    }
                    else
                    {
                        MessageBox.Show("Debes seleccionar un origen y un destino para que esto funcione");
                    }
                }
                else
                {
                    MessageBox.Show("No hay artículos sincronizados en la tienda");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo salió mal :( \n\n Error: " + ex.Message);
            }
        }

        public static bool tieneCodigoExternoInformado(string codigo)
        {
            if ((codigo != "") && (codigo != "0"))
                return true;
            return false;
        }

        public static string getCodartByKlsIdShop(string kls_id_shop)
        {
            string codart = "";
            if (kls_id_shop != "")
            {
                csSqlConnects sql = new csSqlConnects();
                codart = sql.obtenerCampoTabla("select codart from articulo where kls_id_shop = '" + kls_id_shop + "'");
            }

            return codart;
        }

        public static string getKlsIdShopByCodart(string codart)
        {
            string kls_id_shop = "";
            if ((codart != "") && (codart != "0"))
            {
                csSqlConnects sql = new csSqlConnects();
                codart = sql.obtenerCampoTabla("select kls_id_shop from articulo where ltrim(codart) = '" + codart + "'");
            }

            return kls_id_shop;
        }

        public static string resolverPadRight(string word, int size)
        {
            if (word.Length > size)
                return word.Substring(0, size);
            return word;
        }

        public static string descriptionShort(string word, int size)
        {
            if (word.Length > size)
            {
               // Esto esta mal y duplica la descripcion corta.
                word = word.Substring(0, size);
                word += word + " (...)";
            }

            return word;
        }

        public static string cleanPhone(string phone)
        {
            var digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(phone, "");
        }

        public static void actualizarProductoPS(string codart, string id_product)
        {
            ejecutarConsulta("update ps_product set reference = '" + codart + "' where id_product = " + id_product,true);

            ejecutarConsulta(
                "update ps_order_detail set product_reference = '" + codart + "' where product_id = " + id_product +
                " and product_attribute_id = 0", true);
        }

        public static bool isAnAddressFromSpain(string id_address_invoice)
        {
            var mysql = new csMySqlConnect();

            var country =
                mysql.obtenerDatoFromQuery("select id_country from ps_address where id_address = " + id_address_invoice);

            if (Convert.ToInt32(country) == 6)
                return true;
            return false;
        }

        public static string obtenerProvinciaEnBaseAlCodigoPostal(string zipcode)
        {
            var retorno = "";
            try
            {
                var sql = new csSqlConnects();
                var check = sql.cargarDatosTablaA3("select * from provinci");
                var zipi = zipcode.Substring(0, 2);
                var lenCheck = check.Select("CODPROVI like '%" + zipi + "%'").Length;
                if (lenCheck > 0)
                {
                    retorno =
                        sql.obtenerCampoTabla("select LTRIM(CODPROVI) from provinci where ltrim(CODPROVI) like '" +
                                              zipcode.Substring(0, 2) + "%'");

                    return retorno;
                }
                return retorno;
            }
            catch (Exception ex)
            {
            }

            return retorno;
        }

        public static string quitarAcentos(string word)
        {
            return Encoding.ASCII.GetString(Encoding.GetEncoding(1251).GetBytes(word));
        }

        public static void addDataSource(DataGridView dgv, BindingSource YOUR_BINDING_SOURCE)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke(new MethodInvoker(() => dgv.DataSource = YOUR_BINDING_SOURCE));
            else
                dgv.DataSource = YOUR_BINDING_SOURCE;
        }

        public static void addDataSource(DataGridView dgv, DataTable YOUR_DATASOURCE)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke(new MethodInvoker(() => dgv.DataSource = YOUR_DATASOURCE));
            else
                dgv.DataSource = YOUR_DATASOURCE;
        }

        public static bool existeColumnaSQL(string tabla, string columna)
        {
            var sql = new csSqlConnects();

            if (
                sql.consultaExiste("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tabla +
                                   "' AND COLUMN_NAME = '" + columna + "'"))
                return true;
            return false;
        }

        public static bool existeRegistroSQL(string query)
        {
            var sql = new csSqlConnects();

            if (sql.consultaExiste(query))
                return true;
            return false;
        }

        public static string subString(string word, int size = 0)
        {
            if (word.Length > 30)
                return word.Substring(0, size);
            return word;
        }

        public static bool emailIsValid(string email)
        {
            try
            {
                var m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        ///     XML
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static DataTable XmlDocument2DataTable(XmlDocument xml)
        {
            XmlReader xmlReader = new XmlNodeReader(xml);
            var dataSet = new DataSet();
            dataSet.ReadXml(xmlReader);
            return dataSet.Tables[0];
        }

        /// <summary>
        ///     XML
        /// </summary>
        /// <param name="xDocument"></param>
        /// <returns></returns>
        public static XmlDocument ToXmlDocument(this XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        public static void salirDelPrograma()
        {
            if (Application.MessageLoop)
                Application.Exit();
            else
                Environment.Exit(1);
        }

        //Verificación de conexión a MYSQL
        public static bool check_MySQL_DB()
        {
            OdbcConnection connODBC = new OdbcConnection("DSN=ps_shop");
            string verificaciones = "";


            verificaciones = "Verificar IP " + csGlobal.ipKlosions + "\n" +
                "Verificar IP de Klosions " + "\n" +
                "Verificar si el Hosting permite la IP de conexión " + "\n" +
                "Verificar Servidor del Cliente " + "\n" +
                "Verificar Usuario y Password de conexión";

            if (csGlobal.PortPS == "")
                csGlobal.PortPS = "3306";

            var conn_info = "Server=" + csGlobal.ServerPS + ";Port=" + csGlobal.PortPS + ";Database=" +
                            csGlobal.databasePS + ";Uid=" + csGlobal.userPS + ";Pwd=" + csGlobal.passwordPS + ";";
            var isConn = false;
            MySqlConnection conn = null;
            try
            {
                if (csGlobal.isODBCConnection)
                {
                    connODBC.Open();
                }
                else { 
                    conn = new MySqlConnection(conn_info);
                    conn.Open();
                }
                isConn = true;
            }
            catch (ArgumentException a_ex)
            {
                if (csGlobal.modoManual)
                {

                    ("Los datos de conexión a la base de datos mySQL no son correctos" + "\n" + verificaciones).mb();
                }
                /*
                Console.WriteLine("Check the Connection String.");
                Console.WriteLine(a_ex.Message);
                Console.WriteLine(a_ex.ToString());
                */
            }
            catch (MySqlException ex)
            {
                /*string sqlErrorMessage = "Message: " + ex.Message + "\n" +
                "Source: " + ex.Source + "\n" +
                "Number: " + ex.Number;
                Console.WriteLine(sqlErrorMessage);
                */
                isConn = false;
                switch (ex.Number)
                {
                    //http://dev.mysql.com/doc/refman/5.0/en/error-messages-server.html
                    case 1042: // Unable to connect to any of the specified MySQL hosts (Check Server,Port)
                        break;
                    case 0: // Access denied (Check DB name,username,password)
                        if (csGlobal.modoManual)
                        {
                            "Los datos de conexión a la base de datos mySQL no son correctos".mb();
                        }
                        
                        break;
                    default:
                        break;
                }
            }
            finally
            {
                if (csGlobal.isODBCConnection)
                {
                    if (connODBC.State == ConnectionState.Open)
                        connODBC.Close();
                }
                else
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return isConn;
        }

        public static bool check_SQL_DB()
        {
            using (var connection = new SqlConnection(csGlobal.cadenaConexion))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
        }

        public static string delimitarPorComasDataTable(DataTable dt, string name,bool texto=false)
        {
            return dt.AsEnumerable()
                .Select(row => Convert.ToDouble(row[name].ToString()).ToString())
                .Aggregate((s1, s2) => string.Concat(s1, "," + s2));
        }

        public static string delimitarPorComasDataTableNotDouble(DataTable dt, string name)
        {
            return dt.AsEnumerable()
                .Select(row => row[name].ToString().ToString())
                .Aggregate((s1, s2) => string.Concat(s1, "','" + s2));
        }

        /// <summary>
        ///     comprobamos que la columna exista en la tabla
        /// </summary>
        /// <param name="db"></param>
        /// <param name="table"></param>
        /// <param name="column">Columna a Evaluar</param>
        /// <returns></returns>
        public static bool existeColumnaMySQL(string db, string table, string column)
        {
            var mysql = new csMySqlConnect();

            if (
                mysql.comprobarConsulta("SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" + db +
                                        "' AND TABLE_NAME = '" + table + "' AND COLUMN_NAME = '" + column + "'"))
                return true;
            return false;
        }

        public static bool existeTablaMySQL(string table)
        {
            var mysql = new csMySqlConnect();
            if (
                mysql.comprobarConsulta(
                    "SELECT EXISTS(SELECT `TABLE_NAME` FROM `INFORMATION_SCHEMA`.`TABLES` WHERE (`TABLE_NAME` = 'kls_invoice_erp')) as existe;"))
                return true;
            return false;
        }

        /// <summary>
        /// Función que convierte los valores de un datatable en un string separado por comas
        /// indicado para hacer el filtro en los where in ()
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="campo"></param>
        /// <returns></returns>
        public static string concatenarValoresQueryFromDataTable(DataTable dt, string campo)
        {
            var cadena = "";

            for (var i = 0; i < dt.Rows.Count; i++)
                if (i == dt.Rows.Count - 1)
                    cadena += dt.Rows[i][campo].ToString();
                else
                    cadena += dt.Rows[i][campo] + ",";

            return cadena;
        }

        public static bool ventanaPassword()
        {
            var passDialog = new frDialogPassword();

            return passDialog.ShowDialog() == DialogResult.OK;
        }

        public static void contarFilasGrid(DataTable dt, Stopwatch watch, ToolStripStatusLabel numFilas)
        {
            watch.Stop();

            if (watch.Elapsed.Seconds >= 1)
                numFilas.Text = dt.Rows.Count + " filas" + " | " + watch.Elapsed.Seconds + " s";
            else
                numFilas.Text = dt.Rows.Count + " filas" + " | " + watch.Elapsed.Milliseconds + " ms";
        }



        /// <summary>
        ///     Devuelve un DataTable con las filas seleccionadas del datagridview, o bien todas
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        /// <param name="selected">true si quieres las seleccionadas, false si quieres todas, dependiendo del segundo parámetro</param>
        /// <returns>DataTable</returns>
        public static DataTable fillDataTableFromDataGridView(DataGridView dgv, bool selected = true)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn col in dgv.Columns)
                dt.Columns.Add(col.HeaderText);
            if (selected)
                foreach (DataGridViewRow row in dgv.SelectedRows)
                {
                    var dRow = dt.NewRow();
                    foreach (DataGridViewCell cell in row.Cells)
                        dRow[cell.ColumnIndex] = cell.Value;

                    dt.Rows.Add(dRow);
                }
            else
                foreach (DataGridViewRow row in dgv.Rows)
                {
                    var dRow = dt.NewRow();
                    foreach (DataGridViewCell cell in row.Cells)
                        dRow[cell.ColumnIndex] = cell.Value;

                    dt.Rows.Add(dRow);
                }

            return dt;
        }

        /// <summary>
        ///     Actualizar codigo de repasat
        /// </summary>
        /// <param name="url"></param>
        /// <param name="campo">codExternoCli por ejemplo</param>
        /// <param name="valor">Valor que le queramos dar al campo</param>
        /// <returns></returns>
        public static bool enviarDatosPUT(string url, string campo, string valor)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var response = client.UploadValues(url, "PUT", new NameValueCollection { { campo, valor } });

                    var result = Encoding.UTF8.GetString(response);

                    return result == "OK" ? true : false;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

        public static void poblarCombo(ComboBox combo, DataTable dt, string campo, bool cadena = false)
        {
            var view = new DataView(dt);
            foreach (DataRow item in dt.Rows)
            {
                if (cadena)
                {
                    if (!string.IsNullOrEmpty(item[campo].ToString()))
                        item[campo] = item[campo].ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(item[campo].ToString()))
                        item[campo] = Convert.ToInt32(item[campo]).ToString();
                }
            }

            var distinctValues = view.ToTable(true, campo);


            combo.ValueMember = campo;
            combo.DisplayMember = campo;
            combo.DataSource = distinctValues;
        }

        public static DataTable ConvertCSVtoDataTable(string path, bool captio = true, char tab = ';')
        {
            var dt = new DataTable();
            using (var sr = new StreamReader(path, Encoding.Default))
            {
                var headers = sr.ReadLine().Split(tab);

                foreach (var header in headers)
                    try
                    {
                        dt.Columns.Add(header);
                    }
                    catch (DuplicateNameException ex)
                    {
                        dt.Columns.Add(header + "_1");
                    }

                while (!sr.EndOfStream)
                {
                    var rows = sr.ReadLine().Split(tab);
                    var dr = dt.NewRow();
                    for (var i = 0; i < headers.Length; i++)
                        if (captio)
                            dr[i] = rows[i].Replace("\"", "");
                        else
                            dr[i] = rows[i];

                    dt.Rows.Add(dr);
                }
            }


            return dt;
        }



        public static DataTable check2DataTablesSQL(DataTable dt, string consulta)
        {
            var sql = new csSqlConnects();
            var dtA3 = sql.cargarDatosTablaA3(consulta);

            var differences = dt.AsEnumerable().Except(dtA3.AsEnumerable(), DataRowComparer.Default);

            return differences.Any() ? differences.CopyToDataTable() : new DataTable();
        }

        public static DataTable check2DataTablesMYSQL(DataTable dt, string consulta)
        {
            var mysql = new csMySqlConnect();
            var dtPS = mysql.cargarTabla(consulta);

            var differences = dt.AsEnumerable().Except(dtPS.AsEnumerable(), DataRowComparer.Default);

            return differences.Any() ? differences.CopyToDataTable() : new DataTable();
        }

        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item) ?? DBNull.Value;
                table.Rows.Add(values);
            }
            return table;
        }

        public static DataTable getDiffDataTables(DataTable dt1, DataTable dt2)
        {
            DataTable dtDifferenceA3 = new DataTable();
            DataTable dtDifferencePS = new DataTable();

            var differencesA3 = dt1.AsEnumerable().Except(dt2.AsEnumerable(), DataRowComparer.Default);

            dtDifferenceA3 = differencesA3.Any() ? differencesA3.CopyToDataTable() : new DataTable();


            var differencesPS = dt2.AsEnumerable().Except(dt1.AsEnumerable(), DataRowComparer.Default);
            dtDifferencePS = differencesPS.Any() ? differencesPS.CopyToDataTable() : new DataTable();

            return dtDifferenceA3;
        }

        // Devuelve un string con el ultimo dia del mes, así: 24/02/2016 -> 29/02/2016
        public static string getLastDayOfMonth(string fecha)
        {
            int year = 0, month = 0;

            year = Convert.ToInt32(Convert.ToDateTime(fecha).ToString("yyyy"));
            month = Convert.ToInt32(Convert.ToDateTime(fecha).ToString("MM"));

            return fecha = new DateTime(year, month, DateTime.DaysInMonth(year, month)).ToShortDateString();
        }

        public static void cargarDGV(DataGridView dgv, string consulta, bool MySQL = true)
        {
            try
            {
                if (MySQL)
                {
                    var mysql = new csMySqlConnect();
                    dgv.DataSource = mysql.cargarTabla(consulta);
                }
                else
                {
                    var sql = new csSqlConnects();
                    dgv.DataSource = sql.cargarDatosTablaA3(consulta);
                }
            }
            catch (Exception)
            {
            }
        }

        public static object ejecutarConsultaScalar(string consulta, bool MySQL = true)
        {
            try
            {
                if (MySQL)
                {
                    // Conexión MySQL
                    var mysql = new csMySqlConnect();
                    return mysql.obtenerDatoFromQuery(consulta);  
                }
                else
                {
                    // Conexión SQL Server
                    var sql = new csSqlConnects();
                    return sql.obtenerValorScalarSQLServer(consulta); 
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    MessageBox.Show(ex.Message);
                }
                return null;
            }
        }

        public static void ejecutarConsulta(string consulta, bool MySQL = true, bool gestionarConexiones=true, object sqlConnection=null)
        {
            try
            {
                if (MySQL)
                {
                    var mysql = new csMySqlConnect();
                    mysql.actualizaStock(consulta, gestionarConexiones);
                    mysql.CloseConnection();
                }
                else
                {
                    var sql = new csSqlConnects();
                    sql.actualizarCampo(consulta, gestionarConexiones);
                    sql.cerrarConexion();
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    //LLUIS
                    MessageBox.Show(ex.Message);
                }
            }
            
        }


        public static bool comprobarEsNumero(string valor)
        {
            try
            {
                double num;
                bool isNumeric = double.TryParse(valor, out num);
                return isNumeric;
            }
            catch
            { return false; }
        }



        //24/12/2020 comentada por lvg (utilizar función de arriba)
        /// <summary>
        ///     Método que permite realizar updates, inserts y deletes en MySQL o SQL Server
        /// </summary>
        /// <param name="consulta">Query</param>
        /// <param name="MySQL">Constantes csUtilidades.MySQL y csUtilidades.SQL</param>
        /// <param name="tipo">Constantes csUtilidades.INSERT, UPDATE, DELETE</param>
        //public static void ejecutarConsulta(string consulta, bool MySQL = true, int tipo = 3)
        //{
        //    int num = 0;
        //    try
        //    {
        //        if (MySQL)
        //        {
        //            var mysql = new csMySqlConnect();

        //            if (tipo == UPDATE)
        //                mysql.actualizaStock(consulta);
        //            else if (tipo == INSERT)
        //                mysql.insertItems(consulta);
        //            else if (tipo == DELETE)
        //                mysql.consultaBorrar(consulta);
        //        }
        //        else
        //        {
        //            var sql = new csSqlConnects();

        //            if (tipo == UPDATE)
        //                sql.actualizarCampo(consulta);
        //            else if (tipo == INSERT)
        //                sql.consultaInsertar(consulta);
        //            else if (tipo == DELETE)
        //                sql.consultaBorrar(consulta);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        csUtilidades.log(ex.Message);
        //    }
        //}




        /// <summary>
        /// Funcion para borrado de registros más rápido
        /// Se pasa un datatable con los ids y hará un borrado con un select in
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="campo"></param>
        public static void eliminarRegistrosDeTabla(DataTable dt, string campo, string tabla)
        {
            try
            {
                string where_in = "";
                MySqlConnection connection;
                connection = new MySqlConnection(csGlobal.cadenaConexionPS);
                connection.Open();

                //MySqlCommand cmd = new MySqlCommand((query, connection);
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection=connection;

                int numfila = 0;
                DataTable tablaRegistros = new DataTable();
                tablaRegistros.Columns.Add(campo);
                
                foreach (DataRow item in dt.Rows)
                {
                    DataRow fila = tablaRegistros.NewRow();
                    fila[campo] = item[campo].ToString();
                    tablaRegistros.Rows.Add(fila);
                    numfila++;

                    if (numfila == 500)
                    {

                        where_in = csUtilidades.delimitarPorComasDataTableNotDouble(tablaRegistros, campo);

                        cmd.CommandText = "delete from " + tabla + " where " + campo + " in ('" + where_in + "')";
                        cmd.ExecuteNonQuery();

                        tablaRegistros.Rows.Clear();
                        numfila = 0;
                    }
                }
                if (numfila < 500)
                {
                    where_in = csUtilidades.delimitarPorComasDataTableNotDouble(dt, "id_product");

                    cmd.CommandText = "delete from " + tabla + " where " + campo + " in (" + where_in + ")";
                    cmd.ExecuteNonQuery();
                }
            
            connection.Close();
            }
            catch
            { 
            
            }
        
        
        }

        public static DataTable dataRowArray2DataTable(DataRow[] rowArray)
        {
            try
            {
                DataTable dt = null;

                if (rowArray.Length > 0)
                    dt = rowArray.CopyToDataTable();

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string removeDigitsFromString(string output)
        {
            return Regex.Replace(output, @"[\d-]", string.Empty);
        }

        /// <summary>
        ///     Si la carpeta no existe, la crea
        /// </summary>
        /// <param name="ruta"></param>
        public static void openFolder(string ruta)
        {
            try
            {
                if (!File.Exists(ruta))
                    Directory.CreateDirectory(ruta);

                Process.Start("explorer.exe", ruta);
                csUtilidades.registroProceso("Paso 1", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                csUtilidades.registroProceso(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void DataTable2CSV(DataTable dt, string caracter = ";", string nombreFichero = "Exportacion")
        {
            var sb = new StringBuilder();

            var columnNames = dt.Columns.Cast<DataColumn>().
                Select(column => column.ColumnName).
                ToArray();
            sb.AppendLine(string.Join(caracter, columnNames));

            foreach (DataRow row in dt.Rows)
            {
                var fields = row.ItemArray.Select(field => field.ToString()).
                    ToArray();
                sb.AppendLine(string.Join(caracter, fields));
            }

            //File.WriteAllText("dt.csv", sb.ToString());

            using (var sfd = new System.Windows.Forms.SaveFileDialog())
            {
                sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.FilterIndex = 2;
                sfd.RestoreDirectory = true;
                sfd.FileName = DateTime.Now.ToShortDateString().Replace("/", "") + " - " + nombreFichero + ".csv";

                if (sfd.ShowDialog() == DialogResult.OK)
                    File.WriteAllText(sfd.FileName, sb.ToString());
            }
        }

        public static DataTable dgv2dtSelectedRows(DataGridView dgv)
        {
            var dt = ((DataTable)dgv.DataSource).Clone();

            foreach (DataGridViewRow row in dgv.SelectedRows)
                dt.ImportRow(((DataTable)dgv.DataSource).Rows[row.Index]);

            dt.AcceptChanges();

            return dt;

            //DataTable dt = new DataTable();
            //foreach (DataGridViewColumn column in dgv.Columns)
            //    dt.Columns.Add(column.Name, column.CellType); //better to have cell type
            //for (int i = 0; i < dgv.SelectedRows.Count; i++)
            //{
            //    dt.Rows.Add();
            //    for (int j = 0; j < dgv.Columns.Count; j++)
            //    {
            //        dt.Rows[i][j] = dgv.SelectedRows[i].Cells[j].Value;
            //    }
            //}

            //return dt;
        }

        public static bool enviarMail(string subject,
            string messageBody,
            List<string> ccs,
            string adjunto = "",
            string destinatario = "",
            string origen = "",
            string password = "",
            string smtp = "",
            bool notificacionLectura = false,
            bool html = true)
        {
            try
            {
                var mail = new MailMessage(origen, destinatario);

                var client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = smtp;
                client.Credentials = new NetworkCredential(origen, password);

                if (ccs.Count > 0)
                {
                    foreach (string cc in ccs)
                    {
                        if (cc != null)
                        {
                            mail.Bcc.Add(cc);
                        }
                    }
                }

                mail.Subject = subject;
                mail.IsBodyHtml = html;
                mail.Body = messageBody;
                if (notificacionLectura)
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                client.Send(mail);
                return true;
            }
            catch (SmtpFailedRecipientException ex)
            {
                Program.guardarErrorFichero(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Si no lo encuentra, genérico
        /// </summary>
        /// <param name="nif_ps"></param>
        /// <returns></returns>
        public static string modoMixtoBuscarClienteNIF(string nif_ps)
        {
            csSqlConnects sql = new csSqlConnects();

            string consulta = sql.obtenerCampoTabla("select ltrim(codcli) as codcli from clientes where ltrim(nifcli) = '" + nif_ps + "'");

            if (consulta == "")
                return csGlobal.clienteGenerico;
            else
                return consulta;
        }

        public static string modoMixtoBuscarClienteEmail(string email_ps)
        {
            csSqlConnects sql = new csSqlConnects();

            string consulta = sql.obtenerCampoTabla("select ltrim(codcli) as codcli from clientes where e_mail_docs = '" + email_ps + "'");

            if (consulta == "")
                return csGlobal.clienteGenerico;
            else
                return consulta;
        }

        public static DataTable DataGridViewSelectedRowCollection2DataTable(DataGridView dgv)
        {
            DataTable dt = ((DataTable)dgv.DataSource).Clone();
            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                dt.ImportRow(((DataTable)dgv.DataSource).Rows[row.Index]);
            }
            dt.AcceptChanges();

            return dt;
        }

        public static DataTable GetDataSourceFromFile(string fileName)
        {
            DataTable dt = new DataTable();
            using (System.IO.TextReader tr = File.OpenText(fileName))
            {
                string line;
                while ((line = tr.ReadLine()) != null)
                {

                    string[] items = line.Trim().Split(' ');
                    if (dt.Columns.Count == 0)
                    {
                        // Create the data columns for the data table based on the number of items
                        // on the first line of the file
                        for (int i = 0; i < items.Length; i++)
                            dt.Columns.Add(new DataColumn("Column" + i, typeof(string)));
                    }

                    dt.Rows.Add(items);
                }
            }

            return dt;
        }

        public static void crearCategoria(string consulta, bool MySQL = true, bool gestionarConexiones = true, object sqlConnection = null)
        {
            try
            {
                if (MySQL)
                {
                    var mysql = new csMySqlConnect();
                    mysql.actualizaStock(consulta, gestionarConexiones);
                }
                else
                {
                    var sql = new csSqlConnects();
                    sql.actualizarCampo(consulta, gestionarConexiones);
                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                   // MessageBox.Show(ex.Message);
                }
            }

        }

        static public void cambiarCampoWebservice(string objeto, string id, string campoNuevo, string valorNuevo)
        {
            RestRequest request;
            var client = new RestClient(csGlobal.APIUrl);
            request = new RestRequest(objeto + "/" + id + "?ws_key=" + csGlobal.webServiceKey, Method.GET);
            IRestResponse response = client.Execute(request);

            XElement orderXML = XElement.Parse(response.Content);
            XElement orderEl = orderXML.Descendants().FirstOrDefault();
            orderEl.Element(campoNuevo).Value = valorNuevo;

            request = new RestRequest(objeto + "?ws_key=" + csGlobal.webServiceKey, Method.PUT);
            request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
            IRestResponse response2 = client.Execute(request);
        }

        static public DataTable conectarMySQL(string connection_string, string query)
        {
            MySqlConnection conexionBD = new MySqlConnection(connection_string);
            try
            {
                MySqlCommand comando = new MySqlCommand(query); //Declaración SQL para ejecutar contra una base de datos MySQL
                comando.Connection = conexionBD; //Establece la MySqlConnection utilizada por esta instancia de MySqlCommand
                conexionBD.Open(); //Abre la conexión
                comando.CommandText = string.Format(query, csGlobal.conexionDB);
                DataTable dt = new DataTable();
                dt.Load(comando.ExecuteReader());

                return dt;

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message); //Si existe un error aquí muestra el mensaje
                return null;
            }
            finally
            {
                conexionBD.Close(); //Cierra la conexión a MySQL
            }
        }
        
        

        static public string obtenerValorMySQL(string connection_string, string query)
        {

            Utilidades.csLogErrores cslog = new Utilidades.csLogErrores();

            MySqlConnection conexionBD = new MySqlConnection(connection_string);
            try
            {
                MySqlCommand comando = new MySqlCommand(query);
                comando.Connection = conexionBD; 

                cslog.guardarLogProceso("Preparando conexion realizada con Esync");
                cslog.guardarLogProceso(connection_string);

                try
                {
                    conexionBD.Open();
                }
                catch (Exception ex) {
                    string errortext = ex.Message;
                }

                if (csGlobal.modeDebug) csUtilidades.log("Conexion abierta");
                cslog.guardarLogProceso("Conexion abierta");

                comando.CommandText = string.Format(query, csGlobal.conexionDB);
                DataTable dt = new DataTable();
                dt.Load(comando.ExecuteReader());

                cslog.guardarLogProceso("conexion realizada con Esync ");
                cslog.guardarLogProceso("Filas devueltas: " + dt.Rows[0][0].ToString());

                return dt.Rows[0][0].ToString();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message); 
                return null;
            }
            finally
            {
                conexionBD.Close();
            }
        }

        public static void ejecutarComandoMySQL(string connection_string, string query)
        {
            Utilidades.csLogErrores cslog = new Utilidades.csLogErrores();
            MySqlConnection conexionBD = new MySqlConnection(connection_string);

            try
            {
                MySqlCommand comando = new MySqlCommand(query, conexionBD);

                cslog.guardarLogProceso("Preparando conexión a Esync para ejecutar comando SQL.");
                cslog.guardarLogProceso(connection_string);

                conexionBD.Open();
                comando.ExecuteNonQuery();  // Ejecuta la consulta sin esperar un resultado

                cslog.guardarLogProceso("Comando SQL ejecutado correctamente en Esync.");
            }
            catch (MySqlException ex)
            {
                MessageBox.Show($"Error al ejecutar comando SQL: {ex.Message}");
            }
            finally
            {
                conexionBD.Close();
            }
        }


        public static DateTime? obtenerFechaMaximaDesdeDB()
        {
            try
            {
                string connectionString = dbConnectEsync.cadenaConexionEsyncDB();

                string query = "SELECT fecha_maxima_selector AS FechaMaxima FROM config WHERE connection_name = '" + csGlobal.conexionDB + "'";

                DataTable dt = cargarTablaMySQL(connectionString, query);

                if (dt.Rows.Count > 0 && dt.Rows[0]["FechaMaxima"] != DBNull.Value)
                {
                    return Convert.ToDateTime(dt.Rows[0]["FechaMaxima"]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener la fecha máxima: {ex.Message}");
            }

            return null; 
        }

        public static DateTime? obtenerFechaMinimaDesdeDB()
        {
            try
            {
                string connectionString = dbConnectEsync.cadenaConexionEsyncDB();

                string query = "SELECT fecha_minima_selector AS FechaMinima FROM config WHERE connection_name = '" + csGlobal.conexionDB + "'";

                DataTable dt = cargarTablaMySQL(connectionString, query);

                if (dt.Rows.Count > 0 && dt.Rows[0]["FechaMinima"] != DBNull.Value)
                {
                    return Convert.ToDateTime(dt.Rows[0]["FechaMinima"]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener la fecha minima: {ex.Message}");
            }

            return null;
        }


        public static int obtenerShortDescriptionLength()
        {
            csMySqlConnect mySql = new csMySqlConnect();
            string query = "SELECT value FROM ps_configuration WHERE name = 'PS_PRODUCT_SHORT_DESC_LIMIT'";
            int resultado = mySql.ObtenerIdInsertado(query);
            
            return resultado;
        }




        public static DataTable cargarTablaMySQL(string connectionString, string query)
        {
            Utilidades.csLogErrores cslog = new Utilidades.csLogErrores();
            MySqlConnection conexionBD = new MySqlConnection(connectionString);
            DataTable tablaDatos = new DataTable();

            try
            {
                MySqlCommand comando = new MySqlCommand(query, conexionBD);

                cslog.guardarLogProceso("Preparando conexión a Esync para cargar tabla.");
                cslog.guardarLogProceso(connectionString);

                conexionBD.Open();

                MySqlDataAdapter myDA = new MySqlDataAdapter(comando);
                myDA.Fill(tablaDatos);

                cslog.guardarLogProceso("Datos cargados correctamente en DataTable desde Esync.");
            }
            catch (MySqlException ex)
            {
                MessageBox.Show($"Error al cargar datos en DataTable: {ex.Message}");
            }
            finally
            {
                conexionBD.Close();
            }

            return tablaDatos;
        }


        static public string obtenerSerieA3ERP(string idSerieRPST)
        {
            csSqlConnects sql = new csSqlConnects();
            return sql.obtenerCampoTabla("SELECT LTRIM(SERIE) FROM SERIES WHERE RPST_ID_SERIE=" + idSerieRPST);
        }

        static public void modificarConfiguracionEsync(string query)
        {
            try
            {
                
                using (MySqlConnection mcon = new MySqlConnection(dbConnectEsync.cadenaConexionEsyncDB()))
                using (MySqlCommand cmd = mcon.CreateCommand())
                {
                    mcon.Open();
                    cmd.CommandText = string.Format(query, csGlobal.conexionDB);
                    cmd.ExecuteReader();
                    mcon.Close();

                }
            }
            catch
            {
            }

        }





        static public void ConsultaMySQL(string connection_string, string query)
        {
            MySqlConnection conexionBD = new MySqlConnection(connection_string);
            try
            {
                MySqlCommand comando = new MySqlCommand(query); //Declaración SQL para ejecutar contra una base de datos MySQL
                comando.Connection = conexionBD; //Establece la MySqlConnection utilizada por esta instancia de MySqlCommand
                conexionBD.Open(); //Abre la conexión
                comando.CommandText = string.Format(query, csGlobal.conexionDB);
                comando.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message); //Si existe un error aquí muestra el mensaje
            }
            finally
            {
                conexionBD.Close(); //Cierra la conexión a MySQL
            }
        }


        ////////////////////////


        static public DateTime TransformarFormatoInvalido(string expirationDate, string format)
        {
            DateTime expiration_date;
            if (DateTime.TryParseExact(expirationDate, format,
                                        System.Globalization.CultureInfo.InvariantCulture,
                                        System.Globalization.DateTimeStyles.None, out expiration_date))
            {
                return expiration_date;
            }

            return expiration_date;
        }


        static public void GuardarConexion()
        {
            // Modificacion hecha por: Alex Cobas; 24-10-04
            //string query = "UPDATE config SET fecha_ultima_conexion = @fechaUltimaConexion WHERE connection_name = @nombreConexion";
            string fechaUltimaConexion = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string nombreConexion = csGlobal.conexionDB;
            string query = "UPDATE config SET fecha_ultima_conexion = '" + fechaUltimaConexion + "' WHERE connection_name = '" + nombreConexion + "'";
            modificarConfiguracionEsync(query);
            GuardarRegistroConexion(nombreConexion);
        }
        static public void GuardarRegistroConexion(string nombreConexion)
        {
            // Modificacion hecha por: Alex Cobas; 24-10-08
            string query = "";
            int mesesBorrar = int.Parse(obtenerValorMySQL(dbConnectEsync.cadenaConexionEsyncDB(), "SELECT meses_eliminar_conexion FROM config WHERE connection_name = '" + nombreConexion + "'"));
            if (mesesBorrar <= 0)
            {
                mesesBorrar = 12;
                query = "UPDATE config SET meses_eliminar_conexion = '" + mesesBorrar + "' WHERE connection_name = '" + nombreConexion + "'";
                modificarConfiguracionEsync(query);
            }
            bool borrarAccesos = int.Parse(obtenerValorMySQL(dbConnectEsync.cadenaConexionEsyncDB(), "SELECT count(*) FROM conexiones WHERE fecha_conexion < DATE_SUB(CURDATE(), INTERVAL " + mesesBorrar + " MONTH) AND connection_name = '" + nombreConexion + "'")) >= 1;
            if (borrarAccesos)
            {
                query = "DELETE FROM conexiones WHERE fecha_conexion < DATE_SUB(CURDATE(), INTERVAL " + mesesBorrar + " MONTH) AND connection_name = '" + nombreConexion + "'";
                modificarConfiguracionEsync(query);
            }
            string fechaConexion = DateTime.Today.ToString("yyyy-MM-dd");
            TimeSpan horaConexion = DateTime.Now.TimeOfDay;
            string ipOrdenador = ObtenerIPLocal();
            string hostname = ObtenerHostname();
            query = "INSERT INTO conexiones (connection_name, fecha_conexion, hora_conexion, ip_ordenador, hostname) VALUES('" + nombreConexion + "', '" + fechaConexion + "', '" + horaConexion + "', '" + ipOrdenador + "', '" + hostname + "')";
            modificarConfiguracionEsync(query);
        }
        public static string ObtenerIPLocal()
        {
            string ipLocal = "";
            try
            {
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipLocal = ip.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error obteniendo la IP local: " + ex.Message);
            }

            return ipLocal;
        }

        public static string ObtenerHostname()
        {
            // Modificacion hecha por: Alex Cobas; 24-10-07
            string hostname = "";
            try
            {
                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                hostname = host.HostName;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener el hostname: " + ex.Message);
            }

            return hostname;
        }

        static public void GuardarVersionA3()
        {
            csSqlConnects sql = new csSqlConnects();
            //string versionA3 = sql.obtenerCampoTabla("select CONCAT(version,'.',revision,'.',parche) from [A3ERP$SISTEMA].[dbo].[VERSION] where LIBRERIA='A3ERP'");

            string parche = sql.obtenerCampoTabla("SELECT SUBSTRING(parche, 3, LEN(parche)) AS nuevo_parche FROM [A3ERP$SISTEMA].[dbo].[VERSION] WHERE LIBRERIA = 'A3ERP'");
            string versionA3 = sql.obtenerCampoTabla("select CONCAT(version,'.',revision) from [A3ERP$SISTEMA].[dbo].[VERSION] where LIBRERIA='A3ERP'");
            versionA3 += "." + parche;

            sql.cerrarConexion();
            //string query = "UPDATE config SET versionA3 = @versionA3_ultima_conexion WHERE connection_name = @nombreConexion";
            string query = "UPDATE config SET versionA3 = '" + versionA3 + "' WHERE connection_name = '" + csGlobal.conexionDB + "'";
            modificarConfiguracionEsync(query);
        }

        static public void GuardarVersionEsync()
        {
            // Modificacion hecha por: Alex Cobas; 24-10-07
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            //string query = "UPDATE config SET versionEsync = @versionInfo WHERE connection_name = @nombreConexion";
            string query = "UPDATE conexiones SET versionEsync = '" + versionInfo.FileVersion + "' WHERE connection_name = '" + csGlobal.conexionDB + "'";
            modificarConfiguracionEsync(query);
        }


        //////////////
        ///FUNCION PARA AÑADIR EL CONTROL DE LAS FUNCINOES EN DISMAY
        ///
        public static void guardarProgresoFichero(string texto)
        {
            const string fic = @"progresoLog.txt";
            using (System.IO.StreamWriter swq = new System.IO.StreamWriter(fic, true))
            {
                swq.WriteLine("_____________________________________________________________ " + DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToShortTimeString() + " __________________________________________________________________________");
                swq.WriteLine(texto + " - " + DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToShortTimeString());
                swq.WriteLine();
            }
        }



        public static void actualizarKlsA3erpId(DataGridView dgvClientes)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlConnects sqlConector = new csSqlConnects();

                DataTable ps_final = mysql.cargarTabla("SELECT id_customer AS ID, email AS EMAIL, ltrim(kls_a3erp_id) AS CODCLI FROM ps_customer WHERE kls_a3erp_id IS NULL");
                DataTable a3_final = sqlConector.cargarDatosTablaA3("SELECT KLS_CODCLIENTE AS ID, KLS_EMAIL_USUARIO AS EMAIL, LTRIM(CODCLI) AS CODCLI, NIFCLI FROM CLIENTES");

                foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                {
                    try
                    {
                        string idCustomer = fila.Cells["id_customer"].Value.ToString();
                        string codA3ERPPS = fila.Cells["CodA3ERP"].Value.ToString();

                        if (!string.IsNullOrEmpty(codA3ERPPS))
                        {
                            continue;
                        }

                        DataRow[] psClientes = ps_final.Select($"ID = '{idCustomer}'");

                        if (psClientes.Length > 0)
                        {
                            DataRow ps = psClientes[0];
                            string emailPS = ps["EMAIL"].ToString().Trim();

                            DataTable psAddress = mysql.cargarTabla($"SELECT dni AS NIF FROM ps_address WHERE id_customer = '{idCustomer}'");
                            string nifPS = (psAddress.Rows.Count > 0) ? psAddress.Rows[0]["NIF"].ToString().Trim() : null;

                            DataRow[] a3Clientes = a3_final.Select($"ID = '{idCustomer}'");

                            if (a3Clientes.Length > 0)
                            {
                                DataRow a3 = a3Clientes[0];
                                string nifA3 = a3["NIFCLI"].ToString().Trim();
                                string codcliA3 = a3["CODCLI"].ToString().Trim();
                                string emailA3 = a3["EMAIL"].ToString().Trim();

                                if (!string.IsNullOrEmpty(nifA3))
                                {
                                    if (string.IsNullOrEmpty(codA3ERPPS) && emailPS == emailA3 && nifPS == nifA3)
                                    {
                                        string updateQuery = $"UPDATE ps_customer SET kls_a3erp_id = '{codcliA3}' WHERE id_customer = {idCustomer}";
                                        mysql.ejecutarConsulta(updateQuery);
                                    }
                                    else if (string.IsNullOrEmpty(codA3ERPPS) && (emailPS == emailA3 || nifPS == nifA3))
                                    {
                                        if (MessageBox.Show("DATOS A3: \n CODIGO: " + codcliA3 + "\n EMAIL: " + emailA3 + "\n NIF: " + nifA3 + "\n \n DATOS PRESTASHOP: \n CODIGO: " + codA3ERPPS + "\n EMAIL: " + emailPS + "\n NIF: " + nifPS + "\n\n\n Algun dato no coincide ¿Quieres sincronizar el Codigo Externo?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            string updateQuery = $"UPDATE ps_customer SET kls_a3erp_id = '{codcliA3}' WHERE id_customer = {idCustomer}";
                                            mysql.ejecutarConsulta(updateQuery);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error procesando el cliente con id_customer {fila.Cells["id_customer"].Value.ToString()}: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error general al actualizar los clientes: {ex.Message}");
            }
        }

    }


    public static class DataTableExt
    {
        public static DataTable buscar(this DataTable dt, string busqueda)
        {
            try
            {
                return csUtilidades.dataRowArray2DataTable(dt.Select(busqueda));
            }
            catch
            {
                csUtilidades.log("Error en registro: " + busqueda);
                return null;
            }
        }

        public static bool hasRows(this DataTable dt)
        {
            return csUtilidades.verificarDt(dt);
        }

        // <summary>
        /// Export DataTable to Excel file
        /// </summary>
        /// <param name="DataTable">Source DataTable</param>
        /// <param name="ExcelFilePath">Path to result file name</param>
        public static void exportToExcel(this DataTable dt, string fileName)
        {
            //Exportar a excel el order_date lo introduce mal cambia el mes por el dia MM/DD/AA
            // Y esto falla al pasarselo a Alfa CODIGO=2
            csUtilidades.registroProceso("Paso 0 - Validando Entorno Excel", System.Reflection.MethodBase.GetCurrentMethod().Name);
            /*Set up work book, work sheets, and excel application*/
            Microsoft.Office.Interop.Excel.Application oexcel = new Microsoft.Office.Interop.Excel.Application();
            try
            {

                csUtilidades.registroProceso("Paso 1 - Excel Instalado", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string filepath = "";
                string path = AppDomain.CurrentDomain.BaseDirectory;
                object misValue = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Excel.Workbook obook = oexcel.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Worksheet osheet = new Microsoft.Office.Interop.Excel.Worksheet();


                //  obook.Worksheets.Add(misValue);

                osheet = (Microsoft.Office.Interop.Excel.Worksheet)obook.Sheets["Hoja1"];
                int colIndex = 0;
                int rowIndex = 1;

                csUtilidades.registroProceso("Paso 1", System.Reflection.MethodBase.GetCurrentMethod().Name);

                foreach (DataColumn dc in dt.Columns)
                {
                    colIndex++;
                    osheet.Cells[1, colIndex] = dc.ColumnName;
                }
                foreach (DataRow dr in dt.Rows)
                {
                    rowIndex++;
                    colIndex = 0;

                    foreach (DataColumn dc in dt.Columns)
                    {
                        colIndex++;
                        osheet.Cells[rowIndex, colIndex] = dr[dc.ColumnName];
                    }
                }

                osheet.Columns.AutoFit();
                if (csGlobal.fileExportPath == "")
                {
                    filepath = csUtilidades.getDesktopPath() + @"\Vilardell\" + fileName;
                }
                else
                {
                    filepath = csGlobal.fileExportPath + "\\" + fileName;
                }

                //Release and terminate excel

                obook.SaveAs(filepath);
                obook.Close();
                oexcel.Quit();
                releaseObject(osheet);

                releaseObject(obook);

                releaseObject(oexcel);
                GC.Collect();
                csUtilidades.registroProceso("Paso 2", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                csUtilidades.registroProceso(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
                oexcel.Quit();
            }
        }

        public static string eliminarEtiquetasHTML(string texto)
        {
            if (string.IsNullOrEmpty(texto)) return texto;
            return Regex.Replace(texto, "<.*?>", string.Empty); 
        }

        public static void exportToExcelNew(this DataTable dt, string fileName)
        {
            csUtilidades.registroProceso("Paso 0 - Validando Entorno Excel", System.Reflection.MethodBase.GetCurrentMethod().Name);

            Microsoft.Office.Interop.Excel.Application oexcel = new Microsoft.Office.Interop.Excel.Application
            {
                Visible = false,
                DisplayAlerts = false
            };

            try
            {
                csUtilidades.registroProceso("Paso 1 - Excel Instalado", System.Reflection.MethodBase.GetCurrentMethod().Name);

                string filepath;
                string path = AppDomain.CurrentDomain.BaseDirectory;

                object misValue = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Excel.Workbook obook = oexcel.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Worksheet osheet = (Microsoft.Office.Interop.Excel.Worksheet)obook.Sheets["Hoja1"];

                int colIndex = 0;
                int rowIndex = 1;

                foreach (DataColumn dc in dt.Columns)
                {
                    colIndex++;
                    osheet.Cells[1, colIndex] = dc.ColumnName;

                    Microsoft.Office.Interop.Excel.Range headerRange = osheet.Cells[1, colIndex];
                    headerRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    headerRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    headerRange.Font.Bold = true;
                }

                foreach (DataRow dr in dt.Rows)
                {
                    rowIndex++;
                    colIndex = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        colIndex++;
                        Microsoft.Office.Interop.Excel.Range cellRange = osheet.Cells[rowIndex, colIndex];

                        if (dc.ColumnName == "EAN")
                        {
                            cellRange.NumberFormat = "@";
                            cellRange.Value = "'" + dr[dc.ColumnName].ToString();
                        }
                        else if (dc.ColumnName == "URL de la imagen de portada")
                        {
                            string url = dr[dc.ColumnName].ToString();
                            if (!string.IsNullOrEmpty(url))
                            {
                                osheet.Hyperlinks.Add(cellRange, url, Type.Missing, "Abrir URL", url);
                            }
                        }
                        else if (dc.ColumnName == "Breve descripción" || dc.ColumnName == "Descripción")
                        {
                            string rawText = dr[dc.ColumnName].ToString();
                            string cleanText = System.Text.RegularExpressions.Regex.Replace(rawText, "<.*?>", string.Empty);
                            cellRange.Value = cleanText;
                        }
                        else
                        {
                            cellRange.Value = dr[dc.ColumnName] == DBNull.Value ? "" : dr[dc.ColumnName];
                        }

                        cellRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        cellRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    }
                }

                osheet.Columns.AutoFit();

                filepath = string.IsNullOrEmpty(csGlobal.fileExportPath) ? Path.Combine(csUtilidades.getDesktopPath(), "EXPORT_ESYNC", fileName) : Path.Combine(csGlobal.fileExportPath, fileName);

                if (!Directory.Exists(Path.GetDirectoryName(filepath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                }

                obook.SaveAs(filepath);
                obook.Close();
                oexcel.Quit();

                releaseObject(osheet);
                releaseObject(obook);
                releaseObject(oexcel);

                GC.Collect();
                csUtilidades.registroProceso("Paso 2 - Archivo guardado correctamente", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                csUtilidades.registroProceso($"Error al exportar a Excel: {ex.Message}", System.Reflection.MethodBase.GetCurrentMethod().Name);
                Console.WriteLine($"Detalles del error: {ex}");
                if (oexcel != null)
                {
                    oexcel.Quit();
                    releaseObject(oexcel);
                }
            }
        }
        public static void releaseObject(object o) { try { while (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0) { } } catch { } finally { o = null; } }
    }

        public static class StringExt
        {
            public static string Truncate(this string value, int maxLength)
            {
                if (string.IsNullOrEmpty(value)) return value;
                return value.Length <= maxLength ? value : value.Substring(0, maxLength);

            }

            public static void mb(this string value)
            {
                MessageBox.Show(value);
            }

            public static bool what(this string value)
            {
                return MessageBox.Show(value, "", MessageBoxButtons.YesNo) == DialogResult.Yes;
            }



          




           

            public static string Hash(this string input)
            {
                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                    var sb = new StringBuilder(hash.Length * 2);

                    foreach (byte b in hash)
                    {
                        // can be "x2" if you want lowercase
                        sb.Append(b.ToString("X2"));
                    }

                    return sb.ToString();
                }
            }
        }
   
       

        public sealed class StringWriterWithEncoding : StringWriter
        {
            private readonly Encoding encoding;

            public StringWriterWithEncoding(Encoding encoding)
            {
                this.encoding = encoding;
            }

            public override Encoding Encoding
            {
                get { return encoding; }
            }
        }
   



}