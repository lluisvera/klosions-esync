﻿namespace klsync
{
    partial class frSincronizarMarcas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frSincronizarMarcas));
            this.splitContainerSincMarcas = new System.Windows.Forms.SplitContainer();
            this.dgvA3 = new System.Windows.Forms.DataGridView();
            this.toolStripSincMarcasA3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonA3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonActA3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelProgresoA3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripProgressBarActA3 = new System.Windows.Forms.ToolStripProgressBar();
            this.dgvPS = new System.Windows.Forms.DataGridView();
            this.toolStripSincMarcasPS = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonInsertarMarcasPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelProgresoPS = new System.Windows.Forms.ToolStripLabel();
            this.toolStripProgressActPS = new System.Windows.Forms.ToolStripProgressBar();
            this.splitContainerSincMarcas.Panel1.SuspendLayout();
            this.splitContainerSincMarcas.Panel2.SuspendLayout();
            this.splitContainerSincMarcas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).BeginInit();
            this.toolStripSincMarcasA3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).BeginInit();
            this.toolStripSincMarcasPS.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerSincMarcas
            // 
            this.splitContainerSincMarcas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerSincMarcas.Location = new System.Drawing.Point(0, 0);
            this.splitContainerSincMarcas.Name = "splitContainerSincMarcas";
            // 
            // splitContainerSincMarcas.Panel1
            // 
            this.splitContainerSincMarcas.Panel1.Controls.Add(this.dgvA3);
            this.splitContainerSincMarcas.Panel1.Controls.Add(this.toolStripSincMarcasA3);
            // 
            // splitContainerSincMarcas.Panel2
            // 
            this.splitContainerSincMarcas.Panel2.Controls.Add(this.dgvPS);
            this.splitContainerSincMarcas.Panel2.Controls.Add(this.toolStripSincMarcasPS);
            this.splitContainerSincMarcas.Size = new System.Drawing.Size(1486, 708);
            this.splitContainerSincMarcas.SplitterDistance = 709;
            this.splitContainerSincMarcas.TabIndex = 0;
            // 
            // dgvA3
            // 
            this.dgvA3.AllowUserToAddRows = false;
            this.dgvA3.AllowUserToDeleteRows = false;
            this.dgvA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvA3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvA3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvA3.Location = new System.Drawing.Point(0, 60);
            this.dgvA3.Name = "dgvA3";
            this.dgvA3.ReadOnly = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvA3.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvA3.Size = new System.Drawing.Size(709, 648);
            this.dgvA3.TabIndex = 1;
            // 
            // toolStripSincMarcasA3
            // 
            this.toolStripSincMarcasA3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripSincMarcasA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonA3,
            this.toolStripSeparator1,
            this.toolStripButtonActA3,
            this.toolStripSeparator5,
            this.toolStripLabelProgresoA3,
            this.toolStripProgressBarActA3});
            this.toolStripSincMarcasA3.Location = new System.Drawing.Point(0, 0);
            this.toolStripSincMarcasA3.Name = "toolStripSincMarcasA3";
            this.toolStripSincMarcasA3.Size = new System.Drawing.Size(709, 60);
            this.toolStripSincMarcasA3.TabIndex = 0;
            this.toolStripSincMarcasA3.Text = "toolStrip1";
            // 
            // toolStripButtonA3
            // 
            this.toolStripButtonA3.Image = global::klsync.Properties.Resources.a3erp;
            this.toolStripButtonA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonA3.Name = "toolStripButtonA3";
            this.toolStripButtonA3.Size = new System.Drawing.Size(138, 57);
            this.toolStripButtonA3.Text = "Cargar Marcas A3";
            this.toolStripButtonA3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButtonA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonA3.Click += new System.EventHandler(this.toolStripButtonA3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButtonActA3
            // 
            this.toolStripButtonActA3.Image = global::klsync.Properties.Resources.refresh_button;
            this.toolStripButtonActA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonActA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonActA3.Name = "toolStripButtonActA3";
            this.toolStripButtonActA3.Size = new System.Drawing.Size(136, 57);
            this.toolStripButtonActA3.Text = "Actualizar Marcas";
            this.toolStripButtonActA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonActA3.Click += new System.EventHandler(this.toolStripButtonActA3_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripLabelProgresoA3
            // 
            this.toolStripLabelProgresoA3.Name = "toolStripLabelProgresoA3";
            this.toolStripLabelProgresoA3.Size = new System.Drawing.Size(73, 57);
            this.toolStripLabelProgresoA3.Text = "Progreso";
            // 
            // toolStripProgressBarActA3
            // 
            this.toolStripProgressBarActA3.Margin = new System.Windows.Forms.Padding(2, 2, 1, 1);
            this.toolStripProgressBarActA3.Name = "toolStripProgressBarActA3";
            this.toolStripProgressBarActA3.Size = new System.Drawing.Size(100, 57);
            // 
            // dgvPS
            // 
            this.dgvPS.AllowUserToAddRows = false;
            this.dgvPS.AllowUserToDeleteRows = false;
            this.dgvPS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPS.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPS.Location = new System.Drawing.Point(0, 60);
            this.dgvPS.Name = "dgvPS";
            this.dgvPS.ReadOnly = true;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvPS.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPS.Size = new System.Drawing.Size(773, 648);
            this.dgvPS.TabIndex = 2;
            // 
            // toolStripSincMarcasPS
            // 
            this.toolStripSincMarcasPS.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripSincMarcasPS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPS,
            this.toolStripSeparator2,
            this.toolStripButtonInsertarMarcasPS,
            this.toolStripSeparator3,
            this.toolStripLabelProgresoPS,
            this.toolStripProgressActPS});
            this.toolStripSincMarcasPS.Location = new System.Drawing.Point(0, 0);
            this.toolStripSincMarcasPS.Name = "toolStripSincMarcasPS";
            this.toolStripSincMarcasPS.Size = new System.Drawing.Size(773, 60);
            this.toolStripSincMarcasPS.TabIndex = 1;
            this.toolStripSincMarcasPS.Text = "toolStrip1";
            // 
            // toolStripButtonPS
            // 
            this.toolStripButtonPS.Image = global::klsync.Properties.Resources.marker20;
            this.toolStripButtonPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPS.Name = "toolStripButtonPS";
            this.toolStripButtonPS.Size = new System.Drawing.Size(137, 57);
            this.toolStripButtonPS.Text = "Cargar Marcas PS";
            this.toolStripButtonPS.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripButtonPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonPS.ToolTipText = "Prestashop";
            this.toolStripButtonPS.Click += new System.EventHandler(this.toolStripButtonPS_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripButtonInsertarMarcasPS
            // 
            this.toolStripButtonInsertarMarcasPS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonInsertarMarcasPS.Image")));
            this.toolStripButtonInsertarMarcasPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonInsertarMarcasPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInsertarMarcasPS.Name = "toolStripButtonInsertarMarcasPS";
            this.toolStripButtonInsertarMarcasPS.Size = new System.Drawing.Size(121, 57);
            this.toolStripButtonInsertarMarcasPS.Text = "Insertar Marcas";
            this.toolStripButtonInsertarMarcasPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButtonInsertarMarcasPS.ToolTipText = "Insertar las marcas que no existen en A3 pero sí en PS";
            this.toolStripButtonInsertarMarcasPS.Click += new System.EventHandler(this.toolStripButtonInsertarMarcasPS_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripLabelProgresoPS
            // 
            this.toolStripLabelProgresoPS.Name = "toolStripLabelProgresoPS";
            this.toolStripLabelProgresoPS.Size = new System.Drawing.Size(73, 57);
            this.toolStripLabelProgresoPS.Text = "Progreso";
            // 
            // toolStripProgressActPS
            // 
            this.toolStripProgressActPS.Name = "toolStripProgressActPS";
            this.toolStripProgressActPS.Size = new System.Drawing.Size(100, 57);
            // 
            // frSincronizarMarcas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1486, 708);
            this.Controls.Add(this.splitContainerSincMarcas);
            this.Name = "frSincronizarMarcas";
            this.Text = "Sincronizador de Marcas";
            this.splitContainerSincMarcas.Panel1.ResumeLayout(false);
            this.splitContainerSincMarcas.Panel1.PerformLayout();
            this.splitContainerSincMarcas.Panel2.ResumeLayout(false);
            this.splitContainerSincMarcas.Panel2.PerformLayout();
            this.splitContainerSincMarcas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).EndInit();
            this.toolStripSincMarcasA3.ResumeLayout(false);
            this.toolStripSincMarcasA3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).EndInit();
            this.toolStripSincMarcasPS.ResumeLayout(false);
            this.toolStripSincMarcasPS.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerSincMarcas;
        private System.Windows.Forms.ToolStrip toolStripSincMarcasA3;
        private System.Windows.Forms.ToolStripButton toolStripButtonA3;
        private System.Windows.Forms.ToolStrip toolStripSincMarcasPS;
        private System.Windows.Forms.ToolStripButton toolStripButtonPS;
        private System.Windows.Forms.DataGridView dgvA3;
        private System.Windows.Forms.DataGridView dgvPS;
        private System.Windows.Forms.ToolStripButton toolStripButtonActA3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabelProgresoA3;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarActA3;
        private System.Windows.Forms.ToolStripLabel toolStripLabelProgresoPS;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressActPS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButtonInsertarMarcasPS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}