﻿namespace klsync
{
    partial class frEnvioDocumentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frEnvioDocumentos));
            this.tsEnvioDocumentos = new System.Windows.Forms.ToolStrip();
            this.dgvEnvioDocumentos = new System.Windows.Forms.DataGridView();
            this.contextMenuStripEnvioDocumentos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aCCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelarENVIADOSeleccionadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.fILTROSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.documentosConEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripSeur = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.numFilas = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainerEnvioDoc = new System.Windows.Forms.SplitContainer();
            this.labelSelector = new System.Windows.Forms.Label();
            this.comboBoxSelector = new System.Windows.Forms.ComboBox();
            this.lblModeloFactura = new System.Windows.Forms.Label();
            this.comboModelo = new System.Windows.Forms.ComboBox();
            this.labelContainerEnvioDoc = new System.Windows.Forms.Label();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.lblFormatoFichero = new System.Windows.Forms.Label();
            this.lblCuerpoMensaje = new System.Windows.Forms.Label();
            this.lblAsuntoMensaje = new System.Windows.Forms.Label();
            this.tbMensaje = new System.Windows.Forms.TextBox();
            this.tbAsunto = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnvioDocumentos)).BeginInit();
            this.contextMenuStripEnvioDocumentos.SuspendLayout();
            this.statusStripSeur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEnvioDoc)).BeginInit();
            this.splitContainerEnvioDoc.Panel1.SuspendLayout();
            this.splitContainerEnvioDoc.Panel2.SuspendLayout();
            this.splitContainerEnvioDoc.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsEnvioDocumentos
            // 
            this.tsEnvioDocumentos.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsEnvioDocumentos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsEnvioDocumentos.Location = new System.Drawing.Point(0, 0);
            this.tsEnvioDocumentos.Name = "tsEnvioDocumentos";
            this.tsEnvioDocumentos.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.tsEnvioDocumentos.Size = new System.Drawing.Size(1326, 25);
            this.tsEnvioDocumentos.TabIndex = 0;
            this.tsEnvioDocumentos.Text = "toolStrip1";
            // 
            // dgvEnvioDocumentos
            // 
            this.dgvEnvioDocumentos.AllowUserToAddRows = false;
            this.dgvEnvioDocumentos.AllowUserToDeleteRows = false;
            this.dgvEnvioDocumentos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEnvioDocumentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEnvioDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEnvioDocumentos.ContextMenuStrip = this.contextMenuStripEnvioDocumentos;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEnvioDocumentos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEnvioDocumentos.Location = new System.Drawing.Point(13, 10);
            this.dgvEnvioDocumentos.Name = "dgvEnvioDocumentos";
            this.dgvEnvioDocumentos.ReadOnly = true;
            this.dgvEnvioDocumentos.Size = new System.Drawing.Size(909, 600);
            this.dgvEnvioDocumentos.TabIndex = 1;
            // 
            // contextMenuStripEnvioDocumentos
            // 
            this.contextMenuStripEnvioDocumentos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCCIONESToolStripMenuItem,
            this.toolStripMenuItem3,
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem,
            this.cancelarENVIADOSeleccionadosToolStripMenuItem,
            this.toolStripMenuItem1,
            this.fILTROSToolStripMenuItem,
            this.toolStripMenuItem2,
            this.documentosConEmailToolStripMenuItem});
            this.contextMenuStripEnvioDocumentos.Name = "contextMenuStripEnvioDocumentos";
            this.contextMenuStripEnvioDocumentos.Size = new System.Drawing.Size(251, 132);
            // 
            // aCCIONESToolStripMenuItem
            // 
            this.aCCIONESToolStripMenuItem.Enabled = false;
            this.aCCIONESToolStripMenuItem.Name = "aCCIONESToolStripMenuItem";
            this.aCCIONESToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.aCCIONESToolStripMenuItem.Text = "ACCIONES";
            this.aCCIONESToolStripMenuItem.Click += new System.EventHandler(this.aCCIONESToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(247, 6);
            // 
            // cancelarIMPRESOSeleccionadosToolStripMenuItem
            // 
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem.Name = "cancelarIMPRESOSeleccionadosToolStripMenuItem";
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem.Text = "Cancelar IMPRESO seleccionados";
            this.cancelarIMPRESOSeleccionadosToolStripMenuItem.Click += new System.EventHandler(this.cancelarIMPRESOSeleccionadosToolStripMenuItem_Click);
            // 
            // cancelarENVIADOSeleccionadosToolStripMenuItem
            // 
            this.cancelarENVIADOSeleccionadosToolStripMenuItem.Name = "cancelarENVIADOSeleccionadosToolStripMenuItem";
            this.cancelarENVIADOSeleccionadosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.cancelarENVIADOSeleccionadosToolStripMenuItem.Text = "Cancelar ENVIADO seleccionados";
            this.cancelarENVIADOSeleccionadosToolStripMenuItem.Click += new System.EventHandler(this.cancelarENVIADOSeleccionadosToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(247, 6);
            // 
            // fILTROSToolStripMenuItem
            // 
            this.fILTROSToolStripMenuItem.Enabled = false;
            this.fILTROSToolStripMenuItem.Name = "fILTROSToolStripMenuItem";
            this.fILTROSToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.fILTROSToolStripMenuItem.Text = "FILTROS";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(247, 6);
            // 
            // documentosConEmailToolStripMenuItem
            // 
            this.documentosConEmailToolStripMenuItem.Name = "documentosConEmailToolStripMenuItem";
            this.documentosConEmailToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.documentosConEmailToolStripMenuItem.Text = "Documentos con email";
            this.documentosConEmailToolStripMenuItem.Click += new System.EventHandler(this.documentosConEmailToolStripMenuItem_Click);
            // 
            // statusStripSeur
            // 
            this.statusStripSeur.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.numFilas});
            this.statusStripSeur.Location = new System.Drawing.Point(0, 714);
            this.statusStripSeur.Name = "statusStripSeur";
            this.statusStripSeur.Size = new System.Drawing.Size(1326, 22);
            this.statusStripSeur.TabIndex = 2;
            this.statusStripSeur.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(1274, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // numFilas
            // 
            this.numFilas.Name = "numFilas";
            this.numFilas.Size = new System.Drawing.Size(37, 17);
            this.numFilas.Text = "0 filas";
            // 
            // splitContainerEnvioDoc
            // 
            this.splitContainerEnvioDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerEnvioDoc.Location = new System.Drawing.Point(12, 82);
            this.splitContainerEnvioDoc.Name = "splitContainerEnvioDoc";
            // 
            // splitContainerEnvioDoc.Panel1
            // 
            this.splitContainerEnvioDoc.Panel1.Controls.Add(this.dgvEnvioDocumentos);
            // 
            // splitContainerEnvioDoc.Panel2
            // 
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.labelSelector);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.comboBoxSelector);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.lblModeloFactura);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.comboModelo);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.labelContainerEnvioDoc);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.txtFormat);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.lblFormatoFichero);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.lblCuerpoMensaje);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.lblAsuntoMensaje);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.tbMensaje);
            this.splitContainerEnvioDoc.Panel2.Controls.Add(this.tbAsunto);
            this.splitContainerEnvioDoc.Size = new System.Drawing.Size(1302, 629);
            this.splitContainerEnvioDoc.SplitterDistance = 936;
            this.splitContainerEnvioDoc.TabIndex = 3;
            // 
            // labelSelector
            // 
            this.labelSelector.AutoSize = true;
            this.labelSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelSelector.Location = new System.Drawing.Point(13, 10);
            this.labelSelector.Name = "labelSelector";
            this.labelSelector.Size = new System.Drawing.Size(124, 20);
            this.labelSelector.TabIndex = 10;
            this.labelSelector.Text = "Selector de tipo:";
            // 
            // comboBoxSelector
            // 
            this.comboBoxSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSelector.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxSelector.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSelector.FormattingEnabled = true;
            this.comboBoxSelector.Location = new System.Drawing.Point(15, 33);
            this.comboBoxSelector.Name = "comboBoxSelector";
            this.comboBoxSelector.Size = new System.Drawing.Size(297, 28);
            this.comboBoxSelector.TabIndex = 9;
            this.comboBoxSelector.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelector_SelectedIndexChanged);
            // 
            // lblModeloFactura
            // 
            this.lblModeloFactura.AutoSize = true;
            this.lblModeloFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblModeloFactura.Location = new System.Drawing.Point(13, 117);
            this.lblModeloFactura.Name = "lblModeloFactura";
            this.lblModeloFactura.Size = new System.Drawing.Size(128, 20);
            this.lblModeloFactura.TabIndex = 8;
            this.lblModeloFactura.Text = "Plantilla del PDF:";
            // 
            // comboModelo
            // 
            this.comboModelo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboModelo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboModelo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboModelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboModelo.FormattingEnabled = true;
            this.comboModelo.Location = new System.Drawing.Point(17, 140);
            this.comboModelo.Name = "comboModelo";
            this.comboModelo.Size = new System.Drawing.Size(297, 28);
            this.comboModelo.TabIndex = 7;
            this.comboModelo.SelectedIndexChanged += new System.EventHandler(this.comboModelo_SelectedIndexChanged);
            // 
            // labelContainerEnvioDoc
            // 
            this.labelContainerEnvioDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelContainerEnvioDoc.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.labelContainerEnvioDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelContainerEnvioDoc.Location = new System.Drawing.Point(19, 462);
            this.labelContainerEnvioDoc.Name = "labelContainerEnvioDoc";
            this.labelContainerEnvioDoc.Padding = new System.Windows.Forms.Padding(10);
            this.labelContainerEnvioDoc.Size = new System.Drawing.Size(293, 148);
            this.labelContainerEnvioDoc.TabIndex = 6;
            this.labelContainerEnvioDoc.Text = resources.GetString("labelContainerEnvioDoc.Text");
            // 
            // txtFormat
            // 
            this.txtFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtFormat.Location = new System.Drawing.Point(21, 433);
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.Size = new System.Drawing.Size(293, 26);
            this.txtFormat.TabIndex = 5;
            // 
            // lblFormatoFichero
            // 
            this.lblFormatoFichero.AutoSize = true;
            this.lblFormatoFichero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblFormatoFichero.Location = new System.Drawing.Point(19, 410);
            this.lblFormatoFichero.Name = "lblFormatoFichero";
            this.lblFormatoFichero.Size = new System.Drawing.Size(150, 20);
            this.lblFormatoFichero.TabIndex = 4;
            this.lblFormatoFichero.Text = "Formato del fichero:";
            // 
            // lblCuerpoMensaje
            // 
            this.lblCuerpoMensaje.AutoSize = true;
            this.lblCuerpoMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblCuerpoMensaje.Location = new System.Drawing.Point(17, 223);
            this.lblCuerpoMensaje.Name = "lblCuerpoMensaje";
            this.lblCuerpoMensaje.Size = new System.Drawing.Size(154, 20);
            this.lblCuerpoMensaje.TabIndex = 3;
            this.lblCuerpoMensaje.Text = "Cuerpo del mensaje:";
            // 
            // lblAsuntoMensaje
            // 
            this.lblAsuntoMensaje.AutoSize = true;
            this.lblAsuntoMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblAsuntoMensaje.Location = new System.Drawing.Point(13, 171);
            this.lblAsuntoMensaje.Name = "lblAsuntoMensaje";
            this.lblAsuntoMensaje.Size = new System.Drawing.Size(153, 20);
            this.lblAsuntoMensaje.TabIndex = 2;
            this.lblAsuntoMensaje.Text = "Asunto del mensaje:";
            // 
            // tbMensaje
            // 
            this.tbMensaje.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbMensaje.Location = new System.Drawing.Point(19, 246);
            this.tbMensaje.Multiline = true;
            this.tbMensaje.Name = "tbMensaje";
            this.tbMensaje.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMensaje.Size = new System.Drawing.Size(295, 161);
            this.tbMensaje.TabIndex = 1;
            // 
            // tbAsunto
            // 
            this.tbAsunto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAsunto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tbAsunto.Location = new System.Drawing.Point(17, 194);
            this.tbAsunto.Name = "tbAsunto";
            this.tbAsunto.Size = new System.Drawing.Size(295, 26);
            this.tbAsunto.TabIndex = 0;
            // 
            // frEnvioDocumentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1326, 736);
            this.Controls.Add(this.splitContainerEnvioDoc);
            this.Controls.Add(this.statusStripSeur);
            this.Controls.Add(this.tsEnvioDocumentos);
            this.Name = "frEnvioDocumentos";
            this.Text = "Envío de Documentos";
            this.Load += new System.EventHandler(this.frEnvioDocumentos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnvioDocumentos)).EndInit();
            this.contextMenuStripEnvioDocumentos.ResumeLayout(false);
            this.statusStripSeur.ResumeLayout(false);
            this.statusStripSeur.PerformLayout();
            this.splitContainerEnvioDoc.Panel1.ResumeLayout(false);
            this.splitContainerEnvioDoc.Panel2.ResumeLayout(false);
            this.splitContainerEnvioDoc.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEnvioDoc)).EndInit();
            this.splitContainerEnvioDoc.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsEnvioDocumentos;
        private System.Windows.Forms.DataGridView dgvEnvioDocumentos;
        private System.Windows.Forms.StatusStrip statusStripSeur;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel numFilas;
        private System.Windows.Forms.SplitContainer splitContainerEnvioDoc;
        private System.Windows.Forms.TextBox tbMensaje;
        private System.Windows.Forms.TextBox tbAsunto;
        private System.Windows.Forms.Label lblCuerpoMensaje;
        private System.Windows.Forms.Label lblAsuntoMensaje;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.Label lblFormatoFichero;
        private System.Windows.Forms.Label labelContainerEnvioDoc;
        private System.Windows.Forms.Label lblModeloFactura;
        private System.Windows.Forms.ComboBox comboModelo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripEnvioDocumentos;
        private System.Windows.Forms.ToolStripMenuItem cancelarIMPRESOSeleccionadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelarENVIADOSeleccionadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem documentosConEmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem fILTROSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.Label labelSelector;
        private System.Windows.Forms.ComboBox comboBoxSelector;
    }
}