﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace klsync.Mimasa
{
    public partial class frMimasa : Form
    {
        private static frMimasa m_FormDefInstance;
        public static frMimasa DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frMimasa();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        Mimasa.csMimasa mimasa = new Mimasa.csMimasa();
        Mimasa.csScriptsMimasa scriptMimasa = new csScriptsMimasa();
        public frMimasa()
        {
            InitializeComponent();
        }



        private void cargarMovimientos(string codart, string lote)
        {
            csUtilidades.cargarDGV(dgvMovStock, scriptMimasa.selectMovStockArtLote(codart, lote, filtroTipoMovs()), false);
        }

        private string filtroTipoMovs()
        {
            //Añado un valor XX para así añadir la coma a todos los valores que añadamos
            string filtroTipoMovs = " AND TIPDOC IN ('XX'";

            filtroTipoMovs = cboxAC.Checked ? filtroTipoMovs + ",'AC'" : filtroTipoMovs;
            filtroTipoMovs = cboxFC.Checked ? filtroTipoMovs + ",'FC'" : filtroTipoMovs;
            filtroTipoMovs = cboxAV.Checked ? filtroTipoMovs + ",'AV'" : filtroTipoMovs;
            filtroTipoMovs = cboxFV.Checked ? filtroTipoMovs + ",'FV'" : filtroTipoMovs;
            filtroTipoMovs = cboxRE.Checked ? filtroTipoMovs + ",'RE'" : filtroTipoMovs;
            filtroTipoMovs = cboxTR.Checked ? filtroTipoMovs + ",'TR'" : filtroTipoMovs;
            filtroTipoMovs = cboxPR.Checked ? filtroTipoMovs + ",'PR'" : filtroTipoMovs;
            filtroTipoMovs = cboxIN.Checked ? filtroTipoMovs + ",'IN'" : filtroTipoMovs;
            filtroTipoMovs = filtroTipoMovs + ") ";

            return filtroTipoMovs;
        }

        private void dgvOrdFab_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string codart = dgvOrdFab.CurrentRow.Cells["CODART"].Value.ToString().Trim();
            string lote = dgvOrdFab.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
            cargarMovimientos(codart, lote);
        }

        private void dgvMimasa_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMimasa.CurrentRow.Index >= 0)
            {
                string codart = dgvMimasa.CurrentRow.Cells["CODART"].Value.ToString().Trim();
                string lote = dgvMimasa.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
                cargarMovimientos(codart, lote);
            }
        }

        private void dgvMovStock_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvMovStock.CurrentRow.Index >= 0)
            {
                string codart = dgvMovStock.CurrentRow.Cells["CODART"].Value.ToString().Trim();
                string lote = dgvMovStock.CurrentRow.Cells["LOTE"].Value.ToString().Trim();
                string tipoDoc = dgvMovStock.CurrentRow.Cells["TIPDOC"].Value.ToString();
                string idDoc = dgvMovStock.CurrentRow.Cells["IDENTIFICADOR"].Value.ToString();

                if (tipoDoc == "AC")
                {
                    if (dgvOrdFab.Rows.Count == 0)
                    {
                        dgvOrdFab.Height = 25;
                    }

                    csUtilidades.cargarDGV(dgvAlbCompra, scriptMimasa.selectAlbCompras(idDoc,codart, lote), false);
                    csUtilidades.cargarDGV(dgvFactCompra, scriptMimasa.selectFactCompras(codart, lote), false);

                    dgvFactCompra.Refresh();
                    if (dgvFactCompra.Rows.Count>0)
                    {
                        string idFraCompra = dgvFactCompra.Rows[0].Cells["IDFACC"].Value.ToString();
                        csUtilidades.cargarDGV(dgvFactCompraAsoc, scriptMimasa.selectFacturasAsociadas(idFraCompra), false);
                    }

                }
                if (tipoDoc == "PR")
                {
                    csUtilidades.cargarDGV(dgvOrdFab, scriptMimasa.selectOrdFab(idDoc, codart), false);

                }
            }
        }

        private void dgvMimasa_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvMimasa.Rows)
            {
                height += dr.Height;
            }

            dgvMimasa.Height = height;

            lblMovStock.Location = new Point(3, dgvMimasa.Location.Y + dgvMimasa.Height + 30);
            int posicionDgvMovimientos = lblMovStock.Location.Y;
            dgvMovStock.Location = new Point(3,  posicionDgvMovimientos + 20);

            lblOrdFab.Location = new Point(3, dgvMovStock.Location.Y + dgvMovStock.Height + 30);
            dgvOrdFab.Location= new Point(3,  lblOrdFab.Location.Y + 20);
            ajustarPosicionesDGV();

        }

        private void dgvMovStock_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvMovStock.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvMovStock.Height = height;

            lblOrdFab.Location = new Point(3, dgvMovStock.Location.Y + dgvMovStock.Height + 30);
            dgvOrdFab.Location = new Point(3, lblOrdFab.Location.Y + 20);

            ajustarPosicionesDGV();
        }

        private void dgvOrdFab_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvOrdFab.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvOrdFab.Height = height;

            ajustarPosicionesDGV();
        }

        private void ajustarPosicionDgvAlbCompra()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvAlbCompra.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvAlbCompra.Height = height;

            lblAlbCompra.Location = new Point(3, dgvOrdFab.Location.Y + dgvOrdFab.Height + 30);
            dgvAlbCompra.Location = new Point(3, lblAlbCompra.Location.Y + 20);
        }

        private void ajustarPosicionDgvFactCompra()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvFactCompra.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvFactCompra.Height = height;

            lblFactCompra.Location = new Point(3, dgvAlbCompra.Location.Y + dgvAlbCompra.Height + 30);
            dgvFactCompra.Location = new Point(3, lblFactCompra.Location.Y + 20);
        }

        private void ajustarPosicionDgvFactCompraAsoc()
        {
            var height = 50;
            foreach (DataGridViewRow dr in dgvFactCompraAsoc.Rows)
            {
                height += dr.Height;
            }

            height = height > 250 ? 250 : height;
            dgvFactCompraAsoc.Height = height;

            lblFactCompraAsoc.Location = new Point(3, dgvFactCompra.Location.Y + dgvFactCompra.Height + 30);
            dgvFactCompraAsoc.Location = new Point(3, lblFactCompraAsoc.Location.Y + 20);
        }

        private void ajustarPosicionesDGV()
        {
            ajustarPosicionDgvAlbCompra();
            ajustarPosicionDgvFactCompra();
            ajustarPosicionDgvFactCompraAsoc();
        }

        private void dgvAlbCompra_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvAlbCompra();
            ajustarPosicionesDGV();
        }

        private void dgvFactCompra_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvFactCompra();
            ajustarPosicionesDGV();
        }

        private void dgvFactCompraAsoc_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            ajustarPosicionDgvFactCompraAsoc();
            ajustarPosicionesDGV();
        }

        private void btnLoadProducts_Click(object sender, EventArgs e)
        {
            dgvAlbCompra.DataSource = null;
            dgvFactCompra.DataSource = null;
            dgvFactCompraAsoc.DataSource = null;
            csUtilidades.cargarDGV(dgvMimasa, scriptMimasa.selectArticulosLote(rbCargaBasica.Checked, cboxArticulos.SelectedValue.ToString(),rbStockDif0.Checked ,false), false);
        }

        private void cargarArticulos()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS CODART,'' AS DESCART " +
                                                        " UNION " +
                                                        " SELECT LTRIM(CODART), LTRIM(CODART) + '-' + DESCART FROM ARTICULO " +
                                                        " ORDER BY CODART ", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxArticulos.ValueMember = "CODART";
                cboxArticulos.DisplayMember = "DESCART";
                cboxArticulos.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void cargarProveedores()
        {
            try
            {

                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter("SELECT '0' AS CODPRO,'' AS NOMPRO " +
                                                        " UNION " +
                                                        " SELECT LTRIM(CODPRO), LTRIM(CODPRO) + '-' + NOMPRO FROM PROVEED ", dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxProveedores.ValueMember = "CODPRO";
                cboxProveedores.DisplayMember = "NOMPRO";
                cboxProveedores.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void cargarFacturas(string idProv)
        {
            try
            {
                string script = "SELECT " +
                    " CAST(IDFACC as int) as IDFACC, CAST(CAST(NUMDOC AS int) AS varchar) + ' - ' + NOMPRO + ' - ' + convert(VARCHAR(10),FECHA,103) + ' - ' + REFERENCIA AS FACTURA " +
                    " FROM CABEFACC " +
                    " WHERE LTRIM(CODPRO)='" + idProv +  "' ORDER BY FECHA DESC";
                SqlConnection dataConnection = new SqlConnection();
                csSqlConnects sqlConnect = new csSqlConnects();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;
                dataConnection.Open();
                SqlDataAdapter a = new SqlDataAdapter(script, dataConnection);
                DataTable t1 = new DataTable();
                a.Fill(t1);

                cboxDocs.ValueMember = "IDFACC";
                cboxDocs.DisplayMember = "FACTURA";
                cboxDocs.DataSource = t1;

                dataConnection.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void frMimasa_Load(object sender, EventArgs e)
        {
            cargarArticulos();
            cargarProveedores();
        }

        private void cboxArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvMimasa.DataSource = null;
            dgvMovStock.DataSource = null;
            dgvOrdFab.DataSource = null;
            dgvAlbCompra.DataSource = null;
            dgvFactCompra.DataSource = null;
            dgvFactCompraAsoc.DataSource = null;
            csUtilidades.cargarDGV(dgvMimasa, scriptMimasa.selectArticulosLote(rbCargaBasica.Checked, cboxArticulos.SelectedValue.ToString(), rbStockDif0.Checked, false), false);
        }

        private void btnLoadDocs_Click(object sender, EventArgs e)
        {
            string doc = cboxDocs.SelectedValue.ToString();
            csUtilidades.cargarDGV(dgvDocsCompraMP, scriptMimasa.selectFactComprasMP(doc), false);
            csUtilidades.cargarDGV(dgvDocsCompraAsoc, scriptMimasa.selectFactComprasMP(null, doc), false);
            csUtilidades.cargarDGV(dgvImputacion, scriptMimasa.selectImportesImputar(doc), false);
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            double totalDocumento = 0;
            double importeUnitario = 0;
            double unidadesCalculo = 0;
            double importeDocsAImputar = 0;
            double importeArticuloImputar = 0;
            double pesoReferenciaArticulo = 0;
            string textBox = "";
            double costeAdicional = 0;
            string codartMP = "";
            string codartIMP = "";      //Articulo tabla imputación

            //Calculo las unidades a imputar
            for (int i = 0; i < dgvDocsCompraMP.Rows.Count; i++)
            {
                totalDocumento = totalDocumento + Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["COEF_REPARTO"].Value.ToString());
            }

            //Calculo Importes
            for (int i = 0; i < dgvImputacion.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(dgvImputacion.Rows[i].Cells["CODART"].Value.ToString()))
                {
                    importeDocsAImputar = importeDocsAImputar + Convert.ToDouble(dgvImputacion.Rows[i].Cells["IMPORTE"].Value.ToString());
                }
            }

            importeUnitario = importeDocsAImputar / totalDocumento;
            importeUnitario = Math.Round(importeUnitario, 2);

            textBox = "REPARTO ENTRE " + totalDocumento.ToString() + "\r\n" +
                "IMPORTE UNITARIO POR Udad: " + importeUnitario.ToString() + "\n";

            tbResumenAcciones.Text = textBox;

            for (int i = 0; i < dgvDocsCompraMP.Rows.Count; i++)
            {
                codartMP = dgvDocsCompraMP.Rows[i].Cells["CODART"].Value.ToString();
                unidadesCalculo = Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["UNIDADES"].Value.ToString());

                for (int ii = 0; ii < dgvImputacion.Rows.Count; ii++)
                {
                    codartIMP= string.IsNullOrEmpty(dgvImputacion.Rows[ii].Cells["CODART"].Value.ToString()) ? "" : dgvImputacion.Rows[ii].Cells["CODART"].Value.ToString();
                    if (codartIMP == codartMP)
                    {
                        importeArticuloImputar = Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["UNIDADES"].Value.ToString());
                    }
                }
                pesoReferenciaArticulo = Convert.ToDouble(dgvDocsCompraMP.Rows[i].Cells["PESO"].Value.ToString());
                costeAdicional = Math.Round(( importeUnitario) + (importeArticuloImputar/unidadesCalculo), 5);
                costeAdicional = Math.Round(costeAdicional * pesoReferenciaArticulo,5);
                dgvDocsCompraMP.Rows[i].Cells["COSTEADIC"].Value = costeAdicional;
                importeArticuloImputar = 0;
            }

        }

        private void dgvDocsCompraMP_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
           

            if (this.dgvDocsCompraMP.Columns[e.ColumnIndex].Name == "COSTEADIC")
            {
                e.CellStyle.ForeColor = Color.Red;
                e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            //if (this.dgvStock.Columns[e.ColumnIndex].Name == "StockPS")
            //{
            //    try
            //    {
            //        e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //        cantidadPS = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockPS"].Value);
            //        cantidadA3 = Convert.ToInt32(this.dgvStock.Rows[e.RowIndex].Cells["StockA3"].Value);
            //        if (cantidadA3 != cantidadPS)
            //        {

            //            e.CellStyle.ForeColor = Color.Red;
            //        }

            //    }
            //    catch (Exception ex)
            //    { }
            //}
        }

        private void cboxProveedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            string codProveedor = cboxProveedores.SelectedValue.ToString();
            cargarFacturas(codProveedor);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveCosteAdicional_Click(object sender, EventArgs e)
        {
            DialogResult dialogo = MessageBox.Show("¿Se van a modificar datos en los costes de artículo, ¿Está seguro?", "Actualizar Costes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dialogo == DialogResult.Yes)
            {
                string codart = "";
                string lote = "";
                string fecCaduc = "";
                double prcMedio = 0;
                double costeAdic = 0;
                string idFacC = "";


                foreach (DataGridViewRow fila in dgvDocsCompraMP.Rows)
                {
                    codart = fila.Cells["CODART"].Value.ToString().Trim();
                    lote = fila.Cells["LOTE"].Value.ToString().Trim();
                    costeAdic = Convert.ToDouble(fila.Cells["COSTEADIC"].Value.ToString());
                    prcMedio = Convert.ToDouble(fila.Cells["PRECIO"].Value.ToString()) + costeAdic;
                    idFacC = fila.Cells["IDFACC"].Value.ToString().Replace(",0000", "");


                    csUtilidades.ejecutarConsulta("UPDATE LINEFACT SET COSTEADIC=" + costeAdic.ToString().Replace(",", ".") + " WHERE IDFACC=" + idFacC + " AND LTRIM(CODART) = '" + codart + "' AND LTRIM(LOTE) = '" + lote + "'", false);
                    csUtilidades.ejecutarConsulta("UPDATE STOCKALM SET PRCMEDIO=" + prcMedio.ToString().Replace(",", ".") + " WHERE LTRIM(CODART)='" + codart + "' AND LTRIM(LOTE)='" + lote + "'", false);

                }
            }

            "Proceso finalizado".mb();


        }
    }
}
