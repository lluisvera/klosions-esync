﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32.TaskScheduler;

namespace klsync
{
    class csTasks
    {
        public bool addTask(string name, string path2file, string args, string path, double repetir, DateTime fecha, double hora, double minuto)
        {
            using (TaskService ts = new TaskService())
            {
                bool check = false;

                try
                {
                    // Create a new task definition and assign properties
                    TaskDefinition td = ts.NewTask();
                    td.RegistrationInfo.Description = "Esync";
                    td.Settings.Enabled = true;
                    td.Settings.StartWhenAvailable = true;
                    td.Principal.LogonType = TaskLogonType.InteractiveToken;
                    td.Principal.RunLevel = TaskRunLevel.Highest;

                    // Add a trigger that will fire the task at this time every other day
                    DailyTrigger dt = (DailyTrigger)td.Triggers.Add(new DailyTrigger());
                    //dt.Repetition.Duration = TimeSpan.FromHours(4);
                    dt.Repetition.Interval = TimeSpan.FromMinutes(repetir);
                    dt.StartBoundary = Convert.ToDateTime(fecha.ToShortDateString()) + TimeSpan.FromHours(hora) + TimeSpan.FromMinutes(minuto);
                    
                    // Add an action
                    td.Actions.Add(new ExecAction(path2file, csGlobal.conexionDB + args, path));

                    // Register the task in the root folder
                    ts.RootFolder.RegisterTaskDefinition(name, td);

                    check = true;
                }
                catch (Exception ex)
                {
                    Program.guardarErrorFichero("Programando una tarea - " + ex.ToString());
                }

                return check;
            }
        }

        public TreeView getTasks()
        {
            using (TaskService ts = new TaskService())
            {
                TreeView tv = new TreeView();
                tv.Nodes.Clear();
                TaskFolder tf = ts.RootFolder;
                foreach (Task tc in tf.Tasks)
                {
                    TreeNode parent = new TreeNode();
                    tv.Nodes.Add(parent);
                    parent.Text = tc.Name;
                    foreach (Trigger trg in tc.Definition.Triggers)
                    {
                        TreeNode desencadenador = new TreeNode();
                        desencadenador.Text = trg.ToString();
                        parent.Nodes.Add(desencadenador);
                    }
                    foreach (Microsoft.Win32.TaskScheduler.Action act in tc.Definition.Actions)
                    {
                        TreeNode accion = new TreeNode();
                        accion.Text = act.ToString();
                        parent.Nodes.Add(accion);
                    }
                }



                return tv;
            }
        }
    }
}
