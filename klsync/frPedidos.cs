﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.IO;

namespace klsync
{
       public partial class frPedidos : Form
    {
        private static frPedidos m_FormDefInstance;
        public static frPedidos DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frPedidos();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frPedidos()
        {
            InitializeComponent();

            if (csGlobal.conexionDB.Contains("SGI"))
            {
                abrirCarpetaToolStripMenuItem.Visible = false;
                procesoAlbaranToolStripMenuItem.Visible = false;
                servirAAlbaránToolStripMenuItem.Visible = false;
                toolStripMenuItem1.Visible = false;
            }
            else
            {
                exportarAFicheroXMLToolStripMenuItem.Visible = false;
                toolStripMenuItem1.Visible = false;
            }
        }

        private void cargarPedidos(string fechaDesde, string fechaHasta)
        {
            csSqlConnects sqlconnect = new csSqlConnects();
            dgvPedidos.DataSource = sqlconnect.obtenerCabeceraPedidosV(fechaDesde, fechaHasta);
        }

        private void cargarDocumentos(string fechaDesde, string fechaHasta, string numDoc)
        {
            if (rbtAlbaranes.Checked)
            {
                cboxDocs.Items.Clear();
                string numAlb = "";
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;

                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectOrders(fechaDesde, fechaHasta, numDoc), dataConnection);
                DataTable t = new DataTable();

                a.Fill(t);
                dgvPedidos.DataSource = t;
                if (dgvPedidos.Rows.Count > 0)
                {
                    //frPedidos.ActiveForm.Text = "Pedidos: " + dgvPedidos.RowCount.ToString() + " filas";
                    for (int i = 0; i < dgvPedidos.Rows.Count; i++)
                    {
                        //numAlb = dgvPedidos.Rows[i].Cells[1].Value.ToString();
                        if (numAlb != dgvPedidos.Rows[i].Cells[1].Value.ToString())
                        {
                            numAlb = dgvPedidos.Rows[i].Cells[1].Value.ToString();

                            if (numAlb != "")
                            {
                                cboxDocs.Items.Add(numAlb.Replace(",0000", ""));
                            }
                        }
                    }
                }
            }
            else if (rbtPedidos.Checked)
            {

                cargarPedidos(fechaDesde, fechaHasta);
            }
        }

        private void btnPedidos_Click(object sender, EventArgs e)
        {
            cargarDocumentos(dtpDesde.Value.ToShortDateString(), dtpHasta.Value.ToShortDateString(), cboxDocs.Text);

        }




        public string generarTxtFileAlmacen(DataSet Documentos, string numAlb)
        {
            try
            {
                DataTable Cabecera = Documentos.Tables["cabeceraXML"];
                DataTable Lineas = Documentos.Tables["lineasXML"];
                csSqlConnects sql = new csSqlConnects();

                string cabecera = "";
                string linea = "";
                string tercero = "";
                string numero_pedido = "";
                int numLinea = 1;

                //CAMPOS REGISTRO TERCERO EXPEDICION TRTER
                string TIPREG_CLI = "TE  ";         //tipo de Registro (4)
                string ACCION_CLI = "";         //Tipo de Acción: AG= Agregar, MO= Modificar, DE= Destruir. (2)
                string TERREF_0_CLI = "B5852";         //Referencia del tercero (5 de 16)
                string TERREF_1_CLI = "";         //Referencia del tercero (11 de 16)
                string TERDES = "";         //Nombre del tercero    (40)
                string TERPRIREF = "";      //Referencia del tercero principal. (16) = TERREF_0 + TERREF_1
                string ALIAS = "";          //Alias (12)
                string TERORI = "CLTE";         //Origen del Tercero (4)
                string DIR1 = "";           //Literal 1 de dirección de envío.  (40)
                string DIR2 = "";           //Literal 2 de dirección de envío.  (40)
                string DIR3 = "";           //Literal 3 de dirección de envío.  (40)
                string DIR4 = "";           //Literal 4 de dirección de envío.  (40)
                string POSCOD = "";         //Código Postal (6)
                string PAISCOD = "";        //Codigo Pais (4)    
                string PERAJU = "N";        //Permite pequeños ajustes en las cantidades solicitadas para ajustar a contenedores en las expediciones. (1) S/N
                string CNTEXPTIP = "";      //Tipo contenedor PL / RL (4)
                string NUMTELSMS = "";      //Teléfono (80)
                string NUMTELFAX = "";      //Fax (80)
                string EMAILD = "";         //Email (80)
                string CNTEXPTIPCAJ = "";   //Tipo de Caja contenedora (4)
                string POSCODINT = "";      //codigo Postal internacional (8)






                //CAMPOS REGISTRO EXPEDICIÓN CABECERA
                string TIPREG = "OECA";           //tipo de Registro (4)
                string ACCION = "AG";             //Tipo de Acción: AG= Agregar, MO= Modificar, DE= Destruir. (2)
                string EXPORDREF_0 = "B5852";    //Prefijo Referencia de la Orden de Expedicion (5) (de 15)
                string EXPORDREF_1 = numAlb;      //Referencia de la Orden de Expedicion (10) (de 15) NÚMERO DEL PEDIDO
                string ALMREF = "01";            //Almacén que debe expedir la mercancia (2)
                string TERREF_0 = "B5852";              //Referencia del Destinatario de la Mercancía (16)
                string TERREF_1 = "";              //Referencia del Destinatario de la Mercancía (16)
                string TERPROREF = "B5852";      //Referencia del Tercero propietario de la mercancía solicitada (en el caso de operadores logísticos) (16)
                string EXPORDTIP = "    ";       //Tipo de la Orden de Expedición (A pactar con el cliente). (4)
                string EXPORDSUBTIP = "    ";    //Subtipo de la Orden de Expedición (A pactar con el cliente). (4)
                string TRAREF = "";      //Referencia del transportista que se llevará la mercancía preparada a destino. (16)
                string FECORD = "";      //  Fecha de emisión de la Orden de Expedición. (YYYYMMDD)
                string FECEXP = "";      //Fecha de expedición de la mercancía, si está planificada (YYYYMMDD)
                string HOREXP = "0000";      //  Hora de expedición de la mercancía, si está planificada (HHMI)
                string URGENT = "N";      //  Orden Urgente. (S/N) (1)
                string OBS = "";         //Observaciones sobre la Orden de Expedición (40)
                string MUEEXP = "";      //Referencia del Muelle del mapa de almacén ADAIA (6)
                string PRVCAMCOM = "";   //  Campo comodin (160)
                string AGROEXGENACC = "";    //Código de agrupación de órdenes de expedición (16)
                string NUMVER = "0";      //Campo que indica si se puede modificar o no la Orden de Expedición desde ADAIA. 0- No modificable, 1- Modificable.

                //CAMPOS REGISTRO EXPEDICIÓN LINEAS

                string TIPREG_LIN = "OELI";      //tipo de Registro (4)
                //string ACCION ="";   Se repite de la cabecera
                string EXPORDREF_LIN_0 = EXPORDREF_0;    //Prefijo Referencia de la Orden de Expedicion (5) (de 15)
                string EXPORDREF_LIN_1 = EXPORDREF_1;     //Referencia de la Orden de Expedicion (10) (de 15)
                string EXPORDLIN = "";               //Número de línea de la Orden de Expedición. (9)
                string ARTREF = "";                  //Referencia del Artículo pedido. (16)
                string CANPED = "";                  //Cantidad pedida (en unidades mínimas). (9)
                string TIPUNI = "";                  //Tipo de unidad en la que está expresada la cantidad pedida (UD). (4)
                string NUMUNI = "";                  //Número de unidades mínimas o básicas que contiene la unidad indicada. (8)
                string PESPED = "";                  //Peso de la mercancía pedida en Kg.(9) --> 6 ENTEROS 3 DECIMALES
                string PREVEN = "";                  //Precio de venta de la mercancía (informativo) (9) --> 7 ENTEROS 2 DECIMALES
                string OBS_LIN = "";                 //Observaciones/Especificaciones (40)
                string LOTE = "";                    //Lote, en el caso de Gestión de Articulo Concreto por Lote. (20)


                //Campo de Relleno
                char padZero = '0';
                char padSpace = ' ';

                //FIN CAMPOS







                using (StreamWriter writeTextFile = new StreamWriter("c:\\Albaranes\\TREXPORD. " + numAlb.PadLeft(10, padZero) + ".txt"))
                {
                    foreach (DataRow row in Cabecera.Rows)
                    {
                        if (numero_pedido != row[1].ToString())
                        {
                            //GENERACION CABECERA
                            numLinea = 1;
                            EXPORDREF_1 = EXPORDREF_1.PadLeft(10, padZero);
                            TERREF_1 = row["CODCLI"].ToString().PadLeft(11, padSpace);
                            TERPROREF = TERPROREF.PadLeft(16, padSpace);
                            TRAREF = TRAREF.PadLeft(16, padZero);
                            TERPROREF = TERPROREF.PadLeft(16, padZero);
                            //conversión de fecha
                            DateTime fecha = DateTime.Parse(row["FechaPedido"].ToString());
                            FECORD = fecha.ToString("yyyyMMdd");
                            //conversión de fecha
                            DateTime fechaExp = DateTime.Parse(row["FechaPedido"].ToString());
                            FECEXP = fechaExp.ToString("yyyyMMdd");

                            OBS = OBS.PadLeft(40, padZero);
                            MUEEXP = MUEEXP.PadLeft(6, padSpace);
                            PRVCAMCOM = PRVCAMCOM.PadLeft(160, padSpace);
                            AGROEXGENACC = AGROEXGENACC.PadLeft(160, padSpace);

                            cabecera = TIPREG + ACCION + EXPORDREF_0 + EXPORDREF_1 + ALMREF + TERREF_0 + TERREF_1 + TERPROREF + EXPORDTIP + EXPORDSUBTIP + TRAREF + FECORD + FECEXP + HOREXP + URGENT + OBS + MUEEXP + PRVCAMCOM + AGROEXGENACC + NUMVER;
                            writeTextFile.WriteLine(cabecera);
                            numero_pedido = row[1].ToString();


                            foreach (DataRow detalle in Lineas.Rows)
                            {
                                EXPORDLIN = numLinea.ToString();

                                EXPORDLIN = detalle["Linea"].ToString();
                                EXPORDREF_LIN_0 = EXPORDREF_0;
                                EXPORDREF_LIN_1 = EXPORDREF_1;
                                EXPORDLIN = EXPORDLIN.PadLeft(9, padZero);
                                numLinea++;

                                ARTREF = detalle["CodigoArticulo"].ToString();
                                ARTREF = ARTREF.PadLeft(16, padZero);
                                CANPED = detalle["Cantidad"].ToString();
                                CANPED = CANPED.PadLeft(9, padZero);
                                TIPUNI = "  UD";
                                NUMUNI = detalle["Cantidad"].ToString();
                                NUMUNI = NUMUNI.PadLeft(8, padZero);
                                PESPED = "000000000";
                                PREVEN = "000000000";
                                OBS = OBS.PadLeft(40, padSpace);
                                LOTE = LOTE.PadLeft(20, padSpace);


                                linea = TIPREG_LIN + ACCION + EXPORDREF_LIN_0 + EXPORDREF_LIN_1 + EXPORDLIN + ARTREF + CANPED + TIPUNI + NUMUNI + PESPED + PREVEN + OBS + LOTE;
                                writeTextFile.WriteLine(linea);

                            }

                        }
                    }
                    //return linea;
                }

                using (StreamWriter writeTextFileCustomer = new StreamWriter("c:\\Albaranes\\TRTER. " + numAlb.PadLeft(10, padZero) + ".txt"))
                {

                    foreach (DataRow row in Cabecera.Rows)
                    {

                            //GENERACIÓN FICHERO TERCERO
                            TIPREG_CLI = "TE  ";
                            ACCION_CLI = "AG";
                            TERREF_1_CLI = row["CODCLI"].ToString().PadLeft(11, padSpace);
                            TERDES = row["nombreDestinatario"].ToString().PadLeft(40, padSpace);
                            TERPRIREF = TERREF_0_CLI + TERREF_1_CLI;
                            ALIAS = ALIAS.PadLeft(12, padSpace);
                            TERORI = "CLTE";
                            DIR1 = row["DireccionDestinatario"].ToString().PadLeft(40, padSpace);
                            DIR2 = row["PoblacionDestinatario"].ToString().PadLeft(40, padSpace);
                            DIR3 = row["Provincia"].ToString().PadLeft(40, padSpace);
                            DIR4 = DIR4.PadLeft(40, padSpace);
                            POSCOD = row["CPostalDestinatario"].ToString().PadLeft(6, padSpace);
                            PAISCOD = "ES  ";
                            PERAJU = "N";
                            CNTEXPTIP = "    ";
                            NUMTELSMS = row["TELCLI"].ToString().PadLeft(80, padSpace);
                            NUMTELFAX = NUMTELFAX.PadLeft(80, padSpace);
                            EMAILD = row["EMAIL"].ToString().PadLeft(80, padSpace);
                            CNTEXPTIPCAJ = CNTEXPTIPCAJ.PadLeft(4, padSpace);
                            POSCODINT = CNTEXPTIPCAJ.PadLeft(4, padSpace);

                            tercero = TIPREG_CLI + ACCION_CLI + TERREF_0_CLI + TERREF_1_CLI + TERDES + TERPRIREF + ALIAS + TERORI + DIR1 +
                              DIR2 + DIR3 + DIR4 + POSCOD + PAISCOD + PERAJU + CNTEXPTIP + NUMTELSMS +
                              NUMTELFAX + EMAILD + CNTEXPTIPCAJ + POSCODINT;

                            writeTextFileCustomer.WriteLine(tercero);

                    }
                }
                return linea;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

                return null;
            }
        }




        public string generarXMLAlmacen(DataSet Documentos,string numAlb)
        {
            try
            {
                StringWriter stream = new StringWriter();

                #region SGI
                if (csGlobal.conexionDB.ToUpper().Contains("SGI"))
                {
                    DataTable Cabecera = Documentos.Tables["cabeceraXML"];
                    DataTable Lineas = Documentos.Tables["lineasXML"];
                    csSqlConnects sql = new csSqlConnects();

                    string numero_pedido = "";

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.IndentChars = "  ";
                    settings.NewLineChars = "\r\n";
                    settings.NewLineHandling = NewLineHandling.Replace;
                    settings.Encoding = Encoding.UTF8;

                    XmlWriter writer = XmlWriter.Create(stream, settings);
                    writer.WriteStartElement("Pedidos");

                    foreach (DataRow row in Cabecera.Rows)
                    {
                        if (numero_pedido != row[0].ToString())
                        {
                            numero_pedido = row[0].ToString();
                            writer.WriteStartElement("Pedido");
                            writer.WriteElementString("numDoc", numAlb);
                            writer.WriteElementString("fecha", row["fecha"].ToString());
                            writer.WriteElementString("codcli", row["codCli"].ToString().Trim());
                            writer.WriteElementString("nomcli", row["nomCliente"].ToString());
                            writer.WriteElementString("nifcli", row["nifCli"].ToString());
                            writer.WriteElementString("telcli", row["telCli"].ToString());
                            writer.WriteElementString("referencia", row["refCli"].ToString());

                            writer.WriteElementString("dirCli", row["direccionEnt"].ToString());
                            writer.WriteElementString("dtoCli", row["cPostal"].ToString());
                            writer.WriteElementString("pobCli", row["poblacion"].ToString());
                            writer.WriteElementString("codPro", "");
                            writer.WriteElementString("Provincia", row["provincia"].ToString().Trim());

                            writer.WriteStartElement("Lineas");

                            foreach (DataRow detalle in Lineas.Rows)
                            {
                                writer.WriteStartElement("Linea");
                                writer.WriteAttributeString("id", detalle["ORDLIN"].ToString());
                                writer.WriteElementString("codart", detalle["CODART"].ToString().Trim());
                                writer.WriteElementString("artalias", detalle["ARTALIAS"].ToString().Trim());
                                writer.WriteElementString("desclin", detalle["DESCLIN"].ToString());
                                writer.WriteElementString("unidades", detalle["UNIDADES"].ToString());
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }

                    writer.WriteEndElement();
                    writer.Close();
                    writer.Flush();
                }
                #endregion
                #region BASTIDE
                else
                {
                    DataTable Cabecera = Documentos.Tables["cabeceraXML"];
                    DataTable Lineas = Documentos.Tables["lineasXML"];
                    csSqlConnects sql = new csSqlConnects();

                    string numero_pedido = "";

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.IndentChars = "  ";
                    settings.NewLineChars = "\r\n";
                    settings.NewLineHandling = NewLineHandling.Replace;
                    settings.Encoding = Encoding.UTF8;
                    settings.Encoding = Encoding.GetEncoding("utf-8");

                    XmlWriter writer = XmlWriter.Create(stream, settings);
                    writer.WriteStartElement("Pedidos");

                    foreach (DataRow row in Cabecera.Rows)
                    {
                        if (numero_pedido != row[1].ToString())
                        {
                            numero_pedido = row[1].ToString();
                            writer.WriteStartElement("Cabecera");
                        
                            writer.WriteElementString("TipoRegistro", "C");
                            writer.WriteElementString("CodigoPedido", numAlb);
                            writer.WriteElementString("FechaPedido", row["FechaPedido"].ToString());
                            writer.WriteElementString("NombreDestinatario", row["NombreDestinatario"].ToString());
                            writer.WriteElementString("DireccionDestinatario", csUtilidades.resolverPadRight(row["DireccionDestinatario"].ToString(), 60));
                            writer.WriteElementString("CPostalDestinatario", csUtilidades.resolverPadRight(row["CPostalDestinatario"].ToString(), 20));
                            writer.WriteElementString("PoblacionDestinatario", row["PoblacionDestinatario"].ToString());
                            writer.WriteElementString("PaisDestinatario", row["PaisDestinatario"].ToString());
                            writer.WriteElementString("CIFDestinatario", row["CIFDestinatario"].ToString());
                            writer.WriteElementString("TelefonoDestinatario", csUtilidades.resolverPadRight(row["TelefonoDestinatario"].ToString(), 20).Replace(".", ""));
                            writer.WriteElementString("Portes", "PAGADOS");
                            writer.WriteElementString("ObsDestinatario", "");
                            writer.WriteElementString("Reembolso", "");
                            writer.WriteElementString("Transportista", "");
                            writer.WriteElementString("ServicioTransportista", "");
                            writer.WriteElementString("NumAlbaran", numAlb);
                            writer.WriteElementString("AlbaranValorado", "N");
                            writer.WriteElementString("TipoDocumento", "P");
                            writer.WriteElementString("Provincia", row["Provincia"].ToString().Trim());

                            writer.WriteStartElement("Lineas");

                            foreach (DataRow detalle in Lineas.Rows)
                            {
                                writer.WriteStartElement("Linea");
                                writer.WriteAttributeString("id", detalle["Linea"].ToString());
                                writer.WriteElementString("TipoRegistro", "D");
                                writer.WriteElementString("CodigoPedido", numAlb);
                                writer.WriteElementString("Linea", detalle["Linea"].ToString());
                                writer.WriteElementString("CodigoArticulo", detalle["CodigoArticulo"].ToString());
                                writer.WriteElementString("DescArticulo", detalle["DescArticulo"].ToString());
                                writer.WriteElementString("Cantidad", detalle["Cantidad"].ToString());
                                writer.WriteElementString("CantidadServida", "");
                                writer.WriteElementString("Precio", detalle["Precio"].ToString());
                                writer.WriteElementString("SinCargo", "NO");
                                writer.WriteElementString("Observaciones", detalle["Observaciones"].ToString());
                                writer.WriteElementString("TipoDocumento", "P");
                                writer.WriteElementString("PVP", detalle["PVP"].ToString());
                                writer.WriteElementString("Marca", "");
                                writer.WriteEndElement();
                            }

                            writer.WriteEndElement();
                            writer.WriteEndElement();
                        }
                    }

                    writer.WriteEndElement();
                    writer.Close();
                    writer.Flush();
                }

                #endregion

                return stream.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

                return null;
            }
        }
        
        private void exportarAFicheroXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csPedidos ped = new csPedidos();

            //deprecated 14-5-2018
            //if (dgvPedidos.SelectedRows.Count == 1)
            //{
            //    string numdoc = dgvPedidos.SelectedRows[0].Cells["NUMDOC"].Value.ToString();
            //    ped.exportarFicheroXMLSGI(numdoc);
            //}

            foreach (DataGridViewRow filas in dgvPedidos.SelectedRows)
            {
          
                    int numdoc = Convert.ToInt32(filas.Cells["NUMDOC"].Value);
                    ped.exportarFicheroXMLSGI(numdoc.ToString());
            }
        }

        private void exportarficheroXMLBastide()
        {
            int contador = 1;
            if (dgvPedidos.Rows.Count > 0)
            {
                try
                {
                    if (comprobarSiColumnaVacia(dgvPedidos, "DIRCLI") &&
                        comprobarSiColumnaVacia(dgvPedidos, "DTOCLI") &&
                        comprobarSiColumnaVacia(dgvPedidos, "POBCLI") &&
                        comprobarSiColumnaVacia(dgvPedidos, "CODPAIS"))
                    {
                        string numero_pedido = "";
                        string numero_pedido_detalle = "";

                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Indent = true;
                        settings.IndentChars = "  ";
                        settings.NewLineChars = "\r\n";
                        settings.NewLineHandling = NewLineHandling.Replace;
                        XmlWriter writer = XmlWriter.Create("c:/albaranes/pedido-" + cboxDocs.Text + ".xml", settings);
                        writer.WriteStartElement("PEDIDOS");

                        foreach (DataGridViewRow row in dgvPedidos.Rows)
                        {
                            if (numero_pedido != row.Cells[0].Value.ToString())
                            {
                                string pais = ((row.Cells["CODPAIS"].Value.ToString() == "ES") ? "ESP" : "POR");
                                numero_pedido = row.Cells[0].Value.ToString();
                                writer.WriteStartElement("CABECERA");
                                writer.WriteElementString("TipoRegistro", "C");
                                writer.WriteElementString("CodigoPedido", Convert.ToInt32(row.Cells["NUMDOC"].Value).ToString());
                                writer.WriteElementString("FechaPedido", DateTime.Parse(row.Cells["FECHA"].Value.ToString()).ToString("dd/MM/yyyy"));
                                writer.WriteElementString("NombreDestinatario", csUtilidades.resolverPadRight(row.Cells["NOMCLI"].Value.ToString(), 60));
                                writer.WriteElementString("DireccionDestinatario", csUtilidades.resolverPadRight(row.Cells["DIRCLI"].Value.ToString(), 60));
                                writer.WriteElementString("CPostalDestinatario", csUtilidades.resolverPadRight(row.Cells["DTOCLI"].Value.ToString(), 20));
                                writer.WriteElementString("PoblacionDestinatario", csUtilidades.resolverPadRight(row.Cells["POBCLI"].Value.ToString(), 60));
                                writer.WriteElementString("PaisDestinatario", csUtilidades.resolverPadRight(pais, 10));
                                writer.WriteElementString("CIFDestinatario", csUtilidades.resolverPadRight(row.Cells["NIFCLI"].Value.ToString(), 20));
                                writer.WriteElementString("TelefonoDestinatario", csUtilidades.resolverPadRight(row.Cells["TELCLI"].Value.ToString(), 20));
                                writer.WriteElementString("Portes", "PAGADOS");
                                writer.WriteElementString("ObsDestinatario", "");
                                writer.WriteElementString("Reembolso", "");
                                writer.WriteElementString("Transportista", "");
                                writer.WriteElementString("ServicioTransportista", "");
                                writer.WriteElementString("NumAlbaran", csUtilidades.resolverPadRight(Convert.ToInt32(row.Cells["NUMDOC"].Value).ToString(), 1));
                                writer.WriteElementString("AlbaranValorado", "N");
                                writer.WriteElementString("TipoDocumento", "P");
                                writer.WriteElementString("Provincia", row.Cells["NOMPROVI"].Value.ToString().Trim());

                                writer.WriteStartElement("LINEAS");

                                foreach (DataGridViewRow detalle in dgvPedidos.Rows)
                                {
                                    numero_pedido_detalle = detalle.Cells[0].Value.ToString();

                                    if (numero_pedido == numero_pedido_detalle)
                                    {
                                        writer.WriteStartElement("LINEA");
                                        writer.WriteAttributeString("id", contador.ToString());
                                        writer.WriteElementString("TipoRegistro", "D");
                                        writer.WriteElementString("CodigoPedido", Convert.ToInt32(row.Cells["NUMDOC"].Value).ToString());
                                        writer.WriteElementString("Linea", contador.ToString());
                                        writer.WriteElementString("CodigoArticulo", detalle.Cells["CODART"].Value.ToString().Trim());
                                        writer.WriteElementString("DescArticulo", csUtilidades.resolverPadRight(detalle.Cells["descripcion"].Value.ToString(), 50));
                                        writer.WriteElementString("Cantidad", detalle.Cells["UNIDADES"].Value.ToString());
                                        writer.WriteElementString("CantidadServida", "");
                                        writer.WriteElementString("Precio", "0");
                                        writer.WriteElementString("SinCargo", "NO");
                                        writer.WriteElementString("Observaciones", detalle.Cells["CODTALLAV"].Value.ToString());
                                        writer.WriteElementString("TipoDocumento", "P");
                                        writer.WriteElementString("PVP", detalle.Cells["PRCMONEDA"].Value.ToString());
                                        //writer.WriteElementString("CodigoBarras", detalle.Cells["UNIDADES"].Value.ToString());
                                        writer.WriteElementString("Marca", "");
                                        writer.WriteEndElement();

                                        contador++;
                                    }
                                }

                                writer.WriteEndElement();
                                writer.WriteEndElement();
                            }
                        }

                        writer.WriteEndElement();
                        writer.Close();
                    }
                    else
                    {
                        MessageBox.Show("La direccion, el código postal, la poblacion y el país son obligatorios");
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private bool comprobarSiColumnaVacia(DataGridView dgv, string campo)
        {
            bool check = true;

            foreach (DataGridViewRow item in dgv.Rows)
            {
                if (item.Cells[campo].Value.ToString() == "")
                {
                    check = false;
                    break;
                }
            }

            return check;
        }

        

        private void btFilter_Click(object sender, EventArgs e)
        {
            cargarDocumentos(dtpDesde.Value.ToShortDateString(), dtpHasta.Value.ToShortDateString(), cboxDocs.Text);
        }

        private void cboxDocs_MouseDown(object sender, MouseEventArgs e)
        {
            cargarDocumentos(dtpDesde.Value.ToShortDateString(), dtpHasta.Value.ToShortDateString(), "");
        }

        private void generarXMLyPDFdesdeAlbaran()
        { 

        }
        

        private void servirAAlbaránToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idpedidoV = "";
            string numdoc = "", str_pedidos_sin_stock = "";
            List<string> pedidos_sin_stock = new List<string>();

            if (dgvPedidos.SelectedRows.Count > 0)
            {
                csa3erp a3erp = new csa3erp();
                a3erp.abrirEnlace();
                foreach (DataGridViewRow fila in dgvPedidos.SelectedRows)
                {
                    idpedidoV = fila.Cells["idpedv"].Value.ToString();
                    idpedidoV = Math.Truncate(Convert.ToDecimal(idpedidoV)).ToString();
                    numdoc = fila.Cells["numdoc"].Value.ToString();
                    numdoc = Math.Truncate(Convert.ToDecimal(numdoc)).ToString();

                    if (a3erp.pedidoEntregable(idpedidoV.ToString()))
                    {
                        a3erp.servirPedidoPorLineas(csGlobal.serie, numdoc, idpedidoV);
                    }
                    else
                    {
                        pedidos_sin_stock.Add(numdoc);
                    }
                }

                a3erp.cerrarEnlace();
            }

            if (pedidos_sin_stock.Count > 0)
            {
                foreach (string pedido in pedidos_sin_stock)
                {
                    str_pedidos_sin_stock += pedido + ", ";
                }

                string.Format("Los siguientes pedidos no tienen stock: \n\n{0}", str_pedidos_sin_stock).mb();
            }


        }

        private void abrirCarpetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csUtilidades.openFolder("C:");
        }

        private void rbtAlbaranes_MouseClick(object sender, MouseEventArgs e)
        {
            procesoAlbaranToolStripMenuItem.Enabled = true;
            servirAAlbaránToolStripMenuItem.Enabled = false;
        }

        private void rbtPedidos_Click(object sender, EventArgs e)
        {
            procesoAlbaranToolStripMenuItem.Enabled = false;
            servirAAlbaránToolStripMenuItem.Enabled = true;
        }

        private void procesoAlbaranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idalbv = "";
            string numdoc = "", str_pedidos_sin_stock = "";
            List<string> pedidos_sin_stock = new List<string>();

            if (dgvPedidos.SelectedRows.Count > 0)
            {
                csa3erp a3erp = new csa3erp();
                a3erp.abrirEnlace();
                foreach (DataGridViewRow fila in dgvPedidos.SelectedRows)
                {
                    idalbv = fila.Cells["idalbv"].Value.ToString();
                    idalbv = Math.Truncate(Convert.ToDecimal(idalbv)).ToString();
                    numdoc = fila.Cells["numdoc"].Value.ToString();
                    numdoc = Math.Truncate(Convert.ToDecimal(numdoc)).ToString();

                    a3erp.servirAlbaranPorLineas(csGlobal.serie, numdoc, idalbv);                    
                }

                a3erp.cerrarEnlace();
            }

        }


    }
}
