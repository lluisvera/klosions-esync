﻿namespace klsync.TRS
{
    partial class frGenerarDocsTRS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frGenerarDocsTRS));
            this.dgvLineasDocumentos = new System.Windows.Forms.DataGridView();
            this.contextMenuRightBtn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarLineaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rbtnSingleDocument = new System.Windows.Forms.RadioButton();
            this.rbtnMultipleDocument = new System.Windows.Forms.RadioButton();
            this.cboxTercero = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboxTipoDocumento = new System.Windows.Forms.ComboBox();
            this.lblTipoDocumento = new System.Windows.Forms.Label();
            this.lblTercero = new System.Windows.Forms.Label();
            this.cBoxSerie = new System.Windows.Forms.ComboBox();
            this.lblRefPedido = new System.Windows.Forms.Label();
            this.txtRefPedido = new System.Windows.Forms.TextBox();
            this.lblFechaDoc = new System.Windows.Forms.Label();
            this.dtpFechaDoc = new System.Windows.Forms.DateTimePicker();
            this.tbColumnas = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbStStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cboxModulo = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGenerarDocumento = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasDocumentos)).BeginInit();
            this.contextMenuRightBtn.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvLineasDocumentos
            // 
            this.dgvLineasDocumentos.AllowUserToAddRows = false;
            this.dgvLineasDocumentos.AllowUserToDeleteRows = false;
            this.dgvLineasDocumentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLineasDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLineasDocumentos.ContextMenuStrip = this.contextMenuRightBtn;
            this.dgvLineasDocumentos.Location = new System.Drawing.Point(285, 0);
            this.dgvLineasDocumentos.Name = "dgvLineasDocumentos";
            this.dgvLineasDocumentos.ReadOnly = true;
            this.dgvLineasDocumentos.Size = new System.Drawing.Size(918, 697);
            this.dgvLineasDocumentos.TabIndex = 2;
            // 
            // contextMenuRightBtn
            // 
            this.contextMenuRightBtn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pegarToolStripMenuItem,
            this.limpiarToolStripMenuItem,
            this.borrarLineaToolStripMenuItem});
            this.contextMenuRightBtn.Name = "contextMenuRightBtn";
            this.contextMenuRightBtn.Size = new System.Drawing.Size(138, 70);
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pegarToolStripMenuItem.Text = "&Pegar";
            this.pegarToolStripMenuItem.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // limpiarToolStripMenuItem
            // 
            this.limpiarToolStripMenuItem.Name = "limpiarToolStripMenuItem";
            this.limpiarToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.limpiarToolStripMenuItem.Text = "&Limpiar";
            this.limpiarToolStripMenuItem.Click += new System.EventHandler(this.limpiarToolStripMenuItem_Click);
            // 
            // borrarLineaToolStripMenuItem
            // 
            this.borrarLineaToolStripMenuItem.Name = "borrarLineaToolStripMenuItem";
            this.borrarLineaToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.borrarLineaToolStripMenuItem.Text = "&Borrar Linea";
            this.borrarLineaToolStripMenuItem.Click += new System.EventHandler(this.borrarLineaToolStripMenuItem_Click);
            // 
            // rbtnSingleDocument
            // 
            this.rbtnSingleDocument.AutoSize = true;
            this.rbtnSingleDocument.Checked = true;
            this.rbtnSingleDocument.Location = new System.Drawing.Point(23, 82);
            this.rbtnSingleDocument.Name = "rbtnSingleDocument";
            this.rbtnSingleDocument.Size = new System.Drawing.Size(89, 17);
            this.rbtnSingleDocument.TabIndex = 9;
            this.rbtnSingleDocument.TabStop = true;
            this.rbtnSingleDocument.Text = "1 Documento";
            this.rbtnSingleDocument.UseVisualStyleBackColor = true;
            this.rbtnSingleDocument.CheckedChanged += new System.EventHandler(this.rbtnSingleDocument_CheckedChanged);
            // 
            // rbtnMultipleDocument
            // 
            this.rbtnMultipleDocument.AutoSize = true;
            this.rbtnMultipleDocument.Location = new System.Drawing.Point(136, 82);
            this.rbtnMultipleDocument.Name = "rbtnMultipleDocument";
            this.rbtnMultipleDocument.Size = new System.Drawing.Size(129, 17);
            this.rbtnMultipleDocument.TabIndex = 10;
            this.rbtnMultipleDocument.Text = "Múltiples Documentos";
            this.rbtnMultipleDocument.UseVisualStyleBackColor = true;
            this.rbtnMultipleDocument.Visible = false;
            this.rbtnMultipleDocument.CheckedChanged += new System.EventHandler(this.rbtnMultipleDocument_CheckedChanged);
            // 
            // cboxTercero
            // 
            this.cboxTercero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTercero.FormattingEnabled = true;
            this.cboxTercero.Location = new System.Drawing.Point(23, 136);
            this.cboxTercero.Name = "cboxTercero";
            this.cboxTercero.Size = new System.Drawing.Size(238, 21);
            this.cboxTercero.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Serie";
            // 
            // cboxTipoDocumento
            // 
            this.cboxTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTipoDocumento.FormattingEnabled = true;
            this.cboxTipoDocumento.Items.AddRange(new object[] {
            "PEDIDO",
            "ALBARÁN",
            "FACTURA"});
            this.cboxTipoDocumento.Location = new System.Drawing.Point(158, 238);
            this.cboxTipoDocumento.Name = "cboxTipoDocumento";
            this.cboxTipoDocumento.Size = new System.Drawing.Size(121, 21);
            this.cboxTipoDocumento.TabIndex = 16;
            this.cboxTipoDocumento.Visible = false;
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.AutoSize = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(155, 222);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(101, 13);
            this.lblTipoDocumento.TabIndex = 17;
            this.lblTipoDocumento.Text = "Tipo de Documento";
            this.lblTipoDocumento.Visible = false;
            // 
            // lblTercero
            // 
            this.lblTercero.AutoSize = true;
            this.lblTercero.Location = new System.Drawing.Point(20, 120);
            this.lblTercero.Name = "lblTercero";
            this.lblTercero.Size = new System.Drawing.Size(75, 13);
            this.lblTercero.TabIndex = 15;
            this.lblTercero.Text = "Codigo Cliente";
            // 
            // cBoxSerie
            // 
            this.cBoxSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxSerie.FormattingEnabled = true;
            this.cBoxSerie.Location = new System.Drawing.Point(23, 186);
            this.cBoxSerie.Name = "cBoxSerie";
            this.cBoxSerie.Size = new System.Drawing.Size(88, 21);
            this.cBoxSerie.TabIndex = 19;
            // 
            // lblRefPedido
            // 
            this.lblRefPedido.AutoSize = true;
            this.lblRefPedido.Location = new System.Drawing.Point(21, 275);
            this.lblRefPedido.Name = "lblRefPedido";
            this.lblRefPedido.Size = new System.Drawing.Size(94, 13);
            this.lblRefPedido.TabIndex = 25;
            this.lblRefPedido.Text = "Referencia pedido";
            // 
            // txtRefPedido
            // 
            this.txtRefPedido.Location = new System.Drawing.Point(24, 291);
            this.txtRefPedido.Name = "txtRefPedido";
            this.txtRefPedido.Size = new System.Drawing.Size(152, 20);
            this.txtRefPedido.TabIndex = 24;
            // 
            // lblFechaDoc
            // 
            this.lblFechaDoc.AutoSize = true;
            this.lblFechaDoc.Location = new System.Drawing.Point(20, 223);
            this.lblFechaDoc.Name = "lblFechaDoc";
            this.lblFechaDoc.Size = new System.Drawing.Size(95, 13);
            this.lblFechaDoc.TabIndex = 23;
            this.lblFechaDoc.Text = "Fecha Documento";
            // 
            // dtpFechaDoc
            // 
            this.dtpFechaDoc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaDoc.Location = new System.Drawing.Point(23, 239);
            this.dtpFechaDoc.Name = "dtpFechaDoc";
            this.dtpFechaDoc.Size = new System.Drawing.Size(121, 20);
            this.dtpFechaDoc.TabIndex = 22;
            // 
            // tbColumnas
            // 
            this.tbColumnas.Location = new System.Drawing.Point(12, 356);
            this.tbColumnas.Multiline = true;
            this.tbColumnas.Name = "tbColumnas";
            this.tbColumnas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbColumnas.Size = new System.Drawing.Size(267, 308);
            this.tbColumnas.TabIndex = 21;
            this.tbColumnas.Text = "COLUMNAS A INCORPORAR\r\n(INCLUIR TITULOS COLUMNA AL PEGAR)\r\n\r\nCOLUMNAS\r\n1 - \"CODAR" +
    "T\"\r\n2-  \"MES 1\"\r\n3 - \"MES 2\"\r\n4-  \"MES 3\"\r\n...\r\nN-\"MES N\"\r\n(titulos de columnas:" +
    " Parte entre comillas)\r\n\r\n";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbStStatus,
            this.toolStripStatusMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 697);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1205, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbStStatus
            // 
            this.lbStStatus.Name = "lbStStatus";
            this.lbStStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.cboxModulo,
            this.toolStripSeparator1,
            this.btnGenerarDocumento});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1205, 54);
            this.toolStrip1.TabIndex = 28;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(58, 51);
            this.toolStripLabel1.Text = "MÓDULO";
            // 
            // cboxModulo
            // 
            this.cboxModulo.Items.AddRange(new object[] {
            "VENTAS"});
            this.cboxModulo.Name = "cboxModulo";
            this.cboxModulo.Size = new System.Drawing.Size(121, 54);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 54);
            // 
            // btnGenerarDocumento
            // 
            this.btnGenerarDocumento.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerarDocumento.Image")));
            this.btnGenerarDocumento.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnGenerarDocumento.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGenerarDocumento.Name = "btnGenerarDocumento";
            this.btnGenerarDocumento.Size = new System.Drawing.Size(76, 51);
            this.btnGenerarDocumento.Text = "Generar Doc";
            this.btnGenerarDocumento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGenerarDocumento.Click += new System.EventHandler(this.btnGenerarDocumento_Click);
            // 
            // toolStripStatusMessage
            // 
            this.toolStripStatusMessage.Name = "toolStripStatusMessage";
            this.toolStripStatusMessage.Size = new System.Drawing.Size(0, 17);
            // 
            // frGenerarDocsTRS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 719);
            this.Controls.Add(this.dgvLineasDocumentos);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblRefPedido);
            this.Controls.Add(this.txtRefPedido);
            this.Controls.Add(this.lblFechaDoc);
            this.Controls.Add(this.dtpFechaDoc);
            this.Controls.Add(this.tbColumnas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBoxSerie);
            this.Controls.Add(this.lblTipoDocumento);
            this.Controls.Add(this.cboxTipoDocumento);
            this.Controls.Add(this.lblTercero);
            this.Controls.Add(this.cboxTercero);
            this.Controls.Add(this.rbtnMultipleDocument);
            this.Controls.Add(this.rbtnSingleDocument);
            this.Name = "frGenerarDocsTRS";
            this.Text = "frGenerarDocsTRS";
            this.Load += new System.EventHandler(this.frGenerarDocsA3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLineasDocumentos)).EndInit();
            this.contextMenuRightBtn.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvLineasDocumentos;
        private System.Windows.Forms.RadioButton rbtnSingleDocument;
        private System.Windows.Forms.RadioButton rbtnMultipleDocument;
        private System.Windows.Forms.ComboBox cboxTercero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxTipoDocumento;
        private System.Windows.Forms.Label lblTipoDocumento;
        private System.Windows.Forms.Label lblTercero;
        private System.Windows.Forms.ComboBox cBoxSerie;
        private System.Windows.Forms.Label lblRefPedido;
        private System.Windows.Forms.TextBox txtRefPedido;
        private System.Windows.Forms.Label lblFechaDoc;
        private System.Windows.Forms.DateTimePicker dtpFechaDoc;
        private System.Windows.Forms.TextBox tbColumnas;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbStStatus;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightBtn;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarLineaToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cboxModulo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnGenerarDocumento;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusMessage;
    }
}