﻿namespace klsync
{
    partial class frCompletarDeclaracionIntrastat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.txtBEstadoProceso = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 43);
            this.button1.TabIndex = 0;
            this.button1.Text = "Actualizar Declaraciones";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtBEstadoProceso
            // 
            this.txtBEstadoProceso.Enabled = false;
            this.txtBEstadoProceso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBEstadoProceso.Location = new System.Drawing.Point(30, 94);
            this.txtBEstadoProceso.Name = "txtBEstadoProceso";
            this.txtBEstadoProceso.ReadOnly = true;
            this.txtBEstadoProceso.Size = new System.Drawing.Size(182, 26);
            this.txtBEstadoProceso.TabIndex = 1;
            // 
            // frCompletarDeclaracionIntrastat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 407);
            this.Controls.Add(this.txtBEstadoProceso);
            this.Controls.Add(this.button1);
            this.Name = "frCompletarDeclaracionIntrastat";
            this.Text = "Completar Declaración Intrastat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBEstadoProceso;
    }
}