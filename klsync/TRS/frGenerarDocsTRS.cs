﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace klsync.TRS
{
    public partial class frGenerarDocsTRS : Form
    {
        private static frGenerarDocsTRS m_FormDefInstance;
        public static frGenerarDocsTRS DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frGenerarDocsTRS();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        private bool multipleDocs = false;

        ArrayList mesesPedidos = new ArrayList();

        public frGenerarDocsTRS()
        {
            InitializeComponent();
        }

        private void limpiarDatagridview()
        {
            dgvLineasDocumentos.Rows.Clear();
            dgvLineasDocumentos.Columns.Clear();
        }

        private void generarSingleDocument()
        {
            Objetos.csCabeceraDoc[] objCabeceras = new Objetos.csCabeceraDoc[1];
            Objetos.csLineaDocumento[] objLineas = new Objetos.csLineaDocumento[dgvLineasDocumentos.Rows.Count];

            bool documentoCompras = false;
            if (cboxModulo.Text.ToUpper() == "COMPRAS")
            {
                documentoCompras = true;
            }
            if (cboxTercero.Text == "")
            {
                cboxTercero.BackColor = Color.Red;
                return;

            }

            DataTable dtExcel = csUtilidades.fillDataTableFromDataGridView(dgvLineasDocumentos, false);
            int mesPedidoToDo = 0;


            csa3erp a3erp = new csa3erp();
            a3erp.abrirEnlace();

            foreach (var mesPedido in mesesPedidos)
            {
                lbStStatus.Text = ">>>>>>PROCESANDO - PREPARANDO EL DOCUMENTO MES: " + mesPedido + " ESPERE POR FAVOR.....";
                this.Refresh();
                mesPedidoToDo = Convert.ToInt32(mesPedido);
                objCabeceras = generarCabecera(mesPedidoToDo);
                objLineas = generarLineasTYC(dtExcel, mesPedidoToDo);
                lbStStatus.Text = ">>>>>>PROCESANDO - GENERANDO DOCUMENTO EN A3ERP MES: " + mesPedido + " ESPERE POR FAVOR .....";
                this.Refresh();
                a3erp.generarDocA3Objeto(objCabeceras, objLineas, documentoCompras, "NO");
            }

            a3erp.cerrarEnlace();
            lbStStatus.Text = "......>>>>>>PROCESO FINALIZADO - DOCUMENTOS GENERADOS";



        }

        private void GenerarMultipleDocuments()
        {
            string texto = dgvLineasDocumentos.Columns[2].Name.ToString();
            Objetos.csCabeceraDoc[] objCabeceras = new Objetos.csCabeceraDoc[dgvLineasDocumentos.Rows.Count];
            Objetos.csLineaDocumento[] objLineas = new Objetos.csLineaDocumento[dgvLineasDocumentos.Rows.Count];
            Objetos.csDomBanca[] objDomiciliacion = new Objetos.csDomBanca[dgvLineasDocumentos.Rows.Count];
            Objetos.csProveedor[] objProveedores = new Objetos.csProveedor[dgvLineasDocumentos.Rows.Count];
            DataTable dtExcel = csUtilidades.fillDataTableFromDataGridView(dgvLineasDocumentos, false);

            bool documentoCompras = false;
            if (cboxModulo.Text.ToUpper() == "COMPRAS")
            {
                documentoCompras = true;
            }

            objCabeceras = generarCabeceraMultipledocs();
            if (csGlobal.modoTallasYColores == "A3SIPSSI")
            {
                //pasamo parametro un 1, pero debería ser el mes de la columna a generar
                objLineas = generarLineasTYC(dtExcel, 1);
            }
            else
            {
                objLineas = generarLineas();
            }
            objDomiciliacion = generarDomiciliacionesMultipleDocs();
            objProveedores = generarListadoProveedores();

            csGlobal.docDestino = "Factura";

            csa3erp A3Func = new csa3erp();
            A3Func.abrirEnlace();
            A3Func.generarDocA3Objeto(objCabeceras, objLineas, false, "NO", null, null);
            A3Func.cerrarEnlace();

        }

        private bool verificarLineas()
        {
            bool lineasCorrectas = true;
            mesesPedidos.Clear();
            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable dtExcel = csUtilidades.fillDataTableFromDataGridView(dgvLineasDocumentos, false);
            string[] articulos = new string[dgvLineasDocumentos.Rows.Count];
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                articulos[i] = "'" + dtExcel.Rows[i]["CODART"].ToString() + "'";

            }

            //Unifico códigos y agrupos códigos de artículos
            string[] articulosA3 = articulos.Distinct().ToArray();
            var filtroArticulos = string.Join(",", articulosA3);
            DataTable dtArticulos = sqlConnect.obtenerDatosSQLScript("SELECT LTRIM(CODART) AS CODART, BLOQUEADO FROM ARTICULO WHERE LTRIM(CODART) IN (" + filtroArticulos + ")");

            //VALIDAR TITULOS DE COLUMNAS
            

            foreach (DataColumn mes in dtExcel.Columns)
            {
                if (mes.ToString().ToUpper().Contains("MES"))
                {
                    mesesPedidos.Add(mes.ToString().ToUpper().Trim().Replace("MES","").Replace(" ", ""));
                    mes.ColumnName = mes.ToString().ToUpper().Trim().Replace("MES", "").Replace(" ","");
                }
            }



            //VALIDO QUE LAS CANTIDADES Y PRECIOS SEAN NUMÉRICOS

            foreach (var valorArraylist in mesesPedidos)
            {
                foreach (DataRow dr in dtExcel.Rows)
                {
                    string precio = "";
                    int quantity;
                    bool isNumeric = int.TryParse(dr[valorArraylist.ToString()].ToString(), out quantity);
                    if (!isNumeric)
                    {
                        MessageBox.Show("La línea con el artículo " + dr["CODART"].ToString() + " tiene una incidencia");
                        lineasCorrectas = false;
                        break;
                    }

                }
            }
            



            //VALIDACIÓN DE QUE TODOS LOS ARTÍCULOS EXISTEN
            if (dtArticulos.Rows.Count == articulosA3.Length)
            {
                //Todos los artículos Existen
            }
            else
            {
                string articulosInexistentes = "";
                bool existe = false;
                for (int index = 0; index < articulosA3.Length; index++)
                {
                    existe = false;
                    for (int index2 = 0; index2 < dtArticulos.Rows.Count; index2++)
                    {
                        if (articulosA3[index].Replace("'", "") == dtArticulos.Rows[index2]["CODART"].ToString())
                        {
                            existe = true;
                            break;
                        }

                    }
                    if (!existe)
                    {
                        articulosInexistentes = articulosInexistentes + articulosA3[index].Replace("'", "") + "\n";

                    }



                }
                //Hay algún artículo que no existe
                MessageBox.Show("Los siguientes artículos no están dados de alta" + "\n" + articulosInexistentes);
                lineasCorrectas = false;
            }
            //VALIDACIÓN SI HAY ALGÚN ARTÍCULO BLOQUEADO
            if (dtArticulos.hasRows())
            {
                string articulosBloqueados = "";
                DataView dvArticulosBloqueados = new DataView(dtArticulos);
                dvArticulosBloqueados.RowFilter = "BLOQUEADO='T'";


                if (dvArticulosBloqueados.Count > 0)
                {
                    foreach (DataRowView row in dvArticulosBloqueados)
                    {
                        articulosBloqueados = articulosBloqueados + row["CODART"].ToString() + "\n";

                    }
                    MessageBox.Show("Los siguientes artículos están bloqueados en A3ERP:\n" + articulosBloqueados);
                    lineasCorrectas = false;
                }
            }

            return lineasCorrectas;



            //throw new NotImplementedException();
        }

        private Objetos.csCabeceraDoc[] generarCabecera(int mes)
        {

            var cabecera = new Objetos.csCabeceraDoc[1];
            cabecera[0] = new Objetos.csCabeceraDoc();
            cabecera[0].codIC = cboxTercero.SelectedValue.ToString().Replace(" ", "");
            cabecera[0].serieDoc = cBoxSerie.SelectedValue.ToString().Replace(" ", "");
            cabecera[0].fechaDoc = dtpFechaDoc.Value.AddMonths(mes-1);
            cabecera[0].referencia = txtRefPedido.Text.Trim();
            cabecera[0].extNumdDoc = "0";

            return cabecera;
        }

        //Obtener Datatable from un Datagridview
        private DataTable GetDataTableFromDGV(DataGridView dgv)
        {
            var dt = new DataTable();
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.Visible)
                {
                    // You could potentially name the column based on the DGV column name (beware of dupes)
                    // or assign a type based on the data type of the data bound to this DGV column.
                    dt.Columns.Add();
                }
            }

            object[] cellValues = new object[dgv.Columns.Count];
            foreach (DataGridViewRow row in dgv.Rows)
            {
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    cellValues[i] = row.Cells[i].Value;
                }
                dt.Rows.Add(cellValues);
            }

            return dt;
        }

        private Objetos.csCabeceraDoc[] generarCabeceraMultipledocs()
        {
            string numLinea = "";
            int totalDocs = 0;
            string header = "";
            //DataTable data = GetDataTableFromDGV(dgvLineasDocumentos);

            //Verifico el número de lineas número 1 que hay, porque así creo el número de cabeceras que haya
            foreach (DataGridViewRow fila in dgvLineasDocumentos.Rows)
            {
                numLinea = fila.Cells["NUMLINDOC"].Value.ToString();
                if (numLinea == "1")
                {
                    totalDocs++;
                }
            }

            var cabecera = new Objetos.csCabeceraDoc[totalDocs];
            var domiciliacion = new Objetos.csDomBanca[totalDocs];
            int i = 0;

            foreach (DataGridViewRow fila in dgvLineasDocumentos.Rows)
            {
                numLinea = fila.Cells["NUMLINDOC"].Value.ToString();
                if (numLinea == "1")
                {
                    //Por cada objeto debo inicializarlo
                    cabecera[i] = new Objetos.csCabeceraDoc();
                    cabecera[i].fechaDoc = Convert.ToDateTime(fila.Cells["FECHA"].Value.ToString().Replace(" ", ""));
                    cabecera[i].serieDoc = fila.Cells["SERIE"].Value.ToString();
                    cabecera[i].numDocA3 = fila.Cells["NUMDOC"].Value.ToString();
                    cabecera[i].referencia = fila.Cells["REFDOC"].Value.ToString();
                    cabecera[i].codIC = fila.Cells["CODIGO"].Value.ToString().Replace(" ", "");
                    cabecera[i].nombreIC = fila.Cells["NOMBRE"].Value.ToString().ToUpper();
                    cabecera[i].direccionDirFacturacion = fila.Cells["DIR"].Value.ToString().ToUpper();
                    cabecera[i].codigoPostalDirFacturacion = fila.Cells["CODPOSTAL"].Value.ToString().ToUpper();
                    cabecera[i].poblacionDirFacturacion = fila.Cells["POB"].Value.ToString().ToUpper();
                    cabecera[i].nif = fila.Cells["CIF"].Value.ToString().Replace(" ", "");
                    cabecera[i].agenteComercial = fila.Cells["CODREPRE"].Value.ToString().Replace(" ", "");
                    cabecera[i].portes = fila.Cells["TOTPORTESDOC"].Value.ToString().ToUpper();
                    cabecera[i].regIva = fila.Cells["CODREGIVA"].Value.ToString().ToUpper();
                    i++;
                }
            }

            return cabecera;

        }


        private Objetos.csProveedor[] generarListadoProveedores()
        {

            var cabecera = new Objetos.csProveedor[dgvLineasDocumentos.Rows.Count];
            int i = 0;

            foreach (DataGridViewRow fila in dgvLineasDocumentos.Rows)
            {
                //Por cada objeto debo inicializarlo
                cabecera[i] = new Objetos.csProveedor();
                cabecera[i].codProv = fila.Cells[4].Value.ToString().Replace(" ", "");
                cabecera[i].nombre = fila.Cells[5].Value.ToString().Replace(" ", "");
                cabecera[i].nifcif = fila.Cells[7].Value.ToString().Replace(" ", "");
                cabecera[i].codFormaPago = "TR";

                i++;
            }

            return cabecera;

        }

        private Objetos.csDomBanca[] generarDomiciliacionesMultipleDocs()
        {

            var domiciliacion = new Objetos.csDomBanca[dgvLineasDocumentos.Rows.Count];
            int i = 0;

            foreach (DataGridViewRow fila in dgvLineasDocumentos.Rows)
            {
                //Por cada objeto debo inicializarlo
                domiciliacion[i] = new Objetos.csDomBanca();
                domiciliacion[i].iban = fila.Cells["IBAN"].Value.ToString();
                domiciliacion[i].banco = fila.Cells["BANCO"].Value.ToString().Replace(" ", "");
                domiciliacion[i].agencia = fila.Cells["OFICINA"].Value.ToString().Replace(" ", "");
                domiciliacion[i].digitoControl = fila.Cells["DIGCONTROL"].Value.ToString().Replace(" ", "");
                domiciliacion[i].numcuenta = fila.Cells["NUMCUENTA"].Value.ToString().Replace(" ", "");
                domiciliacion[i].titular = fila.Cells["NOMBRE"].Value.ToString().ToUpper();

                i++;
            }

            return domiciliacion;


        }

        

        private Objetos.csLineaDocumento[] generarLineas()
        {
            //inicializamos el objeto a 0 y lo iremos redimensionando
            var lineaDoc = new Objetos.csLineaDocumento[dgvLineasDocumentos.Rows.Count];
            int i = 0;

            foreach (DataGridViewRow fila in dgvLineasDocumentos.Rows)
            {
                lineaDoc[i] = new Objetos.csLineaDocumento();
                lineaDoc[i].codigoArticulo = fila.Cells["CODART"].Value.ToString();
                lineaDoc[i].cantidad = fila.Cells["CANTIDAD"].Value.ToString();
                lineaDoc[i].precio = fila.Cells["PRECIO"].Value.ToString();
                lineaDoc[i].descripcionArticulo = fila.Cells["DESCART"].Value.ToString();
                lineaDoc[i].numeroDoc = fila.Cells["NUMDOC"].Value.ToString();
                lineaDoc[i].numeroLinea = fila.Cells["NUMLINDOC"].Value.ToString();
                lineaDoc[i].numCabecera = fila.Cells["NUMDOC"].Value.ToString();
                lineaDoc[i].numSerie = fila.Cells["NUMSERIE"].Value.ToString();
                i++;

            }


            return lineaDoc;
        }



        private Objetos.csLineaDocumento[] generarLineasTYC(DataTable dtExcel, int mes)
        {
            try
            {

                string codart = "";
                string nomColumna = "";
                int index = 0;

                DataTable articulo = new DataTable();

                csSqlConnects sqlConnect = new csSqlConnects();


                int i = 0;
                csSqlConnects sql = new csSqlConnects();

                //Leemos el Excel y lo convertimos en un Datatable
                // Si pasamos a datatable tenemos acceso a las cabeceras, si no no
               
                lbStStatus.Text = "CREANDO DOCUMENTO";
                this.Refresh();

                //inicializamos el objeto a 0 y lo iremos redimensionando
                var lineaDoc = new Objetos.csLineaDocumento[1];

                List<string> articulos_inexistentes = new List<string>();

                DataTable articulosA3ERP = new DataTable();
                articulosA3ERP = sqlConnect.obtenerDatosSQLScript("SELECT lTRIM(CODART) as CODART FROM ARTICULO ");

                if (dtExcel.hasRows())
                {
                    //Itero cada fila de Excel

                    foreach (DataRow fila in dtExcel.Rows)
                    {
                        //Obtengo el artículo
                        codart = fila["CODART"].ToString();
                        //Verifico si tienen tallas o no



                        DataView selectArt = new DataView(articulosA3ERP);
                        selectArt.RowFilter = ("CODART='" + codart + "'");
                        articulo = selectArt.ToTable();

                        Array.Resize(ref lineaDoc, i + 1);
                        lineaDoc[i] = new Objetos.csLineaDocumento();
                        lineaDoc[i].codigoArticulo = dtExcel.Rows[index]["codart"].ToString().Trim();
                        lineaDoc[i].cantidad = fila[mes].ToString().Trim();

                        if (rbtnSingleDocument.Checked)
                        {
                            lineaDoc[i].numeroDoc = "0";
                        }
                        i++;

                        index++;
                    }
                }


                string codigoArticuloToCheck = "";
                int cantidad = 0;

                string codigoArticuloTemp = "";
                int cantidadTemp = 0;


                return lineaDoc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void frGenerarDocsA3_Load(object sender, EventArgs e)
        {
            cboxModulo.SelectedIndex = 0;
            cboxTipoDocumento.SelectedIndex = 0;
            btnGenerarDocumento.Enabled = false;
            cargarSeries();
            cargarTerceros(true);
           


        }


        private void cargarTerceros(bool clientes)
        {
            DataTable dt;
            csSqlConnects sqlConnect = new csSqlConnects();

            if (!clientes)
            {
                dt = sqlConnect.cargarTabla("PROVEED", "CODPRO as CODIC, NOMPRO + ' - ' + LTRIM(CODPRO) as NOMIC");
            }
            else
            {
                dt = sqlConnect.cargarTabla("CLIENTES", "CODCLI as CODIC,NOMCLI + ' - ' + LTRIM(CODCLI) as NOMIC");
            }

            cboxTercero.DataSource = dt;
            cboxTercero.ValueMember = "CODIC";
            cboxTercero.DisplayMember = "NOMIC";
        }

        private void cargarSeries()
        {

            DataTable dt;
            csSqlConnects sqlConnect = new csSqlConnects();

            dt = sqlConnect.cargarTabla("SERIES", " NOMSERIE, SERIE ");

            cBoxSerie.DataSource = dt;
            cBoxSerie.ValueMember = "SERIE";
            cBoxSerie.DisplayMember = "NOMSERIE";

        }

        private void rbtnMultipleDocument_CheckedChanged(object sender, EventArgs e)
        {
            multipleDocs = false;
            if (rbtnMultipleDocument.Checked)
            {
                multipleDocs = true;

            }
            mostrarOpciones(multipleDocs);
        }

        private void rbtnSingleDocument_CheckedChanged(object sender, EventArgs e)
        {
            multipleDocs = false;
            if (rbtnMultipleDocument.Checked)
            {
                multipleDocs = true;
            }
            mostrarOpciones(multipleDocs);
        }

        private void mostrarOpciones(bool multipleDocs)
        {
            if (multipleDocs)
            {
                cboxTercero.Visible = false;
                lblTercero.Visible = false;
                lblTipoDocumento.Visible = false;
                cboxTipoDocumento.Visible = false;
                lblFechaDoc.Visible = false;
                dtpFechaDoc.Visible = false;
                tbColumnas.Text = "PARA IMPORTAR DOCUMENTOS LA HOJA DE CÁLCULO DEBE INCORPORAR " + "\r\n" +
                            "LAS SIGUIENTES COLUMNAS EN EL ORDEN INDICADO " + "\r\n" + "\r\n" +
                            "(LA PRIMERA FILA DEBE INCLUIR LOS TÍTULOS DE COLUMNA) " + "\r\n" + "\r\n" +
                            " COLUMNAS: " + "\r\n" +
                            " DATOS CABECERA_________________ " + "\r\n" +
                            "  1 - FECHA DE DOCUMENTO " + "\r\n" +
                            "  2 - SERIE DE DOCUMENTO " + "\r\n" +
                            "  3 - NÚMERO DE DOCUMENTO " + "\r\n" +
                            "  4 - REFERENCIA DE DOCUMENTO " + "\r\n" +
                            "  5 - CÓDIGO DE CLIENTE/PROVEEDOR " + "\r\n" +
                            "  6 - NOMBRE CLIENTE/PROVEEDOR " + "\r\n" +
                            "  7 - DIRECCIÓN " + "\r\n" +
                            "  8 - CÓDIGO POSTAL " + "\r\n" +
                            "  9 - POBLACIÓN " + "\r\n" +
                            " 10 - PROVINCIA " + "\r\n" +
                            " 11 - PAÍS " + "\r\n" +
                            " 12 - CIF/NIF " + "\r\n" +
                            " 13 - CÓDIGO DE REPRESENTANTE " + "\r\n" +
                            " 14 - TOTAL BASE DOCUMENTO " + "\r\n" +
                            " 15 - TOTAL IVA DOCUMENTO " + "\r\n" +
                            " 16 - TOTAL PORTES DOCUMENTO " + "\r\n" +
                            " 17 - TOTAL REC.EQUIV. DOCUMENTO " + "\r\n" +
                            " 18 - CÓDIGO RÉGIMEN DE IVA A3ERP " + "\r\n" +
                            " 19 - CÓDIGO FORMA DE PAGO " + "\r\n" +
                            " 20 - CÓDIGO DOCUMENTO DE PAGO " + "\r\n" +
                            " DATOS LÍNEAS_________________ " + "\r\n" +
                            " 21 - CÓDIGO ARTÍCULO " + "\r\n" +
                            " 22 - DESCRIPCIÓN ARTÍCULO " + "\r\n" +
                            " 23 - PRECIO " + "\r\n" +
                            " 24 - CANTIDAD " + "\r\n" +
                            " 25 - DTO" + "\r\n" +
                            " 26 - CÓDIGO TIPO IVA (A3ERP)" + "\r\n" +
                            " DATOS BANCARIOS_________________ " + "\r\n" +
                            " 27 - IBAN" + "\r\n" +
                            " 28 - BANCO (4 dígitos) " + "\r\n" +
                            " 29 - OFICINA (4 dígitos) " + "\r\n" +
                            " 30 - DIGITO CONTROL (2 dígitos) " + "\r\n" +
                            " 31 - NÚMERO CUENTA (10 dígitos) " + "\r\n" +
                            " 32 - NÚMERO DE LÍNEA DEL DOCUMENTO " + "\r\n" + "(Las lineas comienzan por 1) ";
            }
            else
            {
                cboxTercero.Visible = true;
                lblTercero.Visible = true;
                lblTipoDocumento.Visible = true;
                cboxTipoDocumento.Visible = true;
                lblFechaDoc.Visible = true;
                dtpFechaDoc.Visible = true;
                tbColumnas.Text = "COLUMNAS A INCORPORAR " + "\r\n" +
                            "(INCLUIR TITULOS COLUMNA AL PEGAR) " + "\r\n" + "\r\n" +
                            " COLUMNAS " + "\r\n" +
                            " 1 - CÓDIGO ARTÍCULO A3ERP " + "\r\n" +
                            " 2 - DESCRIPCIÓN ARTÍCULO " + "\r\n" +
                            " 3 - CANTIDAD " + "\r\n" +
                            " 4 - PRECIO " + "\r\n" +
                            " 5 - DTO";
            }


        }



        private bool verificarEstadoCliente()
        {
            bool clienteBloqueado = false;
            string codCliA3 = "";
            csSqlConnects sqlConnect = new csSqlConnects();
            codCliA3 = cboxTercero.SelectedValue.ToString().Replace(" ", "");
            DataTable dtClientBlocked = sqlConnect.obtenerDatosSQLScript("SELECT CODCLI, NOMCLI FROM CLIENTES WHERE LTRIM(CODCLI)='" + codCliA3 + "' AND BLOQUEADO='T'");
            if (dtClientBlocked.hasRows())
            {
                MessageBox.Show("Cliente Bloqueado");
                clienteBloqueado = true;
            }
            return clienteBloqueado;
        }

        private bool verificarArticulos()
        {
            bool articulosExisten = false;



            return articulosExisten;


        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csCopyPaste copyPaste = new csCopyPaste();
            copyPaste.PasteClipboard(dgvLineasDocumentos);
            if (dgvLineasDocumentos.Rows.Count > 0)
            {
                btnGenerarDocumento.Enabled = true;
                lbStStatus.Text = dgvLineasDocumentos.Rows.Count.ToString() + " Líneas";

            }
        }

        private void limpiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            limpiarDatagridview();
            btnGenerarDocumento.Enabled = false;
            lbStStatus.Text = "";
        }

        private void borrarLineaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvLineasDocumentos.SelectedRows)
            {
                if (!row.IsNewRow)
                    dgvLineasDocumentos.Rows.Remove(row);
            }
        }

        private void cboxModulo_TextChanged(object sender, EventArgs e)
        {
            if (cboxModulo.SelectedIndex == 0)
            {
                lblTercero.Text = "Código de Cliente";
                cargarTerceros(true);
                cargarSeries();
            }
            else
            {
                lblTercero.Text = "Código de Proveedor";
                cargarTerceros(false);
                cargarSeries();
            }
        }

        private void btnGenerarDocumento_Click(object sender, EventArgs e)
        {
            if (!multipleDocs)
            {
                if (!verificarEstadoCliente() && verificarLineas())
                {
                    generarSingleDocument();
                    lbStStatus.Text = "PROCESO FINALIZADO";
                    MessageBox.Show("PROCESO FINALIZADO");
                }
            }
            else
            {
                GenerarMultipleDocuments();
            }
        }

       
    }
}
