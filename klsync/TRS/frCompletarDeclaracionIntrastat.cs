﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace klsync
{
    public partial class frCompletarDeclaracionIntrastat : Form
    {
        private static frCompletarDeclaracionIntrastat m_FormDefInstance;
        public static frCompletarDeclaracionIntrastat DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCompletarDeclaracionIntrastat();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frCompletarDeclaracionIntrastat()
        {
            InitializeComponent();
        }

        private void actualizarIntrastat()
        {

            string codart = "";
            string codCN = "";
            double peso = 0;
            string formula = "555";
            int idIntrastat = 0;
            int numLin = 0;
            double unidades = 0;
            double importeBase = 0;
            double valorEstadistico = 0;
            string idIntrastat2 = "";
            double pesoEstadistico = 0;
            string scriptSQL = "";
            string puerto = "";
            string tipoTransporte = "";
            string condEntregaA = "";
            string condEntregaB = "";
            string codNaturalezaA = "";
            string codNaturalezaB = "";
            string codRegEstadistico = "";
            string codIC = "";  //Interlocutor puede ser cliente o proveedor

            try
            {
                DataTable dtLineasIntrastat = new DataTable();
                DataTable dtLineasAlbVenta = new DataTable();
                DataTable dtLineasAlbCopmra = new DataTable();
                DataTable dtArticulo = new DataTable();
                DataTable dtClientes = new DataTable();
                DataTable dtProveedores = new DataTable();
                DataTable dtCuenta = new DataTable();


                csSqlConnects sql = new csSqlConnects();

                dtLineasIntrastat = sql.obtenerDatosSQLScript("SELECT * FROM __LINEINTRASTAT");
                dtArticulo = sql.obtenerDatosSQLScript("SELECT CODART, DESCART, CODCN, PESO FROM ARTICULO");
                dtClientes = sql.obtenerDatosSQLScript("SELECT CODCLI as CODIC, NOMCLI, PUERTO, TIPTRA, CODENTA,CODENTB, CODNATA,CODNATB, CODREGEST from CLIENTES");
                dtProveedores = sql.obtenerDatosSQLScript("SELECT CODPRO AS CODIC, NOMPRO, PUERTO, TIPTRA, CODENTA,CODENTB, CODNATA,CODNATB, CODREGEST FROM PROVEED");
                // dtLineasAlbCopmra = sql.obtenerDatosSQLScript("");

                sql.abrirConexion();

                foreach (DataRow lineaIntrastat in dtLineasIntrastat.Rows)
                {
                    idIntrastat = Convert.ToInt32(lineaIntrastat["IDINTRASTAT"].ToString().Replace(",0000",""));
                    numLin = Convert.ToInt32(lineaIntrastat["NUMLIN"].ToString().Replace(",0000", ""));
                    if (numLin == 359)
                    {
                        string ojo = "0";
                    }
                    codIC = string.IsNullOrEmpty(lineaIntrastat["CODCLI"].ToString())? lineaIntrastat["CODPRO"].ToString():lineaIntrastat["CODCLI"].ToString();
                    dtCuenta = string.IsNullOrEmpty(lineaIntrastat["CODCLI"].ToString()) ? dtProveedores :dtClientes;
                    codart = lineaIntrastat["CODART"].ToString();
                    importeBase = Convert.ToDouble(string.IsNullOrEmpty(lineaIntrastat["BASE"].ToString()) ? "0": lineaIntrastat["BASE"].ToString());
                    string unit = lineaIntrastat["UNIDADES"].ToString();
                    unidades = Convert.ToInt32(Convert.ToDouble(lineaIntrastat["UNIDADES"].ToString()));
                    //unidades = Convert.ToInt32(lineaIntrastat["UNIDADES"].ToString().Replace(",0000", ""));



                    //busco en el datatable los valores de la ficha de cuentas, clientes o proveedores
                    foreach (DataRow filaCuenta in dtCuenta.Rows)
                    {
                        if (filaCuenta["CODIC"].ToString() == codIC)
                        {
                            puerto = string.IsNullOrEmpty(filaCuenta["PUERTO"].ToString()) ? "NULL" : "'" + filaCuenta["PUERTO"].ToString() + "'" ;
                            tipoTransporte = string.IsNullOrEmpty(filaCuenta["TIPTRA"].ToString()) ? "NULL" : "'" + filaCuenta["TIPTRA"].ToString() + "'";
                            condEntregaA = string.IsNullOrEmpty(filaCuenta["CODENTA"].ToString()) ? "NULL" : "'" + filaCuenta["CODENTA"].ToString() + "'";
                            condEntregaB = string.IsNullOrEmpty(filaCuenta["CODENTB"].ToString()) ? "NULL" : "'" + filaCuenta["CODENTB"].ToString() + "'";
                            codNaturalezaA = string.IsNullOrEmpty(filaCuenta["CODNATA"].ToString()) ? "NULL" : "'" + filaCuenta["CODNATA"].ToString() + "'";
                            codNaturalezaB = string.IsNullOrEmpty(filaCuenta["CODNATB"].ToString()) ? "NULL" : "'" + filaCuenta["CODNATB"].ToString() + "'";
                            codRegEstadistico = string.IsNullOrEmpty(filaCuenta["CODREGEST"].ToString()) ? "NULL" : "'" + filaCuenta["CODREGEST"].ToString() + "'";
                            codIC = "";
                            break;
                        }
                    }

                    //busco en el datatable los valores de la ficha de artículo
                    foreach (DataRow filaArticulo in dtArticulo.Rows)
                    {
                        if (filaArticulo["CODART"].ToString() == codart)
                        {
                            codCN = filaArticulo["CODCN"].ToString();
                            codCN = string.IsNullOrEmpty(codCN) ? "NULL" : codCN;
                            peso = Convert.ToDouble(string.IsNullOrEmpty(filaArticulo["PESO"].ToString()) ? "0" : filaArticulo["PESO"].ToString());  
                            pesoEstadistico = peso * unidades; // EL PESO HAY QUE PONER PESO UNITARIO * UNIDADES
                            //VALOR ESTADÍSTICO = (PESO UNITARIO + UNIDADES) * BASE
                            valorEstadistico = ((peso * unidades) + unidades) * importeBase;
                            valorEstadistico = Math.Round(valorEstadistico, 2);
                            scriptSQL = "UPDATE __LINEINTRASTAT SET CODCN=" + codCN + 
                                ", CODFORMULA='     555'" +
                                ", PESO=" + pesoEstadistico.ToString().Replace(",", ".") +
                                ", VALORESTADISTICO=" + valorEstadistico.ToString().Replace(",", ".") +
                                ", PUERTO=" + puerto + 
                                ", TIPTRA=" + tipoTransporte + 
                                ", CODNATA=" + codNaturalezaA + 
                                ", CODNATB=" + codNaturalezaB + 
                                ", CODENTA=" + condEntregaA + 
                                ", CODENTB=" + condEntregaB + 
                                ", CODREGEST=" + codRegEstadistico + 
                                " WHERE IDINTRASTAT=" + idIntrastat + " AND NUMLIN=" + numLin;
                            sql.actualizarCampo(scriptSQL, false);
                            break;
                        }
                    }


                }
                sql.cerrarConexion();
            }
            catch (Exception ex)
            {
                idIntrastat=idIntrastat;
                numLin=numLin;
                scriptSQL = scriptSQL;

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult Resultado;
            Resultado = MessageBox.Show("¿Quiere actualizar los registros de las declaraciones de Intrastat?", "Actualizar Intrastat", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Resultado == DialogResult.Yes)
            {
                txtBEstadoProceso.Text = "PROCESANDO";
                actualizarIntrastat();
                txtBEstadoProceso.Text = "FINALIZADO";
            }
        }
    }
}
