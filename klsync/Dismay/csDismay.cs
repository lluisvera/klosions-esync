﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace klsync.Dismay
{
    class csDismay
    {

        csTallasYColoresV2 TyCV2 = new csTallasYColoresV2();
        public void procedimientoDismay()
        {
            sincronizarTallasyColoresDismay();
            asignarTallasyColoresArticulosDismay();
            borrarStockTallasyColoresDismay();
            actualizarStockTallasyColoresDismay();
        }

        private void vaciarTablasTallasyColoresDismay()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_attribute");
            mySqlConnect.borrar("ps_attribute_lang");
            mySqlConnect.borrar("ps_attribute_shop");

        }

        private void sincronizarTallasyColoresDismay()
        {
            vaciarTablasTallasyColoresDismay();
            int orden = 0, contador = 0;
            //Necesito insertar valores en 3 tablas, creo variables para cada contenido
            string Lineas = ""; //ps_attribute
            string LineasLang = "";
            string LineasShop = "";
            string codigoGrupo = "";
            string descripcion = "";
            string idCaracteristica = "";
            csSqlConnects sqlConnect = new csSqlConnects();
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            DataTable familiaTallasYColores = new DataTable("TallasColores");
            familiaTallasYColores = sqlConnect.cargarAtributosDismay();
            string defaultLangCode = mySqlConnect.defaultLangPS().ToString();
            for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
            {
                orden++;
                codigoGrupo = "1";
                descripcion = familiaTallasYColores.Rows[i].ItemArray[1].ToString();

                descripcion = descripcion.Replace("'", @"\'");
                //Utilizo el id_product como identificador del atributo

                idCaracteristica = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                if (contador > 0)
                {
                    Lineas = Lineas + ",";
                    LineasShop = LineasShop + ",";
                }

                Lineas = Lineas + "('" + idCaracteristica + "'," + codigoGrupo + "," + orden + ")";
                LineasShop = LineasShop + "('" + idCaracteristica + "',1)";

                contador++;

                if (contador > 500)
                {
                    mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
                    mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");
                    Lineas = "";
                    LineasShop = "";

                    contador = 0;
                }
            }


            mySqlConnect.InsertValoresEnTabla("ps_attribute", "(id_attribute, id_attribute_group, position)", Lineas, "");
            mySqlConnect.InsertValoresEnTabla("ps_attribute_shop", "(id_attribute, id_shop)", LineasShop, "");

            //MessageBox.Show("Exportación Finalizada");

            //Para la tabla afectada por idiomas, tenemos que repetirlo para cada idioma
            foreach (KeyValuePair<int, string> valuePair in csGlobal.IdiomasEsync)
            {
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    codigoGrupo = "1";
                    descripcion = familiaTallasYColores.Rows[i].ItemArray[1].ToString();
                    descripcion = descripcion.Replace("'", @"\'");
                    idCaracteristica = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (i > 0)
                    {
                        LineasLang = LineasLang + ",";
                    }
                    LineasLang = LineasLang + "(" + idCaracteristica + "," + defaultLangCode + ",'" + descripcion + "')";
                }
                mySqlConnect.InsertValoresEnTabla("ps_attribute_lang", "(id_attribute, id_lang,name)", LineasLang, "");
                LineasLang = "";
            }
        }

        private void asignarTallasyColoresArticulosDismay()
        {
            try
            {
                csCheckVersion checkVersion = new csCheckVersion();
                TyCV2.borrarAsignacionTallasycolores();
                //Necesito insertar valores en 3 tablas, creo variables para cada contenido
                string query = "";
                string Lineas = ""; //ps_attribute
                string LineasLang = "";
                string LineasShop = "";
                string idCombinacion = "";
                string atributoTalla = "";
                string atributoColor = "";
                string articulo = "";
                string defaultAtributoArticulo = "";
                string default_on = "0";
                string reference = "";

                csSqlScripts sqlScript = new csSqlScripts();

                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect = new csMySqlConnect();
                DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
                familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStocTallasyColoresDismay(), "");
                for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
                {
                    //articulo = familiaTallasYColores.Rows[i].ItemArray[1].ToString().Replace(" ", "");
                    //articulo = familiaTallasYColores.Rows[i].ItemArray[9].ToString();
                    articulo = familiaTallasYColores.Rows[i].ItemArray[6].ToString().Replace(" ", "");
                    reference = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (defaultAtributoArticulo != articulo)
                    {
                        default_on = "1";
                    }
                    else
                    {
                        default_on = "Null";
                    }


                    idCombinacion = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    //atributoColor = familiaTallasYColores.Rows[i].ItemArray[7].ToString().Replace(" ", "");
                    atributoTalla = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                    if (i > 0)
                    {
                        Lineas = Lineas + ",";
                        LineasLang = LineasLang + ",";
                        LineasShop = LineasShop + ",";
                    }
                    Lineas = Lineas + "(" + idCombinacion + "," + articulo + ",'" + reference + "','','','')";
                    //Lineas = Lineas + "(" + idCombinacion + "," + articulo + ",'" + reference + "','','','','')";
                    LineasLang = LineasLang + "(" + atributoTalla + "," + idCombinacion + ")";
                    if (checkVersion.checkVersión("1.6.1"))
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + "," + articulo + ")";
                    }
                    else
                    {
                        LineasShop = LineasShop + "(" + idCombinacion + ",1," + default_on + ")";
                    }
                    defaultAtributoArticulo = articulo;
                }

                mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product,reference,supplier_reference,ean13,upc)", Lineas, "");
                //mySqlConnect.InsertValoresEnTabla("ps_product_attribute", "(id_product_attribute, id_product,reference,supplier_reference,location,ean13,upc)", Lineas, "");
                mySqlConnect.InsertValoresEnTabla("ps_product_attribute_combination", "(id_attribute, id_product_attribute)", LineasLang, "");
                //mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", LineasShop, "");
                if (checkVersion.checkVersión("1.6.1"))
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on,id_product)", LineasShop, "");
                }
                else
                {
                    mySqlConnect.InsertValoresEnTabla("ps_product_attribute_shop", "(id_product_attribute, id_shop, default_on)", LineasShop, "");
                }
                //MessageBox.Show("Exportación Finalizada");
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero("actualizarStockTallasyColores: " + ex.ToString());
            }

        }

        private void actualizarStockTallasyColoresDismay()
        {
            borrarStockTallasyColoresDismay();
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csSqlScripts sqlScript = new csSqlScripts();
            int ultimoId = mySqlConnect.selectID("ps_stock_available", "id_stock_available") + 1;

            string Lineas = "";

            string idCombinacion = "";
            string cantidad = "";
            string articulo = "";

            csSqlConnects sqlConnect = new csSqlConnects();
            DataTable familiaTallasYColores = new DataTable("AsignacionTallasColores");
            familiaTallasYColores = sqlConnect.cargarDatosScriptEnTabla(sqlScript.selectStocTallasyColoresDismay(), "");
            for (int i = 0; i < familiaTallasYColores.Rows.Count; i++)
            {
                articulo = familiaTallasYColores.Rows[i].ItemArray[6].ToString().Replace(" ", "");
                //articulo = "1520";
                idCombinacion = familiaTallasYColores.Rows[i].ItemArray[0].ToString().Replace(" ", "");
                cantidad = familiaTallasYColores.Rows[i].ItemArray[7].ToString().Replace(" ", "");
                if (cantidad == "")
                {
                    cantidad = "0";
                }
                if (i > 0)
                {
                    Lineas = Lineas + ",";

                }
                Lineas = Lineas + "(" + ultimoId.ToString() + "," + articulo + "," + idCombinacion + ",1," + cantidad + ",0,2)";
                ultimoId = ultimoId + 1;
            }

            mySqlConnect.InsertValoresEnTabla("ps_stock_available", "(id_stock_available, id_product,id_product_attribute,id_shop,quantity, depends_on_stock, out_of_stock)", Lineas, "");
            //MessageBox.Show("Stock Actualizado");
        }

        private void borrarStockTallasyColoresDismay()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            mySqlConnect.borrar("ps_stock_available", " id_product_attribute<>0 ");
        }

    }
}
