﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frRepasatClientes : Form
    {

        private static frRepasatClientes m_FormDefInstance;
        public static frRepasatClientes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRepasatClientes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frRepasatClientes()
        {
            InitializeComponent();
        }
        private void btCargarClientes_Click(object sender, EventArgs e)
        {
            cargarClientesRPST();
        }

        private void cargarClientesRPST()
        {
            csSqlScriptRepasat csScriptRPST = new csSqlScriptRepasat();
            conectarDB(csScriptRPST.selectClientesRPST(), false, dgvClientesRPST);
        }

        private void conectarDB(string sqlScript, bool queryDocs, DataGridView dgv)
        {
            csMySqlConnect conector = new csMySqlConnect();


            try
            {
                MySqlConnection conn = new MySqlConnection(conector.conexionDestino());
                conn.Open();

                MySqlDataAdapter MyDA = new MySqlDataAdapter();
                
                MyDA.SelectCommand = new MySqlCommand(sqlScript, conn);
                
                DataTable table = new DataTable();
                MyDA.Fill(table);

                BindingSource bSource = new BindingSource();
                bSource.DataSource = table;

                dgv.DataSource = bSource;

                if (queryDocs)
                {

                    int numeroDocumentos = 0;
                    int contador = 0;
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        if (table.Rows[i].ItemArray[11].ToString() == "0")
                        {
                            contador = contador + 1;
                        }

                    }
                    numeroDocumentos = table.Rows.Count - contador;
                    ////MessageBox.Show("hay " + table.Rows.Count.ToString() + " filas en el grid");
                    ////MessageBox.Show("Hay " + numeroDocumentos);
                }


            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                Close();
            }

        }

        private void btCargarClientesA3_Click(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = csGlobal.ServerA3;
            csb.InitialCatalog = csGlobal.databaseA3;
            csb.IntegratedSecurity = false;
            csb.UserID = csGlobal.userA3;
            csb.Password = csGlobal.passwordA3;
            return csb.ConnectionString;
        }

        private void cargarClientesA3()
        {
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString =csGlobal.cadenaConexion;

            dataConnection.Open();

            csSqlScripts sqlScript = new csSqlScripts();
            SqlDataAdapter a = new SqlDataAdapter(sqlScript.selectClientesA3RPST(), dataConnection);

            DataTable t = new DataTable();
            a.Fill(t);
            dgvClientesRPST.DataSource = t;


        }

        private void  InsertarClientes(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();
            string Lineas = "";
            string nomCliente="";
            string razon="";
            string cif="";
            string telefono="";
            string email="";
            string web="";
            string codExterno="";


            for (int i=0; i< dgvClientesRPST.SelectedRows.Count;i++)
            {
                nomCliente = dgvClientesRPST.SelectedRows[i].Cells["NOMCLI"].Value.ToString();
                razon = dgvClientesRPST.SelectedRows[i].Cells["RAZON"].Value.ToString();
                cif = dgvClientesRPST.SelectedRows[i].Cells["NIFCLI"].Value.ToString();
                telefono = dgvClientesRPST.SelectedRows[i].Cells["TELCLI"].Value.ToString();
                telefono=telefono.Replace(".","");
                telefono=telefono.Replace(" ","");
                telefono=telefono.Replace("-","");

                email = dgvClientesRPST.SelectedRows[i].Cells["E_MAIL"].Value.ToString(); 
                web= dgvClientesRPST.Rows[i].Cells["PAGINAWEB"].Value.ToString() ;
                codExterno = dgvClientesRPST.SelectedRows[i].Cells["CODCLI"].Value.ToString();
                codExterno = codExterno.Replace(" ", "");
                if (i > 0)
                    {
                        Lineas = Lineas + ",";
                    }
                Lineas = Lineas + "(101,'" + nomCliente + "','" + razon + "','" + cif + "'," + telefono + ",'" + web + "','" + email + "','" + codExterno + "',1)";
                }
                mysqlconect.InsertValoresEnTabla("clientes", "(idEntidadCli,nomCli,razonSocialcli, cifCli,tel1,webCli,emailCli,codExternoCli,activoCli)", Lineas, "");

                //MessageBox.Show("Exportación Finalizada");

            }


        private void BorrarClientes(DataGridViewSelectedRowCollection Filas)
        {
            csSqlScriptRepasat scriptRepasat = new csSqlScriptRepasat();
            csMySqlConnect mysqlconect = new csMySqlConnect();

            foreach (DataGridViewRow fila in Filas)
            {
                mysqlconect.insertItems(scriptRepasat.borrarClientesRPST(fila.Cells["CODCLI"].Value.ToString()));

            }

        }

        private void seleccionarTodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvClientesRPST.SelectAll();
        }

        private void exportarARepasatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            csSqlConnects sqlConnect = new csSqlConnects();
            InsertarClientes(dgvClientesRPST.SelectedRows);
            sqlConnect.actualizarclientesEnlazados(mySqlConnect.selectClientesEnlazadosRPST("101"));
            
        }

        private void borrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BorrarClientes(dgvClientesRPST.SelectedRows);
            cargarClientesRPST();
        }

        private void btSincronizarClientes_Click(object sender, EventArgs e)
        {
            actualizarClientesRPST();
        }

        private void actualizarClientesRPST()
        {
            csMySqlConnect mySqlConnect = new csMySqlConnect();
            string nomCli="";
            string razon="";
            string cifCli="";
            string webcli="";
            string setValue="";
            string idCliRPST="";

            for (int i = 0; i < dgvClientesRPST.SelectedRows.Count; i++)
            { 
                nomCli=dgvClientesRPST.SelectedRows[i].Cells["NOMCLI"].Value.ToString();
                razon=dgvClientesRPST.SelectedRows[i].Cells["RAZON"].Value.ToString();
                cifCli=dgvClientesRPST.SelectedRows[i].Cells["NIFCLI"].Value.ToString();
                idCliRPST=dgvClientesRPST.SelectedRows[i].Cells["IDREPASAT"].Value.ToString();
                webcli = dgvClientesRPST.SelectedRows[i].Cells["PAGINAWEB"].Value.ToString();

                setValue= "nomcli='" + nomCli + "',razonSocialCli='"+razon+"',cifCli='" + cifCli + "'";
                mySqlConnect.actualizarValoresEnTabla("clientes", setValue, "idCli", idCliRPST);

                //MessageBox.Show("Registro Actualizado");
            
            
            
            }
        
        }

    }
}
