﻿namespace klsync
{
    partial class frTesting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frTesting));
            this.contextActualizarIdentificadores = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.actualizarDesdeAbajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarDesdeArribaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.btnReminder = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelProgresoTags = new System.Windows.Forms.Label();
            this.progressBarTags = new System.Windows.Forms.ProgressBar();
            this.btnPSProductTag = new System.Windows.Forms.Button();
            this.btnUpdateShortDescription = new System.Windows.Forms.Button();
            this.btnSincFamilias = new System.Windows.Forms.Button();
            this.btnRegenerarImagenes = new System.Windows.Forms.Button();
            this.btAlbRegularizacion = new System.Windows.Forms.Button();
            this.btnFranchutes = new System.Windows.Forms.Button();
            this.btnSubirImagen = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btSetColorsPSToA3 = new System.Windows.Forms.Button();
            this.btAsignarColores = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnUpdateCategories = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnProductsWithNoImage = new System.Windows.Forms.Button();
            this.btnIdParent = new System.Windows.Forms.Button();
            this.btnSetProductAttribute = new System.Windows.Forms.Button();
            this.btnDeleteProductAttribute = new System.Windows.Forms.Button();
            this.btnUpdateStock = new System.Windows.Forms.Button();
            this.btnDownMarcas = new System.Windows.Forms.Button();
            this.btnDownCar = new System.Windows.Forms.Button();
            this.btnDownCat = new System.Windows.Forms.Button();
            this.btnBorrarMarcas = new System.Windows.Forms.Button();
            this.btnBorrarCar = new System.Windows.Forms.Button();
            this.btnBorrarCat = new System.Windows.Forms.Button();
            this.tabUtils = new System.Windows.Forms.TabPage();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnResetTYCPrestashop = new System.Windows.Forms.Button();
            this.btnSubirCatA3toPS = new System.Windows.Forms.Button();
            this.txtThagsonCaracteristicas = new System.Windows.Forms.TextBox();
            this.btnCaracteristicasThagson = new System.Windows.Forms.Button();
            this.txtActivoInactivo = new System.Windows.Forms.TextBox();
            this.btnActivoInactivo = new System.Windows.Forms.Button();
            this.dgvActivoInactivo = new System.Windows.Forms.DataGridView();
            this.cmsActivoInactivo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.desactivarPsproductshopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.activarPsproductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desactivarPsproductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.btnCSVMover = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btnMoverImagenes = new System.Windows.Forms.Button();
            this.tbFechaRecepcion = new System.Windows.Forms.TextBox();
            this.btActualizarFechaRecepcion = new System.Windows.Forms.Button();
            this.tbDefaultCategory = new System.Windows.Forms.TextBox();
            this.btCheckProductCategory = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btResetAttributes = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btReorganizarAttributos = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.cmsPedidos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pEDIDOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.borrarPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarEstadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvCombinaciones = new System.Windows.Forms.DataGridView();
            this.contextMenuProd = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.artículosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sincronizarAtributosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artículoSeleccionadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.resetearAtributosSelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarCombinacionVisibleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarCombinaciónYSincronizarReseteandoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tssCombinaciones = new System.Windows.Forms.ToolStripButton();
            this.tabStock = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btNextAvailableDate = new System.Windows.Forms.Button();
            this.tabCategories = new System.Windows.Forms.TabPage();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.btReorgLevelCategories = new System.Windows.Forms.Button();
            this.tabLogistica = new System.Windows.Forms.TabPage();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.tabAttributos = new System.Windows.Forms.TabPage();
            this.tbCombinacionesVisibles = new System.Windows.Forms.TextBox();
            this.btCombinacionesVisibles = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnLoadOrders = new System.Windows.Forms.Button();
            this.LINQ = new System.Windows.Forms.Button();
            this.contextActualizarIdentificadores.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabUtils.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActivoInactivo)).BeginInit();
            this.cmsActivoInactivo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.cmsPedidos.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCombinaciones)).BeginInit();
            this.contextMenuProd.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabStock.SuspendLayout();
            this.tabCategories.SuspendLayout();
            this.tabLogistica.SuspendLayout();
            this.tabAttributos.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextActualizarIdentificadores
            // 
            this.contextActualizarIdentificadores.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextActualizarIdentificadores.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actualizarDesdeAbajoToolStripMenuItem,
            this.actualizarDesdeArribaToolStripMenuItem});
            this.contextActualizarIdentificadores.Name = "contextActualizarIdentificadores";
            this.contextActualizarIdentificadores.Size = new System.Drawing.Size(239, 56);
            // 
            // actualizarDesdeAbajoToolStripMenuItem
            // 
            this.actualizarDesdeAbajoToolStripMenuItem.Name = "actualizarDesdeAbajoToolStripMenuItem";
            this.actualizarDesdeAbajoToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.actualizarDesdeAbajoToolStripMenuItem.Text = "Actualizar desde abajo";
            this.actualizarDesdeAbajoToolStripMenuItem.Click += new System.EventHandler(this.actualizarDesdeAbajoToolStripMenuItem_Click);
            // 
            // actualizarDesdeArribaToolStripMenuItem
            // 
            this.actualizarDesdeArribaToolStripMenuItem.Enabled = false;
            this.actualizarDesdeArribaToolStripMenuItem.Name = "actualizarDesdeArribaToolStripMenuItem";
            this.actualizarDesdeArribaToolStripMenuItem.Size = new System.Drawing.Size(238, 26);
            this.actualizarDesdeArribaToolStripMenuItem.Text = "Actualizar desde arriba";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabUtils);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabStock);
            this.tabControl1.Controls.Add(this.tabCategories);
            this.tabControl1.Controls.Add(this.tabLogistica);
            this.tabControl1.Controls.Add(this.tabAttributos);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1234, 657);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LINQ);
            this.tabPage4.Controls.Add(this.button3);
            this.tabPage4.Controls.Add(this.btnReminder);
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.button4);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Controls.Add(this.btnSincFamilias);
            this.tabPage4.Controls.Add(this.btnRegenerarImagenes);
            this.tabPage4.Controls.Add(this.btAlbRegularizacion);
            this.tabPage4.Controls.Add(this.btnFranchutes);
            this.tabPage4.Controls.Add(this.btnSubirImagen);
            this.tabPage4.Controls.Add(this.button2);
            this.tabPage4.Controls.Add(this.btSetColorsPSToA3);
            this.tabPage4.Controls.Add(this.btAsignarColores);
            this.tabPage4.Controls.Add(this.progressBar1);
            this.tabPage4.Controls.Add(this.btnUpdateCategories);
            this.tabPage4.Controls.Add(this.button1);
            this.tabPage4.Controls.Add(this.btnProductsWithNoImage);
            this.tabPage4.Controls.Add(this.btnIdParent);
            this.tabPage4.Controls.Add(this.btnSetProductAttribute);
            this.tabPage4.Controls.Add(this.btnDeleteProductAttribute);
            this.tabPage4.Controls.Add(this.btnUpdateStock);
            this.tabPage4.Controls.Add(this.btnDownMarcas);
            this.tabPage4.Controls.Add(this.btnDownCar);
            this.tabPage4.Controls.Add(this.btnDownCat);
            this.tabPage4.Controls.Add(this.btnBorrarMarcas);
            this.tabPage4.Controls.Add(this.btnBorrarCar);
            this.tabPage4.Controls.Add(this.btnBorrarCat);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1226, 628);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Admin";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1034, 137);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(163, 52);
            this.button3.TabIndex = 33;
            this.button3.Text = "Reminder PDF";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnReminder
            // 
            this.btnReminder.Location = new System.Drawing.Point(1034, 79);
            this.btnReminder.Name = "btnReminder";
            this.btnReminder.Size = new System.Drawing.Size(163, 52);
            this.btnReminder.TabIndex = 32;
            this.btnReminder.Text = "Reminder Payment";
            this.btnReminder.UseVisualStyleBackColor = true;
            this.btnReminder.Click += new System.EventHandler(this.btnReminder_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(498, 547);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(163, 52);
            this.button8.TabIndex = 31;
            this.button8.Text = "Albarán de Regularización";
            this.button8.Click += new System.EventHandler(this.btActualizarRecEquivalenciaVilardell);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(783, 195);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(273, 52);
            this.button6.TabIndex = 30;
            this.button6.Text = "Setear Grupos y Atributos";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1034, 21);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(163, 52);
            this.button5.TabIndex = 29;
            this.button5.Text = "Jason to Repasat 2";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(498, 137);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(273, 52);
            this.button4.TabIndex = 27;
            this.button4.Text = "Actualizar Marcar de A3 a PS";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.labelProgresoTags);
            this.groupBox1.Controls.Add(this.progressBarTags);
            this.groupBox1.Controls.Add(this.btnPSProductTag);
            this.groupBox1.Controls.Add(this.btnUpdateShortDescription);
            this.groupBox1.Location = new System.Drawing.Point(667, 320);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 221);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tags";
            // 
            // labelProgresoTags
            // 
            this.labelProgresoTags.AutoSize = true;
            this.labelProgresoTags.Location = new System.Drawing.Point(7, 140);
            this.labelProgresoTags.Name = "labelProgresoTags";
            this.labelProgresoTags.Size = new System.Drawing.Size(66, 17);
            this.labelProgresoTags.TabIndex = 4;
            this.labelProgresoTags.Text = "Progreso";
            // 
            // progressBarTags
            // 
            this.progressBarTags.Location = new System.Drawing.Point(7, 85);
            this.progressBarTags.Name = "progressBarTags";
            this.progressBarTags.Size = new System.Drawing.Size(403, 48);
            this.progressBarTags.TabIndex = 3;
            // 
            // btnPSProductTag
            // 
            this.btnPSProductTag.Location = new System.Drawing.Point(212, 26);
            this.btnPSProductTag.Name = "btnPSProductTag";
            this.btnPSProductTag.Size = new System.Drawing.Size(198, 52);
            this.btnPSProductTag.TabIndex = 2;
            this.btnPSProductTag.Text = "2. Pob. ps_product_tag";
            this.btnPSProductTag.UseVisualStyleBackColor = true;
            // 
            // btnUpdateShortDescription
            // 
            this.btnUpdateShortDescription.Location = new System.Drawing.Point(6, 25);
            this.btnUpdateShortDescription.Name = "btnUpdateShortDescription";
            this.btnUpdateShortDescription.Size = new System.Drawing.Size(200, 53);
            this.btnUpdateShortDescription.TabIndex = 1;
            this.btnUpdateShortDescription.Text = "1. Poblar tabla ps_tag";
            this.btnUpdateShortDescription.UseVisualStyleBackColor = true;
            // 
            // btnSincFamilias
            // 
            this.btnSincFamilias.Location = new System.Drawing.Point(219, 373);
            this.btnSincFamilias.Name = "btnSincFamilias";
            this.btnSincFamilias.Size = new System.Drawing.Size(190, 52);
            this.btnSincFamilias.TabIndex = 25;
            this.btnSincFamilias.Text = "Sincronizar Familias";
            this.btnSincFamilias.UseVisualStyleBackColor = true;
            this.btnSincFamilias.Click += new System.EventHandler(this.btnSincFamilias_Click_1);
            // 
            // btnRegenerarImagenes
            // 
            this.btnRegenerarImagenes.Location = new System.Drawing.Point(219, 315);
            this.btnRegenerarImagenes.Name = "btnRegenerarImagenes";
            this.btnRegenerarImagenes.Size = new System.Drawing.Size(190, 52);
            this.btnRegenerarImagenes.TabIndex = 24;
            this.btnRegenerarImagenes.Text = "Regenerar Imágenes";
            this.btnRegenerarImagenes.UseVisualStyleBackColor = true;
            // 
            // btAlbRegularizacion
            // 
            this.btAlbRegularizacion.Location = new System.Drawing.Point(498, 489);
            this.btAlbRegularizacion.Name = "btAlbRegularizacion";
            this.btAlbRegularizacion.Size = new System.Drawing.Size(163, 52);
            this.btAlbRegularizacion.TabIndex = 23;
            this.btAlbRegularizacion.Text = "Albarán de Regularización";
            // 
            // btnFranchutes
            // 
            this.btnFranchutes.Location = new System.Drawing.Point(23, 315);
            this.btnFranchutes.Name = "btnFranchutes";
            this.btnFranchutes.Size = new System.Drawing.Size(190, 52);
            this.btnFranchutes.TabIndex = 20;
            this.btnFranchutes.Text = "Imágenes bastide";
            this.btnFranchutes.UseVisualStyleBackColor = true;
            // 
            // btnSubirImagen
            // 
            this.btnSubirImagen.Location = new System.Drawing.Point(498, 373);
            this.btnSubirImagen.Name = "btnSubirImagen";
            this.btnSubirImagen.Size = new System.Drawing.Size(163, 52);
            this.btnSubirImagen.TabIndex = 21;
            this.btnSubirImagen.Text = "Selector TyC";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(23, 195);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(190, 52);
            this.button2.TabIndex = 18;
            this.button2.Text = "Borrar Tablas Atributos A3ERP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btSetColorsPSToA3
            // 
            this.btSetColorsPSToA3.Location = new System.Drawing.Point(23, 547);
            this.btSetColorsPSToA3.Name = "btSetColorsPSToA3";
            this.btSetColorsPSToA3.Size = new System.Drawing.Size(190, 52);
            this.btSetColorsPSToA3.TabIndex = 17;
            this.btSetColorsPSToA3.Text = "Asignar Colores PS --> A3";
            this.btSetColorsPSToA3.UseVisualStyleBackColor = true;
            // 
            // btAsignarColores
            // 
            this.btAsignarColores.Location = new System.Drawing.Point(23, 489);
            this.btAsignarColores.Name = "btAsignarColores";
            this.btAsignarColores.Size = new System.Drawing.Size(190, 52);
            this.btAsignarColores.TabIndex = 16;
            this.btAsignarColores.Text = "Asignar Colores A3 -->PS";
            this.btAsignarColores.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(899, 567);
            this.progressBar1.MarqueeAnimationSpeed = 1000;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(157, 52);
            this.progressBar1.TabIndex = 15;
            // 
            // btnUpdateCategories
            // 
            this.btnUpdateCategories.Location = new System.Drawing.Point(667, 21);
            this.btnUpdateCategories.Name = "btnUpdateCategories";
            this.btnUpdateCategories.Size = new System.Drawing.Size(163, 52);
            this.btnUpdateCategories.TabIndex = 14;
            this.btnUpdateCategories.Text = "Actualizar";
            this.btnUpdateCategories.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(498, 315);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 52);
            this.button1.TabIndex = 13;
            this.button1.Text = "Replog";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnProductsWithNoImage
            // 
            this.btnProductsWithNoImage.Location = new System.Drawing.Point(23, 431);
            this.btnProductsWithNoImage.Name = "btnProductsWithNoImage";
            this.btnProductsWithNoImage.Size = new System.Drawing.Size(190, 52);
            this.btnProductsWithNoImage.TabIndex = 11;
            this.btnProductsWithNoImage.Text = "Productos sin imagen";
            this.btnProductsWithNoImage.UseVisualStyleBackColor = true;
            // 
            // btnIdParent
            // 
            this.btnIdParent.Location = new System.Drawing.Point(498, 21);
            this.btnIdParent.Name = "btnIdParent";
            this.btnIdParent.Size = new System.Drawing.Size(163, 52);
            this.btnIdParent.TabIndex = 10;
            this.btnIdParent.Text = "Act. id_parent";
            this.btnIdParent.UseVisualStyleBackColor = true;
            // 
            // btnSetProductAttribute
            // 
            this.btnSetProductAttribute.Location = new System.Drawing.Point(498, 195);
            this.btnSetProductAttribute.Name = "btnSetProductAttribute";
            this.btnSetProductAttribute.Size = new System.Drawing.Size(273, 52);
            this.btnSetProductAttribute.TabIndex = 8;
            this.btnSetProductAttribute.Text = "Setear Atributo de producto";
            this.btnSetProductAttribute.UseVisualStyleBackColor = true;
            this.btnSetProductAttribute.Click += new System.EventHandler(this.btnSetProductAttribute_Click);
            // 
            // btnDeleteProductAttribute
            // 
            this.btnDeleteProductAttribute.Location = new System.Drawing.Point(219, 195);
            this.btnDeleteProductAttribute.Name = "btnDeleteProductAttribute";
            this.btnDeleteProductAttribute.Size = new System.Drawing.Size(273, 52);
            this.btnDeleteProductAttribute.TabIndex = 7;
            this.btnDeleteProductAttribute.Text = "Borrar product_attribute";
            this.btnDeleteProductAttribute.UseVisualStyleBackColor = true;
            // 
            // btnUpdateStock
            // 
            this.btnUpdateStock.BackColor = System.Drawing.Color.PaleGreen;
            this.btnUpdateStock.Location = new System.Drawing.Point(23, 373);
            this.btnUpdateStock.Name = "btnUpdateStock";
            this.btnUpdateStock.Size = new System.Drawing.Size(190, 52);
            this.btnUpdateStock.TabIndex = 6;
            this.btnUpdateStock.Text = "Actualizar Stock";
            this.btnUpdateStock.UseVisualStyleBackColor = false;
            // 
            // btnDownMarcas
            // 
            this.btnDownMarcas.Location = new System.Drawing.Point(219, 137);
            this.btnDownMarcas.Name = "btnDownMarcas";
            this.btnDownMarcas.Size = new System.Drawing.Size(273, 52);
            this.btnDownMarcas.TabIndex = 5;
            this.btnDownMarcas.Text = "Descargar marcas de PS a A3";
            this.btnDownMarcas.UseVisualStyleBackColor = true;
            this.btnDownMarcas.Click += new System.EventHandler(this.btnDownMarcas_Click);
            // 
            // btnDownCar
            // 
            this.btnDownCar.Location = new System.Drawing.Point(219, 79);
            this.btnDownCar.Name = "btnDownCar";
            this.btnDownCar.Size = new System.Drawing.Size(273, 52);
            this.btnDownCar.TabIndex = 4;
            this.btnDownCar.Text = "Descargar caracteristicas de PS a A3";
            this.btnDownCar.UseVisualStyleBackColor = true;
            this.btnDownCar.Click += new System.EventHandler(this.btnDownCar_Click);
            // 
            // btnDownCat
            // 
            this.btnDownCat.Location = new System.Drawing.Point(219, 21);
            this.btnDownCat.Name = "btnDownCat";
            this.btnDownCat.Size = new System.Drawing.Size(273, 52);
            this.btnDownCat.TabIndex = 3;
            this.btnDownCat.Text = "Descargar categorias de PS a A3";
            this.btnDownCat.UseVisualStyleBackColor = true;
            this.btnDownCat.Click += new System.EventHandler(this.btnDownCat_Click);
            // 
            // btnBorrarMarcas
            // 
            this.btnBorrarMarcas.Location = new System.Drawing.Point(23, 137);
            this.btnBorrarMarcas.Name = "btnBorrarMarcas";
            this.btnBorrarMarcas.Size = new System.Drawing.Size(190, 52);
            this.btnBorrarMarcas.TabIndex = 2;
            this.btnBorrarMarcas.Text = "Borrar marcas";
            this.btnBorrarMarcas.UseVisualStyleBackColor = true;
            this.btnBorrarMarcas.Click += new System.EventHandler(this.btnBorrarMarcas_Click);
            // 
            // btnBorrarCar
            // 
            this.btnBorrarCar.Location = new System.Drawing.Point(23, 79);
            this.btnBorrarCar.Name = "btnBorrarCar";
            this.btnBorrarCar.Size = new System.Drawing.Size(190, 52);
            this.btnBorrarCar.TabIndex = 1;
            this.btnBorrarCar.Text = "Borrar caracteristicas";
            this.btnBorrarCar.UseVisualStyleBackColor = true;
            this.btnBorrarCar.Click += new System.EventHandler(this.btnBorrarCar_Click);
            // 
            // btnBorrarCat
            // 
            this.btnBorrarCat.Location = new System.Drawing.Point(23, 21);
            this.btnBorrarCat.Name = "btnBorrarCat";
            this.btnBorrarCat.Size = new System.Drawing.Size(190, 52);
            this.btnBorrarCat.TabIndex = 0;
            this.btnBorrarCat.Text = "Borrar categorias";
            this.btnBorrarCat.UseVisualStyleBackColor = true;
            this.btnBorrarCat.Click += new System.EventHandler(this.btnBorrarCat_Click);
            // 
            // tabUtils
            // 
            this.tabUtils.Controls.Add(this.textBox6);
            this.tabUtils.Controls.Add(this.btnResetTYCPrestashop);
            this.tabUtils.Controls.Add(this.btnSubirCatA3toPS);
            this.tabUtils.Controls.Add(this.txtThagsonCaracteristicas);
            this.tabUtils.Controls.Add(this.btnCaracteristicasThagson);
            this.tabUtils.Controls.Add(this.txtActivoInactivo);
            this.tabUtils.Controls.Add(this.btnActivoInactivo);
            this.tabUtils.Controls.Add(this.dgvActivoInactivo);
            this.tabUtils.Controls.Add(this.textBox5);
            this.tabUtils.Controls.Add(this.btnCSVMover);
            this.tabUtils.Controls.Add(this.textBox4);
            this.tabUtils.Controls.Add(this.btnMoverImagenes);
            this.tabUtils.Controls.Add(this.tbFechaRecepcion);
            this.tabUtils.Controls.Add(this.btActualizarFechaRecepcion);
            this.tabUtils.Controls.Add(this.tbDefaultCategory);
            this.tabUtils.Controls.Add(this.btCheckProductCategory);
            this.tabUtils.Controls.Add(this.textBox2);
            this.tabUtils.Controls.Add(this.btResetAttributes);
            this.tabUtils.Controls.Add(this.textBox1);
            this.tabUtils.Controls.Add(this.btReorganizarAttributos);
            this.tabUtils.Location = new System.Drawing.Point(4, 25);
            this.tabUtils.Name = "tabUtils";
            this.tabUtils.Size = new System.Drawing.Size(1226, 628);
            this.tabUtils.TabIndex = 4;
            this.tabUtils.Text = "Utilidades";
            this.tabUtils.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(215, 156);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(426, 59);
            this.textBox6.TabIndex = 25;
            this.textBox6.Text = "Borrar todo lo relacionado con Atributos en Prestashop\r\n(Grupos de Atributos y At" +
    "ributos)";
            // 
            // btnResetTYCPrestashop
            // 
            this.btnResetTYCPrestashop.Location = new System.Drawing.Point(17, 156);
            this.btnResetTYCPrestashop.Name = "btnResetTYCPrestashop";
            this.btnResetTYCPrestashop.Size = new System.Drawing.Size(128, 59);
            this.btnResetTYCPrestashop.TabIndex = 24;
            this.btnResetTYCPrestashop.Text = "Resetear Tallas y Colores";
            this.btnResetTYCPrestashop.UseVisualStyleBackColor = true;
            this.btnResetTYCPrestashop.Click += new System.EventHandler(this.btnResetTYCPrestashop_Click);
            // 
            // btnSubirCatA3toPS
            // 
            this.btnSubirCatA3toPS.Location = new System.Drawing.Point(17, 552);
            this.btnSubirCatA3toPS.Name = "btnSubirCatA3toPS";
            this.btnSubirCatA3toPS.Size = new System.Drawing.Size(128, 52);
            this.btnSubirCatA3toPS.TabIndex = 23;
            this.btnSubirCatA3toPS.Text = "Subir Categorias Articulo";
            this.btnSubirCatA3toPS.UseVisualStyleBackColor = true;
            // 
            // txtThagsonCaracteristicas
            // 
            this.txtThagsonCaracteristicas.Location = new System.Drawing.Point(215, 487);
            this.txtThagsonCaracteristicas.Multiline = true;
            this.txtThagsonCaracteristicas.Name = "txtThagsonCaracteristicas";
            this.txtThagsonCaracteristicas.Size = new System.Drawing.Size(426, 59);
            this.txtThagsonCaracteristicas.TabIndex = 14;
            this.txtThagsonCaracteristicas.Text = "Proceso que copia las categorias de los parametrizables a las categorias de A3";
            // 
            // btnCaracteristicasThagson
            // 
            this.btnCaracteristicasThagson.Location = new System.Drawing.Point(17, 487);
            this.btnCaracteristicasThagson.Name = "btnCaracteristicasThagson";
            this.btnCaracteristicasThagson.Size = new System.Drawing.Size(128, 59);
            this.btnCaracteristicasThagson.TabIndex = 13;
            this.btnCaracteristicasThagson.Text = "Caracteristicas Thagson";
            this.btnCaracteristicasThagson.UseVisualStyleBackColor = true;
            // 
            // txtActivoInactivo
            // 
            this.txtActivoInactivo.Location = new System.Drawing.Point(215, 422);
            this.txtActivoInactivo.Multiline = true;
            this.txtActivoInactivo.Name = "txtActivoInactivo";
            this.txtActivoInactivo.Size = new System.Drawing.Size(426, 59);
            this.txtActivoInactivo.TabIndex = 12;
            this.txtActivoInactivo.Text = "Cargas las discrepancias de los articulos (activo/inactivo) en el datagrid de la " +
    "derecha";
            // 
            // btnActivoInactivo
            // 
            this.btnActivoInactivo.Location = new System.Drawing.Point(17, 422);
            this.btnActivoInactivo.Name = "btnActivoInactivo";
            this.btnActivoInactivo.Size = new System.Drawing.Size(128, 59);
            this.btnActivoInactivo.TabIndex = 11;
            this.btnActivoInactivo.Text = "Articulos activo/inactivo";
            this.btnActivoInactivo.UseVisualStyleBackColor = true;
            // 
            // dgvActivoInactivo
            // 
            this.dgvActivoInactivo.AllowUserToAddRows = false;
            this.dgvActivoInactivo.AllowUserToDeleteRows = false;
            this.dgvActivoInactivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActivoInactivo.ContextMenuStrip = this.cmsActivoInactivo;
            this.dgvActivoInactivo.Location = new System.Drawing.Point(647, 26);
            this.dgvActivoInactivo.Name = "dgvActivoInactivo";
            this.dgvActivoInactivo.ReadOnly = true;
            this.dgvActivoInactivo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvActivoInactivo.Size = new System.Drawing.Size(474, 384);
            this.dgvActivoInactivo.TabIndex = 10;
            // 
            // cmsActivoInactivo
            // 
            this.cmsActivoInactivo.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsActivoInactivo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.desactivarPsproductshopToolStripMenuItem,
            this.toolStripMenuItem3,
            this.activarPsproductToolStripMenuItem,
            this.desactivarPsproductToolStripMenuItem});
            this.cmsActivoInactivo.Name = "contextActualizarIdentificadores";
            this.cmsActivoInactivo.Size = new System.Drawing.Size(275, 114);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(274, 26);
            this.toolStripMenuItem2.Text = "Activar ps_product_shop";
            // 
            // desactivarPsproductshopToolStripMenuItem
            // 
            this.desactivarPsproductshopToolStripMenuItem.Name = "desactivarPsproductshopToolStripMenuItem";
            this.desactivarPsproductshopToolStripMenuItem.Size = new System.Drawing.Size(274, 26);
            this.desactivarPsproductshopToolStripMenuItem.Text = "Desactivar ps_product_shop";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(271, 6);
            // 
            // activarPsproductToolStripMenuItem
            // 
            this.activarPsproductToolStripMenuItem.Name = "activarPsproductToolStripMenuItem";
            this.activarPsproductToolStripMenuItem.Size = new System.Drawing.Size(274, 26);
            this.activarPsproductToolStripMenuItem.Text = "Activar ps_product";
            // 
            // desactivarPsproductToolStripMenuItem
            // 
            this.desactivarPsproductToolStripMenuItem.Name = "desactivarPsproductToolStripMenuItem";
            this.desactivarPsproductToolStripMenuItem.Size = new System.Drawing.Size(274, 26);
            this.desactivarPsproductToolStripMenuItem.Text = "Desactivar ps_product";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(215, 357);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(426, 59);
            this.textBox5.TabIndex = 9;
            this.textBox5.Text = "Usando un csv, coge todos los ficheros de la carpeta que le dices a otra carpeta";
            // 
            // btnCSVMover
            // 
            this.btnCSVMover.Location = new System.Drawing.Point(17, 357);
            this.btnCSVMover.Name = "btnCSVMover";
            this.btnCSVMover.Size = new System.Drawing.Size(128, 59);
            this.btnCSVMover.TabIndex = 8;
            this.btnCSVMover.Text = "Mover archivos";
            this.btnCSVMover.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(215, 227);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(426, 59);
            this.textBox4.TabIndex = 7;
            this.textBox4.Text = "Le dices un origen y un destino y mueve todos los ficheros de dentro de las subca" +
    "rpetas al destino";
            // 
            // btnMoverImagenes
            // 
            this.btnMoverImagenes.Location = new System.Drawing.Point(17, 227);
            this.btnMoverImagenes.Name = "btnMoverImagenes";
            this.btnMoverImagenes.Size = new System.Drawing.Size(128, 59);
            this.btnMoverImagenes.TabIndex = 6;
            this.btnMoverImagenes.Text = "Mover archivos";
            this.btnMoverImagenes.UseVisualStyleBackColor = true;
            // 
            // tbFechaRecepcion
            // 
            this.tbFechaRecepcion.Location = new System.Drawing.Point(215, 236);
            this.tbFechaRecepcion.Multiline = true;
            this.tbFechaRecepcion.Name = "tbFechaRecepcion";
            this.tbFechaRecepcion.Size = new System.Drawing.Size(426, 59);
            this.tbFechaRecepcion.TabIndex = 7;
            this.tbFechaRecepcion.Text = "Actualizar la fecha de recepción de los artículos";
            // 
            // btActualizarFechaRecepcion
            // 
            this.btActualizarFechaRecepcion.Location = new System.Drawing.Point(993, 565);
            this.btActualizarFechaRecepcion.Name = "btActualizarFechaRecepcion";
            this.btActualizarFechaRecepcion.Size = new System.Drawing.Size(128, 59);
            this.btActualizarFechaRecepcion.TabIndex = 6;
            this.btActualizarFechaRecepcion.Text = "Actualizar Fecha Recepción";
            this.btActualizarFechaRecepcion.UseVisualStyleBackColor = true;
            // 
            // tbDefaultCategory
            // 
            this.tbDefaultCategory.Location = new System.Drawing.Point(215, 292);
            this.tbDefaultCategory.Multiline = true;
            this.tbDefaultCategory.Name = "tbDefaultCategory";
            this.tbDefaultCategory.Size = new System.Drawing.Size(426, 59);
            this.tbDefaultCategory.TabIndex = 5;
            this.tbDefaultCategory.Text = "Asigna la categoria por defecto a todos los artículos \r\nque no tengan una categor" +
    "ía asignada.";
            // 
            // btCheckProductCategory
            // 
            this.btCheckProductCategory.Location = new System.Drawing.Point(17, 292);
            this.btCheckProductCategory.Name = "btCheckProductCategory";
            this.btCheckProductCategory.Size = new System.Drawing.Size(128, 59);
            this.btCheckProductCategory.TabIndex = 4;
            this.btCheckProductCategory.Text = "Asignar Categorías Default";
            this.btCheckProductCategory.UseVisualStyleBackColor = true;
            this.btCheckProductCategory.Click += new System.EventHandler(this.btCheckProductCategory_Click_1);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(215, 91);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(426, 59);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "Borrar todos los atributos de Prestashop";
            // 
            // btResetAttributes
            // 
            this.btResetAttributes.Location = new System.Drawing.Point(17, 91);
            this.btResetAttributes.Name = "btResetAttributes";
            this.btResetAttributes.Size = new System.Drawing.Size(128, 59);
            this.btResetAttributes.TabIndex = 2;
            this.btResetAttributes.Text = "Resetear Atributos";
            this.btResetAttributes.UseVisualStyleBackColor = true;
            this.btResetAttributes.Click += new System.EventHandler(this.btResetAttributes_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(215, 26);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(426, 59);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Reorganiza y reordena todos los atributos por cada grupo de Atributos";
            // 
            // btReorganizarAttributos
            // 
            this.btReorganizarAttributos.Location = new System.Drawing.Point(17, 26);
            this.btReorganizarAttributos.Name = "btReorganizarAttributos";
            this.btReorganizarAttributos.Size = new System.Drawing.Size(128, 59);
            this.btReorganizarAttributos.TabIndex = 0;
            this.btReorganizarAttributos.Text = "Reorganizar Atributos";
            this.btReorganizarAttributos.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvPedidos);
            this.tabPage1.Controls.Add(this.toolStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1226, 628);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Pedidos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidos.ContextMenuStrip = this.cmsPedidos;
            this.dgvPedidos.Location = new System.Drawing.Point(5, 31);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.ReadOnly = true;
            this.dgvPedidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPedidos.Size = new System.Drawing.Size(1215, 593);
            this.dgvPedidos.TabIndex = 1;
            // 
            // cmsPedidos
            // 
            this.cmsPedidos.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsPedidos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pEDIDOSToolStripMenuItem,
            this.toolStripMenuItem1,
            this.borrarPedidoToolStripMenuItem,
            this.cambiarEstadoToolStripMenuItem});
            this.cmsPedidos.Name = "cmsPedidos";
            this.cmsPedidos.Size = new System.Drawing.Size(284, 88);
            // 
            // pEDIDOSToolStripMenuItem
            // 
            this.pEDIDOSToolStripMenuItem.Enabled = false;
            this.pEDIDOSToolStripMenuItem.Name = "pEDIDOSToolStripMenuItem";
            this.pEDIDOSToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.pEDIDOSToolStripMenuItem.Text = "PEDIDOS";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(280, 6);
            // 
            // borrarPedidoToolStripMenuItem
            // 
            this.borrarPedidoToolStripMenuItem.Name = "borrarPedidoToolStripMenuItem";
            this.borrarPedidoToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.borrarPedidoToolStripMenuItem.Text = "Borrar pedidos seleccionados";
            this.borrarPedidoToolStripMenuItem.Click += new System.EventHandler(this.borrarPedidoToolStripMenuItem_Click);
            // 
            // cambiarEstadoToolStripMenuItem
            // 
            this.cambiarEstadoToolStripMenuItem.Name = "cambiarEstadoToolStripMenuItem";
            this.cambiarEstadoToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.cambiarEstadoToolStripMenuItem.Text = "Cambiar estado";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1226, 28);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(135, 25);
            this.toolStripButton1.Text = "Cargar Pedidos";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvCombinaciones);
            this.tabPage2.Controls.Add(this.toolStrip2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1226, 628);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Combinaciones";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvCombinaciones
            // 
            this.dgvCombinaciones.AllowUserToAddRows = false;
            this.dgvCombinaciones.AllowUserToDeleteRows = false;
            this.dgvCombinaciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCombinaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCombinaciones.ContextMenuStrip = this.contextMenuProd;
            this.dgvCombinaciones.Location = new System.Drawing.Point(5, 31);
            this.dgvCombinaciones.Name = "dgvCombinaciones";
            this.dgvCombinaciones.ReadOnly = true;
            this.dgvCombinaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCombinaciones.Size = new System.Drawing.Size(1215, 593);
            this.dgvCombinaciones.TabIndex = 2;
            // 
            // contextMenuProd
            // 
            this.contextMenuProd.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuProd.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artículosToolStripMenuItem});
            this.contextMenuProd.Name = "contextMenuProd";
            this.contextMenuProd.Size = new System.Drawing.Size(142, 30);
            // 
            // artículosToolStripMenuItem
            // 
            this.artículosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sincronizarAtributosToolStripMenuItem,
            this.generarCombinacionVisibleToolStripMenuItem,
            this.generarCombinaciónYSincronizarReseteandoToolStripMenuItem});
            this.artículosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("artículosToolStripMenuItem.Image")));
            this.artículosToolStripMenuItem.Name = "artículosToolStripMenuItem";
            this.artículosToolStripMenuItem.Size = new System.Drawing.Size(141, 26);
            this.artículosToolStripMenuItem.Text = "Artículos";
            // 
            // sincronizarAtributosToolStripMenuItem
            // 
            this.sincronizarAtributosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artículoSeleccionadoToolStripMenuItem,
            this.todosToolStripMenuItem,
            this.toolStripMenuItem10,
            this.resetearAtributosSelToolStripMenuItem});
            this.sincronizarAtributosToolStripMenuItem.Name = "sincronizarAtributosToolStripMenuItem";
            this.sincronizarAtributosToolStripMenuItem.Size = new System.Drawing.Size(402, 26);
            this.sincronizarAtributosToolStripMenuItem.Text = "Sincronizar Atributos";
            // 
            // artículoSeleccionadoToolStripMenuItem
            // 
            this.artículoSeleccionadoToolStripMenuItem.Name = "artículoSeleccionadoToolStripMenuItem";
            this.artículoSeleccionadoToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.artículoSeleccionadoToolStripMenuItem.Text = "Sincronizar seleccionado";
            // 
            // todosToolStripMenuItem
            // 
            this.todosToolStripMenuItem.Name = "todosToolStripMenuItem";
            this.todosToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.todosToolStripMenuItem.Text = "Todos";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(248, 6);
            // 
            // resetearAtributosSelToolStripMenuItem
            // 
            this.resetearAtributosSelToolStripMenuItem.Name = "resetearAtributosSelToolStripMenuItem";
            this.resetearAtributosSelToolStripMenuItem.Size = new System.Drawing.Size(251, 26);
            this.resetearAtributosSelToolStripMenuItem.Text = "Resetear seleccionados";
            // 
            // generarCombinacionVisibleToolStripMenuItem
            // 
            this.generarCombinacionVisibleToolStripMenuItem.Name = "generarCombinacionVisibleToolStripMenuItem";
            this.generarCombinacionVisibleToolStripMenuItem.Size = new System.Drawing.Size(402, 26);
            this.generarCombinacionVisibleToolStripMenuItem.Text = "Generar combinacion visible";
            // 
            // generarCombinaciónYSincronizarReseteandoToolStripMenuItem
            // 
            this.generarCombinaciónYSincronizarReseteandoToolStripMenuItem.Name = "generarCombinaciónYSincronizarReseteandoToolStripMenuItem";
            this.generarCombinaciónYSincronizarReseteandoToolStripMenuItem.Size = new System.Drawing.Size(402, 26);
            this.generarCombinaciónYSincronizarReseteandoToolStripMenuItem.Text = "Generar combinación y sincronizar reseteando";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssCombinaciones});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1226, 28);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tssCombinaciones
            // 
            this.tssCombinaciones.Image = ((System.Drawing.Image)(resources.GetObject("tssCombinaciones.Image")));
            this.tssCombinaciones.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssCombinaciones.Name = "tssCombinaciones";
            this.tssCombinaciones.Size = new System.Drawing.Size(185, 25);
            this.tssCombinaciones.Text = "Cargar combinaciones";
            this.tssCombinaciones.Click += new System.EventHandler(this.tssCombinaciones_Click);
            // 
            // tabStock
            // 
            this.tabStock.Controls.Add(this.textBox3);
            this.tabStock.Controls.Add(this.btNextAvailableDate);
            this.tabStock.Location = new System.Drawing.Point(4, 25);
            this.tabStock.Name = "tabStock";
            this.tabStock.Size = new System.Drawing.Size(1226, 628);
            this.tabStock.TabIndex = 7;
            this.tabStock.Text = "Stock";
            this.tabStock.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(222, 19);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(426, 59);
            this.textBox3.TabIndex = 3;
            this.textBox3.Text = "Actualiza la fecha de próxima recepción";
            // 
            // btNextAvailableDate
            // 
            this.btNextAvailableDate.Location = new System.Drawing.Point(24, 19);
            this.btNextAvailableDate.Name = "btNextAvailableDate";
            this.btNextAvailableDate.Size = new System.Drawing.Size(128, 59);
            this.btNextAvailableDate.TabIndex = 2;
            this.btNextAvailableDate.Text = "Actualiza Fecha Próxima recepción";
            this.btNextAvailableDate.UseVisualStyleBackColor = true;
            this.btNextAvailableDate.Click += new System.EventHandler(this.btNextAvailableDate_Click);
            // 
            // tabCategories
            // 
            this.tabCategories.Controls.Add(this.textBox7);
            this.tabCategories.Controls.Add(this.btReorgLevelCategories);
            this.tabCategories.Location = new System.Drawing.Point(4, 25);
            this.tabCategories.Name = "tabCategories";
            this.tabCategories.Size = new System.Drawing.Size(1226, 628);
            this.tabCategories.TabIndex = 8;
            this.tabCategories.Text = "Categorias";
            this.tabCategories.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(222, 22);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(426, 59);
            this.textBox7.TabIndex = 5;
            this.textBox7.Text = "Reorganiza los niveles de categorías según su posicionamiento de padres e hijos";
            // 
            // btReorgLevelCategories
            // 
            this.btReorgLevelCategories.Location = new System.Drawing.Point(24, 22);
            this.btReorgLevelCategories.Name = "btReorgLevelCategories";
            this.btReorgLevelCategories.Size = new System.Drawing.Size(128, 59);
            this.btReorgLevelCategories.TabIndex = 4;
            this.btReorgLevelCategories.Text = "Reorganizar niveles de Categorias";
            this.btReorgLevelCategories.UseVisualStyleBackColor = true;
            this.btReorgLevelCategories.Click += new System.EventHandler(this.btReorgLevelCategories_Click);
            // 
            // tabLogistica
            // 
            this.tabLogistica.Controls.Add(this.textBox8);
            this.tabLogistica.Controls.Add(this.button7);
            this.tabLogistica.Location = new System.Drawing.Point(4, 25);
            this.tabLogistica.Name = "tabLogistica";
            this.tabLogistica.Padding = new System.Windows.Forms.Padding(3);
            this.tabLogistica.Size = new System.Drawing.Size(1226, 628);
            this.tabLogistica.TabIndex = 9;
            this.tabLogistica.Text = "Logistica Bastide";
            this.tabLogistica.UseVisualStyleBackColor = true;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(203, 21);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(426, 59);
            this.textBox8.TabIndex = 2;
            this.textBox8.Text = "Genera fichero txt para almacén logístico";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(20, 21);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(149, 48);
            this.button7.TabIndex = 0;
            this.button7.Text = "Generar Fichero Almacén";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // tabAttributos
            // 
            this.tabAttributos.Controls.Add(this.tbCombinacionesVisibles);
            this.tabAttributos.Controls.Add(this.btCombinacionesVisibles);
            this.tabAttributos.Location = new System.Drawing.Point(4, 25);
            this.tabAttributos.Name = "tabAttributos";
            this.tabAttributos.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttributos.Size = new System.Drawing.Size(1226, 628);
            this.tabAttributos.TabIndex = 10;
            this.tabAttributos.Text = "Combinaciones";
            this.tabAttributos.UseVisualStyleBackColor = true;
            // 
            // tbCombinacionesVisibles
            // 
            this.tbCombinacionesVisibles.Location = new System.Drawing.Point(210, 6);
            this.tbCombinacionesVisibles.Multiline = true;
            this.tbCombinacionesVisibles.Name = "tbCombinacionesVisibles";
            this.tbCombinacionesVisibles.Size = new System.Drawing.Size(426, 59);
            this.tbCombinacionesVisibles.TabIndex = 3;
            this.tbCombinacionesVisibles.Text = "Borra y genera todas las combinacines visibles en SQL";
            // 
            // btCombinacionesVisibles
            // 
            this.btCombinacionesVisibles.Location = new System.Drawing.Point(12, 6);
            this.btCombinacionesVisibles.Name = "btCombinacionesVisibles";
            this.btCombinacionesVisibles.Size = new System.Drawing.Size(128, 59);
            this.btCombinacionesVisibles.TabIndex = 2;
            this.btCombinacionesVisibles.Text = "Generar Combinaciones Visibles";
            this.btCombinacionesVisibles.UseVisualStyleBackColor = true;
            this.btCombinacionesVisibles.Click += new System.EventHandler(this.btCombinacionesVisibles_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnLoadOrders);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1226, 628);
            this.tabPage3.TabIndex = 11;
            this.tabPage3.Text = "Repasat";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnLoadOrders
            // 
            this.btnLoadOrders.Location = new System.Drawing.Point(0, 0);
            this.btnLoadOrders.Name = "btnLoadOrders";
            this.btnLoadOrders.Size = new System.Drawing.Size(75, 23);
            this.btnLoadOrders.TabIndex = 0;
            // 
            // frTesting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 664);
            this.Controls.Add(this.tabControl1);
            this.Name = "frTesting";
            this.Text = "Testing";
            this.contextActualizarIdentificadores.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabUtils.ResumeLayout(false);
            this.tabUtils.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActivoInactivo)).EndInit();
            this.cmsActivoInactivo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.cmsPedidos.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCombinaciones)).EndInit();
            this.contextMenuProd.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabStock.ResumeLayout(false);
            this.tabStock.PerformLayout();
            this.tabCategories.ResumeLayout(false);
            this.tabCategories.PerformLayout();
            this.tabLogistica.ResumeLayout(false);
            this.tabLogistica.PerformLayout();
            this.tabAttributos.ResumeLayout(false);
            this.tabAttributos.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextActualizarIdentificadores;
        private System.Windows.Forms.ToolStripMenuItem actualizarDesdeAbajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarDesdeArribaToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnBorrarMarcas;
        private System.Windows.Forms.Button btnBorrarCar;
        private System.Windows.Forms.Button btnBorrarCat;
        private System.Windows.Forms.Button btnDownMarcas;
        private System.Windows.Forms.Button btnDownCar;
        private System.Windows.Forms.Button btnDownCat;
        private System.Windows.Forms.Button btnUpdateStock;
        private System.Windows.Forms.Button btnDeleteProductAttribute;
        private System.Windows.Forms.Button btnSetProductAttribute;
        private System.Windows.Forms.Button btnIdParent;
        private System.Windows.Forms.Button btnProductsWithNoImage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnUpdateCategories;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btAsignarColores;
        private System.Windows.Forms.Button btSetColorsPSToA3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSubirImagen;
        private System.Windows.Forms.Button btnFranchutes;
        private System.Windows.Forms.TabPage tabUtils;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btReorganizarAttributos;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btResetAttributes;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip cmsPedidos;
        private System.Windows.Forms.ToolStripMenuItem pEDIDOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem borrarPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarEstadoToolStripMenuItem;
        private System.Windows.Forms.TextBox tbDefaultCategory;
        private System.Windows.Forms.Button btCheckProductCategory;
        private System.Windows.Forms.Button btnMoverImagenes;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button btnCSVMover;
        private System.Windows.Forms.TextBox tbFechaRecepcion;
        private System.Windows.Forms.Button btActualizarFechaRecepcion;
        private System.Windows.Forms.Button btAlbRegularizacion;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tssCombinaciones;
        private System.Windows.Forms.DataGridView dgvCombinaciones;
        private System.Windows.Forms.TextBox txtActivoInactivo;
        private System.Windows.Forms.Button btnActivoInactivo;
        private System.Windows.Forms.DataGridView dgvActivoInactivo;
        private System.Windows.Forms.ContextMenuStrip cmsActivoInactivo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem desactivarPsproductshopToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem activarPsproductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desactivarPsproductToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuProd;
        private System.Windows.Forms.ToolStripMenuItem artículosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sincronizarAtributosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artículoSeleccionadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem resetearAtributosSelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarCombinacionVisibleToolStripMenuItem;
        private System.Windows.Forms.Button btnRegenerarImagenes;
        private System.Windows.Forms.Button btnSincFamilias;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelProgresoTags;
        private System.Windows.Forms.ProgressBar progressBarTags;
        private System.Windows.Forms.Button btnPSProductTag;
        private System.Windows.Forms.Button btnUpdateShortDescription;
        private System.Windows.Forms.Button btnCaracteristicasThagson;
        private System.Windows.Forms.TextBox txtThagsonCaracteristicas;
        private System.Windows.Forms.Button btnSubirCatA3toPS;
        private System.Windows.Forms.ToolStripMenuItem generarCombinaciónYSincronizarReseteandoToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btnResetTYCPrestashop;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage tabStock;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btNextAvailableDate;
        private System.Windows.Forms.TabPage tabCategories;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button btReorgLevelCategories;
        private System.Windows.Forms.TabPage tabLogistica;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TabPage tabAttributos;
        private System.Windows.Forms.TextBox tbCombinacionesVisibles;
        private System.Windows.Forms.Button btCombinacionesVisibles;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnLoadOrders;
        private System.Windows.Forms.Button btnReminder;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button LINQ;
    }
}