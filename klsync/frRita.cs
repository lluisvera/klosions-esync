﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Net;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Serialization;
using klsync.adMan;
using System.Globalization;
using System.Drawing;
using System.Diagnostics;


/// POSIBLES CAUSAS DE QUE NO BAJEN LAS FACTURAS
/// 
/// EL CLIENTE ESTA BLOQUEADO -> MIRAR ERROR EN CORREO Y COMUNICARSELO A MIREIA
/// NO HAY LICENCIAS -> DECIRLES QUE CIERREN UN A3
/// NOTA: SIEMPRE BAJAR DESDE EL SERVIDOR. DESDE EL PC DE MIREIA A VECES NO BAJAN
namespace klsync
{
    public partial class frRita : Form
    {
        // Filtros
        private string FILTRO_FISCAL_IDENTITY = "&has_fiscal_identity=true";
        //private string FILTRO_EXTERNAL_ID = "&external_id=%22%22";
        private string FILTRO_EXTERNAL_ID = "&external_id=null";
        private string FILTRO_FRANCE = "&enterprise=adman%20france";
        private string FILTRO_SPAIN = "&enterprise=adman%20spain";
        private string FILTRO_PER_PAGE = "&per_page=50";
        private string FILTRO_STATUS = "&status=posted";

        private const bool COMPRAS = true;
        private const bool VENTAS = false;
        private string FILTRO_TOKEN_IP = @"&token=1433243290905532672";

        private Dictionary<string, string> facturasConMonedaIncorrecta = new Dictionary<string, string>();
        private Dictionary<string, string> proveedoresBloqueados = new Dictionary<string, string>();
        private Dictionary<string, string> periodoContableCerrado = new Dictionary<string, string>();

        // Variables
        public bool descargarDeNuevo = false;
        static csSqlConnects sql = new csSqlConnects();
        string periodo = sql.obtenerCampoTabla("SELECT FECHACIERRE FROM DATOSCON");
        string fechacierre_facc = sql.obtenerCampoTabla("SELECT FECHA FROM __FECHASCIERRES WHERE TIPODOCUMENTO = 'FACC'");
        string fechacierre_facv = sql.obtenerCampoTabla("SELECT FECHA FROM __FECHASCIERRES WHERE TIPODOCUMENTO = 'FACV'");
        private DataTable dt = sql.cargarDatosTablaA3("SELECT * FROM PAISES WHERE CEE = 'T'");

        private static frRita m_FormDefInstance;
        public static frRita DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frRita();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        #region RITA
        public frRita()
        {
            InitializeComponent();

            // TRUE SI QUEREMOS QUE ACTUALICE EL CLIENTE O PROVEEDOR DE RITA
            // FALSE SI ESTAMOS HACIENDO PRUEBAS Y NO QUEREMOS QUE HAGA NADA EN RITA
            

            if (csGlobal.modeDebug)
            {
                toolStripStatusLabelProgreso.ForeColor = Color.Red;
                splitContainerAdman.BackColor = Color.Red;
            }
        }
        #endregion

        #region Facturas - Invoices, third y advertiser

        public void advertisers_invoices()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    #region Declaracion de variables
                    wc.Encoding = Encoding.UTF8;
                    frSincronizarDocs sinc = new frSincronizarDocs();
                    csa3erp a3erp = new csa3erp();
                    csSqlConnects sql = new csSqlConnects();
                    Control.CheckForIllegalCrossThreadCalls = false;
                    Objetos.csTercero[] proveedor = null;
                    Objetos.csDomBanca[] domiciliacion = null;
                    int contador = 1;
                    #endregion

                    // OBTENEMOS EL OBJETO LIST DE INVOICES 
                    var json = whileJson(wc, "advertiser_invoices/list?", "&page=" + contador);

                    //Convertimos o deserializo el Json y lo separo en facturas o documentos LVG

                    var adv_invoice = JsonConvert.DeserializeObject<adMan.advertisers_invoices.List.RootObject>(json);
                    Objetos.csCabeceraDoc[] cabeceras = null;
                    Objetos.csLineaDocumento[] lineas = null;
                    int page = adv_invoice.response.pagination.page;
                    int page_count = adv_invoice.response.pagination.page_count, total_lineas = 0;

                    if (adv_invoice.response.pagination != null && adv_invoice.response.result.Count != 0)
                    {
                        while (page <= page_count)
                        {
                            progressingBar("advertiser invoices", page, Convert.ToInt32(page_count));

                            adv_invoice = JsonConvert.DeserializeObject<adMan.advertisers_invoices.List.RootObject>(json);

                            cabeceras = new Objetos.csCabeceraDoc[adv_invoice.response.result.Count];
                            proveedor = new Objetos.csTercero[adv_invoice.response.result.Count];
                            domiciliacion = new Objetos.csDomBanca[adv_invoice.response.result.Count];

                            // CALCULAMOS EL TOTAL DE LINEAS
                            for (int ii = 0; ii < adv_invoice.response.result.Count; ii++)
                            {
                                total_lineas += adv_invoice.response.result[ii].advertiser_invoice_item.Count;
                            }
                            lineas = new Objetos.csLineaDocumento[total_lineas];

                            // 1 - OBTENEMOS LA CABECERA
                            for (int i = 0; i < adv_invoice.response.result.Count; i++)
                            {
                                // Si tiene lineas
                                if (adv_invoice.response.result[i].advertiser_invoice_item.Count > 0)
                                {
                                    if (lineas == null)
                                    {
                                        lineas = new Objetos.csLineaDocumento[adv_invoice.response.result.Count];
                                    }

                                    cabeceras[i] = new Objetos.csCabeceraDoc();

                                    cabeceras[i].codIC = (string.IsNullOrEmpty(adv_invoice.response.result[i].external_id)) ? "" : adv_invoice.response.result[i].external_id;
                                    // numero de factura
                                    // 7-5-2018 Cambiamos el procedimiento y asignamos como número de factura la referencia de Rita
                                    //cabeceras[i].numDocA3 = (string.IsNullOrEmpty(adv_invoice.response.result[i].external_id)) ? "" : adv_invoice.response.result[i].external_id;
                                    cabeceras[i].numDocA3 = adv_invoice.response.result[i].number;
                                    
                                    
                                    
                                    cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty
                                        (adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].external_id))
                                            ? "" : adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].external_id;

                                    // Tipo de iva
                                    if (adv_invoice.response.result[i].tax.Count == 0)
                                        cabeceras[i].tipoIva = "Exento";
                                    else
                                        cabeceras[i].tipoIva = "Ordinario";

                                    // Referencia
                                    cabeceras[i].referencia = adv_invoice.response.result[i].number;

                                    cabeceras[i].nombreIC = adv_invoice.response.result[i].advertiser[0].company;
                                    cabeceras[i].objeto = "advertiser_invoices";
                                    cabeceras[i].moneda = adv_invoice.response.result[i].advertiser_currency;
                                    cabeceras[i].PSidCliente = null;
                                    cabeceras[i].codIC = adv_invoice.response.result[i].external_id;
                                    cabeceras[i].permalink = adv_invoice.response.result[i].permalink;
                                    //13/03/2020 Añadimos el tipo de documento como invoice manualmente. Si en el futura da problemas cambiar  
                                    cabeceras[i].tipoDoc = "INVOICE";
                                    if (adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].enterprise.Count != 0)
                                        cabeceras[i].empresa = adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].enterprise[0].code;
                                    else
                                        if (adv_invoice.response.result[i].advertiser[0].enterprise.Count > 0)
                                            cabeceras[i].empresa = adv_invoice.response.result[i].advertiser[0].enterprise[0].code;
                                        else
                                            cabeceras[i].empresa = "-";

                                    // FECHAS
                                    cabeceras[i].fecha = DateTime.ParseExact(adv_invoice.response.result[i].date_tag, "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    cabeceras[i].fechaDoc = DateTime.ParseExact(adv_invoice.response.result[i].date_tag, "yyyyMMdd", CultureInfo.InvariantCulture);

                                    if (adv_invoice.response.result[i].advertiser[0].fiscal_identity != null)
                                        cabeceras[i].nif = adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].document_number;
                                    else
                                        cabeceras[i].nif = "";

                                    ////////////////////////////////////////////////////
                                    ////////////////////  LINEAS  //////////////////////
                                    ////////////////////////////////////////////////////

                                    lineas[i] = new Objetos.csLineaDocumento();
                                    lineas[i].codigoArticulo = "705003";
                                    lineas[i].cantidad = "1";
                                    lineas[i].numCabecera = cabeceras[i].permalink;
                                    lineas[i].price = Convert.ToDouble(adv_invoice.response.result[i].advertiser_amount.ToString().Replace(".", ","));


                                    ////////////////////////////////////////////////////
                                    //////////////////  CLIENTES ///////////////////////
                                    ////////////////////////////////////////////////////

                                    // COMPROBAR QUE TENEMOS EL CÓDIGO DE PROVEEDOR EN LA BASE DE DATOS
                                    if (sql.consultaExiste("param1", "__clientes", adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].permalink))
                                    {
                                        proveedor[i] = new Objetos.csTercero();
                                        proveedor[i].codIC = cabeceras[i].id_Customer_Ext;
                                        proveedor[i].permalinkFiscalIdentity = adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].permalink;
                                        if (adv_invoice.response.result[i].advertiser[0].permalink != null)
                                        {
                                            proveedor[i].permalink = adv_invoice.response.result[i].advertiser[0].permalink;
                                        }
                                    }
                                    // SI NO EXISTE LO VAMOS A BUSCAR
                                    else
                                    {
                                        if (adv_invoice.response.result[i].advertiser[0].permalink != null){
                                            //Showadvertiser me muestra la ficha del cliente o proveedor de Rita
                                            proveedor[i] = showAdvertiser(wc, adv_invoice.response.result[i].advertiser[0].permalink, adv_invoice.response.result[i].advertiser[0].fiscal_identity[0].permalink);
                                        }

                                        if (proveedor[i] != null)
                                        {
                                            // CONTROL DE DATOS BANCARIOS
                                            if (proveedor[i].identifier_number != "" && proveedor[i].identifier_number != null && proveedor[i].identifier_number.Length >= 20)
                                            {
                                                domiciliacion[i] = new Objetos.csDomBanca();
                                                domiciliacion[i].iban = proveedor[i].identifier_number.Substring(0, 4);
                                                domiciliacion[i].banco = proveedor[i].identifier_number.Substring(4, 4);
                                                domiciliacion[i].agencia = proveedor[i].identifier_number.Substring(8, 4);
                                                domiciliacion[i].digitoControl = proveedor[i].identifier_number.Substring(12, 2);
                                                domiciliacion[i].numcuenta = proveedor[i].identifier_number.Substring(14);
                                                domiciliacion[i].titular = proveedor[i].nombre.ToUpper();
                                                if (proveedor[i].identifier_number.Length > 7)
                                                    domiciliacion[i].bic = proveedor[i].identifier_number;
                                                else
                                                    domiciliacion[i].bic = null;
                                            }
                                        }
                                    }
                                }

                                // Si la fecha no esta definida, tendrá un valor de 1970
                                // en ese caso, no será valida y pondremos a null 
                                if (cabeceras[i].fechaDoc.Year == 1970)
                                {
                                    proveedor[i] = null;
                                    lineas[i] = null;
                                    cabeceras[i] = null;
                                    domiciliacion[i] = null;
                                }
                            }

                            a3erp.abrirEnlace();
                            a3erp.generarDocA3Objeto(cabeceras, lineas, VENTAS, "NO", proveedor, domiciliacion);
                            a3erp.cerrarEnlace();

                            page++;

                            json = whileJson(wc, "advertiser_invoices/list?", "&page=" + page.ToString());
                        }

                        progressingBar("advertiser invoices", 0, 0, true, false);
                    }
                    else
                    {
                        progressingBar("advertiser invoices", 0, 0, false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        // Affi
        public void invoices()
        {
            int page = 1;
            string page_count = "";

            try
            {
                using (WebClient wc = new WebClient())
                {
                    #region Declaracion de variables
                    wc.Encoding = Encoding.UTF8;
                    frSincronizarDocs sinc = new frSincronizarDocs();
                    Control.CheckForIllegalCrossThreadCalls = false;
                    csa3erp a3erp = new csa3erp();
                    csSqlConnects sql = new csSqlConnects();
                    Objetos.csTercero[] proveedor = null;
                    Objetos.csDomBanca[] domiciliacion = null;
                    int contador = 1;
                    int total_lineas = 0;
                    bool iva = false, irpf = false;
                    double precio = 0;
                    #endregion

                    #region Invoices
                    // OBTENEMOS EL OBJETO LIST DE INVOICES 
                    var json = whileJson(wc, "invoices/list?", "&page=" + contador);
                    var invoice = JsonConvert.DeserializeObject<adMan.invoices.List.RootObject>(json);

                    if (invoice.response.pagination != null && invoice.response.result.Count != 0)
                    {
                        Objetos.csCabeceraDoc[] cabeceras = null;
                        Objetos.csLineaDocumento[] lineas = null;
                        page = invoice.response.pagination.page;
                        page_count = invoice.response.pagination.page_count.ToString();
                        while (page <= Convert.ToInt32(page_count))
                        {
                            progressingBar("invoices", page, Convert.ToInt32(page_count));

                            invoice = JsonConvert.DeserializeObject<adMan.invoices.List.RootObject>(json);

                            // Paginación
                            cabeceras = new Objetos.csCabeceraDoc[invoice.response.result.Count];
                            proveedor = new Objetos.csTercero[invoice.response.result.Count];
                            domiciliacion = new Objetos.csDomBanca[invoice.response.result.Count];

                            // CALCULAMOS EL TOTAL DE LINEAS
                            for (int ii = 0; ii < invoice.response.result.Count; ii++)
                            {
                                total_lineas += invoice.response.result[ii].details.Count;
                            }
                            lineas = new Objetos.csLineaDocumento[total_lineas];

                            // 1 - OBTENEMOS LA CABECERA
                            for (int i = 0; i < invoice.response.result.Count; i++)
                            {
                                if (invoice.response.result[i].details.Count > 0)
                                {
                                    cabeceras[i] = new Objetos.csCabeceraDoc();

                                    // External id de la factura



                                    cabeceras[i].codIC = (string.IsNullOrEmpty(invoice.response.result[i].external_id)) ? "" : invoice.response.result[i].external_id;
                                    // Número de factura
                                    cabeceras[i].numDocA3 = (string.IsNullOrEmpty(invoice.response.result[i].external_id)) ? "" : invoice.response.result[i].external_id;


                                    if (cabeceras[i].codIC.Trim() == "153")
                                    {
 
                                    }



                                    // External id del proveedor
                                    if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty
                                        (invoice.response.result[i].affiliate.fiscal_identity.external_id_es)) ? "" : invoice.response.result[i].affiliate.fiscal_identity.external_id_es;
                                    }
                                    else
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty
                                        (invoice.response.result[i].affiliate.fiscal_identity.external_id_fr)) ? "" : invoice.response.result[i].affiliate.fiscal_identity.external_id_fr;
                                    }

                                    // Referencia
                                    cabeceras[i].referencia = invoice.response.result[i].affiliate_invoice_number; // REVISAR
                                    // Nombre
                                    cabeceras[i].nombreIC = invoice.response.result[i].affiliate.fiscal_identity.business_name;
                                    // Método
                                    cabeceras[i].objeto = "invoices";
                                    // Moneda
                                    cabeceras[i].moneda = invoice.response.result[i].currency;
                                    // Ps id Cliente
                                    cabeceras[i].PSidCliente = null;
                                    // Permalink factura
                                    cabeceras[i].permalink = invoice.response.result[i].permalink;
                                    // Numero de documento A3
                                    //cabeceras[i].numDocA3 = invoice.response.result[i].number;
                                    if (invoice.response.result[i].taxes == null)
                                        cabeceras[i].tipoIva = "Exento";
                                    else
                                        cabeceras[i].tipoIva = "Ordinario";

                                    cabeceras[i].empresa = invoice.response.result[i].enterprise;

                                    //13/03/2020 Añadimos el tipo de documento como invoice manualmente. Si en el futura da problemas cambiar  
                                    cabeceras[i].tipoDoc = "INVOICE";

                                    // FECHAS
                                    cabeceras[i].fecha = DateTime.ParseExact(invoice.response.result[i].accounting_date_tag, "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                    cabeceras[i].fechaDoc = DateTime.ParseExact(invoice.response.result[i].accounting_date_tag, "yyyyMMdd", CultureInfo.InvariantCulture);

                                    if (invoice.response.result[i].affiliate.fiscal_identity != null)
                                        cabeceras[i].nif = invoice.response.result[i].affiliate.fiscal_identity.document_number;
                                    else
                                        cabeceras[i].nif = "";

                                    //////////////////////////////
                                    // NUEVA GESTIÓN DE LAS LINEAS
                                    //////////////////////////////

                                    lineas[i] = new Objetos.csLineaDocumento();
                                    lineas[i].codigoArticulo = "705001";
                                    lineas[i].cantidad = "1";
                                    lineas[i].numCabecera = cabeceras[i].permalink;
                                    precio = 0;

                                    for (int ii = 0; ii < invoice.response.result[i].details.Count; ii++)
                                    {
                                        precio += Convert.ToDouble(invoice.response.result[i].details[ii].amount.ToString().Replace(".", ","));
                                    }

                                    lineas[i].price = precio;

                                    ////////////////////////////////////////////////////
                                    ////////////////// PROVEEDOR ///////////////////////
                                    ////////////////////////////////////////////////////

                                    // COMPROBAR QUE TENEMOS EL CÓDIGO DE PROVEEDOR EN LA BASE DE DATOS
                                    string perma_affi_fiscal = invoice.response.result[i].affiliate.fiscal_identity.permalink;
                                    if (sql.consultaExiste("param1", "__proveed", perma_affi_fiscal))
                                    {
                                        proveedor[i] = new Objetos.csTercero();
                                        proveedor[i].codIC = sql.obtenerCampoTabla("select codpro from __proveed where param1 = '" + perma_affi_fiscal + "'").Trim();
                                        proveedor[i].nomIC = "AFI";

                                        if (invoice.response.result[i].affiliate.permalink != null)
                                        {
                                            proveedor[i].permalink = invoice.response.result[i].affiliate.permalink;
                                            //if (invoice.response.result[i].affiliate.fiscal_identity != null)
                                            proveedor[i].permalinkFiscalIdentity = invoice.response.result[i].affiliate.fiscal_identity.permalink;
                                        }
                                    }
                                    // SI NO EXISTE LO VAMOS A BUSCAR
                                    else
                                    {
                                        // 3 - OBTENEMOS LOS DATOS DEL PROVEEDOR  ATRAVÉS DEL PERMALINK
                                        // DE LA FACTURA. TAMBIÉN RECUPERAMOS SU DOMICILIACION. PERO SOLO SI NO EXISTE
                                        // ESE PERMALINK EN LA BASE DE DATOS

                                        proveedor[i] = showAffiliate(wc, invoice.response.result[i].affiliate.permalink, perma_affi_fiscal);

                                        if (proveedor[i] != null)
                                        {
                                            if (proveedor[i].account_number != "" && proveedor[i].account_number != null && proveedor[i].account_number.Length >= 20)
                                            {
                                                /////////////////////////////////////////////////////
                                                ////////////////// DOMICILIACIÓN ////////////////////
                                                ////////////////////////////////////////////////////

                                                domiciliacion[i] = new Objetos.csDomBanca();
                                                domiciliacion[i].cuentaext = proveedor[i].account_number.Substring(4);
                                                domiciliacion[i].iban = proveedor[i].account_number.Substring(0, 4);
                                                domiciliacion[i].banco = proveedor[i].account_number.Substring(4, 4);
                                                domiciliacion[i].agencia = proveedor[i].account_number.Substring(8, 4);
                                                domiciliacion[i].digitoControl = proveedor[i].account_number.Substring(12, 2);
                                                domiciliacion[i].numcuenta = proveedor[i].account_number.Substring(14);
                                                domiciliacion[i].titular = proveedor[i].nombre.ToUpper();
                                                if (proveedor[i].identifier_number.Length > 7)
                                                    domiciliacion[i].bic = proveedor[i].identifier_number;
                                                else
                                                    domiciliacion[i].bic = null;
                                            }
                                        }
                                    }

                                    bool bloqueado = false, moneda = true, boolPeriodo = false;

                                    for (int index = 0; index < invoice.response.result[i].taxes.Count; index++)
                                    {
                                        if (invoice.response.result[i].taxes[index].name == "IVA21")
                                        {
                                            iva = true;
                                            proveedor[i].porcentajeIRPF = invoice.response.result[i].taxes[index].value;
                                        }
                                        else
                                        {
                                            irpf = true;
                                            proveedor[i].tipoIRPF = "111";
                                            proveedor[i].porcentajeIRPF = invoice.response.result[i].taxes[index].value.Replace("-", "");
                                        }

                                        if (iva && irpf)
                                        {
                                            proveedor[i].claveIRPF = "2";
                                            proveedor[i].subclaveIRPF = "4";
                                        }
                                        else if (irpf)
                                        {
                                            proveedor[i].claveIRPF = "1";
                                            proveedor[i].subclaveIRPF = "2";
                                        }

                                        bloqueado = sql.consultaExiste("SELECT * FROM __PROVEED WHERE BLOQUEADO = 'T' AND PARAM1 = '" + proveedor[i].permalink + "'");

                                        if (bloqueado)
                                        {
                                            proveedoresBloqueados.Add(proveedor[i].email, proveedor[i].permalink);
                                        }

                                        moneda = sql.consultaExiste("SELECT CODISO3166 FROM MONEDAS WHERE CODISO3166 ='" + cabeceras[i].moneda + "'");
                                        if (!moneda)
                                        {
                                            facturasConMonedaIncorrecta.Add(cabeceras[i].permalink, cabeceras[i].moneda); // permalink de la factura y moneda en cuestion
                                        }
                                        else
                                        {
                                            proveedor[i].moneda = cabeceras[i].moneda;
                                        }

                                        boolPeriodo = false;
                                        if (cabeceras[i].fechaDoc <= Convert.ToDateTime(periodo) || cabeceras[i].fechaDoc <= Convert.ToDateTime(fechacierre_facc))
                                        {
                                            boolPeriodo = true;
                                            if (cabeceras[i].numDocA3 == "")
                                                if (!periodoContableCerrado.ContainsKey(cabeceras[i].permalink) && !periodoContableCerrado.ContainsValue(cabeceras[i].fechaDoc.ToString()))
                                                    periodoContableCerrado.Add(cabeceras[i].permalink, cabeceras[i].fechaDoc.ToString());
                                        }
                                    }

                                    bool any = false;
                                    if (cabeceras[i].fechaDoc.Year == 1970)
                                    {
                                        any = true;
                                    }

                                    if (bloqueado || !moneda || boolPeriodo || any)
                                    {
                                        proveedor[i] = null;
                                        lineas[i] = null;
                                        cabeceras[i] = null;
                                        domiciliacion[i] = null;
                                    }
                                }
                            }
                    #endregion

                            a3erp.abrirEnlace();
                            a3erp.generarDocA3Objeto(cabeceras, lineas, true, "NO", proveedor, domiciliacion);
                            a3erp.cerrarEnlace();

                            page++;

                            json = whileJson(wc, "invoices/list?", "&page=" + page);
                        }

                        progressingBar("invoices", 0, 0, true, false, facturasConMonedaIncorrecta, proveedoresBloqueados, periodoContableCerrado);
                    }
                    else
                    {
                        progressingBar("invoices", 0, 0, false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        // Sup y Affi
        public void third_invoices()
        {
            try
            {
                int page_count = 0;

                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    frSincronizarDocs sinc = new frSincronizarDocs();
                    Control.CheckForIllegalCrossThreadCalls = false;
                    csa3erp a3erp = new csa3erp();
                    csSqlConnects sql = new csSqlConnects();
                    Objetos.csTercero[] proveedor = null;
                    Objetos.csDomBanca[] domiciliacion = null;
                    int contador = 1, total_lineas = 0;
                    bool iva = false, irpf = false, sup = false;
                    double precio = 0;

                    // OBTENEMOS EL OBJETO LIST DE INVOICES 
                    var json = whileJson(wc, "third_invoices/list?", "&page=" + contador);
                    var third_invoice = JsonConvert.DeserializeObject<adMan.third_invoices.List.RootObject>(json).response;

                    if (third_invoice.pagination != null && third_invoice.result.Count != 0)
                    {
                        var valid = JsonConvert.DeserializeObject<adMan.third_invoices.List.RootObject>(json).valid;
                        int page = third_invoice.pagination.page;
                        page_count = third_invoice.pagination.page_count;

                        Objetos.csCabeceraDoc[] cabeceras = null;
                        Objetos.csLineaDocumento[] lineas = null;

                        while (page <= page_count)
                        {
                            // Progressbar
                            progressingBar("third_invoices", page, Convert.ToInt32(page_count));

                            third_invoice = JsonConvert.DeserializeObject<adMan.third_invoices.List.RootObject>(json).response;

                            cabeceras = new Objetos.csCabeceraDoc[third_invoice.result.Count];
                            proveedor = new Objetos.csTercero[third_invoice.result.Count];
                            domiciliacion = new Objetos.csDomBanca[third_invoice.result.Count];

                            // CALCULAMOS EL TOTAL DE LINEAS
                            for (int ii = 0; ii < third_invoice.result.Count; ii++)
                            {
                                total_lineas += third_invoice.result[ii].third_invoice_item.Count;
                            }
                            lineas = new Objetos.csLineaDocumento[total_lineas];

                            // 1 - OBTENEMOS LA CABECERA
                            for (int i = 0; i < third_invoice.result.Count && third_invoice.result[i].third_invoice_item.Count > 0; i++)
                            {
                                sup = false;
                                if (third_invoice.result[i].supplier.Count > 0)
                                    sup = true;

                                cabeceras[i] = new Objetos.csCabeceraDoc();

                                //cabeceras[i].codIC = (string.IsNullOrEmpty(third_invoice.result[i].external_id)) ? "" : third_invoice.result[i].external_id.Trim();
                                cabeceras[i].numDocA3 = (string.IsNullOrEmpty(third_invoice.result[i].external_id)) ? "" : third_invoice.result[i].external_id;

                                if (sup == true) // Supplier
                                {
                                    // NIF de la factura
                                    cabeceras[i].nif = third_invoice.result[i].supplier[0].fiscal_identity[0].document_number;
                                    // Nombre
                                    cabeceras[i].nombreIC = third_invoice.result[i].supplier[0].fiscal_identity[0].business_name.ToUpper();
                                    if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty(third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_es))
                                        ? "" : third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_es;
                                    }
                                    else
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty(third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_fr))
                                        ? "" : third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_fr;
                                    }

                                }
                                else // Affiliate
                                {
                                    // NIF de la factura
                                    cabeceras[i].nif = third_invoice.result[i].affiliate[0].fiscal_identity[0].document_number;
                                    // Nombre
                                    cabeceras[i].nombreIC = third_invoice.result[i].supplier_name.ToUpper();

                                    if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty(third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_es))
                                       ? "" : third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_es;
                                    }
                                    else
                                    {
                                        cabeceras[i].id_Customer_Ext = (string.IsNullOrEmpty(third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_fr))
                                       ? "" : third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_fr;
                                    }
                                }

                                // Referencia
                                cabeceras[i].referencia = third_invoice.result[i].number;

                                // Tipo de objeto
                                cabeceras[i].objeto = "third_invoices";
                                // Moneda
                                cabeceras[i].moneda = third_invoice.result[i].currency[0].code;
                                // PS id Cliente
                                cabeceras[i].PSidCliente = null;
                                // Permalink
                                cabeceras[i].permalink = third_invoice.result[i].permalink;

                                // Fecha
                                cabeceras[i].fecha = DateTime.ParseExact(third_invoice.result[i].accounting_date_tag, "yyyyMMdd", CultureInfo.InvariantCulture).ToString();
                                // Fecha Documento
                                cabeceras[i].fechaDoc = DateTime.ParseExact(third_invoice.result[i].accounting_date_tag, "yyyyMMdd", CultureInfo.InvariantCulture);
                                //13/03/2020 Añadimos el tipo de documento como invoice manualmente. Si en el futura da problemas cambiar  
                                cabeceras[i].tipoDoc = "INVOICE";

                                if (third_invoice.result[i].tax.Count == 0)
                                {
                                    cabeceras[i].tipoIva = "Exento";
                                }
                                else
                                {
                                    cabeceras[i].tipoIva = "Ordinario";
                                }

                                if (third_invoice.result[i].enterprise.Count != 0)
                                    cabeceras[i].empresa = third_invoice.result[i].enterprise[0].name;
                                else
                                    cabeceras[i].empresa = null;
                                if (third_invoice.result[i].enterprise.Capacity == 1)
                                    cabeceras[i].empresa = third_invoice.result[i].enterprise[0].name;
                                else
                                    cabeceras[i].empresa = "";
                                cabeceras[i].moneda = third_invoice.result[i].currency[0].code;

                                /////////////////////////////////
                                // NUEVA GESTIÓN DE LAS LINEAS //
                                /////////////////////////////////

                                lineas[i] = new Objetos.csLineaDocumento();
                                lineas[i].codigoArticulo = "705002";
                                lineas[i].cantidad = "1";
                                lineas[i].numCabecera = cabeceras[i].permalink;
                                precio = 0;

                                for (int ii = 0; ii < third_invoice.result[i].third_invoice_item.Count; ii++)
                                {
                                    precio += Convert.ToDouble(third_invoice.result[i].third_invoice_item[ii].amount.Replace(".", ","));
                                }

                                lineas[i].price = precio;

                                ////////////////////////////////////////////////////
                                ////////////////////////////////////////////////////
                                ////////////////// PROVEEDOR ///////////////////////
                                ////////////////////////////////////////////////////

                                if (sup == true) // Supplier
                                {
                                    // COMPROBAR QUE TENEMOS EL CÓDIGO DE PROVEEDOR EN LA BASE DE DATOS
                                    string external_sup = (csGlobal.databaseA3 == "ADMANMEDIA_ES") ? third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_es : third_invoice.result[i].supplier[0].fiscal_identity[0].external_id_fr;
                                    string perma_sup_fiscal = third_invoice.result[i].supplier[0].fiscal_identity[0].permalink;

                                    if (sql.consultaExiste("SELECT PARAM1 FROM __PROVEED WHERE PARAM1 = '" + perma_sup_fiscal + "'"))
                                    {
                                        proveedor[i] = new Objetos.csTercero();
                                        proveedor[i].codIC = sql.obtenerCampoTabla("SELECT CODPRO FROM __PROVEED WHERE PARAM1 = '" + perma_sup_fiscal + "'").Trim();
                                        proveedor[i].nomIC = "SUP";
                                        proveedor[i].permalink = third_invoice.result[i].supplier[0].permalink;
                                        //proveedor[i].permalink = third_invoice.result[i].supplier;
                                        proveedor[i].permalinkFiscalIdentity = perma_sup_fiscal;

                                        if (proveedor[i].codIC == null)
                                        {
                                            proveedor[i] = showSupplier(wc, third_invoice.result[i].supplier[0].permalink, perma_sup_fiscal);
                                        }
                                    }
                                    // SI NO EXISTE LO VAMOS A BUSCAR
                                    else
                                    {
                                        if (perma_sup_fiscal != null)
                                            proveedor[i] = showSupplier(wc, third_invoice.result[i].supplier[0].permalink, perma_sup_fiscal);
                                    }

                                    if (proveedor[i] != null)
                                    {
                                        if (proveedor[i].account_number != "" && proveedor[i].account_number != null && proveedor[i].account_number.Length >= 20)
                                        {
                                            domiciliacion[i] = new Objetos.csDomBanca();
                                            domiciliacion[i].cuentaext = proveedor[i].account_number;
                                            domiciliacion[i].iban = proveedor[i].account_number.Substring(0, 4);
                                            domiciliacion[i].banco = proveedor[i].account_number.Substring(4, 4);
                                            domiciliacion[i].agencia = proveedor[i].account_number.Substring(8, 4);
                                            domiciliacion[i].digitoControl = proveedor[i].account_number.Substring(12, 2);
                                            domiciliacion[i].numcuenta = proveedor[i].account_number.Substring(14);
                                            domiciliacion[i].titular = proveedor[i].nombre.ToUpper();
                                            if (proveedor[i].identifier_number.Length > 7)
                                                domiciliacion[i].bic = proveedor[i].identifier_number.Replace(" ", "");
                                            else
                                                domiciliacion[i].bic = null;
                                        }
                                    }
                                }
                                else // Affiliate
                                {
                                    string external_affi = (csGlobal.databaseA3 == "ADMANMEDIA_ES") ? third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_es : third_invoice.result[i].affiliate[0].fiscal_identity[0].external_id_fr;
                                    string perma_affi_fiscal = third_invoice.result[i].affiliate[0].fiscal_identity[0].permalink;

                                    if (sql.consultaExiste("SELECT PARAM1 FROM __PROVEED WHERE PARAM1 = '" + perma_affi_fiscal + "'"))
                                    {
                                        proveedor[i] = new Objetos.csTercero();
                                        proveedor[i].codIC = sql.obtenerCampoTabla("SELECT CODPRO FROM __PROVEED WHERE PARAM1 = '" + perma_affi_fiscal + "'").Trim();
                                        proveedor[i].nomIC = "AFI";
                                        proveedor[i].permalink = third_invoice.result[i].affiliate[0].permalink;
                                        //proveedor[i].permalink = perma_affi_fiscal;
                                        proveedor[i].permalinkFiscalIdentity = perma_affi_fiscal;

                                        if (proveedor[i].codIC == null)
                                        {
                                            proveedor[i] = showAffiliate(wc, third_invoice.result[i].affiliate[0].permalink, perma_affi_fiscal);
                                        }
                                    }
                                    // SI NO EXISTE LO VAMOS A BUSCAR
                                    else
                                    {
                                        if (perma_affi_fiscal != null)
                                            proveedor[i] = showAffiliate(wc, third_invoice.result[i].affiliate[0].permalink, perma_affi_fiscal);
                                    }

                                    if (proveedor[i] != null)
                                    {
                                        if (proveedor[i].account_number != "" && proveedor[i].account_number != null && proveedor[i].account_number.Length >= 20)
                                        {
                                            /////////////////////////////////////////////////////
                                            ////////////////// DOMICILIACIÓN ////////////////////
                                            ////////////////////////////////////////////////////

                                            domiciliacion[i] = new Objetos.csDomBanca();
                                            domiciliacion[i].cuentaext = proveedor[i].account_number.Substring(4);
                                            domiciliacion[i].iban = proveedor[i].account_number.Substring(0, 4);
                                            domiciliacion[i].banco = proveedor[i].account_number.Substring(4, 4);
                                            domiciliacion[i].agencia = proveedor[i].account_number.Substring(8, 4);
                                            domiciliacion[i].digitoControl = proveedor[i].account_number.Substring(12, 2);
                                            domiciliacion[i].numcuenta = proveedor[i].account_number.Substring(14);
                                            domiciliacion[i].titular = proveedor[i].nombre.ToUpper();
                                            if (proveedor[i].identifier_number.Length > 7)
                                                domiciliacion[i].bic = proveedor[i].identifier_number;
                                            else
                                                domiciliacion[i].bic = null;
                                        }
                                    }
                                }


                                if (third_invoice.result[i].tax.Count != 0)
                                {
                                    for (int index = 0; index < third_invoice.result[i].tax.Count; index++)
                                    {
                                        if (third_invoice.result[i].tax[index].name.Contains("IRPF"))
                                        {
                                            irpf = true;
                                            proveedor[i].tipoIRPF = "111";
                                            proveedor[i].porcentajeIRPF = third_invoice.result[i].tax[index].value.Replace("-", "");
                                        }
                                        else
                                        {
                                            iva = true;
                                        }
                                    }

                                    if (iva && irpf)
                                    {
                                        proveedor[i].claveIRPF = "2";
                                        proveedor[i].subclaveIRPF = "4";
                                    }
                                    else if (irpf)
                                    {
                                        proveedor[i].claveIRPF = "1";
                                        proveedor[i].subclaveIRPF = "2";
                                    }
                                }


                                bool bloqueado = sql.consultaExiste("SELECT * FROM __PROVEED WHERE BLOQUEADO = 'T' AND PARAM1 = '" + proveedor[i].permalink + "'");

                                if (bloqueado)
                                {
                                    proveedoresBloqueados.Add(cabeceras[i].permalink, proveedor[i].permalink);
                                }

                                bool moneda = sql.consultaExiste("SELECT CODISO3166 FROM MONEDAS WHERE CODISO3166 ='" + cabeceras[i].moneda + "'");

                                if (!moneda)
                                {
                                    facturasConMonedaIncorrecta.Add(cabeceras[i].permalink, cabeceras[i].moneda); // permalink de la factura y moneda en cuestion
                                }
                                else
                                {
                                    proveedor[i].moneda = cabeceras[i].moneda;
                                }

                                bool boolPeriodo = false;

                                if (cabeceras[i].fechaDoc <= Convert.ToDateTime(periodo) || cabeceras[i].fechaDoc <= Convert.ToDateTime(fechacierre_facc))
                                {
                                    boolPeriodo = true;
                                    if (cabeceras[i].numDocA3 == "")
                                        if (!periodoContableCerrado.ContainsKey(cabeceras[i].permalink) && !periodoContableCerrado.ContainsValue(cabeceras[i].fechaDoc.ToString()))
                                            periodoContableCerrado.Add(cabeceras[i].permalink, cabeceras[i].fechaDoc.ToString());
                                }

                                bool any = false;
                                if (cabeceras[i].fechaDoc.Year == 1970)
                                {
                                    any = true;
                                }


                                if (bloqueado || !moneda || boolPeriodo || any)
                                {
                                    proveedor[i] = null;
                                    lineas[i] = null;
                                    cabeceras[i] = null;
                                    domiciliacion[i] = null;
                                }
                            }




                            //MessageBox.Show("Se van a traspasar " + cabeceras.Length.ToString() + " documentos, ¿está seguro?");

                            if (MessageBox.Show("Se van a traspasar " + cabeceras.Length.ToString() + " documentos, ¿está seguro?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            {
                             
                                a3erp.abrirEnlace();
                                a3erp.generarDocA3Objeto(cabeceras, lineas, COMPRAS, "NO", proveedor, domiciliacion);
                                a3erp.cerrarEnlace();
                            }
                            page++;

                            json = whileJson(wc, "third_invoices/list?", "&page=" + page.ToString());
                        }

                        progressingBar("third invoices", 0, 0, true, false, facturasConMonedaIncorrecta, proveedoresBloqueados, periodoContableCerrado);
                    }
                    else
                    {
                        progressingBar("third invoices", 0, 0, false, true);
                    }
                }
            }
            catch (JsonException ex) { Program.guardarErrorFichero(ex.Message.ToString()); }
            catch (Exception ex) {  }
        }

        #endregion

        #region Show - Affiliates, suppliers y advertisers

        private Objetos.csTercero showAffiliate(WebClient wc, string permalink_affiliate, string permalink_fiscal_identity)
        {
            Objetos.csTercero proveedor = new Objetos.csTercero();
            Objetos.csDomBanca domiciliacion = new Objetos.csDomBanca();

            try
            {
                var json = whileJson(wc, "affiliates/show?permalink=" + permalink_affiliate + "&", "");
                var user = JsonConvert.DeserializeObject<adMan.affiliates.Show.RootObject>(json).data;

                proveedor.descFormaPago = user.payment_method;
                if (user.paypal_email != null || user.paypal_email == "")
                {
                    //proveedor.descFormaPago = "paypal";
                    proveedor.paypal = user.paypal_email;
                }

                switch (proveedor.descFormaPago)
                {
                    case "paypal":
                        proveedor.paypal = user.paypal_email;
                        break;
                    case "check":
                        break;
                    case "direct_deposit":
                        break;
                    default:
                        break;
                }

                proveedor.permalink = permalink_affiliate;
                if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                    proveedor.codIC = user.fiscal_identity.external_id_es.Trim();
                else
                    proveedor.codIC = user.fiscal_identity.external_id_fr.Trim();
                proveedor.permalinkFiscalIdentity = permalink_fiscal_identity;
                proveedor.razonSocial = user.company;
                //proveedor.razonSocial = user.company.ToUpper();
                //proveedor.direccion = user.address1.ToUpper();
                proveedor.direccion = user.fiscal_identity.address1.ToUpper();
                proveedor.email = user.paypal_email;
                //proveedor.email = user.affiliate_users[0].email;
                proveedor.codigoPostal = user.fiscal_identity.zipcode;
                //proveedor.codigoPostal = user.zipcode;
                proveedor.poblacion = user.fiscal_identity.city.ToUpper();
                //proveedor.poblacion = user.city.ToUpper();
                proveedor.telf = user.phone.Replace(".", "").Replace("-","").Replace(" ", "");
                //proveedor.telf = user.affiliate_users[0].phone;
                //proveedor.movil = user.affiliate_users[0].cell_phone;
                proveedor.nombre = user.fiscal_identity.business_name;
                //proveedor.nombre = user.affiliate_users[0].first_name.ToUpper();
                proveedor.apellidos = "";
                /////

                // Testing
                proveedor.codPais = user.fiscal_identity.country.iso_code;
                //proveedor.codPais = user.affiliate_users[0].country.iso_code;
                proveedor.regimenIva = getRegimenIva(proveedor.codPais);
                //////////


                proveedor.account_number = user.direct_deposit_iban.Replace(" ", "");
                proveedor.identifier_number = user.direct_deposit_swift.Replace(" ", "");
                //proveedor.cuentaContable = user.direct_deposit_account_number.Replace("-","").Replace(" ", "");

                proveedor.nomIC = "AFI";

                // NIFCIF
                proveedor.nifcif = user.fiscal_identity.document_number;
            }
            catch (Exception ex)
            {

            }

            return proveedor;
        }

        private Objetos.csTercero showSupplier(WebClient wc, string permalink_supplier, string permalink_fiscal_identity)
        {
            Objetos.csTercero proveedor = new Objetos.csTercero();
            Objetos.csDomBanca domiciliacion = new Objetos.csDomBanca();

            try
            {
                var json = whileJson(wc, "suppliers/show?permalink=" + permalink_supplier + "&", "");
                var user = JsonConvert.DeserializeObject<adMan.suppliers.Show.RootObject>(json).data;

                proveedor.descFormaPago = user.payment_method;

                switch (proveedor.descFormaPago)
                {
                    case "0": // paypal
                        proveedor.paypal = user.paypal_email;
                        break;
                    case "1": // deposit
                        break;
                }

                proveedor.permalink = permalink_supplier;
                proveedor.permalinkFiscalIdentity = permalink_fiscal_identity;
                if (user.fiscal_identity.enterprise == null)
                    proveedor.razonSocial = user.fiscal_identity.business_name;
                else
                    proveedor.razonSocial = user.fiscal_identity.enterprise.name.ToUpper();
                proveedor.direccion = user.address1;
                proveedor.email = user.paypal_email;
                proveedor.codigoPostal = user.zipcode;
                proveedor.poblacion = user.city.ToUpper();
                proveedor.telf = user.phone;
                proveedor.movil = user.phone;
                proveedor.nombre = user.fiscal_identity.business_name.ToUpper();
                proveedor.apellidos = "";

                // Testing
                if (user.fiscal_identity.country != null)
                {
                    proveedor.codPais = user.fiscal_identity.country.iso_code;
                    proveedor.regimenIva = getRegimenIva(proveedor.codPais);
                }
                //////////


                if (csGlobal.databaseA3 == "ADMANMEDIA_ES")
                    proveedor.codIC = user.fiscal_identity.external_id_es.Trim();
                else
                    proveedor.codIC = user.fiscal_identity.external_id_fr.Trim();

                proveedor.account_number = user.account_number.Replace(" ", ""); ;
                proveedor.identifier_number = user.identifier_number.Replace(" ", ""); ;
                proveedor.nomIC = "SUP";

                // NIFCIF
                proveedor.nifcif = user.fiscal_identity.document_number.Trim();
            }
            catch (Exception ex)
            {

            }

            return proveedor;
        }

        private Objetos.csTercero showAdvertiser(WebClient wc, string permalink_advertiser, string permalink_fiscal_identity)
        {
            Objetos.csTercero cliente = new Objetos.csTercero();
            Objetos.csDomBanca domiciliacion = new Objetos.csDomBanca();

            try
            {
                var json = whileJson(wc, "advertisers/show?permalink=" + permalink_advertiser + "&", "");
                var adv = JsonConvert.DeserializeObject<adMan.advertisers.Show.RootObject>(json).data;

                cliente.descFormaPago = "";
                cliente.permalink = permalink_advertiser;
                cliente.permalinkFiscalIdentity = permalink_fiscal_identity;
                if (adv.billing_country.adman_company_detail != null)
                    cliente.razonSocial = adv.billing_country.adman_company_detail.registry_data;
                else
                    cliente.razonSocial = "";
                cliente.direccion = adv.address1;
                cliente.email = adv.user_email;
                cliente.codigoPostal = adv.zipcode;
                cliente.poblacion = adv.city.ToUpper();
                cliente.telf = adv.user_phone;
                cliente.movil = adv.user_cell_phone;
                cliente.codPais = adv.country.iso_code;

                cliente.nombre = adv.fiscal_identity.business_name.ToUpper();
                cliente.apellidos = "";
                

                cliente.nifcif = adv.cif;
            }
            catch (Exception ex)
            {

            }

            return cliente;
        }

        #endregion

        #region Varios

        private string getRegimenIva(string iso_code = "ES")
        {
            bool ccee = false;
            string codpais = "";

            if (iso_code == "FR" || iso_code == "ES")
            {
                return "CNAC";
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    codpais = dr["CODPAIS"].ToString().Trim();
                    if (codpais == iso_code)
                    {
                        ccee = true;
                        break;
                    }
                }
            }

            if (ccee)
                return "CCEE";
            else
                return "SUJPAS";
        }

        /// <summary>
        /// Método que controla el progreso de cada uno
        /// </summary>
        /// <param name="objeto"></param>
        /// <param name="page"></param>
        /// <param name="page_count"></param>
        /// <param name="finished"></param>
        /// <param name="empty"></param>
        /// <param name="diccionarioMoneda"></param>
        /// <param name="diccionarioProveedores"></param>
        /// <param name="diccionarioPeriodoContable"></param>
        private void progressingBar(string objeto, int page, int page_count, bool finished = false, bool empty = false, Dictionary<string, string> diccionarioMoneda = null, Dictionary<string, string> diccionarioProveedores = null, Dictionary<string, string> diccionarioPeriodoContable = null)
        {
            string stringDicMoneda = "\nLa facturas siguientes no se han contabilizado por tener una moneda que no existen en la base de datos:\n\n";
            string stringDicProveedores = "\nLas facturas siguientes contienen proveedores que están bloqueados:\n\n";
            string stringDicPeriodo = "\nLas facturas siguientes no se han introducido porque pertenecen a un periodo contable cerrado:\n\n";
            string codproveedor = "";

            if (empty)
            {
                string s = new CultureInfo("en-US").TextInfo.ToTitleCase(objeto);
                toolStripStatusLabelProgreso.Text = s + " no tiene facturas disponibles en el año " + any.Value.ToString();
                toolStripProgressBarRita.Value = 0;
            }
            else
            {
                if (finished)
                {
                    toolStripStatusLabelProgreso.Text = objeto + " ha finalizado con éxito";
                    toolStripProgressBarRita.Value = 0;
                }
                else
                {
                    toolStripStatusLabelProgreso.Text = "Progreso: página " + page.ToString() + "/" + page_count.ToString() + " de " + objeto;
                    toolStripProgressBarRita.Maximum = page_count;
                    toolStripProgressBarRita.Value = page;
                }
            }

            if (diccionarioMoneda != null || diccionarioProveedores != null || diccionarioPeriodoContable != null)
            {
                csSqlConnects sql = new csSqlConnects();
                bool checkMoneda = false, checkProv = false, checkPeriodo = false;
                if (diccionarioMoneda.Count > 0 || diccionarioProveedores.Count > 0 || diccionarioPeriodoContable.Count > 0)
                {
                    foreach (KeyValuePair<string, string> item in diccionarioMoneda)
                    {
                        checkMoneda = true;
                        stringDicMoneda += "Factura: " + item.Key.ToString() + " - Moneda: " + item.Value.ToString() + "\n";
                    }

                    foreach (KeyValuePair<string, string> item in diccionarioProveedores)
                    {
                        checkProv = true;
                        codproveedor = sql.obtenerCampoTabla("SELECT LTRIM(CODPRO) FROM __PROVEED WHERE PARAM1 = '" + item.Value + "'").Trim();
                        stringDicProveedores += "Factura: " + item.Value.ToString() + " - Código del proveedor: " + codproveedor + " - Permalink del proveedor: " + item.Value.ToString() + "\n";
                    }

                    foreach (KeyValuePair<string, string> item in diccionarioPeriodoContable)
                    {
                        checkPeriodo = true;
                        stringDicPeriodo += "Código de la factura: " + item.Key.ToString() + " - Fecha: " + Convert.ToDateTime(item.Value.ToString()).ToShortDateString().ToString() + "\n";
                    }

                    MessageBox.Show(((checkMoneda) ? stringDicMoneda + "\n\n" : "") + ((checkProv) ? stringDicProveedores : "") + ((checkPeriodo) ? stringDicPeriodo : ""));
                }
            }
        }

        //Para formatear la url del webservice
        public string getUrl(string type, string page = "")
        {
            #region Seteamos los filtros
            string filtro_year = "&year=" + any.Value.ToString();
            string filtro_pais_seleccionado = "";
            string filtro_external_id_seleccionado = "";

            if (descargarDeNuevo == false)
                filtro_external_id_seleccionado = FILTRO_EXTERNAL_ID;
            if (csGlobal.databaseA3 == "ADMANMEDIA_ES" || csGlobal.databaseA3 == "ADMAN_ES")
                filtro_pais_seleccionado = FILTRO_SPAIN;
            else
                filtro_pais_seleccionado = FILTRO_FRANCE;
            #endregion

            string url = csGlobal.APIUrl
                + type
                + FILTRO_TOKEN_IP
                + page
                + FILTRO_PER_PAGE
                + filtro_pais_seleccionado
                + FILTRO_FISCAL_IDENTITY
                + filtro_external_id_seleccionado
                + filtro_year
                + FILTRO_STATUS;

            return url;
        }

        private string whileJson(WebClient wc, string url, string page)
        {
            /*
             * No parará hasta que conecte
             * 
             */
            string rt = "";
            bool ok = false;

            while (!ok)
            {
                try
                {
                    rt = wc.DownloadString(getUrl(url, page));
                    if (rt != null)
                    {
                        ok = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return rt;
        }

        public string updateIdAdman(string objeto, string permalink, string external_id)
        {
            string country = "";
            string status = "";
            if (csGlobal.databaseA3 == "ADMANMEDIA_ES" && objeto == "fiscal_identities")
                country = "&country=es";
            else if (csGlobal.databaseA3 == "ADMANMEDIA_FR" && objeto == "fiscal_identities")
                country = "&country=fr";
            if (objeto != "fiscal_identities")
                status = FILTRO_STATUS;

            String test = String.Empty;
            // http://api.admanmedia.com/fiscal_identities/set_external_id?&permalink=<el_que_querais>&token=&external_id=3702&country=es
            //System.Net.WebRequest urlUpdate = WebRequest.Create(csGlobal.APIUrl + objeto + "/set_external_id?permalink=" + permalink + "&external_id=" + external_id.Trim() + "&token=1433243290905532672&server=http://192.168.187.243/" + country);
            System.Net.WebRequest urlUpdate =
                WebRequest.Create(csGlobal.APIUrl
                + objeto
                + "/set_external_id?permalink="
                + permalink
                + "&external_id="
                + external_id.Trim()
                + FILTRO_TOKEN_IP
                + country
                + status);

            try
            {
                var request = urlUpdate;
                SetBasicAuthHeader(request, csGlobal.webServiceKey);

                request.Method = "GET";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    test = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                }

                //lbInfoLog.Items.Add("Añadido el item con permalink: " + permalink + " e id: " + external_id);
                return test;
            }
            catch (WebException ex)
            {
                Program.guardarErrorFichero(ex.ToString());
                return ex.ToString();
            }
        }

        public void SetBasicAuthHeader(WebRequest request, String userName)
        {
            string authInfo = userName + ":";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;
        }

        #endregion

        #region Botones
        private void _invoices()
        {
            Thread t = new Thread(invoices);
            t.Start();
        }

        private void _third_invoices()
        {
            Thread t = new Thread(third_invoices);
            t.Start();
        }

        private void _adv_invoices()
        {
            Thread t = new Thread(advertisers_invoices);
            t.Start();
        }

        private void btnInvoices_Click(object sender, EventArgs e)
        {
            btnInvoices.Enabled = false;
            this.Refresh();
            descargarDeNuevo = true;
            _invoices();
            descargarDeNuevo = false;
            btnInvoices.Enabled = true;
        }

        private void btnThirdInvoices_Click(object sender, EventArgs e)
        {
            btnThirdInvoices.Enabled = false;
            this.Refresh();
            descargarDeNuevo = true;
            _third_invoices();
            descargarDeNuevo = false;
            btnThirdInvoices.Enabled = true;
        }

        private void btnAdvInvoices_Click(object sender, EventArgs e)
        {
            btnAdvInvoices.Enabled=false;
            this.Refresh();
            descargarDeNuevo = true;
            _adv_invoices();
            descargarDeNuevo = false;
            btnAdvInvoices.Enabled=true;;
        }

        private void btnInvoices2_Click(object sender, EventArgs e)
        {
            descargarDeNuevo = false;
            _invoices();
            descargarDeNuevo = true;
        }

        private void btnThirdInvoices2_Click(object sender, EventArgs e)
        {
            descargarDeNuevo = false;
            _third_invoices();
            descargarDeNuevo = true;
        }

        private void btnAdvInvoices2_Click(object sender, EventArgs e)
        {
            descargarDeNuevo = false;
            _adv_invoices();
            descargarDeNuevo = true;
        }

        private void btnSetExtThridInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                frDialogInput input = new frDialogInput("Permalink de la factura a borrar: ");
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    updateIdAdman("third_invoices", csGlobal.comboFormValue, string.Empty);

                    MessageBox.Show("External id de la factura puesto a null");
                }
            }
            catch (Exception) { MessageBox.Show("Error 500"); }
        }

        private void btnSetExtNULLInvoices_Click(object sender, EventArgs e)
        {
            try
            {
                frDialogInput input = new frDialogInput("Permalink de la factura a borrar: ");
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    updateIdAdman("invoices", csGlobal.comboFormValue, string.Empty);

                    MessageBox.Show("External id de la factura puesto a null");
                }
            }
            catch (Exception) { MessageBox.Show("Error 500"); }
        }

        private void btnSetExtNULLAdvInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                frDialogInput input = new frDialogInput("Permalink de la factura a borrar: ");
                if (input.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    updateIdAdman("advertiser_invoices", csGlobal.comboFormValue, string.Empty);

                    MessageBox.Show("External id de la factura puesto a null");
                }
            }
            catch (Exception) { MessageBox.Show("Error 500"); }
        }
        #endregion

        private void frRita_Load(object sender, EventArgs e)
        {
            any.Value = DateTime.Now.Year; 
        }
    }
}
