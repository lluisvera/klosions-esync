﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace klsync
{
    class csBastide
    {
        public void generarFicheroTXTAlmacenLogistica()
        {

            //columnas del fichero
            string TIPREG = "AR  ";           //Tipo de Registro (4)
            string ACCION = "AG";             //Tipo de Acción: AG= Agregar, MO= Modificar, DE= Destruir (2)
            string ARTREF_0 = "B5852";        //Referencia del artículo (5 de 16)
            string ARTREF_1 = "";             //Referencia del artículo (11 de 16)
            string ARTDES = "";               //Nombre del Artículo (40)
            string ARTPRIREF_0 = "B5852";     //Referencia del artículo principal. Agrupa a los distintos formatos de un mismo artículo.(5 de 16)
            string ARTPRIREF_1 = "";          //Referencia del artículo principal. Agrupa a los distintos formatos de un mismo artículo.(11 de 16)
            string ROTTIP = "FE  ";           //Tipo de Criterio de rotación: FC= Fecha Caducidad, FE= Fecha Entrada,    FF= Fecha Fabricación, LO= Lote (4)
            string DIACAD = "00000";          //Días de caducidad mínimos para aceptación de entradas si el criterio de rotación es la fecha de caducidad. (5)
            string DIACADMAX = "00000";       //Días de caducidad máximos para validar errores en entrada si el criterio de rotación es la fecha de caducidad.. (5)
            string DIAMINCADEXP = "00000";    //Días mínimos que deben faltar a la mercancía para caducar para poder expedir al punto de entrega, si el criterio de rotación es la fecha de caducidad.. (5)
            string LOTREQ = "N";              //Lote requerido en la recepción de mercancía.(1)
            string PESVAR = "N";              //Articulo de peso variable. N o  “”: sin gestión de peso. M: se calcula el peso medio. F: se pide el peso al final de la preparación. P: se pide el peso durante la preparación. (1)
            string SINEAN = "N";              //Artículo sin código EAN s/n (1)
            string UNIMIN = "UD  ";           //Unidad mínima o básica (UD) (4)
            string UNIREC = "";               //Unidad de Recepción (CJ, UD, ...). Debe ser una presentación definida para el artículo. (Fichero de interfaz TRARPR)  (4)
            string UNIALM = "";               //Unidad de Almacenamiento o Inventario (CJ, UD,...). Debe ser una presentación definida para el artículo. (Fichero de interfaz TRARPR) (4)
            string UNIEXP = "UD";             //Unidad de Expedición (CJ, UD,...). Debe ser una presentación definida para el artículo. (Fichero de interfaz TRARPR) (4)
            string GESSER = "N";              //Necesario número de serie. (N-No, S-Salidas, E-Entrada, A-Ambas). (1)
            string TERPROREF_0 = "B5852";     //Referencia del Tercero propietario de la mercancía  (en caso de operadores logísticos) (5 de 16)
            string TERPROREF_1 = "";          //Referencia del Tercero propietario de la mercancía  (en caso de operadores logísticos) (11 de 16)
            string SITINI = "    ";           //Situación en la que nacen los nuevos contenedores de este artículo  (OK= Disponible, BL= Bloqueada, etc... Posibles valores en tabla ‘SNDO’) (4)
            string BLOQUEADO = "N";           //Articulo Bloqueado. Control en la Recepción S/N (1)
            string NUMDIACALCAD = "    ";     //Numero de días que se suma a la Fecha de Entrada o Fecha de Fabricación, para calcular la Fecha de Caducidad. (Solo si el artículo rota por FE o FF).(4)
            string ADMCANMAYPOR = "000";      //Porcentaje admitido al recibir una cantidad mayor que la pedida. (3)
            string LOTE = "";                 //Lote, en el caso de Gestión de Articulo Concreto por Lote. (20)
            string CONCRETO = "P ";            //Gestión por Propietario (P), Lote (L) o Ambos (PL) (2)
            string CNTTIP = "";               //Soporte asociado al artículo en la recepción (8)

            string registroArticulo = "";

            //Columnas fichero manipulación TRARPR 

            string TIPREG_MAN = "ARPR";         //Tipo de Registro  (ARPR) (4)
            string ACCION_MAN = "AG";       //tipo de Acción: AG= Agregar, MO= Modificar, DE= Destruir (2)
            string ARTREF_MAN_0 = "B5852";  //Referencia del artículo(5)
            string ARTREF_MAN_1 = "";       //Referencia del artículo(16)
            string UNITIP = "";             //Tipo de unidad para esta presentación (‘CJ’, ‘UD’, etc) (4)
            string NUMUNI = "";             //Número de unidades básicas que incluye esta presentación (8)
            string CUB = "";                //Volumen en centímetros cúbicos. (6,3-->9)
            string MEDLAR = "";             //Largo en cm.(6,3-->9)
            string MEDALT = "";             //Alto en cm.(6,3-->9)
            string MEDANC = "";             //Ancho en cm.(6,3-->9)
            string PESUNI = "";             //Peso unitario de la unidad de esta presentación en kg.(5,3-->8)
            string PESINF = "";             //Si se trata de un artículo de peso variable, peso mínimo en kg que se admite como válido en entradas o salidas de mercancía.(5,3-->8)
            string PESSUP = "";             //Si se trata de un artículo de peso variable, peso máximo en kg que se admite como válido en entradas o salidas de mercancía.(5,3-->8)
            string UNIPAL = "";             //Número de unidades de esta presentación que conforman un pallet estándar. (9)
            string BASE = "";               //Cantidad de unidades de esta presentación en la base del pallet.(5)
            string ALTURA = "";             //Cantidad de unidades de esta presentación en la altura del pallet.(5)

            string manipulacionArticulo = "";


            //Campo de Relleno
            char padZero = '0';
            char padSpace = ' ';

            DataTable Articulos = new DataTable();
            csSqlConnects sqlConnect = new csSqlConnects();
            string query = "SELECT        CODART, DESCART, KLS_ID_SHOP, KLS_ARTICULO_TIENDA, " +
                " CASE WHEN PESO ='' THEN CAST(0 AS DECIMAL(6,3)) when peso  is null then CAST(0 AS DECIMAL(6,3)) else cast(peso as decimal(6,3)) END as PESO  " +
                " FROM   dbo.ARTICULO  WHERE (KLS_ID_SHOP > 0)";

            Articulos = sqlConnect.cargarDatosScriptEnTabla(query, "");

            using (StreamWriter writeTextFile = new StreamWriter("c:\\Albaranes\\TRART. " + DateTime.Now.ToString("yyMMdd").PadLeft(10, padZero) + ".txt"))
            {
                foreach (DataRow row in Articulos.Rows)
                {
                    ARTREF_1 = row["CODART"].ToString().Trim();
                    ARTREF_1 = ARTREF_1.PadLeft(11, padSpace);
                    //Recorto la descripción a 40 caracteres
                    ARTDES = row["DESCART"].ToString().ToUpper();
                    if (ARTDES.Length > 40)
                    {
                        ARTDES = ARTDES.Substring(0, 40);
                    }
                    ARTDES = ARTDES.PadLeft(40, padSpace);

                    ARTPRIREF_1 = ARTREF_1;

                    UNIREC = UNIREC.PadLeft(4, padSpace);
                    UNIALM = UNIALM.PadLeft(4, padSpace);
                    UNIEXP = UNIEXP.PadLeft(4, padSpace);

                    TERPROREF_1 = ARTREF_1;

                    SITINI = SITINI.PadLeft(4, padZero);
                    NUMDIACALCAD = NUMDIACALCAD.PadLeft(4, padZero);
                    LOTE = LOTE.PadLeft(20, padSpace);
                    CONCRETO = CONCRETO.PadLeft(2, padSpace);
                    CNTTIP = CNTTIP.PadLeft(8, padSpace);


                    registroArticulo = TIPREG + ACCION + ARTREF_0 + ARTREF_1 +
                                        ARTDES + ARTPRIREF_0 + ARTPRIREF_1 +
                                        ROTTIP + DIACAD + DIACADMAX +
                                        DIAMINCADEXP + LOTREQ + PESVAR +
                                        SINEAN + UNIMIN + UNIREC +
                                        UNIALM + UNIEXP + GESSER +
                                        TERPROREF_0 + TERPROREF_1 + SITINI +
                                        BLOQUEADO + NUMDIACALCAD + ADMCANMAYPOR +
                                        LOTE + CONCRETO + CNTTIP;

                    writeTextFile.WriteLine(registroArticulo);

                }
            }


            //FICHERO MANIPULACIÓN ARTÍCULOS
            using (StreamWriter writeTextFile = new StreamWriter("c:\\Albaranes\\TRARPR . " + DateTime.Now.ToString("yyMMdd").PadLeft(10, padZero) + ".txt"))
            {
                foreach (DataRow row in Articulos.Rows)
                {
                    //TIPREG_MAN=;
                    //ACCION_MAN=;
                    //ARTREF_MAN_0=;
                    ARTREF_MAN_1 = row["CODART"].ToString().Trim().PadLeft(11, padSpace);
                    UNITIP = "UD  ";
                    NUMUNI = "1";
                    NUMUNI = NUMUNI.PadLeft(8, padZero); ;
                    CUB = CUB.PadLeft(9, padZero);
                    MEDLAR = MEDLAR.PadLeft(9, padZero);
                    MEDALT = MEDALT.PadLeft(9, padZero);
                    MEDANC = MEDANC.PadLeft(9, padZero);
                    PESUNI = row["PESO"].ToString();
                    string[] parts = PESUNI.Split(',');
                    string PESO_ENTERO = parts[0].PadLeft(6, padZero);
                    string PESO_DECIMAL = parts[1].PadRight(3, padZero);
                    PESINF = PESINF.PadLeft(8, padZero);
                    PESSUP = PESSUP.PadLeft(8, padZero);
                    UNIPAL = UNIPAL.PadLeft(9, padZero);
                    BASE = BASE.PadLeft(5, padZero);
                    ALTURA = ALTURA.PadLeft(5, padZero);

                    manipulacionArticulo = TIPREG_MAN + ACCION_MAN + ARTREF_MAN_0 + ARTREF_MAN_1 + UNITIP + NUMUNI + CUB +
                                            MEDLAR + MEDALT + MEDANC + PESO_ENTERO + PESO_DECIMAL + PESINF +
                                            PESSUP + UNIPAL + BASE + ALTURA;

                    writeTextFile.WriteLine(manipulacionArticulo);
                }
            }


            //abrir carpeta destino de ficheros
            csUtilidades.openFolder(csGlobal.fileExportPath);
        }


    }
}
