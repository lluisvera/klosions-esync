﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using System.Diagnostics;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Diagnostics;
using System.Globalization;

namespace klsync
{
    class csRepasatWebService
    {
        SqlConnection dataconnection = new SqlConnection();
        csSqlConnects sql = new csSqlConnects();
        Repasat.csUpdateData updateDataRPST = new Repasat.csUpdateData();
        Utilidades.csLogErrores logProceso = new Utilidades.csLogErrores(); 


        //string webService = csGlobal.webServiceKey;
        //  string tipoObjeto = "products";
        //  string urlTarget = "https://panel.repasat.com/es/webservice/" + tipoObjeto + "/?api_token=" + webService;
        //  string myParameters = "nomArticulo=ARTICULO BORRAME MAÑANA" + "&" +
        //      "descArticulo=BORRAME 2018-2-19" + "&" +
        //      "codExternoArticulo=123456" + "&" +
        //      "precioVentaArticulo=98.9" + "&" +
        //      "precioCompraArticulo=13.8";
        //  using (WebClient wc = new WebClient())
        //  {
        //      wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //      string HtmlResult = wc.UploadString(urlTarget, myParameters);
        //  }

        public string urlRepasatWS(string tipoObjeto, string metodo, string filtros, string id, string page="", string repasatFunction=null, Objetos.csTercero account=null)
        {
            string webService = "&api_token=" + csGlobal.webServiceKey;
            string anexoMetodo = "";
            string anexoPagina = "";

            switch (metodo)
            {
                case "POST":
                    anexoMetodo = "index?";
                    break;
                case "PUT":
                    anexoMetodo = "index/" + id + "?";
                    break;
                case "GET":
                    anexoMetodo = "list?";
                    if (page != "")
                    {
                        anexoPagina =page;
                    }
                    //anexoPagina = "&page=" + page;
                    break;
            }

            //Modificación para añadir función de pago 
            anexoMetodo = repasatFunction== "cancelPayment"?  "cancel/" + id + "?" : anexoMetodo;

            anexoMetodo = repasatFunction == "checkdocument" ? "checkdocument/?" : anexoMetodo;


            if (repasatFunction == "checkaccount") {

                anexoMetodo = "checkaccount/" + account.codIC + "/" + account.nifcif + "/" + csGlobal.id_conexion_externa + "/" + csGlobal.idUnidadNegocio + "/?";
            }

            if (repasatFunction == "updatetbai")
            {
                anexoMetodo = "updatetbai/" + id + "?"; 
            }
            if (repasatFunction == "cancelsync")
            {
                anexoMetodo = "cancelsync/" + id + "?";
            }





            //if (tipoObjeto == "addresses")
            //{
            //    tipoObjeto = "accountaddresses";
            //}
            //if (csGlobal.conexionDB.Contains("KLS")) {
            //    csGlobal.APIUrl = "http://localhost:8080/repasat/es/webservice/";
            //}

            //string urlTarget = "https://panel.repasat.com/es/webservice/" + tipoObjeto + "/" + anexoMetodo + webService + filtros + anexoPagina;
            string urlTarget = csGlobal.APIUrl + tipoObjeto + "/" + anexoMetodo + webService + filtros + anexoPagina;


            return urlTarget;
        }

        /// <summary>
        /// Función para actualizar un registro en Repasat
        /// </summary>
        /// <param name="tipoObjeto"></param>
        /// <param name="field"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="cliente"></param>
        public void actualizarDocumentoRepasat(string tipoObjeto, string field,string key, string value, bool cliente=false, string funcion=null)
        {
            try
            {
                DataTable dtParams = new DataTable();
                dtParams.TableName = "dtParams";
                dtParams.Columns.Add("FIELD");
                dtParams.Columns.Add("KEY");
                dtParams.Columns.Add("VALUE");

                DataTable dtKeys = new DataTable();
                dtKeys.TableName = "dtKeys";
                dtKeys.Columns.Add("KEY");

                DataRow drParams = dtParams.NewRow();
                drParams["FIELD"] = field;
                drParams["KEY"] = key;
                drParams["VALUE"] = value;
                dtParams.Rows.Add(drParams);

                DataRow drKeys = dtKeys.NewRow();
                drKeys["KEY"] = key;
                dtKeys.Rows.Add(drKeys);

                csRepasatWebService rpstWS = new csRepasatWebService();
                rpstWS.sincronizarObjetoRepasat(tipoObjeto, "PUT", dtKeys, dtParams, key, cliente,false, funcion);
            }
            catch (Exception ex)
            { 
            }
        
        }

        private string parametrosWebServiceAccounts(string key, DataTable dtParams)
        {
            string parametros = "";
            string nomParametro = "";
            string valorParametro = "";
            int contador = 0;

            // string nomCliente = "MAÑANA CÚSTOMER";

            foreach (DataRow dr in dtParams.Rows)
            {
                nomParametro = dr["FIELD"].ToString();
                valorParametro = dr["VALUE"].ToString();

                if (dr["KEY"] == key)
                {
                    if (contador > 0) parametros = parametros + "&";

                    if (nomParametro != "ticketbai[url]") {
                        parametros = parametros + nomParametro + "=" + csUtilidades.encodeUrl2TPST(valorParametro);
                    }
                    else {

                        valorParametro = valorParametro.Replace("%", "%25").Replace("&", "%26");
                        valorParametro = valorParametro.Replace("&", "%26");
                        parametros = parametros + nomParametro + "=" + WebUtility.UrlEncode(valorParametro);
                    }
                    
                    contador++;
                }
            }

            return parametros;
        }




        public void sincronizarObjetoRepasat(string tipoObjeto, string metodo, DataTable dtKey, DataTable dtParams, string filtros, bool cliente = false, bool isUpdate = false, string repasatFunction = null, bool updateRPSTFromA3CronJob = false)
        {
            string errorMetodo = "";
            string errorObjeto = "";
            string errorKeyData = "";
            string errorUrl = "";
            string errorLogMessage = "";
            bool responseValidJSON = false;
            int actuRealizadas = 0;

            try
            {
                string key = null;
                string erpKey = "";
                string query = "";

                foreach (DataRow dr in dtKey.Rows)
                {
                    erpKey = null;
                    key = dr["KEY"].ToString();
                    string myParameters = parametrosWebServiceAccounts(key, dtParams);
                    string urlTarget = urlRepasatWS(tipoObjeto, metodo, "", key, "", repasatFunction);

                    // Variables para capturar errores si ocurre una excepción
                    errorUrl = urlTarget;
                    errorMetodo = metodo;
                    errorObjeto = tipoObjeto;
                    errorKeyData = key;

                    errorLogMessage = "Método: " + errorMetodo + "\n" +
                                        "Objeto: " + errorObjeto + "\n" +
                                        "Url: " + errorUrl + "\n" +
                                        "Valor Clave: " + errorKeyData + "\n";

                    string[] parametros = myParameters.Split('&');

                    if (tipoObjeto == "accounts")
                    {
                        if (metodo == "PUT")
                        {
                            using (WebClient wc = new WebClient())
                            {
                                try
                                {
                                    erpKey = parametros.Count() == 1 ? parametros[0].Split('=')[1] : parametros[10].Split('=')[1];
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                    wc.Encoding = Encoding.UTF8;

                                    // Llamada al servicio
                                    string json = wc.UploadString(urlTarget, metodo, myParameters);

                                    // Verificación de la respuesta
                                    if (json.Contains("\"status\":false,\"error\":\"El recurso no existe\"") || json.Contains("El recurso no existe"))
                                    {
                                        if (csGlobal.modoManual) logProceso.guardarLogProceso("Error en la cuenta: " + key + " RPST / A3ERP " + erpKey + " url " + urlTarget + "\n" + "Parámetros: " + myParameters);
                                    }
                                    else
                                    {
                                        actualizarObjetoA3ERP(json, tipoObjeto, true, true);
                                    }
                                }
                                catch (WebException webEx)
                                {
                                    // Captura de error en la conexión con detalles de la respuesta
                                    string response = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                                    Program.guardarErrorFichero($"Error de conexión WebException: URL: {urlTarget}, Clave: {key}, Respuesta: {response}, Error: {webEx.Message}");
                                }
                                catch (Exception ex)
                                {
                                    // Cualquier otro error
                                    Program.guardarErrorFichero($"Error en sincronizarObjetoRepasat (PUT): URL: {urlTarget}, Clave: {key}, Error: {ex.Message}");
                                }
                            }
                        }
                        else if (metodo == "POST")
                        {
                            Objetos.csTercero account = new Objetos.csTercero();
                            account.codIC = key;
                            account.nifcif = dtParams.Rows[2]["VALUE"].ToString();
                            account.conexionExterna = csGlobal.id_conexion_externa;
                            account.unidadNegocio = csGlobal.idUnidadNegocio;

                            // Verificación de existencia del cliente
                            erpKey = verificarRegistroEnRepasat("accounts", "GET", key.Trim(), cliente, account);

                            if (!string.IsNullOrEmpty(erpKey) && tipoObjeto == "accounts")
                            {
                                // Actualizar A3ERP con el Id de la cuenta
                                string tabla = cliente ? " __CLIENTES " : " __PROVEED ";
                                string campo = cliente ? "CODCLI" : "CODPRO";
                                string campoExterno = cliente ? "RPST_ID_CLI" : "RPST_ID_PROV";
                                query = $"UPDATE {tabla} SET {campoExterno} = {erpKey} WHERE LTRIM({campo}) = '{key}'";
                                csUtilidades.ejecutarConsulta(query, false);
                            }
                            else
                            {
                                using (WebClient wc = new WebClient())
                                {
                                    try
                                    {
                                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                        wc.Encoding = Encoding.UTF8;

                                        string json = wc.UploadString(urlTarget, metodo, myParameters);
                                        if (json.Contains("\"status\":true"))
                                        {
                                            actualizarObjetoA3ERP(json, tipoObjeto, cliente, isUpdate);
                                        }
                                    }
                                    catch (WebException webEx)
                                    {
                                        string response = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                                        Program.guardarErrorFichero($"Error de conexión WebException (POST accounts): URL: {urlTarget}, Clave: {key}, Respuesta: {response}, Error: {webEx.Message}");
                                    }
                                    catch (Exception ex)
                                    {
                                        Program.guardarErrorFichero($"Error en sincronizarObjetoRepasat (POST accounts): URL: {urlTarget}, Clave: {key}, Error: {ex.Message}");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                                string jsonResponse = wc.UploadString(urlTarget, metodo, myParameters);

                                if (jsonResponse.Contains("\"status\":true") && !jsonResponse.Contains("\"status\":\"error\"") && metodo == "POST")
                                {
                                    if (jsonResponse.Contains("CheckDocument\":\"true"))
                                    {
                                        try
                                        {
                                            while (!responseValidJSON)
                                            {
                                                refactorizarJSON(ref jsonResponse, ref responseValidJSON);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Program.guardarErrorFichero($"Error refactorizando JSON: {ex.Message}");
                                        }

                                        var repasat = JsonConvert.DeserializeObject<Repasat.csSaleInvoiceCheck.Root>(jsonResponse);
                                        string jsonFormatted = JValue.Parse(jsonResponse).ToString(Newtonsoft.Json.Formatting.Indented);
                                        MessageBox.Show(jsonFormatted, "Validación de Documento");

                                        if (repasat.data.resource.documentoSubido != "OK")
                                        {
                                            string idFacA3 = repasat.data.resource.idDocA3ERP.ToString();
                                            string idDocRPST = repasat.data.resource.idDocumento.ToString();
                                            string queryUpdate = $"UPDATE CABEFACV SET RPST_ID_fACV={idDocRPST} WHERE IDFACV={idFacA3}";
                                            sql.ejecutarQuery(queryUpdate);
                                        }
                                        if (repasat.data.resource.Direccion != "OK")
                                        {
                                            string idAddresRPST = repasat.data.resource.idDireccion.ToString();
                                            string queryUpdate = $"UPDATE DIRENT SET RPST_ID_DIR=NULL WHERE RPST_ID_DIR={idAddresRPST}";
                                            if (!string.IsNullOrEmpty(repasat.data.resource.CuentaA3))
                                            {
                                                sql.ejecutarQuery(queryUpdate);
                                                crearDireccionesCuentasEnRepasat(repasat.data.resource.CuentaA3, cliente);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            actualizarObjetoA3ERP(jsonResponse, tipoObjeto, cliente);
                                        }
                                        catch (Exception ex)
                                        {
                                            Program.guardarErrorFichero($"Error actualizando objeto A3ERP: {ex.Message}");
                                        }
                                    }
                                } 
                                else if (metodo == "PUT")
                                {
                                    if (jsonResponse.Contains("\"status\":true") && !jsonResponse.Contains("\"status\":\"error\""))
                                    {
                                        actuRealizadas++;
                                        logProceso.guardarLogProceso($"Registro actualizado exitosamente: Clave {key} en URL {urlTarget}");
                                    }
                                    else
                                    {
                                        logProceso.guardarLogProceso($"Error al actualizar: Clave {key} en URL {urlTarget} , Respuesta: {jsonResponse}");
                                    }    
                                }
                            }
                        }
                        catch (WebException webEx)
                        {
                            string response = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                            Program.guardarErrorFichero($"Error de conexión WebException (POST otros): URL: {urlTarget}, Clave: {key}, Respuesta: {response}, Error: {webEx.Message}");
                        }
                        catch (Exception ex)
                        {
                            Program.guardarErrorFichero($"Error en sincronizarObjetoRepasat (POST otros): URL: {urlTarget}, Clave: {key}, Error: {ex.Message}");
                        }
                    }
                }

                logProceso.guardarLogProceso($"Total de registros actualizados exitosamente: {actuRealizadas}");

                Repasat.csUpdateData updateData = new Repasat.csUpdateData();
                if (isUpdate)
                {
                    updateData.actualizarFechaSync(tipoObjeto, dtKey.Rows.Count, updateRPSTFromA3CronJob);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero($"Error general en sincronizarObjetoRepasat: {ex.Message}");
            }
        }



        ///// <summary>
        ///// Sincronizar Objeto En Repasat
        ///// </summary>
        ///// <param name="tipoObjeto"></param>
        ///// <param name="metodo"></param>
        ///// <param name="dtKey"></param>
        ///// <param name="dtParams"></param>
        ///// <param name="filtros"></param>
        ///// <param name="cliente"></param>
        ///// <param name="isUpdate"></param>
        //public void sincronizarObjetoRepasat(string tipoObjeto, string metodo, DataTable dtKey, DataTable dtParams, string filtros, bool cliente=false,bool isUpdate=false, string repasatFunction=null, bool updateRPSTFromA3CronJob=false )
        //{
        //    string errorMetodo = "";
        //    string errorObjeto = "";
        //    string errorKeyData = "";
        //    string errorUrl = "";
        //    string errorLogMessage = "";
        //    bool responseValidJSON = false;
        //    try
        //    {
        //        string key = null;
        //        string erpKey = "";
        //        string query = "";

        //        foreach (DataRow dr in dtKey.Rows)
        //        {
        //            erpKey = null;
        //            key = dr["KEY"].ToString();
        //            string myParameters = parametrosWebServiceAccounts(key, dtParams);
        //            string urlTarget = urlRepasatWS(tipoObjeto, metodo, "", key,"",repasatFunction);


        //            errorUrl = urlTarget;       //Variable para enviar al catch
        //            errorMetodo = metodo;       //Variable para enviar al catch
        //            errorObjeto = tipoObjeto;   //Variable para enviar al catch
        //            errorKeyData = key;         //Variable para enviar al catch

        //            errorLogMessage = "Metodo: " + errorMetodo + "\n" +
        //                                "Objeto: " + errorObjeto + "\n" +
        //                                "Url: " + errorUrl + "\n" +
        //                                "Valor Clave: " + errorKeyData + "\n";


        //            string[] parametros = myParameters.Split('&');



        //            if (tipoObjeto == "accounts")
        //            {
        //                if (metodo == "PUT")
        //                {
        //                    using (WebClient wc = new WebClient())
        //                    {
        //                        try
        //                        {
        //                            erpKey = parametros.Count() == 1 ? erpKey = parametros[0].Split('=')[1] : parametros[10].Split('=')[1];
        //                            //if (csGlobal.modoManual) logProceso.guardarLogProceso("Procesando cuenta: " + urlTarget + "\n" + "Parametros: " + myParameters);
        //                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //                            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //                            wc.Encoding = Encoding.UTF8;

        //                            string json = wc.UploadString(urlTarget, metodo, myParameters);

        //                            if (json.Contains("\"status\":false,\"error\":\"El recurso no existe\"") || json.Contains("El recurso no existe"))
        //                            {
        //                                if (csGlobal.modoManual) logProceso.guardarLogProceso("Error en la cuenta: " + key + " RPST / A3ERP " + erpKey + " url " + urlTarget + "\n" + "Parametros: " + myParameters);
        //                                //break;
        //                            }
        //                            else
        //                            {
        //                                //if (csGlobal.modoManual) logProceso.guardarLogProceso("Cuenta actualizada: " + urlTarget);
        //                                actualizarObjetoA3ERP(json, tipoObjeto, true, true);
        //                            }
        //                        }
        //                        catch (Exception ex) {
        //                            if (csGlobal.modoManual) logProceso.guardarLogProceso("Error en la cuenta: " + urlTarget + "\n" + "Parametros: " + myParameters);
        //                        }
        //                    }
        //                }
        //                else if (metodo=="POST")
        //                {
        //                    Objetos.csTercero account = new Objetos.csTercero();
        //                    account.codIC = key;
        //                    account.nifcif = dtParams.Rows[2]["VALUE"].ToString();
        //                    account.conexionExterna = csGlobal.id_conexion_externa;
        //                    account.unidadNegocio = csGlobal.idUnidadNegocio;


        //                    //Verifico si existe el cliente en Repasat antes de Crearlo
        //                    erpKey = verificarRegistroEnRepasat("accounts", "GET", key.Trim(), cliente, account);

        //                    if (!string.IsNullOrEmpty(erpKey) && tipoObjeto == "accounts")
        //                    {
        //                        //Actualizar A3ERP e informar el Id de la cuenta

        //                        string tabla = cliente ? " __CLIENTES " : " __PROVEED ";
        //                        string campo = cliente ? "CODCLI" : "CODPRO";
        //                        string campoExterno = cliente ? "RPST_ID_CLI" : "RPST_ID_PROV";
        //                        query = "UPDATE " + tabla + " SET " + campoExterno + "=" + erpKey + " WHERE LTRIM(" + campo + ")='" + key + "'";
        //                        csUtilidades.ejecutarConsulta(query, false);
        //                    }
        //                    else //Aqui se crea la cuenta en RPST
        //                    {
        //                        using (WebClient wc = new WebClient())
        //                        {
        //                            wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //                           // wc.Headers["Content-Type"] = "application/json";
        //                            wc.Encoding = Encoding.UTF8;

        //                            string json = wc.UploadString(urlTarget, metodo, myParameters);
        //                            if (json.Contains("\"status\":true"))
        //                            {
        //                                actualizarObjetoA3ERP(json, tipoObjeto, cliente, isUpdate);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                try
        //                {
        //                    using (WebClient wc = new WebClient())
        //                    {
        //                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //                        //INFORMACION QUE DEVUELVE REPASAT = 0 (LO ARREGLA lLUIS)
        //                        string jsonResponse = wc.UploadString(urlTarget, metodo, myParameters);
        //                        // var content = wc.UploadString(urlTarget, metodo, myParameters);
        //                        //Actualizo A3ERP con los valores de Repasat

        //                        if (jsonResponse.Contains("\"status\":true") && !jsonResponse.Contains("\"status\":\"error\"") && metodo == "POST")
        //                        {
        //                            if (jsonResponse.Contains("CheckDocument\":\"true"))
        //                            {
        //                                try
        //                                {
        //                                    while (!responseValidJSON)
        //                                    {
        //                                        refactorizarJSON(ref jsonResponse, ref responseValidJSON);
        //                                    }

        //                                }
        //                                catch (Exception ex) { }

        //                                var repasat = JsonConvert.DeserializeObject<Repasat.csSaleInvoiceCheck.Root>(jsonResponse);
        //                                string jsonFormatted = JValue.Parse(jsonResponse).ToString(Newtonsoft.Json.Formatting.Indented);
        //                                MessageBox.Show(jsonFormatted, "Validación de Documento");

        //                                if (repasat.data.resource.documentoSubido != "OK")
        //                                {
        //                                    string idFacA3 = repasat.data.resource.idDocA3ERP.ToString();
        //                                    string idDocRPST = repasat.data.resource.idDocumento.ToString();
        //                                    string queryUpdate = "UPDATE CABEFACV SET RPST_ID_fACV=" + idDocRPST + " WHERE IDFACV=" + idFacA3;
        //                                    sql.ejecutarQuery(queryUpdate);
        //                                }
        //                                if (repasat.data.resource.Direccion != "OK")
        //                                {
        //                                    //crear la dirección en Repasat
        //                                    //string idAddressA3 = repasat.data.resource..ToString();
        //                                    string idAddresRPST = repasat.data.resource.idDireccion.ToString();
        //                                    string queryUpdate = "UPDATE DIRENT SET RPST_ID_DIR=NULL WHERE RPST_ID_DIR=" + idAddresRPST;
        //                                    if (!string.IsNullOrEmpty(repasat.data.resource.CuentaA3))
        //                                    {
        //                                        sql.ejecutarQuery(queryUpdate);
        //                                        crearDireccionesCuentasEnRepasat(repasat.data.resource.CuentaA3, cliente);
        //                                        //
        //                                    }

        //                                }
        //                            }
        //                            else
        //                            {
        //                                try { 
        //                                actualizarObjetoA3ERP(jsonResponse, tipoObjeto, cliente);
        //                                }
        //                                catch (Exception ex) { }
        //                            }
        //                        }

        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    csUtilidades.log(errorLogMessage + ex.Message);
        //                    if (csGlobal.modoManual) logProceso.guardarLogProceso("Error en el procceso: " + urlTarget + "\n" + "Parametros: " + myParameters);
        //                    MessageBox.Show("Error en el procceso: " + urlTarget + "\n" + "Parametros: " + myParameters + "\n" + ex.Message);

        //                }
        //            }
        //        }

        //        Repasat.csUpdateData updateData = new Repasat.csUpdateData();
        //        if (isUpdate)
        //        {
        //            updateData.actualizarFechaSync(tipoObjeto, dtKey.Rows.Count, updateRPSTFromA3CronJob);
        //        }
        //    }
        //    catch (Exception ex)
        //    { 
        //      csUtilidades.log(errorLogMessage + ex.Message);
        //    }
        //}

        public void refactorizarJSON(ref string json, ref bool responseValidJSON) 
        {
            try
            {
                JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json);
                responseValidJSON = true;
            }
            catch
            {
                json = json + '}';
            }
        }



        private void actualizarObjetoA3ERP(string json, string tipoObjeto, bool esCliente=false, bool update =false)
        {
            try
            {
                string idRepasat = "";
                string codA3ERP = "";
                string query = "";
                string tipoCli = "";
                bool cliente = true;
                bool proveedor = false;
                bool responseValidJSON = false;

                if (json.Contains("<b>Fatal error</b>"))
                {
                    return;
                }

                //Se añade este bloque, porque al leer el json, borra las llaves al final 
                while (!responseValidJSON)
                {
                    refactorizarJSON(ref json, ref responseValidJSON);
                }


                if (tipoObjeto == "products")
                {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csProductsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idArticulo.ToString();
                        codA3ERP = repasat.data.resource.codExternoArticulo.ToString();

                        query = "UPDATE ARTICULO SET RPST_ID_PROD =" + idRepasat + " WHERE LTRIM(CODART)='" + codA3ERP + "'";
                        csUtilidades.ejecutarConsulta(query, false);
                }



                if (tipoObjeto == "accounts")
                {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csAccountsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idCli.ToString();
                        codA3ERP = repasat.data.resource.codExternoCli.ToString();
                        tipoCli = repasat.data.resource.tipoCli.ToString();

                        cliente = (tipoCli == "CLI") ? true : false;
                        string tabla = cliente ? "__CLIENTES" : "__PROVEED";
                        string campo = cliente ? "CODCLI" : "RPST_ID_PROV";
                        string campoExterno = cliente ? "RPST_ID_CLI" : "CODPRO";
                        query = "UPDATE "+ tabla + " SET "+ campoExterno + "=" + idRepasat + " WHERE LTRIM(" + campo + ")='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                        //Creo las direcciones en Repasat
                        if (!update)
                        {
                            //22-03-2021 MODIFICACIÓN LVG
                            //crearDireccionesCuentasEnRepasat(codA3ERP, esCliente);
                            crearDireccionesCuentasEnRepasat(codA3ERP, cliente);
                            //crear datos bancarios clietne
                            updateDataRPST.gestionarCuentasBancariasA3ToRepasat(cliente, false, codA3ERP);
                        }

                }
                if (tipoObjeto == "addresses" || tipoObjeto == "accountaddresses")
                {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csAddressesResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDireccion.ToString();
                        codA3ERP = repasat.data.resource.codExternoDireccion.ToString();

                        string tabla = esCliente ? "DIRENT" : "__DIRENTPRO";
                        string campoExterno = esCliente ? "RPST_ID_DIR" : "RPST_ID_DIRPROV";
                        query = "UPDATE " + tabla + " SET " + campoExterno + " =" + idRepasat + " WHERE IDDIRENT=" + codA3ERP + "";
                        csUtilidades.ejecutarConsulta(query, false);
                }

                if (tipoObjeto == "contacts")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csContactsResponsepost.RootObject>(json);
                        idRepasat = repasat.data.resource.idContacto.ToString();
                        codA3ERP = repasat.data.resource.codExternoContacto.ToString();

                        if (csGlobal.versionA3ERP > 11)
                        {
                            query = "UPDATE __CONTACTOS SET RPST_ID_CONTACT =" + idRepasat + " WHERE ID=" + codA3ERP;
                        }
                        else {
                            query = "UPDATE CONTACTOS SET RPST_ID_CONTACT =" + idRepasat + " WHERE IDCONTACTO=" + codA3ERP;
                        }


                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "banks")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csBanksResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDatosBancarios.ToString();
                        codA3ERP = repasat.data.resource.codExternoDatosBancarios.ToString();

                        query = "UPDATE DOMBANCA SET RPST_ID_BANCO =" + idRepasat + " WHERE IDDOMBANCA=" + codA3ERP;

                        csUtilidades.ejecutarConsulta(query, false);
                    }

                }

                if (tipoObjeto == "routes")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csRoutasResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idRuta.ToString();
                        codA3ERP = repasat.data.resource.codExternoRuta.ToString();

                        query = "UPDATE RUTAS SET RPST_ID_RUTA =" + idRepasat + " WHERE LTRIM(RUTA)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "employees")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csEmployeesResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idTrabajador.ToString();
                        codA3ERP = repasat.data.resource.codExternoTrabajador.ToString();

                        query = "UPDATE REPRESEN SET RPST_ID_REPRE =" + idRepasat + " WHERE LTRIM(CODREP)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                
                if (tipoObjeto == "paymentmethods")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentMethodsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idFormaPago.ToString();
                        codA3ERP = repasat.data.resource.codExternoFormaPago.ToString();

                        query = "UPDATE FORMAPAG SET RPST_ID_FORMAPAG =" + idRepasat + " WHERE LTRIM(FORPAG)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }


                if (tipoObjeto == "paymentdocuments")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csPaymentDocsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDocuPago.ToString();
                        codA3ERP = repasat.data.resource.codExternoDocuPago.ToString();

                        query = "UPDATE DOCUPAGO SET RPST_ID_DOCPAGO =" + idRepasat + " WHERE LTRIM(DOCPAG)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "geozones")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeoResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idZonaGeo.ToString();
                        codA3ERP = repasat.data.resource.codExternoZonaGeo.ToString();

                        query = "UPDATE ZONAS SET RPST_ID_ZONA =" + idRepasat + " WHERE LTRIM(ZONA)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "families")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        string fam = csGlobal.familia_art;
                        string subfam = csGlobal.subfamilia_art;
                        var repasat = JsonConvert.DeserializeObject<Repasat.csFamiliaArtResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idFam.ToString();
                        codA3ERP = repasat.data.resource.codExternoFamilia.ToString();
                        tipoCli = repasat.data.resource.tipoFam.ToString(); //Se llama tipocli pero es el tipoFam que sirve para ver si es subfamilia o no 

                        if(tipoCli == "0")
                        {
                            query = "UPDATE CARACTERISTICAS SET RPST_ID_CARACTERISTICA =" + idRepasat + " where LTRIM(NUMCAR) = '" + fam + "' and TIPCAR = 'A' and LTRIM(CODCAR)='" + codA3ERP + "'";

                        }
                        else  
                        {
                            query = "UPDATE CARACTERISTICAS SET RPST_ID_CARACTERISTICA =" + idRepasat + " where LTRIM(NUMCAR) = '" + subfam + "' and TIPCAR = 'A' and LTRIM(CODCAR)='" + codA3ERP + "'";

                        }
                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                //if (tipoObjeto == "costcenters")
                //{
                //    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                //    {
                //       var repasat = JsonConvert.DeserializeObject<Repasat.csCostCentersPost.RootObject>(json);
                //        idRepasat = repasat.data.resource.uuidCentroCoste.ToString();
                //        codA3ERP = repasat.data.resource.codExternoCentroC.ToString();

                //        query = "UPDATE CENTROSC SET RPST_ID_CENTROC ='" + idRepasat + "' WHERE LTRIM(CENTROCOSTE)='" + codA3ERP + "'";

                //        csUtilidades.ejecutarConsulta(query, false);
                //    }
                //}
                if (tipoObjeto == "brands")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        string marca = csGlobal.marcas_art;
                        var repasat = JsonConvert.DeserializeObject<Repasat.csMarcaArtResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.uuidMarca.ToString();
                        codA3ERP = repasat.data.resource.codExternoMarca.ToString();

                        query = "UPDATE CARACTERISTICAS SET RPST_ID_CARACTERISTICA ='" + idRepasat + "' where LTRIM(NUMCAR) = '" + marca + "' and TIPCAR = 'A' and LTRIM(CODCAR)='" + codA3ERP + "'";
                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "producttypes")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        string tipoArt = csGlobal.tipo_art;
                        var repasat = JsonConvert.DeserializeObject<Repasat.csTipoArtResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idTipoArticulo.ToString();
                        codA3ERP = repasat.data.resource.codExternoTipoArticulo.ToString();

                        query = "UPDATE CARACTERISTICAS SET RPST_ID_CARACTERISTICA ='" + idRepasat + "' where LTRIM(NUMCAR) = '" + tipoArt + "' and TIPCAR = 'A' and LTRIM(CODCAR)='" + codA3ERP + "'";
                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }
            

                if (tipoObjeto == "geozones")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csZonasGeoResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idZonaGeo.ToString();
                        codA3ERP = repasat.data.resource.codExternoZonaGeo.ToString();

                        query = "UPDATE ZONAS SET RPST_ID_ZONA =" + idRepasat + " WHERE LTRIM(ZONA)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "costcenters")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csCostCentersPost.RootObject>(json);
                        idRepasat = repasat.data.resource.uuidCentroCoste.ToString();
                        codA3ERP = repasat.data.resource.codExternoCentroC.ToString();

                        query = "UPDATE CENTROSC SET RPST_ID_CENTROC ='" + idRepasat + "' WHERE LTRIM(CENTROCOSTE)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "seriesdocs")
                {
                
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csSeriesdocsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idSerie.ToString();
                        codA3ERP = repasat.data.resource.nomSerie.ToString();

                        query = "UPDATE SERIES SET RPST_ID_SERIE =" + idRepasat + " WHERE LTRIM(SERIE)='" + codA3ERP + "'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }

                }



                if (tipoObjeto == "saleorders")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csSaleOrdersResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDocumento.ToString();
                        codA3ERP = repasat.data.resource.codExternoDocumento.ToString();

                        query = "UPDATE CABEPEDV SET RPST_ID_PEDV =" + idRepasat + " WHERE IDPEDV=" + codA3ERP ;

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }

                if (tipoObjeto == "purchaseorders")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csPurchaseOrdersPost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDocumento.ToString();
                        codA3ERP = repasat.data.resource.codExternoDocumento.ToString();

                        query = "UPDATE CABEPEDC SET RPST_ID_PEDC =" + idRepasat + " WHERE IDPEDC=" + codA3ERP;

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }
                if (tipoObjeto == "saleinvoices")
                {
                    try
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csSaleInvoicePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idDocumento.ToString();
                        codA3ERP = repasat.data.resource.codExternoDocumento.ToString();

                        if (!string.IsNullOrEmpty(idRepasat) && !string.IsNullOrEmpty(codA3ERP))
                        {

                            query = "UPDATE CABEFACV SET RPST_ID_FACV =" + idRepasat + " WHERE IDFACV=" + codA3ERP;
                            csUtilidades.ejecutarConsulta(query, false);
                        }
                    } catch (Exception ex) { }
                }
                if (tipoObjeto == "accountingaccounts")
                {
                    if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                    {
                        var repasat = JsonConvert.DeserializeObject<Repasat.csAccountingAccountsResponsePost.RootObject>(json);
                        idRepasat = repasat.data.resource.idCuentaContable.ToString();
                        codA3ERP = repasat.data.resource.codExtCuentaContable.ToString();

                        query = "UPDATE CUENTAS SET RPST_ID_CTA =" + idRepasat + " WHERE CUENTA=" + codA3ERP + " AND NIVEL=5 AND PLACON='NPGC'";

                        csUtilidades.ejecutarConsulta(query, false);
                    }
                }
                if (tipoObjeto == "incomingpayments")
                {
                    try
                    {
                        if ((JsonConvert.DeserializeObject<Repasat.csEmptyResponse.RootObject>(json)).status != false)
                        {
                            var repasat = JsonConvert.DeserializeObject<Repasat.csIncomingPaymentsPost.RootObject>(json);
                            idRepasat = repasat.data.resource.idCarteraCobros.ToString();
                            codA3ERP = string.IsNullOrEmpty(repasat.data.resource.codExternoCarteraCobros) ? string.Empty :  repasat.data.resource.codExternoCarteraCobros.ToString();
                            bool impagado = repasat.data.resource.impagadoCarteraCobros == 1 ? true : false;

                            if (!impagado)
                            {
                                if (!string.IsNullOrEmpty(codA3ERP))
                                {
                                    query = "UPDATE CARTERA SET RPST_ID_CARTERA =" + idRepasat + " WHERE IDCARTERA=" + codA3ERP;
                                    csUtilidades.ejecutarConsulta(query, false);
                                }
                            }


                        }
                    }
                    catch (Exception ex) {
                        ex.Message.mb();
                    }
                }

            }
            catch (Exception ex)
            {
                Program.escribirErrorEnFichero(ex.Message);
            }
        }

        private void crearDireccionesCuentasEnRepasat(string codCli, bool esCliente)
        {
            Repasat.csAddresses addresses = new Repasat.csAddresses();
            addresses.cargarDireccionesA3ToRepasat(codCli,esCliente);
        
        }

        private string whileJson(WebClient wc, string type, string page, string filtroURL, string repasatFunction = null, Objetos.csTercero account = null)
        {
            /*
             * No parará hasta que conecte
             * 
             */
            string rt = "";
            bool ok = false;

            while (!ok)
            {
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    rt = wc.DownloadString(urlRepasatWS("accounts", "GET",filtroURL ,"","",repasatFunction, account));
                    if (rt != null)
                    {
                        ok = true;
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
            }

            return rt;
        }

        public string verificarRegistroEnRepasat(string objeto, string metodo, string key, bool cliente=false, Objetos.csTercero account = null)
        {
            bool existeRegistro = false;
            csSqlConnects sql = new csSqlConnects();
            Objetos.csTercero[] clientes = null;
            int total_lineas = 0, contador = 0;
            string filtroURL = "";
            string idRegistroEnRepasat = null;

            string tablaToUpdate = "";
            string campoKeyTablaToUpdate = "";
            string campoKeyTabla = "";


            switch (objeto)
            { 
                case "accounts":
                    if (cliente)
                    {
                        filtroURL = "&filter[tipoCli]=CLI&filter[codExternoCli]=" + key;
                        tablaToUpdate = "__CLIENTES";
                        campoKeyTabla = "CODCLI";
                        campoKeyTablaToUpdate = "RPST_ID_CLI";
                    }
                    else
                    {
                        filtroURL = "&filter[tipoCli]=PRO&filter[codExternoCli]=" + key;
                        tablaToUpdate = "__PROVEED";
                        campoKeyTabla = "CODPRO";
                        campoKeyTablaToUpdate = "RPST_ID_PROV";
                    }
                    break;
                case "products":
                    filtroURL = "&filter[idArticulo]=" + key;
                    break;
            }


            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;

                var json = whileJson(wc, key, "&page=" + 1,filtroURL, null, account);

                if ((JsonConvert.DeserializeObject<Repasat.csEmpty.RootObject>(json)).total != 0)
                {
                    var repasat = JsonConvert.DeserializeObject<Repasat.CustomerList.RootObject>(json);
                    clientes = new Objetos.csTercero[repasat.data.Count];

                    for (int i = repasat.current_page; i <= repasat.last_page || i == repasat.current_page; i++) // páginas
                    {

                        for (int ii = 0; ii < repasat.data.Count; ii++)
                        {
                            clientes[ii] = new Objetos.csTercero();

                            //clientes[ii].codIC = repasat.data[ii].codExternoCli;
                            if (repasat.data[ii].codExternoCli == key)
                            {
                                idRegistroEnRepasat = repasat.data[ii].idCli.ToString();

                                string idRepasat = repasat.data[ii].idCli.ToString();
                                string queryToUpdate = "UPDATE " + tablaToUpdate + " SET " + campoKeyTablaToUpdate + " = " + idRepasat + " where ltrim(" + campoKeyTabla + " )='" + key + "'";
                                sql.actualizarCampo(queryToUpdate);
                                //existeRegistro = true;
                                break;
                            }
                        }
                    }
                }
            }
            return idRegistroEnRepasat;
        }

        public string obtenerIdCliJSON(string idDireccion)
        {
            string urlBase = "https://panel.repasat.com/es/webservice/accounts/list?&api_token=" + csGlobal.webServiceKey + "&per_page=50";
            using (HttpClient httpClient = new HttpClient())
            {
                string idCli = "";
                bool encontrado = false;
                int pagina = 1;

                do
                {
                    string urlWebService = $"{urlBase}&page={pagina}";

                    try
                    {
                        HttpResponseMessage response = httpClient.GetAsync(urlWebService).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            string contenidoJson = response.Content.ReadAsStringAsync().Result;
                            var data = JsonConvert.DeserializeAnonymousType(contenidoJson, new { data = new[] { new { idCli = 0, addresses = new[] { new { idDireccion = 0 } } } } });

                            foreach (var item in data.data)
                            {
                                foreach (var direccion in item.addresses)
                                {

                                    int idDirec = direccion.idDireccion;
                                    if(idDirec.ToString() == idDireccion)
                                    {
                                        idCli = item.idCli.ToString();
                                        encontrado = true;
                                        break;
                                    }
                                }
                                if (encontrado)
                                    break;  
                            }
                                pagina++;
                            }
                            else
                            {
                                Console.WriteLine($"Error al obtener el JSON desde la URL. Código de estado: {response.StatusCode}");
                                break;
                            }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        break;
                    }
                } while (!encontrado);  
                return idCli;
            }
        }


        /*Cuentas: Clientes y Proveedores
         * https://panel.repasat.com/es/webservice/accounts/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq
         * Facturas de Venta
         * https://panel.repasat.com/es/webservice/saleinvoices/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq
         * Facturas de Compra
         * https://panel.repasat.com/es/webservice/purchaseinvoices/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq
         * Artículos
         * https://panel.repasat.com/es/webservice/products/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq 
         * Contactos
         * https://panel.repasat.com/es/webservice/contacts/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq 
         * Routes
         * https://panel.repasat.com/es/webservice/routes/list?api_token=0EeYL948hSLediJuLGPcLsp8aPUFBFHq 

         * 
         * 
         * */


    }
}

