﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace klsync
{
    public partial class frReport : Form
    {
        public frReport()
        {
            InitializeComponent();
        }

        private void btnEnviarMensaje_Click(object sender, EventArgs e)
        {
            btnEnviarMensaje.Enabled = false;
            bool estado = false;

            if (MessageBox.Show("¿Estás seguro/a de querer enviar el mensaje?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                if (txtMensaje.Text != "" && txtAsunto.Text != "")
                    estado = csUtilidades.enviarMail(
                        "(ESYNC) " + txtAsunto.Text,
                        csGlobal.nombreEmpresaA3 + " te envía el siguiente mensaje: " + txtMensaje.Text,
                        new List<string>{""},
                        "dev@klosions.com",
                        "notificaciones@repasat.com",
                        "KL0S10N$",
                        "mail.repasat.com",
                        "lluisvera@klosions.com");
                else
                    MessageBox.Show("Rellena los campos para enviar el mensaje");

                if (estado)
                {
                    lblEstadoMail.ForeColor = Color.Green;
                    lblEstadoMail.Text = "El correo se ha enviado con éxito";
                }
                else
                {
                    lblEstadoMail.ForeColor = Color.Red;
                    lblEstadoMail.Text = "El correo no se ha podido enviar";
                }
            }
            else
                MessageBox.Show("Operación cancelada");

            btnEnviarMensaje.Enabled = true;

        }
    }
}
