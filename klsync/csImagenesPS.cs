﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Drawing;
using System.Resources;
using Microsoft.VisualBasic.Logging;
using System.Data;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Xml;

namespace klsync
{
    class csImagenesPS
    {
        //Lista donde guado las imagenes que se están manipulando
        List<string> imagenes = new List<string>();

        public void copiarImagenes(DataGridViewSelectedRowCollection filas)
        {
            try
            {
                csFtpTools ftpTool = new csFtpTools();
                string ficOrigen = "";
                string ficDestino = "";

                //string fichero = "";
                string nombreDestino = "";
                string ruta = "";
                string extension = "";
                string nombreImagenSinExtension = "";
                Stream myStream = null;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                bool renombrarImagen = false;

                openFileDialog1.InitialDirectory = csGlobal.rutaImagenes;
                openFileDialog1.Filter = "txt files (*.jpg)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;
                //indico que se pueden coger varios ficheros a la vez
                openFileDialog1.Multiselect = false;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            renombrarImagen = false;

                            string fichero = openFileDialog1.FileName;
                            nombreDestino = System.IO.Path.GetFileName(fichero);
                            ruta = System.IO.Path.GetDirectoryName(fichero);
                            extension = System.IO.Path.GetExtension(fichero);

                            string[] nombreImagen = nombreDestino.Split('.');
                            nombreImagenSinExtension = nombreImagen[0];
                            string[] nombreSinGuiones = nombreImagenSinExtension.Split('-');
                            nombreImagenSinExtension = nombreSinGuiones[0];
                            if (myStream != null)
                            {
                                myStream.Close();

                                foreach (DataGridViewRow fila in filas)
                                {
                                    ficOrigen = ruta + "\\" + nombreDestino;
                                    ficDestino = ruta + "\\" + fila.Cells[0].Value.ToString() + extension;

                                    System.IO.File.Copy(ficOrigen, ficDestino, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void regenerarImagenes()
        {
            csFtpTools ftpTool = new csFtpTools();

            // tamaño imagenes
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable imgSize = mysql.cargarTabla("select name, width, height from ps_image_type where ps_image_type.products=1");

            //Corrección Imagenes Dismay, 
            //Esta función sirve para añadir sólo determinados ficheros
            //DataTable imgSize = mysql.cargarTabla("select name, width, height from ps_image_type where ps_image_type.id_image_type in (264)");

            // si está vacío, estableceremos null al datatable para indicarle los tamaños
            // que nosotros queramos, si no pondremos los tamaños de la base de datos
            if (imgSize.Rows.Count == 0)
                imgSize = null;


            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //bool renombrarImagen = false;

            openFileDialog1.InitialDirectory = csGlobal.rutaImagenes;
            openFileDialog1.Filter = "txt files (*.jpg)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            //indico que se pueden coger varios ficheros a la vez
            openFileDialog1.Multiselect = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        foreach (var fichero in openFileDialog1.FileNames.OrderBy(f => f))
                        {
                            subirImagenesRegeneradasPS(fichero.ToString(), myStream, imgSize);
                        }
                    }
                }
            }
        }

        public void subirImagenes()
        {
            try
            {
                Stream myStream = null;
                OpenFileDialog ofd = new OpenFileDialog();

                ofd.InitialDirectory = csGlobal.rutaImagenes;
                ofd.Filter = "txt files (*.jpg)|*.txt|All files (*.*)|*.*";
                ofd.FilterIndex = 2;
                ofd.RestoreDirectory = true;
                ofd.Multiselect = true; // indico que se pueden coger varios ficheros a la vez

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = ofd.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            foreach (var fichero in ofd.FileNames.OrderBy(f => f))
                            {
                                subirImagenV2(fichero, Convert.ToChar(csGlobal.separadorImagen));
                            }
                        }

                        procedimientoCarpetasImagenes();
                        "Imagenes subidas con exito".mb();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.log(true);
            }
        }


        /// <summary>
        /// Reviso todas las imagenes que tengo en la lista "imagenes" y las traslado a la carpeta upload
        /// </summary>
        public void procedimientoCarpetasImagenes()
        {
            try {
                string uploadDir = Directory.GetParent(imagenes[0].ToString()).ToString() + @"\upload\";
                if (!Directory.Exists(uploadDir))
                {
                    Directory.CreateDirectory(uploadDir);
                }
                //Si no existe la carpeta upload en la ruta, dara un error y los ficheros no se moveran. 
                imagenes.ForEach(x => { if (imagenes.Count > 0) File.Move(x.ToString(), Directory.GetParent(x.ToString()).ToString() + @"\upload\" + Path.GetFileNameWithoutExtension(x.ToString()) + "-upl.jpg"); });
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
        }

        public void subirImagenesSilentMode(string codigo = "")
        {
            // Tamaño imagenes
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable imgSize = mysql.cargarTabla("select name, width, height from ps_image_type where ps_image_type.products=1");
            if (imgSize.Rows.Count == 0)
                imgSize = null;

            var filePaths = Directory.GetFiles(csGlobal.rutaImagenes, codigo + "*.jpg", SearchOption.TopDirectoryOnly).OrderBy(f => f);

            if (string.IsNullOrEmpty(codigo))
            {
                //cuando se le pase el articulo unicamente buscara las imagenes con la referencia
                filePaths = Directory.GetFiles(csGlobal.rutaImagenes, codigo + "*.jpg", SearchOption.TopDirectoryOnly).OrderBy(f => f);
            }

            foreach (var file in filePaths)
            {
                subirImagenV2(file, Convert.ToChar(csGlobal.separadorImagen));
            }
        }

        

        /// <summary>
        /// Método de subir imágenes con webservice
        /// </summary>
        /// <param name="path"></param>
        /// <param name="separador"></param>
        private void subirImagenV2(string path, char separador = '-')
        {
            try
            {
                //24/12/2020 Añadido para CadCanarias, se añade porque el certificado no era compatible con esync.
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                csSqlScripts sqlScript = new csSqlScripts();
                DataTable imagenAtributosA3 = new DataTable();
               
                csSqlConnects sqlConnect = new csSqlConnects();
                csMySqlConnect mySqlConnect=new csMySqlConnect();
                csSqlConnects sql = new csSqlConnects();
                csXMLUtilities xmlUtil = new csXMLUtilities();
                bool asignarColorImagen = false;

                string nombreDestino = System.IO.Path.GetFileName(path);    //nombre del fichero completo con extensión
                string ruta = System.IO.Path.GetDirectoryName(path);
                string extension = System.IO.Path.GetExtension(path);

                string scriptRutaDestino = "";


                string withoutExtension = Path.GetFileNameWithoutExtension(path);
                string codart_sin_guiones = withoutExtension.Split(separador)[0];
                string color = "";
                string kls_id_shop ="";
                string idImagenSubida = "";
                string imagenSubida2="";
                string idColorPS = "";
                
                int numeroParametros = withoutExtension.Split(separador).Length;

                if (numeroParametros==3)
                {
                    color = withoutExtension.Split(separador)[2];
                    imagenAtributosA3 = sqlConnect.obtenerDatosSQLScript(sqlScript.selectAtributosImagenA3(codart_sin_guiones,color));
                    asignarColorImagen = true;
                    idColorPS = imagenAtributosA3.Rows[0]["ID"].ToString();
                }

                if (csGlobal.A3ERPConnection=="N")
                {
                    kls_id_shop=mySqlConnect.obtenerDatoFromQuery("select id_product from ps_product where reference = '" + codart_sin_guiones + "'");
                }
                else
                {
                    kls_id_shop = sql.obtenerCampoTabla("select kls_id_shop from articulo where ltrim(codart) = '" + codart_sin_guiones + "'");
                }

                if (kls_id_shop != "")
                {
                    csPrestashop ps = new csPrestashop();
                    idImagenSubida = ps.uploadImage(path, kls_id_shop);

                    if (asignarColorImagen)
                    {

                        xmlUtil.buscarValorXML(idImagenSubida);
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(idImagenSubida);
                        imagenSubida2 = xmlUtil.buscarValorXML("id", xmlDoc);
                        AsignarImagenAtributos(kls_id_shop, idColorPS , imagenSubida2);
                    }

                    string dir = Directory.GetParent(path).ToString();

                    imagenes.Add(path);

                    if (!File.Exists(path + "\\upload\\" + withoutExtension + "-upl" + extension))
                    { 
                    
                        scriptRutaDestino = ruta + "\\upload\\" + withoutExtension + "-upl" + extension;


                        System.IO.File.Move(path ,scriptRutaDestino);

                    }

                }
            }
            catch (Exception ex)
            {
                if (csGlobal.modoManual)
                {
                    //MessageBox.Show(ex.Message);   
                }
            }

        }

        private void AsignarImagenAtributos(string idProduct, string idColor, string idImage)
        {
            string query="select id_attribute,ps_product_attribute.id_product_attribute,id_product " +
                        " from ps_product_attribute_combination inner join ps_product_attribute on " +
                        " ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute " +
                        " where id_attribute=" + idColor;

            string id_product_attribute="";
             
            csMySqlConnect mySqlConnect=new csMySqlConnect();
            DataTable atributosAsignarImagenPS = new DataTable();
            atributosAsignarImagenPS = mySqlConnect.obtenerDatosPS(query);

            mySqlConnect.OpenConnection();
            
            foreach (DataRow fila in atributosAsignarImagenPS.Rows)
            {
                id_product_attribute = fila["id_product_attribute"].ToString();
                mySqlConnect.ejecutarConsulta("insert into ps_product_attribute_image  ( id_product_attribute,id_image) values (" + id_product_attribute + "," + idImage + ")");
            }
            mySqlConnect.CloseConnection();

        }

        private bool checkImagen(string rutaDestino, string nombreDestino)
        {
            string[] sinExtension = nombreDestino.Split('.');
            if (File.Exists(rutaDestino + "\\" + sinExtension[0] + "-upl." + sinExtension[1]))
            {
                return true;
            }

            return false;
        }

        // oficial
        public void subirImagenPS(string fichero, Stream myStream = null, DataTable imgSize = null, char separador = '-')
        {
            csFtpTools ftpTool = new csFtpTools();
            csCheckVersion checkVersion = new csCheckVersion();
            string nombreDestino = "";
            string ruta = "";
            string extension = "";
            string nombreImagenSinExtension = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            bool renombrarImagen = false;
            renombrarImagen = false;
            csSqlConnects sql = new csSqlConnects();
            bool hasCodart = false;

            //fichero = openFileDialog1.FileName;
            nombreDestino = System.IO.Path.GetFileName(fichero);
            ruta = System.IO.Path.GetDirectoryName(fichero);
            extension = System.IO.Path.GetExtension(fichero);

            //nombreImagenSinExtension = nombreImagen[0];
            nombreImagenSinExtension = System.IO.Path.GetFileNameWithoutExtension(fichero); // nuevo metodo para lantec 2017
            string[] nombreSinGuiones = nombreImagenSinExtension.Split(separador);
            nombreImagenSinExtension = nombreSinGuiones[0];
            string imagesDirectory = Path.GetDirectoryName(fichero);
            if (sql.consultaExiste("CODART", "ARTICULO", nombreImagenSinExtension) == true)
                hasCodart = true;

            // si la imagen no existe en la carpeta upload
            if (!checkImagen(ruta + "\\upload\\", nombreDestino))
            {
                try
                {
                    csMySqlConnect mysql = new csMySqlConnect();
                    int articulo = mysql.codArticulo("ps_product", nombreImagenSinExtension);
                    if (articulo != 0) // el articulo existe en ps
                    {
                        string directorioNuevo = "";
                        //obtener ultimo id imagen
                        csMySqlConnect mySql = new csMySqlConnect();

                        // función original, se altera temporalmente (válido)
                        int idFichero = mySql.selectID("ps_image", "id_image") + 1;

                        //1-11-15 corrección regeneración imagenes Dismay
                        //La función añade una nueva imagen en el directorio
                        //int idFichero = mySql.obtenerIdImagen(nombreImagenSinExtension);

                        char[] idfic = Convert.ToString(idFichero).ToCharArray();

                        //Contruyo el path de la imagen en función del número de imagen
                        string pathDestino = "";
                        for (int cont = 0; cont < idfic.Count(); cont++)
                        {
                            pathDestino = pathDestino + idfic[cont] + "/";
                            directorioNuevo = idfic[cont] + "/";
                        }

                        //Creo el directorio si no existe
                        ftpTool.ExisteDirectorio(csGlobal.rutaFTPImagenes + pathDestino, csGlobal.userFTP, csGlobal.passwordFTP, idFichero.ToString());

                        //creo los ficheros temporales para cada tamaño
                        if (crearImagenesAux(fichero, idFichero, pathDestino, imgSize))
                        {
                            renombrarImagen = true;
                        }

                        csMySqlConnect consulta = new csMySqlConnect();

                        string portada;

                        //15-3-2017 añadimos modificación                         

                        if (consulta.comprobarSiElArticuloTienePortada(articulo))
                        {

                            //Si la versión es superior a la versión 1.6.1 cuando no es cover el valor es null
                            if (checkVersion.checkVersión("1.6.1"))
                            {
                                portada = "NULL";
                            }
                            else
                            {
                                //Si la versión es anterior a la 1.6.1 default_on no puede ser null, y su valor por defecto es 0
                                portada = "0";
                            }

                        }
                        else
                        {
                            portada = "1";
                        }

                        // Cambios entre versiones de Prestashop 1.6.0 y 1.6.1, se añaden campos
                        // el codigo de la portada (cover) se ve afectado
                        // en las tablas ps_image y ps_image_shop
                        //desabilito 1-11-2015
                        mySql.insertItems("insert into ps_image values (" + idFichero + "," + articulo + ",1," + portada + ");"); // 1 producto 1 portada
                        //mySql.insertItems("insert into ps_image values (" + idFichero + "," + articulo + ",1," + portada + ");"); // 1 producto 1 portada
                        if (hasCodart == true)
                        {
                            //15-03-2017 comentamos y verificamos la versión 1.6.1
                            //if (checkVersion.checkVersión("1.6.1.1"))
                            //{
                            //    // dismay 1.6.1.1
                            //    mySql.insertItems("insert into ps_image_shop(id_product,id_image,id_shop,cover) values (" + articulo + ", " + idFichero + ",1," + portada + ");"); // 1 producto 1 portada
                            //}
                            if (checkVersion.checkVersión("1.6.1"))
                            {
                                // ROKA 1
                                mySql.insertItems("insert into ps_image_shop (id_product,id_image,id_shop,cover) values (" + articulo + ", " + idFichero + ",1," + portada + ");"); // 1 producto 1 portada
                            }
                            else // (spacers 1.6.0.9 por ejemplo)
                            {
                                //Para versiones iguales o anteriores a la 1.6.0
                                mySql.insertItems("insert into ps_image_shop values (" + idFichero + ",1," + portada + ");"); // 1 producto 1 portada
                            }
                        }
                        csIdiomas Idiomas = new csIdiomas();
                        string[] idioma = new string[Idiomas.idiomasActivosPS().Count()];
                        idioma = Idiomas.idiomasActivosPS();
                        //INSERTO UNA IMAGEN PARA CADA CÓDIGO DE IDIOMA
                        foreach (string codIdioma in idioma)
                        {
                            mySql.insertItems("insert into ps_image_lang (id_image, id_lang) values (" + idFichero + "," + codIdioma + ")");
                        }

                        //mySql.insertItems("insert into ps_image_shop values (" + idFichero + ",1," + portada + ");"); // indicar las imagenes de la tienda
                    }
                }

                finally
                {
                    if (myStream != null)
                        myStream.Close();

                    if (renombrarImagen)
                    {
                        if (!File.Exists(ruta + "\\upload\\" + System.IO.Path.GetFileNameWithoutExtension(fichero) + "-upl" + System.IO.Path.GetExtension(fichero)))
                        {
                            System.IO.File.Move(ruta + "\\" + nombreDestino, ruta + "\\upload\\" + System.IO.Path.GetFileNameWithoutExtension(fichero) + "-upl" + System.IO.Path.GetExtension(fichero));
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("La imagen " + fichero + " ya está subida");
                if (!Directory.Exists(ruta + "\\dup\\"))
                {
                    Directory.CreateDirectory(ruta + "\\dup\\");
                }

                if (myStream != null)
                    myStream.Close();
                if (!File.Exists(ruta + "\\dup\\" + nombreImagenSinExtension + "-dup" + System.IO.Path.GetExtension(fichero)))
                {
                    //System.IO.File.Move(ruta + "\\" + nombreDestino, ruta + "\\dup\\" + nombreImagenSinExtension + "-dup" + extension);
                }
            }
        }
        /*
            ImageFactory imgFac = new ImageFactory(csGlobal.APIUrl, csGlobal.webServiceKey, "");
            imgFac.AddProductImage(articulo, fichero);
            File.Move(fichero, imagesDirectory + @"\Upload\" + nombreImagenSinExtension + "-upl" + extension);
            }
            catch (Exception ex)
            {
                File.Move(fichero, imagesDirectory + @"\disabled\" + nombreImagenSinExtension + "-dis" + extension);
            }          
         */

        public void subirImagenesRegeneradasPS(string fichero, Stream myStream = null, DataTable imgSize = null)
        {
            csFtpTools ftpTool = new csFtpTools();
            csCheckVersion checkVersion = new csCheckVersion();
            string nombreDestino = "";
            string ruta = "";
            string extension = "";
            string nombreImagenSinExtension = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            bool renombrarImagen = false;
            renombrarImagen = false;

            //fichero = openFileDialog1.FileName;
            nombreDestino = System.IO.Path.GetFileName(fichero);
            ruta = System.IO.Path.GetDirectoryName(fichero);
            extension = System.IO.Path.GetExtension(fichero);

            // si la imagen no existe en la carpeta upload
            if (!checkImagen(ruta + "\\upload\\", nombreDestino))
            {
                string[] nombreImagen = nombreDestino.Split('.');
                nombreImagenSinExtension = nombreImagen[0];
                string[] nombreSinGuiones = nombreImagenSinExtension.Split('-');
                nombreImagenSinExtension = nombreSinGuiones[0];
                try
                {
                    csMySqlConnect mysql = new csMySqlConnect();
                    int articulo = mysql.codArticulo("ps_product", nombreImagenSinExtension);
                    if (articulo != 0)
                    {
                        string directorioNuevo = "";
                        //obtener ultimo id imagen
                        csMySqlConnect mySql = new csMySqlConnect();


                        // función original, se altera temporalmente (válido)
                        //int idFichero = mySql.selectID("ps_image", "id_image") + 1;

                        //Obtengo el id de la imagen subida asignada a ese artículo
                        int idImagenAsignada = mySql.obtenerIdImagen(nombreImagenSinExtension, true);

                        //1-11-15 corrección regeneración imagenes Dismay
                        //La función añade una nueva imagen en el directorio
                        //int idFichero = mySql.obtenerIdImagen(nombreImagenSinExtension);

                        char[] idfic = Convert.ToString(idImagenAsignada).ToCharArray();

                        //Contruyo el path de la imagen en función del número de imagen
                        string pathDestino = "";
                        for (int cont = 0; cont < idfic.Count(); cont++)
                        {
                            pathDestino = pathDestino + idfic[cont] + "/";
                            directorioNuevo = idfic[cont] + "/";
                        }

                        //Creo el directorio si no existe
                        ftpTool.ExisteDirectorio(csGlobal.rutaFTPImagenes + pathDestino, csGlobal.userFTP, csGlobal.passwordFTP, idImagenAsignada.ToString());

                        //creo los ficheros temporales para cada tamaño
                        if (crearImagenesAux(fichero, idImagenAsignada, pathDestino, imgSize))
                        {
                            renombrarImagen = true;
                        }

                        csMySqlConnect consulta = new csMySqlConnect();
                        int portada;

                        if (consulta.comprobarSiElArticuloTienePortada(articulo))
                        {
                            portada = 0; // el articulo ya tiene portada por lo tanto se pone 0
                        }
                        else
                        {
                            portada = 1;
                        }


                        // Cambios entre versiones de Prestashop 1.6.0 y 1.6.1, se añaden campos
                        // el codigo de la portada (cover) se ve afectado
                        // en las tablas ps_image y ps_image_shop
                        //desabilito 1-11-2015
                        mySql.insertItems("insert into ps_image values (" + idImagenAsignada + "," + articulo + ",1," + portada + ");"); // 1 producto 1 portada

                        if (checkVersion.checkVersión("1.6.1"))
                        {
                            //Para versiones iguales o superiores a la 1.6.1
                            mySql.insertItems("insert into ps_image_shop values (" + idImagenAsignada + ",1," + portada + "," + articulo + ");"); // 1 producto 1 portada
                        }
                        else
                        {
                            //Para versiones iguales o anteriores a la 1.6.0
                            mySql.insertItems("insert into ps_image_shop values (" + idImagenAsignada + ",1," + portada + ");"); // 1 producto 1 portada
                        }




                        csIdiomas Idiomas = new csIdiomas();
                        string[] idioma = new string[Idiomas.idiomasActivosPS().Count()];
                        idioma = Idiomas.idiomasActivosPS();
                        //INSERTO UNA IMAGEN PARA CADA CÓDIGO DE IDIOMA
                        foreach (string codIdioma in idioma)
                        {
                            mySql.insertItems("insert into ps_image_lang (id_image, id_lang) values (" + idImagenAsignada + "," + codIdioma + ")");
                        }

                        //mySql.insertItems("insert into ps_image_shop values (" + idImagenAsignada + ",1," + portada + ");"); // indicar las imagenes de la tienda
                    }
                }

                finally
                {
                    if (myStream != null)
                        myStream.Close();

                    if (renombrarImagen)
                    {
                        if (!File.Exists(ruta + "\\upload\\" + nombreImagen[0] + "-upl" + extension))
                        {
                            System.IO.File.Move(ruta + "\\" + nombreDestino, ruta + "\\upload\\" + nombreImagen[0] + "-upl" + extension);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("La imagen ya está subida");
            }
            if (!checkImagen(ruta + "\\upload\\", nombreDestino))
            {
                string[] nombreImagen = nombreDestino.Split('.');
                nombreImagenSinExtension = nombreImagen[0];
                string[] nombreSinGuiones = nombreImagenSinExtension.Split('-');
                nombreImagenSinExtension = nombreSinGuiones[0];
                try
                {
                    csMySqlConnect mysql = new csMySqlConnect();
                    int articulo = mysql.codArticulo("ps_product", nombreImagenSinExtension);
                    if (articulo != 0)
                    {
                        string directorioNuevo = "";
                        //obtener ultimo id imagen
                        csMySqlConnect mySql = new csMySqlConnect();
                        int idFichero = mySql.selectID("ps_image", "id_image") + 1;
                        char[] idfic = Convert.ToString(idFichero).ToCharArray();

                        //Contruyo el path de la imagen en función del número de imagen
                        string pathDestino = "";
                        for (int cont = 0; cont < idfic.Count(); cont++)
                        {
                            pathDestino = pathDestino + idfic[cont] + "/";
                            directorioNuevo = idfic[cont] + "/";
                        }

                        //Creo el directorio si no existe
                        ftpTool.ExisteDirectorio(csGlobal.rutaFTPImagenes + pathDestino, csGlobal.userFTP, csGlobal.passwordFTP);

                        //creo los ficheros temporales para cada tamaño
                        if (crearImagenesAux(fichero, idFichero, pathDestino, imgSize))
                        {
                            renombrarImagen = true;
                        }

                        csMySqlConnect consulta = new csMySqlConnect();
                        int portada;

                        if (consulta.comprobarSiElArticuloTienePortada(articulo))
                        {
                            portada = 0; // el articulo ya tiene portada por lo tanto se pone 0
                        }
                        else
                        {
                            portada = 1;
                        }

                        // el codigo de la portada (cover) se ve afectado
                        // en las tablas ps_image y ps_image_shop
                        mySql.insertItems("insert into ps_image values (" + idFichero + "," + articulo + ",1," + portada + ");"); // 1 producto 1 portada
                        mySql.insertItems("insert into ps_image_shop values (" + idFichero + ",1," + portada + ");"); // 1 producto 1 portada

                        csIdiomas Idiomas = new csIdiomas();
                        string[] idioma = new string[Idiomas.idiomasActivosPS().Count()];
                        idioma = Idiomas.idiomasActivosPS();
                        //INSERTO UNA IMAGEN PARA CADA CÓDIGO DE IDIOMA
                        foreach (string codIdioma in idioma)
                        {
                            mySql.insertItems("insert into ps_image_lang (id_image, id_lang) values (" + idFichero + "," + codIdioma + ")");
                        }

                        //mySql.insertItems("insert into ps_image_shop values (" + idFichero + ",1," + portada + ");"); // indicar las imagenes de la tienda
                    }
                }

                finally
                {
                    if (myStream != null)
                        myStream.Close();


                    /* Si se han creado bien en el ftp los ficheros,
                     * moveremos la imagen a la carpeta upload
                     * Comprobamos si la imagen ya existe en la carpeta upload
                     * Y si no está, lo movemos
                     * */
                    if (renombrarImagen)
                    {
                        if (!File.Exists(ruta + "\\upload\\" + nombreImagen[0] + "-upl" + extension))
                        {
                            System.IO.File.Move(ruta + "\\" + nombreDestino, ruta + "\\upload\\" + nombreImagen[0] + "-upl" + extension);
                        }
                    }
                    else // si no se han creado bien en el ftp, se mueven a la carpeta disabled
                    {
                        if (!File.Exists(ruta + "\\disabled\\" + nombreImagen[0] + "-dis" + extension))
                        {
                            System.IO.File.Move(ruta + "\\" + nombreDestino, ruta + "\\disabled\\" + nombreImagen[0] + "-dis" + extension);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            if (image.Width != image.Height)
            {
                csPrestaTools.PadImage(image);
            }

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private bool crearImagenesAux(string fichero, int idFichero, string pathDestino, DataTable imgSize = null)
        {
            bool imagenesCreadas = true;
            Image newImage = Image.FromFile(fichero);
            string nombreImagen = "";

            try
            {
                //Guardo la imagen original
                Upload(csGlobal.nombreServidor, csGlobal.userFTP, csGlobal.passwordFTP, fichero, csGlobal.rutaRemotaImagenes + pathDestino, idFichero.ToString() + ".jpg");

                //string rutaImagenes = csGlobal.rutaImagenes + "\\temp\\" + idFichero;
                //string rutaImagenes = "C:\\Users\\LLUIS\\Pictures\\Copilot\\Temp\\" + idFichero;  //Ruta donde se graban las imágenes auxiliares
                string rutaImagenes = csGlobal.rutaImagenes + "\\temp\\" + idFichero;  //Ruta donde se graban las imágenes auxiliares
                string directorioFTP = csGlobal.nombreServidor;
                string directorioFTPImagenes = csGlobal.rutaRemotaImagenes;
                string usuarioftp = csGlobal.userFTP;
                string passftp = csGlobal.passwordFTP;
                int alto = 0;
                int ancho = 0;
                Image finalImage;
                csPrestaTools prestaTool = new csPrestaTools();

                if (imgSize != null)
                {
                    finalImage = prestaTool.CambiarTamanoImagen(newImage, 600, 600);
                    finalImage.Save(rutaImagenes + ".jpg");

                    // recorremos el dataset y ponemos los tamaños
                    // correspondientes de la base de datos de mysql
                    foreach (DataRow row in imgSize.Rows)
                    {
                        alto = Convert.ToInt32(row["height"].ToString());
                        ancho = Convert.ToInt32(row["width"].ToString());

                        nombreImagen = row["name"].ToString();
                        //finalImage = prestaTool.CambiarTamanoImagen(newImage, alto, ancho);
                        //Se altera el orden de ancho y alto por funcionalidad para que quede bien
                        finalImage = prestaTool.CambiarTamanoImagen(newImage, ancho, alto);
                        finalImage.Save(rutaImagenes + "-" + nombreImagen + ".jpg");
                    }

                    //subo la imagen base
                    Upload(directorioFTP, usuarioftp, passftp, csGlobal.rutaImagenes + "\\temp\\" + idFichero + ".jpg", directorioFTPImagenes + pathDestino, idFichero.ToString() + ".jpg");
                    //Subo el resto de imagenes

                    foreach (DataRow row in imgSize.Rows)
                    {
                        nombreImagen = row["name"].ToString();
                        Upload(directorioFTP, usuarioftp, passftp, csGlobal.rutaImagenes + "\\temp\\" + idFichero + "-" + nombreImagen + ".jpg", directorioFTPImagenes + pathDestino, idFichero.ToString() + "-" + nombreImagen + ".jpg");
                    }
                }
            }
            catch (Exception ex)
            {
                imagenesCreadas = false;
                //Log l = new Log();
                //l.WriteEntry(ex.ToString());
                // MessageBox.Show(ex.ToString());
            }

            finally
            {
                if (newImage != null)
                {

                    newImage.Dispose();
                }
            }
            return imagenesCreadas;
        }

        private void Upload2(string server, string user, string pass, string origen, string rutadestino, string nombredestino)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Credentials = new NetworkCredential(user, pass);
                    client.UploadFile(server + rutadestino + nombredestino, "STOR", origen);
                }
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }

        }

        private void Upload(string server, string user, string pass, string origen, string rutadestino, string nombredestino)
        {
            bool check = true;
            string rutaDestinoFTP = "";
            int contador = 0;
            FileStream fileStream = File.OpenRead(origen);
            while (contador <= 25 && check != false)
            {
                try
                {
                    rutaDestinoFTP = server + rutadestino + nombredestino;
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(rutaDestinoFTP);

                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.Credentials = new NetworkCredential(user, pass);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;
                    //FileStream stream = File.OpenRead(origen);

                    byte[] buffer = new byte[fileStream.Length];
                    fileStream.Read(buffer, 0, buffer.Length);
                    fileStream.Close();
                    request.Timeout = 30000; // 2 minutos
                    Stream reqStream = request.GetRequestStream();
                    reqStream.Write(buffer, 0, buffer.Length);
                    reqStream.Flush();
                    reqStream.Close();
                    check = true;
                    contador++;
                }
                catch (Exception ex)
                {
                    check = false;
                }
                finally
                {
                    if (fileStream != null)
                    {
                        fileStream.Close();
                    }
                }
            }

        }

        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 30 * 60 * 1000;
                return w;
            }
        }
    }
}
