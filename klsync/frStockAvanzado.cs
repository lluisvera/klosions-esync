﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace klsync
{
    public partial class frStockAvanzado : Form
    {
        private static frStockAvanzado m_FormDefInstance;
        public static frStockAvanzado DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frStockAvanzado();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frStockAvanzado()
        {
            InitializeComponent();
        }


        public void actualizar_stock_tyc()
        {
            try
            {
                csTallasYColoresV2 stock = new csTallasYColoresV2();
                csMySqlConnect mysql = new csMySqlConnect();
                csSqlConnects sql = new csSqlConnects();
                string almacen = "'" + csGlobal.almacenA3.Replace(",", "','") + "'";
                string consulta_id_products = "select id_product from ps_stock_available where ps_stock_available.id_product_attribute > 0 group by id_product";
                string consulta_a3 = "SELECT dbo.STOCKALM.CODALM, dbo.STOCKALM.CODART, dbo.STOCKALM.CODFAMTALLAH, dbo.STOCKALM.CODFAMTALLAV, dbo.STOCKALM.CODTALLAH, dbo.STOCKALM.CODTALLAV, dbo.STOCKALM.UNIDADES, TALLAS_1.ID AS idAtributo1, dbo.TALLAS.ID AS idAtributo2, dbo.ARTICULO.KLS_ID_SHOP FROM dbo.STOCKALM INNER JOIN dbo.ARTICULO ON dbo.STOCKALM.CODART = dbo.ARTICULO.CODART LEFT OUTER JOIN dbo.TALLAS ON dbo.STOCKALM.CODFAMTALLAV = dbo.TALLAS.CODFAMTALLA AND dbo.STOCKALM.CODTALLAV = dbo.TALLAS.CODTALLA LEFT OUTER JOIN dbo.TALLAS AS TALLAS_1 ON dbo.STOCKALM.CODFAMTALLAH = TALLAS_1.CODFAMTALLA AND dbo.STOCKALM.CODTALLAH = TALLAS_1.CODTALLA WHERE (dbo.ARTICULO.KLS_ID_SHOP > 0) AND LTRIM(STOCKALM.CODALM) in (" + almacen + ")";//   AND (STOCKALM.CODFAMTALLAH is not null OR STOCKALM.CODFAMTALLAV is not null)";
                string id_product_attribute = "";
                string atr1 = "", atr2 = "", prod = "";
                DataTable id_products = mysql.cargarTabla(consulta_id_products);
                DataTable a3 = sql.cargarDatosTablaA3(consulta_a3);

                if (id_products.Rows.Count > 0 && a3.Rows.Count > 0)
                {
                    // Iteramos los productos con combinaciones
                    foreach (DataRow id_product in id_products.Rows)
                    {
                        prod = id_product["id_product"].ToString();
                        string consulta_ps = "select id_product, ps_product_attribute.id_product_attribute, count(ps_product_attribute.id_product_attribute) AS atributos, max(id_attribute) AS idAtributo1, min(id_attribute) AS idAtributo2 FROM ps_product_attribute_combination INNER JOIN ps_product_attribute ON ps_product_attribute.id_product_attribute=ps_product_attribute_combination.id_product_attribute where id_product = " + prod + " GROUP BY ps_product_attribute.id_product_attribute";
                        DataTable ps = mysql.cargarTabla(consulta_ps);

                        // Iteramos las combinaciones de ese producto
                        foreach (DataRow rowPS in ps.Rows)
                        {
                            atr1 = rowPS["idAtributo1"].ToString();
                            atr2 = rowPS["idAtributo2"].ToString();
                            id_product_attribute = rowPS["id_product_attribute"].ToString();
                            if (stock.existeCombinacionPSDT(prod, atr1, atr2, a3))
                            {
                                // Filtramos las combinaciones de a3 en base al producto y los atributos
                                DataRow[] filteredRowsA =
                                    a3.Select(string.Format("{0} = {1} AND {2} = {3} AND {4} = {5}",
                                    "KLS_ID_SHOP", prod,
                                    "idAtributo1", atr1,
                                    "idAtributo2", atr2));

                                DataRow[] filteredRowsB =
                                    a3.Select(string.Format("{0} = {1} AND {2} = {3} AND {4} = {5}",
                                    "KLS_ID_SHOP", prod,
                                    "idAtributo1", atr2,
                                    "idAtributo2", atr1));

                                if (filteredRowsA.Length > 0)
                                {
                                    updateStock(filteredRowsA, prod, id_product_attribute, a3);
                                }
                                else if (filteredRowsB.Length > 0)
                                {
                                    updateStock(filteredRowsB, prod, id_product_attribute, a3);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Aqui la idea es actualizar el stock available - HEBO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            actualizar_stock_tyc();
        }

        private void updateStock(DataRow[] items, string id_product, string id_product_attribute, DataTable a3) 
        {
            foreach(DataRow item in items)
            {
                int unidades = Convert.ToInt32(item[6].ToString());
                int idprod = Convert.ToInt32(id_product);
                int idprodattr = Convert.ToInt32(id_product_attribute);
                string consulta_update = "update ps_stock_available set quantity = " + unidades + " where id_product = " + idprod + " and id_product_attribute = " + idprodattr;
                csUtilidades.ejecutarConsulta(consulta_update, true);

                // Actualizar el total // esto no se toca que ya funciona bien
                string idPS = Convert.ToString(id_product);
                IEnumerable collection = a3.AsEnumerable().Where(data => Convert.ToString(data.Field<int>("KLS_ID_SHOP")).Trim() == Convert.ToInt32(idPS).ToString());
                Double suma = (from DataRow dr in a3.Rows
                               where (string)dr["KLS_ID_SHOP"].ToString() == Convert.ToString(id_product)
                               select (double)dr["UNIDADES"]).AsEnumerable().Sum();
                consulta_update = "update ps_stock_available set quantity = " + Convert.ToInt32(suma) + " where id_product = " +id_product + " and id_product_attribute = 0";
                csUtilidades.ejecutarConsulta(consulta_update, true);    
            }
        }
    }
}
