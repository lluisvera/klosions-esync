﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace klsync
{
    public partial class frCodBarr : Form
    {

        private static frCodBarr m_FormDefInstance;
        public static frCodBarr DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCodBarr();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }


        public frCodBarr()
        {
            InitializeComponent();
        }

        private void frCodBarr_Load(object sender, EventArgs e)
        {
            cargarArticulos();
        }

        private void cargarArticulos()
        {


            lblMensaje.Text = "";
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = CadenaConexion();
            dataConnection.Open();

            string consulta = "SELECT DISTINCT LTRIM(CODART) AS CODIGO, DESCART AS DESCRIPCION, CODFAMTALLAV AS V,  CODFAMTALLAH AS H " +
                               "FROM ARTICULO ";

            csSqlConnects sqlConnect = new csSqlConnects();

            cargarDGV(dgvArticulos, sqlConnect.obtenerDatosSQLScript(consulta));
        }



        private void cargarCodigosDeBarras(string codart)
        {
            lblMensaje.Text = "";
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = CadenaConexion();
            dataConnection.Open();

            string consulta = "SELECT CODART, LTRIM(CODALT) AS CODIGO_BARRAS, LTRIM(CODTALLAH) AS TALLA, LTRIM(CODTALLAV) AS COLOR FROM ALTERNA WHERE LTRIM(CODART)='" + codart + "'";

            csSqlConnects sqlConnect = new csSqlConnects();

            cargarDGV(dgvCodigosBarra, sqlConnect.obtenerDatosSQLScript(consulta));
            if (dgvCodigosBarra.Rows.Count > 0)
            {
                lblMensaje.Text = "EL ARTÍCULO " + codart.ToUpper() + " TIENE " + dgvCodigosBarra.Rows.Count.ToString() + " CÓDIGOS DE BARRAS ASOCIADOS";

            }
            else
            {
                lblMensaje.Text = "ESTE ARTÍCULO NO TIENE CÓDIGOS DE BARRA ASOCIADOS";
            }

        }

        private void cargarDetalleAtributos(string familia, DataGridView dgv)
        {
            string consulta = "";
            SqlConnection dataConnection = new SqlConnection();
            dataConnection.ConnectionString = CadenaConexion();
            dataConnection.Open();

            

            csSqlConnects sqlConnect = new csSqlConnects();

            consulta = "select CODTALLA as CODIGO from tallas WHERE LTRIM(CODFAMTALLA)='" + familia + "'";
            cargarDGV(dgv, sqlConnect.obtenerDatosSQLScript(consulta));
        }






        public string CadenaConexion()
        {
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder();
            csb.DataSource = csGlobal.ServerA3;
            csb.InitialCatalog = csGlobal.databaseA3;
            csb.IntegratedSecurity = csGlobal.integratedSecurity;
            csb.UserID = csGlobal.userA3;
            csb.Password = csGlobal.passwordA3;
            return csb.ConnectionString;
        }

        private void cargarDGV(DataGridView dgv, DataTable dt)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke(new MethodInvoker(() => dgv.DataSource = dt));
            else
                dgv.DataSource = dt;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                csSqlConnects sql = new csSqlConnects();
                string search = Microsoft.VisualBasic.Interaction.InputBox("", "Búsqueda", "");
                if (search != "")
                {
                    buscarLupa(search);
                }
                else
                {
                    buscarLupa("");
                }

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void buscarLupa(string search)
        {
            string whereQuery = "";
            if (search != "")
            { 
                whereQuery = " WHERE CODART LIKE '%" + search + "%' OR DESCART LIKE '%" + search + "%'";
            }



            string consulta = "SELECT DISTINCT LTRIM(CODART) AS CODIGO, DESCART AS DESCRIPCION, CODFAMTALLAV AS V,  CODFAMTALLAH AS H " +
                              "FROM ARTICULO " + whereQuery;


            csSqlConnects sqlConnect = new csSqlConnects();

            cargarDGV(dgvArticulos, sqlConnect.obtenerDatosSQLScript(consulta));
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            string search = Microsoft.VisualBasic.Interaction.InputBox("", "Búsqueda", "");

            buscarLupa(search);
        }

        private void btLoadAll_Click(object sender, EventArgs e)
        {
            cargarArticulos();
        }

        private void dgvArticulos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            cargarAtributos();
            actualizarDatosCodigosBarra();
            
        }

        private void actualizarDatosCodigosBarra()
        {
            string codart = dgvArticulos.SelectedRows[0].Cells["CODIGO"].Value.ToString().Trim();
            cargarCodigosDeBarras(codart);
            calcularUltimoCodigoBarras();
            tbPais.Text = tbLastBarcode.Text.Substring(0, 2);
            tbEmpresa.Text = tbLastBarcode.Text.Substring(2, 5);
            tbContador.Text = tbLastBarcode.Text.Substring(7, 5);
        
        }


        private void calcularUltimoCodigoBarras()
        {
            csSqlConnects sqlConnect = new csSqlConnects();
            tbLastBarcode.Text = sqlConnect.obtenerCampoTabla("select max(CONVERT(bigint, CODALT)) from alterna ");

        }


        private void crearCodigoBarras()
        {
            string codart = dgvArticulos.SelectedRows[0].Cells["CODIGO"].Value.ToString();
            string famTalla = dgvArticulos.SelectedRows[0].Cells["H"].Value.ToString();
            string famColor = dgvArticulos.SelectedRows[0].Cells["V"].Value.ToString();
            string color = "";
            string talla = "";
            string codigoBarras="";
            string query = "Insert into Alterna  (AUTOMATICO, CODART, CODALT, CODFAMTALLAH, CODFAMTALLAV, CODTALLAH, CODTALLAV) values ";
            string values = "";
            int codigoBarrasBase = Convert.ToInt32(tbContador.Text);
            codigoBarrasBase++;
            int combinaciones = 0;

            if ((famTalla!="") && (famColor!=""))
            {
                combinaciones=2;

                foreach (DataGridViewRow filaTalla in dgvTallas.SelectedRows)
                {
                    talla = filaTalla.Cells["CODIGO"].Value.ToString();

                    foreach (DataGridViewRow filaColor in dgvColores.SelectedRows)
                    {
                        //Añadimos 2 espacios porque el campo alterna es de 15 dígitos
                        codigoBarras = CalculateChecksumDigit(tbPais.Text + tbEmpresa.Text + codigoBarrasBase);
                        color = filaColor.Cells["CODIGO"].Value.ToString();
                        values = " ('F','" + codart + "','  " + tbPais.Text + tbEmpresa.Text + codigoBarrasBase + codigoBarras + "','" + famTalla + "','" + famColor + "','" + talla + "','" + color + "') ";

                        csUtilidades.ejecutarConsulta(query + values, false);
                        codigoBarrasBase++;
                    }
                }




            }
                //Solo hay Tallas
            else if ((famTalla!="") & (famColor==""))
            {
                combinaciones=1;

                foreach (DataGridViewRow filaTalla in dgvTallas.SelectedRows)
                {
                    talla = filaTalla.Cells["CODIGO"].Value.ToString();

                        codigoBarras = CalculateChecksumDigit(tbPais.Text + tbEmpresa.Text + codigoBarrasBase);
                        //color = NULL;
                        values = " ('F','" + codart + "','  " + tbPais.Text + tbEmpresa.Text + codigoBarrasBase + codigoBarras + "','" + famTalla + "',NULL,'" + talla + "',NULL) ";

                        csUtilidades.ejecutarConsulta(query + values, false);
                        codigoBarrasBase++;
                }
            }

                //Sólo hay colores
            else if ((famTalla=="" && famColor!=""))
            {
                combinaciones=1;

                    foreach (DataGridViewRow filaColor in dgvColores.SelectedRows)
                    {
                        codigoBarras = CalculateChecksumDigit(tbPais.Text + tbEmpresa.Text + codigoBarrasBase);
                        color = filaColor.Cells["CODIGO"].Value.ToString();
                        values = " ('F','" + codart + "','  " + tbPais.Text + tbEmpresa.Text + codigoBarrasBase + codigoBarras + "',NULL,'" + famColor + "',NULL,'" + color + "') ";

                        csUtilidades.ejecutarConsulta(query + values, false);
                        codigoBarrasBase++;
                    }
            }
            else if ((famTalla == "" && famColor == ""))
            {

                combinaciones = 0;

               
                    codigoBarras = CalculateChecksumDigit(tbPais.Text + tbEmpresa.Text + codigoBarrasBase);
                    values = " ('F','" + codart + "','  " + tbPais.Text + tbEmpresa.Text + codigoBarrasBase + codigoBarras + "',NULL,NULL,NULL,NULL) ";

                    csUtilidades.ejecutarConsulta(query + values, false);
                    codigoBarrasBase++;

            
            }



          

            actualizarDatosCodigosBarra();
            cargarCodigosDeBarras(codart.Trim());
        
        
        }

        private void cargarAtributos()
        {
            string codart = dgvArticulos.SelectedRows[0].Cells["CODIGO"].Value.ToString().Trim();
            string famTalla = dgvArticulos.SelectedRows[0].Cells["V"].Value.ToString().Trim();
            string famColor = dgvArticulos.SelectedRows[0].Cells["H"].Value.ToString().Trim();

            cargarDetalleAtributos(famTalla, dgvColores); 
            cargarDetalleAtributos(famColor, dgvTallas);
        
        }

        private void btCreateCodBarr_Click(object sender, EventArgs e)
        {

            crearCodigoBarras();
            //string barrcode12 = tbPais.Text + tbEmpresa.Text + tbLastBarcode.Text;

            //tbBarrcodeGen.Text = CalculateChecksumDigit(barrcode12).ToString();
        }

        public string CalculateChecksumDigit(string data)
        {
            string sTemp =data;
            int iSum = 0;
            int iDigit = 0;

            // Calculate the checksum digit here.
            for (int i = sTemp.Length; i >= 1; i--)
            {
                iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                // This appears to be backwards but the 
                // EAN-13 checksum must be calculated
                // this way to be compatible with UPC-A.
                if (i % 2 == 0)
                { // odd  
                    iSum += iDigit * 3;
                }
                else
                { // even
                    iSum += iDigit * 1;
                }
            }
            int iCheckSum = (10 - (iSum % 10)) % 10;
            //this.ChecksumDigit = iCheckSum.ToString();
            return iCheckSum.ToString();
        }

        private void borrarCódigoDeBarrasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            borrarCodigosBarras();
            dgvCodigosBarra.Refresh();
        }

        private void borrarCodigosBarras()
        {
            string codbarr = "";
            string codart = "";
            string query = "";

            DialogResult dialogo = MessageBox.Show("¿Desea borrar los Códigos de Barras Seleccionados? \n Este proceso no es reversible", "Borrar Códigos de Barras", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dialogo == DialogResult.Yes)
            {
                foreach (DataGridViewRow fila in dgvCodigosBarra.SelectedRows)
                {
                    codbarr = fila.Cells["CODIGO_BARRAS"].Value.ToString();
                    codart = fila.Cells["CODART"].Value.ToString();
                    query = "DELETE FROM ALTERNA WHERE LTRIM(CODART)='" + codart + "' AND LTRIM(CODALT)='" + codbarr + "'";
                    csUtilidades.ejecutarConsulta(query, false);
                }
            }
            cargarCodigosDeBarras(codart);
        
        }

        private void btCrearCodBarr_Click(object sender, EventArgs e)
        {
            crearCodigoBarras();
        }

    }
}
