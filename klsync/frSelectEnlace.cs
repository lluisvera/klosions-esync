﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Xml;

namespace klsync
{
    public partial class frSelectEnlace : Form
    {

        Utilidades.csConexiones dbConnectEsync = new Utilidades.csConexiones();

        private static frSelectEnlace m_FormDefInstance;
        public static frSelectEnlace DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frSelectEnlace();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frSelectEnlace()
        {
            InitializeComponent();
        }

        private void cargarEnlacesMysql(DataTable dt)
        {
            string[] enlaces = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                enlaces[i] = dt.Rows[i][0].ToString();
            }
            cargarEnlaces(enlaces);
        }

        private void frSelectEnlace_Load(object sender, EventArgs e)
        {
            csXMLConfig xmlConfig = new csXMLConfig();
            cargarEnlaces(xmlConfig.cargarConexionesXML());
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btCargarEnlace_Click(object sender, EventArgs e)
        {
            string enlace = csGlobal.conexionDB;
            csGlobal.conexionDB = cbEnlace.Text;
            csXMLConfig xml = new csXMLConfig();
            xml.cargarConfiguracionConexionesXML();
            csSqlConnects conectorSQL = new csSqlConnects();
            conectorSQL.cargarIdiomas();



            //si han cambiado la conexión cierro todas las ventanas abiertas
            //Defino una lista donde guardaré los formularios abiertos
            List<Form> openForms = new List<Form>();

            if (csGlobal.conexionDB != enlace)
            {
                foreach (Form formulario in Application.OpenForms)
                {
                    openForms.Add(formulario);
                }
                foreach (Form formToclose in openForms)
                {
                    if (formToclose.Name != "frMainForm")
                    {
                        formToclose.Close();
                    }
                    else
                    {
                        //csTitulos titulos = new csTitulos();
                        //formToclose.Text = titulos.tituloSincronizarDocs();
                    }
                }
            }


            this.Close();
        }

        public void ChangeSize(int width, int height)
        {
            this.Size = new Size(width, height);
            CenterToParent();
        }

        private void cargarEnlaces(string[] enlaces)
        {
            try
            {
                //borrar todos los controles
                if (flowLayoutPanel1 != null)
                {
                    List<Control> listControls = flowLayoutPanel1.Controls.Cast<Control>().ToList();

                    foreach (Control control in listControls)
                    {
                        flowLayoutPanel1.Controls.Remove(control);
                        control.Dispose();
                    }

                    listControls.Clear();
                }

                int enlacesCount = enlaces.Count();
                if (enlaces.Count() < 9)
                    enlacesCount = 9;
                int ancho = (int)Math.Ceiling((double)enlacesCount / (double)4) * 115;
                int filas = (int)Math.Ceiling((double)enlacesCount / (double)10);
                ChangeSize(ancho, 200*filas);
                //ChangeSize(ancho, 365);

                // Ordena los nodos 25-09-2017
                Array.Sort(enlaces, StringComparer.InvariantCulture);

                for (int i = 0; i < enlaces.Count(); i++)
                {
                    Button boton = new Button();
                    boton.BackColor = Color.Transparent;

                    boton.AllowDrop = true;
                    boton.MouseMove += OnPictureMouseMove;
                    boton.DragDrop += OnPictureDragDrop;
                    boton.DragEnter += OnPictureDragEnter;

                    boton.Click += (s, e) =>
                    {

                       

                        if (csGlobal.modeDebug) csUtilidades.log("HA HECHO CLICK EN EL ENLACE");
                        //Cambio el cursos para indicar que estoy trabajando
                        Cursor.Current = Cursors.WaitCursor;

                        csGlobal.conexionDB = boton.Text;
                        string enlace = csGlobal.conexionDB;

                        if (csGlobal.modeDebug) csUtilidades.log("INICIO CARGA CONEXION XML");

                        csXMLConfig xml = new csXMLConfig();

                        xml.cargarConfiguracionConexionesXML();

                        if (csGlobal.modeDebug) csUtilidades.log("CARGA DE IDIOMAS");
                        csSqlConnects conectorSQL = new csSqlConnects();
                        if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 1");
                        conectorSQL.cargarIdiomas();
                        if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 2");
                        //Si han cambiado la conexión cierro todas las ventanas abiertas
                        //Defino una lista donde guardaré los formularios abiertos

                        if (!comprobarExpirationDate())
                        {
                            if (csGlobal.conexionDB.Contains("PUNTES"))
                            {
                                
                            }
                            if (MessageBox.Show(
                                "No podemos conectarnos con la base de datos. \n\nPor favor, contacta con tu distribuidor", "Klosions", MessageBoxButtons.OK, MessageBoxIcon.Asterisk
                            ) == DialogResult.OK)
                            {
                                List<string> ccs = new List<string>();
                                ccs.Add("dev@klosions.com");
                                System.Diagnostics.Process.Start("http://www.esync.es");
                                csUtilidades.enviarMail("(" + csGlobal.conexionDB + ") Fin de fecha de expiración",
                                    "El cliente " + csGlobal.conexionDB + " no puede conectar por fin de fecha de expiración",
                                    ccs,
                                    "",
                                    "lluisvera@klosions.com",
                                    "notificaciones@repasat.com",
                                    "KL0S10N$",
                                    "mail.repasat.com",
                                    false,
                                    false);
                            }

                            csUtilidades.salirDelPrograma();
                        }
                        if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 3");
                        if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 3.Valor: CSG.Cdb="+csGlobal.conexionDB+" enlace="+enlace);
                        if (csGlobal.conexionDB != enlace)
                        {
                            if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 3.1");
                            List<Form> openForms = new List<Form>();

                            foreach (Form formulario in Application.OpenForms)
                            {
                                openForms.Add(formulario);
                            }
                            foreach (Form formToclose in openForms)
                            {
                                if (formToclose.Name != "frMainForm")
                                {
                                    formToclose.Close();
                                }
                                else
                                {
                                    //csTitulos titulos = new csTitulos();
                                    //formToclose.Text = titulos.tituloSincronizarDocs();
                                }
                            }
                        }

                        this.Close();
                    };
                    if (csGlobal.modeDebug) csUtilidades.log("SelectEnlace 4");
                    //boton.BorderStyle = BorderStyle.FixedSingle;
                    boton.Size = new Size(100, 100);
                    //boton.BackColor = Color.FromArgb(i * 20, 0, 0);

                    //Color aleatorio

                    Random Aleatorios = new Random();
                    int Red = Aleatorios.Next(250);
                    int Blue = Aleatorios.Next(250);
                    int Green = Aleatorios.Next(250);
                    int alpha = 100;
                    //Color colorBorde = Color.FromArgb(alpha, Red, Green, Blue);
                    Color colorBorde = Color.FromArgb(alpha, 0, 82, 204);

                    Random randomGen = new Random();
                    KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
                    KnownColor randomColorName = names[randomGen.Next(names.Length)];
                    Color randomColor = Color.FromKnownColor(randomColorName);

                    //Formateo el botón
                    boton.FlatAppearance.BorderSize = 1;
                    boton.FlatStyle = FlatStyle.Flat;
                    //boton.FlatAppearance.BorderColor = randomColor;
                    boton.FlatAppearance.BorderColor = Color.FromArgb(alpha, 0, 82, 204);
                    boton.Text = enlaces[i].ToString().ToUpper();

                    this.flowLayoutPanel1.Controls.Add(boton);
                    randomColor = Color.White;
                }

                this.flowLayoutPanel1.Height = this.flowLayoutPanel1.Controls[this.flowLayoutPanel1.Controls.Count - 1].Bottom + 5;
            }
            catch (Exception ex)
            {
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        //Verificar si se ha marcado una fecha de expiración en Esync para la empresa
        //private bool comprobarExpirationDate()
        //{
        //    using (MySqlConnection mcon = new MySqlConnection(dbConnectEsync.cadenaConexionEsyncDB()))
        //    using (MySqlCommand cmd = mcon.CreateCommand())
        //    {
        //        mcon.Open();
        //        cmd.CommandText = string.Format("SELECT * FROM config where connection_name = '{0}'", csGlobal.conexionDB);
        //        DataTable dt = new DataTable();
        //        dt.Load(cmd.ExecuteReader());

        //        if (dt.hasRows())
        //        {
        //            DateTime expiration_date = Convert.ToDateTime(dt.Rows[0]["expiration_date"].ToString());

        //            if (expiration_date < DateTime.Now)
        //            {
        //                return false;
        //            }
        //        }
        //    }

        //    return true;
        //}
        private bool comprobarExpirationDate()
        {
            using (MySqlConnection mcon = new MySqlConnection(dbConnectEsync.cadenaConexionEsyncDB()))
            using (MySqlCommand cmd = mcon.CreateCommand())
            {
                mcon.Open();
                cmd.CommandText = string.Format("SELECT * FROM config where connection_name = '{0}'", csGlobal.conexionDB);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                DateTime expiration_date = new DateTime();
                if (dt.hasRows())
                {
                    string fecha = dt.Rows[0]["expiration_date"].ToString();
                    try
                    {
                        expiration_date = Convert.ToDateTime(fecha);
                    }
                    catch
                    {
                        string formatoEsperado = "MM/dd/yyyy";
                        expiration_date = csUtilidades.TransformarFormatoInvalido(fecha, formatoEsperado);

                    }
                    if (expiration_date < DateTime.Now)
                    {
                        return false;
                    }
                }
            }

            return true;
        }


        private void OnPictureMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Button boton = sender as Button;

                //boton.BorderStyle = BorderStyle.Fixed3D;
                boton.DoDragDrop(boton, DragDropEffects.Move);

            }
        }

        private void OnPictureDragDrop(object sender, DragEventArgs e)
        {
            Button target = sender as Button;
            if (target != null)
            {
                int targetIndex = FindButtonIndex(target);
                if (targetIndex != -1)
                {
                    string pictureBoxFormat = typeof(Button).FullName;
                    if (e.Data.GetDataPresent(pictureBoxFormat))
                    {
                        Button source = e.Data.GetData(pictureBoxFormat) as Button;

                        int sourceIndex = this.FindButtonIndex(source);

                        if (targetIndex != -1)
                            this.flowLayoutPanel1.Controls.SetChildIndex(source, targetIndex);
                    }
                }
            }
        }

        private void OnPictureDragEnter(object sender, DragEventArgs e)
        {
            //e.Effect = DragDropEffects.Move;
        }

        private int FindButtonIndex(Button boton)
        {
            for (int i = 0; i < this.flowLayoutPanel1.Controls.Count; i++)
            {
                Button target = this.flowLayoutPanel1.Controls[i] as Button;

                if (boton == target)
                    return i;
            }
            return -1;
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}