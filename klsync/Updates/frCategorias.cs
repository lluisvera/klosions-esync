﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync.Updates
{
    public partial class frCategorias : Form
    {
        csMySqlConnect mySql = new csMySqlConnect();
        DataTable dtFinal = new DataTable();
        DataTable dtProduct = new DataTable();

        csSqlConnects sql = new csSqlConnects();
        SqlConnection conn = new SqlConnection(csGlobal.cadenaConexion);

        private static frCategorias m_FormDefInstance;
        public static frCategorias DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frCategorias();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frCategorias()
        {
            InitializeComponent();
            cargaCategorias();
            //cargaA3(dtProduct);

        }
        private void cargaCategorias()
        {
            //DataTable dtCat = new DataTable();
            DataTable dtLevels = new DataTable();

            int cont = 0;

            //dtCat = mySql.obtenerDatosPS("SELECT * FROM ps_category;");
            string idIdioma = mySql.obtenerDatoFromQuery("SELECT id_lang FROM ps_lang WHERE iso_code='es'");
            dtLevels = mySql.obtenerDatosPS("SELECT level_depth FROM ps_category GROUP BY level_depth;");
            string query = "SELECT product.id_product AS IdProducto, reference as Referencia, nomProd.name AS NomProd,product.id_manufacturer AS IdMarca, " +
                                                "marca.name AS NomMarca, cat1.id_category AS IdHijo, nomHijo.name AS NomHijo, " +
                                                "cat1.id_parent AS IdPadre, nomPadre.name AS nomPadre, cat2.id_parent AS IdAbuelo, nomAbu.name AS nomAbu " +
                                                "FROM ps_product product " +

                                                "INNER JOIN ps_product_lang nomProd " +
                                                "ON product.id_product = nomProd.id_product " +

                                                "LEFT JOIN ps_category cat1 " +
                                                "ON cat1.id_category = product.id_category_default " +

                                                "LEFT JOIN ps_category cat2 " +
                                                "ON cat1.id_parent = cat2.id_category " +

                                                "LEFT JOIN ps_manufacturer marca " +
                                                "ON product.id_manufacturer = marca.id_manufacturer " +

                                                "LEFT JOIN ps_category_lang nomHijo " +
                                                "ON nomHijo.id_category = cat1.id_category " +

                                                "LEFT JOIN ps_category_lang nomPadre " +
                                                "ON nomPadre.id_category = cat2.id_category " +

                                                "LEFT JOIN ps_category_lang nomAbu " +
                                                "ON nomAbu.id_category = cat2.id_parent " +

                                                "WHERE (nomAbu.id_lang=" + idIdioma + " OR nomAbu.id_lang IS NULL)" +
                                                "AND(nomPadre.id_lang=" + idIdioma + " OR nomPadre.id_lang IS NULL)" +
                                                "AND nomHijo.id_lang= " + idIdioma + " AND nomProd.id_lang = " + idIdioma;
            dtProduct = mySql.obtenerDatosPS(query);
            //dtFinal = mySql.obtenerDatosPS("SELECT id_category, id_parent as PADRE FROM ps_category;");
            if (dgvCategorias.Rows.Count > 0)
            {
                this.dgvCategorias.DataSource = null;
                dgvCategorias.Rows.Clear();
            }


            dgvCategorias.DataSource = dtProduct;
        }

        public void actMarcas(string codart, string idmarca)
        {
           
            try
            {
                string update = "UPDATE ARTICULO SET KLS_CODMARCA='" + idmarca + "' WHERE LTRIM(CODART)='" + codart + "'";
                sql.actualizarCampo(update, false);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            

        }
        public void actCategorias(string codart, string idHijo, int contador)
        {
            try
            {
                string del = "DELETE FROM KLS_CATEGORIAS_ART WHERE LTRIM(CODART)='" + codart + "'";
                string insertH = "INSERT INTO KLS_CATEGORIAS_ART(CODART, KLS_CODCATEGORIA, KLS_PRINCIPAL) VALUES ";
                //valueFinal += value + ((contador == 500 || contador == last) ? "" : "('" + codart + "','" + idHijo + "',null),");
                contador++;
                //sql.borrarDatosTabla
                csUtilidades.ejecutarConsulta(del, false);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }


        }

        private void Actualizar_Click(object sender, EventArgs e)
        {
            int contador = 0; 
            conn.Open();
            sql.abrirConexion();

            for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
            {
                int s = checkedListBox1.CheckedIndices[i];
                string insertH = "INSERT INTO KLS_CATEGORIAS_ART(CODART, KLS_CODCATEGORIA, KLS_PRINCIPAL) VALUES ";
                string valueFinal = "";
                int filas = dtProduct.Rows.Count;
                foreach (DataRow fila in dtProduct.Rows)
                {
                    string idProduct = fila["IdProducto"].ToString();
                    string idMarca = fila["IdMarca"].ToString();

                    string idHijo = fila["IdHijo"].ToString();
                    string idPadre = fila["IdPadre"].ToString();
                    string idAbuelo = fila["IdAbuelo"].ToString();

                    string codart = fila["Referencia"].ToString();


                    switch (s)
                    {
                        case 0:
                            //string del = "DELETE FROM KLS_CATEGORIAS_ART WHERE LTRIM(CODART)='" + codart + "'";

                            valueFinal += "('" + codart + "','" + idHijo + "',null)" + ",";
                            contador++;
                            //Se van restando las filas para evitar que queden excluidos cuando queden menos de 500.
                            filas--;
                            // Cuando las filas sean menores a 500, el bucle seguira concatenando los insert hasta que la fila llegue a 0
                            // Cuando la fila es 0, entra en el if y ejecuta el insert con las filas restantes
                            if (contador == 500 || filas == 0)
                            {
                                valueFinal = valueFinal.TrimEnd(',');
                                string insert = insertH + valueFinal;
                                
                                contador = 0;
                                //csUtilidades.ejecutarConsulta(insert, false);
                                insert = "";
                                valueFinal = "";
                            }

                            //actCategorias(codart, idHijo,contador);
                            break;
                        case 1:
                            actMarcas(codart, idMarca);
                            break;
                          
                    }
                }



            }
            MessageBox.Show("Se ha terminado la actualización.");
            conn.Close();
            sql.cerrarConexion();
        }
    }
}
