﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync
{
    public partial class frReminderPayment : Form
    {

        private static frReminderPayment m_FormDefInstance;
        public static frReminderPayment DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frReminderPayment();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frReminderPayment()
        {
            InitializeComponent();
        }

        private void frReminderPayment_Load(object sender, EventArgs e)
        {
            tbDaysAfter.Text = csGlobal.reminderDaysAfter.ToString();
            tbDaysBefore.Text = csGlobal.reminderDaysBefore.ToString();
            tbEmailsEmpresa.Text = csGlobal.emailNotifyEmpresa.ToString();
            rbutYes.Checked = csGlobal.remindSendToCustomer ? true : false;
            rbutNo.Checked = !csGlobal.remindSendToCustomer ? true : false;
        }

        private void btnSendReminder_Click(object sender, EventArgs e)
        {
            sendReminder();
        }

        public void sendReminder()
        {
            PaymentReminder.csPaymentReminder carteraReminder = new PaymentReminder.csPaymentReminder();
            carteraReminder.obtenerListaEfectos(false);
            carteraReminder.obtenerListaEfectos(true);

        }

        private void btSave_Click(object sender, EventArgs e)
        {
            string anexoEnvioCliente = " send_to_customer= '" + (rbutNo.Checked ? "N" : "Y") + "',";
            string anexoDiasAntes = " days_before_filter= " + (tbDaysBefore.Text) + ", ";
            string anexoDiasDespues = " days_after_filter= " + (tbDaysAfter.Text) + ", ";
            string anexoEmailsEmpresa = " email_notify_empresa= '" + (tbEmailsEmpresa.Text) + "' ";

            csUtilidades.modificarConfiguracionEsync("update reminder set " + anexoEnvioCliente + anexoDiasAntes + anexoDiasDespues + anexoEmailsEmpresa + " where connection_name='" + csGlobal.conexionDB + "'");
            "Datos Actualizados".mb();
        }
    }
}
