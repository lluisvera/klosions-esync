﻿namespace klsync
{
    partial class frPedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.contextDGVPedidos = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportarAFicheroXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servirAAlbaránToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesoAlbaranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.abrirCarpetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPedidos = new System.Windows.Forms.Button();
            this.rbtAlbaranes = new System.Windows.Forms.RadioButton();
            this.rbtPedidos = new System.Windows.Forms.RadioButton();
            this.cboxDocs = new System.Windows.Forms.ComboBox();
            this.btFilter = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.contextDGVPedidos.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidos.ContextMenuStrip = this.contextDGVPedidos;
            this.dgvPedidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPedidos.Location = new System.Drawing.Point(3, 164);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.ReadOnly = true;
            this.dgvPedidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPedidos.Size = new System.Drawing.Size(1072, 490);
            this.dgvPedidos.TabIndex = 0;
            // 
            // contextDGVPedidos
            // 
            this.contextDGVPedidos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportarAFicheroXMLToolStripMenuItem,
            this.servirAAlbaránToolStripMenuItem,
            this.procesoAlbaranToolStripMenuItem,
            this.toolStripMenuItem1,
            this.abrirCarpetaToolStripMenuItem});
            this.contextDGVPedidos.Name = "contextDGVPedidos";
            this.contextDGVPedidos.Size = new System.Drawing.Size(194, 98);
            // 
            // exportarAFicheroXMLToolStripMenuItem
            // 
            this.exportarAFicheroXMLToolStripMenuItem.Name = "exportarAFicheroXMLToolStripMenuItem";
            this.exportarAFicheroXMLToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.exportarAFicheroXMLToolStripMenuItem.Text = "Exportar a fichero XML";
            this.exportarAFicheroXMLToolStripMenuItem.Click += new System.EventHandler(this.exportarAFicheroXMLToolStripMenuItem_Click);
            // 
            // servirAAlbaránToolStripMenuItem
            // 
            this.servirAAlbaránToolStripMenuItem.Name = "servirAAlbaránToolStripMenuItem";
            this.servirAAlbaránToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.servirAAlbaránToolStripMenuItem.Text = "Servir a Albarán";
            this.servirAAlbaránToolStripMenuItem.Click += new System.EventHandler(this.servirAAlbaránToolStripMenuItem_Click);
            // 
            // procesoAlbaranToolStripMenuItem
            // 
            this.procesoAlbaranToolStripMenuItem.Enabled = false;
            this.procesoAlbaranToolStripMenuItem.Name = "procesoAlbaranToolStripMenuItem";
            this.procesoAlbaranToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.procesoAlbaranToolStripMenuItem.Text = "Generar desde albaran";
            this.procesoAlbaranToolStripMenuItem.Click += new System.EventHandler(this.procesoAlbaranToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(190, 6);
            // 
            // abrirCarpetaToolStripMenuItem
            // 
            this.abrirCarpetaToolStripMenuItem.Name = "abrirCarpetaToolStripMenuItem";
            this.abrirCarpetaToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.abrirCarpetaToolStripMenuItem.Text = "Abrir carpeta";
            this.abrirCarpetaToolStripMenuItem.Click += new System.EventHandler(this.abrirCarpetaToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dgvPedidos, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.65753F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.34247F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1078, 657);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 155);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.089552F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.91045F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1072, 155);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnPedidos);
            this.panel2.Controls.Add(this.rbtAlbaranes);
            this.panel2.Controls.Add(this.rbtPedidos);
            this.panel2.Controls.Add(this.cboxDocs);
            this.panel2.Controls.Add(this.btFilter);
            this.panel2.Controls.Add(this.tableLayoutPanel3);
            this.panel2.Location = new System.Drawing.Point(78, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(661, 149);
            this.panel2.TabIndex = 2;
            // 
            // btnPedidos
            // 
            this.btnPedidos.Location = new System.Drawing.Point(354, 27);
            this.btnPedidos.Name = "btnPedidos";
            this.btnPedidos.Size = new System.Drawing.Size(158, 79);
            this.btnPedidos.TabIndex = 5;
            this.btnPedidos.Text = "Cargar Documentos";
            this.btnPedidos.UseVisualStyleBackColor = true;
            this.btnPedidos.Click += new System.EventHandler(this.btnPedidos_Click);
            // 
            // rbtAlbaranes
            // 
            this.rbtAlbaranes.AutoSize = true;
            this.rbtAlbaranes.Checked = true;
            this.rbtAlbaranes.Location = new System.Drawing.Point(97, 3);
            this.rbtAlbaranes.Name = "rbtAlbaranes";
            this.rbtAlbaranes.Size = new System.Drawing.Size(72, 17);
            this.rbtAlbaranes.TabIndex = 4;
            this.rbtAlbaranes.TabStop = true;
            this.rbtAlbaranes.Text = "Albaranes";
            this.rbtAlbaranes.UseVisualStyleBackColor = true;
            this.rbtAlbaranes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rbtAlbaranes_MouseClick);
            // 
            // rbtPedidos
            // 
            this.rbtPedidos.AutoSize = true;
            this.rbtPedidos.Location = new System.Drawing.Point(9, 3);
            this.rbtPedidos.Name = "rbtPedidos";
            this.rbtPedidos.Size = new System.Drawing.Size(63, 17);
            this.rbtPedidos.TabIndex = 3;
            this.rbtPedidos.Text = "Pedidos";
            this.rbtPedidos.UseVisualStyleBackColor = true;
            this.rbtPedidos.Click += new System.EventHandler(this.rbtPedidos_Click);
            // 
            // cboxDocs
            // 
            this.cboxDocs.FormattingEnabled = true;
            this.cboxDocs.Location = new System.Drawing.Point(39, 124);
            this.cboxDocs.Name = "cboxDocs";
            this.cboxDocs.Size = new System.Drawing.Size(121, 21);
            this.cboxDocs.TabIndex = 2;
            this.cboxDocs.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cboxDocs_MouseDown);
            // 
            // btFilter
            // 
            this.btFilter.Location = new System.Drawing.Point(191, 124);
            this.btFilter.Name = "btFilter";
            this.btFilter.Size = new System.Drawing.Size(122, 23);
            this.btFilter.TabIndex = 1;
            this.btFilter.Text = "Filtrar por Documento";
            this.btFilter.UseVisualStyleBackColor = true;
            this.btFilter.Click += new System.EventHandler(this.btFilter_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 255F));
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.dtpHasta, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.dtpDesde, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(313, 79);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hasta";
            // 
            // dtpHasta
            // 
            this.dtpHasta.Location = new System.Drawing.Point(61, 42);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(249, 20);
            this.dtpHasta.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "De";
            // 
            // dtpDesde
            // 
            this.dtpDesde.Location = new System.Drawing.Point(61, 3);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(249, 20);
            this.dtpDesde.TabIndex = 3;
            // 
            // frPedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 657);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frPedidos";
            this.Text = "frPedidos";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.contextDGVPedidos.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip contextDGVPedidos;
        private System.Windows.Forms.ToolStripMenuItem exportarAFicheroXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servirAAlbaránToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirCarpetaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesoAlbaranToolStripMenuItem;
        private System.Windows.Forms.Button btnPedidos;
        private System.Windows.Forms.RadioButton rbtAlbaranes;
        private System.Windows.Forms.RadioButton rbtPedidos;
        private System.Windows.Forms.ComboBox cboxDocs;
        private System.Windows.Forms.Button btFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDesde;
    }
}