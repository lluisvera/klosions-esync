﻿namespace klsync
{
    partial class frIncidencias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvDetalleA3 = new System.Windows.Forms.DataGridView();
            this.cmsDetalleA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ponerANULLLosProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvA3 = new System.Windows.Forms.DataGridView();
            this.contextMenuStripA3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aCCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.arreglarIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarElDePSAA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arreglarVisibleEnTiendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activarEnTiendaEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activarEnTiendaEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.desactivarEnTiendaEnA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desactivarEnTiendaEnPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arreglarDescripcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarDePSAA3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarDeA3APSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoactualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsA3 = new System.Windows.Forms.ToolStrip();
            this.btnUpdateA3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnIDRepetidosA3 = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvPS = new System.Windows.Forms.DataGridView();
            this.dgvPSReferenciasDetalle = new System.Windows.Forms.DataGridView();
            this.cmsReferenciaDetalle = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.eliminarProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsPS = new System.Windows.Forms.ToolStrip();
            this.tsbtnActualizarPS = new System.Windows.Forms.ToolStripButton();
            this.btnReferenciasRepetidasPS = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleA3)).BeginInit();
            this.cmsDetalleA3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).BeginInit();
            this.contextMenuStripA3.SuspendLayout();
            this.tsA3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSReferenciasDetalle)).BeginInit();
            this.cmsReferenciaDetalle.SuspendLayout();
            this.tsPS.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(866, 494);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvDetalleA3);
            this.tabPage1.Controls.Add(this.dgvA3);
            this.tabPage1.Controls.Add(this.tsA3);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(858, 461);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "A3";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvDetalleA3
            // 
            this.dgvDetalleA3.AllowUserToAddRows = false;
            this.dgvDetalleA3.AllowUserToDeleteRows = false;
            this.dgvDetalleA3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDetalleA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDetalleA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalleA3.ContextMenuStrip = this.cmsDetalleA3;
            this.dgvDetalleA3.Location = new System.Drawing.Point(435, 65);
            this.dgvDetalleA3.Name = "dgvDetalleA3";
            this.dgvDetalleA3.ReadOnly = true;
            this.dgvDetalleA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalleA3.Size = new System.Drawing.Size(417, 390);
            this.dgvDetalleA3.TabIndex = 2;
            this.dgvDetalleA3.Visible = false;
            // 
            // cmsDetalleA3
            // 
            this.cmsDetalleA3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsDetalleA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripSeparator2,
            this.ponerANULLLosProductosToolStripMenuItem});
            this.cmsDetalleA3.Name = "contextMenuStripA3";
            this.cmsDetalleA3.Size = new System.Drawing.Size(274, 62);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Enabled = false;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(273, 26);
            this.toolStripMenuItem4.Text = "ACCIONES";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(270, 6);
            // 
            // ponerANULLLosProductosToolStripMenuItem
            // 
            this.ponerANULLLosProductosToolStripMenuItem.Name = "ponerANULLLosProductosToolStripMenuItem";
            this.ponerANULLLosProductosToolStripMenuItem.Size = new System.Drawing.Size(273, 26);
            this.ponerANULLLosProductosToolStripMenuItem.Text = "Poner a NULL los productos";
            this.ponerANULLLosProductosToolStripMenuItem.Click += new System.EventHandler(this.ponerANULLLosProductosToolStripMenuItem_Click);
            // 
            // dgvA3
            // 
            this.dgvA3.AllowUserToAddRows = false;
            this.dgvA3.AllowUserToDeleteRows = false;
            this.dgvA3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvA3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvA3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvA3.ContextMenuStrip = this.contextMenuStripA3;
            this.dgvA3.Location = new System.Drawing.Point(6, 65);
            this.dgvA3.Name = "dgvA3";
            this.dgvA3.ReadOnly = true;
            this.dgvA3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvA3.Size = new System.Drawing.Size(846, 390);
            this.dgvA3.TabIndex = 1;
            this.dgvA3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvA3_CellClick);
            // 
            // contextMenuStripA3
            // 
            this.contextMenuStripA3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.contextMenuStripA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCCIONESToolStripMenuItem,
            this.toolStripMenuItem1,
            this.arreglarIDToolStripMenuItem,
            this.arreglarVisibleEnTiendaToolStripMenuItem,
            this.arreglarDescripcionesToolStripMenuItem,
            this.autoactualizarToolStripMenuItem});
            this.contextMenuStripA3.Name = "contextMenuStripA3";
            this.contextMenuStripA3.Size = new System.Drawing.Size(332, 140);
            // 
            // aCCIONESToolStripMenuItem
            // 
            this.aCCIONESToolStripMenuItem.Enabled = false;
            this.aCCIONESToolStripMenuItem.Name = "aCCIONESToolStripMenuItem";
            this.aCCIONESToolStripMenuItem.Size = new System.Drawing.Size(331, 26);
            this.aCCIONESToolStripMenuItem.Text = "ACCIONES";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(328, 6);
            // 
            // arreglarIDToolStripMenuItem
            // 
            this.arreglarIDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarElDePSAA3ToolStripMenuItem});
            this.arreglarIDToolStripMenuItem.Name = "arreglarIDToolStripMenuItem";
            this.arreglarIDToolStripMenuItem.Size = new System.Drawing.Size(331, 26);
            this.arreglarIDToolStripMenuItem.Text = "Arreglar ID";
            // 
            // copiarElDePSAA3ToolStripMenuItem
            // 
            this.copiarElDePSAA3ToolStripMenuItem.Name = "copiarElDePSAA3ToolStripMenuItem";
            this.copiarElDePSAA3ToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.copiarElDePSAA3ToolStripMenuItem.Text = "Copiar de PS a A3";
            this.copiarElDePSAA3ToolStripMenuItem.Click += new System.EventHandler(this.copiarElDePSAA3ToolStripMenuItem_Click);
            // 
            // arreglarVisibleEnTiendaToolStripMenuItem
            // 
            this.arreglarVisibleEnTiendaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.activarEnTiendaEnA3ToolStripMenuItem,
            this.activarEnTiendaEnPSToolStripMenuItem,
            this.toolStripMenuItem3,
            this.desactivarEnTiendaEnA3ToolStripMenuItem,
            this.desactivarEnTiendaEnPSToolStripMenuItem});
            this.arreglarVisibleEnTiendaToolStripMenuItem.Name = "arreglarVisibleEnTiendaToolStripMenuItem";
            this.arreglarVisibleEnTiendaToolStripMenuItem.Size = new System.Drawing.Size(331, 26);
            this.arreglarVisibleEnTiendaToolStripMenuItem.Text = "Arreglar Visible en tienda";
            // 
            // activarEnTiendaEnA3ToolStripMenuItem
            // 
            this.activarEnTiendaEnA3ToolStripMenuItem.Name = "activarEnTiendaEnA3ToolStripMenuItem";
            this.activarEnTiendaEnA3ToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.activarEnTiendaEnA3ToolStripMenuItem.Text = "Activar en tienda en A3";
            this.activarEnTiendaEnA3ToolStripMenuItem.Click += new System.EventHandler(this.activarEnTiendaEnA3ToolStripMenuItem_Click);
            // 
            // activarEnTiendaEnPSToolStripMenuItem
            // 
            this.activarEnTiendaEnPSToolStripMenuItem.Name = "activarEnTiendaEnPSToolStripMenuItem";
            this.activarEnTiendaEnPSToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.activarEnTiendaEnPSToolStripMenuItem.Text = "Activar en tienda en PS";
            this.activarEnTiendaEnPSToolStripMenuItem.Click += new System.EventHandler(this.activarEnTiendaEnPSToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(261, 6);
            // 
            // desactivarEnTiendaEnA3ToolStripMenuItem
            // 
            this.desactivarEnTiendaEnA3ToolStripMenuItem.Name = "desactivarEnTiendaEnA3ToolStripMenuItem";
            this.desactivarEnTiendaEnA3ToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.desactivarEnTiendaEnA3ToolStripMenuItem.Text = "Desactivar en tienda en A3";
            this.desactivarEnTiendaEnA3ToolStripMenuItem.Click += new System.EventHandler(this.desactivarEnTiendaEnA3ToolStripMenuItem_Click);
            // 
            // desactivarEnTiendaEnPSToolStripMenuItem
            // 
            this.desactivarEnTiendaEnPSToolStripMenuItem.Name = "desactivarEnTiendaEnPSToolStripMenuItem";
            this.desactivarEnTiendaEnPSToolStripMenuItem.Size = new System.Drawing.Size(264, 26);
            this.desactivarEnTiendaEnPSToolStripMenuItem.Text = "Desactivar en tienda en PS";
            this.desactivarEnTiendaEnPSToolStripMenuItem.Click += new System.EventHandler(this.desactivarEnTiendaEnPSToolStripMenuItem_Click);
            // 
            // arreglarDescripcionesToolStripMenuItem
            // 
            this.arreglarDescripcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarDePSAA3ToolStripMenuItem,
            this.copiarDeA3APSToolStripMenuItem});
            this.arreglarDescripcionesToolStripMenuItem.Name = "arreglarDescripcionesToolStripMenuItem";
            this.arreglarDescripcionesToolStripMenuItem.Size = new System.Drawing.Size(331, 26);
            this.arreglarDescripcionesToolStripMenuItem.Text = "Arreglar descripciones";
            // 
            // copiarDePSAA3ToolStripMenuItem
            // 
            this.copiarDePSAA3ToolStripMenuItem.Name = "copiarDePSAA3ToolStripMenuItem";
            this.copiarDePSAA3ToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.copiarDePSAA3ToolStripMenuItem.Text = "Copiar de PS a A3";
            this.copiarDePSAA3ToolStripMenuItem.Click += new System.EventHandler(this.copiarDePSAA3ToolStripMenuItem_Click);
            // 
            // copiarDeA3APSToolStripMenuItem
            // 
            this.copiarDeA3APSToolStripMenuItem.Name = "copiarDeA3APSToolStripMenuItem";
            this.copiarDeA3APSToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.copiarDeA3APSToolStripMenuItem.Text = "Copiar de A3 a PS";
            this.copiarDeA3APSToolStripMenuItem.Click += new System.EventHandler(this.copiarDeA3APSToolStripMenuItem_Click);
            // 
            // autoactualizarToolStripMenuItem
            // 
            this.autoactualizarToolStripMenuItem.Checked = true;
            this.autoactualizarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoactualizarToolStripMenuItem.Name = "autoactualizarToolStripMenuItem";
            this.autoactualizarToolStripMenuItem.Size = new System.Drawing.Size(331, 26);
            this.autoactualizarToolStripMenuItem.Text = "Auto-actualizar después de la acción";
            this.autoactualizarToolStripMenuItem.Click += new System.EventHandler(this.autoactualizarToolStripMenuItem_Click);
            // 
            // tsA3
            // 
            this.tsA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsA3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsA3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUpdateA3,
            this.toolStripLabel1,
            this.tsbtnIDRepetidosA3});
            this.tsA3.Location = new System.Drawing.Point(3, 3);
            this.tsA3.Name = "tsA3";
            this.tsA3.Size = new System.Drawing.Size(852, 59);
            this.tsA3.TabIndex = 0;
            this.tsA3.Text = "toolStrip1";
            // 
            // btnUpdateA3
            // 
            this.btnUpdateA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateA3.Image = global::klsync.Properties.Resources.syncA3ERP;
            this.btnUpdateA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnUpdateA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpdateA3.Name = "btnUpdateA3";
            this.btnUpdateA3.Size = new System.Drawing.Size(83, 56);
            this.btnUpdateA3.Text = "Actualizar";
            this.btnUpdateA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnUpdateA3.Click += new System.EventHandler(this.btnUpdateA3_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(492, 56);
            this.toolStripLabel1.Text = "Las acciones (clic derecho) serán sobre los productos seleccionados";
            // 
            // tsbtnIDRepetidosA3
            // 
            this.tsbtnIDRepetidosA3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbtnIDRepetidosA3.Image = global::klsync.Properties.Resources.Tienda;
            this.tsbtnIDRepetidosA3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnIDRepetidosA3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnIDRepetidosA3.Name = "tsbtnIDRepetidosA3";
            this.tsbtnIDRepetidosA3.Size = new System.Drawing.Size(100, 56);
            this.tsbtnIDRepetidosA3.Text = "ID repetidos";
            this.tsbtnIDRepetidosA3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnIDRepetidosA3.Click += new System.EventHandler(this.tsbtnIDRepetidosA3_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Controls.Add(this.tsPS);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(858, 461);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(6, 65);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvPS);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvPSReferenciasDetalle);
            this.splitContainer1.Size = new System.Drawing.Size(846, 390);
            this.splitContainer1.SplitterDistance = 413;
            this.splitContainer1.TabIndex = 3;
            // 
            // dgvPS
            // 
            this.dgvPS.AllowUserToAddRows = false;
            this.dgvPS.AllowUserToDeleteRows = false;
            this.dgvPS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPS.Location = new System.Drawing.Point(0, 0);
            this.dgvPS.MultiSelect = false;
            this.dgvPS.Name = "dgvPS";
            this.dgvPS.ReadOnly = true;
            this.dgvPS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPS.Size = new System.Drawing.Size(413, 390);
            this.dgvPS.TabIndex = 2;
            this.dgvPS.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPS_CellClick);
            // 
            // dgvPSReferenciasDetalle
            // 
            this.dgvPSReferenciasDetalle.AllowUserToAddRows = false;
            this.dgvPSReferenciasDetalle.AllowUserToDeleteRows = false;
            this.dgvPSReferenciasDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSReferenciasDetalle.ContextMenuStrip = this.cmsReferenciaDetalle;
            this.dgvPSReferenciasDetalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPSReferenciasDetalle.Location = new System.Drawing.Point(0, 0);
            this.dgvPSReferenciasDetalle.Name = "dgvPSReferenciasDetalle";
            this.dgvPSReferenciasDetalle.ReadOnly = true;
            this.dgvPSReferenciasDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPSReferenciasDetalle.Size = new System.Drawing.Size(429, 390);
            this.dgvPSReferenciasDetalle.TabIndex = 3;
            // 
            // cmsReferenciaDetalle
            // 
            this.cmsReferenciaDetalle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmsReferenciaDetalle.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripSeparator1,
            this.eliminarProductosToolStripMenuItem});
            this.cmsReferenciaDetalle.Name = "contextMenuStripA3";
            this.cmsReferenciaDetalle.Size = new System.Drawing.Size(212, 62);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(211, 26);
            this.toolStripMenuItem2.Text = "ACCIONES";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(208, 6);
            // 
            // eliminarProductosToolStripMenuItem
            // 
            this.eliminarProductosToolStripMenuItem.Name = "eliminarProductosToolStripMenuItem";
            this.eliminarProductosToolStripMenuItem.Size = new System.Drawing.Size(211, 26);
            this.eliminarProductosToolStripMenuItem.Text = "Eliminar productos";
            this.eliminarProductosToolStripMenuItem.Click += new System.EventHandler(this.eliminarProductosToolStripMenuItem_Click);
            // 
            // tsPS
            // 
            this.tsPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsPS.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsPS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnActualizarPS,
            this.btnReferenciasRepetidasPS,
            this.toolStripLabel2});
            this.tsPS.Location = new System.Drawing.Point(3, 3);
            this.tsPS.Name = "tsPS";
            this.tsPS.Size = new System.Drawing.Size(852, 59);
            this.tsPS.TabIndex = 1;
            this.tsPS.Text = "toolStrip2";
            // 
            // tsbtnActualizarPS
            // 
            this.tsbtnActualizarPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbtnActualizarPS.Image = global::klsync.Properties.Resources.syncA3ERP;
            this.tsbtnActualizarPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnActualizarPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnActualizarPS.Name = "tsbtnActualizarPS";
            this.tsbtnActualizarPS.Size = new System.Drawing.Size(83, 56);
            this.tsbtnActualizarPS.Text = "Actualizar";
            this.tsbtnActualizarPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnActualizarPS.Visible = false;
            // 
            // btnReferenciasRepetidasPS
            // 
            this.btnReferenciasRepetidasPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReferenciasRepetidasPS.Image = global::klsync.Properties.Resources.Tienda;
            this.btnReferenciasRepetidasPS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnReferenciasRepetidasPS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReferenciasRepetidasPS.Name = "btnReferenciasRepetidasPS";
            this.btnReferenciasRepetidasPS.Size = new System.Drawing.Size(169, 56);
            this.btnReferenciasRepetidasPS.Text = "Referencias repetidas";
            this.btnReferenciasRepetidasPS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReferenciasRepetidasPS.Click += new System.EventHandler(this.btnUpdatePS_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(492, 56);
            this.toolStripLabel2.Text = "Las acciones (clic derecho) serán sobre los productos seleccionados";
            // 
            // frIncidencias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 518);
            this.Controls.Add(this.tabControl1);
            this.Name = "frIncidencias";
            this.Text = "v";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalleA3)).EndInit();
            this.cmsDetalleA3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvA3)).EndInit();
            this.contextMenuStripA3.ResumeLayout(false);
            this.tsA3.ResumeLayout(false);
            this.tsA3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSReferenciasDetalle)).EndInit();
            this.cmsReferenciaDetalle.ResumeLayout(false);
            this.tsPS.ResumeLayout(false);
            this.tsPS.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip tsA3;
        private System.Windows.Forms.ToolStrip tsPS;
        private System.Windows.Forms.ToolStripButton btnUpdateA3;
        private System.Windows.Forms.ToolStripButton btnReferenciasRepetidasPS;
        private System.Windows.Forms.DataGridView dgvA3;
        private System.Windows.Forms.DataGridView dgvPS;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripA3;
        private System.Windows.Forms.ToolStripMenuItem arreglarIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arreglarVisibleEnTiendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copiarElDePSAA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activarEnTiendaEnA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activarEnTiendaEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem desactivarEnTiendaEnA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desactivarEnTiendaEnPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripMenuItem arreglarDescripcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarDePSAA3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarDeA3APSToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvPSReferenciasDetalle;
        private System.Windows.Forms.ToolStripButton tsbtnActualizarPS;
        private System.Windows.Forms.ContextMenuStrip cmsReferenciaDetalle;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem eliminarProductosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoactualizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbtnIDRepetidosA3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.DataGridView dgvDetalleA3;
        private System.Windows.Forms.ContextMenuStrip cmsDetalleA3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem ponerANULLLosProductosToolStripMenuItem;
    }
}