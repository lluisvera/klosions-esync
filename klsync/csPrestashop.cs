﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using RestSharp;
using System.IO;
using System.Xml.Linq;
using System.Net;

namespace klsync
{
    class csPrestashop
    {
        csMySqlConnect mysql = new csMySqlConnect();

        public string getPhoneNumberByIdAddress(string id_address)
        {
            return mysql.obtenerDatoFromQuery("select phone from ps_address where id_address = " + id_address);
        }

        public string getNIFByIdAddress(string id_address)
        {
            return mysql.obtenerDatoFromQuery("select dni from ps_address where id_address = " + id_address);
        }

        /// <summary>
        /// No funciona
        /// No se puede acceder al backoffice con los datos introducidos
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        public void addEmployee(string firstname, string lastname, string password, string email)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest("employees?schema=synopsis&ws_key=" + csGlobal.webServiceKey, Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                orderEl.Element("active").Value = "1";
                orderEl.Element("passwd").Value = password;
                orderEl.Element("firstname").Value = firstname;
                orderEl.Element("lastname").Value = lastname;
                orderEl.Element("email").Value = email;
                orderEl.Element("id_lang").Value = "3";
                orderEl.Element("id_profile").Value = "1";

                request = new RestRequest("employees?ws_key=" + csGlobal.webServiceKey, Method.POST);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception)
            { }
        }

        public void addClientVilardell(Objetos.csCliente cliente) // Vilardell
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest("customers?schema=synopsis&ws_key=" + csGlobal.webServiceKey, Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                orderEl.Element("company").Value = cliente.company; // !
                orderEl.Element("lastname").Value = cliente.apellidos; // !
                orderEl.Element("firstname").Value = cliente.nombre; // !
                orderEl.Element("email").Value = cliente.email; // !
                orderEl.Element("passwd").Value = cliente.password; // !
                orderEl.Element("active").Value = "1";
                orderEl.Element("id_default_group").Value = cliente.grupoPS;
                orderEl.Element("associations").Element("groups").Descendants().Last().Value = cliente.grupoPS;
                orderEl.Element("kls_a3erp_id").Value = cliente.codIC; // !
                orderEl.Element("type").Value = cliente.type; // F, PF, MF, MPF,
                orderEl.Element("customer_type").Value = cliente.customer_type; // !
                orderEl.Element("kls_payment_method").Value = cliente.payment_method; // !
                orderEl.Element("kls_payment_method_id").Value = cliente.payment_method_id; // !

                #region campos no obligatorios
                //orderEl.Element("id_lang").Value = mysql.defaultLangPS().ToString();
                //orderEl.Element("newsletter_date_add").Value = "";
                //orderEl.Element("ip_registration_newsletter").Value = "";
                //orderEl.Element("last_passwd_gen").Value = "";
                //orderEl.Element("secure_key").Value = "";
                //orderEl.Element("deleted").Value = "";
                //orderEl.Element("birthday").Value = ""; 
                //orderEl.Element("newsletter").Value = "";
                //orderEl.Element("optin").Value = "";
                //orderEl.Element("website").Value = "";
                //orderEl.Element("company").Value = "";
                //orderEl.Element("siret").Value = "";
                //orderEl.Element("ape").Value = "";
                //orderEl.Element("outstanding_allow_amount").Value = "";
                //orderEl.Element("show_public_prices").Value = "";
                //orderEl.Element("id_risk").Value = "";
                //orderEl.Element("max_payment_days").Value = "";            
                //orderEl.Element("note").Value = "";
                //orderEl.Element("is_guest").Value = "";
                //orderEl.Element("id_shop").Value = "";
                //orderEl.Element("id_shop_group").Value = "";
                //orderEl.Element("date_add").Value = "";
                //orderEl.Element("date_upd").Value = "";
                #endregion

                //orderEl.Element("kls_a3erp_id").Value = "";
                //orderEl.Element("kls_a3erp_fam_id").Value = "";


                request = new RestRequest("customers?ws_key=" + csGlobal.webServiceKey, Method.POST);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception ex)
            { }
        }

        public void addCategory(Dictionary<int, string> name, string id_parent)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest("categories?schema=synopsis&ws_key=" + csGlobal.webServiceKey, Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                orderEl.Element("active").Value = "1";
                orderEl.Element("id_parent").Value = id_parent;

                var names = orderEl.Element("name").Elements();

                foreach (var n in names)
                {
                    foreach (var item in name)
                    {
                        if (item.Key.ToString() == n.FirstAttribute.Value)
                        {
                            n.Value = item.Value;
                        }
                    }
                }

                var link_rewrites = orderEl.Element("link_rewrite").Elements();

                foreach (var n in link_rewrites)
                {
                    foreach (var item in name)
                    {
                        if (item.Key.ToString() == n.FirstAttribute.Value)
                        {
                            n.Value = item.Value.ToString().linkRewrite();
                        }
                    }
                }

                orderEl.Element("level_depth").Remove();
                orderEl.Element("nb_products_recursive").Remove();

                request = new RestRequest("categories?ws_key=" + csGlobal.webServiceKey, Method.POST);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception)
            { }
        }

        public void renameCategory(Dictionary<int, string> name, string id)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest(string.Format("categories/{0}?ws_key={1}", id, csGlobal.webServiceKey), Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                var names = orderEl.Element("name").Elements();

                foreach (var n in names)
                {
                    foreach (var item in name)
                    {
                        if (item.Key.ToString() == n.FirstAttribute.Value)
                        {
                            n.Value = item.Value;
                        }
                    }
                }

                var link_rewrites = orderEl.Element("link_rewrite").Elements();

                foreach (var n in link_rewrites)
                {
                    foreach (var item in name)
                    {
                        if (item.Key.ToString() == n.FirstAttribute.Value)
                        {
                            n.Value = item.Value.ToString().linkRewrite();
                        }
                    }
                }

                orderEl.Element("level_depth").Remove();
                orderEl.Element("nb_products_recursive").Remove();

                request = new RestRequest("categories?ws_key=" + csGlobal.webServiceKey, Method.PUT);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception)
            { }
        }

        public void deleteCategory(string id_category)
        {
            try
            {
                string sURL = csGlobal.APIUrl + "categories/" + id_category + "?ws_key=" + csGlobal.webServiceKey;

                WebRequest request = WebRequest.Create(sURL);
                request.Method = "DELETE";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex) { }
        }

        public void deleteImage(string id_image, string id_product)
        {
            //24/12/2020 Añadido para CadCanarias, se añade porque el certificado no era compatible con esync.
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string sURL = string.Format("{0}images/products/{1}/{2}?ws_key={3}", csGlobal.APIUrl, id_product, id_image, csGlobal.webServiceKey);

                WebRequest request = WebRequest.Create(sURL);
                request.Method = "DELETE";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex) { }
        }

        public Objetos.csTercero getInfoClient(string id_address)
        {
            DataTable ps = mysql.cargarTabla("select ps_customer.id_customer,  ps_customer.firstname,  ps_customer.lastname,  " +
                              " ps_customer.email, ps_address.dni, ps_address.phone, ps_address.dni, " +
                              " ps_address.address1, ps_address.company, ps_address.city, ps_address.postcode " +
                              " FROM ps_customer " +
                              " LEFT JOIN ps_address on ps_address.id_customer = ps_customer.id_customer " +
                              " WHERE ps_address.id_address = " + id_address +
                              " and ps_customer.active = 1");

            Objetos.csTercero client = new Objetos.csTercero();

            foreach (DataRow item in ps.Rows)
            {
                client.direccion = csUtilidades.quitarAcentos(item["address1"].ToString());
                client.nombre = item["firstname"].ToString().ToUpper(); ;
                client.apellidos = item["lastname"].ToString().ToUpper(); ;
                client.razonSocial = item["company"].ToString().ToUpper(); ;
                client.poblacion = item["city"].ToString().ToUpper();
                client.nifcif = item["dni"].ToString();
                client.email = item["email"].ToString();
                client.codigoPostal = item["postcode"].ToString();
                client.nomIC = item["firstname"].ToString().ToUpper() + " " + item["lastname"].ToString().ToUpper();
                client.telf = item["phone"].ToString().Replace(" ", "");
            }

            return client;
        }

        /// <summary>
        /// Funciona - Marzo 2017
        /// 
        /// </summary>
        /// <param name="path">Ruta donde se encuentra la imagen</param>
        /// <param name="id_product">id del producto al que hace referencia</param>
        /// <returns>Contenido de la respuesta</returns>
        public string uploadImage(string path, string id_product)
        {
            try
            {
                var client = new RestClient(csGlobal.APIUrl);
                byte[] file = File.ReadAllBytes(path);

                var request = new RestRequest(string.Format("images/products/{0}?ws_key={1}", id_product, csGlobal.webServiceKey), Method.POST);

                request.AddFileBytes("image", file, Path.GetFileName(path));

                IRestResponse response = client.Execute(request);

                return response.Content;
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.ToString();
            }
        }

        /// <summary>
        /// Solo sube la imagen grande. La pequeña no
        /// </summary>
        /// <param name="path"></param>
        /// <param name="id_category"></param>
        /// <returns></returns>
        public string subirImagenCategoria(string path, string id_category)
        {
            try
            {
                var client = new RestClient(csGlobal.APIUrl);
                byte[] file = File.ReadAllBytes(path);

                var request = new RestRequest("images/categories/" + id_category + "?ws_key=" + csGlobal.webServiceKey, Method.POST);

                request.AddFileBytes("image", file, Path.GetFileName(path));

                IRestResponse response = client.Execute(request);

                return response.Content;
            }
            catch (Exception ex)
            {
                return "ERROR: " + ex.ToString();
            }
        }


        internal void borrarPedido(string id_order)
        {
            RestRequest request;
            var client = new RestClient(csGlobal.APIUrl);
            request = new RestRequest(string.Format("orders/{0}?ws_key={1}", id_order, csGlobal.webServiceKey), Method.DELETE);
            IRestResponse response = client.Execute(request);
        }

        internal void cambiarEstadoPedido(string id_order, string id_state)
        {
            RestRequest request;
            var client = new RestClient(csGlobal.APIUrl);
            request = new RestRequest(string.Format("orders/{0}?ws_key={1}", id_order, csGlobal.webServiceKey), Method.GET);
            IRestResponse response = client.Execute(request);

            XElement orderXML = XElement.Parse(response.Content);
            XElement orderEl = orderXML.Descendants().FirstOrDefault();

            orderEl.Element("current_state").Value = id_state;

            request = new RestRequest("orders?ws_key=" + csGlobal.webServiceKey, Method.PUT);
            request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
            IRestResponse response2 = client.Execute(request);
        }

        internal void cambiarProductAttributeIdOrderDetail(string id_order_detail, string product_attribute_id)
        {
            RestRequest request;
            var client = new RestClient(csGlobal.APIUrl);
            request = new RestRequest(string.Format("order_details/{0}?ws_key={1}", id_order_detail, csGlobal.webServiceKey), Method.GET);
            IRestResponse response = client.Execute(request);

            XElement orderXML = XElement.Parse(response.Content);
            XElement orderEl = orderXML.Descendants().FirstOrDefault();

            orderEl.Element("product_attribute_id").Value = product_attribute_id;

            request = new RestRequest("order_details?ws_key=" + csGlobal.webServiceKey, Method.PUT);
            request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
            IRestResponse response2 = client.Execute(request);
        }

        internal void deactivateCategory(string id, string estado)
        {
            try
            {
                csMySqlConnect mysql = new csMySqlConnect();

                RestRequest request;
                var client = new RestClient(csGlobal.APIUrl);
                request = new RestRequest(string.Format("categories/{0}?ws_key={1}", id, csGlobal.webServiceKey), Method.GET);
                IRestResponse response = client.Execute(request);

                XElement orderXML = XElement.Parse(response.Content);
                XElement orderEl = orderXML.Descendants().FirstOrDefault();

                orderEl.Element("active").Value = estado;
                orderEl.Element("level_depth").Remove();
                orderEl.Element("nb_products_recursive").Remove();

                request = new RestRequest("categories?ws_key=" + csGlobal.webServiceKey, Method.PUT);
                request.AddParameter("xml", orderXML.ToString(), ParameterType.RequestBody);
                IRestResponse response2 = client.Execute(request);
            }
            catch (Exception)
            { }
        }

        /**********************************/
        // VILARDELL

        public void bajarClientesPendientes()
        {
            try
            {
                // procedimiento para bajar los clientes que no tengan el kls_a3erp_id informado
                csMySqlConnect mysql = new csMySqlConnect();
                /*
                string consulta = "SELECT ps_customer.id_customer, " +
                "       ps_customer.firstname, " +
                "       ps_customer.lastname, " +
                "       case when   ps_customer.customer_type='DIRECT' then 'DIRECTOS' else ps_customer.customer_type end as customer_type, " +
                "       ps_customer.company, " +
                "       ps_customer.email, " +
                "       'TRANSF' AS 'forma_pagament', " +
                "       'REP03' AS 'representant', " +
                "       '1' AS 'AutorizarDelegaciones', " +
                "       IF (ps_address.id_country in ('6'),'NAC', IF (ps_address.id_country in (8,3,1,10,17,26,20,23,18,7,2,15,9,14,12,37,193,86,125,131,130,148,13),\"U.E.\", \"RESTO MUNDO\")) AS 'tractament_fiscal', " +
                "       ps_country.iso_code AS pais_iso_code, " +
                "       left(ps_address.postcode,2) AS provincia_iso_code, " +
                "       ps_address.city, " +
                "       ps_address.address1 AS dir1, " +
                "       ps_address.address2 AS dir2, " +
                "       CONCAT(char(39), ps_address.postcode) AS codigopostal, " +
                "       ps_address.phone AS tel, " +
                "       ps_address.dni " +
                "       FROM ps_customer " +

                "       JOIN ps_address ON ps_address.id_customer = ps_customer.id_customer " +
                "       LEFT JOIN ps_country ON ps_country.id_country = ps_address.id_country " +
                "       LEFT JOIN ps_state ON ps_state.id_state = ps_address.id_state " +
                "       WHERE ps_customer.kls_a3erp_id IS NULL AND ps_address.deleted=0   AND ps_address.main=1" +
                "       order by ps_customer.id_customer; ";
                */
                string consulta = "SELECT ps_customer.id_customer, " +
                "       ps_customer.type, ps_customer.firstname, " +
                "       ps_customer.lastname, " +
                "       case when   ps_customer.customer_type='DIRECT' then 'DIRECTOS' else ps_customer.customer_type end as customer_type, " +
                "       ps_customer.company, " +
                "       ps_customer.email, " +
                "       'TRANSF' AS 'forma_pagament', " +
                "       'REP03' AS 'representant', " +
                "       '1' AS 'AutorizarDelegaciones', " +
                "       IF (ps_address.id_country in ('6'),'NAC', IF (ps_address.id_country in (8,3,1,10,17,26,20,23,18,7,2,15,9,14,12,37,193,86,125,131,130,148,13),\"U.E.\", \"RESTO MUNDO\")) AS 'tractament_fiscal', " +
                "       ps_country.iso_code AS pais_iso_code, " +
                "       left(ps_address.postcode,2) AS provincia_iso_code, " +
                "       ps_address.city, " +
                "       ps_address.address1 AS dir1, " +
                "       ps_address.address2 AS dir2, " +
                "       CONCAT(char(39), ps_address.postcode) AS codigopostal, " +
                "       ps_address.phone AS tel, " +
                "       ps_address.dni " +
                "       FROM ps_customer " +

                "       JOIN ps_address ON ps_address.id_customer = ps_customer.id_customer " +
                "       LEFT JOIN ps_country ON ps_country.id_country = ps_address.id_country " +
                "       LEFT JOIN ps_state ON ps_state.id_state = ps_address.id_state " +
                "       WHERE ps_customer.kls_a3erp_id IS NULL AND ps_address.deleted=0   AND ps_address.main=1" +
                "       order by ps_customer.id_customer; ";

                DataTable customers = mysql.cargarTabla(consulta);

                csUtilidades.registroProceso("Paso 1", System.Reflection.MethodBase.GetCurrentMethod().Name);

                //si la conexion es vilardell al excel le añadimos estos campos que el importador de alfa si lo acepta, no añado mas cosas porque no las coge.
                if (csGlobal.nombreServidor.Contains("VILARDELL"))
                {
                    customers.Columns.Add("sigla_via", typeof(String));
                    customers.Columns.Add("codigo_actividad1", typeof(String));

                }

                foreach (DataRow row in customers.Rows)
                {
                 
                    row["sigla_via"] = "CL";
                    row["codigo_actividad1"] = row["type"].ToString() ;


                }


            


                if (customers.hasRows())
            {
                csUtilidades.registroProceso("Paso 2", System.Reflection.MethodBase.GetCurrentMethod().Name);
                customers.exportToExcel("clientes");
                csUtilidades.registroProceso("Paso 3", System.Reflection.MethodBase.GetCurrentMethod().Name);
                csUtilidades.openFolder(csGlobal.fileExportPath);
                csUtilidades.registroProceso("Paso 4", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            csUtilidades.registroProceso("Paso 5", System.Reflection.MethodBase.GetCurrentMethod().Name);

        }
            catch (Exception ex)
            {
                csUtilidades.registrarLogError(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }


}
    }
}
