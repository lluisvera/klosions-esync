﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExcelDataReader;
using System.Collections;






namespace klsync.Fundacio
{
    public partial class frGastosFundacio : Form
    {
        private static frGastosFundacio m_FormDefInstance;

  
        public static frGastosFundacio DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frGastosFundacio();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }

        public frGastosFundacio()
        {
            InitializeComponent();
            button2.Enabled = false;
        }



        private DataSet leerExcel(Stream stream)
        {
            var result=new DataSet();

            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                result = reader.AsDataSet();
                return result;
            }



            }

        private DataSet dtsResult;
        private DataTable dtFacturas;
        private DataTable dtGastos;
        private DataTable dtDatosComercial;
        private string rutaArchivo;


        private void button1_Click(object sender, EventArgs e)
         {
            if (dgvFacturas.Rows.Count > 0)
            {
                this.dgvFacturas.DataSource = null;
                dgvFacturas.Rows.Clear();
            }
            if (dgvGastos.Rows.Count > 0)
            {
                this.dgvGastos.DataSource = null;
                dgvGastos.Rows.Clear();
            }
            
            Stream myStream;
             OpenFileDialog abrirArchivo = new OpenFileDialog();

             abrirArchivo.InitialDirectory = "c:\\";
             abrirArchivo.Filter = "CSV files (*.csv)|*.csv|Excel Files|*.xls;*.xlsx";
             abrirArchivo.FilterIndex = 2;
             abrirArchivo.RestoreDirectory = true;

             if (abrirArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
             {
                rutaArchivo = abrirArchivo.FileName;
                 try
                 {
                     if ((myStream = abrirArchivo.OpenFile()) != null)
                     {
                        //Dataset completo
                        dtsResult = leerExcel(myStream);

                        dtDatosComercial = this.dtsResult.Tables[0];
                       
                        dtGastos = this.dtsResult.Tables[1];
                        dtFacturas = this.dtsResult.Tables[2];

                        infoComercial();
                        transformarFacturas();
                        transformarGastos();
                        dateFormat();

                        dgvFacturas.DataSource = this.dtFacturas;
                        dgvGastos.DataSource = this.dtGastos;
                        cellFomat();
                        
                        textBox1.Text = abrirArchivo.SafeFileName;
                        button2.Enabled = true;
                     }
                 }catch(Exception ex)
                 {
                     MessageBox.Show("Cierre el documento previamente. ");
                 }
             }
         }
        private void cellFomat()
        {
            var alineacion= DataGridViewContentAlignment.MiddleRight; 
            dgvFacturas.Columns["Base Despesa Iva 21%"].DefaultCellStyle.Alignment = alineacion;
            dgvFacturas.Columns["Base Despesa Iva 10%"].DefaultCellStyle.Alignment = alineacion;
            dgvFacturas.Columns["Base Despesa Iva 4%"].DefaultCellStyle.Alignment = alineacion;
            dgvFacturas.Columns["Base Despesa Exento"].DefaultCellStyle.Alignment = alineacion;
            dgvFacturas.Columns["Total Despesa"].DefaultCellStyle.Alignment = alineacion;
            dgvGastos.Columns["Total Despesa"].DefaultCellStyle.Alignment = alineacion;

        }
        private void dateFormat()
        {
            try
            {
                System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("es-ES");
                foreach (DataRow row in dtFacturas.Rows)
                {
                    DateTime date = Convert.ToDateTime(row["DATA DESPESA"].ToString());
                    row["DATA DESPESA"] = date.ToString("d", culture);
                }
                foreach (DataRow row in dtGastos.Rows)
                {
                    DateTime date = Convert.ToDateTime(row["DATA DESPESA"].ToString());
                    row["DATA DESPESA"] = date.ToString("d", culture);
                }
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }
        private void transformarFacturas()
        {
            try
            {
                for (int i = 0; i < dtFacturas.Columns.Count; i++)
                {
                    dtFacturas.Columns["Column" + i.ToString()].ColumnName = dtFacturas.Rows[0].ItemArray[i].ToString() != "" ? dtFacturas.Rows[0].ItemArray[i].ToString().ToUpper() : "Column" + i.ToString();
                }

                dtFacturas.Rows[0].Delete();

                //Reviso si hay lineas en blanco y las borro del datatable
                ArrayList linesToDelete = new ArrayList();

                for (int i=1; i<dtFacturas.Rows.Count;i++)
                {
                    if (string.IsNullOrEmpty(dtFacturas.Rows[i][0].ToString()))
                    {
                        linesToDelete.Add(i.ToString());
                    }
                }

                for (int ii = 0; ii < linesToDelete.Count; ii++)
                {

                    dtFacturas.Rows[Convert.ToInt32(linesToDelete[ii].ToString())].Delete();
                }
                
                dtFacturas.AcceptChanges();
            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
        }
        private void transformarGastos()
        {
            for (int i = 0; i < dtGastos.Columns.Count; i++)
            {
                dtGastos.Columns["Column"+i.ToString()].ColumnName = dtGastos.Rows[0].ItemArray[i].ToString() != "" ? dtGastos.Rows[0].ItemArray[i].ToString().ToUpper() : "Column" + i.ToString();
            }
            dtGastos.Rows[0].Delete();

            //Reviso si hay lineas en blanco y las borro del datatable
            ArrayList linesToDelete = new ArrayList();

            for (int i = 1; i < dtGastos.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(dtGastos.Rows[i][0].ToString()))
                {
                    linesToDelete.Add(i.ToString());
                }
            }

            for (int ii = 0; ii < linesToDelete.Count; ii++)
            {

                dtGastos.Rows[Convert.ToInt32(linesToDelete[ii].ToString())].Delete();
            }


            dtGastos.AcceptChanges();
        }
        private void infoComercial()
        {
            // DateTime fecha = DateTime.ParseExact(datosComercial.Rows[1]["Column1"].ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("es-ES");
            DateTime date = Convert.ToDateTime(dtDatosComercial.Rows[1]["Column1"].ToString());

            cuadroInformativo.Text = " Comercial: " + dtDatosComercial.Rows[1]["Column0"].ToString();
            cuadroInformativo.Text += "\n Data: " + date.ToString("d",culture);
            cuadroInformativo.Text += "\n NIF: " + dtDatosComercial.Rows[1]["Column2"].ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
                button2.Enabled = false;
                a3erp.abrirEnlace();
                factGastos();
                factFacturas();
                a3erp.cerrarEnlace();
                button2.Enabled = true;

        }

    
        csa3erp a3erp = new csa3erp();

        /// <summary>
        /// Notas de Gastos
        /// </summary>
        private void factGastos()
        {
            try
            {
                string[] docPago = new string[2];
                Dictionary<string, ArrayList> datos = new Dictionary<string, ArrayList>();

                foreach (DataRow row in dtGastos.Rows)
                {
                    try
                    {
                        string formaPago = row["FORMA PAGO"].ToString();

                        if (!datos.Keys.Contains(formaPago))
                        {
                            formaPago = formaPago.Split('-')[0].ToString();
                            datos.Add(formaPago, new ArrayList());
                        }

                        datos[formaPago].Add(row);
                    }
                    catch (Exception ex)
                    {

                    }

                }
                foreach (KeyValuePair<string, ArrayList> entrada in datos)
                {

                    Objetos.csCabeceraDoc[] cabecera = new Objetos.csCabeceraDoc[1];
                    Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[this.dtGastos.Rows.Count];

                    //Si no existe el proveedor debe dar un error.
                    DataTable proveedor = recuperarProveedores(this.dtDatosComercial.Rows[1].ItemArray[2].ToString());

                    try
                    {
                        if (proveedor.Rows.Count > 0)
                        {

                            cabecera[0] = new Objetos.csCabeceraDoc();
                            cabecera[0].nombreIC = this.dtDatosComercial.Rows[1].ItemArray[0].ToString();
                            cabecera[0].serieDoc = dtGastos.Rows[1]["SERIE"].ToString();
                            cabecera[0].nif = this.dtDatosComercial.Rows[1].ItemArray[2].ToString();
                            cabecera[0].codIC = proveedor.Rows[0]["CODPRO"].ToString();
                            cabecera[0].fechaDoc = Convert.ToDateTime(dtGastos.Rows[1]["DATA DESPESA"].ToString());
                            cabecera[0].codDocumentoPago = entrada.Key;
                            cabecera[0].regIva = "CEXE";
                            docPago = entrada.Key.Split('-');
                            cabecera[0].codFormaPago = docPago[0];

                            DataTable dtLineasPorFPago = new DataTable();
                            dtLineasPorFPago = dtGastos.Select("[FORMA PAGO] ='" + entrada.Key + "'").CopyToDataTable();


                            for (int i = 0; i < entrada.Value.Count; i++)
                            {

                                DataTable articulo = recuperarArticulo(this.dtGastos.Rows[i]["TIPUS DESPESA"].ToString().Split('-')[0]);

                                if (articulo.Rows.Count < 0)
                                {
                                    MessageBox.Show("EL artículo no tiene la referencia de proveedor creada correctamente. debe corregirlo para generar las facturas de dtGastos");
                                    break;
                                }

                                lineas[i] = new Objetos.csLineaDocumento();
                                lineas[i].nombreArticulo = articulo.Rows[0]["ARTALIAS"].ToString();
                                lineas[i].descripcionArticulo = articulo.Rows[0]["DESCART"].ToString().Trim();
                                lineas[i].precio = dtLineasPorFPago.Rows[i]["TOTAL DESPESA"].ToString();
                                lineas[i].ivaIncluido = true;
                                lineas[i].codigoArticulo = articulo.Rows[0]["CODART"].ToString().Trim();
                                lineas[i].cantidad = 1.ToString();
                                lineas[i].codProyecto = dtLineasPorFPago.Rows[i]["PROJECTE"].ToString().Split('-')[0];
                                lineas[i].cuentaContable = dtLineasPorFPago.Rows[i]["COMPTE COMPTABLE"].ToString().Split('-')[0];

                            };

                            csGlobal.docDestino = "Factura";
                            this.a3erp.generarDocA3Objeto(cabecera, lineas, true, "no",null);
                        }
                        else
                        {
                            MessageBox.Show("El proveedor no existe, debe crearlo previamente para generar las facturas de gastos");
                            break;
                            throw new System.ArgumentException("El comercial no existe");

                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }

            }
            catch (Exception ex)
            {
                (ex.Message).mb();
            }
           
        }
        
        private void factFacturas()
        {
           for (int i=0;i<this.dtFacturas.Rows.Count;i++)
            {
                //verificamos que no la factura no esté introducida
                csSqlConnects sql = new csSqlConnects();
                DataTable dtCheckFacturas = new DataTable();
                dtCheckFacturas = sql.obtenerDatosSQLScript("SELECT FECHA, SERIE, NUMDOC, CODPRO, NOMPRO,REFERENCIA, TOTDOC " +
                    " FROM CABEFACC WHERE REFERENCIA='" + dtFacturas.Rows[i]["NOMBRE FACTURA"].ToString() + "'");

                if (dtCheckFacturas.Rows.Count > 0)
                {
                    MessageBox.Show("Ya hay introducida una factura con la referencia " + dtCheckFacturas.Rows[0]["REFERENCIA"].ToString() + "\n" + "\n" +
                        "PROVEEDOR: " + dtCheckFacturas.Rows[0]["CODPRO"].ToString() + "-" + dtCheckFacturas.Rows[0]["NOMPRO"].ToString() + "\n" +
                        "FECHA: " + dtCheckFacturas.Rows[0]["FECHA"].ToString().Substring(0,10) + " | " + dtCheckFacturas.Rows[0]["SERIE"].ToString() +" / " + Convert.ToInt32(dtCheckFacturas.Rows[0]["NUMDOC"]).ToString() + "\n" +
                        "IMPORTE: " + dtCheckFacturas.Rows[0]["TOTDOC"].ToString());
                    continue;

                }

                string test = this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString();

                double iva21 = this.dtFacturas.Rows[i]["BASE DESPESA IVA 21%"].ToString() == "" ? 0 : Double.Parse(this.dtFacturas.Rows[i]["BASE DESPESA IVA 21%"].ToString()) * 1.21;
                double iva10 = this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString()=="" ? 0 : Double.Parse(this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString()) * 1.10;
                double iva4  = this.dtFacturas.Rows[i]["BASE DESPESA IVA 4%"].ToString() == "" ? 0 : Double.Parse(this.dtFacturas.Rows[i]["BASE DESPESA IVA 4%"].ToString()) * 1.04;
                double exento = this.dtFacturas.Rows[i]["BASE DESPESA EXENTO"].ToString() == "" ? 0 : Double.Parse(this.dtFacturas.Rows[i]["BASE DESPESA EXENTO"].ToString());
                double irpf = this.dtFacturas.Rows[i]["BASE IRPF"].ToString() == "" || this.dtFacturas.Rows[i]["%IRPF"].ToString() == "" ? 0 : Double.Parse(this.dtFacturas.Rows[i]["BASE IRPF"].ToString()) * Double.Parse(this.dtFacturas.Rows[i]["%IRPF"].ToString())*0.01;


                double sumaTotal = Math.Round(iva21+iva10+iva4+exento-irpf,2);
                double totalDocumento = Math.Round(Double.Parse(this.dtFacturas.Rows[i]["TOTAL DESPESA"].ToString()),2);
                
                Objetos.csTercero[] objetoProveedor = new Objetos.csTercero[1];
                try
                {

                    DataTable proveedor = recuperarProveedores(this.dtFacturas.Rows[i]["CIF"].ToString());

                   if(sumaTotal != totalDocumento)
                    {
                        MessageBox.Show("En la linea " + (i+1).ToString()+ " no coincide el importe total con la suma de las lineas");
                        return;
                    }
                   
                    if (proveedor.Rows.Count<=0)
                    {
                        objetoProveedor[0] = new Objetos.csTercero();
                        objetoProveedor[0].nifcif = this.dtFacturas.Rows[i]["CIF"].ToString();
                        objetoProveedor[0].direccion = this.dtFacturas.Rows[i]["ADREÇA"].ToString();
                        objetoProveedor[0].nombre =     this.dtFacturas.Rows[i]["PROVEIDOR"].ToString();
                        objetoProveedor[0].codigoPostal = this.dtFacturas.Rows[i]["CP"].ToString()!=""? this.dtFacturas.Rows[i]["CP"].ToString():"";
                        objetoProveedor[0].poblacion = this.dtFacturas.Rows[i]["POBLACIÓ"].ToString()!=""? this.dtFacturas.Rows[i]["POBLACIÓ"].ToString():"";
                        objetoProveedor[0].pais = this.dtFacturas.Rows[i]["PAÍS"].ToString()!=""? this.dtFacturas.Rows[i]["PAÍS"].ToString():"";
                        objetoProveedor[0].razonSocial= this.dtFacturas.Rows[i]["PROVEIDOR"].ToString();
                        objetoProveedor[0].apellidos = "";
                        objetoProveedor[0].telf = "";
                        objetoProveedor[0].email = "";
                        objetoProveedor[0].permalink = "";
                        objetoProveedor[0].paypal = "";
                        objetoProveedor[0].tipoIRPF = "110";
                        objetoProveedor[0].claveIRPF = "G";
                        objetoProveedor[0].porcentajeIRPF =this.dtFacturas.Rows[i]["%IRPF"].ToString();
                        objetoProveedor[0].subclaveIRPF = "01";
                    }
                    

                    Objetos.csCabeceraDoc[] cabecera= new Objetos.csCabeceraDoc[1];
                    Objetos.csLineaDocumento[] lineas = new Objetos.csLineaDocumento[4];

                    cabecera[0]=generarCabecerasFactura(proveedor,objetoProveedor[0],i);

                    if (this.dtFacturas.Rows[i]["BASE DESPESA IVA 21%"].ToString() != "" && !this.dtFacturas.Rows[i]["BASE DESPESA IVA 21%"].ToString().Equals("0"))
                        lineas[0]=generarLineasFactura(i, this.dtFacturas.Rows[i]["BASE DESPESA IVA 21%"].ToString(),"ORD21");

                    if (this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString() != "" && !this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString().Equals("0") )
                        lineas[1] = generarLineasFactura(i, this.dtFacturas.Rows[i]["BASE DESPESA IVA 10%"].ToString(),"RED10");

                    if (this.dtFacturas.Rows[i]["BASE DESPESA IVA 4%"].ToString() != "" && !this.dtFacturas.Rows[i]["BASE DESPESA IVA 4%"].ToString().Equals("0"))
                        lineas[2] = generarLineasFactura(i , this.dtFacturas.Rows[i]["BASE DESPESA IVA 4%"].ToString(),"SRED");

                    if (this.dtFacturas.Rows[i]["BASE DESPESA EXENTO"].ToString() != "" && !this.dtFacturas.Rows[i]["BASE DESPESA EXENTO"].ToString().Equals("0"))
                        lineas[3] = generarLineasFactura(i, exento.ToString());



                    csGlobal.docDestino = "Factura";
                    //a3erp.generarDocA3Objeto(cabecera, lineas, true, "no", objetoProveedor);
                    a3erp.generarDocA3Objeto(cabecera, lineas, true, "no", objetoProveedor);


                }
                catch (Exception ex) {
                    MessageBox.Show("No se han creado las facturas con exito");
                    return;
                }
            }
            //MessageBox.Show("Facturas creadas con exito");

        }
        private Objetos.csCabeceraDoc generarCabecerasFactura(DataTable proveedor, Objetos.csTercero objeto,int i)
        {
            Objetos.csCabeceraDoc cabecera = new Objetos.csCabeceraDoc();

            try
            {
                cabecera = new Objetos.csCabeceraDoc();
                cabecera.nombreIC = proveedor.Rows.Count > 0 ? proveedor.Rows[0]["ALIAS"].ToString() : objeto.nombre;
                cabecera.nif = this.dtFacturas.Rows[i]["CIF"].ToString();
                cabecera.serieDoc = this.dtFacturas.Rows[i]["SERIE"].ToString();
                cabecera.id_Customer_Ext = proveedor.Rows.Count > 0 ? proveedor.Rows[0]["CODPRO"].ToString() : null;
                cabecera.fechaDoc = Convert.ToDateTime(this.dtFacturas.Rows[i]["DATA DESPESA"].ToString());
                cabecera.codDocumentoPago = this.dtFacturas.Rows[i]["FORMA PAGO"].ToString().Split('-')[0];
                cabecera.referencia = this.dtFacturas.Rows[i]["NOMBRE FACTURA"].ToString();
                cabecera.codIC = proveedor.Rows.Count > 0 ? proveedor.Rows[0]["CODPRO"].ToString() : "";
                cabecera.regIva = "CNAC";
                cabecera.tipoRentencion = "110";
                cabecera.claveIRPF = "G";
                cabecera.baseRetencion = this.dtFacturas.Rows[i]["BASE IRPF"].ToString();
                cabecera.porcentajeRetencion = this.dtFacturas.Rows[i]["%IRPF"].ToString();
                cabecera.subclaveIRPF = "01";
            }
            catch(Exception ex)
            {

            }
            return cabecera;
        }
       private Objetos.csLineaDocumento generarLineasFactura(int i,string precio,string tipoIva="exe")
        {
            Objetos.csLineaDocumento linea = new Objetos.csLineaDocumento();

                            
            DataTable articulo = recuperarArticulo(this.dtFacturas.Rows[i]["TIPUS DESPESA"].ToString().Split('-')[0]);
            if (articulo.Rows.Count <= 0)
            {
                throw new Exception("El artículo no tiene indicada correctamente la referencía con el proveedor, debe corregirlo para generar las facturas");
            }

            linea= new Objetos.csLineaDocumento();
            linea.nombreArticulo = articulo.Rows[0]["ARTALIAS"].ToString().Split('-')[0];
            linea.codigoArticulo = articulo.Rows[0]["CODART"].ToString().Split('-')[0];
            linea.descripcionArticulo = articulo.Rows[0]["DESCART"].ToString();
            linea.precio = precio;
            linea.price = Double.Parse(precio);
            linea.cantidad = 1.ToString();
            linea.ivaIncluido = false;
            linea.tipoIVA = tipoIva;
            linea.cuentaContable = dtFacturas.Rows[i]["COMPTE COMPTABLE"].ToString().Split('-')[0];


            return linea;
        }


            
        
        public DataTable recuperarProveedores(string nif) {

            csSqlConnects sql = new csSqlConnects();

            DataTable clientes = new DataTable();

            clientes = sql.cargarDatosTablaA3("select __PROVEED.CODPRO as CODPRO,__ORGANIZACION.NIF, __ORGANIZACION.ALIAS  from __ORGANIZACION inner join __PROVEED on __ORGANIZACION.IDORG=__PROVEED.IDORG where NIF = '" + nif+"'");

            return clientes;
        }
        public DataTable recuperarArticulo(string artpro)
        {

            csSqlConnects sql = new csSqlConnects();

            DataTable articulo = new DataTable();

            articulo = sql.cargarDatosTablaA3("select ARTALIAS,TIPIVA,CODART,DESCART from articulo where LTRIM(CODART)='" + artpro+"'");

            return articulo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(rutaArchivo);
            }
            catch (Exception ex) { }
        }


        private void dgvGastos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnUpdateFras_Click(object sender, EventArgs e)
        {
            int numFila = 0;
            try
            {

                csSqlConnects sql = new csSqlConnects();
                DataTable dtLineasFras = sql.obtenerDatosSQLScript("SELECT * FROM LINEASFRASPUNTCAT");
                DataTable dtAsientos = sql.obtenerDatosSQLScript("select DESCAPU, FECHA, DEBE, HABER, IDAPUNTE,__ASIENTOS.IDCUENTA, " +
                                " CUENTA, DESCCUE, NOMODIFICABLE, NUMAPUNTE, NUMASIENTO, NUMLIN,PROCEDE, PROCEDEID " +
                                " FROM  __ASIENTOS INNER JOIN CUENTAS " +
                                " ON CUENTAS.IDCUENTA = __ASIENTOS.IDCUENTA where PROCEDE = 'FC' AND FECHA >= '01-01-2020'");
                DataTable dtAsientosToUpdate = new DataTable();
                string numDoc = "";
                string idLinea = "";
                string nuevoArticulo = "";
                string nuevoCentroCoste = "";
                string descNuevoArt = "";
                string queryUpdateLineasFra = "";
                string nuevaCuentacontable="";
                string idCuentaContable = "";
                string idFactura = "";
                string CuentaContableOld = "";
                string idLineaAsientoToUpdate = "";


                sql.abrirConexion();
                foreach (DataRow drLinea in dtLineasFras.Rows)
                {
                    numFila++;
                    if (!string.IsNullOrEmpty(drLinea["NUMDOC"].ToString()))
                    {
                        nuevoArticulo = "";
                        nuevoCentroCoste = "";
                        nuevaCuentacontable = "";
                        idLinea = drLinea["IDLIN"].ToString();
                        nuevoArticulo = drLinea["NUEVO"].ToString().Split('-')[0];
                        nuevoCentroCoste = drLinea["CODI PROJECTE"].ToString();
                        nuevaCuentacontable = drLinea["NUEVA CUENTA"].ToString();
                        descNuevoArt = sql.obtenerValorArticuloA3(nuevoArticulo, "DESCART");
                        nuevoArticulo = nuevoArticulo.PadLeft(15, ' ');
                        nuevoCentroCoste = nuevoCentroCoste.PadLeft(8, ' ');


                        queryUpdateLineasFra = "UPDATE LINEFACT SET CODART='" + nuevoArticulo
                            + "', DESCLIN='" + descNuevoArt
                            + "', CENTROCOSTE='" + nuevoCentroCoste
                            + "', CTACONL=" + nuevaCuentacontable
                            + "  WHERE IDLIN =" + idLinea;
                        csUtilidades.ejecutarConsulta(queryUpdateLineasFra, false);
                    }
                }
                "lineas actualizadas".mb();
                
                //ACTUALIZACIÓN DE ASIENTOS
                foreach (DataRow drLinea in dtLineasFras.Rows)
                {
                    numFila++;
                    if (!string.IsNullOrEmpty(drLinea["NUMDOC"].ToString()))
                    {


                        idLinea = drLinea["IDLIN"].ToString();
                        nuevoCentroCoste = drLinea["CODI PROJECTE"].ToString();
                        nuevaCuentacontable = drLinea["NUEVA CUENTA"].ToString();
                        nuevoCentroCoste = nuevoCentroCoste.PadLeft(8, ' ');
                        idCuentaContable = "";
                        idFactura = drLinea["IDFACC"].ToString();
                        CuentaContableOld = drLinea["CTACONL"].ToString();


                        //1 obtengo los asientos
                        dtAsientosToUpdate = sql.obtenerDatosSQLScript("select DESCAPU, FECHA, DEBE, HABER, CAST(IDAPUNTE AS INT) AS IDAPUNTE, __ASIENTOS.IDCUENTA, " +
                                " CUENTA, DESCCUE, NOMODIFICABLE, NUMAPUNTE, NUMASIENTO, NUMLIN,PROCEDE, PROCEDEID " +
                                " FROM  __ASIENTOS INNER JOIN CUENTAS " +
                                " ON CUENTAS.IDCUENTA = __ASIENTOS.IDCUENTA where PROCEDE = 'FC' AND FECHA >= '01-01-2020' AND  PROCEDEID=" + idFactura + " AND CUENTA=" + CuentaContableOld);

                        idCuentaContable = sql.obtenerCampoTabla("SELECT CAST(IDCUENTA AS INT) AS IDCUENTAC FROM CUENTAS WHERE PLACON='NPGC' AND CUENTA=" + nuevaCuentacontable);
                        //2 actualizo los asientos
                        foreach (DataRow fila in dtAsientosToUpdate.Rows)
                        {
                            idLineaAsientoToUpdate = fila["IDAPUNTE"].ToString();

                             queryUpdateLineasFra = "UPDATE __ASIENTOS SET IDCUENTA=" + idCuentaContable + ", CENTROCOSTE='" + nuevoCentroCoste + "' WHERE IDAPUNTE =" + idLineaAsientoToUpdate;
                                                    csUtilidades.ejecutarConsulta(queryUpdateLineasFra, false);
                        }
                    }
                }
                sql.cerrarConexion();
                "asientos contables actualizados".mb();





            }
            catch (Exception ex)
            {
                ("fila numero " + numFila + "-" + ex.Message).mb();
            }




        }


        /// <summary>
        /// Función para recodificar codigos de articulos de fras de compras del 2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateFras2_Click(object sender, EventArgs e)
        {
            try {
                csSqlConnects sql = new csSqlConnects();
                DataTable dtRecodificacion = new DataTable();
                DataTable dtLineasFras = new DataTable();
                DataTable dtArticulos = new DataTable();

                string linCodPro = "";
                string linCodArt = "";
                string linCodLin = "";
                string recodCodPro = "";
                string recodCodArt = "";
                string recodNewCodArt = "";
                string sqlscript = "";
                string recodNewDescart = "";
                
                dtRecodificacion = sql.obtenerDatosSQLScript("SELECT CODPRO,NOMPRO,CODART,NEWCODART,NEWDESCART  FROM AAARECODIFICACION");
                dtLineasFras = sql.obtenerDatosSQLScript("select serie, NUMDOC, CABEFACC.FECHA,NOMPRO, CODPRO,CODART, DESCLIN, LINEFACT.IDLIN " +
                    " from cabefacc " +
                    " inner  join linefact on cabefacc.idfacc=linefact.idfacc " +
                    " where CABEFACC.FECHA >= '01-01-2021'");

                dtArticulos = sql.obtenerDatosSQLScript("select CODART, DESCART from ARTICULO");


                foreach (DataRow filaDoc in dtLineasFras.Rows)
                {
                    linCodPro= filaDoc["CODPRO"].ToString().Trim();
                    linCodArt = filaDoc["CODART"].ToString().Trim();
                    linCodLin = filaDoc["IDLIN"].ToString();
                    linCodLin = linCodLin.Replace(",0000", "");

                    if (linCodPro == "6173")
                    {
                        string para="";
                    }

                    foreach (DataRow filaRecod in dtRecodificacion.Rows)
                    {
                        recodCodPro = filaRecod["CODPRO"].ToString();
                        recodCodArt = filaRecod["CODART"].ToString();
                        recodNewCodArt = filaRecod["NEWCODART"].ToString().PadLeft(15);
                        recodNewDescart = filaRecod["NEWDESCART"].ToString();

                        if (linCodPro == recodCodPro && linCodArt == recodCodArt)
                        {
                            //actualizar codigo linea
                            sqlscript = "UPDATE LINEFACT SET CODART='" + recodNewCodArt + "', DESCLIN='" + recodNewDescart + "' WHERE IDLIN=" + linCodLin;
                            sql.actualizarCampo(sqlscript);
                        }
                    }
                }


                foreach (DataRow filaRecodArt in dtRecodificacion.Rows)
                {
                    recodCodArt = filaRecodArt["NEWCODART"].ToString();
                    recodNewDescart = filaRecodArt["NEWDESCART"].ToString();

                    foreach (DataRow filaArt in dtArticulos.Rows)
                    {
                        linCodArt=filaArt["CODART"].ToString().Trim();

                        if (recodCodArt == linCodArt)
                        {
                            sqlscript = "UPDATE ARTICULO SET DESCART= '" + recodNewDescart + "' WHERE LTRIM(CODART)='" + recodCodArt + "'";
                            sql.actualizarCampo(sqlscript);
                        }


                    }





                }




                MessageBox.Show("SE han actualizado " + dtLineasFras.Rows.Count + " filas");




            }
            catch(Exception ex) { }
        }
    }
}
