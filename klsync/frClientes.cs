﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;

namespace klsync
{
    public partial class frClientes : Form
    {
        csMySqlConnect mySql = new csMySqlConnect();
        private static frClientes m_FormDefInstance;
        public static frClientes DefInstance
        {
            get
            {
                if (m_FormDefInstance == null || m_FormDefInstance.IsDisposed)
                    m_FormDefInstance = new frClientes();
                return m_FormDefInstance;
            }
            set
            {
                m_FormDefInstance = value;
            }
        }
        public frClientes()
        {
            InitializeComponent();

            cboxClientes.SelectedIndex = 0;
            cargarClientesA3();
        }

        private void changeColor(DataGridView dgv)
        {
            if (cboxClientes.Text != "En Prestashop")
            {
                if (cboxClientes.Text != "Repetidos")
                {
                    foreach (DataGridViewRow row in dgv.Rows)
                    {
                        if (row.Cells["BLOQUEADO"].Value.ToString() == "T")
                        {
                            row.DefaultCellStyle.ForeColor = Color.Red;
                        }
                    }
                }
            }
        }

       

        private void cargarClientesA3()
        {
            int MAL_FORMATEADOS = 3;
            int REPETIDOS = 4;
            int ID_INCORRECTAS = 5;

            int tipoCargaClientes = 0;

            string filtroEstadoSincro = "TODOS";
            string filtroSincronizar = "TODOS";
            string filtroEstadoBloqueo = "TODOS";

            try
            {
                Stopwatch wc = new Stopwatch();
                SqlConnection dataConnection = new SqlConnection();
                dataConnection.ConnectionString = csGlobal.cadenaConexion;

                dataConnection.Open();

                csSqlScripts sqlScript = new csSqlScripts();
                SqlDataAdapter a = null;


                filtroEstadoSincro = rbEnPSSi.Checked ? "SI" : filtroEstadoSincro; 
                filtroEstadoSincro = rbEnPSNo.Checked ? "NO" : filtroEstadoSincro;

                filtroSincronizar = rbSincSi.Checked ? "SI" : filtroSincronizar;
                filtroSincronizar = rbSincNo.Checked ? "NO" : filtroSincronizar;

                filtroEstadoBloqueo = rbBlockSi.Checked ? "SI" : filtroEstadoBloqueo;
                filtroEstadoBloqueo = rbBlockNo.Checked ? "NO" : filtroEstadoBloqueo;

                tipoCargaClientes = cboxClientes.Text == "Mal formateados" ? MAL_FORMATEADOS : tipoCargaClientes; //3
                tipoCargaClientes = cboxClientes.Text == "Repetidos" ? REPETIDOS : tipoCargaClientes; //4
                tipoCargaClientes = cboxClientes.Text == "Id incorrectas" ? ID_INCORRECTAS : tipoCargaClientes; //5

                a = new SqlDataAdapter(sqlScript.selectClientesA3(filtroEstadoSincro,tipoCargaClientes, tbSearch.Text,filtroSincronizar,filtroEstadoBloqueo), dataConnection);

                if (cboxClientes.Text != "En Prestashop")
                {
                    DataTable t = new DataTable();
                    
                    a.Fill(t);
                    csUtilidades.addDataSource(dgvClientes, t);
                    toolStripButtonExportar.Enabled = true;

                }
                else
                {
                    string kls_payment_method = "";
                    bool existe_kls_payment_method = csUtilidades.existeColumnaMySQL(csGlobal.conexionDB, "ps_customer", "kls_payment_method");
                    if (existe_kls_payment_method) kls_payment_method = ", kls_payment_method as metodo_de_pago ";
                    csMySqlConnect mysql = new csMySqlConnect();
                    csUtilidades.addDataSource(dgvClientes, mysql.cargarTabla("select id_customer, company, firstname, lastname, email, active, kls_a3erp_id as codcli " + kls_payment_method + " from ps_customer"));

                    for (int i = 0; i < dgvClientes.Columns.Count; i++)
                    {
                        dgvClientes.Columns[i].ReadOnly = true;
                    }
                    toolStripButtonExportar.Enabled = false;
                }

                if (cboxClientes.Text == "Repetidos")
                    toolStripButtonExportar.Enabled = false;

                if (rbLoadA3ERP.Checked && cboxClientes.Text != "Repetidos")
                {
                    changeColor(dgvClientes);
                }

                csUtilidades.contarFilasGrid((DataTable)dgvClientes.DataSource, wc, toolStripStatusLabelClientesContadorFilas);
                dgvClientes.Columns["CODCLI"].ReadOnly = true;

            }
            catch (InvalidOperationException ex) { }
        }

        private void btCargarClientesA3_Click(object sender, EventArgs e)
        {
            cargarClientesA3();
            dgvClientes.Columns["CODCLI"].ReadOnly = true;
        }


        private void btExportToPS_Click(object sender, EventArgs e)
        {
            if (dgvClientes.Rows.Count > 0)
            {
                exportarClientesPS();
                cargarClientesA3();
            }
        }


        private void exportarClientesPS()
        {
            csPSWebService psWebService = new csPSWebService();

            try
            {
                List<string> clientesQueYaExisten = new List<string>();
                csMySqlConnect mysql = new csMySqlConnect();
                string clientes = "Los clientes siguientes ya existen en la tienda: \n\n";

                //DataTable dt = csUtilidades.dgv2dtSelectedRows(dgvClientesA3);

                try
                {
                    foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                    {
                        //Valido cada fila 

                        if (!validarDatosCliente(fila.Cells["CODCLI"].Value.ToString(), fila.Cells["RAZON"].Value.ToString(), fila.Cells["KLS_CLIENTE_TIENDA"].Value.ToString(), fila.Cells["KLS_CODCLIENTE"].Value.ToString(), fila.Cells["KLS_NOM_USUARIO"].Value.ToString(), fila.Cells["KLS_APELL_USUARIO"].Value.ToString(), fila.Cells["BLOQUEADO"].Value.ToString(), fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString()))
                        {
                            continue;   //Si algún valor no es válido, salto al siguiente registro
                        }
                        
                        string kls_codcliente = fila.Cells["KLS_CODCLIENTE"].Value.ToString();
                        // Si el KLS_CODCLIENTE está vacío o si KLS_EMAIL_USUARIO contiene una arroba
                        //if (kls_codcliente == "" && csUtilidades.emailIsValid(fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString()))// && fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString().Contains("@"))
                        string email = fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString();

                        //if (kls_codcliente == "" && csUtilidades.emailIsValid(fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString()))
                        //{
                        //    string existeEmail = "select count(email) from ps_customer where email = '" + fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString() + "'";
                        //    string existeCodigoCliente = "select count(kls_a3erp_id) from ps_customer where kls_a3erp_id = '" + fila.Cells["CODCLI"].Value.ToString().Trim() + "'";
                        //    if (!mysql.existeCampo(existeEmail) || !mysql.existeCampo(existeCodigoCliente))
                        //    {
                        //        psWebService.cdPSWebService("POST", "customers", "", fila.Cells["CODCLI"].Value.ToString(), true, ""); //en minúsculas el objeto
                        //    }
                        //    else
                        //    {
                        //        clientesQueYaExisten.Add(fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString());
                        //    }
                        //}else if(!csUtilidades.emailIsValid(fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString()))
                        //{ MessageBox.Show("El formato del email no es valido"); }

                        // Si el KLS_CODCLIENTE está vacío y el email es válido, creamos el cliente.
                        if (string.IsNullOrEmpty(kls_codcliente) && csUtilidades.emailIsValid(email))
                        {
                            string existeEmail = "select count(email) from ps_customer where email = '" + email + "'";
                            string existeCodigoCliente = "select count(kls_a3erp_id) from ps_customer where kls_a3erp_id = '" + fila.Cells["CODCLI"].Value.ToString().Trim() + "'";

                            if (!mysql.existeCampo(existeEmail) || !mysql.existeCampo(existeCodigoCliente))
                            {
                                // Crear nuevo cliente en Prestashop
                                psWebService.cdPSWebService("POST", "customers", "", fila.Cells["CODCLI"].Value.ToString(), true, "");
                            }
                            else
                            {
                                clientesQueYaExisten.Add(email);
                            }
                        }
                        // Si el KLS_CODCLIENTE ya existe, verificamos si el cliente está en Prestashop para actualizarlo.
                        //else if (!string.IsNullOrEmpty(kls_codcliente) && csUtilidades.emailIsValid(email))
                        //{
                        //    string existeCodigoCliente = "select kls_a3erp_id from ps_customer where kls_a3erp_id = '" + fila.Cells["CODCLI"].Value.ToString().Trim() + "'";
                        //    if (mysql.existeCampo(existeCodigoCliente))
                        //    {
                        //        // Actualizar el cliente en Prestashop
                        //        psWebService.cdPSWebService("PUT", "customers", "", fila.Cells["CODCLI"].Value.ToString(), false, "");
                        //    }
                        //}
                        //else if (!csUtilidades.emailIsValid(email))
                        //{
                        //    MessageBox.Show("El formato del email no es valido");
                        //}



                    }

                    if (clientesQueYaExisten.Count > 0)
                    {
                        foreach (var item in clientesQueYaExisten)
                        {
                            clientes += ">> " + item + "\n";
                        }

                        MessageBox.Show(clientes);

                        if (MessageBox.Show("¿Sincronizar clientes?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            syncBetweenPSA3_by_Email();

                            MessageBox.Show("Clientes sincronizados con éxito");
                        }
                    }
                }
                catch (Exception ex) { }
            }
            catch (Exception ex) { }
        }

        private bool validarDatosCliente(string codCliente, string nombreCliente, string traspasarCliente, string codClientePS, string nombre, string apellido, string bloqueado, string email )
        {
            bool clienteValido = true;
            string errorMessage = "";

            errorMessage = string.IsNullOrEmpty(nombre) ? errorMessage + "El Nombre del Usuario no está informado" + "\n" : errorMessage;
            errorMessage = string.IsNullOrEmpty(apellido) ? errorMessage + "El Apellido del Usuario no está informado" + "\n" : errorMessage;
            errorMessage = string.IsNullOrEmpty(traspasarCliente) ? errorMessage + "Debe marcar 'T' en el cliente para traspasarlo a Prestashop" + "\n" : errorMessage;
            errorMessage = string.IsNullOrEmpty(email) ? errorMessage + "El email del Usuari no está informado" + "\n" : errorMessage;

            if (!string.IsNullOrEmpty(errorMessage))
            {
                MessageBox.Show(errorMessage,"Error en datos");
                clienteValido = false;
            }

            return clienteValido;


        }

        private void rbCodigo_CheckedChanged(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void rbTodos_CheckedChanged(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void rbNoCode_CheckedChanged(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void rbMal_CheckedChanged(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void frClientes_Load(object sender, EventArgs e)
        {
            //Inicializo el tamñao de la barra de buscador
            asignarGrupoClienteToolStripMenuItem.Visible = false;
            cargarDatosDefault();
            cargarGruposCliente();
            splitContainer1.SplitterDistance = 30;
            splitContainer1.FixedPanel = FixedPanel.Panel1;
            hideShowElements(false);
            //al estar incluido en un contenedor, hay que volver a indicar que se adapte
            splitContainer1.Dock = DockStyle.Fill;
            cargarClientesA3();
            loadAddresses(true, "");
            //cargarDirA3();
            //cargarDirPS();
        }

        private void cargarDatosDefault()
        {

            string grupoDefault = csGlobal.defCustomerGroup;
            string script="";
            script= "select " +
                    " concat(id_group,' - ', name) from ps_group_lang " +
                    " where id_lang=" + csGlobal.defaultIdLangPS + " and  id_group=" + csGlobal.defCustomerGroup;

            csMySqlConnect mySql = new csMySqlConnect();

            tbDefaultCustomerGroup.Text = mySql.obtenerDatoFromQuery(script);
        
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //Program.clientes();
        }

        private void ponerANULLElKLSCODCLIENTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ponerANULL();
        }

        public void ponerANULL()
        {
            csSqlConnects sql = new csSqlConnects();

            foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
            {
                sql.actualizarANull("__CLIENTES", "KLS_CODCLIENTE", " WHERE KLS_CODCLIENTE='" + fila.Cells["KLS_CODCLIENTE"].Value.ToString() + "'");
            }

            cargarClientesA3();
        }

        private void activarEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarClientesEnTienda(true);
            cargarClientesA3();
        }

        private void desactivarEnTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            activarClientesEnTienda(false);
            cargarClientesA3();
        }

        private void activarClientesEnTienda(bool activar)
        {
            bool activarClientes = false;

            if (activar)
            {
                activarClientes = true;
            }

            csSqlConnects sqlConector = new csSqlConnects();
            DataTable Tabla = new DataTable();
            Tabla.Columns.Add("Articulo");

            for (int i = 0; i < dgvClientes.SelectedRows.Count; i++)
            {
                Tabla.Rows.Add(dgvClientes.SelectedRows[i].Cells.ToString());
            }

            sqlConector.actualizarClienteA3(Tabla, activarClientes);
        }

        private void btnCargarDirPS_Click(object sender, EventArgs e)
        {
            //cargarDirPS();
        }

        //private void cargarDirPS()
        //{
        //    csMySqlConnect mysql = new csMySqlConnect();
        //    csSqlScripts scripts = new csSqlScripts();

        //    switch (rbTodas.Checked)
        //    {
        //        case true:
        //            dgvDirPS.DataSource = mysql.cargarTabla(scripts.selectDireccionesPS(true));
        //            break;
        //        case false:
        //            dgvDirPS.DataSource = mysql.cargarTabla(scripts.selectDireccionesPS(false));
        //            break;
        //    }
        //}

        private void pasarCódigoAA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //pasarCodigoAA3();
        }

        //private void pasarCodigoAA3()
        //{
        //    string id_customer = "";
        //    string id_address = "";
        //    if (rbUnicas.Checked)
        //    {
        //        foreach (DataGridViewRow fila in dgvDirPS.Rows)
        //        {
        //            id_customer = fila.Cells["id_customer"].Value.ToString();
        //            id_address = fila.Cells["id_address"].Value.ToString();

        //            updateCodigoA3(id_customer, id_address);
        //        }
        //    }
        //}

        private void updateCodigoA3(string id_customer, string id_address)
        {
            csSqlConnects sql = new csSqlConnects();
            sql.actualizarCampo(
                                "UPDATE D " +
                                    " SET ID_PS = " + id_address +
                                " FROM " +
                                    " DIRENT D INNER JOIN __CLIENTES C" +
                                    " ON D.CODCLI = C.CODCLI " +
                                " WHERE " +
                                    " C.KLS_CODCLIENTE = " + id_customer);
        }

        private void btnCargarDirA3_Click(object sender, EventArgs e)
        {
            //cargarDirA3();
        }

        //private void cargarDirA3()
        //{
        //    bool check = false;
        //    csSqlConnects sql = new csSqlConnects();
        //    csSqlScripts scripts = new csSqlScripts();

        //    if (cbIDPS.Checked)
        //        check = true;

        //    dgvDirA3.DataSource = sql.cargarDatosTablaA3(scripts.selectDireccionesA3(check));
        //}

        //private void dgvDirA3_DoubleClick(object sender, EventArgs e)
        //{
        //    cambiarCodigoIdPsEnA3();
        //    cargarDirA3();
        //}

        //private void cambiarCodigoIdPsEnA3()
        //{
        //    csSqlConnects sql = new csSqlConnects();

        //    if (dgvDirA3.Rows.Count > 0)
        //    {
        //        string codigo = Microsoft.VisualBasic.Interaction.InputBox("Código ID_PS: ", "Introduce el código", "");
        //        if (!string.IsNullOrEmpty(codigo))
        //        {
        //            string iddirent = Math.Round(Convert.ToDouble(dgvDirA3.SelectedRows.Cells["IDDIRENT"].Value.ToString())).ToString();
        //            sql.actualizarCampo("UPDATE DIRENT SET ID_PS = " + codigo + " WHERE IDDIRENT = " + iddirent);
        //        }
        //    }
        //}

        //private void cbIDPS_Click(object sender, EventArgs e)
        //{
        //    cargarDirA3();
        //}

        //private void rbTodas_CheckedChanged(object sender, EventArgs e)
        //{
        //    cargarDirPS();
        //}

        //private void rbUnicas_CheckedChanged(object sender, EventArgs e)
        //{
        //    cargarDirPS();
        //}

        private void actualizarCamposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvClientes.SelectedRows.Count > 0)
                actualizarCamposClientes();
            cargarClientesA3();
        }

        private void actualizarCamposClientes()
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();

                foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                {
                    string razon = fila.Cells["RAZON"].Value.ToString().Trim();
                    string codcli = fila.Cells["CODCLI"].Value.ToString().Trim();
                    string nifcli = fila.Cells["NIFCLI"].Value.ToString().Replace(".", "").Replace("-", "");
                    string emaildocs = fila.Cells["E_MAIL_DOCS"].Value.ToString().Replace(" ", "");
                    string email = fila.Cells["KLS_EMAIL_USUARIO"].Value.ToString().Replace(" ", "");

                    if (csGlobal.conexionDB.Contains("THAGSON"))
                    {
                        if (emaildocs == "" || !csUtilidades.emailIsValid(emaildocs))
                            email = "'cliente" + codcli + "@thagson.com'";
                        else
                            email = "E_MAIL_DOCS";
                    }
                    else
                        //email = "E_MAIL_DOCS";
                    razon = razon.Replace(",", "").Replace(".", "").Replace("-", " ").Trim().Replace("(", "").Replace(")", "").Replace("'", "");
                    string lcFirst = razon;
                    string lcStart = razon;
                    string lcRest = razon;
                    int lnSpace = lcStart.IndexOf(' ');
                    if (lnSpace > -1)
                    {
                        lcFirst = lcStart.Substring(0, lnSpace).ToUpper();
                        lcRest = lcStart.Substring(lnSpace + 1).ToUpper();
                    }

                    lcFirst = csUtilidades.subString(lcFirst, 30);
                    lcRest = csUtilidades.subString(lcRest, 30);
                    email = csUtilidades.subString(email, 125);

                    sql.actualizarCampo(" UPDATE __CLIENTES " +
                                        " SET KLS_NOM_USUARIO = '" + lcFirst + "', KLS_PASS_INICIAL = 'SECRETO', " +
                                        " KLS_APELL_USUARIO = '" + lcRest + "', " +
                                        " E_MAIL_DOCS = " + email + ", " +
                                        " KLS_CLIENTE_TIENDA = 'T' , " +
                                        " KLS_EMAIL_USUARIO = " + email +
                                        " FROM __CLIENTES " +
                                        " WHERE LTRIM(CODCLI) = '" + codcli + "'");
                }
            }
            catch (Exception ex)
            {

            }
        }

        //private void dgvClientesA3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    string celda = dgvClientesA3.SelectedCells.Value.ToString();
        //    int indiceColumna = dgvClientesA3.CurrentCell.ColumnIndex;
        //    string nombreColumna = dgvClientesA3.Columns[indiceColumna].Name;
        //    int indicecodCli = dgvClientesA3.SelectedCells.RowIndex;
        //    string codCli = dgvClientesA3.Rows[indicecodCli].Cells.Value.ToString();

        //    csSqlConnects sql = new csSqlConnects();

        //    if (dgvClientesA3.SelectedCells.Count == 1)
        //    {
        //        string codigo = Microsoft.VisualBasic.Interaction.InputBox("Cambiar el valor " + celda + " por: ", "Introduce el nuevo valor", "");
        //        if (!string.IsNullOrEmpty(codigo))
        //            sql.actualizarCampo("UPDATE __CLIENTES SET " + nombreColumna + " = '" + codigo + "' WHERE LTRIM(CODCLI) = '" + codCli.Trim() + "'");
        //    }

        //    cargarClientesA3();
        //}

        private void corregirCorreosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                csSqlConnects sql = new csSqlConnects();
                DataTable dt = new DataTable();

                dt = sql.obtenerDatosSQLScript("SELECT CODCLI, E_MAIL_DOCS, NIFCLI FROM CLIENTES WHERE E_MAIL_DOCS IS NOT NULL");

                string email = "";

                foreach (DataRow dr in dt.Rows)
                {
                    //if (dr["E_MAIL_DOCS"].ToString().Contains(","))
                    //{
                    //    email = dr["E_MAIL_DOCS"].ToString().Split(',');
                    //    sql.actualizarCampo("UPDATE __CLIENTES SET E_MAIL_DOCS = '" + email + "' WHERE LTRIM(CODCLI) = '" + dr["CODCLI"].ToString().Trim() + "'");
                    //}
                    //else if (dr["E_MAIL_DOCS"].ToString().Contains(";"))
                    //{
                    //    email = dr["E_MAIL_DOCS"].ToString().Split(';');
                    //    sql.actualizarCampo("UPDATE __CLIENTES SET E_MAIL_DOCS = '" + email + "' WHERE LTRIM(CODCLI) = '" + dr["CODCLI"].ToString().Trim() + "'");
                    //}

                    //sql.actualizarCampo("UPDATE __CLIENTES SET E_MAIL_DOCS = '" + dr["E_MAIL_DOCS"].ToString().ToLower().Trim() + "' WHERE LTRIM(CODCLI) = '" + dr["CODCLI"].ToString().Trim() + "'");
                    ////sql.actualizarCampo("UPDATE __CLIENTES SET NIFCLI");
                }

                MessageBox.Show("Emails corregidos con éxito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo ha salido mal.\n\n" + ex.ToString());
                Program.guardarErrorFichero(ex.ToString());
            }
        }

        private void sincronizarIDsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Para actualizar los IDs en A3ERP los usuarios deben tener el mismo email tanto en A3ERP como en Prestashop y el External code de prestashop debe coincidir con el Código cliente en A3ERP.");
            syncBetweenPSA3_by_Email();
            //sincronizarIDsToolStripMenuItem.Enabled = false;
            //toolStripProgressBar.Style = ProgressBarStyle.Marquee;
            //toolStripProgressBar.MarqueeAnimationSpeed = 50;

            //BackgroundWorker bw = new BackgroundWorker();
            //bw.DoWork += bw_DoWork2;
            //bw.RunWorkerCompleted += bw_RunWorkerCompleted2;
            //bw.RunWorkerAsync();
        }

        void bw_DoWork2(object sender, DoWorkEventArgs e)
        {
            syncBetweenPSA3_by_Email();
        }

        void bw_RunWorkerCompleted2(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripProgressBar.MarqueeAnimationSpeed = 0;
            toolStripProgressBar.Style = ProgressBarStyle.Blocks;
            toolStripProgressBar.Value = toolStripProgressBar.Maximum;

            sincronizarIDsToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// Sincronizar por email o por nif
        /// </summary>
        /// <param name="email"></param>
        private void syncBetweenPSA3_by_Email()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();

            bool check = false;
            string consulta = "SELECT * " +
                                " FROM information_schema.COLUMNS " +
                                " WHERE " +
                                " TABLE_SCHEMA = '" + csGlobal.databasePS + "' " +
                                " AND TABLE_NAME = 'ps_customer' " +
                                " AND COLUMN_NAME = 'kls_a3erp_id'";

            if (mysql.comprobarConsulta(consulta))
            {
                check = true;
            }

            if (check)
            {
                csUtilidades.ejecutarConsulta("UPDATE CLIENTES SET KLS_CODCLIENTE=NULL",false);
                // bajamos los de ps a a3
                DataTable ps_final = mysql.cargarTabla("select id_customer as ID,email as EMAIL, ltrim(kls_a3erp_id) as CODCLI from ps_customer where kls_a3erp_id>0");
                DataTable a3_final = sql.cargarDatosTablaA3("SELECT KLS_CODCLIENTE as ID, KLS_EMAIL_USUARIO as EMAIL, LTRIM(CODCLI) as CODCLI FROM __CLIENTES");
                int contador = 0;

                foreach (DataRow ps in ps_final.Rows)
                {
                    foreach (DataRow a3 in a3_final.Rows)
                    {
                        string email_ps = ps["EMAIL"].ToString();
                        string email_a3 = a3["EMAIL"].ToString();
                        string codcli_ps = ps["CODCLI"].ToString().Trim();
                        string id_ps = ps["ID"].ToString();

                        if (email_ps == email_a3)
                        {
                            sql.actualizarCampo("UPDATE __CLIENTES SET KLS_CODCLIENTE = '" + id_ps
                                + "' WHERE KLS_EMAIL_USUARIO = '" + email_ps + "' AND LTRIM(CODCLI) = '" + codcli_ps + "'");
                            sql.actualizarCampo("UPDATE CLIENTES SET KLS_CODCLIENTE = '" + id_ps
                                + "' WHERE KLS_EMAIL_USUARIO = '" + email_ps + "' AND LTRIM(CODCLI) = '" + codcli_ps + "'");


                            contador += 1;

                            break;
                        }
                        
                    }
                }

                cargarClientesA3();
                MessageBox.Show("Clientes de A3 sincronizados con la tienda \n Se han actualizado: "+contador+" clientes" );

            }
            else
            {
                MessageBox.Show("No tiene instalado el módulo KLS eSync Clientes");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            esyncro.csSyncAddresses syncAddress = new esyncro.csSyncAddresses();
            syncAddress.actualizarDirección("1", "1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            esyncro.csSyncAddresses syncAddress = new esyncro.csSyncAddresses();
            syncAddress.actualizarInfoDirecciones("52");
        }

        private void dgvClientesA3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            loadCustomers(rbLoadA3ERP.Checked, tbSearch.Text);

        }

        private void cargarGruposCliente()
        {
            DataTable dt = new DataTable();
            dt = mySql.cargarTabla("select id_group,name from ps_group_lang where id_lang=" + csGlobal.defaultIdLangPS);
            foreach (DataRow dr in dt.Rows)
            {
                tscboxCustomerGroup.Items.Add(dr["name"] + "-" + dr["id_group"]);
            }
        }

        private void loadAddresses(bool A3ERP, string customer)
        {
            csSqlConnects sql = new csSqlConnects();
            string script = "";
            string anexo="";

            if (A3ERP)
            {
                if (customer != "")
                { 
                    anexo="WHERE LTRIM(CODCLI)='" + customer + "'";
                }
                script = "SELECT CODCLI, CODPAIS, CODPROVI, CODREP, DEFECTO, DIRENT1, DIRENT2, DTOENT, EMAIL, IDDIRENT, NOMENT, NUMDIR, POBENT, TELENT1, ID_PS FROM DIRENT " + anexo;
                dgvDirecciones.DataSource = sql.cargarDatosTablaA3(script);
            }

            if (!A3ERP)
            {
                anexo = "where id_customer=" + customer;

                string query = "SELECT " +
                    " id_address, id_country, id_state, id_customer, alias, company, lastname, firstname, " +
                    " address1, address2, postcode, city, phone, phone_mobile, vat_number, dni, date_add, date_upd, " +
                    " active, deleted, kls_id_dirent " +
                    " FROM ps_address " + anexo;

                csUtilidades.cargarDGV(dgvDirecciones, query, true);
            }
        }


        private void loadCustomers(bool A3ERP, string searchText)
        {
            try
            {
               csSqlConnects sql = new csSqlConnects();
               
               if (!A3ERP)
               {
                    string query = "select id_customer,firstname,lastname,company,ps_customer.id_default_group as IdGrupo,name as Grupo, " +
                        " kls_a3erp_id as CodA3ERP,kls_a3erp_fam_id as FAMILIA_A3ERP,kls_payment_method " +
                        " from " +
                        " ps_customer inner join ps_group_lang " +
                        " on ps_customer.id_default_group=ps_group_lang.id_group " +
                        " where ps_group_lang.id_lang=" + csGlobal.defaultIdLangPS + " and " +
                        " (id_customer like '%" + searchText + "%' or " +
                        " firstname like '%" + searchText + "%' or " +
                        " lastname like '%" + searchText + "%' or " +
                        " company like '%" + searchText + "%' or " +
                        " id_shop_group  like '%" + searchText + "%' or " +
                        " name  like '%" + searchText + "%' or " +
                        " kls_a3erp_id    like '%" + searchText + "%' or " +
                        " kls_a3erp_fam_id   like '%" + searchText + "%' or " +
                        " kls_payment_method  like '%" + searchText + "%')";

                    csUtilidades.cargarDGV(dgvClientes, query, true);
                }
                else
                {
                    cargarClientesA3();
                }

            }
            catch (Exception ex)
            {

            }



        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                csSqlConnects sql = new csSqlConnects();
                string search = Microsoft.VisualBasic.Interaction.InputBox("", "Búsqueda", "");
                if (search != "")
                {
                    dgvClientes.DataSource = sql.cargarDatosTablaA3("SELECT CODCLI, RAZON, NIFCLI, KLS_EMAIL_USUARIO, CONTACTO, KLS_CODCLIENTE, " +
                        " KLS_CLIENTE_TIENDA, KLS_NOM_USUARIO, KLS_APELL_USUARIO, E_MAIL_DOCS, BLOQUEADO FROM dbo.CLIENTES WHERE " +

                        " E_MAIL_DOCS LIKE '%" + search + "%' OR " +
                        " NIFCLI LIKE '%" + search + "%' OR " +
                        " KLS_EMAIL_USUARIO LIKE '%" + search + "%' OR " +
                        " CODCLI LIKE '%" + search + "%' OR " +
                        " CONTACTO LIKE '%" + search + "%' OR" +
                        " KLS_NOM_USUARIO LIKE '%" + search + "%' OR" +
                        " KLS_APELL_USUARIO LIKE '%" + search + "%' OR" +
                        " RAZON LIKE '%" + search + "%' " +

                        " ORDER BY E_MAIL_DOCS desc");
                }
                else
                {
                    cargarClientesA3();
                }

                changeColor(dgvClientes);

                return true;
            }
            else if (keyData == (Keys.F1))
            {
                //Ayuda.frAyudaImagenes ayuda = new Ayuda.frAyudaImagenes();
                //ayuda.Show();
                //ayuda.WindowState = FormWindowState.Maximized;
            }
            else if (keyData == (Keys.F5))
            {
                cargarClientesA3();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void toolStripButtonBusqueda_Click(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void toolStripButtonExportar_Click(object sender, EventArgs e)
        {
            if (dgvClientes.SelectedRows.Count > 0)
            {
                toolStripButtonExportar.Enabled = false;
                toolStripProgressBar.Style = ProgressBarStyle.Marquee;
                toolStripProgressBar.MarqueeAnimationSpeed = 50;

                BackgroundWorker bw = new BackgroundWorker();
                bw.DoWork += bw_UploadClients;
                bw.RunWorkerCompleted += bw_UploadClientesCompleted;
                bw.RunWorkerAsync();
               
            }
        }

        void bw_UploadClients(object sender, DoWorkEventArgs e)
        {
            if (dgvClientes.Rows.Count > 0)
            {
                exportarClientesPS();
                cargarClientesA3();
                loadCustomers(rbLoadA3ERP.Checked, tbSearch.Text);
            }
        }

        void bw_UploadClientesCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripProgressBar.MarqueeAnimationSpeed = 0;
            toolStripProgressBar.Style = ProgressBarStyle.Blocks;
            toolStripProgressBar.Value = toolStripProgressBar.Maximum;


            toolStripButtonExportar.Enabled = true;
        }

        private void toolStripButtonAyudaClientes_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SendKeys.SendWait("{F1}");
        }

        private void cboxClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarClientesA3();
        }

        private void toolStripButtonBusquedaClientes_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.SendKeys.SendWait("^f");
        }

        private void dgvClientesA3_CellEndEdit_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Estas seguro/a?", "ATENCION", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string celda_a_cambiar = dgvClientes.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    string codcli = dgvClientes.Rows[e.RowIndex].Cells.ToString().Trim();
                    string cabecera = dgvClientes.Columns[e.ColumnIndex].Name.ToString();

                    csUtilidades.ejecutarConsulta("update clientes set " + cabecera + " = '" + celda_a_cambiar + "' WHERE LTRIM(CODCLI) = '" + codcli + "'", false);

                    cargarClientesA3();
                }

            }
            catch (Exception ex) { Program.guardarErrorFichero(ex.ToString()); }
        }

        private void toolStripButtonExportarExcel_Click(object sender, EventArgs e)
        {
            if (dgvClientes.Rows.Count > 0)
                csUtilidades.DataTable2CSV((DataTable)dgvClientes.DataSource, ";", "Exportacion de clientes");
        }

        private void bloquearClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bloquearClientes("T");
        }

        private void bloquearClientes(string block = "T")
        {
            try
            {
                string idClientePS = "";
                string codcliA3 = "";
                string estadoBloqueo = block == "T" ? "0" : "1";
                string accion = block == "T" ? "bloquear" : "activar";
                if (dgvClientes.SelectedRows.Count > 0)
                {
                    if (MessageBox.Show("Deseas " + accion + " los clientes seleccionados?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (DataGridViewRow dr in dgvClientes.SelectedRows)
                        {
                            idClientePS = dr.Cells["KLS_CODCLIENTE"].Value.ToString().Trim();
                            codcliA3 = dr.Cells["CODCLI"].Value.ToString().Trim();

                            csUtilidades.ejecutarConsulta("UPDATE __CLIENTES SET BLOQUEADO = '" + block + "' WHERE LTRIM(CODCLI) = '" + codcliA3 + "'", false);

                            if (!string.IsNullOrEmpty(idClientePS))     //sólo actualizamos is el cliente está en la tienda
                            {
                                csUtilidades.ejecutarConsulta("update ps_customer set active=" + estadoBloqueo + " where id_customer=" + idClientePS, true);
                            }
                        }

                        cargarClientesA3();
                    }
                }
            }
            catch { }
        }

        private void desbloquearClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bloquearClientes("F");
        }





        private void dgvClientes_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvClientes.Columns[e.ColumnIndex].Name != "CODCLI")
                {
                    if (MessageBox.Show("Vas a actualizar datos. ¿Estas seguro/a?", "ATENCION", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string celda_a_cambiar = dgvClientes.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                        string codcli = dgvClientes.Rows[e.RowIndex].Cells["CODCLI"].Value.ToString().Trim();
                        string cabecera = dgvClientes.Columns[e.ColumnIndex].Name.ToString();
                        string id_customer = dgvClientes.Rows[e.RowIndex].Cells["KLS_CODCLIENTE"].Value.ToString().Trim();

                        //Verifico si el valor es numérico para ponerle las comillas
                        celda_a_cambiar = csUtilidades.comprobarEsNumero(celda_a_cambiar) ? celda_a_cambiar.Replace(",", ".") : "'" + celda_a_cambiar + "'";

                        csUtilidades.ejecutarConsulta("UPDATE __CLIENTES SET " + cabecera + " = " + celda_a_cambiar + " WHERE LTRIM(CODCLI) = '" + codcli + "'", false);

                        //Actualizo el valor en PS
                        if (cabecera == "KLS_PEDIDO_MINIMO" && !string.IsNullOrEmpty(id_customer))
                        {
                            csUtilidades.ejecutarConsulta("update ps_customer set purchase_minimum=" + celda_a_cambiar + " where id_customer=" + id_customer, true);
                        }

                        cargarClientesA3();
                    }
                }

            }
            catch (Exception ex) { }
        }

        private void actualizarPedidoMinimo(string id_cliente=null, string importePedidoMin = "0")
        {
            csUtilidades.ejecutarConsulta("update ps_customer set purchase_minimum=" + importePedidoMin + " where id_customer=" + id_cliente, true);

        }

        private void dgvClientesA3_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            changeColor(dgvClientes);
        }

        private void actualizarCodigoKlosionsTiendaToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if ("Estas seguro de querer sincronizar los códigos de klosions con los de a3?".what())
            {
                csUtilidades.ejecutarConsulta("update ps_customer set kls_a3erp_id = NULL where kls_a3erp_id = 0", true);

                csSqlConnects sql = new csSqlConnects();
                csMySqlConnect mysql = new csMySqlConnect();

                DataTable a3 = sql.cargarDatosTablaA3("select ltrim(codcli) as id, __clientes.E_MAIL_DOCS as email from __clientes where E_MAIL_DOCS <> ''");
                DataTable ps = mysql.cargarTabla("select distinct id_customer as id, email from ps_customer");
                Dictionary<string, string> final = new Dictionary<string, string>();

                foreach (DataRow a3_row in a3.Rows)
                {
                    foreach (DataRow ps_row in ps.Rows)
                    {
                        string email_ps = ps_row["email"].ToString();
                        string email_a3 = a3_row["email"].ToString();
                        string id_a3 = a3_row["id"].ToString();
                        string id_ps = ps_row["id"].ToString();

                        if (email_a3 == email_ps)
                        {
                            final.Add(id_a3, id_ps);
                            break;
                        }
                    }
                }

                foreach (KeyValuePair<string, string> entry in final)
                {
                    // do something with entry.Value or entry.Key
                    string consulta = string.Format("update ps_customer set kls_a3erp_id = {0} where id_customer = {1}", entry.Key, entry.Value);
                    csUtilidades.ejecutarConsulta(consulta, true);
                }

                "Sincronización finalizada".mb();
                cargarClientesA3();
            }
            else
            {
                csUtilidades.operacionCancelada();
            }
        }

        private void btHide_Click(object sender, EventArgs e)
        {
            ocultarPanelBusqueda();
            hideShowElements(false);
        }


        private void hideShowElements(bool ocultar)
        {
            rbLoadA3ERP.Visible = ocultar;
            rbLoadPS.Visible = ocultar;
            tbDefaultCustomerGroup.Visible = ocultar;
            tbSearch.Visible = ocultar;
            lbDefaultCustomers.Visible = ocultar;
            btAdvancedSearch.Visible = ocultar;
            lbFilterby.Visible = ocultar;
            cboxClientes.Visible = ocultar;
            tbSearch.Visible = ocultar;
        
        }

        private void ocultarPanelBusqueda()
        {
            splitContainer1.SplitterDistance = 30;
        }

        private void mostrarPanelBusqueda()
        {
            splitContainer1.SplitterDistance = 250;
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            mostrarPanelBusqueda();
            hideShowElements(true);
        }

        private void btAdvancedSearch_Click(object sender, EventArgs e)
        {
            loadCustomers(rbLoadA3ERP.Checked, tbSearch.Text);
            loadAddresses(rbLoadA3ERP.Checked, "");
        }

        private void actualizarGruposClientes()
        { 
             string customerGroup = "";
                string idCustomer="";
                string scriptGroups="";
                string scriptCustomer = "";

                DialogResult dialogResult = MessageBox.Show("¿Quieres asignar el grupo de clientes (" + tscboxCustomerGroup.Text + ") a los clientes seleccionados?", "Asignar Grupo de Cliente", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string[] group = tscboxCustomerGroup.Text.Split('-');
                    customerGroup = group[1];

                    foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                    {
                        idCustomer = fila.Cells["id_customer"].Value.ToString();

                        scriptGroups = "delete from ps_customer_group where id_customer=" + idCustomer;
                        csUtilidades.ejecutarConsulta(scriptGroups, true);

                        scriptGroups = "insert into ps_customer_group (id_customer,id_group) VALUES (" + idCustomer + "," + customerGroup + ") ON DUPLICATE KEY UPDATE id_group=" + customerGroup;
                        csUtilidades.ejecutarConsulta(scriptGroups, true);
                    
                        scriptCustomer= "update ps_customer set id_default_group=" + customerGroup + "  where id_customer=" + idCustomer;
                        csUtilidades.ejecutarConsulta(scriptCustomer, true);
                    }
                    contextClientes.Hide();
                    loadCustomers(rbLoadA3ERP.Checked,tbSearch.Text);

                }
                else if (dialogResult == DialogResult.No)
                {
                    //do something else
                }
        
        }



        private void tscboxCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (tscboxCustomerGroup.SelectedIndex != -1)
            {
                actualizarGruposClientes();
            
            }
        }

        private void rbLoadA3ERP_CheckedChanged(object sender, EventArgs e)
        {
            if (rbLoadA3ERP.Checked)
            {
                asignarGrupoClienteToolStripMenuItem.Visible = false;
                loadCustomers(rbLoadA3ERP.Checked,"");
                grBoxEnPrestashop.Visible = true;
                grBoxSincronizar.Visible = true;
                grBoxBloqueados.Visible = true;
            }
            else
            {
                asignarGrupoClienteToolStripMenuItem.Visible = true;
                loadCustomers(rbLoadA3ERP.Checked, "");
                grBoxEnPrestashop.Visible = false;
                grBoxSincronizar.Visible = false;
                grBoxBloqueados.Visible = false;
            }
        }

        private void asignarGrupoClienteToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {
            tscboxCustomerGroup.ComboBox.SelectedIndex = -1;
        }

        private void btClearTextSearch_Click(object sender, EventArgs e)
        {
            tbSearch.Text = "";
        }

  

        private void crearDireccionFromA3ERPToPS(DataGridViewSelectedRowCollection clientes, bool direccion=false)
        {
            string codCli = "";
            string idDireccion = "";

            if (dgvDirecciones.SelectedRows.Count == 1 && direccion)
            {
                idDireccion = dgvDirecciones.CurrentRow.Cells["IDDIRENT"].Value.ToString().Replace(",0000", "");
                codCli = dgvClientes.Rows[0].Cells["KLS_CODCLIENTE"].Value.ToString();
                esyncro.csSyncAddresses syncDirecciones = new esyncro.csSyncAddresses();
                syncDirecciones.crearDireccionesA3ToPS(codCli.Trim(),idDireccion);
            }
            else
            {
                if (dgvClientes.SelectedRows.Count != 1)
                {
                    MessageBox.Show("Debe seleccionar solo una direccion.");
                }
                else
                {
                    foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                    {
                        codCli = fila.Cells["KLS_CODCLIENTE"].Value.ToString();
                        esyncro.csSyncAddresses syncDirecciones = new esyncro.csSyncAddresses();
                        syncDirecciones.crearDireccionesA3ToPS(codCli.Trim());
                    }
                }
            }
        }

        private void crearDireccionesEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearDireccionFromA3ERPToPS(dgvClientes.SelectedRows);
        }

        private void dgvClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (rbLoadA3ERP.Checked)
                {
                    string customer = "";
                    customer = dgvClientes["CODCLI", e.RowIndex].Value.ToString();
                    customer = customer.Trim();
                    loadAddresses(rbLoadA3ERP.Checked, customer);
                }
                if (rbLoadPS.Checked)
                {
                    string customer = "";
                    customer = dgvClientes["id_customer", e.RowIndex].Value.ToString();
                    customer = customer.Trim();
                    loadAddresses(rbLoadA3ERP.Checked, customer);
                }
            }
        }

        private void crearClienteEnA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            crearClientesEnA3ERP(dgvClientes);
        }

        private void crearClientesEnA3ERP(DataGridView dgv)
        {
            csa3erp a3erpmain = new csa3erp();
            csa3erpTercero a3erp = new csa3erpTercero();


            Objetos.csTercero clientes = null;

            string codCli = "";
            string nomCli = "";
            string tipCli = "";

            try
            {
                    //31-03-2021 Sergi, modificación para poder pasar clientes de PS a A3 sin necessidad de que tengan registrada una dirección. Comprovación de si el cliente esta ya registrado con el mismo correo en A3. 

                    foreach (DataGridViewRow fila in dgv.SelectedRows)
                    {
                        codCli = fila.Cells["id_customer"].Value.ToString();
                        nomCli = fila.Cells["firstname"].Value.ToString() + " " + fila.Cells["lastname"].Value.ToString();
                        tipCli = fila.Cells["idGrupo"].Value.ToString();


                        string correoPS = mySql.obtenerDatoFromQuery("SELECT email FROM ps_customer WHERE id_customer='" + codCli + "'");
                        csSqlConnects sql = new csSqlConnects();
                        bool correoA3 = sql.consultaExiste("SELECT KLS_EMAIL_USUARIO FROM dbo.__CLIENTES where KLS_EMAIL_USUARIO='" + correoPS + "'");
                        //Si no existe ningun usuario en A3 con el mail del usuario que queremos pasar de PrestaShop entra en el condicional, sino nos avisa de que el cliente X ya existe y no lo crea
                        if (!correoA3)
                        {
                            DataTable idadress = mySql.obtenerDatosPS("SELECT ps_address.id_address FROM ps_address INNER JOIN ps_customer ON ps_address.id_customer=ps_customer.id_customer WHERE ps_customer.id_customer=" + codCli);
                            //Si el cliente no tiene dirección registrada:
                            if (!idadress.hasRows())
                            {
                                //Si se quiere pasar el cliente sin dirección igualmente solo cojerá datos de la table de ps_customer.
                                if (MessageBox.Show("El cliente " + codCli + " " + nomCli + " no tiene ninguna dirección registrada. ¿Quieres pasarlo de todos modos?", "ATENCIÓN", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    DataTable dt = mySql.obtenerDatosPS("select ps_customer.id_customer, ps_customer.kls_a3erp_fam_id as famcli,ps_customer.firstname, ps_customer.lastname, email, company" +
                                " from ps_customer " +
                                " where ps_customer.id_customer=" + codCli);

                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        clientes = new Objetos.csTercero();
                                        clientes.razonSocial = dr["company"].ToString();
                                        clientes.nomIC = dr["company"].ToString();
                                        clientes.nombre = dr["firstname"].ToString();
                                        clientes.apellidos = dr["lastname"].ToString();
                                        clientes.email = dr["email"].ToString();

                                        //Cambio estos campos para que nos ponga la dirección correctamente con su país correspondiente.
                                        //clientes.provincia = dr["id_state"].ToString();
                                        //clientes.pais = dr["id_country"].ToString();

                                        clientes.codIC = dr["id_customer"].ToString();
                                        clientes.famCliEsp = "";



                                        if (csGlobal.conexionDB.ToUpper().Contains("MIMASA") && !dr["id_customer"].ToString().Equals(""))
                                        {
                                            clientes.famCliEsp = dr["famcli"].ToString();
                                            clientes.carDos = dr["iso_code"].ToString();

                                        }
                                        csa3erpTercero tercero = new csa3erpTercero();



                                        a3erpmain.abrirEnlace();

                                        //No descomentar, sino se creara un cliente duplicado en A3
                                        //a3erp.crearClienteA3(clientes, null);

                                        //Se ejecuta el metodo de crear cliente y el return se asigna a una variable.
                                        string codcli = a3erp.crearClienteA3(clientes, null);
                                        // setIdPs(codcli, dr["id_address"].ToString());

                                        //tercero.actualidarIdDirecciones(codcli, dr["id_customer"].ToString(), dr["id_address"].ToString());
                                        a3erpmain.cerrarEnlace();
                                    }
                                }

                            }
                            //Si tienen dirección registrada se cojen datos de las tablas ps_customer, ps_address y ps_country
                            else
                            {
                                DataTable dt = mySql.obtenerDatosPS("select ps_customer.id_customer, ps_customer.kls_a3erp_fam_id as famcli,ps_customer.firstname, ps_customer.lastname, " +
                                    " email,ps_address.alias, ps_address.id_address ,ps_address.company,ps_address.address1, ps_address.address2, ps_address.postcode, ps_address.city, ps_address.phone, ps_address.phone_mobile, " +
                                    " ps_country.iso_code,ps_address.vat_number, ps_address.dni, ps_address.id_state,ps_address.id_country, ps_customer.firstname, ps_customer.lastname  " +
                                    " from ps_customer inner join ps_address on ps_address.id_customer=ps_customer.id_customer " +
                                    " JOIN ps_country on ps_country.id_country=ps_address.id_country " +
                                    " where ps_customer.id_customer=" + codCli + " and ps_address.active=1 and ps_address.deleted=0 order by ps_address.date_add LIMIT 1");

                                foreach (DataRow dr in dt.Rows)
                                {
                                    clientes = new Objetos.csTercero();
                                    clientes.nomIC = dr["company"].ToString().ToUpper();
                                    clientes.nombre = dr["firstname"].ToString();
                                    clientes.apellidos = dr["lastname"].ToString();
                                    clientes.razonSocial = dr["company"].ToString();
                                    clientes.nifcif = dr["dni"].ToString();
                                    clientes.telf = dr["phone"].ToString();
                                    clientes.movil = dr["phone_mobile"].ToString();
                                    clientes.email = dr["email"].ToString();
                                    clientes.direccion = dr["address1"].ToString().ToUpper();
                                    clientes.direccion2 = dr["address2"].ToString();
                                    clientes.codigoPostal = dr["postcode"].ToString();
                                    clientes.poblacion = dr["city"].ToString().ToUpper();
                                    clientes.provincia = dr["city"].ToString().ToUpper();

                                    //Cambio estos campos para que nos ponga la dirección correctamente con su país correspondiente.
                                    //clientes.provincia = dr["id_state"].ToString();
                                    //clientes.pais = dr["id_country"].ToString();

                                    clientes.pais = dr["iso_code"].ToString();

                                    clientes.codIC = dr["id_customer"].ToString();
                                    clientes.famCliEsp = "";

                                    clientes.idDireccionPS = dr["id_address"].ToString();

                                    if (csGlobal.conexionDB.ToUpper().Contains("MIMASA") && !dr["id_customer"].ToString().Equals(""))
                                    {
                                        clientes.famCliEsp = dr["famcli"].ToString();
                                        clientes.carDos = dr["iso_code"].ToString();

                                    }
                                    csa3erpTercero tercero = new csa3erpTercero();



                                    a3erpmain.abrirEnlace();

                                    //No descomentar, sino se creara un cliente duplicado en A3
                                    //a3erp.crearClienteA3(clientes, null);

                                    //Se ejecuta el metodo de crear cliente y el return se asigna a una variable.
                                    string codcli = a3erp.crearClienteA3(clientes, null);
                                    // setIdPs(codcli, dr["id_address"].ToString());

                                    tercero.actualidarIdDirecciones(codcli, dr["id_customer"].ToString(), dr["id_address"].ToString());

                                    a3erpmain.cerrarEnlace();
                                }

                            }
                        }
                        else   //En caso de que ya hubiera un cliente en A3 con el mismo correo que el cliente que se quiere traspassar desde PrestaShop, nos muestra este mensaje y no nos crea un cliente repetido.
                        {
                            MessageBox.Show("El clinte num: " + codCli + " " + nomCli + " ya existe en A3ERP", "¡AVISO!");
                        }
                    }
                }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error durante la creación de los clientes." + ex, "¡ERROR!");
            }
        }

        private void setIdPs(string codcli,string idPS)
        {
            DataTable dt = new DataTable();
            csSqlConnects sql = new csSqlConnects();
            dt = sql.cargarDatosTablaA3("SELECT IDDIRENT FROM DIRENT WHERE CODCLI='"+codcli+"'");
            sql.actualizarCampo("UPDATE DIRENT SET ID_PS=" + idPS + "WHERE IDDIRENT="+dt.Rows[0]["IDDIRENT"]);

        }
        private void pSPonerANULLCodClienteA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idCustomer = "";
            if (dgvClientes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                {
                    idCustomer = fila.Cells["id_customer"].Value.ToString();
                    csUtilidades.ejecutarConsulta("update ps_customer set kls_a3erp_id=null where id_customer=" + idCustomer, true);

                }
                loadCustomers(rbLoadA3ERP.Checked, tbSearch.Text);
                loadAddresses(rbLoadA3ERP.Checked, "");

            }
        }

        private void activarClienteParaPrestashopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string idCustomer = "";
            if (dgvClientes.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                {
                    idCustomer = fila.Cells["CODCLI"].Value.ToString().Trim();
                    csUtilidades.ejecutarConsulta("UPDATE __CLIENTES SET KLS_CLIENTE_TIENDA='T' WHERE LTRIM(CODCLI)='" + idCustomer + "'", false);

                }
                loadCustomers(rbLoadA3ERP.Checked, tbSearch.Text);
                loadAddresses(rbLoadA3ERP.Checked, "");

            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csa3erpDirecciones a3dir = new csa3erpDirecciones();
            csa3erp a3erp = new csa3erp();

            string direccion = "";
            string nombre_direccion = "";
            string codcli = "";
            string contacto = "";
            string ciudad = "";
            string zipcode = "";
            string id_ps = "";
            string email = "";
            string codProvincia = "";

            string idDireccionPS = "";
            string idDireccionA3 = "";
            string idClientePS = "";
            string idClienteA3 = "";


            foreach (DataGridViewRow fila in dgvDirecciones.SelectedRows)
            {
                //idDireccionPS = fila.Cells["id_address"].Value.ToString();
                idDireccionPS = fila.Cells["id_address"].Value.ToString();
                idClientePS = fila.Cells["id_customer"].Value.ToString();
                idDireccionA3 = sql.obtenerCampoTabla("SELECT CONVERT(INT, IDDIRENT) AS IDDIRECCION from DIRENT WHERE ID_PS=" + idDireccionPS);
                if (!string.IsNullOrEmpty(idDireccionA3))
                {
                    csUtilidades.ejecutarConsulta("update ps_address set kls_id_dirent='" + idDireccionA3 + "' where id_address =" + idDireccionPS, true);
                }
                else
                {
                    direccion = fila.Cells["address1"].Value.ToString().ToUpper();
                    DialogResult result = MessageBox.Show("¡La dirección: " + direccion + " no existe en A3ERp. ¿Desea crearla?", "Sincronización Direcciones", MessageBoxButtons.YesNoCancel);

                    if (result == DialogResult.Yes)
                    {
                        codcli = sql.obtenerCampoTabla("SELECT LTRIM(CODCLI) FROM __CLIENTES WHERE KLS_CODCLIENTE='" + idClientePS + "'");
                        nombre_direccion = fila.Cells["alias"].Value.ToString();
                        contacto = "";
                        ciudad = fila.Cells["city"].Value.ToString();
                        zipcode = fila.Cells["postcode"].Value.ToString();
                        id_ps = idDireccionPS;
                        email = "";
                        codProvincia = string.IsNullOrEmpty(zipcode) ? "": zipcode.Substring(0, 2);

                        a3erp.abrirEnlace();
                        a3dir.crearDireccion(direccion, nombre_direccion, codcli, contacto, ciudad, zipcode, id_ps, email,codProvincia);
                        a3erp.cerrarEnlace();
                    }

                }

            }
            MessageBox.Show("Proceso finalizado");
        }

        private void dgvDirecciones_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvDirecciones.Columns[e.ColumnIndex].Name.ToString()=="ID_PS")
                if (MessageBox.Show("Vas a actualizar datos de la dirección del cliente. ¿Estas seguro/a?", "ATENCION", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string celda_a_cambiar = dgvDirecciones.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    string idDirent = dgvDirecciones.Rows[e.RowIndex].Cells["IDDIRENT"].Value.ToString().Trim().Replace(",0000","");
                    string cabecera = dgvDirecciones.Columns[e.ColumnIndex].Name.ToString();

                    csUtilidades.ejecutarConsulta("UPDATE DIRENT SET ID_PS=" + celda_a_cambiar + " WHERE IDDIRENT = " + idDirent, false);

                    cargarClientesA3();
                }

            }
            catch (Exception ex) { }
        }

        private void crearDirecciónEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                crearDireccionFromA3ERPToPS(dgvClientes.SelectedRows,true);
            }
            catch(Exception ex)
            { }

        }

        private void marcarDirecciónFiscalEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Objetos.csDireccion direcciones = new Objetos.csDireccion();
            direcciones.actualizarDireccionesFiscales();
        }



        private void actualizarDireccionesFiscalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.csDireccion direcciones = new Objetos.csDireccion();
                if (MessageBox.Show("¿Quieres actualizar las direcciones fiscales en Prestashop?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    string idClientePS = dgvClientes.CurrentRow.Cells["KLS_CODCLIENTE"].Value.ToString();
                    if (!string.IsNullOrEmpty(idClientePS))
                    {
                        direcciones.actualizarDireccionesFiscales(idClientePS);
                    }
                        MessageBox.Show("direcciones sincronizadas con éxito");
                }
            }
            catch(Exception ex) { Program.guardarErrorFichero("Error: "+ ex.StackTrace);}
        }

        private void ponerANULLIdDireccionPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string idDirent = "";

                foreach (DataGridViewRow fila in dgvDirecciones.SelectedRows)
                {
                    idDirent = fila.Cells["IDDIRENT"].Value.ToString().Replace(",0000","");
                    csUtilidades.ejecutarConsulta("UPDATE DIRENT SET ID_PS=NULL WHERE IDDIRENT = " + idDirent, false);
                }
                

            }
            catch { }
        }



        private void actualizarImportePedidoMinimoPS()
        {
            try
            {
                if (MessageBox.Show("Vas a actualizar datos. ¿Estas seguro/a?", "ATENCION", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    double importePedidoMinimo= 0;
                    string id_customer = "";

                    if (csUtilidades.existeColumnaSQL("__CLIENTES", "KLS_PEDIDO_MINIMO"))
                    {
                        foreach (DataGridViewRow fila in dgvClientes.SelectedRows)
                        {
                            importePedidoMinimo = Convert.ToDouble(fila.Cells["KLS_PEDIDO_MINIMO"].Value.ToString());
                            id_customer = fila.Cells["KLS_CODCLIENTE"].Value.ToString().Trim();
                            if (!string.IsNullOrEmpty(id_customer) && importePedidoMinimo > 0)
                            {
                                csUtilidades.ejecutarConsulta("update ps_customer set purchase_minimum=" + importePedidoMinimo.ToString() + " where id_customer=" + id_customer, true);
                            }
                            
                        }
                        cargarClientesA3();
                    }
                    else
                    {
                        "Este módulo no está configurado".mb();
                    }
                }

            }
            catch { }

        }

        private void actualizarImportePedidoMínimoEnPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizarImportePedidoMinimoPS();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            csMySqlConnect mysql = new csMySqlConnect();
            csSqlConnects sql = new csSqlConnects();
            List<string> values = new List<string>();

            if (MessageBox.Show("¿Desea sincronizar los IDs de las direcciones de A3 a partir de PS?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                //Ponemos a null los campos de ID_PS para actualizarlos luego

                csUtilidades.ejecutarConsulta("update DIRENT set ID_PS=NULL",false);
                DataTable ps_final = mysql.cargarTabla("select * from ps_address where kls_id_dirent>0");

                sql.abrirConexion();
                foreach (DataRow valor in ps_final.Rows) {
                    try
                    {
                        sql.actualizarCampo("update DIRENT set ID_PS=" + valor["id_address"] + " where IDDIRENT=" + valor["kls_id_dirent"], false);
                        //values.Add("update DIRENT set ID_PS="+valor["id_address"]+" where IDDIRENT="+ valor["kls_id_dirent"]);
                    }
                    catch (Exception ex) {
                        
                    }
                }
                sql.cerrarConexion();
                //Ejecutar la consulta y listo. Esperar a que este Lluís por si acaso
                
            }
        }

        private void actualizarFamiliaDeClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csPrecios clasePrecios = new csPrecios();

            if (MessageBox.Show("Va a actualizar las familias de precios especiales de clientes en Prestashop ¿Desea continuar?", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes) {
                clasePrecios.actualizarFamiliasClientesPS();
            }

        }

        private void contextProviPS_Opening(object sender, CancelEventArgs e)
        {

        }

        private void statusStripClientes_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void rossoSincDirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable clientes, direccionesA3, direccionesPs;
            string idCli;
            //csUtilidades.DataTable2CSV((DataTable)dgvClientes.DataSource, ";", "Exportacion de clientes");
            int i = 0;
            clientes = sql.cargarTabla("CLIENTES","CODCLI");
            foreach(DataRow cli in clientes.Rows)
            {
                idCli = cli["CODCLI"].ToString().Trim();
                if (idCli!="NULL")
                {
                    direccionesA3 = sql.cargarDatosTablaA3("SELECT IDDIRENT FROM DIRENT WHERE LTRIM(CODCLI) = '" + idCli + "'");
                    direccionesPs = mysql.cargarTabla("SELECT id_address FROM ps_address WHERE id_customer='"+ idCli +"'");
                    if (direccionesA3.Rows.Count==1 && direccionesPs.Rows.Count==1)
                    {
                        string idDirA3 = direccionesA3.Rows[0][0].ToString().Split(',')[0];
                        string idDirPs = direccionesPs.Rows[0][0].ToString();
                        sql.actualizarCampo("UPDATE DIRENT SET ID_PS='"+ idDirPs + "' WHERE LTRIM(CODCLI) = '" + idCli + "'");
                        mysql.ejecutarConsulta("UPDATE ps_address SET kls_id_dirent='"+ idDirA3  + "' WHERE id_customer=" + idCli );
                        i++;
                    }
                }
            }
            MessageBox.Show("Se han sincronizado " + i + " direcciones");
        }

        private void rossoSincToolStripMenuItem_Click(object sender, EventArgs e)
        {
            csSqlConnects sql = new csSqlConnects();
            csMySqlConnect mysql = new csMySqlConnect();
            DataTable clientesA3, clientesPs;
            string idCli,email;
            //csUtilidades.DataTable2CSV((DataTable)dgvClientes.DataSource, ";", "Exportacion de clientes");
            int i = 0;
            clientesA3 = sql.cargarTabla("CLIENTES", "CODCLI, E_MAIL_DOCS");

            foreach (DataRow cli in clientesA3.Rows)
            {
                idCli = cli["CODCLI"].ToString().Trim();
                email = cli["E_MAIL_DOCS"].ToString().Trim();
                clientesPs = mysql.cargarTabla("select id_customer, email from ps_customer where id_customer=" + idCli);

                if (idCli != "NULL")
                {
                    string kk = clientesPs.Rows[0][1].ToString();
                    if (email==clientesPs.Rows[0][1].ToString())
                    {
                        sql.actualizarCampo("UPDATE CLIENTES SET KLS_CODCLIENTE='" + clientesPs.Rows[0][0].ToString() + "' WHERE LTRIM(CODCLI) = '" + idCli + "'");
                        mysql.ejecutarConsulta("UPDATE ps_customer SET kls_a3erp_id='" + idCli + "' WHERE id_customer=" + idCli + " AND email='" + email + "'");
                    }
                    i++;
                }
            }
            MessageBox.Show("Se han sincronizado " + i + " clientes");
        }

        private void contextClientes_Opening(object sender, CancelEventArgs e)
        {

        }

        private void actualizarCampoKlsa3erpidVacíoYSiExisteEnA3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvClientes.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Va a actualizar el código externo de Prestashop ¿Desea continuar?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    csUtilidades.actualizarKlsA3erpId(dgvClientes);
                }
            }
            else
            {
                MessageBox.Show("No se han seleccionado clientes para actualizar.");
            }
        }

        private void utilidadesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void actualizarManualmenteCodA3ERPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FUNCIÓN PARA PODER EDITAR EL CODIGO EXTERNO DE EL CLIENTE DE PRESTASHOP Y EL CODCLI EN  A3

        }
    }
}
