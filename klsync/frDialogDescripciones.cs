﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace klsync
{
    public partial class frDialogDescripciones : Form
    {
        public frDialogDescripciones(DataTable dtListBox, DataTable dtCombo, string campoComboBox, string labelListBox, string labelCombo,  bool cadena = false)
        {
            InitializeComponent();
            lblListBox.Text = labelListBox;
            lblCombo.Text = labelCombo;
            csUtilidades.poblarCombo(combo, dtCombo, campoComboBox, cadena);

            lb.DataSource = dtListBox;
            lb.ValueMember = "code";
            lb.DisplayMember = "name";

            for (int i = 0; i < lb.Items.Count; i++)
            {
                lb.SetItemChecked(i, true);
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (lb.CheckedItems.Count > 0)
            {
                this.DialogResult = DialogResult.OK;

                //Se han marcado todas las opciones, name, short, longdesc
                if (lb.CheckedItems.Count == lb.Items.Count)
                {
                    csGlobal.multiOptionFormValue = 0;
                }

                //Sólo marcado name
                if (lb.CheckedItems.Count==1 && lb.GetItemCheckState(0) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 1;                    
                }
                //Descripcion larga
                if (lb.CheckedItems.Count == 1 && lb.GetItemCheckState(1) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 2;
                }

                //Descripcion corta
                if (lb.CheckedItems.Count == 1 && lb.GetItemCheckState(2) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 3;
                }

                if (lb.CheckedItems.Count == 2 && lb.GetItemCheckState(0) == CheckState.Checked && lb.GetItemCheckState(1) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 4;
                }

                if (lb.CheckedItems.Count == 2 && lb.GetItemCheckState(0) == CheckState.Checked && lb.GetItemCheckState(2) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 5;
                }

                if (lb.CheckedItems.Count == 2 && lb.GetItemCheckState(1) == CheckState.Checked && lb.GetItemCheckState(2) == CheckState.Checked)
                {
                    csGlobal.multiOptionFormValue = 6;
                }

                csGlobal.comboFormValue = combo.Text;
            }
            else
            {
                MessageBox.Show("Debes seleccionar algun campo para actualizar\n\nOperacion cancelada.");
                csGlobal.comboFormValue = "";
            }

            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                this.Close();

                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
